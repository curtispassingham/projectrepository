
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE COMPETITOR_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------
-- Name:       COMPETITOR_EXIST
-- Purpose:    This function determines if a competitor number is in use in 
--             the system.
-----------------------------------------------------------------------------
FUNCTION COMPETITOR_EXIST(O_error_message  IN OUT  VARCHAR2,
                          O_exist          IN OUT  BOOLEAN,
                          I_competitor     IN      COMPETITOR.COMPETITOR%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Name:       COMP_STORE_EXIST
-- Purpose:    This function determines if a competitor store number is in use in 
--             the system.
-----------------------------------------------------------------------------
FUNCTION COMP_STORE_EXIST(O_error_message  IN OUT  VARCHAR2,
                          O_exist          IN OUT  BOOLEAN,
                          I_comp_store     IN      COMP_STORE.STORE%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Name:       COMP_SHOPPER_EXIST
-- Purpose:    This function determines if a competitor shopper number is in use in 
--             the system.
-----------------------------------------------------------------------------
FUNCTION COMP_SHOPPER_EXIST(O_error_message  IN OUT  VARCHAR2,
                            O_exist          IN OUT  BOOLEAN,
                            I_comp_shopper   IN      COMP_SHOPPER.SHOPPER%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------- 
-- Name:       GET_NAME
-- Purpose:    This function retrieves the competitor name for the passed in
--             competitor.
-----------------------------------------------------------------------------
FUNCTION GET_NAME(O_error_message IN OUT VARCHAR2,
                  O_comp_name     IN OUT competitor.comp_name%TYPE,
                  I_competitor    IN     competitor.competitor%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------- 
-- Name:       GET_STORE_NAME
-- Purpose:    This function retrieves the competitor store name for the 
--             passed in competitor store.
-----------------------------------------------------------------------------
FUNCTION GET_STORE_NAME(O_error_message IN OUT VARCHAR2,
                        O_store_name    IN OUT comp_store.store_name%TYPE,
                        O_competitor    IN OUT competitor.competitor%TYPE,
                        O_comp_name     IN OUT competitor.comp_name%TYPE,
                        O_currency      IN OUT comp_store.currency_code%TYPE,
                        I_comp_store    IN     comp_store.store%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------- 
-- Name:       GET_SHOPPER_NAME
-- Purpose:    This function retrieves the shopper name for the passed in
--             shopper.
-----------------------------------------------------------------------------
FUNCTION GET_SHOPPER_NAME(O_error_message IN OUT VARCHAR2,
                          O_shop_name     IN OUT comp_shopper.shopper_name%TYPE,
                          I_shopper       IN     comp_shopper.shopper%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Name:    NEXT_COMP_NUMBER
-- Purpose: This Competitor number sequence generator will return to the calling 
--          program/procedure.  Upon success (TRUE) a competitor number will be returned.
--          Upon failure (FALSE) an appropriate 
--          error message for display purposes by the calling program/procedure.
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION NEXT_COMP_NUMBER( O_error_message IN OUT VARCHAR2,
                           O_comp_number   IN OUT NUMBER)
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Name:    NEXT_COMP_ST_NUMBER
-- Purpose: This Competitor Store number sequence generator will return to the calling 
--          program/procedure.  Upon success (TRUE) a competitor store number will be returned.
--          Upon failure (FALSE) an appropriate 
--          error message for display purposes by the calling program/procedure.
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION NEXT_COMP_ST_NUMBER(O_error_message IN OUT VARCHAR2,
                             O_store_number   IN OUT NUMBER)
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Name:    NEXT_SHOPPER_NUMBER
-- Purpose: This Competitor Shopper number sequence generator will return to the calling 
--          program/procedure.  Upon success (TRUE) a competitor shopper number will be returned.
--          Upon failure (FALSE) an appropriate 
--          error message for display purposes by the calling program/procedure.
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION NEXT_SHOPPER_NUMBER(O_error_message    IN OUT VARCHAR2,
                             O_shopper_number   IN OUT NUMBER)
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Name:    CHECK_COMP_DELETE
-- Purpose: This function checks for conflicts when a competitor is deleted. 
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION CHECK_COMP_DELETE (O_error_message IN OUT  VARCHAR2,
                            O_exists        IN OUT  BOOLEAN,
                            I_competitor	  IN	competitor.competitor%TYPE)
  RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Name:    CHECK_COMP_ST_DELETE
-- Purpose: This function checks for conflicts when a competitor store is deleted. 
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION CHECK_COMP_ST_DELETE (O_error_message IN OUT  VARCHAR2,
                            O_exists        IN OUT  BOOLEAN,
                            I_comp_store	  IN	competitor.competitor%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Name:    CHECK_COMP_SHOPPER_DELETE
-- Purpose: This function checks for conflicts when a shopper is deleted. 
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION CHECK_COMP_SHOPPER_DELETE(O_error_message IN OUT  VARCHAR2,
                            O_exists        IN OUT  BOOLEAN,
                            I_shopper	  IN	comp_shopper.shopper%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Name:       CREATE_SHOP_LIST
-- Purpose:    This function will populate the comp_shop_list_values table using the
--             the passed in itemlist and the values on the comp_list_temp table.
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION CREATE_SHOP_LIST(O_error_message IN OUT  VARCHAR2,
                          I_itemlist      IN      skulist_head.skulist%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Name:       COMP_STORE_LINK_EXIST
-- Purpose:    This function determines if competitor store and store numbers   
--             exist in comp_store_link table.
-----------------------------------------------------------------------------
FUNCTION COMP_STORE_LINK_EXIST(O_error_message  IN OUT  VARCHAR2,
                               O_exist          IN OUT  BOOLEAN,
                               I_comp_store     IN      comp_store_link.comp_store%TYPE,
                               I_store          IN      comp_store_link.store%TYPE)

RETURN BOOLEAN;

-----------------------------------------------------------------------------
END COMPETITOR_SQL;
/


