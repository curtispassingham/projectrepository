
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY POS_CONFIG_LOCK_SQL AS

---------------------------------------------------------------------------
--- FUNCTION:  LOCK_POS_COUPON_HEAD
--- Purpose:   Locks entries so users can not edit the same id at the same time.
--- Called By: When Tab Page Changed and When Mouse Doubleclick
---------------------------------------------------------------------------
FUNCTION LOCK_POS_COUPON_HEAD (O_error_message IN OUT VARCHAR2,
                               O_lock_ind      IN OUT BOOLEAN,
                               I_config_id     IN     POS_COUPON_HEAD.COUPON_ID%TYPE)
RETURN BOOLEAN IS

   L_table         VARCHAR2(30)                          := 'POS_COUPON_HEAD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_POS_COUPON_HEAD is
      select 'x'
        from pos_coupon_head
       where coupon_id = I_config_id
         for update nowait;

BEGIN
   if I_config_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',NULL,NULL,NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('OPEN','C_LOCK_POS_COUPON_HEAD','POS_COUPON_HEAD', 'coupon id: '||TO_CHAR(I_config_id));
      open C_LOCK_POS_COUPON_HEAD;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_POS_COUPON_HEAD','POS_COUPON_HEAD','coupon id: '||TO_CHAR(I_config_id));
      close C_LOCK_POS_COUPON_HEAD;
      O_lock_ind := TRUE;
      return TRUE;
   end if;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('POS_TABLE_LOCKED',
                                             L_table,
                                             TO_CHAR(I_config_id),
                                             NULL);
      O_lock_ind := FALSE;
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'POS_CONFIG_LOCK_SQL.LOCK_COUPON_HEAD',
                                             TO_CHAR(SQLCODE));
      return FALSE;

END LOCK_POS_COUPON_HEAD;
---------------------------------------------------------------------------
--- FUNCTION:  LOCK_POS_PROD_REST_HEAD
--- Purpose:   Locks entries so users can not edit the same id at the same time.
--- Called By: When Tab Page Changed and When Mouse Doubleclick
---------------------------------------------------------------------------
FUNCTION LOCK_POS_PROD_REST_HEAD (O_error_message IN OUT VARCHAR2,
                                  O_lock_ind      IN OUT BOOLEAN,
                                  I_config_id     IN     POS_PROD_REST_HEAD.POS_PROD_REST_ID%TYPE)
RETURN BOOLEAN IS

   L_table         VARCHAR2(30)                          := 'POS_PROD_REST_HEAD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_POS_PROD_REST_HEAD is
      select 'x'
        from pos_prod_rest_head
       where pos_prod_rest_id = I_config_id
         for update nowait;

BEGIN
   if I_config_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',NULL,NULL,NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('OPEN','C_LOCK_POS_PROD_REST_HEAD','POS_PROD_REST_HEAD','pos_prod_rest_id: '||TO_CHAR(I_config_id));
      open C_LOCK_POS_PROD_REST_HEAD;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_POS_PROD_REST_HEAD','POS_PROD_REST_HEAD','pos_prod_rest_id: '||TO_CHAR(I_config_id));
      close C_LOCK_POS_PROD_REST_HEAD;
      O_lock_ind := TRUE;
      return TRUE;
   end if;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('POS_TABLE_LOCKED',
                                             L_table,
                                             TO_CHAR(I_config_id),
                                             NULL);
      O_lock_ind := FALSE;
      return TRUE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'POS_CONFIG_LOCK_SQL.LOCK_PROD_REST_HEAD',
                                             TO_CHAR(SQLCODE));
      return FALSE;

END LOCK_POS_PROD_REST_HEAD;
---------------------------------------------------------------------------
--- FUNCTION:  LOCK_POS_TENDER_TYPE_HEAD
--- Purpose:   Locks entries so users can not edit the same id at the same time.
--- Called By: When Tab Page Changed and When Mouse Doubleclick
---------------------------------------------------------------------------
FUNCTION LOCK_POS_TENDER_TYPE_HEAD (O_error_message IN OUT VARCHAR2,
                                    O_lock_ind      IN OUT BOOLEAN,
                                    I_config_id     IN     POS_TENDER_TYPE_HEAD.TENDER_TYPE_ID%TYPE)
RETURN BOOLEAN IS

   L_table         VARCHAR2(30)                          := 'POS_TENDER_TYPE_HEAD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_POS_TENDER_TYPE_HEAD is
      select 'x'
        from pos_tender_type_head
       where tender_type_id = I_config_id
         for update nowait;

BEGIN
   if I_config_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',NULL,NULL,NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('OPEN','C_LOCK_POS_TENDER_TYPE_HEAD','POS_TENDER_TYPE_HEAD','tender_type_id:'||TO_CHAR(I_config_id));
      open C_LOCK_POS_TENDER_TYPE_HEAD;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_POS_TENDER_TYPE_HEAD','POS_TENDER_TYPE_HEAD','tender_type_id:'||TO_CHAR(I_config_id));
      close C_LOCK_POS_TENDER_TYPE_HEAD;
      O_lock_ind := TRUE;
      return TRUE;
   end if;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('POS_TABLE_LOCKED',
                                             L_table,
                                             TO_CHAR(I_config_id),
                                             NULL);
      O_lock_ind := FALSE;
      return TRUE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'POS_CONFIG_LOCK_SQL.LOCK_TENDER_TYPE_HEAD',
                                             TO_CHAR(SQLCODE));
      return FALSE;

END LOCK_POS_TENDER_TYPE_HEAD;
---------------------------------------------------------------------------
END POS_CONFIG_LOCK_SQL;
/


