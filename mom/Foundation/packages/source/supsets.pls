CREATE OR REPLACE PACKAGE SUPPLIER_SETUP_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------
-- Function Name : LOCK_SUPS
-- Purpose       : Lock record in SUPS table for given supplier.
---------------------------------------------------------------------
FUNCTION LOCK_SUPS (O_error_message IN OUT VARCHAR2,
                    I_supplier      IN     SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name : DELETE_CHILD_RECORDS
-- Purpose       : Delete all child records for a given supplier.
---------------------------------------------------------------------
FUNCTION DEL_CHILD_RECORDS
                  (O_error_message   IN OUT  VARCHAR2,
                   I_supplier        IN      SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name : INSERT_SUPPLIER_SITE_ADDRESS
-- Purpose       : Inserts all address for the Supplier Parent to the Supplier.
---------------------------------------------------------------------
FUNCTION INSERT_SUPPLIER_SITE_ADDRESS (O_error_message   IN OUT VARCHAR2,
                                       I_supplier        IN     SUPS.SUPPLIER%TYPE,
                                       I_supplier_parent IN     SUPS.SUPPLIER_PARENT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION DEL_SUPS_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_supplier        IN       SUPS_L10N_EXT.SUPPLIER%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
-- Function: CUSTOM_VAL
-- Purpose : This function will be used for validationg the custom function.
--------------------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_supplier_id       IN       SUPS.SUPPLIER%TYPE,
                    I_function_key      IN       VARCHAR2,
                    I_seq_no            IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END SUPPLIER_SETUP_SQL;
/


