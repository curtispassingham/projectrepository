
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BUYER_ATTRIB_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-- Name:    GET_NAME
-- Purpose: To to get the buyer name from the BUYER table for a given buyer ID.
-- Created By: Julian Aggerbeck, 23-SEP-96.
-------------------------------------------------------------------------------
FUNCTION GET_NAME(I_BUYER         IN NUMBER,
                  O_BUYER_NAME    IN OUT VARCHAR2,
                  O_error_message IN OUT VARCHAR2) return BOOLEAN;

-------------------------------------------------------------------------------
-- Name:      EXIST
-- Purpose:   Takes a buyer number and checks for existence on the buyer
--		  table.  Returns boolean TRUE or FALSE.
-- Created By: Matt Sniffen, 17-OCT-96
-------------------------------------------------------------------------------
FUNCTION EXIST(   I_BUYER         IN NUMBER,
                  O_EXIST         IN OUT BOOLEAN,
                  O_error_message IN OUT VARCHAR2) 
	return BOOLEAN;
-------------------------------------------------------------------------------
-- Function : BUYER_DETAILS
-- Purpose  : This function will return attributes related to the buyer
-------------------------------------------------------------------------------
FUNCTION BUYER_DETAILS(O_error_message IN OUT VARCHAR2,
                       O_buyer_name    IN OUT buyer.buyer_name%TYPE,
                       O_buyer_phone   IN OUT buyer.buyer_phone%TYPE,
                       O_buyer_fax     IN OUT buyer.buyer_fax%TYPE,
                       I_buyer         IN     buyer.buyer%TYPE)
                       RETURN BOOLEAN;
-------------------------------------------------------------------------------
END BUYER_ATTRIB_SQL;
/


