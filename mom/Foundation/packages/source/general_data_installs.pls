
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE GENERAL_DATA_INSTALL AUTHID CURRENT_USER AS
-----------------------------------------------------------------
FUNCTION VAT_CODE_REGION(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION SYSTEM_VARIABLES(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION BANNER(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION CHANNEL(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION BUYERS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION BRAND(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------
FUNCTION MERCHANTS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION COMPHEAD(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION LOCHIER(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION TSF_ZONE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION TSF_ENTITY(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION STORE_FORMAT(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION WHS(O_error_message      IN OUT   VARCHAR2,
             I_default_tax_type   IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION STORES(O_error_message      IN OUT   VARCHAR2,
                I_default_tax_type   IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION SUPS_ADDR(O_error_message         IN OUT   VARCHAR2,
                   I_default_tax_type      IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION CAL454(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION UDAS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION DIFFS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION DEAL_COMP_TYPE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION COST_ZONE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION DIVISION(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION GROUPS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION DEPT(O_error_message      IN OUT   VARCHAR2,
              I_default_tax_type   IN       VARCHAR2,
              I_vat_class_ind      IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION CLASS_SUBCLASS(O_error_message      IN OUT   VARCHAR2,
                        I_default_tax_type   IN       VARCHAR2,
                        I_vat_class_ind      IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION RPM_MERCH_HIER(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION PO_TYPE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION OUTLOC(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION RTK_ROLE_PRIVS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION FREIGHT_TYPE_SIZE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION DOC(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION POS_TENDER_TYPE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION NON_MERCH_CODES(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION FIF_GL_SETUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION ORG_UNIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION TSF_ENTITY_ORG_UNIT_SOB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION REFRESH_MV_LOC_SOB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION REFRESH_MV_CURRENCY_CONV_RATES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION FREIGHT_TERMS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION PAYMENT_TERMS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION SUPS_ADDR_SUP_SITE(O_error_message         IN OUT   VARCHAR2,
                            I_default_tax_type      IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------
FUNCTION WF_CUSTOMER_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION WF_CUSTOMER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION PARTNER_ORG_UNIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION ITEM_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_demo_data_ind        IN VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
---------------------------------------------------------------------------
FUNCTION FUTURE_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN ;
-----------------------------------------------------------------------------

END GENERAL_DATA_INSTALL;
/
