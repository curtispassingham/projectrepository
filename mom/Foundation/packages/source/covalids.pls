CREATE OR REPLACE PACKAGE COUNTRY_VALIDATE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
-- Name:          EXISTS_ON_TABLE
-- Purpose:       Check to see if the country_id already exists in the country table for
--                a given country_id and a table name.                
-------------------------------------------------------------------------------------------
   FUNCTION EXISTS_ON_TABLE(I_country_id    IN     country.country_id%TYPE,
                              I_table         IN     VARCHAR2,
                              O_error_message IN OUT VARCHAR2,
               O_exists IN OUT BOOLEAN)
      RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:          CONSTRAINTS_EXIST
-- Purpose:       Check to see if the country_id exists in the ordhead table
--                and the sups_add table for a given country_id.
-------------------------------------------------------------------------------------------
  FUNCTION CONSTRAINTS_EXIST(I_Foreign_Constraints  IN country.country_id%TYPE,
                             O_error_message      IN OUT VARCHAR2,
              O_found                IN OUT BOOLEAN)
      RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:    GET_NAME 
-- Purpose: Returns the corresponding name for the country_id.
-------------------------------------------------------------------------------------------
  FUNCTION GET_NAME(O_error_message    IN OUT VARCHAR2,
                    I_country_id       IN     country.country_id%TYPE,
                    O_country_desc     IN OUT VARCHAR2) 
      RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:     GET_COUNTRY_ATTRIB
-- Purpose:  This function will retrieve the country attributes from the COUNTRY_ATTRIB
--           table for a given country id or if location is passed then it would retrieve
--           the country of the location first and then retrieve the country attributes.
-----------------------------------------------------------------------------------
FUNCTION GET_COUNTRY_ATTRIB(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_country_attrib_row   IN OUT   COUNTRY_ATTRIB%ROWTYPE,
                            I_default_loc          IN       COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                            I_country_id           IN       COUNTRY_ATTRIB.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:     GET_COUNTRY_ATTRIB
-- Purpose:  This Overloaded function function will retrieve the country attributes from
--           the COUNTRY_ATTRIB table for a given country id or if location is passed then 
--           it would retrieve the country of the location first and then retrieve the country
--           attributes.The output variables are single columns of %TYPE instead of %ROWTYPE.
-----------------------------------------------------------------------------------
FUNCTION GET_COUNTRY_ATTRIB(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_localized_ind            IN OUT   COUNTRY_ATTRIB.LOCALIZED_IND%TYPE,
                            O_item_cost_tax_incl_ind   IN OUT   COUNTRY_ATTRIB.ITEM_COST_TAX_INCL_IND%TYPE,
                            O_default_po_cost          IN OUT   COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE,
                            O_default_deal_cost        IN OUT   COUNTRY_ATTRIB.DEFAULT_DEAL_COST%TYPE,
                            O_default_cost_comp_cost   IN OUT   COUNTRY_ATTRIB.DEFAULT_COST_COMP_COST%TYPE,
                            O_default_loc              IN OUT   COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                            O_default_loc_type         IN OUT   COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE,
                            I_default_loc              IN       COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                            I_country_id               IN       COUNTRY_ATTRIB.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--FUNCTION NAME: DEL_COUNTRY_ATTRIB
--Purpose:       This function will delete the record from the COUNTRY_L10N_EXT table
--               for the given country.
-----------------------------------------------------------------------------------
FUNCTION DEL_COUNTRY_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_KEY%TYPE,
                            I_country_id      IN       COUNTRY_ATTRIB.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------   
END;
/

