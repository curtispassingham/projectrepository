
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRRCLS_VALIDATE AS

----------------------------------------------------------------------------
   -- LOCAL PACKAGE VARIABLES
----------------------------------------------------------------------------
   LP_action_add   VARCHAR2(1) := 'A';
   LP_action_mod   VARCHAR2(1) := 'M';

   -- valid purchase types
   LP_normal_merchandise NUMBER(1) := 0;
   LP_consignment_stock  NUMBER(1) := 1;

   -- valid profic calc types
   LP_direct_cost        NUMBER(1) := 1;
   LP_retail_inventory   NUMBER(1) := 2;

   -- valid otb and markup calc types
   LP_cost               VARCHAR2(1) := 'C';
   LP_retail             VARCHAR2(1) := 'R';
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This overloaded private function should check all required
   --                fields in the create or modify message type to ensure that the
   --                message structure is correct.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrRclsDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This overloaded private function should check all required
   --                fields in the delete message to ensure that the message
   --                structure is correct.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrRclsRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_EXISTENCE
   -- Purpose      : This private function should check the existence of a merchandise
   --                hierarchy based on the message type and action type.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTENCE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_message         IN       "RIB_XMrchHrRclsDesc_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_EXISTENCE_CREATE
   -- Purpose      : This private function should check the existence of a merchandise
   --                hierarchy for a create message.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTENCE_CREATE(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                                I_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                                I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                                I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE,
                                I_action_type           IN       PEND_MERCH_HIER.ACTION_TYPE%TYPE,
                                I_effective_date        IN       DATE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_EXISTENCE_MODIFY
   -- Purpose      : This private function should check the existence of a merchandise
   --                hierarchy based for a modify message.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTENCE_MODIFY(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                                I_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                                I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                                I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE,
                                I_effective_date        IN       DATE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_EXISTENCE_PARENT
   -- Purpose      : This private function should check the existence of parent
   --                hierarchy of a certain hierarchy type.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTENCE_PARENT(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                                I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                                I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE,
                                I_effective_date        IN       DATE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This overloaded private function copies the values from the message object
   --                "RIB_XMrchHrRclsDesc_REC" to the business object
   --                O_pend_merch_hier_rec, which is a fully populated
   --                PEND_MERCH_HIER record.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message         IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_pend_merch_hier_rec   OUT    NOCOPY   PEND_MERCH_HIER%ROWTYPE,
                         I_message               IN              "RIB_XMrchHrRclsDesc_REC")
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This overloaded private function copies the values from the message object
   --                "RIB_XMrchHrRclsRef_REC" to the business object
   --                O_pend_merch_hier_rec, which is a thinly populated
   --                PEND_MERCH_HIER record with only the unique keys
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message         IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_pend_merch_hier_rec   OUT    NOCOPY   PEND_MERCH_HIER%ROWTYPE,
                         I_message               IN              "RIB_XMrchHrRclsRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_DELETE
   -- Purpose      : This private function checks if the PEND_MERCH_HIER record
   --                can be deleted.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message         IN       "RIB_XMrchHrRclsRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message         IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_pend_merch_hier_rec   OUT    NOCOPY   PEND_MERCH_HIER%ROWTYPE,
                       I_message               IN              "RIB_XMrchHrRclsDesc_REC",
                       I_message_type          IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'RMSSUB_XMRCHHRRCLS_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message,
                                I_message_type) then
      return FALSE;
   end if;

   if not SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) then
      return FALSE;
   end if;

   if not CHECK_EXISTENCE(O_error_message,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_pend_merch_hier_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message         IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_pend_merch_hier_rec   OUT    NOCOPY   PEND_MERCH_HIER%ROWTYPE,
                       I_message               IN              "RIB_XMrchHrRclsRef_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRRCLS_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not CHECK_DELETE(O_error_message,
                       I_message) then
         return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_pend_merch_hier_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrRclsDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRRCLS_VALIDATE.CHECK_REQUIRED_FIELDS';

   L_date      DATE         := GET_VDATE;

BEGIN

   --REQUIRED TO ALL HIERARCHY LEVELS
   if I_message.effective_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'effective_date',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.merch_hier_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'merch_hier_level',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.merch_hier_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'merch_hier_id',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.merch_hier_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'merch_hier_name',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.merch_hier_parent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                               'merch_hier_parent_id',
                                               NULL,
                                               NULL);
         return FALSE;
   end if;

   if I_message.effective_date < L_date then
      O_error_message := SQL_LIB.CREATE_MSG('EDATE_EQUAL_AFTER_TODAY',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.merch_hier_level not in (MERCH_RECLASS_VALIDATE_SQL.LP_div_code,
                                         MERCH_RECLASS_VALIDATE_SQL.LP_grp_code,
                                         MERCH_RECLASS_VALIDATE_SQL.LP_dept_code,
                                         MERCH_RECLASS_VALIDATE_SQL.LP_cls_code,
                                         MERCH_RECLASS_VALIDATE_SQL.LP_scls_code) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_MERCH_LEVEL',
                                            I_message.merch_hier_level,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---

   --REQUIRED FIELDS SPECIFIC TO HIERARCHY LEVELS

   -- Dept Required fields
   if I_message.merch_hier_level = MERCH_RECLASS_VALIDATE_SQL.LP_dept_code then
      if I_message.max_avg_counter is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_RPM_Y',
                                               'max_avg_counter',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.max_avg_counter < 0 then
         O_error_message := SQL_LIB.CREATE_MSG('NOT_NEGATIVE',
                                               'max_avg_counter',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.avg_tolerance_pct is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_RPM_Y',
                                               'avg_tolerance_pct',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.avg_tolerance_pct < 0 then
         O_error_message := SQL_LIB.CREATE_MSG('NOT_NEGATIVE',
                                               'avg_tolerance_pct',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.merch_hier_grandparent_id is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'merch_hier_grandparent_id',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.purchase_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                               'purchase_type',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.purchase_type not in (LP_normal_merchandise, LP_consignment_stock) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DEPT_PURCH_TYPE',
                                               I_message.purchase_type,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.profit_calc_type is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                                  'profit_calc_type',
                                                  NULL,
                                                  NULL);
            return FALSE;
      end if;

      if I_message.profit_calc_type not in (LP_direct_cost, LP_retail_inventory) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DEPT_PROF_CALC_TYPE',
                                               I_message.profit_calc_type,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.purchase_type = LP_consignment_stock AND
         I_message.profit_calc_type = LP_direct_cost then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PROF_CALC_PURCH_TYPE',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if (I_message.bud_mkup is NULL and I_message.bud_int is NULL) or
         (I_message.bud_mkup is not NULL and I_message.bud_int is not NULL) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_BUD_MKUP_INT',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.markup_calc_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                               'markup_calc_type',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.markup_calc_type not in (LP_cost, LP_retail) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_MKUP_CALC_TYPE',
                                               I_message.markup_calc_type,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.otb_calc_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                               'otb_calc_type',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.otb_calc_type not in (LP_cost, LP_retail) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_OTB_CALC_TYPE',
                                               I_message.otb_calc_type,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.class_vat_ind is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'class_vat_ind',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

   --Non-department required fields
   else
      -- Class Required field
      if I_message.merch_hier_level != MERCH_RECLASS_VALIDATE_SQL.LP_cls_code then
         if I_message.class_vat_ind is not NULL then
            O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                                  'class_vat_ind',
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      end if;

      -- Subclass Required field
      if I_message.merch_hier_level = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code and
         I_message.merch_hier_grandparent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('HIER_GRANDPRNT_EXP',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.merch_hier_level != MERCH_RECLASS_VALIDATE_SQL.LP_scls_code and
         I_message.merch_hier_grandparent_id is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('HIER_GRANDPRNT_NOT_EXP',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.profit_calc_type is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'profit_calc_type',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.purchase_type is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'purchase_type',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.bud_int is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'bud_int',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.bud_mkup is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'bud_mkup',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.markup_calc_type is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'markup_calc_type',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.otb_calc_type is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'otb_calc_type',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.dept_vat_incl_ind is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'dept_vat_incl_ind',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.merch_hier_level != MERCH_RECLASS_VALIDATE_SQL.LP_div_code and 
         I_message.total_market_amt is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'total_market_amt',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.max_avg_counter is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'max_avg_counter',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.avg_tolerance_pct is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'avg_tolerance_pct',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;
   ---

   --CHECK FOR ACTION TYPE
   if I_message_type = RMSSUB_XMRCHHRRCLS.LP_cre_type then
      if I_message.action_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                               'action_type',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.action_type not in (LP_action_add, LP_action_mod) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ACTION_TYPE',
                                               I_message.action_type,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRRCLS.LP_mod_type then
      if I_message.action_type is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
                                               'action_type',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrRclsRef_REC")

   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'RMSSUB_XMRCHHRRCLS_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.merch_hier_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'merch_hier_level',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.merch_hier_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'merch_hier_id',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.merch_hier_level not in (MERCH_RECLASS_VALIDATE_SQL.LP_div_code,
                                         MERCH_RECLASS_VALIDATE_SQL.LP_grp_code,
                                         MERCH_RECLASS_VALIDATE_SQL.LP_dept_code,
                                         MERCH_RECLASS_VALIDATE_SQL.LP_cls_code,
                                         MERCH_RECLASS_VALIDATE_SQL.LP_scls_code) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_MERCH_LEVEL',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.merch_hier_level = MERCH_RECLASS_VALIDATE_SQL.LP_cls_code then
      if I_message.merch_hier_parent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('HIER_PARENT_EXP',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   elsif I_message.merch_hier_level = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
      if I_message.merch_hier_parent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('HIER_PARENT_EXP',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.merch_hier_grandparent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('HIER_GRANDPRNT_EXP',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message         IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_pend_merch_hier_rec   OUT    NOCOPY   PEND_MERCH_HIER%ROWTYPE,
                         I_message               IN              "RIB_XMrchHrRclsDesc_REC")
   RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'RMSSUB_XMRCHHRRCLS_VALIDATE.POPULATE_RECORD';

   L_dept_vat_incl_ind  VARCHAR2(1) := NULL;
   L_class_vat_ind      VARCHAR2(1) := NULL;

   L_domain             PEND_MERCH_HIER.DOMAIN%TYPE := NULL;

   cursor C_DEPT_DOMAIN is
      select domain_id
        from domain_dept
       where dept = I_message.merch_hier_id;

   cursor C_CLASS_DOMAIN is
      select domain_id
        from domain_class
       where class = I_message.merch_hier_id
         and dept = I_message.merch_hier_parent_id;

   cursor C_SUBCLASS_DOMAIN is
      select domain_id
        from domain_subclass
       where subclass = I_message.merch_hier_id
         and class = I_message.merch_hier_parent_id
         and dept = I_message.merch_hier_grandparent_id;

BEGIN

   -- determine dept_vat_incl_ind based on system options
   if I_message.merch_hier_level = MERCH_RECLASS_VALIDATE_SQL.LP_dept_code then
      if SYSTEM_OPTIONS_SQL.GP_system_options_row.default_tax_type = 'SALES'  then
         L_dept_vat_incl_ind := 'N';
      elsif SYSTEM_OPTIONS_SQL.GP_system_options_row.CLASS_LEVEL_VAT_IND = 'N' then
         L_dept_vat_incl_ind := 'Y';
      elsif I_message.dept_vat_incl_ind is NULL then
         L_dept_vat_incl_ind := 'N';
      else
         L_dept_vat_incl_ind := I_message.dept_vat_incl_ind;
      end if;
   end if;

   -- determine class_vat_ind based on system options
   if I_message.merch_hier_level = MERCH_RECLASS_VALIDATE_SQL.LP_cls_code then
      if SYSTEM_OPTIONS_SQL.GP_system_options_row.default_tax_type = 'SALES'  then
         L_class_vat_ind := 'N';
      elsif SYSTEM_OPTIONS_SQL.GP_system_options_row.CLASS_LEVEL_VAT_IND = 'N' then
         L_class_vat_ind := 'Y';
      elsif I_message.class_vat_ind is NULL then
         L_class_vat_ind := 'N';
      else
         L_class_vat_ind := I_message.class_vat_ind;
      end if;
   end if;

   --- if a dept/class/subclass is being modified
   --- query the domain tables based on the system level
   --- domain grouping level

   if I_message.action_type = LP_action_mod then

      if I_message.merch_hier_level = SYSTEM_OPTIONS_SQL.GP_system_options_row.domain_level then

         if SYSTEM_OPTIONS_SQL.GP_system_options_row.domain_level = 'D' then

            SQL_LIB.SET_MARK('OPEN',
                             'C_DEPT_DOMAIN',
                             'DOMAIN_DEPT',
                             ' merch_hier_id: ' || TO_CHAR(I_message.merch_hier_id));
            open C_DEPT_DOMAIN;

            SQL_LIB.SET_MARK('FETCH',
                             'C_DEPT_DOMAIN',
                             'DOMAIN_DEPT',
                             ' merch_hier_id: ' || TO_CHAR(I_message.merch_hier_id));
            fetch C_DEPT_DOMAIN into L_domain;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_DEPT_DOMAIN',
                             'DOMAIN_DEPT',
                             ' merch_hier_id: ' || TO_CHAR(I_message.merch_hier_id));
            close C_DEPT_DOMAIN;

         elsif SYSTEM_OPTIONS_SQL.GP_system_options_row.domain_level = 'C' then

            SQL_LIB.SET_MARK('OPEN',
                             'C_CLASS_DOMAIN',
                             'DOMAIN_CLASS',
                             ' merch_hier_id: ' || TO_CHAR(I_message.merch_hier_id)||
                             ' merch_hier_parent_id: ' || TO_CHAR(I_message.merch_hier_parent_id));
            open C_CLASS_DOMAIN;

            SQL_LIB.SET_MARK('FETCH',
                             'C_CLASS_DOMAIN',
                             'DOMAIN_CLASS',
                             ' merch_hier_id: ' || TO_CHAR(I_message.merch_hier_id)||
                             ' merch_hier_parent_id: ' || TO_CHAR(I_message.merch_hier_parent_id));
            fetch C_CLASS_DOMAIN into L_domain;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_CLASS_DOMAIN',
                             'DOMAIN_CLASS',
                             ' merch_hier_id: ' || TO_CHAR(I_message.merch_hier_id)||
                             ' merch_hier_parent_id: ' || TO_CHAR(I_message.merch_hier_parent_id));
            close C_CLASS_DOMAIN;

         elsif SYSTEM_OPTIONS_SQL.GP_system_options_row.domain_level = 'S' then

            SQL_LIB.SET_MARK('OPEN',
                             'C_SUBCLASS_DOMAIN',
                             'DOMAIN_SUBCLASS',
                             ' merch_hier_id: ' || TO_CHAR(I_message.merch_hier_id)||
                             ' merch_hier_parent_id: ' || TO_CHAR(I_message.merch_hier_parent_id)||
                             ' merch_hier_grandparent_id: ' || TO_CHAR(I_message.merch_hier_grandparent_id));
            open C_SUBCLASS_DOMAIN;

            SQL_LIB.SET_MARK('FETCH',
                             'C_SUBCLASS_DOMAIN',
                             'DOMAIN_SUBCLASS',
                             ' merch_hier_id: ' || TO_CHAR(I_message.merch_hier_id)||
                             ' merch_hier_parent_id: ' || TO_CHAR(I_message.merch_hier_parent_id)||
                             ' merch_hier_grandparent_id: ' || TO_CHAR(I_message.merch_hier_grandparent_id));
            fetch C_SUBCLASS_DOMAIN into L_domain;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_SUBCLASS_DOMAIN',
                             'DOMAIN_SUBCLASS',
                             ' merch_hier_id: ' || TO_CHAR(I_message.merch_hier_id)||
                             ' merch_hier_parent_id: ' || TO_CHAR(I_message.merch_hier_parent_id)||
                             ' merch_hier_grandparent_id: ' || TO_CHAR(I_message.merch_hier_grandparent_id));
            close C_SUBCLASS_DOMAIN;
         else
            L_domain := NULL;
         end if;
      end if;
   end if;

   O_pend_merch_hier_rec.action_type               := I_message.action_type;
   O_pend_merch_hier_rec.hier_type                 := I_message.merch_hier_level;
   O_pend_merch_hier_rec.merch_hier_id             := I_message.merch_hier_id;
   O_pend_merch_hier_rec.merch_hier_name           := I_message.merch_hier_name;
   O_pend_merch_hier_rec.effective_date            := I_message.effective_date;
   O_pend_merch_hier_rec.buyer                     := I_message.buyer;
   O_pend_merch_hier_rec.domain                    := L_domain;
   O_pend_merch_hier_rec.purchase_type             := I_message.purchase_type;
   O_pend_merch_hier_rec.total_market_amt          := I_message.total_market_amt;
   O_pend_merch_hier_rec.merch                     := I_message.merch;
   O_pend_merch_hier_rec.bud_mkup                  := I_message.bud_mkup;
   O_pend_merch_hier_rec.profit_calc_type          := I_message.profit_calc_type;
   O_pend_merch_hier_rec.markup_calc_type          := I_message.markup_calc_type;
   O_pend_merch_hier_rec.otb_calc_type             := I_message.otb_calc_type;
   O_pend_merch_hier_rec.max_avg_counter           := I_message.max_avg_counter;
   O_pend_merch_hier_rec.avg_tolerance_pct         := I_message.avg_tolerance_pct;
   O_pend_merch_hier_rec.bud_int                   := I_message.bud_int;
   O_pend_merch_hier_rec.dept_vat_incl_ind         := L_dept_vat_incl_ind;
   O_pend_merch_hier_rec.class_vat_incl_ind        := L_class_vat_ind;
   O_pend_merch_hier_rec.merch_hier_grandparent_id := I_message.merch_hier_grandparent_id;

   if I_message.merch_hier_level = MERCH_RECLASS_VALIDATE_SQL.LP_div_code then
      O_pend_merch_hier_rec.merch_hier_parent_id   := NULL;
   else
      O_pend_merch_hier_rec.merch_hier_parent_id   := I_message.merch_hier_parent_id;
   end if;

   if I_message.bud_mkup is NULL and I_message.bud_int is not NULL then
      O_pend_merch_hier_rec.bud_mkup := (I_message.bud_int * 100/(100 - I_message.bud_int));
   elsif I_message.bud_mkup is not NULL and I_message.bud_int is NULL then
      O_pend_merch_hier_rec.bud_int  := (I_message.bud_mkup * 100/(100 + I_message.bud_mkup));
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message         IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_pend_merch_hier_rec   OUT    NOCOPY   PEND_MERCH_HIER%ROWTYPE,
                         I_message               IN              "RIB_XMrchHrRclsRef_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRRCLS_VALIDATE.POPULATE_RECORD';

BEGIN


   O_pend_merch_hier_rec.hier_type                 := I_message.merch_hier_level;
   O_pend_merch_hier_rec.merch_hier_id             := I_message.merch_hier_id;
   O_pend_merch_hier_rec.merch_hier_parent_id      := I_message.merch_hier_parent_id;
   O_pend_merch_hier_rec.merch_hier_grandparent_id := I_message.merch_hier_grandparent_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message         IN       "RIB_XMrchHrRclsRef_REC")
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50) := 'RMSSUB_XMRCHHRRCLS_VALIDATE.CHECK_DELETE';

   L_action_type       VARCHAR2(1);
   L_exists            BOOLEAN := FALSE;
BEGIN

   if not MERCH_RECLASS_VALIDATE_SQL.GET_MERCH_ACTION(O_error_message,
                                                      L_action_type,
                                                      I_message.merch_hier_level,
                                                      I_message.merch_hier_id,
                                                      I_message.merch_hier_parent_id,
                                                      I_message.merch_hier_grandparent_id) then
      return FALSE;
   end if;

   if (L_action_type = LP_action_add) AND (I_message.merch_hier_level != MERCH_RECLASS_VALIDATE_SQL.LP_scls_code) then
      if not MERCH_RECLASS_VALIDATE_SQL.CHILD_EXIST(O_error_message,
                                                    L_exists,
                                                    I_message.merch_hier_level,
                                                    I_message.merch_hier_id,
                                                    I_message.merch_hier_parent_id,
                                                    NULL) then

         return FALSE;
      end if;

      if L_exists then
         O_error_message := SQL_LIB.CREATE_MSG('CHILD_EXIST_NO_DELETE',
                                               I_message.merch_hier_level,
                                               to_char(I_message.merch_hier_id) ||
                                               '/' || to_char(I_message.merch_hier_parent_id),
                                               NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_DELETE;
-------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTENCE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_message         IN       "RIB_XMrchHrRclsDesc_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'RMSSUB_XMRCHHRRCLS_VALIDATE.CHECK_EXISTENCE';

BEGIN

   if I_message_type = RMSSUB_XMRCHHRRCLS.LP_cre_type then
      if not CHECK_EXISTENCE_CREATE(O_error_message,
                                    I_message.merch_hier_level,
                                    I_message.merch_hier_id,
                                    I_message.merch_hier_parent_id,
                                    I_message.merch_hier_grandparent_id,
                                    I_message.action_type,
                                    I_message.effective_date) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRRCLS.LP_mod_type then
      if not CHECK_EXISTENCE_MODIFY(O_error_message,
                                    I_message.merch_hier_level,
                                    I_message.merch_hier_id,
                                    I_message.merch_hier_parent_id,
                                    I_message.merch_hier_grandparent_id,
                                    I_message.effective_date) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_EXISTENCE;
-------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTENCE_MODIFY(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                                I_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                                I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                                I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE,
                                I_effective_date        IN       DATE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'RMSSUB_XMRCHHRRCLS_VALIDATE.CHECK_EXISTENCE_MODIFY';

   L_action_type           PEND_MERCH_HIER.ACTION_TYPE%TYPE               := NULL;
   L_exists                BOOLEAN   := FALSE;

BEGIN

   -- For a modify message, the hier_type/hierarchy must exist on PEND_MERCH_HIER table already.
   -- If modify a record with Add action type, need to make sure there is no child hierarchy
   -- with earlier effective date if effective date is changed.
   -- Also check parent is valid with regard to effective date.

   -- get action type while doing existence check
   if not MERCH_RECLASS_VALIDATE_SQL.GET_MERCH_ACTION(O_error_message,
                                                      L_action_type,
                                                      I_hier_type,
                                                      I_hier_id,
                                                      I_hier_parent_id,
                                                      I_hier_grandparent_id) then
      return FALSE;
   end if;

   if L_action_type = LP_action_add AND I_hier_type != MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
      L_exists := NULL;

      if MERCH_RECLASS_VALIDATE_SQL.CHILD_EXIST(O_error_message,
                                                L_exists,
                                                I_hier_type,
                                                I_hier_id,
                                                I_hier_parent_id,
                                                I_effective_date) = FALSE then
         return FALSE;
      end if;

      if L_exists = TRUE then
         O_error_message := SQL_LIB.CREATE_MSG('CHILD_EXIST_LESS_EDATE',
                                               TO_CHAR(I_effective_date, 'YYYYMMDD'),
                                               I_hier_type,
                                               TO_CHAR(I_hier_id) ||
                                               '/' || TO_CHAR(I_hier_parent_id));
         return FALSE;
      end if;
   end if;

   -- check parent
   if not CHECK_EXISTENCE_PARENT(O_error_message,
                                 I_hier_type,
                                 I_hier_parent_id,
                                 I_hier_grandparent_id,
                                 I_effective_date) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_EXISTENCE_MODIFY;
-------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTENCE_CREATE(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                                I_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                                I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                                I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE,
                                I_action_type           IN       PEND_MERCH_HIER.ACTION_TYPE%TYPE,
                                I_effective_date        IN       DATE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'RMSSUB_XMRCHHRRCLS_VALIDATE.CHECK_EXISTENCE_CREATE';

   L_hier_id               PEND_MERCH_HIER.MERCH_HIER_ID%TYPE := I_hier_id;
   L_hier_parent_id        PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE;
   L_hier_grandparent_id   PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE;

   L_exists             BOOLEAN := NULL;

BEGIN

   -- For a create message with Mod action type, the hierarchy must be in RMS
   -- merch hier tables (division, groups, deps, class, subclass), but NOT in
   -- PEND_MERCH_HIER (regardless of effective date).
   -- For a create message with Add action type, the hierarchy must NOT be in
   -- v_merch_hier (regardless of effective date).
   -- Also check parent is valid with regard to effective date.

   if I_hier_type in (MERCH_RECLASS_VALIDATE_SQL.LP_cls_code,
                      MERCH_RECLASS_VALIDATE_SQL.LP_scls_code) then
      L_hier_parent_id := I_hier_parent_id;
      L_hier_grandparent_id := I_hier_grandparent_id;
   end if;

   -- Add action: validate hierarchy is NOT in view
   if (I_action_type = LP_action_add) then
      if not MERCH_VALIDATE_SQL.EXIST(O_error_message,
                                      L_exists,
                                      I_hier_type,
                                      L_hier_id,
                                      L_hier_parent_id,
                                      L_hier_grandparent_id,
                                      NULL) then  -- effective date
         return FALSE;
      end if;

      if L_exists = TRUE then
         O_error_message := SQL_LIB.CREATE_MSG('HIER_EXIST_IN_VIEW',
                                               I_hier_type,
                                               to_char(L_hier_id) ||
                                               '/' || to_char(L_hier_parent_id) ||
                                               '/' || to_char(L_hier_grandparent_id),
                                               null);
         return FALSE;
      end if;
   end if;

   -- Mod action, validate hierarchy is NOT in PEND_MERCH_HIER
   -- but in division, groups, deps, class, subclass
   if (I_action_type = LP_action_mod) then

      -- validate the hierarchy is not in PEND_MERCH_HIER
      if not MERCH_RECLASS_VALIDATE_SQL.EXIST(O_error_message,
                                              L_exists,
                                              I_hier_type,
                                              L_hier_id,
                                              L_hier_parent_id,
                                              L_hier_grandparent_id) then
         return FALSE;
      end if;

      if L_exists = TRUE then
         O_error_message := SQL_LIB.CREATE_MSG('HIER_EXIST',
                                               I_hier_type,
                                               to_char(L_hier_id) ||
                                               '/' || to_char(L_hier_parent_id) ||
                                               '/' || to_char(L_hier_grandparent_id),
                                               null);
         return FALSE;
      end if;

      L_exists := NULL;

      -- check division
      if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_div_code then
         if MERCH_VALIDATE_SQL.DIVISION_EXIST(O_error_message,
                                              L_exists,
                                              L_hier_id) = FALSE then
            return FALSE;
         end if;

         if L_exists = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('INV_MERCH_DIV',
                                                  L_hier_id,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      -- check group
      elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_grp_code then
         if MERCH_VALIDATE_SQL.GROUP_EXIST(O_error_message,
                                           L_exists,
                                           L_hier_id) = FALSE then
            return FALSE;
         end if;

         if L_exists = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('INV_GROUP',
                                                  L_hier_id,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      -- check dept
      elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_dept_code then
         if DEPT_VALIDATE_SQL.EXIST(O_error_message,
                                    L_hier_id,
                                    L_exists) = FALSE then
            return FALSE;
         end if;

         if L_exists = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('DEPT_NO_EXIST',
                                                  L_hier_id,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      -- check class
      elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_cls_code then
         if CLASS_VALIDATE_SQL.EXIST(O_error_message,
                                     L_hier_parent_id,
                                     L_hier_id,
                                     L_exists) = FALSE then
            return FALSE;
         end if;

         if L_exists = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('CLASS_NO_EXIST_DEPT',
                                                  L_hier_id,
                                                  L_hier_parent_id,
                                                  NULL);
            return FALSE;
         end if;
      -- check subclass
      elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
         if SUBCLASS_VALIDATE_SQL.EXIST(O_error_message,
                                        L_hier_grandparent_id,
                                        L_hier_parent_id,
                                        L_hier_id,
                                        L_exists) = FALSE then
            return FALSE;
         end if;

         if L_exists = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('SUBCLASS_NO_EXIST_HIER',
                                                  L_hier_id,
                                                  L_hier_grandparent_id,
                                                  L_hier_parent_id);
            return FALSE;
         end if;
      end if;
   end if;  -- mod action

   -- check parent with the parent/grandparent from the message
   if not CHECK_EXISTENCE_PARENT(O_error_message,
                                 I_hier_type,
                                 I_hier_parent_id,
                                 I_hier_grandparent_id,
                                 I_effective_date) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_EXISTENCE_CREATE;
-------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTENCE_PARENT(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                                I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                                I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE,
                                I_effective_date        IN       DATE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'RMSSUB_XMRCHHRRCLS_VALIDATE.CHECK_EXISTENCE_PARENT';

   L_company_exists   BOOLEAN      := FALSE;
   L_parent_exists    BOOLEAN      := FALSE;

BEGIN

   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_div_code then
      -- for division, parent is company
      if not MERCH_VALIDATE_SQL.COMPANY_EXIST(O_error_message,
                                              L_company_exists,
                                              I_hier_parent_id) then
         return FALSE;
      end if;

      if L_company_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('PARENT_NOT_EXIST',
                                               TO_CHAR(I_effective_date, 'YYYYMMDD'),
                                               I_hier_type,
                                               TO_CHAR(I_hier_parent_id));
         return FALSE;
      end if;
   else
      -- for all others, check against view with effective date
      if not MERCH_VALIDATE_SQL.PARENT_EXIST(O_error_message,
                                             L_parent_exists,
                                             I_hier_type,
                                             I_hier_parent_id,
                                             I_hier_grandparent_id,
                                             I_effective_date) then
         return FALSE;
      end if;

      if L_parent_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('PARENT_NOT_EXIST',
                                               TO_CHAR(I_effective_date, 'YYYYMMDD'),
                                               I_hier_type,
                                               TO_CHAR(I_hier_parent_id) ||
                                               '/' || TO_CHAR(I_hier_grandparent_id));
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_EXISTENCE_PARENT;
-------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRRCLS_VALIDATE;
/
