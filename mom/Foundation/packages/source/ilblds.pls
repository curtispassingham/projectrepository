
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEMLIST_BUILD_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------
-- Function: CLEAR_LIST
-- Purpose:  The clear_list function will delete the items on the
--           specified item list before it is rebuilt.  This function
--           will be called from the Rebuild_List function.  This
--           is done to avoid overlapping SKUs on the item list.
----------------------------------------------------------------
FUNCTION CLEAR_LIST (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_itemlist      IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Function: REBUILD_LIST
-- Purpose:  The REBUILD_LIST function will rebuild the specified
--           item list based on the criteria saved on the
--           new SKULIST_CRITERIA table.
----------------------------------------------------------------
FUNCTION REBUILD_LIST (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_no_items      IN OUT NUMBER,
                       I_itemlist      IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Function: GET_MAX_SEQUENCE_NO
-- Purpose:  The GET_MAX_SEQUENCE_NO function will retrieve the
--           maximum sequence number for the specified list.
----------------------------------------------------------------
FUNCTION GET_MAX_SEQUENCE_NO (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_sequence_no   IN OUT SKULIST_CRITERIA.SEQ_NO%TYPE,
                              I_itemlist      IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Name:    INSERT_CRITERIA
-- Purpose: This function inserts the criteria for the given step
--          of building the item list.
----------------------------------------------------------------
FUNCTION INSERT_CRITERIA(I_itemlist          IN     SKULIST_HEAD.SKULIST%TYPE,
                         I_seq_no            IN     SKULIST_CRITERIA.SEQ_NO%TYPE,
                         I_action_type       IN     SKULIST_CRITERIA.ACTION_TYPE%TYPE,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_item_parent       IN     ITEM_MASTER.ITEM%TYPE,
                         I_item_grandparent  IN     ITEM_MASTER.ITEM%TYPE,
                         I_uda_id            IN     SKULIST_CRITERIA.UDA_ID%TYPE,
                         I_uda_min_date      IN     SKULIST_CRITERIA.UDA_VALUE_MIN_DATE%TYPE,
                         I_uda_max_date      IN     SKULIST_CRITERIA.UDA_VALUE_MAX_DATE%TYPE,
                         I_uda_value_lov     IN     SKULIST_CRITERIA.UDA_VALUE_LOV%TYPE,
                         I_supplier          IN     SKULIST_CRITERIA.SUPPLIER%TYPE,
                         I_dept              IN     SKULIST_CRITERIA.DEPT%TYPE,
                         I_class             IN     SKULIST_CRITERIA.CLASS%TYPE,
                         I_subclass          IN     SKULIST_CRITERIA.SUBCLASS%TYPE,
                         I_diff_1            IN     SKULIST_CRITERIA.DIFF_1%TYPE,
                         I_diff_2            IN     SKULIST_CRITERIA.DIFF_2%TYPE,
                         I_diff_3            IN     SKULIST_CRITERIA.DIFF_3%TYPE,
                         I_diff_4            IN     SKULIST_CRITERIA.DIFF_4%TYPE,
                         I_season_id         IN     SKULIST_CRITERIA.SEASON_ID%TYPE,
                         I_phase_id          IN     SKULIST_CRITERIA.PHASE_ID%TYPE,
                         I_item_level        IN     SKULIST_CRITERIA.ITEM_LEVEL%TYPE,
                         O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Name:    DELETE_CRITERIA
-- Purpose: This function deletes the criteria for an item
----------------------------------------------------------------
FUNCTION DELETE_CRITERIA(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_itemlist          IN     SKULIST_CRITERIA.SKULIST%TYPE,
                         I_item              IN     SKULIST_CRITERIA.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Name:    LOCK_RECORDS
-- Purpose: This function locks the detail records of an item list
--          so that the SKULIST_CRITERIA.SEQ_NO remains in tact.
----------------------------------------------------------------
FUNCTION LOCK_RECORDS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_itemlist          IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Name:    COPY_CRITERIA
-- Purpose: This function copies the criteria from one
--          skulist to another, used in create from existing
--          in SLHEAD.fmb.
----------------------------------------------------------------
FUNCTION COPY_CRITERIA(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_itemlist_old      IN     SKULIST_HEAD.SKULIST%TYPE,
                       I_itemlist_new      IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Name:    INSERT_SINGLE_ITEM
-- Purpose: This function inserts a single sku into SKULIST_DETAIL.
----------------------------------------------------------------
FUNCTION INSERT_SINGLE_ITEM(O_error_message      IN OUT	RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item               IN         ITEM_MASTER.ITEM%TYPE,
                            I_itemlist           IN         SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Name:    DELETE_SINGLE_ITEM
-- Purpose: This function deletes a single sku from SKULIST_DETAIL.
----------------------------------------------------------------
FUNCTION DELETE_SINGLE_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item          IN     ITEM_MASTER.ITEM%TYPE,
                            I_itemlist      IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Name:    SKULIST_DEPT_EXIST
-- Purpose: This function validates whether or not there already exists any
--          skulist/dept combination in the table SKULIST_DEPT for the parsed in
--          I_itemlist/I_merch_level/I_merch_id.

-- note:
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS
-- which only returns data that the user has permission to access.
----------------------------------------------------------------
FUNCTION SKULIST_DEPT_EXIST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist             IN OUT BOOLEAN,
                            I_itemlist          IN     SKULIST_HEAD.SKULIST%TYPE,
                            I_merch_level       IN     SYSTEM_OPTIONS.SKULIST_ORG_LEVEL_CODE%TYPE,
                            I_merch_id          IN     DEPS.DEPT%TYPE,
                            I_merch_class_id    IN     CLASS.CLASS%TYPE,
                            I_merch_subclass_id IN     SUBCLASS.SUBCLASS%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Name:    INSERT_SKULIST_DEPT
-- Purpose: This function inserts departments into SKULIST_DEPT.

-- note:
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_DEPS
-- which only returns data that the user has permission to access.
----------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION INSERT_SKULIST_DEPT(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_itemlist          IN     SKULIST_HEAD.SKULIST%TYPE,
                             I_merch_level       IN     SYSTEM_OPTIONS.SKULIST_ORG_LEVEL_CODE%TYPE,
                             I_merch_id          IN     DEPS.DEPT%TYPE,
                             I_merch_class_id    IN     CLASS.CLASS%TYPE,
                             I_merch_subclass_id IN     SUBCLASS.SUBCLASS%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Name:    COPY_SKULIST_DEPT
-- Purpose: Call from form when creating New From Existing.
----------------------------------------------------------------
FUNCTION COPY_SKULIST_DEPT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_skulist_old      IN     SKULIST_HEAD.SKULIST%TYPE,
                           I_skulist_new      IN     SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN;
END;
/
