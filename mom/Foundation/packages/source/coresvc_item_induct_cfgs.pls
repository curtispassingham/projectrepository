create or replace PACKAGE CORESVC_ITEM_INDUCT_CGF AUTHID CURRENT_USER AS
   template_key                           CONSTANT VARCHAR2(255):='ITEM_INDUCT_CGF_DATA';
   action_new                                      VARCHAR2(25)          := 'NEW';
   action_mod                                      VARCHAR2(25)          := 'MOD';
   action_del                                      VARCHAR2(25)          := 'DEL';
   ITEM_INDUCT_CONFIG_sheet                        VARCHAR2(255)         := 'ITEM_INDUCT_CONFIG';
   ITM_INT_CFG$Action                              NUMBER                :=1;
   ITM_INT_CFG$MAX_ITEMS_FOR_DNLD                  NUMBER                :=2;
   ITM_INT_CFG$MX_ITM_FR_SNC_DNLD                  NUMBER                :=3;
   ITM_INT_CFG$MX_FL_SZ_FR_UPLD                    NUMBER                :=4;
   ITM_INT_CFG$MX_FL_SZ_FR_SNC_UP                  NUMBER                :=5;
   ITM_INT_CFG$MX_CC_FR_SYNC_DNLD                  NUMBER                :=6;
   ITM_INT_CFG$MAX_CC_FOR_DNLD                     NUMBER                :=7;
   sheet_name_trans                                S9T_PKG.trans_map_typ;
   action_column                                   VARCHAR2(255) := 'ACTION';
   template_category                               CODE_DETAIL.CODE%TYPE := 'RMSADM';
   TYPE ITEM_INDUCT_CONFIG_rec_tab IS TABLE OF ITEM_INDUCT_CONFIG%ROWTYPE;
   CORESVC_ITEM_CONFIG_sheet                                VARCHAR2(255)         := 'CORESVC_ITEM_CONFIG';
   CORESVC_ITEM_CONFIG$Action                               NUMBER                :=1;
   CRSV_ITM_CFG$CSCD_IUD_ITM_SUPP                           NUMBER                :=16;
   CRSV_ITM_CFG$MAX_THREADS                                 NUMBER                :=13;
   CRSV_ITM_CFG$WAIT_BTWN_THREADS                           NUMBER                :=14;
   CRSV_ITM_CFG$CSCD_IIM_DETAILS                            NUMBER                :=15;
   CRSV_ITM_CFG$MAX_CHUNK_SIZE                              NUMBER                :=2;
   CRSV_ITM_CFG$PRC_ERR_RTNTN_DAY                           NUMBER                :=3;
   CRSV_ITM_CFG$CSCD_UDA_DETAILS                            NUMBER                :=4;
   CRSV_ITM_CFG$CSCD_IUITMSPP_CTR                           NUMBER                :=5;
   CRSV_ITM_CFG$CSCDIUDISC_DIM                              NUMBER                :=6;
   CRSV_ITM_CFG$CSCD_IUD_ISMC                               NUMBER                :=7;
   CRSV_ITM_CFG$ISC_UPD_ALL_LC                              NUMBER                :=8;
   CRSV_ITM_CFG$ISC_UPD_AL_CHD_LC                           NUMBER                :=9;
   CRSV_ITM_CFG$CASCADE_VAT_ITEM                            NUMBER                :=10;
   CRSV_ITM_CFG$MAX_ITEM_RESV_QTY                           NUMBER                :=11;
   CRSV_ITM_CFG$MAX_ITM_EXPRY_DAY                           NUMBER                :=12;
 

---------------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------   
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER
                        )
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------   
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER
                   )
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------   
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
END CORESVC_ITEM_INDUCT_CGF;
/