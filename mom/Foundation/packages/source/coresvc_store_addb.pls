CREATE OR REPLACE PACKAGE BODY CORESVC_STORE_ADD_SQL AS
Lp_likestore_program_name VARCHAR2(14):='LIKE_STORE';
  ------------------------------------------------------------------------------------------------
  FUNCTION get_likestore_restart_name(
      I_new_store IN store_add.store%type)
    RETURN VARCHAR2
  IS
  BEGIN
    RETURN Lp_likestore_program_name||'_'||I_new_store;
  END get_likestore_restart_name;
------------------------------------------------------------------------------------------------
FUNCTION NEW_STORE (O_error_message IN OUT VARCHAR2,
                    I_store         IN     STORE.STORE%TYPE,
                    I_district      IN     DISTRICT.DISTRICT%TYPE)
RETURN BOOLEAN IS

   L_function_name   VARCHAR2(100) := 'CORESVC_STORE_ADD_SQL.NEW_STORE';
   
BEGIN
   insert into store_hierarchy (company,
                                chain,
                                area,
                                region,
                                district,
                                store)
							    (select c.company,
									    ch.chain,
									    ar.area,
									    re.region,
									    I_district,
									    I_store
								   from comphead c,
									    chain ch,
									    area ar,
									    region re,
									    district di
							   where di.district = I_district
								 and di.region = re.region
								 and re.area = ar.area
								 and ar.chain = ch.chain);
  
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function_name,
                                            to_char(SQLCODE));
      return FALSE;
END NEW_STORE;
----------------------------------------------------------------------------------------------------
  FUNCTION LIKE_STORE_THREAD( O_error_message        IN   OUT RTK_ERRORS.RTK_TEXT%TYPE,
							  I_dept_thread        IN restart_program_status.thread_val%type,
							  I_commit_max_ctr     IN restart_control.commit_max_ctr%type,
							  I_new_store            IN   STORE_ADD.STORE%TYPE,
							  I_like_store           IN   STORE_ADD.LIKE_STORE%TYPE,
							  I_new_store_currency   IN   STORE.CURRENCY_CODE%TYPE,
							  I_copy_repl_ind        IN   VARCHAR2,
							  I_copy_clearance_ind   IN   VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'CORESVC_STORE_ADD_SQL.LIKE_STORE_THREAD';
   L_elc_ind         SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_vdate           PERIOD.VDATE%TYPE;
   L_std_av_ind      SYSTEM_OPTIONS.STD_AV_IND%TYPE;
   L_exchange_type   CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;
   L_nil_input       NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;
  
   L_is_restart BOOLEAN:=false;
   L_bookmark_params varr;
   L_program_name restart_control.program_name%type :='LIKE_STORE';
   L_restart_item item_master.item%type;
   L_restart_pack_ind item_master.pack_ind%type;
   L_uncommited_count NUMBER:=0;
   L_pack_ind item_master.pack_ind%type;
   L_item item_master.item%type;
	
   CURSOR c_exchange_type IS
      SELECT DECODE(consolidation_ind, 'Y', 'C', 'O')
        FROM system_options;

   CURSOR c_item_exp_head IS
      SELECT ieh.item
            ,ieh.supplier
            ,ieh.item_exp_type
            ,ieh.item_exp_seq
            ,ieh.origin_country_id
            ,ieh.zone_id
            ,ieh.lading_port
            ,ieh.discharge_port
            ,ieh.zone_group_id
            ,ieh.base_exp_ind
            ,(
              SELECT MAX(ieh1.item_exp_seq)
                FROM item_exp_head ieh1
               WHERE ieh1.item          = ieh.item
                 AND ieh1.supplier      = ieh.supplier
                 AND ieh1.item_exp_type = ieh.item_exp_type
              ) AS max_seq
            ,ROW_NUMBER() OVER (PARTITION BY ieh.item,
                                             ieh.supplier,
                                             ieh.item_exp_type
                                    ORDER BY ieh.item_exp_seq) AS ins_seq
        FROM item_exp_head ieh
            ,item_master   im
            ,cost_zone_group czg
       WHERE ieh.item              = im.item
	     AND im.dept               = I_dept_thread
         AND im.cost_zone_group_id = ieh.zone_group_id
         AND ieh.zone_group_id     = czg.zone_group_id
         AND czg.cost_level        = 'L'
         AND ieh.item_exp_type     = 'Z'
         AND ieh.zone_id           = I_like_store
         AND NOT EXISTS (select 'x'
                           from daily_purge dp
                          where dp.key_value = im.item
                            and dp.table_name ='ITEM_MASTER')
         AND NOT EXISTS (select 'x'
                           from item_exp_head ieh2
                          where ieh2.item = ieh.item
                            and ieh2.supplier = ieh.supplier
                            and ieh2.item_exp_type = ieh.item_exp_type
                            and ieh2.zone_group_id = ieh.zone_group_id
                            and ieh2.zone_id = I_new_store)
                            ;
   TYPE TAB_TYP_IEH IS TABLE OF C_ITEM_EXP_HEAD%ROWTYPE;
   tab_ieh TAB_TYP_IEH;
  --
  
   CURSOR C_NIL_INPUT IS
      SELECT im.item
            ,im.item_parent
            ,im.item_grandparent
            ,im.short_desc
            ,im.dept
            ,im.class
            ,im.subclass
            ,im.item_level
            ,im.tran_level
            ,im.status
            ,im.waste_type
            ,im.sellable_ind
            ,im.orderable_ind
            ,im.pack_ind
            ,im.pack_type
            ,im.item_desc
            ,im.diff_1
            ,im.diff_2
            ,im.diff_3
            ,im.diff_4
            ,I_new_store
            ,'S'
            ,il.daily_waste_pct
            ,iscl.unit_cost * mvc.exchange_rate
            ,il.regular_unit_retail
            ,il.regular_unit_retail
            ,il.selling_uom
            ,il.multi_units
            ,il.multi_unit_retail
            ,il.multi_selling_uom
            ,il.status
            ,il.taxable_ind
            ,il.ti
            ,il.hi
            ,il.store_ord_mult
            ,il.meas_of_each
            ,il.meas_of_price
            ,il.uom_of_price
            ,il.primary_variant
            ,il.primary_supp
            ,il.primary_cntry
            ,il.local_item_desc
            ,il.local_short_desc
            ,il.primary_cost_pack
            ,il.receive_as_type
            ,il.store_price_ind
            ,il.uin_type
            ,il.uin_label
            ,il.capture_time
            ,il.ext_uin_ind
            ,il.source_method
            ,il.source_wh
            ,NULL
            ,I_new_store_currency
            ,I_like_store
            ,'N'
            ,cl.class_vat_ind
            ,NULL
            ,NULL
            ,NULL
            ,il.costing_loc
            ,il.costing_loc_type
            ,il.ranged_ind
            ,NULL
            ,NULL
        FROM store                 st
            ,item_master           im
            ,class                 cl
            ,item_loc              il
            ,item_supp_country_loc iscl
            ,addr
            ,country_attrib        ca
            ,mv_currency_conversion_rates mvc
       WHERE st.store              = I_like_store
         AND st.store              = il.loc
         AND il.item               = im.item
         AND im.dept               = cl.dept
         AND im.dept               = I_dept_thread
         AND im.class              = cl.class
         AND addr.primary_addr_ind = 'Y'
         AND addr_type             = DECODE(module, 'WFST', '07', '01')
         AND addr.key_value_1      = TO_CHAR(I_new_store)
         AND addr.module IN ('ST','WFST')
         AND ca.country_id         = addr.country_id
         AND il.item               = iscl.item(+)
         AND il.loc                = iscl.loc(+)
         AND il.primary_supp       = iscl.supplier(+)
         AND il.primary_cntry      = iscl.origin_country_id(+)
         AND il.clear_ind          = DECODE(I_copy_clearance_ind, 'N', 'N', il.clear_ind)
         AND mvc.from_currency     = st.currency_code
         AND mvc.to_currency       = I_new_store_currency
         AND mvc.exchange_type     = L_exchange_type
         AND mvc.effective_date    = (SELECT max(effective_date)
                                        FROM mv_currency_conversion_rates mccr,
                                             period                       p
                                       WHERE mccr.from_currency   = mvc.from_currency
                                         AND mccr.to_currency     = mvc.to_currency
                                         AND mccr.exchange_type   = L_exchange_type
                                         AND mccr.effective_date <= p.vdate)
         AND NOT EXISTS (select 'x'
                           from daily_purge dp
                          where dp.key_value = im.item
                            and dp.table_name ='ITEM_MASTER')
         AND NOT EXISTS (select 'x'
                           from item_loc il1
                          where im.item = il1.item
                            and il1.loc = I_new_store)
       ORDER BY im.pack_ind ASC
               ,il.item;

BEGIN

   --
  IF mthread_restart_sql.start_thread(O_error_message,Lp_likestore_program_name,get_likestore_restart_name(I_new_store),I_dept_thread,L_is_restart,L_bookmark_params)=false THEN
    RETURN false;
  END IF;
--
IF L_is_restart AND L_bookmark_params IS NOT NULL THEN
  IF L_bookmark_params.exists(1) THEN
    L_restart_pack_ind := L_bookmark_params(1);
  END IF;
  IF L_bookmark_params.exists(2) THEN
    L_restart_item := L_bookmark_params(2);
  END IF;
END IF;

   SELECT s.elc_ind
         ,vdate
         ,s.std_av_ind
     INTO L_elc_ind
         ,L_vdate
         ,L_std_av_ind
     FROM system_options s
         ,period         p;
 IF NOT (L_is_restart AND L_restart_item IS NOT NULL) THEN

   OPEN C_ITEM_EXP_HEAD;

   LOOP
      FETCH C_ITEM_EXP_HEAD BULK COLLECT INTO tab_ieh LIMIT 10000;
      ---
      FORALL i IN 1..tab_ieh.COUNT
         INSERT INTO item_exp_head(
                                   item
                                  ,supplier
                                  ,item_exp_type
                                  ,item_exp_seq
                                  ,origin_country_id
                                  ,zone_id
                                  ,lading_port
                                  ,discharge_port
                                  ,zone_group_id
                                  ,base_exp_ind
                                  ,create_datetime
                                  ,last_update_datetime
                                  ,last_update_id
                                   )
                            VALUES(
                                   tab_ieh(i).item
                                  ,tab_ieh(i).supplier
                                  ,tab_ieh(i).item_exp_type
                                  ,tab_ieh(i).ins_seq + tab_ieh(i).max_seq
                                  ,tab_ieh(i).origin_country_id
                                  ,I_new_store
                                  ,tab_ieh(i).lading_port
                                  ,tab_ieh(i).discharge_port
                                  ,tab_ieh(i).zone_group_id
                                  ,tab_ieh(i).base_exp_ind
                                  ,SYSDATE
                                  ,SYSDATE
                                  ,USER
                                   );
      ---
      FORALL i IN 1..tab_ieh.COUNT
         INSERT INTO item_exp_detail(
                                     ITEM
                                    ,SUPPLIER
                                    ,ITEM_EXP_TYPE
                                    ,ITEM_EXP_SEQ
                                    ,COMP_ID
                                    ,CVB_CODE
                                    ,COMP_RATE
                                    ,COMP_CURRENCY
                                    ,PER_COUNT
                                    ,PER_COUNT_UOM
                                    ,EST_EXP_VALUE
                                    ,NOM_FLAG_1
                                    ,NOM_FLAG_2
                                    ,NOM_FLAG_3
                                    ,NOM_FLAG_4
                                    ,NOM_FLAG_5
                                    ,DISPLAY_ORDER
                                    ,CREATE_DATETIME
                                    ,LAST_UPDATE_DATETIME
                                    ,LAST_UPDATE_ID
                                    ,DEFAULTED_FROM
                                    ,KEY_VALUE_1
                                    ,KEY_VALUE_2
                                     )
                              SELECT item
                                    ,supplier
                                    ,item_exp_type
                                    ,tab_ieh(i).ins_seq + tab_ieh(i).max_seq
                                    ,comp_id
                                    ,cvb_code
                                    ,comp_rate
                                    ,comp_currency
                                    ,per_count
                                    ,per_count_uom
                                    ,est_exp_value
                                    ,nom_flag_1
                                    ,nom_flag_2
                                    ,nom_flag_3
                                    ,nom_flag_4
                                    ,nom_flag_5
                                    ,display_order
                                    ,create_datetime
                                    ,last_update_datetime
                                    ,last_update_id
                                    ,defaulted_from
                                    ,key_value_1
                                    ,key_value_2
                                FROM item_exp_detail ied
                               WHERE ied.item          = tab_ieh(i).item
                                 AND ied.supplier      = tab_ieh(i).supplier
                                 AND ied.item_exp_type = tab_ieh(i).item_exp_type
                                 AND ied.item_exp_seq  = tab_ieh(i).item_exp_seq;
      ---
      EXIT WHEN C_ITEM_EXP_HEAD%NOTFOUND;
  END LOOP;

  CLOSE C_ITEM_EXP_HEAD;
  END IF;

   
   OPEN c_exchange_type;
   FETCH c_exchange_type INTO L_exchange_type;
   CLOSE c_exchange_type;
   
   OPEN C_NIL_INPUT;
   LOOP
   FETCH C_NIL_INPUT BULK COLLECT INTO L_nil_input LIMIT I_commit_max_ctr;
   IF L_nil_input is not null and L_nil_input.count >0 THEN 
     L_pack_ind                                                    := L_nil_input(L_nil_input.count).pack_ind; 
     L_item                                                        := L_nil_input(L_nil_input.count).item; 
		 IF NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message, L_nil_input) = FALSE THEN 
            RETURN false; 
		 END IF; 
         IF mthread_restart_sql.commit_thread(O_error_message,get_likestore_restart_name(I_new_store),I_dept_thread,NEW varr(L_pack_ind,L_item))=false THEN 
			RETURN false;
		 END IF;
	     L_uncommited_count :=0;
	END IF; 
 EXIT 
 WHEN C_NIL_INPUT%NOTFOUND; 

 END LOOP; 

 CLOSE C_NIL_INPUT;
   INSERT INTO like_store_dept(
                               STORE,
                               DEPT
                              )
                              VALUES
                              (
                              I_new_store,
                              I_dept_thread
                              );
   IF mthread_restart_sql.finish_thread(O_error_message,get_likestore_restart_name(I_new_store),I_dept_thread,'Y','N',NULL)=false THEN 
		RETURN false; 
   END IF; 
  RETURN true; 
EXCEPTION 
WHEN OTHERS THEN 
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE)); 
     RETURN FALSE; 
END LIKE_STORE_THREAD; 
------------------------------------------------------------------------------------------------ 
FUNCTION copy_repl( 
				O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
				I_new_store     IN STORE_ADD.STORE%TYPE ) 
    RETURN BOOLEAN 
IS 
L_program VARCHAR2(64) := 'CORESVC_STORE_ADD_SQL.like_store'; 
BEGIN 

      FOR repl_rec IN (
                       SELECT sa.store
                             ,sa.like_store
                             ,sa.copy_repl_ind
                             ,sa.store_open_date
                             ,sa.store_close_date
                             ,sa.start_order_days
                             ,sa.stop_order_days
                             ,sa.rowid sa_rowid
                             ,st.rowid st_rowid
                         FROM store_add sa
                             ,store     st
                        WHERE sa.store = st.store
                          AND sa.store = I_new_store
                       )
      LOOP
         INSERT INTO repl_item_loc(
                                   item
                                  ,location
                                  ,loc_type
                                  ,item_parent
                                  ,item_grandparent
                                  ,primary_repl_supplier
                                  ,origin_country_id
                                  ,review_cycle
                                  ,stock_cat
                                  ,repl_order_ctrl
                                  ,source_wh
                                  ,activate_date
                                  ,deactivate_date
                                  ,pres_stock
                                  ,demo_stock
                                  ,repl_method
                                  ,min_stock
                                  ,max_stock
                                  ,incr_pct
                                  ,min_supply_days
                                  ,max_supply_days
                                  ,time_supply_horizon
                                  ,inv_selling_days
                                  ,service_level
                                  ,lost_sales_factor
                                  ,reject_store_ord_ind
                                  ,non_scaling_ind
                                  ,max_scale_value
                                  ,pickup_lead_time
                                  ,wh_lead_time
                                  ,terminal_stock_qty
                                  ,season_id
                                  ,phase_id
                                  ,last_review_date
                                  ,next_review_date
                                  ,primary_pack_no
                                  ,primary_pack_qty
                                  ,unit_tolerance
                                  ,pct_tolerance
                                  ,item_season_seq_no
                                  ,use_tolerance_ind
                                  ,last_delivery_date
                                  ,next_delivery_date
                                  ,mbr_order_qty
                                  ,adj_pickup_lead_time
                                  ,adj_supp_lead_time
                                  ,tsf_po_link_no
                                  ,last_roq
                                  ,status
                                  ,dept
                                  ,class
                                  ,subclass
                                  ,store_ord_mult
                                  ,unit_cost
                                  ,supp_lead_time
                                  ,inner_pack_size
                                  ,supp_pack_size
                                  ,ti
                                  ,hi
                                  ,round_lvl
                                  ,round_to_inner_pct
                                  ,round_to_case_pct
                                  ,round_to_layer_pct
                                  ,round_to_pallet_pct
                                  ,last_update_datetime
                                  ,last_update_id
                                  ,create_datetime
                                  ,mult_runs_per_day_ind
                                  ,tsf_zero_soh_ind
                                   )
                                  (
                                   SELECT r1.item
                                         ,repl_rec.store
                                         ,r1.loc_type
                                         ,r1.item_parent
                                         ,r1.item_grandparent
                                         ,r1.primary_repl_supplier
                                         ,r1.origin_country_id
                                         ,r1.review_cycle
                                         ,r1.stock_cat
                                         ,r1.repl_order_ctrl
                                         ,r1.source_wh
                                         ,repl_rec.store_open_date  - repl_rec.start_order_days
                                         ,repl_rec.store_close_date - repl_rec.stop_order_days
                                         ,r1.pres_stock
                                         ,r1.demo_stock
                                         ,r1.repl_method
                                         ,r1.min_stock
                                         ,r1.max_stock
                                         ,r1.incr_pct
                                         ,r1.min_supply_days
                                         ,r1.max_supply_days
                                         ,r1.time_supply_horizon
                                         ,r1.inv_selling_days
                                         ,r1.service_level
                                         ,r1.lost_sales_factor
                                         ,r1.reject_store_ord_ind
                                         ,r1.non_scaling_ind
                                         ,r1.max_scale_value
                                         ,r1.pickup_lead_time
                                         ,r1.wh_lead_time
                                         ,r1.terminal_stock_qty
                                         ,r1.season_id
                                         ,r1.phase_id
                                         ,r1.last_review_date
                                         ,r1.next_review_date
                                         ,r1.primary_pack_no
                                         ,r1.primary_pack_qty
                                         ,r1.unit_tolerance
                                         ,r1.pct_tolerance
                                         ,r1.item_season_seq_no
                                         ,r1.use_tolerance_ind
                                         ,r1.last_delivery_date
                                         ,r1.next_delivery_date
                                         ,r1.mbr_order_qty
                                         ,r1.adj_pickup_lead_time
                                         ,r1.adj_supp_lead_time
                                         ,r1.tsf_po_link_no
                                         ,r1.last_roq
                                         ,r1.status
                                         ,r1.dept
                                         ,r1.class
                                         ,r1.subclass
                                         ,r1.store_ord_mult
                                         ,r1.unit_cost
                                         ,r1.supp_lead_time
                                         ,r1.inner_pack_size
                                         ,r1.supp_pack_size
                                         ,r1.ti
                                         ,r1.hi
                                         ,r1.round_lvl
                                         ,r1.round_to_inner_pct
                                         ,r1.round_to_case_pct
                                         ,r1.round_to_layer_pct
                                         ,r1.round_to_pallet_pct
                                         ,SYSDATE
                                         ,USER
                                         ,SYSDATE
                                         ,r1.mult_runs_per_day_ind
                                         ,r1.tsf_zero_soh_ind
                                     FROM repl_item_loc r1
                                         ,item_loc      il
                                    WHERE r1.location = repl_rec.like_store
                                      AND r1.item     = il.item
                                      AND il.loc      = repl_rec.store
                                      AND NOT EXISTS (select 'x'
                                                        from repl_item_loc ril
                                                       where r1.item = ril.item
                                                         and ril.location = repl_rec.store)
                                   );
         ---
         INSERT INTO repl_day(
                              item
                             ,location
                             ,weekday
                             ,loc_type
                              )
                             (SELECT rs.item
                                    ,repl_rec.store
                                    ,rs.weekday
                                    ,rs.loc_type
                                FROM repl_day rs
                                    ,item_loc il
                               WHERE rs.location = repl_rec.like_store
                                 AND rs.item     = il.item
                                 AND il.loc      = repl_rec.store
                                 AND NOT EXISTS (select 'x'
                                                   from repl_day rd
                                                  where rd.item = rs.item
                                                    and rd.location = repl_rec.store
                                                    and rd.weekday = rs.weekday)

                              );
         ---
         INSERT INTO repl_item_loc_updates(
                                           item
                                          ,location
                                          ,loc_type
                                          ,change_type
                                           )
                                          (SELECT ril.item
                                                 ,repl_rec.store
                                                 ,'S'
                                                 ,'RIL'
                                             FROM repl_item_loc ril
                                            WHERE ril.location = repl_rec.store
                                              AND NOT EXISTS (select 'x'
                                                                from repl_item_loc_updates rilu
                                                               where rilu.item = ril.item
                                                                 and rilu.location = repl_rec.store
                                                                 and rilu.change_type = 'RIL')
                                           );
      END LOOP;

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END copy_repl;
------------------------------------------------------------------------------------------------
PROCEDURE LIKE_STORE_THREAD
  (
    I_dept_thread        IN restart_program_status.thread_val%type,
    I_commit_max_ctr     IN restart_control.commit_max_ctr%type,
    I_new_store          IN STORE_ADD.STORE%TYPE,
    I_like_store         IN STORE_ADD.LIKE_STORE%TYPE,
    I_new_store_currency IN STORE.CURRENCY_CODE%TYPE,
    I_copy_repl_ind      IN VARCHAR2,
    I_copy_clearance_ind IN VARCHAR2
  )
IS
  L_error_message rtk_errors.rtk_text%type;
BEGIN
  IF LIKE_STORE_THREAD(L_error_message,I_dept_thread,I_commit_max_ctr,I_new_store,I_like_store,I_new_store_currency,I_copy_repl_ind,I_copy_clearance_ind)=false THEN
    IF mthread_restart_sql.upd_error(L_error_message,get_likestore_restart_name(I_new_store),I_dept_thread,L_error_message)                              =false THEN
      raise_application_error(-20001,L_error_message);
    END IF;
    raise_application_error(-20001,L_error_message);
  END IF;
END LIKE_STORE_THREAD;
------------------------------------------------------------------------------------------------
FUNCTION like_store
  (
    O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_new_store          IN STORE_ADD.STORE%TYPE,
    I_like_store         IN STORE_ADD.LIKE_STORE%TYPE,
    I_new_store_currency IN STORE.CURRENCY_CODE%TYPE,
    I_copy_repl_ind      IN VARCHAR2,
    I_copy_clearance_ind IN VARCHAR2
  )
  RETURN BOOLEAN
IS
  L_program   VARCHAR2(64) := 'CORESVC_STORE_ADD_SQL.like_store';
  L_job_name  VARCHAR2(30);
  L_comments  VARCHAR(240);
  L_pls_block VARCHAR2(32000);
  l_quotes    VARCHAR2(5):='''';
  L_jobs_coll varr;
  L_max_threads NUMBER;
  l_wait_ms     NUMBER:=2000;
  CURSOR c_restart_control
  IS
    SELECT num_threads,
      commit_max_ctr
    FROM restart_control
    WHERE program_name = Lp_likestore_program_name;
  L_commit_ctr restart_control.commit_max_ctr%type;
  L_unfinished_count NUMBER:=0;
BEGIN
  OPEN c_restart_control;
  FETCH c_restart_control INTO L_max_threads, L_commit_ctr;
  CLOSE c_restart_control;
  MERGE INTO pos_store PS USING (SELECT pos_config_type,
                                        pos_config_id,
                                        store,
                                        status
                                   FROM pos_store
                                  WHERE store = I_like_store) USE_THIS
                             ON (PS.store = USE_THIS.store
                                 AND PS.pos_config_type = USE_THIS.pos_config_type
                                 AND PS.pos_config_id = USE_THIS.pos_config_id )
  WHEN MATCHED THEN
     UPDATE SET PS.status = USE_THIS.status
  WHEN NOT MATCHED THEN
  INSERT 
    (
      pos_config_type,
      pos_config_id,
      store,
      status
    )
     VALUES
    (USE_THIS.pos_config_type,
     USE_THIS.pos_config_id,
     USE_THIS.store,
     USE_THIS.status
    );
  --
  FOR rec IN
  (SELECT dept FROM deps
    MINUS
   SELECT dept FROM like_store_dept where store = I_new_store
  )
  LOOP
    L_job_name := get_likestore_restart_name
    (
      I_new_store
    )
    ||'_'||rec.dept;
    L_comments  := 'Like Store thread for new-store:'||I_new_store||' like-store:'||I_like_store||' and dept:'||rec.dept;
    L_pls_block := get_likestore_pls_block(rec.dept,L_commit_ctr,I_new_store,I_like_store,I_new_store_currency,I_copy_repl_ind,I_copy_clearance_ind);
    IF mthread_restart_sql.submit_job(--
      O_error_message                 --
      ,L_jobs_coll                    --
      ,L_job_name                     --
      ,L_pls_block                    --
      ,L_comments                     --
      )=false THEN
      RETURN false;
    END IF;
    IF mthread_restart_sql.wait4jobs(O_error_message,L_jobs_coll,L_max_threads,l_wait_ms)=false THEN
      RETURN false;
    END IF;
  END LOOP;
  --Wait for all jobs to finish hence passing max_threads as 1
  IF mthread_restart_sql.wait4jobs(O_error_message,L_jobs_coll,1,l_wait_ms)=false THEN
    RETURN false;
  END IF;
  IF mthread_restart_sql.check_unfinished(O_error_message,L_unfinished_count,get_likestore_restart_name(I_new_store))=false THEN
    RETURN false;
  END IF;
  IF L_unfinished_count > 0 THEN
    O_error_message    := SQL_LIB.CREATE_MSG('THREADS_FAILED', get_likestore_restart_name(I_new_store),L_unfinished_count, NULL);
    RETURN false;
  END IF;
  UPDATE store_add
  SET process_status = STATUS_03LIKESTORE
  WHERE store        = I_new_store;
  COMMIT;
  DBMS_STANDARD.SAVEPOINT('ASYNC');
  IF I_copy_repl_ind                         = 'Y' THEN
    IF copy_repl(O_error_message,I_new_store)=false THEN
      RETURN false;
    END IF;
  END IF;
  COMMIT;
  DBMS_STANDARD.SAVEPOINT('ASYNC');
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END like_store;
------------------------------------------------------------------------------------------------
-- Function Name: ADD_STORE
-- Purpose      : This function contains the core logic for adding a new store to RMS.
--                The process of adding a warehouse to RMS starts with store.fmb form. When user
--                creates a new store using the form an entry is made to store_add table.
--                The overall logic of the program is as follows.
--1. Notify RPM
-- a. Call PM_NOTIFY_API_SQL.NEW_LOCATION
--2. Call L10N_FLEX_ATTRIB_SQL.ADD_STORE_ATTRIB and CFA_SQL.ADD_STORE_ATTRIB.
--3. Insert cost zone
-- a. Check if a store or wh exists for the new store's currency.
--  If yes then new cost-zone need not be inserted.
--  If no and if elc-ind is Y then new cost-zone needs to be inserted.
-- b. For Corporate level zone group, add store to existing zone.
-- c. For Location level zone group, add new zone for the store and add store to the new zone.
-- d. For each zone level zone group, add store to the cost-zone mentioned.
--  If no cost zone mentioned and new cost zone needs to be created (a),
--  then create a cost zone also.
--4. If like-store is mentioned and delivery schedule needs to be copied then
-- copy source-delivery-schedule information (source_dlvry_sched/sched_exc/sched_days tables).
--5. If like-store is mentioned and location close information needs to be copied then
-- do so (company_closed_excep, location_closed).
--6. Insert into store_hierarchy (by calling NEW_STORE)
--7. Make stockledger entries (STKLEDGR_SQL.STOCK_LEDGER_INSERT).
--8. Copy wf_cost_relationship and deal_passthru data for the specified costing location.
--9. If like-store is mentioned then perform like-store functionality.
--11. Clean up  (delete store_add/ _l10n_ext/ _cfa_ext).
------------------------------------------------------------------------------------------------
FUNCTION ADD_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_rms_async_id    IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'CORESVC_STORE_ADD_SQL.ADD_STORE';
   L_store_added_ind CHAR         :='N';
  CURSOR c_check_month_data(I_store store.store%type)
  IS
    SELECT 'Y'
    FROM month_data
    WHERE location         = I_store;
  L_month_data_exists CHAR:='N';

BEGIN

   FOR rec IN (
               SELECT sa.store
                     ,sa.store_name
                     ,sa.store_name10
                     ,sa.store_name3
                     ,sa.store_name_secondary
                     ,sa.email
                     ,sa.store_class
                     ,sa.store_mgr_name
                     ,sa.store_open_date
                     ,sa.store_close_date
                     ,sa.acquired_date
                     ,sa.remodel_date
                     ,sa.fax_number
                     ,sa.phone_number
                     ,sa.total_square_ft
                     ,sa.selling_square_ft
                     ,sa.linear_distance
                     ,sa.vat_region
                     ,sa.vat_include_ind
                     ,sa.store_format
                     ,sa.mall_name
                     ,sa.district
                     ,sa.transfer_zone
                     ,sa.default_wh
                     ,sa.stop_order_days
                     ,sa.start_order_days
                     ,sa.currency_code
                     ,sa.lang
                     ,sa.copy_repl_ind
                     ,sa.like_store
                     ,sa.price_store
                     ,sa.cost_location
                     ,sa.tran_no_generated
                     ,sa.integrated_pos_ind
                     ,sa.copy_dlvry_ind
                     ,sa.copy_activity_ind
                     ,sa.stockholding_ind
                     ,sa.channel_id
                     ,sa.duns_number
                     ,sa.duns_loc
					 ,sa.rowid AS sa_rowid
                     ,sa.sister_store
                     ,st.store_open_date st_store_open_date
                     ,sa.tsf_entity_id
                     ,sa.org_unit_id
                     ,sa.auto_rcv
                     ,sa.remerch_ind
                     ,sa.store_type
                     ,sa.wf_customer_id
                     ,sa.timezone_name
                     ,sa.customer_order_loc_ind
                     ,sa.copy_clearance_ind
                     ,DECODE(st.store, NULL, 'N', 'Y') store_added_ind
					 ,sa.process_mode AS process_mode
                      -- check if a store or wh exists for the currency of the new store.
                     ,NVL( (
                            SELECT 'Y'
                              FROM wh w1
                             WHERE w1.currency_code = sa.currency_code
                               AND rownum = 1
                            ), NVL( (
                                     SELECT 'Y'
                                       FROM store s1
                                      WHERE s1.currency_code = sa.currency_code
                                        AND rownum           = 1
                                     ), 'N'
                                   )
                          ) AS curr_loc_exist
                      -- check if a store or wh exists for the cost_location of the new store.
                     ,NVL( (
                            SELECT 'Y'
                              FROM wh w1
                             WHERE w1.wh = sa.cost_location
                               AND rownum  = 1
                            ), NVL( (
                                     SELECT 'Y'
                                       FROM store s1
                                      WHERE s1.store = sa.cost_location
                                        AND rownum = 1
                                     ), 'N'
                                   )
                          ) AS cost_loc_exist
                 FROM store_add sa
                     ,store     st
                WHERE sa.store        = st.store(+)
                  AND sa.rms_async_id = I_rms_async_id
                ORDER BY sa.store
               )
   LOOP
      -- This check is to handle the scenario where store add processing was successful
      -- MV refresh was called and it committed the changes. And, then failure happened
      -- In that case when retry is done, this will ensure that store add processing is skipped
      -- and flow goes to like-store processing.
	  L_store_added_ind     := rec.store_added_ind;
      IF rec.store_added_ind = 'N' THEN
         -- make entry into store table. this is needed before entry in other tables can
         -- be made.
         INSERT INTO store(
                           store
                          ,store_name
                          ,store_name10
                          ,store_name3
                          ,store_name_secondary
                          ,email
                          ,store_class
                          ,store_mgr_name
                          ,store_open_date
                          ,store_close_date
                          ,acquired_date
                          ,remodel_date
                          ,fax_number
                          ,phone_number
                          ,total_square_ft
                          ,selling_square_ft
                          ,linear_distance
                          ,vat_region
                          ,vat_include_ind
                          ,stockholding_ind
                          ,channel_id
                          ,store_format
                          ,mall_name
                          ,district
                          ,transfer_zone
                          ,default_wh
                          ,stop_order_days
                          ,start_order_days
                          ,currency_code
                          ,orig_currency_code
                          ,lang
                          ,duns_number
                          ,duns_loc
                          ,tran_no_generated
                          ,integrated_pos_ind
                          ,sister_store
                          ,tsf_entity_id
                          ,org_unit_id
                          ,auto_rcv
                          ,remerch_ind
                          ,store_type
                          ,wf_customer_id
                          ,timezone_name
                          ,customer_order_loc_ind
                           )
                    VALUES(
                           rec.store
                          ,rec.store_name
                          ,rec.store_name10
                          ,rec.store_name3
                          ,rec.store_name_secondary
                          ,rec.email
                          ,rec.store_class
                          ,rec.store_mgr_name
                          ,rec.store_open_date
                          ,rec.store_close_date
                          ,rec.acquired_date
                          ,rec.remodel_date
                          ,rec.fax_number
                          ,rec.phone_number
                          ,rec.total_square_ft
                          ,rec.selling_square_ft
                          ,rec.linear_distance
                          ,rec.vat_region
                          ,rec.vat_include_ind
                          ,rec.stockholding_ind
                          ,rec.channel_id
                          ,rec.store_format
                          ,rec.mall_name
                          ,rec.district
                          ,rec.transfer_zone
                          ,rec.default_wh
                          ,rec.stop_order_days
                          ,rec.start_order_days
                          ,rec.currency_code
                          ,rec.currency_code
                          ,rec.lang
                          ,rec.duns_number
                          ,rec.duns_loc
                          ,rec.tran_no_generated
                          ,rec.integrated_pos_ind
                          ,rec.sister_store
                          ,rec.tsf_entity_id
                          ,rec.org_unit_id
                          ,rec.auto_rcv
                          ,rec.remerch_ind
                          ,rec.store_type
                          ,rec.wf_customer_id
                          ,rec.timezone_name
                          ,rec.customer_order_loc_ind
                           );

         insert into store_tl( lang
                              ,store
                              ,store_name
                              ,store_name_secondary
                              ,create_datetime
                              ,create_id
                              ,last_update_datetime
                              ,last_update_id
                              )
                      select  lang
                             ,store
                             ,store_name
                             ,store_name_secondary
                             ,create_datetime
                             ,create_id
                             ,last_update_datetime
                             ,last_update_id
                        from store_add_tl
                       where store = rec.store;

         -- if price store was given then, create pricing records.
         IF rec.price_store IS NOT NULL THEN
            IF PM_NOTIFY_API_SQL.NEW_LOCATION(O_error_message,
                                              rec.store,
                                              'S',
                                              rec.price_store) = FALSE THEN
               RETURN FALSE;
            END IF;
         END IF;
         ---
		 IF CFA_SQL.ADD_STORE_ATTRIB(O_error_message,rec.store) = FALSE THEN
			RETURN FALSE;
		 END IF;
		 ---
         FOR zg IN (
                    SELECT czg.zone_group_id
                          ,czg.cost_level
                          ,(
                            CASE
                               WHEN czg.cost_level = 'C'
                               THEN (
                                     SELECT cz.zone_id
                                       FROM cost_zone cz
                                      WHERE cz.zone_group_id = czg.zone_group_id
                                     )
                               WHEN     czg.cost_level     = 'Z'
                                    AND rec.cost_loc_exist = 'Y'
                                    AND rec.curr_loc_exist = 'Y'
                               THEN (
                                     SELECT czgl.zone_id
                                       FROM cost_zone_group_loc czgl
                                      WHERE czgl.zone_group_id = czg.zone_group_id
                                        AND czgl.location      = rec.cost_location
                                     )
                               WHEN     czg.cost_level = 'Z'
                                    AND rec.cost_loc_exist = 'N'
                                    AND rec.curr_loc_exist = 'Y'
                               THEN rec.cost_location
                               WHEN czg.cost_level = 'L'
                               THEN rec.store
                               WHEN     czg.cost_level     = 'Z'
                                    AND rec.curr_loc_exist = 'N'
                               THEN NVL( (
                                          SELECT MIN(MOD(c1.zone_id, 9999999999) + 1)
                                            FROM cost_zone c1
                                           WHERE NOT EXISTS (
                                                             SELECT c2.zone_id
                                                               FROM cost_zone c2
                                                              WHERE c2.zone_group_id = czg.zone_group_id
                                                                AND c2.zone_id       = (MOD(c1.zone_id, 9999999999) + 1)
                                                             )
                                             AND c1.zone_group_id = czg.zone_group_id
                                          ), 1
                                        )
                               ELSE NULL
                            END
                            ) AS zone_id
                      FROM cost_zone_group czg
                          ,system_options  so
                     WHERE so.elc_ind = 'Y'
                    )
         LOOP
            IF zg.zone_id IS NOT NULL THEN
               IF    zg.cost_level = 'L'
                  OR (    zg.cost_level      = 'Z'
                      AND rec.curr_loc_exist = 'N') THEN
                  ---
                  INSERT INTO cost_zone(
                                        zone_group_id
                                       ,zone_id
                                       ,description
                                       ,currency_code
                                       ,base_cost_ind
                                        )
                                 VALUES(
                                        zg.zone_group_id
                                       ,zg.zone_id
                                       ,rec.store_name
                                       ,rec.currency_code
                                       ,'N'
                                        );
               END IF;
               ---
               INSERT INTO cost_zone_group_loc(
                                               zone_group_id
                                              ,location
                                              ,loc_type
                                              ,zone_id
                                               )
                                        VALUES(
                                               zg.zone_group_id
                                              ,rec.store
                                              ,'S'
                                              ,zg.zone_id
                                               );                                              
            END IF;
         END LOOP;
         ---
         IF     rec.like_store     IS NOT NULL
            AND rec.copy_dlvry_ind  = 'Y' THEN
            ---
            INSERT INTO source_dlvry_sched(
                                           source
                                          ,source_type
                                          ,location
                                          ,loc_type
                                          ,delivery_cycle
                                          ,start_date
                                           )
                                          (
                                           SELECT s1.source
                                                 ,s1.source_type
                                                 ,rec.store
                                                 ,s1.loc_type
                                                 ,s1.delivery_cycle
                                                 ,s1.start_date
                                             FROM source_dlvry_sched s1
                                            WHERE s1.location = rec.like_store
                                              AND s1.loc_type = 'S'
                                           );
            ---
            INSERT INTO source_dlvry_sched_exc(
                                               source
                                              ,source_type
                                              ,location
                                              ,loc_type
                                              ,day
                                              ,item
                                               )
                                              (
                                               SELECT s1.source
                                                     ,s1.source_type
                                                     ,rec.store
                                                     ,s1.loc_type
                                                     ,s1.day
                                                     ,s1.item
                                                FROM source_dlvry_sched_exc s1
                                               WHERE s1.location   = rec.like_store
                                                 AND s1.loc_type   = 'S'
                                               );
            ---
            INSERT INTO source_dlvry_sched_days(
                                                source
                                               ,source_type
                                               ,location
                                               ,loc_type
                                               ,DAY
                                               ,start_time
                                               ,end_time
                                                )
                                               (
                                                SELECT s1.source
                                                      ,s1.source_type
                                                      ,rec.store
                                                      ,s1.loc_type
                                                      ,s1.day
                                                      ,s1.start_time
                                                      ,s1.end_time
                                                  FROM source_dlvry_sched_days s1
                                                 WHERE s1.location = rec.like_store
                                                   AND s1.loc_type = 'S'
                                                );
         END IF;
         ---
         IF     rec.like_store        IS NOT NULL
            AND rec.copy_activity_ind  = 'Y' THEN
            ---
            INSERT INTO company_closed_excep(
                                             close_date
                                            ,location
                                            ,loc_type
                                            ,sales_ind
                                            ,recv_ind
                                            ,ship_ind
                                             )
                                            (
                                             SELECT c1.close_date
                                                   ,rec.store
                                                   ,c1.loc_type
                                                   ,c1.sales_ind
                                                   ,c1.recv_ind
                                                   ,c1.ship_ind
                                               FROM company_closed_excep c1
                                                   ,period               p
                                              WHERE c1.location    = rec.like_store
                                                AND c1.loc_type    = 'S'
                                                AND c1.close_date >= p.vdate
                                           );
            ---
            INSERT INTO location_closed(
                                        close_date
                                       ,location
                                       ,loc_type
                                       ,sales_ind
                                       ,recv_ind
                                       ,ship_ind
                                       ,reason
                                       )
                                      (
                                       SELECT c1.close_date
                                             ,rec.store
                                             ,c1.loc_type
                                             ,c1.sales_ind
                                             ,c1.recv_ind
                                             ,c1.ship_ind
                                             ,c1.reason
                                         FROM location_closed c1
                                             ,period          p
                                        WHERE c1.location    = rec.like_store
                                          AND c1.loc_type    = 'S'
                                          AND c1.close_date >= p.vdate
                                       );
         END IF;
         ---
         IF NEW_STORE(O_error_message,
                      rec.store,
                      rec.district) = FALSE THEN
            RETURN false;
         END IF;
         ---
         IF     rec.cost_location  IS NOT NULL
            AND rec.cost_loc_exist  = 'Y' THEN
            ---
            INSERT INTO wf_cost_relationship(
                                             dept
                                            ,class
                                            ,subclass
                                            ,location
                                            ,start_date
                                            ,end_date
                                            ,templ_id
                                            ,item
                                             )
                                            (
                                             SELECT dept
                                                   ,class
                                                   ,subclass
                                                   ,rec.store
                                                   ,start_date
                                                   ,end_date
                                                   ,templ_id
                                                   ,item
                                               FROM wf_cost_relationship
                                              WHERE location = rec.cost_location
                                             );
            ---
            INSERT INTO deal_passthru(
                                      dept
                                     ,supplier
                                     ,costing_loc
                                     ,location
                                     ,loc_type
                                     ,passthru_pct
                                      )
                                     (
                                      SELECT dept
                                            ,supplier
                                            ,costing_loc
                                            ,rec.store
                                            ,loc_type
                                            ,passthru_pct
                                        FROM deal_passthru
                                       WHERE location = rec.cost_location
                                      );
         END IF;
      END IF; --store already added
    -- 
     UPDATE store_add 
     SET process_status = STATUS_01STOREADD 
     WHERE rowid        = rec.sa_rowid; 
     COMMIT; 
     DBMS_STANDARD.SAVEPOINT('ASYNC'); 
    -- 

      -- refresh MVs. This will commit the changes done so far.
      IF SET_OF_BOOKS_SQL.REFRESH_MV_LOC_SOB(O_error_message) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      IF L10N_SQL.REFRESH_MV_L10N_ENTITY(O_error_message) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      IF ADDRESS_SQL.REFRESH_MV_LOC_PRIM_ADDR(O_error_message) = FALSE THEN
         RETURN FALSE;
      END IF;
      
      -- The save point named ASYNC was issued at the very beginning in NOTIFY_STORE_ADD
      -- Since MV refresh has committed the data hence the save point has been lost.
      -- reissue save point so that WRITE_ERROR function does not fail while trying to rollback to it.
      DBMS_STANDARD.SAVEPOINT('ASYNC');
      
      IF NOT (    rec.store_type = 'F'
              AND rec.stockholding_ind = 'N') THEN
	   --if store was already added then check if month-data is also added 
       --call STOCK_LEDGER_INSERT only if month-data is not already created 
		 IF L_store_added_ind = 'Y' THEN 
             OPEN c_check_month_data(rec.store); 
             FETCH c_check_month_data INTO L_month_data_exists; 
             CLOSE c_check_month_data; 
         END IF; 
		IF L_month_data_exists = 'N' THEN

         ---
         IF STKLEDGR_SQL.STOCK_LEDGER_INSERT('S', 
                                             NULL, 
                                             NULL, 
                                             NULL, 
                                             rec.store, 
                                             O_error_message) = FALSE THEN
            RETURN false;
		  END IF;
         END IF;
      END IF;
      --
	 -- 
     UPDATE store_add 
     SET process_status = STATUS_02STOREADD_POST 
     WHERE rowid        = rec.sa_rowid; 
     COMMIT; 
     DBMS_STANDARD.SAVEPOINT('ASYNC'); 
     -- 
    IF rec.like_store IS NOT NULL AND rec.process_mode = 'ASYNC' THEN
         IF LIKE_STORE(O_error_message,
                       rec.store,
                       rec.like_store,
                       rec.currency_code,
                       rec.copy_repl_ind,
                       rec.copy_clearance_ind) = FALSE THEN
            RETURN false;
         END IF;
       DELETE FROM like_store_dept WHERE store = rec.store;
      END IF;
    IF rec.like_store IS NULL OR rec.process_mode = 'ASYNC' THEN
      DELETE FROM store_add_l10n_ext WHERE store = rec.store;
      DELETE FROM store_add_cfa_ext WHERE store = rec.store;
      DELETE FROM store_add_tl WHERE store = rec.store;
      DELETE FROM store_add WHERE store_add.rowid = rec.sa_rowid;
	END IF;
	--
      
  END LOOP;
  
  RETURN TRUE;
  
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END ADD_STORE;
------------------------------------------------------------------------------------------------
FUNCTION like_store_batch(
    O_error_message IN OUT rtk_errors.rtk_text%type)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_STORE_ADD_SQL.like_store_batch';
BEGIN
  FOR rec IN
  (SELECT store,
    like_store,
    currency_code,
    copy_repl_ind,
    copy_clearance_ind,
    rowid AS sa_rowid
  FROM store_add
  WHERE process_mode ='BATCH'
  AND process_status = STATUS_02STOREADD_POST
  )
  LOOP
    IF rec.like_store                                                                                                  IS NOT NULL THEN
      IF LIKE_STORE(O_error_message,rec.store,rec.like_store,rec.currency_code,rec.copy_repl_ind,rec.copy_clearance_ind)=false THEN
        RETURN false;
      END IF;
       DELETE FROM like_store_dept WHERE store = rec.store;
    END IF;
    DELETE FROM store_add_l10n_ext WHERE store = rec.store;
    DELETE FROM store_add_cfa_ext WHERE store = rec.store;
    DELETE FROM store_add_tl WHERE store = rec.store;
    DELETE FROM store_add WHERE store_add.rowid = rec.sa_rowid;
    --
  END LOOP;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END like_store_batch;
------------------------------------------------------------------------------------------------
PROCEDURE like_store_batch
IS
  L_error_message rtk_errors.rtk_text%type;
BEGIN
  IF like_store_batch(L_error_message)=false THEN
    raise_application_error(-20001,L_error_message);
  END IF;
END like_store_batch;
------------------------------------------------------------------------------------------------
FUNCTION wait4status(
    O_error_message IN OUT rtk_errors.rtk_text%type,
    I_status        IN store_add.process_status%type,
    I_wait_sec      IN NUMBER DEFAULT 30)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_STORE_ADD_SQL.wait4status';
  CURSOR c_check_status
  IS
    SELECT store FROM store_add WHERE NVL(process_status,'00NEW')<I_status;
  L_store store_add.store%type;
  L_exit BOOLEAN:=false;
BEGIN
  IF I_status       IS NULL OR I_status NOT IN (STATUS_01STOREADD,STATUS_02STOREADD_POST,STATUS_03LIKESTORE) THEN
    O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_PROCESS_STATUS',NULL,NULL,NULL);
    RETURN false;
  END IF;
  LOOP
    L_store := NULL;
    OPEN c_check_status;
    FETCH c_check_status INTO L_store;
    IF c_check_status%notfound THEN
      L_exit := true;
    END IF;
    CLOSE c_check_status;
    EXIT
  WHEN L_exit;
    mthread_restart_sql.sleep_ms(NVL(I_wait_sec,30)*1000);
  END LOOP;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END wait4status;
------------------------------------------------------------------------------------------------
PROCEDURE wait4status(
    I_status   IN store_add.process_status%type,
    I_wait_sec IN NUMBER DEFAULT 30)
IS
  L_error_message rtk_errors.rtk_text%type;
BEGIN
  IF wait4status(L_error_message,I_status,I_wait_sec)=false THEN
    raise_application_error(-20001,L_error_message);
  END IF;
END wait4status;
------------------------------------------------------------------------------------------------
FUNCTION add_store_batch(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_STORE_ADD_SQL.add_store_batch';
BEGIN
  FOR rec IN
  (SELECT rms_async_id FROM store_add
  )
  LOOP
    IF add_store(O_error_message,rec.rms_async_id)=false THEN
      RETURN false;
    END IF;
    COMMIT;
	DBMS_STANDARD.SAVEPOINT('ASYNC');
  END LOOP;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END add_store_batch;
------------------------------------------------------------------------------------------------
PROCEDURE add_store_batch
IS
  L_error_message RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  IF add_store_batch(L_error_message)=false THEN
    raise_application_error(-20001,L_error_message);
  END IF;
END add_store_batch;
------------------------------------------------------------------------------------------------
FUNCTION get_likestore_pls_block(
    I_dept               IN deps.dept%type,
    I_commit_ctr         IN restart_control.commit_max_ctr%type,
    I_new_store          IN store.store%type,
    I_like_store         IN store_add.like_store%type,
    I_new_store_currency IN store.currency_code%type,
    I_copy_repl_ind      IN store_add.copy_repl_ind%type,
    I_copy_clearance_ind IN store_add.copy_clearance_ind%type)
  RETURN VARCHAR2
IS
  l_quotes CHAR:='''';
BEGIN
  RETURN 'begin coresvc_store_add_sql.like_store_thread('||I_dept||','||I_commit_ctr||','||I_new_store||','||I_like_store||','||l_quotes||I_new_store_currency||l_quotes||','||l_quotes||I_copy_repl_ind||l_quotes||','||l_quotes||I_copy_clearance_ind||l_quotes||'); end;';
END get_likestore_pls_block;
------------------------------------------------------------------------------------------------
PROCEDURE set_process_mode(
    I_mode IN process_config.process_mode%type)
IS
  L_error_message rtk_errors.rtk_text%type;
BEGIN
  IF I_mode         IS NULL OR I_mode NOT IN ('ASYNC','BATCH') THEN
    L_error_message := SQL_LIB.CREATE_MSG('INV_PROCESS_MODE',NULL,NULL,NULL);
    raise_application_error(-20001,L_error_message);
  END IF;
  UPDATE process_config
  SET process_mode   = I_mode
  WHERE process_name = Lp_likestore_program_name;
END set_process_mode;
------------------------------------------------------------------------------------------------
FUNCTION GET_PROCESS_MODE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_Process_Mode       OUT    PROCESS_CONFIG.PROCESS_MODE%TYPE )
    RETURN BOOLEAN
  IS
  L_program_name VARCHAR2(255) := 'CORESVC_STORE_ADD_SQL.GET_PROCESS_MODE';
  Cursor C_GET_PROCESS_MODE is
  Select process_mode
   from process_config
  where process_name = 'LIKE_STORE';
BEGIN
   open C_GET_PROCESS_MODE;
	fetch C_GET_PROCESS_MODE into O_Process_Mode;
	close C_GET_PROCESS_MODE;
   
   return TRUE;
EXCEPTION
 when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END GET_PROCESS_MODE;
----------------------------------------------------------------------------------------------
END CORESVC_STORE_ADD_SQL;
/
