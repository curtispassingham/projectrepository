
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY POS_VALIDATION_SQL AS
---------------------------------------------------------------------------------------
FUNCTION STORE (O_error_message   IN OUT  VARCHAR2,
                O_exists          IN OUT  VARCHAR2,
                O_store_name      IN OUT  VARCHAR2,
                I_pos_config_type IN      POS_STORE.POS_CONFIG_TYPE%TYPE,
                I_pos_config_id   IN      POS_STORE.POS_CONFIG_ID%TYPE,
                I_store           IN      POS_STORE.STORE%TYPE) return BOOLEAN IS

   L_fetched_store   POS_STORE.STORE%TYPE;

   cursor C_GET_STORE_NAME_COUP is   
      select s.store
        from store s,
             pos_coupon_head pch
       where s.currency_code = pch.currency_code
         and pch.coupon_id = I_pos_config_id
         and I_pos_config_type = 'COUP'
         and I_store = s.store;

   cursor C_GET_STORE_NAME_PRES is
      select s.store
        from store s,
             pos_prod_rest_head   pprr
       where s.currency_code = nvl(pprr.currency_code, s.currency_code)
         and pprr.pos_prod_rest_id = I_pos_config_id
         and I_pos_config_type = 'PRES'
         and I_store = s.store;

BEGIN
   if I_pos_config_type = 'COUP' then
      SQL_LIB.SET_MARK('OPEN','C_GET_STORE_NAME_COUP','STORE','store: '||TO_CHAR(I_store));
      open C_GET_STORE_NAME_COUP;
      SQL_LIB.SET_MARK('FETCH','C_GET_STORE_NAME_COUP','STORE','store: '||TO_CHAR(I_store));
      fetch C_GET_STORE_NAME_COUP into L_fetched_store;
      if C_GET_STORE_NAME_COUP%NOTFOUND then
         O_exists :='N';
         O_error_message := 'STR_COUP_CURR_NO_MATCH';
      else   
         O_exists := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_STORE_NAME_COUP','STORE','store: '||TO_CHAR(I_store));
      close C_GET_STORE_NAME_COUP;
   elsif I_pos_config_type = 'PRES' then
      SQL_LIB.SET_MARK('OPEN','C_GET_STORE_NAME_PRES','STORE','store: '||TO_CHAR(I_store));
      open C_GET_STORE_NAME_PRES;
      SQL_LIB.SET_MARK('FETCH','C_GET_STORE_NAME_PRES','STORE','store: '||TO_CHAR(I_store));
      fetch C_GET_STORE_NAME_PRES into L_fetched_store;
      if C_GET_STORE_NAME_PRES%NOTFOUND then
         O_exists :='N';
      else   
         O_exists := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_STORE_NAME_PRES','STORE','store: '||TO_CHAR(I_store));
      close C_GET_STORE_NAME_PRES;
   end if;
   ---
   if O_exists = 'Y' then
      if STORE_ATTRIB_SQL.GET_NAME(O_error_message,
                                   L_fetched_store,
                                   O_store_name) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'POS_VALIDATION_SQL.STORE',
                                            to_char(SQLCODE));
      return FALSE;


END STORE;
--------------------------------------------------------------------------------
FUNCTION DISTRICT (O_error_message   IN OUT  VARCHAR2,
                   O_exists          IN OUT  VARCHAR2,
                   O_district_name   IN OUT  VARCHAR2,
                   I_pos_config_type IN      POS_STORE.POS_CONFIG_TYPE%TYPE,
                   I_pos_config_id   IN      POS_STORE.POS_CONFIG_ID%TYPE,
                   I_district        IN      NUMBER) return BOOLEAN IS

   L_fetched_district    NUMBER;

   cursor C_GET_DISTRICT_NAME_COUP is
      select d.district
        from district d,
             store s,
             pos_coupon_head      pch
       where s.currency_code = pch.currency_code
         and pch.coupon_id   = I_pos_config_id
         and I_pos_config_type = 'COUP'
         and s.district = d.district
         and I_district = d.district;

   cursor C_GET_DISTRICT_NAME_PRES is
      select d.district
        from district d,
             store s,
             pos_prod_rest_head   pprr
       where s.currency_code = nvl(pprr.currency_code, s.currency_code)
         and pprr.pos_prod_rest_id = I_pos_config_id
         and I_pos_config_type = 'PRES'
         and s.district = d.district
         and I_district = d.district;

BEGIN
   if I_pos_config_type = 'COUP' then
      SQL_LIB.SET_MARK('OPEN','C_GET_DISTRICT_NAME_COUP','DISTRICT','district: '||TO_CHAR(I_district));
      open C_GET_DISTRICT_NAME_COUP;
      SQL_LIB.SET_MARK('FETCH','C_GET_DISTRICT_NAME_COUP','DISTRICT','district: '||TO_CHAR(I_district));
      fetch C_GET_DISTRICT_NAME_COUP into L_fetched_district;
      if C_GET_DISTRICT_NAME_COUP%NOTFOUND then
         O_exists :='N';
      else   
         O_exists := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DISTRICT_NAME_COUP','DISTRICT','district: '||TO_CHAR(I_district));
      close C_GET_DISTRICT_NAME_COUP;
   elsif I_pos_config_type = 'PRES' then
      SQL_LIB.SET_MARK('OPEN','C_GET_DISTRICT_NAME_PRES','DISTRICT','district: '||TO_CHAR(I_district));
      open C_GET_DISTRICT_NAME_PRES;
      SQL_LIB.SET_MARK('FETCH','C_GET_DISTRICT_NAME_PRES','DISTRICT','district: '||TO_CHAR(I_district));
      fetch C_GET_DISTRICT_NAME_PRES into L_fetched_district;
      if C_GET_DISTRICT_NAME_PRES%NOTFOUND then
         O_exists :='N';
      else   
         O_exists := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DISTRICT_NAME_PRES','DISTRICT','district: '||TO_CHAR(I_district));
      close C_GET_DISTRICT_NAME_PRES;
   end if;
   ---
   if O_exists = 'Y' then
      if ORGANIZATION_ATTRIB_SQL.DISTRICT_NAME(O_error_message,
                                               L_fetched_district,
                                               O_district_name) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'POS_VALIDATION_SQL.DISTRICT',
                                            to_char(SQLCODE));
      return FALSE;


END DISTRICT;
--------------------------------------------------------------------------------
FUNCTION REGION (O_error_message   IN OUT  VARCHAR2,
                 O_exists          IN OUT  VARCHAR2,
                 O_region_name     IN OUT  VARCHAR2,
                 I_pos_config_type IN      POS_STORE.POS_CONFIG_TYPE%TYPE,
                 I_pos_config_id   IN      POS_STORE.POS_CONFIG_ID%TYPE,
                 I_region          IN      NUMBER) return BOOLEAN IS

   L_fetched_region  NUMBER;

   cursor C_GET_REGION_NAME_COUP is
      select r.region
        from region r,
             district d,
             store s,
             pos_coupon_head pch
       where s.currency_code = pch.currency_code
         and pch.coupon_id   = I_pos_config_id
         and I_pos_config_type = 'COUP'
         and I_region = r.region
         and d.region = r.region
         and s.district = d.district;

   cursor C_GET_REGION_NAME_PRES is
      select r.region
        from region r,
             district d,
             store s,
             pos_prod_rest_head pprh
       where s.currency_code = nvl(pprh.currency_code, s.currency_code)
         and pprh.pos_prod_rest_id = I_pos_config_id
         and I_pos_config_type = 'PRES'
         and I_region = r.region
         and d.region = r.region
         and s.district = d.district;

BEGIN
   if I_pos_config_type = 'COUP' then
      SQL_LIB.SET_MARK('OPEN','C_GET_REGION_NAME_COUP','REGION','region: '||TO_CHAR(I_region));
      open C_GET_REGION_NAME_COUP;
      SQL_LIB.SET_MARK('FETCH','C_GET_REGION_NAME_COUP','REGION','region: '||TO_CHAR(I_region));
      fetch C_GET_REGION_NAME_COUP into L_fetched_region;
      if C_GET_REGION_NAME_COUP%NOTFOUND then
         O_exists :='N';
      else   
         O_exists := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_REGION_NAME_COUP','REGION','region: '||TO_CHAR(I_region));
      close C_GET_REGION_NAME_COUP;
   elsif I_pos_config_type = 'PRES' then
      SQL_LIB.SET_MARK('OPEN','C_GET_REGION_NAME_PRES','REGION','region: '||TO_CHAR(I_region));
      open C_GET_REGION_NAME_PRES;
      SQL_LIB.SET_MARK('FETCH','C_GET_REGION_NAME_PRES','REGION','region: '||TO_CHAR(I_region));
      fetch C_GET_REGION_NAME_PRES into L_fetched_region;
      if C_GET_REGION_NAME_PRES%NOTFOUND then
         O_exists :='N';
      else   
         O_exists := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_REGION_NAME_PRES','REGION','region: '||TO_CHAR(I_region));
      close C_GET_REGION_NAME_PRES;
   end if;
   ---
   if O_exists = 'Y' then
      if ORGANIZATION_ATTRIB_SQL.REGION_NAME(O_error_message,
                                             L_fetched_region,
                                             O_region_name) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'POS_VALIDATION_SQL.REGION',
                                            to_char(SQLCODE));
      return FALSE;


END REGION;
--------------------------------------------------------------------------------
FUNCTION AREA (O_error_message   IN OUT  VARCHAR2,
               O_exists          IN OUT  VARCHAR2,
               O_area_name       IN OUT  VARCHAR2,
               I_pos_config_type IN      POS_STORE.POS_CONFIG_TYPE%TYPE,
               I_pos_config_id   IN      POS_STORE.POS_CONFIG_ID%TYPE,
               I_area            IN      NUMBER) return BOOLEAN IS

   L_fetched_area   NUMBER;

   cursor C_GET_AREA_NAME_COUP is
      select a.area
        from area a,
             region r,
             district d,
             store s,
             pos_coupon_head pch
       where s.currency_code = pch.currency_code
         and pch.coupon_id = I_pos_config_id
         and I_pos_config_type = 'COUP'
         and I_area = a.area
         and r.area = a.area
         and d.region = r.region
         and s.district = d.district;

   cursor C_GET_AREA_NAME_PRES is
      select a.area
        from area a,
             region r,
             district d,
             store s,
             pos_prod_rest_head pprh
       where s.currency_code = nvl(pprh.currency_code, s.currency_code)
         and pprh.pos_prod_rest_id = I_pos_config_id
         and I_pos_config_type = 'PRES'
         and I_area = a.area
         and r.area = a.area
         and d.region = r.region
         and s.district = d.district;

BEGIN
   if I_pos_config_type = 'COUP' then
      SQL_LIB.SET_MARK('OPEN','C_GET_AREA_NAME_COUP','AREA','area: '||TO_CHAR(I_area));
      open C_GET_AREA_NAME_COUP;
      SQL_LIB.SET_MARK('FETCH','C_GET_AREA_NAME_COUP','AREA','area: '||TO_CHAR(I_area));
      fetch C_GET_AREA_NAME_COUP into L_fetched_area;
      if C_GET_AREA_NAME_COUP%NOTFOUND then
         O_exists :='N';
      else   
         O_exists := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_AREA_NAME_COUP','AREA','area: '||TO_CHAR(I_area));
      close C_GET_AREA_NAME_COUP;
   elsif I_pos_config_type = 'PRES' then
      SQL_LIB.SET_MARK('OPEN','C_GET_AREA_NAME_PRES','AREA','area: '||TO_CHAR(I_area));
      open C_GET_AREA_NAME_PRES;
      SQL_LIB.SET_MARK('FETCH','C_GET_AREA_NAME_PRES','AREA','area: '||TO_CHAR(I_area));
      fetch C_GET_AREA_NAME_PRES into L_fetched_area;
      if C_GET_AREA_NAME_PRES%NOTFOUND then
         O_exists :='N';
      else   
         O_exists := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_AREA_NAME_PRES','AREA','area: '||TO_CHAR(I_area));
      close C_GET_AREA_NAME_PRES;
   end if;
   ---
   if O_exists = 'Y' then
      if ORGANIZATION_ATTRIB_SQL.AREA_NAME(O_error_message,
                                           L_fetched_area,
                                           O_area_name) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'POS_VALIDATION_SQL.AREA',
                                            to_char(SQLCODE));
      return FALSE;

END AREA;
--------------------------------------------------------------------------------
FUNCTION LOC_LIST (O_error_message   IN OUT  VARCHAR2,
                   O_exists          IN OUT  VARCHAR2,
                   O_loc_list_desc   IN OUT  LOC_LIST_HEAD.LOC_LIST_DESC%TYPE,
                   I_pos_config_type IN      POS_STORE.POS_CONFIG_TYPE%TYPE,
                   I_pos_config_id   IN      POS_STORE.POS_CONFIG_ID%TYPE,
                   I_loc_list        IN      LOC_LIST_HEAD.LOC_LIST%TYPE) return BOOLEAN IS

   L_loc_list_fetched    LOC_LIST_HEAD.LOC_LIST%TYPE;

   cursor C_GET_LOC_LIST_DESC_COUP is
      select lld.loc_list
        from loc_list_detail lld,
             store s,
             pos_coupon_head pch
       where get_primary_lang = get_user_lang
         and s.store = lld.location
         and s.currency_code = pch.currency_code
         and pch.coupon_id = I_pos_config_id
         and I_pos_config_type = 'COUP'
         and I_loc_list = lld.loc_list;

   cursor C_GET_LOC_LIST_DESC_PRES is
      select lld.loc_list
        from loc_list_detail lld,
             store s,
             pos_prod_rest_head pprh
       where s.store = lld.location
         and s.currency_code = nvl(pprh.currency_code, s.currency_code)
         and pprh.pos_prod_rest_id = I_pos_config_id
         and I_pos_config_type = 'PRES'
         and I_loc_list = lld.loc_list;

BEGIN
   if I_pos_config_type = 'COUP' then
      SQL_LIB.SET_MARK('OPEN','C_GET_LOC_LIST_DESC_COUP','LOC_LIST','loc_list: '||TO_CHAR(I_loc_list));
      open C_GET_LOC_LIST_DESC_COUP;
      SQL_LIB.SET_MARK('FETCH','C_GET_LOC_LIST_DESC_COUP','LOC_LIST','loc_list: '||TO_CHAR(I_loc_list));
      fetch C_GET_LOC_LIST_DESC_COUP into L_loc_list_fetched;
      if C_GET_LOC_LIST_DESC_COUP%NOTFOUND then
         O_exists :='N';
      else   
         O_exists := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_LOC_LIST_DESC_COUP','LOC_LIST','loc_list: '||TO_CHAR(I_loc_list));
      close C_GET_LOC_LIST_DESC_COUP;
   elsif I_pos_config_type = 'PRES' then
      SQL_LIB.SET_MARK('OPEN','C_GET_LOC_LIST_DESC_PRES','LOC_LIST','loc_list: '||TO_CHAR(I_loc_list));
      open C_GET_LOC_LIST_DESC_PRES;
      SQL_LIB.SET_MARK('FETCH','C_GET_LOC_LIST_DESC_PRES','LOC_LIST','loc_list: '||TO_CHAR(I_loc_list));
      fetch C_GET_LOC_LIST_DESC_PRES into L_loc_list_fetched;
      if C_GET_LOC_LIST_DESC_PRES%NOTFOUND then
         O_exists :='N';
      else   
         O_exists := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_LOC_LIST_DESC_PRES','LOC_LIST','loc_list: '||TO_CHAR(I_loc_list));
      close C_GET_LOC_LIST_DESC_PRES;
   end if;
   ---
   if LOCLIST_ATTRIBUTE_SQL.GET_DESC(O_error_message,
                                     O_loc_list_desc,
                                     L_loc_list_fetched) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'POS_VALIDATION_SQL.LOC_LIST',
                                            to_char(SQLCODE));
      return FALSE;


END LOC_LIST;
---------------------------------------------------------------------------------
END POS_VALIDATION_SQL;

/


