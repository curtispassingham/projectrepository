
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSMFM_SEEDDATA AUTHID CURRENT_USER AS
/*--- message type parameters ---*/
HDR_CRE_TYPE          VARCHAR2(30) := 'CodeHdrCre';
HDR_MOD_TYPE          VARCHAR2(30) := 'CodeHdrMod';
HDR_DEL_TYPE          VARCHAR2(30) := 'CodeHdrDel';
DTL_CRE_TYPE          VARCHAR2(30) := 'CodeDtlCre';
DTL_MOD_TYPE          VARCHAR2(30) := 'CodeDtlMod';
DTL_DEL_TYPE          VARCHAR2(30) := 'CodeDtlDel';
DIFF_TYPE_CRE_TYPE    VARCHAR2(30) := 'DiffTypeCre';
DIFF_TYPE_MOD_TYPE    VARCHAR2(30) := 'DiffTypeMod';
DIFF_TYPE_DEL_TYPE    VARCHAR2(30) := 'DiffTypeDel';

/*--- doc type parameters ---*/
HDR_DESC_MSG          CONSTANT  VARCHAR2(30) := 'CodeHdrDesc';
HDR_REF_MSG           CONSTANT  VARCHAR2(30) := 'CodeHdrRef';
DTL_DESC_MSG          CONSTANT  VARCHAR2(30) := 'CodeDtlDesc';
DTL_REF_MSG           CONSTANT  VARCHAR2(30) := 'CodeDtlRef';
DIFF_TYPE_DESC_MSG    CONSTANT  VARCHAR2(30) := 'DiffTypeDesc';
DIFF_TYPE_REF_MSG     CONSTANT  VARCHAR2(30) := 'DiffTypeRef';

--------------------------------------------------------------------------------
PROCEDURE ADDTOQ(O_status         OUT VARCHAR2,
                 O_text           OUT VARCHAR2,
                 I_message_type   IN CODES_MFQUEUE.MESSAGE_TYPE%TYPE,
                 I_code_type      IN CODES_MFQUEUE.CODE_TYPE%TYPE,
                 I_message        IN OUT rib_sxw.SXWHandle);
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code   OUT VARCHAR2,
                 O_error_msg     OUT VARCHAR2,
                 O_message_type  OUT CODES_MFQUEUE.MESSAGE_TYPE%TYPE,
                 O_message       OUT nocopy CLOB,
                 O_code_type     OUT CODES_MFQUEUE.CODE_TYPE%TYPE);
--------------------------------------------------------------------------------
END RMSMFM_SEEDDATA;
/
