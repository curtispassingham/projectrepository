
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRSCLS_VALIDATE AS
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function should check all required fields in the subclass
   --                portion of the create or modify message to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrSclsDesc_REC")
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function should check all required fields in the subclass
   --                portion of the delete message to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrSclsRef_REC")
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_DELETE
   -- Purpose      : This function will verify that the subclass can be deleted.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message         IN       "RIB_XMrchHrSclsRef_REC")
   RETURN BOOLEAN;
   
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert the RIB subclass object into a subclass record
   --                defined in the MERCH_SQL package if the message is a create or a modify.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                         O_subclass_rec    OUT   NOCOPY   SUBCLASS%ROWTYPE,
                         I_message         IN             "RIB_XMrchHrSclsDesc_REC")
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert the RIB subclass object into a subclass record
   --                defined in the MERCH_SQL package if the message is a delete message.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                         O_subclass_rec    OUT   NOCOPY   SUBCLASS%ROWTYPE,
                         I_message         IN             "RIB_XMrchHrSclsRef_REC")
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_SUBCLASS_EXISTS
   -- Purpose      : This function will verify the subclass and any node being
   --                modified already exists.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_SUBCLASS_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrSclsDesc_REC")

   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_SUBCLASS_EXISTS
   -- Purpose      : This function will verify the subclass and any node to be
   --                deleted already exists.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_SUBCLASS_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrSclsRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message  IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       O_subclass_rec   OUT   NOCOPY   SUBCLASS%ROWTYPE,
                       I_message        IN             "RIB_XMrchHrSclsDesc_REC",
                       I_message_type   IN             VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'RMSSUB_XMRCHHRSCLS_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XMRCHHRSCLS.LP_mod_type then
      if not CHECK_SUBCLASS_EXISTS(O_error_message,
                                   I_message) then
         return FALSE;
      end if;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_subclass_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       O_subclass_rec    OUT   NOCOPY   SUBCLASS%ROWTYPE,
                       I_message         IN             "RIB_XMrchHrSclsRef_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not CHECK_SUBCLASS_EXISTS(O_error_message,
                                I_message) then
         return FALSE;
      end if;

   if not CHECK_DELETE(O_error_message,
                       I_message) then
         return FALSE;
      end if;
 
   if not POPULATE_RECORD(O_error_message,
                          O_subclass_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrSclsDesc_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'dept', NULL, NULL);
      return FALSE;
   end if;

   if I_message.class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'class', NULL, NULL);
      return FALSE;
   end if;

   if I_message.subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'subclass', NULL, NULL);
      return FALSE;
   end if;

   if I_message.subclass_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'sub_name', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrSclsRef_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'dept', NULL, NULL);
      return FALSE;
   end if;

   if I_message.class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'class', NULL, NULL);
      return FALSE;
   end if;

   if I_message.subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'subclass', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message         IN       "RIB_XMrchHrSclsRef_REC")
   RETURN BOOLEAN IS

   L_program             VARCHAR2(60)  := 'RMSSUB_XMRCHHRCLS_VALIDATE.CHECK_DELETE';
   L_exist               VARCHAR2(1)   := 'N';
   L_subclass_exist      BOOLEAN       := FALSE;
   L_filter_merch_level  FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE := 'S';

BEGIN

   -- checks if the Merchandise Hierarchy subclass exists in one of the Data Element.
   if not FILTER_GROUP_HIER_SQL.VALIDATE_GROUP_MERCH(O_error_message,
                                                     L_subclass_exist,
                                                     L_filter_merch_level,
                                                     I_message.dept,
                                                     I_message.class,
                                                     I_message.subclass) then
      return FALSE;
   end if;

   if L_subclass_exist then
      O_error_message := SQL_LIB.CREATE_MSG('CNT_DEL_REC', 
                                            NULL, 
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DELETE;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                         O_subclass_rec    OUT   NOCOPY   SUBCLASS%ROWTYPE,
                         I_message         IN             "RIB_XMrchHrSclsDesc_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_VALIDATE.POPULATE_RECORD';

BEGIN

   O_subclass_rec.dept     := I_message.dept;
   O_subclass_rec.class    := I_message.class;
   O_subclass_rec.subclass := I_message.subclass;
   O_subclass_rec.sub_name := I_message.subclass_name;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                         O_subclass_rec    OUT   NOCOPY   SUBCLASS%ROWTYPE,
                         I_message         IN             "RIB_XMrchHrSclsRef_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_VALIDATE.POPULATE_RECORD';

BEGIN

   O_subclass_rec.dept     := I_message.dept;
   O_subclass_rec.class    := I_message.class;
   O_subclass_rec.subclass := I_message.subclass;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------
FUNCTION CHECK_SUBCLASS_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrSclsDesc_REC")

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_VALIDATE.CHECK_SUBCLASS_EXISTS';
   L_exists       BOOLEAN      := FALSE;
   L_table_name   VARCHAR2(10) := 'SUBCLASS';
   L_key_value    DAILY_PURGE.KEY_VALUE%TYPE;

BEGIN
   L_key_value := substr(to_char(I_message.dept, '0999'),2,4)|| ';' ||
                  substr(to_char(I_message.class, '0999'),2,4)|| ';' ||
                  substr(to_char(I_message.subclass, '0999'),2,4);                     

   -- Check for daily_purge existence
   if NOT DAILY_PURGE_SQL.CHECK_EXISTS(O_error_message,
                                       L_exists,
                                       L_key_value,
                                       L_table_name) then
      return FALSE;
   end if;
   
   if L_exists then      
      O_error_message := SQL_LIB.CREATE_MSG('EXIST_IN_DAILY_PURGE',
                                            L_key_value,
                                            NULL,
                                            NULL);
      return FALSE;                                   
   end if;   

   -- Check for Subclass
   L_exists := FALSE;
   if not SUBCLASS_VALIDATE_SQL.EXIST(O_error_message,
                                      I_message.dept,
                                      I_message.class,
                                      I_message.subclass,
                                      L_exists) then
      return FALSE;
   end if;

   if not L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUBCLASS');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_SUBCLASS_EXISTS;
-------------------------------------------------------------------------------------------------
FUNCTION CHECK_SUBCLASS_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrSclsRef_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_VALIDATE.CHECK_SUBCLASS_EXISTS';
   L_exists    BOOLEAN      := FALSE;
   L_table_name   VARCHAR2(10) := 'SUBCLASS';
   L_key_value    DAILY_PURGE.KEY_VALUE%TYPE;

BEGIN
   L_key_value := substr(to_char(I_message.dept, '0999'),2,4)|| ';' ||
                  substr(to_char(I_message.class, '0999'),2,4)|| ';' ||
                  substr(to_char(I_message.subclass, '0999'),2,4);                     

   -- Check for daily_purge existence
   if NOT DAILY_PURGE_SQL.CHECK_EXISTS(O_error_message,
                                       L_exists,
                                       L_key_value,
                                       L_table_name) then
      return FALSE;
   end if;
   
   if L_exists then      
      O_error_message := SQL_LIB.CREATE_MSG('EXIST_IN_DAILY_PURGE',
                                            L_key_value,
                                            NULL,
                                            NULL);
      return FALSE;                                   
   end if;   

   -- Check for Subclass
   L_exists := FALSE;
   if not SUBCLASS_VALIDATE_SQL.EXIST(O_error_message,
                                      I_message.dept,
                                      I_message.class,
                                      I_message.subclass,
                                      L_exists) then
      return FALSE;
   end if;

   if not L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUBCLASS');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_SUBCLASS_EXISTS;
-------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRSCLS_VALIDATE;
/
