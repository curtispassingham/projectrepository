CREATE OR REPLACE PACKAGE BODY LOC_TRAITS_SQL AS

------------------------------------------------------------------------
--  INTERNAL GLOBAL VARIABLES
------------------------------------------------------------------------
LP_package    VARCHAR2(40)               := 'LOC_TRAITS_SQL.';
LP_loc_trait  loc_traits.loc_trait%TYPE  := NULL;
LP_area       area.area%TYPE             := NULL;
LP_region     region.region%TYPE         := NULL;
LP_district   district.district%TYPE     := NULL;
LP_store      store.store%TYPE           := NULL;

------------------------------------------------------------------------
--  INTERNAL GLOBAL CURSORS
------------------------------------------------------------------------

cursor CP_store_trait is
   select 'Y'
     from loc_traits_matrix
    where loc_trait = LP_loc_trait
      and store = LP_store;

cursor CP_loc_trait is
   select 'Y'
      from loc_traits_matrix
      where loc_trait = LP_loc_trait;

FUNCTION PARAM_NOT_NULL RETURN NUMBER;

PROCEDURE CLEAN_LP_VARS;
-------------------------------------------------------------------------------------------------------
-- Function Name:  DEFAULT_AREA
-- Purpose      :  Inserts records into LOC_TRAITS_MATRIX
-------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_AREA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_loc_traits      IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                      I_hier_value      IN      NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  DEFAULT_REGION
-- Purpose      :  Inserts records into LOC_TRAITS_MATRIX
-------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_REGION(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_loc_traits      IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                        I_hier_value      IN      NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  DELETE_AREA
-- Purpose      :  Deletes records from the LOC_TRAITS_MATRIX
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_AREA(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_loc_traits     IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                     I_hier_value     IN      NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  DELETE_REGION
-- Purpose      :  Deletes records from the LOC_TRAITS_MATRIX
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_REGION(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_loc_traits     IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                       I_hier_value     IN      NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  DELETE_DISTRICT
-- Purpose      :  Deletes records from the LOC_TRAITS_MATRIX
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DISTRICT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loc_traits     IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                         I_hier_value     IN      NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  DEFAULT_DISTRICT
-- Purpose      :  Inserts records into LOC_TRAITS_MATRIX
-------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_DISTRICT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_loc_traits      IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                          I_hier_value      IN      NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: LOCK_LOC_TRAITS_MATRIX
-- Purpose      : This function will lock the LOC_TRAITS_MATRIX table for deletion.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_LOC_TRAITS_MATRIX(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_loc_traits      IN       LOC_TRAITS_MATRIX.LOC_TRAIT%TYPE,
                                I_store_id        IN       STORE_ADD.STORE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------

FUNCTION GET_DESC (O_error_message    IN OUT  VARCHAR2,
                   O_loc_trait_desc   IN OUT  VARCHAR2,
                   I_loc_trait        IN      NUMBER)
   RETURN BOOLEAN IS

cursor C_loc_trait_desc is
   select description
      from v_loc_traits_tl
      where loc_trait = I_loc_trait;

   L_cursor       VARCHAR2(20)  := 'C_LOC_TRAIT_DESC';
   L_program VARCHAR2(50)  := 'GET_DESC';

BEGIN
   SQL_LIB.SET_MARK('OPEN', L_cursor, 'V_LOC_TRAITS_TL',
                    'Loc_trait '||to_char(I_loc_trait));
   open C_loc_trait_desc;
   SQL_LIB.SET_MARK('FETCH', L_cursor, 'V_LOC_TRAITS_TL',
                    'Loc_trait '||to_char(I_loc_trait));
   fetch C_loc_trait_desc into O_loc_trait_desc;
   if C_loc_trait_desc%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', L_cursor, 'V_LOC_TRAITS_TL',
                       'Loc_trait '||to_char(I_loc_trait));
      close C_loc_trait_desc;
      raise NO_DATA_FOUND;
   end if;
   SQL_LIB.SET_MARK('CLOSE', L_cursor, 'V_LOC_TRAITS_TL',
                    'Loc_trait '||to_char(I_loc_trait));
   close C_loc_trait_desc;

   return TRUE;

EXCEPTION
   when NO_DATA_FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TRAIT',
                            NULL, NULL, NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                            LP_package||L_program, to_char(SQLCODE));
      return FALSE;

END GET_DESC;
------------------------------------------------------------------------
FUNCTION RELATION_EXISTS (O_error_message IN OUT  VARCHAR2,
                          O_exists        IN OUT  BOOLEAN,
                          I_loc_trait     IN      NUMBER,
                          I_store         IN      NUMBER)
   RETURN BOOLEAN IS

   L_yes          VARCHAR2(1)   := NULL;
   L_program VARCHAR2(50)  := 'RELATION_EXISTS';
   WRONG_PARAM    EXCEPTION;
BEGIN
   LP_loc_trait := I_loc_trait;
   LP_store := I_store;

   if PARAM_NOT_NULL IS NULL OR PARAM_NOT_NULL > 1 then
      raise WRONG_PARAM;
   end if;

   if LP_store IS NOT NULL then
      SQL_LIB.SET_MARK('OPEN', 'CP_store_trait', 'LOC_TRAITS_MATRIX',
                       'Store '||to_char(LP_store));
      open CP_store_trait;
      SQL_LIB.SET_MARK('FETCH', 'CP_store_trait', 'LOC_TRAITS_MATRIX',
                       'Store '||to_char(LP_store));
      fetch CP_store_trait into L_yes;
      if CP_store_trait%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'CP_store_trait', 'LOC_TRAITS_MATRIX',
                       'Store '||to_char(LP_store));
      close CP_store_trait;
   else
      SQL_LIB.SET_MARK('OPEN', 'CP_loc_traits', 'LOC_TRAITS_MATRIX',
                       'Loc_trait '||to_char(LP_loc_trait));
      open CP_loc_trait;
      SQL_LIB.SET_MARK('FETCH', 'CP_loc_traits', 'LOC_TRAITS_MATRIX',
                       'Loc_trait '||to_char(LP_loc_trait));
      fetch CP_loc_trait into L_yes;
      if CP_loc_trait%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'CP_loc_traits', 'LOC_TRAITS_MATRIX',
                       'Loc_trait '||to_char(LP_loc_trait));
      close CP_loc_trait;
   end if;
   CLEAN_LP_VARS;
   return TRUE;

EXCEPTION
   when WRONG_PARAM then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC', NULL,
                                            NULL, NULL);
      CLEAN_LP_VARS;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
         LP_package||L_program, to_char(SQLCODE));
      CLEAN_LP_VARS;
      return FALSE;

END RELATION_EXISTS;

------------------------------------------------------------------------
FUNCTION RELATION_EXISTS (O_error_message IN OUT  VARCHAR2,
                          O_exists        IN OUT  BOOLEAN,
                          I_loc_trait     IN      NUMBER)
RETURN BOOLEAN IS
BEGIN

   if(RELATION_EXISTS(O_error_message,
                      O_exists,
                      I_loc_trait,
                      NULL) = FALSE) then
      return FALSE;
   end if;

   return TRUE;
END RELATION_EXISTS;
------------------------------------------------------------------------
FUNCTION DELETE_LOC_TRAITS_MATRIX(O_error_message IN OUT  VARCHAR2,
                                  I_loc_trait     IN      NUMBER,
                                  I_area          IN      NUMBER,
                                  I_region        IN      NUMBER,
                                  I_district      IN      NUMBER)
   RETURN BOOLEAN IS

   L_yes           VARCHAR2(1)   := NULL;
   L_program  VARCHAR2(50)  := 'DELETE_LOC_TRAITS_MATRIX';
   WRONG_PARAM     EXCEPTION;
   L_exception_id  NUMBER(2);
   L_dummy         VARCHAR2(1);
   L_rowid         ROWID;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_locked, -54);
   L_store         loc_traits_matrix.store%TYPE;
   L_loc_trait     loc_traits_matrix.loc_trait%TYPE;
   L_Exists       VARCHAR2(1);
   SA_DETAILS_EXISTS EXCEPTION;

  cursor C_GET_LOC_TRAITS_MATRIX is
      select loc_trait, store, rowid
         from loc_traits_matrix
      where loc_trait = I_loc_trait
         and store IN (select store
                        from store
                        where district = I_district);
   cursor C_LOCK_LOC_TRAITS_MATRIX is
        select 'x'
         from loc_traits_matrix
        where rowid = L_rowid
          for update nowait;

   cursor C_GET_LOC_TRAITS_MATRIX_2 is
      select store, loc_trait, rowid
         from loc_traits_matrix
      where loc_trait = I_loc_trait
          and store IN (select s.store
                        from store s, district d
                        where s.district = d.district
                           and d.region   = I_region);

   cursor C_GET_LOC_TRAITS_MATRIX_3 is
      select loc_trait, store, rowid
         from loc_traits_matrix
         where loc_trait = I_loc_trait
            and store IN (select s.store
                             from store s, district d, region r
                          where s.district = d.district
                             and d.region   = r.region
                             and r.area     = I_area);

   cursor C_GET_LOC_TRAITS_MATRIX_ROWID is
      select store, rowid
         from loc_traits_matrix
         where loc_trait = I_loc_trait;
         
   cursor C_SA_LOC_TRAIT_EXISTS is
      select 1
         from SA_TOTAL_LOC_TRAIT
         where loc_trait = I_loc_trait
         and rownum=1
         union
      select 1
         from SA_RULE_LOC_TRAIT
         where loc_trait = I_loc_trait
         and rownum=1
       union    
      select 1
         from SA_CORP_EMP
         where loc_trait = I_loc_trait
         and rownum=1;

BEGIN
   LP_loc_trait := I_loc_trait;
   LP_area      := I_area;
   LP_region    := I_region;
   LP_district  := I_district;
   if PARAM_NOT_NULL IS NULL OR PARAM_NOT_NULL > 1 then
      raise WRONG_PARAM;
   end if;

   open C_SA_LOC_TRAIT_EXISTS;
   fetch C_SA_LOC_TRAIT_EXISTS into L_Exists;
   if C_SA_LOC_TRAIT_EXISTS%FOUND then
     Raise SA_DETAILS_EXISTS;
   end if;
   close C_SA_LOC_TRAIT_EXISTS;

   if I_district is NOT NULL then
      L_exception_id := 2;
      for C_rec in C_GET_LOC_TRAITS_MATRIX
      LOOP
         L_loc_trait  := C_rec.loc_trait;
         L_store      := C_rec.store;
         L_rowid      := C_rec.rowid;

         open C_LOCK_LOC_TRAITS_MATRIX;
         fetch C_LOCK_LOC_TRAITS_MATRIX into L_dummy;
         close C_LOCK_LOC_TRAITS_MATRIX;
      END LOOP;

      SQL_LIB.SET_MARK('DELETE', NULL, 'LOC_TRAITS_MATRIX, STORE',
         'District '||to_char(LP_district)||' Loc_Trait '||to_char(LP_loc_trait));
      delete from loc_traits_matrix
       where loc_trait = I_loc_trait
         and store IN (select store
                        from store
                        where district = I_district);

   elsif I_region is NOT NULL then
      L_exception_id := 2;
      for C_rec in C_GET_LOC_TRAITS_MATRIX_2
      LOOP
         L_loc_trait  := C_rec.loc_trait;
         L_store      := C_rec.store;
         L_rowid      := C_rec.rowid;

         open C_LOCK_LOC_TRAITS_MATRIX;
         fetch C_LOCK_LOC_TRAITS_MATRIX into L_dummy;
         close C_LOCK_LOC_TRAITS_MATRIX;
      END LOOP;

      SQL_LIB.SET_MARK('DELETE', NULL, 'LOC_TRAITS_MATRIX, DISTRICT, STORE',
         'Region '||to_char(LP_region)||' Loc_Trait '||to_char(LP_loc_trait));
      delete from loc_traits_matrix
         where loc_trait = I_loc_trait
          and store IN (select s.store
                        from store s, district d
                        where s.district = d.district
                           and d.region   = I_region);

   elsif I_area is NOT NULL then
      L_exception_id := 2;
      for C_rec in C_GET_LOC_TRAITS_MATRIX_3
      LOOP
         L_loc_trait  := C_rec.loc_trait;
         L_store      := C_rec.store;
         L_rowid      := C_rec.rowid;

         open C_LOCK_LOC_TRAITS_MATRIX;
         fetch C_LOCK_LOC_TRAITS_MATRIX into L_dummy;
         close C_LOCK_LOC_TRAITS_MATRIX;
      END LOOP;

      SQL_LIB.SET_MARK('DELETE', NULL, 'LOC_TRAITS_MATRIX, REGION, DISTRICT, STORE',
         'Area '||to_char(LP_area)||' Loc_Trait '||to_char(LP_loc_trait));
      delete from loc_traits_matrix
         where loc_trait = I_loc_trait
            and store IN (select s.store
                             from store s, district d, region r
                          where s.district = d.district
                             and d.region   = r.region
                             and r.area     = I_area);

   else                -- all locations are NULL

      L_exception_id := 2;
      for C_rec in C_GET_LOC_TRAITS_MATRIX_ROWID
      LOOP
         L_rowid        := C_rec.rowid;
         L_store        := C_rec.store;

         open C_LOCK_LOC_TRAITS_MATRIX;
         fetch C_LOCK_LOC_TRAITS_MATRIX into L_dummy;
         close C_LOCK_LOC_TRAITS_MATRIX;
      END LOOP;

      SQL_LIB.SET_MARK('DELETE', NULL, 'LOC_TRAITS_MATRIX',
                       'Loc_Trait '||to_char(LP_loc_trait));
      delete from loc_traits_matrix
         where loc_trait = I_loc_trait;
   end if;
   CLEAN_LP_VARS;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if L_exception_id = 2 then
         o_error_message := sql_lib.create_msg('DEL_STORE_LOCKED',
                                               to_char(L_store),
                                               to_char(LP_loc_trait));
      end if;
      return FALSE;

   when WRONG_PARAM then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC', NULL,
                                            NULL, NULL);
      CLEAN_LP_VARS;
      return FALSE;
   when SA_DETAILS_EXISTS then
      O_error_message := SQL_LIB.CREATE_MSG('NOT_LOC_TRAIT_SA', NULL,
                                            NULL, NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
         LP_package||L_program, to_char(SQLCODE));
      CLEAN_LP_VARS;
      return FALSE;

END DELETE_LOC_TRAITS_MATRIX;
------------------------------------------------------------------------
FUNCTION PARAM_NOT_NULL RETURN NUMBER IS
BEGIN
   if LP_area is NULL AND LP_region is NULL AND
      LP_district is NULL AND LP_store is NULL then
      return 0;
   elsif LP_area is NOT NULL AND LP_region is NOT NULL
      AND LP_district is NOT NULL AND LP_store is NOT NULL then
      return 4;
   elsif (LP_area is NOT NULL AND LP_region is NOT NULL
             AND LP_district is NOT NULL AND LP_store is NULL)
          OR (LP_area is NOT NULL AND LP_region is NOT NULL
              AND LP_district is NULL AND LP_store is NOT NULL)
          OR (LP_area is NOT NULL AND LP_region is NULL
              AND LP_district is NOT NULL AND LP_store is NOT NULL)
          OR (LP_area is NULL AND LP_region is NOT NULL
              AND LP_district is NOT NULL AND LP_store is NOT NULL) then
      return 3;
   elsif (LP_area is NOT NULL AND LP_region is NOT NULL)
         OR (LP_area is NOT NULL AND LP_district is NOT NULL)
         OR (LP_area is NOT NULL AND LP_store is NOT NULL)
         OR (LP_region is NOT NULL AND LP_district is NOT NULL)
         OR (LP_region is NOT NULL AND LP_store is NOT NULL)
         OR (LP_district is NOT NULL AND LP_store is NOT NULL) then
      return 2;
   elsif (LP_area is NOT NULL AND LP_region is NULL AND LP_district is NULL
                              AND LP_store is NULL)
         OR (LP_area is NULL AND LP_region is NOT NULL AND LP_district is NULL
                             AND LP_store is NULL)
         OR (LP_area is NULL AND LP_region is NULL AND LP_district is NOT NULL
                             AND LP_store is NULL)
         OR (LP_area is NULL AND LP_region is NULL AND LP_district is NULL
                             AND LP_store is NOT NULL) then
      return 1;
   else
      return NULL;
   end if;
END PARAM_NOT_NULL;
------------------------------------------------------------------------
PROCEDURE CLEAN_LP_VARS IS
BEGIN
   LP_loc_trait := NULL;
   LP_area      := NULL;
   LP_region    := NULL;
   LP_district  := NULL;
   LP_store     := NULL;
END CLEAN_LP_VARS;
--------------------------------------------------------------------------
FUNCTION VALIDATE_AREA_TRAIT(O_error_message IN OUT  VARCHAR2,
                             O_valid         IN OUT  BOOLEAN,
                             I_area          IN      AREA.AREA%TYPE,
                             I_loc_trait     IN      LOC_TRAITS.LOC_TRAIT%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(80) := 'LOC_TRAITS_SQL.VALIDATE_AREA_TRAIT';

   cursor C_org_id is
      select filter_org_id
        from loc_traits
       where loc_trait = I_loc_trait;

   L_org_id              loc_traits.filter_org_id%type;

   cursor C_area_loc_trait is
      select 'Y'
        from area a,
             loc_traits ltr
       where a.chain = ltr.filter_org_id
         and ltr.loc_trait = I_loc_trait
         and a.area = I_area;

   L_exist           VARCHAR2(1) := 'N';

BEGIN
   ---
   if I_area is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_area',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_loc_trait is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_trait',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_org_id;
   fetch C_org_id into L_org_id;
   ---
   if C_org_id%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TRAIT',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_org_id;
      return FALSE;
   end if;
   ---
   close C_org_id;
   ---
   if L_org_id is null then
      O_valid := TRUE;
   else
      ---
      open C_area_loc_trait;
      fetch C_area_loc_trait into L_exist;
      close C_area_loc_trait;
      ---
      if L_exist = 'Y' then
         O_valid := TRUE;
      else
         O_valid := FALSE;
      end if;
      ---
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_AREA_TRAIT;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_DISTRICT_TRAIT(O_error_message IN OUT  VARCHAR2,
                                 O_valid         IN OUT  BOOLEAN,
                                 I_district      IN      DISTRICT.DISTRICT%TYPE,
                                 I_loc_trait     IN      LOC_TRAITS.LOC_TRAIT%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(80) := 'LOC_TRAITS_SQL.VALIDATE_DISTRICT_TRAIT';

   cursor C_org_id is
      select filter_org_id
        from loc_traits
       where loc_trait = I_loc_trait;

   L_org_id              loc_traits.filter_org_id%type;

   cursor C_district_loc_trait is
      select 'Y'
        from loc_traits ltr,
             district d,
             region r,
             area a,
             system_options s
       where d.district = I_district
         and d.region = r.region
         and r.area = a.area
         and ((a.chain = ltr.filter_org_id and s.loc_trait_org_level_code = 'C')
       or (r.area = ltr.filter_org_id and s.loc_trait_org_level_code = 'A')
              or (d.region = ltr.filter_org_id and s.loc_trait_org_level_code = 'R'))
         and ltr.loc_trait = I_loc_trait;

   L_exist               VARCHAR2(1) := 'N';

BEGIN
   ---
   if I_district is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_district',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_loc_trait is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_trait',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_org_id;
   fetch C_org_id into L_org_id;
   ---
   if C_org_id%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TRAIT',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_org_id;
      return FALSE;
   end if;
   ---
   close C_org_id;
   ---
   if L_org_id is null then
      O_valid := TRUE;
   else
      ---
      open C_district_loc_trait;
      fetch C_district_loc_trait into L_exist;
      close C_district_loc_trait;
      ---
      if L_exist = 'Y' then
         O_valid := TRUE;
      else
         O_valid := FALSE;
      end if;
      ---
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_DISTRICT_TRAIT;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCK_LOCTRAIT
   -- Purpose      : This function will lock the LOC_TRAITS table for update or delete.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_loctrait        IN       LOC_TRAITS.LOC_TRAIT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_loctrait        IN       LOC_TRAITS.LOC_TRAIT%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'LOC_TRAITS_SQL.LOCK_LOCTRAIT';
   L_table        VARCHAR2(50) := 'LOC_TRAITS';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_LOCTRAIT IS
      select 'x'
        from LOC_TRAITS
       where loc_trait = I_loctrait
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_LOCTRAIT', 'LOC_TRAITS', 'loc_trait: '||I_loctrait);
   open C_LOCK_LOCTRAIT;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_LOCTRAIT', 'LOC_TRAITS', 'loc_trait: '||I_loctrait);
   close C_LOCK_LOCTRAIT;

   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            L_table,
                                            'LOC_TRAIT:'||I_loctrait,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_LOCTRAIT;
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCK_LOCTRAIT_TL
   -- Purpose      : This function will lock the LOC_TRAITS_TL table for delete.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_LOCTRAIT_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_loctrait        IN       LOC_TRAITS.LOC_TRAIT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_LOCTRAIT_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_loctrait        IN       LOC_TRAITS.LOC_TRAIT%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'LOC_TRAITS_SQL.LOCK_LOCTRAIT_TL';
   L_table        VARCHAR2(50) := 'LOC_TRAITS_TL';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_LOCTRAIT_TL IS
      select 'x'
        from LOC_TRAITS_TL
       where loc_trait = I_loctrait
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_LOCTRAIT_TL', 'LOC_TRAITS_TL', 'loc_trait: '||I_loctrait);
   open C_LOCK_LOCTRAIT_TL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_LOCTRAIT_TL', 'LOC_TRAITS_TL', 'loc_trait: '||I_loctrait);
   close C_LOCK_LOCTRAIT_TL;

   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            L_table,
                                            'LOC_TRAIT:'||I_loctrait,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_LOCTRAIT_TL;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'LOC_TRAITS_SQL.INSERT_LOCTRAIT';

BEGIN

   SQL_LIB.SET_MARK('INSERT', NULL, 'LOC_TRAITS', 'loc_trait: '||I_loctrait_rec.loc_trait);
   insert into LOC_TRAITS (loc_trait,
                           description)
                   values (I_loctrait_rec.loc_trait,
                           I_loctrait_rec.description);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_LOCTRAIT;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'LOC_TRAITS_SQL.UPDATE_LOCTRAIT';

BEGIN
   if not LOCK_LOCTRAIT(O_error_message,
                        I_loctrait_rec.loc_trait) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'LOC_TRAITS', 'loc_trait: '||I_loctrait_rec.loc_trait);
   update LOC_TRAITS
      set description = NVL(I_loctrait_rec.description, description)
    where loc_trait = I_loctrait_rec.loc_trait;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_LOCTRAIT;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'LOC_TRAITS_SQL.DELETE_LOCTRAIT';

BEGIN
   if not LOCK_LOCTRAIT_TL(O_error_message,
                           I_loctrait_rec.loc_trait) then
      return FALSE;
   end if;
   ---
   if not LOCK_LOCTRAIT(O_error_message,
                        I_loctrait_rec.loc_trait) then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, 'LOC_TRAITS_TL', 'loc_trait: '||I_loctrait_rec.loc_trait);
   delete from LOC_TRAITS_TL
    where loc_trait = I_loctrait_rec.loc_trait;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, 'LOC_TRAITS', 'loc_trait: '||I_loctrait_rec.loc_trait);
   delete from LOC_TRAITS
    where loc_trait = I_loctrait_rec.loc_trait;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_LOCTRAIT;
-------------------------------------------------------------------------------------------------------
FUNCTION LOC_TRAIT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          OUT      BOOLEAN,
                          I_loctrait        IN       LOC_TRAITS.LOC_TRAIT%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'LOC_TRAITS_SQL.LOC_TRAIT_EXISTS';
   L_exist        VARCHAR2(1);

   cursor C_LOCTRAIT_EXISTS is
      select  'X'
        from  loc_traits
       where  loc_trait = I_loctrait;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_LOCTRAIT_EXISTS', 'loc_traits', 'loc_trait:'||I_loctrait);
   open C_LOCTRAIT_EXISTS;

   SQL_LIB.SET_MARK('FETCH', 'C_LOCTRAIT_EXISTS', 'loc_traits', 'loc_trait:'||I_loctrait);
   fetch C_LOCTRAIT_EXISTS into L_exist;

   SQL_LIB.SET_MARK('CLOSE', 'C_LOCTRAIT_EXISTS', 'loc_traits', 'loc_trait:'||I_loctrait);
   close C_LOCTRAIT_EXISTS;

   if L_exist is not NULL then
     O_exists:= TRUE;
   else
     O_exists:= FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
   RETURN FALSE;

END LOC_TRAIT_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_PARENT_AREA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_hier_value      IN      NUMBER,
                            I_parent_id       IN      NUMBER)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'LOC_TRAITS_SQL.DELETE_PARENT_AREA';

   cursor C_LOCK_LOC_TRAITS_MATRIX is
      select 'x'
        from loc_traits_matrix lm
       where exists (select 1
                       from region r,
                            district d,
                            store s
                      where r.region = I_hier_value
                        and r.area = I_parent_id
                        and r.region = d.region
                        and d.district = s.district
                        and s.store = lm.store)
      for update nowait;

BEGIN


   SQL_LIB.SET_MARK('OPEN','C_LOCK_LOC_TRAITS_MATRIX',NULL,'Region: ' || I_hier_value);
   open C_LOCK_LOC_TRAITS_MATRIX;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_LOC_TRAITS_MATRIX',NULL,'Region: ' || I_hier_value);
   close C_LOCK_LOC_TRAITS_MATRIX;

   delete from loc_traits_matrix lm
    where exists (select 1
                    from region r,
                         district d,
                         store s
                   where r.region = I_hier_value
                     and r.area = I_parent_id
                     and r.region = d.region
                     and d.district = s.district
                     and s.store = lm.store);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
   return FALSE;
END DELETE_PARENT_AREA;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_PARENT_REGION(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_hier_value      IN      NUMBER,
                              I_parent_id       IN      NUMBER)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'LOC_TRAITS_SQL.DELETE_PARENT_REGION';


   cursor C_LOCK_LOC_TRAITS_MATRIX is
      select 'x'
        from loc_traits_matrix lm
       where exists (select 1
                       from district d,
                            store s
                      where d.district = I_hier_value
                        and d.region = I_parent_id
                        and d.district = s.district
                        and s.store = lm.store)
     for update nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_LOCK_LOC_TRAITS_MATRIX',NULL,'District: ' || I_hier_value);
   open C_LOCK_LOC_TRAITS_MATRIX;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_LOC_TRAITS_MATRIX',NULL,'District: ' || I_hier_value);
   close C_LOCK_LOC_TRAITS_MATRIX;

   delete from loc_traits_matrix lm
    where exists (select 1
                    from district d,
                         store s
                   where d.district = I_hier_value
                     and d.region = I_parent_id
                     and d.district = s.district
                     and s.store = lm.store);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
   return FALSE;
END DELETE_PARENT_REGION;
-------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_AREA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_loc_traits      IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                      I_hier_value      IN      NUMBER)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'LOC_TRAITS_SQL.DEFAULT_AREA';

BEGIN

   FOR i in I_loc_traits.first..I_loc_traits.last LOOP
      if I_loc_traits(i).loc_trait_id is not NULL then
         ---
         insert into LOC_TRAITS_MATRIX(loc_trait,
                                       store)
                                select I_loc_traits(i).loc_trait_id,
                                       s.store
                                  from region r,
                                       district d,
                                       store s
                                 where r.area = I_hier_value
                                   and d.region = r.region
                                   and s.district = d.district
                                   and not exists (select 1
                                                     from loc_traits_matrix lm
                                                    where lm.store = s.store
                                                      and lm.loc_trait = I_loc_traits(i).loc_trait_id);
      end if;
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
   return FALSE;

END DEFAULT_AREA;
---------------------------------------------------------------------
FUNCTION DEFAULT_REGION(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_loc_traits      IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                        I_hier_value      IN      NUMBER)
   RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'LOC_TRAITS_SQL.DEFAULT_REGION';

BEGIN

   FOR i in I_loc_traits.first..I_loc_traits.last LOOP
      if I_loc_traits(i).loc_trait_id is not NULL then
         ---
         insert into LOC_TRAITS_MATRIX(loc_trait,
                                       store)
                                select I_loc_traits(i).loc_trait_id,
                                       s.store
                                  from district d,
                                       store s
                                 where d.region = I_hier_value
                                   and s.district = d.district
                                   and not exists (select 1
                                                     from loc_traits_matrix lm
                                                    where lm.store = s.store
                                                      and lm.loc_trait = I_loc_traits(i).loc_trait_id);
      end if;
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
   return FALSE;

END DEFAULT_REGION;
---------------------------------------------------------------------
FUNCTION DEFAULT_DISTRICT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_loc_traits      IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                          I_hier_value      IN      NUMBER)
   RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'LOC_TRAITS_SQL.DEFAULT_DISTRICT';

BEGIN

   FOR i in I_loc_traits.first..I_loc_traits.last LOOP
      if I_loc_traits(i).loc_trait_id is not NULL then
         insert into LOC_TRAITS_MATRIX(loc_trait,
                                       store)
                                select I_loc_traits(i).loc_trait_id,
                                       s.store
                                  from store s
                                 where s.district = I_hier_value
                                   and not exists(select 1
                                                    from loc_traits_matrix lm
                                                   where lm.store = s.store
                                                     and lm.loc_trait = I_loc_traits(i).loc_trait_id);
      end if;
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
   return FALSE;

END DEFAULT_DISTRICT;
---------------------------------------------------------------------
FUNCTION DELETE_AREA(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_loc_traits     IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                     I_hier_value     IN      NUMBER)
   RETURN BOOLEAN IS
   L_program      VARCHAR2(50) := 'LOC_TRAITS_SQL.DELETE_AREA';
   L_loctrt_id    LOC_TRAITS.LOC_TRAIT%TYPE;

   cursor C_LOCK_LOC_TRAITS_MATRIX is
      select 'x'
        from LOC_TRAITS_MATRIX lm
       where lm.loc_trait = L_loctrt_id
         and exists (select 1
                       from region r,
                            district d,
                            store s
                      where r.area = I_hier_value
                        and d.region = r.region
                        and s.district = d.district
                        and lm.store = s.store)
      for update nowait;

BEGIN

   FOR i in I_loc_traits.first..I_loc_traits.last LOOP
      if I_loc_traits(i).loc_trait_id is not NULL then

         L_loctrt_id := I_loc_traits(i).loc_trait_id;
         ---
         SQL_LIB.SET_MARK('OPEN','C_LOCK_LOC_TRAITS_MATRIX',NULL,'Hier value: ' || I_hier_value);
         open C_LOCK_LOC_TRAITS_MATRIX;

         SQL_LIB.SET_MARK('CLOSE','C_LOCK_LOC_TRAITS_MATRIX',NULL,'Hier value: ' || I_hier_value);
         close C_LOCK_LOC_TRAITS_MATRIX;

         delete from LOC_TRAITS_MATRIX lm
          where lm.loc_trait = I_loc_traits(i).loc_trait_id
            and exists (select 1
                          from region r,
                               district d,
                               store s
                         where r.area = I_hier_value
                           and d.region = r.region
                           and s.district = d.district
                           and lm.store = s.store);
      end if;
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
   return FALSE;
END DELETE_AREA;
---------------------------------------------------------------------
FUNCTION DELETE_REGION(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_loc_traits     IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                       I_hier_value     IN      NUMBER)
   RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'LOC_TRAITS_SQL.DELETE_REGION';
   L_loctrt_id    LOC_TRAITS.LOC_TRAIT%TYPE;

   cursor C_LOCK_LOC_TRAITS_MATRIX is
      select 'x'
        from LOC_TRAITS_MATRIX lm
       where lm.loc_trait = L_loctrt_id
         and exists (select 1
                       from district d,
                            store s
                      where d.region = I_hier_value
                        and s.district = d.district
                        and lm.store = s.store)
      for update nowait;

BEGIN

   FOR i in I_loc_traits.first..I_loc_traits.last LOOP
   if I_loc_traits(i).loc_trait_id is not NULL then

         L_loctrt_id := I_loc_traits(i).loc_trait_id;
            ---
         SQL_LIB.SET_MARK('OPEN','C_LOCK_LOC_TRAITS_MATRIX',NULL,'Hier value: ' || I_hier_value);
         open C_LOCK_LOC_TRAITS_MATRIX;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_LOC_TRAITS_MATRIX',NULL,'Hier value: ' || I_hier_value);
         close C_LOCK_LOC_TRAITS_MATRIX;

            delete from LOC_TRAITS_MATRIX lm
             where lm.loc_trait = I_loc_traits(i).loc_trait_id
               and exists (select 1
                             from district d,
                                  store s
                            where d.region = I_hier_value
                              and s.district = d.district
                              and lm.store = s.store);
      end if;
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
   return FALSE;
END DELETE_REGION;
---------------------------------------------------------------------
FUNCTION DELETE_DISTRICT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loc_traits     IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                         I_hier_value     IN      NUMBER)
   RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'LOC_TRAITS_SQL.DELETE_DISTRICT';
   L_loctrt_id    LOC_TRAITS.LOC_TRAIT%TYPE;

   cursor C_LOCK_LOC_TRAITS_MATRIX is
      select 'x'
        from LOC_TRAITS_MATRIX lm
       where lm.loc_trait = L_loctrt_id
         and exists (select 1
                       from district d,
                            store s
                      where d.district = I_hier_value
                        and s.district = d.district
                        and lm.store = s.store)
      for update nowait;

BEGIN

   FOR i in I_loc_traits.first..I_loc_traits.last LOOP
      if I_loc_traits(i).loc_trait_id is not NULL then

         L_loctrt_id := I_loc_traits(i).loc_trait_id;

         SQL_LIB.SET_MARK('OPEN','C_LOCK_LOC_TRAITS_MATRIX',NULL,'Hier value: ' || I_hier_value);
         open C_LOCK_LOC_TRAITS_MATRIX;

         SQL_LIB.SET_MARK('CLOSE','C_LOCK_LOC_TRAITS_MATRIX',NULL,'Hier value: ' || I_hier_value);
         close C_LOCK_LOC_TRAITS_MATRIX;

         delete from LOC_TRAITS_MATRIX lm
          where lm.loc_trait = I_loc_traits(i).loc_trait_id
            and exists (select 1
                          from district d,
                               store s
                         where d.district = I_hier_value
                           and s.district = d.district
                           and lm.store = s.store);
      end if;
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
   return FALSE;
END DELETE_DISTRICT;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_LOC_TRAITS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_org_hier_rec    IN      ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50) := 'LOC_TRAITS_SQL.INSERT_LOC_TRAITS';

BEGIN
   if UPPER(I_org_hier_rec.hier_level) = ORGANIZATION_SQL.LP_area then
      ---
      if not DEFAULT_AREA(O_error_message,
                          I_org_hier_rec.loc_trait_ids,
                          I_org_hier_rec.hier_value) then
         return FALSE;
      end if;
   elsif UPPER(I_org_hier_rec.hier_level) = ORGANIZATION_SQL.LP_region then
      ---
      if not DEFAULT_REGION(O_error_message,
                            I_org_hier_rec.loc_trait_ids,
                            I_org_hier_rec.hier_value) then
         return FALSE;
      end if;
   elsif UPPER(I_org_hier_rec.hier_level) = ORGANIZATION_SQL.LP_district then
       ---
       if not DEFAULT_DISTRICT(O_error_message,
                               I_org_hier_rec.loc_trait_ids,
                               I_org_hier_rec.hier_value) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
   return FALSE;

END INSERT_LOC_TRAITS;
---------------------------------------------------------------------
FUNCTION DELETE_LOC_TRAITS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50) := 'LOC_TRAITS_SQL.DELETE_LOC_TRAITS';
   L_table             VARCHAR2(20);
   L_column            VARCHAR2(10);
   L_hier_value        NUMBER;
   L_loc_trait         LOC_TRAITS.LOC_TRAIT%TYPE;

BEGIN

   if UPPER(I_org_hier_rec.hier_level) = ORGANIZATION_SQL.LP_area then
      ---
      if not DELETE_AREA(O_error_message,
                         I_org_hier_rec.loc_trait_ids,
                         I_org_hier_rec.hier_value) then
         return FALSE;
      end if;
   elsif UPPER(I_org_hier_rec.hier_level) = ORGANIZATION_SQL.LP_region then

      if not DELETE_REGION(O_error_message,
                           I_org_hier_rec.loc_trait_ids,
                           I_org_hier_rec.hier_value) then
         return FALSE;
      end if;
   elsif UPPER(I_org_hier_rec.hier_level) = ORGANIZATION_SQL.LP_district then
      if not DELETE_DISTRICT(O_error_message,
                             I_org_hier_rec.loc_trait_ids,
                             I_org_hier_rec.hier_value) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
   return FALSE;

END DELETE_LOC_TRAITS;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_STORE_TRAIT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_loc_traits      IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                            I_store           IN      STORE_ADD.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'LOC_TRAITS_SQL.INSERT_STORE_TRAIT';
   L_loc_trait     LOC_TRAITS_MATRIX.LOC_TRAIT%TYPE;

BEGIN

   if I_loc_traits is NOT NULL AND I_loc_traits.count > 0 then
      FOR i in I_loc_traits.first..I_loc_traits.last LOOP
         L_loc_trait := I_loc_traits(i).loc_trait_id;
         insert into LOC_TRAITS_MATRIX(loc_trait,
                                       store)
                                select L_loc_trait,
                                       s.store
                                  from store s
                                 where s.store = I_store
                                   and not exists (select 1
                                                     from loc_traits_matrix lm
                                                    where lm.loc_trait = L_loc_trait
                                                      and lm.store = s.store);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
   return FALSE;
END INSERT_STORE_TRAIT;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_STORE_TRAIT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_loc_traits      IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                            I_store           IN      STORE_ADD.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'LOC_TRAITS_SQL.DELETE_STORE_TRAIT';

BEGIN

   if I_loc_traits.count > 0 then
      FOR i in I_loc_traits.first..I_loc_traits.last LOOP

         if not LOCK_LOC_TRAITS_MATRIX(O_error_message,
                                       I_loc_traits(i).loc_trait_id,
                                       I_store) then
            return FALSE;
         end if;

         delete from loc_traits_matrix
          where loc_trait = I_loc_traits(i).loc_trait_id
            and store     = I_store;
         ---
         if SQL%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('NO_LOC_TRAIT_DEL');
            return FALSE;
         end if;
      end LOOP;
   end if;
   ---
   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_STORE_TRAIT;
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_LOC_TRAITS_MATRIX(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_loc_traits      IN       LOC_TRAITS_MATRIX.LOC_TRAIT%TYPE,
                                I_store_id        IN       STORE_ADD.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'STORE_SQL.LOCK_LOC_TRAITS_MATRIX';
   L_table        VARCHAR2(20)  := 'LOC_TRAITS_MATRIX';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_LOC_TRAITS_MATRIX IS
      select 'x'
        from LOC_TRAITS_MATRIX
       where store     = I_store_id
         and loc_trait = I_loc_traits
         for update nowait;
BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_LOC_TRAITS_MATRIX', 'STORE', 'store: '||I_store_id);
   open C_LOCK_LOC_TRAITS_MATRIX;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_LOC_TRAITS_MATRIX', 'STORE', 'store: '||I_store_id);
   close C_LOCK_LOC_TRAITS_MATRIX;
   ---
   return TRUE;
EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            L_table,
                                            'store '||I_store_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_LOC_TRAITS_MATRIX;
---------------------------------------------------------------------
END LOC_TRAITS_SQL;
/