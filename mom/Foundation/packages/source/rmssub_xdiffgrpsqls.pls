
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XDIFFGRP_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_diffgrp_rec     IN       DIFF_GROUP_SQL.DIFF_GROUP_REC,
                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XDIFFGRP_SQL;
/

