
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE PACKITEM_BREAK_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------
-- Name:      BREAK_TO_COMPONENTS
-- Purpose:   This function will break a specified quantity of a pack down to its 
--            component level. The individual stock on hand will decrease for the 
--            specified pack. The pack component stock on hand will decrease for 
--            component items within the pack. The individual stock on hand will 
--            increase for component items within the pack. I_break_qty must 
--            always be a positive number less than the current total stock on 
--            hand for the pack. 
--
FUNCTION BREAK_TO_COMPONENTS( O_error_message  IN OUT VARCHAR2,
                              I_pack_no        IN     PACKITEM.PACK_NO%TYPE,
                              I_loc            IN     ITEM_LOC.LOC%TYPE,
                              I_break_qty      IN     NUMBER,
                              I_break_to_level IN     VARCHAR2)
   return BOOLEAN;
------------------------------------------------------------------------------
END;
/


