CREATE OR REPLACE PACKAGE NEW_ITEM_LOC_SQL AUTHID CURRENT_USER AS

GV_who_called_me VARCHAR2(255) := NULL;
GV_country_id    COUNTRY.COUNTRY_ID%TYPE := NULL;
-------------------------------------------------------------------------------
TYPE NIL_INPUT_RECORD IS RECORD
   (
    ITEM                      ITEM_MASTER.ITEM%TYPE
   ,ITEM_PARENT               ITEM_MASTER.ITEM_PARENT%TYPE
   ,ITEM_GRANDPARENT          ITEM_MASTER.ITEM_GRANDPARENT%TYPE
   ,ITEM_SHORT_DESC           ITEM_MASTER.SHORT_DESC%TYPE
   ,DEPT                      ITEM_MASTER.DEPT%TYPE
   ,ITEM_CLASS                ITEM_MASTER.CLASS%TYPE
   ,SUBCLASS                  ITEM_MASTER.SUBCLASS%TYPE
   ,ITEM_LEVEL                ITEM_MASTER.ITEM_LEVEL%TYPE
   ,TRAN_LEVEL                ITEM_MASTER.TRAN_LEVEL%TYPE
   ,ITEM_STATUS               ITEM_MASTER.STATUS%TYPE
   ,WASTE_TYPE                ITEM_MASTER.WASTE_TYPE%TYPE
   ,SELLABLE_IND              ITEM_MASTER.SELLABLE_IND%TYPE
   ,ORDERABLE_IND             ITEM_MASTER.ORDERABLE_IND%TYPE
   ,PACK_IND                  ITEM_MASTER.PACK_IND%TYPE
   ,PACK_TYPE                 ITEM_MASTER.PACK_TYPE%TYPE
   ,ITEM_DESC                 ITEM_MASTER.ITEM_DESC%TYPE
   ,DIFF_1                    ITEM_MASTER.DIFF_1%TYPE
   ,DIFF_2                    ITEM_MASTER.DIFF_2%TYPE
   ,DIFF_3                    ITEM_MASTER.DIFF_3%TYPE
   ,DIFF_4                    ITEM_MASTER.DIFF_4%TYPE
   ,LOC                       ITEM_LOC.LOC%TYPE
   ,LOC_TYPE                  ITEM_LOC.LOC_TYPE%TYPE
   ,DAILY_WASTE_PCT           ITEM_LOC.DAILY_WASTE_PCT%TYPE
   ,UNIT_COST_LOC             ITEM_LOC_SOH.UNIT_COST%TYPE
   ,UNIT_RETAIL_LOC           ITEM_LOC.UNIT_RETAIL%TYPE
   ,SELLING_RETAIL_LOC        ITEM_LOC.SELLING_UNIT_RETAIL%TYPE
   ,SELLING_UOM               ITEM_LOC.SELLING_UOM%TYPE
   ,MULTI_UNITS               ITEM_LOC.MULTI_UNITS%TYPE
   ,MULTI_UNIT_RETAIL         ITEM_LOC.MULTI_UNIT_RETAIL%TYPE
   ,MULTI_SELLING_UOM         ITEM_LOC.MULTI_SELLING_UOM%TYPE
   ,ITEM_LOC_STATUS           ITEM_LOC.STATUS%TYPE
   ,TAXABLE_IND               ITEM_LOC.TAXABLE_IND%TYPE
   ,TI                        ITEM_LOC.TI%TYPE
   ,HI                        ITEM_LOC.HI%TYPE
   ,STORE_ORD_MULT            ITEM_LOC.STORE_ORD_MULT%TYPE
   ,MEAS_OF_EACH              ITEM_LOC.MEAS_OF_EACH%TYPE
   ,MEAS_OF_PRICE             ITEM_LOC.MEAS_OF_PRICE%TYPE
   ,UOM_OF_PRICE              ITEM_LOC.UOM_OF_PRICE%TYPE
   ,PRIMARY_VARIANT           ITEM_LOC.PRIMARY_VARIANT%TYPE
   ,PRIMARY_SUPP              ITEM_LOC.PRIMARY_SUPP%TYPE
   ,PRIMARY_CNTRY             ITEM_LOC.PRIMARY_CNTRY%TYPE
   ,LOCAL_ITEM_DESC           ITEM_LOC.LOCAL_ITEM_DESC%TYPE
   ,LOCAL_SHORT_DESC          ITEM_LOC.LOCAL_SHORT_DESC%TYPE
   ,PRIMARY_COST_PACK         ITEM_LOC.PRIMARY_COST_PACK%TYPE
   ,RECEIVE_AS_TYPE           ITEM_LOC.RECEIVE_AS_TYPE%TYPE
   ,STORE_PRICE_IND           ITEM_LOC.STORE_PRICE_IND%TYPE
   ,UIN_TYPE                  ITEM_LOC.UIN_TYPE%TYPE
   ,UIN_LABEL                 ITEM_LOC.UIN_LABEL%TYPE
   ,CAPTURE_TIME              ITEM_LOC.CAPTURE_TIME%TYPE
   ,EXT_UIN_IND               ITEM_LOC.EXT_UIN_IND%TYPE
   ,SOURCE_METHOD             ITEM_LOC.SOURCE_METHOD%TYPE
   ,SOURCE_WH                 ITEM_LOC.SOURCE_WH%TYPE
   ,INBOUND_HANDLING_DAYS     ITEM_LOC.INBOUND_HANDLING_DAYS%TYPE
   ,CURRENCY_CODE             CURRENCIES.CURRENCY_CODE%TYPE
   ,LIKE_STORE                ITEM_LOC.LOC%TYPE
   ,DEFAULT_TO_CHILDREN_IND   VARCHAR2(1)
   ,CLASS_VAT_IND             CLASS.CLASS_VAT_IND%TYPE
   ,HIER_LEVEL                VARCHAR2(6)
   ,HIER_NUM_VALUE            NUMBER
   ,HIER_CHAR_VALUE           VARCHAR2(6)
   ,COSTING_LOC               NUMBER(10)
   ,COSTING_LOC_TYPE          VARCHAR2(1)
   ,RANGED_IND                VARCHAR2(1)
   ,DEFAULT_WH                NUMBER(10)
   ,ITEM_LOC_IND              VARCHAR2(1)
    );
TYPE NIL_INPUT_TBL IS TABLE OF NIL_INPUT_RECORD INDEX BY BINARY_INTEGER;
--------------------------------------------------------------------------------
--Function Name: NEW_ITEM_LOC
--Purpose      : Range new location to items.
--------------------------------------------------------------------------------
FUNCTION NEW_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_input           IN       NIL_INPUT_TBL,
                      I_apply_sec_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION NEW_ITEM_LOC_WRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_input           IN       WRP_NIL_INPUT_TBL,
                          I_apply_sec_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN NUMBER;
--------------------------------------------------------------------------------
END NEW_ITEM_LOC_SQL;
/
