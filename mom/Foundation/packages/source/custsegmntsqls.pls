CREATE OR REPLACE PACKAGE CUST_SEGMENT_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------------------
--- Procedure Name: CHECK_CUST_SEGMENT_EXISTS
--- Purpose:        This function will check if the input Customer Segment is present in the
---                 CUSTOMER_SEGMENTS table.If present it will return the description.
------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CUST_SEGMENT_ID_EXISTS (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists                  IN OUT   BOOLEAN,
                                       O_customer_segment_desc   IN OUT   CUSTOMER_SEGMENTS.CUSTOMER_SEGMENT_DESC%TYPE,
                                       I_customer_segment_id     IN       CUSTOMER_SEGMENTS.CUSTOMER_SEGMENT_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
--- Procedure Name: CUST_SEGMENT_ID_EXCEP_EXISTS
--- Purpose:        This function will check if the input Customer Segment ID has any exceptions
---                 that prevent the segment type from being deleted.
------------------------------------------------------------------------------------------------------------
FUNCTION CUST_SEGMENT_ID_EXCEP_EXISTS (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists                 IN OUT   BOOLEAN,
                                       I_customer_segment_id    IN       CUSTOMER_SEGMENTS.CUSTOMER_SEGMENT_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
END CUST_SEGMENT_SQL;
/
