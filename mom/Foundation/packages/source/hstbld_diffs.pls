
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE HSTBLD_DIFF_PROCESS AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------
--- Function: DIFF_PROCESS
--- Purpose:  This package is called from batch program hstbld_diff.pc and is used to
---           check if a record already exists on tables item_diff_loc_hist and
---           item_parent_diff_loc_hist.  If the record exists, it is updated and if
---           it doesn't exist, a new record is added to the table.
---
---           Two functions were also added to summarize the data in item_diff_loc to
---           month-level. The data will be inserted in item_diff_loc_hist_mth and
---           item_parent_diff_loc_hist_mth. If the record exists, it is updated and if
---           it doesn't exist, a new record is added to the table.

---------------------------------------------------------------------------------------
FUNCTION DIFF_PROCESS(O_return_code   IN OUT   VARCHAR2,
                      O_error_msg     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_vdate         IN       ITEM_LOC_HIST.EOW_DATE%TYPE,
                      I_mode          IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
END HSTBLD_DIFF_PROCESS;
/
