
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XITEMRCLS_SQL AS

-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ITEMRECLASS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_itemreclass_rec   IN       RECLASS_SQL.RECLASS_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_RECLASS_ITEMS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_itemreclass_rec   IN       RECLASS_SQL.RECLASS_REC)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEMRECLASS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_itemreclass_rec   IN       RECLASS_SQL.RECLASS_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_RECLASS_ITEMS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_itemreclass_rec   IN       RECLASS_SQL.RECLASS_REC)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_itemreclass_rec   IN       RECLASS_SQL.RECLASS_REC,
                 I_message_type      IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEMRCLS_SQL.PERSIST';

BEGIN

   if I_message_type = RMSSUB_XITEMRCLS.LP_cre_type then
      if CREATE_ITEMRECLASS(O_error_message,
                            I_itemreclass_rec) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEMRCLS.LP_dtl_cre_type then
      if CREATE_RECLASS_ITEMS(O_error_message,
                              I_itemreclass_rec) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEMRCLS.LP_del_type then
      if DELETE_ITEMRECLASS(O_error_message,
                            I_itemreclass_rec) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEMRCLS.LP_dtl_del_type then
      if DELETE_RECLASS_ITEMS(O_error_message,
                              I_itemreclass_rec) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ITEMRECLASS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_itemreclass_rec   IN       RECLASS_SQL.RECLASS_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEMRCLS_SQL.CREATE_ITEMRECLASS';


BEGIN

   if RECLASS_SQL.CREATE_RECLASS(O_error_message,
                                 I_itemreclass_rec) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;

END CREATE_ITEMRECLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_RECLASS_ITEMS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_itemreclass_rec   IN       RECLASS_SQL.RECLASS_REC)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEMRCLS_SQL.CREATE_RECLASS_ITEMS';

BEGIN

   if RECLASS_SQL.CREATE_RECLASS_ITEM(O_error_message,
                                      I_itemreclass_rec) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END CREATE_RECLASS_ITEMS;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEMRECLASS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_itemreclass_rec   IN       RECLASS_SQL.RECLASS_REC)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEMRCLS_SQL.DELETE_ITEMRECLASS';

BEGIN

   if RECLASS_SQL.DELETE_RECLASS(O_error_message,
                                 I_itemreclass_rec) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;

END DELETE_ITEMRECLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_RECLASS_ITEMS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_itemreclass_rec   IN       RECLASS_SQL.RECLASS_REC)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEMRCLS_SQL.DELETE_RECLASS_ITEMS';



BEGIN

   if RECLASS_SQL.DELETE_RECLASS_ITEM(O_error_message,
                                      I_itemreclass_rec) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;

END DELETE_RECLASS_ITEMS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XITEMRCLS_SQL;
/
