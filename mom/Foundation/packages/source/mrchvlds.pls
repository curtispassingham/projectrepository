
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE MERCH_VALIDATE_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------
--Function Name: DIVISION_EXIST
--Purpose:       To check for the existence of a division
--               in the system.
--Created by:    Erica Oesting, 27-April-98.
----------------------------------------------------------
FUNCTION DIVISION_EXIST (O_error_message IN OUT VARCHAR2,
                         O_exists        IN OUT BOOLEAN,
                         I_division      IN     DIVISION.DIVISION%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------
--Function Name: GROUP_EXIST
--Purpose:       To check for the existence of a group
--               in the sysem.
--Created by:    Erica Oesting, 27-April-98.
----------------------------------------------------------
FUNCTION GROUP_EXIST (O_error_message IN OUT VARCHAR2,
                      O_exists        IN OUT BOOLEAN,
                      I_group         IN     GROUPS.GROUP_NO%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------
--Function Name:  GROUP_IN_DIVISION
--Purpose:        To check for the existence of a group/division
--                combination.
--Created by:     Erica Oesting, 27-April-98
----------------------------------------------------------
FUNCTION GROUP_IN_DIVISION (O_error_message IN OUT VARCHAR2,
                            O_exists        IN OUT BOOLEAN,
                            I_division      IN     GROUPS.DIVISION%TYPE,
                            I_group         IN     GROUPS.GROUP_NO%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------
--Function Name:  EXIST
--Purpose:        To check for the existence of a merch level combination
--Created by:     Gerson Berena, 05-September-03
----------------------------------------------------------
FUNCTION EXIST(O_error_message         IN OUT   VARCHAR2,
               O_exists                IN OUT   BOOLEAN,
               I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
               I_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
               I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
               I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE,
               I_effective_date        IN       PEND_MERCH_HIER.EFFECTIVE_DATE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------
--Function Name:  PARENT_EXIST
--Purpose:        To check for the existence of a possible parent for a merch level
--Created by:     Gerson Berena, 05-September-03
----------------------------------------------------------
FUNCTION PARENT_EXIST(O_error_message         IN OUT   VARCHAR2,
                      O_exists                IN OUT   BOOLEAN,
                      I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                      I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                      I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE,
                      I_effective_date        IN       PEND_MERCH_HIER.EFFECTIVE_DATE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------
--Function Name:  PARENT_EXIST
--Purpose:        To check for the existence of a possible parent company for a merch level
--                division
--Created by:     Gerson Berena, 05-September-03
----------------------------------------------------------
FUNCTION COMPANY_EXIST(O_error_message   IN OUT   VARCHAR2,
                       O_exists          IN OUT   BOOLEAN,
                       I_company         IN       COMPHEAD.COMPANY%TYPE)

   RETURN BOOLEAN;
----------------------------------------------------------
END MERCH_VALIDATE_SQL;
/
