CREATE OR REPLACE PACKAGE ITEM_COUNTRY_SQL AUTHID CURRENT_USER AS
GV_country_id    COUNTRY.COUNTRY_ID%TYPE := NULL;
---------------------------------------------------------------------------------------------
-- Function Name : CHECK_DUP_ITEM_COUNTRY
-- Purpose       : This function checks if any record exists in ITEM_COUNTRY for the input item,
--                 country. Returns TRUE if records exist else FALSE.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DUP_ITEM_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists        OUT    BOOLEAN,
                                I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                                I_item          IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : DELETE_ITEM_COUNTRY
-- Purpose       : This function deletes if any record exists in ITEM_COUNTRY_EXT, ITEM_COUNTRY
--                 for the input item, country if the country is not the primary delivery country
--                 in ITEM_SUPP_COUNTRY. It also delets the reocrds from ITEM_COST_DETAIL, ITEM_COST_HEAD
--                 for matching records. Throws an error expression if trying to delete country which
--                 is primary delivery country. Returns TRUE after successful deletion if not FALSE.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_item          IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : CHECK_COUNTRY_ATTRIB_EXIST
-- Purpose       : This function checks if any record exists in ITEM_COUNTRY_L10N_EXT for the input item,
--                 country. Returns TRUE if records exists else FALSE.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_COUNTRY_ATTRIB_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists        OUT    BOOLEAN,
                                    I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                                    I_item          IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : CHECK_ITEM_COUNTRY_EXIST
-- Purpose       : This function checks if any record exists in ITEM_COUNTRY for the input item.
--                 Returns TRUE if records exists else FALSE.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_COUNTRY_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists        IN OUT BOOLEAN,
                                  I_item          IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : CHECK_COUNTRY_EXISTS
-- Purpose       : This function checks if any record exists in ITEM_COUNTRY for the input country.
--                 Returns TRUE if records exists else FALSE.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_COUNTRY_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists        IN OUT BOOLEAN,
                              I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : COPY_DOWN_COUNTRY
-- Purpose       : This function updates/inserts records into ITEM_COUNTRY for the input item
--                 (parent/grand parent). Throws an error message if item cannot be exploded. 
--                 Returns TRUE for successful update/insert else FALSE.
---------------------------------------------------------------------------------------------
FUNCTION COPY_DOWN_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item          IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : DEFAULT_LOC_REQ
-- Purpose       : This function checks for presence of default country in COUNTRY for the input coutry
--                 Returns TRUE if records exists else FALSE.
---------------------------------------------------------------------------------------------
FUNCTION DEFAULT_LOC_REQ(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : CREATE_DEFAULT_ITEM_COUNTRY
-- Purpose       : This function checks for presence of default country in COUNTRY for the input coutry
--                 Returns TRUE if records exists else FALSE.
---------------------------------------------------------------------------------------------
FUNCTION CREATE_DEFAULT_ITEM_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item          IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : INSERT_ITEM_COUNTRY
-- Purpose       : This function inserts records into item_country for the input item, coutry
--                 Checks for existing records before insert. Returns TRUE if records exists else FALSE.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_item          IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : GET_ITEM_COUNTRY_ID
-- Purpose       : This function will return the country id from item_country for the input item.
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_COUNTRY_ID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_country_id    OUT    COUNTRY.COUNTRY_ID%TYPE,
                             I_item          IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : CHECK_ALL_COUNTRY_ATTRIB_EXIST
-- Purpose       : This function checks if any record exists in ITEM_COUNTRY_L10N_EXT for 
--                 for all localized countries in ITEM_COUNTRY based on the input item.
--                 Returns TRUE if records exists else FALSE.
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_ALL_COUNTRY_ATTRIB_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_exists        OUT    BOOLEAN,
                                        I_item          IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : VALIDATE_ITEM_COUNTRY
-- Purpose       : This function validates the country attributes and checks L10N functionality.
--                 Returns TRUE is validation succeeds else FALSE.          
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE,
                               I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : INSERT_ITEM_DLVRY_COUNTRY
-- Purpose       : This function validates if delivery country exists for the store country.
--                 If not, it will insert into item_cost_head for the store's country.         
---------------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_DLVRY_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                   I_supplier      IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                                   I_origin_cntry  IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                                   I_country_id    IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_DLVRY_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                   I_supplier      IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                                   I_origin_cntry  IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : INSERT_ITEM_COUNTRY
-- Purpose       : This function bulk inserts location's country into item_country if
--                 a record doesn't already exist. 
---------------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_COUNTRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_locs            IN       LOC_TBL)
return BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name :  DELETE_CHILD_ITEM_COUNTRY
-- Function Name : CHECK_LOCALIZED_CTRY_ATTRIB
-- Purpose       : This function checks if at least one localized country record exists in
--                 ITEM_COUNTRY and ITEM_COUNTRY_L10N_EXT tables based on the input item.
--                 Returns TRUE if records exists else FALSE.
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_LOCALIZED_CTRY_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists          IN OUT   BOOLEAN,
                                     I_item            IN       ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;

---------------------------------------------------------------------------------------------
--Purpose       :  This function deletes the country for child items.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_CHILD_ITEM_COUNTRY(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_parent_country     IN      ITEM_COUNTRY.COUNTRY_ID%TYPE,
                                   I_item               IN      ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------
--Function Name   :  CHECK_ITEM_LOC_EXISTS
--Purpose         :  This function checks if there exists a loc for an item.
-------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LOC_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists        OUT    BOOLEAN,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE,
                               I_supplier      IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                               I_origin_cntry  IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------
END ITEM_COUNTRY_SQL;
/