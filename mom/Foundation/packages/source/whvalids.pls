
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE WAREHOUSE_VALIDATE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-- Function Name: EXIST
-- Purpose	: checks to see that a given store exists on the 
--		  store table.
-- Return Values: O_exist, BOOLEAN, set to TRUE if store exists
--		  on table, FALSE otherwise.
-- Calls	: NONE	
-- Created	: 22-AUG_96 by Matt Sniffen
--------------------------------------------------------------------
FUNCTION EXIST(	O_error_message	IN OUT	VARCHAR2,
		I_WH		IN	NUMBER,
		O_exist		IN OUT	BOOLEAN)
	RETURN BOOLEAN;
--------------------------------------------------------------------
END WAREHOUSE_VALIDATE_SQL;
/


