CREATE OR REPLACE PACKAGE BODY LOCLIST_BUILD_SQL AS

-----------------------------------------------------------------------
FUNCTION LOCK_DETAILS(O_error_message IN OUT VARCHAR2,
                      I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64) := 'LOCLIST_BUILD_SQL.LOCK_DETAILS';
   L_table        VARCHAR2(20) := 'LOC_LIST_DETAIL';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_DETAIL is
      select 'x'
        from loc_list_detail
       where loc_list = I_loc_list
         for update nowait;

BEGIN
   open C_LOCK_DETAIL;
   close C_LOCK_DETAIL;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_DETAIL%ISOPEN then
         close C_LOCK_DETAIL;
      end if;
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_loc_list));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END LOCK_DETAILS;

-----------------------------------------------------------------------
FUNCTION DELETE_DETAILS(O_error_message IN OUT VARCHAR2,
                        I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64) := 'LOCLIST_BUILD_SQL.DELETE_DETAILS';
   L_table        VARCHAR2(20) := 'LOC_LIST_DETAIL';

BEGIN
   delete from loc_list_detail
    where loc_list = I_loc_list;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END DELETE_DETAILS;

-----------------------------------------------------------------------
FUNCTION LOCK_CRITERIA(O_error_message IN OUT VARCHAR2,
                       I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64) := 'LOCLIST_BUILD_SQL.LOCK_CRITERIA';
   L_table        VARCHAR2(20) := 'LOC_LIST_CRITERIA';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_CRITERIA is
      select 'x'
        from loc_list_criteria
       where loc_list = I_loc_list
         for update nowait;

BEGIN
   open C_LOCK_CRITERIA;
   close C_LOCK_CRITERIA;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_CRITERIA%ISOPEN then
         close C_LOCK_CRITERIA;
      end if;
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_loc_list));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END LOCK_CRITERIA;

-----------------------------------------------------------------------
FUNCTION DELETE_CRITERIA(O_error_message IN OUT VARCHAR2,
                         I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64) := 'LOCLIST_BUILD_SQL.DELETE_CRITERIA';

BEGIN
   delete from loc_list_criteria
    where loc_list = I_loc_list;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END DELETE_CRITERIA;

-----------------------------------------------------------------------
FUNCTION DELETE_CRITERIA_TEMP(O_error_message IN OUT VARCHAR2,
                              I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64) := 'LOCLIST_BUILD_SQL.DELETE_CRITERIA_TEMP';
   L_table        VARCHAR2(30) := 'LOC_LIST_CRITERIA_TEMP';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_CRITERIA_TEMP is
      select 'x'
        from loc_list_criteria_temp
       where loc_list = I_loc_list
         for update nowait;

BEGIN
   open C_LOCK_CRITERIA_TEMP;
   close C_LOCK_CRITERIA_TEMP;
   ---
   delete from loc_list_criteria_temp
    where loc_list = I_loc_list;
   --
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_CRITERIA_TEMP%ISOPEN then
         close C_LOCK_CRITERIA_TEMP;
      end if;
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_loc_list));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END DELETE_CRITERIA_TEMP;

-----------------------------------------------------------------------
FUNCTION BUILD_ST_CRITERIA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_where_clause    IN OUT   VARCHAR2,
                           I_loc_list        IN       LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'LOCLIST_BUILD_SQL.BUILD_ST_CRITERIA';
   L_open_parenthesis  LOC_LIST_CRITERIA.OPEN_PARENTHESIS%TYPE;
   L_element           LOC_LIST_CRITERIA.ELEMENT%TYPE;
   L_comparison        LOC_LIST_CRITERIA.COMPARISON%TYPE;
   L_value             LOC_LIST_CRITERIA.VALUE%TYPE;
   L_close_parenthesis LOC_LIST_CRITERIA.CLOSE_PARENTHESIS%TYPE;
   L_logic_operation   LOC_LIST_CRITERIA.LOGIC_OPERATION%TYPE;
   ---
   L_first_time        BOOLEAN := TRUE;
   L_field_name        VARCHAR2(100);
   L_field             VARCHAR2(50);
   L_comp_op           VARCHAR2(10);
   L_val               VARCHAR2(200);
   L_temp              VARCHAR2(1000);
   L_last_eow_date     VARCHAR2(11);
   L_related_value     VARCHAR2(15);
   L_NULL              VARCHAR2(4) := 'NULL';

   ---
   cursor C_ST_CRITERIA is
      select open_parenthesis,
             element,
             comparison,
             value,
             close_parenthesis,
             logic_operation,
             related_value
        from loc_list_criteria
       where loc_list = I_loc_list
         and loc_type = 'S'
       order by seq_no;

   cursor C_LAST_EOW_DATE is
      select to_char(last_eow_date, 'DD-MON-YYYY')
        from system_variables;
   

BEGIN
   
   open C_ST_CRITERIA;
   LOOP
      fetch C_ST_CRITERIA into L_open_parenthesis,
                               L_element,
                               L_comparison,
                               L_value,
                               L_close_parenthesis,
                               L_logic_operation,
                               L_related_value;
      if C_ST_CRITERIA%NOTFOUND then
         Exit;
      end if;
      ---
      if L_related_value is NULL then
         L_related_value := L_NULL;
      end if;
      ---
      if L_first_time = TRUE then
         O_where_clause := 'where (';
         L_first_time := FALSE;
      end if;
      ---
      if L_comparison = 'EQ' then
         L_comp_op := '= ';
      elsif L_comparison = 'NE' then
         L_comp_op := '!= ';
      elsif L_comparison = 'GT' then
         L_comp_op := '> ';
      elsif L_comparison = 'LT' then
         L_comp_op := '< ';
      elsif L_comparison = 'GE' then
         L_comp_op := '>= ';
      elsif L_comparison = 'LE' then
         L_comp_op := '<= ';
      elsif L_comparison is NULL then
         L_comp_op := NULL;
      else
         O_error_message := sql_lib.create_msg('INV_COMP_OP', null, null, null);
         return FALSE;
      end if;
      ---
      if L_comparison = 'NE' then --this if statement was added so that null records would be returned
         if L_element = 'SN' then  -- Store Number
            L_field_name := 'store ';
            L_val        := RTRIM(L_value);
            L_temp       := L_field_name || L_comp_op || L_val;
         elsif L_element = 'ST' then  -- State
            L_field_name := 'NVL(state, -999) ';
            L_val := '''' || RTRIM(L_value) || '''';
            L_temp := 'exists (select ''x'' ' ||
                                'from addr ' ||
                               'where store.store = addr.key_value_1 ' ||
                                  'and module in (''ST'',''WFST'') '||
                                 'and ' || L_field_name || L_comp_op || L_val || ')';
         elsif L_element = 'CTY' then  -- Country ID
            L_field_name := 'country_id ';
            L_val := '''' || RTRIM(L_value) || '''';
            L_temp := 'exists (select ''x'' ' ||
                                'from addr ' ||
                               'where store.store = addr.key_value_1 ' ||
                                 'and module in (''ST'',''WFST'') '||
                                 'and ' || L_field_name || L_comp_op || L_val || ')';
         elsif L_element = 'WFC' then  -- WF Customer ID
            L_field_name := 'wf_customer_id ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'ZC' then  -- Zip Code
            L_field_name := 'post ';
            L_val := '''' || RTRIM(L_value) || '''';
            L_temp := 'exists (select ''x'' ' ||
                                'from addr ' ||
                               'where store.store = addr.key_value_1 ' ||
                                 'and module in (''ST'',''WFST'') '||
                                 'and ' || L_field_name || L_comp_op || L_val || ')';
         elsif L_element = 'VR' then  -- VAT Region
            L_field_name := 'NVL(vat_region, -999) ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'CUR' then  -- Currency
            L_field_name := 'NVL(currency_code, -999) ';
            L_val := '''' || RTRIM(L_value) || '''';
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'DT' then  -- District
            L_field_name := 'district ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'DW' then  -- Default WH
            L_field_name := 'NVL(default_wh, -999) ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'TSF' then  -- Transfer Zone
            L_field_name := 'NVL(transfer_zone, -999) ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'LNG' then  -- Language
            L_field_name := 'lang ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'SC' then  -- Store Class
            L_field_name := 'store_class ';
            L_val := '''' || RTRIM(L_value) || '''';
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'SF' then  -- Store Format
            L_field_name := 'NVL(store_format, -999) ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'TA' then  -- Total Area
            L_field_name := 'NVL(total_square_ft, -999) ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'SA' then  -- Selling Area
            L_field_name := 'NVL(selling_square_ft, -999) ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'OD' then  -- Store Open Date
            L_field_name := 'store_open_date ';
            L_val := 'to_date(''' || RTRIM(L_value) || ''', ''DD-MM-YYYY'')';
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'AD' then  -- Store Acquired Date
            L_field_name := 'NVL(acquired_date,  to_date(''01-01-1951'', ''DD-MM-YYYY'')) ';
            L_val := 'to_date(''' || RTRIM(L_value) || ''', ''DD-MM-YYYY'')';
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'RD' then -- Store Remodel Date
            L_field_name := 'NVL(remodel_date,  to_date(''01-01-1951'', ''DD-MM-YYYY'')) ';
            L_val := 'to_date(''' || RTRIM(L_value) || ''', ''DD-MM-YYYY'')';
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'SG' then  -- Store Grade
            L_field_name := 'store_grade ';
            L_field      := 'store_grade_group_id';
            L_val := '''' || RTRIM(L_value) || '''';
            L_temp := 'exists (select ''x'' ' ||
                                'from store_grade_store ' ||
                               'where store.store = store_grade_store.store ' ||
                                 'and ' || L_field_name || L_comp_op || L_val ||
                                 'and store_grade_group_id = nvl('||RTRIM(L_related_value)||', '||L_field||')'|| ')';
         elsif L_element = 'CZ' then  -- Cost Zone
            L_field_name := 'zone_id ';
            L_field      := 'zone_group_id';
            L_val := RTRIM(L_value);
            L_temp := 'exists (select ''x'' ' ||
                                'from cost_zone_group_loc ' ||
                               'where store.store = cost_zone_group_loc.location ' ||
                                 'and loc_type = ''S'' ' ||
                                 'and ' || L_field_name || L_comp_op || L_val ||
                                 'and zone_group_id = nvl('||RTRIM(L_related_value)||', '||L_field||')'|| ')';
         elsif L_element = 'LT' then  -- Location Traits
            L_field_name := 'loc_trait ';
            L_val := RTRIM(L_value);
            L_temp := 'exists (select ''x'' ' ||
                                'from loc_traits_matrix ' ||
                               'where store.store = loc_traits_matrix.store ' ||
                                 'and ' || L_field_name || L_comp_op || L_val || ')';
         elsif L_element = 'SL' then  -- Store Sales Level for Last Closed Week
            if L_last_eow_date is NULL then
               open C_LAST_EOW_DATE;
               fetch C_LAST_EOW_DATE into L_last_eow_date;
               if C_LAST_EOW_DATE%NOTFOUND then
                  O_error_message := sql_lib.create_msg('NO_LAST_EOW_DATE', null, null, null);
                  return FALSE;
               end if;
            end if;
            L_temp := 'exists (select ''x'' ' ||
                                'from week_data ' ||
                               'where store.store = week_data.location ' ||                                
                               'group by store ' ||
                              'having sum(net_sales_retail) ' || L_comp_op || L_value || ' ' ||
                                 'and max(eow_date) < ''' || L_last_eow_date || ''')';
         elsif L_element = 'FL' then  -- Store Forecast Level for Last Closed Week
            if L_last_eow_date is NULL then
               open C_LAST_EOW_DATE;
               fetch C_LAST_EOW_DATE into L_last_eow_date;
               if C_LAST_EOW_DATE%NOTFOUND then
                  O_error_message := sql_lib.create_msg('NO_LAST_EOW_DATE', null, null, null);
                  return FALSE;
               end if;
            end if;
            L_temp := 'exists (select ''x'' ' ||
                                'from item_forecast ' ||
                               'where store.store = item_forecast.loc ' ||
                               'group by loc ' ||
                              'having sum(forecast_sales) ' || L_comp_op || L_value || ' ' ||
                                 'and max(eow_date) < ''' || L_last_eow_date || ''')';
         elsif L_element = 'TE' then  -- Transfer Entity ID
            L_field_name := 'tsf_entity_id ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element is NULL then
            L_field_name := NULL;
            L_val := NULL;
            L_temp := NULL;
         else
            O_error_message := sql_lib.create_msg('INV_ST_CRIT', null, null, null);
            return FALSE;
         end if;
      else
         if L_element = 'SN' then  -- Store Number
            L_field_name := 'store ';
            L_val        := RTRIM(L_value);
            L_temp       := L_field_name || L_comp_op || L_val;
         elsif L_element = 'ST' then  -- State
            L_field_name := 'NVL(state, -999) ';
            L_val := '''' || RTRIM(L_value) || '''';
            L_temp := 'exists (select ''x'' ' ||
                                'from addr ' ||
                               'where store.store = addr.key_value_1 ' ||
                                 'and module in (''ST'',''WFST'') '||
                                 'and ' || L_field_name || L_comp_op || L_val || ')';
          elsif L_element = 'CTY' then  -- Country ID
            L_field_name := 'country_id ';
            L_val := '''' || RTRIM(L_value) || '''';
            L_temp := 'exists (select ''x'' ' ||
                                'from addr ' ||
                               'where store.store = addr.key_value_1 ' ||
                                 'and module in (''ST'',''WFST'') '||
                                 'and ' || L_field_name || L_comp_op || L_val || ')';
          elsif L_element = 'WFC' then -- WF Customer ID
             L_field_name := 'wf_customer_id ';
             L_val := RTRIM(L_value);
             L_temp := L_field_name || L_comp_op || L_val;
          elsif L_element = 'ZC' then  -- Zip Code
            L_field_name := 'post ';
            L_val := '''' || RTRIM(L_value) || '''';
            L_temp := 'exists (select ''x'' ' ||
                               'from addr ' ||
                               'where store.store = addr.key_value_1 ' ||
                                'and module in (''ST'',''WFST'') '||
                                 'and ' || L_field_name || L_comp_op || L_val || ')';
         elsif L_element = 'VR' then  -- VAT Region
            L_field_name := 'vat_region ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'CUR' then  -- Currency
            L_field_name := 'currency_code ';
            L_val := '''' || RTRIM(L_value) || '''';
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'DT' then  -- District
            L_field_name := 'district ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'DW' then  -- Default WH
            L_field_name := 'default_wh ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'TSF' then  -- Transfer Zone
            L_field_name := 'transfer_zone ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'LNG' then  -- Language
            L_field_name := 'lang ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'SC' then  -- Store Class
            L_field_name := 'store_class ';
            L_val := '''' || RTRIM(L_value) || '''';
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'SF' then  -- Store Format
            L_field_name := 'store_format ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'TA' then  -- Total Area
            L_field_name := 'total_square_ft ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'SA' then  -- Selling Area
            L_field_name := 'selling_square_ft ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'OD' then  -- Store Open Date
            L_field_name := 'store_open_date ';
            L_val := 'to_date(''' || RTRIM(L_value) || ''', ''DD-MM-YYYY'')';
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'AD' then  -- Store Acquired Date
            L_field_name := 'acquired_date ';
            L_val := 'to_date(''' || RTRIM(L_value) || ''', ''DD-MM-YYYY'')';
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'RD' then -- Store Remodel Date
            L_field_name := 'remodel_date ';
            L_val := 'to_date(''' || RTRIM(L_value) || ''', ''DD-MM-YYYY'')';
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element = 'SG' then  -- Store Grade
            L_field_name := 'store_grade ';
            L_field      := 'store_grade_group_id';
            L_val := '''' || RTRIM(L_value) || '''';
            L_temp := 'exists (select ''x'' ' ||
                                'from store_grade_store ' ||
                               'where store.store = store_grade_store.store ' ||
                                 'and ' || L_field_name || L_comp_op || L_val ||
                                 'and store_grade_group_id = nvl('||RTRIM(L_related_value)||', '||L_field||')'|| ')';
         elsif L_element = 'CZ' then  -- Cost Zone
            L_field_name := 'zone_id ';
            L_field      := 'zone_group_id';
            L_val := RTRIM(L_value);
            L_temp := 'exists (select ''x'' ' ||
                                'from cost_zone_group_loc ' ||
                               'where store.store = cost_zone_group_loc.location ' ||
                                 'and loc_type = ''S'' ' ||
                                 'and ' || L_field_name || L_comp_op || L_val ||
                                 'and zone_group_id = nvl('||RTRIM(L_related_value)||', '||L_field||')'|| ')';
         elsif L_element = 'LT' then  -- Location Traits
            L_field_name := 'loc_trait ';
            L_val := RTRIM(L_value);
            L_temp := 'exists (select ''x'' ' ||
                                'from loc_traits_matrix ' ||
                               'where store.store = loc_traits_matrix.store ' ||
                                 'and ' || L_field_name || L_comp_op || L_val || ')';
         elsif L_element = 'SL' then  -- Store Sales Level for Last Closed Week
            if L_last_eow_date is NULL then
               open C_LAST_EOW_DATE;
               fetch C_LAST_EOW_DATE into L_last_eow_date;
               if C_LAST_EOW_DATE%NOTFOUND then
                  O_error_message := sql_lib.create_msg('NO_LAST_EOW_DATE', null, null, null);
                  return FALSE;
               end if;
            end if;
            L_temp := 'exists (select ''x'' ' ||
                                'from week_data ' ||
                               'where store.store = week_data.location ' ||                                
                               'group by store ' ||
                              'having sum(net_sales_retail) ' || L_comp_op || L_value || ' ' ||
                                 'and max(eow_date) < ''' || L_last_eow_date || ''')';
         elsif L_element = 'FL' then  -- Store Forecast Level for Last Closed Week
            if L_last_eow_date is NULL then
               open C_LAST_EOW_DATE;
               fetch C_LAST_EOW_DATE into L_last_eow_date;
               if C_LAST_EOW_DATE%NOTFOUND then
                  O_error_message := sql_lib.create_msg('NO_LAST_EOW_DATE', null, null, null);
                  return FALSE;
               end if;
            end if;
            L_temp := 'exists (select ''x'' ' ||
                                'from item_forecast ' ||
                               'where store.store = item_forecast.loc ' ||
                               'group by loc ' ||
                              'having sum(forecast_sales) ' || L_comp_op || L_value || ' ' ||
                                 'and max(eow_date) < ''' || L_last_eow_date || ''')';
         elsif L_element = 'TE' then  -- Transfer Entity ID
            L_field_name := 'tsf_entity_id ';
            L_val := RTRIM(L_value);
            L_temp := L_field_name || L_comp_op || L_val;
         elsif L_element is NULL then
            L_field_name := NULL;
            L_val := NULL;
            L_temp := NULL;
         else
            O_error_message := sql_lib.create_msg('INV_ST_CRIT', null, null, null);
            return FALSE;
         end if;
      end if;
      ---
      if (LENGTH(O_where_clause) + LENGTH(L_temp) + 10) > 32600 then
         O_error_message := sql_lib.create_msg('CRIT_QRY_TOO_LONG');
         return FALSE;
      end if;
      O_where_clause := O_where_clause || ' ' ||
                        L_open_parenthesis ||
                        L_temp ||
                        L_close_parenthesis || ' ' ||
                        L_logic_operation;
   END LOOP;

   if O_where_clause is NOT NULL then
      O_where_clause := O_where_clause || ')' ;
   end if;

   close C_ST_CRITERIA;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END BUILD_ST_CRITERIA;

-----------------------------------------------------------------------
FUNCTION BUILD_WH_CRITERIA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_where_clause  IN OUT VARCHAR2,
                           I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'LOCLIST_BUILD_SQL.BUILD_WH_CRITERIA';
   L_open_parenthesis  LOC_LIST_CRITERIA.OPEN_PARENTHESIS%TYPE;
   L_element           LOC_LIST_CRITERIA.ELEMENT%TYPE;
   L_comparison        LOC_LIST_CRITERIA.COMPARISON%TYPE;
   L_value             LOC_LIST_CRITERIA.VALUE%TYPE;
   L_close_parenthesis LOC_LIST_CRITERIA.CLOSE_PARENTHESIS%TYPE;
   L_logic_operation   LOC_LIST_CRITERIA.LOGIC_OPERATION%TYPE;
   L_pwh               VARCHAR2(1) := 'N';
   ---
   L_first_time        BOOLEAN := TRUE;
   L_field_name        VARCHAR2(40);
   L_comp_op           VARCHAR2(10);
   L_val               VARCHAR2(200);
   L_temp              VARCHAR2(32000);
   ---
   cursor C_WH_CRITERIA is
      select open_parenthesis,
             element,
             comparison,
             value,
             close_parenthesis,
             logic_operation
        from loc_list_criteria
       where loc_list = I_loc_list
         and loc_type = 'W'
       order by seq_no;

BEGIN
   open C_WH_CRITERIA;
   LOOP
      fetch C_WH_CRITERIA into L_open_parenthesis,
                               L_element,
                               L_comparison,
                               L_value,
                               L_close_parenthesis,
                               L_logic_operation;
      if C_WH_CRITERIA%NOTFOUND then
         Exit;
      end if;
      ---
      if L_first_time = TRUE then
         O_where_clause := 'where';
         L_first_time := FALSE;
      end if;
      ---
      if L_comparison = 'EQ' then
         L_comp_op := '= ';
      elsif L_comparison = 'NE' then
         L_comp_op := '!= ';
      elsif L_comparison = 'GT' then
         L_comp_op := '> ';
      elsif L_comparison = 'LT' then
         L_comp_op := '< ';
      elsif L_comparison = 'GE' then
         L_comp_op := '>= ';
      elsif L_comparison = 'LE' then
         L_comp_op := '<= ';
      elsif L_comparison is NULL then
         L_comp_op := NULL;
      else
         O_error_message := sql_lib.create_msg('INV_COMP_OP', null, null, null);
         return FALSE;
      end if;
      ---
      if L_element = 'PWN' then --Physical Warehouse Number
         L_field_name := 'NVL(physical_wh, -999) ';
         L_val := RTRIM(L_value);
         if L_pwh != 'Y' then
            L_pwh := 'Y';
         end if;
         L_temp := L_field_name || L_comp_op || L_val ;
      elsif L_element = 'WN' then  -- Warehouse Number
         L_field_name := 'NVL(wh, -999) ';
         L_val := RTRIM(L_value);
         if L_pwh != 'Y' then
            L_pwh := 'Y';
         end if;
         L_temp := L_field_name || L_comp_op || L_val ;
      elsif L_element = 'ST' then  -- State
         L_field_name := 'NVL(state, -999) ';
         L_val := '''' || RTRIM(L_value) || '''';
         L_temp := 'exists (select ''x'' ' ||
                             'from addr ' ||
                            'where wh.wh = addr.key_value_1 ' ||
                              'and module = ''WH'' '||
                              'and ' || L_field_name || L_comp_op || L_val || ')';
         if L_pwh != 'Y' then
            L_pwh := 'Y';
         end if;
      elsif L_element = 'CTY' then  -- Country ID
         L_field_name := 'country_id ';
         L_val := '''' || RTRIM(L_value) || '''';
         L_temp := 'exists (select ''x'' ' ||
                             'from addr ' ||
                            'where wh.wh = addr.key_value_1 ' ||
                              'and module = ''WH'' '||
                              'and ' || L_field_name || L_comp_op || L_val || ')';
         if L_pwh != 'Y' then
     L_pwh := 'Y';
         end if;
      elsif L_element = 'ZC' then  -- Zip Code
         L_field_name := 'post ';
         L_val := '''' || RTRIM(L_value) || '''';
         L_temp := 'exists (select ''x'' ' ||
                             'from addr ' ||
                            'where wh.wh = addr.key_value_1 ' ||
                              'and module = ''WH'' '||
                              'and ' || L_field_name || L_comp_op || L_val || ')';
         if L_pwh != 'Y' then
             L_pwh := 'Y';
         end if;
      elsif L_element = 'VR' then  -- VAT Region
         L_field_name := 'NVL(vat_region, -999) ';
         L_val := RTRIM(L_value);
         if L_pwh != 'Y' then
            L_pwh := 'Y';
         end if;
         L_temp := L_field_name || L_comp_op || L_val ;
      elsif L_element = 'CUR' then  -- Currency
         L_field_name := 'currency_code ';
         L_val := '''' || RTRIM(L_value) || '''';
         if L_pwh != 'Y' then
            L_pwh := 'Y';
         end if;
         L_temp := L_field_name || L_comp_op || L_val ;
      elsif L_element = 'TE' then  -- Transfer Entity ID
         L_field_name := 'tsf_entity_id ';
         L_val := RTRIM(L_value);
         if L_pwh != 'Y' then
            L_pwh := 'Y';
         end if;
         L_temp := L_field_name || L_comp_op || L_val ;
      elsif L_element is NULL then
         L_field_name := NULL;
         L_val := NULL;
         L_temp := NULL;
      else
         O_error_message := sql_lib.create_msg('INV_WH_CRIT', null, null, null);
         return FALSE;
      end if;
      ---      
      if (LENGTH(O_where_clause) +
          LENGTH(L_temp) + 10) > 32600 then
         O_error_message := sql_lib.create_msg('CRIT_QRY_TOO_LONG');
         return FALSE;
      end if;
      O_where_clause := O_where_clause || ' ' ||
                        L_open_parenthesis ||
                        L_temp ||
                        L_close_parenthesis || ' ' ||
                        L_logic_operation;
   END LOOP;
   close C_WH_CRITERIA;
   ---
   if L_pwh = 'Y' then
      O_where_clause := O_where_clause || ' AND stockholding_ind = ''Y''';
   end if;
   if O_where_clause is not null then
      O_where_clause := O_where_clause || ' AND finisher_ind = ''N''';
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END BUILD_WH_CRITERIA;

-----------------------------------------------------------------------
FUNCTION REBUILD_LIST(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_criteria_exist IN OUT VARCHAR2,
                      I_loc_list       IN     LOC_LIST_HEAD.LOC_LIST%TYPE,
                      I_org_level      IN     VARCHAR2 DEFAULT NULL,
                      I_org_id         IN     STORE.STORE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'LOCLIST_BUILD_SQL.REBUILD_LIST';
   L_table             VARCHAR2(20) := 'SIT_EXPLODE';
   L_source            LOC_LIST_HEAD.SOURCE%TYPE;
   L_exist             VARCHAR2(1);
   L_sit_exists        BOOLEAN;
   L_itemlist          SKULIST_HEAD.SKULIST%TYPE;
   L_itemloc_link_id   SIT_HEAD.ITEMLOC_LINK_ID%TYPE;
   ---
   L_st_cursor         INTEGER;
   L_wh_cursor         INTEGER;
   L_st_where_clause   VARCHAR2(32600) := NULL;
   L_wh_where_clause   VARCHAR2(32600) := NULL;
   L_org_lvl_where_clause VARCHAR2(32600) := NULL;
   L_st_insert_stmt    VARCHAR2(32767);
   L_wh_insert_stmt    VARCHAR2(32767);
   L_rows_processed    INTEGER;
   ---
   L_mark              NUMBER(1) := 0; -- to locate PARSE error
   ---
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---

   cursor C_CRITERIA_EXIST is
      select 'x'
        from loc_list_criteria
       where loc_list = I_loc_list;

   cursor C_LOCK_SIT_EXPLODE is
      select 'x'
        from sit_explode
       where loc_list = I_loc_list
         for update nowait;

   cursor C_SIT_INFO is
      select itemloc_link_id,
             skulist
        from sit_head
       where loc_list = I_loc_list;

   cursor C_LOCK_LOCLIST_HEAD is
      select 'x'
        from loc_list_head
       where loc_list = I_loc_list
         for update nowait;


BEGIN
   -- check if the location list is created from RMS
   -- if not, cannot rebuild location list.
   if LOCLIST_ATTRIBUTE_SQL.GET_SOURCE(O_error_message,
                                       L_source,
                                       I_loc_list) = FALSE then
      return FALSE;
   end if;
   if L_source != 'RMS' then
      O_error_message := sql_lib.create_msg('REBUILD_RMS_ONLY', null, null, null);
      return FALSE;
   end if;

   -- check if there is any location list criteria associated with the location list
   -- if not, cannot rebuild the location list. (For a location list generated from RMS,
   -- there shouldn't be any detail records if there is no criteria record.)

   SQL_LIB.SET_MARK('OPEN', 'C_CRITERIA_EXISTS', 'LOC_LIST_CRITERIA', ' LOC_LIST: '||to_char(I_loc_list));
   open C_CRITERIA_EXIST;
   SQL_LIB.SET_MARK('FETCH', 'C_CRITERIA_EXISTS', 'LOC_LIST_CRITERIA', ' LOC_LIST: '||to_char(I_loc_list));
   fetch C_CRITERIA_EXIST into L_exist;
   if C_CRITERIA_EXIST%NOTFOUND then
      O_error_message := sql_lib.create_msg('NO_CRIT_FOR_REBUILD', null, null, null);
      SQL_LIB.SET_MARK('CLOSE', 'C_CRITERIA_EXISTS', 'LOC_LIST_CRITERIA', ' LOC_LIST: '||to_char(I_loc_list));
      close C_CRITERIA_EXIST;
      O_criteria_exist := 'N';
      return TRUE;
   else
      O_criteria_exist := 'Y';
      SQL_LIB.SET_MARK('CLOSE', 'C_CRITERIA_EXISTS', 'LOC_LIST_CRITERIA', ' LOC_LIST: '||to_char(I_loc_list));
      close C_CRITERIA_EXIST;
   end if;
   -- rebuild store and/or warehouse location list
   if LOCK_DETAILS(O_error_message,
                   I_loc_list) = FALSE then
      return FALSE;
   end if;
   if DELETE_DETAILS(O_error_message,
                     I_loc_list) = FALSE then
      return FALSE;
   end if;
   ---
   if BUILD_ST_CRITERIA(O_error_message,
                        L_st_where_clause,
                        I_loc_list) = FALSE then
      return FALSE;
   end if;
   ---
   --  Set up the initial org_lvl_where_clause
   L_org_lvl_where_clause := ' and store in (select str.store ' ||
                             '               from   store str, ' ||
                             '                      district dis, ' ||
                             '                      region   rgn, ' ||
                             '                      area     are, ' ||
                             '                      chain    chn  ' ||
                             '               where  chn.chain = are.chain ' ||
                             '               and    are.area = rgn.area ' ||
                             '               and    rgn.region = dis.region ' ||
                             '               and    dis.district = str.district ';

   -- Set the where clause for the appropriate org level if i_org_id has a value
   if I_org_id is not null then
      if I_org_level = 'C' then
         L_org_lvl_where_clause := L_org_lvl_where_clause || ' and chn.chain = ' || I_org_id;
      end if;

      if I_org_level = 'A' then
         L_org_lvl_where_clause := L_org_lvl_where_clause || ' and are.area = ' || I_org_id;
      end if;

      if I_org_level = 'R' then
         L_org_lvl_where_clause := L_org_lvl_where_clause || ' and rgn.region = ' || I_org_id;
      end if;

      if I_org_level = 'D' then
         L_org_lvl_where_clause := L_org_lvl_where_clause || ' and dis.district = ' || I_org_id ;
      end if;
   end if;

   L_org_lvl_where_clause := L_org_lvl_where_clause || ') ';

   -- if store criteria exist, use dynamic SQL package to insert stores into the LOC_LIST_DETAIL table

   if L_st_where_clause is NOT NULL then
      L_st_insert_stmt := 'insert into loc_list_detail (loc_list, ' ||
                                                        'location, ' ||
                                                        'loc_type) ' ||
                                                'select distinct :L_loc_list'|| ', ' ||
                                                        'store, ' ||
                                                        '''S'' ' ||
                                                'from   store ' ||
                                                L_st_where_clause ||
                                                L_org_lvl_where_clause;

      L_st_cursor := DBMS_SQL.OPEN_CURSOR;
      L_mark := 1;
      DBMS_SQL.PARSE(L_st_cursor, L_st_insert_stmt, DBMS_SQL.V7);
	  DBMS_SQL.BIND_VARIABLE(L_st_cursor,'L_loc_list',I_loc_list);
	  
      L_mark := 0;
      L_rows_processed := DBMS_SQL.EXECUTE(L_st_cursor);

      DBMS_SQL.CLOSE_CURSOR(L_st_cursor);
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_LOCLIST_HEAD', 'LOC_LIST_HEAD', ' LOC_LIST: '||to_char(I_loc_list));
      open C_LOCK_LOCLIST_HEAD;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_LOCLIST_HEAD', 'LOC_LIST_HEAD', ' LOC_LIST: '||to_char(I_loc_list));
      close C_LOCK_LOCLIST_HEAD;
      update loc_list_head set last_rebuild_date = sysdate where loc_list = I_loc_list;
   end if;

   ---
   if BUILD_WH_CRITERIA(O_error_message,
                        L_wh_where_clause,
                        I_loc_list) = FALSE then
      return FALSE;
   end if;
   ---
   -- if wh criteria exist, use dynamic SQL package to insert warehouses into the LOC_LIST_DETAIL table
   if L_wh_where_clause is NOT NULL then
      L_wh_insert_stmt := 'insert into loc_list_detail (loc_list, ' ||
                                                        'location, ' ||
                                                        'loc_type) ' ||
                                                'select :L_loc_list' || ', ' ||
                                                        'wh, ' ||
                                                        '''W'' ' ||
                                                  'from wh ' || L_wh_where_clause;
      L_wh_cursor := DBMS_SQL.OPEN_CURSOR;
      L_mark := 2;
      DBMS_SQL.PARSE(L_wh_cursor, L_wh_insert_stmt, DBMS_SQL.V7);
      L_mark := 0;
	  DBMS_SQL.BIND_VARIABLE(L_wh_cursor,'L_loc_list',I_loc_list);
      L_rows_processed := DBMS_SQL.EXECUTE(L_wh_cursor);
      DBMS_SQL.CLOSE_CURSOR(L_wh_cursor);
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_LOCLIST_HEAD', 'LOC_LIST_HEAD', ' LOC_LIST: '||to_char(I_loc_list));
      open C_LOCK_LOCLIST_HEAD;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_LOCLIST_HEAD', 'LOC_LIST_HEAD', ' LOC_LIST: '||to_char(I_loc_list));
      close C_LOCK_LOCLIST_HEAD;
      update loc_list_head set last_rebuild_date = sysdate where loc_list = I_loc_list;
   end if;
   ---
   if SIT_SQL.SIT_EXISTS(O_error_message,
                         L_sit_exists,
                         NULL,
                         I_loc_list) = FALSE then
      return FALSE;
   end if;
   ---
   if L_sit_exists then
      if SIT_SQL.COPY_SIT_CONFLICT(O_error_message,
                                   NULL,
                                   I_loc_list) =  FALSE then
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_SIT_EXPLODE', 'SIT_EXPLODE', ' LOC_LIST: '||to_char(I_loc_list));
      open C_LOCK_SIT_EXPLODE;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_SIT_EXPLODE', 'SIT_EXPLODE', ' LOC_LIST: '||to_char(I_loc_list));
      close C_LOCK_SIT_EXPLODE;
      delete from sit_explode
            where loc_list = I_loc_list;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_SIT_INFO', 'SIT_HEAD', ' LOC_LIST: '||to_char(I_loc_list));
      for rec in C_SIT_INFO LOOP
         L_itemloc_link_id := rec.itemloc_link_id;
         L_itemlist        := rec.skulist;
         ---
         if SIT_SQL.EXPLODE_RECORDS(O_error_message,
                                    L_itemloc_link_id,
                                    L_itemlist,
                                    I_loc_list,
                                    'Y') = FALSE then
            return FALSE;
         end if;
      END LOOP;
      SQL_LIB.SET_MARK('CLOSE','C_SIT_INFO', 'SIT_HEAD', ' LOC_LIST: '||to_char(I_loc_list));
      ---
      if SIT_SQL.REBUILD_SIT_CONFLICT(O_error_message,
                                      NULL,
                                      I_loc_list) = FALSE then
         return FALSE;
      end if;
   end if; -- End of L_sit_exist
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_SIT_EXPLODE%ISOPEN then
         close C_LOCK_SIT_EXPLODE;
      elsif C_LOCK_LOCLIST_HEAD%ISOPEN then
         close C_LOCK_LOCLIST_HEAD;
      end if;
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_loc_list));
      return FALSE;
   when OTHERS then
      if L_mark = 1 then
         O_error_message := sql_lib.create_msg('INV_ST_CRIT_FAIL_RBLD',
                                               SQLERRM,
                                               SQLCODE);
      elsif L_mark = 2 then
         O_error_message := sql_lib.create_msg('INV_WH_CRIT_FAIL_RBLD',
                                               SQLERRM,
                                               SQLCODE);
      else
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               SQLCODE);
      end if;
      ---
      if DBMS_SQL.IS_OPEN(L_st_cursor) then
         DBMS_SQL.CLOSE_CURSOR(L_st_cursor);
      end if;
      ---
      if DBMS_SQL.IS_OPEN(L_wh_cursor) then
         DBMS_SQL.CLOSE_CURSOR(L_wh_cursor);
      end if;
      ---
      return FALSE;
END REBUILD_LIST;

-----------------------------------------------------------------------
FUNCTION BUILD_ST_CRITERIA_TEMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_where_clause    IN OUT   VARCHAR2,
                                I_loc_list        IN       LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program             VARCHAR2(64) := 'LOCLIST_BUILD_SQL.BUILD_ST_CRITERIA_TEMP';
   L_open_parenthesis    LOC_LIST_CRITERIA_TEMP.OPEN_PARENTHESIS%TYPE;
   L_element             LOC_LIST_CRITERIA_TEMP.ELEMENT%TYPE;
   L_comparison          LOC_LIST_CRITERIA_TEMP.COMPARISON%TYPE;
   L_value               LOC_LIST_CRITERIA_TEMP.VALUE%TYPE;
   L_close_parenthesis   LOC_LIST_CRITERIA_TEMP.CLOSE_PARENTHESIS%TYPE;
   L_logic_operation     LOC_LIST_CRITERIA_TEMP.LOGIC_OPERATION%TYPE;
   L_related_value       VARCHAR2(15);--LOC_LIST_CRITERIA_TEMP.RELATED_VALUE%TYPE;
   L_count_seq_no        LOC_LIST_CRITERIA_TEMP.SEQ_NO%TYPE;
   L_count               LOC_LIST_CRITERIA_TEMP.SEQ_NO%TYPE;
   ---
   L_first_time          BOOLEAN := TRUE;
   L_field_name          VARCHAR2(100);
   L_field               VARCHAR2(50);
   L_comp_op             VARCHAR2(10);
   L_val                 VARCHAR2(200);
   L_temp                VARCHAR2(1000);
   L_last_eow_date       SYSTEM_VARIABLES.LAST_EOW_DATE%TYPE;
   L_NULL                VARCHAR2(4) := 'NULL'; 
   ---
   cursor C_ST_CRITERIA is
      select open_parenthesis,
             element,
             comparison,
             value,
             close_parenthesis,
             logic_operation,
             related_value
        from loc_list_criteria_temp
       where loc_list = I_loc_list
         and loc_type = 'S'
       order by seq_no;

   cursor C_LAST_EOW_DATE is
      select to_char(last_eow_date, 'DD-MON-YYYY')
        from system_variables;

   cursor C_COUNT is
      select count(*)
        from loc_list_criteria_temp
       where loc_list = I_loc_list
	     and loc_type = 'S';

BEGIN

   open C_COUNT;
   fetch C_COUNT into L_count_seq_no;
   close C_COUNT;
   L_count := 0;   
   open C_ST_CRITERIA;
   LOOP
      fetch C_ST_CRITERIA into L_open_parenthesis,
                               L_element,
                               L_comparison,
                               L_value,
                               L_close_parenthesis,
                               L_logic_operation,
                               L_related_value;
      if C_ST_CRITERIA%NOTFOUND then
         Exit;
      end if;
      ---
      if L_related_value is NULL then
         L_related_value := L_NULL;
      end if;
      ---
      if L_first_time = TRUE then
         O_where_clause := 'where (';
         L_first_time   := FALSE;
      end if;
      ---
      if L_comparison    = 'EQ' then
         L_comp_op := '= ';
      elsif L_comparison = 'NE' then
         L_comp_op := '!= ';
      elsif L_comparison = 'GT' then
         L_comp_op := '> ';
      elsif L_comparison = 'LT' then
         L_comp_op := '< ';
      elsif L_comparison = 'GE' then
         L_comp_op := '>= ';
      elsif L_comparison = 'LE' then
         L_comp_op := '<= ';
      elsif L_comparison is NULL then
         L_comp_op := NULL;
      else
         O_error_message := sql_lib.create_msg('INV_COMP_OP',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      ---
      if L_element    = 'SN' then  -- Store Number
         L_field_name := 'store ';
         L_val        := RTRIM(L_value);
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'ST' then  -- State
         L_field_name := 'NVL(state, -999) ';
         L_val := '''' || RTRIM(L_value) || '''';
         L_temp := 'exists (select ''x'' ' ||
                             'from addr ' ||
                            'where store.store = addr.key_value_1 ' ||
                              'and module in (''ST'',''WFST'') '||
                              'and ' || L_field_name || L_comp_op || L_val || ')';
      elsif L_element = 'CTY' then  -- Country ID
         L_field_name := 'country_id ';
         L_val := '''' || RTRIM(L_value) || '''';
         L_temp := 'exists (select ''x'' ' ||
                             'from addr ' ||
                            'where store.store = addr.key_value_1 ' ||
                              'and module in (''ST'',''WFST'') '||
                              'and ' || L_field_name || L_comp_op || L_val || ')';
      elsif L_element = 'WFC' then  -- WF Customer ID
         L_field_name := 'wf_customer_id ';
         L_val := RTRIM(L_value);
         L_temp := L_field_name || L_comp_op || L_val;
      elsif L_element = 'ZC' then  -- Zip Code
         L_field_name := 'post ';
         L_val := '''' || RTRIM(L_value) || '''';
         L_temp := 'exists (select ''x'' ' ||
                             'from addr ' ||
                            'where store.store = addr.key_value_1 ' ||
                              'and module in (''ST'',''WFST'') '||
                              'and ' || L_field_name || L_comp_op || L_val || ')';
      elsif L_element = 'VR' then  -- VAT Region
         L_field_name := 'vat_region ';
         L_val        := RTRIM(L_value);
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'CUR' then  -- Currency
         L_field_name := 'currency_code ';
         L_val        := '''' || RTRIM(L_value) || '''';
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'DT' then  -- District
         L_field_name := 'district ';
         L_val        := RTRIM(L_value);
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'DW' then  -- Default WH
         L_field_name := 'default_wh ';
         L_val        := RTRIM(L_value);
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'PM' then  -- Promo Zone
         L_field_name := 'promo_zone ';
         L_val        := RTRIM(L_value);
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'TSF' then  -- Transfer Zone
         L_field_name := 'transfer_zone ';
         L_val        := RTRIM(L_value);
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'LNG' then  -- Language
         L_field_name := 'lang ';
         L_val        := RTRIM(L_value);
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'SC' then  -- Store Class
         L_field_name := 'store_class ';
         L_val        := '''' || RTRIM(L_value) || '''';
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'SF' then  -- Store Format
         L_field_name := 'store_format ';
         L_val        := RTRIM(L_value);
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'TA' then  -- Total Area
         L_field_name := 'total_square_ft ';
         L_val        := RTRIM(L_value);
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'SA' then  -- Selling Area
         L_field_name := 'selling_square_ft ';
         L_val        := RTRIM(L_value);
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'OD' then  -- Store Open Date
         L_field_name := 'store_open_date ';
         L_val        := 'to_date(''' || RTRIM(L_value) || ''', ''DD-MM-YYYY'')';
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'AD' then  -- Store Acquired Date
         L_field_name := 'NVL(acquired_date,  to_date(''01-01-1951'', ''DD-MM-YYYY'')) ';
         L_val        := 'to_date(''' || RTRIM(L_value) || ''', ''DD-MM-YYYY'')';
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'RD' then -- Store Remodel Date
         L_field_name := 'NVL(remodel_date,  to_date(''01-01-1951'', ''DD-MM-YYYY'')) ';
         L_val        := 'to_date(''' || RTRIM(L_value) || ''', ''DD-MM-YYYY'')';
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element = 'SG' then  -- Store Grade
         L_field_name := 'store_grade ';
         L_field      := 'store_grade_group_id';
         L_val        := '''' || RTRIM(L_value) || '''';
         L_temp       := 'exists (select ''x'' ' ||
                                   'from store_grade_store ' ||
                                  'where store.store = store_grade_store.store ' ||
                                    'and ' || L_field_name || L_comp_op || L_val ||
                                   ' and store_grade_group_id = nvl('||RTRIM(L_related_value)||', '||L_field||')'|| ')';
      elsif L_element = 'CZ' then  -- Cost Zone
         L_field_name := 'zone_id ';
         L_field      := 'zone_group_id';
         L_val        := RTRIM(L_value);
         L_temp       := 'exists (select ''x'' ' ||
                                   'from cost_zone_group_loc ' ||
                                  'where store.store = cost_zone_group_loc.location ' ||
                                    'and loc_type = ''S'' ' ||
                                    'and ' || L_field_name || L_comp_op || L_val ||
                                   ' and zone_group_id = nvl('||RTRIM(L_related_value)||' , '||L_field||')'|| ')';
      elsif L_element = 'LT' then  -- Location Traits
         L_field_name := 'loc_trait ';
         L_val        := RTRIM(L_value);
         L_temp       := 'exists (select ''x'' ' ||
                                   'from loc_traits_matrix ' ||
                                  'where store.store = loc_traits_matrix.store ' ||
                                    'and ' || L_field_name || L_comp_op || L_val || ')';
      elsif L_element = 'SL' then  -- Store Sales Level for Last Closed Week
         if L_last_eow_date is NULL then
            open C_LAST_EOW_DATE;
            fetch C_LAST_EOW_DATE into L_last_eow_date;
            if C_LAST_EOW_DATE%NOTFOUND then
               O_error_message := sql_lib.create_msg('NO_LAST_EOW_DATE', null, null, null);
               return FALSE;
            end if;
         end if;
         L_temp := 'exists (select ''x'' ' ||
                             'from week_data ' ||
                            'where store.store = week_data.location ' ||                            
                            'group by store ' ||
                           'having sum(net_sales_retail) ' || L_comp_op || L_value || ' ' ||
                              'and max(eow_date) < ''' || L_last_eow_date || ''')';
      elsif L_element = 'FL' then  -- Store Forecast Level for Last Closed Week
         if L_last_eow_date is NULL then
            open C_LAST_EOW_DATE;
            fetch C_LAST_EOW_DATE into L_last_eow_date;
            if C_LAST_EOW_DATE%NOTFOUND then
               O_error_message := sql_lib.create_msg('NO_LAST_EOW_DATE', null, null, null);
               return FALSE;
            end if;
         end if;
         L_temp := 'exists (select ''x'' ' ||
                             'from item_forecast ' ||
                            'where store.store = item_forecast.loc ' ||
                            'group by loc ' ||
                           'having sum(forecast_sales) ' || L_comp_op || L_value || ' ' ||
                              'and max(eow_date) < ''' || L_last_eow_date || ''')';
      elsif L_element = 'TE' then  -- Transfer Entity ID
         L_field_name := 'tsf_entity_id ';
         L_val        := RTRIM(L_value);
         L_temp       := L_field_name || L_comp_op || L_val;
      elsif L_element is NULL then
         L_field_name := NULL;
         L_val        := NULL;
         L_temp       := NULL;
      else
         O_error_message := sql_lib.create_msg('INV_ST_CRIT', null, null, null);
         return FALSE;
      end if;
      ---
      if LENGTH(O_where_clause) + LENGTH(L_temp) + 10 > 32600 then
         O_error_message := sql_lib.create_msg('CRIT_QRY_TOO_LONG');
         return FALSE;
      end if;
	  ---
	  L_count := L_count+1;
      if ((L_count = L_count_seq_no) and (L_logic_operation is not NULL)) then 
        L_logic_operation := NULL;
      end if;
	  ---
      O_where_clause := O_where_clause || ' ' ||
                        L_open_parenthesis ||
                        L_temp ||
                        L_close_parenthesis || ' ' ||
                        L_logic_operation;
   END LOOP;

   if O_where_clause is NOT NULL then
      O_where_clause := O_where_clause || ')'; 
   end if;

   close C_ST_CRITERIA;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END BUILD_ST_CRITERIA_TEMP;

-----------------------------------------------------------------------
FUNCTION TEST_ST_CRITERIA_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_query_stmt    IN OUT VARCHAR2,
                               O_valid         IN OUT BOOLEAN,
                               I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'LOCLIST_BUILD_SQL.TEST_ST_CRITERIA_TEMP';
   L_where_clause      VARCHAR2(32600);
   L_st_cursor         INTEGER;
   L_row_processed     INTEGER;
   ---
   L_mark              NUMBER(1) := 0;  -- to locate PARSE error
BEGIN
   if BUILD_ST_CRITERIA_TEMP(O_error_message,
                             L_where_clause,
                             I_loc_list) = FALSE then
      O_query_stmt := NULL;
      O_valid := FALSE;
      return FALSE;
   end if;
   ---
   if L_where_clause is NULL then
      O_error_message := sql_lib.create_msg('NO_ST_CRIT_FOR_TEST', null, null, null);
      O_query_stmt := NULL;
      O_valid := TRUE;
   end if;
   ---
   O_query_stmt := 'select store from store ' || L_where_clause;
   L_st_cursor := DBMS_SQL.OPEN_CURSOR;
   L_mark := 1;
   DBMS_SQL.PARSE(L_st_cursor, O_query_stmt, DBMS_SQL.V7);
   L_mark := 0;
   L_row_processed := DBMS_SQL.EXECUTE(L_st_cursor);
   DBMS_SQL.CLOSE_CURSOR(L_st_cursor);
   ---
   O_valid := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then
      if L_mark = 1 then
         O_error_message := sql_lib.create_msg('FIX_CRITERIA',
                                               SQLERRM,
                                               SQLCODE);
      else  -- L_mark = 0
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               SQLCODE);
      end if;
      ---
      if DBMS_SQL.IS_OPEN(L_st_cursor) then
         DBMS_SQL.CLOSE_CURSOR(L_st_cursor);
      end if;
      ---
      O_valid := FALSE;
      return FALSE;
END TEST_ST_CRITERIA_TEMP;

-----------------------------------------------------------------------
FUNCTION BUILD_WH_CRITERIA_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_where_clause  IN OUT VARCHAR2,
                                I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'LOCLIST_BUILD_SQL.BUILD_WH_CRITERIA_TEMP';
   L_open_parenthesis  LOC_LIST_CRITERIA_TEMP.OPEN_PARENTHESIS%TYPE;
   L_element           LOC_LIST_CRITERIA_TEMP.ELEMENT%TYPE;
   L_comparison        LOC_LIST_CRITERIA_TEMP.COMPARISON%TYPE;
   L_value             LOC_LIST_CRITERIA_TEMP.VALUE%TYPE;
   L_close_parenthesis LOC_LIST_CRITERIA_TEMP.CLOSE_PARENTHESIS%TYPE;
   L_logic_operation   LOC_LIST_CRITERIA_TEMP.LOGIC_OPERATION%TYPE;
   L_pwh               VARCHAR2(1) := 'N';
   L_count_wh_seq_no   LOC_LIST_CRITERIA_TEMP.SEQ_NO%TYPE;
   L_wh_count          LOC_LIST_CRITERIA_TEMP.SEQ_NO%TYPE;
   ---
   L_first_time        BOOLEAN := TRUE;
   L_field_name        VARCHAR2(40);
   L_comp_op           VARCHAR2(10);
   L_val               VARCHAR2(200);
   L_temp              VARCHAR2(32000);
   ---
   cursor C_WH_CRITERIA is
      select open_parenthesis,
             element,
             comparison,
             value,
             close_parenthesis,
             logic_operation
        from loc_list_criteria_temp
       where loc_list = I_loc_list
         and loc_type = 'W'
       order by seq_no;
   ---
   cursor C_WH_COUNT is
      select count(*)
        from loc_list_criteria_temp
       where loc_list = I_loc_list
       and loc_type = 'W';

BEGIN
   open C_WH_COUNT;
   fetch C_WH_COUNT into L_count_wh_seq_no;
   close C_WH_COUNT;
   L_wh_count := 0;
   open C_WH_CRITERIA;
   LOOP
      fetch C_WH_CRITERIA into L_open_parenthesis,
                               L_element,
                               L_comparison,
                               L_value,
                               L_close_parenthesis,
                               L_logic_operation;
      if C_WH_CRITERIA%NOTFOUND then
         Exit;
      end if;
      ---
      if L_first_time = TRUE then
         O_where_clause := 'where';
         L_first_time := FALSE;
      end if;
      ---
      if L_comparison = 'EQ' then
         L_comp_op := '= ';
      elsif L_comparison = 'NE' then
         L_comp_op := '!= ';
      elsif L_comparison = 'GT' then
         L_comp_op := '> ';
      elsif L_comparison = 'LT' then
         L_comp_op := '< ';
      elsif L_comparison = 'GE' then
         L_comp_op := '>= ';
      elsif L_comparison = 'LE' then
         L_comp_op := '<= ';
      elsif L_comparison is NULL then
         L_comp_op := NULL;
      else
         O_error_message := sql_lib.create_msg('INV_COMP_OP', null, null, null);
         return FALSE;
      end if;
      ---
      if L_element = 'PWN' then --Physical Warehouse Number
         L_field_name := 'NVL(physical_wh, -999) ';
         L_val := RTRIM(L_value);
         L_temp := L_field_name ||L_comp_op ||L_val ;
         if L_pwh != 'Y' then
            L_pwh := 'Y';
         end if;
      elsif L_element = 'WN' then  -- Warehouse Number
         L_field_name := 'wh ';
         L_val := RTRIM(L_value);
         L_temp := L_field_name ||L_comp_op ||L_val ;
      elsif L_element = 'ST' then  -- State
         L_field_name := 'NVL(state, -999) ';
         L_val := '''' || RTRIM(L_value) || '''';
         L_temp := 'exists (select ''x'' ' ||
                             'from addr ' ||
                            'where wh.wh = addr.key_value_1 ' ||
                              'and module = ''WH'' '||
                              'and ' || L_field_name || L_comp_op || L_val || ')';
         if L_pwh != 'Y' then
            L_pwh := 'Y';
         end if;
      elsif L_element = 'CTY' then  -- Country ID
         L_field_name := 'country_id ';
         L_val := '''' || RTRIM(L_value) || '''';
         L_temp := 'exists (select ''x'' ' ||
                             'from addr ' ||
                            'where wh.wh = addr.key_value_1 ' ||
                              'and module = ''WH'' '||
                              'and ' || L_field_name || L_comp_op || L_val || ')';
         if L_pwh != 'Y' then
            L_pwh := 'Y';
         end if;
      elsif L_element = 'ZC' then  -- Zip Code
         L_field_name := 'post ';
         L_val := '''' || RTRIM(L_value) || '''';
         L_temp := 'exists (select ''x'' ' ||
                             'from addr ' ||
                            'where wh.wh = addr.key_value_1 ' ||
                              'and module = ''WH'' '||
                              'and ' || L_field_name || L_comp_op || L_val || ')';
         if L_pwh != 'Y' then
     L_pwh := 'Y';
         end if;
      elsif L_element = 'VR' then  -- VAT Region
         L_field_name := 'vat_region ';
         L_val := RTRIM(L_value);
         L_temp := L_field_name ||L_comp_op ||L_val ;
      elsif L_element = 'CUR' then  -- Currency
         L_field_name := 'currency_code ';
         L_val := '''' || RTRIM(L_value) || '''';
         L_temp := L_field_name ||L_comp_op ||L_val ;
      elsif L_element = 'TE' then  -- Transfer Entity ID
         L_field_name := 'tsf_entity_id ';
         L_val := RTRIM(L_value);
         L_temp := L_field_name ||L_comp_op ||L_val ;
      elsif L_element is NULL then
         L_field_name := NULL;
         L_val := NULL;
         L_temp := NULL;
      else
         O_error_message := sql_lib.create_msg('INV_WH_CRIT', null, null, null);
         return FALSE;
      end if;
      ---      
      if (LENGTH(O_where_clause) +
          LENGTH(L_temp) + 10) > 32600 then
          O_error_message := sql_lib.create_msg('CRIT_QRY_TOO_LONG');
         return FALSE;
      end if;
	  ---
      L_wh_count := L_wh_count+1;
      if ( (L_wh_count = L_count_wh_seq_no) and (L_logic_operation is not NULL) ) then 
        L_logic_operation := NULL;
      end if;
	  ---
      O_where_clause := O_where_clause || ' ' ||
                        L_open_parenthesis ||
                        L_temp ||
                        L_close_parenthesis || ' ' ||
                        L_logic_operation;
   END LOOP;
   close C_WH_CRITERIA;
   ---
   if L_pwh= 'Y' then
      O_where_clause := O_where_clause || ' AND stockholding_ind = ''Y''';
   end if;
   if O_where_clause is not null then
      O_where_clause := O_where_clause || ' AND finisher_ind = ''N''' ;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END BUILD_WH_CRITERIA_TEMP;
-----------------------------------------------------------------------
FUNCTION TEST_WH_CRITERIA_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_query_stmt    IN OUT VARCHAR2,
                               O_valid         IN OUT BOOLEAN,
                               I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'LOCLIST_BUILD_SQL.TEST_WH_CRITERIA_TEMP';
   L_where_clause      VARCHAR2(32600);
   L_wh_cursor         INTEGER;
   L_row_processed     INTEGER;
   ---
   L_mark              NUMBER(1) := 0;  -- to locate PARSE error
BEGIN
   if BUILD_WH_CRITERIA_TEMP(O_error_message,
                             L_where_clause,
                             I_loc_list) = FALSE then
      O_query_stmt := NULL;
      O_valid := FALSE;
      return FALSE;
   end if;
   ---
   if L_where_clause is NULL then
      O_error_message := sql_lib.create_msg('NO_WH_CRIT_FOR_TEST', null, null, null);
      O_query_stmt := NULL;
      O_valid := TRUE;
      return TRUE;
   end if;
   ---
   O_query_stmt := 'select wh from wh ' || L_where_clause;
   L_wh_cursor := DBMS_SQL.OPEN_CURSOR;
   L_mark := 1;
   DBMS_SQL.PARSE(L_wh_cursor, O_query_stmt, DBMS_SQL.V7);
   L_mark := 2;
   L_row_processed := DBMS_SQL.EXECUTE(L_wh_cursor);
   DBMS_SQL.CLOSE_CURSOR(L_wh_cursor);
   ---
   O_valid := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then
      if L_mark = 1 then
         O_error_message := sql_lib.create_msg('FIX_CRITERIA',
                                               SQLERRM,
                                               SQLCODE);
      else -- L_mark = 0
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               SQLCODE);
      end if;
      ---
      if DBMS_SQL.IS_OPEN(L_wh_cursor) then
         DBMS_SQL.CLOSE_CURSOR(L_wh_cursor);
      end if;
      ---
      O_valid := FALSE;
      return FALSE;
END TEST_WH_CRITERIA_TEMP;

-----------------------------------------------------------------------
FUNCTION POPULATE_CRITERIA_TEMP(O_error_message IN OUT VARCHAR2,
                                I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'LOCLIST_BUILD_SQL.POPULATE_CRITERIA_TEMP';

BEGIN
   if DELETE_CRITERIA_TEMP(O_error_message,
                           I_loc_list) = FALSE then
      return FALSE;
   end if;
   ---
   insert into loc_list_criteria_temp(loc_list,
                                      loc_type,
                                      seq_no,
                                      open_parenthesis,
                                      element,
                                      comparison,
                                      value,
                                      close_parenthesis,
                                      logic_operation,
                                      related_value)
                               select loc_list,
                                      loc_type,
                                      seq_no,
                                      open_parenthesis,
                                      element,
                                      comparison,
                                      value,
                                      close_parenthesis,
                                      logic_operation,
                                      related_value
                                 from loc_list_criteria
                                where loc_list = I_loc_list;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END POPULATE_CRITERIA_TEMP;

-----------------------------------------------------------------------
FUNCTION POPULATE_CRITERIA(O_error_message IN OUT VARCHAR2,
                           I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program    VARCHAR2(64) := 'LOCLIST_BUILD_SQL.POPULATE_CRITERIA';
   L_st_cnt     LOC_LIST_CRITERIA.SEQ_NO%TYPE;
   L_wh_cnt     LOC_LIST_CRITERIA.SEQ_NO%TYPE;
   L_st_count   LOC_LIST_CRITERIA.SEQ_NO%TYPE;
   L_wh_count   LOC_LIST_CRITERIA.SEQ_NO%TYPE;
   ---
   cursor C_ST_CRITERIA is
      select loc_list,
             loc_type,
             open_parenthesis,
             element,
             comparison,
             value,
             close_parenthesis,
             logic_operation,
             related_value
        from loc_list_criteria_temp
       where loc_list = I_loc_list
         and loc_type = 'S'
       order by seq_no;
   ---
   cursor C_ST_COUNT is
      select count(*)
        from loc_list_criteria_temp
       where loc_list = I_loc_list
        and loc_type = 'S';
   ---
   cursor C_WH_CRITERIA is
      select loc_list,
             loc_type,
             open_parenthesis,
             element,
             comparison,
             value,
             close_parenthesis,
             logic_operation
        from loc_list_criteria_temp
       where loc_list = I_loc_list
         and loc_type = 'W'
       order by seq_no;
   ---
   cursor C_WH_COUNT is
      select count(*)
        from loc_list_criteria_temp
       where loc_list = I_loc_list
        and loc_type = 'W';

BEGIN
   open C_ST_COUNT;
   fetch C_ST_COUNT into L_st_count;
   close C_ST_COUNT;
   open C_WH_COUNT;
   fetch C_WH_COUNT into L_wh_count;
   close C_WH_COUNT;
   if LOCK_CRITERIA(O_error_message,
                    I_loc_list) = FALSE then
      return FALSE;
   end if;
   if DELETE_CRITERIA(O_error_message,
                      I_loc_list) = FALSE then
      return FALSE;
   end if;
   ---
   L_st_cnt := 1;
   for rec in C_ST_CRITERIA loop
   	  if L_st_cnt = L_st_count and rec.logic_operation is not NULL then
          rec.logic_operation := NULL;
      end if;  
      insert into loc_list_criteria(loc_list,
                                    loc_type,
                                    seq_no,
                                    open_parenthesis,
                                    element,
                                    comparison,
                                    value,
                                    close_parenthesis,
                                    logic_operation,
                                    related_value)
                             values(rec.loc_list,
                                    rec.loc_type,
                                    L_st_cnt,
                                    rec.open_parenthesis,
                                    rec.element,
                                    rec.comparison,
                                    rec.value,
                                    rec.close_parenthesis,
                                    rec.logic_operation,
                                    rec.related_value);
      L_st_cnt := L_st_cnt+1;
   end loop;
   ---
   L_wh_cnt := 1;
   for rec in C_WH_CRITERIA loop
      if L_wh_cnt = L_wh_count and rec.logic_operation is not NULL then
          rec.logic_operation := NULL;
      end if; 
      insert into loc_list_criteria(loc_list,
                                    loc_type,
                                    seq_no,
                                    open_parenthesis,
                                    element,
                                    comparison,
                                    value,
                                    close_parenthesis,
                                    logic_operation)
                             values(rec.loc_list,
                                    rec.loc_type,
                                    L_wh_cnt,
                                    rec.open_parenthesis,
                                    rec.element,
                                    rec.comparison,
                                    rec.value,
                                    rec.close_parenthesis,
                                    rec.logic_operation);
      L_wh_cnt := L_wh_cnt+1;
   end loop;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END POPULATE_CRITERIA;

------------------------------------------------------------------------------
FUNCTION COPY_CRITERIA(O_error_message        IN OUT VARCHAR2,
                       I_loc_list_existing    IN     LOC_LIST_HEAD.LOC_LIST%TYPE,
                       I_loc_list_new         IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'LOCLIST_BUILD_SQL.COPY_CRITERIA';

BEGIN
   insert into loc_list_criteria(loc_list,
                                 loc_type,
                                 seq_no,
                                 open_parenthesis,
                                 element,
                                 comparison,
                                 value,
                                 close_parenthesis,
                                 logic_operation)
                          select I_loc_list_new,
                                 loc_type,
                                 seq_no,
                                 open_parenthesis,
                                 element,
                                 comparison,
                                 value,
                                 close_parenthesis,
                                 logic_operation
                            from loc_list_criteria
                           where loc_list = I_loc_list_existing;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END COPY_CRITERIA;

------------------------------------------------------------------------------
FUNCTION ADD_ST_CRITERIA(O_error_message IN OUT VARCHAR2,
                         I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'LOCLIST_BUILD_SQL.ADD_ST_CRITERIA';
   ---
   L_st_crit_exist VARCHAR2(1) := 'N';
   L_delete_total  NUMBER(6);
   L_add_total     NUMBER(6);
   L_next_seq_no   LOC_LIST_CRITERIA_TEMP.SEQ_NO%TYPE;
   L_delete_cnt    NUMBER(6);
   L_add_cnt       NUMBER(6);
   ---
   L_table         VARCHAR2(20) := 'LOC_LIST_CRITERIA';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_ST_CRIT_EXIST is
      select 'Y'
        from loc_list_criteria
       where loc_list = I_loc_list
         and loc_type = 'S';

   cursor C_DELETE_TOTAL is
      select count(*)
        from loc_list_detail
       where loc_list = I_loc_list
         and loc_type = 'S'
         and action_type = 'D';

   cursor C_ADD_TOTAL is
      select count(*)
        from loc_list_detail
       where loc_list = I_loc_list
         and loc_type = 'S'
         and action_type = 'A';

   cursor C_NEXT_SEQ_NO is
      select max(seq_no)+1
        from loc_list_criteria_temp
       where loc_list = I_loc_list
         and loc_type = 'S';

   cursor C_DELETE_STORE is
      select location
        from loc_list_detail
       where loc_list = I_loc_list
         and loc_type = 'S'
         and action_type = 'D';

   cursor C_ADD_STORE is
      select location
        from loc_list_detail
       where loc_list = I_loc_list
         and loc_type = 'S'
         and action_type = 'A';

   cursor C_LOCK_ST_CRITERIA is
      select 'x'
        from loc_list_criteria
       where loc_list = I_loc_list
         and loc_type = 'S'
         for update nowait;

BEGIN
   -- check if any store criteria already exist for the location list,
   -- if not, do not add '(...) AND/OR'
   open C_ST_CRIT_EXIST;
   fetch C_ST_CRIT_EXIST into L_st_crit_exist;
   close C_ST_CRIT_EXIST;

   -- check if there are any stores to be added or deleted; if not, quit processing
   open C_DELETE_TOTAL;
   fetch C_DELETE_TOTAL into L_delete_total;
   close C_DELETE_TOTAL;
   ---
   open C_ADD_TOTAL;
   fetch C_ADD_TOTAL into L_add_total;
   close C_ADD_TOTAL;
   ---
   if L_delete_total = 0 and L_add_total = 0 then
      return TRUE;
   end if;

   -- if one or more store criteria already exists,
   -- enclose the existing criteria with '( exiting criteria ) AND/OR'
   if L_st_crit_exist = 'Y' then
      -- insert an open parenthesis with seq_no = 1
      insert into loc_list_criteria_temp(loc_list,
                                         loc_type,
                                         seq_no,
                                         open_parenthesis)
                                  values(I_loc_list,
                                         'S',
                                         1,
                                         '(');

      -- copy store criteria from LOC_LIST_CRITERIA to LOC_LIST_CRITERIA_TEMP
      insert into loc_list_criteria_temp(loc_list,
                                         loc_type,
                                         seq_no,
                                         open_parenthesis,
                                         element,
                                         comparison,
                                         value,
                                         close_parenthesis,
                                         logic_operation)
                                  select loc_list,
                                         loc_type,
                                         seq_no+1,
                                         open_parenthesis,
                                         element,
                                         comparison,
                                         value,
                                         close_parenthesis,
                                         logic_operation
                                    from loc_list_criteria
                                   where loc_list = I_loc_list
                                     and loc_type = 'S';

      -- insert a close parenthesis with possible AND/OR logic to LOC_LIST_CRITERIA_TEMP
      open C_NEXT_SEQ_NO;
      fetch C_NEXT_SEQ_NO into L_next_seq_no;
      close C_NEXT_SEQ_NO;
      ---
      if L_delete_total != 0 then
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            close_parenthesis,
                                            logic_operation)
                                     values(I_loc_list,
                                            'S',
                                            L_next_seq_no,
                                            ')',
                                            'AND');
      elsif L_add_total != 0 then
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            close_parenthesis,
                                            logic_operation)
                                     values(I_loc_list,
                                            'S',
                                            L_next_seq_no,
                                            ')',
                                           'OR');
      end if;
      L_next_seq_no := L_next_seq_no+1;  -- next available seq_no
   else
      L_next_seq_no := 1;
   end if;

   -- add all stores marked as 'D' on LOC_LIST_DETAIL to LOC_LIST_CRITERIA_TEMP
   -- insert a criteria record of 'AND store != X'; AND should go with the previous record
   ---
   L_delete_cnt := 0;                 -- total number of stores deleted so far
   for rec in C_DELETE_STORE loop
      if L_delete_cnt+1 < L_delete_total then
         -- at least one more delete record after this one,
         -- insert an AND for connecting the next delete
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            element,
                                            comparison,
                                            value,
                                            logic_operation)
                                     values(I_loc_list,
                                            'S',
                                            L_next_seq_no,
                                            'SN',
                                            'NE',
                                            rec.location,
                                            'AND');
      elsif L_add_total != 0 then
         -- no more delete store, but at least one store to be added,
         -- insert an OR for connecting the next store add
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            element,
                                            comparison,
                                            value,
                                            logic_operation)
                                     values(I_loc_list,
                                            'S',
                                            L_next_seq_no,
                                            'SN',
                                            'NE',
                                            rec.location,
                                            'OR');
      else
         -- no more delete store after this one and no add store
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            element,
                                            comparison,
                                            value)
                                     values(I_loc_list,
                                            'S',
                                            L_next_seq_no,
                                            'SN',
                                            'NE',
                                            rec.location);
      end if;
      ---
      L_delete_cnt := L_delete_cnt+1;
      L_next_seq_no := L_next_seq_no+1;
   end loop;

   -- add all stores marked as 'A' on LOC_LIST_DETAIL to LOC_LIST_CRITERIA_TEMP
   -- insert a criteria record of 'OR store = X'; OR should go with the previous record
   ---
   L_add_cnt := 0;   -- total number of stores added so far
   for rec in C_ADD_STORE loop
      if L_add_cnt+1 < L_add_total then
         -- at least one more add record after this one
         -- insert an OR for connecting the next add
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            element,
                                            comparison,
                                            value,
                                            logic_operation)
                                     values(I_loc_list,
                                            'S',
                                            L_next_seq_no,
                                            'SN',
                                            'EQ',
                                            rec.location,
                                            'OR');
      else
         -- no more stores to add after this one
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            element,
                                            comparison,
                                            value)
                                     values(I_loc_list,
                                            'S',
                                            L_next_seq_no,
                                            'SN',
                                            'EQ',
                                            rec.location);
      end if;
      ---
      L_add_cnt := L_add_cnt+1;
      L_next_seq_no := L_next_seq_no +1;
   end loop;

   -- lock and delete all records from LOC_LIST_CRITERIA for the location list/store
   open C_LOCK_ST_CRITERIA;
   close C_LOCK_ST_CRITERIA;
   delete from loc_list_criteria
      where loc_list = I_loc_list
        and loc_type = 'S';

   -- copy all records from LOC_LIST_CRITERIA_TEMP to the LOC_LIST_CRITERIA for the location list/store
   insert into loc_list_criteria(loc_list,
                                 loc_type,
                                 seq_no,
                                 open_parenthesis,
                                 element,
                                 comparison,
                                 value,
                                 close_parenthesis,
                                 logic_operation)
                          select loc_list,
                                 loc_type,
                                 seq_no,
                                 open_parenthesis,
                                 element,
                                 comparison,
                                 value,
                                 close_parenthesis,
                                 logic_operation
                            from loc_list_criteria_temp
                           where loc_list = I_loc_list
                             and loc_type = 'S';
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_ST_CRITERIA%ISOPEN then
         close C_LOCK_ST_CRITERIA;
      end if;
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_loc_list));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END ADD_ST_CRITERIA;

------------------------------------------------------------------------------
FUNCTION ADD_WH_CRITERIA(O_error_message IN OUT VARCHAR2,
                         I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'LOCLIST_BUILD_SQL.ADD_WH_CRITERIA';
   ---
   L_wh_crit_exist VARCHAR2(1) := 'N';
   L_delete_total  NUMBER(6);
   L_add_total     NUMBER(6);
   L_next_seq_no   LOC_LIST_CRITERIA_TEMP.SEQ_NO%TYPE;
   L_delete_cnt    NUMBER(6);
   L_add_cnt       NUMBER(6);
   ---
   L_table         VARCHAR2(20) := 'LOC_LIST_CRITERIA';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_WH_CRIT_EXIST is
      select 'Y'
        from loc_list_criteria
       where loc_list = I_loc_list
         and loc_type = 'W';

   cursor C_DELETE_TOTAL is
      select count(*)
        from loc_list_detail
       where loc_list = I_loc_list
         and loc_type = 'W'
         and action_type = 'D';

   cursor C_ADD_TOTAL is
      select count(*)
        from loc_list_detail
       where loc_list = I_loc_list
         and loc_type = 'W'
         and action_type = 'A';

   cursor C_NEXT_SEQ_NO is
      select max(seq_no)+1
        from loc_list_criteria_temp
       where loc_list = I_loc_list
         and loc_type = 'W';

   cursor C_DELETE_WH is
      select location
        from loc_list_detail
       where loc_list = I_loc_list
         and loc_type = 'W'
         and action_type = 'D';

   cursor C_ADD_WH is
      select location
        from loc_list_detail
       where loc_list = I_loc_list
         and loc_type = 'W'
         and action_type = 'A';

   cursor C_LOCK_WH_CRITERIA is
      select 'x'
        from loc_list_criteria
       where loc_list = I_loc_list
         and loc_type = 'W'
         for update nowait;

BEGIN
   -- check if any wh criteria already exists for the location list
   -- if not, do not add '(...) AND/OR'
   open C_WH_CRIT_EXIST;
   fetch C_WH_CRIT_EXIST into L_wh_crit_exist;
   close C_WH_CRIT_EXIST;

   -- check if there are any warehouses to be added or deleted; if not, quit processing
   open C_DELETE_TOTAL;
   fetch C_DELETE_TOTAL into L_delete_total;
   close C_DELETE_TOTAL;
   ---
   open C_ADD_TOTAL;
   fetch C_ADD_TOTAL into L_add_total;
   close C_ADD_TOTAL;
   ---
   if L_delete_total = 0 and L_add_total = 0 then
      return TRUE;
   end if;

   -- if one or more wh criteria already exists,
   -- enclose the existing criteria with '( exiting criteria ) AND/OR'
   if L_wh_crit_exist = 'Y' then
      -- insert an open parenthesis with seq_no = 1
      insert into loc_list_criteria_temp(loc_list,
                                         loc_type,
                                         seq_no,
                                         open_parenthesis)
                                  values(I_loc_list,
                                         'W',
                                         1,
                                         '(');

      -- copy warehouse criteria from LOC_LIST_CRITERIA to LOC_LIST_CRITERIA_TEMP
      insert into loc_list_criteria_temp(loc_list,
                                         loc_type,
                                         seq_no,
                                         open_parenthesis,
                                         element,
                                         comparison,
                                         value,
                                         close_parenthesis,
                                         logic_operation)
                                  select loc_list,
                                         loc_type,
                                         seq_no+1,
                                         open_parenthesis,
                                         element,
                                         comparison,
                                         value,
                                         close_parenthesis,
                                         logic_operation
                                    from loc_list_criteria
                                   where loc_list = I_loc_list
                                     and loc_type = 'W';

      -- insert a close parenthesis with possible AND/OR logic to LOC_LIST_CRITERIA_TEMP
      open C_NEXT_SEQ_NO;
      fetch C_NEXT_SEQ_NO into L_next_seq_no;
      close C_NEXT_SEQ_NO;
      ---
      if L_delete_total != 0 then
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            close_parenthesis,
                                            logic_operation)
                                     values(I_loc_list,
                                            'W',
                                            L_next_seq_no,
                                            ')',
                                            'AND');
      elsif L_add_total != 0 then
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            close_parenthesis,
                                            logic_operation)
                                     values(I_loc_list,
                                            'W',
                                            L_next_seq_no,
                                            ')',
                                            'OR');
      end if;
      L_next_seq_no := L_next_seq_no+1;  -- next available seq_no
   else
      L_next_seq_no := 1;
   end if;

   -- add all warehouses marked as 'D' on LOC_LIST_DETAIL to LOC_LIST_CRITERIA_TEMP
   -- insert a criteria record of 'AND wh != X'; AND should go with the previous record
   ---
   L_delete_cnt := 0;                 -- total number of warehouses deleted so far
   for rec in C_DELETE_WH loop
      if L_delete_cnt+1 < L_delete_total then
         -- at least one more delete record after this one,
         -- insert an AND for connecting the next delete
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            element,
                                            comparison,
                                            value,
                                            logic_operation)
                                     values(I_loc_list,
                                            'W',
                                            L_next_seq_no,
                                            'WN',
                                            'NE',
                                            rec.location,
                                            'AND');
      elsif L_add_total != 0 then
         -- no more delete warehouse, but at least one warehouse to be added,
         -- insert an OR for connecting the next warehouse add
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            element,
                                            comparison,
                                            value,
                                            logic_operation)
                                     values(I_loc_list,
                                            'W',
                                            L_next_seq_no,
                                            'WN',
                                            'NE',
                                            rec.location,
                                            'OR');
      else
         -- no more delete warehouse after this one and no add warehouse
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            element,
                                            comparison,
                                            value)
                                     values(I_loc_list,
                                            'W',
                                            L_next_seq_no,
                                            'WN',
                                            'NE',
                                            rec.location);
      end if;
      ---
      L_delete_cnt := L_delete_cnt+1;
      L_next_seq_no := L_next_seq_no+1;
   end loop;

   -- add all warehouses marked as 'A' on LOC_LIST_DETAIL to LOC_LIST_CRITERIA_TEMP
   -- insert a criteria record of 'OR wh = X'; OR should go with the previous record
   ---
   L_add_cnt := 0;   -- total number of warehouses added so far
   for rec in C_ADD_WH loop
      if L_add_cnt+1 < L_add_total then
         -- at least one more add record after this one
         -- insert an OR for connecting the next add
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            element,
                                            comparison,
                                            value,
                                            logic_operation)
                                     values(I_loc_list,
                                            'W',
                                            L_next_seq_no,
                                            'WN',
                                            'EQ',
                                            rec.location,
                                            'OR');
      else
         -- no more warehouses to add after this one
         insert into loc_list_criteria_temp(loc_list,
                                            loc_type,
                                            seq_no,
                                            element,
                                            comparison,
                                            value)
                                     values(I_loc_list,
                                            'W',
                                            L_next_seq_no,
                                            'WN',
                                            'EQ',
                                            rec.location);
      end if;
      ---
      L_add_cnt := L_add_cnt+1;
      L_next_seq_no := L_next_seq_no +1;
   end loop;

   -- lock and delete all records from LOC_LIST_CRITERIA for the location list/warehouse
   open C_LOCK_WH_CRITERIA;
   close C_LOCK_WH_CRITERIA;
   delete from loc_list_criteria
      where loc_list = I_loc_list
        and loc_type = 'W';

   -- copy all records from LOC_LIST_CRITERIA_TEMP to the LOC_LIST_CRITERIA for the location list/warehouse
   insert into loc_list_criteria(loc_list,
                                 loc_type,
                                 seq_no,
                                 open_parenthesis,
                                 element,
                                 comparison,
                                 value,
                                 close_parenthesis,
                                 logic_operation)
                          select loc_list,
                                 loc_type,
                                 seq_no,
                                 open_parenthesis,
                                 element,
                                 comparison,
                                 value,
                                 close_parenthesis,
                                 logic_operation
                            from loc_list_criteria_temp
                           where loc_list = I_loc_list
                             and loc_type = 'W';
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_WH_CRITERIA%ISOPEN then
         close C_LOCK_WH_CRITERIA;
      end if;
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_loc_list));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END ADD_WH_CRITERIA;

------------------------------------------------------------------------------
FUNCTION UPDATE_LOC_LIST_DETAIL(O_error_message IN OUT VARCHAR2,
                                I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'LOCLIST_BUILD_SQL.UPDATE_LOC_LIST_DETAIL';
   L_sit_exists        BOOLEAN;
   L_table             VARCHAR2(20) := 'LOC_LIST_DETAIL';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_LOCK_DELETE_DETAIL is
      select 'x'
        from loc_list_detail
       where loc_list = I_loc_list
         and action_type = 'D'
         for update nowait;

   cursor C_LOCK_ADD_DETAIL is
      select 'x'
        from loc_list_detail
       where loc_list = I_loc_list
         and action_type = 'A'
         for update nowait;

BEGIN

   open C_LOCK_DELETE_DETAIL;
   close C_LOCK_DELETE_DETAIL;
   delete from loc_list_detail
      where loc_list = I_loc_list
        and action_type = 'D';
   ---
   open C_LOCK_ADD_DETAIL;
   close C_LOCK_ADD_DETAIL;
   update loc_list_detail
      set action_type = ''
    where loc_list = I_loc_list
      and action_type = 'A';
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_DELETE_DETAIL%ISOPEN then
         close C_LOCK_DELETE_DETAIL;
      end if;
      if C_LOCK_ADD_DETAIL%ISOPEN then
         close C_LOCK_ADD_DETAIL;
      end if;
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_loc_list));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END UPDATE_LOC_LIST_DETAIL;

------------------------------------------------------------------------------
FUNCTION ADD_CRITERIA(O_error_message IN OUT VARCHAR2,
                      I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'LOCLIST_BUILD_SQL.ADD_CRITERIA';

BEGIN
   if DELETE_CRITERIA_TEMP(O_error_message,
                           I_loc_list) = FALSE then
      return FALSE;
   end if;
   ---
   if ADD_ST_CRITERIA(O_error_message,
                      I_loc_list) = FALSE then
      return FALSE;
   end if;
   ---
   if ADD_WH_CRITERIA(O_error_message,
                      I_loc_list) = FALSE then
      return FALSE;
   end if;
   ---
   if UPDATE_LOC_LIST_DETAIL(O_error_message,
                             I_loc_list) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END ADD_CRITERIA;

-----------------------------------------------------------------------
FUNCTION DELETE_STAKE_SCHEDULE(O_error_message IN OUT VARCHAR2,
                               I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS

BEGIN

   SQL_LIB.SET_MARK('DELETE',NULL,'STAKE_SCHEDULE','Location: '||to_char(I_loc_list));
   delete from stake_schedule
    where location = I_loc_list
      and loc_type = 'L';
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOCLIST_BUILD_SQL.DELETE_STAKE_SCHEDULE',
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_STAKE_SCHEDULE;
-------------------------------------------------------------------------
FUNCTION LOCK_STAKE_SCHEDULE(O_error_message IN OUT VARCHAR2,
                             I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_STAKE_SCHEDULE is
      select 'x'
        from stake_schedule
       where location = I_loc_list
         and loc_type = 'L'
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_LOCK_STAKE_SCHEDULE','STAKE_SCHEDULE','Location: '||to_char(I_loc_list));
   open C_LOCK_STAKE_SCHEDULE;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_STAKE_SCHEDULE','STAKE_SCHEDULE','Location: '||to_char(I_loc_list));
   close C_LOCK_STAKE_SCHEDULE;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_STAKE_SCHEDULE%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_STAKE_SCHEDULE','STAKE_SCHEDULE','Location: '||to_char(I_loc_list));
         close C_LOCK_STAKE_SCHEDULE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'STAKE_SCHEDULE',
                                            'Location: '||to_char(I_loc_list));
      return FALSE;
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              'LOCLIST_BUILD_SQL.LOCK_STAKE_SCHEDULE',
                                              to_char(SQLCODE));
        return FALSE;

END LOCK_STAKE_SCHEDULE;
----------------------------------------------------------------------------------------
FUNCTION TEST_CRITERIA_CHANGE(O_error_message IN OUT VARCHAR2,
                              O_valid         OUT    BOOLEAN,
                              I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'LOCLIST_BUILD_SQL.TEST_CRITERIA_CHANGE';
   L_static_ind        LOC_LIST_HEAD.STATIC_IND%TYPE;
   L_count             NUMBER(7);
   cursor C_CHECK_STATIC_LIST is
      select static_ind
        from loc_list_head
       where loc_list =  I_loc_list; 
   cursor C_CHECK_CRITERIA_CHANGE is
      select count(1)
        from ((select loc_type,
                     open_parenthesis,
                     element,
                     comparison,
                     value,
                     close_parenthesis,
                     logic_operation,
                     related_value
               from  loc_list_criteria
              where  loc_list =I_loc_list
              minus
              select loc_type,
                     open_parenthesis,
                     element,
                     comparison,
                     value,
                     close_parenthesis,
                     logic_operation,
                     related_value
               from  loc_list_criteria_temp
              where  loc_list =I_loc_list)
              union all
              (select loc_type,
                     open_parenthesis,
                     element,
                     comparison,
                     value,
                     close_parenthesis,
                     logic_operation,
                     related_value
               from  loc_list_criteria_temp
              where  loc_list =I_loc_list
              minus
              select loc_type,
                     open_parenthesis,
                     element,
                     comparison,
                     value,
                     close_parenthesis,
                     logic_operation,
                     related_value
               from  loc_list_criteria
              where  loc_list =I_loc_list));
              
              
        
BEGIN
   open  C_CHECK_STATIC_LIST;
   fetch C_CHECK_STATIC_LIST into L_static_ind;
   close C_CHECK_STATIC_LIST;
   if L_static_ind = 'N' then 
      O_valid := TRUE;
   else
      open  C_CHECK_CRITERIA_CHANGE;
      fetch C_CHECK_CRITERIA_CHANGE into L_count;
      close C_CHECK_CRITERIA_CHANGE;
      
      if L_count=0 then
         O_valid := FALSE;
      else
         O_valid := TRUE;
      end if;
   end if;   
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END TEST_CRITERIA_CHANGE;
-----------------------------------------------------------------------------------------

END LOCLIST_BUILD_SQL;
/
