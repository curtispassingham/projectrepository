CREATE OR REPLACE PACKAGE BODY CATCH_WEIGHT_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION CALC_AV_COST_CATCH_WEIGHT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_new_av_cost     IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                                   I_soh_curr        IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                   I_av_cost_curr    IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                   I_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                   I_total_cost      IN       ORDLOC.UNIT_COST%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'CATCH_WEIGHT_SQL.CALC_AV_COST_CATCH_WEIGHT';

BEGIN
   ---
   if I_soh_curr is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_soh_curr',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_qty',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_av_cost_curr is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_av_cost_curr',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_total_cost is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_total_cost',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_soh_curr + I_qty <= 0 then
      -- do not change av_cost if negatively adjusted qty results in stock on hand of 0
      O_new_av_cost := I_av_cost_curr;
   else
      O_new_av_cost := ((I_soh_curr*I_av_cost_curr) + I_total_cost)/(I_soh_curr + I_qty);
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CALC_AV_COST_CATCH_WEIGHT;
--------------------------------------------------------------------------------
FUNCTION CONVERT_WEIGHT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_weight_cw_uom    IN OUT  ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                        O_catch_weight_uom IN OUT  ITEM_SUPP_COUNTRY.COST_UOM%TYPE,
                        I_item             IN      ITEM_MASTER.ITEM%TYPE,
                        I_weight           IN      ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                        I_weight_uom       IN      UOM_CLASS.UOM%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'CATCH_WEIGHT_SQL.CONVERT_WEIGHT';

   L_uom_class     UOM_CLASS.UOM_CLASS%TYPE;
   L_item_rec      ITEM_MASTER%ROWTYPE;

BEGIN

   -- This function converts I_weight from I_weight_uom to item's weight uom as defined on
   -- ITEM_MASTER table.  The weight UOM class must be MASS.

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_weight is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_weight',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_weight_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_weight_uom',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_CATCH_WEIGHT_UOM(O_error_message,
                                           O_catch_weight_uom,
                                           I_item) = FALSE then
      return FALSE;
   end if;
   ---
   if O_catch_weight_uom = I_weight_uom then
      O_weight_cw_uom := I_weight;
      return TRUE;
   else
      if NOT UOM_SQL.GET_CLASS(O_error_message,
                               L_uom_class,
                               O_catch_weight_uom) then
         return FALSE;
      end if;
      ---
      if L_uom_class != 'MASS' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_CUOM_CLASS',
                                               O_catch_weight_uom,
                                               I_item,
                                               NULL);
         return FALSE;
      end if;
      ---
      if NOT UOM_SQL.CONVERT(O_error_message,
                             O_weight_cw_uom,
                             O_catch_weight_uom,
                             I_weight,
                             I_weight_uom,
                             I_item,
                             NULL,
                             NULL) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CONVERT_WEIGHT;
--------------------------------------------------------------------------------
FUNCTION CALC_AVERAGE_WEIGHT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_avg_weight_new    IN OUT   ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                             I_item              IN       ITEM_LOC_SOH.ITEM%TYPE,
                             I_loc               IN       ITEM_LOC_SOH.LOC%TYPE,
                             I_loc_type          IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                             I_soh_curr          IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_avg_weight_curr   IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                             I_qty               IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_weight            IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                             I_weight_uom        IN       UOM_CLASS.UOM%TYPE,
                             I_recalc_ind        IN       BOOLEAN DEFAULT NULL)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'CATCH_WEIGHT_SQL.CALC_AVERAGE_WEIGHT';

   L_av_weight          ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE := I_avg_weight_curr;
   L_catch_weight_uom   ITEM_MASTER.CATCH_WEIGHT_UOM%TYPE;
   L_standard_uom       UOM_CLASS.UOM%TYPE;
   L_standard_class     UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor        ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_get_class_ind      VARCHAR2(1) := 'Y';
   L_tran_weight        ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_av_new_weight      ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_post_variance      VARCHAR2(1) := 'N';
   L_old_total_weight   ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_units              ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;

BEGIN

   -- This function calculates the new average weight for an item based on the
   -- current stock on hand and current average weight (in the items's weight uom) and the weight
   -- (in I_weight_uom) of the additional quantity. If I_weight_uom is NULL,
   -- I_weight is in item's weight uom. I_soh_curr and I_qty are both in item's standard uom.
   -- I_avg_weight_curr and the new average weight are both in item's weight uom.

   if I_weight is NULL then
      -- No change, return the current average weight
      O_avg_weight_new := I_avg_weight_curr;
      return TRUE;
   end if;

   -- if I_weight_uom is null, assume item's cost uom.
   if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                       L_standard_uom,
                                       L_standard_class,
                                       L_conv_factor,
                                       I_item,
                                       L_get_class_ind) = FALSE then
      return FALSE;
   end if;

   if L_standard_class != 'QTY' then
      -- No change, return the current average weight
      O_avg_weight_new := I_avg_weight_curr;
      return TRUE;
   end if;

   if I_soh_curr is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_soh_curr',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   ---
   if I_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_qty',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   if L_av_weight is NULL then
      ---
      if I_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_loc',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
      if NOT ITEMLOC_ATTRIB_SQL.GET_AVERAGE_WEIGHT(O_error_message,
                                                   L_av_weight,
                                                   I_item,
                                                   I_loc,
                                                   I_loc_type) then
         return FALSE;
      end if;
   end if;
   ---

   -- if I_weight_uom is null, assume item's weight uom.
   if I_weight_uom is NULL then
      L_tran_weight := I_weight;
   else
      ---
      if I_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_item',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
      ---
      if NOT CONVERT_WEIGHT(O_error_message,
                            L_tran_weight,
                            L_catch_weight_uom,
                            I_item,
                            I_weight,
                            I_weight_uom) then
            return FALSE;
         end if;
      end if;
      ---
   ---note : the L_tran_weight holds old average weight
   ---l_weight_cuom holds transaction weight
   if I_qty >= 0 then
      if I_soh_curr >= 0 then
         L_av_new_weight := ((I_soh_curr * L_av_weight)+L_tran_weight)/(I_soh_curr + I_qty);
      else
         -- reset new weight to receipt weight
         L_av_new_weight := L_tran_weight/I_qty;
         L_post_variance := 'Y';
      end if; 
   else
      -- set new weight = old weight
      if (I_soh_curr + I_qty) <= 0 or I_recalc_ind = FALSE then 
         L_av_new_weight := L_av_weight;
         L_post_variance := 'Y';
      else -- New soh > 0 and and I_recalc_ind = TRUE
         if (L_av_weight * I_soh_curr + L_tran_weight) > 0 then
            L_av_new_weight := ((I_soh_curr * L_av_weight)+ L_tran_weight)/(I_soh_curr + I_qty);
         else
            L_av_new_weight := L_av_weight;
            L_post_variance := 'Y';
         end if;
      end if;
   end if;

   if L_post_variance = 'Y' then
      if I_qty > 0 then
         -- The old (negative) SOH is being revalued from the old average weight to the new average weight
         L_old_total_weight := L_av_weight * I_soh_curr;
         L_units := I_soh_curr;
      else
         -- we aren't recalculating average weight.  
         -- So, we are revaluing the transaction units at the average weight instead of the transaction weight.
         L_old_total_weight := L_tran_weight;
         L_units  := I_qty;
      end if;

      if NOT POST_WEIGHT_VARIANCE(O_error_message,
                                  I_item,
                                  I_loc,
                                  I_loc_type,
                                  L_units,
                                  NULL,
                                  NULL,
                                  L_old_total_weight,
                                  L_av_new_weight * L_units,
                                  NULL,
                                  NULL,
                                  NULL) then
         return FALSE;
       end if;
   end if;

   O_avg_weight_new := L_av_new_weight;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CALC_AVERAGE_WEIGHT;
---------------------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_COST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_total_cost       IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                         I_pack_item        IN       ITEM_LOC_SOH.ITEM%TYPE,
                         I_loc              IN       ITEM_LOC_SOH.LOC%TYPE,
                         I_loc_type         IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                         I_pack_unit_cost   IN       ITEM_LOC_SOH.AV_COST%TYPE,
                         I_qty              IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                         I_weight           IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                         I_weight_uom       IN       UOM_CLASS.UOM%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'CATCH_WEIGHT_SQL.CALC_TOTAL_COST';

   L_tran_weight        ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_nom_weight         ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_av_weight          ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_catch_weight_uom   ITEM_MASTER.CATCH_WEIGHT_UOM%TYPE;
   L_cost_uom           ITEM_SUPP_COUNTRY.COST_UOM%TYPE;

BEGIN

   -- This function will calculate the total cost of a simple pack catch weight item.
   -- I_qty is the number of packs. I_weight is the weight of I_qty.
   -- If I_weight_uom is NULL, I_weight is in the pack item's cost unit of measure.
   -- If I_weight and I_weight_uom are both NULL, the average weight of the pack item
   -- at the location will be used.

   if I_pack_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_pack_unit_cost is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_unit_cost',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if NOT ITEM_SUPP_COUNTRY_DIM_SQL.GET_NOMINAL_WEIGHT(O_error_message,
                                                       L_nom_weight,
                                                       I_pack_item) then
      return FALSE;
   end if;
   ---
   if NOT ITEM_SUPP_COUNTRY_SQL.GET_COST_UOM(O_error_message,
                                             L_cost_uom,
                                             I_pack_item) then
      return FALSE;
   end if;

   if L_cost_uom = 'EA' then
      if I_qty is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                               'I_qty', 
                                               L_program, 
                                               NULL);
         return FALSE;
      end if;
      O_total_cost := I_pack_unit_cost * I_qty;
   else
      if I_weight is NOT NULL then
         -- weight is passed in
         if I_weight_uom is NOT NULL then
            if NOT CONVERT_WEIGHT(O_error_message,
                                  L_tran_weight,
                                  L_catch_weight_uom,
                                  I_pack_item,
                                  I_weight,
                                  I_weight_uom) then
               return FALSE;
            end if;
         else
            -- weight uom is not defined, I_weight is in cuom.
            L_tran_weight := I_weight;
         end if;
         ---
         O_total_cost := I_pack_unit_cost/L_nom_weight*L_tran_weight;
         ---

      else
         -- weight is not passed in, use average weight
         -- average weight is always in item's cuom.
         if I_loc is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_loc',
                                                  L_program,
                                                  NULL);
            return FALSE;
         end if;
         ---
         if I_qty is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_qty',
                                                  L_program,
                                                  NULL);
            return FALSE;
         end if;
         ---
         if NOT ITEMLOC_ATTRIB_SQL.GET_AVERAGE_WEIGHT(O_error_message,
                                                      L_av_weight,
                                                      I_pack_item,
                                                      I_loc,
                                                      I_loc_type) then
            return FALSE;
         end if;
         ---
         O_total_cost := I_pack_unit_cost/L_nom_weight*(L_av_weight*I_qty);
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CALC_TOTAL_COST;
--------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_COST(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_total_cost      IN OUT  ITEM_LOC_SOH.UNIT_COST%TYPE,
                         I_pack_item       IN      ITEM_LOC_SOH.ITEM%TYPE,
                         I_comp_unit_cost  IN      ITEM_LOC_SOH.AV_COST%TYPE,
                         I_weight_cw_uom   IN      ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                         I_receipt_qty     IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(50) := 'CATCH_WEIGHT_SQL.CALC_TOTAL_COST';

   L_nom_weight        ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_packsku_qty       V_PACKSKU_QTY.QTY%TYPE := NULL;

   L_exists            BOOLEAN;
   L_item              ITEM_MASTER.ITEM%TYPE;
   L_qty               PACKITEM.PACK_QTY%TYPE;
   L_standard_uom      UOM_CLASS.UOM%TYPE;
   L_standard_class    UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor       ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_get_class_ind     VARCHAR2(1) := 'Y';

   cursor C_QTY is
      select qty
        from v_packsku_qty
       where pack_no = I_pack_item;

BEGIN

   -- This function will calculate the total cost of a simple pack catch weight item.
   -- I_weight_cw_uom is the weight of the pack item in catch weight UOM.
   -- I_comp_unit_cost is the component item's unit cost.
   -- Pack's nominal weight will be used to calculate the total cost.

   if I_pack_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_comp_unit_cost is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_comp_unit_cost',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_weight_cw_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_weight_cw_uom',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if PACKITEM_ATTRIB_SQL.GET_ITEM_AND_QTY(O_error_message,
                                           L_exists,
                                           L_item,
                                           L_qty,
                                           I_pack_item) = FALSE then
      return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                       L_standard_uom,
                                       L_standard_class,
                                       L_conv_factor,
                                       L_item,
                                       L_get_class_ind) = FALSE then
      return FALSE;
   end if;

   if NOT ITEM_SUPP_COUNTRY_DIM_SQL.GET_NOMINAL_WEIGHT(O_error_message,
                                                       L_nom_weight,
                                                       I_pack_item) then
      return FALSE;
   end if;
   ---
   open C_QTY;
   fetch C_QTY into L_packsku_qty;
   close C_QTY;

   if L_packsku_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('PACK_ITEM_QTY_REQ',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if L_standard_uom = 'EA' then
      O_total_cost := I_comp_unit_cost * I_receipt_qty;
   else
      O_total_cost := (I_comp_unit_cost*L_packsku_qty)/L_nom_weight*I_weight_cw_uom;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CALC_TOTAL_COST;
--------------------------------------------------------------------------------
FUNCTION PRORATE_WEIGHT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_weight_cw_uom      IN OUT   ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                        O_catch_weight_uom   IN OUT   ITEM_MASTER.CATCH_WEIGHT_UOM%TYPE,
                        I_pack_no            IN       ITEM_MASTER.ITEM%TYPE,
                        I_loc                IN       ITEM_LOC_SOH.LOC%TYPE,
                        I_loc_type           IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                        I_total_weight       IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                        I_total_weight_uom   IN       UOM_CLASS.UOM%TYPE,
                        I_total_qty          IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                        I_qty                IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(60) := 'CATCH_WEIGHT_SQL.PRORATE_WEIGHT';

   L_average_weight        ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_total_weight_cw_uom   ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;

BEGIN
   -- This function will return the weight in catch_weight_uom corresponds to I_qty.
   -- I_total_weight is the weight of I_total_qty in I_total_weight_uom.
   -- If I_total_weight_uom is NULL, use the pack's average weight to derive weight.

   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_qty',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_total_weight is NOT NULL and I_total_weight_uom is NOT NULL then
      ---
      if I_total_qty is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_total_qty',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
      -- convert to cw_uom
      if NOT CATCH_WEIGHT_SQL.CONVERT_WEIGHT(O_error_message,
                                             L_total_weight_cw_uom,
                                             O_catch_weight_uom,
                                             I_pack_no,
                                             I_total_weight,
                                             I_total_weight_uom) then
         return FALSE;
      end if;
      --- In case of catchweight item , only weight adjustment done in RWMS.
      if I_qty = 0 then
         O_weight_cw_uom := L_total_weight_cw_uom;
      else
         O_weight_cw_uom := L_total_weight_cw_uom/I_total_qty * I_qty;
      end if;
   else
      ---
      if I_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_loc',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
      ---
      if NOT ITEMLOC_ATTRIB_SQL.GET_AVERAGE_WEIGHT(O_error_message,
                                                   L_average_weight,
                                                   I_pack_no,
                                                   I_loc,
                                                   I_loc_type) then
         return FALSE;
      end if;
      O_weight_cw_uom := L_average_weight * I_qty;

      if NOT ITEM_ATTRIB_SQL.GET_CATCH_WEIGHT_UOM(O_error_message,
                                                  O_catch_weight_uom,
                                                  I_pack_no) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;
END PRORATE_WEIGHT;
--------------------------------------------------------------------------------
FUNCTION CALC_COMP_UPDATE_QTY(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_upd_qty         IN OUT  ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                              I_comp_item       IN      ITEM_MASTER.ITEM%TYPE,
                              I_unit_qty        IN      V_PACKSKU_QTY.QTY%TYPE,
                              I_weight          IN      ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_weight_uom      IN      UOM_CLASS.UOM%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(60) := 'CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY';

   L_standard_uom      UOM_CLASS.UOM%TYPE;
   L_standard_class    UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor       ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_get_class_ind     VARCHAR2(1) := 'Y';

BEGIN

   if I_comp_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_comp_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_unit_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_unit_qty',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_weight is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_weight',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_weight_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_weight_uom',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                       L_standard_uom,
                                       L_standard_class,
                                       L_conv_factor,
                                       I_comp_item,
                                       L_get_class_ind) = FALSE then
      return FALSE;
   end if;

   if L_standard_class = 'MASS' then
      -- component suom is MASS
      -- component stock buckets will be updated by weight in component's standard uom
      if I_weight <> 0 and I_weight_uom is not null then  
         if NOT UOM_SQL.CONVERT(O_error_message,
                                O_upd_qty,  -- weight of qty returned
                                L_standard_uom,
                                I_weight,
                                I_weight_uom,
                                I_comp_item,
                                NULL,
                                NULL) then
            return FALSE;
         end if;
      else
         O_upd_qty := I_unit_qty;
      end if;
   elsif L_standard_uom = 'EA' then
      -- component suom is Eaches
      -- component stock buckets will be updated with unit qty
      O_upd_qty := I_unit_qty;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUOM_CATCH_WGT',
                                            L_standard_class,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;
END CALC_COMP_UPDATE_QTY;
--------------------------------------------------------------------------------
FUNCTION CALC_COMP_UPDATE_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_upd_qty         IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                              I_comp_item       IN       ITEM_MASTER.ITEM%TYPE,
                              I_comp_unit_qty   IN       V_PACKSKU_QTY.QTY%TYPE,
                              I_weight          IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_weight_uom      IN       UOM_CLASS.UOM%TYPE,
                              I_pack_no         IN       V_PACKSKU_QTY.PACK_NO%TYPE,
                              I_location        IN       ITEM_LOC_SOH.LOC%TYPE,
                              I_loc_type        IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                              I_pack_unit_qty   IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(60) := 'CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY';

   L_average_weight    ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_catch_weight_uom  ITEM_MASTER.CATCH_WEIGHT_UOM%TYPE;

BEGIN

   -- This is an overloaded function for calculating the update qty for a
   -- simple pack catch weight component item. If weight and weight_uom are
   -- not passed in, it uses the pack item's average weight to derive weight.

   if I_weight is NOT NULL and I_weight_uom is NOT NULL then
      -- weight is defined, convert weight into item's SUOM
      if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                               O_upd_qty,
                                               I_comp_item,
                                               I_comp_unit_qty,
                                               I_weight,
                                               I_weight_uom) = FALSE then
         return FALSE;
      end if;
   else
      -- weight is NOT defined, use pack's average weight (in catch_weight_uom) to derive weight
      if ITEMLOC_ATTRIB_SQL.GET_AVERAGE_WEIGHT(O_error_message,
                                               L_average_weight,
                                               I_pack_no,
                                               I_location,
                                               I_loc_type) = FALSE then
         return FALSE;
      end if;

      if NOT ITEM_ATTRIB_SQL.GET_CATCH_WEIGHT_UOM(O_error_message,
                                                  L_catch_weight_uom,
                                                  I_pack_no) then
         return FALSE;
      end if;

      -- convert into item's SUOM
      if CATCH_WEIGHT_SQL.CALC_COMP_UPDATE_QTY(O_error_message,
                                               O_upd_qty,
                                               I_comp_item,
                                               I_comp_unit_qty,
                                               L_average_weight*I_pack_unit_qty,
                                               L_catch_weight_uom) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;
END CALC_COMP_UPDATE_QTY;
--------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_RETAIL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_comp_item              IN       ITEM_MASTER.ITEM%TYPE,
                           I_qty                    IN       ITEM_LOC_SOH.STOCK_ON_Hand%TYPE,
                           I_unit_retail            IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                           I_location               IN       ITEM_LOC_SOH.LOC%TYPE,
                           I_loc_type               IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                           I_weight                 IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                           I_weight_uom             IN       UOM_CLASS.UOM%TYPE,
                           O_total_retail           IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           I_post_weight_variance   IN       BOOLEAN DEFAULT FALSE,
                           I_inbound                IN       BOOLEAN DEFAULT TRUE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(60) := 'CATCH_WEIGHT_SQL.CALC_TOTAL_RETAIL';
   L_catchweight_ind   ITEM_MASTER.CATCH_WEIGHT_IND%TYPE;
   L_standard_uom      UOM_CLASS.UOM%TYPE;
   L_standard_class    UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor       ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_get_class_ind     VARCHAR2(1) := 'Y';
   L_comp_av_cost      ITEM_LOC_SOH.AV_COST%TYPE;
   L_unit_retail       ITEM_LOC.UNIT_RETAIL%TYPE;
   L_unit_cost         ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_selling_retail    ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom       ITEM_LOC.SELLING_UOM%TYPE;
   L_average_weight    ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_total_weight      ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_nom_weight        ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_weight_cw_uom     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_catch_weight_uom  ITEM_MASTER.CATCH_WEIGHT_UOM%TYPE;
   L_old_weight        ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_new_weight        ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;

   cursor C_ITEM_MASTER is
    select catch_weight_ind
      from item_master
     where item = I_comp_item;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_MASTER',
                    'ITEM_MASTER',
                    'Item: '||I_comp_item);
   open C_ITEM_MASTER;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_MASTER',
                    'ITEM_MASTER',
                    'Item: '||I_comp_item);
   fetch C_ITEM_MASTER into L_catchweight_ind;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_MASTER',
                    'ITEM_MASTER',
                    'Item: '||I_comp_item);
   close C_ITEM_MASTER;

   --- Get standard uom
   if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                       L_standard_uom,
                                       L_standard_class,
                                       L_conv_factor,
                                       I_comp_item,
                                       L_get_class_ind) = FALSE then
      return FALSE;
   end if;

    --- Get unit_cost and unit_retail and selling uom from item_loc in local currency.
   if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                               I_comp_item,
                                               I_location,
                                               I_loc_type,
                                               L_comp_av_cost,
                                               L_unit_cost,    -- local currency
                                               L_unit_retail,  -- local currency
                                               L_selling_retail,
                                               L_selling_uom) = FALSE then
      return FALSE;
   end if;
   if I_unit_retail is NOT NULL then
      L_unit_retail := I_unit_retail;
   end if;

   if L_catchweight_ind = 'N' or  L_standard_uom != 'EA' or L_selling_uom = 'EA' then
      O_total_retail := I_qty * L_unit_retail;
   else
      -- look up nominal weight

      if I_weight is NULL then
         -- get average weight
         if NOT ITEMLOC_ATTRIB_SQL.GET_AVERAGE_WEIGHT(O_error_message,
                                                      L_average_weight,
                                                      I_comp_item,
                                                      I_location,
                                                      I_loc_type) then
            return FALSE;
         end if;
         ---
         L_total_weight :=  L_average_weight * I_qty;
      else
         -- I_weight is not null
         -- convert to get the weight
         -- weight is passed in
         if I_weight_uom is NOT NULL then
            if NOT CONVERT_WEIGHT(O_error_message,
                                  L_weight_cw_uom,
                                  L_catch_weight_uom,
                                  I_comp_item,
                                  I_weight,
                                  I_weight_uom) then
               return FALSE;
            end if;
         else
            -- weight uom is not defined, I_weight is in cuom.
            L_weight_cw_uom := I_weight;

         end if;

         L_total_weight := L_weight_cw_uom;

         if I_post_weight_variance = TRUE then
            if NOT ITEMLOC_ATTRIB_SQL.GET_AVERAGE_WEIGHT(O_error_message,
                                                         L_average_weight,
                                                         I_comp_item,
                                                         I_location,
                                                         I_loc_type) then
               return FALSE;
            end if; 
            -- we aren't recalculating average weight.
            -- So, we are revaluing the transaction units at the average weight instead of the transaction weight.
            if NOT I_inbound then
               L_old_weight :=  L_average_weight * I_qty;
               L_new_weight :=  L_total_weight;
            else
               L_old_weight :=  L_total_weight;
               L_new_weight :=  L_average_weight * I_qty;
            end if;
            if NOT POST_WEIGHT_VARIANCE(O_error_message,
                                        I_comp_item,
                                        I_location,
                                        I_loc_type,
                                        I_qty,
                                        NULL,
                                        NULL,
                                        L_old_weight,
                                        L_new_weight,
                                        NULL,
                                        L_unit_retail,
                                        NULL) then
               return FALSE;
            end if;
         end if;
      end if;

      if NOT ITEM_SUPP_COUNTRY_DIM_SQL.GET_NOMINAL_WEIGHT(O_error_message,
                                                          L_nom_weight,
                                                          I_comp_item) then
         return FALSE;
      end if;

      --
      O_total_retail :=  L_unit_retail * L_total_weight / L_nom_weight;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CALC_TOTAL_RETAIL;

--------------------------------------------------------------------------
FUNCTION POST_WEIGHT_VARIANCE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item                IN       ITEM_MASTER.ITEM%TYPE,
                              I_location            IN       ITEM_LOC.LOC%TYPE,
                              I_loc_type            IN       ITEM_LOC.LOC_TYPE%TYPE,
                              I_units               IN       TRAN_DATA.UNITS%TYPE,
                              I_ref_no_1            IN       TRAN_DATA.REF_NO_1%TYPE,
                              I_ref_no_2            IN       TRAN_DATA.REF_NO_2%TYPE,
                              I_total_weight_old    IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_total_weight_new    IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                              I_weight_uom          IN       UOM_CLASS.UOM%TYPE,
                              I_unit_retail         IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                              I_tran_date           IN       PERIOD.VDATE%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(60) := 'CATCH_WEIGHT_SQL.POST_WEIGHT_VARIANCE';

   L_tran_code          TRAN_DATA.TRAN_CODE%TYPE  := 10;
   L_total_cost         TRAN_DATA.TOTAL_COST%TYPE := NULL;
   L_item_rec           ITEM_MASTER%ROWTYPE;
   L_old_unit_retail    ITEM_LOC.UNIT_RETAIL%TYPE;
   L_new_unit_retail    ITEM_LOC.UNIT_RETAIL%TYPE;
   L_old_total_retail   ITEM_LOC.UNIT_RETAIL%TYPE;
   L_new_total_retail   ITEM_LOC.UNIT_RETAIL%TYPE;

BEGIN

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_rec,
                                      I_item) = FALSE then
      return FALSE;
   end if;
   if L_item_rec.pack_ind = 'Y' then
      return TRUE;
   end if;

   if CATCH_WEIGHT_SQL.CALC_TOTAL_RETAIL(O_error_message,
                                         I_item,
                                         I_units,
                                         I_unit_retail,
                                         I_location,
                                         I_loc_type,
                                         I_total_weight_old,
                                         I_weight_uom,
                                         L_old_total_retail)= FALSE then
      return FALSE;
   end if;
   if CATCH_WEIGHT_SQL.CALC_TOTAL_RETAIL(O_error_message,
                                         I_item,
                                         I_units,
                                         I_unit_retail,
                                         I_location,
                                         I_loc_type,
                                         I_total_weight_new,
                                         I_weight_uom,
                                         L_new_total_retail)= FALSE then
      return FALSE;
   end if;

   if L_old_total_retail != L_new_total_retail then
      -- retail variance is weight variance * selling price
      if I_units = 0 then
         L_old_unit_retail := 0;
         L_new_unit_retail := 0;
      else
         -- average unit retail is average weight * selling price
         L_old_unit_retail := L_old_total_retail / I_units;
         L_new_unit_retail := L_new_total_retail / I_units;
      end if;

      if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             L_item_rec.dept,
                                             L_item_rec.class,
                                             L_item_rec.subclass,
                                             I_location,
                                             I_loc_type,
                                             NVL(I_tran_date, GET_VDATE),
                                             L_tran_code,
                                             NULL,
                                             I_units,
                                             L_total_cost,   -- total cost
                                             L_old_total_retail - L_new_total_retail,
                                             I_ref_no_1,
                                             I_ref_no_2,
                                             NULL,
                                             NULL,
                                             L_old_unit_retail,
                                             L_new_unit_retail,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_program,
                                             NULL) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POST_WEIGHT_VARIANCE;
--------------------------------------------------------------------------
FUNCTION DETERMINE_CATCH_WEIGHT_TYPE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_catch_weight_type   IN OUT   ITEM_MASTER.CATCH_WEIGHT_TYPE%TYPE,
                                     I_item                IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN is

   L_program                VARCHAR2(60) := 'CATCH_WEIGHT_SQL.DETERMINE_CATCH_WEIGHT_TYPE';
   L_item_record            ITEM_MASTER%ROWTYPE;
   L_simple_pack_ind        ITEM_MASTER.SIMPLE_PACK_IND%TYPE := NULL;
   L_catch_weight_ind       ITEM_MASTER.CATCH_WEIGHT_IND%TYPE := NULL;
   L_exists                 BOOLEAN := FALSE;
   L_component_item         PACKITEM.ITEM%TYPE := NULL;
   L_qty                    PACKITEM.PACK_QTY%TYPE := NULL;
   L_uom_class              UOM_CLASS.UOM_CLASS%TYPE := NULL;

   BEGIN
      if I_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_item',
                                               'NOT NULL',
                                               'NULL');
         return FALSE;
      end if;

      O_catch_weight_type := NULL;
      -- Get item details for Pack Item
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_item_record,
                                         I_item) = FALSE then
         return FALSE;
      end if;
      if NVL(L_item_record.simple_pack_ind,'N')  = 'Y' and
         NVL(L_item_record.catch_weight_ind,'N') = 'Y' and
         L_item_record.order_type                in ('V','F') and  --Variable Weight
         L_item_record.pack_type                 = 'V' then  --Vendor Pack

         if PACKITEM_ATTRIB_SQL.GET_ITEM_AND_QTY(O_error_message,
                                                 L_exists,
                                                 L_component_item,
                                                 L_qty,
                                                 I_item) = TRUE then
            if L_exists = TRUE then
               -- Get Item Details for Component Item
               if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                                  L_item_record,
                                                  L_component_item) = FALSE then
                  return FALSE;
               end if;
               if UOM_SQL.GET_CLASS(O_error_message,
                                    L_uom_class,
                                    L_item_record.standard_uom) = FALSE then
                  return FALSE;
               end if;

               if L_item_record.sale_type = 'L' and -- Loose Weight
                  L_uom_class = 'MASS' then
                  if L_item_record.order_type = 'F' then -- Fixed Weight
                     O_catch_weight_type := '1';
                  elsif L_item_record.order_type = 'V' then -- Variable Weight
                     O_catch_weight_type := '2';
                  end if;
               elsif L_item_record.sale_type = 'V' and --Variable Weight
                  L_item_record.standard_uom = 'EA'  then
                  if L_item_record.order_type = 'F' then -- Fixed Weight
		         O_catch_weight_type := '3';
		      elsif L_item_record.order_type = 'V' then -- Variable Weight
		         O_catch_weight_type := '4';
                  end if;
               end if;
            end if;
         end if; 
      end if;
      return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;

   END DETERMINE_CATCH_WEIGHT_TYPE;
----------------------------------------------------------------
END CATCH_WEIGHT_SQL;
/