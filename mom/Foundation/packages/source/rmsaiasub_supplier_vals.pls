CREATE OR REPLACE PACKAGE RMSAIASUB_SUPPLIER_VALIDATE AUTHID CURRENT_USER AS
-------------------------------------------------------------------------
TYPE ADDRESS_TYPE_DATA IS TABLE OF ADDR.ADDR_TYPE%TYPE
   INDEX BY BINARY_INTEGER;
-------------------------------------------------------------------------
-- Function Name: PROCESS_SUPPLIER_RECORD
-- Purpose      : This function will validate the input supplier record and
--                populate the local tables to be used for processing.
--                Also this populates output ref_object, which will be sent back to
--                AIA for Xref key and Retail key mapping.
-------------------------------------------------------------------------
FUNCTION PROCESS_SUPPLIER_RECORD(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_supplier_object   IN OUT  SUPP_REC,
                                 O_ref_outputobject  IN OUT  "RIB_SupplierColRef_REC",
                                 I_inputobject       IN      "RIB_SupplierColDesc_REC",
                                 I_inputobject_type  IN      VARCHAR2)
return BOOLEAN;
-------------------------------------------------------------------------
END RMSAIASUB_SUPPLIER_VALIDATE;
/
