CREATE OR REPLACE PACKAGE BODY SIT_SQL AS
---------------------------------------------------------------------------------------------------------
FUNCTION SIT_EXISTS(O_error_message   IN OUT  VARCHAR2,
                    O_exists          IN OUT  BOOLEAN,
                    I_itemlist        IN      SKULIST_HEAD.SKULIST%TYPE,
                    I_loc_list        IN      LOC_LIST_HEAD.LOC_LIST%TYPE)
   RETURN BOOLEAN IS

   L_dummy      VARCHAR2(1);
   L_program    VARCHAR2(50) := 'SIT_SQL.SIT_EXISTS';

   cursor C_SIT_EXISTS is
      select 'x'
        from sit_head
       where skulist  = nvl(I_itemlist, skulist)
         and loc_list = nvl(I_loc_list, loc_list);

BEGIN

   if I_itemlist is NULL and I_loc_list is NULL then
      O_error_message := sql_lib.create_msg('INV_PARAM_EITHER',
                                            'I_itemlist',
                                            'I_loc_list',
                                            L_program);
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_SIT_EXISTS', 'SIT_HEAD', 'ITEMLIST: '||I_itemlist||' LOC_LIST: '||I_loc_list);
   open C_SIT_EXISTS;

   SQL_LIB.SET_MARK('FETCH', 'C_SIT_EXISTS', 'SIT_HEAD', 'ITEMLIST: '||I_itemlist||' LOC_LIST: '||I_loc_list);
   fetch C_SIT_EXISTS into L_dummy;

   if C_SIT_EXISTS%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_SIT_EXISTS', 'SIT_HEAD', 'ITEMLIST: '||I_itemlist||' LOC_LIST: '||I_loc_list);
   close C_SIT_EXISTS;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END SIT_EXISTS;
----------------------------------------------------------------------------------------------
FUNCTION DELETE_SIT(O_error_message   IN OUT  VARCHAR2,
                    I_itemloc_link_id IN      SIT_HEAD.ITEMLOC_LINK_ID%TYPE,
                    I_itemlist        IN      SKULIST_HEAD.SKULIST%TYPE,
                    I_loc_list        IN      LOC_LIST_HEAD.LOC_LIST%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'SIT_SQL.DELETE_SIT';
   L_table                  VARCHAR2(20) := NULL;
   L_itemloc_link_id        SIT_HEAD.ITEMLOC_LINK_ID%TYPE;
   L_delete_link_ind        VARCHAR2(1);
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_LINK_ID is
   select itemloc_link_id
     from sit_head
    where skulist  = nvl(I_itemlist, skulist)
      and loc_list = nvl(I_loc_list, loc_list);

   cursor C_SIT_HEAD_LOCK is
   select 'x'
     from sit_head
    where itemloc_link_id = L_itemloc_link_id
      for update nowait;

   cursor C_SIT_DETAIL_LOCK is
   select 'x'
     from sit_detail
    where itemloc_link_id = L_itemloc_link_id
      for update nowait;

   cursor C_SIT_EXPLODE_LOCK is
   select 'x'
     from sit_explode
    where itemloc_link_id = L_itemloc_link_id
      for update nowait;

   cursor C_SIT_CONFLICT_LOCK is
   select 'x'
     from sit_conflict
    where itemloc_link_id          = L_itemloc_link_id
       or conflict_itemloc_link_id = L_itemloc_link_id
      for update nowait;

BEGIN

   if I_itemloc_link_id is NULL
   and I_itemlist is NULL and I_loc_list is NULL then
      O_error_message := sql_lib.create_msg('INV_PARAM_EITHER',
                                            'I_itemloc_link_id',
                                            'I_itemlist/I_loc_list',
                                            L_program);

   elsif I_itemloc_link_id is not NULL then
      L_itemloc_link_id := I_itemloc_link_id;
      L_delete_link_ind := 'Y';
   else

      SQL_LIB.SET_MARK('OPEN', 'C_GET_LINK_ID', 'SIT_HEAD', 'ITEMLIST: '||I_itemlist||' LOC_LIST:                                      '||I_loc_list);
      open C_GET_LINK_ID;
      ---
   end if;
   ---
   LOOP
      if I_itemloc_link_id is NULL then
         fetch C_GET_LINK_ID into L_itemloc_link_id;
         if C_GET_LINK_ID%NOTFOUND then
            SQL_LIB.SET_MARK('CLOSE', 'C_GET_LINK_ID', 'SIT_HEAD', 'ITEMLIST: '||I_itemlist||' LOC_LIST: '||I_loc_list);
            close C_GET_LINK_ID;
            EXIT;
         end if;
      end if;
      ---
      open C_SIT_CONFLICT_LOCK;
      close C_SIT_CONFLICT_LOCK;
      ---
      delete from sit_conflict
       where itemloc_link_id          = L_itemloc_link_id
          or conflict_itemloc_link_id = L_itemloc_link_id;
      ---
      open C_SIT_EXPLODE_LOCK;
      close C_SIT_EXPLODE_LOCK;
      ---
      delete from sit_explode
       where itemloc_link_id = L_itemloc_link_id;
      ---
      open C_SIT_DETAIL_LOCK;
      close C_SIT_DETAIL_LOCK;
      ---
      delete from sit_detail
       where itemloc_link_id = L_itemloc_link_id;
      ---
      open C_SIT_HEAD_LOCK;
      close C_SIT_HEAD_LOCK;
      ---
      delete from sit_head
       where itemloc_link_id = L_itemloc_link_id;
      ---
      if L_delete_link_ind = 'Y' then
         --Only go trough loop once;
         EXIT;
      end if;
   end LOOP;
   ---
   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_SIT_CONFLICT_LOCK%ISOPEN then
         close C_SIT_CONFLICT_LOCK;
         L_table := 'SIT_CONFLICT';
      elsif C_SIT_EXPLODE_LOCK%ISOPEN then
         close C_SIT_EXPLODE_LOCK;
         L_table := 'SIT_EXPLODE';
      elsif C_SIT_DETAIL_LOCK%ISOPEN then
         close C_SIT_DETAIL_LOCK;
         L_table := 'SIT_DETAIL';
      elsif C_SIT_HEAD_LOCK%ISOPEN then
         close C_SIT_HEAD_LOCK;
         L_table := 'SIT_HEAD';
      end if;
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_itemloc_link_id));
      RETURN FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END DELETE_SIT;
---------------------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM(O_error_message   IN OUT  VARCHAR2,
                     I_itemlist        IN      SKULIST_HEAD.SKULIST%TYPE,
                     I_item            IN      SKULIST_DETAIL.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'SIT_SQL.INSERT_ITEM';
   L_itemloc_link_id     SIT_HEAD.ITEMLOC_LINK_ID%TYPE;
   L_loc_list            SIT_HEAD.LOC_LIST%TYPE;
   L_loc                 LOC_LIST_DETAIL.LOCATION%TYPE;

   cursor C_INFO is
   select itemloc_link_id,
          loc_list
     from sit_head
    where skulist = I_itemlist;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_INFO', 'SIT_HEAD', 'ITEMLIST: '||I_itemlist);
   open C_INFO;
   ---
   LOOP
      SQL_LIB.SET_MARK('FETCH', 'C_INFO', 'SIT_HEAD', 'ITEMLIST: '||I_itemlist);
      fetch C_INFO into L_itemloc_link_id, L_loc_list;

      if C_INFO%FOUND then
      ---
         insert into sit_explode(itemloc_link_id,
                                 skulist,
                                 item,
                                 loc_list,
                                 location,
                                 update_ind)
                          select L_itemloc_link_id,
                                 I_itemlist,
                                 I_item,
                                 L_loc_list,
                                 location,
                                 'Y'
                            from loc_list_detail
                           where loc_list = L_loc_list;
      else
         EXIT;
      end if;
   end LOOP;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_INFO', 'SIT_HEAD', 'ITEMLIST: '||I_itemlist);
   close C_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END INSERT_ITEM;
-------------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM(O_error_message   IN OUT  VARCHAR2,
                     I_itemlist        IN      SKULIST_HEAD.SKULIST%TYPE,
                     I_item            IN      SKULIST_DETAIL.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'SIT_SQL.DELETE_ITEM';
   L_table                  VARCHAR2(20) := 'SIT_EXPLODE';
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_SIT_EXPLODE IS
   SELECT se.rowid
     FROM sit_explode se
    WHERE se.skulist = I_itemlist
      AND se.item = I_item 
      FOR UPDATE nowait;

   CURSOR C_LOCK_SIT_CONFLICT IS
   SELECT sc.rowid
     FROM sit_conflict sc, 
          (SELECT /*+ INDEX (se SIT_EXPLODE_I2) +*/ 
                  item,
                  location,
                  se.itemloc_link_id                                                
             FROM sit_explode se
            WHERE se.skulist = I_itemlist
           ) se
    WHERE sc.item = I_item
      AND (se.itemloc_link_id = sc.itemloc_link_id
           OR se.itemloc_link_id = sc.conflict_itemloc_link_id)
      AND se.item     = sc.item
      AND se.location = sc.location 
      FOR UPDATE nowait;
      
   TYPE L_rowid IS TABLE OF rowid INDEX BY BINARY_INTEGER;
   L_list L_rowid;


BEGIN

   if I_itemlist is not NULL and I_item is not NULL then

      OPEN C_LOCK_SIT_CONFLICT;
      FETCH C_LOCK_SIT_CONFLICT bulk collect
            INTO L_list;
      CLOSE C_LOCK_SIT_CONFLICT;
      
      FORALL i IN 1..L_list.COUNT
         DELETE FROM sit_conflict
          WHERE rowid = L_list(i);
          
      OPEN C_LOCK_SIT_EXPLODE;
      FETCH C_LOCK_SIT_EXPLODE bulk collect
       INTO L_list;
      CLOSE C_LOCK_SIT_EXPLODE;

      FORALL i IN 1..L_list.COUNT
         DELETE FROM sit_explode
          WHERE rowid = L_list(i);

   else
      O_error_message := sql_lib.create_msg('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_SIT_EXPLODE%ISOPEN then
         close C_LOCK_SIT_EXPLODE;
      elsif C_LOCK_SIT_CONFLICT%ISOPEN then
         close C_LOCK_SIT_CONFLICT;
      end if;
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_itemlist));
      RETURN FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END DELETE_ITEM;
---------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_RECORDS(O_error_message   IN OUT  VARCHAR2,
                         I_itemloc_link_id IN      SIT_HEAD.ITEMLOC_LINK_ID%TYPE,
                         I_itemlist        IN      SKULIST_HEAD.SKULIST%TYPE,
                         I_loc_list        IN      LOC_LIST_HEAD.LOC_LIST%TYPE,
                         I_update_ind      IN      SIT_EXPLODE.UPDATE_IND%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'SIT_SQL.EXPLODE_RECORDS';

BEGIN
   insert into sit_explode(itemloc_link_id,
                           skulist,
                           item,
                           loc_list,
                           location,
                           update_ind)
                    select I_itemloc_link_id,
                           I_itemlist,
                           item,
                           I_loc_list,
                           location,
                           nvl(I_update_ind, 'Y')
                      from loc_list_detail,
                           skulist_detail
                     where loc_list = I_loc_list
                       and skulist  = I_itemlist;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END EXPLODE_RECORDS;
-----------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_NEW_LINK(O_error_message    IN OUT  VARCHAR2,
                           O_dup_flag         IN OUT  VARCHAR2,
                           I_itemloc_link_id  IN      SIT_HEAD.ITEMLOC_LINK_ID%TYPE)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50) := 'SIT_SQL.VALIDATE_NEW_LINK';

BEGIN

   O_dup_flag := 'N';

   SQL_LIB.SET_MARK('INSERT', NULL, 'SIT_CONFLICT', 'ITEMLOC_LINK_ID: '|| I_itemloc_link_id);

   insert into sit_conflict(itemloc_link_id,
                            conflict_itemloc_link_id,
                            item,
                            location,
                            insert_date)
                     select I_itemloc_link_id,
                            se.itemloc_link_id,
                            se.item,
                            se.location,
                            sysdate
                       from sit_explode se,
                            sit_head sh,
                            skulist_detail sd,
                            loc_list_detail lld
                      where sh.itemloc_link_id = I_itemloc_link_id
                        and sh.skulist = sd.skulist
                        and sd.item = se.item
                        and sh.loc_list = lld.loc_list
                        and lld.location = se.location
                        and se.itemloc_link_id != I_itemloc_link_id;


   if SQL%ROWCOUNT > 0 then
      O_dup_flag := 'Y';
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END VALIDATE_NEW_LINK;
------------------------------------------------------------------------------------------------------
FUNCTION DETAIL_EXISTS(O_error_message      IN OUT  VARCHAR2,
                       O_exists             IN OUT  BOOLEAN,
                       I_itemloc_link_id    IN      SIT_DETAIL.ITEMLOC_LINK_ID%TYPE)
   RETURN BOOLEAN IS

   L_dummy      VARCHAR2(1);
   L_program    VARCHAR2(50) := 'SIT_SQL.DETAIL_EXISTS';

   cursor C_DETAIL_EXISTS is
   select 'x'
     from sit_detail
    where itemloc_link_id = I_itemloc_link_id;

BEGIN
   if I_itemloc_link_id is NULL then
      O_error_message := sql_lib.create_msg('INVALID_ITEMLOC_LINK_ID',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_DETAIL_EXISTS', 'SIT_DETAIL', 'ITEMLOC_LINK_ID: '||I_itemloc_link_id);
   open C_DETAIL_EXISTS;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_DETAIL_EXISTS', 'SIT_DETAIL', 'ITEMLOC_LINK_ID: '||I_itemloc_link_id);
   fetch C_DETAIL_EXISTS into L_dummy;
   ---
   if C_DETAIL_EXISTS%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_DETAIL_EXISTS', 'SIT_DETAIL', 'ITEMLOC_LINK_ID: '||I_itemloc_link_id);
   close C_DETAIL_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END DETAIL_EXISTS;
--------------------------------------------------------------------------------------------------------
FUNCTION COPY_SIT_CONFLICT(O_error_message   IN OUT VARCHAR2,
                           I_itemlist        IN     SIT_HEAD.SKULIST%TYPE,
                           I_loc_list        IN     SIT_HEAD.LOC_LIST%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'SIT_SQL.COPY_SIT_CONFLICT';
   L_table          VARCHAR2(50) := 'SIT_CONFLICT';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   L_sit SIT_TBL;

BEGIN

   if I_itemlist is not NULL and I_loc_list is not NULL then
      O_error_message := sql_lib.create_msg('TOO_MANY_PARMS','ITEMLIST','LOC_LIST',L_program);
   elsif I_itemlist is NULL and I_loc_list is NULL then
      O_error_message := sql_lib.create_msg('INV_PARAM_EITHER','I_itemlist','I_loc_list',L_program);
   end if;

   IF i_itemlist IS NOT NULL THEN
      SELECT /*+ ordered rowid(sc) use_nl(sc) */ 
             OBJ_SIT_REC(sc.rowid,
                         sc.conflict_itemloc_link_id,
                         sc.item,
                         sc.location,
                         sc.itemloc_link_id, 
                         sc.insert_date)
         BULK COLLECT INTO L_sit
         FROM sit_conflict sc,
             (SELECT /*+ INDEX (se SIT_EXPLODE_I2) +*/
                     item,
                     location,
                     itemloc_link_id
                FROM sit_explode
               WHERE skulist = i_itemlist) se
         WHERE (se.itemloc_link_id = sc.itemloc_link_id
              OR se.itemloc_link_id = sc.conflict_itemloc_link_id)
         AND se.item = sc.item
         AND se.location = sc.location FOR UPDATE OF sc.itemloc_link_id NOWAIT;

      INSERT
         INTO sit_conflict_temp(conflict_itemloc_link_id,
                               item,
                               location,
                               itemloc_link_id,
                               insert_date)
      SELECT conflict_itemloc_link_id,
             item,
             location,
             itemloc_link_id,
             insert_date
         FROM TABLE(L_sit);

      DELETE FROM sit_conflict
         WHERE rowid IN (SELECT rid FROM TABLE(L_sit));
   ELSE
      SELECT /*+ ordered rowid(sc) use_nl(sc) */
             OBJ_SIT_REC(sc.rowid,
                         sc.conflict_itemloc_link_id,
                         sc.item,
                         sc.location,
                         sc.itemloc_link_id,
                         sc.insert_date)
         BULK COLLECT INTO    L_sit
         FROM sit_conflict sc,
             (SELECT /*+ INDEX(se SIT_EXPLODE_I1) +*/
                     location,
                     item,
                     itemloc_link_id
                FROM sit_explode
               WHERE   loc_list = i_loc_list) se
         WHERE (se.itemloc_link_id = sc.itemloc_link_id
              OR se.itemloc_link_id = sc.conflict_itemloc_link_id)
         AND se.location = sc.location
         AND se.item = sc.item FOR UPDATE OF sc.itemloc_link_id NOWAIT;

      INSERT
         INTO sit_conflict_temp (conflict_itemloc_link_id,
                                 item,
                                 location,
                                 itemloc_link_id,
                                 insert_date)
         SELECT sc.conflict_itemloc_link_id,
              sc.item,
              sc.location,
              sc.itemloc_link_id,
              sc.insert_date
         FROM TABLE(L_sit) sc;

      -- Delete SIT_CONFLICT Data after copying the conflict data into SIT_CONFLICT_TEMP
      DELETE FROM sit_conflict
         WHERE rowid IN (SELECT rid FROM TABLE(L_sit));
   END IF;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_itemlist));
      RETURN FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;

END COPY_SIT_CONFLICT;
--------------------------------------------------------------------------------------------------------
FUNCTION REBUILD_SIT_CONFLICT(O_error_message    IN OUT VARCHAR2,
                              I_itemlist         IN     SIT_HEAD.SKULIST%TYPE,
                              I_loc_list   IN     SIT_HEAD.LOC_LIST%TYPE)
   RETURN BOOLEAN is

   L_program              VARCHAR2(50) := 'SIT_SQL.REBUILD_SIT_CONFLICT';
   L_table                VARCHAR2(50) := 'SIT_CONFLICT';
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);
   L_sit SIT_TBL;


BEGIN
   if I_itemlist is not NULL and I_loc_list is not NULL then
      O_error_message := sql_lib.create_msg('TOO_MANY_PARMS','ITEMLIST','LOC_LIST',L_program);
   elsif I_itemlist is NULL and I_loc_list is NULL then
      O_error_message := sql_lib.create_msg('INV_PARAM_EITHER','I_itemlist','I_loc_list',L_program);
   end if;


   IF I_itemlist IS NOT NULL THEN
      SELECT /*+ ordered rowid(sct) use_nl(sct) */
             OBJ_SIT_REC(sct.rowid,
                         sct.conflict_itemloc_link_id,
                         sct.item,
                         sct.location,
                         sct.itemloc_link_id,
                         sct.insert_date)
        BULK COLLECT INTO L_sit
        FROM sit_conflict_temp sct,
             (SELECT /*+ INDEX(se SIT_EXPLODE_I2)+*/
                     item,
                     location,
                     itemloc_link_id
                FROM sit_explode
               WHERE skulist = i_itemlist) se
       WHERE (se.itemloc_link_id = sct.itemloc_link_id
              OR se.itemloc_link_id = sct.conflict_itemloc_link_id)
         AND se.item = sct.item
         AND se.location = sct.location FOR UPDATE OF sct.itemloc_link_id NOWAIT;
 
     SQL_LIB.SET_MARK('MERGE', 'SIT_CONFLICT', 'SIT_EXPLODE', ' ITEMLIST: '||to_char(I_loc_list));
 
      MERGE
       INTO sit_conflict sc
      USING (SELECT distinct conflict_itemloc_link_id,
                    item,
                    location,
                    itemloc_link_id,
                    insert_date
               FROM TABLE(L_sit)) sct1 
         ON (sc.item = sct1.item 
             AND sc.location = sct1.location 
             AND sc.conflict_itemloc_link_id = sct1.conflict_itemloc_link_id 
             AND sc.itemloc_link_id = sct1.itemloc_link_id)
       WHEN matched THEN
            UPDATE SET sc.insert_date = sct1.insert_date
       WHEN NOT matched THEN
            INSERT (sc.conflict_itemloc_link_id,
                    sc.item,
                    sc.location,
                    sc.itemloc_link_id,
                    sc.insert_date)
            VALUES (sct1.conflict_itemloc_link_id,
                    sct1.item,
                    sct1.location,
                    sct1.itemloc_link_id,
                    sct1.insert_date);

      -- Delete SIT_CONFLICT_TEMP Data after rebuild is completed
      DELETE FROM sit_conflict_temp
       WHERE rowid  IN (SELECT rid FROM TABLE(L_sit));                
   ELSE
      SELECT /*+ ordered rowid(sct) use_nl(sct) */
             OBJ_SIT_REC(sct.rowid,
                         sct.conflict_itemloc_link_id,
                         sct.item,
                         sct.location,
                         sct.itemloc_link_id,
                         sct.insert_date)
        BULK COLLECT INTO L_sit
        FROM sit_conflict_temp sct,
             (SELECT /*+ INDEX(se SIT_EXPLODE_I1)+*/
                     location,
                     item,
                     itemloc_link_id
                FROM sit_explode se
               WHERE   loc_list = i_loc_list) se
       WHERE (se.itemloc_link_id = sct.itemloc_link_id
              OR se.itemloc_link_id = sct.conflict_itemloc_link_id)
         AND se.location = sct.location
         AND se.item     = sct.item FOR UPDATE OF sct.itemloc_link_id NOWAIT;
 
      SQL_LIB.SET_MARK('MERGE', 'SIT_CONFLICT', 'SIT_EXPLODE', ' LOC_LIST: '||to_char(I_loc_list));
      MERGE
       INTO sit_conflict sc
      USING (SELECT distinct conflict_itemloc_link_id,
                    item,
                    location,
                    itemloc_link_id,
                    insert_date
               FROM TABLE(L_sit)) sct1
         ON (sc.item = sct1.item 
             AND sc.location = sct1.location 
             AND sc.conflict_itemloc_link_id = sct1.conflict_itemloc_link_id 
             AND sc.itemloc_link_id = sct1.itemloc_link_id)
        WHEN matched THEN
             UPDATE SET sc.insert_date = sct1.insert_date
        WHEN NOT matched THEN
             INSERT (sc.conflict_itemloc_link_id,
                     sc.item,
                     sc.location,
                     sc.itemloc_link_id,
                     sc.insert_date)
              VALUES (sct1.conflict_itemloc_link_id,
                      sct1.item,
                      sct1.location,
                      sct1.itemloc_link_id,
                      sct1.insert_date);

      -- Delete SIT_CONFLICT_TEMP Data after rebuild is completed
      DELETE FROM sit_conflict_temp
       WHERE rowid IN (SELECT rid FROM TABLE(L_sit));
   END IF;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_itemlist));
      RETURN FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;

END REBUILD_SIT_CONFLICT;
--------------------------------------------------------------------------------------------------------
FUNCTION SITMAIN_PRE_PROCESS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE) 
   RETURN BOOLEAN IS
   
   L_program           VARCHAR2(50) := 'SIT_SQL.SITMAIN_PRE_PROCESS';
   L_item              ITEM_COST_HEAD.ITEM%TYPE;
   L_supplier          ITEM_COST_HEAD.SUPPLIER%TYPE;
   L_origin_country_id ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE;
   L_country_id        ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE;
   
   cursor C_RECORD_NOT_IN_ITEM_COUNTRY is
      SELECT a.item, 
             a.country_id 
        FROM (SELECT distinct im.item,
                     mva.country_id
                FROM sit_explode se,
                     sit_detail sd,
                     item_master im,
                     mv_loc_prim_addr mva,
                     period per
               WHERE sd.status_update_date = per.vdate+1
                 AND sd.itemloc_link_id    = se.itemloc_link_id
                 AND (im.item = se.item or im.item_parent = se.item or im.item_grandparent = se.item)
                 AND im.item_level  <= im.tran_level
                 AND mva.loc = se.location
                 AND NOT EXISTS (SELECT 1
                                   FROM item_loc il
                                  WHERE se.item     = il.item
                                    AND se.location = il.loc)) a 
        WHERE NOT EXISTS (SELECT 1 
                           FROM  item_country ic
                           WHERE ic.item = a.item
                           AND   ic.country_id = a.country_id);
                          
   cursor C_RECORD_NOT_IN_ITEM_COST_HEAD is
      SELECT isc.item,
             isc.supplier,
             isc.origin_country_id,
             a.country_id
        FROM item_supp_country isc,
             (SELECT distinct im.item,
                     mva.country_id
                FROM sit_explode se,
                     sit_detail sd,
                     item_master im,
                     mv_loc_prim_addr mva,
                     period per
               WHERE sd.status_update_date = per.vdate+1
                 AND sd.itemloc_link_id    = se.itemloc_link_id
                 AND (im.item = se.item or im.item_parent = se.item or im.item_grandparent = se.item)
                 AND im.item_level  <= im.tran_level
                 AND mva.loc = se.location
                 AND NOT EXISTS (SELECT 1
                                   FROM item_loc il
                                  WHERE se.item     = il.item
                                    AND se.location = il.loc)) a
       WHERE isc.item = a.item
         AND NOT EXISTS (SELECT 1
                          FROM  item_cost_head ich
                          WHERE ich.item=isc.item
                            AND ich.supplier=isc.supplier
                            AND ich.origin_country_id=isc.origin_country_id
                            AND ich.delivery_country_id=a.country_id);
   
BEGIN

   FOR ic_rec IN C_RECORD_NOT_IN_ITEM_COUNTRY LOOP
      
      if ITEM_COUNTRY_SQL.INSERT_ITEM_COUNTRY(O_error_message,
                                              ic_rec.country_id,
                                              ic_rec.item) = FALSE then
         return FALSE;
      end if;
               
   END LOOP;
    
   FOR ich_rec IN C_RECORD_NOT_IN_ITEM_COST_HEAD LOOP
 
      if ITEM_COUNTRY_SQL.INSERT_ITEM_DLVRY_COUNTRY(O_error_message,
                                                    ich_rec.item,
                                                    ich_rec.supplier,
                                                    ich_rec.origin_country_id,
                                                    ich_rec.country_id) = FALSE then
         return FALSE;
      end if;
   END LOOP;
        
   return TRUE;
 
EXCEPTION
   WHEN OTHERS THEN
      if C_RECORD_NOT_IN_ITEM_COUNTRY%ISOPEN then
         close C_RECORD_NOT_IN_ITEM_COUNTRY;
      end if;
      if C_RECORD_NOT_IN_ITEM_COST_HEAD%ISOPEN then
         close C_RECORD_NOT_IN_ITEM_COST_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SITMAIN_PRE_PROCESS;
--------------------------------------------------------------------------------------------------------
END SIT_SQL;
/
