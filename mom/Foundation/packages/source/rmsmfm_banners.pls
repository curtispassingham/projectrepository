
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSMFM_BANNER AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
PROCEDURE ADDTOQ(O_status       OUT VARCHAR2,
                 O_text         OUT VARCHAR2,
                 I_message_type IN  BANNER_MFQUEUE.MESSAGE_TYPE%TYPE,
                 I_banner_id    IN  CHANNELS.BANNER_ID%TYPE,
                 I_channel_id   IN  CHANNELS.CHANNEL_ID%TYPE,
                 I_message      IN  CLOB);
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_STATUS_CODE   OUT VARCHAR2,
                 O_ERROR_MSG     OUT VARCHAR2, 
                 O_MESSAGE_TYPE  OUT VARCHAR2, 
                 O_MESSAGE       OUT CLOB,
                 O_banner_id     OUT NUMBER,
                 O_channel_id    OUT NUMBER);
--------------------------------------------------------------------------------
END RMSMFM_BANNER;
/
