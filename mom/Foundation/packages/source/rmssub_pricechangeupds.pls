CREATE OR REPLACE PACKAGE RMSSUB_PRICECHANGE_UPDATE AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
--                              PUBLIC GLOBALS                                --
--------------------------------------------------------------------------------

SKIP_STAGE_ITEM_LOC       VARCHAR2(1) := 'N'; 

--------------------------------------------------------------------------------
--                             PUBLIC PROTOTYPES                              --
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
FUNCTION BUILD_PRICE_CHANGE(O_error_message     OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message        IN      OBJ_PRICEEVENT_IL_TBL)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message  OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION PROCESS_VFM(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_message        IN      OBJ_PRICEEVENT_IL_TBL)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION PROCESS_STAGED_DEALS (O_ERROR_MESSAGE  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION PROCESS_STAGED_DEALS(O_ERROR_MESSAGE  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_EVENTS_TBL     IN      OBJ_PRICE_CHANGE_TBL)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
END RMSSUB_PRICECHANGE_UPDATE;
/
