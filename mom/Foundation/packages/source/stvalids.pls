
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE STORE_VALIDATE_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------
-- Function Name: EXIST
-- Purpose	: checks to see that a given store exists on the 
--		  store table.
-- Return Values: O_exist, BOOLEAN, set to TRUE if store exists
--		  on table, FALSE otherwise.
-- Calls	: NONE	
-- Created	: 22-AUG_96 by Matt Sniffen
--------------------------------------------------------------------
FUNCTION EXIST(	O_error_message	IN OUT	VARCHAR2,
		I_store		IN	NUMBER,
		O_exist		IN OUT	BOOLEAN)
	RETURN BOOLEAN;

--------------------------------------------------------------------
-- Function Name: STORE_VALID_FOR_CHAIN 
-- Purpose	: checks to see that a given store exists for a given
--                chain.  If the store exists, also returns the store
--                description
-- Return Values: O_store_is_valid, BOOLEAN, set to TRUE if store is valid
--		  for the chain, FALSE otherwise.  O_store_name returns 
--                name of valid stores
--------------------------------------------------------------------
FUNCTION STORE_VALID_FOR_CHAIN (O_error_message  IN OUT         VARCHAR2,
                                O_store_is_valid IN OUT         BOOLEAN,
                                O_store_name     IN OUT         VARCHAR2,
                                I_chain          IN             NUMBER,
                                I_store          IN             NUMBER)

RETURN BOOLEAN;
--------------------------------------------------------------------
--- Function:  GET_CHAIN_FOR_STORE
--- Purpose:   Returns the chain and chain desc for a passed store.
-----------------------------------------------------------------------
FUNCTION GET_CHAIN_FOR_STORE(O_error_message      IN OUT VARCHAR2,
                             I_store              IN     STORE.STORE%TYPE,
                             O_chain              IN OUT CHAIN.CHAIN%TYPE,
                             O_chain_name         IN OUT CHAIN.CHAIN_NAME%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: CHECK_STORE_CURRENCY
-- Purpose      : This function will check if all stores have the same currency code
--                as specified. I_stores contains distinct store ids.
---------------------------------------------------------------------------------
FUNCTION CHECK_STORE_CURRENCY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_stores          IN       LOC_TBL,
                              I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: CHECK_STORE_COUNTRY
-- Purpose      : This function will check if all stores have the same country id 
--                as specified. I_stores contains distinct store ids.
---------------------------------------------------------------------------------
FUNCTION CHECK_STORE_COUNTRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_stores          IN       LOC_TBL,
                             I_country_id      IN       COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Function Name: VALIDATE_STORES
-- Purpose      : This funciton will build a dynamic sql to validate that all 
--                I_stores passed in are valid with regard to the currency code 
--                and country code, if defined. I_stores should have no duplicates.
---------------------------------------------------------------------------------
FUNCTION VALIDATE_STORES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_stores          IN       LOC_TBL,
                         I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE,
                         I_country_id      IN       COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
END STORE_VALIDATE_SQL;
/


