CREATE OR REPLACE PACKAGE BODY CURRENCY_SQL AS

--------------------------------------------------------------------------------
--                            PRIVATE GLOBALS                                 --
--------------------------------------------------------------------------------

type CURR_RECORD is record
(
  currency_code       currencies.currency_code%type,
  currency_desc       currencies.currency_desc%type,
  currency_cost_fmt   currencies.currency_cost_fmt%type,
  currency_rtl_fmt    currencies.currency_rtl_fmt%type,
  currency_cost_dec   currencies.currency_cost_dec%type,
  currency_rtl_dec    currencies.currency_rtl_dec%type
);
type CURR_TABLE is table of CURR_RECORD
index by CURRENCIES.CURRENCY_CODE%TYPE;
LP_curr_cache      CURR_TABLE;
LP_curr_cache_ind  BOOLEAN := FALSE;

--

type CURRENCY_RATE_RECORD is record
(
  exchange_rate   CURRENCY_RATES.EXCHANGE_RATE%TYPE
);
type CURRENCY_RATE_TABLE is table of CURRENCY_RATE_RECORD
index by CURRENCY_RATES.EXCHANGE_TYPE%TYPE;

type PRIM_CURRENCY_RECORD is record
(
  rate_tbl            CURRENCY_RATE_TABLE
);
type PRIM_CURRENCY_TABLE is table of PRIM_CURRENCY_RECORD
index by CURRENCIES.CURRENCY_CODE%TYPE;

--------------------------------------------------------------------------------

LP_vdate_curr_rate_cache_date   DATE := null;
LP_vdate_curr_rate_cache        PRIM_CURRENCY_TABLE;
LP_curr_rate_cache_date         DATE := null;
LP_curr_rate_cache              PRIM_CURRENCY_TABLE;

--

LP_order_no                    ordhead.order_no%TYPE;
LP_order_curr_code             system_options.currency_code%TYPE;

LP_wh_no                       wh.wh%TYPE;
LP_wh_curr_code                system_options.currency_code%TYPE;

LP_store_no                    store.store%TYPE;
LP_store_curr_code             system_options.currency_code%TYPE;

LP_external_finisher           v_external_finisher.finisher_id%TYPE;
LP_ext_fin_curr_code           v_external_finisher.currency_code%TYPE;

LP_supplier                    sups.supplier%TYPE;
LP_supplier_curr_code          system_options.currency_code%TYPE;

LP_zone_group_id               cost_zone.zone_group_id%TYPE;
LP_zone_id                     cost_zone.zone_id%TYPE;
LP_zone_curr_code              system_options.currency_code%TYPE;
LP_rounding_ind                VARCHAR2(2) := 'Y';

--------------------------------------------------------------------------------
--                          PRIVATE PROTOTYPES                                --
--------------------------------------------------------------------------------

FUNCTION GET_CURR_EXCHG_RATE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_currency_code    IN       CURRENCIES.CURRENCY_CODE%TYPE,
                             I_effective_date   IN       DATE,
                             I_exchange_type    IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                             O_exchange_rate    OUT      CURRENCY_RATES.EXCHANGE_RATE%TYPE)
   RETURN BOOLEAN;
--
FUNCTION LOAD_CURR_CACHE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
--
FUNCTION GET_CURR_INFO(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_effective_date   IN       DATE)
   RETURN BOOLEAN;
--
FUNCTION CONVERT_MV(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_currency_value    IN       NUMBER,
                    I_currency          IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                    I_currency_out      IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                    O_currency_value    IN OUT   NUMBER,
                    I_cost_retail_ind   IN       VARCHAR2,
                    I_effective_date    IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                    I_exchange_type     IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--                          PUBLIC PROCEDURES                                 --
--------------------------------------------------------------------------------

FUNCTION CONVERT_PERM(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_currency_value      IN       NUMBER,
                      I_currency            IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                      I_currency_out        IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                      O_currency_value      IN OUT   NUMBER,
                      I_cost_retail_ind     IN       VARCHAR2,
                      I_effective_date      IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                      I_exchange_type       IN      CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                      I_in_exchange_rate    IN      CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                      I_out_exchange_rate   IN      CURRENCY_RATES.EXCHANGE_RATE%TYPE)

   RETURN BOOLEAN IS
   L_program              VARCHAR2(64)  := 'CURRENCY_SQL.CONVERT_PERM';
   L_in_exchange_rate     currency_rates.exchange_rate%TYPE := I_in_exchange_rate;
   L_out_exchange_rate    currency_rates.exchange_rate%TYPE := I_out_exchange_rate;
   L_primary_currency     system_options.currency_code%TYPE;

BEGIN

   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                       L_primary_currency) = FALSE then
      return FALSE;
   end if;

   if (L_in_exchange_rate is NULL or L_in_exchange_rate = 0) then
      if (I_currency = L_primary_currency) then 
         L_in_exchange_rate := 1;
      else
         if not CURRENCY_SQL.GET_RATE(O_error_message,
                                   L_in_exchange_rate,
                                   I_currency,
                                   I_exchange_type,
                                   I_effective_date) then
            return FALSE;
         end if;
      end if;
      ---
   end if;
   ---
   if (L_out_exchange_rate is NULL or L_out_exchange_rate = 0) then
      if (I_currency_out = L_primary_currency) then
         L_out_exchange_rate := 1;
      else
         if not CURRENCY_SQL.GET_RATE(O_error_message,
                                      L_out_exchange_rate,
                                      I_currency_out,
                                      I_exchange_type,
                                      I_effective_date) then
            return FALSE;
         end if;
      end if;
      ---
   end if;

   ---

   /* Convert the currency value to the new currency */
   O_currency_value := NVL(I_currency_value,0) * (NVL(L_out_exchange_rate,0)/
                                          NVL(L_in_exchange_rate,1));

   /* Round the currency amount to the appropriate number of decimal places */
   if ROUND_CURRENCY(O_error_message,
                     O_currency_value,
                     I_currency_out,
                     I_cost_retail_ind) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
        return FALSE;
END CONVERT_PERM;
-------------------------------------------------------------------------------------------
FUNCTION GET_NAME (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE,
                   O_currency_desc   IN OUT   CURRENCIES.CURRENCY_DESC%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'CURRENCY_SQL.GET_NAME';

BEGIN

   if LOAD_CURR_CACHE(O_error_message) = FALSE then
      return FALSE;
   end if;

   --

   if LP_curr_cache.exists(I_currency_code) then
      O_currency_desc := LP_curr_cache(I_currency_code).currency_desc;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_CURRENCY',
                                            NULL, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_NAME;
-----------------------------------------------------------------------------------
FUNCTION GET_RATE (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exchange_rate    IN OUT   CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                   I_currency_code    IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                   I_exchange_type    IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                   I_effective_date   IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE)

   RETURN BOOLEAN IS

   L_program              VARCHAR2(64)                          := 'CURRENCY_SQL.GET_RATE';
   L_exchange_type        CURRENCY_RATES.EXCHANGE_TYPE%TYPE;
   L_effective_date       CURRENCY_RATES.EFFECTIVE_DATE%TYPE;
   L_participating        BOOLEAN                               := NULL;
   L_currency_code        CURRENCY_RATES.CURRENCY_CODE%TYPE     := I_currency_code;
   L_prim_participating   BOOLEAN                               := NULL;

   L_consolidation_ind    system_options.consolidation_ind%TYPE;
   L_primary_currency     system_options.currency_code%TYPE;

BEGIN

   if SYSTEM_OPTIONS_SQL.CONSOLIDATION_IND (O_error_message,
                                            L_consolidation_ind) = FALSE then
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                       L_primary_currency) = FALSE then
      return FALSE;
   end if;
   ---
   if I_exchange_type is NULL then
      ---
      if L_consolidation_ind = 'Y' then
         L_exchange_type := 'C';
      else
         L_exchange_type := 'O';
      end if;
      ---
   else
      L_exchange_type := I_exchange_type;
   end if;
   ---
   if I_effective_date is NULL then
      L_effective_date := GET_VDATE;
   else
      L_effective_date := I_effective_date;
   end if;
   ---
   if GET_CURR_EXCHG_RATE(O_error_message,
                          L_currency_code,
                          L_effective_date,
                          L_exchange_type,
                          O_exchange_rate) = FALSE then
      return FALSE;
   end if;
   ---
   --- If exchange rate of the exchange type does not exist
   if O_exchange_rate is NULL and L_consolidation_ind = 'Y' and L_exchange_type != 'C' then
      --Look for an effective consolidated rate if consolidation indicator is Y
      L_exchange_type := 'C';
      --
      if GET_CURR_EXCHG_RATE(O_error_message,
                             L_currency_code,
                             L_effective_date,
                             L_exchange_type,
                             O_exchange_rate) = FALSE then
      return FALSE;
      end if;
   end if;
   
   if O_exchange_rate is NULL and L_exchange_type != 'O' then
      --Look for an effective operational rate
      L_exchange_type := 'O';
      --
      if GET_CURR_EXCHG_RATE(O_error_message,
                             L_currency_code,
                             L_effective_date,
                             L_exchange_type,
                             O_exchange_rate) = FALSE then
      return FALSE;
      end if;
   end if;

   if O_exchange_rate is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('EXCHANGE_RATE_NOT_EXIST',
                                            NULL, NULL, NULL);
      return FALSE;
   end if; 
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_RATE;
------------------------------------------------------------------------------------
FUNCTION GET_FORMAT (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_currency_code       IN       CURRENCIES.CURRENCY_CODE%TYPE,
                     O_currency_rtl_fmt    IN OUT   CURRENCIES.CURRENCY_RTL_FMT%TYPE,
                     O_currency_rtl_dec    IN OUT   CURRENCIES.CURRENCY_RTL_DEC%TYPE,
                     O_currency_cost_fmt   IN OUT   CURRENCIES.CURRENCY_COST_FMT%TYPE,
                     O_currency_cost_dec   IN OUT   CURRENCIES.CURRENCY_COST_DEC%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'CURRENCY_SQL.GET_FORMAT';
   L_vdate    DATE := GET_VDATE;

BEGIN

   if LOAD_CURR_CACHE(O_error_message) = FALSE then
      return FALSE;
   end if;

   --

   if LP_curr_cache.exists(I_currency_code) then
      O_currency_rtl_fmt := LP_curr_cache(I_currency_code).currency_rtl_fmt;
      O_currency_rtl_dec := LP_curr_cache(I_currency_code).currency_rtl_dec;
      O_currency_cost_fmt := LP_curr_cache(I_currency_code).currency_cost_fmt;
      O_currency_cost_dec := LP_curr_cache(I_currency_code).currency_cost_dec;
   else
      O_currency_rtl_fmt  := null;
      O_currency_rtl_dec  := null;
      O_currency_cost_fmt := null;
      O_currency_cost_dec := null;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_FORMAT;
------------------------------------------------------------------------
FUNCTION CONVERT_BY_LOCATION (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_location_in         IN       VARCHAR2,
                              I_location_type_in    IN       VARCHAR2,
                              I_zone_group_id_in    IN       COST_ZONE.ZONE_GROUP_ID%TYPE,
                              I_location_out        IN       VARCHAR2,
                              I_location_type_out   IN       VARCHAR2,
                              I_zone_group_id_out   IN       COST_ZONE.ZONE_GROUP_ID%TYPE,
                              I_currency_value      IN       NUMBER,
                              O_currency_value      IN OUT   NUMBER,
                              I_cost_retail_ind     IN       VARCHAR2,
                              I_effective_date      IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                              I_exchange_type       IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                              I_calling_program      IN      VARCHAR2  DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(64)  := 'CURRENCY_SQL.CONVERT_BY_LOCATION';
   L_currency_in            CURRENCIES.CURRENCY_CODE%TYPE;
   L_currency_out           CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate_in       ORDHEAD.EXCHANGE_RATE%TYPE;
   L_exchange_rate_out      ORDHEAD.EXCHANGE_RATE%TYPE;

BEGIN

   if (I_location_in is NULL and I_location_out is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                       NULL, NULL, NULL);
      return FALSE;
   end if;

   ---
   if I_location_in is not NULL then
      ---
      if I_location_type_in = 'O' then
         ---
         if ORDER_ATTRIB_SQL.GET_CURRENCY_RATE(O_error_message,
                                               L_currency_in,
                                               L_exchange_rate_in,
                                               I_location_in) = FALSE then
            return FALSE;
         end if;
         ---
      elsif I_location_type_in = 'I' then
         ---
         if INVC_ATTRIB_SQL.GET_CURRENCY_RATE(O_error_message,
                                              L_currency_in,
                                              L_exchange_rate_in,
                                              I_location_in) = FALSE then
            return FALSE;
         end if;
         ---
      else
         ---
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                      I_location_in,
                                      I_location_type_in,
                                      I_zone_group_id_in,
                                      L_currency_in) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   if I_location_out is not NULL then
      ---
      if I_location_type_out = 'O' then
         ---
         if ORDER_ATTRIB_SQL.GET_CURRENCY_RATE(O_error_message,
                                               L_currency_out,
                                               L_exchange_rate_out,
                                               I_location_out) = FALSE then
            return FALSE;
         end if;
         ---
      elsif I_location_type_out = 'I' then
         ---
         if INVC_ATTRIB_SQL.GET_CURRENCY_RATE(O_error_message,
                                              L_currency_out,
                                              L_exchange_rate_out,
                                              I_location_out) = FALSE then
            return FALSE;
         end if;
         ---
      else
         ---
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                      I_location_out,
                                      I_location_type_out,
                                      I_zone_group_id_out,
                                      L_currency_out) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   if (I_calling_program = 'ALC_SQL.UPDATE_STKLEDGR_LOC') then
      if CURRENCY_SQL.CONVERT(O_error_message,
                              I_currency_value,
                              L_currency_in,
                              L_currency_out,
                              O_currency_value,
                              I_cost_retail_ind,
                              I_effective_date,
                              I_exchange_type,
                              L_exchange_rate_in,
                              L_exchange_rate_out,
                              'N') = FALSE then
         return FALSE;
      end if;
   else 
      if CURRENCY_SQL.CONVERT(O_error_message, 
                                       I_currency_value, 
                                       L_currency_in, 
                                       L_currency_out, 
                                       O_currency_value, 
                                       I_cost_retail_ind, 
                                       I_effective_date, 
                                       I_exchange_type, 
                                       L_exchange_rate_in, 
                                       L_exchange_rate_out) = FALSE then 
          return FALSE; 
      end if; 
   -- 
   end if; 
   -- 
   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
        return FALSE;
END CONVERT_BY_LOCATION;
-------------------------------------------------------------------------------------------
FUNCTION CONVERT (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_currency_value      IN       NUMBER,
                  I_currency            IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  I_currency_out        IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  O_currency_value      IN OUT   NUMBER,
                  I_cost_retail_ind     IN       VARCHAR2,
                  I_effective_date      IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                  I_exchange_type       IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                  I_in_exchange_rate    IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                  I_out_exchange_rate   IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(64)  := 'CURRENCY_SQL.CONVERT';
   L_in_currency_code  currencies.currency_code%TYPE := I_currency;
   L_out_currency_code currencies.currency_code%TYPE := I_currency_out;
   L_in_exchange_rate  currency_rates.exchange_rate%TYPE := I_in_exchange_rate;
   L_out_exchange_rate currency_rates.exchange_rate%TYPE := I_out_exchange_rate;

   L_primary_currency  system_options.currency_code%TYPE;

BEGIN
   
   if (I_currency is NULL AND I_currency_out is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC', NULL, NULL, NULL);
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                       L_primary_currency) = FALSE then
      return FALSE;
   end if;
   ---
   if I_currency is NULL then
      if L_in_exchange_rate is NULL then
         L_in_exchange_rate  := 1;
      end if;
      L_in_currency_code  := L_primary_currency;
   elsif I_currency_out is NULL then
      if L_out_exchange_rate is NULL then
         L_out_exchange_rate := 1;
      end if;
      L_out_currency_code := L_primary_currency;
   end if;
   ---
   if (L_in_currency_code = L_out_currency_code)
    and I_in_exchange_rate is NULL
    and I_out_exchange_rate is NULL then
      O_currency_value := I_currency_value;
      ---
      if LP_rounding_ind = 'N' then
         O_currency_value := ROUND(O_currency_value, 10);
      else
         O_currency_value := ROUND(O_currency_value, 4);
      end if;
      LP_rounding_ind := 'Y';
      ---
      return TRUE;
   end if;
   ---

   if (L_in_exchange_rate is NULL or L_in_exchange_rate = 0) and
      (L_out_exchange_rate is NULL or L_out_exchange_rate = 0) then

      -- when neither exchange rate is give, we can use mv_currency_conversion_rates

      if CONVERT_MV(O_error_message,
                    I_currency_value,
                    L_in_currency_code,
                    L_out_currency_code,
                    O_currency_value,
                    I_cost_retail_ind,
                    I_effective_date,
                    I_exchange_type) = FALSE then
         return FALSE;
      end if;

   else

      -- when one or both exhange rates are given, we need to use 
      -- currency_rates.

      if CURRENCY_SQL.CONVERT_PERM(O_error_message,
                                   I_currency_value,
                                   L_in_currency_code,
                                   L_out_currency_code,
                                   O_currency_value,
                                   I_cost_retail_ind,
                                   I_effective_date,
                                   I_exchange_type,
                                   L_in_exchange_rate,
                                   L_out_exchange_rate) = FALSE then
         return FALSE;
      end if;
      
   end if;
   ---
   if LP_rounding_ind = 'N' then
      O_currency_value := ROUND(O_currency_value, 10);
   else
      O_currency_value := ROUND(O_currency_value, 4);
   end if;
   ---
   LP_rounding_ind := 'Y';

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
        return FALSE;
END CONVERT;
-------------------------------------------------------------------------------------------
FUNCTION CONVERT (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_currency_value    IN       NUMBER,
                  I_currency          IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  I_currency_out      IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  O_currency_value    IN OUT   NUMBER,
                  I_cost_retail_ind   IN       VARCHAR2,
                  I_effective_date    IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                  I_exchange_type     IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE)

   RETURN BOOLEAN IS

   L_program       VARCHAR2(64)  := 'CURRENCY_SQL.CONVERT';

BEGIN

  if CURRENCY_SQL.CONVERT(O_error_message,
                          I_currency_value,
                          I_currency,
                          I_currency_out,
                          O_currency_value,
                          I_cost_retail_ind,
                          I_effective_date,
                          I_exchange_type,
                          NULL,
                          NULL) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
        return FALSE;

END CONVERT;
-------------------------------------------------------------------------------------------
FUNCTION ROUND_CURRENCY (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_currency_value    IN OUT   NUMBER,
                         I_currency_out      IN       VARCHAR2,
                         I_cost_retail_ind   IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'CURRENCY_SQL.ROUND_CURRENCY';
   L_round_digits        NUMBER(2);
   L_vdate               DATE := GET_VDATE;
   L_currency_cost_dec   CURRENCIES.CURRENCY_COST_DEC%TYPE;
   L_currency_rtl_dec    CURRENCIES.CURRENCY_RTL_DEC%TYPE;

BEGIN

   if LOAD_CURR_CACHE(O_error_message) = FALSE then
      return FALSE;
   end if;

   ---
   if LP_curr_cache.exists(I_currency_out) then
      L_currency_cost_dec := LP_curr_cache(I_currency_out).currency_cost_dec;
      L_currency_rtl_dec := LP_curr_cache(I_currency_out).currency_rtl_dec;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_ROUND_DIGIT',
                                            I_currency_out,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_cost_retail_ind = 'C' then
      L_round_digits := L_currency_cost_dec;
   elsif I_cost_retail_ind in ('R','P') then
      L_round_digits := L_currency_rtl_dec;
   else
      L_round_digits :=  4;
   end if;
   ---
   if LP_rounding_ind = 'N' then
      L_round_digits := 10;
   else
      if I_cost_retail_ind = 'C' then
         L_round_digits := L_currency_cost_dec;
      elsif I_cost_retail_ind in ('R','P') then
         L_round_digits := L_currency_rtl_dec;
      else
         L_round_digits :=  4;
      end if;
   end if;
   ---
   O_currency_value := ROUND(O_currency_value, L_round_digits);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ROUND_CURRENCY;
--------------------------------------------------------------------
FUNCTION GET_CURR_LOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_location        IN       VARCHAR2,
                       I_location_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                       I_zone_group_id   IN       COST_ZONE.ZONE_GROUP_ID%TYPE,
                       O_currency_code   IN OUT   CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64)  := 'CURRENCY_SQL.GET_CURR_LOC';
   L_cursor        VARCHAR2(20)  := NULL;
   L_error_message VARCHAR2(255) := NULL;
   L_exchange_rate         invc_head.exchange_rate%TYPE;

   cursor C_PARTNER is
      select currency_code
        from partner
       where partner_id = I_location
         and partner_type = I_location_type;

   cursor C_CODE_DECODE is
      select 'x'
        from code_detail
       where code = I_location_type
         and code_type = 'PTAL';

   cursor C_WAREHOUSE is
      select wh.currency_code
        from wh
       where wh.wh = I_location;

   cursor C_STORE is
      select store.currency_code
        from store
       where store.store = I_location;

   cursor C_SUPPLIER is
      select sups.currency_code
        from sups
       where sups.supplier = I_location;

   cursor C_ORDER is
      select ordhead.currency_code
        from ordhead
       where ordhead.order_no = I_location;

   cursor C_CONTRACT is
      select contract_header.currency_code
        from contract_header
       where contract_header.contract_no = I_location;

   cursor C_COST_ZONE is
      select cost_zone.currency_code
        from cost_zone
       where cost_zone.zone_id       = I_location
         and cost_zone.zone_group_id = I_zone_group_id;

   cursor C_DEAL is
      select currency_code
        from deal_head
       where deal_id = I_location;

BEGIN
   if I_location = '0' or I_location is null then
      if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(L_error_message,
                                          O_currency_code) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_LOCATION',
                                               I_location_type, I_location, I_zone_group_id);
         return FALSE;
      end if;
   else
      if I_location_type = 'W' then
         if I_location = LP_wh_no then
            O_currency_code := LP_wh_curr_code;
         else
            SQL_LIB.SET_MARK('OPEN', 'C_WAREHOUSE', 'WH', NULL);
            open C_WAREHOUSE;
            SQL_LIB.SET_MARK('FETCH', 'C_WAREHOUSE', 'WH', NULL);
            fetch C_WAREHOUSE into O_currency_code;
            if C_WAREHOUSE%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_LOCATION',
                                                     I_location_type, I_location, I_zone_group_id);
               SQL_LIB.SET_MARK('CLOSE', 'C_WAREHOUSE', 'WH', NULL);
               close C_WAREHOUSE;
               return FALSE;
            end if;
            SQL_LIB.SET_MARK('CLOSE', 'C_WAREHOUSE', 'WH', NULL);
            close C_WAREHOUSE;

            LP_wh_no := I_location;
            LP_wh_curr_code := O_currency_code;

         end if;
      --External Finishers
      elsif I_location_type = 'E' then
         if I_location = LP_external_finisher then
            O_currency_code := LP_ext_fin_curr_code;
         else
            SQL_LIB.SET_MARK('OPEN', 'C_partner', 'EXTERNAL FINISHER', NULL);
            open C_PARTNER;
            SQL_LIB.SET_MARK('FETCH', 'C_partner', 'EXTERNAL FINISHER', NULL);
            fetch C_PARTNER into O_currency_code;
            if C_PARTNER%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_LOCATION',
                                                     I_location_type, I_location, I_zone_group_id);
               SQL_LIB.SET_MARK('CLOSE', 'C_PARTNER', 'EXTERNAL FINISHER', NULL);
               close C_PARTNER;
               return FALSE;
            end if;
            SQL_LIB.SET_MARK('CLOSE', 'C_PARTNER', 'EXTERNAL FINISHER', NULL);
            close C_PARTNER;

            LP_external_finisher := I_location;
            LP_ext_fin_curr_code := O_currency_code;
         end if;      
      elsif I_location_type = 'S' then
         if I_location = LP_store_no then
            O_currency_code := LP_store_curr_code;
         else
            SQL_LIB.SET_MARK('OPEN', 'C_store', 'STORE', NULL);
            open C_STORE;
            SQL_LIB.SET_MARK('FETCH', 'C_store', 'STORE', NULL);
            fetch C_STORE into O_currency_code;
            if C_STORE%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_LOCATION',
                                                     I_location_type, I_location, I_zone_group_id);
               SQL_LIB.SET_MARK('CLOSE', 'C_STORE', 'STORE', NULL);
               close C_STORE;
               return FALSE;
            end if;
            SQL_LIB.SET_MARK('CLOSE', 'C_STORE', 'STORE', NULL);
            close C_STORE;

            LP_store_no := I_location;
            LP_store_curr_code := O_currency_code;

         end if;
      elsif I_location_type = 'V' then
         if I_location = LP_supplier then
            O_currency_code := LP_supplier_curr_code;
         else
            SQL_LIB.SET_MARK('OPEN', 'C_SUPPLIER', 'SUPS', NULL);
            open C_SUPPLIER;
            SQL_LIB.SET_MARK('FETCH', 'C_SUPPLIER', 'SUPS', NULL);
            fetch C_SUPPLIER into O_currency_code;
            if C_SUPPLIER%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_LOCATION',
                                                     I_location_type, I_location, I_zone_group_id);
               SQL_LIB.SET_MARK('CLOSE', 'C_SUPPLIER', 'SUPS', NULL);
               close C_SUPPLIER;
               return FALSE;
            end if;
            SQL_LIB.SET_MARK('CLOSE', 'C_SUPPLIER', 'SUPS', NULL);
            close C_SUPPLIER;

            LP_supplier  := I_location;
            LP_supplier_curr_code := O_currency_code;

         end if;
      elsif I_location_type = 'O' then
         if I_location = LP_order_no then
            O_currency_code := LP_order_curr_code;
         else
            SQL_LIB.SET_MARK('OPEN', 'C_ORDER', 'ORDHEAD', NULL);
            open C_ORDER;
            SQL_LIB.SET_MARK('FETCH', 'C_ORDER', 'ORDHEAD', NULL);
            fetch C_ORDER into O_currency_code;
            if C_ORDER%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_LOCATION',
                                                     I_location_type, I_location, I_zone_group_id);
               SQL_LIB.SET_MARK('CLOSE', 'C_ORDER', 'ORDHEAD', NULL);
               close C_ORDER;
               return FALSE;
            end if;
            SQL_LIB.SET_MARK('CLOSE', 'C_ORDER', 'ORDHEAD', NULL);
            close C_ORDER;

            LP_order_no := I_location;
            LP_order_curr_code := O_currency_code;

         end if;

      elsif I_location_type = 'I' then
         ---
         if INVC_ATTRIB_SQL.GET_CURRENCY_RATE(O_error_message,
                                              O_currency_code,
                                              L_exchange_rate,
                                              I_location) = FALSE then
            return FALSE;
         end if;
         ---
      elsif I_location_type = 'C' then
         SQL_LIB.SET_MARK('OPEN', 'C_CONTRACT', 'CONTRACT_HEADER', NULL);
         open C_CONTRACT;
         SQL_LIB.SET_MARK('FETCH', 'C_CONTRACT', 'CONTRACT_HEADER', NULL);
         fetch C_CONTRACT into O_currency_code;
         if C_CONTRACT%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_LOCATION',
                                                  I_location_type, I_location, I_zone_group_id);
            SQL_LIB.SET_MARK('CLOSE', 'C_CONTRACT', 'CONTRACT_HEADER', NULL);
            close C_CONTRACT;
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE', 'C_CONTRACT', 'CONTRACT_HEADER', NULL);
         close C_CONTRACT;
      elsif I_location_type = 'L' then
         SQL_LIB.SET_MARK('OPEN', 'C_COST_ZONE', 'COST_ZONE', NULL);
         open C_COST_ZONE;
         SQL_LIB.SET_MARK('FETCH', 'C_COST_ZONE', 'COST_ZONE', NULL);
         fetch C_COST_ZONE into O_currency_code;
         if C_COST_ZONE%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_LOCATION',
                                                  I_location_type, I_location, I_zone_group_id);
            SQL_LIB.SET_MARK('CLOSE','C_COST_ZONE', 'COST_ZONE' , NULL);
            close C_COST_ZONE;
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE','C_COST_ZONE', 'COST_ZONE' , NULL);
         close C_COST_ZONE;
      elsif I_location_type = 'D' then
         SQL_LIB.SET_MARK('OPEN', 'C_DEAL', 'DEAL_HEAD', 'DEAL: '||I_location);
         open C_DEAL;
         SQL_LIB.SET_MARK('FETCH', 'C_DEAL', 'DEAL_HEAD', 'DEAL: '||I_location);
         fetch C_DEAL into O_currency_code;
         ---
         if C_DEAL%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_LOCATION',
                                                  I_location_type, I_location, I_zone_group_id);
            SQL_LIB.SET_MARK('CLOSE', 'C_DEAL', 'DEAL_HEAD', 'DEAL: '||I_location);
            close C_DEAL;
            return FALSE;
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_DEAL', 'DEAL_HEAD', 'DEAL: '||I_location);
         close C_DEAL;
      else
         SQL_LIB.SET_MARK('OPEN', 'C_CODE_DECODE', 'CODE_DETAIL', NULL);
         open C_CODE_DECODE;
         SQL_LIB.SET_MARK('FETCH', 'C_CODE_DECODE', 'CODE_DETAIL', NULL);
         fetch C_CODE_DECODE into O_currency_code;
         if C_CODE_DECODE%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('ERROR_CURR_LOCATION',
                                                  I_location_type, I_location, I_zone_group_id);
            SQL_LIB.SET_MARK('CLOSE', 'C_CODE_DECODE', 'CODE_DETAIL', NULL);
            close C_CODE_DECODE;
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE', 'C_CODE_DECODE', 'CODE_DETAIL', NULL);
         close C_CODE_DECODE;

         SQL_LIB.SET_MARK('OPEN', 'C_PARTNER', 'PARTNER', NULL);
         open C_PARTNER;
         SQL_LIB.SET_MARK('FETCH', 'C_PARTNER', 'PARTNER', NULL);
         fetch C_PARTNER into O_currency_code;
         if C_PARTNER%NOTFOUND then
            SQL_LIB.SET_MARK('CLOSE', 'C_PARTNER', 'PARTNER', NULL);
            close C_PARTNER;
            O_error_message := SQL_LIB.CREATE_MSG('INV_CURRENCY',
                                                   NULL, NULL, NULL);
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE', 'C_PARTNER', 'PARTNER', NULL);
         close C_PARTNER;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
        return FALSE;
END GET_CURR_LOC;
-------------------------------------------------------------------
FUNCTION EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
               I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE,
               O_exists          IN OUT   BOOLEAN)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)  := 'CURRENCY_SQL.EXIST';
   L_vdate              DATE := GET_VDATE;

BEGIN


   if LOAD_CURR_CACHE(O_error_message) = FALSE then
      return FALSE;
   end if;

   --

   if LP_curr_cache.exists(I_currency_code) then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
        return FALSE;
END EXIST;
-------------------------------------------------------------------------------------------
FUNCTION CHECK_EMU_COUNTRIES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_participating_ind   IN OUT   BOOLEAN,
                             I_currency            IN       SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN IS

   L_vdate                DATE := GET_VDATE;
   L_euro_exchange_rate   EURO_EXCHANGE_RATE.EXCHANGE_RATE%TYPE;

BEGIN

   O_participating_ind := FALSE;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'CURRENCY_SQL.CHECK_EMU_COUNTRIES',
                                               to_char(SQLCODE));
        return FALSE;
END CHECK_EMU_COUNTRIES;
----------------------------------------------------------------------------------
FUNCTION CHECK_EMU_COUNTRIES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_participating_ind   IN OUT   BOOLEAN,
                             I_currency            IN       SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                             I_effective_date      IN       DATE)
   RETURN BOOLEAN IS


BEGIN

   if CURRENCY_SQL.CHECK_EMU_COUNTRIES(O_error_message,
                                       O_participating_ind,
                                       I_currency) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'CURRENCY_SQL.CHECK_EMU_COUNTRIES',
                                               to_char(SQLCODE));
        return FALSE;
END CHECK_EMU_COUNTRIES;
----------------------------------------------------------------------------------
FUNCTION CONVERT_VALUE(I_cost_retail_ind   IN   VARCHAR2,
                       I_currency_out      IN   CURRENCIES.CURRENCY_CODE%TYPE,
                       I_currency_in       IN   CURRENCIES.CURRENCY_CODE%TYPE,
                       I_currency_value    IN   NUMBER)
   RETURN NUMBER IS

   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
   L_currency_out      NUMBER;

BEGIN

   if CURRENCY_SQL.CONVERT(L_error_message,
                           I_currency_value,
                           I_currency_in,
                           I_currency_out,
                           L_currency_out,
                           I_cost_retail_ind,
                           NULL,    -- Effective date
                           NULL) = FALSE then
      raise_application_error(-20000, 'Currency conversion error');
   end if;

   return L_currency_out;

EXCEPTION
   when OTHERS then
      raise;

END CONVERT_VALUE;

--------------------------------------------------------------------------------
--                          PRIVATE PROCEDURES                                --
--------------------------------------------------------------------------------

FUNCTION GET_CURR_EXCHG_RATE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_currency_code    IN       CURRENCIES.CURRENCY_CODE%TYPE,
                             I_effective_date   IN       DATE,
                             I_exchange_type    IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                             O_exchange_rate    OUT      CURRENCY_RATES.EXCHANGE_RATE%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'CURRENCY_SQL.GET_CURR_EXCHG_RATE';
  
BEGIN

   if GET_CURR_INFO(O_error_message,
                    I_effective_date) = FALSE then
      return FALSE;
   end if;

   --

   O_exchange_rate := null;

   if I_effective_date = LP_vdate_curr_rate_cache_date then
      if LP_vdate_curr_rate_cache.exists(I_currency_code) then
         if LP_vdate_curr_rate_cache(I_currency_code).rate_tbl.exists(I_exchange_type) then
            O_exchange_rate := LP_vdate_curr_rate_cache(I_currency_code).rate_tbl(I_exchange_type).exchange_rate;     
         end if;
      end if;
   elsif I_effective_date = LP_curr_rate_cache_date then
      if LP_curr_rate_cache.exists(I_currency_code) then
         if LP_curr_rate_cache(I_currency_code).rate_tbl.exists(I_exchange_type) then
            O_exchange_rate := LP_curr_rate_cache(I_currency_code).rate_tbl(I_exchange_type).exchange_rate;     
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_CURR_EXCHG_RATE;
--------------------------------------------------------------------------------
FUNCTION GET_CURR_INFO(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_effective_date   IN       DATE)
   RETURN BOOLEAN IS

L_program               VARCHAR2(100) := 'CURRENCY_SQL.GET_CURR_INFO';
L_vdate                 DATE := GET_VDATE;
L_get_vdate_curr_info   BOOLEAN := FALSE;
L_get_curr_info         BOOLEAN := FALSE;

cursor C_CURRENCY(I_date DATE) is
select /*+ INDEX(c,PK_CURRENCIES) INDEX(cr, PK_CURRENCY_RATES) */  
       distinct c.currency_code,
                --
                first_value(cr.exchange_rate) over
                (partition by c.currency_code, cr.exchange_type order by cr.effective_date desc) exchange_rate,
                cr.exchange_type
  from currencies c,
       currency_rates cr
 where cr.effective_date (+) <= I_date
   and cr.currency_code (+) = c.currency_code;

BEGIN

   if LP_vdate_curr_rate_cache_date is null then
      L_get_vdate_curr_info := TRUE;
   else
      if LP_vdate_curr_rate_cache_date != L_vdate then
         L_get_vdate_curr_info := TRUE;
      end if;
   end if;

   if LP_curr_rate_cache_date is null then
      if I_effective_date != L_vdate then
         L_get_curr_info := TRUE;
      end if;
   else
      if LP_curr_rate_cache_date != I_effective_date then
         L_get_curr_info := TRUE;
      end if;
   end if;

   if L_get_vdate_curr_info then
      LP_vdate_curr_rate_cache.delete;
      for rec in C_CURRENCY(L_vdate) loop
         if rec.exchange_type is not null then
            LP_vdate_curr_rate_cache(rec.currency_code).rate_tbl(rec.exchange_type).exchange_rate := rec.exchange_rate;
         end if;
      end loop;
      LP_vdate_curr_rate_cache_date := L_vdate;
   end if;

   if L_get_curr_info then
      LP_curr_rate_cache.delete;
      for rec in C_CURRENCY(I_effective_date) loop
         if rec.exchange_type is not null then
            LP_curr_rate_cache(rec.currency_code).rate_tbl(rec.exchange_type).exchange_rate := rec.exchange_rate;
         end if;
      end loop;
      LP_curr_rate_cache_date := I_effective_date; 
   end if;

/*is:empty raise error*/
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_CURR_INFO;
--------------------------------------------------------------------------------
FUNCTION CONVERT (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_currency_value      IN       NUMBER,
                  I_currency            IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  I_currency_out        IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  O_currency_value      IN OUT   NUMBER,
                  I_cost_retail_ind     IN       VARCHAR2,
                  I_effective_date      IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                  I_exchange_type       IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                  I_in_exchange_rate    IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                  I_out_exchange_rate   IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                  I_rounding_ind        IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program       VARCHAR2(64)  := 'CURRENCY_SQL.CONVERT';

BEGIN
  LP_rounding_ind := I_rounding_ind;
  ---
  if CURRENCY_SQL.CONVERT(O_error_message,
                          I_currency_value,
                          I_currency,
                          I_currency_out,
                          O_currency_value,
                          I_cost_retail_ind,
                          I_effective_date,
                          I_exchange_type,
                          I_in_exchange_rate,
                          I_out_exchange_rate) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CONVERT;
--------------------------------------------------------------------------------
FUNCTION LOAD_CURR_CACHE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)  := 'CURRENCY_SQL.LOAD_CURR_CACHE';

   cursor c_euro is
      select c.currency_code,
             vc.currency_desc,
             c.currency_cost_fmt,
             c.currency_rtl_fmt,
             c.currency_cost_dec,
             c.currency_rtl_dec
        from currencies c,
             v_currencies_tl vc
       where vc.currency_code = c.currency_code;

BEGIN

   if LP_curr_cache_ind = FALSE then
      for rec in c_euro loop
         LP_curr_cache(rec.currency_code).currency_code     := rec.currency_code;
         LP_curr_cache(rec.currency_code).currency_desc     := rec.currency_desc;
         LP_curr_cache(rec.currency_code).currency_cost_fmt := rec.currency_cost_fmt;
         LP_curr_cache(rec.currency_code).currency_rtl_fmt  := rec.currency_rtl_fmt;
         LP_curr_cache(rec.currency_code).currency_cost_dec := rec.currency_cost_dec;
         LP_curr_cache(rec.currency_code).currency_rtl_dec  := rec.currency_rtl_dec;
      end loop;
      LP_curr_cache_ind := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOAD_CURR_CACHE;
--------------------------------------------------------------------------------
FUNCTION CONVERT_MV(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_currency_value    IN       NUMBER,
                    I_currency          IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                    I_currency_out      IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                    O_currency_value    IN OUT   NUMBER,
                    I_cost_retail_ind   IN       VARCHAR2,
                    I_effective_date    IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                    I_exchange_type     IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)  := 'CURRENCY_SQL.CONVERT_MV';

   L_exchange_type        CURRENCY_RATES.EXCHANGE_TYPE%TYPE;
   L_consolidation_ind    system_options.consolidation_ind%TYPE;
   L_vdate                DATE := get_vdate;
   L_effective_date       DATE := I_effective_date;
   L_exchange_rate        mv_currency_conversion_rates.exchange_rate%TYPE := null;
   L_exists               BOOLEAN := TRUE;

   cursor c_convert is
      select I_currency_value * exchange_rate
        from mv_currency_conversion_rates
       where from_currency = I_currency
         and to_currency   = I_currency_out
         and exchange_type = L_exchange_type
         and effective_date <= L_effective_date
      order by effective_date desc;


BEGIN
   
   if SYSTEM_OPTIONS_SQL.CONSOLIDATION_IND (O_error_message,
                                            L_consolidation_ind) = FALSE then
      return FALSE;
   end if;

   if I_exchange_type is NULL then
      if L_consolidation_ind = 'Y' then
         L_exchange_type := 'C';
      else
         L_exchange_type := 'O';
      end if;
   else
      L_exchange_type := I_exchange_type;
   end if;
   ---
   if L_effective_date is NULL then
      L_effective_date := L_vdate;
   end if;

   open c_convert;
   fetch c_convert into O_currency_value;
   if c_convert%NOTFOUND then
      L_exists := FALSE;
      close c_convert;
   end if;
   ---
   if NOT L_exists then
      -- If the exchange_rate is not found, and the exchange_type is neither
      -- consolidation nor operational, then retrieve the consolidation or
      -- operational exchange_rate based on system_options.consolidation_ind.
      -- In case consolidation exchange rate is not found, retreive the 
      -- operation exchange rate as the last resort. 
      if L_consolidation_ind = 'Y' and L_exchange_type != 'C' then
         L_exchange_type := 'C';
      else
         L_exchange_type := 'O';
      end if;   
      ---
      open c_convert;
      fetch c_convert into O_currency_value;
      if c_convert%FOUND then
         L_exists := TRUE;
      end if;
      close c_convert;
      ---
      if NOT L_exists AND L_consolidation_ind = 'Y' and L_exchange_type = 'C' then
         L_exchange_type := 'O';
         open c_convert;
         fetch c_convert into O_currency_value;
         if c_convert%FOUND then
            L_exists := TRUE;
         end if;
         close c_convert;
      end if;   
   end if;
   ---
   if NOT L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('EXCHANGE_RATE_NOT_EXIST',
                                             NULL, NULL, NULL);
      return FALSE;
   end if;
   --
   if ROUND_CURRENCY(O_error_message,
                     O_currency_value,
                     I_currency_out,
                     I_cost_retail_ind) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CONVERT_MV;
--------------------------------------------------------------------------------
END CURRENCY_SQL;
/

