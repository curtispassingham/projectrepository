
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_PRICECHANGE AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
--                         PUBLIC GLOBAL VARIABLES                            --
--------------------------------------------------------------------------------

PERM_PC     CONSTANT VARCHAR2(2) := 'PC';
PROM_ST     CONSTANT VARCHAR2(2) := 'PS';
PROM_END    CONSTANT VARCHAR2(2) := 'PE';
CLEARANCE   CONSTANT VARCHAR2(2) := 'CL';
CL_RESET    CONSTANT VARCHAR2(2) := 'CR';


--------------------------------------------------------------------------------
--                         PUBLIC PROTOTYPES                                  --
--------------------------------------------------------------------------------

PROCEDURE CONSUME(O_status_code   IN OUT VARCHAR2,
                  O_error_message IN OUT VARCHAR2,
                  I_message       IN     OBJ_PRICEEVENT_IL_TBL);

PROCEDURE PROCESS_STAGED_DEALS (O_status_code   IN OUT VARCHAR2, 
                                O_ERROR_MESSAGE IN OUT VARCHAR2);

PROCEDURE PROCESS_STAGED_DEALS (O_status_code   IN OUT VARCHAR2, 
                                O_ERROR_MESSAGE IN OUT VARCHAR2,
                                I_EVENTS_TBL    IN OBJ_PRICE_CHANGE_TBL);
                                
--------------------------------------------------------------------------------

END RMSSUB_PRICECHANGE;
/


