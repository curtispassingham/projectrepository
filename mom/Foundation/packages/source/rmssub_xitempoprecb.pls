CREATE OR REPLACE PACKAGE BODY RMSSUB_XITEM_POP_RECORD AS
----------------------------------------------------------------------------
----------------------------------------------------------------------------
   LP_im_row                ITEM_MASTER%ROWTYPE;
   LP_isup_row              ITEM_SUPPLIER%ROWTYPE;
   LP_isc_row               ITEM_SUPP_COUNTRY%ROWTYPE;
   LP_ismc_row              ITEM_SUPP_MANU_COUNTRY%ROWTYPE;
   LP_iscl_row              ITEM_SUPP_COUNTRY_LOC%ROWTYPE;
   LP_iscd_row              ITEM_SUPP_COUNTRY_DIM%ROWTYPE;

   LP_system_options_row    SYSTEM_OPTIONS%ROWTYPE;

   LP_mod_isup_tbl          RMSSUB_XITEM.ISUP_TBLTYPE;
   LP_mod_isc_tbl           RMSSUB_XITEM.ISC_TBLTYPE;
   LP_mod_ismc_tbl          RMSSUB_XITEM.ISMC_TBLTYPE;
   LP_pi_tbl                RMSSUB_XITEM.PACKITEM_TBLTYPE;

   LP_isc_hier_level        VARCHAR2(2);
   LP_izp_hier_level        VARCHAR2(2);
   LP_pack_type             VARCHAR2(1) := 'N';
   LP_buyer_pack            BOOLEAN := FALSE;
   LP_wksht_flag            BOOLEAN := FALSE;
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
-- Function Name: POP_BUYER_PACK
-- Purpose      : This function populates the package global variabel LP_buyer_pack for orderable
--                buyer packs.  This is needed for isc and iscl validating of unit_cost.  If the item
--                is an orderable buyer pack the unit_cost needs to be calculated from the component
--                costs.
-------------------------------------------------------------------------------------------------------
FUNCTION POP_BUYER_PACK(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pack_type         IN       ITEM_MASTER.PACK_TYPE%TYPE,
                        I_orderable         IN       ITEM_MASTER.ORDERABLE_IND%TYPE)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_ISC_ISMC
-- Purpose      : This function validates required fields for the item_supplier node and validates
--                only 1 primary supplier (primary_supp_ind).
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ISC_ISMC(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_isup_rows            IN       "RIB_XItemSupDesc_TBL",
                           I_sups                 IN       "RIB_XItemSupRef_TBL",
                           I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                           I_dept                 IN       ITEM_MASTER.DEPT%TYPE,
                           I_message_type         IN       VARCHAR2,
                           I_item_level           IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                           I_tran_level           IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                           I_aip_case_type        IN       ITEM_MASTER.AIP_CASE_TYPE%TYPE,
                           I_standard_uom         IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                           I_orderable_ind        IN       ITEM_MASTER.ORDERABLE_IND%TYPE)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--Function Name: POPULATE_ISC_TABLE
-- Purpose      : This function will check that there is only 1 primary country (primary_country_ind). It
--                also validates that buyer packs have no associated unit_cost value and required fields.
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ISC_TABLE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_isc_rows            IN       "RIB_XItemSupCtyDesc_TBL",
                            I_countries           IN       "RIB_XItemSupCtyRef_TBL",
                            I_item                IN       ITEM_MASTER.ITEM%TYPE,
                            I_supplier            IN       SUPS.SUPPLIER%TYPE,
                            I_message_type        IN       VARCHAR2)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--Function Name: POPULATE_ISMC_TABLE
-- Purpose      : This function will check that there is only 1 primary country of manufacture
--                (primary_manu_ctry_ind = 'Y'). It also validates required fields.
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ISMC_TABLE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_ismc_rows           IN       "RIB_XItmSupCtyMfrDesc_TBL",
                             I_countries           IN       "RIB_XItemSupCtyMfrRef_TBL",
                             I_item                IN       ITEM_MASTER.ITEM%TYPE,
                             I_supplier            IN       SUPS.SUPPLIER%TYPE,
                             I_message_type        IN       VARCHAR2)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: VALID_HIER_LEVEL
-- Purpose      : This function will check the message for the valid values of hier_level.
-------------------------------------------------------------------------------------------------------
FUNCTION VALID_HIER_LEVEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hier_level        IN       VARCHAR2)
   RETURN BOOLEAN;

------------------------------------------------------------------------------------------------------
-- Function Name: ORGHIER_EXISTS
-- Purpose      : This function calls ORGANIZATION_VALIDATE_SQL.EXIST to check existance of hierarchy
--                ids above the store and warehouse level.
-------------------------------------------------------------------------------------------------------
FUNCTION ORGHIER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_message         IN       "RIB_XItemDesc_REC",
                        I_del_message     IN       "RIB_XItemRef_REC",
                        I_node            IN       VARCHAR2,
                        I_hier_level      IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_PACKITEM_TABLE
-- Purpose      : This function populates the packitem collection.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_PACKITEM_TABLE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_message         IN       "RIB_XItemDesc_REC",
                                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: POP_SYSTEM_OPTIONS
-- Purpose      : This function populates the system_options row for all functions to use.
-------------------------------------------------------------------------------------------------------
FUNCTION POP_SYSTEM_OPTIONS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: ISC_DEFAULTS
-- Purpose      : This function will populate the item supplier country record with defaults.
-------------------------------------------------------------------------------------------------------
FUNCTION ISC_DEFAULTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_create_date     IN       DATE)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: ISCL_DEFAULTS
-- Purpose      : This function will populate the item supplier country location record with defaults.
---------------------------------------------------------------------------------------------------------
FUNCTION ISCL_DEFAULTS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_create_date         IN       DATE)

   RETURN BOOLEAN ;
   
------------------------------------------------------------------------------------------------------------
-- Function Name: ISCD_DEFAULTS
-- Purpose      : This function will populate the item supplier country dimension record with defaults.
-------------------------------------------------------------------------------------------------------
FUNCTION ISCD_DEFAULTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_create_date     IN       DATE)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: ISUP_DEFAULTS
-- Purpose      : This function will populate the item supplier record with defaults.
-------------------------------------------------------------------------------------------------------
FUNCTION ISUP_DEFAULTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_create_date     IN       DATE)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_IM
-- Purpose      : This function will populate the item master record with values from the message
--                and with defaults.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_IM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_message         IN       "RIB_XItemDesc_REC",
                     I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_RECORD
-- Purpose      : This function will populate all the fields in the RMSSUB_XITEM.ITEM_API_REC with
--                the values from the delete RIB message.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_rec        IN OUT   RMSSUB_XITEM.ITEM_API_DEL_REC,
                         I_message         IN       "RIB_XItemRef_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_RECORD
-- Purpose      : This function will populate all the fields in the RMSSUB_XITEM.ITEM_API_REC with
--                the values from the create/modify RIB message.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_rec        IN OUT   RMSSUB_XITEM.ITEM_API_REC,
                         I_message         IN       "RIB_XItemDesc_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_LOCS
-- Purpose      : This function will populate all the fields in the RMSSUB_XITEM.ISCL_TBL with
--                the values from the create/modify RIB message and add all locations.
----------------------------------------------------------------------------------------------------
FUNCTION POPULATE_LOCS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_iscl_tbl            OUT      RMSSUB_XITEM.ISCL_TBLTYPE,
                       I_iscl_rows           IN       "RIB_XISCLocDesc_TBL",
                       I_supplier            IN       SUPS.SUPPLIER%TYPE,
                       I_country             IN       COUNTRY.COUNTRY_ID%TYPE,
                       I_unit_cost           IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE, --- isc default
                       I_item                IN       ITEM_MASTER.ITEM%TYPE,
                       I_message_type        IN       VARCHAR2)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_DEL_LOCS
-- Purpose      : This function will populate all the fields in the RMSSUB_XITEM.ISCL_DEL_TBL with
--                the values from the delete RIB message and add all locations.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DEL_LOCS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_item_rec            OUT      RMSSUB_XITEM.ITEM_API_DEL_REC,
                           I_isup_rows           IN       "RIB_XItemSupRef_TBL",
                           I_item                IN       ITEM_MASTER.ITEM%TYPE,
                           I_message_type        IN       VARCHAR2)

   RETURN BOOLEAN;

------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_ZONES
-- Purpose      : This function will populate all the fields in the RMSSUB_XITEM.IZP_TBL with
--                the values from the create/modify RIB message and add all locations.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ZONES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_izp_tbl         OUT      RMSSUB_XITEM.IZP_TBLTYPE,
                        I_izp_rows        IN       "RIB_XIZPDesc_TBL",
                        I_standard_uom    IN       ITEM_MASTER.STANDARD_UOM%TYPE)

   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_ITEM_INFO
-- Purpose      : This function calls ITEM_ATTRIB_SQL.GET_API_INFO to populate the LP_im_row
--                that is used for validation of adding updating child nodes.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE,
                       I_message         IN       "RIB_XItemDesc_REC"  DEFAULT NULL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: PRIMARY_COUNTRY
-- Purpose      : This function returns the primary country for an item/supplier and an exists param.
---------------------------------------------------------------------------------------------------------
FUNCTION PRIMARY_COUNTRY(O_error_message   IN OUT   VARCHAR2,
                         O_exists          IN OUT   BOOLEAN,
                         O_prim_country    IN OUT   ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_item            IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                         I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: PRIMARY_SUPPLIER
-- Purpose      : This function returns the primary supplier for an item and an exists param.
---------------------------------------------------------------------------------------------------------
FUNCTION PRIMARY_SUPPLIER(O_error_message   IN OUT   VARCHAR2,
                          O_exists          IN OUT   BOOLEAN,
                          O_prim_supplier   IN OUT   SUPS.SUPPLIER%TYPE,
                          I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: WH_QUERY
-- Purpose      : This function builds a warehouse query for locations/zones.
---------------------------------------------------------------------------------------------------------
FUNCTION WH_QUERY(O_error_message   IN OUT   VARCHAR2,
                  O_subquery        IN OUT   VARCHAR2,
                  I_hier_id         IN       VARCHAR2)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: RESET_GLOBALS
-- Purpose      : This function clears globals or resets default values.
---------------------------------------------------------------------------------------------------------
FUNCTION RESET_GLOBALS(O_error_message   IN OUT   VARCHAR2)

   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_TRANS_LANG
-- Purpose      : This function will populate all the fields in the Lang node of RMSSUB_XITEM.ITEM_API_REC
--                record (Item_Master_lang, Item_Supp_lang and Item_Images_lang) with translated values
--                from the create, modify or delete RIB message.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_TRANS_LANG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_item_rec        IN OUT   RMSSUB_XITEM.ITEM_API_REC,
                             O_item_del_rec    IN OUT   RMSSUB_XITEM.ITEM_API_DEL_REC,
                             I_message_desc    IN       "RIB_XItemDesc_REC",
                             I_message_ref     IN       "RIB_XItemRef_REC",
                             I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                  IO_item_rec       IN OUT NOCOPY   RMSSUB_XITEM.ITEM_API_REC,
                  IO_message        IN OUT          "RIB_XItemDesc_REC",
                  I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)  := 'RMSSUB_XITEM_POP_RECORD.POPULATE';
   L_exists          BOOLEAN       := FALSE;
   L_parent_id       NUMBER;
   L_item_rec        RMSSUB_XITEM.ITEM_API_REC;

BEGIN

   if RESET_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;

   if I_message_type IN (RMSSUB_XITEM.ISCL_ADD) then
      if IO_message.iscloc_hier_level is null then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'hier_level', NULL, NULL);
         return FALSE;
      end if;
   end if;
   
    if I_message_type IN (RMSSUB_XITEM.ISCL_ADD) then
      if IO_message.iscloc_hier_level is NOT NULL then
         if not VALID_HIER_LEVEL(O_error_message,
                                 IO_message.iscloc_hier_level) then
            return FALSE;
         end if;
      end if;

      LP_isc_hier_level := IO_message.iscloc_hier_level;
      if NOT ORGHIER_EXISTS(O_error_message,
                            IO_message,
                            NULL, --- delete message
                            'ISCL',
                            LP_isc_hier_level) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.ISCL_UPD then
      --- Change of primary loc can only be done at the store or warehouse level
      if IO_message.iscloc_hier_level NOT IN ('S','W') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE','iscloc_hier_level',IO_message.iscloc_hier_level,NULL);
         return FALSE;
      else
         LP_isc_hier_level := IO_message.iscloc_hier_level;
      end if;
   end if;
  
   LP_isc_hier_level := IO_message.iscloc_hier_level;

   if I_message_type = RMSSUB_XITEM.ITEM_ADD and
      (IO_message.XIZPDesc_TBL is NOT NULL and
       IO_message.XIZPDesc_TBL.COUNT > 0) and
      (NVL(IO_message.pack_ind, 'N') = 'N' or
      (IO_message.pack_ind = 'Y' and NVL(IO_message.sellable_ind,'N') = 'Y')) then

      LP_izp_hier_level := IO_message.izp_hier_level;

   end if;

   if not POP_SYSTEM_OPTIONS(O_error_message) then
      return FALSE;
   end if;

   if I_message_type IN (RMSSUB_XITEM.ITEM_ADD,RMSSUB_XITEM.ITEM_UPD) then
      if NVL(IO_message.pack_ind, 'N') = 'Y' then
         if NOT POP_BUYER_PACK(O_error_message,
                               IO_message.pack_type,
                               IO_message.orderable_ind) then
            return FALSE;
         end if;
      end if;
   else -- Get item information for validation of child node adds and updates
      if NOT GET_ITEM_INFO(O_error_message,
                           IO_message.item,
                           IO_message) then
         return FALSE; -- if item does not exist
      end if;
      IO_message.aip_case_type :=  LP_im_row.aip_case_type;
   end if;

   if not POPULATE_ISC_ISMC(O_error_message,
                            IO_message.XItemSupDesc_TBL,
                            NULL, -- for Ref message
                            IO_message.item,
                            IO_message.dept,
                            I_message_type,
                            IO_message.item_level,
                            IO_message.tran_level,
                            IO_message.aip_case_type,
                            IO_message.standard_uom,
                            IO_message.orderable_ind) then
      return FALSE;
   end if;

   if not POPULATE_PACKITEM_TABLE(O_error_message,
                                  IO_message,
                                  I_message_type) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          L_item_rec,
                          IO_message,
                          I_message_type) then
      return FALSE;
   end if;

   IO_item_rec := L_item_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                  O_item_rec        OUT   NOCOPY    RMSSUB_XITEM.ITEM_API_DEL_REC,
                  I_message         IN              "RIB_XItemRef_REC",
                  I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XITEM_POP_RECORD.POPULATE';
   L_exists           BOOLEAN      := FALSE;

BEGIN

   if RESET_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XITEM.ISCL_DEL then
   -- item/supplier/country/loc delete
   
      if I_message.item is null then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'item', NULL, NULL);
         return FALSE;
      end if;
      
      if I_message.hier_level is null then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'hier_level', NULL, NULL);
         return FALSE;
      end if;
   end if;

  if I_message_type = RMSSUB_XITEM.ISCL_DEL then
      if not VALID_HIER_LEVEL(O_error_message,
                              I_message.hier_level) then
         return FALSE;
      end if;

      LP_isc_hier_level := I_message.hier_level;

      if NOT ORGHIER_EXISTS(O_error_message,
                            NULL, --- create message
                            I_message,
                            'DELISCL',
                            LP_isc_hier_level) then
         return FALSE;
      end if;
   end if;

   if not POP_SYSTEM_OPTIONS(O_error_message) then
      return FALSE;
   end if;

   if I_message_type != RMSSUB_XITEM.ITEM_DEL then
      -- Determine if parent item or tran item
      if NOT GET_ITEM_INFO(O_error_message,
                           I_message.item) then
         return FALSE; -- if item does not exist
      end if;
      ---
      O_item_rec.item := I_message.item;
      O_item_rec.item_level := LP_im_row.item_level;
      O_item_rec.tran_level := LP_im_row.tran_level;
   end if;
   
   if I_message_type = RMSSUB_XITEM.ISCL_DEL and
      I_message.XItemSupRef_TBL is NOT NULL  and
      I_message.XItemSupRef_TBL.COUNT > 0    then
      if not POPULATE_DEL_LOCS(O_error_message,
                               O_item_rec,
                               I_message.XItemSupRef_TBL,
                               I_message.item,
                               I_message_type) then
         return FALSE;
      end if;
   end if;

   if not POPULATE_ISC_ISMC(O_error_message,
                            NULL, -- for Desc message
                            I_message.XItemSupRef_TBL,
                            I_message.item,
                            NULL,
                            I_message_type,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL) then
      return FALSE;
   end if;

   if I_message_type != RMSSUB_XITEM.ISCL_DEL then
      if POPULATE_RECORD(O_error_message,
                         O_item_rec,
                         I_message,
                         I_message_type) = FALSE then
         return FALSE;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE;
-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION POP_BUYER_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pack_type       IN       ITEM_MASTER.PACK_TYPE%TYPE,
                        I_orderable       IN       ITEM_MASTER.ORDERABLE_IND%TYPE)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_POP_RECORD.POP_BUYER_PACK';

BEGIN
   if NVL(I_pack_type,'V') = 'B' and
      NVL(I_orderable,'N') = 'Y' then
      LP_buyer_pack := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POP_BUYER_PACK;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ISC_ISMC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_isup_rows       IN       "RIB_XItemSupDesc_TBL",
                           I_sups            IN       "RIB_XItemSupRef_TBL",
                           I_item            IN       ITEM_MASTER.ITEM%TYPE,
                           I_dept            IN       ITEM_MASTER.DEPT%TYPE,
                           I_message_type    IN       VARCHAR2,
                           I_item_level      IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                           I_tran_level      IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                           I_aip_case_type   IN       ITEM_MASTER.AIP_CASE_TYPE%TYPE,
                           I_standard_uom    IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                           I_orderable_ind   IN       ITEM_MASTER.ORDERABLE_IND%TYPE)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'RMSSUB_XITEM_POP_RECORD.POPULATE_ISC_ISMC';
   L_exists            BOOLEAN;
   L_ind               VARCHAR2(1);
   L_prim_supplier     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE;
   L_mod_cnt           NUMBER := 1; -- Row count for mod table

BEGIN
   if I_isup_rows is NOT NULL and
      I_isup_rows.COUNT > 0   then
      FOR i in I_isup_rows.FIRST..I_isup_rows.LAST LOOP
         if I_message_type = RMSSUB_XITEM.ISUP_UPD and
            L_exists then
            L_exists := FALSE;
            if NOT PRIMARY_SUPPLIER(O_error_message,
                                    L_exists,
                                    L_prim_supplier,
                                    I_item) then
               return FALSE;
            elsif L_prim_supplier = I_isup_rows(i).supplier and L_exists then
               L_ind := 'Y';
            else
               L_ind := 'N';
            end if;
         end if;

         if I_message_type = RMSSUB_XITEM.ISUP_UPD then
            if LP_mod_isup_tbl is NOT NULL and LP_mod_isup_tbl.COUNT > 0 then
               L_mod_cnt := LP_mod_isup_tbl.LAST + 1;
            end if;
            LP_mod_isup_tbl(L_mod_cnt).supplier := I_isup_rows(i).supplier;
            LP_mod_isup_tbl(L_mod_cnt).prim_supp := L_ind;  --- current database value
         end if;  --- update message type
      END LOOP;
   end if;  --- Check for populated node

   if I_message_type IN (RMSSUB_XITEM.ISMC_UPD, RMSSUB_XITEM.ISC_UPD) and
      I_isup_rows is NOT NULL and I_isup_rows.COUNT > 0 then
      FOR i in I_isup_rows.FIRST..I_isup_rows.LAST LOOP
         ---
         if I_message_type IN (RMSSUB_XITEM.ISC_UPD, RMSSUB_XITEM.ISCD_UPD) and
            I_isup_rows(i).XItemSupCtyDesc_TBL is NOT NULL and
            I_isup_rows(i).XItemSupCtyDesc_TBL.COUNT > 0 then
            if not POPULATE_ISC_TABLE(O_error_message,
                                      I_isup_rows(i).XItemSupCtyDesc_TBL,
                                      NULL,
                                      I_item,
                                      I_isup_rows(i).supplier,
                                      I_message_type) then
               exit;
            end if;
         end if;

         if I_message_type = RMSSUB_XITEM.ISMC_UPD and
            I_isup_rows(i).XItmSupCtyMfrDesc_TBL is NOT NULL and
            I_isup_rows(i).XItmSupCtyMfrDesc_TBL.COUNT > 0   then
            if not POPULATE_ISMC_TABLE(O_error_message,
                                       I_isup_rows(i).XItmSupCtyMfrDesc_TBL,
                                       NULL,
                                       I_item,
                                       I_isup_rows(i).supplier,
                                       I_message_type) then
               exit;
            end if;
         end if;
      END LOOP;

   end if;

   if O_error_message is NOT NULL then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_ISC_ISMC;
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ISC_TABLE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_isc_rows        IN       "RIB_XItemSupCtyDesc_TBL",
                            I_countries       IN       "RIB_XItemSupCtyRef_TBL",
                            I_item            IN       ITEM_MASTER.ITEM%TYPE,
                            I_supplier        IN       SUPS.SUPPLIER%TYPE,
                            I_message_type    IN       VARCHAR2)


   RETURN BOOLEAN IS

   L_program         VARCHAR2(61) := 'RMSSUB_XITEM_POP_RECORD.POPULATE_ISC_TABLE';
   L_exists          BOOLEAN := TRUE;
   L_isc_ind         VARCHAR2(1);
   L_prim_country    ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_mod_cnt         NUMBER := 1;

BEGIN
   -- item_supplier_country modify
   if I_message_type IN (RMSSUB_XITEM.ISC_UPD) then
      FOR i in I_isc_rows.FIRST..I_isc_rows.LAST LOOP
         if LP_mod_isc_tbl is NOT NULL and LP_mod_isc_tbl.COUNT > 0 then
            L_mod_cnt := LP_mod_isc_tbl.LAST + 1;
         end if;
         LP_mod_isc_tbl(L_mod_cnt).supplier := I_supplier;
         LP_mod_isc_tbl(L_mod_cnt).country := I_isc_rows(i).origin_country_id;
         LP_mod_isc_tbl(L_mod_cnt).prim_cty  := NVL(I_isc_rows(i).primary_country_ind, 'N');
         LP_mod_isc_tbl(L_mod_cnt).lead_time  := I_isc_rows(i).lead_time;
         LP_mod_isc_tbl(L_mod_cnt).pickup_lead_time  := I_isc_rows(i).pickup_lead_time;
         LP_mod_isc_tbl(L_mod_cnt).min_order_qty  := I_isc_rows(i).min_order_qty;
         LP_mod_isc_tbl(L_mod_cnt).max_order_qty  := I_isc_rows(i).max_order_qty;
         LP_mod_isc_tbl(L_mod_cnt).supp_hier_lvl_1  := I_isc_rows(i).supp_hier_lvl_1;
         LP_mod_isc_tbl(L_mod_cnt).supp_hier_lvl_2  := I_isc_rows(i).supp_hier_lvl_2;
         LP_mod_isc_tbl(L_mod_cnt).supp_hier_lvl_3  := I_isc_rows(i).supp_hier_lvl_3;
         LP_mod_isc_tbl(L_mod_cnt).default_uop  := I_isc_rows(i).default_uop;
         LP_mod_isc_tbl(L_mod_cnt).supp_pack_size  := I_isc_rows(i).supp_pack_size;
         LP_mod_isc_tbl(L_mod_cnt).inner_pack_size  := I_isc_rows(i).inner_pack_size;
         LP_mod_isc_tbl(L_mod_cnt).ti  := I_isc_rows(i).ti;
         LP_mod_isc_tbl(L_mod_cnt).hi  := I_isc_rows(i).hi;
         LP_mod_isc_tbl(L_mod_cnt).round_lvl  := I_isc_rows(i).round_lvl;
         LP_mod_isc_tbl(L_mod_cnt).round_to_inner_pct  := I_isc_rows(i).round_to_inner_pct;
         LP_mod_isc_tbl(L_mod_cnt).round_to_case_pct  := I_isc_rows(i).round_to_case_pct;
         LP_mod_isc_tbl(L_mod_cnt).round_to_layer_pct  := I_isc_rows(i).round_to_layer_pct;
         LP_mod_isc_tbl(L_mod_cnt).round_to_pallet_pct  := I_isc_rows(i).round_to_pallet_pct;
      END LOOP;
       --
   end if; --- message type

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_ISC_TABLE;
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ISMC_TABLE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_ismc_rows       IN       "RIB_XItmSupCtyMfrDesc_TBL",
                             I_countries       IN       "RIB_XItemSupCtyMfrRef_TBL",
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_supplier        IN       SUPS.SUPPLIER%TYPE,
                             I_message_type    IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program         VARCHAR2(61) := 'RMSSUB_XITEM_POP_RECORD.POPULATE_ISMC_TABLE';
   L_exists          BOOLEAN := TRUE;
   L_prim_country    ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE;
   L_mod_cnt         NUMBER := 1;

BEGIN
   -- POPULATE ISMC NODE
   -- item_supplier_country of manufacture create or modify
   if I_message_type IN (RMSSUB_XITEM.ISMC_UPD) then
      ---
      FOR i in I_ismc_rows.FIRST..I_ismc_rows.LAST LOOP
         if I_message_type = RMSSUB_XITEM.ISMC_UPD then
            if LP_mod_ismc_tbl is NOT NULL and LP_mod_ismc_tbl.COUNT > 0 then
               L_mod_cnt := LP_mod_ismc_tbl.LAST + 1;
            end if;
            LP_mod_ismc_tbl(L_mod_cnt).supplier := I_supplier;
            LP_mod_ismc_tbl(L_mod_cnt).country := I_ismc_rows(i).manufacturer_ctry_id;
            LP_mod_ismc_tbl(L_mod_cnt).prim_cty  := NVL(I_ismc_rows(i).primary_manufacturer_ctry_ind, 'N');
            exit;  --- only 1 country for an item-supplier can be updated to primary country
         end if;

         if I_message_type IN (RMSSUB_XITEM.ISMC_ADD) then
            ---
            if NOT ITEM_SUPP_MANU_COUNTRY_SQL.GET_PRIMARY_MANU_COUNTRY(O_error_message,
                                                                       L_exists,
                                                                       L_prim_country,
                                                                       I_item,
                                                                       I_supplier) then
                return FALSE;
             elsif L_exists then
                if NVL(I_ismc_rows(i).primary_manufacturer_ctry_ind, 'N') = 'Y' then
                   -- Populate the UPD record for changing the primary manu country for existing is in isc
                   if LP_mod_ismc_tbl is NOT NULL and LP_mod_ismc_tbl.COUNT > 0 then
                      L_mod_cnt := LP_mod_ismc_tbl.LAST + 1;
                   end if;
                   LP_mod_ismc_tbl(L_mod_cnt).supplier := I_supplier;
                   LP_mod_ismc_tbl(L_mod_cnt).country  := I_ismc_rows(i).manufacturer_ctry_id;
                end if;
            end if;
         end if;
      END LOOP;
   --
   end if; --- message type
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_ISMC_TABLE;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_PACKITEM_TABLE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_message          IN       "RIB_XItemDesc_REC",
                                I_message_type     IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'RMSSUB_XITEM_POP_RECORD.POPULATE_PACKITEM_TABLE';
   L_pack_ind          VARCHAR2(1);
   L_comp_seq_no       PACKITEM.SEQ_NO%TYPE := NULL;
   L_comp_item_record  ITEM_MASTER%ROWTYPE;

   cursor C_COMP_SEQ_NO is
      select NVL(MAX(seq_no), 0) + 1
        from packitem
       where pack_no = I_message.item;

BEGIN
   -- packitem create or modify
   if I_message_type = RMSSUB_XITEM.ITEM_ADD and
      NVL(I_message.pack_ind, 'N') = 'Y' and
      I_message.xitembomdesc_TBL is NOT NULL and
      I_message.item_level = I_message.tran_level and
      I_message.xitembomdesc_TBL.COUNT > 0   then
      ---
      LP_im_row.item := I_message.item;
      LP_im_row.dept := I_message.dept;
      LP_im_row.pack_ind := NVL(I_message.pack_ind, 'N');
      if LP_im_row.pack_type is NULL and NVL(I_message.simple_pack_ind, 'N') = 'Y' then
         LP_im_row.pack_type := 'V';
      else
         LP_im_row.pack_type := I_message.pack_type;
      end if;
      LP_im_row.order_as_type := I_message.order_as_type;
      LP_im_row.orderable_ind := NVL(I_message.orderable_ind, 'N');
      LP_im_row.contains_inner_ind := NVL(I_message.contains_inner_ind, 'N');

      FOR i IN I_message.xitembomdesc_TBL.FIRST..I_message.xitembomdesc_TBL.LAST LOOP
         if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                            L_comp_item_record,
                                            I_message.xitembomdesc_TBL(i).component_item) = FALSE then
            return FALSE;
         end if;

         if I_message.status = 'A' and L_comp_item_record.status <> 'A' then
            LP_wksht_flag := TRUE;
         end if;
         --- populate packitem table for item_rec
         if I_message_type = RMSSUB_XITEM.ITEM_ADD then
            L_comp_seq_no := i;
         elsif L_comp_seq_no is NULL then
            SQL_LIB.SET_MARK('OPEN',
                             'C_COMP_SEQ_NO',
                             'PACKITEM',
                             'item: '|| I_message.item);

            open C_COMP_SEQ_NO;

            SQL_LIB.SET_MARK('FETCH',
                             'C_COMP_SEQ_NO',
                             'PACKITEM',
                             'item: '|| I_message.item);

            fetch C_COMP_SEQ_NO into L_comp_seq_no;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_COMP_SEQ_NO',
                             'PACKITEM',
                             'item: '|| I_message.item);

            close C_COMP_SEQ_NO;
         else
            L_comp_seq_no := L_comp_seq_no + 1;
         end if;

         LP_pi_tbl(i).seq_no    := L_comp_seq_no;
         LP_pi_tbl(i).comp_item := I_message.xitembomdesc_TBL(i).component_item;
         LP_pi_tbl(i).comp_qty  := I_message.xitembomdesc_TBL(i).pack_qty;
         LP_pi_tbl(i).pack_ind  := L_pack_ind;  -- to check for inner packs
      END LOOP;
      ---
      if O_error_message is NOT NULL then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_PACKITEM_TABLE;
-------------------------------------------------------------------------------------------------------
FUNCTION POP_SYSTEM_OPTIONS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_POP_RECORD.SYSTEM_OPTIONS';

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('FAILED_SYSTEM_OPTIONS',NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POP_SYSTEM_OPTIONS;
-------------------------------------------------------------------------------------------------------
FUNCTION ISC_DEFAULTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_create_date     IN       DATE)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'RMSSUB_XITEM_POP_RECORD.ISC_DEFAULTS';

BEGIN

   LP_isc_row.supp_pack_size := 1;
   LP_isc_row.inner_pack_size := 1;
   LP_isc_row.round_lvl := LP_system_options_row.round_lvl;
   LP_isc_row.round_to_inner_pct := LP_system_options_row.round_to_inner_pct;
   LP_isc_row.round_to_case_pct := LP_system_options_row.round_to_case_pct;
   LP_isc_row.round_to_layer_pct := LP_system_options_row.round_to_layer_pct;
   LP_isc_row.round_to_pallet_pct := LP_system_options_row.round_to_pallet_pct;
   LP_isc_row.ti := 1;
   LP_isc_row.hi := 1;
   LP_isc_row.create_datetime := I_create_date;
   LP_isc_row.cost_uom := LP_im_row.standard_uom;

   --

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END ISC_DEFAULTS;
-------------------------------------------------------------------------------------------------------
FUNCTION ISCL_DEFAULTS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_create_date         IN       DATE)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'RMSSUB_XITEM_VALIDATE.ISCL_DEFAULTS';

BEGIN

   LP_iscl_row.round_lvl := LP_system_options_row.round_lvl;
   LP_iscl_row.round_to_inner_pct := LP_system_options_row.round_to_inner_pct;
   LP_iscl_row.round_to_case_pct := LP_system_options_row.round_to_case_pct;
   LP_iscl_row.round_to_layer_pct := LP_system_options_row.round_to_layer_pct;
   LP_iscl_row.round_to_pallet_pct := LP_system_options_row.round_to_pallet_pct;
   LP_iscl_row.create_datetime := I_create_date;
   if LP_isc_hier_level = 'W' then
      LP_iscl_row.loc_type := 'W';
   else
      LP_iscl_row.loc_type := 'S';
   end if;

   --

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END ISCL_DEFAULTS;
-------------------------------------------------------------------------------------------------------
FUNCTION ISCD_DEFAULTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_create_date     IN       DATE)

   RETURN BOOLEAN IS

   L_program   VARCHAR2(61) := 'RMSSUB_XITEM_POP_RECORD.ISCD_DEFAULTS';

BEGIN

   LP_iscd_row.create_datetime := I_create_date;

   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END ISCD_DEFAULTS;
-------------------------------------------------------------------------------------------------------
FUNCTION ISUP_DEFAULTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_create_date     IN       DATE)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'RMSSUB_XITEM_POP_RECORD.ISUP_DEFAULTS';

BEGIN

   LP_isup_row.pallet_name := LP_system_options_row.default_pallet_name;
   LP_isup_row.case_name := LP_system_options_row.default_case_name;
   LP_isup_row.inner_name := LP_system_options_row.default_inner_name;
   LP_isup_row.direct_ship_ind := 'N';
   LP_isup_row.create_datetime := I_create_date;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END ISUP_DEFAULTS;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_IM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_message         IN       "RIB_XItemDesc_REC",
                     I_message_type    IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'RMSSUB_XITEM_POP_RECORD.POPULATE_IM';
   L_desc_len          NUMBER := lengthb(I_message.item_desc);
   L_short_len         NUMBER;  -- length of short desc
   L_dept_type         VARCHAR2(1);
   L_exist             BOOLEAN;
   L_item_master_row   ITEM_MASTER%ROWTYPE;

   cursor C_CHK_DEPT_TYPE is
      select purchase_type
        from deps
       where dept=I_message.dept;

BEGIN

   LP_im_row.item := I_message.item;
   ---
   SQL_LIB.SET_MARK('OPEN',
                 'C_CHK_DEPT_TYPE',
                 'DEPS',
                 'Dept: '|| I_message.dept);

   open C_CHK_DEPT_TYPE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_DEPT_TYPE',
                    'DEPS',
                    'Dept: '|| I_message.dept);

   fetch C_CHK_DEPT_TYPE into L_dept_type;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_DEPT_TYPE',
                    'DEPS',
                    'Dept: '|| I_message.dept);

   close C_CHK_DEPT_TYPE;
   ---
   if I_message_type = RMSSUB_XITEM.ITEM_ADD then
      --- Populate message values
      LP_im_row.item_parent := I_message.item_parent;
      LP_im_row.item_grandparent := I_message.item_grandparent;
      LP_im_row.pack_ind := NVL(I_message.pack_ind, 'N');
      LP_im_row.item_level := I_message.item_level;
      LP_im_row.tran_level := I_message.tran_level;
      LP_im_row.diff_1 := upper(I_message.diff_1);
      LP_im_row.diff_2 := upper(I_message.diff_2);
      LP_im_row.diff_3 := upper(I_message.diff_3);
      LP_im_row.diff_4 := upper(I_message.diff_4);
      LP_im_row.dept := I_message.dept;
      LP_im_row.class := I_message.class;
      LP_im_row.subclass := I_message.subclass;
      LP_im_row.item_desc := I_message.item_desc;
      LP_im_row.format_id := I_message.format_id;
      LP_im_row.prefix := I_message.prefix;
      if L_desc_len >= 120 then  -- 120 is the size of ITEM_MASTER.SHORT_DESC
         L_short_len := 120;
      else
         L_short_len := L_desc_len;
      end if;
      LP_im_row.short_desc := NVL(I_message.short_desc,rtrim(substrb(I_message.item_desc,1,L_short_len)));
      LP_im_row.cost_zone_group_id := I_message.cost_zone_group_id;
      LP_im_row.store_ord_mult := I_message.store_ord_mult;
      LP_im_row.forecast_ind := NVL(I_message.forecast_ind, 'N');
      LP_im_row.comments := I_message.comments;

      if NVL(I_message.orderable_ind, 'N') = 'Y' then
         LP_im_row.order_type := I_message.order_type;
      else
         LP_im_row.order_type := NULL;
      end if;
      if NVL(I_message.sellable_ind, 'N') = 'Y' then
         LP_im_row.sale_type := I_message.sale_type;
      else
         LP_im_row.sale_type := NULL;
      end if;
      LP_im_row.catch_weight_uom := I_message.catch_weight_uom;

      --- Populate pack item fields
      if NVL(I_message.pack_ind, 'N') = 'Y' then
         LP_im_row.sellable_ind := NVL(I_message.sellable_ind, 'N');
         if NVL(I_message.simple_pack_ind, 'N') = 'N' then
            LP_im_row.orderable_ind := NVL(I_message.orderable_ind,'Y');
         else
            LP_im_row.orderable_ind := 'Y';
         end if;
         LP_im_row.simple_pack_ind := NVL(I_message.simple_pack_ind, 'N');
         LP_im_row.contains_inner_ind := NVL(I_message.contains_inner_ind, 'N');
         LP_im_row.inventory_ind := 'Y';
         if I_message.pack_type is NULL and NVL(I_message.simple_pack_ind, 'N') = 'Y' then
            LP_im_row.pack_type := 'V';
         else
            LP_im_row.pack_type := I_message.pack_type;
         end if;
         LP_im_row.pack_type := I_message.pack_type;
         LP_im_row.order_as_type := I_message.order_as_type;
      else --- pack_ind = N
         LP_im_row.sellable_ind := NVL(I_message.sellable_ind, 'N');
         LP_im_row.orderable_ind := NVL(I_message.orderable_ind, 'N');
         LP_im_row.simple_pack_ind := 'N';
         LP_im_row.contains_inner_ind := 'N';
      end if;
      ---
      --- Populate defaults
      LP_im_row.item_number_type := NVL(I_message.item_number_type,'ITEM');
      if I_message.item_level <> 1 or NVL(I_message.pack_ind, 'N') = 'Y' then
         LP_im_row.item_aggregate_ind := 'N';
      else
         LP_im_row.item_aggregate_ind := NVL(I_message.item_aggregate_ind,'N');
      end if;
      if LP_im_row.item_aggregate_ind = 'N' then
         LP_im_row.diff_1_aggregate_ind := 'N';
         LP_im_row.diff_2_aggregate_ind := 'N';
         LP_im_row.diff_3_aggregate_ind := 'N';
         LP_im_row.diff_4_aggregate_ind := 'N';
      else
         LP_im_row.diff_1_aggregate_ind := NVL(I_message.diff_1_aggregate_ind,'N');
         LP_im_row.diff_2_aggregate_ind := NVL(I_message.diff_2_aggregate_ind,'N');
         LP_im_row.diff_3_aggregate_ind := NVL(I_message.diff_3_aggregate_ind,'N');
         LP_im_row.diff_4_aggregate_ind := NVL(I_message.diff_4_aggregate_ind,'N');
      end if;
      if LP_wksht_flag = TRUE then
         LP_im_row.status := 'W';
      else
         LP_im_row.status := NVL(I_message.status,'A');
      end if;
      LP_im_row.desc_up := upper(I_message.item_desc);
      LP_im_row.primary_ref_item_ind := NVL(I_message.PRIMARY_UPC_IND,'N');
      LP_im_row.standard_uom := NVL(I_message.standard_uom, LP_system_options_row.default_standard_uom);
      if LP_im_row.standard_uom = 'EA' then
         LP_im_row.uom_conv_factor := NULL;
      else
         LP_im_row.uom_conv_factor := I_message.uom_conv_factor;
      end if;
      LP_im_row.merchandise_ind := NVL(I_message.merchandise_ind,'Y');
      if I_message.deposit_item_type is not null or L_dept_type = 1 or L_dept_type = 2 or (I_message.item_xform_ind = 'Y' and I_message.sellable_ind= 'Y') then
         LP_im_row.catch_weight_ind := 'N';
      else
         LP_im_row.catch_weight_ind := NVL(I_message.catch_weight_ind,'N');
      end if;
      LP_im_row.const_dimen_ind := NVL(I_message.const_dimen_ind,'N');
      LP_im_row.gift_wrap_ind := NVL(I_message.gift_wrap_ind,'N');
      LP_im_row.ship_alone_ind := NVL(I_message.ship_alone_ind,'N');
      if (I_message.xitemudadtl_tbl is NOT NULL  and  I_message.xitemudadtl_tbl.count > 0) then
         LP_im_row.check_uda_ind := 'Y';
      else
         LP_im_row.check_uda_ind := 'N';
      end if;
      LP_im_row.item_xform_ind := NVL(I_message.item_xform_ind,'N');
      LP_im_row.deposit_item_type := I_message.deposit_item_type;
      LP_im_row.container_item := I_message.container_item;

      LP_im_row.waste_type := I_message.waste_type;
      LP_im_row.waste_pct  := I_message.waste_pct;
      if I_message.waste_type='SP' then
         LP_im_row.default_waste_pct := I_message.default_waste_pct;
      end if;
      if L_dept_type=1 or L_dept_type=2 then
         LP_im_row.inventory_ind := 'N';
         LP_im_row.sellable_ind := 'Y';
         LP_im_row.orderable_ind := 'N';
      else
         LP_im_row.inventory_ind := NVL(I_message.inventory_ind, 'Y');
      end if;
      LP_im_row.last_update_id := get_user;
      if I_message.sellable_ind = 'Y' or L_dept_type = 1 or L_dept_type = 2 then
         LP_im_row.mfg_rec_retail  := I_message.mfg_rec_retail;
      else
         LP_im_row.mfg_rec_retail  := NULL;
      end if;
      LP_im_row.create_datetime := NVL(I_message.create_datetime,sysdate);
      LP_im_row.perishable_ind  := NVL(I_message.perishable_ind, 'N');
      if (I_message.pack_ind = 'Y' and NVL(I_message.simple_pack_ind, 'N') = 'Y') then
         LP_im_row.notional_pack_ind       := NVL(I_message.notional_pack_ind, 'N');
         LP_im_row.soh_inquiry_at_pack_ind := NVL(I_message.soh_inquiry_at_pack_ind, 'N');
      else
         LP_im_row.notional_pack_ind       := 'N';
         LP_im_row.soh_inquiry_at_pack_ind := 'N';
      end if;
      LP_im_row.package_size := I_message.package_size;
      LP_im_row.package_uom := I_message.package_uom;
      LP_im_row.handling_sensitivity := I_message.handling_sensitivity;
      LP_im_row.handling_temp := I_message.handling_temp;
      LP_im_row.item_desc_secondary := rtrim(substr(I_message.item_desc_secondary,1,250));
      LP_im_row.desc_up := upper(NVL(I_message.desc_up,I_message.item_desc));
      LP_im_row.item_service_level := I_message.item_service_level;
      if I_message.deposit_item_type = 'E' then
         LP_im_row.deposit_in_price_per_uom := I_message.deposit_in_price_per_uom;
         LP_im_row.sellable_ind := 'Y';
         LP_im_row.orderable_ind := 'Y';
         LP_im_row.inventory_ind := 'Y';
      elsif I_message.deposit_item_type = 'A' then
         LP_im_row.sellable_ind := 'Y';
         LP_im_row.orderable_ind := 'N';
         LP_im_row.inventory_ind := 'N';
      elsif I_message.deposit_item_type = 'Z' then
         LP_im_row.sellable_ind := 'Y';
         LP_im_row.orderable_ind := 'Y';
         LP_im_row.inventory_ind := 'Y';
      elsif I_message.deposit_item_type = 'T' or I_message.deposit_item_type = 'P' then
         LP_im_row.sellable_ind := 'Y';
         LP_im_row.orderable_ind := 'N';
         LP_im_row.inventory_ind := 'Y';
      end if;
      if I_message.item_xform_ind = 'Y' then
         if I_message.sellable_ind = 'Y' then
            LP_im_row.orderable_ind := 'N';
            LP_im_row.inventory_ind := 'N';
         elsif I_message.orderable_ind = 'Y' then
            LP_im_row.sellable_ind := 'N';
            LP_im_row.inventory_ind := 'Y';
         end if;
      end if;
      LP_im_row.original_retail := I_message.original_retail;
      LP_im_row.retail_label_type := I_message.retail_label_type;
      LP_im_row.retail_label_value := I_message.retail_label_value;
      -- Populate brand and product classification
      LP_im_row.brand_name := I_message.brand;
      LP_im_row.product_classification := I_message.product_classification;
      --
   elsif I_message_type = RMSSUB_XITEM.ITEM_UPD then
      ---
      if ITEM_VALIDATE_SQL.EXIST(O_error_message,
                                 L_exist,
                                 I_message.item) = FALSE then
         return FALSE;
      end if;

      if L_exist then
         if NOT ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                                LP_im_row,
                                                I_message.item) then
            return FALSE;
         end if;
      end if;
      ---
      LP_im_row.item_desc := I_message.item_desc;
      LP_im_row.short_desc := NVL(I_message.short_desc,LP_im_row.short_desc);
      LP_im_row.store_ord_mult := I_message.store_ord_mult;
      LP_im_row.forecast_ind := NVL(I_message.forecast_ind, 'N');
      LP_im_row.comments := I_message.comments;
      if LP_im_row.status <> 'A' then
         LP_im_row.original_retail := NVL(I_message.original_retail,LP_im_row.original_retail);
      end if;
      LP_im_row.status := NVL(I_message.status,LP_im_row.status);
      LP_im_row.create_datetime := NVL(I_message.create_datetime,sysdate);
      LP_im_row.primary_ref_item_ind := NVL(I_message.primary_upc_ind, 'N');
      LP_im_row.container_item := I_message.container_item;
      if NVL(LP_im_row.sellable_ind, 'N') = 'Y' then
         LP_im_row.mfg_rec_retail  := NVL(I_message.mfg_rec_retail,LP_im_row.mfg_rec_retail);
      end if;
      LP_im_row.package_size := I_message.package_size;
      LP_im_row.package_uom := I_message.package_uom;
      if LP_im_row.standard_uom = 'EA' then
         LP_im_row.uom_conv_factor := NULL;
      else
         LP_im_row.uom_conv_factor := I_message.uom_conv_factor;
      end if;
      LP_im_row.handling_sensitivity := I_message.handling_sensitivity;
      LP_im_row.handling_temp := I_message.handling_temp;
      LP_im_row.item_desc_secondary := rtrim(substr(I_message.item_desc_secondary,1,250));
      LP_im_row.desc_up := upper(NVL(I_message.desc_up,I_message.item_desc));
      LP_im_row.item_service_level := I_message.item_service_level;
      if I_message.deposit_item_type = 'E' then
         LP_im_row.deposit_in_price_per_uom := I_message.deposit_in_price_per_uom;
      end if;
      LP_im_row.retail_label_type := I_message.retail_label_type;
      LP_im_row.retail_label_value := I_message.retail_label_value;
      LP_im_row.waste_type := I_message.waste_type;
      LP_im_row.waste_pct  := I_message.waste_pct;
      if I_message.waste_type='SP' then
         LP_im_row.default_waste_pct := I_message.default_waste_pct;
      end if;
      LP_im_row.brand_name := I_message.brand;
      LP_im_row.product_classification := I_message.product_classification;
      if I_message.gift_wrap_ind is NOT NULL then
         LP_im_row.gift_wrap_ind := I_message.gift_wrap_ind;
      end if;
   end if;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_IM;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_rec        IN OUT   RMSSUB_XITEM.ITEM_API_REC,
                         I_message         IN       "RIB_XItemDesc_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS
   L_program                VARCHAR2(61)           := 'RMSSUB_XITEM_POP_RECORD.POPULATE_RECORD';
   L_iscl_tbl               RMSSUB_XITEM.ISCL_TBLTYPE;
   L_finaL_iscl_tbl         RMSSUB_XITEM.ISCL_TBLTYPE;
   L_loc_cnt                NUMBER                 := 0;
   L_izp_tbl                RMSSUB_XITEM.IZP_TBLTYPE;
   L_isup_rows              "RIB_XItemSupDesc_TBL" := I_message.XItemSupDesc_TBL;
   L_isc_rows               "RIB_XItemSupCtyDesc_TBL";
   L_ismc_rows              "RIB_XItmSupCtyMfrDesc_TBL";
   L_izp_rows               "RIB_XIZPDesc_TBL"     := I_message.XIZPDesc_TBL;
   L_item_rec               RMSSUB_XITEM.ITEM_API_REC;
   L_standard_uom           ITEM_MASTER.STANDARD_UOM%TYPE;

   L_item_seasons_rec       RMSSUB_XITEM.ITEMSEASON_RECTYPE;
   L_season_id_tbl          RMSSUB_XITEM.SEASON_ID_TBLTYPE;
   L_phase_id_tbl           RMSSUB_XITEM.PHASE_ID_TBLTYPE ;
   L_seq_no_tbl             RMSSUB_XITEM.SEQ_NO_TBLTYPE;
   L_diff_id_tbl            RMSSUB_XITEM.DIFF_ID_TBLTYPE;
   L_createdate_tbl         RMSSUB_XITEM.CREATE_DATE_TBLTYPE;
   L_last_updatedate_tbl    RMSSUB_XITEM.LAST_UPDATE_DATE_TBLTYPE;
   L_last_updateid_tbl      RMSSUB_XITEM.UPDATE_ID_TBLTYPE;

   --UDADATE
   L_count_udadate          NUMBER                 := 1;
   L_udaid_udadate_tbl      RMSSUB_XITEM.UDA_ID_TBLTYPE;
   L_uda_date_tbl           RMSSUB_XITEM.UDA_DATE_TBLTYPE;
   L_crtdt_udadate_tbl      RMSSUB_XITEM.CREATE_DATE_TBLTYPE;
   L_lstupddt_udadate_tbl   RMSSUB_XITEM.LAST_UPDATE_DATE_TBLTYPE;
   L_lstupdid_udadate_tbl   RMSSUB_XITEM.UPDATE_ID_TBLTYPE;
   --UDAFF
   L_item_udaff_rec         RMSSUB_XITEM.ITEMUDAFF_RECTYPE;
   L_uda_text_tbl           RMSSUB_XITEM.UDA_TEXT_TBLTYPE;
   L_udaid_udaff_tbl        RMSSUB_XITEM.UDA_ID_TBLTYPE;
   L_count_udaff            NUMBER                 := 1;
   L_crtdt_udaff_tbl        RMSSUB_XITEM.CREATE_DATE_TBLTYPE;
   L_lstupddt_udaff_tbl     RMSSUB_XITEM.LAST_UPDATE_DATE_TBLTYPE;
   L_lstupdid_udaff_tbl     RMSSUB_XITEM.UPDATE_ID_TBLTYPE;
   --UDASLOV
   L_item_udaslov_rec       RMSSUB_XITEM.ITEMUDALOV_RECTYPE;
   L_count_udaslov          NUMBER                 := 1;
   L_udaid_udaslov_tbl      RMSSUB_XITEM.UDA_ID_TBLTYPE;
   L_uda_value_tbl          RMSSUB_XITEM.UDA_VALUE_TBLTYPE;
   L_crtdt_udaslov_tbl      RMSSUB_XITEM.CREATE_DATE_TBLTYPE;
   L_lstupddt_udaslov_tbl   RMSSUB_XITEM.LAST_UPDATE_DATE_TBLTYPE;
   L_lstupdid_udaslov_tbl   RMSSUB_XITEM.UPDATE_ID_TBLTYPE;
   --IMAGES
   L_item_image_rec         RMSSUB_XITEM.ITEMIMAGE_RECTYPE;
   L_count_itemimage        NUMBER                 := 1;
   L_imagename_tbl          RMSSUB_XITEM.IMAGE_NAME_TBLTYPE;
   L_imageaddr_tbl          RMSSUB_XITEM.IMAGE_ADDR_TBLTYPE;
   L_imagedesc_tbl          RMSSUB_XITEM.IMAGE_DESC_TBLTYPE;
   L_imagetype_tbl          RMSSUB_XITEM.IMAGE_TYPE_TBLTYPE;
   L_imageprimary_tbl       RMSSUB_XITEM.IMAGE_PRIM_TBLTYPE;
   L_imagepriority_tbl      RMSSUB_XITEM.IMAGE_DISPLAY_TBLTYPE;
   L_imagecreatedt_tbl      RMSSUB_XITEM.IMAGE_CREATE_DATE_TBLTYPE;
   L_imageupdatedt_tbl      RMSSUB_XITEM.IMAGE_UPDATE_DATE_TBLTYPE;
   L_imageupdateid_tbl      RMSSUB_XITEM.IMAGE_UPDATE_ID_TBLTYPE;
   L_item_del_rec           RMSSUB_XITEM.ITEM_API_DEL_REC := NULL;

BEGIN
   --- Always set these system options in the output record
   L_item_rec.rpm_ind := 'Y';
   L_item_rec.prim_currency_code := LP_system_options_row.currency_code;

   if I_message_type in (RMSSUB_XITEM.ITEM_ADD,RMSSUB_XITEM.ITEM_UPD) then
      if not POPULATE_IM(O_error_message,
                         I_message,
                         I_message_type) then
         return FALSE;
      end if;
      if LP_system_options_row.aip_ind = 'Y' then
         if NVL(I_message.pack_ind, 'N') = 'N' then
            LP_im_row.aip_case_type := I_message.aip_case_type;
         else
            LP_im_row.aip_case_type := NULL;
         end if;
      else
         LP_im_row.aip_case_type := NULL;
      end if;
   else  --- Populate item ID for all other message types
      LP_im_row.item := I_message.item;
      LP_im_row.create_datetime := NVL(I_message.create_datetime,sysdate);
   end if;

   if I_message_type IN (RMSSUB_XITEM.ITEM_ADD,RMSSUB_XITEM.ISUP_ADD) then
      if not ISUP_DEFAULTS(O_error_message,
                           LP_im_row.create_datetime) then
         return FALSE;
      end if;
      LP_isup_row.item := I_message.item;
   end if;

   if I_message_type IN (RMSSUB_XITEM.ITEM_ADD,RMSSUB_XITEM.ISC_ADD) then
      if not ISC_DEFAULTS(O_error_message,
                          LP_im_row.create_datetime) then
         return FALSE;
      end if;
      LP_isc_row.item := I_message.item;
   end if;

   if I_message_type IN (RMSSUB_XITEM.ITEM_ADD,RMSSUB_XITEM.ISMC_ADD) then
      LP_ismc_row.item := I_message.item;
   end if;

   if I_message_type IN (RMSSUB_XITEM.ITEM_ADD,RMSSUB_XITEM.ISCD_ADD) then
      if NOT ISCD_DEFAULTS(O_error_message,
                           LP_im_row.create_datetime) then
         return FALSE;
      end if;
      LP_iscd_row.item := I_message.item;
   end if;
   
   if I_message_type IN (RMSSUB_XITEM.ISCL_ADD) then
      if not ISCL_DEFAULTS(O_error_message,
                           LP_im_row.create_datetime) then
         return FALSE;
      end if;
      LP_iscl_row.item := I_message.item;
   end if;
   
    if I_message_type IN (RMSSUB_XITEM.ISCL_ADD,RMSSUB_XITEM.ISCL_UPD) then
       if I_message.XItemSupDesc_TBL is NOT NULL and I_message.XItemSupDesc_TBL.count > 0 then
         FOR i IN L_isup_rows.FIRST..L_isup_rows.LAST LOOP
            L_isc_rows := L_isup_rows(i).XItemSupCtyDesc_TBL;
               if L_isc_rows is NOT NULL and L_isc_rows.COUNT > 0 then
                  FOR j IN L_isc_rows.FIRST..L_isc_rows.LAST LOOP
                     if L_isc_rows(j).XISCLocDesc_TBL is NOT NULL and L_isc_rows(j).XISCLocDesc_TBL.COUNT > 0  then
                        if POPULATE_LOCS(O_error_message,
                                         L_iscl_tbl,
                                         L_isc_rows(j).XISCLocDesc_TBL,
                                         L_isup_rows(i).supplier,
                                         L_isc_rows(j).origin_country_id,
                                         L_isc_rows(j).unit_cost,
                                         I_message.item,
                                         I_message_type) = FALSE then
                            return FALSE;
                        end if;

                   if L_iscl_tbl is NOT NULL and L_iscl_tbl.COUNT > 0 then
                       L_loc_cnt := L_final_iscl_tbl.COUNT + 1;

                      FOR tbl_cnt IN L_iscl_tbl.FIRST..L_iscl_tbl.LAST LOOP
                         L_final_iscl_tbl(L_loc_cnt) := L_iscl_tbl(tbl_cnt);
                         L_final_iscl_tbl(L_loc_cnt).pickup_lead_time := L_isc_rows(j).XISCLocDesc_TBL(tbl_cnt).pickup_lead_time;
                         L_loc_cnt := L_loc_cnt + 1;
                      END LOOP;
                   end if;
                end if; --- check populated location (hier id) node

             END LOOP; --- loop through countries
          end if;  --- check populated country node
      END LOOP; --- loop through suppliers
    end if;
   end if;
   
   if I_message_type = RMSSUB_XITEM.ITEM_ADD and
      L_izp_rows is NOT NULL and L_izp_rows.COUNT > 0 and
      NVL(LP_im_row.sellable_ind, 'N') = 'Y' then

      L_standard_uom := NVL(I_message.standard_uom, LP_system_options_row.default_standard_uom);
      if NOT POPULATE_ZONES(O_error_message,
                            L_izp_tbl,
                            L_izp_rows,
                            L_standard_uom) then
         return FALSE;
      end if;
   end if;
   ---
   L_item_rec.item_master_def := LP_im_row;
   L_item_rec.item_supp_def := LP_isup_row;
   L_item_rec.isc_def := LP_isc_row;
   L_item_rec.ismc_def := LP_ismc_row;
   L_item_rec.iscl_def := LP_iscl_row;
   L_item_rec.supp_mod_tbl := LP_mod_isup_tbl;
   L_item_rec.isc_mod_tbl := LP_mod_isc_tbl;
   L_item_rec.ismc_mod_tbl := LP_mod_ismc_tbl;
   L_item_rec.packs := LP_pi_tbl;
   L_item_rec.iscl_tbl := L_final_iscl_tbl;
   L_item_rec.izp_tbl := L_izp_tbl;
   ---

   if I_message.xitemseason_tbl is NOT NULL and I_message.xitemseason_tbl.count > 0 then
      FOR i IN I_message.xitemseason_tbl.FIRST..I_message.xitemseason_tbl.LAST LOOP
         L_season_id_tbl(i)          := I_message.xitemseason_tbl(i).season_id;
         L_phase_id_tbl(i)           := I_message.xitemseason_tbl(i).phase_id;
         L_seq_no_tbl(i)             := I_message.xitemseason_tbl(i).item_season_seq_no;
         L_diff_id_tbl(i)            := I_message.xitemseason_tbl(i).diff_id;
         L_createdate_tbl(i)         := I_message.xitemseason_tbl(i).create_datetime;
         L_last_updatedate_tbl(i)    := I_message.xitemseason_tbl(i).last_update_datetime;
         L_last_updateid_tbl(i)      := I_message.xitemseason_tbl(i).last_update_id;
      END LOOP;
      ---
      L_item_rec.seasons.season_id           := L_season_id_tbl;
      L_item_rec.seasons.phase_id            := L_phase_id_tbl;
      L_item_rec.seasons.seq_no              := L_seq_no_tbl;
      L_item_rec.seasons.diff_id             := L_diff_id_tbl;
      L_item_rec.seasons.create_date         := L_createdate_tbl;
      L_item_rec.seasons.last_update_date    := L_last_updatedate_tbl;
      L_item_rec.seasons.last_update_id      := L_last_updateid_tbl;
   end if;

   if I_message.xitemudadtl_tbl is NOT NULL and I_message.xitemudadtl_tbl.count > 0 then
      FOR i IN I_message.xitemudadtl_tbl.FIRST..I_message.xitemudadtl_tbl.LAST LOOP
         if I_message.xitemudadtl_tbl(i).display_type = 'DT' then
            L_udaid_udadate_tbl(L_count_udadate)        := I_message.xitemudadtl_tbl(i).uda_id;
            L_uda_date_tbl(L_count_udadate)             := I_message.xitemudadtl_tbl(i).uda_date;
            L_crtdt_udadate_tbl(L_count_udadate)        := I_message.xitemudadtl_tbl(i).create_datetime;
            L_lstupddt_udadate_tbl(L_count_udadate)     := I_message.xitemudadtl_tbl(i).last_update_datetime;
            L_lstupdid_udadate_tbl(L_count_udadate)     := I_message.xitemudadtl_tbl(i).last_update_id;
            ---
            L_count_udadate := L_count_udadate + 1;
         elsif I_message.xitemudadtl_tbl(i).display_type = 'FF' then
            L_udaid_udaff_tbl(L_count_udaff)          := I_message.xitemudadtl_tbl(i).uda_id;
            L_uda_text_tbl(L_count_udaff)             := I_message.xitemudadtl_tbl(i).uda_text;
            L_crtdt_udaff_tbl(L_count_udaff)          := I_message.xitemudadtl_tbl(i).create_datetime;
            L_lstupddt_udaff_tbl(L_count_udaff)       := I_message.xitemudadtl_tbl(i).last_update_datetime;
            L_lstupdid_udaff_tbl(L_count_udaff)       := I_message.xitemudadtl_tbl(i).last_update_id;
            ---
            L_count_udaff  := L_count_udaff + 1;
         elsif I_message.xitemudadtl_tbl(i).display_type = 'LV' then
            L_udaid_udaslov_tbl(L_count_udaslov)        := I_message.xitemudadtl_tbl(i).uda_id;
            L_uda_value_tbl(L_count_udaslov)            := I_message.xitemudadtl_tbl(i).uda_value;
            L_crtdt_udaslov_tbl(L_count_udaslov)        := I_message.xitemudadtl_tbl(i).create_datetime;
            L_lstupddt_udaslov_tbl(L_count_udaslov)     := I_message.xitemudadtl_tbl(i).last_update_datetime;
            L_lstupdid_udaslov_tbl(L_count_udaslov)     := I_message.xitemudadtl_tbl(i).last_update_id;
            ---
            L_count_udaslov   := L_count_udaslov + 1;
         end if;
      END LOOP;
      ---
      L_item_rec.udadate.uda_id            := L_udaid_udadate_tbl;
      L_item_rec.udadate.uda_date          := L_uda_date_tbl;
      L_item_rec.udadate.create_date       := L_crtdt_udadate_tbl;
      L_item_rec.udadate.last_update_date  := L_lstupddt_udadate_tbl;
      L_item_rec.udadate.last_update_id    := L_lstupdid_udadate_tbl;
      ---
      L_item_rec.udaff.uda_id              := L_udaid_udaff_tbl;
      L_item_rec.udaff.uda_text            := L_uda_text_tbl;
      L_item_rec.udaff.create_date         := L_crtdt_udaff_tbl;
      L_item_rec.udaff.last_update_date    := L_lstupddt_udaff_tbl;
      L_item_rec.udaff.last_update_id      := L_lstupdid_udaff_tbl;
      ---
      L_item_rec.udaslov.uda_id            := L_udaid_udaslov_tbl;
      L_item_rec.udaslov.uda_value         := L_uda_value_tbl;
      L_item_rec.udaslov.create_date       := L_crtdt_udaslov_tbl;
      L_item_rec.udaslov.last_update_date  := L_lstupddt_udaslov_tbl;
      L_item_rec.udaslov.last_update_id    := L_lstupdid_udaslov_tbl;
   end if;
   ---
   if I_message.xitemimage_tbl is NOT NULL and I_message.xitemimage_tbl.COUNT > 0 then
      FOR i IN I_message.xitemimage_tbl.FIRST..I_message.xitemimage_tbl.LAST LOOP
         L_imagename_tbl(L_count_itemimage)     := I_message.xitemimage_tbl(i).image_name;
         L_imageaddr_tbl(L_count_itemimage)     := I_message.xitemimage_tbl(i).image_addr;
         L_imagedesc_tbl(L_count_itemimage)     := I_message.xitemimage_tbl(i).image_desc;
         L_imagetype_tbl(L_count_itemimage)     := I_message.xitemimage_tbl(i).image_type;
         L_imageprimary_tbl(L_count_itemimage)  := I_message.xitemimage_tbl(i).primary_ind;
         L_imagepriority_tbl(L_count_itemimage) := I_message.xitemimage_tbl(i).display_priority;
         L_imagecreatedt_tbl(L_count_itemimage) := I_message.xitemimage_tbl(i).create_datetime;
         L_imageupdatedt_tbl(L_count_itemimage) := I_message.xitemimage_tbl(i).last_update_datetime;
         L_imageupdateid_tbl(L_count_itemimage) := I_message.xitemimage_tbl(i).last_update_id;
         ---
         L_count_itemimage                      := L_count_itemimage + 1;
      END LOOP;
      ---
      L_item_rec.images.image_name       := L_imagename_tbl;
      L_item_rec.images.image_addr       := L_imageaddr_tbl;
      L_item_rec.images.image_desc       := L_imagedesc_tbl;
      L_item_rec.images.image_type       := L_imagetype_tbl;
      L_item_rec.images.primary_ind      := L_imageprimary_tbl;
      L_item_rec.images.display_priority := L_imagepriority_tbl;
      L_item_rec.images.create_date      := L_imagecreatedt_tbl;
      L_item_rec.images.last_update_date := L_imageupdatedt_tbl;
      L_item_rec.images.last_update_id   := L_imageupdateid_tbl;
   end if;
   ---
   if POPULATE_TRANS_LANG(O_error_message,
                          L_item_rec,
                          L_item_del_rec,
                          I_message,
                          NULL,
                          I_message_type) = FALSE then
      return FALSE;
   end if;
   O_item_rec := L_item_rec;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_rec        IN OUT   RMSSUB_XITEM.ITEM_API_DEL_REC,
                         I_message         IN       "RIB_XItemRef_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(50) := 'RMSSUB_XITEM_POP_RECORD.POPULATE_RECORD';
   L_seq_no_tbl            RMSSUB_XITEM.SEQ_NO_TBLTYPE;
   L_item_rec              RMSSUB_XITEM.ITEM_API_DEL_REC;
   L_season_id_tbl         RMSSUB_XITEM.SEASON_ID_TBLTYPE;
   L_phase_id_tbl          RMSSUB_XITEM.PHASE_ID_TBLTYPE ;
   L_diff_id_tbl           RMSSUB_XITEM.DIFF_ID_TBLTYPE;
   L_udaid_udadate_tbl     RMSSUB_XITEM.UDA_ID_TBLTYPE;
   L_udaid_udaff_tbl       RMSSUB_XITEM.UDA_ID_TBLTYPE;
   L_udaid_udaslov_tbl     RMSSUB_XITEM.UDA_ID_TBLTYPE;
   L_uda_date_tbl          RMSSUB_XITEM.UDA_DATE_TBLTYPE;
   L_uda_value_tbl         RMSSUB_XITEM.UDA_VALUE_TBLTYPE;
   L_uda_text_tbl          RMSSUB_XITEM.UDA_TEXT_TBLTYPE;
   L_count_udadate         NUMBER       := 1;
   L_count_udaff           NUMBER       := 1;
   L_count_udaslov         NUMBER       := 1;
   L_imagename_tbl         RMSSUB_XITEM.IMAGE_NAME_TBLTYPE;
   L_count_itemimage       NUMBER       := 1;
   L_item_desc_rec         RMSSUB_XITEM.ITEM_API_REC := NULL;

BEGIN
   L_item_rec.item := I_message.item;

   if I_message_type = RMSSUB_XITEM.SEASON_DEL and (I_message.xitemseasonref_tbl is NOT NULL and I_message.xitemseasonref_tbl.count > 0) then
      FOR i IN I_message.xitemseasonref_tbl.FIRST..I_message.xitemseasonref_tbl.LAST LOOP
         L_season_id_tbl(i)          := I_message.xitemseasonref_tbl(i).season_id;
         L_phase_id_tbl(i)           := I_message.xitemseasonref_tbl(i).phase_id;
         L_diff_id_tbl(i)            := I_message.xitemseasonref_tbl(i).diff_id;
      END LOOP;
      ---
      L_item_rec.seasons.season_id           := L_season_id_tbl;
      L_item_rec.seasons.phase_id            := L_phase_id_tbl;
      L_item_rec.seasons.diff_id             := L_diff_id_tbl;
   end if;

   if I_message_type = RMSSUB_XITEM.ITEMUDA_DEL and (I_message.xitemudaref_tbl is NOT NULL and I_message.xitemudaref_tbl.count > 0) then
      FOR i IN I_message.xitemudaref_tbl.FIRST..I_message.xitemudaref_tbl.LAST LOOP
         if I_message.xitemudaref_tbl(i).display_type = 'DT' then
            L_udaid_udadate_tbl(L_count_udadate)           := I_message.xitemudaref_tbl(i).uda_id;
            L_uda_date_tbl(L_count_udadate)                := to_date(SUBSTR(I_message.xitemudaref_tbl(i).uda_value, 1, 10), 'YYYY-MM-DD');
            ---
            L_count_udadate   := L_count_udadate + 1;
         elsif I_message.xitemudaref_tbl(i).display_type = 'FF' then
            L_udaid_udaff_tbl(L_count_udaff)               := I_message.xitemudaref_tbl(i).uda_id;
            L_uda_text_tbl(L_count_udaff)                  := I_message.xitemudaref_tbl(i).uda_value;
            ---
            L_count_udaff   := L_count_udaff + 1;
         elsif I_message.xitemudaref_tbl(i).display_type = 'LV' then
            L_udaid_udaslov_tbl(L_count_udaslov)           := I_message.xitemudaref_tbl(i).uda_id;
            L_uda_value_tbl(L_count_udaslov)               := I_message.xitemudaref_tbl(i).uda_value;
            ---
            L_count_udaslov   := L_count_udaslov + 1;
         end if;
      END LOOP;

      L_item_rec.udadate.uda_id            := L_udaid_udadate_tbl;
      L_item_rec.udadate.uda_date          := L_uda_date_tbl;
      L_item_rec.udaff.uda_id              := L_udaid_udaff_tbl;
      L_item_rec.udaff.uda_text            := L_uda_text_tbl;
      L_item_rec.udaslov.uda_id            := L_udaid_udaslov_tbl;
      L_item_rec.udaslov.uda_value         := L_uda_value_tbl;
   end if;
   ---
   if I_message_type = RMSSUB_XITEM.IMAGE_DEL and (I_message.xitemimageref_tbl is NOT NULL and I_message.xitemimageref_tbl.COUNT > 0) then
      FOR i IN I_message.xitemimageref_tbl.FIRST..I_message.xitemimageref_tbl.LAST LOOP
         L_imagename_tbl(L_count_itemimage) := I_message.xitemimageref_tbl(i).image_name;
         L_count_itemimage                  := L_count_itemimage + 1;
      END LOOP;
      ---
      L_item_rec.images.image_name := L_imagename_tbl;
   end if;
   ---
   L_item_rec.item_level := LP_im_row.item_level;
   L_item_rec.tran_level := LP_im_row.tran_level;
   if POPULATE_TRANS_LANG(O_error_message,
                          L_item_desc_rec,
                          L_item_rec,
                          NULL,
                          I_message,
                          I_message_type) = FALSE then
      return FALSE;
   end if;
   O_item_rec            := L_item_rec;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_LOCS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_iscl_tbl            OUT      RMSSUB_XITEM.ISCL_TBLTYPE,
                       I_iscl_rows           IN       "RIB_XISCLocDesc_TBL",
                       I_supplier            IN       SUPS.SUPPLIER%TYPE,
                       I_country             IN       COUNTRY.COUNTRY_ID%TYPE,
                       I_unit_cost           IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE, --- isc default
                       I_item                IN       ITEM_MASTER.ITEM%TYPE,
                       I_message_type        IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'RMSSUB_XITEM_POP_RECORD.POPULATE_LOCS';
   L_hier_ids          LOC_TBL := LOC_TBL();
   L_locs              LOC_TBL := LOC_TBL();

   L_subquery          VARCHAR2(2000);
   L_iscl_tbl          RMSSUB_XITEM.ISCL_TBLTYPE;
   L_hier_level        VARCHAR2(2) := LP_isc_hier_level;
   L_main        VARCHAR2(30) := NULL;
   L_tbl_count         NUMBER := 0;

   L_exists            BOOLEAN;
   L_unit_cost         ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_prim_loc          ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_prim_loc_chg      BOOLEAN := FALSE;
   L_phy_wh_ind        BOOLEAN := FALSE;
   L_prim_loc_cnt      NUMBER := 0;
   L_bind_no_sup       VARCHAR2(1) := NULL;

BEGIN
   --- Process one hierarchy value at a time because of iscl attributes are for each value
   FOR i IN I_iscl_rows.FIRST..I_iscl_rows.LAST LOOP
      L_hier_ids.EXTEND;
      L_hier_ids(1) := I_iscl_rows(i).hier_id;
      if L_hier_level = 'W' then
         if WH_QUERY(O_error_message,
                     L_subquery,
                     L_hier_ids(1)) = FALSE then
            exit;
         end if;
         if LP_system_options_row.org_unit_ind = 'Y' then
            L_subquery := L_subquery || '  and exists (select 1 '||
                                        '                from partner_org_unit pou'||
                                        '               where pou.partner = :3'||
                                        '                 and pou.partner_type in (''S'',''U'')'||
                                        '                 and (pou.org_unit_id = wh.org_unit_id) or'||
                                        '                      exists (select ''x'' '||
                                        '                                from sups_imp_exp sie'||
                                        '                               where sie.supplier = pou.partner'||
                                        '                                 and rownum = 1))';
         end if;
      else
         if LP_system_options_row.org_unit_ind = 'Y' then
            L_bind_no_sup := '3';
         end if;
         if ORGANIZATION_SQL.ORG_HIER_SUBQUERY(O_error_message,
                                               L_subquery,
                                               '1', -- bind number
                                               L_hier_level,
                                               L_hier_ids,
                                               L_main,
                                               L_bind_no_sup) = FALSE then
            return FALSE;
         end if;
      end if;

     if L_hier_level = 'W' then
        if not WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                       L_phy_wh_ind,
                                       L_hier_ids(1)) then
           return false;
        end if;
     end if;
     if I_message_type = RMSSUB_XITEM.ISCL_ADD and
        L_hier_level not in ('S','W')          then
         L_subquery := L_subquery ||
         ' and not exists (select ''x'' from item_supp_country_loc i '||
         '                  where i.loc = s.store '||
         '                    and i.item = :2 and i.supplier = :3 '||
         '                    and i.origin_country_id = :4 )'||
         ' and exists (select ''x'' from item_loc il '||
         '              where il.item = :2 '||
         '                and il.loc = :1)';

         if LP_system_options_row.org_unit_ind = 'Y' then
            EXECUTE IMMEDIATE L_subquery
               bulk collect into L_locs
               USING L_hier_ids(1),I_supplier,I_item,I_supplier,I_country;
         else
            EXECUTE IMMEDIATE L_subquery
               bulk collect into L_locs
               USING L_hier_ids(1),I_item,I_supplier,I_country;
         end if;
      elsif I_message_type = RMSSUB_XITEM.ISCL_UPD and
            L_hier_level = 'W' and L_phy_wh_ind = TRUE then
            
            L_subquery := L_subquery ||
            ' and exists (select ''x'' from item_loc il '||
            '              where il.item = :2 '||
            '                and il.loc = wh.wh '||
            '                and il.loc_type = ''W'' )';
            
            if LP_system_options_row.org_unit_ind = 'Y' then
               EXECUTE IMMEDIATE L_subquery
                  bulk collect into L_locs
                  USING L_hier_ids(1),I_supplier,I_item;
            else
               EXECUTE IMMEDIATE L_subquery
                  bulk collect into L_locs
                  USING L_hier_ids(1),I_item;
            end if;
      else       
         if LP_system_options_row.org_unit_ind = 'Y' then
            EXECUTE IMMEDIATE L_subquery
               bulk collect into L_locs
               USING L_hier_ids(1),I_supplier;
         else
            EXECUTE IMMEDIATE L_subquery
               bulk collect into L_locs
               USING L_hier_ids(1);
          end if;
      end if;

      if L_locs is NOT NULL and L_locs.COUNT > 0 then -- Make sure hierarchy has stores
         ---
         L_iscl_tbl(i).supplier         := I_supplier;
         L_iscl_tbl(i).country          := I_country;
         L_iscl_tbl(i).locs             := L_locs;
         L_iscl_tbl(i).pickup_lead_time := I_iscl_rows(i).pickup_lead_time;
         L_iscl_tbl(i).round_lvl := I_iscl_rows(i).round_lvl;
         L_iscl_tbl(i).round_to_inner_pct := I_iscl_rows(i).round_to_inner_pct;
         L_iscl_tbl(i).round_to_case_pct := I_iscl_rows(i).round_to_case_pct;
         L_iscl_tbl(i).round_to_layer_pct := I_iscl_rows(i).round_to_layer_pct;
         L_iscl_tbl(i).round_to_pallet_pct := I_iscl_rows(i).round_to_pallet_pct;
         L_iscl_tbl(i).supp_hier_lvl_1 := I_iscl_rows(i).supp_hier_lvl_1;
         L_iscl_tbl(i).supp_hier_lvl_2 := I_iscl_rows(i).supp_hier_lvl_2;
         L_iscl_tbl(i).supp_hier_lvl_3 := I_iscl_rows(i).supp_hier_lvl_3;
         L_iscl_tbl(i).primary_loc_ind := I_iscl_rows(i).primary_loc_ind;
         
         if L_hier_level = 'W' and L_phy_wh_ind = TRUE then
            L_iscl_tbl(i).phy_wh_ind := 'Y';
         end if;

         FOR j in 1..L_locs.COUNT LOOP
         
            L_iscl_tbl(i).loc_cost(j).location := L_locs(j);
              L_iscl_tbl(i).loc_cost(j).unit_cost := I_iscl_rows(i).unit_cost;
            -- I_unit_cost is the default from ISC payload. Need to pass in the costs in ISCL payload
            -- There is no base_cost on payload, assume unit_cost.
            L_iscl_tbl(i).loc_cost(j).base_cost := I_iscl_rows(i).unit_cost;
            L_iscl_tbl(i).loc_cost(j).negotiated_item_cost := I_iscl_rows(i).negotiated_item_cost;
         END LOOP;

         --- Re-initialize to get the next single hier id
            L_hier_ids := NULL;
            L_hier_ids := loc_tbl();
            
     else  --- No valid stores in hierarchy or invalid store/warehouse
         O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE_LOC_SUPP',I_iscl_rows(i).hier_id,I_supplier,NULL);
         return FALSE;
     end if;
   END LOOP;
   
   if O_error_message is NOT NULL then
      return FALSE;
   end if;
   ---
   O_iscl_tbl := L_iscl_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_LOCS;
------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DEL_LOCS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_item_rec            OUT      RMSSUB_XITEM.ITEM_API_DEL_REC,
                           I_isup_rows           IN       "RIB_XItemSupRef_TBL",
                           I_item                IN       ITEM_MASTER.ITEM%TYPE,
                           I_message_type        IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'RMSSUB_XITEM_VALIDATE.POPULATE_DEL_LOCS';
   L_hier_ids          loc_tbl := loc_tbl();
   L_locs              loc_tbl := loc_tbl();

   L_iscl_tbl          RMSSUB_XITEM.ISCL_DEL_TBLTYPE;
   L_finaL_iscl_tbl    RMSSUB_XITEM.ISCL_DEL_TBLTYPE;
   L_isc_rows          "RIB_XItemSupCtyRef_TBL";
   L_iscl_rows         "RIB_XISCLocRef_TBL";

   L_subquery          VARCHAR2(2000);
   L_hier_level        VARCHAR2(2) := LP_isc_hier_level;
   L_main_table        VARCHAR2(30) := NULL;
   L_tbl_count         NUMBER := 0;
   L_loc_cnt           NUMBER := 0;

   L_exists            BOOLEAN;

BEGIN
   if I_message_type = RMSSUB_XITEM.ISCL_DEL then
      FOR i IN I_isup_rows.FIRST..I_isup_rows.LAST LOOP
         L_isc_rows := I_isup_rows(i).XItemSupCtyRef_TBL;
         --- Countries
         if L_isc_rows is NOT NULL and L_isc_rows.COUNT > 0 then
            FOR j IN L_isc_rows.FIRST..L_isc_rows.LAST LOOP
               if L_isc_rows(j).XISCLocRef_TBL is NOT NULL and
                  L_isc_rows(j).XISCLocRef_TBL.COUNT > 0   then
                  L_iscl_rows := L_isc_rows(j).XISCLocRef_TBL;
                  --- Process one hierarchy value at a time because of iscl attributes are for each value
                  FOR h IN L_iscl_rows.FIRST..L_iscl_rows.LAST LOOP
                     L_hier_ids.EXTEND;
                     L_hier_ids(1) := L_iscl_rows(h).hier_id;

                     if L_hier_level = 'W' then
                        if WH_QUERY(O_error_message,
                                    L_subquery,
                                    L_hier_ids(1)) = FALSE then
                           exit;
                        end if;
                     else
                        if ORGANIZATION_SQL.ORG_HIER_SUBQUERY(O_error_message,
                                                              L_subquery,
                                                              '1', -- bind number
                                                              L_hier_level,
                                                              L_hier_ids,
                                                              L_main_table) = FALSE then
                           return FALSE;
                        end if;
                     end if;

                     EXECUTE IMMEDIATE L_subquery
                        bulk collect into L_locs
                        USING L_hier_ids(1);

                     if L_locs is NOT NULL and L_locs.COUNT > 0 then -- Make sure hierarchy has stores
                        --
                        L_iscl_tbl(h).supplier        := I_isup_rows(i).supplier;
                        L_iscl_tbl(h).country         := L_isc_rows(j).origin_country_id;
                        L_iscl_tbl(h).locs            := L_locs;
                        --- Re-initialize to get the next single hier id
                        L_hier_ids := NULL;
                        L_hier_ids := loc_tbl();
                     else  --- No stores for the hierarchy value
                        O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE','Hier Id',L_iscl_rows(h).hier_id,NULL);
                        return FALSE;
                     end if;
                  END LOOP; --- hier ids

                  if L_iscl_tbl is NOT NULL and L_iscl_tbl.COUNT > 0 then
                     L_loc_cnt := L_final_iscl_tbl.COUNT + 1;
                     FOR tbl_cnt IN L_iscl_tbl.FIRST..L_iscl_tbl.LAST LOOP
                        L_final_iscl_tbl(L_loc_cnt) := L_iscl_tbl(tbl_cnt);
                        L_loc_cnt := L_loc_cnt + 1;
                     END LOOP;
                     L_iscl_tbl.DELETE;
                  end if;

               end if;  --- check for location (hier id) populated node
            END LOOP; --- countries
         end if; --- check for countries populated node
      END LOOP;  ---- suppliers

   O_item_rec.iscl_tbl := L_final_iscl_tbl;

   end if;  --- check message type

   O_item_rec.item := I_item;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_DEL_LOCS;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ZONES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_izp_tbl         OUT      RMSSUB_XITEM.IZP_TBLTYPE,
                        I_izp_rows        IN       "RIB_XIZPDesc_TBL",
                        I_standard_uom    IN       ITEM_MASTER.STANDARD_UOM%TYPE)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'RMSSUB_XITEM_POP_RECORD.POPULATE_ZONES';
   L_hier_ids          loc_tbl := loc_tbl();
   L_zones             loc_tbl := loc_tbl();

   L_subquery          VARCHAR2(2000);
   L_izp_tbl           RMSSUB_XITEM.IZP_TBLTYPE;
   L_hier_level        VARCHAR2(2) := LP_izp_hier_level;
   L_main_table        VARCHAR2(30) := NULL;
   L_currency_bind_no  VARCHAR2(1);
   L_country_bind_no   VARCHAR2(1);

BEGIN
   --- Zone node populated checked in populate record
   --- Process one hierarchy value at a time because of izp attributes are for each value
   if I_izp_rows is NOT NULL and I_izp_rows.COUNT > 0 then
      FOR i IN I_izp_rows.FIRST..I_izp_rows.LAST LOOP
         if I_izp_rows(i).currency_code is NOT NULL and
            I_izp_rows(i).country_id    is NOT NULL then
            L_currency_bind_no := '2';
            L_country_bind_no  := '3';
         elsif I_izp_rows(i).currency_code is NOT NULL and
            I_izp_rows(i).country_id is NULL           then
            L_currency_bind_no := '2';
            L_country_bind_no  := NULL;
         elsif I_izp_rows(i).currency_code is NULL and
            I_izp_rows(i).country_id is NOT NULL   then
            L_currency_bind_no := NULL;
            L_country_bind_no  := '2';
         end if;
         ---
         L_hier_ids.EXTEND;
         L_hier_ids(1) := I_izp_rows(i).hier_id;
         if L_hier_level = 'W' then
            if WH_QUERY(O_error_message,
                        L_subquery,
                        L_hier_ids(1)) = FALSE then
               exit;
            end if;
            if I_izp_rows(i).currency_code is NOT NULL then
               L_subquery := L_subquery||' and currency_code = :'||L_currency_bind_no||' ';
            end if;
            if I_izp_rows(i).country_id is NOT NULL then
               if ORGANIZATION_SQL.WH_COUNTRY_SUBQUERY(O_error_message,
                                                       L_subquery,
                                                       L_country_bind_no) = FALSE then
                  return FALSE;
               end if;
            end if;
         else
            if ORGANIZATION_SQL.ORG_HIER_SUBQUERY(O_error_message,
                                                  L_subquery,
                                                  '1', -- bind number
                                                  L_hier_level,
                                                  L_hier_ids,
                                                  L_main_table,
                                                  L_currency_bind_no,
                                                  L_country_bind_no,
                                                  NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if I_izp_rows(i).currency_code is NOT NULL and
            I_izp_rows(i).country_id is NOT NULL then
               EXECUTE IMMEDIATE L_subquery
                  bulk collect into L_zones
                  USING L_hier_ids(1),I_izp_rows(i).currency_code,I_izp_rows(i).country_id;
         elsif I_izp_rows(i).currency_code is NOT NULL and
            I_izp_rows(i).country_id is NULL then
               EXECUTE IMMEDIATE L_subquery
                  bulk collect into L_zones
                  USING L_hier_ids(1),I_izp_rows(i).currency_code;
         elsif I_izp_rows(i).currency_code is NULL and
            I_izp_rows(i).country_id is NOT NULL then
               EXECUTE IMMEDIATE L_subquery
                  bulk collect into L_zones
                  USING L_hier_ids(1),I_izp_rows(i).country_id;
         else
               EXECUTE IMMEDIATE L_subquery
                  bulk collect into L_zones
                  USING L_hier_ids(1);
         end if;
         if L_zones is NOT NULL and L_zones.COUNT > 0 then -- Make sure hierarchy has stores
            L_izp_tbl(i).selling_unit_retail  := I_izp_rows(i).selling_unit_retail;
            L_izp_tbl(i).selling_uom          := I_izp_rows(i).selling_uom;
            L_izp_tbl(i).multi_selling_uom    := I_izp_rows(i).multi_selling_uom;
            L_izp_tbl(i).multi_units          := I_izp_rows(i).multi_units;
            L_izp_tbl(i).multi_unit_retail    := I_izp_rows(i).multi_unit_retail;
            if I_izp_rows(i).currency_code is NOT NULL then
               L_izp_tbl(i).currency_code        := I_izp_rows(i).currency_code;
            elsif L_hier_level = 'W' then
               if WH_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                                  L_izp_tbl(i).currency_code,
                                                  L_zones(1)) = FALSE then
                  return FALSE;
               end if;
            elsif L_hier_level = 'S' then
               if STORE_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                                     L_izp_tbl(i).currency_code,
                                                     L_zones(1)) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
            L_izp_tbl(i).zones := L_zones;
            ---
            --- Create unit_retail based on selling_uom
            if I_standard_uom = nvl(I_izp_rows(i).selling_uom,I_standard_uom) then
               L_izp_tbl(i).unit_retail := I_izp_rows(i).selling_unit_retail;
            end if;

            ---
            if O_error_message is NOT NULL then
               return FALSE;
            end if;
            --- Re-initialize to get the next single hier id
            L_hier_ids := NULL;
            L_hier_ids := loc_tbl();
         end if;
      END LOOP;
   end if;
   O_izp_tbl := L_izp_tbl;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_ZONES;
----------------------------------------------------------------------------------------
FUNCTION VALID_HIER_LEVEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hier_level      IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XITEM_VALIDATE.VALID_HIER_LEVEL';

BEGIN

   if UPPER(I_hier_level) not in (ORGANIZATION_SQL.LP_chain,
                                  ORGANIZATION_SQL.LP_area,
                                  ORGANIZATION_SQL.LP_region,
                                  ORGANIZATION_SQL.LP_district,'S','W') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_LEVEL',
                                            'Organization Level: '||I_hier_level,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALID_HIER_LEVEL;
------------------------------------------------------------------------------------------------------
FUNCTION ORGHIER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_message         IN       "RIB_XItemDesc_REC",
                        I_del_message     IN       "RIB_XItemRef_REC",
                        I_node            IN       VARCHAR2,
                        I_hier_level      IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'RMSSUB_XITEM_VALIDATE.ORGHIER_EXISTS';

   L_exists         BOOLEAN := FALSE;
   L_type           NUMBER;
   L_hier_ids       loc_tbl := loc_tbl();
   L_hier_ids_pop   BOOLEAN := TRUE;
   L_isc_rows       "RIB_XItemSupCtyDesc_TBL";
   L_del_rows       "RIB_XItemSupCtyRef_TBL";

BEGIN

   if I_hier_level NOT in ('S', 'W') then
      if UPPER(I_hier_level) = ORGANIZATION_SQL.LP_chain then
         L_type := 10;
      elsif UPPER(I_hier_level) = ORGANIZATION_SQL.LP_area then
         L_type := 20;
      elsif UPPER(I_hier_level) = ORGANIZATION_SQL.LP_region then
         L_type := 30;
      elsif UPPER(I_hier_level) = ORGANIZATION_SQL.LP_district then
         L_type := 40;
      end if;

      if I_node = 'IZP' then
         if I_message.XIZPDesc_TBL is NOT NULL AND I_message.XIZPDesc_TBL.COUNT > 0 then
            FOR i IN I_message.XIZPDesc_TBL.FIRST..I_message.XIZPDesc_TBL.LAST LOOP
               L_hier_ids.EXTEND;
               L_hier_ids(i) := I_message.XIZPDesc_TBL(i).hier_id;
            END LOOP;
         else
            L_hier_ids_pop := FALSE;
         end if;
      elsif I_node = 'ISCL' then
         if I_message.XItemSupDesc_TBL is NOT NULL AND I_message.XItemSupDesc_TBL.COUNT > 0 then
            FOR i IN I_message.XItemSupDesc_TBL.FIRST..I_message.XItemSupDesc_TBL.LAST LOOP
               L_isc_rows := I_message.XItemSupDesc_TBL(i).XItemSupCtyDesc_TBL;
               if L_isc_rows is NOT NULL AND L_isc_rows.COUNT > 0 then
                  FOR j IN L_isc_rows.FIRST..L_isc_rows.LAST LOOP
                     if L_isc_rows(j).XISCLocDesc_TBL is NOT NULL AND L_isc_rows(j).XISCLocDesc_TBL.COUNT > 0 then
                        FOR l IN L_isc_rows(j).XISCLocDesc_TBL.FIRST..L_isc_rows(j).XISCLocDesc_TBL.LAST LOOP
                           L_hier_ids.EXTEND;
                           L_hier_ids(L_hier_ids.count) := L_isc_rows(j).XISCLocDesc_TBL(l).hier_id;
                        END LOOP; --- locs
                     else
                        L_hier_ids_pop := FALSE;
                     end if; --- locs pop
                  END LOOP;--- countries
               else
                  L_hier_ids_pop := FALSE;
               end if; --- countries pop
            END LOOP;---sups
         else
            L_hier_ids_pop := FALSE;
         end if; --- sups populated
      elsif I_node = 'DELISCL' then
         if I_del_message.XItemSupRef_TBL is NOT NULL AND I_del_message.XItemSupRef_TBL.COUNT > 0 then
            FOR i IN I_del_message.XItemSupRef_TBL.FIRST..I_del_message.XItemSupRef_TBL.LAST LOOP
               L_del_rows := I_del_message.XItemSupRef_TBL(i).XItemSupCtyRef_TBL;
               if L_del_rows is NOT NULL AND L_del_rows.COUNT > 0 then
                  FOR j IN L_del_rows.FIRST..L_del_rows.LAST LOOP
                     if L_del_rows(j).XISCLocRef_TBL is NOT NULL AND L_del_rows(j).XISCLocRef_TBL.COUNT > 0 then
                        FOR l IN L_del_rows(j).XISCLocRef_TBL.FIRST..L_del_rows(j).XISCLocRef_TBL.LAST LOOP
                           L_hier_ids.EXTEND;
                           L_hier_ids(L_hier_ids.count) := L_del_rows(j).XISCLocRef_TBL(l).hier_id;
                        END LOOP; --- locs
                     else
                        L_hier_ids_pop := FALSE;
                     end if; --- locs pop
                  END LOOP;--- countries
               else
                  L_hier_ids_pop := FALSE;
               end if; --- countries pop
            END LOOP;---sups
         else
            L_hier_ids_pop := FALSE;
         end if; --- sups populated
      end if; --- I_node
      ---
      if L_hier_ids_pop then
         FOR i in L_hier_ids.FIRST..L_hier_ids.LAST LOOP
            if L_hier_ids(i) is NOT NULL then
               if not ORGANIZATION_VALIDATE_SQL.EXIST(O_error_message,
                                                      L_type,
                                                      L_hier_ids(i),
                                                      L_exists) then
                  return FALSE;
               end if;

               if NOT L_exists then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE',
                                                        'Hier Id',
                                                        L_hier_ids(i),
                                                        NULL);
                  return FALSE;
               end if;
            end if;
         end LOOP;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END ORGHIER_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE,
                       I_message         IN       "RIB_XItemDesc_REC"  DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)                   := 'RMSSUB_XITEM_POP_RECORD.GET_ITEM_INFO';
   L_item_api_rec    ITEM_MASTER%ROWTYPE            := NULL;
   L_pack_type       ITEM_MASTER.PACK_TYPE%TYPE     := NULL;
   L_orderable_ind   ITEM_MASTER.orderable_ind%TYPE := NULL;
   L_exist           BOOLEAN;

BEGIN

   LP_im_row.item := I_item;
   ---
   if ITEM_VALIDATE_SQL.EXIST(O_error_message,
                              L_exist,
                              I_item) = FALSE then
      return FALSE;
   end if;

   if L_exist then
      if NOT ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                             L_item_api_rec,
                                             I_item) then
         return FALSE;
      end if;

      LP_im_row.item_level := L_item_api_rec.item_level;
      LP_im_row.tran_level := L_item_api_rec.tran_level;
      LP_im_row.pack_ind := NVL(L_item_api_rec.pack_ind, 'N');
      LP_im_row.pack_type := L_item_api_rec.pack_type;
      LP_im_row.orderable_ind := NVL(L_item_api_rec.orderable_ind, 'N');
      LP_im_row.status := L_item_api_rec.status;
      LP_im_row.standard_uom := L_item_api_rec.standard_uom;
      LP_im_row.aip_case_type := L_item_api_rec.aip_case_type;
      L_pack_type := L_item_api_rec.pack_type;
      L_orderable_ind := L_item_api_rec.orderable_ind;
   else
      LP_im_row.item_level := I_message.item_level;
      LP_im_row.tran_level := I_message.tran_level;
      LP_im_row.pack_ind := I_message.pack_ind;
      LP_im_row.pack_type := I_message.pack_type;
      LP_im_row.orderable_ind := I_message.orderable_ind;
      LP_im_row.status := I_message.status;
      LP_im_row.standard_uom := I_message.standard_uom;
      LP_im_row.aip_case_type := I_message.aip_case_type;
      L_pack_type := I_message.pack_type;
      L_orderable_ind := I_message.orderable_ind;
      O_error_message := NULL;
   end if;

   if NOT POP_BUYER_PACK(O_error_message,
                         L_pack_type,
                         L_orderable_ind) then
      return FALSE;
   end if;

   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ITEM_INFO;
---------------------------------------------------------------------------------------------------------
FUNCTION PRIMARY_COUNTRY(O_error_message   IN OUT   VARCHAR2,
                         O_exists          IN OUT   BOOLEAN,
                         O_prim_country    IN OUT   ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_item            IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                         I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_POP_RECORD.PRIMARY_COUNTRY';

BEGIN

   if NOT ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                    O_exists,
                                                    O_prim_country,
                                                    I_item,
                                                    I_supplier) then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PRIMARY_COUNTRY;
---------------------------------------------------------------------------------------------------------
FUNCTION PRIMARY_SUPPLIER(O_error_message   IN OUT   VARCHAR2,
                          O_exists          IN OUT   BOOLEAN,
                          O_prim_supplier   IN OUT   SUPS.SUPPLIER%TYPE,
                          I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_POP_RECORD.PRIMARY_SUPPLIER';

BEGIN
   if NOT SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_SUPP(O_error_message,
                                                O_exists,
                                                O_prim_supplier,
                                                I_item) then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PRIMARY_SUPPLIER;
---------------------------------------------------------------------------------------------------------
FUNCTION WH_QUERY(O_error_message   IN OUT   VARCHAR2,
                  O_subquery        IN OUT   VARCHAR2,
                  I_hier_id         IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_POP_RECORD.WH_QUERY';
   L_flag         BOOLEAN := false;

BEGIN

   O_subquery := 'select wh ' ||
                 '  from wh ';
   --
   if not WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                  L_flag,
                                  I_hier_id) then
      return false;
   end if;
   --
   if L_flag = TRUE then
      O_subquery := O_subquery || ' where wh.physical_wh = :1 ';
   else
      O_subquery := O_subquery || ' where wh = :1 ';
   end if;
   
   O_subquery := O_subquery || '   and wh.wh != wh.physical_wh ';
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END WH_QUERY;
---------------------------------------------------------------------------------------------------------
FUNCTION RESET_GLOBALS(O_error_message   IN OUT   VARCHAR2)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_POP_RECORD.RESET_GLOBALS';

BEGIN

   LP_im_row := NULL;
   LP_isup_row := NULL;
   LP_isc_row := NULL;
   LP_iscl_row := NULL;

   LP_system_options_row := NULL;

   LP_mod_isup_tbl.DELETE;
   LP_mod_isc_tbl.DELETE;
   LP_mod_ismc_tbl.DELETE;
   LP_pi_tbl.DELETE;

   LP_isc_hier_level := NULL;
   LP_izp_hier_level := NULL;
   LP_pack_type  := 'N';
   LP_buyer_pack  := FALSE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END RESET_GLOBALS;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_TRANS_LANG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_item_rec        IN OUT   RMSSUB_XITEM.ITEM_API_REC,
                             O_item_del_rec    IN OUT   RMSSUB_XITEM.ITEM_API_DEL_REC,
                             I_message_desc    IN       "RIB_XItemDesc_REC",
                             I_message_ref     IN       "RIB_XItemRef_REC",
                             I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'RMSSUB_XITEM_POP_RECORD.POPULATE_TRANS_LANG';
   L_imtl_cnt             NUMBER       := 1;
   L_im_lang_tbl          RMSSUB_XITEM.LANG_TBLTYPE;
   L_shortdesc_tbl        RMSSUB_XITEM.SHORT_DESC_TBLTYPE;
   L_itemdesc_tbl         RMSSUB_XITEM.ITEM_DESC_TBLTYPE;
   L_itemdescsec_tbl      RMSSUB_XITEM.ITEM_DESC_SEC_TBLTYPE;
   L_istl_cnt             NUMBER       := 1;
   L_is_lang_tbl          RMSSUB_XITEM.LANG_TBLTYPE;
   L_supplier_tbl         RMSSUB_XITEM.SUPPLIER_TBLTYPE;
   L_supplabel_tbl        RMSSUB_XITEM.SUPP_LABEL_TBLTYPE;
   L_suppdiff1_tbl        RMSSUB_XITEM.SUPP_DIFF1_TBLTYPE;
   L_suppdiff2_tbl        RMSSUB_XITEM.SUPP_DIFF2_TBLTYPE;
   L_suppdiff3_tbl        RMSSUB_XITEM.SUPP_DIFF3_TBLTYPE;
   L_suppdiff4_tbl        RMSSUB_XITEM.SUPP_DIFF4_TBLTYPE;
   L_iitl_cnt             NUMBER       := 1;
   L_ii_lang_tbl          RMSSUB_XITEM.LANG_TBLTYPE;
   L_imagename_tbl        RMSSUB_XITEM.IMAGE_NAME_TBLTYPE;
   L_imagedesc_tbl        RMSSUB_XITEM.IMAGE_DESC_TBLTYPE;
   L_imdel_cnt            NUMBER       := 1;
   L_imdel_lang_tbl       RMSSUB_XITEM.LANG_TBLTYPE;
   L_isdel_cnt            NUMBER       := 1;
   L_isdel_lang_tbl       RMSSUB_XITEM.LANG_TBLTYPE;
   L_isdel_supp_tbl       RMSSUB_XITEM.SUPPLIER_TBLTYPE;
   L_iidel_cnt            NUMBER       := 1;
   L_iidel_lang_tbl       RMSSUB_XITEM.LANG_TBLTYPE;
   L_iidel_image_tbl      RMSSUB_XITEM.IMAGE_NAME_TBLTYPE;

BEGIN

   if I_message_type in (RMSSUB_XITEM.ITEM_ADD, RMSSUB_XITEM.IMTL_ADD, RMSSUB_XITEM.IMTL_UPD) then
      if I_message_desc.LangOfXItemDesc_TBL is NOT NULL and I_message_desc.LangOfXItemDesc_TBL.count > 0 then
         FOR i in I_message_desc.LangOfXItemDesc_TBL.FIRST..I_message_desc.LangOfXItemDesc_TBL.LAST LOOP
            L_im_lang_tbl(L_imtl_cnt)     := I_message_desc.LangOfXItemDesc_TBL(i).lang;
            if I_message_type = RMSSUB_XITEM.ITEM_ADD then
               L_shortdesc_tbl(L_imtl_cnt)   := NVL(I_message_desc.LangOfXItemDesc_TBL(i).short_desc, RTRIM(SUBSTRB(I_message_desc.LangOfXItemDesc_TBL(i).item_desc,1,120)));
            else
               L_shortdesc_tbl(L_imtl_cnt)   := I_message_desc.LangOfXItemDesc_TBL(i).short_desc;
            end if;
            L_itemdesc_tbl(L_imtl_cnt)    := I_message_desc.LangOfXItemDesc_TBL(i).item_desc;
            L_itemdescsec_tbl(L_imtl_cnt) := I_message_desc.LangOfXItemDesc_TBL(i).item_desc_secondary;
            L_imtl_cnt                    := L_imtl_cnt + 1;
         END LOOP;
         O_item_rec.Item_Master_lang.lang          := L_im_lang_tbl;
         O_item_rec.Item_Master_lang.short_desc    := L_shortdesc_tbl;
         O_item_rec.Item_Master_lang.item_desc     := L_itemdesc_tbl;
         O_item_rec.Item_Master_lang.item_desc_sec := L_itemdescsec_tbl;
      end if;
   end if;

   if I_message_type in (RMSSUB_XITEM.ITEM_ADD, RMSSUB_XITEM.ISUP_ADD, RMSSUB_XITEM.ISTL_ADD, RMSSUB_XITEM.ISTL_UPD) then
      if I_message_desc.XItemSupDesc_TBL is NOT NULL and I_message_desc.XItemSupDesc_TBL.count > 0 then
         FOR i in I_message_desc.XItemSupDesc_TBL.FIRST..I_message_desc.XItemSupDesc_TBL.LAST LOOP
            if I_message_desc.XItemSupDesc_TBL(i).LangOfXItemSupDesc_TBL is NOT NULL and I_message_desc.XItemSupDesc_TBL(i).LangOfXItemSupDesc_TBL.count > 0 then
               FOR j in I_message_desc.XItemSupDesc_TBL(i).LangOfXItemSupDesc_TBL.FIRST..I_message_desc.XItemSupDesc_TBL(i).LangOfXItemSupDesc_TBL.LAST LOOP
                  L_is_lang_tbl(L_istl_cnt)   := I_message_desc.XItemSupDesc_TBL(i).LangOfXItemSupDesc_TBL(j).lang;
                  L_supplier_tbl(L_istl_cnt)  := I_message_desc.XItemSupDesc_TBL(i).supplier;
                  L_supplabel_tbl(L_istl_cnt) := I_message_desc.XItemSupDesc_TBL(i).LangOfXItemSupDesc_TBL(j).supp_label;
                  L_suppdiff1_tbl(L_istl_cnt) := I_message_desc.XItemSupDesc_TBL(i).LangOfXItemSupDesc_TBL(j).supp_diff_1;
                  L_suppdiff2_tbl(L_istl_cnt) := I_message_desc.XItemSupDesc_TBL(i).LangOfXItemSupDesc_TBL(j).supp_diff_2;
                  L_suppdiff3_tbl(L_istl_cnt) := I_message_desc.XItemSupDesc_TBL(i).LangOfXItemSupDesc_TBL(j).supp_diff_3;
                  L_suppdiff4_tbl(L_istl_cnt) := I_message_desc.XItemSupDesc_TBL(i).LangOfXItemSupDesc_TBL(j).supp_diff_4;
                  L_istl_cnt                  := L_istl_cnt + 1;
               END LOOP;
               O_item_rec.Item_Supp_lang.lang        := L_is_lang_tbl;
               O_item_rec.Item_Supp_lang.supplier    := L_supplier_tbl;
               O_item_rec.Item_Supp_lang.supp_label  := L_supplabel_tbl;
               O_item_rec.Item_Supp_lang.supp_diff_1 := L_suppdiff1_tbl;
               O_item_rec.Item_Supp_lang.supp_diff_2 := L_suppdiff2_tbl;
               O_item_rec.Item_Supp_lang.supp_diff_3 := L_suppdiff3_tbl;
               O_item_rec.Item_Supp_lang.supp_diff_4 := L_suppdiff4_tbl;
            end if;
         END LOOP;
      end if;
   end if;

   if I_message_type in (RMSSUB_XITEM.ITEM_ADD, RMSSUB_XITEM.IMAGE_ADD, RMSSUB_XITEM.IITL_ADD, RMSSUB_XITEM.IITL_UPD) then
      if I_message_desc.XItemImage_TBL is NOT NULL and I_message_desc.XItemImage_TBL.count > 0 then
         FOR i in I_message_desc.XItemImage_TBL.FIRST..I_message_desc.XItemImage_TBL.LAST LOOP
            if I_message_desc.XItemImage_TBL(i).LangOfXItemImage_TBL is NOT NULL and I_message_desc.XItemImage_TBL(i).LangOfXItemImage_TBL.count > 0 then
               FOR j in I_message_desc.XItemImage_TBL(i).LangOfXItemImage_TBL.FIRST..I_message_desc.XItemImage_TBL(i).LangOfXItemImage_TBL.LAST LOOP
                  L_ii_lang_tbl(L_iitl_cnt)   := I_message_desc.XItemImage_TBL(i).LangOfXItemImage_TBL(j).lang;
                  L_imagename_tbl(L_iitl_cnt) := I_message_desc.XItemImage_TBL(i).image_name;
                  L_imagedesc_tbl(L_iitl_cnt) := I_message_desc.XItemImage_TBL(i).LangOfXItemImage_TBL(j).image_desc;
                  L_iitl_cnt                  := L_iitl_cnt + 1;
               END LOOP;
               O_item_rec.Item_Images_lang.lang       := L_ii_lang_tbl;
               O_item_rec.Item_Images_lang.image_name := L_imagename_tbl;
               O_item_rec.Item_Images_lang.image_desc := L_imagedesc_tbl;
            end if;
         END LOOP;
      end if;
   end if;

   if I_message_type = RMSSUB_XITEM.IMTL_DEL and (I_message_ref.LangOfXItemRef_TBL is NOT NULL and I_message_ref.LangOfXItemRef_TBL.count > 0) then
      FOR i in I_message_ref.LangOfXItemRef_TBL.FIRST..I_message_ref.LangOfXItemRef_TBL.LAST LOOP
         L_imdel_lang_tbl(L_imdel_cnt) := I_message_ref.LangOfXItemRef_TBL(i).lang;
         L_imdel_cnt                   := L_imdel_cnt + 1;
      END LOOP;
      O_item_del_rec.Item_Master_lang.lang := L_imdel_lang_tbl;
   end if;

   if I_message_type = RMSSUB_XITEM.ISTL_DEL and (I_message_ref.XItemSupRef_TBL is NOT NULL and I_message_ref.XItemSupRef_TBL.count > 0) then
      FOR i in I_message_ref.XItemSupRef_TBL.FIRST..I_message_ref.XItemSupRef_TBL.LAST LOOP
         if I_message_ref.XItemSupRef_TBL(i).LangOfXItemSupRef_TBL is NOT NULL and I_message_ref.XItemSupRef_TBL(i).LangOfXItemSupRef_TBL.count > 0 then
            FOR j in I_message_ref.XItemSupRef_TBL(i).LangOfXItemSupRef_TBL.FIRST..I_message_ref.XItemSupRef_TBL(i).LangOfXItemSupRef_TBL.LAST LOOP
               L_isdel_lang_tbl(L_isdel_cnt) := I_message_ref.XItemSupRef_TBL(i).LangOfXItemSupRef_TBL(j).lang;
               L_isdel_supp_tbl(L_isdel_cnt) := I_message_ref.XItemSupRef_TBL(i).supplier;
               L_isdel_cnt                   := L_isdel_cnt + 1;
            END LOOP;
            O_item_del_rec.Item_Supp_lang.lang     := L_isdel_lang_tbl;
            O_item_del_rec.Item_Supp_lang.supplier := L_isdel_supp_tbl;
         end if;
      END LOOP;
   end if;

   if I_message_type = RMSSUB_XITEM.IITL_DEL and (I_message_ref.XItemImageRef_TBL is NOT NULL and I_message_ref.XItemImageRef_TBL.count > 0) then
      FOR i in I_message_ref.XItemImageRef_TBL.FIRST..I_message_ref.XItemImageRef_TBL.LAST LOOP
         if I_message_ref.XItemImageRef_TBL(i).LangOfXItemImageRef_TBL is NOT NULL and I_message_ref.XItemImageRef_TBL(i).LangOfXItemImageRef_TBL.count > 0 then
            FOR j in I_message_ref.XItemImageRef_TBL(i).LangOfXItemImageRef_TBL.FIRST..I_message_ref.XItemImageRef_TBL(i).LangOfXItemImageRef_TBL.LAST LOOP
               L_iidel_lang_tbl(L_iidel_cnt)  := I_message_ref.XItemImageRef_TBL(i).LangOfXItemImageRef_TBL(j).lang;
               L_iidel_image_tbl(L_iidel_cnt) := I_message_ref.XItemImageRef_TBL(i).image_name;
               L_iidel_cnt                    := L_iidel_cnt + 1;
            END LOOP;
            O_item_del_rec.Item_Images_lang.lang       := L_iidel_lang_tbl;
            O_item_del_rec.Item_Images_lang.image_name := L_iidel_image_tbl;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_TRANS_LANG;
--------------------------------------------------------------------------------
END RMSSUB_XITEM_POP_RECORD;
/
