CREATE OR REPLACE PACKAGE ITEMLIST_LOC_MC_SQL AUTHID CURRENT_USER AS

-----------------------------------------------------------------------
-- ITEM_LIST:
--   Called from the Mass Item Location form if updates are being done
--     to an item list
--   Loop through items in the item list and do all the item/location
--     level checking to determine if the item's attributes can be
--     updated with the values the user has specified. Once all the
--     appropriate checks have been performed and passed, it updates
--     the database fields which the user has specified.
-----------------------------------------------------------------------
 FUNCTION ITEM_LIST (O_error_message           IN OUT   VARCHAR2,
                     O_reject_report           IN OUT   VARCHAR2,
                     I_item_list               IN       SKULIST_HEAD.SKULIST%TYPE,
                     I_cb1                     IN       VARCHAR2,
                     I_status                  IN       ITEM_LOC.STATUS%TYPE,
                     I_cb2                     IN       VARCHAR2,
                     I_taxable_ind             IN       ITEM_LOC.TAXABLE_IND%TYPE,
                     I_cb3                     IN       VARCHAR2,
                     I_supplier                IN       SUPS.SUPPLIER%TYPE,
                     I_primary_country         IN       ITEM_LOC.PRIMARY_CNTRY%TYPE,
                     I_cb4                     IN       VARCHAR2,
                     I_daily_waste_pct         IN       ITEM_LOC.DAILY_WASTE_PCT%TYPE,
                     I_cb5                     IN       VARCHAR2,
                     I_meas_of_each            IN       ITEM_LOC.MEAS_OF_EACH%TYPE,
                     I_cb6                     IN       VARCHAR2,
                     I_meas_of_price           IN       ITEM_LOC.MEAS_OF_PRICE%TYPE,
                     I_cb7                     IN       VARCHAR2,
                     I_uom_of_price            IN       ITEM_LOC.UOM_OF_PRICE%TYPE,
                     I_cb8                     IN       VARCHAR2,
                     I_primary_variant         IN       ITEM_LOC.PRIMARY_VARIANT%TYPE,
                     I_cb9                     IN       VARCHAR2,
                     I_ti                      IN       ITEM_LOC.TI%TYPE,
                     I_cb10                    IN       VARCHAR2,
                     I_hi                      IN       ITEM_LOC.HI%TYPE,
                     I_cb11                    IN       VARCHAR2,
                     I_loc_item_desc           IN       ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
                     I_cb12                    IN       VARCHAR2,
                     I_loc_short_desc          IN       ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
                     I_cb13                    IN       VARCHAR2,
                     I_primary_cost_pack       IN       ITEM_LOC.PRIMARY_COST_PACK%TYPE,
                     I_cb14                    IN       VARCHAR2,
                     I_source_method           IN       ITEM_LOC.SOURCE_METHOD%TYPE,
                     I_source_wh               IN       ITEM_LOC.SOURCE_WH%TYPE,
                     I_cb15                    IN       VARCHAR2,
                     I_store_ord_mult          IN       ITEM_LOC.STORE_ORD_MULT%TYPE,
                     I_cb16                    IN       VARCHAR2,
                     I_inbound_handling_days   IN       ITEM_LOC.INBOUND_HANDLING_DAYS%TYPE,
                     I_cb17                    IN       VARCHAR2,
                     I_uin_type                IN       ITEM_LOC.UIN_TYPE%TYPE,
                     I_uin_label               IN       ITEM_LOC.UIN_LABEL%TYPE,
                     I_capture_time            IN       ITEM_LOC.CAPTURE_TIME%TYPE,
                     I_ext_uin_ind             IN       ITEM_LOC.EXT_UIN_IND%TYPE,
                     I_cb18                    IN       VARCHAR2,
                     I_ranged_ind              IN       ITEM_LOC.RANGED_IND%TYPE,
                     I_cb19                    IN       VARCHAR2,
                     I_costing_loc             IN       ITEM_LOC.COSTING_LOC%TYPE,
                     I_costing_loc_type        IN       ITEM_LOC.COSTING_LOC_TYPE%TYPE,
                     I_user_id                 IN       USER_USERS.USERNAME%TYPE)
              RETURN BOOLEAN;

-----------------------------------------------------------------------
-- ITEM_PARENT:
--   Called from the Mass Item Location form if updates are being done
--     to a parent item
--   Loop through ITEMs in the parente item and does all the item/location
--     level checking to determine if the item's attributes can be
--     updated with the values the user has specified. Once all the
--     appropriate checks have been performed and passed, it updates
--     the database fields which the user has specified.
-----------------------------------------------------------------------
 FUNCTION ITEM_PARENT (O_error_message           IN OUT   VARCHAR2,
                       O_reject_report           IN OUT   VARCHAR2,
                       I_item                    IN       ITEM_MASTER.ITEM%TYPE,
                       I_cb1                     IN       VARCHAR2,
                       I_status                  IN       ITEM_LOC.STATUS%TYPE,
                       I_cb2                     IN       VARCHAR2,
                       I_taxable_ind             IN       ITEM_LOC.TAXABLE_IND%TYPE,
                       I_cb3                     IN       VARCHAR2,
                       I_supplier                IN       SUPS.SUPPLIER%TYPE,
                       I_primary_country         IN       ITEM_LOC.PRIMARY_CNTRY%TYPE,
                       I_cb4                     IN       VARCHAR2,
                       I_daily_waste_pct         IN       ITEM_LOC.DAILY_WASTE_PCT%TYPE,
                       I_cb5                     IN       VARCHAR2,
                       I_meas_of_each            IN       ITEM_LOC.MEAS_OF_EACH%TYPE,
                       I_cb6                     IN       VARCHAR2,
                       I_meas_of_price           IN       ITEM_LOC.MEAS_OF_PRICE%TYPE,
                       I_cb7                     IN       VARCHAR2,
                       I_uom_of_price            IN       ITEM_LOC.UOM_OF_PRICE%TYPE,
                       I_cb8                     IN       VARCHAR2,
                       I_primary_variant         IN       ITEM_LOC.PRIMARY_VARIANT%TYPE,
                       I_cb9                     IN       VARCHAR2,
                       I_ti                      IN       ITEM_LOC.TI%TYPE,
                       I_cb10                    IN       VARCHAR2,
                       I_hi                      IN       ITEM_LOC.HI%TYPE,
                       I_cb11                    IN       VARCHAR2,
                       I_loc_item_desc           IN       ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
                       I_cb12                    IN       VARCHAR2,
                       I_loc_short_desc          IN       ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
                       I_cb13                    IN       VARCHAR2,
                       I_primary_cost_pack       IN       ITEM_LOC.PRIMARY_COST_PACK%TYPE,
                       I_cb14                    IN       VARCHAR2,
                       I_source_method           IN       ITEM_LOC.SOURCE_METHOD%TYPE,
                       I_source_wh               IN       ITEM_LOC.SOURCE_WH%TYPE,
                       I_cb15                    IN       VARCHAR2,
                       I_store_ord_mult          IN       ITEM_LOC.STORE_ORD_MULT%TYPE,
                       I_cb16                    IN       VARCHAR2,
                       I_inbound_handling_days   IN       ITEM_LOC.INBOUND_HANDLING_DAYS%TYPE,
                       I_cb17                    IN       VARCHAR2,
                       I_uin_type                IN       ITEM_LOC.UIN_TYPE%TYPE,
                       I_uin_label               IN       ITEM_LOC.UIN_LABEL%TYPE,
                       I_capture_time            IN       ITEM_LOC.CAPTURE_TIME%TYPE,
                       I_ext_uin_ind             IN       ITEM_LOC.EXT_UIN_IND%TYPE,
                       I_cb18                    IN       VARCHAR2,
                       I_ranged_ind              IN       ITEM_LOC.RANGED_IND%TYPE,
                       I_cb19                    IN       VARCHAR2,
                       I_costing_loc             IN       ITEM_LOC.COSTING_LOC%TYPE,
                       I_costing_loc_type        IN       ITEM_LOC.COSTING_LOC_TYPE%TYPE,
                       I_user_id                 IN       USER_USERS.USERNAME%TYPE)
         RETURN BOOLEAN ;

-----------------------------------------------------------------------
-- ITEM:
--   Called from the Mass Item Location form if updates are being done
--     to an individual ITEM, or called from ITEM_LIST for an item in
--     an item list, or called from ITEM_PARENT for a child item;
--   Does all the item/location level checking to determine if the item's
--     attributes can be updated with the values the user has specified.
--     Once all the appropriate checks have been performed and passed,
--     it updates the database fields which the user has specified.
-----------------------------------------------------------------------
 FUNCTION ITEM (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                O_reject_report           IN OUT   VARCHAR2,
                I_item                    IN       ITEM_MASTER.ITEM%TYPE,
                I_cb1                     IN       VARCHAR2,
                I_status                  IN       ITEM_LOC.STATUS%TYPE,
                I_cb2                     IN       VARCHAR2,
                I_taxable_ind             IN       ITEM_LOC.TAXABLE_IND%TYPE,
                I_cb3                     IN       VARCHAR2,
                I_supplier                IN       SUPS.SUPPLIER%TYPE,
                I_primary_country         IN       ITEM_LOC.PRIMARY_CNTRY%TYPE,
                I_cb4                     IN       VARCHAR2,
                I_daily_waste_pct         IN       ITEM_LOC.DAILY_WASTE_PCT%TYPE,
                I_cb5                     IN       VARCHAR2,
                I_meas_of_each            IN       ITEM_LOC.MEAS_OF_EACH%TYPE,
                I_cb6                     IN       VARCHAR2,
                I_meas_of_price           IN       ITEM_LOC.MEAS_OF_PRICE%TYPE,
                I_cb7                     IN       VARCHAR2,
                I_uom_of_price            IN       ITEM_LOC.UOM_OF_PRICE%TYPE,
                I_cb8                     IN       VARCHAR2,
                I_primary_variant         IN       ITEM_LOC.PRIMARY_VARIANT%TYPE,
                I_cb9                     IN       VARCHAR2,
                I_ti                      IN       ITEM_LOC.TI%TYPE,
                I_cb10                    IN       VARCHAR2,
                I_hi                      IN       ITEM_LOC.HI%TYPE,
                I_cb11                    IN       VARCHAR2,
                I_loc_item_desc           IN       ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
                I_cb12                    IN       VARCHAR2,
                I_loc_short_desc          IN       ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
                I_cb13                    IN       VARCHAR2,
                I_primary_cost_pack       IN       ITEM_LOC.PRIMARY_COST_PACK%TYPE,
                I_cb14                    IN       VARCHAR2,
                I_source_method           IN       ITEM_LOC.SOURCE_METHOD%TYPE,
                I_source_wh               IN       ITEM_LOC.SOURCE_WH%TYPE,
                I_cb15                    IN       VARCHAR2,
                I_store_ord_mult          IN       ITEM_LOC.STORE_ORD_MULT%TYPE,
                I_cb16                    IN       VARCHAR2,
                I_inbound_handling_days   IN       ITEM_LOC.INBOUND_HANDLING_DAYS%TYPE,
                I_cb17                    IN       VARCHAR2,
                I_uin_type                IN       ITEM_LOC.UIN_TYPE%TYPE,
                I_uin_label               IN       ITEM_LOC.UIN_LABEL%TYPE,
                I_capture_time            IN       ITEM_LOC.CAPTURE_TIME%TYPE,
                I_ext_uin_ind             IN       ITEM_LOC.EXT_UIN_IND%TYPE,
                I_cb18                    IN       VARCHAR2,
                I_ranged_ind              IN       ITEM_LOC.RANGED_IND%TYPE,
                I_cb19                    IN       VARCHAR2,
                I_costing_loc             IN       ITEM_LOC.COSTING_LOC%TYPE,
                I_costing_loc_type        IN       ITEM_LOC.COSTING_LOC_TYPE%TYPE,
                I_user_id                 IN       USER_USERS.USERNAME%TYPE)
   RETURN BOOLEAN;

-----------------------------------------------------------------------
-- PRIMARY_COST_PACK_LOC:
--   Called from the Mass Item Location form when the primary cost pack
--     attribute is being specified.
--   Function will verify that all of the locations specified by the user,
--     stored on the MC_LOCATION_TEMP table, are also setup for the specified
--     simple pack.
-----------------------------------------------------------------------
 FUNCTION PRIMARY_COST_PACK_LOC (O_error_message         IN OUT VARCHAR2,
                                 O_valid_simple_pack     IN OUT BOOLEAN,
                                 I_primary_cost_pack     IN     ITEM_LOC.PRIMARY_COST_PACK%TYPE)

   RETURN BOOLEAN;

-----------------------------------------------------------------------
-- CHECK_PRIMARY_COST_PACK_LOC:
--   Called from the Mass Change Item Location form when the item status is being
--     changed to a value other than 'Active'.
--   Function will check if the simple pack is also a primary cost pack at
--     any of the locations specified by the user, stored on the MC_LOCATION_TEMP table.
-----------------------------------------------------------------------
 FUNCTION CHECK_PRIMARY_COST_PACK_LOC (O_error_message         IN OUT VARCHAR2,
                                       O_exists                IN OUT VARCHAR2,
                                       I_primary_cost_pack     IN     ITEM_LOC.PRIMARY_COST_PACK%TYPE)

   RETURN BOOLEAN;

-----------------------------------------------------------------------
-- INSERT_MC_LOC_TEMP:
--   Called from mainschd.fmb
--   Function will locations into the MC_LOCATION_TEMP table based on group type.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.  
-----------------------------------------------------------------------
 FUNCTION INSERT_MC_LOC_TEMP (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_group_value     IN     VARCHAR2,
                              I_group_type      IN     CODE_DETAIL.CODE%TYPE)
   RETURN BOOLEAN;

-----------------------------------------------------------------------
-- INSERT_MC_LOC_TEMP:
--   Called from mclocn.fmb
--   Function will locations into the MC_LOCATION_TEMP table based on group type.

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.  
-----------------------------------------------------------------------
FUNCTION INSERT_MC_LOC_TEMP (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_group_value     IN     VARCHAR2,
                             I_group_type      IN     CODE_DETAIL.CODE%TYPE,
                             I_item            IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------
-- INSERT_MC_LOC_TMP:
--   Called from mainschd.fmb
--   Function will insert locations into the MC_LOCATION_TEMP table based on group type.

-----------------------------------------------------------------------
FUNCTION INSERT_MC_LOC_TMP (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_group_value     IN       VARCHAR2,
                            I_group_type      IN       CODE_DETAIL.CODE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------
-- DELETE_MC_LOC_TMP:
--   Called from mclocn.fmb
--   Function will delete the data from the MC_LOCATION_TEMP table based on group type.
-----------------------------------------------------------------------
FUNCTION DELETE_MC_LOC_TMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_group_value     IN       VARCHAR2,
                           I_group_type      IN       CODE_DETAIL.CODE%TYPE)
   RETURN BOOLEAN;                            
-----------------------------------------------------------------------
END ITEMLIST_LOC_MC_SQL;
/