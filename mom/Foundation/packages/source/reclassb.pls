create or replace PACKAGE BODY RECLASS_SQL AS
--------------------------------------------------------
  LP_sdate                PERIOD.VDATE%TYPE := SYSDATE;
  LP_user                 VARCHAR2(50)      := GET_USER;
--------------------------------------------------------
FUNCTION NEXT_RECLASS_NO( O_error_message IN OUT VARCHAR2,
                          O_reclass_no    IN OUT reclass_head.reclass_no%TYPE )
   RETURN BOOLEAN IS

   L_reclass_no  reclass_head.reclass_no%TYPE := 0;
   L_first_time  VARCHAR2(1)                  := 'Y';
   L_wrap_seq_no NUMBER(4)                    := 0;
   L_program     VARCHAR2(64)                 := 'RECLASS_SQL.NEXT_RECLASS_NO';
   L_exists      VARCHAR2(1)                  := 'N';

   CURSOR C_EXIST_RECLASS IS
      SELECT 'Y'
        FROM reclass_head
       WHERE reclass_no = O_reclass_no;

BEGIN
   -- Search for a unique Reclassification Number in the database.
   LOOP
      SQL_LIB.SET_MARK('SELECT', NULL, 'SYS.DUAL', NULL);
      SELECT RECLASS_NO_SEQUENCE.NEXTVAL
        INTO L_reclass_no
        FROM sys.dual;
      --
      IF (L_first_time = 'Y') THEN
         L_wrap_seq_no := L_reclass_no;
         L_first_time  := 'N';
      ELSIF (L_reclass_no = L_wrap_seq_no) THEN
         O_error_message := SQL_LIB.CREATE_MSG('NO_RECLASS_AVAIL', NULL, NULL, NULL);
         RETURN FALSE;
      END IF;
      --
      O_reclass_no := L_reclass_no;
      SQL_LIB.SET_MARK('OPEN', 'C_EXIST_RECLASS', 'RECLASS_HEAD', TO_CHAR(O_reclass_no));
      OPEN C_EXIST_RECLASS;
      SQL_LIB.SET_MARK('FETCH', 'C_EXIST_RECLASS', 'RECLASS_HEAD', TO_CHAR(O_reclass_no));
      FETCH C_EXIST_RECLASS INTO L_exists;
      ---
      IF (C_EXIST_RECLASS%NOTFOUND) THEN
         SQL_LIB.SET_MARK('CLOSE', 'C_EXIST_RECLASS', 'RECLASS_HEAD', TO_CHAR(O_reclass_no));
         CLOSE C_EXIST_RECLASS;
         RETURN TRUE;
      END IF;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_EXIST_RECLASS', 'RECLASS_HEAD', TO_CHAR(O_reclass_no));
      CLOSE C_EXIST_RECLASS;
      -- if Reclassification number is unique, return true
   END LOOP;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END NEXT_RECLASS_NO;

---------------------------------------------------------------------------------------------
FUNCTION EXIST( O_error_message IN OUT VARCHAR2,
                O_exists        IN OUT BOOLEAN,
                I_reclass_no    IN     reclass_head.reclass_no%TYPE )
   RETURN BOOLEAN IS

   L_program     VARCHAR2(64)                 := 'RECLASS_SQL.EXIST';
   L_exists      VARCHAR2(1)                  := 'N';

   CURSOR C_EXIST_RECLASS IS
      SELECT 'Y'
        FROM reclass_head
       WHERE reclass_no = I_reclass_no;

BEGIN

   -- Check to see if Reclassification Number exists
   SQL_LIB.SET_MARK('OPEN', 'C_EXIST_RECLASS', 'RECLASS_HEAD', TO_CHAR(I_reclass_no));
   OPEN C_EXIST_RECLASS;
   SQL_LIB.SET_MARK('FETCH', 'C_EXIST_RECLASS', 'RECLASS_HEAD', TO_CHAR(I_reclass_no));
   FETCH C_EXIST_RECLASS INTO L_exists;
   SQL_LIB.SET_MARK('CLOSE', 'C_EXIST_RECLASS', 'RECLASS_HEAD', TO_CHAR(I_reclass_no));
   CLOSE C_EXIST_RECLASS;
   -- if Reclassification number is unique, return true
   IF L_exists = 'N' THEN
      O_exists := FALSE;
   ELSE
      O_exists := TRUE;
   END IF;
   --
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END EXIST;

---------------------------------------------------------------------------------------------
FUNCTION GET_RECLASS_DESC( O_error_message   IN OUT VARCHAR2,
                           O_reclass_desc    IN OUT reclass_head.reclass_desc%TYPE,
                           O_reclass_desc_tl IN OUT reclass_head.reclass_desc%TYPE,
                           I_reclass_no      IN     reclass_head.reclass_no%TYPE )
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'RECLASS_SQL.GET_RECLASS_DESC';

   CURSOR C_DESC IS
      SELECT rh.reclass_desc,
             vrh.reclass_desc
        FROM reclass_head rh,
             v_reclass_head_tl vrh
       WHERE rh.reclass_no = I_reclass_no
         AND rh.reclass_no = vrh.reclass_no;

BEGIN
   -- Retrieve descriptions from reclass_head table.
   SQL_LIB.SET_MARK('OPEN', 'C_DESC',  'RECLASS_HEAD', TO_CHAR(I_reclass_no));
   OPEN C_DESC;
   SQL_LIB.SET_MARK('FETCH', 'C_DESC', 'RECLASS_HEAD', TO_CHAR(I_reclass_no));
   FETCH C_DESC INTO O_reclass_desc, O_reclass_desc_tl;
   SQL_LIB.SET_MARK('CLOSE', 'C_DESC', 'RECLASS_HEAD', TO_CHAR(I_reclass_no));
   CLOSE C_DESC;
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END GET_RECLASS_DESC;

---------------------------------------------------------------------------------------------
FUNCTION ITEM_VALIDATION( O_error_message           IN OUT   VARCHAR2,
                          O_invalid_item_level      IN OUT   VARCHAR2,
                          O_consign_conflict_flag   IN OUT   VARCHAR2,
                          O_reclass_exist           IN OUT   VARCHAR2,
                          O_order_flag              IN OUT   VARCHAR2,
                          O_uda_reqd                IN OUT   VARCHAR2,
                          O_simple_pack             IN OUT   ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                          IO_no_domain_exists       IN OUT   VARCHAR2,
                          I_system_forecast_ind     IN       VARCHAR2,
                          I_domain_exists           IN       BOOLEAN,
                          I_item                    IN       ITEM_MASTER.ITEM%TYPE,
                          I_item_level              IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                          I_tran_level              IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                          I_pack_ind                IN       ITEM_MASTER.PACK_IND%TYPE,
                          I_old_dept                IN       ITEM_MASTER.DEPT%TYPE,
                          I_new_dept                IN       ITEM_MASTER.DEPT%TYPE,
                          I_new_class               IN       ITEM_MASTER.CLASS%TYPE,
                          I_new_subclass            IN       ITEM_MASTER.SUBCLASS%TYPE)

   RETURN BOOLEAN IS

   L_program             VARCHAR2(64)              :=   'RECLASS_SQL.ITEM_VALIDATION';
   L_old_pur_type        deps.purchase_type%TYPE   :=   NULL;
   L_new_pur_type        deps.purchase_type%TYPE   :=   NULL;
   L_item_forecast_ind   VARCHAR2(1)               :=   'N';
   L_system_options      SYSTEM_OPTIONS%ROWTYPE;
   L_dummy               VARCHAR2(1);

   CURSOR C_PURCHASE_TYPE (I_dept IN deps.dept%TYPE) IS
      SELECT NVL(purchase_type, 0)
        FROM deps
       WHERE dept = I_dept;


   CURSOR C_RECLASS_EXIST IS
      SELECT 'Y'
        FROM reclass_item
       WHERE item = I_item;

   cursor C_APP_ORD_EXIST IS
      select 'Y'
        from ordhead oh,
             ordloc ol,
             item_master im
       where oh.order_no = ol.order_no
         and oh.status   = 'A'
         and ol.item = im.item
         and (im.item_parent = I_item or im.item_grandparent = I_item)
         and NVL(ol.qty_received,0) < ol.qty_ordered
         and rownum = 1
     UNION
        select 'Y'
        from ordhead oh,
             ordloc ol
       where oh.order_no = ol.order_no
         and oh.status   = 'A'
         and ol.item     = I_item
         and NVL(ol.qty_received,0) < ol.qty_ordered
         and rownum = 1;         

BEGIN
   --initialize variables
   O_invalid_item_level := 'N';
   O_consign_conflict_flag := 'N';
   O_reclass_exist := 'N';
   O_order_flag := 'N';

   IF I_item_level != 1 THEN
      O_invalid_item_level := 'Y';
      RETURN TRUE;
   ELSE
      IF ITEM_ATTRIB_SQL.GET_SIMPLE_PACK_IND(O_error_message,
                                             O_simple_pack,
                                             I_item) = FALSE THEN
         RETURN FALSE;
      END IF;

      IF O_simple_pack = 'Y' THEN
         RETURN TRUE;
      END IF;

      SQL_LIB.SET_MARK('OPEN', 'C_RECLASS_EXIST', 'RECLASS_ITEM', I_item);
      OPEN C_RECLASS_EXIST;
      SQL_LIB.SET_MARK('FETCH', 'C_RECLASS_EXIST', 'RECLASS_ITEM', I_item);
      FETCH C_RECLASS_EXIST INTO O_reclass_exist;
      SQL_LIB.SET_MARK('CLOSE', 'C_RECLASS_EXIST', 'RECLASS_ITEM', I_item);
      CLOSE C_RECLASS_EXIST;
      IF O_reclass_exist = 'Y' THEN
          RETURN TRUE;
      END IF;

      -- Search for any conflicts in consignment
      IF I_old_dept != I_new_dept THEN
         -- Get purchase_type for old dept
         SQL_LIB.SET_MARK('OPEN', 'C_PURCHASE_TYPE', 'DEPS', TO_CHAR(I_old_dept));
         OPEN  C_PURCHASE_TYPE(I_old_dept);
         SQL_LIB.SET_MARK('FETCH', 'C_PURCHASE_TYPE', 'DEPS', TO_CHAR(I_old_dept));
         FETCH C_PURCHASE_TYPE INTO L_old_pur_type;
         SQL_LIB.SET_MARK('CLOSE', 'C_PURCHASE_TYPE', 'DEPS', NULL);
         CLOSE C_PURCHASE_TYPE;
         -- Get purchase_type for new dept
         SQL_LIB.SET_MARK('OPEN', 'C_PURCHASE_TYPE', 'DEPS', TO_CHAR(I_new_dept));
         OPEN  C_PURCHASE_TYPE(I_new_dept);
         SQL_LIB.SET_MARK('FETCH', 'C_PURCHASE_TYPE', 'DEPS', TO_CHAR(I_new_dept));
         FETCH C_PURCHASE_TYPE INTO L_new_pur_type;
         SQL_LIB.SET_MARK('CLOSE', 'C_PURCHASE_TYPE', 'DEPS', NULL);
         CLOSE C_PURCHASE_TYPE;
         -- Exit function if confliction found
         IF L_old_pur_type != L_new_pur_type THEN
            O_consign_conflict_flag := 'Y';
            RETURN TRUE;
         END IF;
      END IF;

      -- Search for any existing Purchase Orders that consist of this item
      O_order_flag := 'N';
      -- Retrieve the value of the system level variable RECLASS_APPR_ORDER_IND
      IF SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options) = FALSE THEN
         RETURN FALSE;
      END IF;


      if L_system_options.RECLASS_APPR_ORDER_IND = 'N' then
         --
         SQL_LIB.SET_MARK('OPEN',
                          'C_APP_ORD_EXIST',
                          'ORDHEAD, ORDLOC',
                          'item: ' ||TO_CHAR(I_item));
         open C_APP_ORD_EXIST;
         SQL_LIB.SET_MARK('FETCH',
                          'C_APP_ORD_EXIST',
                          'ORDHEAD, ORDLOC',
                          'item: ' ||TO_CHAR(I_item));
         fetch C_APP_ORD_EXIST into L_dummy;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_APP_ORD_EXIST',
                          'ORDHEAD, ORDLOC',
                          'item: ' ||TO_CHAR(I_item));
         close C_APP_ORD_EXIST;
         --
         if L_dummy = 'Y' then
            O_order_flag := 'Y';
         else
            O_order_flag := 'N';
         end if;
      end if;

      --------------------------------------------------------------------------------
      -- If no item has yet to fail the domain assocation to the new merchandise hierarchy
      -- level check, check if the item is forecastable.  If it is and no domain association
      -- to the new merchandise hierarchy level exists, fail the item by setting the IO
      -- variable to 'Y'.  This will indicate in the calling form (MCMRHIER) that a message
      -- should be displayed, informing the user that at least one item has failed the
      -- domain association check.
      --------------------------------------------------------------------------------
      IF IO_no_domain_exists = 'N' AND I_system_forecast_ind = 'Y'
         AND I_domain_exists = FALSE AND I_pack_ind = 'N'
         AND I_item_level = I_tran_level THEN
            IF FORECASTS_SQL.GET_ITEM_FORECAST_IND(O_error_message,
                                                   I_item,
                                                   L_item_forecast_ind) = FALSE THEN
               RETURN FALSE;
            END IF;
            IF L_item_forecast_ind = 'Y' THEN
               IO_no_domain_exists := 'Y';
            END IF;
      END IF;


      IF UDA_SQL.CHECK_REQD_NO_VALUE(O_error_message,
                                     O_uda_reqd,
                                     I_item,
                                     I_new_dept,
                                     I_new_class,
                                     I_new_subclass) = FALSE THEN
         RETURN FALSE;
      ELSE
         RETURN TRUE;
      END IF;

      RETURN TRUE;
   END IF;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END ITEM_VALIDATION;
--------------------------------------------------------------------------------
FUNCTION ITEM_PROCESS( O_error_message         IN OUT   VARCHAR2,
                       IO_retry_count          IN OUT   INTEGER,
                       IO_reclass_failed       IN OUT   BOOLEAN,
                       IO_failed_reason        IN OUT   MC_REJECTION_REASONS.REASON_KEY%TYPE,
                       I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                       I_item_level            IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                       I_tran_level            IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                       I_pack_ind              IN       ITEM_MASTER.PACK_IND%TYPE,
                       I_reclass_date          IN       DATE,
                       I_system_forecast_ind   IN       VARCHAR2,
                       I_otb_system_ind        IN       SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE,
                       I_stake_lockout_days    IN       SYSTEM_OPTIONS.STAKE_LOCKOUT_DAYS%TYPE,
                       I_domain_exists         IN       BOOLEAN,
                       I_new_domain_id         IN       DOMAIN.DOMAIN_ID%TYPE,
                       I_old_dept              IN       ITEM_MASTER.DEPT%TYPE,
                       I_old_class             IN       ITEM_MASTER.CLASS%TYPE,
                       I_old_subclass          IN       ITEM_MASTER.SUBCLASS%TYPE,
                       I_new_dept              IN       ITEM_MASTER.DEPT%TYPE,
                       I_new_class             IN       ITEM_MASTER.CLASS%TYPE,
                       I_new_subclass          IN       ITEM_MASTER.SUBCLASS%TYPE,
                       I_mode                  IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(64)              := 'RECLASS_SQL.ITEM_PROCESS';
   L_sellable_ind         ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind        ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_item_forecast_ind    VARCHAR2(1)               := NULL;
   L_reqd_no_value        VARCHAR2(1)               := 'N';
   L_stock_count          VARCHAR2(1)               := 'N';
   L_tran_code            TRAN_DATA.TRAN_CODE%TYPE  := 34;
   L_total_cost           TRAN_DATA.TOTAL_COST%TYPE := NULL;
   L_units                TRAN_DATA.UNITS%TYPE      := 0;
   L_old_domain_id        DOMAIN.DOMAIN_ID%TYPE     := NULL;
   L_results              VARCHAR2(1)               := 'N';
   L_action_type          VARCHAR2(1)               := 'M';
   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_logistics_lock_sku   ITEM_MASTER.ITEM%TYPE;
   L_uda_reqd             VARCHAR2(1);
   L_locked               BOOLEAN := TRUE;
   L_table                VARCHAR2(30);
   L_order_flag           VARCHAR2(1)               := 'N';
   L_item_status          ITEM_MASTER.STATUS%TYPE;
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);
   L_cycle_count          STAKE_SKU_LOC.CYCLE_COUNT%TYPE;
   L_stkcount_type        VARCHAR2(1);
   L_stocktake_date       DATE;
   L_rowid                ROWID;
   ---
   L_equal                VARCHAR2(1);
   L_order_appr_flag      VARCHAR2(1)               := 'N';
   L_system_options       SYSTEM_OPTIONS%ROWTYPE;
   L_rpm_ind              VARCHAR2(1):=NULL;
   L_eow_date             SYSTEM_VARIABLES.NEXT_EOW_DATE_UNIT%TYPE ;
      
   cursor C_RPM_IND is 
      select RPM_IND
	    from system_options; 
   
   
   
   cursor C_APP_ORD_EXIST IS
      select 'Y'
        from ordhead oh,
             ordloc ol
       where oh.order_no = ol.order_no
         and oh.status   = 'A'
         and ol.item     = I_item
         and NVL(ol.qty_received,0) < ol.qty_ordered
         and rownum = 1;

   cursor C_CHECK_STKCOUNT is
   select 'Y'
     from stake_prod_loc sp,
          stake_head sh
    where sp.processed      = 'N'
      and sp.cycle_count    = sh.cycle_count
      and sh.stocktake_type = 'B'
      and ((sp.dept     = I_old_dept and
            sp.class    = I_old_class and
            sp.subclass = I_old_subclass) or
           (sp.dept     = I_new_dept and
            sp.class    = I_new_class and
            sp.subclass = I_new_subclass))
      and exists (select 'x'
                    from item_master
                   where item = I_item
                     and inventory_ind = 'Y'
                     and rownum = 1);
     
   cursor C_STKCOUNT is
   select distinct ssl.cycle_count,
          'I',
          sh.stocktake_date
     from stake_head sh,
          stake_sku_loc ssl
    where ssl.item           = I_item
      and ssl.dept           = I_old_dept
      and ssl.class          = I_old_class
      and ssl.subclass       = I_old_subclass
      and ssl.processed      = 'N'
      and sh.stocktake_type  = 'U'
      and sh.product_level_ind is NULL
      and sh.cycle_count     = ssl.cycle_count
      and sh.stocktake_date >= I_reclass_date
   union all
   select distinct sp.cycle_count,
          'B',
          sh.stocktake_date
     from stake_head sh,
          stake_product sp
    where ((sh.product_level_ind = 'D'
            and sp.dept          = I_old_dept
            and exists (select 'x'
                          from stake_product sp2
                         where sp2.dept        = I_new_dept
                           and sp2.cycle_count = sp.cycle_count))
       or (sh.product_level_ind = 'C'
           and sp.dept          = I_old_dept
           and sp.class         = I_old_class
           and exists (select 'x'
                         from stake_product sp2
                        where sp2.dept        = I_new_dept
                          and sp2.class       = I_new_class
                          and sp2.cycle_count = sp.cycle_count))
       or (sh.product_level_ind = 'S'
           and sp.dept          = I_old_dept
           and sp.class         = I_old_class
           and sp.subclass      = I_old_subclass
           and exists (select 'x'
                         from stake_product sp2
                        where sp2.dept        = I_new_dept
                          and sp2.class       = I_new_class
                          and sp2.subclass    = I_new_subclass
                          and sp2.cycle_count = sp.cycle_count)))
      and sh.cycle_count    = sp.cycle_count
      and sh.stocktake_type = 'U'
      and (I_reclass_date between (sh.stocktake_date - I_stake_lockout_days)
          and sh.stocktake_date)
   union all
   select distinct sp.cycle_count,
          'O',
          sh.stocktake_date
     from stake_head sh,
          stake_product sp
    where ((sh.product_level_ind = 'D'
            and sp.dept          = I_old_dept
            and not exists (select 'x'
                              from stake_product sp2
                             where sp2.dept        = I_new_dept
                               and sp2.cycle_count = sp.cycle_count))
       or (sh.product_level_ind = 'C'
           and sp.dept          = I_old_dept
           and sp.class         = I_old_class
           and not exists (select 'x'
                             from stake_product sp2
                            where sp2.dept        = I_new_dept
                              and sp2.class       = I_new_class
                              and sp2.cycle_count = sp.cycle_count))
       or (sh.product_level_ind = 'S'
           and sp.dept          = I_old_dept
           and sp.class         = I_old_class
           and sp.subclass      = I_old_subclass
           and not exists (select 'x'
                             from stake_product sp2
                            where sp2.dept        = I_new_dept
                              and sp2.class       = I_new_class
                              and sp2.subclass    = I_new_subclass
                              and sp2.cycle_count = sp.cycle_count)))
      and sh.cycle_count    = sp.cycle_count
      and sh.stocktake_type = 'U'
      and (I_reclass_date between (sh.stocktake_date - I_stake_lockout_days)
           and sh.stocktake_date)
   union all
   select distinct sp.cycle_count,
          'N',
          sh.stocktake_date
     from stake_head sh,
          stake_product sp
    where ((sh.product_level_ind = 'D'
            and sp.dept          = I_new_dept
            and not exists (select 'x'
                              from stake_product sp2
                             where sp2.dept        = I_old_dept
                               and sp2.cycle_count = sp.cycle_count))
       or (sh.product_level_ind = 'C'
           and sp.dept          = I_new_dept
           and sp.class         = I_new_class
           and not exists (select 'x'
                             from stake_product sp2
                            where sp2.dept        = I_old_dept
                              and sp2.class       = I_old_class
                              and sp2.cycle_count = sp.cycle_count))
       or (sh.product_level_ind = 'S'
           and sp.dept     = I_new_dept
           and sp.class    = I_new_class
           and sp.subclass = I_new_subclass
           and not exists (select 'x'
                             from stake_product sp2
                            where sp2.dept        = I_old_dept
                              and sp2.class       = I_old_class
                              and sp2.subclass    = I_old_subclass
                              and sp2.cycle_count = sp.cycle_count)))
      and sh.cycle_count    = sp.cycle_count
      and sh.stocktake_type = 'U'
      and (I_reclass_date between (sh.stocktake_date - I_stake_lockout_days)
           and sh.stocktake_date)
   order by 3;

   cursor C_LOCK_ITEM_MASTER is
      select rowid,
             status
        from item_master
       where item = I_item
         for update nowait;

   cursor C_LOCK_ITEM_LOC_SOH is
      select 'x'
        from item_loc_soh
       where item = I_item
         for update nowait;
         
   cursor C_LOCK_ITEM_LOC_HIST is
      select  'x'
        from item_loc_hist ilh
       where ilh.item      = I_item     
         and ilh.eow_date  = L_eow_date
         for update nowait;         

BEGIN

   SELECT NEXT_EOW_DATE_UNIT  INTO L_eow_date
       from SYSTEM_VARIABLES;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_item_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_level',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_tran_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tran_level',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_pack_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_ind',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_reclass_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_reclass_date',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_system_forecast_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_system_forecast_ind',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_otb_system_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_otb_system_ind',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_stake_lockout_days is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_stake_lockout_days',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_domain_exists is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_domain_exists',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_old_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_dept',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_old_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_class',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_old_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_subclass',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_new_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_dept',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_new_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_class',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_new_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_subclass',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_mode is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_mode',
                                            'RECLASS_SQL.ITEM_PROCESS',
                                            'NULL');
      return FALSE;
   end if;

   ----------------------------------------------------------------------------
   -- Validation
   ----------------------------------------------------------------------------
   if I_old_dept     = I_new_dept AND
      I_old_class    = I_new_class AND
      I_old_subclass = I_new_subclass then
      return TRUE;
   end if;
   
   -----------------------------------------------------------------------------
   -- Search for any approved Purchase Orders that consist of this item
   -- Retrieve the value of the system level variable RECLASS_APPR_ORDER_IND
   -----------------------------------------------------------------------------
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE THEN
      return FALSE;
   end if;


   if L_system_options.reclass_appr_order_ind = 'N' then
      --
      SQL_LIB.SET_MARK('OPEN',
                       'C_APP_ORD_EXIST',
                       'ORDHEAD, ORDLOC',
                       'item: ' ||TO_CHAR(I_item));
      open C_APP_ORD_EXIST;
      SQL_LIB.SET_MARK('FETCH',
                       'C_APP_ORD_EXIST',
                       'ORDHEAD, ORDLOC',
                       'item: ' ||TO_CHAR(I_item));
      fetch C_APP_ORD_EXIST into L_order_appr_flag;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_APP_ORD_EXIST',
                       'ORDHEAD, ORDLOC',
                       'item: ' ||TO_CHAR(I_item));
      close C_APP_ORD_EXIST;
      --
      if L_order_appr_flag = 'Y' then
         IO_reclass_failed := TRUE;
         IO_failed_reason  := 'ITEM_ON_APP_ORD';
         return TRUE;
      end if;
   end if;

   -----------------------------------------------------------------------------
   -- Check if the item is on any not-yet-processed "unit and value" stock count.
   -- If it is, it can not be reclassified. A rejection record must be written.
   -----------------------------------------------------------------------------
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_STKCOUNT',
                    'STAKE_SKU_LOC STAKE_HEAD',
                    I_item);
   open C_CHECK_STKCOUNT;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_STKCOUNT',
                    'STAKE_SKU_LOC STAKE_HEAD',
                    I_item);
   fetch C_CHECK_STKCOUNT INTO L_stock_count;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_STKCOUNT',
                    'STAKE_SKU_LOC STAKE_HEAD',
                    I_item);
   close C_CHECK_STKCOUNT;

   if L_stock_count = 'Y' then
      IO_reclass_failed := TRUE;
      IO_failed_reason  := 'STOCK_COUNT_PEND';
      return TRUE;
   end if;

   if ORDER_VALIDATE_SQL.ITEM_ON_RECV_ORD(O_error_message,
                                          L_order_flag,
                                          I_item) = FALSE then
      return FALSE;
   end if;

   if L_order_flag = 'Y' then
      IO_reclass_failed := TRUE;
      IO_failed_reason  := 'PARTIAL_RECV_ORD_EXIST';
      return TRUE;
   end if;

   -----------------------------------------------------------------------------
   -- Check if item is forecastable and for the existence of a domain
   -- If the item is forecastable and no domain association to the new
   -- merchandise heirarchy level exists, reject the item.
   -- This can only be done for Staple and Fashion
   -----------------------------------------------------------------------------
   if I_system_forecast_ind = 'Y'
      AND I_domain_exists   = FALSE
      AND I_item_level     <= I_tran_level
      AND I_pack_ind        = 'N' then
      if FORECASTS_SQL.GET_ITEM_FORECAST_IND(O_error_message,
                                             I_item,
                                             L_item_forecast_ind) = FALSE then
         return FALSE;
      end if;
      if L_item_forecast_ind = 'Y' then
         IO_reclass_failed := TRUE;
         IO_failed_reason  := 'RECLASS_NO_DOMAIN';
         return TRUE;
      end if;
   end if;

   -----------------------------------------------------------------------------------------
   -- This check is made to determine if the item is missing any User Defined Attributes
   -- that are required by the new department/class/subclass.  The functions only needs
   -- to be called for items at or above the tran_level because UDAs do not default
   -- below the tran_level.
   -----------------------------------------------------------------------------------------
   if I_item_level <= I_tran_level then
      if UDA_SQL.CHECK_REQD_NO_VALUE(O_error_message,
                                     L_uda_reqd,
                                     I_item,
                                     I_new_dept,
                                     I_new_class,
                                     I_new_subclass) = FALSE then
         return FALSE;
      end if;

      if L_uda_reqd = 'Y' then
         IO_reclass_failed := TRUE;
         IO_failed_reason  := 'NO_ASSOC_UDA_VALUE';
         return TRUE;
      end if;
   end if;

   -- If I_mode is 'P', exit the function and forego the processes below
   if I_mode = 'P' then
      return TRUE;
   end if;
   
   L_table := 'ITEM_MASTER';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_MASTER',
                    'ITEM_MASTER',
                    I_item);
   open  C_LOCK_ITEM_MASTER;
   SQL_LIB.SET_MARK('FETCH',
                    'C_LOCK_ITEM_MASTER',
                    'ITEM_MASTER',
                    I_item);
   fetch C_LOCK_ITEM_MASTER into L_rowid,
                                 L_item_status;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_MASTER',
                    'ITEM_MASTER',
                    I_item);
   close C_LOCK_ITEM_MASTER;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'ITEM_MASTER',
                    I_item);
   update item_master
      set dept                 = I_new_dept,
          class                = I_new_class,
          subclass             = I_new_subclass,
          last_update_id       = GET_USER,
          last_update_datetime = SYSDATE
    where rowid = L_rowid;
	
	
	   SQL_LIB.SET_MARK('UPDATE',
                    NULL,

                    'WF_COST_RELATIONSHIP',
                    I_item);     
                    
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_LOC_HIST',
                    'ITEM_LOC_HIST',
                    I_item);
					
   open  C_LOCK_ITEM_LOC_HIST;
           
	  
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_LOC_HIST',
                    'ITEM_LOC_HIST',
                    I_item);
	
   close  C_LOCK_ITEM_LOC_HIST;
   
   update item_loc_hist
      set dept                 = I_new_dept,
          class                = I_new_class,
          subclass             = I_new_subclass,
          last_update_id       = USER,
          last_update_datetime = SYSDATE
    where item = I_item
	  and eow_date  = L_eow_date;

   update wf_cost_relationship
      set dept                 = I_new_dept,
          class                = I_new_class,
          subclass             = I_new_subclass
    where item     = I_item
      and dept     = I_old_dept
      and class    = I_old_class
      and subclass = I_old_subclass; 

   insert into reclass_item_temp (item
                                 ,new_dept
                                 ,new_class
                                 ,new_subclass
                                 ,old_dept
                                 ,old_class
                                 ,old_subclass
                                 ,reclass_date
                                 )
                            values
                                 (I_item
                                 ,I_new_dept
                                 ,I_new_class
                                 ,I_new_subclass
                                 ,I_old_dept
                                 ,I_old_class
                                 ,I_old_subclass
                                 ,I_reclass_date
                                 );

   if PM_NOTIFY_API_SQL.ITEM_RECLASS(O_error_message,
                                     I_item,
                                     I_reclass_date,
                                     I_new_dept,
                                     I_new_class,
                                     I_new_subclass) = FALSE then
      return FALSE;
   end if;
   
   --
   open C_RPM_IND;
   fetch C_RPM_IND into L_rpm_ind;
   close C_RPM_IND;
   --
   if L_rpm_ind = 'Y' then 
      
      -- Delete RPM_ITEM_ZONE_PRICE when neccessary
      if PM_RETAIL_API_SQL.COMPARE_RETAIL_DEF(O_error_message,
                                           L_equal,
                                           I_new_dept,
                                           I_new_class,
                                           I_new_subclass,
                                           I_old_dept,
                                           I_old_class,
                                           I_old_subclass) = FALSE then
          return FALSE;
      end if;

      if L_equal != 'Y' then   
         if PM_RETAIL_API_SQL.DELETE_RPM_ITEM_ZONE_PRICE(O_error_message,
                                                         I_item) = FALSE then
            return FALSE;
         end if;	  
      end if;
   end if;	  
------------------------------------------------------------------

-- If item exists on "unit only" stock count --
--   if stock count is scheduled using itemlist (L_stkcount_type = 'I'),
--       update dept/class/subclass on stake_sku_loc with new d/c/s
--   if stock count is scheduled using dept, class or subclass,
--       if both old d/c/s and new d/c/s exist on stock count
--                  (L_stkcount_type = 'B'),
--         update dept/class/subclass on stake_sku_loc with new d/c/s,
--       if only old d/c/s exists on stock count,
--                  (L_stkcount_type = 'O'),
--         delete stake_sku_loc for this item,
--       if only new d/c/s exists on stock count,
--                  (L_stkcount_type = 'N'),
--         insert into stake_sku_loc for this item

   SQL_LIB.SET_MARK('OPEN',
                    'C_STKCOUNT',
                    'STAKE_SKU_LOC',
                    'Item: ' || I_item);
   open C_STKCOUNT;

   LOOP

      SQL_LIB.SET_MARK('FETCH',
                       'C_STKCOUNT',
                       'STAKE_SKU_LOC',
                       'Item: ' || I_item);
      fetch C_STKCOUNT into L_cycle_count,
                            L_stkcount_type,
                            L_stocktake_date;

      if C_STKCOUNT%NOTFOUND then
        EXIT;
      end if;

      if L_stkcount_type = 'I' or
         L_stkcount_type = 'B' then
         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'STAKE_SKU_LOC',
                          'Cycle count: ' ||L_cycle_count||', Item: '||I_item||', Dept: '||I_old_dept||', Class: '||I_old_class||', Subclass: '||I_old_subclass);
         update stake_sku_loc
            set dept     = I_new_dept,
                class    = I_new_class,
                subclass = I_new_subclass
          where cycle_count = L_cycle_count
            and item        = I_item
            and dept        = I_old_dept
            and class       = I_old_class
            and subclass    = I_old_subclass;

      elsif L_stkcount_type = 'O' then
         SQL_LIB.SET_MARK('DELETE',
                          NULL,
                          'STAKE_SKU_LOC',
                          'Cycle count: ' ||L_cycle_count||', Item: '||I_item||', Dept: '||I_old_dept||', Class: '||I_old_class||', Subclass: '||I_old_subclass);
         delete from stake_sku_loc
          where cycle_count = L_cycle_count
            and item        = I_item
            and dept        = I_old_dept
            and class       = I_old_class
            and subclass    = I_old_subclass;

      elsif L_stkcount_type = 'N' then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'STAKE_SKU_LOC',
                          'Cycle count: ' ||L_cycle_count||', Item: '||I_item||', Dept: '||I_old_dept||', Class: '||I_old_class||', Subclass: '||I_old_subclass);
         insert into stake_sku_loc(cycle_count,
                                   loc_type,
                                   location,
                                   item,
                                   snapshot_on_hand_qty,
                                   snapshot_in_transit_qty,
                                   snapshot_unit_cost,
                                   snapshot_unit_retail,
                                   processed,
                                   physical_count_qty,
                                   pack_comp_qty,
                                   dept,
                                   class,
                                   subclass)
                            select cycle_count,
                                   sl.loc_type,
                                   sl.location,
                                   I_item,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NVL(il.unit_retail,0),
                                   'N',
                                   0,
                                   0,
                                   I_new_dept,
                                   I_new_class,
                                   I_new_subclass
                              from stake_location sl,
                                   item_loc il
                             where sl.location    = il.loc
                               and il.item        = I_item
                               and sl.cycle_count = L_cycle_count;
       end if;
   END LOOP;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_STKCOUNT',
                    'STAKE_SKU_LOC',
                    'Item: ' || I_item);
   close C_STKCOUNT;

   if I_item_level = I_tran_level and L_item_status = 'A' then

      if I_pack_ind = 'N' then

         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'REPL_ITEM_LOC_UPDATES',
                          I_item);
         insert into repl_item_loc_updates(item,
                                           change_type)
                                          (select DISTINCT I_item,
                                                  'RECLAS'
                                             from repl_item_loc
                                            where item = I_item);
      end if;
   end if;   /* if item is the transaction level item */

   ------------------------------------------------------------------------
   -- If the system forecast indicator is 'Y', get the domain association for
   -- the old merchandise hierarchy
   ------------------------------------------------------------------------
   if I_system_forecast_ind = 'Y' then
      if FORECASTS_SQL.GET_DOMAIN(O_error_message,
                                  I_old_dept,
                                  I_old_class,
                                  I_old_subclass,
                                  L_old_domain_id) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_pack_ind = 'N' and I_item_level = I_tran_level then

      if STKLEDGR_SQL.TRAN_DATA_INSERT (O_error_message,
                                     I_item,
                                     I_new_dept,
                                     I_new_class,
                                     I_new_subclass,
                                     NULL,           /* location */
                                     NULL,           /* loc_type */
                                     I_reclass_date,
                                     L_tran_code,    /* tran_code */
                                     NULL,           /* adj_code */
                                     L_units,
                                     L_total_cost,   /* total_cost */
                                     NULL,           /* total_retail */
                                     NULL,           /* ref_no_1 */
                                     NULL,           /* ref_no_2 */
                                     NULL,           /* source_store */
                                     NULL,           /* source wh */
                                     NULL,           /* old retail */
                                     NULL,           /* new retail */
                                     I_old_dept,
                                     I_old_class,
                                     I_old_subclass,
                                     L_program) = FALSE then
         return FALSE;
      end if;
   end if;

   ---------------------------------------------------------------------
   -- If the RMS is interfacing with an external forecasting system (RDF),
   -- update the last_sales/issues_export_date for all item stores/whs to
   -- NULL if the reclassification puts the item in a new domain.
   -- This will flag the item stores/whs so all their sales/issues history
   -- is downloaded in the next sales/issues download to the forecasting system.
   ---------------------------------------------------------------------
   if L_old_domain_id != I_new_domain_id AND I_system_forecast_ind = 'Y' then
      L_table := 'ITEM_LOC_SOH';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ITEM_LOC_SOH',
                       'ITEM_LOC_SOH',
                       'ITEM: '||I_item);
      open C_LOCK_ITEM_LOC_SOH;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ITEM_LOC_SOH',
                       'ITEM_LOC_SOH',
                       'ITEM: '||I_item);
      close C_LOCK_ITEM_LOC_SOH;

      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'ITEM_LOC_SOH',
                       'ITEM: '||I_item);
      update item_loc_soh
         set last_hist_export_date = NULL,
             last_update_datetime  = LP_sdate,
             last_update_id        = LP_user
       where item = I_item;
   end if;

   --- Check to be sure reclassifications of merchandise criteria remain
   --- accurate across the system. Delete old dept/class/subclass info for
   --- reclass and add new to the table pos_temp.

   if POS_CONFIG_SQL.CHECK_ITEM(O_error_message,
                                I_item,
                                I_old_dept,
                                I_old_class,
                                I_old_subclass,
                                NULL,
                                'D') = FALSE then
      return FALSE;
   end if;

   if POS_CONFIG_SQL.CHECK_ITEM(O_error_message,
                                I_item,
                                I_new_dept,
                                I_new_class,
                                I_new_subclass,
                                NULL,
                                'A') = FALSE then
      return FALSE;
   end if;

   if POS_CONFIG_SQL.UPDATE_MERCH_CRITERIA(O_error_message,
                                           I_item,
                                           I_new_dept,
                                           I_new_class,
                                           I_new_subclass) = FALSE then
      return FALSE;
   end if;   
   IO_reclass_failed := FALSE;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_PROCESS;
---------------------------------------------------------------------------------------------
FUNCTION ITEMLIST_PROCESS( O_error_message  IN OUT VARCHAR2,
                           O_reject         IN OUT BOOLEAN,
                           O_order_flag     IN OUT VARCHAR2,
                           O_no_domain_flag IN OUT VARCHAR2,
                           I_item           IN     skulist_detail.item%TYPE,
                           I_skulist        IN     skulist_detail.skulist%TYPE,
                           I_item_level     IN     skulist_detail.item_level%TYPE,
                           I_tran_level     IN     skulist_detail.tran_level%TYPE,
                           I_reclass_no     IN     reclass_head.reclass_no%TYPE,
                           I_reclass_date   IN     DATE,
                           I_reclass_desc   IN     reclass_head.reclass_desc%TYPE,
                           I_new_dept       IN     item_master.dept%TYPE,
                           I_new_class      IN     item_master.CLASS%TYPE,
                           I_new_subclass   IN     item_master.subclass%TYPE,
                           I_username       IN     user_users.username%TYPE )
   RETURN BOOLEAN IS

   L_item_desc             item_master.item_desc%TYPE             := NULL;
   L_status                item_master.status%TYPE                := NULL;
   L_dept                  item_master.dept%TYPE                  := NULL;
   L_dept_name             deps.dept_name%TYPE                    := NULL;
   L_class                 item_master.CLASS%TYPE                 := NULL;
   L_class_name            CLASS.class_name%TYPE                  := NULL;
   L_subclass              item_master.subclass%TYPE              := NULL;
   L_subclass_name         subclass.sub_name%TYPE                 := NULL;
   L_sellable_ind          item_master.sellable_ind%TYPE          := NULL;
   L_orderable_ind         item_master.orderable_ind%TYPE         := NULL;
   L_pack_type             item_master.pack_type%TYPE             := NULL;
   L_simple_pack_ind       item_master.simple_pack_ind%TYPE       := NULL;
   L_waste_type            item_master.waste_type%TYPE            := NULL;
   L_item_parent           item_master.item_parent%TYPE           := NULL;
   L_item_grandparent      item_master.item_grandparent%TYPE      := NULL;
   L_short_desc            item_master.short_desc%TYPE            := NULL;
   L_waste_pct             item_master.waste_pct%TYPE             := NULL;
   L_default_waste_pct     item_master.default_waste_pct%TYPE     := NULL;
   L_item_level            item_master.item_level%TYPE            := I_item_level;
   L_tran_level            item_master.tran_level%TYPE            := I_tran_level;
   L_pack_ind              item_master.pack_ind%TYPE              := NULL;
   L_program               VARCHAR2(64)                           := 'RECLASS_SQL.ITEMLIST_PROCESS';
   L_domain_exists         BOOLEAN                                := TRUE;
   L_system_forecast_ind   VARCHAR2(1)                            := NULL;
   L_counter               NUMBER(5)                                := 0;

   CURSOR C_SKULIST IS
      SELECT item,
             item_level,
             tran_level,
             pack_ind
        FROM skulist_detail
       WHERE skulist = I_skulist;

---------------------------------------------------------------------------------------
--- The following internal function will check if the item is valid for insert into
--- reclass_item.
---------------------------------------------------------------------------------------
FUNCTION INSERT_RECLASS_ITEM(O_error_message         IN OUT   VARCHAR2,
                             O_reject                IN OUT   BOOLEAN,
                             O_order_flag            IN OUT   VARCHAR2,
                             O_no_domain_flag        IN OUT   VARCHAR2,
                             O_counter               IN OUT   NUMBER,
                             I_system_forecast_ind   IN       VARCHAR2,
                             I_domain_exists         IN       BOOLEAN,
                             I_reclass_no            IN       RECLASS_HEAD.RECLASS_NO%TYPE,
                             I_new_dept              IN       ITEM_MASTER.DEPT%TYPE,
                             I_new_class             IN       ITEM_MASTER.CLASS%TYPE,
                             I_new_subclass          IN       ITEM_MASTER.SUBCLASS%TYPE,
                             I_item_level            IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                             I_tran_level            IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                             I_pack_ind              IN       ITEM_MASTER.PACK_IND%TYPE,
                             I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                             I_username              IN       USER_USERS.USERNAME%TYPE)
   RETURN BOOLEAN IS

   L_old_dept              item_master.dept%TYPE        := NULL;
   L_old_class             item_master.CLASS%TYPE       := NULL;
   L_old_subclass          item_master.subclass%TYPE    := NULL;
   L_invalid_item_level    VARCHAR2(1)                  := NULL;
   L_consign_conflict_flag VARCHAR2(1)                  := NULL;
   L_reclass_exist         VARCHAR2(1)                  := NULL;
   L_order_flag            VARCHAR2(1)                  := 'N';
   L_uda_reqd              VARCHAR2(1)                  := NULL;
   L_no_domain_exists      VARCHAR2(1)                  := 'N';
   L_simple_pack_ind       ITEM_MASTER.SIMPLE_PACK_IND%TYPE;

BEGIN
   -- Retrieve old_dept, old_class, old_subclass for the item
   IF ITEM_ATTRIB_SQL.GET_MERCH_HIER( O_error_message,
                                      I_item,
                                      L_old_dept,
                                      L_old_class,
                                      L_old_subclass ) = FALSE THEN
         RETURN FALSE;
   END IF;

   IF ITEM_VALIDATION( O_error_message,
                       L_invalid_item_level,
                       L_consign_conflict_flag,
                       L_reclass_exist,
                       L_order_flag,
                       L_uda_reqd,
                       L_simple_pack_ind,
                       L_no_domain_exists,
                       I_system_forecast_ind,
                       I_domain_exists,
                       I_item,
                       I_item_level,
                       I_tran_level,
                       I_pack_ind,
                       L_old_dept,
                       I_new_dept,
                       I_new_class,
                       I_new_subclass ) = FALSE THEN
         RETURN FALSE;
   ELSE
      IF L_invalid_item_level = 'Y' THEN
         -- Insert item into mc_rejections table.
         IF ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS( O_error_message,
                                                    I_item,
                                                    NULL,
                                                    NULL,
                                                    'M',
                                                    'REJECT_ITEM_LEVEL',
                                                    I_username,
                                                    I_item,
                                                    TO_CHAR(I_item_level),
                                                    NULL ) = FALSE THEN
            RETURN FALSE;
         END IF;
         O_reject := TRUE;
      ELSIF L_uda_reqd = 'Y' THEN
       -- Insert UDA into mc_rejections table.
         IF ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS( O_error_message,
                                                    I_item,
                                                    NULL,
                                                    NULL,
                                                    'M',
                                                    'UDA_REQD',
                                                    I_username,
                                                    NULL,
                                                    NULL,
                                                    NULL ) = FALSE THEN
            RETURN FALSE;
         END IF;
         O_reject := TRUE;
      ELSIF L_reclass_exist = 'Y' THEN
         IF ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS( O_error_message,
                                                    I_item,
                                                    NULL,
                                                    NULL,
                                                    'M',
                                                    'RECLASS_EXIST',
                                                    I_username,
                                                    NULL,
                                                    NULL,
                                                    NULL ) = FALSE THEN
            RETURN FALSE;
         END IF;
         O_reject := TRUE;
      ELSIF L_consign_conflict_flag = 'Y' THEN
         IF ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS( O_error_message,
                                                    I_item,
                                                    NULL,
                                                    NULL,
                                                    'M',
                                                    'CONSIGNMENT',
                                                    I_username,
                                                    TO_CHAR(L_old_dept),
                                                    TO_CHAR(I_new_dept),
                                                    NULL ) = FALSE THEN
            RETURN FALSE;
         END IF;
         O_reject := TRUE;
      ELSIF L_simple_pack_ind = 'Y' THEN
         IF ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS( O_error_message,
                                                    I_item,
                                                    NULL,
                                                    NULL,
                                                    'M',
                                                    'SIMPLE_PACK',
                                                    I_username,
                                                    I_item,
                                                    NULL,
                                                    NULL) = FALSE THEN
            RETURN FALSE;
         END IF;
         O_reject := TRUE;
      elsif L_order_flag = 'Y' THEN
         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS( O_error_message,
                                                    I_item,
                                                    NULL,
                                                    NULL,
                                                    'M',
                                                    'ITEM_ON_APP_ORD',
                                                    I_username,
                                                    I_item,
                                                    NULL,
                                                    NULL) = FALSE THEN
            return FALSE;
         END IF;
         O_reject := TRUE;
         O_order_flag := 'Y';
      else
         -------------------------------------------------------------------------
         -- If domain existance check fails, set flag so calling form (MCMRHIER)
         -- displays message informing user that at least one item is forecastable
         -- and can't be reclassifed because the new merchandise hierarchy isn't
         -- associated to a domain.
         -------------------------------------------------------------------------
         IF L_no_domain_exists = 'Y' THEN
            O_no_domain_flag := 'Y';
         END IF;
         --------------------------------------------------------------
         -- Create reclassification item record in reclass_item table
         --------------------------------------------------------------
         SQL_LIB.SET_MARK('INSERT', NULL, 'RECLASS_ITEM', I_item);
         INSERT INTO reclass_item( reclass_no,
                                   item )
                           VALUES( I_reclass_no,
                                   I_item );
         O_counter := O_counter + 1;
      END IF;
   END IF;

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RECLASS_SQL.INSERT_RECLASS_ITEM',
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END INSERT_RECLASS_ITEM;
---------------------------------------------------------------------------------------
BEGIN
   -- Initialize variables
   O_order_flag := 'N';
   O_reject     := FALSE;
   O_no_domain_flag := 'N';
   --------------------------------------------------------------
   -- Create reclassification header record in reclass_head table
   --------------------------------------------------------------
   SQL_LIB.SET_MARK('INSERT', NULL, 'RECLASS_HEAD', TO_CHAR(I_reclass_no));
   INSERT INTO reclass_head( reclass_no,
                             reclass_desc,
                             reclass_date,
                             to_dept,
                             to_class,
                             to_subclass )
                     VALUES( I_reclass_no,
                             I_reclass_desc,
                             I_reclass_date,
                             I_new_dept,
                             I_new_class,
                             I_new_subclass );

   -------------------------------------------------------------------------------
   -- If the forecast system indicator is 'Y', check if a domain association to the
   -- new merchandise hierarchy exists.  Set flag to store result of existence check.
   -- This flag will be used during ITEM_VALIDATION to determine if user should be
   -- warned that new merchandise hierarchy isn't associated to a domain.
   -------------------------------------------------------------------------------
   IF FORECASTS_SQL.GET_SYSTEM_FORECAST_IND(O_error_message,
                                            L_system_forecast_ind) = FALSE THEN
      RETURN FALSE;
   END IF;
   IF L_system_forecast_ind = 'Y' THEN
      IF FORECASTS_SQL.DOMAIN_EXISTS(O_error_message,
                                     I_new_dept,
                                     I_new_class,
                                     I_new_subclass,
                                     L_domain_exists) = FALSE THEN
         RETURN FALSE;
      END IF;
   END IF;

   --------------------------------------------------------------
   -- Reclassify both single items and itemlist records.  If
   -- itemlist then retrieve and validate all records in
   -- C_SKULIST cursor
   --------------------------------------------------------------
   IF I_item IS NULL THEN

      SQL_LIB.SET_MARK('FETCH', 'C_SKULIST', 'SKULIST_DETAIL', TO_CHAR(I_skulist));
      FOR L_item_rec IN C_SKULIST LOOP
         IF INSERT_RECLASS_ITEM(O_error_message,
                                O_reject,
                                O_order_flag,
                                O_no_domain_flag,
                                L_counter,
                                L_system_forecast_ind,
                                L_domain_exists,
                                I_reclass_no,
                                I_new_dept,
                                I_new_class,
                                I_new_subclass,
                                L_item_rec.item_level,
                                L_item_rec.tran_level,
                                L_item_rec.pack_ind,
                                L_item_rec.item,
                                I_username) = FALSE THEN
            RETURN FALSE;
         END IF;
      END LOOP;
   ELSE
      IF L_item_level IS NULL OR L_tran_level IS NULL
      OR L_pack_ind IS NULL THEN
         IF ITEM_ATTRIB_SQL.GET_INFO(O_error_message,
                                       L_item_desc,
                                       L_item_level,
                                       L_tran_level,
                                       L_status,
                                       L_pack_ind,
                                       L_dept,
                                       L_dept_name,
                                       L_class,
                                       L_class_name,
                                       L_subclass,
                                       L_subclass_name,                                     
                                       L_sellable_ind,
                                       L_orderable_ind,
                                       L_pack_type,
                                       L_simple_pack_ind,
                                       L_waste_type,
                                       L_item_parent,
                                       L_item_grandparent,
                                       L_short_desc,
                                       L_waste_pct,
                                       L_default_waste_pct,
                                       I_item) = FALSE THEN
            RETURN FALSE;
         END IF;
      END IF;

      IF INSERT_RECLASS_ITEM(O_error_message,
                             O_reject,
                             O_order_flag,
                             O_no_domain_flag,
                             L_counter,
                             L_system_forecast_ind,
                             L_domain_exists,
                             I_reclass_no,
                             I_new_dept,
                             I_new_class,
                             I_new_subclass,
                             L_item_level,
                             L_tran_level,
                             L_pack_ind,
                             I_item,
                             I_username) = FALSE THEN
         RETURN FALSE;
      END IF;
   END IF;

   if L_counter < 1 then
      SQL_LIB.SET_MARK('DELETE', NULL, 'RECLASS_HEAD_TL', TO_CHAR(I_reclass_no));

      delete from reclass_head_tl
            where reclass_no = I_reclass_no;

      SQL_LIB.SET_MARK('DELETE', NULL, 'RECLASS_HEAD', TO_CHAR(I_reclass_no));

      delete from reclass_head
            where reclass_no = I_reclass_no;

   end if;
   return TRUE;
   --
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
   RETURN FALSE;
END ITEMLIST_PROCESS;
---------------------------------------------------------------------------------------------
FUNCTION GET_PEND_HIER_DESC(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_hier_desc                   IN OUT   PEND_MERCH_HIER.MERCH_HIER_NAME%TYPE,
                            I_hier_type                   IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                            I_merch_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                            I_merch_parent_hier_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                            I_merch_grandparent_hier_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64)                          := 'RECLASS_SQL.GET_PEND_HIER_DESC';
   L_merch_hier_desc PEND_MERCH_HIER.MERCH_HIER_NAME%TYPE  := NULL;

   CURSOR C_PEND_HIER_DESC IS
      SELECT merch_hier_name
        FROM v_pend_merch_hier_tl
       WHERE hier_type = I_hier_type
         AND (merch_hier_id = NVL(I_merch_hier_id, NVL(merch_hier_id, -1)) OR
              merch_hier_id IS NULL)
         AND (merch_hier_parent_id = NVL(I_merch_parent_hier_id, NVL(merch_hier_parent_id, -1)) OR
              merch_hier_parent_id IS NULL)
         AND (merch_hier_grandparent_id = NVL(I_merch_grandparent_hier_id, NVL(merch_hier_grandparent_id, -1)) OR
              merch_hier_grandparent_id IS NULL);

BEGIN

   --- Check if the required input parameters are null
   IF I_hier_type IS NULL THEN
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hier_type',
                                            L_program);
      RETURN FALSE;
   END IF;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_PEND_HIER_DESC',
                    'V_PEND_MERCH_HIER_TL',
                    'HIER ID: ' || TO_CHAR(I_merch_hier_id));
   OPEN C_PEND_HIER_DESC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_PEND_HIER_DESC',
                    'V_PEND_MERCH_HIER_TL',
                    'HIER ID: ' || TO_CHAR(I_merch_hier_id));
   FETCH C_PEND_HIER_DESC INTO O_hier_desc;
   ---
   IF C_PEND_HIER_DESC%NOTFOUND THEN
      O_error_message := SQL_LIB.CREATE_MSG('INV_HIER_ID');
      SQL_LIB.SET_MARK('CLOSE',
                       'C_PEND_HIER_DESC',
                       'V_PEND_MERCH_HIER_TL',
                       'HIER ID: ' || TO_CHAR(I_merch_hier_id));
      CLOSE C_PEND_HIER_DESC;
      RETURN FALSE;
   END IF;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_PEND_HIER_DESC',
                    'V_PEND_MERCH_HIER_TL',
                    'HIER ID: ' || TO_CHAR(I_merch_hier_id));
   CLOSE C_PEND_HIER_DESC;


   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   RETURN FALSE;

END GET_PEND_HIER_DESC;
---------------------------------------------------------------------------------------------
FUNCTION GET_HIER_DESC(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_hier_desc                   IN OUT   PEND_MERCH_HIER.MERCH_HIER_NAME%TYPE,
                       I_hier_type                   IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                       I_merch_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                       I_merch_parent_hier_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                       I_merch_grandparent_hier_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(64)                          := 'RECLASS_SQL.GET_HIER_DESC';

   CURSOR C_HIER_DESC IS
      SELECT hierarchy_name
        FROM v_merch_hier
       WHERE hier_type = I_hier_type
         AND hierarchy_id = I_merch_hier_id
         AND (parent_hierarchy_id = NVL(I_merch_parent_hier_id, NVL(parent_hierarchy_id, -1)) OR
              parent_hierarchy_id IS NULL)
         AND (grandparent_hierarchy_id = NVL(I_merch_grandparent_hier_id, NVL(grandparent_hierarchy_id, -1)) OR
              grandparent_hierarchy_id IS NULL);

BEGIN

   --- Check if the required input parameters are null
   IF I_hier_type IS NULL THEN
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hier_type',
                                            L_program);
      RETURN FALSE;
   END IF;

   IF I_merch_hier_id IS NULL THEN
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_merch_hier_id',
                                               L_program);
      RETURN FALSE;
   END IF;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_HIER_DESC',
                    'V_MERCH_HIER',
                    'HIER ID: ' || TO_CHAR(I_merch_hier_id));
   OPEN C_HIER_DESC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_HIER_DESC',
                    'V_MERCH_HIER',
                    'HIER ID: ' || TO_CHAR(I_merch_hier_id));
   FETCH C_HIER_DESC INTO O_hier_desc ;

   IF C_HIER_DESC%NOTFOUND THEN
      O_error_message := SQL_LIB.CREATE_MSG('INV_HIER_ID');
      SQL_LIB.SET_MARK('CLOSE',
                       'C_HIER_DESC',
                       'V_MERCH_HIER',
                       'HIER ID: ' || TO_CHAR(I_merch_hier_id));
      CLOSE C_HIER_DESC;
      RETURN FALSE;
   END IF;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_HIER_DESC',
                    'V_MERCH_HIER',
                    'HIER ID: ' || TO_CHAR(I_merch_hier_id));
   CLOSE C_HIER_DESC;


   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   RETURN FALSE;

END GET_HIER_DESC;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_EFFECTIVE_DATE(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid                    IN OUT   VARCHAR2,
                              O_valid_reclass_date       IN OUT   RECLASS_HEAD.RECLASS_DATE%TYPE,
                              I_dept                     IN       DEPS.DEPT%TYPE,
                              I_class                    IN       CLASS.CLASS%TYPE,
                              I_subclass                 IN       SUBCLASS.SUBCLASS%TYPE,
                              I_reclass_effective_date   IN       RECLASS_HEAD.RECLASS_DATE%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'RECLASS_SQL.CHECK_EFFECTIVE_DATE';

   CURSOR C_CHECK_DATE IS
      SELECT effective_date
        FROM v_merch_hier
       WHERE hierarchy_id = I_subclass
         AND parent_hierarchy_id = I_class
         AND grandparent_hierarchy_id = I_dept
         AND I_reclass_effective_date <= effective_date;

BEGIN
   --- Check if the required input parameters are null
   IF I_dept IS NULL THEN
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program);
      RETURN FALSE;
   END IF;

   IF I_class IS NULL THEN
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_class',
                                             L_program);
      RETURN FALSE;
   END IF;

   IF I_subclass IS NULL THEN
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_subclass',
                                            L_program);
      RETURN FALSE;
   END IF;

   IF I_reclass_effective_date IS NULL THEN
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_reclass_effective_date',
                                            L_program);
      RETURN FALSE;
   END IF;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_DATE',
                    'V_MERCH_HIER',
                    'HIER ID: ' || TO_CHAR(I_subclass));
   OPEN C_CHECK_DATE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_DATE',
                    'V_MERCH_HIER',
                    'HIER ID: ' || TO_CHAR(I_subclass));
   FETCH C_CHECK_DATE INTO O_valid_reclass_date;

   IF C_CHECK_DATE%NOTFOUND THEN
      O_valid := 'Y';
   ELSE
      O_valid := 'N';
   END IF;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_DATE',
                    'V_MERCH_HIER',
                    'HIER ID: ' || TO_CHAR(I_subclass));
   CLOSE C_CHECK_DATE;

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   RETURN FALSE;

END CHECK_EFFECTIVE_DATE;
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_RECLASS_DATE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_date             IN OUT   DATE,
                               I_dept             IN       DEPS.DEPT%TYPE,
                               I_class            IN       CLASS.CLASS%TYPE,
                               I_subclass         IN       SUBCLASS.SUBCLASS%TYPE)

   RETURN BOOLEAN IS

   L_program  VARCHAR2(64)                   := 'RECLASS_SQL.GET_ITEM_RECLASS_DATE';

   CURSOR C_RECLASS_EXIST IS
      SELECT MIN(r.reclass_date)
        FROM reclass_head r,
             pend_merch_hier p
       WHERE r.to_dept = I_dept
         AND r.to_class = NVL(I_class,r.to_class)
         AND r.to_subclass = NVL(I_subclass,r.to_subclass)
         AND ((p.hier_type ='S' AND
               p.merch_hier_id = I_subclass AND
               p.merch_hier_parent_id = I_class AND
               p.merch_hier_grandparent_id = r.to_dept) OR
              (p.hier_type = 'C' AND
               p.merch_hier_id = I_class AND
               p.merch_hier_parent_id = r.to_dept) OR
              (p.hier_type = 'D' AND
               p.merch_hier_id = r.to_dept));

BEGIN

   --- Check if the required input parameters are null
   IF I_dept IS NULL THEN
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                             L_program);
      RETURN FALSE;
   END IF;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_RECLASS_EXIST',
                    'RECLASS_HEAD',
                    'HIER ID: ' || TO_CHAR(I_subclass));
   OPEN C_RECLASS_EXIST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_RECLASS_EXIST',
                    'RECLASS_HEAD',
                    'HIER ID: ' || TO_CHAR(I_subclass));
   FETCH C_RECLASS_EXIST INTO O_date;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_RECLASS_EXIST',
                    'RECLASS_HEAD',
                    'HIER ID: ' || TO_CHAR(I_subclass));
   CLOSE C_RECLASS_EXIST;

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   RETURN FALSE;

END GET_ITEM_RECLASS_DATE;
--------------------------------------------------------------------------------------------
FUNCTION ITEM_ON_EXISTING_RECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists          IN OUT   BOOLEAN,
                                  I_item_no         IN       RECLASS_ITEM.ITEM%TYPE,
                                  I_reclass_no      IN       RECLASS_ITEM.RECLASS_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'RECLASS_SQL.ITEM_ON_EXISTING_RECLASS';
   L_exists    VARCHAR2(1)   := NULL;

   cursor C_ITEM_ON_RECLASS is
      select 'x'
        from reclass_item
       where item = I_item_no
         and reclass_no = nvl(I_reclass_no, reclass_no);

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_ON_RECLASS',
                    'RECLASS_ITEM',
                    'Item: ' || I_item_no||' , Reclass no: '||I_reclass_no);
   open C_ITEM_ON_RECLASS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_ON_RECLASS',
                    'RECLASS_ITEM',
                    'Item: ' || I_item_no||' , Reclass no: '||I_reclass_no);
   fetch C_ITEM_ON_RECLASS into L_exists;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_ON_RECLASS',
                    'RECLASS_ITEM',
                    'Item: ' || I_item_no||' , Reclass no: '||I_reclass_no);
   close C_ITEM_ON_RECLASS;
   ---
   if L_exists is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_ON_EXISTING_RECLASS;
--------------------------------------------------------------------------------------------
FUNCTION RECLASS_DATE_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist           IN OUT   BOOLEAN,
                            I_reclass_date    IN       RECLASS_HEAD.RECLASS_DATE%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(50)  := 'RECLASS_SQL.RECLASS_DATE_EXIST';
   L_exist    VARCHAR2(1)   := NULL;

   cursor C_RECLASS_DATE is
      select 'x'
        from reclass_head
       where reclass_date = I_reclass_date;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_RECLASS_DATE',
                    'RECLASS_ITEM',
                    'Reclass date: ' || TO_CHAR(I_reclass_date));
   open C_RECLASS_DATE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_RECLASS_DATE',
                    'RECLASS_ITEM',
                    'Reclass date: ' || TO_CHAR(I_reclass_date));
  fetch C_RECLASS_DATE into L_exist;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_RECLASS_DATE',
                    'RECLASS_ITEM',
                    'Reclass date: ' || TO_CHAR(I_reclass_date));
   close C_RECLASS_DATE;

   if L_exist is NOT NULL then
      O_exist := TRUE;
   else
      O_exist := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RECLASS_DATE_EXIST;
--------------------------------------------------------------------------------------------
FUNCTION CREATE_RECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_reclass_rec     IN       RECLASS_REC)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(50)  := 'RECLASS_SQL.CREATE_RECLASS';

BEGIN
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'RECLASS_HEAD',
                    'Reclass no: ' || TO_CHAR(I_reclass_rec.reclass_head_row.reclass_no));
   insert into reclass_head(reclass_no,
                            reclass_desc,
                            reclass_date,
                            to_dept,
                            to_class,
                            to_subclass)
                     values(I_reclass_rec.reclass_head_row.reclass_no,
                            I_reclass_rec.reclass_head_row.reclass_desc,
                            I_reclass_rec.reclass_head_row.reclass_date,
                            I_reclass_rec.reclass_head_row.to_dept,
                            I_reclass_rec.reclass_head_row.to_class,
                            I_reclass_rec.reclass_head_row.to_subclass);

   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_INSERT_REC');
      return FALSE;
   end if;

   if CREATE_RECLASS_ITEM(O_error_message,
                          I_reclass_rec) = FALSE then
      return FALSE;
   end if;

   -- Invoke the future cost engine,
   if CREATE_FC_FOR_RECLASS(O_error_message,
                            I_reclass_rec.reclass_head_row.reclass_no) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_RECLASS;
--------------------------------------------------------------------------------------------
FUNCTION CREATE_RECLASS_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_reclass_rec     IN       RECLASS_REC)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(50)  := 'RECLASS_SQL.CREATE_RECLASS_ITEM';

BEGIN

   if I_reclass_rec.reclass_item_tbl is NOT NULL and
      I_reclass_rec.reclass_item_tbl.COUNT > 0 then
      ---
      FOR i in I_reclass_rec.reclass_item_tbl.FIRST..I_reclass_rec.reclass_item_tbl.LAST LOOP
         if I_reclass_rec.reclass_item_tbl(i) is NOT NULL then
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'RECLASS_ITEM',
                             'Reclass no: ' || TO_CHAR(I_reclass_rec.reclass_head_row.reclass_no) ||
                             'Item: ' || TO_CHAR(I_reclass_rec.reclass_item_tbl(i)));
            insert into reclass_item(reclass_no,
                                     item)
                              values(I_reclass_rec.reclass_head_row.reclass_no,
                                     I_reclass_rec.reclass_item_tbl(i));
            if SQL%NOTFOUND then
               O_error_message :=  SQL_LIB.CREATE_MSG('COULD_NOT_INSERT_REC');
               return FALSE;
            end if;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_RECLASS_ITEM;
--------------------------------------------------------------------------------------------
FUNCTION DELETE_RECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_reclass_rec     IN       RECLASS_REC)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)  := 'RECLASS_SQL.DELETE_RECLASS';
   L_table          VARCHAR2(20);
   L_reclass_no     RECLASS_HEAD.RECLASS_NO%TYPE    :=  I_reclass_rec.reclass_head_row.reclass_no;
   L_reclass_date   RECLASS_HEAD.RECLASS_DATE%TYPE  :=  I_reclass_rec.reclass_head_row.reclass_date;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_RECLASS_HEAD_TL is
      select 'x'
        from reclass_head_tl
       where reclass_no in (select reclass_no
                              from reclass_head
                             where reclass_no = NVL(L_reclass_no, reclass_no)
                               and reclass_date = NVL(L_reclass_date, reclass_date))
         for update nowait;

   cursor C_LOCK_RECLASS_HEAD is
      select 'x'
        from reclass_head
       where reclass_no = NVL(L_reclass_no, reclass_no)
         and reclass_date = NVL(L_reclass_date, reclass_date)
         for update nowait;

   cursor C_LOCK_RECLASS_ITEM is
      select 'x'
        from reclass_item ri,
             reclass_head rh
       where rh.reclass_no = ri.reclass_no
         and rh.reclass_date = NVL(L_reclass_date, rh.reclass_date)
         and rh.reclass_no = NVL(L_reclass_no, rh.reclass_no)
         for update nowait;

   cursor C_LOCK_RECLASS_HEAD_PURGE_TL is
      select 'x'
        from reclass_head_tl
        for update nowait;

   cursor C_LOCK_RECLASS_HEAD_PURGE is
      select 'x'
        from reclass_head
        for update nowait;

   cursor C_LOCK_RECLASS_ITEM_PURGE is
      select 'x'
        from reclass_item
        for update nowait;

   cursor C_GET_RECLASS is
      select obj_rcls_cost_event_rec(rh.reclass_no,
                                     rh.reclass_date,
                                     ri.item,
                                     rh.to_dept,
                                     rh.to_class,
                                     rh.to_subclass)
        from reclass_head rh,
             reclass_item ri
       where rh.reclass_no = ri.reclass_no
         and rh.reclass_no = NVL(L_reclass_no, rh.reclass_no)
         and rh.reclass_date = NVL(L_reclass_date, rh.reclass_date);

BEGIN
   if UPPER(I_reclass_rec.purge_all_ind) = 'Y' then
      L_reclass_no   := NULL;
      L_reclass_date := NULL;
   elsif L_reclass_no is NOT NULL then
      L_reclass_date := NULL;
   elsif L_reclass_date is NOT NULL then
      L_reclass_no := NULL;
   end if;

   if UPPER(I_reclass_rec.purge_all_ind) = 'Y' then
      -- Fetch reclass table
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_RECLASS',
                       'RECLASS_HEAD,RECLASS_ITEM',
                       NULL);
      open C_GET_RECLASS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_RECLASS',
                       'RECLASS_HEAD,RECLASS_ITEM',
                       NULL);
      fetch C_GET_RECLASS bulk collect into L_reclass_tbl;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_RECLASS',
                       'RECLASS_HEAD,RECLASS_ITEM',
                       NULL);
      close C_GET_RECLASS;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_RECLASS_ITEM_PURGE',
                       'RECLASS_ITEM',
                       NULL);
      open C_LOCK_RECLASS_ITEM_PURGE;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_RECLASS_ITEM_PURGE',
                       'RECLASS_ITEM',
                       NULL);
      close C_LOCK_RECLASS_ITEM_PURGE;

      -- delete all records from the reclass_item table
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'RECLASS_ITEM',
                       NULL);
      delete from reclass_item;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_RECLASS_HEAD_PURGE_TL',
                       'RECLASS_HEAD_TL',
                       NULL);
      open C_LOCK_RECLASS_HEAD_PURGE_TL;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_RECLASS_HEAD_PURGE_TL',
                       'RECLASS_HEAD_TL',
                       NULL);
      close C_LOCK_RECLASS_HEAD_PURGE_TL;

      -- delete all records from the reclass_head_tl table
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'RECLASS_HEAD_TL',
                       NULL);
      delete from reclass_head_tl;

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_RECLASS_HEAD_PURGE',
                       'RECLASS_HEAD',
                       NULL);
      open C_LOCK_RECLASS_HEAD_PURGE;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_RECLASS_HEAD_PURGE',
                       'RECLASS_HEAD',
                       NULL);
      close C_LOCK_RECLASS_HEAD_PURGE;

      -- delete all records from the reclass_head table
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'RECLASS_HEAD',
                       NULL);
      delete from reclass_head;

      -- Invoke the future cost engine,
      if DELETE_FC_FOR_RECLASS(O_error_message,
                               L_reclass_tbl) = FALSE then
         return FALSE;
      end if;
      ---
   else
      -- Fetch reclass table
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_RECLASS',
                       'RECLASS_HEAD,RECLASS_ITEM',
                       NULL);
      open C_GET_RECLASS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_RECLASS',
                       'RECLASS_HEAD,RECLASS_ITEM',
                       NULL);
      fetch C_GET_RECLASS bulk collect into L_reclass_tbl;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_RECLASS',
                       'RECLASS_HEAD,RECLASS_ITEM',
                       NULL);
      close C_GET_RECLASS;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_RECLASS_ITEM',
                       'RECLASS_ITEM',
                       'Reclass no: ' || L_reclass_no ||
                       'Reclass date: ' || L_reclass_date);
      open C_LOCK_RECLASS_ITEM;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_RECLASS_ITEM',
                       'RECLASS_ITEM',
                       'Reclass no: ' || L_reclass_no ||
                       'Reclass date: ' || L_reclass_date);
      close C_LOCK_RECLASS_ITEM;

      L_table := 'RECLASS_ITEM';
      ---
      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'RECLASS_ITEM',
                       'Reclass no: ' || L_reclass_no ||
                       'Reclass date: ' || L_reclass_date);
      delete from reclass_item ri
       where exists (select 1
                       from reclass_head rh
                      where rh.reclass_no = ri.reclass_no
                        and rh.reclass_no = NVL(L_reclass_no, rh.reclass_no)
                        and rh.reclass_date = NVL(L_reclass_date, rh.reclass_date));

     if SQL%NOTFOUND then
        O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS');
        return FALSE;
     end if;

     ---
     SQL_LIB.SET_MARK('OPEN',
                      'C_LOCK_RECLASS_HEAD_TL',
                      'RECLASS_HEAD_TL',
                      'Reclass no: ' || L_reclass_no ||
                      'Reclass date: ' || L_reclass_date);
     open C_LOCK_RECLASS_HEAD_TL;

     SQL_LIB.SET_MARK('CLOSE',
                      'C_LOCK_RECLASS_HEAD_TL',
                      'RECLASS_HEAD_TL',
                      'Reclass no: ' || L_reclass_no ||
                      'Reclass date: ' || L_reclass_date);
     close C_LOCK_RECLASS_HEAD_TL;

     L_table := 'RECLASS_HEAD_TL';
     ---
     SQL_LIB.SET_MARK('DELETE',
                      NULL,
                      'RECLASS_HEAD_TL',
                      'Reclass no: ' || L_reclass_no ||
                      'Reclass date: ' || L_reclass_date);
     delete from reclass_head_tl
      where reclass_no in (select reclass_no
                             from reclass_head
                            where reclass_no = NVL(L_reclass_no, reclass_no)
                              and reclass_date = NVL(L_reclass_date, reclass_date));
     
     SQL_LIB.SET_MARK('OPEN',
                      'C_LOCK_RECLASS_HEAD',
                      'RECLASS_HEAD',
                      'Reclass no: ' || L_reclass_no ||
                      'Reclass date: ' || L_reclass_date);
     open C_LOCK_RECLASS_HEAD;

     SQL_LIB.SET_MARK('CLOSE',
                      'C_LOCK_RECLASS_HEAD',
                      'RECLASS_HEAD',
                      'Reclass no: ' || L_reclass_no ||
                      'Reclass date: ' || L_reclass_date);
     close C_LOCK_RECLASS_HEAD;

     L_table := 'RECLASS_HEAD';
     ---
     SQL_LIB.SET_MARK('DELETE',
                      NULL,
                      'RECLASS_HEAD',
                      'Reclass no: ' || L_reclass_no ||
                      'Reclass date: ' || L_reclass_date);
     delete from reclass_head
      where reclass_no = NVL(L_reclass_no, reclass_no)
        and reclass_date = NVL(L_reclass_date, reclass_date);

     if SQL%NOTFOUND then
        O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS');
        return FALSE;
     end if;

     -- Invoke the future cost engine,
     if DELETE_FC_FOR_RECLASS(O_error_message,
                              L_reclass_tbl) = FALSE then
        return FALSE;
     end if;
     ---
  end if;

  return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'Reclass no: ' || TO_CHAR(L_reclass_no),
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_RECLASS;
--------------------------------------------------------------------------------------------
FUNCTION DELETE_RECLASS_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_reclass_rec     IN       RECLASS_REC)
   RETURN BOOLEAN IS

   L_program                 VARCHAR2(50) := 'RECLASS_SQL.DELETE_RECLASS_ITEM';
   L_table                   VARCHAR2(20);
   L_exists                  VARCHAR2(1);
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(Record_Locked, -54);
   L_reclass_no              RECLASS_HEAD.RECLASS_NO%TYPE;
   L_reclass_id_update_tbl   OBJ_NUMERIC_ID_TABLE;

   cursor C_LOCK_RECLASS_ITEM is
      select 'x'
        from reclass_item
       where item in (select *
                        from table(cast (I_reclass_rec.reclass_item_tbl as item_tbl)))
         and reclass_no = NVL(L_reclass_no, reclass_no)
         for update nowait;

   cursor C_LOCK_RECLASS_HEAD_TL is
      select 'x'
        from reclass_head_tl
       where reclass_no = L_reclass_no
         for update nowait;

   cursor C_LOCK_RECLASS_HEAD is
      select 'x'
        from reclass_head
       where reclass_no = L_reclass_no
         for update nowait;

   cursor C_CHECK_RECLASS_ITEM is
      select 'x'
        from reclass_item
       where reclass_no = L_reclass_no
         and rownum = 1;

   cursor C_LOCK_RECLASS_TL is
      select 'x'
        from reclass_head_tl rh
       where not exists (select 'x'
                           from reclass_item ri
                          where rh.reclass_no = ri.reclass_no
                            and rownum = 1)
         for update nowait;

   cursor C_LOCK_RECLASS is
      select 'x'
        from reclass_head rh
       where not exists (select 'x'
                           from reclass_item ri
                          where rh.reclass_no = ri.reclass_no
                          and rownum = 1)
         for update nowait;

   cursor C_GET_RECLASS is
      select obj_rcls_cost_event_rec(rh.reclass_no,
                                     rh.reclass_date,
                                     ri.item,
                                     rh.to_dept,
                                     rh.to_class,
                                     rh.to_subclass)
        from reclass_head rh,
             reclass_item ri,
             table(cast (I_reclass_rec.reclass_item_tbl as item_tbl)) t
       where rh.reclass_no = ri.reclass_no
         and rh.reclass_no = NVL(L_reclass_no, rh.reclass_no)
         and ri.item = value(t);

BEGIN
   L_reclass_no := I_reclass_rec.reclass_head_row.reclass_no;
   -- Fetch reclass table
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_RECLASS',
                    'RECLASS_HEAD,RECLASS_ITEM',
                    L_reclass_no);
   open C_GET_RECLASS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_RECLASS',
                    'RECLASS_HEAD,RECLASS_ITEM',
                    L_reclass_no);
   fetch C_GET_RECLASS bulk collect into L_reclass_tbl;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_RECLASS',
                    'RECLASS_HEAD,RECLASS_ITEM',
                    L_reclass_no);
   close C_GET_RECLASS;
   ---
   L_table := 'RECLASS_ITEM';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_RECLASS_ITEM',
                    'RECLASS_ITEM',
                    NULL);
   open C_LOCK_RECLASS_ITEM;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_RECLASS_ITEM',
                    'RECLASS_ITEM',
                    NULL);
   close C_LOCK_RECLASS_ITEM;

   FORALL i in I_reclass_rec.reclass_item_tbl.FIRST..I_reclass_rec.reclass_item_tbl.LAST
      delete from reclass_item
       where item = I_reclass_rec.reclass_item_tbl(i)
         and reclass_no = NVL(L_reclass_no, reclass_no);
   ---
   if L_reclass_no is NOT NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_RECLASS_ITEM',
                       'RECLASS_ITEM',
                       NULL);
      open C_CHECK_RECLASS_ITEM;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_RECLASS_ITEM',
                       'RECLASS_ITEM',
                       NULL);
      fetch C_CHECK_RECLASS_ITEM into L_exists;
      ---
      if C_CHECK_RECLASS_ITEM%NOTFOUND then

         L_table := 'RECLASS_HEAD_TL';

         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_RECLASS_HEAD_TL',
                          'RECLASS_HEAD_TL',
                          NULL);
         open C_LOCK_RECLASS_HEAD_TL;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_RECLASS_HEAD_TL',
                          'RECLASS_HEAD_TL',
                          NULL);
         close C_LOCK_RECLASS_HEAD_TL;

         delete from reclass_head_tl
          where reclass_no = L_reclass_no;

         L_table := 'RECLASS_HEAD';

         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_RECLASS_HEAD',
                          'RECLASS_HEAD',
                          NULL);
         open C_LOCK_RECLASS_HEAD;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_RECLASS_HEAD',
                          'RECLASS_HEAD',
                          NULL);
         close C_LOCK_RECLASS_HEAD;

         delete from reclass_head
          where reclass_no = L_reclass_no;

      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_RECLASS_ITEM',
                       'RECLASS_ITEM',
                       NULL);
      close C_CHECK_RECLASS_ITEM;
   else
      L_table := 'RECLASS_HEAD_TL';

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_RECLASS_TL',
                       'RECLASS_HEAD_TL',
                       NULL);
      open C_LOCK_RECLASS_TL;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_RECLASS_TL',
                       'RECLASS_HEAD_TL',
                       NULL);
      close C_LOCK_RECLASS_TL;

      delete from reclass_head_tl rh
       where not exists (select 'x'
                           from reclass_item ri
                          where rh.reclass_no = ri.reclass_no
                            and rownum = 1);

      L_table := 'RECLASS_HEAD';

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_RECLASS',
                       'RECLASS_HEAD',
                       NULL);
      open C_LOCK_RECLASS;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_RECLASS',
                       'RECLASS_HEAD',
                       NULL);
      close C_LOCK_RECLASS;

      delete from reclass_head rh
       where not exists (select 'x'
                           from reclass_item ri
                          where rh.reclass_no = ri.reclass_no
                            and rownum = 1);
   end if;
   -- Invoke the future cost engine,
   if DELETE_FC_FOR_RECLASS(O_error_message,
                            L_reclass_tbl) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'Reclass no: ' || TO_CHAR(L_reclass_no),
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return FALSE;
END DELETE_RECLASS_ITEM;
--------------------------------------------------------------------------------------------
FUNCTION PARENT_EFFECTIVE_DATE(O_error_message         IN OUT   VARCHAR2,
                               O_date                  IN OUT   DATE,
                               I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                               I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                               I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'RECLASS_SQL.PARENT_EFFECTIVE_DATE';

   L_hier_id               PEND_MERCH_HIER.MERCH_HIER_ID%TYPE              := NULL;
   L_hier_parent_id        PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE       := NULL;
   L_hier_grandparent_id   PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE  := NULL;
   L_parent_hier_type      PEND_MERCH_HIER.HIER_TYPE%TYPE;

   cursor C_GET_DATE is
      select effective_date
        from v_merch_hier
       where hier_type = L_parent_hier_type
         and  hierarchy_id = L_hier_id
         and NVL(parent_hierarchy_id, -999) =
             NVL(L_hier_parent_id, NVL(parent_hierarchy_id, -999))
         and NVL(grandparent_hierarchy_id, -999) =
             NVL(L_hier_grandparent_id, NVL(grandparent_hierarchy_id, -999));

BEGIN

   if I_hier_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_hier_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_hier_type NOT IN (MERCH_RECLASS_VALIDATE_SQL.LP_grp_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_dept_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_cls_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_scls_code) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_MERCH_LEVEL',
                                            I_hier_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_hier_parent_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_hier_parent_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
      if I_hier_grandparent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_hier_grandparent_id',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
   end if;

   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_grp_code then
      L_parent_hier_type := MERCH_RECLASS_VALIDATE_SQL.LP_div_code;
   elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_dept_code then
      L_parent_hier_type := MERCH_RECLASS_VALIDATE_SQL.LP_grp_code;
   elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_cls_code then
      L_parent_hier_type := MERCH_RECLASS_VALIDATE_SQL.LP_dept_code;
   elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
      L_parent_hier_type := MERCH_RECLASS_VALIDATE_SQL.LP_cls_code;
   end if;

   if MERCH_RECLASS_SQL.DETERMINE_HIERARCHY(O_error_message,
                                            L_hier_id,
                                            L_hier_parent_id,
                                            L_hier_grandparent_id,
                                            L_parent_hier_type,
                                            I_hier_parent_id,
                                            I_hier_grandparent_id,
                                            NULL) = FALSE then
      return FALSE;
   end if;

   -- perform query
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_DATE',
                    'V_MERCH_HIER',
                    ' hierarchy_id: '||TO_CHAR(L_hier_id)||
                    ' parent_hierarchy_id: '||TO_CHAR(L_hier_parent_id)||
                    ' grandparent_hierarchy_id: '||TO_CHAR(L_hier_grandparent_id));
   open C_GET_DATE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_DATE',
                    'V_MERCH_HIER',
                    ' hierarchy_id: '||TO_CHAR(L_hier_id)||
                    ' parent_hierarchy_id: '||TO_CHAR(L_hier_parent_id)||
                    ' grandparent_hierarchy_id: '||TO_CHAR(L_hier_grandparent_id));
   fetch C_GET_DATE INTO O_date;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_DATE',
                    'V_MERCH_HIER',
                    ' hierarchy_id: '||TO_CHAR(L_hier_id)||
                    ' parent_hierarchy_id: '||TO_CHAR(L_hier_parent_id)||
                    ' grandparent_hierarchy_id: '||TO_CHAR(L_hier_grandparent_id));
   close C_GET_DATE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PARENT_EFFECTIVE_DATE;
---------------------------------------------------------------------------------------------
FUNCTION POST_PROCESS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'RECLASS_SQL.POST_PROCESS';
   L_vdate              DATE         := get_vdate;
   L_system_options     SYSTEM_OPTIONS%ROWTYPE;
   L_locked             BOOLEAN := TRUE;
   L_table              VARCHAR2(30);
   
   L_rowid_TBL ROWID_CHAR_TBL;

   TYPE T_dept_TBL is TABLE of ordhead.dept%TYPE INDEX BY PLS_INTEGER;
   L_dept_TBL T_dept_TBL;
   
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   
   -- still need to handle OTB
   cursor C_RECLASS_ITEM is
      select r.item,
             r.old_dept,
             r.old_class,
             r.old_subclass,
             r.new_dept,
             r.new_class,
             r.new_subclass,
             r.reclass_date,
             im.item_level,
             im.tran_level,
             im.pack_ind
        from reclass_item_temp r,
             item_master im
       where im.item = r.item;

   cursor C_ORDERS is
      select h.rowid oh_rowid,
             max(t.new_dept) new_dept
        from reclass_item_temp t,
             ordhead h,
             ordsku s,
             system_options
       where s.item = t.item
         and s.order_no = h.order_no
         and (h.status in ('W','A','S') AND reclass_appr_order_ind = 'Y'
              OR h.status in ('W','S') AND reclass_appr_order_ind = 'N')
         and t.old_dept != t.new_dept
         and h.dept is NOT NULL
       group by h.rowid;

   cursor C_LOCK_ORDERS is
      select 'x'
        from ordhead oh
       where exists (select 'x' 
                       from TABLE(CAST(L_rowid_TBL as ROWID_CHAR_TBL)) rwt
                      where value(rwt) = oh.rowid
                        and rownum = 1)
         for update nowait;

   cursor C_NULL_DEPS is
      select rowid
        from ordhead
       where order_no in (select oh.order_no
                            from ordhead oh,
                                 ordsku os,
                                 item_master im
                           where oh.order_no = os.order_no
                             and os.item = im.item
                             and oh.dept!= im.dept
                             and ((oh.status in ('W','A','S') AND L_system_options.reclass_appr_order_ind = 'Y')
                                  OR (oh.status in ('W','S') AND L_system_options.reclass_appr_order_ind = 'N')))
       for update nowait;
   
   cursor C_CLOSED_DEAL is
      select dile.rowid
        from deal_head dh,
             deal_item_loc_explode dile
       where dh.deal_id = dile.deal_id
         and dh.status = 'A'
         and dile.close_date < dh.close_date
         and (L_vdate > (dile.close_date + NVL(dh.bbd_add_rep_days,0)))
         for update of dile.deal_id nowait;
 
BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      return FALSE;
   end if;
   
   if L_system_options.otb_system_ind = 'Y' then
      for c_reclass in C_RECLASS_ITEM loop
         if c_reclass.item_level = c_reclass.tran_level and c_reclass.pack_ind = 'N' then
            if OTB_SQL.RECLASS(O_error_message,
                               L_locked,
                               c_reclass.reclass_date,
                               c_reclass.item,
                               c_reclass.old_dept,
                               c_reclass.old_class,
                               c_reclass.old_subclass,
                               c_reclass.new_dept,
                               c_reclass.new_class,
                               c_reclass.new_subclass) = FALSE then
                return FALSE;
             end if;
         end if;
      end loop;   
   end if;

   SQL_LIB.SET_MARK('INSERT', 
                     NULL, 
                    'DEAL_CALC_QUEUE,RECLASS_ITEM_TEMP,ORDHEAD,ORDSKU', 
                     NULL);
   insert into deal_calc_queue (order_no,
                                recalc_all_ind,
                                override_manual_ind,
                                order_appr_ind)
                        select  distinct h.order_no,
                                'N',
                                'N',
                                'N'
                          from  reclass_item_temp t,
                                ordhead h,
                                ordsku s
                         where  s.item = t.item
                           and  s.order_no = h.order_no
                           and  h.status in ('W','A','S')
                           and  h.order_no not in (select order_no
                                                     from deal_calc_queue);

   SQL_LIB.SET_MARK('OPEN',
                    'C_ORDERS',
                    'RECLASS_ITEM_TEMP,ORDHEAD,ORDSKU', 
                    NULL);
   open C_ORDERS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ORDERS',
                    'RECLASS_ITEM_TEMP,ORDHEAD,ORDSKU',
                    NULL);
   fetch C_ORDERS bulk collect into L_rowid_TBL, L_dept_TBL;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORDERS',
                    'RECLASS_ITEM_TEMP,ORDHEAD,ORDSKU',
                    NULL);
   close C_ORDERS;

   L_table := 'ORDHEAD';
   
   if L_rowid_TBL.count > 0 then
      -- Lock rowids before update
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ORDERS',
                       'ORDHEAD',
                        NULL);
      open C_LOCK_ORDERS;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ORDERS',
                       'ORDHEAD',
                        NULL);
      close C_LOCK_ORDERS;

      SQL_LIB.SET_MARK('UPDATE', 
                        NULL, 
                       'ORDHEAD', 
                        NULL);
      FORALL i in L_rowid_TBL.first..L_rowid_TBL.last
         update ordhead
            set dept = L_dept_TBL(i),
                last_update_id = get_user,
                last_update_datetime = sysdate
          where rowid = L_rowid_TBL(i);
   end if;
   
   L_rowid_TBL.DELETE;
   SQL_LIB.SET_MARK('OPEN',
                    'C_NULL_DEPS',
                    'ORDHEAD',
                     NULL);
   open C_NULL_DEPS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_NULL_DEPS',
                    'ORDHEAD',
                     NULL);
   fetch C_NULL_DEPS BULK COLLECT into L_rowid_TBL;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_NULL_DEPS',
                    'ORDHEAD',
                    NULL);
   close C_NULL_DEPS;

   SQL_LIB.SET_MARK('UPDATE', 
                    NULL, 
                   'ORDHEAD', 
                    NULL);

   FORALL i in L_rowid_TBL.first..L_rowid_TBL.last
      update ordhead
         set dept = NULL,
             last_update_id = get_user,
             last_update_datetime = sysdate
       where rowid = L_rowid_TBL(i);

   SQL_LIB.SET_MARK('INSERT', 
                     NULL, 
                    'HIST_REBUILD_MASK,RECLASS_ITEM_TEMP', 
                     NULL);
   insert into hist_rebuild_mask(dept)
      select distinct dept
        from (select old_dept dept
                from reclass_item_temp
           union all
              select new_dept dept
                from reclass_item_temp);

   SQL_LIB.SET_MARK('INSERT', 
                     NULL, 
                    'SKULIST_DEPT,RECLASS_ITEM_TEMP,SKULIST_DETAIL', 
                     NULL);
   insert into skulist_dept (skulist,
                             dept,
                             class,
                             subclass)
                      select distinct s.skulist,
                             t.new_dept,
                             t.new_class,
                             t.new_subclass
                        from reclass_item_temp t,
                             skulist_detail s
                       where s.item = t.item
                         and (s.skulist, t.new_dept, t.new_class, t.new_subclass) not in
                                                                                  (select skulist, dept, class,subclass
                                                                                     from skulist_dept);

   L_rowid_TBL.DELETE;
   SQL_LIB.SET_MARK('OPEN',
                      'C_CLOSED_DEAL',
                      'DEAL_ITEM_LOC_EXPLODE,DEALHEAD',
                       NULL);
   open C_CLOSED_DEAL;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CLOSED_DEAL',
                    'DEAL_ITEM_LOC_EXPLODE,DEALHEAD',
                     NULL);
   fetch C_CLOSED_DEAL bulk collect into L_rowid_TBL;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CLOSED_DEAL',
                    'DEAL_ITEM_LOC_EXPLODE,DEALHEAD',
                     NULL);
   close C_CLOSED_DEAL;

   if L_rowid_TBL.count > 0 then
      SQL_LIB.SET_MARK('DELETE',
                        NULL,
                       'DEAL_ITEM_LOC_EXPLODE',
                        NULL);
      FORALL i in L_rowid_TBL.first..L_rowid_TBL.last
         delete from deal_item_loc_explode
               where rowid = L_rowid_TBL(i);
   end if;

   if RECLASS_SQL.UPDATE_ITEM_LOC_DTL_EXPLD(O_error_message) = FALSE then
      return FALSE;
   end if;

return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             NULL,
                                             NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END POST_PROCESS;
---------------------------------------------------------------------------------------------
FUNCTION RECLASS_ITEM_EXIST(O_error_message   IN OUT   VARCHAR2,
                            O_exists          IN OUT   BOOLEAN,
                            I_reclass_no      IN       RECLASS_HEAD.RECLASS_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'RECLASS_SQL.RECLASS_ITEM_EXIST';
   L_exists    VARCHAR2(1)  := NULL;

   cursor C_EXIST_RECLASS_ITEM IS
      select 'Y'
        from reclass_item
       where reclass_no = I_reclass_no
         and rownum = 1;

BEGIN
   -- Check to see if Reclassification Number exists
   SQL_LIB.SET_MARK('OPEN',
                    'C_EXIST_RECLASS_ITEM',
                    'RECLASS_HEAD',
                    TO_CHAR(I_reclass_no));
   open C_EXIST_RECLASS_ITEM;

   SQL_LIB.SET_MARK('FETCH',
                    'C_EXIST_RECLASS_ITEM',
                    'RECLASS_HEAD',
                    TO_CHAR(I_reclass_no));
   fetch C_EXIST_RECLASS_ITEM into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXIST_RECLASS_ITEM',
                    'RECLASS_HEAD',
                    TO_CHAR(I_reclass_no));
   close C_EXIST_RECLASS_ITEM;

   -- if Reclassification number is unique, return true
   if L_exists = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RECLASS_ITEM_EXIST;
---------------------------------------------------------------------------------------------
FUNCTION CREATE_FC_FOR_RECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_reclass_no      IN       RECLASS_HEAD.RECLASS_NO%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'RECLASS_SQL.CREATE_FC_FOR_RECLASS';

   cursor C_GET_RECLASS is
      WITH reclass_item_info AS 
           ( 
               SELECT ri.item item,ri.reclass_no 
                 FROM reclass_item ri 
                WHERE ri.reclass_no = I_reclass_no 
                UNION 
              (SELECT pi.pack_no item, ri.reclass_no 
                FROM item_master im1, packitem pi, item_master comp, 
                     reclass_item ri 
               WHERE pi.pack_no = im1.item 
                 AND im1.simple_pack_ind = 'Y' 
                 AND pi.item = comp.item 
                 AND comp.item_level = comp.tran_level 
                 AND comp.item = ri.item 
                 AND ri.reclass_no = I_reclass_no 
              UNION ALL 
              SELECT pi.pack_no item, ri.reclass_no 
                FROM item_master im1, packitem pi, item_master comp, 
                     reclass_item ri 
               WHERE pi.pack_no = im1.item 
                 AND im1.simple_pack_ind = 'Y' 
                 AND pi.item = comp.item 
                 AND comp.item_level = comp.tran_level 
                 AND comp.item_parent = ri.item 
                 AND ri.reclass_no = I_reclass_no 
              UNION ALL 
              SELECT pi.pack_no item, ri.reclass_no 
                FROM reclass_item ri, item_master comp, packitem pi, 
                     item_master im1 
               WHERE pi.pack_no = im1.item 
                 AND im1.simple_pack_ind = 'Y' 
                 AND pi.item = comp.item 
                 AND comp.item_level = comp.tran_level 
                 AND comp.item_grandparent = ri.item 
                 AND ri.reclass_no = I_reclass_no
                ) 
             ) 
      SELECT  obj_rcls_cost_event_rec(rh.reclass_no, 
                                      rh.reclass_date, 
                                      reclass_item_info.item, 
                                      rh.to_dept, 
                                      rh.to_class, 
                                      rh.to_subclass) 
        FROM reclass_item_info, reclass_head rh 
 WHERE reclass_item_info.reclass_no = rh.reclass_no AND rh.reclass_no = I_reclass_no;

BEGIN
   -- Fetch reclass_table
   open C_GET_RECLASS;
   fetch C_GET_RECLASS bulk collect into L_reclass_tbl;
   close C_GET_RECLASS;

   if L_reclass_tbl is NOT NULL and L_reclass_tbl.COUNT > 0 then
      L_reclass_action := FUTURE_COST_EVENT_SQL.ADD_EVENT;
      -- Invoke the future cost engine.
      if FUTURE_COST_EVENT_SQL.ADD_RECLASS_EVENT(O_error_message,
                                                 L_cost_event_process_id,
                                                 L_reclass_action,
                                                 L_reclass_tbl,
                                                 L_username) = FALSE then
         L_reclass_tbl.DELETE;
         return FALSE;
      end if;
      L_reclass_tbl.DELETE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      IF C_GET_RECLASS%ISOPEN THEN
         CLOSE C_GET_RECLASS;
      END IF;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_FC_FOR_RECLASS;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_FC_FOR_RECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_reclass_tbl     IN       OBJ_RCLS_COST_EVENT_TBL)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(64) := 'RECLASS_SQL.CREATE_FC_FOR_RECLASS';
   L_exists                BOOLEAN := FALSE;
   L_distinct_reclass_no   RECLASS_HEAD.RECLASS_NO%TYPE;

   cursor C_DISTINCT_RECLASS is
      select distinct rec.reclass_no
        from table(cast(I_reclass_tbl as obj_rcls_cost_event_tbl)) rec;

   cursor C_GET_RECLASS is
      select obj_rcls_cost_event_rec(reclass_no,
                                     reclass_date,
                                     item,
                                     to_dept,
                                     to_class,
                                     to_subclass)
        from (select distinct rec.reclass_no,
                              rec.reclass_date,
                              rec.item,
                              rec.to_dept,
                              rec.to_class,
                              rec.to_subclass
                from table(cast(I_reclass_tbl as obj_rcls_cost_event_tbl)) rec
               where rec.reclass_no = L_distinct_reclass_no);

BEGIN
   if I_reclass_tbl is NOT NULL and I_reclass_tbl.COUNT > 0 then
      ---
      for rec in C_DISTINCT_RECLASS
      loop
         --
         L_distinct_reclass_no := rec.reclass_no;
         --
         if RECLASS_SQL.RECLASS_ITEM_EXIST(O_error_message,
                                           L_exists,
                                           L_distinct_reclass_no) = FALSE then
            return FALSE;
         end if;
         -- If all reclass items are deleted then delete reclass head.
         if L_exists = FALSE then
            delete from reclass_head_tl rh
             where rh.reclass_no = L_distinct_reclass_no;

            delete from reclass_head rh
             where rh.reclass_no = L_distinct_reclass_no;
         end if;
         ---

         open C_GET_RECLASS;
         fetch C_GET_RECLASS bulk collect into L_reclass_tbl;
         close C_GET_RECLASS;
         ---
         if L_reclass_tbl is NOT NULL and L_reclass_tbl.COUNT > 0 then
            L_reclass_action := FUTURE_COST_EVENT_SQL.REMOVE_EVENT;
            -- Invoke the future cost engine.
            if FUTURE_COST_EVENT_SQL.ADD_RECLASS_EVENT(O_error_message,
                                                       L_cost_event_process_id,
                                                       L_reclass_action,
                                                       L_reclass_tbl,
                                                       L_username) = FALSE then
               L_reclass_tbl.DELETE;
               return FALSE;
            end if;
            ---
            L_reclass_tbl.DELETE;
         end if;
         ---
      end loop;
      ---
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_FC_FOR_RECLASS;
---------------------------------------------------------------------------------------------
FUNCTION GET_RECLASS_DETAIL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_reclass_delete_tbl   IN OUT   OBJ_RCLS_COST_EVENT_TBL,
                            I_reclass_no           IN       RECLASS_HEAD.RECLASS_NO%TYPE,
                            I_reclass_dtl_tbl      IN       OBJ_RECLASS_ITEM_TBL,
                            I_reclass_ind          IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'RECLASS_SQL.GET_RECLASS_DETAIL';

   cursor C_GET_RECLASS_DETAIL_1 is
      select rh.reclass_no,
             rh.reclass_date,
             ri.item,
             rh.to_dept,
             rh.to_class,
             rh.to_subclass
       from reclass_head rh,
            reclass_item ri
      where rh.reclass_no =  ri.reclass_no
        and rh.reclass_no =  I_reclass_no;

   cursor C_GET_RECLASS_DETAIL_2 is
      select rec.reclass_no,
             rh.reclass_date,
             rec.item,
             rh.to_dept,
             rh.to_class,
             rh.to_subclass
       from reclass_head rh,
            table(cast(I_reclass_dtl_tbl as OBJ_RECLASS_ITEM_TBL)) rec
      where rh.reclass_no =  rec.reclass_no;

   TYPE reclass_detl_1_tbl is table of C_GET_RECLASS_DETAIL_1%ROWTYPE index by binary_integer;
   TYPE reclass_detl_2_tbl is table of C_GET_RECLASS_DETAIL_2%ROWTYPE index by binary_integer;

   L_reclass_detl_1      reclass_detl_1_tbl;
   L_reclass_detl_2      reclass_detl_2_tbl;

BEGIN
   if I_reclass_ind = 'H' then
      open C_GET_RECLASS_DETAIL_1;
      fetch C_GET_RECLASS_DETAIL_1 bulk collect into L_reclass_detl_1;
      close C_GET_RECLASS_DETAIL_1;
   elsif I_reclass_ind = 'D' then
      open C_GET_RECLASS_DETAIL_2;
      fetch C_GET_RECLASS_DETAIL_2 bulk collect into L_reclass_detl_2;
      close C_GET_RECLASS_DETAIL_2;
   end if;

   if O_reclass_delete_tbl is NULL then
      O_reclass_delete_tbl := new OBJ_RCLS_COST_EVENT_TBL();
      O_reclass_delete_tbl.EXTEND;
   else
      O_reclass_delete_tbl.EXTEND;
   end if;

   if I_reclass_ind = 'H' then
      if L_reclass_detl_1.COUNT > 0 then
         for i in L_reclass_detl_1.FIRST..L_reclass_detl_1.LAST loop
            O_reclass_delete_tbl(i) := OBJ_RCLS_COST_EVENT_REC(L_reclass_detl_1(i).reclass_no,
                                                               L_reclass_detl_1(i).reclass_date,
                                                               L_reclass_detl_1(i).item,
                                                               L_reclass_detl_1(i).to_dept,
                                                               L_reclass_detl_1(i).to_class,
                                                               L_reclass_detl_1(i).to_subclass );
            if i != L_reclass_detl_1.LAST then
               O_reclass_delete_tbl.EXTEND;
            end if;
         end loop;
      end if;
   elsif I_reclass_ind = 'D' then
      if L_reclass_detl_2.COUNT > 0 then
         for i in L_reclass_detl_2.First..L_reclass_detl_2.Last loop
            O_reclass_delete_tbl(i) := OBJ_RCLS_COST_EVENT_REC(L_reclass_detl_2(i).reclass_no,
                                                               L_reclass_detl_2(i).reclass_date,
                                                               L_reclass_detl_2(i).item,
                                                               L_reclass_detl_2(i).to_dept,
                                                               L_reclass_detl_2(i).to_class,
                                                               L_reclass_detl_2(i).to_subclass );
            if i != L_reclass_detl_2.LAST then
               O_reclass_delete_tbl.EXTEND;
            end if;
         end loop;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RECLASS_DETAIL;

---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_LOC_DTL_EXPLD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
 
  RETURN BOOLEAN IS
             
   cursor C_DIL_ROWID is
      select dil.rowid d_rowid,
             dil.item item,
             g1.division new_division,
             d1.group_no new_group_no,
             rit.new_dept new_dept,
             rit.new_class new_class,
             rit.new_subclass new_subclass
        from deal_itemloc_item dil,
             reclass_item_temp rit,
             deps d1,
             groups g1,
             deps d2,
             groups g2
       where dil.item = rit.item
         and dil.dept = rit.old_dept
         and dil.class = rit.old_class
         and dil.subclass = rit.old_subclass
         and d1.dept     = rit.new_dept
         and d1.group_no = g1.group_no
         and d2.dept     = rit.old_dept
         and d2.group_no = g2.group_no;

   cursor C_DILE_ROWID_UPD is
     select dile.rowid d_rowid,
            (ri.reclass_date-1) reclass_date
       from reclass_item_temp ri,            
            deps   d1,
            groups g1,
            deps d2,
            groups g2,
            deal_itemloc_div_grp dil,            
            deal_head dh,
            deal_item_loc_explode dile
      where d1.dept     = ri.old_dept
        and d1.group_no = g1.group_no
        and d2.dept = ri.new_dept
        and d2.group_no = g2.group_no
        and dil.deal_id = dile.deal_id
        and dil.deal_id = dh.deal_id
        and dh.status = 'A'
        and dil.excl_ind = 'N'
        and ri.item = dile.item        
        and dile.close_date > ri.reclass_date
        and (
             (dil.merch_level =2        
                and dil.division = g1.division
                and g1.division <> g2.division)                       
              or 
              (dil.merch_level = 3
              and  dil.group_no = g1.group_no
              and  dil.division = g1.division
              and  ((g1.group_no||'-'||g1.division) <> 
                   (g2.group_no ||'-'||g2.division)))
            )
               
     union                                             
   select dile.rowid d_rowid,
            (ri.reclass_date-1) reclass_date
       from reclass_item_temp ri,            
            deps   d1,
            groups g1,
            deps d2,
            groups g2,
            deal_itemloc_dcs dil,            
            deal_head dh,
            deal_item_loc_explode dile
      where d1.dept     = ri.old_dept
        and d1.group_no = g1.group_no
        and d2.dept = ri.new_dept
        and d2.group_no = g2.group_no
        and dil.deal_id = dile.deal_id
        and dil.deal_id = dh.deal_id
        and dh.status = 'A'
        and dil.excl_ind = 'N'
        and ri.item = dile.item        
        and dile.close_date > ri.reclass_date
        and (           
              (dil.merch_level =4
               and  dil.group_no = g1.group_no
               and  dil.division = g1.division
               and dil.dept = ri.old_dept
               and dile.setup_dept = ri.old_dept
               and (ri.old_dept <> ri.new_dept))
               or 
               (dil.merch_level = 5
               and  dil.group_no = g1.group_no
          and  dil.division = g1.division
               and dil.dept = ri.old_dept
               and dile.setup_class = ri.old_class
               and dil.class = ri.old_class
               and ((ri.old_class ||'-'||ri.old_dept) <> (ri.new_class||'-'|| ri.new_dept)))
               or 
               (dil.merch_level = 6
               and  dil.group_no = g1.group_no
          and  dil.division = g1.division               
               and dil.dept= ri.old_dept
               and dil.class= ri.old_class
               and dile.setup_subclass = ri.old_subclass
               and dil.subclass = ri.old_subclass
               and ((ri.old_dept ||'-'||ri.old_class ||'-'||ri.old_subclass) <> (ri.new_dept ||'-'||ri.new_class||'-'||ri.new_subclass))))    
      union               
     select dile.rowid d_rowid,
            (ri.reclass_date-1) reclass_date
       from reclass_item_temp ri,            
            deps   d1,
            groups g1,
            deps d2,
            groups g2,
            deal_itemloc_item dil,            
            deal_head dh,
            deal_item_loc_explode dile
      where d1.dept     = ri.old_dept
        and d1.group_no = g1.group_no
        and d2.dept = ri.new_dept
        and d2.group_no = g2.group_no
        and dil.deal_id = dile.deal_id
        and dil.deal_id = dh.deal_id
        and dh.status = 'A'
        and dil.excl_ind = 'N'
        and ri.item = dile.item        
        and dile.close_date > ri.reclass_date
        and (           
               (dil.merch_level = 6
               and  dil.group_no = g1.group_no
          and  dil.division = g1.division               
               and dil.dept= ri.old_dept
               and dil.class= ri.old_class
               and dil.subclass = ri.old_subclass
               and ((ri.old_dept ||'-'||ri.old_class ||'-'||ri.old_subclass) <> (ri.new_dept ||'-'||ri.new_class||'-'||ri.new_subclass))));                     


   TYPE reclass_dil_rowid_tbl  is table of c_dil_rowid%ROWTYPE index by binary_integer;
   TYPE reclass_deal_rowid_tbl is table of c_dile_rowid_upd%ROWTYPE index by binary_integer;

   L_reclass_dil        reclass_dil_rowid_tbl;
   L_reclass_deal_upd   reclass_deal_rowid_tbl;
     
   L_program            VARCHAR2(64)   := 'RECLASS_SQL.UPDATE_ITEM_LOC_DTL_EXPLD';
           

BEGIN
  
   /* Update deal_item_loc_explode */
   SQL_LIB.SET_MARK('OPEN', 'C_DILE_ROWID_UPD', 'DEAL_ITEMLOC,DEAL_ITEM_LOC_EXPLODE', NULL);
   open C_DILE_ROWID_UPD;

   SQL_LIB.SET_MARK('FETCH', 'C_DILE_ROWID_UPD', 'DEAL_ITEMLOC,DEAL_ITEM_LOC_EXPLODE', NULL);
   fetch C_DILE_ROWID_UPD bulk collect into L_reclass_deal_upd;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_DILE_ROWID_UPD', 'DEAL_ITEMLOC,DEAL_ITEM_LOC_EXPLODE', NULL);   
   close C_DILE_ROWID_UPD;

   SQL_LIB.SET_MARK('UPDATE', 'NULL', 'DEAL_ITEM_LOC_EXPLODE', NULL);
   if L_reclass_deal_upd is not NULL and L_reclass_deal_upd.count > 0 then
        FORALL i in L_reclass_deal_upd.FIRST..L_reclass_deal_upd.LAST
           UPDATE deal_item_loc_explode
              SET close_date = L_reclass_deal_upd(i).reclass_date
            WHERE ROWID      = L_reclass_deal_upd(i).d_rowid;
   end if;

   /* update deal_itemloc */
   SQL_LIB.SET_MARK('OPEN', 'C_DIL_ROWID', 'DEAL_ITEMLOC,DEAL_ITEM_LOC_EXPLODE', NULL);
   open C_DIL_ROWID;
   SQL_LIB.SET_MARK('FETCH', 'C_DIL_ROWID', 'DEAL_ITEMLOC,DEAL_ITEM_LOC_EXPLODE', NULL);
   fetch C_DIL_ROWID bulk collect into L_reclass_dil;
   SQL_LIB.SET_MARK('CLOSE', 'C_DIL_ROWID', 'DEAL_ITEMLOC,DEAL_ITEM_LOC_EXPLODE', NULL);
   close C_DIL_ROWID;

   SQL_LIB.SET_MARK('UPDATE', 'NULL', 'DEAL_ITEMLOC_ITEM', NULL);
   if L_reclass_dil is not NULL and L_reclass_dil.count > 0 then
        FORALL i in L_reclass_dil.FIRST..L_reclass_dil.LAST
           UPDATE deal_itemloc_item 
              SET division = L_reclass_dil(i).new_division,
                  group_no = L_reclass_dil(i).new_group_no,
                      dept = L_reclass_dil(i).new_dept,
                     class = L_reclass_dil(i).new_class,
                  subclass = L_reclass_dil(i).new_subclass
            WHERE rowid = L_reclass_dil(i).d_rowid;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_ITEM_LOC_DTL_EXPLD;
---------------------------------------------------------------------------------------------
FUNCTION LOCK_RECLASS_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_reclass_no      IN       RECLASS_HEAD.RECLASS_NO%TYPE )
   RETURN BOOLEAN IS
   L_table        VARCHAR2(30)  := 'RECLASS_SKU';
   Record_Locked  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);
   L_program      VARCHAR2(64)   := 'RECLASS_SQL.LOCK_RECLASS_ITEM';
   ---
   cursor C_LOCK_RECLASS_SKU is
      select 'x'
        from reclass_item
       where reclass_no = I_reclass_no
         for update of reclass_no nowait;
BEGIN
   -- Lock any detail records related to the chosen
   -- reclassification item
   open  C_LOCK_RECLASS_SKU;
   close C_LOCK_RECLASS_SKU;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
   if C_LOCK_RECLASS_SKU%ISOPEN then
      close C_LOCK_RECLASS_SKU;
   end if; 
   O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                         L_table,
                                         I_reclass_no,
                                         ' '); 
   return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END LOCK_RECLASS_ITEM;
---------------------------------------------------------------------------------------------
END;
/
