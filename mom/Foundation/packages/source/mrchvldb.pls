
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY MERCH_VALIDATE_SQL AS
-----------------------------------------------------------------
FUNCTION DIVISION_EXIST (O_error_message   IN OUT   VARCHAR2,
                         O_exists          IN OUT   BOOLEAN,
                         I_division        IN       DIVISION.DIVISION%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'MERCH_VALIDATE_SQL.DIVISION_EXIST';
   
   L_ind   VARCHAR2(1);

   cursor C_DIV_EXISTS is
      select 'x'
        from division
       where division = I_division;

BEGIN
   if I_division is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_division',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_DIV_EXISTS',
                    'DIVISION',
                    'DIVISION: '||TO_CHAR(I_division));

   open C_DIV_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_DIV_EXISTS',
                    'DIVISION',
                    'DIVISION: '||TO_CHAR(I_division));

   fetch C_DIV_EXISTS into L_ind;

   if C_DIV_EXISTS%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_DIV_EXISTS',
                    'DIVISION',
                    'DIVISION: '||TO_CHAR(I_division));

   close C_DIV_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END DIVISION_EXIST;
-----------------------------------------------------------------------------
FUNCTION GROUP_EXIST (O_error_message   IN OUT   VARCHAR2,
                      O_exists          IN OUT   BOOLEAN,
                      I_group           IN       GROUPS.GROUP_NO%TYPE)
   RETURN BOOLEAN IS
   
   L_program   VARCHAR2(50) := 'MERCH_VALIDATE_SQL.GROUP_EXIST';

   L_ind VARCHAR2(1);

   cursor C_GROUP_EXISTS is
   select 'x'
     from groups
    where group_no = I_group;

BEGIN
   if I_group is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_group',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GROUP_EXISTS',
                    'GROUPS',
                    'GROUP NUMBER: '||TO_CHAR(I_group));

   open C_GROUP_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GROUP_EXISTS',
                    'GROUPS',
                    'GROUP NUMBER: '||TO_CHAR(I_group));

   fetch C_GROUP_EXISTS into L_ind;

   if C_GROUP_EXISTS%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GROUP_EXISTS',
                    'GROUPS',
                    'GROUP NUMBER: '||TO_CHAR(I_group));
   close C_GROUP_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
   return FALSE;

END GROUP_EXIST;
-----------------------------------------------------------------------------
FUNCTION GROUP_IN_DIVISION (O_error_message   IN OUT   VARCHAR2,
                            O_exists          IN OUT   BOOLEAN,
                            I_division        IN       GROUPS.DIVISION%TYPE,
                            I_group           IN       GROUPS.GROUP_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'MERCH_VALIDATE_SQL.GROUP_IN_DIVISION';
   
   L_ind VARCHAR2(1);
   
   cursor C_GROUP_VALID_HIER is
   select 'x'
     from groups
    where group_no = I_group
      and division = I_division;

BEGIN
   if I_group is NULL or I_division is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_group and/or I_division',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GROUP_VALID_HIER',
                    'GROUPS',
                    ' GROUP NUMBER: '||TO_CHAR(I_group)||
                    ' DIVISION: '||TO_CHAR(I_division));

   open C_GROUP_VALID_HIER;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GROUP_VALID_HIER',
                    'GROUPS',
                    ' GROUP NUMBER: '||TO_CHAR(I_group)||
                    ' DIVISION: '||TO_CHAR(I_division));

   fetch C_GROUP_VALID_HIER into L_ind;

   if C_GROUP_VALID_HIER%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GROUP_VALID_HIER',
                    'GROUPS',
                    ' GROUP NUMBER: '||TO_CHAR(I_group)||
                    ' DIVISION: '||TO_CHAR(I_division));

   close C_GROUP_VALID_HIER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END GROUP_IN_DIVISION;
-----------------------------------------------------------------------------
FUNCTION EXIST(O_error_message         IN OUT   VARCHAR2,
               O_exists                IN OUT   BOOLEAN,
               I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
               I_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
               I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
               I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE,
               I_effective_date        IN       PEND_MERCH_HIER.EFFECTIVE_DATE%TYPE)
   RETURN BOOLEAN IS
   
   L_program   VARCHAR2(50) := 'MERCH_VALIDATE_SQL.EXIST';

   L_exist   VARCHAR2(1)    := NULL;

   -- used for retrieving the correct record
   L_hier_id	        PEND_MERCH_HIER.MERCH_HIER_ID%TYPE;
   L_hier_parent_id       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE;
   L_hier_grandparent_id  PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE;
   
   cursor C_EXIST is
      select 'x'
        from V_MERCH_HIER
       where hier_type = I_hier_type
         and hierarchy_id = L_hier_id
         and NVL(parent_hierarchy_id, -999) = 
             NVL(L_hier_parent_id, NVL(parent_hierarchy_id, -999))
         and NVL(grandparent_hierarchy_id, -999) = 
             NVL(L_hier_grandparent_id, NVL(grandparent_hierarchy_id, -999))
         and effective_date <= NVL(I_effective_date, effective_date);

BEGIN

   --CHECK FOR REQUIRED FIELDS
   if I_hier_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_hier_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_hier_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_hier_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_hier_type not in (MERCH_RECLASS_VALIDATE_SQL.LP_div_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_grp_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_dept_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_cls_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_scls_code) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_MERCH_LEVEL',
                                            I_hier_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_cls_code or
      I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
      if I_hier_parent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_hier_parent_id',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
   end if;

   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
      if I_hier_grandparent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_hier_grandparent_id',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
   end if;

   -- To determine which record retrieve, parent id should only be used 
   -- for class and subclass, grandparent id should only be used for subclass. 
   if not MERCH_RECLASS_SQL.DETERMINE_HIERARCHY(O_error_message,
                                                L_hier_id,
                                                L_hier_parent_id,
                                                L_hier_grandparent_id,
                                                I_hier_type,
                                                I_hier_id,
                                                I_hier_parent_id,
                                                I_hier_grandparent_id) then
         return FALSE;
   end if;

   -- perform query
   SQL_LIB.SET_MARK('OPEN',
                    'C_EXIST',
                    'V_MERCH_HIER',
                    ' hierarchy_id: '||TO_CHAR(L_hier_id)||
                    ' parent_hierarchy_id: '||TO_CHAR(L_hier_parent_id)||
                    ' grandparent_hierarchy_id: '||TO_CHAR(L_hier_grandparent_id));
   open C_EXIST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_EXIST',
                    'V_MERCH_HIER',
                    ' hierarchy_id: '||TO_CHAR(L_hier_id)||
                    ' parent_hierarchy_id: '||TO_CHAR(L_hier_parent_id)||
                    ' grandparent_hierarchy_id: '||TO_CHAR(L_hier_grandparent_id));
   fetch C_EXIST into L_exist;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXIST',
                    'V_MERCH_HIER',
                    ' hierarchy_id: '||TO_CHAR(L_hier_id)||
                    ' parent_hierarchy_id: '||TO_CHAR(L_hier_parent_id)||
                    ' grandparent_hierarchy_id: '||TO_CHAR(L_hier_grandparent_id));
   close C_EXIST;
   
   if L_exist is not NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;      
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END EXIST;
-----------------------------------------------------------------------------
FUNCTION PARENT_EXIST(O_error_message         IN OUT   VARCHAR2,
                      O_exists                IN OUT   BOOLEAN,
                      I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                      I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                      I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE,
                      I_effective_date        IN       PEND_MERCH_HIER.EFFECTIVE_DATE%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'MERCH_VALIDATE_SQL.PARENT_EXIST';
      
   L_hier_value         PEND_MERCH_HIER.MERCH_HIER_ID%TYPE          := I_hier_parent_id;
   L_hier_parent_value  PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE   := NULL;
   L_parent_hier_type   PEND_MERCH_HIER.HIER_TYPE%TYPE;

BEGIN

   if I_hier_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_hier_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_hier_type not in (MERCH_RECLASS_VALIDATE_SQL.LP_grp_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_dept_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_cls_code,
                          MERCH_RECLASS_VALIDATE_SQL.LP_scls_code) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_MERCH_LEVEL',
                                            I_hier_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_hier_parent_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_hier_parent_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   
   -- set parent to be hier value
   L_hier_value := I_hier_parent_id;
   
   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
      if I_hier_grandparent_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_hier_grandparent_id',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;

      -- set grandparent to be parent
      L_hier_parent_value  := I_hier_grandparent_id;
   end if;

   if I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_grp_code then
      L_parent_hier_type := MERCH_RECLASS_VALIDATE_SQL.LP_div_code;
   elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_dept_code then
      L_parent_hier_type := MERCH_RECLASS_VALIDATE_SQL.LP_grp_code;
   elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_cls_code then
      L_parent_hier_type := MERCH_RECLASS_VALIDATE_SQL.LP_dept_code;
   elsif I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code then
      L_parent_hier_type := MERCH_RECLASS_VALIDATE_SQL.LP_cls_code;
   end if;

   if not MERCH_VALIDATE_SQL.EXIST(O_error_message,
                                   O_exists,
                                   L_parent_hier_type,
                                   L_hier_value,
                                   L_hier_parent_value,
                                   NULL,
                                   I_effective_date) then      
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END PARENT_EXIST;
----------------------------------------------------------
FUNCTION COMPANY_EXIST(O_error_message   IN OUT   VARCHAR2,
                       O_exists          IN OUT   BOOLEAN,
                       I_company         IN       COMPHEAD.COMPANY%TYPE)

   RETURN BOOLEAN IS
   
   L_program   VARCHAR2(50) := 'MERCH_VALIDATE_SQL.COMPANY_EXIST';
   
   L_exist VARCHAR2(1);

   cursor C_COMPANY_EXIST is
      select 'x'
        from COMPHEAD
       where company = I_company;

BEGIN
   if I_company is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_company',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_COMPANY_EXIST',
                    'COMPHEAD',
                    'Company: '||TO_CHAR(I_company));
   open C_COMPANY_EXIST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_COMPANY_EXIST',
                    'COMPHEAD',
                    'Company: '||TO_CHAR(I_company));
   fetch C_COMPANY_EXIST into L_exist;

   if C_COMPANY_EXIST%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_COMPANY_EXIST',
                    'COMPHEAD',
                    'Company: '||TO_CHAR(I_company));
   close C_COMPANY_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END COMPANY_EXIST;
----------------------------------------------------------
END MERCH_VALIDATE_SQL;
/
