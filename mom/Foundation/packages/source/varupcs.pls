
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE VAR_UPC_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
-- Function: GET_FORMAT_INFO
--  Purpose: Returns info for given format_id.
-------------------------------------------------------------------------------------------
FUNCTION GET_FORMAT_INFO(O_error_message     IN OUT  VARCHAR2,
                         O_valid             IN OUT  BOOLEAN,
                         O_format_desc       IN OUT  VAR_UPC_EAN.FORMAT_DESC%TYPE,
                         O_prefix_length     IN OUT  VAR_UPC_EAN.PREFIX_LENGTH%TYPE,
                         O_begin_item_digit  IN OUT  VAR_UPC_EAN.BEGIN_ITEM_DIGIT%TYPE,
                         O_begin_var_digit   IN OUT  VAR_UPC_EAN.BEGIN_VAR_DIGIT%TYPE,
                         O_check_digit       IN OUT  VAR_UPC_EAN.CHECK_DIGIT%TYPE,
                         O_default_prefix    IN OUT  VAR_UPC_EAN.DEFAULT_PREFIX%TYPE,
                         I_format_id         IN OUT  VAR_UPC_EAN.FORMAT_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
END VAR_UPC_SQL;
/


