create or replace PACKAGE CORESVC_USER_ATTRIB AUTHID CURRENT_USER AS
   template_key                         CONSTANT VARCHAR2(255)         :='USER_ATTRIB_DATA';
   action_new                                    VARCHAR2(25)          := 'NEW';
   action_mod                                    VARCHAR2(25)          := 'MOD';
   action_del                                    VARCHAR2(25)          := 'DEL';
   USER_ATTRIB_sheet                             VARCHAR2(255)         := 'USER_ATTRIB';
   USER_ATTRIB$Action                            NUMBER                :=1;
   USER_ATTRIB$USER_ID                           NUMBER                :=2;
   USER_ATTRIB$USER_NAME                         NUMBER                :=3;
   USER_ATTRIB$LANG                              NUMBER                :=4;
   USER_ATTRIB$STORE_DEFAULT                     NUMBER                :=5;
   USER_ATTRIB$USER_PHONE                        NUMBER                :=6;
   USER_ATTRIB$USER_FAX                          NUMBER                :=7;
   USER_ATTRIB$USER_PAGER                        NUMBER                :=8;
   USER_ATTRIB$USER_EMAIL                        NUMBER                :=9;
   USER_ATTRIB$DEFAULT_PRINTER                   NUMBER                :=10;
   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255) := 'ACTION';
   template_category CODE_DETAIL.CODE%TYPE := 'RMSADM';
   TYPE USER_ATTRIB_rec_tab IS TABLE OF USER_ATTRIB%ROWTYPE;
-------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------   
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER
                        )
   RETURN BOOLEAN;
--------------------------------------------------------------------------   
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER
                    )
   RETURN BOOLEAN;
---------------------------------------------------------------------------   
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
---------------------------------------------------------------------------
END CORESVC_USER_ATTRIB;
/