CREATE OR REPLACE PACKAGE ITEM_HTS_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------
-- Function Name: ITEM_HTS_EXIST
-- Purpose      : Checks to see if a given Item exists on the
--                Item HTS table.
--------------------------------------------------------------------------------------
FUNCTION ITEM_HTS_EXIST(O_error_message IN OUT VARCHAR2,
                        O_exists        IN OUT BOOLEAN,
                        I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: HTS_EXIST
-- Purpose      : Validates the given hts/item record exists.
--------------------------------------------------------------------------------------
FUNCTION HTS_EXIST(O_error_message     IN OUT VARCHAR2,
                   O_exists            IN OUT BOOLEAN,
                   I_item              IN     ITEM_MASTER.ITEM%TYPE,
                   I_hts               IN     HTS.HTS%TYPE,
                   I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                   I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                   I_effect_from       IN     HTS.EFFECT_FROM%TYPE,
                   I_effect_to         IN     HTS.EFFECT_TO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: ASSESS_EXIST
-- Purpose      : Validates the given hts/item/assessment record exists.
--------------------------------------------------------------------------------------
FUNCTION ASSESS_EXIST(O_error_message     IN OUT VARCHAR2,
                      O_exists            IN OUT BOOLEAN,
                      I_item              IN     ITEM_MASTER.ITEM%TYPE,
                      I_hts               IN     HTS.HTS%TYPE,
                      I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                      I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                      I_effect_from       IN     HTS.EFFECT_FROM%TYPE,
                      I_effect_to         IN     HTS.EFFECT_TO%TYPE,
                      I_comp_id           IN     ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: DELETE_ASSESS
-- Purpose      : Deletes the hts/item/assessment record.
--------------------------------------------------------------------------------------
FUNCTION DELETE_ASSESS(O_error_message     IN OUT VARCHAR2,
                       I_item              IN     ITEM_MASTER.ITEM%TYPE,
                       I_hts               IN     HTS.HTS%TYPE,
                       I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                       I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                       I_effect_from       IN     HTS.EFFECT_FROM%TYPE,
                       I_effect_to         IN     HTS.EFFECT_TO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: GET_QUANTITIES
-- Purpose      : Calculates the quantities based on the UOM associated
--                with each quantity.
--------------------------------------------------------------------------------------
FUNCTION GET_QUANTITIES(O_error_message     IN OUT VARCHAR2,
                        O_qty_1             IN OUT NUMBER,
                        O_qty_2             IN OUT NUMBER,
                        O_qty_3             IN OUT NUMBER,
                        I_item              IN     ITEM_MASTER.ITEM%TYPE,
                        I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                        I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                        I_units_1           IN     HTS.UNITS_1%TYPE,
                        I_units_2           IN     HTS.UNITS_2%TYPE,
                        I_units_3           IN     HTS.UNITS_3%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: GET_HTS_DETAILS
-- Purpose      : Retrieves the details of the given hts/item record.
--------------------------------------------------------------------------------------
FUNCTION GET_HTS_DETAILS (O_error_message       IN OUT   VARCHAR2,
                          O_tariff_treatment    IN OUT   HTS_TARIFF_TREATMENT.TARIFF_TREATMENT%TYPE,
                          O_qty_1               IN OUT   NUMBER,
                          O_qty_2               IN OUT   NUMBER,
                          O_qty_3               IN OUT   NUMBER,
                          O_units_1             IN OUT   HTS.UNITS_1%TYPE,
                          O_units_2             IN OUT   HTS.UNITS_2%TYPE,
                          O_units_3             IN OUT   HTS.UNITS_3%TYPE,
                          O_specific_rate       IN OUT   HTS_TARIFF_TREATMENT.SPECIFIC_RATE%TYPE,
                          O_av_rate             IN OUT   HTS_TARIFF_TREATMENT.AV_RATE%TYPE,
                          O_other_rate          IN OUT   HTS_TARIFF_TREATMENT.OTHER_RATE%TYPE,
                          O_cvd_case_no         IN OUT   HTS_CVD.CASE_NO%TYPE,
                          O_ad_case_no          IN OUT   HTS_AD.CASE_NO%TYPE,
                          O_duty_comp_code      IN OUT   HTS.DUTY_COMP_CODE%TYPE,
                          I_item                IN       ITEM_MASTER.ITEM%TYPE,
                          I_supplier            IN       SUPS.SUPPLIER%TYPE,
                          I_hts                 IN       HTS.HTS%TYPE,
                          I_import_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                          I_origin_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                          I_manu_country_id     IN       COUNTRY.COUNTRY_ID%TYPE,
                          I_effect_from         IN       HTS.EFFECT_FROM%TYPE,
                          I_effect_to           IN       HTS.EFFECT_TO%TYPE,
                          I_clearing_zone_id    IN       ORDHEAD.CLEARING_ZONE_ID%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name : DEFAULT_ASSESS
-- Purpose       : This function inserts assessment records into the Item HTS Assessment 
--                 table for HTS codes attached to Items.
-----------------------------------------------------------------------------------------
FUNCTION DEFAULT_ASSESS (O_error_message        IN OUT VARCHAR2,
                         I_item                 IN     ITEM_MASTER.ITEM%TYPE,
                         I_hts                  IN     HTS.HTS%TYPE,
                         I_import_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                         I_origin_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                         I_effect_from          IN     HTS.EFFECT_FROM%TYPE,
                         I_effect_to            IN     HTS.EFFECT_TO%TYPE)
RETURN BOOLEAN; 
--------------------------------------------------------------------------------------
-- Function Name : DEFAULT_CALC_ASSESS
-- Purpose       : This function inserts assessment records into the Item HTS Assessment 
--                 table for HTS codes attached to Items and then calculates the values
--                 of the assessments that were inserted.
-----------------------------------------------------------------------------------------
FUNCTION DEFAULT_CALC_ASSESS (O_error_message        IN OUT VARCHAR2,
                              I_item                 IN     ITEM_MASTER.ITEM%TYPE,
                              I_hts                  IN     HTS.HTS%TYPE,
                              I_import_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                              I_origin_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                              I_effect_from          IN     HTS.EFFECT_FROM%TYPE,
                              I_effect_to            IN     HTS.EFFECT_TO%TYPE)
RETURN BOOLEAN; 
--------------------------------------------------------------------------------------
-- Function Name : COPY_DOWN_PARENT_HTS
-- Purpose       : Remove old ITEM_HTS and ITEM_HTS_ASSESS values of all tran level
--               : or above children or grandchildren of passed the passed in item,
--               : get latest values for the item, and insert new ITEM_HTS and 
--               : ITEM_HTS_ASSESS values for the valid children and grandchildren.
-----------------------------------------------------------------------------------------
FUNCTION COPY_DOWN_PARENT_HTS (O_error_message  IN OUT VARCHAR2,
                               O_warn_flag      IN OUT VARCHAR2,
                               I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN; 
--------------------------------------------------------------------------------------
-- Function Name: CHECK_ITEM_HTS_MANU_CTRY
-- Purpose      : Checks to see if a given Item/Country of Manufacture exists on the
--                Item HTS table.
--------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_HTS_MANU_CTRY(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists             IN OUT BOOLEAN,
                                  I_item               IN     ITEM_MASTER.ITEM%TYPE,
                                  I_import_country_id  IN     ITEM_HTS.IMPORT_COUNTRY_ID%TYPE,
                                  I_manu_country_id    IN     ITEM_HTS.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: GET_ITEM_CLEAR_ZONE
-- Purpose      : Retrives the Clearing Zone for the input from ITEM_HTS table.
--------------------------------------------------------------------------------------
FUNCTION GET_ITEM_CLEAR_ZONE (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists             IN OUT   BOOLEAN,
                              O_clearing_zone_id   IN OUT   ITEM_HTS.CLEARING_ZONE_ID%TYPE,
                              I_item               IN       ITEM_MASTER.ITEM%TYPE,
                              I_import_country_id  IN       ITEM_HTS.IMPORT_COUNTRY_ID%TYPE,
                              I_origin_country_id  IN       ITEM_HTS.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: UPDATE_ITEM_CLEAR_ZONE
-- Purpose      : Updates the clearing zone for in Item_HTS table for all records
--                Item/Origin Country/Import Country except the one with input HTS
--------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_CLEAR_ZONE (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item               IN       ITEM_MASTER.ITEM%TYPE,
                                 I_import_country_id  IN       ITEM_HTS.IMPORT_COUNTRY_ID%TYPE,
                                 I_origin_country_id  IN       ITEM_HTS.ORIGIN_COUNTRY_ID%TYPE,
                                 I_clearing_zone_id   IN       ITEM_HTS.CLEARING_ZONE_ID%TYPE,
                                 I_hts                IN       HTS.HTS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name : INS_ITEM_HTS
-- Purpose       : Retains the records of ITEM_HTS and ITEM_HTS_ASSESS table
--               : for the old date range
-----------------------------------------------------------------------------------------
FUNCTION INS_ITEM_HTS (O_error_message IN OUT rtk_errors.rtk_text%TYPE,
                       I_tariff_no ITEM_HTS.HTS%TYPE,
                       I_country_id ITEM_HTS.IMPORT_COUNTRY_ID%TYPE,
                       I_effect_from DATE,
                       I_effect_to DATE,
                       I_new_effect_to DATE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name : GET_RATE
-- Purpose       : Get the rates for the specific duty components.
-----------------------------------------------------------------------------------------
FUNCTION GET_RATE (O_error_message       IN OUT rtk_errors.rtk_text%TYPE,
                   O_specific_flag       IN OUT VARCHAR2,
                   O_av_flag             IN OUT VARCHAR2,
                   O_other_flag          IN OUT VARCHAR2,
                   I_duty_comp_code      IN OUT   HTS.DUTY_COMP_CODE%TYPE,
                   I_import_country_id   IN     COUNTRY.COUNTRY_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name : COMPUTATION_CODE_CONFIGURABLE
-- Purpose       : To check if it is configurable for a specific comp code and import country.
--               : 
-----------------------------------------------------------------------------------------
FUNCTION COMPUTATION_CODE_CONFIGURABLE (O_error_message       IN OUT   rtk_errors.rtk_text%TYPE,
                                        O_exists              IN OUT   BOOLEAN,
                                        I_import_country_id   IN       ITEM_HTS.IMPORT_COUNTRY_ID%TYPE,
                                        I_duty_comp_code      IN       HTS.DUTY_COMP_CODE%TYPE)  
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
END ITEM_HTS_SQL;
/


