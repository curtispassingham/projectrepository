-- File Name : CORESVC_TICKET_TYPE_HEAD_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_TICKET_TYPE_HEAD AUTHID CURRENT_USER AS
   template_key          CONSTANT    VARCHAR2(255) :='TICKET_TYPE_DATA';
   action_new                        VARCHAR2(25)  :='NEW';
   action_mod                        VARCHAR2(25)  :='MOD';
   action_del                        VARCHAR2(25)  :='DEL';
   TICKET_TYPE_HEAD_sheet            VARCHAR2(255) :='TICKET_TYPE_HEAD';
   TICKET_TYPE_HEAD$Action           NUMBER        :=1;
   TICKET_TYPE_HEAD$TKET_ID          NUMBER        :=2;
   TICKET_TYPE_HEAD$TKET_DESC        NUMBER        :=3;
   TICKET_TYPE_HEAD$SEL_IND          NUMBER        :=4;
   TICKET_TYPE_HEAD$ORG_ID           NUMBER        :=5;
   TICKET_TYPE_HEAD$MERCH_ID         NUMBER        :=6;
   TICKET_TYPE_HEAD$MERCH_CL         NUMBER        :=7;
   TICKET_TYPE_HEAD$MERCH_SC         NUMBER        :=8;
   TICKET_TYPE_DETAIL_sheet          VARCHAR2(255) :='TICKET_TYPE_DETAIL';
   TICKET_TYPE_DETAIL$Action         NUMBER        :=1;
   TICKET_TYPE_DETAIL$TKET_ID        NUMBER        :=2;
   TICKET_TYPE_DETAIL$SEQ_NO         NUMBER        :=3;
   TICKET_TYPE_DETAIL$TKET_ITM_ID    NUMBER        :=4;
   TICKET_TYPE_DETAIL$UDA_ID         NUMBER        :=5;
   TICKET_TYPE_HEAD_TL_sheet         VARCHAR2(255) :='TICKET_TYPE_HEAD_TL';
   TICKET_TYPE_HEAD_TL$Action        NUMBER        :=1;
   TICKET_TYPE_HEAD_TL$Lang          NUMBER        :=2;
   TICKET_TYPE_HEAD_TL$TKET_ID       NUMBER        :=3;
   TICKET_TYPE_HEAD_TL$TKET_DESC     NUMBER        :=4;

   sheet_name_trans               S9T_PKG.trans_map_typ;
   action_column                  VARCHAR2(255) :='ACTION';
   template_category CODE_DETAIL.CODE%TYPE      :='RMSFND';

   TYPE TICKET_TYPE_HEAD_rec_tab   IS TABLE OF TICKET_TYPE_HEAD%ROWTYPE;
   TYPE TICKET_TYPE_DETAIL_rec_tab IS TABLE OF TICKET_TYPE_DETAIL%ROWTYPE;
-------------------------------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT s9t_folder.file_id%TYPE,
                       I_template_only_ind IN     CHAR DEFAULT 'N' )
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count   IN OUT NUMBER,
                        I_file_id       IN     s9t_folder.file_id%TYPE,
                        I_process_id    IN     NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;
-------------------------------------------------------------------------------------------------------
END CORESVC_TICKET_TYPE_HEAD;
/
