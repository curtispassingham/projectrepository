
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
----------------------------------------------------------------------------------
-- Name:    CAL_TO_454_WEEKNO
-- Purpose: This procedure determines accepts a calendar date, validates it and converts
--          it to a week number within the 454 half.
-----------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CAL_TO_454_WEEKNO 
                            (in_dd         IN     NUMBER,
                             in_mm         IN     NUMBER,
                             in_yyyy       IN     NUMBER,
                             week_no       IN OUT NUMBER,
                             return_code   IN OUT VARCHAR2,
                             error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   ABORTING EXCEPTION;

   /* Local variables */
   half_no                NUMBER(5);
   month_in_half          NUMBER;
   work_day               NUMBER(2);
   work_month             NUMBER(2);
   work_year              NUMBER(4);
   in_ddmmyyyy            NUMBER(8);
   first_doh              NUMBER(8);
   days_between           VARCHAR2(3);

BEGIN
   return_code := 'TRUE';

   VALIDATE_CAL(in_dd, in_mm, in_yyyy,
                return_code, error_message);
   IF (return_code = 'FALSE') THEN
      raise ABORTING;
   END IF;

   CAL_TO_454_HALF(in_dd, in_mm, in_yyyy,
                   half_no, month_in_half,
                   return_code, error_message);
   IF (return_code = 'FALSE') THEN
      raise ABORTING;
   END IF;

   HALF_TO_454_FDOH(half_no,
                    work_day, work_month, work_year,
                    return_code, error_message);
   IF (return_code = 'FALSE') THEN
      raise ABORTING;
   END IF;

   in_ddmmyyyy := (in_dd * 1000000) + (in_mm * 10000) + in_yyyy;
   first_doh   := (work_day * 1000000) + (work_month * 10000) + work_year;

   days_between := 
     to_date(to_char(in_ddmmyyyy, '09999999'), 'DDMMYYYY') -
     to_date(to_char(first_doh, '09999999'), 'DDMMYYYY');

   week_no := trunc(to_number(days_between) / 7) + 1;

EXCEPTION
   when ABORTING then
      NULL;
   when OTHERS then
      return_code := 'FALSE';
      error_message := SQLERRM || ' from CAL_TO_454_WEEKNO';
END CAL_TO_454_WEEKNO;
/


