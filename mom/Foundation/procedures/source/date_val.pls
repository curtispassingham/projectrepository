
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------------------------
-- Name:    DATE_VAL
-- Purpose: This procedure validates a calendar day, month and year.
--------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE DATE_VAL (in_date       IN     VARCHAR2,
                                      return_code   IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   dummy_date date;
BEGIN
   dummy_date := to_date(nvl(in_date, '999999'), 'MMDDYY');
   return_code := 'TRUE';
EXCEPTION
   WHEN OTHERS THEN
      return_code := 'FALSE';
END DATE_VAL;
/


