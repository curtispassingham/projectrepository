CREATE OR REPLACE PROCEDURE NOTIFY_TAX_EVENT(context raw,
                                             reginfo sys.aq$_reg_info,
                                             descr sys.aq$_descriptor,
                                             payload raw,
                                             payloadl number)
AUTHID CURRENT_USER AS
   L_vdate PERIOD.VDATE%TYPE  := GET_VDATE; 
   L_dequeue_options dbms_aq.dequeue_options_t;
   L_message_properties dbms_aq.message_properties_t;
   L_message_handle RAW(16);
   L_message tax_event_msg;

   L_order_no ordhead.order_no%TYPE;
   O_error_message RTK_ERRORS.RTK_TEXT%TYPE;
   L_l10n_obj L10N_OBJ := L10N_OBJ();

   cursor C_GET_ORDER_NO is
      select order_no 
        from tax_calc_event
       where tax_event_id = L_message.tax_event_id;

BEGIN

   L_dequeue_options.msgid := descr.msg_id;
   L_dequeue_options.consumer_name := descr.consumer_name;
   L_dequeue_options.visibility := DBMS_AQ.IMMEDIATE;
   
   DBMS_AQ.DEQUEUE(QUEUE_NAME => descr.queue_name,
                   DEQUEUE_OPTIONS => L_dequeue_options,
                   MESSAGE_PROPERTIES => L_message_properties,
                   PAYLOAD => L_message,
                   MSGID => L_message_handle);

   open C_GET_ORDER_NO;
   fetch C_GET_ORDER_NO into L_order_no;
   close C_GET_ORDER_NO;

   L_l10n_obj.procedure_key := 'LOAD_ORDER_TAX_OBJECT';
   L_l10n_obj.doc_type := 'PO';
   L_l10n_obj.doc_id := L_order_no;

   if L10N_SQL.EXEC_FUNCTION(O_error_message,
                             L_l10n_obj) = FALSE then
      UPDATE tax_calc_event
         SET tax_event_result = 'E',
             tax_event_error = O_error_message,
             last_update_datetime = L_vdate,
             last_update_id = USER
       WHERE tax_event_id = L_message.tax_event_id;
   else
      UPDATE tax_calc_event
         SET tax_event_result = 'C',
             last_update_datetime = L_vdate,
             last_update_id = USER
       WHERE tax_event_id = L_message.tax_event_id;
   end if;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'NOTIFY_TAX_EVENT',
                                            TO_CHAR(SQLCODE));

      UPDATE tax_calc_event
         SET tax_event_result = 'E',
             tax_event_error = O_error_message,
             last_update_datetime = L_vdate,
             last_update_id = USER
       WHERE tax_event_id = L_message.tax_event_id;
END;
/
