
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------------
-- Name:    NEXT_LOC_TRAIT
-- Purpose: This location trait number sequence generator will return to the calling 
--          program/procedure.  Upon success (TRUE) a location trait number.  
--          Upon failure (FALSE) an appropriate error message for display purposes by
--          the calling program/procedure. 
--------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE next_loc_trait (v_loc_trait     IN OUT NUMBER,
                                            return_code     IN OUT VARCHAR2,
                                            error_message   IN OUT VARCHAR2) AUTHID CURRENT_USER AS

   wrap_sequence_number   NUMBER(4);
   first_time             VARCHAR2(3) := 'Yes';
   dummy                  VARCHAR2(1);


   CURSOR c_trait_exists(v_loc_trait_param NUMBER) IS
      SELECT   'x'
         FROM  loc_traits
         WHERE loc_trait = v_loc_trait_param;

BEGIN
    LOOP
        SELECT location_trait_sequence.NEXTVAL
          INTO v_loc_trait
          FROM sys.dual;

         IF (first_time = 'Yes') THEN
            wrap_sequence_number := v_loc_trait;
            first_time := 'No';
         ELSIF (v_loc_trait = wrap_sequence_number) THEN
            error_message := 'Fatal error - no available trait numbers';
            return_code := 'FALSE';
            EXIT;
         END IF;

         OPEN  c_trait_exists(v_loc_trait);
         FETCH c_trait_exists into dummy;
         IF (c_trait_exists%notfound) THEN
             return_code := 'TRUE';
             CLOSE c_trait_exists;
             EXIT;
         END IF;
         CLOSE c_trait_exists;
    END LOOP;
EXCEPTION
    WHEN OTHERS THEN
        error_message := SQLERRM || ' from NEXT_LOC_TRAIT proc.';
        return_code := 'FALSE';
END next_loc_trait;
/


