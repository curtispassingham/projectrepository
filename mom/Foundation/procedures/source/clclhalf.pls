
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------------
-- Name:    CAL_TO_CAL_HALF
-- Purpose: This procedure validates a calendar day, month and year and then
--          converts it a Calendar Half format, along with the month in the half.
-------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CAL_TO_CAL_HALF 
                            (in_dd             IN     NUMBER,
                             in_mm             IN     NUMBER,
                             in_yyyy           IN     NUMBER,
                             half_no           IN OUT NUMBER,
                             month_in_half     IN OUT NUMBER,
                             return_code       IN OUT VARCHAR2,
                             error_message     IN OUT VARCHAR2) AUTHID CURRENT_USER AS

   /* User defined exceptions */
   ABORTING EXCEPTION;

   /* Local variables */
   L_dummy             VARCHAR2(1);
   start_of_half_month NUMBER(2);
   start_of_last_year  NUMBER;
   out_dd_454          NUMBER(1);
   out_wk_454          NUMBER(2);
   out_mm_454          NUMBER(2);
   out_yyyy_454        NUMBER(4);
   half                NUMBER(5);
   nett_addition       NUMBER(9);

BEGIN
   return_code := 'TRUE';
  
   if SYSTEM_CALENDAR_SQL.GET_CAL_OPTS ( error_message,
                                         L_dummy,
                                         start_of_half_month,
                                         start_of_last_year) = FALSE then
      return_code := 'FALSE';
      raise ABORTING;
   end if; 

   half_no := (in_yyyy -
                TRUNC((start_of_half_month - in_mm + 11) / 12)) * 10;

   IF (start_of_last_year < 0) THEN
      half_no := half_no + 10;
   END IF;

   half := TRUNC((start_of_half_month - in_mm + 11) / 6);

   IF (MOD(half, 2) = 1) THEN
      half_no := half_no + 1;
   ELSE
      half_no := half_no + 2;
   END IF;

   nett_addition := MOD((in_mm - start_of_half_month + 12), 6);
   IF (nett_addition = 6) THEN
      nett_addition := 0;
   END IF;
   month_in_half := in_mm + (nett_addition - in_mm + 1);

EXCEPTION
   WHEN ABORTING THEN
      NULL;
   WHEN OTHERS THEN
       error_message := SQLERRM || ' from CAL_TO_CAL_HALF';
       return_code := 'FALSE';
END CAL_TO_CAL_HALF;
/


