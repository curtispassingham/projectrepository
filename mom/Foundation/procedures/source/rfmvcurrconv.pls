CREATE OR REPLACE FUNCTION REFRESH_MV_CURR_CONV_RATES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   return BOOLEAN IS

   ------------------------------------------------------------------------------
   --- Function Name:  REFRESH_MV_CURR_CONV_RATES
   --- Purpose:        This Function will perform a complete refresh of the materialized view
   ---                 MV_CURRENCY_CONVERSION_RATES
------------------------------------------------------------------------------

   L_program               VARCHAR2(64) := 'MV_CURRENCY_CONVERSION_RATES';

BEGIN
   DBMS_MVIEW.REFRESH('mv_currency_conversion_rates','c');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('FUNCTION_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;

END REFRESH_MV_CURR_CONV_RATES;
/
