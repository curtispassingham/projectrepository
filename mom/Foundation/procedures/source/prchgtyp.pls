
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-- Purpose: This function takes an old and new retail
--          returns the mark type and amount:
--	11 -	MU  -	Markup
--	12 -	MUC -	Markup Cancel
--	13 -	MD  -	Markdown
--	14 -	MDC -	Markdown Cancel
--
--  Note that in some cases it can return two records.
----------------------------------------------------------
CREATE OR REPLACE PROCEDURE prcchgtyp  (I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                        I_location      IN     PRICE_HIST.LOC%TYPE,
                                        I_loc_type      IN     PRICE_HIST.LOC_TYPE%TYPE,
                                        I_new_retail    IN     PRICE_HIST.UNIT_RETAIL%TYPE,
                                        I_old_retail    IN     PRICE_HIST.UNIT_RETAIL%TYPE,
                                        O_mark_type     IN OUT NUMBER,
                                        O_mark_amt      IN OUT NUMBER,
                                        O_mark_type_2   IN OUT NUMBER,
                                        O_mark_amt_2    IN OUT NUMBER,
                                        O_return_code   IN OUT VARCHAR2, 
                                        O_error_message IN OUT VARCHAR2) AUTHID CURRENT_USER IS

   L_boh_retail    price_hist.unit_retail%TYPE := 0;
   L_fdoh_date     DATE;
   L_half_no       period.half_no%TYPE :=0;
   L_day           NUMBER(2);
   L_month         NUMBER(2);
   L_year          NUMBER(4);
   L_cal           VARCHAR2(1);
   ABORTING        EXCEPTION;

   cursor C_ORIGINAL_RETAIL is
      select unit_retail
        from price_hist
       where tran_type = 0
         and item = I_item
         and loc = I_location
         and loc_type = I_loc_type;

   cursor C_BOH_RETAIL is
      select unit_retail
        from price_hist
       where item = I_item
         and loc = I_location
         and loc_type = I_loc_type
         and tran_type = 4
         and action_date = (select max(action_date)
                              from price_hist
                             where item = I_item
                               and loc = I_location
                               and loc_type = I_loc_type    
                               and action_date <= L_fdoh_date);

   cursor C_CALENDAR is
      select 'x'
        from system_options
       where calendar_454_ind = 'C';

   cursor C_HALF is
      select half_no
        from period; 

BEGIN

   O_mark_type   := 0;
   O_mark_amt    := 0;
   O_mark_type_2 := 0;
   O_mark_amt_2  := 0;

   open C_HALF;
   fetch C_HALF into L_half_no;
   if C_HALF%NOTFOUND then
      close C_HALF;
      RAISE NO_DATA_FOUND;
   end if;
   close c_half;

   open C_CALENDAR;
   fetch C_CALENDAR into L_cal;
   if C_CALENDAR%FOUND then
      HALF_TO_CAL_FDOH(L_half_no,
                       L_day,
                       L_month,
                       L_year,
                       O_return_code,
                       O_error_message);
      if (O_return_code = 'FALSE') then
         close C_CALENDAR;
         O_error_message := 'Date conversion error from PRCCHGTYP '||SQLERRM;
         raise ABORTING;
      else
         L_fdoh_date := TO_DATE(TO_CHAR(L_day,'09')||TO_CHAR(L_month,'09')||
	    TO_CHAR(L_year,'0999'),'DDMMYYYY');
      end if;
      close c_calendar;
   else
      HALF_TO_454_FDOH(L_half_no,
                       L_day,
                       L_month,
                       L_year,
                       O_return_code,
                       O_error_message);
      if (O_return_code = 'FALSE') then
         close C_CALENDAR;
         O_error_message := 'Date conversion error from PRCCHGTYP '||SQLERRM;
         raise ABORTING;
      else
         L_fdoh_date := TO_DATE(TO_CHAR(L_day,'09')||TO_CHAR(L_month,'09')||
	    TO_CHAR(L_year,'0999'),'DDMMYYYY');
      end if;
      close C_CALENDAR;
   end if;

   open C_BOH_RETAIL;
   fetch C_BOH_RETAIL into L_boh_retail;
   if C_BOH_RETAIL%NOTFOUND then
      close C_BOH_RETAIL;
      open C_ORIGINAL_RETAIL;
      fetch C_ORIGINAL_RETAIL into L_boh_retail;
      if C_ORIGINAL_RETAIL%NOTFOUND then
         close C_ORIGINAL_RETAIL;
         RAISE NO_DATA_FOUND;
      end if;
      close C_ORIGINAL_RETAIL;
   else
     close C_BOH_RETAIL;
   end if;

   if (DETERMINE_MARK (L_boh_retail,
                       I_old_retail,
                       I_new_retail,
                       O_mark_type,
                       O_mark_amt,
                       O_mark_type_2,
                       O_mark_amt_2,
                       O_error_message)) = 'FALSE' then
      O_return_code := 'FALSE';
   else
      O_return_code := 'TRUE';
   end if;
 
EXCEPTION
   WHEN NO_DATA_FOUND then
      O_error_message := 'No record found from cursor in function PRCCHGTYP for item: '||I_item||' and location: '||TO_CHAR(I_location)||' and location type: '||I_loc_type;
      O_return_code := 'FALSE';
   WHEN OTHERS then
      O_error_message := SQLERRM || ' from function PRCCHGTYP';
      O_return_code := 'FALSE';
END PRCCHGTYP;
/
