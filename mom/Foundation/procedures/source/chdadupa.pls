
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------------------------------
-- Name:    CHKDIG_ADD_UPC_A
-- Purpose: This procedure accepts a number to perform a check digit routine
--          against, then returns the number with the check digit appended.
--------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CHKDIG_ADD_UPC_A(number_to_chk IN OUT NUMBER,
                                             check_digit   IN OUT NUMBER ) AUTHID CURRENT_USER IS

   counter     NUMBER(2)    := 0;
   char_value  VARCHAR2(11) := NULL;
   even_digits NUMBER(4)    := 0;
   odd_digits  NUMBER(4)    := 0;
   last_digit  NUMBER(4)    := 0;
BEGIN

   char_value := LPAD(TO_CHAR(number_to_chk), 11, '0');

   /* Get each of the odd placed digits, summing them together
      and then multiplying the result by 3.
   */
   counter := 1;
   WHILE (counter < 12) LOOP
      odd_digits := odd_digits + to_number(substr(char_value, counter, 1));
      counter := counter + 2;
   END LOOP;
   odd_digits := odd_digits * 3;

   /* Get each of the even placed digits, summing them together. */
   counter := 2;
   WHILE (counter < 12) LOOP
      even_digits := even_digits + to_number(substr(char_value, counter, 1));
      counter := counter + 2;
   END LOOP;

   /* Add the even and odd digits together and MOD the result by 10
      to get the last digit of the result (if the MOD returns itself, it
      will be handled by the next step).
   */
   last_digit := MOD(odd_digits + even_digits, 10);

   /* Decode the last digit to determine what would be required to make
      the last digit equal to 10.  If the last digit is not from 1 to 9
      then it is assumed that the last digit was a 10 (or actually it was
      set to (odd_digits + even_digits).
   */
   SELECT DECODE(last_digit, 1, 9,
                             2, 8,
                             3, 7,
                             4, 6,
                             5, 5,
                             6, 4,
                             7, 3,
                             8, 2,
                             9, 1,
                             0) 
     INTO check_digit
     FROM dual;

   /* Now recalculate the number_to_chk with the new check digit. */
   number_to_chk := (number_to_chk * 10) + check_digit;
   
END CHKDIG_ADD_UPC_A;
/


