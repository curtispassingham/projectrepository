
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
------------------------------------------------------------------------------
-- Name:    WH_VAL
-- Purpose: Validate the wh by seeing if it exists on file.
--   If it does then return 'TRUE' and the wh name.
--   If it doesn't, return 'FALSE' and the error of not finding it on file.
--   If there was any other error then return 'NULL' and the error message. 
------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE WH_VAL (in_wh         IN     NUMBER,
                                    out_wh_name   IN OUT VARCHAR2,
                                    return_code   IN OUT VARCHAR2,
                                    error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   cursor C_WH_EXISTS is
      SELECT wh_name
        FROM v_wh_tl
       WHERE wh = in_wh;
BEGIN
   open  c_wh_exists;
   fetch c_wh_exists into out_wh_name;

   if c_wh_exists%NOTFOUND then
      return_code := 'FALSE';
      error_message := 'Invalid Warehouse Number entered.';
   else
      return_code := 'TRUE';
   end if;

   close c_wh_exists;
EXCEPTION
   when OTHERS then
      return_code := 'NULL';
      error_message := SQLERRM || ' from WH_VAL.';
END WH_VAL;
/


