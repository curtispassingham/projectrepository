
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-----------------------------------------------------------------------------
-- Name:    STORE_VAL
-- Purpose: Validate the store by seeing if it exists on file.
--   If it does then return 'TRUE' and the store name.
--   If it doesn't, return 'FALSE' and the error of not finding it on file.
--   If there was any other error then return 'NULL' and the error message.
-----------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE STORE_VAL (in_store       IN     NUMBER,
                                       out_store_name IN OUT VARCHAR2,
                                       return_code    IN OUT VARCHAR2,
                                       error_message  IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   cursor C_STORE_EXISTS is
      SELECT store_name
        FROM v_store_tl
       WHERE store = in_store;
BEGIN
   open  c_store_exists;
   fetch c_store_exists into out_store_name;
   if (c_store_exists%NOTFOUND) then
      return_code := 'FALSE';
      error_message := 'Invalid Store Number entered.';
   else
      return_code := 'TRUE';
   end if;
   close c_store_exists;
EXCEPTION
    when OTHERS then
        return_code := 'NULL';
        error_message := SQLERRM || ' from STORE_VAL proc.';
END STORE_VAL;
/


