---------------------------------------------------------------------------
-- Name:    CAL_TO_454_LDOM
-- Purpose: This procedure accepts a date in calendar format, converts it 454
--          format, gets the number of weeks for the corresponding 454 month,
--          then converts the last day of that 454 month into a calendar date
--          which is then returned.
----------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CAL_TO_454_LDOM 
                            (in_dd         IN     NUMBER,
                             in_mm         IN     NUMBER,
                             in_yyyy       IN     NUMBER,
                             out_dd_last   IN OUT NUMBER,
                             out_mm_last   IN OUT NUMBER,
                             out_yyyy_last IN OUT NUMBER,
                             return_code   IN OUT VARCHAR2,
                             error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS

   /* User defined exceptions */
   ABORTING EXCEPTION;

   work_day    NUMBER(2);
   work_week   NUMBER(2);
   work_month  NUMBER(2);
   work_year   NUMBER(4);

BEGIN
   CAL_TO_454(in_dd, in_mm, in_yyyy,
              work_day, work_week, work_month, work_year,
              return_code, error_message);
   IF (return_code = 'FALSE') THEN
      raise ABORTING;
   END IF;

   work_day := 7;

   select no_of_weeks
     into work_week
     from calendar
     where month_454 = work_month
       and year_454 = work_year;

   C454_TO_CAL(work_day, work_week, work_month, work_year,
               out_dd_last, out_mm_last, out_yyyy_last,
               return_code, error_message);
   IF (return_code = 'FALSE') THEN
      raise ABORTING;
   END IF;

   return_code := 'TRUE';
   
EXCEPTION
   when ABORTING then
      error_message := error_message || ' from CAL_TO_454_LDOM';
   WHEN OTHERS THEN
      error_message := SQLERRM || ' from CAL_TO_454_LDOM';
      return_code := 'FALSE';
END CAL_TO_454_LDOM;
/


