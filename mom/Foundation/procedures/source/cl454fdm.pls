
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
------------------------------------------------------------------------------
-- Name:    CAL_TO_454_FDOM
-- Purpose: This procedure accepts a calendar date and calculates the first day
--          of the 454 month, then returns the start of the month in date format.
-------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CAL_TO_454_FDOM (in_dd         IN     NUMBER,
                                             in_mm         IN     NUMBER,
                                             in_yyyy       IN     NUMBER,
                                             out_dd        IN OUT NUMBER,
                                             out_mm        IN OUT NUMBER,
                                             out_yyyy      IN OUT NUMBER,
                                             return_code   IN OUT VARCHAR2,
                                             error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS

   ABORTING        EXCEPTION;

   work_454_year   NUMBER(4);
   work_454_month  NUMBER(2);
   dummy           NUMBER(1);

BEGIN
   return_code := 'TRUE';
   
   CAL_TO_454(in_dd, in_mm, in_yyyy,
              dummy, dummy, work_454_month, work_454_year,
              return_code, error_message);
   IF (return_code = 'FALSE') THEN
      raise ABORTING;
   END IF;

   C454_TO_CAL(1, 1, work_454_month, work_454_year,
               out_dd, out_mm, out_yyyy,
               return_code, error_message);
   IF (return_code = 'FALSE') THEN
      raise ABORTING;
   END IF;

EXCEPTION
   WHEN ABORTING THEN
      NULL;
   WHEN OTHERS THEN
       error_message := SQLERRM || ' from CAL_TO_454_FDOM proc.';
       return_code := 'FALSE';
END CAL_TO_454_FDOM;
/


