
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------------
-- Name:    NEXT_MERCH_NUMBER
-- Purpose: This merchant number sequence generator will return to the calling
--          program/procedure.  Upon success (TRUE) a merchant number.  
--          Upon failure (FALSE) an appropriate error message for display purposes by
--          the calling program/procedure.
-------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_MERCH_NUMBER (merch_number  IN OUT NUMBER,
                                               return_code   IN OUT VARCHAR2,
                                               error_message IN OUT VARCHAR2) AUTHID CURRENT_USER IS
 
   L_merch_sequence       NUMBER(6);
   wrap_sequence_number   NUMBER(6);
   check_digit            NUMBER(1);
   first_time             VARCHAR2(3) := 'Yes';
   dummy                  VARCHAR2(1);
 
   CURSOR c_merch_exists(merch_number_param NUMBER) IS
      SELECT 'x'
         FROM merchant 
         WHERE merchant.merch = merch_number_param;
 
BEGIN
    LOOP
        SELECT merch_sequence.NEXTVAL
          INTO L_merch_sequence
          FROM sys.dual;
 
        IF (first_time = 'Yes') THEN
            wrap_sequence_number := L_merch_sequence;
            first_time := 'No';
        ELSIF (L_merch_sequence = wrap_sequence_number) THEN
            error_message := 'Fatal error - no available merch numbers';
            return_code := 'FALSE';
            EXIT;
        END IF;
 
        merch_number := L_merch_sequence;
        OPEN  c_merch_exists(merch_number);
        FETCH c_merch_exists into dummy;
        IF (c_merch_exists%notfound) THEN
            return_code := 'TRUE';
            CLOSE c_merch_exists;
            EXIT;
        END IF;
        CLOSE c_merch_exists;
    END LOOP;
EXCEPTION
    WHEN OTHERS THEN
      error_message := SQLERRM || ' from NEXT_MERCH_NUMBER proc.';
        return_code := 'FALSE';
END NEXT_MERCH_NUMBER;
/


