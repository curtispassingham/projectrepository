
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
---------------------------------------------------------------------------------
-- Name:       C454_TO_CAL
-- Purpose:    This procedure validates a date in 454 format and converts it 
--             to a calendar day, month and year.  It is called by CAL_TO_454_FDOM
--             and CAL_TO_454_LDOM.
---------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE C454_TO_CAL (in_dd_454     IN     NUMBER,
                                         in_wk_454     IN     NUMBER,
                                         in_mm_454     IN     NUMBER,
                                         in_yyyy_454   IN     NUMBER,
                                         out_dd        IN OUT NUMBER,
                                         out_mm        IN OUT NUMBER,
                                         out_yyyy      IN OUT NUMBER,
                                         return_code   IN OUT VARCHAR2,
                                         error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   ABORTING    EXCEPTION;
   cal_date    DATE;
   no_of_weeks NUMBER;
BEGIN
   return_code := 'TRUE';

   IF (in_dd_454 < 1) OR
      (in_dd_454 > 7) THEN
      raise ABORTING;
   ELSIF (in_wk_454 < 1) OR
      (in_wk_454 > 5) THEN
      raise ABORTING;
   ELSIF (in_mm_454 < 1) OR
      (in_mm_454 > 12) THEN
      raise ABORTING;
   END IF;

   SELECT calendar.no_of_weeks,
          calendar.first_day + (in_wk_454 - 1) * 7 + in_dd_454 - 1
     INTO no_of_weeks,
          cal_date
     FROM calendar
     WHERE calendar.year_454 = in_yyyy_454
       AND calendar.month_454 = in_mm_454;

   IF (in_wk_454 > no_of_weeks) THEN
      raise ABORTING;
   END IF;

   out_dd   := to_char(cal_date, 'DD');
   out_mm   := to_char(cal_date, 'MM');
   out_yyyy := to_char(cal_date, 'YYYY');

EXCEPTION
    WHEN ABORTING THEN
        return_code := 'FALSE';
        error_message := error_message || ' from C454_TO_CAL';
    WHEN OTHERS THEN
        error_message := SQLERRM || ' from C454_TO_CAL';
        return_code := 'FALSE';
END C454_TO_CAL;
/


