
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
----------------------------------------------------------------------------
-- Name:    DISTRICT_VAL
-- Purpose: Validate the district by seeing if it exists on file.
--          If it does then return 'TRUE' and the district name.
--          If it doesn't, return 'FALSE' and the error of not finding it on file.
--          If there was any other error then return 'NULL' and the error message.
----------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE DISTRICT_VAL (in_district       IN     NUMBER,
                                          out_district_name IN OUT VARCHAR2,
                                          return_code       IN OUT VARCHAR2,
                                          error_message     IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   cursor C_DISTRICT_EXISTS is
      SELECT district_name
        FROM v_district_tl
       WHERE district = in_district;
BEGIN
   open  c_district_exists;
   fetch c_district_exists into out_district_name;
   if c_district_exists%NOTFOUND then
      return_code := 'FALSE';
      error_message := 'Invalid District Number entered.';
   else
      return_code := 'TRUE';
   end if;
   close c_district_exists;
EXCEPTION
   when OTHERS then
      return_code := 'NULL';
      error_message := SQLERRM || ' from DISTRICT_VAL proc.';
END DISTRICT_VAL;
/


