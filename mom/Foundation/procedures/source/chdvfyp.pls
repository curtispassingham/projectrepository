
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PROCEDURE CHKDIG_VERIFY_PRC(O_error_message          OUT VARCHAR2,
                            O_return_code            OUT VARCHAR2,
                            I_price_to_chk IN      VARCHAR2) AUTHID CURRENT_USER is

   L_price_len          NUMBER(2)   := 0;
   L_check_digit        NUMBER(1)   := 0;
   L_temp_number        NUMBER(3)   := 0;
   L_actual_check_digit NUMBER(1)   := 0;
   L_first_digit        NUMBER(1);
   L_second_digit       NUMBER(1);
   L_third_digit        NUMBER(1);
   L_fourth_digit       NUMBER(1);

    L_program  VARCHAR2(50) := 'CHKDIG_VERIFY_PRC';

BEGIN

   L_price_len := length(I_price_to_chk);
   L_actual_check_digit := substr(I_price_to_chk, 1, 1);
   L_fourth_digit := substr(I_price_to_chk, L_price_len, 1);
   L_third_digit  := substr(I_price_to_chk, L_price_len-1, 1);
   L_second_digit := substr(I_price_to_chk, L_price_len-2, 1);
   L_first_digit  := substr(I_price_to_chk, L_price_len-3, 1);

   select decode(L_first_digit, 1, 2,
                                2, 4,
                                3, 6,
                                4, 8,
                                5, 9,
                                6, 1,
                                7, 3,
                                8, 5,
                                9, 7,
                                0, 0)
   into L_first_digit
   from dual;

   select DECODE(L_second_digit, 1, 2,
                                 2, 4,
                                 3, 6,
                                 4, 8,
                                 5, 9,
                                 6, 1,
                                 7, 3,
                                 8, 5,
                                 9, 7,
                                 0, 0)
   into L_second_digit
   from dual;

   select DECODE(L_third_digit, 1, 3,
                                2, 6,
                                3, 9,
                                4, 2,
                                5, 5,
                                6, 8,
                                7, 1,
                                8, 4,
                                9, 7,
                                0, 0)
   into L_third_digit
   from dual;

   select DECODE(L_fourth_digit, 1, 5,
                                 2, 9,
                                 3, 4,
                                 4, 8,
                                 5, 3,
                                 6, 7,
                                 7, 2,
                                 8, 6,
                                 9, 1,
                                 0, 0)
   into L_fourth_digit
   from dual;

   L_temp_number := L_first_digit+L_second_digit+L_third_digit+L_fourth_digit;
   L_temp_number := L_temp_number*3;
   L_check_digit := mod(L_temp_number, 10);

   if (L_actual_check_digit = L_check_digit) then
      O_return_code := 'TRUE';
   else
      O_return_code := 'FALSE';
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_CHK_DIG',
                                                  NULL,
                                                  NULL,
                                                  NULL);
   end if;

EXCEPTION
   when OTHERS then
      O_return_code := 'NULL';
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
END CHKDIG_VERIFY_PRC;
/


