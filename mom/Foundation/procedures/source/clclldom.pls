
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
------------------------------------------------------------------------
-- Name:    CAL_TO_CAL_LDOM
-- Purpose: This procedure determines the last day of a calendar month
--          given a calendar date.
------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CAL_TO_CAL_LDOM (in_dd         IN     NUMBER,
                                             in_mm         IN     NUMBER,
                                             in_yyyy       IN     NUMBER,
                                             out_dd        IN OUT NUMBER,
                                             out_mm        IN OUT NUMBER,
                                             out_yyyy      IN OUT NUMBER,
                                             return_code   IN OUT VARCHAR2,
                                             error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   dummy_date date;
BEGIN
   dummy_date := last_day(to_date(to_char(in_dd, '09') ||
                                  to_char(in_mm, '09') || 
                                  to_char(in_yyyy, '0999'), 'DDMMYYYY'));
   out_dd     := substr(to_char(dummy_date, 'DDMMYYYY'), 1, 2);
   out_mm     := substr(to_char(dummy_date, 'DDMMYYYY'), 3, 2);
   out_yyyy   := substr(to_char(dummy_date, 'DDMMYYYY'), 5, 4);
EXCEPTION
   when OTHERS then
      return_code := 'FALSE';
      error_message := SQLERRM || ' from CAL_TO_CAL_LDOM';
END CAL_TO_CAL_LDOM;
/


