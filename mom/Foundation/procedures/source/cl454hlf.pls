
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
----------------------------------------------------------------------------
-- Name:    CAL_TO_454_HALF
-- Purpose: This procedure validates a calendar day, month and year and then
--          converts it a 454 Half format.
----------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CAL_TO_454_HALF 
                            (in_dd             IN     NUMBER,
                             in_mm             IN     NUMBER,
                             in_yyyy           IN     NUMBER,
                             half_no           IN OUT NUMBER,
                             month_in_half     IN OUT NUMBER,
                             return_code       IN OUT VARCHAR2,
                             error_message     IN OUT VARCHAR2) AUTHID CURRENT_USER AS

   /* User defined exceptions */
   ABORTING EXCEPTION;

   /* Local variables */
   L_dummy             VARCHAR2(1);
   start_of_half_month NUMBER(2);
   start_of_last_year  NUMBER(1);
   out_dd_454          NUMBER(1);
   out_wk_454          NUMBER(2);
   out_mm_454          NUMBER(2);
   out_yyyy_454        NUMBER(4);
   half                NUMBER(5);
   nett_addition       NUMBER(9);

BEGIN
   return_code := 'TRUE';

   if SYSTEM_CALENDAR_SQL.GET_CAL_OPTS ( error_message,
                                         L_dummy,
                                         start_of_half_month,
                                         start_of_last_year) = FALSE then
      return_code := 'FALSE';
      raise ABORTING;
   end if;                                      

   CAL_TO_454(in_dd, in_mm, in_yyyy,
              out_dd_454, out_wk_454, out_mm_454, out_yyyy_454,
              return_code, error_message);
   IF (return_code = 'FALSE') THEN
      raise ABORTING;
   END IF;

   half_no := (out_yyyy_454 -
                TRUNC((start_of_half_month - out_mm_454 + 11) / 12)) * 10;

   IF (start_of_last_year < 0) THEN
      half_no := half_no + 10;
   END IF;

   half := TRUNC((start_of_half_month - out_mm_454 + 11) / 6);

   IF (MOD(half, 2) = 1) THEN
      half_no := half_no + 1;
   ELSE
      half_no := half_no + 2;
   END IF;

   nett_addition := MOD((out_mm_454 - start_of_half_month + 12), 6);
   IF (nett_addition = 6) THEN
      nett_addition := 0;
   END IF;
   month_in_half := out_mm_454 + (nett_addition - out_mm_454 + 1);

EXCEPTION
   WHEN ABORTING THEN
      NULL;
   WHEN OTHERS THEN
       error_message := SQLERRM || ' from CAL_TO_454_HALF';
       return_code := 'FALSE';
END CAL_TO_454_HALF;
/


