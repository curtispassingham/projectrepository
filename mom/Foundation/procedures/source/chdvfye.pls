
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PROCEDURE CHKDIG_VERIFY_EAN(I_number_to_chk IN     VARCHAR2,
                                              return_code            OUT VARCHAR2,
                                              error_message          OUT VARCHAR2) AUTHID CURRENT_USER IS

   temp_number        NUMBER(13)   := 0;
   number_len         NUMBER(2)    := 0;
   counter            NUMBER(2)    := 0;
   char_value         VARCHAR2(13) := NULL;
   even_digits        NUMBER(4)    := 0;
   odd_digits         NUMBER(4)    := 0;
   last_digit         NUMBER(4)    := 0;
   check_digit        NUMBER(4)    := 0;
   actual_check_digit NUMBER(4)    := 0;

BEGIN

   number_len := LENGTH(I_number_to_chk);

   IF number_len != 8 AND number_len != 13 AND number_len != 14 THEN
      error_message := 'EAN number is of incorrect length.';
      return_code := 'FALSE';
      RETURN;
   END IF;

   /* Drop off the check digit from the argument EAN number. */
   temp_number := TRUNC(To_Number(I_number_to_chk) / 10);
   /* Left pad the test number with 0's to make it max EAN length
      (14 minus the check digit = 13).
   */
   char_value  := LPAD(TO_CHAR(temp_number), 13, '0');

   /* Get each of the odd placed digits starting on the right,
      summing them together and then multiplying the result by 3.
      (Note that in the original EAN number, these are the even
      digits like 2, 4, 6, 8, etc.)
   */
   counter := 13;
   WHILE (counter > 13-number_len) LOOP
      odd_digits := odd_digits + to_number(substr(char_value, counter, 1));
      counter := counter - 2;
   END LOOP;
   odd_digits := odd_digits * 3;

   /* Get each of the even placed digits, summing them together.
      (Note that in the original EAN number, these are the odd
      digits like 1, 3, 5, 7, etc.)
   */
   counter := 12;
   WHILE (counter > 14-number_len) LOOP
      even_digits := even_digits + to_number(substr(char_value, counter, 1));
      counter := counter - 2;
   END LOOP;

   /* Add the even and odd digits together and MOD the result by 10
      to get the last digit of the result (if the MOD returns itself, it
      will be handled by the next step).
   */
   last_digit := MOD(odd_digits + even_digits, 10);

   /* Decode the last digit to determine what would be required to make
      the last digit equal to 10.  If the last digit is not from 1 to 9
      then it is assumed that the last digit was a 10 (or actually it was
      set to (odd_digits + even_digits).
   */
   SELECT DECODE(last_digit, 1, 9,
                             2, 8,
                             3, 7,
                             4, 6,
                             5, 5,
                             6, 4,
                             7, 3,
                             8, 2,
                             9, 1,
                             0)
   INTO check_digit
   FROM dual;


   /* Determine if the calculated check digit is equal to the actual
      check digit.
   */
   actual_check_digit := MOD(I_number_to_chk, 10);
   IF (actual_check_digit > 9) THEN
      actual_check_digit := 0;
   END IF;

   IF (actual_check_digit = check_digit) THEN
      return_code := 'TRUE';
   ELSE
      return_code := 'FALSE';
      error_message := 'Invalid EAN entered.';
   END IF;
EXCEPTION
   when OTHERS then
      return_code := 'NULL';
      error_message := SQLERRM || ' from CHKDIG_VERIFY_EAN proc.';
END CHKDIG_VERIFY_EAN;
/


