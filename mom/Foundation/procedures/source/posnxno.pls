
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
---------------------------------------------------------------------------------
-- Name:    POS_NEXT_GENERATED_NUMBER
-- Purpose: This number sequence generator will return to the calling 
--          program/procedure based on the calling dialogue within RMS. 
--          The dialogues included in this function are: Coupon and
--          Product Restriction.
---------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION POS_NEXT_GENERATED_NUMBER(O_error_message    IN OUT VARCHAR2,
                                                     O_generated_number IN OUT NUMBER,
                                                     I_calling_dialogue IN  VARCHAR2)
RETURN BOOLEAN AUTHID CURRENT_USER IS

   L_number_sequence        NUMBER(6);
   L_wrap_sequence_number   NUMBER(6);
   L_first_time             VARCHAR2(3) := 'Yes';
   L_dummy                  VARCHAR2(1);

   cursor C_COUPON_ID_EXISTS is
      select 'x'
        from pos_coupon_head
       where coupon_id = O_generated_number;

   cursor C_POS_COUPON_NEXTVAL is
      select pos_coupon_sequence.NEXTVAL
        from sys.dual;


   cursor C_PROD_REST_ID_EXISTS is
      select 'x'
        from pos_prod_rest_head
       where pos_prod_rest_id = O_generated_number;

   cursor C_POS_PROD_REST_NEXTVAL is
      select pos_product_restr_sequence.NEXTVAL
        from sys.dual;



BEGIN
      if I_calling_dialogue = 'COUP' then
         LOOP
            SQL_LIB.SET_MARK('OPEN','C_POS_COUPON_NEXTVAL','DUAL',NULL);
            open C_POS_COUPON_NEXTVAL;
            SQL_LIB.SET_MARK('FETCH','C_POS_COUPON_NEXTVAL','DUAL',NULL);
            fetch C_POS_COUPON_NEXTVAL into L_number_sequence;
            if C_POS_COUPON_NEXTVAL%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_POS_COUPON_NEXTVAL','DUAL',NULL);
               close C_POS_COUPON_NEXTVAL;
               O_error_message := SQL_LIB.CREATE_MSG('NO_AVAIL_COUPON_ID',NULL,NULL,NULL);
               return FALSE;
            end if;
            SQL_LIB.SET_MARK('CLOSE','C_POS_COUPON_NEXTVAL','DUAL',NULL);
            close C_POS_COUPON_NEXTVAL;

            if L_first_time = 'Yes' then
               L_wrap_sequence_number := L_number_sequence;
               L_first_time           := 'No';
            elsif L_number_sequence = L_wrap_sequence_number then
               O_error_message := SQL_LIB.CREATE_MSG('NO_AVAIL_COUPON_ID',NULL,NULL,NULL); 
               RETURN FALSE;
            end if;

            O_generated_number      := L_number_sequence;
            
            SQL_LIB.SET_MARK('OPEN','C_COUPON_ID_EXISTS','POS_COUPON_HEAD',
                             'COUPON ID: '||TO_CHAR(O_generated_number));
            open  C_COUPON_ID_EXISTS;
            SQL_LIB.SET_MARK('FETCH','C_COUPON_ID_EXISTS','POS_COUPON_HEAD',
                             'COUPON ID: '||TO_CHAR(O_generated_number));

            fetch C_COUPON_ID_EXISTS into L_dummy;
            if C_COUPON_ID_EXISTS%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_COUPON_ID_EXISTS','POS_COUPON_HEAD',
                                'COUPON ID: '||TO_CHAR(O_generated_number));
               close C_COUPON_ID_EXISTS;
               RETURN TRUE;
            end if;
            SQL_LIB.SET_MARK('CLOSE','C_COUPON_ID_EXISTS','POS_COUPON_HEAD',
                             'COUPON ID: '||TO_CHAR(O_generated_number));
            close C_COUPON_ID_EXISTS;
         END LOOP;
      
      elsif I_calling_dialogue = 'PRES' then
         LOOP
            SQL_LIB.SET_MARK('OPEN','C_POS_PROD_REST_NEXTVAL','DUAL',NULL);
            open C_POS_PROD_REST_NEXTVAL;
            SQL_LIB.SET_MARK('FETCH','C_POS_PROD_REST_NEXTVAL','DUAL',NULL);
            fetch C_POS_PROD_REST_NEXTVAL into L_number_sequence;
            if C_POS_PROD_REST_NEXTVAL%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_POS_PROD_REST_NEXTVAL','DUAL',NULL);
               close C_POS_PROD_REST_NEXTVAL;
               O_error_message := SQL_LIB.CREATE_MSG('NO_AVAIL_PROD_REST_ID',NULL,NULL,NULL);
               return FALSE;
            end if;
            SQL_LIB.SET_MARK('CLOSE','C_POS_PROD_REST_NEXTVAL','DUAL',NULL);
            close C_POS_PROD_REST_NEXTVAL;

            if L_first_time = 'Yes' then
               L_wrap_sequence_number := L_number_sequence;
               L_first_time           := 'No';
            elsif L_number_sequence = L_wrap_sequence_number then
               O_error_message := SQL_LIB.CREATE_MSG('NO_AVAIL_PROD_REST_ID',NULL,NULL,NULL); 
               RETURN FALSE;
            end if;

            O_generated_number      := L_number_sequence;

            SQL_LIB.SET_MARK('OPEN','C_PROD_REST_ID_EXISTS','POS_PROD_REST_HEAD',
                             'POS PROD REST ID: '||TO_CHAR(O_generated_number));
            open  C_PROD_REST_ID_EXISTS;
            SQL_LIB.SET_MARK('FETCH','C_PROD_REST_ID_EXISTS','POS_PROD_REST_HEAD',
                             'POS PROD REST ID: '||TO_CHAR(O_generated_number));
            fetch C_PROD_REST_ID_EXISTS into L_dummy;
            if C_PROD_REST_ID_EXISTS%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_PROD_REST_ID_EXISTS','POS_PROD_REST_HEAD',
                                'POS PROD REST ID: '||TO_CHAR(O_generated_number));
               close C_PROD_REST_ID_EXISTS;
               RETURN TRUE;
            end if;
            SQL_LIB.SET_MARK('CLOSE','C_PROD_REST_ID_EXISTS','POS_PROD_REST_HEAD',
                             'POS PROD REST ID: '||TO_CHAR(O_generated_number));
            close C_PROD_REST_ID_EXISTS;
         END LOOP;

      else
        O_error_message := SQL_LIB.CREATE_MSG('POS_INV_CONFIG_TYPE',NULL,NULL,NULL); 	
      	RETURN TRUE;
      end if;
  
EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'NEW_GENERATED_NUMBER',
                                            to_char(SQLCODE));
      RETURN FALSE;
END POS_NEXT_GENERATED_NUMBER;
/


