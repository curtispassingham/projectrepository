
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PROCEDURE NEXT_UPC_A (O_error_message      OUT VARCHAR2,
                      O_return_code        OUT VARCHAR2,
                      IO_out_upc        IN OUT VARCHAR2) AUTHID CURRENT_USER is

   L_first_time      VARCHAR2(3) := 'Yes';
   L_wrap_seq_number NUMBER(13);
   L_check_digit     NUMBER(1);
   L_dummy           VARCHAR2(1);
   L_num_upc         NUMBER(13)  := 0;

   cursor C_UPC_EXISTS(in_item VARCHAR2) is
      select 'x'
         from  item_master
         where item = in_item;

BEGIN
   LOOP
      SELECT UPC_A_SEQUENCE.NEXTVAL
        INTO L_num_upc
        FROM sys.dual;

      L_num_upc := (40000000000) + L_num_upc;

      if (L_first_time = 'Yes') then
         L_wrap_seq_number := L_num_upc;
         L_first_time := 'No';
      elsif (L_num_upc = L_wrap_seq_number) then
         O_error_message := 'Fatal error - no available upc_a_sequence numbers';
         O_return_code := 'FALSE';
         return;
      end if;

      /*
        Num_upc is a number and will be passed to chkdig_add_upc_a as a
        number.
      */
      CHKDIG_ADD_UPC_A(L_num_upc, L_check_digit);

      /*
         Once the check digit is added, the UPC is coverted back to a
         character field.
      */
      IO_out_upc := to_char(L_num_upc);

      open  C_UPC_EXISTS(IO_out_upc);
      fetch C_UPC_EXISTS into L_dummy;
      if (C_UPC_EXISTS%NOTFOUND) then
         O_return_code := 'TRUE';
         close C_UPC_EXISTS;
         return;
      end if;
      close C_UPC_EXISTS;
   END LOOP;
EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM || ' from NEXT_UPC_A proc.';
      O_return_code := 'FALSE';
END NEXT_UPC_A;
/


