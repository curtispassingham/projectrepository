
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------------------
-- Name:    VALIDATE_CAL
-- Purpose: This procedure validates a calendar day, month and year.
--------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE VALIDATE_CAL (in_dd         IN     NUMBER,
                                          in_mm         IN     NUMBER,
                                          in_yyyy       IN     NUMBER,
                                          return_code   IN OUT VARCHAR2,
                                          error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   dummy_date date;
BEGIN
   return_code := 'TRUE';
   dummy_date := to_date(to_char(in_dd, '09') || to_char(in_mm, '09') ||
                         to_char(in_yyyy, '0999'), 'DDMMYYYY');
EXCEPTION
    WHEN OTHERS THEN
        error_message := SQLERRM || ' from VALIDATE_CAL proc.';
        return_code := 'FALSE';
END VALIDATE_CAL;
/


