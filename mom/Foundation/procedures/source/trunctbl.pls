---------------------------------------------------------------------------
-- Name:    TRUNCATE_TABLE
-- Purpose: This function will be used to truncate a specific or all
--          partitions in a table.
--
--          The function accepts a parameter of table name and does a truncate.
--          It also accepts an optional parameter of a partition name.
--          If no partition is specified, it does a truncate of the entire table.
--
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION TRUNCATE_TABLE (O_error_message    IN OUT   VARCHAR2,
                                           I_table_name       IN       VARCHAR2,
                                           I_partition_name   IN       VARCHAR2   DEFAULT NULL)
   RETURN BOOLEAN AUTHID CURRENT_USER IS

   L_program   VARCHAR2(30) := 'TRUNCATE_TABLE';

   L_system_options   SYSTEM_OPTIONS%ROWTYPE;
   L_sql              VARCHAR2(1000);

BEGIN
   ---
   IF I_table_name IS NULL THEN
      ---
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_table_name',
                                             L_program,
                                             NULL);
      ---
      RETURN FALSE;
   END IF;
   ---
   IF SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE THEN
      RETURN FALSE;
   END IF;

   -- If no partition is passed in, truncate the entire table.
   IF I_partition_name IS NULL THEN
      L_sql := 'TRUNCATE TABLE ' || L_system_options.table_owner || '.' || I_table_name;
   -- If partition is passed in, truncate only the specific partition
   ELSE
      L_sql := 'ALTER TABLE ' || L_system_options.table_owner || '.' || I_table_name || ' TRUNCATE PARTITION ' || I_partition_name;
   END IF;

   EXECUTE IMMEDIATE L_sql;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END;
/
