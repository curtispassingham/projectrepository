
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------------
-- Name:    MID_MONTH
-- Purpose: This procedure accepts an input date in 'MMDDYYYY' format, converts it to
--          a 454 date and returns the middle day of that 454 month.
--
--   Example: Today's date is 31/7/1990 and 'next month' is needed.
--            Simply adding 1 to the month does not result in a date that
--            falls  in  the  next  4-5-4 month, in fact both 31/7/1990 and
--            31/8/1990 fall in 4-5-4 August,  but the 15th of any calendar
--            month is guaranteed to fall in the same 4-5-4 month.
--------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE MID_MONTH (in_out_date   IN OUT VARCHAR2,
                                       return_code   IN OUT VARCHAR2,
                                       error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS

   /* User defined exceptions */
   ABORTING EXCEPTION;

   /* Local variables */
   cal_454_ind     VARCHAR2(1);
   work_day        NUMBER(2);
   work_month      NUMBER(2);
   work_year       NUMBER(4);
   out_dd_454      NUMBER(1);
   out_wk_454      NUMBER(1);
   out_mm_454      NUMBER(2);
   out_yyyy_454    NUMBER(4);

BEGIN
   return_code := 'TRUE';
   error_message := NULL;

   SELECT DECODE(system_options.calendar_454_ind, 'C', 1, 0)
     INTO cal_454_ind
     FROM system_options;

   work_month := TO_NUMBER(SUBSTR(in_out_date, 1, 2));
   work_day   := TO_NUMBER(SUBSTR(in_out_date, 3, 2));
   work_year  := TO_NUMBER(SUBSTR(in_out_date, 5, 4));

   IF (cal_454_ind = 0) THEN
      CAL_TO_454(work_day, work_month, work_year,
                 out_dd_454, out_wk_454, out_mm_454, out_yyyy_454,
                 return_code, error_message);
      IF (return_code = 'FALSE') THEN
         error_message := 'Date conversion error.' || ' from MID_MONTH proc.';
         raise ABORTING;
      END IF;

      work_month := out_mm_454;
      work_year  := out_yyyy_454;

   END IF;

   in_out_date := 15000000 + (work_month * 10000) + work_year;

EXCEPTION
   WHEN ABORTING THEN
      NULL;
   WHEN OTHERS THEN
      error_message := SQLERRM || ' from MID_MONTH proc.';
      return_code := 'FALSE';
END MID_MONTH;
/


