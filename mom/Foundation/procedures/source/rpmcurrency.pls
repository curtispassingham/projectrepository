
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
----------------------------------------------------------------------------
-- Name:    RPM_CURRENCY_CONVERT
-- Purpose: Contains a call to currency_sql.convert but with out a 
--          boolean return value.  This is needed for JDBC.
--       
--          if there is an error, the O_currency_value will be set to null.
----------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE RPM_CURRENCY_CONVERT (O_error_message        IN OUT  VARCHAR2,
                                                  I_currency_value       IN      NUMBER,
                                                  I_currency             IN      CURRENCY_RATES.CURRENCY_CODE%TYPE,
                                                  I_currency_out         IN      CURRENCY_RATES.CURRENCY_CODE%TYPE,
                                                  O_currency_value       IN OUT  NUMBER,
                                                  I_cost_retail_ind      IN      VARCHAR2,
                                                  I_effective_date       IN      CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                                                  I_exchange_type        IN      CURRENCY_RATES.EXCHANGE_TYPE%TYPE) AUTHID CURRENT_USER AS
BEGIN


   if CURRENCY_SQL.CONVERT(O_error_message,
                           I_currency_value,
                           I_currency,
                           I_currency_out,
                           O_currency_value,
                           I_cost_retail_ind,
                           I_effective_date,
                           I_exchange_type) = FALSE then
      O_currency_value := NULL;
   end if;

EXCEPTION
   when OTHERS then
      O_currency_value := NULL;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RPM_CURRENCY_CONVERT',
                                             to_char(SQLCODE));

END RPM_CURRENCY_CONVERT;
/
