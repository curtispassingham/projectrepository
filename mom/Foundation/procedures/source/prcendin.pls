
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------------
-- Name:    PRICE_ENDS_IN
-- Purpose: This function takes a price and an ends in value (positive value < 1.00)
--          and returns the updated price.
--------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION PRICE_ENDS_IN (O_in_out_amount IN OUT NUMBER,
                                          I_ends_in_amt   IN     NUMBER,
                                          O_error_message IN OUT VARCHAR2) 
	RETURN VARCHAR2 AUTHID CURRENT_USER IS 

   L_io_amt               VARCHAR2(20) := O_in_out_amount;
   L_trunc_io_amt         VARCHAR2(20) := trunc(O_in_out_amount);
   L_ends_in_amt          VARCHAR2(2) := I_ends_in_amt;
   L_test_char            VARCHAR2(1);
   L_test_flag            VARCHAR2(1) := 'N';
   L_count                NUMBER := 0; 

BEGIN

   if nvl(O_in_out_amount, 0) = 0 then
      O_in_out_amount := 0;
   else
      for N in 1 .. length(L_io_amt) LOOP
         if L_test_flag = 'Y' then
            L_count := L_count + 1;
         end if;
         L_test_char := substr(L_io_amt,N,1);    
         if L_test_char = '.' then
         	  L_test_flag := 'Y';
         end if;
      END LOOP;

      if length(L_ends_in_amt) = 1 then
         if L_test_flag = 'Y' then
            if L_count = 1 then
               O_in_out_amount := L_io_amt||L_ends_in_amt;
            else
               O_in_out_amount := substr(L_io_amt,1,(length(L_io_amt) - 1))||L_ends_in_amt;
            end if;
         else
            O_in_out_amount := L_io_amt||'.0'||L_ends_in_amt;
         end if; 
      else
         O_in_out_amount := trunc(O_in_out_amount) + I_ends_in_amt/POWER(10,length(I_ends_in_amt));
      end if;
   end if;

   return 'TRUE';

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQLERRM || ' from function PRICE_ENDS_IN';
      return 'FALSE';

END PRICE_ENDS_IN;
/
