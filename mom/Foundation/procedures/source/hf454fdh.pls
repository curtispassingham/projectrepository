
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------
-- Name:    HALF_TO_454_FDOH
-- Purpose: This procedure accepts a half number, converts it to a date, then
--          gets the first day for the 454 half.
-------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE HALF_TO_454_FDOH (half_no       IN     NUMBER,
                                              out_dd        IN OUT NUMBER,
                                              out_mm        IN OUT NUMBER,
                                              out_yyyy      IN OUT NUMBER,
                                              return_code   IN OUT VARCHAR2,
                                              error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   ABORTING            EXCEPTION;

   L_dummy             VARCHAR2(1);
   start_of_half_month NUMBER(2);
   start_lst_year      NUMBER(1);
   work_day            NUMBER(2);
   work_month          NUMBER(2);
   work_year           NUMBER(4);

BEGIN
   return_code := 'TRUE';

   if SYSTEM_CALENDAR_SQL.GET_CAL_OPTS (error_message,
                                        L_dummy,
                                        start_of_half_month,
                                        start_lst_year) = FALSE then
      return_code := 'FALSE';
      raise ABORTING;
   end if; 

   work_day   := 15;
   work_month := start_of_half_month;

   IF (MOD(half_no, 10) = 2) THEN
      work_month := work_month + 6;
   END IF;

   work_year := to_number(substr(to_char(half_no), 1, 4));

   IF (start_lst_year < 0) THEN
      work_year := work_year - 1;
   END IF;

   IF (work_month > 12) THEN
      work_month := work_month - 12;
      work_year := work_year + 1;
   END IF;

   CAL_TO_454_FDOM(work_day, work_month, work_year,
                   out_dd, out_mm, out_yyyy,
                   return_code, error_message);
   IF (return_code = 'FALSE') THEN
      raise ABORTING;
   END IF;
EXCEPTION
   when ABORTING then
      NULL;
   when OTHERS then
      error_message := SQLERRM || ' from HALF_TO_454_FDOH proc.';
      return_code := 'FALSE';
END HALF_TO_454_FDOH;
/


