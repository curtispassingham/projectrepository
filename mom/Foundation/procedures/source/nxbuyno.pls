
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------------------------------
-- Name:    NEXT_BUYER_NUMBER
-- Purpose: This buyer number sequence generator will return to the calling
--          program/procedure.  Upon success (TRUE) an buyer number.  
--          Upon failure (FALSE) an appropriate error message for display purposes by
--          the calling program/procedure.
--------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_BUYER_NUMBER (buyer_number  IN OUT NUMBER,
                                               return_code   IN OUT VARCHAR2,
                                               error_message IN OUT VARCHAR2) AUTHID CURRENT_USER IS
 
   L_buyer_sequence       NUMBER(6);
   wrap_sequence_number   NUMBER(6);
   check_digit            NUMBER(1);
   first_time             VARCHAR2(3) := 'Yes';
   dummy                  VARCHAR2(1);
 
   CURSOR c_buyer_exists(buyer_number_param NUMBER) IS
      SELECT 'x'
         FROM buyer
         WHERE buyer.buyer = buyer_number_param;
 
BEGIN
    LOOP
        SELECT buyer_sequence.NEXTVAL
          INTO L_buyer_sequence
          FROM sys.dual;
 
        IF (first_time = 'Yes') THEN
            wrap_sequence_number := L_buyer_sequence;
            first_time := 'No';
        ELSIF (L_buyer_sequence = wrap_sequence_number) THEN
            error_message := 'Fatal error - no available buyer numbers';
            return_code := 'FALSE';
            EXIT;
        END IF;
 
        buyer_number := L_buyer_sequence;
        OPEN  c_buyer_exists(buyer_number);
        FETCH c_buyer_exists into dummy;
        IF (c_buyer_exists%notfound) THEN
            return_code := 'TRUE';
            CLOSE c_buyer_exists;
            EXIT;
        END IF;
        CLOSE c_buyer_exists;
    END LOOP;
EXCEPTION
    WHEN OTHERS THEN
        error_message := SQLERRM || ' from NEXT_BUYER_NUMBER proc.';
        return_code := 'FALSE';
END NEXT_BUYER_NUMBER;
/


