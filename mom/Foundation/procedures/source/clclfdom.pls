
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
----------------------------------------------------------------------------
-- Name:    CAL_TO_CAL_FDOM
-- Purpose: This procedure determines the first day of a calendar month
--          given a calendar date.
----------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CAL_TO_CAL_FDOM (in_dd         IN     NUMBER,
                                             in_mm         IN     NUMBER,
                                             in_yyyy       IN     NUMBER,
                                             out_dd        IN OUT NUMBER,
                                             out_mm        IN OUT NUMBER,
                                             out_yyyy      IN OUT NUMBER,
                                             return_code   IN OUT VARCHAR2,
                                             error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS
BEGIN
   out_dd   := 1;
   out_mm   := in_mm;
   out_yyyy := in_yyyy;
EXCEPTION
   when OTHERS then
      return_code := 'FALSE';
      error_message := SQLERRM || ' from CAL_TO_CAL_FDOM proc.';
END CAL_TO_CAL_FDOM;
/


