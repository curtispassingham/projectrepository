
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
---------------------------------------------------------------------------------
-- Name:       CAL_TO_454_LDOW
-- Purpose:    This procedure calculates the last day of week in 454 format and 
--             converts it to a calendar day, month and year.
---------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CAL_TO_454_LDOW (i_dd          IN     NUMBER,
                                             i_mm          IN     NUMBER,
                                             i_yyyy        IN     NUMBER,
                                             o_dd          IN OUT NUMBER,
                                             o_mm          IN OUT NUMBER,
                                             o_yyyy        IN OUT NUMBER,
                                             return_code   IN OUT VARCHAR2,
                                             error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   L_day        NUMBER(2);
   L_week       NUMBER(2);
   L_month      NUMBER(2);
   L_year       NUMBER(4);
   ABORTING     EXCEPTION;
BEGIN
   return_code := 'TRUE';
   error_message := NULL;

   CAL_TO_454(i_dd, i_mm, i_yyyy,
              L_day, L_week, L_month, L_year,
              return_code,
              error_message);
   if return_code <> 'TRUE' then
      raise ABORTING;
   end if;

   L_day := 7;

   C454_TO_CAL(L_day, L_week, L_month, L_year,
               o_dd, o_mm, o_yyyy,
               return_code,
               error_message);
   if return_code <> 'TRUE' then
      raise ABORTING;
   end if;
EXCEPTION
   when ABORTING then
      NULL;
   when OTHERS then
      error_message := SQLERRM||' from CAL_TO_454_LDOW.';
      return_code := 'FALSE';
END CAL_TO_454_LDOW;
/


