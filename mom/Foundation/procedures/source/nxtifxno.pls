
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE FUNCTION NEXT_TIFXPLD_SEQ_NO(O_seq_no IN OUT TIF_EXPLODE.SEQ_NO%TYPE,
                                               O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN AUTHID CURRENT_USER IS
---
L_program      VARCHAR2(64) := 'NEXT_TIFXPLD_SEQ_NO';
L_exists       VARCHAR2(1)  := NULL;
L_first_time   VARCHAR2(3)  := 'YES';
L_wrap_seq_no  TIF_EXPLODE.SEQ_NO%TYPE := NULL;
---

cursor C_GET_SEQ(cc_number_param NUMBER) IS
  select 'x'
    from tif_explode
   where seq_no = cc_number_param
     and rownum = 1;

BEGIN

   O_error_message := NULL;

   LOOP

      select TIF_EXPLODE_SEQUENCE.NEXTVAL
        into O_seq_no
        from sys.dual; 

      if (L_first_time = 'YES') then
         L_wrap_seq_no := O_seq_no;
         L_first_time := 'NO';
      elsif (O_seq_no = L_wrap_seq_no) then
         O_error_message := 'NO_MORE_TIFXPLD_SEQ_NO';
         close C_GET_SEQ;
         return FALSE;
      end if;

      open C_GET_SEQ(O_seq_no);
      fetch C_GET_SEQ INTO L_exists;
      if (C_GET_SEQ%NOTFOUND) THEN
         O_error_message := NULL;
         EXIT;
      end if;

      close C_GET_SEQ;

   END LOOP;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM || ' from NEXT_TIFXPLD_SEQ_NO function.';
      return FALSE;
END NEXT_TIFXPLD_SEQ_NO;
/
