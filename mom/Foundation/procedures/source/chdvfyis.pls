
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------------
-- Name:    CHKDIG_VERIFY_ISBN
-- Purpose: This procedure receives a 10 character ISBN number, calcuates the proper
--          check digit according to ISBN standards, compares the input check digit,
--          and returns a value of true it the digits match, or False with an error 
--          message if the digits do not match.  
-------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CHKDIG_VERIFY_ISBN (ISBN          IN     VARCHAR2,
                                                return_code      OUT VARCHAR2,
                                                error_message    OUT VARCHAR2) AUTHID CURRENT_USER AS

   L_counter          NUMBER(2)    := 0;
   L_weight           NUMBER(2)    := 0;
   char_value         VARCHAR2(25) := NULL;
   L_sum              NUMBER(4)    := 0;
   last_digit         NUMBER(4)    := 0;
   check_digit        VARCHAR2(1)  := NULL;
   actual_check_digit VARCHAR2(1)  := NULL;
   L_isbn_length      NUMBER;

BEGIN
   
   L_isbn_length := length(ISBN);

   if L_isbn_length = 10 then
      --
      -- Trucate the check digit off the ISBN number.
      --
      char_value := substr(ISBN, 1, 9);
   
      -- 
      -- Weight the digits according to ISBN standards
      --
      L_counter := 1;
      L_weight  := 10;
   
      WHILE (L_counter < 10) LOOP
         L_sum := L_sum + (to_number(substr(char_value, L_counter, 1))* L_weight);
         L_counter := L_counter + 1;
         L_weight  := L_weight - 1;
      END LOOP;
   
      --
      --  MOD the result by 11 to get the last digit of the result
      --  
      last_digit := MOD(L_sum, 11);

      --
      -- Decode the last digit to determine what would be required 
      -- to make the sum divisible by 11 without remainder.
      -- If the last digit is not from 1 to 9 then it is assumed
      -- that the last digit was a 0 and is set to '0'
      --
      SELECT DECODE(last_digit, 0, '0',
                                1, 'X',
                                2, '9',
                                3, '8',
                                4, '7',
                                5, '6',
                                6, '5',
                                7, '4',
                                8, '3',
                                9, '2',
                               10, '1',
                                'E')
        INTO check_digit
        FROM dual;

      --
      -- Determine if the calculated check digit is equal to the actual
      --   check digit.
      --
      actual_check_digit := UPPER(substr(ISBN, 10, 1));
   
      IF (actual_check_digit = check_digit) THEN
         return_code := 'TRUE';
      --
      -- Error handling for return value of E - Error.
      --
      elsif check_digit = 'E' then
         return_code   := 'FALSE';
         error_message := 'Error determining check digit in CHKDIG_VERIFY_ISBN stored procedure';
      ELSE
         return_code := 'FALSE';
         error_message := 'Invalid ISBN entered.';
      END IF;
   elsif L_isbn_length = 13 then
      char_value := substr(ISBN, 1, 12);

      L_counter := 1;
      L_weight := 1;
      while L_counter < 13 loop
         L_sum := L_sum + (to_number(substr(char_value, L_counter, 1)) * L_weight);
         --
         L_counter := L_counter + 1;
         if L_weight = 1 then
            L_weight := 3;
         else
            L_weight := 1;
         end if;
      end loop;

      last_digit := mod(L_sum, 10);

      select decode(last_digit, 1, 9,
                                2, 8,
                                3, 7,
                                4, 6,
                                5, 5,
                                6, 4,
                                7, 3,
                                8, 2,
                                9, 1,
                                0)
      into check_digit
      from dual;

      actual_check_digit := substr(ISBN, 13, 1);

      if actual_check_digit = check_digit then
         return_code := 'TRUE';
      else
         return_code := 'FALSE';
         error_message := 'Invalid ISBN entered.';
      end if;
   else
      return_code := 'FALSE';
      error_message := 'Invalid ISBN entered.';
   end if;

EXCEPTION
   when OTHERS then
      return_code := 'NULL';
      error_message := SQLERRM || ' from CHKDIG_VERIFY_ISBN proc.';
END CHKDIG_VERIFY_ISBN;
/



