
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-----------------------------------------------------------------------------
-- Name:    SUPPLIER_VAL
-- Purpose: Validate the supplier by seeing if it exists on file.
--   If it does then return 'TRUE' and the supplier name.
--   If it doesn't, return 'FALSE' and the error of not finding it on file.
--   If there was any other error then return 'NULL' and the error message.
-----------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE SUPPLIER_VAL (in_supplier       IN     NUMBER,
                                          out_supplier_name IN OUT VARCHAR2,
                                          return_code       IN OUT VARCHAR2,
                                          error_message     IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   cursor C_SUPPLIER_EXISTS is
      select sup_name
        from v_sups_tl
       where supplier = in_supplier;
BEGIN
   open  c_supplier_exists;
   fetch c_supplier_exists into out_supplier_name;
   if (c_supplier_exists%NOTFOUND) then
      return_code := 'FALSE';
      error_message := 'Invalid Supplier.';
   else
      return_code := 'TRUE';
   end if;
   close c_supplier_exists;
EXCEPTION
    when OTHERS then
        return_code := 'NULL';
        error_message := SQLERRM || ' from SUPPLIER_VAL proc.';
END SUPPLIER_VAL;
/


