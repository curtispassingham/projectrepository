
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PROCEDURE CHKDIG_ADD(IO_number_to_chk        IN OUT NUMBER,
                     IO_check_digit          IN OUT NUMBER,
                     I_use_check_digit_logic IN     VARCHAR2 DEFAULT NULL ) AUTHID CURRENT_USER is

L_use_check_digit_logic SYSTEM_OPTIONS.CHECK_DIGIT_IND%TYPE := I_use_check_digit_logic;

cursor C_USE_CHECK_DIGITS is
   select check_digit_ind
     from system_options;

BEGIN

   if L_use_check_digit_logic is NULL then
      open C_USE_CHECK_DIGITS;
      fetch C_USE_CHECK_DIGITS into L_use_check_digit_logic;
      close C_USE_CHECK_DIGITS;
   end if;

   if L_use_check_digit_logic = 'Y' then

      /* This statement returns the check digit appended to the number (and the
         check digit itself) or -1 if the number has no valid check digit.
      */
      select mod(system_options.cd_modulus -
                    mod( ((mod(  TRUNC(IO_number_to_chk / 1),       10)  * system_options.cd_weight_1) +
                          (mod(  TRUNC(IO_number_to_chk / 10),      10)  * system_options.cd_weight_2) +
                          (mod(  TRUNC(IO_number_to_chk / 100),     10)  * system_options.cd_weight_3) +
                          (mod(  TRUNC(IO_number_to_chk / 1000),    10)  * system_options.cd_weight_4) +
                          (mod(  TRUNC(IO_number_to_chk / 10000),   10)  * system_options.cd_weight_5) +
                          (mod(  TRUNC(IO_number_to_chk / 100000),  10)  * system_options.cd_weight_6) +
                          (mod(  TRUNC(IO_number_to_chk / 1000000), 10)  * system_options.cd_weight_7) +
                          (mod(  TRUNC(IO_number_to_chk / 10000000),10)  * system_options.cd_weight_8)),
                         system_options.cd_modulus), 10)
        into IO_check_digit
        from system_options;

      if ( IO_check_digit >= 0 ) then
         IO_number_to_chk := (IO_number_to_chk * 10) + IO_check_digit;
      end if;
   else   /* L_use_check_digit_logic != 'Y' */
      IO_check_digit := 0;
   end if;

END CHKDIG_ADD;
/


