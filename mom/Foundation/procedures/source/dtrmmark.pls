
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------
-- Name:    DETERMINE_MARK
-- Purpose: This function takes an old and new retail, and an original retail.
--          It returns the mark type:
--			11 -	MU  -	Markup
--			12 -	MUC -	Markup Cancel
--			13 -	MD  -	Markdown
--			14 -	MDC -	Markdown Cancel
--------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION determine_mark (
                                i_orig_retail	IN NUMBER,
                                i_old_retail	IN NUMBER,
                                i_new_retail	IN NUMBER,
				o_mark_type	OUT NUMBER,
				o_mark_amt	OUT NUMBER,
				o_mark_type_2	OUT NUMBER,
				o_mark_amt_2	OUT NUMBER,
                                error_message    IN OUT VARCHAR2)
                        RETURN VARCHAR2 AUTHID CURRENT_USER IS


BEGIN

o_mark_type   := 0;
o_mark_amt    := 0;
o_mark_type_2 := 0;
o_mark_amt_2  := 0;

-- No Change --
	if i_new_retail = i_old_retail then
		null;

-- Mark UP --
	elsif i_new_retail >  i_old_retail 
	and   i_old_retail >= i_orig_retail then
		o_mark_type := 11;
		o_mark_amt  := i_new_retail - i_old_retail;

-- Mark UP Cancel --
	elsif i_new_retail <  i_old_retail 
	and   i_new_retail >= i_orig_retail then
--              o_mark_type := 12;
                o_mark_type := 13;
		o_mark_amt  := i_old_retail - i_new_retail;

-- Mark DOWN --
	elsif i_new_retail <  i_old_retail 
	and   i_old_retail <= i_orig_retail then
		o_mark_type := 13;
		o_mark_amt  := i_old_retail - i_new_retail;

-- Mark DOWN Cancel --
	elsif i_new_retail >  i_old_retail 
	and   i_new_retail <= i_orig_retail then
		o_mark_type := 14;
		o_mark_amt  := i_new_retail - i_old_retail;

-- Mark DOWN Cancel --
-- and Mark UP        --
	elsif i_new_retail  >  i_orig_retail 
	and   i_orig_retail >  i_old_retail then
		o_mark_type := 14;
		o_mark_amt  := i_orig_retail - i_old_retail;
		o_mark_type_2 := 11;
		o_mark_amt_2  := i_new_retail - i_orig_retail;

-- Mark UP Cancel --
-- and Mark DOWN    --
	elsif i_new_retail  <  i_orig_retail 
	and   i_orig_retail <  i_old_retail then
--            o_mark_type := 12;
--            o_mark_amt  := i_old_retail - i_orig_retail;
--            o_mark_type_2 := 13;
--            o_mark_amt_2  := i_orig_retail - i_new_retail;
              o_mark_type := 13;
              o_mark_amt := i_old_retail - i_new_retail;

-- Should never drop through here --
        else
                error_message := 'Invalid price adjustment type in CALC_ADJUST';
                return 'FALSE';
        end if;

	return 'TRUE';
 
EXCEPTION
   WHEN OTHERS THEN
      error_message := SQLERRM || ' from function DETERMINE_MARK';
      return 'FALSE';
 
END determine_mark;
/



