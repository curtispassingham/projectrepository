
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PROCEDURE CHKDIG_VERIFY(O_check_digit   OUT NUMBER,
                        I_number_to_chk IN  NUMBER) AUTHID CURRENT_USER is

L_use_check_digit_logic SYSTEM_OPTIONS.CHECK_DIGIT_IND%TYPE;

cursor C_USE_CHECK_DIGITS is
   select check_digit_ind
     from system_options;

BEGIN

   open C_USE_CHECK_DIGITS;
   fetch C_USE_CHECK_DIGITS into L_use_check_digit_logic;
   close C_USE_CHECK_DIGITS;

   if L_use_check_digit_logic = 'Y' then

      /*  This statement returns 0 if the number with it's check digit is valid
          else -1
      */
      select decode(
               mod(system_options.cd_modulus -
                      mod( ((mod(  TRUNC(I_number_to_chk / 10),       10)  * system_options.cd_weight_1) +
                            (mod(  TRUNC(I_number_to_chk / 100),      10)  * system_options.cd_weight_2) +
                            (mod(  TRUNC(I_number_to_chk / 1000),     10)  * system_options.cd_weight_3) +
                            (mod(  TRUNC(I_number_to_chk / 10000),    10)  * system_options.cd_weight_4) +
                            (mod(  TRUNC(I_number_to_chk / 100000),   10)  * system_options.cd_weight_5) +
                            (mod(  TRUNC(I_number_to_chk / 1000000),  10)  * system_options.cd_weight_6) +
                            (mod(  TRUNC(I_number_to_chk / 10000000), 10)  * system_options.cd_weight_7) +
                            (mod(  TRUNC(I_number_to_chk / 100000000),10)  * system_options.cd_weight_8)),
                           system_options.cd_modulus), 10),
               mod(I_number_to_chk, 10), 0,
               -1)
          into O_check_digit
          from system_options;

   else   /* L_use_check_digit_logic != 'Y' */
      O_check_digit := 0;
   end if;

END CHKDIG_VERIFY;
/


