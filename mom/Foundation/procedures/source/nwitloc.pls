---------------------------------------------------------------------------
-- Name:    NEW_ITEM_LOC
-- Purpose: This function is used to create new location records for items.
--
--          The item that is passed to this program must exist in Retek but
--          should not have an item/location record.
--
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION NEW_ITEM_LOC ( O_error_message         IN OUT VARCHAR2,
                                          I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                                          I_location              IN     ITEM_LOC.LOC%TYPE,
                                          I_item_parent           IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                                          I_item_grandparent      IN     ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                                          I_loc_type              IN     ITEM_LOC.LOC_TYPE%TYPE,
                                          I_short_desc            IN     ITEM_MASTER.SHORT_DESC%TYPE,
                                          I_dept                  IN     ITEM_MASTER.DEPT%TYPE,
                                          I_class                 IN     ITEM_MASTER.CLASS%TYPE,
                                          I_subclass              IN     ITEM_MASTER.SUBCLASS%TYPE,
                                          I_item_level            IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                                          I_tran_level            IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                                          I_item_status           IN     ITEM_MASTER.STATUS%TYPE,
                                          I_waste_type            IN     ITEM_MASTER.WASTE_TYPE%TYPE,
                                          I_daily_waste_pct       IN     ITEM_LOC.DAILY_WASTE_PCT%TYPE,
                                          I_sellable_ind          IN     ITEM_MASTER.SELLABLE_IND%TYPE,
                                          I_orderable_ind         IN     ITEM_MASTER.ORDERABLE_IND%TYPE,
                                          I_pack_ind              IN     ITEM_MASTER.PACK_IND%TYPE,
                                          I_pack_type             IN     ITEM_MASTER.PACK_TYPE%TYPE,
                                          I_unit_cost_loc         IN     ITEM_LOC_SOH.UNIT_COST%TYPE,
                                          I_unit_retail_loc       IN     ITEM_LOC.UNIT_RETAIL%TYPE,
                                          I_selling_retail_loc    IN     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                          I_selling_uom           IN     ITEM_LOC.SELLING_UOM%TYPE,
                                          I_item_loc_status       IN     ITEM_LOC.STATUS%TYPE,
                                          I_taxable_ind           IN     ITEM_LOC.TAXABLE_IND%TYPE,
                                          I_ti                    IN     ITEM_LOC.TI%TYPE,
                                          I_hi                    IN     ITEM_LOC.HI%TYPE,
                                          I_store_ord_mult        IN     ITEM_LOC.STORE_ORD_MULT%TYPE,
                                          I_meas_of_each          IN     ITEM_LOC.MEAS_OF_EACH%TYPE,
                                          I_meas_of_price         IN     ITEM_LOC.MEAS_OF_PRICE%TYPE,
                                          I_uom_of_price          IN     ITEM_LOC.UOM_OF_PRICE%TYPE,
                                          I_primary_variant       IN     ITEM_LOC.PRIMARY_VARIANT%TYPE,
                                          I_primary_supp          IN     ITEM_LOC.PRIMARY_SUPP%TYPE,
                                          I_primary_cntry         IN     ITEM_LOC.PRIMARY_CNTRY%TYPE,
                                          I_local_item_desc       IN     ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
                                          I_local_short_desc      IN     ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
                                          I_primary_cost_pack     IN     ITEM_LOC.PRIMARY_COST_PACK%TYPE,
                                          I_receive_as_type       IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                                          I_date                  IN     DATE,
                                          I_default_to_children   IN     BOOLEAN,
                                          I_like_store            IN     ITEM_LOC.LOC%TYPE DEFAULT NULL,
                                          I_item_desc             IN     ITEM_MASTER.ITEM_DESC%TYPE DEFAULT NULL,
                                          I_diff_1                IN     ITEM_MASTER.DIFF_1%TYPE DEFAULT NULL,
                                          I_diff_2                IN     ITEM_MASTER.DIFF_2%TYPE DEFAULT NULL,
                                          I_diff_3                IN     ITEM_MASTER.DIFF_3%TYPE DEFAULT NULL,
                                          I_diff_4                IN     ITEM_MASTER.DIFF_4%TYPE DEFAULT NULL,
                                          I_lang                  IN     STORE.LANG%TYPE DEFAULT NULL,
                                          I_class_vat_ind         IN     CLASS.CLASS_VAT_IND%TYPE DEFAULT NULL,
                                          I_elc_ind               IN     SYSTEM_OPTIONS.ELC_IND%TYPE DEFAULT NULL,
                                          I_std_av_ind            IN     SYSTEM_OPTIONS.STD_AV_IND%TYPE DEFAULT NULL,
                                          I_vat_ind               IN     VARCHAR2 DEFAULT NULL,--Column has been made obsolete
                                          I_rpm_ind               IN     VARCHAR2 DEFAULT NULL,
                                          I_inbound_handling_days IN   ITEM_LOC.INBOUND_HANDLING_DAYS%TYPE DEFAULT NULL,
                                          I_group_type            IN     CODE_DETAIL.CODE%TYPE DEFAULT NULL,
                                          I_store_price_ind       IN     ITEM_LOC.STORE_PRICE_IND%TYPE DEFAULT NULL,
                                          I_uin_type              IN     ITEM_LOC.UIN_TYPE%TYPE     DEFAULT NULL,
                                          I_uin_label             IN     ITEM_LOC.UIN_LABEL%TYPE    DEFAULT NULL,
                                          I_capture_time          IN     ITEM_LOC.CAPTURE_TIME%TYPE DEFAULT NULL,
                                          I_ext_uin_ind           IN     ITEM_LOC.EXT_UIN_IND%TYPE  DEFAULT 'N',
                                          I_source_method         IN     ITEM_LOC.SOURCE_METHOD%TYPE DEFAULT NULL,
                                          I_source_wh             IN     ITEM_LOC.SOURCE_WH%TYPE DEFAULT NULL,
                                          I_new_loc_ind           IN     VARCHAR2 DEFAULT NULL,
                                          I_ranged_ind            IN     ITEM_LOC.RANGED_IND%TYPE DEFAULT 'N',
                                          I_costing_loc           IN     ITEM_LOC.COSTING_LOC%TYPE DEFAULT NULL,
                                          I_costing_loc_type      IN     ITEM_LOC.COSTING_LOC_TYPE%TYPE DEFAULT NULL)
   RETURN BOOLEAN AUTHID CURRENT_USER IS

   L_program                 VARCHAR2(30)                          := 'NEW_ITEM_LOC';
   L_hier_level              NIL_INPUT_WORKING.HIER_LEVEL%TYPE     := NULL;
   L_hier_value              NIL_INPUT_WORKING.HIER_NUM_VALUE%TYPE := NULL;
   L_loc                     ITEM_LOC.LOC%TYPE                     := NULL;
   L_loc_type                ITEM_LOC.LOC_TYPE%TYPE                := NULL;
   L_owner                   VARCHAR2(50)                          := NULL;
   L_called_name             VARCHAR2(50)                          := NULL;
   L_line                    NUMBER                                := NULL;
   L_caller_type             VARCHAR2(50)                          := NULL;
   L_default_to_children_ind VARCHAR2(1)                           := 'N';
   L_input_rec               NEW_ITEM_LOC_SQL.nil_input_record;
   L_input                   NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;
   L_nil_found NUMBER := 0;
   
   cursor C_LOC_TYPE is 
     select loc_type 
       from(  
            select store loc, 'S'  loc_type 
              from store 
             where store = I_location 
           union 
            select wh loc, 'W' loc_type 
              from wh 
             where wh = I_location 
                   union 
            select to_number(partner_id) loc, 'E' loc_type 
              from partner 
             where partner_id = I_location 
               and partner_type = 'E') 
      where loc = I_location; 

BEGIN
   ---
   SYS.OWA_UTIL.who_called_me( L_owner ,L_called_name ,L_line, L_caller_type );
   if L_called_name is not null then
      NEW_ITEM_LOC_SQL.GV_who_called_me := L_called_name;
   end if;
   ---
   SELECT COUNT(*) INTO L_nil_found   
    FROM ITEM_LOC  
   WHERE item = I_item  
     AND loc = I_location  
     AND ROWNUM = 1;  
  
   IF L_nil_found = 1 THEN
      Return TRUE;
   END IF;
   ---
   if I_group_type is NOT NULL and I_group_type NOT IN ('S', 'W', 'I', 'E') then
      L_hier_level := I_group_type;
      L_hier_value := I_location;
      L_loc        := NULL;
      L_loc_type   := NULL;
   else
      L_hier_level := NULL;
      L_hier_value := NULL;
      L_loc        := I_location;

      if I_loc_type IS NULL then 
         open  C_LOC_TYPE; 
         fetch C_LOC_TYPE into L_loc_type; 
         close C_LOC_TYPE;
      else
         L_loc_type   := I_loc_type;
      end if;
   end if;
   ---
   if I_default_to_children then
      L_default_to_children_ind := 'Y';
   else
      L_default_to_children_ind := 'N';
   end if;
   ---
   L_input_rec.item                    := I_item;
   L_input_rec.item_parent             := I_item_parent;
   L_input_rec.item_grandparent        := I_item_grandparent;
   L_input_rec.item_desc               := I_item_desc;
   L_input_rec.item_short_desc         := I_short_desc;
   L_input_rec.dept                    := I_dept;
   L_input_rec.item_class              := I_class;
   L_input_rec.subclass                := I_subclass;
   L_input_rec.item_level              := I_item_level;
   L_input_rec.tran_level              := I_tran_level;
   L_input_rec.item_status             := I_item_status;
   L_input_rec.waste_type              := I_waste_type;
   L_input_rec.sellable_ind            := I_sellable_ind;
   L_input_rec.orderable_ind           := I_orderable_ind;
   L_input_rec.pack_ind                := I_pack_ind;
   L_input_rec.pack_type               := I_pack_type;
   L_input_rec.diff_1                  := I_diff_1;
   L_input_rec.diff_2                  := I_diff_2;
   L_input_rec.diff_3                  := I_diff_3;
   L_input_rec.diff_4                  := I_diff_4;
   L_input_rec.loc                     := L_loc;
   L_input_rec.loc_type                := L_loc_type;
   L_input_rec.daily_waste_pct         := I_daily_waste_pct;
   L_input_rec.unit_cost_loc           := I_unit_cost_loc;
   L_input_rec.unit_retail_loc         := I_unit_retail_loc;
   L_input_rec.selling_retail_loc      := I_selling_retail_loc;
   L_input_rec.selling_uom             := I_selling_uom;
   L_input_rec.multi_units             := NULL;
   L_input_rec.multi_unit_retail       := NULL;
   L_input_rec.multi_selling_uom       := NULL;
   L_input_rec.item_loc_status         := I_item_loc_status;
   L_input_rec.taxable_ind             := I_taxable_ind;
   L_input_rec.ti                      := I_ti;
   L_input_rec.hi                      := I_hi;
   L_input_rec.store_ord_mult          := I_store_ord_mult;
   L_input_rec.meas_of_each            := I_meas_of_each;
   L_input_rec.meas_of_price           := I_meas_of_price;
   L_input_rec.uom_of_price            := I_uom_of_price;
   L_input_rec.primary_variant         := I_primary_variant;
   L_input_rec.primary_supp            := I_primary_supp;
   L_input_rec.primary_cntry           := I_primary_cntry;
   L_input_rec.local_item_desc         := I_local_item_desc;
   L_input_rec.local_short_desc        := I_local_short_desc;
   L_input_rec.primary_cost_pack       := I_primary_cost_pack;
   L_input_rec.receive_as_type         := I_receive_as_type;
   L_input_rec.store_price_ind         := I_store_price_ind;
   L_input_rec.uin_type                := I_uin_type;
   L_input_rec.uin_label               := I_uin_label;
   L_input_rec.capture_time            := I_capture_time;
   L_input_rec.ext_uin_ind             := I_ext_uin_ind;
   L_input_rec.source_method           := I_source_method;
   L_input_rec.source_wh               := I_source_wh;
   L_input_rec.inbound_handling_days   := I_inbound_handling_days;
   L_input_rec.CURRENCY_CODE           := NULL;
   L_input_rec.like_store              := I_like_store;
   L_input_rec.default_to_children_ind := L_default_to_children_ind;
   L_input_rec.class_vat_ind           := I_class_vat_ind;
   L_input_rec.hier_level              := L_hier_level;
   L_input_rec.hier_num_value          := L_hier_value;
   L_input_rec.hier_char_value         := NULL;
   L_input_rec.costing_loc             := I_costing_loc;
   L_input_rec.costing_loc_type        := I_costing_loc_type;
   L_input_rec.ranged_ind              := I_ranged_ind;
   L_input_rec.default_wh              := NULL;
   L_input_rec.item_loc_ind            := NULL;
   --
   L_input(1) := L_input_rec;
   ---
   if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message,
                                    L_input) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return FALSE;
END;
/
