
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
---------------------------------------------------------------------------------
-- Name:    NEXT_SKULIST_NUMBER
-- Purpose: This SKU list number sequence generator will return to the calling 
--          program/procedure.  Upon success (TRUE) a SKU list number with a 
--          verified check digit attached.  Upon failure (FALSE) an appropriate 
--          error message for display purposes by the calling program/procedure.
---------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_SKULIST_NUMBER (O_list_number   IN OUT skulist_head.skulist%TYPE,
                                                 O_return_code   IN OUT VARCHAR2,
                                                 O_error_message IN OUT VARCHAR2) AUTHID CURRENT_USER IS

   L_list_sequence          skulist_head.skulist%TYPE;
   L_wrap_sequence_number   skulist_head.skulist%TYPE;
   L_check_digit            NUMBER(1);
   L_first_time             VARCHAR2(3) := 'Yes';
   L_dummy                  VARCHAR2(1);

   cursor C_LIST_EXISTS(list_number_param NUMBER) is
      select 'x'
         from skulist_head
         where skulist_head.skulist = list_number_param;

   cursor C_LIST_SEQUENCE is
     select list_sequence.NEXTVAL
       from sys.dual;

BEGIN
    LOOP
        open C_LIST_SEQUENCE;
        fetch C_LIST_SEQUENCE into L_list_sequence;
        close C_LIST_SEQUENCE;

        if (L_first_time = 'Yes') then
            L_wrap_sequence_number := L_list_sequence;
            L_first_time := 'No';
        elsif (L_list_sequence = L_wrap_sequence_number) then
            O_error_message := 'Fatal error - no available SKU list numbers';
            O_return_code := 'FALSE';
            exit;
        end if;

        O_list_number := L_list_sequence;
        CHKDIG_ADD(O_list_number, L_check_digit);

        if (L_check_digit >= 0) then
            open  c_list_exists(O_list_number);
            fetch c_list_exists into L_dummy;
            if (c_list_exists%notfound) then
                O_return_code := 'TRUE';
                close c_list_exists;
                exit;
            end if;
            close c_list_exists;
        end if;
    END LOOP;
EXCEPTION
    when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              'NEXT_SKULIST_NO',
                                              to_char(SQLCODE));
        O_return_code := 'FALSE';
END NEXT_SKULIST_NUMBER;
/


