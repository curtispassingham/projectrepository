
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
---------------------------------------------------------------------------
-- Name:    REGION_VAL
-- Purpose: Validate the region by seeing if it exists on file.
--          If it does then return 'TRUE' and the region name.
--          If it doesn't, return 'FALSE' and the error of not finding it on file.
--          If there was any other error then return 'NULL' and the error message. 
----------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE REGION_VAL (in_region       IN     NUMBER,
                                        out_region_name IN OUT VARCHAR2,
                                        return_code     IN OUT VARCHAR2,
                                        error_message   IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   cursor C_REGION_EXISTS is
      SELECT region_name 
        FROM v_region_tl
       WHERE region = in_region;
BEGIN
   open  c_region_exists;
   fetch c_region_exists into out_region_name;
   if c_region_exists%NOTFOUND then
      return_code := 'FALSE';
      error_message := 'Invalid Region Number entered.';
   else
      return_code := 'TRUE';
   end if;
   close c_region_exists;
EXCEPTION
    when OTHERS then
        return_code := 'NULL';
        error_message := SQLERRM || ' from REGION_VAL proc.';
END REGION_VAL;
/


