
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
----------------------------------------------------------------------------
-- Name:    RPM_CURRENCY_FUNCTION
-- Purpose: Contains a call to currency_sql.convert but with the result
--           sent back as the function's results.  This is needed so the
--           conversion can be done in a cursor.
--
--          If there is an error, the return value will be -999
----------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION RPM_CURRENCY_FUNCTION  (I_currency_value            IN      NUMBER,
                                                   I_cost_retail_ind           IN      VARCHAR2,
                                                   I_currency_code_in          IN      CURRENCY_RATES.CURRENCY_CODE%TYPE,
                                                   I_currency_code_out         IN      CURRENCY_RATES.CURRENCY_CODE%TYPE)
RETURN NUMBER AUTHID CURRENT_USER AS

   L_error_message  VARCHAR2(255);
   L_currency_value future_cost.pricing_cost%TYPE := NULL;
BEGIN

   if CURRENCY_SQL.CONVERT(L_error_message,
                           I_currency_value,
                           I_currency_code_in,
                           I_currency_code_out,
                           L_currency_value,
                           I_cost_retail_ind,
                           NULL,
                           NULL) = FALSE then
      raise_application_error(-20000, 'Currency conversion error');

   end if;

   return L_currency_value;

EXCEPTION
   when OTHERS then
      raise;

END RPM_CURRENCY_FUNCTION;
/
