
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
---------------------------------------------------------------------
-- Name:    GET_VDATE_STR
-- Purpose: This function returns the current vdate from table period
--          The return string is 6 chars <format = 'mmddyy'>.
--          NO parameters are passed into this function.
--          If there is an error then the NULL is sent back out.
----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION get_vdate_str RETURN VARCHAR2 AUTHID CURRENT_USER AS 
   L_vdate_str varchar2(6) := null;
   L_vdate     date        := null;
begin
   --
   L_vdate := GET_VDATE;
   --
   if L_vdate is not null then
      L_vdate_str := to_char(L_vdate,'mmddyy');
   end if;
   --
   return ( L_vdate_str );
   --
end;
/


