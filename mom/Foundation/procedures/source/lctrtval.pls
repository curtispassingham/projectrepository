
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
---------------------------------------------------------------------------
-- Name:    LOC_TRAIT_VAL
-- Purpose: Validate the location trait by seeing if it exists on file.
--          If it does then return 'TRUE' and the location name.      
--          If it doesn't, return 'FALSE' and the error of not finding it on file.
--          If there was any other error then return 'NULL' and the error message.
----------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE LOC_TRAIT_VAL (in_trait       IN     NUMBER,
                                           out_trait_name IN OUT VARCHAR2,
                                           return_code    IN OUT VARCHAR2,
                                           error_message  IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   cursor C_TRAIT_EXISTS is
      SELECT description
          FROM v_loc_traits_tl
          WHERE loc_trait = in_trait; 
BEGIN
   open  c_trait_exists;
   fetch c_trait_exists into out_trait_name;      
   if (c_trait_exists%NOTFOUND) then
      return_code := 'FALSE';
      error_message := 'INV_LOC_TRAIT';
   else
      return_code := 'TRUE';
   end if;
   close c_trait_exists;              
EXCEPTION
    when OTHERS then
        return_code := 'NULL';
        error_message := SQLERRM || ' from LOC_TRAIT_VAL proc.';        
END LOC_TRAIT_VAL; 
/


