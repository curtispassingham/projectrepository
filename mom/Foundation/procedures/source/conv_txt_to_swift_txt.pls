-------------------------------------------------------------------
-- Name:    CONV_TXT_TO_SWIFT_TXT
-- Purpose: This function removes all unsupported SWIFT characters
--          from the input string and sends a clean SWIFT
--          supported text result.
-------------------------------------------------------------------
create or replace function CONV_TXT_TO_SWIFT_TXT (I_text       IN VARCHAR2,
                                                  I_msg_frmt   IN VARCHAR2)
  RETURN VARCHAR2 AUTHID CURRENT_USER IS
    L_inp_txt VARCHAR2 (4000) := '';
  BEGIN
    L_inp_txt := I_text;
    
    if I_msg_frmt = 'X' then
      L_inp_txt := replace (L_inp_txt, '=', '');
      L_inp_txt := replace (L_inp_txt, '!', '');
      L_inp_txt := replace (L_inp_txt, '%', '');
      L_inp_txt := replace (L_inp_txt, '&', '');
      L_inp_txt := replace (L_inp_txt, '*', '');
      L_inp_txt := replace (L_inp_txt, '@', '');
      L_inp_txt := replace (L_inp_txt, '#', '');

      /* Text not supported in SWIFT X standard */
      L_inp_txt := replace (L_inp_txt, '"', '');
      L_inp_txt := replace (L_inp_txt, '<', '');
      L_inp_txt := replace (L_inp_txt, '>', '');
      L_inp_txt := replace (L_inp_txt, ';', '');
    elsif I_msg_frmt = 'Y' then
      L_inp_txt := replace (L_inp_txt, '@', '');
      L_inp_txt := replace (L_inp_txt, '#', '');

      /* Text not supported in SWIFT Y standard */
      L_inp_txt := regexp_replace(L_inp_txt, '([a-z]*)', '');
      L_inp_txt := replace (L_inp_txt, '{', '');
      L_inp_txt := replace (L_inp_txt, '}', '');
      L_inp_txt := replace (L_inp_txt, chr(10), '');
      L_inp_txt := replace (L_inp_txt, chr(13), '');
    elsif I_msg_frmt = 'Z' then
      /* Text not supported in SWIFT Z standard */
      L_inp_txt := replace (L_inp_txt, '}', '');
    end if;
    
    return L_inp_txt;
  EXCEPTION
    WHEN OTHERS THEN
      L_inp_txt := '';
      RETURN L_inp_txt;
  END;
/