
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
---------------------------------------------------------------------------------
-- Name:    NEXT_LOCLIST_NUMBER
-- Purpose: This location list number sequence generator will return to the calling 
--          program/procedure.  Upon success (TRUE) a SKU list number with a 
--          verified check digit attached.  Upon failure (FALSE) an appropriate 
--          error message for display purposes by the calling program/procedure.
---------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_LOCLIST_NUMBER(O_error_message IN OUT VARCHAR2,
                                                O_list_number   IN OUT LOC_LIST_HEAD.LOC_LIST%TYPE,
                                                O_return_code   IN OUT VARCHAR2) AUTHID CURRENT_USER IS

   L_list_sequence          LOC_LIST_HEAD.LOC_LIST%TYPE;
   L_wrap_sequence_number   LOC_LIST_HEAD.LOC_LIST%TYPE;
   L_first_time             VARCHAR2(3) := 'Yes';
   L_dummy                  VARCHAR2(1);

   cursor C_LIST_EXISTS is
      select 'x'
        from loc_list_head
       where loc_list = O_list_number;

   cursor C_LOC_LIST_SEQUENCE is
      select loc_list_sequence.NEXTVAL
        from sys.dual;

BEGIN
   LOOP
      open C_LOC_LIST_SEQUENCE;
      fetch C_LOC_LIST_SEQUENCE into L_list_sequence;
      close C_LOC_LIST_SEQUENCE;

      if L_first_time = 'Yes' then
         L_wrap_sequence_number := L_list_sequence;
         L_first_time           := 'No';
      elsif L_list_sequence = L_wrap_sequence_number then
         O_error_message := SQL_LIB.CREATE_MSG('NO_AVAIL_LL',NULL,NULL,NULL); 
         O_return_code   := 'FALSE';
         EXIT;
      end if;

      O_list_number      := L_list_sequence;

      open  C_LIST_EXISTS;
      fetch C_LIST_EXISTS into L_dummy;
      if C_LIST_EXISTS%NOTFOUND then
         O_return_code := 'TRUE';
         close C_LIST_EXISTS;
         EXIT;
      end if;
      close C_LIST_EXISTS;
   END LOOP;
EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'NEW_LOCLIST_NUMBER',
                                            to_char(SQLCODE));
      O_return_code := 'FALSE';
END NEXT_LOCLIST_NUMBER;
/
