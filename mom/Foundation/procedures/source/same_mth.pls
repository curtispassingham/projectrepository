
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-------------------------------------------------------------------------
-- Name:    SAME_MONTH
-- Purpose: This procedure accepts two dates in 'MMDDYYYY' format and compares
--          them to make sure that they are in the same 454 month and year.
-------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE SAME_MONTH (in_date1      IN     VARCHAR2,
                                        in_date2      IN     VARCHAR2,
                                        return_code   IN OUT VARCHAR2,
                                        error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS

   /* User defined exceptions */
   ABORTING EXCEPTION;

   /* Local variables */
   cal_454_ind     VARCHAR2(1);
   work_day        NUMBER(2);
   work_month      NUMBER(2);
   work_year       NUMBER(4);
   out_dd_454      NUMBER(1);
   out_wk_454      NUMBER(1);
   out_mm_454      NUMBER(2);
   out_yyyy_454    NUMBER(4);
   date_1_month    NUMBER(2);
   date_1_year     NUMBER(4);

BEGIN
   return_code := 'TRUE';
   error_message := NULL;

   SELECT DECODE(system_options.calendar_454_ind, 'C', 1, 0)
     INTO cal_454_ind
     FROM system_options;

   work_day   := TO_NUMBER(TO_CHAR(TO_DATE(in_date1, 'MMDDYY'), 'DD'));
   work_month := TO_NUMBER(TO_CHAR(TO_DATE(in_date1, 'MMDDYY'), 'MM'));
   work_year  := TO_NUMBER(TO_CHAR(TO_DATE(in_date1, 'MMDDYY'), 'YYYY'));

   IF (cal_454_ind = 1) THEN
      date_1_month := work_month;
      date_1_year  := work_year;
   ELSE
      CAL_TO_454(work_day, work_month, work_year,
                 out_dd_454, out_wk_454, out_mm_454, out_yyyy_454,
                 return_code, error_message);
      IF (return_code = 'FALSE') THEN
         /* Override the returned error with the following. */
         error_message := 'Date conversion error.' || ' from SAME_MONTH proc.';
         raise ABORTING;
      END IF;
      date_1_month := out_mm_454;
      date_1_year  := out_yyyy_454;
   END IF;

   work_day   := TO_NUMBER(TO_CHAR(TO_DATE(in_date2, 'MMDDYY'), 'DD'));
   work_month := TO_NUMBER(TO_CHAR(TO_DATE(in_date2, 'MMDDYY'), 'MM'));
   work_year  := TO_NUMBER(TO_CHAR(TO_DATE(in_date2, 'MMDDYY'), 'YYYY'));
   IF (cal_454_ind = 1) THEN
      out_mm_454   := work_month;
      out_yyyy_454 := work_year;
   ELSE
      CAL_TO_454(work_day, work_month, work_year,
                 out_dd_454, out_wk_454, out_mm_454, out_yyyy_454,
                 return_code,
                 error_message);
      IF (return_code = 'FALSE') THEN
         /* Override the returned error with the following. */
         error_message := 'Date conversion error.' || ' from SAME_MONTH proc.';
         raise ABORTING;
      END IF;
   END IF;

   IF (date_1_month != out_mm_454)   OR
      (date_1_year  != out_yyyy_454) THEN
      return_code := 'FALSE';
   END IF;

EXCEPTION
   WHEN ABORTING THEN
      NULL;
   WHEN OTHERS THEN
      error_message := SQLERRM || ' from SAME_MONTH proc.';
      return_code := 'FALSE';
END SAME_MONTH;
/


