-----------------------------------------------------------------------------------
-- Name:    CAL_TO_454
-- Purpose: This procedure validates a calendar day, month and year and then
--          converts it a 454 format.  When this existed as a User exit, there
--          were 2 methods for calculating the 454 date.  Via an algorithim which
--          calculated the 454 date or via a table where the user's would determine 
--          and then perform data entry of the 454 format.
--
--          This procedure simply uses the table method, where the users have
--          punched all the 454 dates for us.  The algorithim is no longer used
--          and exists only as a user exit for reference in case we decide to revert
--          back to the algorithim method.
-----------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CAL_TO_454 (in_dd         IN     NUMBER,
                                        in_mm         IN     NUMBER,
                                        in_yyyy       IN     NUMBER,
                                        out_dd_454    IN OUT NUMBER,
                                        out_wk_454    IN OUT NUMBER,
                                        out_mm_454    IN OUT NUMBER,
                                        out_yyyy_454  IN OUT NUMBER,
                                        return_code   IN OUT VARCHAR2,
                                        error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS

   ABORTING  EXCEPTION;
   cal_date  DATE;
   first_day DATE;
   days      NUMBER;
   v_wk_454  NUMBER(5);

BEGIN
   VALIDATE_CAL(in_dd, in_mm, in_yyyy, return_code, error_message);
   if (return_code != 'FALSE') then

      cal_date := to_date(to_char(in_dd, '09') || to_char(in_mm, '09') ||
                          to_char(in_yyyy, '0999'), 'DDMMYYYY');

      begin
         select cal_date - calendar.first_day,
                calendar.month_454,
                calendar.year_454
           into days,
                out_mm_454,
                out_yyyy_454
           from calendar
           where calendar.first_day = 
                (select max(calendar.first_day)
                 from calendar
                 where calendar.first_day <= cal_date);

         v_wk_454 := trunc((days / 7)) + 1;

         if v_wk_454 > 5 then 
            raise NO_DATA_FOUND;
         end if;

         out_wk_454 := v_wk_454;

         out_dd_454 := days - ((out_wk_454 - 1) * 7) + 1;
      exception
         when OTHERS then
            error_message := 'CALENDAR table out of range, no data found ' ||
                             'see your DBA';
            raise ABORTING;
      end;

   end if;

   return_code := 'TRUE';
   
EXCEPTION
    when ABORTING then
        return_code := 'FALSE';
        error_message := error_message || ' from CAL_TO_454';
    when OTHERS then
        error_message := SQLERRM || ' from CAL_TO_454';
        return_code := 'FALSE';
END CAL_TO_454;
/


