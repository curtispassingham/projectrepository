
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PROCEDURE CHKDIG_VERIFY_UCC(O_error_message          OUT VARCHAR2,
                            O_return_code            OUT VARCHAR2,
                            I_number_to_chk IN     VARCHAR2) AUTHID CURRENT_USER is

   L_temp_number        NUMBER(17)   := 0;
   L_number_len         NUMBER(2)    := 0;
   L_counter            NUMBER(2)    := 0;
   L_char_value         VARCHAR2(17) := NULL;
   L_even_digits        NUMBER(4)    := 0;
   L_odd_digits         NUMBER(4)    := 0;
   L_last_digit         NUMBER(4)    := 0;
   L_check_digit        NUMBER(4)    := 0;
   L_actual_check_digit NUMBER(4)    := 0;
   L_program  VARCHAR2(50)           := 'CHKDIG_VERIFY_UCC';
BEGIN

   L_number_len := length(I_number_to_chk);

   if L_number_len != 8 AND L_number_len != 12 AND L_number_len != 13 AND L_number_len != 14 AND L_number_len != 18 THEN
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_UCC_LEN',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      O_return_code := 'FALSE';
      return;
   end if;

   /* Drop off the check digit from the argument UCC number. */
   L_temp_number := trunc(to_number(I_number_to_chk) / 10);
   /* Left pad the test number with 0's to make it max UCC length
      (14 minus the check digit = 13).
   */
   L_char_value  := lpad(to_char(L_temp_number), 17, '0');

   /* Get each of the odd placed digits starting on the right,
      summing them together and then multiplying the result by 3.
      (Note that in the original UCC number, these are the even
      digits like 2, 4, 6, 8, etc.)
   */
   L_counter := 17;
   WHILE (L_counter > 17-L_number_len) LOOP
      L_odd_digits := L_odd_digits + to_number(substr(L_char_value, L_counter, 1));
      L_counter := L_counter - 2;
   END LOOP;
   L_odd_digits := L_odd_digits * 3;

   /* Get each of the even placed digits, summing them together.
      (Note that in the original UCC number, these are the odd
      digits like 1, 3, 5, 7, etc.)
   */
   L_counter := 16;
   WHILE (L_counter > 18-L_number_len) LOOP
      L_even_digits := L_even_digits + to_number(substr(L_char_value, L_counter, 1));
      L_counter := L_counter - 2;
   END LOOP;

   /* Add the even and odd digits together and MOD the result by 10
      to get the last digit of the result (if the MOD returns itself, it
      will be handled by the next step).
   */
   L_last_digit := mod(L_odd_digits + L_even_digits, 10);

   /* Decode the last digit to determine what would be required to make
      the last digit equal to 10.  If the last digit is not from 1 to 9
      then it is assumed that the last digit was a 10 (or actually it was
      set to (odd_digits + even_digits).
   */
   select decode(L_last_digit, 1, 9,
                               2, 8,
                               3, 7,
                               4, 6,
                               5, 5,
                               6, 4,
                               7, 3,
                               8, 2,
                               9, 1,
                               0)
   into L_check_digit
   from dual;


   /* Determine if the calculated check digit is equal to the actual
      check digit.
   */
   L_actual_check_digit := mod(I_number_to_chk, 10);
   if (L_actual_check_digit > 9) then
       L_actual_check_digit := 0;
   end if;

   if (L_actual_check_digit = L_check_digit) then
       O_return_code := 'TRUE';
   else
      O_return_code := 'FALSE';
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_CHK_DIG',
                                                  NULL,
                                                  NULL,
                                                  NULL);
   end if;
EXCEPTION
   when OTHERS then
      O_return_code := 'NULL';
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
END CHKDIG_VERIFY_UCC;
/


