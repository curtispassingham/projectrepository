
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PROCEDURE UPC_E_EXPAND(O_error_message  OUT VARCHAR2,
                       O_return_code    OUT VARCHAR2,
                       O_upc_a          OUT VARCHAR2,
                       I_upc_e          IN  VARCHAR2) AUTHID CURRENT_USER is

   L_char_value         VARCHAR2(8)  := NULL;
   L_actual_char        VARCHAR2(12) := 0;

BEGIN
   L_char_value := I_upc_e;

   if (to_number(substr(L_char_value, 7, 1)) between 0 and 2) then
      L_actual_char := '0'                         ||
                        substr(L_char_value, 2, 2) ||
                        substr(L_char_value, 7, 1) ||
                        '0000'                     ||
                        substr(L_char_value, 4, 3) ||
                        substr(L_char_value, 8, 1);
   elsif (to_number(substr(L_char_value, 7, 1)) = 3) then
      L_actual_char := '0'                        ||
                       substr(L_char_value, 2, 3) ||
                       '00000'                    ||
                       substr(L_char_value, 5, 2) ||
                       substr(L_char_value, 8, 1);
   elsif (to_number(substr(L_char_value, 7, 1)) = 4) then
      L_actual_char := '0'                        ||
                       substr(L_char_value, 2, 4) ||
                       '00000'                    ||
                       substr(L_char_value, 6, 1) ||
                       substr(L_char_value, 8, 1);
   else
      L_actual_char := '0'                        ||
                       substr(L_char_value, 2, 5) ||
                       '0000'                     ||
                       substr(L_char_value, 7, 1) ||
                       substr(L_char_value, 8, 1);
   end if;

   O_upc_a := L_actual_char;

   CHKDIG_VERIFY_UCC(O_error_message, O_return_code, O_upc_a);

EXCEPTION
   when OTHERS then
      O_return_code := 'NULL';
      O_error_message := SQLERRM || ' from UPC_E_EXPAND proc.';
END UPC_E_EXPAND;
/


