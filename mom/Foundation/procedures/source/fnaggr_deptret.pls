SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
---------------------------------------------------------------------------
-- Name:    FN_AGGR_Y_DEPT_RET
-- Purpose: This function is used to return the value of department 
--          only if the Aggegage indicator passed is Y else it 
--          returns NULL value.
--
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION FN_AGGR_Y_DEPT_RET(I_item_aggregate_ind   IN VARCHAR2,
                                              I_dept                 IN NUMBER)
     RETURN NUMBER
     DETERMINISTIC
IS
BEGIN
   if I_item_aggregate_ind = 'Y' then
      return I_dept;
   else
      return NULL;
   end if;
END;
/
