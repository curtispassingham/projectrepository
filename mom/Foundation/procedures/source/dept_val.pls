
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
----------------------------------------------------------------------------
-- Name:    DEPT_VAL
-- Purpose: Validate the dept by seeing if it exists on file.
--          If it does then return 'TRUE' and the dept name.
--          If it doesn't, return 'FALSE' and the error of not finding it on file.
--          If there was any other error then return 'NULL' and the error message.
-----------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE DEPT_VAL (in_dept       IN     NUMBER,
                                      out_dept_name IN OUT VARCHAR2,
                                      return_code   IN OUT VARCHAR2,
                                      error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   cursor C_DEPT_EXISTS is
      SELECT dept_name
          FROM v_deps_tl
          WHERE dept = in_dept;
BEGIN
   open  c_dept_exists;
   fetch c_dept_exists into out_dept_name;
   if (c_dept_exists%NOTFOUND) then
      return_code := 'FALSE';
      error_message := 'Invalid Department Number.';
   else
      return_code := 'TRUE';
   end if;
   close c_dept_exists;
EXCEPTION
    when OTHERS then
        return_code := 'NULL';
        error_message := SQLERRM || ' from DEPT_VAL proc.';
END DEPT_VAL;
/


