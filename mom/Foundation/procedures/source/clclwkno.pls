
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-----------------------------------------------------------------------------
-- Name:    CAL_TO_CAL_WEEKNO
-- Purpose: This procedure determines the week number in respect to the week
--          within a calendar year.
-----------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CAL_TO_CAL_WEEKNO (in_dd         IN     NUMBER,
                                               in_mm         IN     NUMBER,
                                               in_yyyy       IN     NUMBER,
                                               week_no       IN OUT NUMBER,
                                               return_code   IN OUT VARCHAR2,
                                               error_message IN OUT VARCHAR2) AUTHID CURRENT_USER AS
   ABORTING EXCEPTION;

   /* Local variables */
   half_no                NUMBER(5);
   dummy                  NUMBER(9);
   work_day               NUMBER(2);
   work_month             NUMBER(2);
   work_year              NUMBER(4);
   total_days             NUMBER(4) := 0;
   out_dd_454             NUMBER(1);
   end_month              NUMBER(6);

   TYPE cal_days_in_month_tab IS TABLE OF NUMBER(2)
      INDEX BY BINARY_INTEGER;
   cal_days_tab cal_days_in_month_tab;

BEGIN
   return_code := 'TRUE';

   CAL_TO_CAL_HALF(in_dd, in_mm, in_yyyy,
                   half_no, dummy,
                   return_code, error_message);
   IF (return_code = 'FALSE') THEN
      raise ABORTING;
   END IF;

   HALF_TO_CAL_FDOH(half_no,
                    work_day, work_month, work_year,
                    return_code, error_message);
   IF (return_code = 'FALSE') THEN
      raise ABORTING;
   END IF;

   CAL_TO_454(work_day, work_month, work_year,
              out_dd_454, dummy, dummy, dummy,
              return_code, error_message);
   IF (return_code = 'FALSE') THEN
      raise ABORTING;
   END IF;

   end_month := in_mm;
   IF (in_mm < work_month) THEN
      end_month := end_month  + 12;
   END IF;

   cal_days_tab(1)  := 31;
   cal_days_tab(2)  := 29;
   cal_days_tab(3)  := 31;
   cal_days_tab(4)  := 30;
   cal_days_tab(5)  := 31;
   cal_days_tab(6)  := 30;
   cal_days_tab(7)  := 31;
   cal_days_tab(8)  := 31;
   cal_days_tab(9)  := 30;
   cal_days_tab(10) := 31;
   cal_days_tab(11) := 30;
   cal_days_tab(12) := 31;

   FOR counter IN work_month..(end_month - 1) LOOP
      IF (counter > 12) THEN
         total_days := total_days + cal_days_tab(counter - 12);
      ELSE
         total_days := total_days + cal_days_tab(counter);
      END IF;
   END LOOP;

   IF (work_month < 3 OR work_month > 9) AND
      (in_mm > 2 AND in_mm < 8) AND
      (MOD(in_yyyy, 4) != 4) THEN
      total_days := total_days - 1;
   END IF;

   total_days := total_days - in_dd;
   total_days := total_days - out_dd_454 - 1;
   week_no := trunc(total_days - 1, 7) + 1;

EXCEPTION
   when OTHERS then
      return_code := 'FALSE';
      error_message := SQLERRM || ' from CAL_TO_CAL_WEEKNO';
END CAL_TO_CAL_WEEKNO;
/


