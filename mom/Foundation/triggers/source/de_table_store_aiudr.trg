PROMPT Creating Trigger 'DE_TABLE_STORE_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_STORE_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON STORE
 FOR EACH ROW
DECLARE

   L_action_type   ORGHIER_EXPORT_STG.ACTION_TYPE%TYPE;
   L_store         STORE.STORE%TYPE;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_status        VARCHAR2(1);

   L_org_lvl_insert     VARCHAR2(1) := 'N';

   program_error EXCEPTION;

BEGIN

   if inserting then
      L_action_type := DATA_EXPORT_SQL.ST_ADD;
      L_store        := :new.store;
      L_org_lvl_insert   := 'Y';
   elsif updating then
      L_action_type := DATA_EXPORT_SQL.ST_UPD;
      L_store := :old.store;

      if :old.store_name != :new.store_name or
         :old.district != :new.district or
         :old.store_mgr_name != :new.store_mgr_name or
         :old.currency_code != :new.currency_code then
         L_org_lvl_insert   := 'Y';
      end if;
   else -- Deleting
      L_action_type := DATA_EXPORT_SQL.ST_DEL;
      L_store := :old.store;

      L_org_lvl_insert   := 'Y';
   end if;

   if NOT DATA_EXPORT_SQL.INS_STORE_EXPORT_STG(L_error_msg,
                                               L_action_type,
                                               L_store,
                                               NULL,
                                               NULL,
                                               NULL) then
      RAISE program_error;
   end if;

   if L_org_lvl_insert = 'Y' then
      if NOT DATA_EXPORT_SQL.INS_ORGHIER_EXPORT_STG(L_error_msg,
                                                    L_action_type,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    L_store,
                                                    NULL) then
         RAISE program_error;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_STORE_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
