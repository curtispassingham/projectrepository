PROMPT Creating Trigger 'EC_TABLE_UDV_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_UDV_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON UDA_VALUES
 FOR EACH ROW
DECLARE

   L_record            UDA_VALUES%ROWTYPE   := NULL;
   L_action_type       VARCHAR2(1)   := NULL;
   L_message           CLOB;
   L_status            VARCHAR2(1)   := NULL;
   L_text              VARCHAR2(255) := NULL;
   L_message_type      UDA_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;

   -- The uda display type will always be 'LV' for uda's on the uda_values table.
   -- The display type is inserted into the message queue.
   L_display_type      UDA_MFQUEUE.DISPLAY_TYPE%TYPE := 'LV';

   PROGRAM_ERROR       EXCEPTION;
BEGIN
   if DELETING then
      L_action_type      := 'D';
      L_message_type     := 'UDAValDel';
      L_record.uda_id    := :old.uda_id;
      L_record.uda_value := :old.uda_value;
   else
      if INSERTING then
         L_action_type  := 'A';
         L_message_type := 'UDAValCre';
      else
         L_action_type  := 'M';
         L_message_type := 'UDAValMod';
      
end if;

      L_record.uda_id           := :new.uda_id;
      L_record.uda_value        := :new.uda_value;
      L_record.uda_value_desc   := :new.uda_value_desc;


   end if;

   /* Creates an XML message, puts it in CLOB form, returns the CLOB.
      L_record should be populated with all of the necessary values
      from the table before calling BUILD_MESSAGE.
      L_action_type should specify the type of event ('A'dd, 'M'odify, or 'D'elete)
   */
   if not UDA_XML.BUILD_UDAV_MSG(L_status,
                                 L_text,
                                 L_message,
                                 L_record,
                                 L_action_type) then
     raise PROGRAM_ERROR;
   end if;

   /* Takes the CLOB returned from BUILD_MESSAGE and puts it in the
      message queue table.
   */
   RMSMFM_UDA.ADDTOQ(L_status,
                     L_text,
                     L_message_type,
                     L_record.uda_id,
                     L_record.uda_value,
                     L_display_type,
                     L_message);

   if L_status = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_UDV_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END;
/
