PROMPT Creating Trigger 'EC_TABLE_RID_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_RID_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON RELATED_ITEM_DETAIL
 FOR EACH ROW
DECLARE
   L_queue_rec     ITEM_MFQUEUE%ROWTYPE := NULL;
   L_status        VARCHAR2(1) := NULL;
   L_text          VARCHAR2(255) := NULL;
   L_item_row      ITEM_MASTER%ROWTYPE := NULL;
   
   PROGRAM_ERROR   EXCEPTION;
   
   cursor C_GET_ITEM is
      select item 
        from related_item_head      
       where relationship_id = L_queue_rec.relationship_id;
   
BEGIN
   if DELETING then
      L_queue_rec.message_type    := RMSMFM_ITEMS.RID_DEL;      
      L_queue_rec.relationship_id := :old.relationship_id;
      L_queue_rec.related_item    := :old.related_item;
   elsif UPDATING then
      if (nvl(:old.priority, -999)                         != nvl(:new.priority, -999)) 
      OR (nvl(:old.start_date, to_date('010190','DDMMRR')) != nvl(:new.start_date, to_date('010190','DDMMRR')))
      OR (nvl(:old.end_date, to_date('010190','DDMMRR'))   != nvl(:new.end_date, to_date('010190','DDMMRR'))) then         
         L_queue_rec.message_type    := RMSMFM_ITEMS.RID_UPD;
         L_queue_rec.relationship_id := :old.relationship_id;
         L_queue_rec.related_item    := :old.related_item;
      else
         RETURN;
      end if;
   else     
      L_queue_rec.message_type    := RMSMFM_ITEMS.RID_ADD;   
      L_queue_rec.relationship_id := :new.relationship_id;
      L_queue_rec.related_item    := :new.related_item;      
   end if;
    
   open C_GET_ITEM;
   fetch C_GET_ITEM into L_queue_rec.item;
   close C_GET_ITEM;
   
   if not ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                          L_item_row,
                                          L_queue_rec.item) then
      raise PROGRAM_ERROR;
   end if;
                                                                              
   if L_item_row.item_level = L_item_row.tran_level then      
      if RMSMFM_ITEMS.ADDTOQ(L_text,
                             L_queue_rec,
                             NULL,               ---I_sellable_ind
                             NULL) = FALSE then  ---I_tran_level_ind
         raise PROGRAM_ERROR;
      end if;       
   end if;       
     
EXCEPTION

   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_RID_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);
      
END EC_TABLE_RID_AIUDR;
/