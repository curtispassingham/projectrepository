PROMPT Creating Trigger 'DE_TABLE_DIS_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_DIS_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON DISTRICT
 FOR EACH ROW
DECLARE

   L_action_type   ORGHIER_EXPORT_STG.ACTION_TYPE%TYPE;
   L_district      DISTRICT.DISTRICT%TYPE;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_status        VARCHAR2(1);

   program_error EXCEPTION;

BEGIN

   if inserting then
      L_action_type := DATA_EXPORT_SQL.DIST_ADD;
      L_district        := :new.district;
   elsif updating then
      L_action_type := DATA_EXPORT_SQL.DIST_UPD;
      L_district := :old.district;
   else -- Deleting
      L_action_type := DATA_EXPORT_SQL.DIST_DEL;
      L_district := :old.district;
   end if;

   if NOT DATA_EXPORT_SQL.INS_ORGHIER_EXPORT_STG(L_error_msg,
                                                 L_action_type,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 L_district,
                                                 NULL,
                                                 NULL) then
      RAISE program_error;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_DIS_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
