PROMPT Creating Trigger 'EC_TABLE_IEM_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_IEM_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF STORE_ORD_MULT
, PACKAGE_UOM
, COST_ZONE_GROUP_ID
, PRIMARY_REF_ITEM_IND
, SHORT_DESC
, DIFF_4
, ITEM_SERVICE_LEVEL
, MERCHANDISE_IND
, PACKAGE_SIZE
, ITEM_DESC
, DIFF_1
, WASTE_TYPE
, RETAIL_LABEL_VALUE
, SUBCLASS
, DEPT
, CATCH_WEIGHT_IND
, DIFF_2
, GIFT_WRAP_IND
, COMMENTS
, HANDLING_SENSITIVITY
, STANDARD_UOM
, HANDLING_TEMP
, UOM_CONV_FACTOR
, WASTE_PCT
, CONST_DIMEN_IND
, DEFAULT_WASTE_PCT
, STATUS
, SHIP_ALONE_IND
, RETAIL_LABEL_TYPE
, MFG_REC_RETAIL
, DESC_UP
, CLASS
, DIFF_3
, FORECAST_IND
, NOTIONAL_PACK_IND
, SOH_INQUIRY_AT_PACK_IND
, PERISHABLE_IND
, BRAND_NAME
, PRODUCT_CLASSIFICATION
, TRAN_LEVEL
 ON ITEM_MASTER
 FOR EACH ROW
DECLARE


   L_queue_rec      ITEM_MFQUEUE%ROWTYPE := NULL;
   L_status         VARCHAR2(1)   := NULL;
   L_text           VARCHAR2(255) := NULL;
   L_item_level     ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level     ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_tran_level_ind ITEM_PUB_INFO.TRAN_LEVEL_IND%TYPE;
   L_sellable_ind   ITEM_PUB_INFO.SELLABLE_IND%TYPE;
   L_nvl_date       DATE := to_date('01-01-1900','DD-MM-RRRR');

   PROGRAM_ERROR    EXCEPTION;
BEGIN

   if DELETING then
      L_item_level := :old.item_level;
      L_tran_level := :old.tran_level;
      L_sellable_ind := :old.sellable_ind;
   else
      L_item_level := :new.item_level;
      L_tran_level := :new.tran_level;
      L_sellable_ind := :new.sellable_ind;
   end if;

   if L_item_level <= L_tran_level then
      -- item is a transaction-level item or above --

      if L_item_level = L_tran_level then
         L_tran_level_ind := 'Y';
      else
         L_tran_level_ind := 'N';
      end if;

      if DELETING then
         /* For deletes, the row identifier will be published. */

         L_queue_rec.message_type := RMSMFM_ITEMS.ITEM_DEL;
         L_queue_rec.item         := :old.item;
         L_queue_rec.approve_ind  := 'N';
      else
         if INSERTING then
            L_queue_rec.message_type   := RMSMFM_ITEMS.ITEM_ADD;
            if :new.status = 'A' then
               L_queue_rec.approve_ind := 'Y';
            else
               L_queue_rec.approve_ind := 'N';
            end if;
         else
            -- When updating, need to make sure that at least         --
            -- one of the columns being published has changed.        --
            -- If none of the columns have changed, exit the trigger. --

            if (:old.status                              != :new.status)
               or (nvl(:old.diff_1,' ')                  != nvl(:new.diff_1,' '))
               or (nvl(:old.diff_2,' ')                  != nvl(:new.diff_2,' '))
               or (nvl(:old.diff_3,' ')                  != nvl(:new.diff_3,' '))
               or (nvl(:old.diff_4,' ')                  != nvl(:new.diff_4,' '))
               or (:old.dept                             != :new.dept)
               or (:old.class                            != :new.class)
               or (:old.subclass                         != :new.subclass)
               or (:old.item_desc                        != :new.item_desc)
               or (:old.short_desc                       != :new.short_desc)
               or (:old.desc_up                          != :new.desc_up)
               or (:old.primary_ref_item_ind             != :new.primary_ref_item_ind)
               or (:old.cost_zone_group_id               != :new.cost_zone_group_id)
               or (:old.standard_uom                     != :new.standard_uom)
               or (nvl(:old.uom_conv_factor,-999)        != nvl(:new.uom_conv_factor,-999))
               or (nvl(:old.package_size,-999)           != nvl(:new.package_size,-999))
               or (nvl(:old.package_uom,' ')             != nvl(:new.package_uom,' '))
               or (:old.merchandise_ind                  != :new.merchandise_ind)
               or (:old.store_ord_mult                   != :new.store_ord_mult)
               or (:old.forecast_ind                     != :new.forecast_ind)
               or (:old.original_retail                  != :new.original_retail)
               or (nvl(:old.mfg_rec_retail,-999)         != nvl(:new.mfg_rec_retail,-999))
               or (nvl(:old.retail_label_type,' ')       != nvl(:new.retail_label_type,' '))
               or (nvl(:old.retail_label_value,-999)     != nvl(:new.retail_label_value,-999))
               or (nvl(:old.handling_temp,' ')           != nvl(:new.handling_temp,' '))
               or (nvl(:old.handling_sensitivity,' ')    != nvl(:new.handling_sensitivity,' '))
               or (:old.catch_weight_ind                 != :new.catch_weight_ind)
               or (nvl(:old.waste_type,' ')              != nvl(:new.waste_type,' '))
               or (nvl(:old.waste_pct,-999)              != nvl(:new.waste_pct,-999))
               or (nvl(:old.default_waste_pct,-999)      != nvl(:new.default_waste_pct,-999))
               or (:old.const_dimen_ind                  != :new.const_dimen_ind)
               or (nvl(:old.order_as_type,' ')           != nvl(:new.order_as_type,' '))
               or (nvl(:old.comments,' ')                != nvl(:new.comments,' '))
               or (nvl(:old.item_service_level,' ')      != nvl(:new.item_service_level,' '))
               or (:old.gift_wrap_ind                    != :new.gift_wrap_ind)
               or (:old.ship_alone_ind                   != :new.ship_alone_ind) 
               or (:old.notional_pack_ind                != :new.notional_pack_ind)
               or (:old.soh_inquiry_at_pack_ind          != :new.soh_inquiry_at_pack_ind) 
               or (:old.perishable_ind                   != :new.perishable_ind)
               or (nvl(:old.brand_name,' ')              != nvl(:new.brand_name, ' '))
               or (nvl(:old.product_classification, ' ') != nvl(:new.product_classification, ' ')) 
               or (:old.tran_level                       != :new.tran_level) then

               L_queue_rec.message_type := RMSMFM_ITEMS.ITEM_UPD;
               if :new.status = 'A' and :old.status != 'A' then
                  L_queue_rec.approve_ind := 'Y';
               else
                  L_queue_rec.approve_ind := 'N';
               end if;
            else
               return;
            end if;

         end if;

         L_queue_rec.item := :new.item;
      end if;

   elsif L_item_level > L_tran_level then
      -- item is below the transaction level --
      --     (reference item a.k.a. UPC)     --

      if DELETING then
         if :old.status = 'A' then   
            L_queue_rec.message_type := RMSMFM_ITEMS.UPC_DEL;
            L_queue_rec.item         := :old.item_parent;
            L_queue_rec.ref_item     := :old.item;
         else
            return;
         end if;
      else
         if INSERTING then
            if :new.status = 'A' then                 
               L_queue_rec.message_type := RMSMFM_ITEMS.UPC_ADD;
            else
               return;
            end if;  
         else

            -- updating, only continue if primary_ref_item_ind --
            -- has changed, since this is the only field sent  --
            -- to the bus for ref items (a.k.a. UPCs)          --
            -- that are modifiable after item has been created --

            if :new.status = 'A' and :old.status != 'A' then
               L_queue_rec.message_type := RMSMFM_ITEMS.UPC_ADD;
            elsif :new.primary_ref_item_ind != :old.primary_ref_item_ind and :new.status = 'A' then        
               L_queue_rec.message_type := RMSMFM_ITEMS.UPC_UPD;              
            else
               return;
            end if;
         end if;

         L_queue_rec.item      := :new.item_parent;
         L_queue_rec.ref_item  := :new.item;
      end if;
   end if;

   /* Puts a record on the item message queue table.
   */

   if RMSMFM_ITEMS.ADDTOQ(L_text,
                          L_queue_rec,
                          L_sellable_ind,
                          L_tran_level_ind) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_IEM_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END EC_TABLE_IEM_AIUDR;
/
