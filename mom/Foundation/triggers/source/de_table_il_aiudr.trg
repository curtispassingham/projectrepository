----------------------------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
----------------------------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Tigger Added:  DE_TABLE_IL_AIUDR
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Tigger
--------------------------------------
PROMPT Creating TRIGGER 'DE_TABLE_IL_AIUDR'
create or replace TRIGGER DE_TABLE_IL_AIUDR
   AFTER INSERT OR UPDATE OR DELETE ON ITEM_LOC
   FOR EACH ROW

DECLARE
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   L_status             VARCHAR2(1);
   L_item               ITEM_LOC.ITEM%TYPE;
   L_loc                ITEM_LOC.LOC%TYPE;
   L_loc_type           ITEM_LOC.LOC_TYPE%TYPE;
   L_action_type        ITEM_EXPORT_STG.ACTION_TYPE%TYPE;
   L_merchandise_ind    VARCHAR2(1);
   PROGRAM_ERROR        EXCEPTION;

   cursor C_SELL_ITEM_EXIST is
      select merchandise_ind
        from item_master
       where item = L_item
         and sellable_ind = 'Y';

BEGIN
   L_item := NVL(:new.item,:old.item);

   open C_SELL_ITEM_EXIST;
   fetch C_SELL_ITEM_EXIST into L_merchandise_ind;
   close C_SELL_ITEM_EXIST;

   if L_merchandise_ind is NOT NULL then
      if inserting then
         L_loc         := :new.loc;
         L_loc_type    := :new.loc_type;
         L_action_type := DATA_EXPORT_SQL.ITLOC_CRE;
      elsif updating then
         if :old.taxable_ind != :new.taxable_ind or
            :old.local_item_desc != :new.local_item_desc or
            NVL(:old.local_short_desc, '-999') != NVL(:new.local_short_desc, '-999') or
            NVL(:old.ti, -999) != NVL(:new.ti, -999) or
            NVL(:old.hi, -999) != NVL(:new.hi, -999) or
            :old.store_ord_mult != :new.store_ord_mult or
            :old.status != :new.status or
            NVL(:old.daily_waste_pct, -999) != NVL(:new.daily_waste_pct, -999) or
            NVL(:old.meas_of_each, -999) != NVL (:new.meas_of_each, -999) or
            NVL(:old.meas_of_price, -999) != NVL(:new.meas_of_price, -999) or
            NVL(:old.uom_of_price, '-999') != NVL(:new.uom_of_price, '-999') or
            NVL(:old.primary_variant, '-999') != NVL(:new.primary_variant, '-999') or
            NVL(:old.primary_cost_pack, '-999') != NVL(:new.primary_cost_pack, '-999') or
            NVL(:old.primary_supp, -999) != NVL(:new.primary_supp, -999) or
            NVL(:old.primary_cntry, '-999') != NVL(:new.primary_cntry, '-999') or
            NVL(:old.receive_as_type, '-999') != NVL(:new.receive_as_type, '-999') or
            NVL(:old.inbound_handling_days, -999) != NVL(:new.inbound_handling_days, -999) or
            NVL(:old.source_method, '-999') != NVL(:new.source_method, '-999') or
            NVL(:old.source_wh, -999) != NVL(:new.source_wh, -999) or
            NVL(:old.uin_type, '-999') != NVL(:new.uin_type, '-999') or
            NVL(:old.uin_label, '-999') != NVL(:new.uin_label, '-999') or
            NVL(:old.capture_time, '-999') != NVL(:new.capture_time, '-999') or
            :old.ext_uin_ind != :new.ext_uin_ind or
            :old.ranged_ind != :new.ranged_ind or
            NVL(:old.costing_loc, -999) != NVL(:new.costing_loc, -999) or
            NVL(:old.costing_loc_type, '-999') != NVL(:new.costing_loc_type, '-999') then
            L_loc       := :old.loc;
            L_loc_type  := :old.loc_type;
            L_action_type := DATA_EXPORT_SQL.ITLOC_UPD;
         end if;
      else -- Deleting
         L_loc         := :old.loc;
         L_loc_type    := :old.loc_type;
         L_action_type := DATA_EXPORT_SQL.ITLOC_DEL;
      end if;
   end if;

   if L_action_type is NOT NULL then
      if DATA_EXPORT_SQL.INS_ITEM_EXPORT_STG(L_error_message,
                                             L_action_type,
                                             L_item,
                                             NULL, -- item_parent
                                             NULL, -- item_grandparent
                                             NULL, -- item_level
                                             NULL, -- tran_level
                                             NULL, -- vat_region
                                             NULL, -- vat_code
                                             NULL, -- vat_type
                                             L_merchandise_ind,
                                             NULL, -- vat_active_date
                                             L_loc,
                                             L_loc_type) = FALSE then
         RAISE PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_IL_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/
