PROMPT Creating Trigger 'RMS_COL_ISL_ROUNDCOLS_AUR'
CREATE OR REPLACE TRIGGER RMS_COL_ISL_ROUNDCOLS_AUR
 AFTER UPDATE OF ROUND_TO_PALLET_PCT
, ROUND_TO_LAYER_PCT
, ROUND_TO_INNER_PCT
, ROUND_LVL
, ROUND_TO_CASE_PCT
, UNIT_COST
, PICKUP_LEAD_TIME
 ON ITEM_SUPP_COUNTRY_LOC
 FOR EACH ROW
BEGIN                                                                           
                                                                                
   if updating then                                                             

      if :new.unit_cost           != :old.unit_cost then                        
                                                                                
         insert into repl_item_loc_updates (item,                               
                                            supplier,                           
                                            origin_country_id,                  
                                            location,                           
                                            loc_type,                           
                                            change_type)                        
                                     select :new.item,                          
                                            :new.supplier,                      
                                            :new.origin_country_id,             
                                            :new.loc,                           
                                            :new.loc_type,                      
                                            'ISCLC'                             
                                       from dual                                
                                      where exists (select 'x'                  
                                                      from repl_item_loc ril    
                                                     where ril.item = :new.item
                                                       and rownum = 1)
                                                                                
                                         or exists (select 'x'                  
                                                      from repl_item_loc ril    
                                                     where ril.primary_pack_no = :new.item
                                                       and rownum = 1);                                                                    
                                                                                
      end if;                                                                   
                                                                                
      if :new.round_lvl           != :old.round_lvl or                          
         :new.round_to_inner_pct  != :old.round_to_inner_pct or                 
         :new.round_to_case_pct   != :old.round_to_case_pct or                  
         :new.round_to_layer_pct  != :old.round_to_layer_pct or                 
         :new.round_to_pallet_pct != :old.round_to_pallet_pct or
         NVL(:new.pickup_lead_time,-999)    != NVL(:old.pickup_lead_time,-999) then
                                                                                            
         insert into repl_item_loc_updates (item,                               
                                            supplier,                           
                                            origin_country_id,                  
                                            location,                           
                                            loc_type,                           
                                            change_type)                        
                                     select :new.item,                          
                                            :new.supplier,                      
                                            :new.origin_country_id,             
                                            :new.loc,                           
                                            :new.loc_type,                      
                                            'ISCLR'                             
                                       from dual                                
                                      where exists (select 'x'                  
                                                      from repl_item_loc ril    
                                                     where ril.item = :new.item)
                                                                                
                                         or exists (select 'x'                  
                                                      from repl_item_loc ril    
                                                     where ril.primary_pack_no =
 :new.item);                                                                    
                                                                                
      end if;                                                                   
   end if;                                                                      
                                                                                
END;
/
