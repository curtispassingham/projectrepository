PROMPT Creating Trigger 'EC_TABLE_CHN_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_CHN_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON CHANNELS
 FOR EACH ROW
DECLARE

   L_record        CHANNELS%ROWTYPE := NULL;
   L_action_type   VARCHAR2(1) := NULL;
   L_message       CLOB;
   L_status        VARCHAR2(1) := NULL;
   L_text          VARCHAR2(255) := NULL;
   L_message_type  BANNER_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;

   PROGRAM_ERROR   EXCEPTION;
BEGIN
   if DELETING then
      /* For deletes, the row identifier will be published. */

      L_action_type := 'D';
      L_message_type := 'ChannelDel';
      L_record.banner_id := :old.banner_id;
      L_record.channel_id := :old.channel_id;
   else
      if INSERTING then
         L_action_type := 'A';
         L_message_type := 'ChannelCre';
      else
         L_action_type := 'M';
         L_message_type := 'ChannelMod';
      
end if;

      /* For inserts and updates, more of the values from the table
         will be published.
      */

      L_record.banner_id := :new.banner_id;
      L_record.channel_id := :new.channel_id;
      L_record.channel_name := :new.channel_name;
      L_record.channel_type := :new.channel_type;

   end if;

   /* Creates an XML message, puts it in CLOB form, returns the CLOB.
      L_record should be populated with all of the necessary values
      from the table before calling BUILD_MESSAGE.
      L_action_type should specify the type of event ('A'dd, 'M'odify, or 'D'elete)
   */
   if not CHANNEL_XML.BUILD_MESSAGE(L_status,
                                    L_text,
                                    L_message,
                                    L_record,
                                    L_action_type) then
     raise PROGRAM_ERROR;
   end if;

   /* Takes the CLOB returned from BUILD_MESSAGE and puts it in the
      message queue table.
   */
   RMSMFM_BANNER.ADDTOQ(L_status,
                        L_text,
                        L_message_type,
                        L_record.banner_id,
                        L_record.channel_id,
                        L_message);

   if L_status = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_CHN_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END;
/
