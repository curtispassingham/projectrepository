PROMPT Creating Trigger 'DE_TABLE_CHA_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_CHA_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON CHAIN
 FOR EACH ROW
DECLARE

   L_action_type   ORGHIER_EXPORT_STG.ACTION_TYPE%TYPE;
   L_chain         CHAIN.CHAIN%TYPE;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_status        VARCHAR2(1);

   program_error EXCEPTION;

BEGIN

   if inserting then
      L_action_type := DATA_EXPORT_SQL.CHAIN_ADD;
      L_chain        := :new.chain;
   elsif updating then
      L_action_type := DATA_EXPORT_SQL.CHAIN_UPD;
      L_chain := :old.chain;
   else -- Deleting
      L_action_type := DATA_EXPORT_SQL.CHAIN_DEL;
      L_chain := :old.chain;
   end if;

   if NOT DATA_EXPORT_SQL.INS_ORGHIER_EXPORT_STG(L_error_msg,
                                                 L_action_type,
                                                 NULL,
                                                 L_chain,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL) then
      RAISE program_error;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_CHA_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
