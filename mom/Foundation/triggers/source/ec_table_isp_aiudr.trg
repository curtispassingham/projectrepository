PROMPT Creating Trigger 'EC_TABLE_ISP_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_ISP_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF PRIMARY_SUPP_IND
, VPN
, SUPP_LABEL
, CONSIGNMENT_RATE
, CONCESSION_RATE
, SUPP_DIFF_1
, SUPP_DIFF_2
, SUPP_DIFF_3
, SUPP_DIFF_4
, PALLET_NAME
, CASE_NAME
, INNER_NAME
, SUPP_DISCONTINUE_DATE
, DIRECT_SHIP_IND
 ON ITEM_SUPPLIER
 FOR EACH ROW
DECLARE

   L_queue_rec     ITEM_MFQUEUE%ROWTYPE := NULL;
   L_status        VARCHAR2(1) := NULL;
   L_text          VARCHAR2(255) := NULL;
   L_message_type  ITEM_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;
   L_item_row      ITEM_MASTER%ROWTYPE := NULL;

   PROGRAM_ERROR   EXCEPTION;
BEGIN
   if DELETING then
      L_queue_rec.message_type := RMSMFM_ITEMS.ISUP_DEL;
      L_queue_rec.item := :old.item;
      L_queue_rec.supplier := :old.supplier;
   else
      if INSERTING then
         L_queue_rec.message_type:= RMSMFM_ITEMS.ISUP_ADD;
      else
         L_queue_rec.message_type := RMSMFM_ITEMS.ISUP_UPD;
      end if;

      L_queue_rec.item := :new.item;
      L_queue_rec.supplier := :new.supplier;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                      L_item_row,
                                      L_queue_rec.item) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---
   if L_item_row.item_level = L_item_row.tran_level then
      if RMSMFM_ITEMS.ADDTOQ(L_text,
                             L_queue_rec,
                             NULL,               ---I_sellable_ind
                             NULL) = FALSE then  ---I_tran_level_ind
         raise PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_ISP_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END;
/
