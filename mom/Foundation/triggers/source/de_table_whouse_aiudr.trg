PROMPT Creating Trigger 'DE_TABLE_WHOUSE_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_WHOUSE_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON WH
 FOR EACH ROW
DECLARE

   L_action_type   ORGHIER_EXPORT_STG.ACTION_TYPE%TYPE;
   L_wh            WH.WH%TYPE;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_status        VARCHAR2(1);

   L_org_lvl_insert     VARCHAR2(1) := 'N';

   program_error EXCEPTION;

BEGIN

   if inserting then
      L_action_type := DATA_EXPORT_SQL.WH_ADD;
      L_wh        := :new.wh;
      L_org_lvl_insert   := 'Y';
   elsif updating then
      L_action_type := DATA_EXPORT_SQL.WH_UPD;
      L_wh := :old.wh;

      if :old.wh_name != :new.wh_name or
         :old.currency_code != :new.currency_code then
         L_org_lvl_insert   := 'Y';
      end if;
   else -- Deleting
      L_action_type := DATA_EXPORT_SQL.WH_DEL;
      L_wh := :old.wh;
      L_org_lvl_insert   := 'Y';
   end if;

   if L_org_lvl_insert = 'Y' then
      if NOT DATA_EXPORT_SQL.INS_ORGHIER_EXPORT_STG(L_error_msg,
                                                    L_action_type,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    L_wh) then
         RAISE program_error;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_WHOUSE_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
/
