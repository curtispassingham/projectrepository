PROMPT Creating Trigger 'EC_TABLE_CRT_AIUR'
CREATE OR REPLACE TRIGGER EC_TABLE_CRT_AIUR
 AFTER INSERT OR UPDATE
 ON CURRENCY_RATES
 FOR EACH ROW
DECLARE



   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE             := NULL;
   L_message_type    SEEDOBJ_MFQUEUE.MESSAGE_TYPE%TYPE    := NULL;
   L_currrate_row    SEEDOBJ_MFQUEUE%ROWTYPE              := NULL;
   L_status_code     VARCHAR2(255)                        := NULL;
   L_update          VARCHAR2(1)                          := 'Y';
   
   PROGRAM_ERROR EXCEPTION;
BEGIN
   
   if INSERTING then

      L_message_type := RMSMFM_SEEDOBJ.CURR_ADD;
     
   elsif UPDATING then

      if (:OLD.effective_date = :NEW.effective_date) and
         (:OLD.exchange_type = :NEW.exchange_type) and
         (:OLD.exchange_rate = :NEW.exchange_rate) then

         L_update := 'N';
      
      else

         L_message_type := RMSMFM_SEEDOBJ.CURR_UPD;

      end if;

   end if;     

   if L_update = 'Y' then
      
      L_currrate_row.currency_code  := :NEW.currency_code;
      L_currrate_row.effective_date := :NEW.effective_date;
      L_currrate_row.exchange_type  := :NEW.exchange_type;
      L_currrate_row.exchange_rate  := :NEW.exchange_rate;
      
      if RMSMFM_SEEDOBJ.ADDTOQ(L_error_message,
                               L_message_type,
                               NULL,
                               L_currrate_row.currency_code,
                               NULL,
                               L_currrate_row.effective_date,
                               L_currrate_row.exchange_type,
                               L_currrate_row.exchange_rate) = FALSE then
         raise PROGRAM_ERROR;
      end if;

   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status_code,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_CRT_AIUR');
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

END EC_TABLE_CRT_AIUDR;
/
