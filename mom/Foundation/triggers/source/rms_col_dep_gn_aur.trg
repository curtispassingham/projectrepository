PROMPT Creating Trigger 'RMS_COL_DEP_GN_AUR'
CREATE OR REPLACE TRIGGER RMS_COL_DEP_GN_AUR
 AFTER UPDATE OF GROUP_NO
 ON DEPS
 FOR EACH ROW
DECLARE
   L_new_group_no  DEPS.GROUP_NO%TYPE;
   L_old_group_no  DEPS.GROUP_NO%TYPE;
   L_new_dept      DEPS.DEPT%TYPE;
   L_error_message RTK_ERRORS.RTK_TEXT%TYPE := NULL;
BEGIN
    L_new_group_no := :new.group_no;
    L_old_group_no := :old.group_no;
    L_new_dept := :new.dept;
    if (L_new_group_no IS NOT NULL) and (L_new_group_no != L_old_group_no) and (L_new_dept IS NOT NULL) then
      INSERT INTO RDW_RECLASS(IDNT,TABLE_NAME) VALUES(L_new_dept, 'DEPS');
    end if;
EXCEPTION
   when OTHERS then
      if SQLCODE = -00001 then  
         -- unique key violation; row already exists. Do nothing
         null;
      else
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'RMS_COL_DEP_GN_AUR',
                                               to_char(SQLCODE));
         raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
      end if;
END RMS_COL_DEP_GN_AUR;
/

