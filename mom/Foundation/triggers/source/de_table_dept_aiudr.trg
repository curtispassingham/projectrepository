--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.0 $
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- TRIGGER ADDED:          DE_TABLE_DEPT_AIUDR
----------------------------------------------------------------------------

whenever SQLERROR exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'DE_TABLE_DEPT_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_DEPT_AIUDR
   AFTER INSERT OR UPDATE OR DELETE ON DEPS
   FOR EACH ROW

DECLARE
   L_dept            DEPS.DEPT%TYPE;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_status          VARCHAR2(1);
   L_action_type     MERCHHIER_EXPORT_STG.ACTION_TYPE%TYPE;
   program_error EXCEPTION;
BEGIN

   if inserting then
      L_dept        := :new.dept;
      L_action_type := DATA_EXPORT_SQL.DEPT_CRE;
   elsif updating then
      if :old.group_no <> :new.group_no or 
         :old.dept_name <> :new.dept_name then
         L_dept        := :old.dept;
         L_action_type := DATA_EXPORT_SQL.DEPT_UPD;
      end if;
   else --Deleting
      L_dept        := :old.dept;
      L_action_type := DATA_EXPORT_SQL.DEPT_DEL;
   end if;

   if L_dept is NOT NULL then
      if DATA_EXPORT_SQL.INS_MERCHHIER_EXPORT_STG(L_error_message,
                                                  L_action_type,
                                                  NULL,
                                                  NULL,
                                                  L_dept,
                                                  NULL,
                                                  NULL) = FALSE then
         RAISE program_error;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_DEPT_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/ 