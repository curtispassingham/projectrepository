PROMPT Creating Trigger 'EC_TABLE_PKS_IUDS'
CREATE OR REPLACE TRIGGER EC_TABLE_PKS_IUDS
 AFTER DELETE OR INSERT OR UPDATE
 ON PACKITEM_BREAKOUT
DECLARE

   L_rowcount            NUMBER := RMSMFM_ITEMS.BOM_TABLE.COUNT;
   L_queue_rec           ITEM_MFQUEUE%ROWTYPE := NULL;
   L_status              VARCHAR2(1) := NULL;
   L_text                VARCHAR2(255) := NULL;
   L_packitem_sum        PACKITEM_BREAKOUT.PACK_ITEM_QTY%TYPE := NULL;
   L_pi_new_sum          PACKITEM_BREAKOUT.PACK_ITEM_QTY%TYPE := NULL;
   L_ALREADY_DELETED     BOOLEAN := NULL;


   PROGRAM_ERROR   EXCEPTION;

   cursor C_PACKITEM_QTY_SUM(I_pack_no IN PACKITEM_BREAKOUT.PACK_NO%TYPE,
                             I_item    IN PACKITEM_BREAKOUT.ITEM%TYPE) is
      select NVL(comp_pack_qty, pack_item_qty)
        from packitem_breakout
       where pack_no                 = I_pack_no
         and NVL(comp_pack_no, item) = I_item;

--- This function checks if the current order, item, physical
--- location has already been processed for a delete or insert
--- (this is only necessary for mass deletes or inserts).  It will
--- return TRUE if it has and FALSE if it has not.
FUNCTION ALREADY_DELETED(I_pack_no  IN  PACKITEM.PACK_NO%TYPE,
                         I_item     IN  PACKITEM.ITEM%TYPE,
                         I_rowcount IN  NUMBER)
RETURN BOOLEAN IS
   L_been_processed    BOOLEAN;
BEGIN
   L_been_processed  := FALSE;
   FOR j IN 1..I_rowcount LOOP
      if RMSMFM_ITEMS.BOM_TABLE(j).PACK_NO    = I_pack_no
       and RMSMFM_ITEMS.BOM_TABLE(j).ITEM     = I_item then
         L_been_processed := TRUE;
         exit;
      end if;
   END LOOP;
   return L_been_processed;
END ALREADY_DELETED;
--------------------------------------------------------------------------------
BEGIN
   FOR i IN 1..L_rowcount LOOP
      open C_PACKITEM_QTY_SUM(RMSMFM_ITEMS.BOM_TABLE(i).PACK_NO,
                              RMSMFM_ITEMS.BOM_TABLE(i).ITEM);
      fetch C_PACKITEM_QTY_SUM into L_packitem_sum;
      close C_PACKITEM_QTY_SUM;

      /* The ABS function is used here to take care of the case when the
         packitem new sum is less than zero */

      L_pi_new_sum := ABS(L_packitem_sum -
                      RMSMFM_ITEMS.BOM_TABLE(i).PACK_ITEM_QTY);

      if L_pi_new_sum is NULL then
         L_pi_new_sum := 0;
      end if;

      L_ALREADY_DELETED := FALSE;

      --- compare the current record with the previously processed records
      --- to determine if the record has already been processed for
      --- deletion.  If we have already created a delete message for the
      --- key, we will not create a second message.  The locations are
      --- physical locations.
      L_ALREADY_DELETED := ALREADY_DELETED(RMSMFM_ITEMS.BOM_TABLE(i).PACK_NO,
                                           RMSMFM_ITEMS.BOM_TABLE(i).ITEM,
                                           i - 1); -- verify against all records
                                                   -- up to current record.

      if L_ALREADY_DELETED = FALSE then
         if DELETING then
            if L_pi_new_sum = 0 then
               L_queue_rec.message_type   := RMSMFM_ITEMS.BOM_DEL;
            elsif L_pi_new_sum > 0 then
               L_queue_rec.message_type := RMSMFM_ITEMS.BOM_UPD;
            end if;
         else
            if INSERTING then
               if L_pi_new_sum = 0 then
                  L_queue_rec.message_type := RMSMFM_ITEMS.BOM_ADD;
               elsif L_pi_new_sum > 0 then
                  L_queue_rec.message_type := RMSMFM_ITEMS.BOM_UPD;
               end if;
            else
               L_queue_rec.message_type := RMSMFM_ITEMS.BOM_UPD;
            end if;
         end if;

         L_queue_rec.ITEM       := RMSMFM_ITEMS.BOM_TABLE(i).PACK_NO;
         L_queue_rec.PACK_COMP  := RMSMFM_ITEMS.BOM_TABLE(i).ITEM;

         if RMSMFM_ITEMS.ADDTOQ(L_text,
                                L_queue_rec,
                                NULL,               ---I_sellable_ind
                                NULL) = FALSE then  ---I_tran_level_ind
            raise PROGRAM_ERROR;
         end if;

         if L_status = API_CODES.UNHANDLED_ERROR then
            raise PROGRAM_ERROR;
         end if;
      end if;
   END LOOP;
   /*  Re-initialize the table and loop variable */
   RMSMFM_ITEMS.BOM_TABLE  :=  RMSMFM_ITEMS.EMPTY_BOM;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_PKS_IUDS');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);
END;
/
