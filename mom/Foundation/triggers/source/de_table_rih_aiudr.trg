--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Trigger Added:  DE_TABLE_RIH_AIUDR
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating trigger             
--------------------------------------
PROMPT CREATING TRIGGER 'DE_TABLE_RIH_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_RIH_AIUDR
   AFTER INSERT OR UPDATE OR DELETE ON RELATED_ITEM_HEAD
   FOR EACH ROW


DECLARE
   L_error_message          RTK_ERRORS.RTK_TEXT%TYPE;
   L_relationship_id        RELATED_ITEM_HEAD.RELATIONSHIP_ID%TYPE;
   L_item                   RELATED_ITEM_HEAD.ITEM%TYPE;
   L_sellable_ind           VARCHAR2(1);
   L_action_type            RELITEM_EXPORT_STG.ACTION_TYPE%TYPE;
   L_status                 VARCHAR2(1);
   PROGRAM_ERROR            EXCEPTION;

   cursor C_SELLABLE_IND is
      select sellable_ind
        from item_master
       where item = L_item;

BEGIN
   L_item := NVL(:new.item,:old.item);

   open C_SELLABLE_IND;
   fetch C_SELLABLE_IND into L_sellable_ind;
   close C_SELLABLE_IND;

   if L_sellable_ind = 'Y' then
      if inserting then
         L_relationship_id   := :new.relationship_id;
         L_action_type       := DATA_EXPORT_SQL.RELITMHDR_CRE;
      elsif updating then
         L_relationship_id   := :old.relationship_id;
         L_action_type       := DATA_EXPORT_SQL.RELITMHDR_UPD;
      else -- Delteing
         L_relationship_id   := :old.relationship_id;
         L_action_type       := DATA_EXPORT_SQL.RELITMHDR_DEL;   
      end if;
   end if;

   if L_action_type is NOT NULL then
      if DATA_EXPORT_SQL.INS_RELITEM_EXPORT_STG(L_error_message,
                                                L_action_type,
                                                L_relationship_id,
                                                L_item,
                                                NULL) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_RIH_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/