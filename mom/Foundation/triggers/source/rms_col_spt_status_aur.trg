PROMPT Creating Trigger 'RMS_COL_SPT_STATUS_AUR'
CREATE OR REPLACE TRIGGER RMS_COL_SPT_STATUS_AUR
 AFTER UPDATE OF STATUS
 ON SVC_PROCESS_TRACKER
 FOR EACH ROW 
DECLARE
   L_taskflow_url       RMS_ASYNC_JOB.TASKFLOW_URL%TYPE := '/WEB-INF/oracle/retail/apps/rms/foundation/other/view/induction/status/flow/MaintainDataLoadingStatusFlow.xml#MaintainDataLoadingStatusFlow';
   PROGRAM_ERROR        EXCEPTION;
   L_job_type           VARCHAR2(30) := RMS_CONSTANTS.NOTIFY_DATA_UPLD_FAILURE;
   L_mode               VARCHAR2(6) := 'EDIT';
   L_key_no NUMBER;
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   
   L_status             VARCHAR2(1) := NULL;
   
BEGIN
   
   L_key_no := :old.process_id;
   if updating then                                                             
      if :new.status in (RMS_CONSTANTS.SYNC_STATUS_ERROR,RMS_CONSTANTS.SYNC_STATUS_WARNING) then   
            if RMS_NOTIFICATION_SQL.WRITE_RAF_NOTIFICATION(L_error_message,
                                                           L_taskflow_url,
                                                           L_job_type,
                                                           L_key_no,
                                                           L_mode,
                                                           NULL) = FALSE then
               
               raise PROGRAM_ERROR;
            end if;  
      end if;
   end if;

EXCEPTION
   when PROGRAM_ERROR then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'RMS_COL_SPT_STATUS_AUR');
      
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/