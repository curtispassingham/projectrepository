--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.1 $
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TRIGGER ADDED:				RMS_TABLE_RPM_ITL_BIUDR
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'RMS_TABLE_RPM_ITL_BIUDR'
CREATE OR REPLACE TRIGGER RMS_TABLE_RPM_ITL_BIUDR
   BEFORE DELETE OR INSERT OR UPDATE
   ON ITEM_LOC
   FOR EACH ROW
declare
   O_text                 VARCHAR2(255);
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE := NULL;
begin
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_text,
                                            L_system_options_rec) = FALSE then
      RAISE_APPLICATION_ERROR(-20000,'INTERNAL ERROR: TRG_ITEM_LOC - SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS failed.');
   end if;
  
   if L_system_options_rec.rpm_ind = 'Y' then
  
      if   (INSERTING and :NEW.status != 'D' and :NEW.loc_type in ('S','W'))
        or (:OLD.loc_type in ('S','W') and RMSSUB_PRICECHANGE_UPDATE.SKIP_STAGE_ITEM_LOC = 'N'
            and (
                (UPDATING and (:OLD.status = 'D' and :NEW.status != 'D')) 
             or (UPDATING and (:OLD.selling_unit_retail!= :NEW.selling_unit_retail))
                )
            ) then 
         merge into rpm_stage_item_loc rsl
         using (select :NEW.item item,
                       :NEW.loc loc,
                       :NEW.selling_unit_retail selling_unit_retail,
                       :NEW.selling_uom selling_uom,
                       'N' status
                  from dual) upd
         on (rsl.item = upd.item and rsl.loc = upd.loc)
         when MATCHED then 
            update set selling_unit_retail = upd.selling_unit_retail
         when not MATCHED then
            insert (stage_item_loc_id,
                    item,
                    loc,
                    loc_type,
                    selling_unit_retail,
                    selling_uom,
                    status,
                    create_date)
            values (rpm_stage_item_loc_seq.nextval,
                    :NEW.item,
                    :NEW.loc,
                    :NEW.loc_type,
                    :NEW.selling_unit_retail,
                    :NEW.selling_uom,
                    'N',
                    sysdate);
      end if;
      
      if DELETING or (UPDATING AND :OLD.status != 'D' AND :NEW.status = 'D') then
         if :OLD.loc_type = 'S' OR :OLD.loc_type = 'W' then
            insert into rpm_stage_deleted_item_loc (deleted_item_loc_id,
                                                    item,
                                                    location,
                                                    status)
                                            values (rpm_stage_deleted_item_loc_seq.nextval,
                                                    :OLD.item,
                                                    :OLD.loc,
                                                    'N');
         end if;
      end if;
         
      if UPDATING and (:OLD.status ='D' and :NEW.status != 'D' and :OLD.loc_type in ('S','W')) then
         delete from rpm_stage_deleted_item_loc
               where item = :NEW.item
                 and location = :NEW.loc; 
      end if;
   end if;   
      
end;
/

