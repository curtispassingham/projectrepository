PROMPT Creating Trigger 'EC_TABLE_ADR_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_ADR_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF COUNTY
, ADD_3
, ORACLE_VENDOR_SITE_ID
, ADD_1
, ADDR_TYPE
, EDI_ADDR_CHG
, STATE
, ADDR_KEY
, COUNTRY_ID
, CONTACT_EMAIL
, KEY_VALUE_1
, CONTACT_FAX
, CITY
, PRIMARY_ADDR_IND
, CONTACT_TELEX
, POST
, ADD_2
, MODULE
, CONTACT_PHONE
, KEY_VALUE_2
, SEQ_NO
, CONTACT_NAME
 ON ADDR
 FOR EACH ROW
DECLARE

   L_supplier             SUPS.SUPPLIER%TYPE;
   L_supplier_parent      SUPS.SUPPLIER_PARENT%TYPE := NULL;
   L_event                VARCHAR2(1) := 'A';
   O_status               VARCHAR2(100);
   L_valid                BOOLEAN;
   O_text                 VARCHAR2(255);
   L_msg                  CLOB;
   L_message_type         VARCHAR2(15) := NULL;
   L_addr                 ADDR%ROWTYPE;
   L_ret_allow_ind        VARCHAR2(2) := NULL;
   L_return               BOOLEAN;
   L_store                STORE.STORE%TYPE;
   L_wh_key_rec           RMSMFM_WH.WH_KEY_REC;
   L_store_key_rec        RMSMFM_STORE.STORE_KEY_REC;
   L_error_message        VARCHAR2(1000) := NULL;
   L_addr_publish_ind     ADDR.PUBLISH_IND%TYPE;
   L_partner_key_rec      RMSMFM_PARTNER.PARTNER_KEY_REC;
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE := NULL;
   
   cursor C_SUPPLIER_PARENT is
      select supplier_parent 
        from SUPS
       --- NVL to handle deletion scenario since only old state will have value for key_value_1
       where supplier = NVL(TO_NUMBER(:new.key_value_1), TO_NUMBER(:old.key_value_1));  
   
BEGIN

   L_addr.ADDR_KEY := :new.ADDR_KEY;
   L_addr.MODULE := :new.MODULE;
   L_addr.KEY_VALUE_1 := :new.KEY_VALUE_1;
   L_addr.KEY_VALUE_2 := :new.KEY_VALUE_2;
   L_addr.SEQ_NO := :new.SEQ_NO;
   L_addr.ADDR_TYPE := :new.ADDR_TYPE;
   L_addr.PRIMARY_ADDR_IND := :new.PRIMARY_ADDR_IND;
   L_addr.ADD_1 := :new.ADD_1;
   L_addr.ADD_2 := :new.ADD_2;
   L_addr.ADD_3 := :new.ADD_3;
   L_addr.CITY := :new.CITY;
   L_addr.STATE := :new.STATE;
   L_addr.COUNTRY_ID := :new.COUNTRY_ID;
   L_addr.POST := :new.POST;
   L_addr.CONTACT_NAME := :new.CONTACT_NAME;
   L_addr.CONTACT_PHONE := :new.CONTACT_PHONE;
   L_addr.CONTACT_TELEX := :new.CONTACT_TELEX;
   L_addr.CONTACT_FAX := :new.CONTACT_FAX;
   L_addr.CONTACT_EMAIL := :new.CONTACT_EMAIL;
   L_addr.ORACLE_VENDOR_SITE_ID := :new.ORACLE_VENDOR_SITE_ID;

   if nvl(:new.module,:old.module) = 'SUPP' then
   ---
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_text,
                                               L_system_options_rec) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      ---
      open C_SUPPLIER_PARENT;
      fetch C_SUPPLIER_PARENT into L_supplier_parent;
      close C_SUPPLIER_PARENT;
      
      -- Do not process Vendor level (supplier parent) information if supplier sites are used
      if L_system_options_rec.supplier_sites_ind = 'Y' and 
         L_supplier_parent is NULL then
         return;
      end if;

      if DELETING then
         L_supplier := TO_NUMBER(:old.key_value_1);
         L_event := 'D';
         L_addr.SEQ_NO := :old.SEQ_NO;
         L_addr.ADDR_TYPE := :old.ADDR_TYPE;
         L_addr.KEY_VALUE_1 := :old.KEY_VALUE_1;
         L_message_type := RMSMFM_SUPPLIER.ADDR_DEL;
      elsif UPDATING then
         L_supplier := TO_NUMBER(:new.key_value_1);
         L_event := 'C';
         L_message_type := RMSMFM_SUPPLIER.ADDR_UPD;
      elsif INSERTING then
         L_supplier := TO_NUMBER(:new.key_value_1);
         L_event := 'A';
         L_message_type := RMSMFM_SUPPLIER.ADDR_CRE;
      end if;
      ---
      supplier_xml.build_address(L_addr,
                                 L_event,
                                 L_msg,
                                 O_status,
                                 O_text);
      ---
      if O_status = API_CODES.UNHANDLED_ERROR then
         raise PROGRAM_ERROR;
      end if;
      ---
      L_return := supplier_xml.get_keys(O_text,
                                        L_valid,
                                        L_ret_allow_ind,
                                        NULL,
                                        L_supplier);
      ---
      if not L_return then
         raise PROGRAM_ERROR;
      end if;
      ---
      rmsmfm_supplier.addtoq(L_message_type,
                             L_supplier,
                             L_addr.SEQ_NO,
                             L_addr.ADDR_TYPE,
                             L_ret_allow_ind,
                             NULL,
                             L_msg,
                             O_status,
                             O_text);
      ---
      if O_status = API_CODES.UNHANDLED_ERROR then
         raise PROGRAM_ERROR;
      end if;

   elsif nvl(:new.module,:old.module) = 'WH' then
      if DELETING then
         L_event := 'D';
         L_wh_key_rec.wh := TO_NUMBER(:old.key_value_1);
         L_wh_key_rec.addr_key := TO_NUMBER(:old.addr_key);
         L_message_type := RMSMFM_WH.DTL_DEL;
         L_addr_publish_ind := :old.publish_ind;
      elsif UPDATING then
         if(:old.ADDR_KEY != :new.ADDR_KEY OR
            :old.MODULE != :new.MODULE OR
            :old.KEY_VALUE_1 != :new.KEY_VALUE_1 OR
            nvl(:old.KEY_VALUE_2,'-999') != nvl(:new.KEY_VALUE_2,'-999') OR
            :old.SEQ_NO != :new.SEQ_NO OR
            :old.ADDR_TYPE != :new.ADDR_TYPE OR                        
            :old.PRIMARY_ADDR_IND != :new.PRIMARY_ADDR_IND OR          
            :old.ADD_1 != :new.ADD_1 OR                                
            nvl(:old.ADD_2,'-999') != nvl(:new.ADD_2,'-999') OR                                
            nvl(:old.ADD_3,'-999') != nvl(:new.ADD_3,'-999') OR                                
            :old.CITY != :new.CITY OR                                  
            nvl(:old.STATE,'-999') != nvl(:new.STATE,'-999') OR                                
            :old.COUNTRY_ID != :new.COUNTRY_ID OR                      
            nvl(:old.POST,'-999') != nvl(:new.POST,'-999') OR                                  
            nvl(:old.CONTACT_NAME,'-999') != nvl(:new.CONTACT_NAME,'-999') OR                  
            nvl(:old.CONTACT_PHONE,'-999') != nvl(:new.CONTACT_PHONE,'-999') OR                
            nvl(:old.CONTACT_TELEX,'-999') != nvl(:new.CONTACT_TELEX,'-999') OR                
            nvl(:old.CONTACT_FAX,'-999') != nvl(:new.CONTACT_FAX,'-999') OR                    
            nvl(:old.CONTACT_EMAIL,'-999') != nvl(:new.CONTACT_EMAIL,'-999') OR                
            nvl(:old.ORACLE_VENDOR_SITE_ID,-999) != nvl(:new.ORACLE_VENDOR_SITE_ID,-999) OR
            nvl(:old.EDI_ADDR_CHG,'-999') != nvl(:new.EDI_ADDR_CHG,'-999') OR                  
            nvl(:old.COUNTY,'-999') != nvl(:new.COUNTY,'-999')) then                           
               L_event := 'C';
               L_wh_key_rec.wh := TO_NUMBER(:old.key_value_1);
               L_wh_key_rec.addr_key := TO_NUMBER(:old.addr_key);
               L_message_type := RMSMFM_WH.DTL_UPD;
               L_addr_publish_ind := :old.publish_ind;
         end if;
      elsif INSERTING then
         L_event := 'A';
         L_wh_key_rec.wh := TO_NUMBER(:new.key_value_1);
         L_wh_key_rec.addr_key := TO_NUMBER(:new.addr_key);
         L_message_type := RMSMFM_WH.DTL_ADD;
         L_addr_publish_ind := :new.publish_ind;
      end if;

      if L_message_type is not null then
         if rmsmfm_wh.addtoq(L_error_message,
                             L_message_type,
                             L_wh_key_rec,
                             L_addr_publish_ind) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if;

   elsif nvl(:new.module,:old.module) in ('ST','WFST') then
      if DELETING then
         L_event := 'D';
         L_store_key_rec.store := TO_NUMBER(:old.key_value_1);
         L_store_key_rec.addr_key := TO_NUMBER(:old.addr_key);
         L_message_type := RMSMFM_STORE.DTL_DEL;
         L_addr_publish_ind := :old.publish_ind;
      elsif UPDATING then
         if(:old.ADDR_KEY != :new.ADDR_KEY OR
            :old.MODULE != :new.MODULE OR
            :old.KEY_VALUE_1 != :new.KEY_VALUE_1 OR
            nvl(:old.KEY_VALUE_2,'-999') != nvl(:new.KEY_VALUE_2,'-999') OR
            :old.SEQ_NO != :new.SEQ_NO OR
            :old.ADDR_TYPE != :new.ADDR_TYPE OR                        
            :old.PRIMARY_ADDR_IND != :new.PRIMARY_ADDR_IND OR          
            :old.ADD_1 != :new.ADD_1 OR                                
            nvl(:old.ADD_2,'-999') != nvl(:new.ADD_2,'-999') OR                                
            nvl(:old.ADD_3,'-999') != nvl(:new.ADD_3,'-999') OR                                
            :old.CITY != :new.CITY OR                                  
            nvl(:old.STATE,'-999') != nvl(:new.STATE,'-999') OR                                
            :old.COUNTRY_ID != :new.COUNTRY_ID OR                      
            nvl(:old.POST,'-999') != nvl(:new.POST,'-999') OR                                  
            nvl(:old.CONTACT_NAME,'-999') != nvl(:new.CONTACT_NAME,'-999') OR                  
            nvl(:old.CONTACT_PHONE,'-999') != nvl(:new.CONTACT_PHONE,'-999') OR                
            nvl(:old.CONTACT_TELEX,'-999') != nvl(:new.CONTACT_TELEX,'-999') OR                
            nvl(:old.CONTACT_FAX,'-999') != nvl(:new.CONTACT_FAX,'-999') OR                    
            nvl(:old.CONTACT_EMAIL,'-999') != nvl(:new.CONTACT_EMAIL,'-999') OR                
            nvl(:old.ORACLE_VENDOR_SITE_ID,-999) != nvl(:new.ORACLE_VENDOR_SITE_ID,-999) OR
            nvl(:old.EDI_ADDR_CHG,'-999') != nvl(:new.EDI_ADDR_CHG,'-999') OR                  
            nvl(:old.COUNTY,'-999') != nvl(:new.COUNTY,'-999')) then                           
               L_event := 'C';
               L_store_key_rec.store := TO_NUMBER(:old.key_value_1);
               L_store_key_rec.addr_key := TO_NUMBER(:old.addr_key);
               L_message_type := RMSMFM_STORE.DTL_UPD;
               L_addr_publish_ind := :old.publish_ind;
         end if;
      elsif INSERTING then
         L_event := 'A';
         L_store_key_rec.store := TO_NUMBER(:new.key_value_1);
         L_store_key_rec.addr_key := TO_NUMBER(:new.addr_key);
         L_message_type := RMSMFM_STORE.DTL_ADD;
         L_addr_publish_ind := :new.publish_ind;
      end if;

      if L_message_type is not null then
         if rmsmfm_store.addtoq(L_error_message,
                                L_message_type,
                                L_store_key_rec,
                                L_addr_publish_ind) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if;

   elsif nvl(:new.module, :old.module) = 'PTNR' then
      if DELETING then
         L_event := 'D';
         L_partner_key_rec.partner_type := :old.key_value_1;
         L_partner_key_rec.partner_id   := :old.key_value_2;
         L_partner_key_rec.addr_key     := :old.addr_key;
         L_addr_publish_ind             := :old.publish_ind;
         L_message_type                 := RMSMFM_PARTNER.DTL_DEL;
      elsif UPDATING then
         if(:old.ADDR_KEY != :new.ADDR_KEY OR
            :old.MODULE != :new.MODULE OR
            :old.KEY_VALUE_1 != :new.KEY_VALUE_1 OR
            nvl(:old.KEY_VALUE_2,'-999') != nvl(:new.KEY_VALUE_2,'-999') OR
            :old.SEQ_NO != :new.SEQ_NO OR
            :old.ADDR_TYPE != :new.ADDR_TYPE OR                        
            :old.PRIMARY_ADDR_IND != :new.PRIMARY_ADDR_IND OR          
            :old.ADD_1 != :new.ADD_1 OR                                
            nvl(:old.ADD_2,'-999') != nvl(:new.ADD_2,'-999') OR                                
            nvl(:old.ADD_3,'-999') != nvl(:new.ADD_3,'-999') OR                                
            :old.CITY != :new.CITY OR                                  
            nvl(:old.STATE,'-999') != nvl(:new.STATE,'-999') OR                                
            :old.COUNTRY_ID != :new.COUNTRY_ID OR                      
            nvl(:old.POST,'-999') != nvl(:new.POST,'-999') OR                                  
            nvl(:old.CONTACT_NAME,'-999') != nvl(:new.CONTACT_NAME,'-999') OR                  
            nvl(:old.CONTACT_PHONE,'-999') != nvl(:new.CONTACT_PHONE,'-999') OR                
            nvl(:old.CONTACT_TELEX,'-999') != nvl(:new.CONTACT_TELEX,'-999') OR                
            nvl(:old.CONTACT_FAX,'-999') != nvl(:new.CONTACT_FAX,'-999') OR                    
            nvl(:old.CONTACT_EMAIL,'-999') != nvl(:new.CONTACT_EMAIL,'-999') OR                
            nvl(:old.ORACLE_VENDOR_SITE_ID,-999) != nvl(:new.ORACLE_VENDOR_SITE_ID,-999) OR
            nvl(:old.EDI_ADDR_CHG,'-999') != nvl(:new.EDI_ADDR_CHG,'-999') OR                  
            nvl(:old.COUNTY,'-999') != nvl(:new.COUNTY,'-999')) then                           
               L_event := 'C';
               L_partner_key_rec.partner_type := :old.key_value_1;
               L_partner_key_rec.partner_id   := :old.key_value_2;
               L_partner_key_rec.addr_key     := :old.addr_key;
               L_addr_publish_ind             := :old.publish_ind;
               L_message_type                 := RMSMFM_PARTNER.DTL_UPD;
         end if;
      elsif INSERTING then
         L_event := 'A';
         L_partner_key_rec.partner_type := :new.key_value_1;
         L_partner_key_rec.partner_id   := :new.key_value_2;
         L_partner_key_rec.addr_key     := :new.addr_key;
         L_addr_publish_ind             := :new.publish_ind;
         L_message_type                 := RMSMFM_PARTNER.DTL_ADD;
      end if;

      if L_message_type is not null then
         if rmsmfm_partner.addtoq(L_error_message,
                                  L_message_type,
                                  L_partner_key_rec,
                                  L_addr_publish_ind) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      if L_error_message is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'EC_TABLE_ADR_AIUDR',
                                               to_char(SQLCODE));
      end if;
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

END;
/
