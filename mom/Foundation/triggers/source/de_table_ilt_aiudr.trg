----------------------------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
----------------------------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Tigger Added:  DE_TABLE_ILT_AIUDR
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Tigger               
--------------------------------------
PROMPT Creating TRIGGER 'DE_TABLE_ILT_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_ILT_AIUDR
   AFTER INSERT OR UPDATE OR DELETE ON ITEM_LOC_TRAITS
   FOR EACH ROW

DECLARE
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   L_status             VARCHAR2(1);
   L_item               ITEM_LOC.ITEM%TYPE;
   L_loc                ITEM_LOC.LOC%TYPE;
   L_loc_type           ITEM_LOC.LOC_TYPE%TYPE;
   L_action_type        ITEM_EXPORT_STG.ACTION_TYPE%TYPE;
   L_merchandise_ind    VARCHAR2(1);
   PROGRAM_ERROR        EXCEPTION;

   cursor C_SELL_ITEM_EXIST is
      select merchandise_ind
        from item_master
       where item = L_item
         and sellable_ind = 'Y';

   cursor C_LOC_TYPE is
      select loc_type
        from item_loc
       where item = L_item
         and loc = L_loc;

BEGIN

   L_item := NVL(:new.item, :old.item);

   open C_SELL_ITEM_EXIST;
   fetch C_SELL_ITEM_EXIST into L_merchandise_ind;
   close C_SELL_ITEM_EXIST;

   if L_merchandise_ind is NOT NULL then
      if NVL(:old.launch_date, TO_DATE ('1900-01-01', 'yyyy-mm-dd')) != NVL(:new.launch_date, TO_DATE ('1900-01-01', 'yyyy-mm-dd')) or
         NVL(:old.qty_key_options, '-999') != NVL(:new.qty_key_options, '-999') or
         NVL(:old.manual_price_entry, '-999') != NVL(:new.manual_price_entry, '-999') or
         NVL(:old.deposit_code, '-999') != NVL(:new.deposit_code, '-999') or
         NVL(:old.food_stamp_ind, '-999') != NVL(:new.food_stamp_ind, '-999') or
         NVL(:old.wic_ind, '-999') != NVL(:new.wic_ind, '-999') or
         NVL(:old.proportional_tare_pct, -999) != NVL(:new.proportional_tare_pct, -999) or
         NVL(:old.fixed_tare_value, -999) != NVL(:new.fixed_tare_value, -999) or
         NVL(:old.fixed_tare_uom, '-999') != NVL(:new.fixed_tare_uom, '-999') or
         NVL(:old.reward_eligible_ind, '-999') != NVL(:new.reward_eligible_ind, '-999') or
         NVL(:old.natl_brand_comp_item, '-999') != NVL(:new.natl_brand_comp_item, '-999') or
         NVL(:old.return_policy, '-999') != NVL(:new.return_policy, '-999') or
         NVL(:old.stop_sale_ind, '-999') != NVL(:new.stop_sale_ind, '-999') or
         NVL(:old.elect_mtk_clubs, '-999') != NVL(:new.elect_mtk_clubs, '-999') or
         NVL(:old.report_code, '-999') != NVL(:new.report_code, '-999') or
         NVL(:old.req_shelf_life_on_selection, -999) != NVL(:new.req_shelf_life_on_selection, -999) or
         NVL(:old.req_shelf_life_on_receipt, -999) != NVL(:new.req_shelf_life_on_receipt, -999) or
         NVL(:old.ib_shelf_life, -999) != NVL(:new.ib_shelf_life, -999) or
         NVL(:old.store_reorderable_ind, '-999') != NVL(:new.store_reorderable_ind, '-999') or
         NVL(:old.rack_size, '-999') != NVL(:new.rack_size, '-999') or
         NVL(:old.full_pallet_item, '-999') != NVL(:new.full_pallet_item, '-999') or
         NVL(:old.in_store_market_basket, '-999') != NVL(:new.in_store_market_basket, '-999') or
         NVL(:old.storage_location, '-999') != NVL(:new.storage_location, '-999') or
         NVL(:old.alt_storage_location, '-999') != NVL(:new.alt_storage_location, '-999') or
         NVL(:old.returnable_ind, '-999') != NVL(:new.returnable_ind, '-999') or
         NVL(:old.refundable_ind, '-999') != NVL(:new.refundable_ind, '-999') or
         NVL(:old.back_order_ind, '-999') != NVL(:new.back_order_ind, '-999') then

         L_loc         := NVL(:new.loc, :old.loc);
         L_action_type := DATA_EXPORT_SQL.ITLOC_UPD;

         open C_LOC_TYPE;
         fetch C_LOC_TYPE into L_loc_type;
         close C_LOC_TYPE;
      end if;

      if L_action_type is NOT NULL then
         if DATA_EXPORT_SQL.INS_ITEM_EXPORT_STG(L_error_message,
                                                L_action_type,
                                                L_item,
                                                NULL, -- item_parent
                                                NULL, -- item_grandparent
                                                NULL, -- item_level
                                                NULL, -- tran_level
                                                NULL, -- vat_region
                                                NULL, -- vat_code
                                                NULL, -- vat_type
                                                L_merchandise_ind,
                                                NULL, -- vat_active_date
                                                L_loc,
                                                L_loc_type) = FALSE then
            RAISE PROGRAM_ERROR;
         end if;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_ILT_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/