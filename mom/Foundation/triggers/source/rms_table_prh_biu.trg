PROMPT Creating Trigger 'RMS_TABLE_PRH_BIU'
CREATE OR REPLACE TRIGGER RMS_TABLE_PRH_BIU
 BEFORE INSERT OR UPDATE
 ON PRICE_HIST
 FOR EACH ROW
DECLARE

   L_vdate          PERIOD.VDATE%TYPE := GET_VDATE;
BEGIN
   ---
   if :new.action_date > L_vdate then
      :new.post_date := :new.action_date;
   else
      :new.post_date := L_vdate;
   end if;

   ---
END;
/
