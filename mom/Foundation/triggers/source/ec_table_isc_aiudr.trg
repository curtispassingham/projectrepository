PROMPT Creating Trigger 'EC_TABLE_ISC_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_ISC_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF PRIMARY_SUPP_IND
, PRIMARY_COUNTRY_IND
, UNIT_COST
, LEAD_TIME
, PICKUP_LEAD_TIME
, SUPP_PACK_SIZE
, INNER_PACK_SIZE
, ROUND_LVL
, MIN_ORDER_QTY
, MAX_ORDER_QTY
, PACKING_METHOD
, DEFAULT_UOP
, TI
, HI
, COST_UOM
, TOLERANCE_TYPE
, MAX_TOLERANCE
, MIN_TOLERANCE
 ON ITEM_SUPP_COUNTRY
 FOR EACH ROW
DECLARE

   L_queue_rec     ITEM_MFQUEUE%ROWTYPE := NULL;
   L_status        VARCHAR2(1) := NULL;
   L_text          VARCHAR2(255) := NULL;
   L_item_row      ITEM_MASTER%ROWTYPE := NULL;

   PROGRAM_ERROR   EXCEPTION;
BEGIN

   if DELETING then
      /* For deletes, the row identifier will be published. */

      L_queue_rec.message_type := RMSMFM_ITEMS.ISC_DEL;

      L_queue_rec.item := :old.item;
      L_queue_rec.supplier := :old.supplier;
      L_queue_rec.country_id := :old.origin_country_id;
   else
      if INSERTING then
         L_queue_rec.message_type := RMSMFM_ITEMS.ISC_ADD;
      else
         L_queue_rec.message_type := RMSMFM_ITEMS.ISC_UPD;
      end if;

      /* For inserts and updates, more of the values from the table
         will be published.
      */

      L_queue_rec.item := :new.item;
      L_queue_rec.supplier := :new.supplier;
      L_queue_rec.country_id := :new.origin_country_id;

   end if;

   if not ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                          L_item_row,
                                          L_queue_rec.item) then
     raise PROGRAM_ERROR;
   end if;

   if L_item_row.item_level = L_item_row.tran_level then
      if RMSMFM_ITEMS.ADDTOQ(L_text,
                             L_queue_rec,
                             NULL,               ---I_sellable_ind
                             NULL) = FALSE then  ---I_tran_level_ind
         raise PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_ISC_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END;
/
