PROMPT Creating Trigger 'EC_TABLE_STR_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_STR_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF DEFAULT_WH
, STORE_FORMAT
, INTEGRATED_POS_IND
, CHANNEL_ID
, EMAIL
, STORE_NAME3
, DUNS_NUMBER
, TRANSFER_ZONE
, TOTAL_SQUARE_FT
, REMODEL_DATE
, LANG
, SELLING_SQUARE_FT
, START_ORDER_DAYS
, MALL_NAME
, STORE_OPEN_DATE
, STORE_MGR_NAME
, STORE_CLASS
, STORE_CLOSE_DATE
, STORE_NAME10
, CURRENCY_CODE
, ACQUIRED_DATE
, STORE_NAME
, DUNS_LOC
, FAX_NUMBER
, ORIG_CURRENCY_CODE
, STOCKHOLDING_IND
, PHONE_NUMBER
, STORE
, STOP_ORDER_DAYS
, DISTRICT
, LINEAR_DISTANCE
, STORE_TYPE
, TIMEZONE_NAME
 ON STORE
 FOR EACH ROW
DECLARE

   L_event            VARCHAR2(1)                := 'A';
   L_valid            BOOLEAN;
   L_message_type     VARCHAR2(15)               := NULL;
   L_return           BOOLEAN;
   L_store            STORE.STORE%TYPE;
   L_store_key_rec    RMSMFM_STORE.STORE_KEY_REC;
   L_addr_publish_ind addr.publish_ind%TYPE      := 'N';
   L_error_message    VARCHAR2(1000)              := NULL;
   PROGRAM_ERROR      EXCEPTION;
BEGIN
      if DELETING then
         L_event := 'D';
         L_store_key_rec.store := :old.store;
         L_store_key_rec.addr_key := NULL;
		 L_store_key_rec.store_type := :old.store_type;
	     L_store_key_rec.stockholding_ind := :old.stockholding_ind;
         L_message_type := RMSMFM_STORE.HDR_DEL;
      elsif UPDATING then
         if (:old.store != :new.store OR
             :old.store_name != :new.store_name OR
             :old.store_name10 != :new.store_name10 OR
             :old.store_name3 != :new.store_name3 OR
             :old.store_class != :new.store_class OR
             :old.store_mgr_name != :new.store_mgr_name OR
             :old.store_open_date != :new.store_open_date OR
             nvl(to_char(:old.store_close_date, 'YYYYMMDD'),'00000000')  != nvl(to_char(:new.store_close_date,'YYYYMMDD'),'00000000') OR
             nvl(to_char(:old.acquired_date,'YYYYMMDD'),'00000000') != nvl(to_char(:new.acquired_date,'YYYYMMDD'),'00000000') OR
             nvl(to_char(:old.remodel_date,'YYYYMMDD'),'00000000') != nvl(to_char(:new.remodel_date,'YYYYMMDD'),'00000000') OR
             nvl(:old.fax_number,'-1') != nvl(:new.fax_number,'-1') OR
             nvl(:old.phone_number,'-1') != nvl(:new.phone_number,'-1') OR
             nvl(:old.email,'-1') != nvl(:new.email,'-1') OR
             nvl(:old.total_square_ft,-1) != nvl(:new.total_square_ft,-1) OR
             nvl(:old.selling_square_ft,-1) != nvl(:new.selling_square_ft,-1) OR
             nvl(:old.linear_distance,-1) != nvl(:new.linear_distance,-1) OR
             :old.stockholding_ind != :new.stockholding_ind OR
             nvl(:old.channel_id,-1) != nvl(:new.channel_id,-1) OR
             nvl(:old.store_format,-1) != nvl(:new.store_format,-1) OR
             nvl(:old.mall_name,'-1') != nvl(:new.mall_name,'-1') OR
             :old.district != :new.district OR
             nvl(:old.transfer_zone,-1) != nvl(:new.transfer_zone,-1) OR
             nvl(:old.default_wh,-1) != nvl(:new.default_wh,-1) OR
             nvl(:old.stop_order_days,-1) != nvl(:new.stop_order_days,-1) OR
             nvl(:old.start_order_days,-1) != nvl(:new.start_order_days,-1) OR
             :old.currency_code != :new.currency_code OR
             :old.lang != :new.lang OR
             :old.integrated_pos_ind != :new.integrated_pos_ind OR
             :old.orig_currency_code != :new.orig_currency_code OR
             nvl(:old.duns_number,'-1') != nvl(:new.duns_number,'-1') OR
             nvl(:old.duns_loc,'-1') != nvl(:new.duns_loc,'-1') OR
             nvl(:old.timezone_name, '-1') != nvl(:new.timezone_name, '-1')) then
                L_event := 'C';
                L_store_key_rec.store := :old.STORE;
                L_store_key_rec.addr_key := NULL;
				L_store_key_rec.store_type := :new.store_type;
				L_store_key_rec.stockholding_ind := :new.stockholding_ind;
                L_message_type := RMSMFM_STORE.HDR_UPD;
         end if;
      elsif INSERTING then
         L_event := 'A';
         L_store_key_rec.store := :new.store;
         L_store_key_rec.addr_key := NULL;
		 L_store_key_rec.store_type := :new.store_type;
	     L_store_key_rec.stockholding_ind := :new.stockholding_ind;
         L_message_type := RMSMFM_STORE.HDR_ADD;
      end if;

      if rmsmfm_store.addtoq(L_error_message,
                             L_message_type,
                             L_store_key_rec,
                             L_addr_publish_ind) = FALSE then
         raise PROGRAM_ERROR;
      end if;

EXCEPTION
   when OTHERS then
      if L_error_message is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'EC_TABLE_STORE_AIUDR',
                                               to_char(SQLCODE));
      end if;
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

END;
/