PROMPT Creating Trigger 'EC_TABLE_DEP_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_DEP_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON DEPS
 FOR EACH ROW
DECLARE


   L_message_type       MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
   L_dept               DEPS.DEPT%TYPE;
   L_dept_rec           DEPS%ROWTYPE;
   L_error_msg          VARCHAR2(255) := NULL;
   L_status             VARCHAR2(1);
   program_error EXCEPTION;
BEGIN

  if inserting then
    L_message_type := RMSMFM_MERCHHIER.DEP_ADD;
    L_dept := :new.dept;
    L_dept_rec.dept := :new.dept;
    L_dept_rec.dept_name := :new.dept_name;
    L_dept_rec.buyer := :new.buyer;
    L_dept_rec.merch := :new.merch;
    L_dept_rec.profit_calc_type := :new.profit_calc_type;
    L_dept_rec.purchase_type := :new.purchase_type;
    L_dept_rec.group_no := :new.group_no;
    L_dept_rec.bud_int := :new.bud_int;
    L_dept_rec.bud_mkup := :new.bud_mkup;
    L_dept_rec.total_market_amt := :new.total_market_amt;
    L_dept_rec.markup_calc_type := :new.markup_calc_type;
    L_dept_rec.otb_calc_type := :new.otb_calc_type;
    L_dept_rec.dept_vat_incl_ind := :new.dept_vat_incl_ind;
  elsif updating then
    L_message_type := RMSMFM_MERCHHIER.DEP_UPD;
    L_dept := :old.dept;
    L_dept_rec.dept := :old.dept;
    L_dept_rec.dept_name := :new.dept_name;
    L_dept_rec.buyer := :new.buyer;
    L_dept_rec.merch := :new.merch;
    L_dept_rec.profit_calc_type := :new.profit_calc_type;
    L_dept_rec.purchase_type := :new.purchase_type;
    L_dept_rec.group_no := :new.group_no;
    L_dept_rec.bud_int := :new.bud_int;
    L_dept_rec.bud_mkup := :new.bud_mkup;
    L_dept_rec.total_market_amt := :new.total_market_amt;
    L_dept_rec.markup_calc_type := :new.markup_calc_type;
    L_dept_rec.otb_calc_type := :new.otb_calc_type;
    L_dept_rec.dept_vat_incl_ind := :new.dept_vat_incl_ind;
  else -- Deleting
    L_message_type := RMSMFM_MERCHHIER.DEP_DEL;
    L_dept := :old.dept;
    IF MERCH_NOTIFY_API_SQL.DELETE_DEPARTMENT( L_error_msg,
                                               L_dept) = FALSE THEN
       RAISE program_error;
    END IF;

  end if;

  if NOT RMSMFM_MERCHHIER.ADDTOQ(L_error_msg, 
                                 L_message_type, 
                                 null, 
                                 null, 
                                 null, 
                                 null, 
                                 L_dept, 
                                 L_dept_rec, 
                                 null, 
                                 null, 
                                 null, 
                                 null) THEN
     RAISE program_error;
  end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_DEP_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
