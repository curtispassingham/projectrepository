PROMPT Creating Trigger 'RMS_TABLE_CPH_BUR'
CREATE OR REPLACE TRIGGER RMS_TABLE_CPH_BUR
 BEFORE UPDATE
 ON COMP_PRICE_HIST
 FOR EACH ROW
DECLARE

   L_vdate      PERIOD.VDATE%TYPE := GET_VDATE;
BEGIN

   if UPDATING then
      :new.post_date := L_vdate;
   end if;

END;
/
