PROMPT Creating Trigger 'RMS_TABLE_REQ_AIR'
CREATE OR REPLACE TRIGGER RMS_TABLE_REQ_AIR
 AFTER INSERT
 ON REQ_DOC
 FOR EACH ROW
DECLARE

  L_error_message      VARCHAR2(255)              := NULL;
  L_attached           BOOLEAN                    := NULL;
  L_date               DATE                       := GET_VDATE;
BEGIN
NULL;

if INSERTING then
   if (:new.module = 'PO' or :new.module = 'POIT') then
      ---
      if LC_SQL.ATTACHED_LC(L_error_message,
                            L_attached,
                            :new.key_value_1) = FALSE then
         RAISE_APPLICATION_ERROR(-20000,'INTERNAL ERROR: TRG_REQ_DOC - LC_SQL.ATTACHED_LC failed.');
      end if;
      ---
      if L_attached = TRUE then
         if :new.module = 'PO' then
            insert into ORD_LC_AMENDMENTS(order_no,
                                          item,
                                          change_type,
                                          new_value,
                                          change_date)
            select :new.key_value_1,
                   NULL,
                   'ARQD',
                   d.doc_id,
                   L_date
              from doc d,
                   doc_link dl
            where d.lc_ind = 'Y'
              and dl.module = 'LC'
              and d.doc_type = dl.doc_type
              and d.doc_id = :new.doc_id;

         else
            insert into ORD_LC_AMENDMENTS(order_no,
                                          item,
                                          change_type,
                                          new_value,
                                          change_date)
            select :new.key_value_1,
                   :new.key_value_2,
                   'ARQD',
                   d.doc_id,
                   L_date
              from doc d,
                   doc_link dl
            where d.lc_ind = 'Y'
              and dl.module = 'LC'
              and d.doc_type = dl.doc_type
              and d.doc_id = :new.doc_id;

         end if;
      end if;
   end if;
end if;
END;
/
