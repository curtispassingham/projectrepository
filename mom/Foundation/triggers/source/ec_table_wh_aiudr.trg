PROMPT Creating Trigger 'EC_TABLE_WH_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_WH_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF DELIVERY_POLICY
, DUNS_NUMBER
, CURRENCY_CODE
, STOCKHOLDING_IND
, PHYSICAL_WH
, WH_NAME
, WH
, EMAIL
, REDIST_WH_IND
, BREAK_PACK_IND
, CHANNEL_ID
, DUNS_LOC
, ORG_ENTITY_TYPE
 ON WH
 FOR EACH ROW

DECLARE
   L_event              VARCHAR2(1)           := 'A';
   L_valid              BOOLEAN;
   L_message_type       VARCHAR2(15)          := NULL;
   L_return             BOOLEAN;
   L_wh                 WH.WH%TYPE;
   L_wh_key_rec         RMSMFM_WH.WH_KEY_REC;
   L_addr_publish_ind   ADDR.PUBLISH_IND%TYPE := 'N';
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   PROGRAM_ERROR        EXCEPTION;

BEGIN

   if DELETING then
      L_event := 'D';
      L_wh_key_rec.wh := :old.wh;
      L_wh_key_rec.addr_key := NULL;
      if :old.wh = :old.physical_wh then
         L_wh_key_rec.wh_type := 'P';
      else
           L_wh_key_rec.wh_type := 'V';
      end if;
      L_message_type := RMSMFM_WH.HDR_DEL;

   elsif UPDATING then
      if (:old.wh != :new.wh OR
          :old.wh_name != :new.wh_name OR
          (nvl(:old.email,' ')             != nvl(:new.email,' ')) OR
          :old.stockholding_ind != :new.stockholding_ind OR
          (nvl(:old.channel_id,-999)       != nvl(:new.channel_id,-999)) OR
          :old.currency_code != :new.currency_code OR
          (nvl(:old.duns_number,' ')       != nvl(:new.duns_number,' ')) OR
          (nvl(:old.duns_loc,' ')          != nvl(:new.duns_loc,' ')) OR
          :old.physical_wh != :new.physical_wh OR
          :old.break_pack_ind != :new.break_pack_ind OR
          :old.redist_wh_ind != :new.redist_wh_ind OR
          :old.delivery_policy != :new.delivery_policy OR
          :old.org_entity_type != :new.org_entity_type) then
          L_event := 'C';
          L_wh_key_rec.wh := :new.wh;
          L_wh_key_rec.addr_key := NULL;
          if :new.wh = :new.physical_wh then
             L_wh_key_rec.wh_type := 'P';
          else
             L_wh_key_rec.wh_type := 'V';
          end if;
          L_message_type := RMSMFM_WH.HDR_UPD;

          if :old.org_entity_type != 'R' and :new.org_entity_type = 'R' then
             L_message_type := RMSMFM_WH.HDR_ADD;
          end if;
       end if;

   elsif INSERTING then
      L_event := 'A';
      L_wh_key_rec.wh := :new.wh;
      L_wh_key_rec.addr_key := NULL;
      if :new.wh = :new.physical_wh then
         L_wh_key_rec.wh_type := 'P';
      else
         L_wh_key_rec.wh_type := 'V';
      end if;

      L_message_type := RMSMFM_WH.HDR_ADD;
   end if;

   if nvl(:new.org_entity_type,:old.org_entity_type) = 'R' then
      if rmsmfm_wh.addtoq(L_error_message,
                          L_message_type,
                          L_wh_key_rec,
                          L_addr_publish_ind) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   end if;


EXCEPTION
   when OTHERS then
      if L_error_message is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'EC_TABLE_WH_AIUDR',
                                               to_char(SQLCODE));
      end if;
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

END;
/
