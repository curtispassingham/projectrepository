PROMPT Creating Trigger 'DE_TABLE_CO_AIUR'
CREATE OR REPLACE TRIGGER DE_TABLE_CO_AIUR
 AFTER INSERT OR UPDATE
 ON COMPHEAD
 FOR EACH ROW
DECLARE

   L_action_type   ORGHIER_EXPORT_STG.ACTION_TYPE%TYPE;
   L_company       COMPHEAD.COMPANY%TYPE;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_status        VARCHAR2(1);

   L_org_lvl_insert     VARCHAR2(1) := 'N';

   program_error EXCEPTION;

BEGIN

   if inserting then
      L_action_type := DATA_EXPORT_SQL.COMP_ADD;
      L_company     := :new.company;
      L_org_lvl_insert   := 'Y';
   elsif updating then
      L_action_type := DATA_EXPORT_SQL.COMP_UPD;
      L_company := :old.company;

      if :old.co_name != :new.co_name then
         L_org_lvl_insert   := 'Y';
      end if;

   end if;

   if L_org_lvl_insert = 'Y' then
      if NOT DATA_EXPORT_SQL.INS_ORGHIER_EXPORT_STG(L_error_msg,
                                                    L_action_type,
                                                    L_company,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL) then
         RAISE program_error;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_CO_AIUR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
