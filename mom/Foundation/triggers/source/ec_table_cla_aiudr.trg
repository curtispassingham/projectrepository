PROMPT Creating Trigger 'EC_TABLE_CLA_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_CLA_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON CLASS
 FOR EACH ROW
DECLARE


   L_message_type       MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
   L_class              CLASS.CLASS%TYPE;
   L_dept               CLASS.DEPT%TYPE;
   L_class_rec          CLASS%ROWTYPE;
   L_error_msg          VARCHAR2(255) := NULL;
   L_status             VARCHAR2(1);
   program_error EXCEPTION;
BEGIN

  if inserting then
    L_message_type := RMSMFM_MERCHHIER.CLS_ADD;
    L_class := :new.class;
    L_dept := :new.dept;
    L_class_rec.class := :new.class;
    L_class_rec.dept := :new.dept;
    L_class_rec.class_name := :new.class_name;
    L_class_rec.class_vat_ind := :new.class_vat_ind;
  elsif updating then
    L_message_type := RMSMFM_MERCHHIER.CLS_UPD;
    L_class := :old.class;
    L_dept := :old.dept;
    L_class_rec.class := :old.class;
    L_class_rec.dept := :old.dept;
    L_class_rec.class_name := :new.class_name;
    L_class_rec.class_vat_ind := :new.class_vat_ind;
  else -- Deleting
    L_message_type := RMSMFM_MERCHHIER.CLS_DEL;
    L_class := :old.class;
    L_dept := :old.dept;
  end if;

  if NOT RMSMFM_MERCHHIER.ADDTOQ(L_error_msg, 
                                 L_message_type, 
                                 null, 
                                 null, 
                                 null, 
                                 null, 
                                 L_dept, 
                                 null, 
                                 L_class, 
                                 L_class_rec, 
                                 null, 
                                 null) THEN
     RAISE program_error;
  end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_CLA_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
/
