PROMPT Creating Trigger 'EC_TABLE_ISD_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_ISD_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF PRESENTATION_METHOD
, LENGTH
, WIDTH
, HEIGHT
, LWH_UOM
, WEIGHT
, NET_WEIGHT
, WEIGHT_UOM
, LIQUID_VOLUME
, LIQUID_VOLUME_UOM
, STAT_CUBE
, TARE_WEIGHT
, TARE_TYPE
 ON ITEM_SUPP_COUNTRY_DIM
 FOR EACH ROW
DECLARE

   L_queue_rec     ITEM_MFQUEUE%ROWTYPE := NULL;
   L_status        VARCHAR2(1) := NULL;
   L_text          VARCHAR2(255) := NULL;
   L_item_row      ITEM_MASTER%ROWTYPE := NULL;
   
   PROGRAM_ERROR   EXCEPTION;
BEGIN
   if DELETING then
   
      L_queue_rec.message_type := RMSMFM_ITEMS.ISCD_DEL;
      L_queue_rec.item := :old.item;
      L_queue_rec.supplier := :old.supplier;
      L_queue_rec.country_id := :old.origin_country;
      L_queue_rec.dim_object := :old.dim_object;
   else
      if INSERTING then
         L_queue_rec.message_type := RMSMFM_ITEMS.ISCD_ADD;
      else
         L_queue_rec.message_type := RMSMFM_ITEMS.ISCD_UPD;
      end if;

      L_queue_rec.item := :new.item;
      L_queue_rec.supplier := :new.supplier;
      L_queue_rec.country_id := :new.origin_country;
      L_queue_rec.dim_object := :new.dim_object;

   end if;

   if not ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                          L_item_row,
                                          L_queue_rec.item) then
      raise PROGRAM_ERROR;
   end if;

   if L_item_row.item_level = L_item_row.tran_level then
      if RMSMFM_ITEMS.ADDTOQ(L_text,
                             L_queue_rec,
                             NULL,               ---I_sellable_ind
                             NULL) = FALSE then  ---I_tran_level_ind
         raise PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_ISD_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END;
/
