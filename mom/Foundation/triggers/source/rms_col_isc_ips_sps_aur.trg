PROMPT Creating Trigger 'RMS_COL_ISC_IPS_SPS_AUR'
CREATE OR REPLACE TRIGGER RMS_COL_ISC_IPS_SPS_AUR
 AFTER UPDATE OF TI
, LEAD_TIME
, SUPP_PACK_SIZE
, INNER_PACK_SIZE
, HI
, PRIMARY_SUPP_IND
, PRIMARY_COUNTRY_IND
 ON ITEM_SUPP_COUNTRY
 FOR EACH ROW
DECLARE

   L_status          VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   if updating then
      if :new.inner_pack_size   != :old.inner_pack_size or
         :new.supp_pack_size    != :old.supp_pack_size or
         :new.ti                != :old.ti or
         :new.hi                != :old.hi or
         NVL(:new.lead_time,-1) != NVL(:old.lead_time,-1)  then
   
         insert into repl_item_loc_updates (item,
                                            supplier,
                                            origin_country_id,
                                            change_type)
                                     select :new.item,
                                            :new.supplier,
                                            :new.origin_country_id,
                                            'ISC'
                                       from dual
                                      where exists (select 'x'
                                                      from repl_item_loc ril
                                                     where ril.item = :new.item)
                                         or exists (select 'x'
                                                      from repl_item_loc ril
                                                     where ril.primary_pack_no = :new.item);
      end if;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'RMS_COL_ISC_IPS_SPS_AUR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

END RMS_COL_ISC_IPS_SPS_AUR;
/
