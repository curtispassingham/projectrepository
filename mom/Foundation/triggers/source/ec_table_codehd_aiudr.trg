PROMPT Creating Trigger 'EC_TABLE_CODEHD_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_CODEHD_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON CODE_HEAD
 FOR EACH ROW
DECLARE

   L_record          CODE_HEAD%ROWTYPE                   :=  NULL;
   L_action_type     VARCHAR2(1)                         :=  NULL;
   L_message         rib_sxw.SXWHandle;
   L_status          VARCHAR2(1)                         :=  NULL;
   L_text            VARCHAR2(255)                       :=  NULL;
   L_message_type    CODES_MFQUEUE.MESSAGE_TYPE%TYPE     :=  NULL;

   PROGRAM_ERROR     EXCEPTION;
BEGIN

   if DELETING then

      L_action_type := 'D';
      L_message_type := RMSMFM_SEEDDATA.HDR_DEL_TYPE;
      L_record.code_type := :old.code_type;

   else
      if INSERTING then

         L_action_type := 'A';
         L_message_type := RMSMFM_SEEDDATA.HDR_CRE_TYPE;

      else

         L_action_type := 'M';
         L_message_type := RMSMFM_SEEDDATA.HDR_MOD_TYPE;

         if ((:old.code_type        = :new.code_type) and
             (:old.code_type_desc   = :new.code_type_desc)) then
            
            return;

         end if;
      end if;

      L_record.code_type       := :new.code_type;
      L_record.code_type_desc  := :new.code_type_desc;

   end if;

   if not CODE_HEAD_XML.BUILD_MESSAGE(L_status,
                                      L_text,
                                      L_message,
                                      L_record,
                                      L_action_type) then
      raise PROGRAM_ERROR;
   end if;

   RMSMFM_SEEDDATA.ADDTOQ(L_status,
                          L_text,
                          L_message_type,
                          L_record.code_type,
                          L_message);

   if L_status = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_CODEHD_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END EC_TABLE_CODEHD_AIUDR;
/
