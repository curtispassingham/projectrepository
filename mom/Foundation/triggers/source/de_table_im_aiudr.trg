----------------------------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
----------------------------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Tigger Added:  DE_TABLE_IM_AIUDR
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Tigger
--------------------------------------
PROMPT Creating Trigger "DE_TABLE_IM_AIUDR"
CREATE OR REPLACE TRIGGER DE_TABLE_IM_AIUDR
   AFTER INSERT OR UPDATE OR DELETE ON ITEM_MASTER
   FOR EACH ROW

DECLARE
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   L_status             VARCHAR2(1);
   L_item               ITEM_MASTER.ITEM%TYPE;
   L_item_parent        ITEM_MASTER.ITEM_PARENT%TYPE;
   L_item_grandparent   ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
   L_item_level         ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level         ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_action_type        ITEM_EXPORT_STG.ACTION_TYPE%TYPE;
   PROGRAM_ERROR        EXCEPTION;

BEGIN
   if inserting then
      if :new.sellable_ind = 'Y' then
         L_item             := :new.item;
         L_item_parent      := :new.item_parent;
         L_item_grandparent := :new.item_grandparent;
         L_item_level       := :new.item_level;
         L_tran_level       := :new.tran_level;
         L_action_type      := DATA_EXPORT_SQL.ITEM_CRE;
      end if;
   elsif updating then
      if :old.sellable_ind = 'Y' then
         -- When updating, need to make sure that at least         --
         -- one of the columns being published has changed.        --
         -- If none of the columns have changed, exit the trigger. --

         if (:old.status                                 != :new.status)
            or (NVL(:old.diff_1,'-999')                  != NVL(:new.diff_1,'-999'))
            or (NVL(:old.diff_2,'-999')                  != NVL(:new.diff_2,'-999'))
            or (NVL(:old.diff_3,'-999')                  != NVL(:new.diff_3,'-999'))
            or (NVL(:old.diff_4,'-999')                  != NVL(:new.diff_4,'-999'))
            or (:old.dept                                != :new.dept)
            or (:old.class                               != :new.class)
            or (:old.subclass                            != :new.subclass)
            or (:old.item_desc                           != :new.item_desc)
            or (:old.short_desc                          != :new.short_desc)
            or (:old.desc_up                             != :new.desc_up)
            or (:old.primary_ref_item_ind                != :new.primary_ref_item_ind)
            or (:old.cost_zone_group_id                  != :new.cost_zone_group_id)
            or (:old.standard_uom                        != :new.standard_uom)
            or (NVL(:old.uom_conv_factor,-999)           != NVL(:new.uom_conv_factor,-999))
            or (NVL(:old.package_size,-999)              != NVL(:new.package_size,-999))
            or (NVL(:old.package_uom,'-999')             != NVL(:new.package_uom,'-999'))
            or (:old.merchandise_ind                     != :new.merchandise_ind)
            or (:old.store_ord_mult                      != :new.store_ord_mult)
            or (:old.forecast_ind                        != :new.forecast_ind)
            or (:old.original_retail                     != :new.original_retail)
            or (NVL(:old.mfg_rec_retail,-999)            != NVL(:new.mfg_rec_retail,-999))
            or (NVL(:old.retail_label_type,'-999')       != NVL(:new.retail_label_type,'-999'))
            or (NVL(:old.retail_label_value,-999)        != NVL(:new.retail_label_value,-999))
            or (NVL(:old.handling_temp,'-999')           != NVL(:new.handling_temp,'-999'))
            or (NVL(:old.handling_sensitivity,'-999')    != NVL(:new.handling_sensitivity,'-999'))
            or (:old.catch_weight_ind                    != :new.catch_weight_ind)
            or (NVL(:old.waste_type,'-999')              != NVL(:new.waste_type,'-999'))
            or (NVL(:old.waste_pct,-999)                 != NVL(:new.waste_pct,-999))
            or (NVL(:old.default_waste_pct,-999)         != NVL(:new.default_waste_pct,-999))
            or (:old.const_dimen_ind                     != :new.const_dimen_ind)
            or (NVL(:old.order_as_type,'-999')           != NVL(:new.order_as_type,'-999'))
            or (NVL(:old.comments,'-999')                != NVL(:new.comments,'-999'))
            or (NVL(:old.item_service_level,'-999')      != NVL(:new.item_service_level,'-999'))
            or (:old.gift_wrap_ind                       != :new.gift_wrap_ind)
            or (:old.ship_alone_ind                      != :new.ship_alone_ind)
            or (:old.notional_pack_ind                   != :new.notional_pack_ind)
            or (:old.soh_inquiry_at_pack_ind             != :new.soh_inquiry_at_pack_ind)
            or (:old.perishable_ind                      != :new.perishable_ind)
            or (NVL(:old.brand_name,'-999')              != NVL(:new.brand_name, '-999'))
            or (NVL(:old.product_classification, '-999') != NVL(:new.product_classification, '-999'))
            or (:old.tran_level                          != :new.tran_level)
            or (NVL(:old.item_desc_secondary,'-999')     != NVL(:new.item_desc_secondary,'-999'))
            or (:old.item_aggregate_ind	                 != :new.item_aggregate_ind)
            or (:old.diff_1_aggregate_ind	             != :new.diff_1_aggregate_ind)
            or (:old.diff_2_aggregate_ind	             != :new.diff_2_aggregate_ind)
            or (:old.diff_3_aggregate_ind	             != :new.diff_3_aggregate_ind) 
            or (:old.diff_4_aggregate_ind	             != :new.diff_4_aggregate_ind)  then

             L_item             := :old.item;
             L_item_parent      := :old.item_parent;
             L_item_grandparent := :old.item_grandparent;
             L_item_level       := :old.item_level;
             L_tran_level       := :old.tran_level;
             L_action_type := DATA_EXPORT_SQL.ITEM_UPD;
         end if;
     end if;
   else
      if :old.sellable_ind = 'Y' then
         L_item             := :old.item;
         L_item_parent      := :old.item_parent;
         L_item_grandparent := :old.item_grandparent;
         L_item_level       := :old.item_level;
         L_tran_level       := :old.tran_level;
         L_action_type      := DATA_EXPORT_SQL.ITEM_DEL;
      end if;
   end if;

   if L_action_type is NOT NULL then
      if DATA_EXPORT_SQL.INS_ITEM_EXPORT_STG(L_error_message,
                                             L_action_type,
                                             L_item,
                                             L_item_parent,
                                             L_item_grandparent,
                                             L_item_level,
                                             L_tran_level,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL) = FALSE then
         RAISE PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_IM_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/