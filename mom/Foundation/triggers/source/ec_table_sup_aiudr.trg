PROMPT Creating Trigger 'EC_TABLE_SUP_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_SUP_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON SUPS
 FOR EACH ROW
DECLARE

   L_program_error        exception;
   L_supplier             SUPS.SUPPLIER%TYPE;
   L_ret_allow_ind        VARCHAR2(2) := NULL;
   L_event                VARCHAR2(1) := 'A';
   L_valid                BOOLEAN;
   O_status               VARCHAR2(100);
   O_text                 VARCHAR2(255);
   L_sups                 SUPS%ROWTYPE;
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE := NULL;
   L_msg                  CLOB;
   L_message_type         VARCHAR2(15) := NULL;
   L_return               BOOLEAN;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_text,
                                            L_system_options_rec) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- Do not process Vendor level (supplier parent) information if supplier sites are used
   if L_system_options_rec.supplier_sites_ind = 'Y' and
      ((:new.SUPPLIER_PARENT is NULL and INSERTING) OR
       (:new.SUPPLIER_PARENT is NULL and UPDATING) OR
       -- Only old state will have value for SUPPLIER_PARENT when deleting
       (:old.SUPPLIER_PARENT is NULL and DELETING)) then
      return;
   end if;

   L_sups.SUPPLIER := :new.SUPPLIER;
   L_sups.SUP_NAME := :new.SUP_NAME;
   L_sups.SUP_NAME_SECONDARY := :new.SUP_NAME_SECONDARY;
   L_sups.CONTACT_NAME := :new.CONTACT_NAME;
   L_sups.CONTACT_PHONE := :new.CONTACT_PHONE;
   L_sups.CONTACT_FAX := :new.CONTACT_FAX;
   L_sups.CONTACT_PAGER := :new.CONTACT_PAGER;
   L_sups.SUP_STATUS := :new.SUP_STATUS;
   L_sups.QC_IND := :new.QC_IND;
   L_sups.QC_PCT := :new.QC_PCT;
   L_sups.QC_FREQ := :new.QC_FREQ;
   L_sups.VC_IND := :new.VC_IND;
   L_sups.VC_PCT := :new.VC_PCT;
   L_sups.VC_FREQ := :new.VC_FREQ;
   L_sups.CURRENCY_CODE := :new.CURRENCY_CODE;
   L_sups.LANG := :new.LANG;
   L_sups.TERMS := :new.TERMS;
   L_sups.FREIGHT_TERMS := :new.FREIGHT_TERMS;
   L_sups.RET_ALLOW_IND := :new.RET_ALLOW_IND;
   L_sups.RET_AUTH_REQ := :new.RET_AUTH_REQ;
   L_sups.RET_MIN_DOL_AMT := :new.RET_MIN_DOL_AMT;
   L_sups.RET_COURIER := :new.RET_COURIER;
   L_sups.HANDLING_PCT := :new.HANDLING_PCT;
   L_sups.EDI_PO_IND := :new.EDI_PO_IND;
   L_sups.EDI_PO_CHG := :new.EDI_PO_CHG;
   L_sups.EDI_PO_CONFIRM := :new.EDI_PO_CONFIRM;
   L_sups.EDI_ASN := :new.EDI_ASN;
   L_sups.EDI_SALES_RPT_FREQ := :new.EDI_SALES_RPT_FREQ;
   L_sups.EDI_SUPP_AVAILABLE_IND := :new.EDI_SUPP_AVAILABLE_IND;
   L_sups.EDI_CONTRACT_IND := :new.EDI_CONTRACT_IND;
   L_sups.EDI_INVC_IND := :new.EDI_INVC_IND;
   L_sups.COST_CHG_PCT_VAR := :new.COST_CHG_PCT_VAR;
   L_sups.COST_CHG_AMT_VAR := :new.COST_CHG_AMT_VAR;
   L_sups.REPLEN_APPROVAL_IND := :new.REPLEN_APPROVAL_IND;
   L_sups.SHIP_METHOD := :new.SHIP_METHOD;
   L_sups.PAYMENT_METHOD := :new.PAYMENT_METHOD;
   L_sups.CONTACT_TELEX := :new.CONTACT_TELEX;
   L_sups.CONTACT_EMAIL := :new.CONTACT_EMAIL;
   L_sups.SETTLEMENT_CODE := :new.SETTLEMENT_CODE;
   L_sups.PRE_MARK_IND := :new.PRE_MARK_IND;
   L_sups.AUTO_APPR_INVC_IND := :new.AUTO_APPR_INVC_IND;
   L_sups.DBT_MEMO_CODE := :new.DBT_MEMO_CODE;
   L_sups.FREIGHT_CHARGE_IND := :new.FREIGHT_CHARGE_IND;
   L_sups.AUTO_APPR_DBT_MEMO_IND := :new.AUTO_APPR_DBT_MEMO_IND;
   L_sups.PREPAY_INVC_IND := :new.PREPAY_INVC_IND;
   L_sups.BACKORDER_IND := :new.BACKORDER_IND;
   L_sups.VAT_REGION := :new.VAT_REGION;
   L_sups.INV_MGMT_LVL := :new.INV_MGMT_LVL;
   L_sups.SERVICE_PERF_REQ_IND := :new.SERVICE_PERF_REQ_IND;
   L_sups.INVC_PAY_LOC := :new.INVC_PAY_LOC;
   L_sups.INVC_RECEIVE_LOC := :new.INVC_RECEIVE_LOC;
   L_sups.ADDINVC_GROSS_NET := :new.ADDINVC_GROSS_NET;
   L_sups.DELIVERY_POLICY := :new.DELIVERY_POLICY;
   L_sups.COMMENT_DESC := :new.COMMENT_DESC;
   L_sups.DEFAULT_ITEM_LEAD_TIME := :new.DEFAULT_ITEM_LEAD_TIME;
   L_sups.DUNS_NUMBER := :new.DUNS_NUMBER;
   L_sups.DUNS_LOC := :new.DUNS_LOC;
   L_sups.BRACKET_COSTING_IND := :new.BRACKET_COSTING_IND;
   L_sups.VMI_ORDER_STATUS := :new.VMI_ORDER_STATUS;
   L_sups.DSD_IND := :new.DSD_IND;
   L_sups.SUPPLIER_PARENT := :new.SUPPLIER_PARENT;
   L_sups.SUP_QTY_LEVEL := :new.SUP_QTY_LEVEL;

   if DELETING then
      L_supplier := TO_NUMBER(:old.supplier);
      L_event := 'D';
      L_sups.SUPPLIER := :old.SUPPLIER;
      L_message_type := RMSMFM_SUPPLIER.VEND_DEL;
   elsif UPDATING then
      L_supplier := TO_NUMBER(:new.supplier);
      L_event := 'C';
      L_message_type := RMSMFM_SUPPLIER.VEND_UPD;
   elsif INSERTING then
      L_supplier := TO_NUMBER(:new.supplier);
      L_event := 'A';
      L_message_type := RMSMFM_SUPPLIER.VEND_ADD;
   end if;
   ---
   supplier_xml.build_supplier(L_sups,
                               L_event,
                               L_msg,
                               O_status,
                               O_text);
   ---
   if O_status = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
   end if;
   ---
   if L_message_type != RMSMFM_SUPPLIER.VEND_DEL then
      L_return := supplier_xml.get_keys(O_text,
                                        L_valid,
                                        L_ret_allow_ind,
                                        L_sups.ret_allow_ind,
                                        L_supplier);
      ---
      if not L_return then
         raise PROGRAM_ERROR;
      end if;
   end if;
   ---
   rmsmfm_supplier.addtoq(L_message_type,
                          L_supplier,
                          NULL,
                          NULL,
                          L_ret_allow_ind,
                          NULL,
                          L_msg,
                          O_status,
                          O_text);
   ---
   if O_status = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_SUP_AIUDR');
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, O_text);

END;
/
