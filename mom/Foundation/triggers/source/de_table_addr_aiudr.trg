PROMPT Creating Trigger 'DE_TABLE_ADDR_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_ADDR_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON ADDR
 FOR EACH ROW
DECLARE

   L_action_type        STORE_EXPORT_STG.ACTION_TYPE%TYPE;
   L_store              STORE.STORE%TYPE;
   L_addr_key           ADDR.ADDR_KEY%TYPE;
   L_addr_type          ADDR.ADDR_TYPE%TYPE;
   L_primary_addr_ind   ADDR.PRIMARY_ADDR_IND%TYPE;
   L_stdtl_insert       VARCHAR2(1) := 'N';

   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_status        VARCHAR2(1);

   program_error EXCEPTION;

BEGIN

   if NVL(:new.module,:old.module) in ('ST','WFST') then
      if inserting then
         L_action_type      := DATA_EXPORT_SQL.STDTL_ADD;
         L_store            := :new.key_value_1;
         L_addr_key         := :new.addr_key;
         L_addr_type        := :new.addr_type;
         L_primary_addr_ind := :new.primary_addr_ind;

         L_stdtl_insert := 'Y';
      elsif updating then
         L_action_type        := DATA_EXPORT_SQL.STDTL_UPD;
         L_store              := NVL(:new.key_value_1, :old.key_value_1);
         L_addr_key           := :old.addr_key;
         L_addr_type          := NVL(:new.addr_type, :old.addr_type);
         L_primary_addr_ind   := NVL(:new.primary_addr_ind, :old.primary_addr_ind);

         if :old.primary_addr_ind != :new.primary_addr_ind or
            :old.add_1 != :new.add_1 or
            NVL(:old.add_2, '-999') != NVL(:new.add_2, '-999') or
            NVL(:old.add_3, '-999') != NVL(:new.add_3, '-999') or
            :old.city != :new.city or
            NVL(:old.state, '-999') != NVL(:new.state, '-999') or
            :old.country_id != :new.country_id or
            NVL(:old.post, '-999') != NVL (:new.post, '-999') or
            NVL(:old.contact_name, '-999') != NVL(:new.contact_name, '-999') or
            NVL(:old.contact_phone, '-999') != NVL(:new.contact_phone, '999') or
            NVL(:old.contact_fax, '-999') != NVL(:new.contact_fax, '-999') or
            NVL(:old.contact_email, '-999') != NVL(:new.contact_email, '-999') or
            NVL(:old.county, '-999') != NVL(:new.county, '-999') or
            NVL(:old.jurisdiction_code, '-999') != NVL(:new.jurisdiction_code, '-999') then
            L_stdtl_insert   := 'Y';
         end if;

      else -- Deleting
         L_action_type      := DATA_EXPORT_SQL.STDTL_DEL;
         L_store            := :old.key_value_1;
         L_addr_key         := :old.addr_key;
         L_addr_type        := :old.addr_type;
         L_primary_addr_ind := :old.primary_addr_ind;

         L_stdtl_insert := 'Y';
      end if;

      if L_stdtl_insert = 'Y' then
         if NOT DATA_EXPORT_SQL.INS_STORE_EXPORT_STG(L_error_msg,
                                                     L_action_type,
                                                     L_store,
                                                     L_addr_key,
                                                     L_addr_type,
                                                     L_primary_addr_ind) then
            RAISE program_error;
         end if;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_ADDR_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
