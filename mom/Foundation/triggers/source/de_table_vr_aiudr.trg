--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.0 $
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- TRIGGER ADDED:          DE_TABLE_VR_AIUDR
----------------------------------------------------------------------------

whenever SQLERROR exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'DE_TABLE_VR_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_VR_AIUDR
   AFTER INSERT OR UPDATE OR DELETE ON VAT_REGION
   FOR EACH ROW
DECLARE
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_vat_region      VAT_REGION.VAT_REGION%TYPE;
   L_action_type     VAT_EXPORT_STG.ACTION_TYPE%TYPE;
   L_vat_calc_type   VAT_REGION.VAT_CALC_TYPE%TYPE;
   program_error     EXCEPTION;
   L_status          VARCHAR2(1);

BEGIN
   L_vat_calc_type := nvl(:new.vat_calc_type,:old.vat_calc_type);
   if L_vat_calc_type <> 'E' then
      if inserting then
         L_vat_region := :new.vat_region;
         L_action_type := DATA_EXPORT_SQL.VAT_CRE;
      elsif updating then
         if :old.vat_region_name <> :new.vat_region_name then
            L_vat_region := :old.vat_region;
            L_action_type := DATA_EXPORT_SQL.VAT_UPD;
         end if;
      else -- Deleting
         L_vat_region := :old.vat_region;
         L_action_type := DATA_EXPORT_SQL.VAT_DEL; 
      end if;
   end if;

   if L_action_type is NOT NULL then
      if DATA_EXPORT_SQL.INS_VAT_EXPORT_STG(L_error_message,
                                            L_action_type,
                                            L_vat_region,
                                            NULL,
                                            NULL) = FALSE then
         RAISE program_error;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_VR_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/