PROMPT Creating TRIGGER 'EC_TABLE_STRADD_BIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_STRADD_BIUDR BEFORE
  DELETE OR
  INSERT OR
  UPDATE ON STORE_ADD FOR EACH ROW DECLARE PROGRAM_ERROR EXCEPTION;
  CURSOR C_get_mode
  IS
    SELECT process_mode FROM process_config WHERE process_name = 'LIKE_STORE';
  L_process_mode process_config.process_mode%type:='ASYNC';
  L_error_message VARCHAR2(1000)                 := NULL;
BEGIN
  IF INSERTING THEN
    OPEN c_get_mode;
    FETCH c_get_mode INTO L_process_mode;
    CLOSE c_get_mode;
    :new.process_mode := L_process_mode;
  END IF;
EXCEPTION
WHEN OTHERS THEN
  IF L_error_message IS NULL THEN
    L_error_message  := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, 'EC_TABLE_STRADD_AIUDR', TO_CHAR(SQLCODE));
  END IF;
  raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/