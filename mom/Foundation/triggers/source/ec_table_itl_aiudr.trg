PROMPT Creating Trigger 'EC_TABLE_ITL_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_ITL_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF PRIMARY_SUPP
, SOURCE_METHOD
, STORE_PRICE_IND
, LOCAL_ITEM_DESC
, PRIMARY_CNTRY
, LOCAL_SHORT_DESC
, TAXABLE_IND
, STATUS
, SOURCE_WH
, RECEIVE_AS_TYPE
, UIN_TYPE
, UIN_LABEL
, CAPTURE_TIME
, EXT_UIN_IND
, RANGED_IND
 ON ITEM_LOC
 FOR EACH ROW
DECLARE

   L_record          ITEM_LOC%ROWTYPE                  := NULL;
   L_status          VARCHAR2(1)                       := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE          := NULL;
   L_message_type    ITEMLOC_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;

   L_item_level      ITEM_MASTER.ITEM_LEVEL%TYPE := NULL;
   L_tran_level      ITEM_MASTER.TRAN_LEVEL%TYPE := NULL;
   L_stkhldg_ind     STORE.STOCKHOLDING_IND%TYPE := NULL;
   
   L_store_record    STORE%ROWTYPE               := NULL;
   L_exists          BOOLEAN                     := FALSE;

   PROGRAM_ERROR   EXCEPTION;
BEGIN

   if DELETING then
      L_record.item     := :old.item;
      L_record.loc      := :old.loc;
      L_record.loc_type := :old.loc_type;
      ---
      L_message_type    := RMSMFM_ITEMLOC.ITEMLOC_DEL;
   else
      if INSERTING then
         L_message_type := RMSMFM_ITEMLOC.ITEMLOC_ADD;

      elsif (:old.local_item_desc                 != :new.local_item_desc)
            or (:old.local_short_desc             != :new.local_short_desc)
            or (:old.status                       != :new.status)
            or (:old.primary_supp                 != :new.primary_supp)
            or (:old.primary_cntry                != :new.primary_cntry)
            or (NVL(:old.receive_as_type, '-999') != :new.receive_as_type)
            or (:old.taxable_ind                  != :new.taxable_ind)
            or (:old.source_method                != :new.source_method)
            or (NVL(:old.source_wh, -999)         != :new.source_wh)
            or (:old.store_price_ind              != :new.store_price_ind)
            or (NVL(:old.uin_type, '-999')        != NVL(:new.uin_type, '-999'))
            or (NVL(:old.uin_label, '-999')       != NVL(:new.uin_label, '-999'))
            or (NVL(:old.capture_time, '-999')    != NVL(:new.capture_time, '-999'))
            or (NVL(:old.ext_uin_ind, 'N')        != NVL(:new.ext_uin_ind, 'N'))
            or (NVL(:old.ranged_ind, 'Y')         != NVL(:new.ranged_ind, 'Y'))then

         L_message_type := RMSMFM_ITEMLOC.ITEMLOC_UPD;
      else
         return;
      end if;

      L_record.item                := :new.item;
      L_record.loc                 := :new.loc;
      L_record.loc_type            := :new.loc_type;
      L_record.local_item_desc     := :new.local_item_desc;
      L_record.local_short_desc    := :new.local_short_desc;
      L_record.status              := :new.status;
      L_record.primary_supp        := :new.primary_supp;
      L_record.primary_cntry       := :new.primary_cntry;
      L_record.receive_as_type     := :new.receive_as_type;
      L_record.taxable_ind         := :new.taxable_ind;
      L_record.source_method       := :new.source_method;
      L_record.source_wh           := :new.source_wh;
      L_record.store_price_ind     := :new.store_price_ind;
      L_record.unit_retail         := :new.unit_retail;
      L_record.selling_unit_retail := :new.selling_unit_retail;
      L_record.selling_uom         := :new.selling_uom;
      L_record.uin_type            := :new.uin_type;
      L_record.uin_label           := :new.uin_label;
      L_record.capture_time        := :new.capture_time;
      L_record.ext_uin_ind         := :new.ext_uin_ind;
      L_record.ranged_ind          := :new.ranged_ind;

   end if;

   -- Check for non stock holding store and do not send message 
   -- to SIM for non stockholding stores.
   if L_record.loc_type = 'S' then
      if STORE_ATTRIB_SQL.GET_ROW(L_error_message,
                                  L_exists,
                                  L_store_record,
                                  L_record.loc) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_exists then
         if L_store_record.stockholding_ind = 'N' and L_store_record.store_type in ('C','F') then
            return;
         end if;
      end if;

   end if;


   if ITEM_ATTRIB_SQL.GET_LEVELS(L_error_message,
                                 L_item_level,
                                 L_tran_level,
                                 L_record.item) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_item_level != L_tran_level then
      return;
   end if;

   if RMSMFM_ITEMLOC.ADDTOQ(L_error_message,
                            L_message_type,
                            L_record,
                            NULL,                --returnable_ind
                            NULL,                --prim_repl_supplier,
                            NULL,                --repl_method,
                            NULL,                --reject_store_ord_ind,
                            NULL,                --next_delivery_date,
                            'N') = FALSE then    --mult_run_per_day_ind
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      if L_error_message is NULL then
         API_LIBRARY.HANDLE_ERRORS(L_status,
                                   L_error_message,
                                   API_LIBRARY.FATAL_ERROR,
                                   'EC_TABLE_ITL_AIUDR');
      end if;

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/