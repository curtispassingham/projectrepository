--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.0 $
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- TRIGGER ADDED:          DE_TABLE_DIFF_AIUDR
----------------------------------------------------------------------------

whenever SQLERROR exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'DE_TABLE_DIFF_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_DIFF_AIUDR
   AFTER INSERT OR UPDATE OR DELETE ON DIFF_IDS
   FOR EACH ROW
DECLARE
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_diff_id         DIFF_IDS.DIFF_ID%TYPE;
   L_action_type     DIFFS_EXPORT_STG.ACTION_TYPE%TYPE;
   program_error     EXCEPTION;
   L_status          VARCHAR2(1);

BEGIN
   if inserting then
      L_diff_id := :new.diff_id;
      L_action_type := DATA_EXPORT_SQL.DIFF_CRE;
   elsif updating then
      L_diff_id := :old.diff_id;
      L_action_type := DATA_EXPORT_SQL.DIFF_UPD; 
   else -- Deleting
      L_diff_id := :old.diff_id;
      L_action_type := DATA_EXPORT_SQL.DIFF_DEL; 
   end if;

   if DATA_EXPORT_SQL.INS_DIFFS_EXPORT_STG(L_error_message,
                                           L_action_type,
                                           L_diff_id) = FALSE then
      RAISE program_error;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_DIFF_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/