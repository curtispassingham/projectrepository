PROMPT Creating Trigger 'DE_TABLE_REG_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_REG_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON REGION
 FOR EACH ROW
DECLARE

   L_action_type   ORGHIER_EXPORT_STG.ACTION_TYPE%TYPE;
   L_region        REGION.REGION%TYPE;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_status        VARCHAR2(1);

   program_error EXCEPTION;

BEGIN

   if inserting then
      L_action_type := DATA_EXPORT_SQL.REG_ADD;
      L_region        := :new.region;
   elsif updating then
      L_action_type := DATA_EXPORT_SQL.REG_UPD;
      L_region := :old.region;
   else -- Deleting
      L_action_type := DATA_EXPORT_SQL.REG_DEL;
      L_region := :old.region;
   end if;

   if NOT DATA_EXPORT_SQL.INS_ORGHIER_EXPORT_STG(L_error_msg,
                                                 L_action_type,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 L_region,
                                                 NULL,
                                                 NULL,
                                                 NULL) then
      RAISE program_error;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_REG_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
