PROMPT Creating Trigger 'EC_TABLE_WHA_AIUR'
CREATE OR REPLACE TRIGGER EC_TABLE_WHA_AIUR
 AFTER INSERT OR UPDATE
 ON WH_ADD
 FOR EACH ROW


DECLARE
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   --
   L_message_type       VARCHAR2(15) := null;
   L_wh_key_rec         RMSMFM_WH.WH_KEY_REC;
   L_addr_publish_ind   ADDR.PUBLISH_IND%TYPE := 'N';
   L_org_entity_type    WH.ORG_ENTITY_TYPE%TYPE;

   cursor C_WH is
      select decode(wh, physical_wh, 'P', 'V'),
             org_entity_type
        from wh
       where wh = :new.wh;

BEGIN

   if INSERTING then
      L_message_type := RMSMFM_WH.WHA_ADD;

      L_wh_key_rec.wh := :new.wh;
      L_wh_key_rec.addr_key := null;
      --
      open C_WH;
      fetch C_WH into L_wh_key_rec.wh_type,
                      L_org_entity_type;
      close C_WH;
      --
      L_wh_key_rec.pricing_loc := :new.pricing_location;
      L_wh_key_rec.pricing_loc_curr := :new.pricing_loc_curr;
   elsif UPDATING then
      if :old.wh_currency != :new.wh_currency or
         :old.pricing_location != :new.pricing_location or
         :old.pricing_loc_curr != :new.pricing_loc_curr then
         --
         L_message_type := RMSMFM_WH.WHA_UPD;

         L_wh_key_rec.wh := :new.wh;
         L_wh_key_rec.addr_key := null;
         --
         open C_WH;
         fetch C_WH into L_wh_key_rec.wh_type,
                         L_org_entity_type;
         close C_WH;
         --
         L_wh_key_rec.pricing_loc := :new.pricing_location;
         L_wh_key_rec.pricing_loc_curr := :new.pricing_loc_curr;
      else
         return;
      end if;
   end if;

   if L_org_entity_type = 'R' then
      if RMSMFM_WH.ADDTOQ(L_error_message,
                          L_message_type,
                          L_wh_key_rec,
                          L_addr_publish_ind) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION

   when OTHERS then
      if L_error_message is null then
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'EC_TABLE_WH_AIUDR',
                                               to_char(SQLCODE));
      end if;

      RAISE_APPLICATION_ERROR(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

END;
/
