PROMPT Creating Trigger 'RMS_TABLE_RPM_DEP_AIR'
CREATE OR REPLACE TRIGGER RMS_TABLE_RPM_DEP_AIR
 AFTER INSERT
 ON DEPS
 FOR EACH ROW
DECLARE

   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE := NULL;
   O_error_message        RTK_ERRORS.RTK_TEXT%TYPE :=NULL;
   L_dept                 deps.dept%TYPE :=NULL;
BEGIN

   IF SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE THEN
      RAISE_APPLICATION_ERROR(-20000,'INTERNAL ERROR: TRG_DEPS - SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS failed.');
   END IF;
   
   IF L_system_options_rec.rpm_ind = 'Y' THEN
   
      L_dept          := :NEW.dept;

      IF PM_NOTIFY_API_SQL.NEW_DEPARTMENT( O_error_message,
                                           L_dept) = FALSE THEN
         RAISE_APPLICATION_ERROR(-20000,'INTERNAL ERROR: TRG_DEPS - PM_NOTIFY_API_SQL.NEW_DEPARTMENT failed.');
      END IF;
   
   END IF;

END;
/