PROMPT Creating Trigger 'RMS_COL_GRO_DV_AUR'
CREATE OR REPLACE TRIGGER RMS_COL_GRO_DV_AUR
 AFTER UPDATE OF DIVISION
 ON GROUPS
 FOR EACH ROW
DECLARE
   L_new_division  GROUPS.DIVISION%TYPE;
   L_old_division  GROUPS.DIVISION%TYPE;
   L_new_group_no  GROUPS.GROUP_NO%TYPE;
   L_error_message RTK_ERRORS.RTK_TEXT%TYPE := NULL;
BEGIN
    L_new_division := :new.division;
    L_old_division := :old.division;
    L_new_group_no := :new.group_no;
    if (L_new_division IS NOT NULL) and (L_new_division != L_old_division) and (L_new_group_no IS NOT NULL) then
      INSERT INTO RDW_RECLASS(IDNT,TABLE_NAME) VALUES(L_new_group_no, 'GROUPS');
    end if;
EXCEPTION
   when OTHERS then
      if SQLCODE = -00001 then  
         -- unique key violation; row already exists. Do nothing
         null;
      else
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'RMS_COL_GRO_DV_AUR',
                                               to_char(SQLCODE));
         raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
      end if;
END RMS_COL_GRO_DV_AUR;
/

