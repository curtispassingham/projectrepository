--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.1 $
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TRIGGER CREATED:			RMS_TABLE_RPM_IEM_ADR	
----------------------------------------------------------------------------


--------------------------------------
--      CREATING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'RMS_TABLE_RPM_IEM_ADR'
CREATE OR REPLACE TRIGGER RMS_TABLE_RPM_IEM_ADR
 AFTER  DELETE 
 ON ITEM_MASTER
 FOR EACH ROW
DECLARE
   O_text                 VARCHAR2(255);
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE := NULL;
   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_dept                 ITEM_MASTER.DEPT%TYPE;
BEGIN
   IF SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_text,
                                            L_system_options_rec) = FALSE THEN
      RAISE_APPLICATION_ERROR(-20000,'INTERNAL ERROR: TRG_ITEM_MASTER - SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS failed.');
   END IF;
   
   IF L_system_options_rec.rpm_ind = 'Y' THEN
      L_item   := :OLD.item;
      L_dept := :OLD.dept;
      INSERT INTO RPM_STAGE_DELETED_ITEM_MASTER(item,
                                                dept,
                                                last_modified_date )
                                         VALUES(L_item,
                                                L_dept,
                                                SYSDATE);
   END IF;             
                          
END RMS_TABLE_RPM_IEM_ADR;
/
