PROMPT Creating Trigger 'EC_TABLE_PRT_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_PRT_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF LINE_OF_CREDIT
, CURRENCY_CODE
, CONTACT_PHONE
, CONTACT_FAX
, TAX_ID
, TERMS
, OPEN_CREDIT
, PARTNER_ID
, PRINCIPLE_COUNTRY_ID
, YTD_CREDIT
, SERVICE_PERF_REQ_IND
, PARTNER_TYPE
, OUTSTAND_CREDIT
, MFG_ID
, CONTACT_EMAIL
, INVC_PAY_LOC
, INVC_RECEIVE_LOC
, LANG
, IMPORT_COUNTRY_ID
, PRIMARY_IA_IND
, CONTACT_NAME
, CONTACT_TELEX
, PARTNER_DESC
, STATUS
, YTD_DRAWDOWNS
 ON PARTNER
 FOR EACH ROW
DECLARE


L_error_message     RTK_ERRORS.RTK_TEXT%TYPE := null;
L_message_type      VARCHAR2(15) := null;
L_partner_key_rec   RMSMFM_PARTNER.PARTNER_KEY_REC;
BEGIN

      if DELETING then
         L_partner_key_rec.partner_type := :old.partner_type;
         L_partner_key_rec.partner_id   := :old.partner_id;
         L_message_type := RMSMFM_PARTNER.HDR_DEL;
      elsif UPDATING then
         if (:old.partner_type != :new.partner_type or
         :old.partner_id != :new.partner_id or
         :old.partner_desc != :new.partner_desc or
         :old.currency_code != :new.currency_code or
         :old.lang != :new.lang or
         :old.status != :new.status or
         :old.contact_name != :new.contact_name or
         :old.contact_phone != :new.contact_phone or
         :old.contact_fax != :new.contact_fax or
         :old.contact_telex != :new.contact_telex or
         :old.contact_email != :new.contact_email or
         :old.mfg_id != :new.mfg_id or
         :old.principle_country_id != :new.principle_country_id or
         :old.line_of_credit != :new.line_of_credit or
         :old.outstand_credit != :new.outstand_credit or
         :old.open_credit != :new.open_credit or
         :old.ytd_credit != :new.ytd_credit or
         :old.ytd_drawdowns != :new.ytd_drawdowns or
         :old.tax_id != :new.tax_id or
         :old.terms != :new.terms or
         :old.service_perf_req_ind != :new.service_perf_req_ind or
         :old.invc_pay_loc != :new.invc_pay_loc or
         :old.invc_receive_loc != :new.invc_receive_loc or
         :old.import_country_id != :new.import_country_id or
         :old.primary_ia_ind != :new.primary_ia_ind) then
            L_partner_key_rec.partner_type := :new.partner_type;
            L_partner_key_rec.partner_id   := :new.partner_id;
            L_message_type := RMSMFM_PARTNER.HDR_UPD;
         else
            return;
         end if;
      elsif INSERTING then
         L_partner_key_rec.partner_type := :new.partner_type;
         L_partner_key_rec.partner_id   := :new.partner_id;
         L_message_type := RMSMFM_PARTNER.HDR_ADD;
      end if;

      if RMSMFM_PARTNER.ADDTOQ(L_error_message,
                               L_message_type,
                               L_partner_key_rec,
                               'N') = FALSE then
         raise PROGRAM_ERROR;
      end if;

EXCEPTION

   when OTHERS then
      if L_error_message is null then
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'EC_TABLE_PRT_AIUDR',
                                               to_char(SQLCODE));
      end if;
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

END;
/
