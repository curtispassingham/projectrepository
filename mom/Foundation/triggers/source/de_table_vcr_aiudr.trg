--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.0 $
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- TRIGGER ADDED:          DE_TABLE_VCR_AIUDR
----------------------------------------------------------------------------

whenever SQLERROR exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'DE_TABLE_VCR_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_VCR_AIUDR
   AFTER INSERT OR UPDATE OR DELETE ON VAT_CODE_RATES
   FOR EACH ROW
DECLARE
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_vat_code        VAT_CODE_RATES.VAT_CODE%TYPE;
   L_vat_rate        VAT_CODE_RATES.VAT_RATE%TYPE;
   L_active_date     VAT_CODE_RATES.ACTIVE_DATE%TYPE;
   L_action_type     VAT_EXPORT_STG.ACTION_TYPE%TYPE;
   program_error     EXCEPTION;
   L_status          VARCHAR2(1);

BEGIN
   if inserting then
      L_vat_code     := :new.vat_code;
      L_vat_rate     := :new.vat_rate;
      L_active_date  := :new.active_date;
      L_action_type  := DATA_EXPORT_SQL.VAT_CRE;
   elsif updating then
      if :old.vat_rate <> :new.vat_rate or
         :old.active_date <> :new.active_date then
         L_vat_code := :old.vat_code;

         if :old.active_date <> :new.active_date then
            L_active_date := :new.active_date;
            if :old.vat_rate <> :new.vat_rate then
               L_vat_rate     := :new.vat_rate;
            else
               L_vat_rate     := :old.vat_rate;
            end if;

            if DATA_EXPORT_SQL.INS_VAT_EXPORT_STG(L_error_message,
                                                  DATA_EXPORT_SQL.VAT_DEL,
                                                  NULL,
                                                  L_vat_code,
                                                  :old.active_date) = FALSE then
               RAISE program_error;
            end if;

            L_action_type  := DATA_EXPORT_SQL.VAT_CRE;
         else
            L_active_date := :old.active_date;
            if :old.vat_rate <> :new.vat_rate then
               L_vat_rate     := :new.vat_rate;
               L_action_type  := DATA_EXPORT_SQL.VAT_UPD;
            end if;
         end if;
      end if;
   else -- Deleting
      L_vat_code     := :old.vat_code;
      L_vat_rate     := :old.vat_rate;
      L_active_date  := :old.active_date;
      L_action_type  := DATA_EXPORT_SQL.VAT_DEL; 
   end if;

   if L_action_type is NOT NULL then
      if DATA_EXPORT_SQL.INS_VAT_EXPORT_STG(L_error_message,
                                            L_action_type,
                                            NULL,
                                            L_vat_code,
                                            L_active_date) = FALSE then
         RAISE program_error;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_VCR_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/