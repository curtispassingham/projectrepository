PROMPT Creating Trigger 'RMS_TABLE_DPE_ADR'
CREATE OR REPLACE TRIGGER RMS_TABLE_DPE_ADR
 AFTER DELETE
 ON DAILY_PURGE
 FOR EACH ROW
BEGIN
   if (:old.table_name = 'ITEM_MASTER') then
      insert into RDW_DELETE_ITEM(ITEM)
         select item from item_master im
            where (im.item = :old.key_value or
            im.item_parent = :old.key_value or
            im.item_grandparent = :old.key_value);
   end if;
END;
/
