--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.0 $
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- TRIGGER ADDED:          RMS_TABLE_SCL_BIR
----------------------------------------------------------------------------

whenever SQLERROR exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT CREATING TRIGGER 'RMS_TABLE_SCL_BIR'
CREATE OR REPLACE TRIGGER RMS_TABLE_SCL_BIR
   BEFORE INSERT ON SUBCLASS
   FOR EACH ROW

DECLARE
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   L_status             VARCHAR2(1);
BEGIN

   if inserting then
      :new.subclass_id := SUBCLASS_ID_SEQUENCE.nextval;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'RMS_TABLE_SCL_BIR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/  