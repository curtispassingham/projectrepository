--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.0 $
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- TRIGGER ADDED:          DE_TABLE_GRP_AIUDR
----------------------------------------------------------------------------

whenever SQLERROR exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'DE_TABLE_GRP_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_GRP_AIUDR 
   AFTER INSERT OR UPDATE OR DELETE ON GROUPS
   FOR EACH ROW

DECLARE
   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   L_group_no            GROUPS.GROUP_NO%TYPE;
   L_action_type         MERCHHIER_EXPORT_STG.ACTION_TYPE%TYPE;
   L_status              VARCHAR2(1);
   program_error         EXCEPTION;

BEGIN

   if inserting then
      L_group_no    := :new.group_no;
      L_action_type := DATA_EXPORT_SQL.GRP_CRE;
   elsif updating then
      if :old.division <> :new.division or 
         :old.group_name <> :new.group_name then
         L_group_no    := :old.group_no;
         L_action_type := DATA_EXPORT_SQL.GRP_UPD;
      end if;
   else -- Deleting
      L_group_no    := :old.group_no;
      L_action_type := DATA_EXPORT_SQL.GRP_DEL;
   end if;
   if L_group_no is NOT NULL then
      if DATA_EXPORT_SQL.INS_MERCHHIER_EXPORT_STG(L_error_message,
                                                  L_action_type,
                                                  NULL,
                                                  L_group_no,
                                                  NULL,
                                                  NULL,
                                                  NULL) = FALSE then
         RAISE program_error;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_GRP_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/