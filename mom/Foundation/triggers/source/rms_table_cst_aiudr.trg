--------------------------------------------------------
-- Copyright (c) 2008, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.7 $
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TRIGGER ADDED:				RMS_TABLE_CST_AIUDR
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'RMS_TABLE_CST_AIUDR'
CREATE OR REPLACE TRIGGER RMS_TABLE_CST_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON CUSTOMER_SEGMENTS
 FOR EACH ROW
DECLARE


L_status         VARCHAR2(1)   := NULL;
L_text           VARCHAR2(255) := NULL;
BEGIN

   if UPDATING then
      insert into customer_segment_pos_stg 
                  (customer_segment_id,
                   change_type)
           values (:new.customer_segment_id,
                  'UPD');
   elsif INSERTING then
      insert into customer_segment_pos_stg 
                  (customer_segment_id,
                   change_type)
           values (:new.customer_segment_id,
                  'ADD');
   elsif DELETING then
      insert into customer_segment_pos_stg 
                  (customer_segment_id,
                   change_type)
           values (:old.customer_segment_id,
                  'DEL');
   end if;
  
EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMS_TABLE_CST_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);
END;
/








