PROMPT Creating Trigger 'EC_TABLE_PKS_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_PKS_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON PACKITEM_BREAKOUT
 FOR EACH ROW
DECLARE

   L_row_number        NUMBER := 0;
   L_status            VARCHAR2(1) := NULL;
   L_text              VARCHAR2(255) := NULL;
   L_already_exists    BOOLEAN := FALSE;
   L_pack_no           ITEM_MASTER.ITEM%TYPE;
   L_item              ITEM_MASTER.ITEM%TYPE;
   L_pack_item_qty     PACKITEM_BREAKOUT.PACK_ITEM_QTY%TYPE;

   PROGRAM_ERROR   EXCEPTION;

--- This function checks if the current item, pack_no combination 
--- already exists in the BOM table
--- return TRUE if it exists and FALSE if it doesn't.
FUNCTION ALREADY_EXISTS(I_pack_no  IN  PACKITEM.PACK_NO%TYPE,
                        I_item     IN  PACKITEM.ITEM%TYPE,
                        I_rowcount IN  NUMBER)
RETURN BOOLEAN IS
   L_record_exists    BOOLEAN;
BEGIN
   L_record_exists  := FALSE;
   FOR j IN 1..I_rowcount LOOP
      if RMSMFM_ITEMS.BOM_TABLE(j).PACK_NO    = I_pack_no
       and RMSMFM_ITEMS.BOM_TABLE(j).ITEM     = I_item then
         L_record_exists := TRUE;
         exit;
      end if;
   END LOOP;
   return L_record_exists;
END ALREADY_EXISTS;
--------------------------------------------------------------------------------
BEGIN
   if DELETING then
      L_pack_no        := :old.pack_no;
      L_item           := NVL(:old.comp_pack_no, :old.item);
      L_pack_item_qty  := NVL(:old.comp_pack_qty, :old.pack_item_qty);
   else
      L_pack_no        := :new.pack_no;
      L_item           := NVL(:new.comp_pack_no, :new.item);
      L_pack_item_qty  := NVL(:new.comp_pack_qty, :new.pack_item_qty);
   end if;
   
   if ALREADY_EXISTS(L_pack_no,
                     L_item,
                     L_row_number - 1) = FALSE then
                     
      L_row_number := NVL(RMSMFM_ITEMS.BOM_TABLE.LAST,0) + 1;
      
      RMSMFM_ITEMS.BOM_TABLE(L_row_number).PACK_NO          := L_pack_no;
      RMSMFM_ITEMS.BOM_TABLE(L_row_number).ITEM             := L_item;
      RMSMFM_ITEMS.BOM_TABLE(L_row_number).PACK_ITEM_QTY    := L_pack_item_qty;
   end if;
EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_PKS_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);
END;
/
