--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.1 $
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TRIGGER ADDED:				RMS_TABLE_RPM_SKULSTDTL_AIUR
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'RMS_TABLE_RPM_SKULSTDTL_AIUR'
CREATE OR REPLACE TRIGGER RMS_TABLE_RPM_SKULSTDTL_AIUR
 AFTER INSERT OR UPDATE OF PACK_IND
, LAST_UPDATE_ID
, TRAN_LEVEL
, SKULIST
, ITEM
, LAST_UPDATE_DATETIME
, CREATE_DATETIME
, ITEM_LEVEL
, INSERT_ID
, INSERT_DATE
 ON SKULIST_DETAIL
 FOR EACH ROW
declare
   O_text                 VARCHAR2(255); 
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE := NULL;
begin
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_text,
                                            L_system_options_rec) = FALSE then
      RAISE_APPLICATION_ERROR(-20000,'INTERNAL ERROR: TRG_SKULIST_DETAIL - SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS failed.');
   end if;
   
   if L_system_options_rec.rpm_ind = 'Y' then
      if UPDATING OR INSERTING then
         merge into SKULIST_DEPT_CLASS_SUBCLASS skdp
         using (select DEPT, CLASS, SUBCLASS
                  from ITEM_MASTER  WHERE  ITEM =:NEW.ITEM ) dsku
         on ( :NEW.SKULIST = skdp.SKULIST and
              dsku.DEPT    = skdp.DEPT    and
              dsku.CLASS   = skdp.CLASS   and
              dsku.SUBCLASS= skdp.SUBCLASS   )
         when not matched then
             insert (SKULIST,      DEPT,      CLASS,      SUBCLASS  )
             values (:NEW.SKULIST, dsku.DEPT, dsku.CLASS, dsku.SUBCLASS);
      end if;
   end if;   

end;
/






