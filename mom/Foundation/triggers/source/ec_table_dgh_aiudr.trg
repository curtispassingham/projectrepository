PROMPT Creating Trigger 'EC_TABLE_DGH_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_DGH_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON DIFF_GROUP_HEAD
 FOR EACH ROW
DECLARE

   L_record        DIFF_GROUP_HEAD%ROWTYPE := NULL;
   L_action_type   VARCHAR2(1) := NULL;
   L_message       CLOB;
   L_status        VARCHAR2(1) := NULL;
   L_text          VARCHAR2(255) := NULL;
   L_message_type  DIFFGRP_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;
   L_dummy_diff_id DIFFGRP_MFQUEUE.DIFF_ID%TYPE := NULL;

   PROGRAM_ERROR   EXCEPTION;
BEGIN
   if DELETING then
      /* For deletes, the row identifier will be published. */

      L_action_type := 'D';
      L_message_type := 'DiffGrpDel';
      L_record.diff_group_id := :old.diff_group_id;
   else
      if INSERTING then
         L_action_type := 'A';
         L_message_type := 'DiffGrpHdrCre';
      else
         L_action_type := 'M';
         L_message_type := 'DiffGrpHdrMod';
      
end if;

      /* For inserts and updates, more of the values from the table
         will be published.
      */

      L_record.diff_group_id   := :new.diff_group_id;
      L_record.diff_type       := :new.diff_type;
      L_record.diff_group_desc := :new.diff_group_desc;

   end if;

   /* Creates an XML message, puts it in CLOB form, returns the CLOB.
      L_record should be populated with all of the necessary values
      from the table before calling BUILD_MESSAGE.
      L_action_type should specify the type of event ('A'dd, 'M'odify, or 'D'elete)
   */
   if not DIFFGRPHDR_XML.BUILD_MESSAGE(L_status,
                                       L_text,
                                       L_message,
                                       L_record,
                                       L_action_type) then
     raise PROGRAM_ERROR;
   end if;

   /* Takes the CLOB returned from BUILD_MESSAGE and puts it in the
      message queue table.
   */
   RMSMFM_DIFFGRP.ADDTOQ(L_status,
                         L_text,
                         L_message_type,
                         L_record.diff_group_id,
                         L_dummy_diff_id,
                         L_message);

   if L_status = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_DGH_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END;
/
