--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Trigger Added:  DE_TABLE_DGDTL_AIUDR
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Trigger               
--------------------------------------
PROMPT Creating Trigger 'DE_TABLE_DGDTL_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_DGDTL_AIUDR
   AFTER INSERT OR UPDATE OR DELETE ON DIFF_GROUP_DETAIL
   FOR EACH ROW

DECLARE

   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_status          VARCHAR2(1);
   L_diff_group_id   DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE;
   L_diff_id         DIFF_IDS.DIFF_ID%TYPE;
   L_action_type     DIFFGRP_EXPORT_STG.ACTION_TYPE%TYPE;
   PROGRAM_ERROR     EXCEPTION;

BEGIN
   if inserting then
      L_diff_group_id := :new.diff_group_id;
      L_diff_id       := :new.diff_id;
      L_action_type   := DATA_EXPORT_SQL.DIFFGRPDTL_CRE;
   elsif updating then
      L_diff_group_id := :old.diff_group_id;
      L_diff_id       := :old.diff_id;
      L_action_type   := DATA_EXPORT_SQL.DIFFGRPDTL_UPD;
   else
      L_diff_group_id := :old.diff_group_id;
      L_diff_id       := :old.diff_id;
      L_action_type   := DATA_EXPORT_SQL.DIFFGRPDTL_DEL;
   end if;

   if DATA_EXPORT_SQL.INS_DIFFGRP_EXPORT_STG(L_error_message,
                                             L_action_type,
                                             L_diff_group_id,
                                             L_diff_id) = FALSE then
      RAISE PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_DGDTL_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/