--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.0 $
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- TRIGGER ADDED:          DE_TABLE_DIVI_AIUDR
----------------------------------------------------------------------------

whenever SQLERROR exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'DE_TABLE_DIVI_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_DIVI_AIUDR
   AFTER INSERT OR UPDATE OR DELETE ON DIVISION 
   FOR EACH ROW

DECLARE
   L_action_type        MERCHHIER_EXPORT_STG.ACTION_TYPE%TYPE;
   L_division           DIVISION.DIVISION%TYPE;
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   L_status             VARCHAR2(1);
   program_error        EXCEPTION;

BEGIN

   if inserting then
      L_action_type := DATA_EXPORT_SQL.DIV_CRE;
      L_division    := :new.division;
   elsif updating then
      if :old.div_name <> :new.div_name then
         L_action_type := DATA_EXPORT_SQL.DIV_UPD;
         L_division    := :old.division;
      end if;
   else -- Deleting
      L_action_type := DATA_EXPORT_SQL.DIV_DEL;
      L_division    := :old.division;
   end if;
   if L_division is NOT NULL then
      if DATA_EXPORT_SQL.INS_MERCHHIER_EXPORT_STG(L_error_message,
                                                  L_action_type,
                                                  L_division,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL) = FALSE then
         RAISE program_error;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_DIVI_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/