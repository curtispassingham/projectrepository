--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.0 $
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- TRIGGER ADDED:          DE_TABLE_VR_AIUDR
----------------------------------------------------------------------------

whenever SQLERROR exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'DE_TABLE_VC_AUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_VC_AUDR
   AFTER UPDATE OR DELETE ON VAT_CODES
   FOR EACH ROW
DECLARE
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_vat_code        VAT_CODES.VAT_CODE%TYPE;
   L_action_type     VAT_EXPORT_STG.ACTION_TYPE%TYPE;
   program_error     EXCEPTION;
   L_status          VARCHAR2(1);

BEGIN
   if updating then
      if :old.vat_code_desc <> :new.vat_code_desc then
         L_vat_code := :old.vat_code;
         L_action_type := DATA_EXPORT_SQL.VAT_UPD;
      end if;
   else -- Deleting
      L_vat_code := :old.vat_code;
      L_action_type := DATA_EXPORT_SQL.VAT_DEL; 
   end if;

   if L_action_type is NOT NULL then
      if DATA_EXPORT_SQL.INS_VAT_EXPORT_STG(L_error_message,
                                            L_action_type,
                                            NULL,
                                            L_vat_code,
                                            NULL) = FALSE then
         RAISE program_error;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_VC_AUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/