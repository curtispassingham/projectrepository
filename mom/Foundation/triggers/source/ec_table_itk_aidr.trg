PROMPT Creating Trigger 'EC_TABLE_ITK_AIDR'
CREATE OR REPLACE TRIGGER EC_TABLE_ITK_AIDR
 AFTER DELETE OR INSERT
 ON ITEM_TICKET
 FOR EACH ROW
DECLARE

   L_queue_rec     ITEM_MFQUEUE%ROWTYPE := NULL;
   L_status        VARCHAR2(1) := NULL;
   L_text          VARCHAR2(255) := NULL;
   L_item_row      ITEM_MASTER%ROWTYPE := NULL;
   
   PROGRAM_ERROR   EXCEPTION;
BEGIN
   if DELETING then
      L_queue_rec.message_type := RMSMFM_ITEMS.TCKT_DEL;
      L_queue_rec.item := :old.item;
      L_queue_rec.ticket_type_id := :old.ticket_type_id;
   else
      L_queue_rec.message_type := RMSMFM_ITEMS.TCKT_ADD;
      L_queue_rec.item := :new.item;
      L_queue_rec.ticket_type_id := :new.ticket_type_id;
   end if;

   if not ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                          L_item_row,
                                          L_queue_rec.item) then
      raise PROGRAM_ERROR;
   end if;

   if L_item_row.item_level = L_item_row.tran_level then
      if RMSMFM_ITEMS.ADDTOQ(L_text,
                             L_queue_rec,
                             NULL,               ---I_sellable_ind
                             NULL) = FALSE then  ---I_tran_level_ind
         raise PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_ITK_AIUR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END;
/
