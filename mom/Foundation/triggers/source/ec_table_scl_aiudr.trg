PROMPT Creating Trigger 'EC_TABLE_SCL_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_SCL_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON SUBCLASS
 FOR EACH ROW
DECLARE


   L_message_type       MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
   L_dept               SUBCLASS.DEPT%TYPE;
   L_class              SUBCLASS.CLASS%TYPE;
   L_subclass           SUBCLASS.SUBCLASS%TYPE;
   L_subclass_rec       SUBCLASS%ROWTYPE;
   L_error_msg          VARCHAR2(255) := NULL;
   L_status             VARCHAR2(1);
   program_error EXCEPTION;
BEGIN

  if inserting then
    L_message_type := RMSMFM_MERCHHIER.SUB_ADD;
    L_dept := :new.dept;
    L_class := :new.class;
    L_subclass := :new.subclass;
    L_subclass_rec.dept := :new.dept;
    L_subclass_rec.class := :new.class;
    L_subclass_rec.subclass := :new.subclass;
    L_subclass_rec.sub_name := :new.sub_name;
  elsif updating then
    L_message_type := RMSMFM_MERCHHIER.SUB_UPD;
    L_dept := :old.dept;
    L_class := :old.class;
    L_subclass := :old.subclass;
    L_subclass_rec.dept := :old.dept;
    L_subclass_rec.class := :old.class;
    L_subclass_rec.subclass := :old.subclass;
    L_subclass_rec.sub_name := :new.sub_name;
  else -- Deleting
    L_message_type := RMSMFM_MERCHHIER.SUB_DEL;
    L_dept := :old.dept;
    L_class := :old.class;
    L_subclass := :old.subclass;
  end if;

  if NOT RMSMFM_MERCHHIER.ADDTOQ(L_error_msg, 
                                 L_message_type, 
                                 null, 
                                 null, 
                                 null, 
                                 null, 
                                 L_dept, 
                                 null, 
                                 L_class, 
                                 null, 
                                 L_subclass, 
                                 L_subclass_rec) THEN
     RAISE program_error;
  end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_SCL_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
/
