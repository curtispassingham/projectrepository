PROMPT Creating Trigger 'EC_TABLE_DIFF_TYPE_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_DIFF_TYPE_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON DIFF_TYPE
 FOR EACH ROW
DECLARE

   L_record          DIFF_TYPE%ROWTYPE                   :=  NULL;
   L_action_type     VARCHAR2(1)                         :=  NULL;
   L_message         rib_sxw.SXWHandle;
   L_status          VARCHAR2(1)                         :=  NULL;
   L_text            VARCHAR2(255)                       :=  NULL;
   L_message_type    CODES_MFQUEUE.MESSAGE_TYPE%TYPE     :=  NULL;

   PROGRAM_ERROR     EXCEPTION;
BEGIN

   if DELETING then

      L_action_type := 'D';
      L_message_type := RMSMFM_SEEDDATA.DIFF_TYPE_DEL_TYPE;
      L_record.diff_type := :old.diff_type;

   else
      if INSERTING then

         L_action_type := 'A';
         L_message_type := RMSMFM_SEEDDATA.DIFF_TYPE_CRE_TYPE;

      else

         L_action_type := 'M';
         L_message_type := RMSMFM_SEEDDATA.DIFF_TYPE_MOD_TYPE;

         if ((:old.diff_type        = :new.diff_type) and
             (:old.diff_type_desc   = :new.diff_type_desc)) then
            
            return;

         end if;
      end if;

      L_record.diff_type       := :new.diff_type;
      L_record.diff_type_desc  := :new.diff_type_desc;

   end if;

   if not DIFF_TYPE_XML.BUILD_MESSAGE(L_status,
                                      L_text,
                                      L_message,
                                      L_record,
                                      L_action_type) then
      raise PROGRAM_ERROR;
   end if;

   RMSMFM_SEEDDATA.ADDTOQ(L_status,
                          L_text,
                          L_message_type,
                          'OOOO',
                          L_message);

   if L_status = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_DIFF_TYPE_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END EC_TABLE_DIFF_TYPE_AIUDR;
/
