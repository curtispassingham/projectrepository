PROMPT Creating Trigger 'EC_TABLE_DIV_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_DIV_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON DIVISION
 FOR EACH ROW
DECLARE


   L_message_type       MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
   L_division           DIVISION.DIVISION%TYPE;
   L_division_rec       DIVISION%ROWTYPE;
   L_error_msg          VARCHAR2(255) := NULL;
   L_status             VARCHAR2(1);
   program_error EXCEPTION;
BEGIN

  if inserting then
    L_message_type := RMSMFM_MERCHHIER.DIV_ADD;
    L_division := :new.division;
    L_division_rec.division := :new.division;
    L_division_rec.div_name := :new.div_name;
    L_division_rec.buyer := :new.buyer;
    L_division_rec.merch := :new.merch;
    L_division_rec.total_market_amt := :new.total_market_amt;
  elsif updating then
    L_message_type := RMSMFM_MERCHHIER.DIV_UPD;
    L_division := :old.division;
    L_division_rec.division := :old.division;
    L_division_rec.div_name := :new.div_name;
    L_division_rec.buyer := :new.buyer;
    L_division_rec.merch := :new.merch;
    L_division_rec.total_market_amt := :new.total_market_amt;
  else -- Deleting
    L_message_type := RMSMFM_MERCHHIER.DIV_DEL;
    L_division := :old.division;
  end if;

  if NOT RMSMFM_MERCHHIER.ADDTOQ(L_error_msg, 
                                 L_message_type, 
                                 L_division, 
                                 L_division_rec, 
                                 null, 
                                 null, 
                                 null, 
                                 null, 
                                 null, 
                                 null, 
                                 null, 
                                 null) THEN
     RAISE program_error;
  end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_DIVISION_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
/
