--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.0 $
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- TRIGGER ADDED:          DE_TABLE_CLS_AIUDR
----------------------------------------------------------------------------

whenever SQLERROR exit

--------------------------------------
--       ADDING TRIGGER
--------------------------------------
PROMPT Creating Trigger 'DE_TABLE_CLS_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_CLS_AIUDR
   AFTER INSERT OR UPDATE OR DELETE ON CLASS
   FOR EACH ROW

DECLARE
   L_class_id        CLASS.CLASS_ID%TYPE;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_status          VARCHAR2(1);
   L_action_type     MERCHHIER_EXPORT_STG.ACTION_TYPE%TYPE;
   program_error     EXCEPTION;
BEGIN

   if inserting then
      L_class_id       := :new.class_id;
      L_action_type    := DATA_EXPORT_SQL.CLASS_CRE;
   elsif updating then
       if :old.class_name <> :new.class_name then
          L_class_id    := :old.class_id;
          L_action_type := DATA_EXPORT_SQL.CLASS_UPD;
       end if;
   else --Deleting
      L_class_id    := :old.class_id;
      L_action_type := DATA_EXPORT_SQL.CLASS_DEL;
   end if;

   if L_class_id is NOT NULL then
      if DATA_EXPORT_SQL.INS_MERCHHIER_EXPORT_STG(L_error_message,
                                                  L_action_type,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  L_class_id,
                                                  NULL) = FALSE then
         RAISE program_error;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_CLS_AIUDR');
   
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/