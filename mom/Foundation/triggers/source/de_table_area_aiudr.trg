PROMPT Creating Trigger 'DE_TABLE_AREA_AIUDR'
CREATE OR REPLACE TRIGGER DE_TABLE_AREA_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON AREA
 FOR EACH ROW
DECLARE

   L_action_type   ORGHIER_EXPORT_STG.ACTION_TYPE%TYPE;
   L_area          AREA.AREA%TYPE;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_status        VARCHAR2(1);

   program_error EXCEPTION;

BEGIN

   if inserting then
      L_action_type := DATA_EXPORT_SQL.AREA_ADD;
      L_area        := :new.area;
   elsif updating then
      L_action_type := DATA_EXPORT_SQL.AREA_UPD;
      L_area := :old.area;
   else -- Deleting
      L_action_type := DATA_EXPORT_SQL.AREA_DEL;
      L_area := :old.area;
   end if;

   if NOT DATA_EXPORT_SQL.INS_ORGHIER_EXPORT_STG(L_error_msg,
                                                 L_action_type,
                                                 NULL,
                                                 NULL,
                                                 L_area,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL) then
      RAISE program_error;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_AREA_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
