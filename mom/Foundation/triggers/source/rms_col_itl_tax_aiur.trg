PROMPT Creating Trigger 'RMS_COL_ITL_TAX_AIUR'
CREATE OR REPLACE TRIGGER RMS_COL_ITL_TAX_AIUR
 AFTER INSERT OR UPDATE OF TAXABLE_IND
 ON ITEM_LOC
 FOR EACH ROW
DECLARE


   L_exists        VARCHAR2(1) := NULL;
   L_tax_change    VARCHAR2(1) := NULL;
   L_seq_no        NUMBER(10)  := NULL;
   L_error_message VARCHAR2(255) := NULL;

   cursor C_RECORD_EXISTS is
      select 'x'
        from tif_explode
       where item             = :new.item
         and store            = :new.loc
         and current_rate_ind = 'Y'
         and changed_ind      = L_tax_change
	   and ROWNUM = 1;
BEGIN

   if :new.loc_type = 'S' then
      if :new.taxable_ind = 'N' then
         L_tax_change := 'Y';
      else
         L_tax_change := 'N';
      end if;

      open  C_RECORD_EXISTS;
      fetch C_RECORD_EXISTS into L_exists;
      close C_RECORD_EXISTS;

      if L_exists IS NULL then

         if NEXT_TIFXPLD_SEQ_NO(L_seq_no,
                                 L_error_message) = FALSE then
              RAISE_APPLICATION_ERROR(-20000, L_error_message);
         end if;

         insert into tif_explode (seq_no,
                                  item,
                                  store,
                                  current_rate_ind,
                                  changed_ind)
                           select L_seq_no,
                                  :new.item,
                                  :new.loc,
                                  'Y',
                                  L_tax_change
                             from item_master
                            where item = :new.item
                              and item_level = tran_level;

      end if;
   end if;
END RMS_COL_ITL_TAX_AIUR;
/
