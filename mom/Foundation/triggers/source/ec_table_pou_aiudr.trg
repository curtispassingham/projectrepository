PROMPT Creating Trigger 'EC_TABLE_POU_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_POU_AIUDR
 AFTER DELETE OR INSERT
 ON PARTNER_ORG_UNIT
 FOR EACH ROW
DECLARE

   L_program_error  exception;
   L_partner        PARTNER_ORG_UNIT.PARTNER%TYPE;
   L_event          VARCHAR2(1) := 'A';
   L_valid          BOOLEAN;
   O_status         VARCHAR2(100);
   O_text           VARCHAR2(255);
   L_pou            PARTNER_ORG_UNIT%ROWTYPE;
   L_msg            CLOB;
   L_message_type   VARCHAR2(15) := NULL;
   L_ret_allow_ind  VARCHAR2(2) := NULL;
   L_return         BOOLEAN;
   
BEGIN
   L_pou.PARTNER := :new.PARTNER;
   L_pou.ORG_UNIT_ID := :new.ORG_UNIT_ID;
   L_pou.PARTNER_TYPE := :new.PARTNER_TYPE;
   L_pou.PRIMARY_PAY_SITE := :new.PRIMARY_PAY_SITE;
   
   if DELETING then
      L_partner := TO_NUMBER(:old.partner);
      L_event := 'D';
      L_pou.PARTNER := :old.PARTNER;
      L_pou.ORG_UNIT_ID := :old.ORG_UNIT_ID;
      L_message_type := RMSMFM_SUPPLIER.OU_DEL;
   elsif INSERTING then
      L_partner := TO_NUMBER(:new.partner);
      L_event := 'A';
      L_message_type := RMSMFM_SUPPLIER.OU_CRE;
   end if;
   ---
   supplier_xml.build_org_unit(L_pou,
                               L_event,
                               L_msg,
                               O_status,
                               O_text);
   ---
   if O_status = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
   end if;
   ---
   if L_message_type != RMSMFM_SUPPLIER.OU_DEL then
      L_return := supplier_xml.get_keys(O_text,
                                        L_valid,
                                        L_ret_allow_ind,
                                        NULL,
                                        L_partner);
      ---
      if not L_return then
         raise PROGRAM_ERROR;
      end if;
   end if;
   ---
   rmsmfm_supplier.addtoq(L_message_type,
                          L_partner,
                          NULL,
                          NULL,
                          L_ret_allow_ind,
                          L_pou.org_unit_id,
                          L_msg,
                          O_status,
                          O_text);
   ---
   if O_status = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
   end if;
   
EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_POU_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, O_text);
END ;
/
