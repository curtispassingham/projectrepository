PROMPT Creating Trigger 'EC_TABLE_UIF_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_UIF_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON UDA_ITEM_FF
 FOR EACH ROW
DECLARE

   L_queue_rec     ITEM_MFQUEUE%ROWTYPE := NULL;
   L_status        VARCHAR2(1) := NULL;
   L_text          VARCHAR2(255) := NULL;
   L_item_row      ITEM_MASTER%ROWTYPE := NULL;
   
   PROGRAM_ERROR   EXCEPTION;
BEGIN
   if DELETING or UPDATING then
      L_queue_rec.message_type := RMSMFM_ITEMS.UDAF_DEL;
      L_queue_rec.item     := :old.item;
      L_queue_rec.uda_id   := :old.uda_id;
      L_queue_rec.uda_text := :old.uda_text;
      
      if UPDATING then
         L_queue_rec.transaction_time_stamp := :new.last_update_datetime;
      end if;

      if not ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                             L_item_row,
                                             L_queue_rec.item) then
         raise PROGRAM_ERROR;
      end if;

      if L_item_row.item_level = L_item_row.tran_level then
         if RMSMFM_ITEMS.ADDTOQ(L_text,
                                L_queue_rec,
                                NULL,               ---I_sellable_ind
                                NULL) = FALSE then  ---I_tran_level_ind
            raise PROGRAM_ERROR;
         end if;
      end if;
   end if;
   
   if INSERTING or UPDATING then 
      L_queue_rec.message_type := RMSMFM_ITEMS.UDAF_ADD;
      L_queue_rec.item     := :new.item;
      L_queue_rec.uda_id   := :new.uda_id;
      L_queue_rec.uda_text := :new.uda_text;
      L_queue_rec.transaction_time_stamp := :new.last_update_datetime;

      if not ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_text,
                                             L_item_row,
                                             L_queue_rec.item) then
         raise PROGRAM_ERROR;
      end if;
      
      if L_item_row.item_level = L_item_row.tran_level then
         if RMSMFM_ITEMS.ADDTOQ(L_text,
                                L_queue_rec,
                                NULL,               ---I_sellable_ind
                                NULL) = FALSE then  ---I_tran_level_ind
            raise PROGRAM_ERROR;
         end if;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_UIF_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END;
/