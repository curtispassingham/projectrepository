----------------------------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
----------------------------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Tigger Added:  DE_TABLE_VI_AIUDR
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Tigger
--------------------------------------
PROMPT Creating Trigger "DE_TABLE_VI_AIUDR"
CREATE OR REPLACE TRIGGER DE_TABLE_VI_AIUDR
   AFTER INSERT OR UPDATE OR DELETE ON VAT_ITEM
   FOR EACH ROW

DECLARE
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_status          VARCHAR2(1);
   L_action_type     ITEM_EXPORT_STG.ACTION_TYPE%TYPE;
   L_item            VAT_ITEM.ITEM%TYPE;
   L_vat_region      VAT_REGION.VAT_REGION%TYPE;
   L_vat_code        VAT_CODES.VAT_CODE%TYPE;
   L_vat_type        VAT_ITEM.VAT_TYPE%TYPE;
   L_sell_exists     VARCHAR2(1) := 'N';
   L_active_date     VAT_ITEM.ACTIVE_DATE%TYPE;
   PROGRAM_ERROR     EXCEPTION;

   cursor C_SELL_ITEM_EXISTS is
      select 'Y'
        from item_master
       where item = L_item
         and sellable_ind = 'Y';

BEGIN
   if inserting then
      L_item := :new.item;

      open C_SELL_ITEM_EXISTS;
      fetch C_SELL_ITEM_EXISTS into L_sell_exists;
      close C_SELL_ITEM_EXISTS;

      if L_sell_exists = 'Y' then
         L_action_type := DATA_EXPORT_SQL.VATITM_CRE;
         L_vat_region  := :new.vat_region;
         L_vat_code    := :new.vat_code;
         L_vat_type    := :new.vat_type;
         L_active_date := :new.active_date;
      end if;
   elsif updating then
      L_item := :old.item;

      open C_SELL_ITEM_EXISTS;
      fetch C_SELL_ITEM_EXISTS into L_sell_exists;
      close C_SELL_ITEM_EXISTS;

      if L_sell_exists = 'Y' then
         L_action_type := DATA_EXPORT_SQL.VATITM_UPD;
         L_vat_region  := :old.vat_region;
         L_vat_code    := :old.vat_code;

         if :old.vat_type <> :new.vat_type then
            L_vat_type := :new.vat_type;
         else
            L_vat_type := :old.vat_type;
         end if;

         if :old.active_date <> :new.active_date then
            L_active_date := :new.active_date;
         else
            L_active_date := :old.active_date;
         end if;
      end if;
   else
      L_item := :old.item;
      open C_SELL_ITEM_EXISTS;
      fetch C_SELL_ITEM_EXISTS into L_sell_exists;
      close C_SELL_ITEM_EXISTS;

      if L_sell_exists = 'Y' then
         L_action_type := DATA_EXPORT_SQL.VATITM_DEL;
         L_vat_region  := :old.vat_region;
         L_vat_code    := :old.vat_code;
         L_vat_type    := :old.vat_type;
         L_active_date := :old.active_date;
      end if;
   end if;

   if L_action_type is NOT NULL then
      if DATA_EXPORT_SQL.INS_ITEM_EXPORT_STG(L_error_message,
                                             L_action_type,
                                             L_item,
                                             NULL, -- item_parent
                                             NULL, -- item_grandparent
                                             NULL, -- item_level
                                             NULL, -- tran_level
                                             L_vat_region,
                                             L_vat_code,
                                             L_vat_type,
                                             NULL, -- merchandise_ind
                                             L_active_date,
                                             NULL, -- location
                                             NULL) = FALSE then
         RAISE PROGRAM_ERROR;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'DE_TABLE_VI_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/