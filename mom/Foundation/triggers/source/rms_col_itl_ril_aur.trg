PROMPT Creating Trigger 'RMS_COL_ITL_RIL_AUR'
CREATE OR REPLACE TRIGGER RMS_COL_ITL_RIL_AUR
 AFTER UPDATE OF STORE_ORD_MULT
, STATUS
, PRIMARY_CNTRY
, PRIMARY_SUPP
 ON ITEM_LOC
 FOR EACH ROW
BEGIN

   if updating then
      if :new.status != 'A' and :old.status = 'A' then

         insert into repl_item_loc_updates (item,
                                            location,
                                            loc_type,
                                            change_type)
                                    values (:new.item,
                                            :new.loc,
                                            :new.loc_type,
                                            'ILST');

      end if;

      if :new.store_ord_mult != :old.store_ord_mult then

         insert into repl_item_loc_updates (item,
                                            location,
                                            loc_type,
                                            change_type)
                                    values (:new.item,
                                            :new.loc,
                                            :new.loc_type,
                                            'ILSOM');

      end if;

      if :new.primary_supp   != :old.primary_supp or
         :new.primary_cntry  != :old.primary_cntry then

         insert into repl_item_loc_updates (item,
                                            location,
                                            loc_type,
                                            change_type)
                                    values (:new.item,
                                            :new.loc,
                                            :new.loc_type,
                                            'ILSC');

         update item_loc_soh
            set primary_supp  = :new.primary_supp,
                primary_cntry = :new.primary_cntry
          where item = :new.item
            and loc  = :new.loc;

      end if;
   end if;

END;
/
