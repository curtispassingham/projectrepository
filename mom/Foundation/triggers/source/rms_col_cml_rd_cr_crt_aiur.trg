PROMPT Creating Trigger 'RMS_COL_CML_RD_CR_CRT_AIUR'
CREATE OR REPLACE TRIGGER RMS_COL_CML_RD_CR_CRT_AIUR
 AFTER INSERT OR UPDATE OF COMP_RETAIL_TYPE
, REC_DATE
, COMP_RETAIL
 ON COMP_SHOP_LIST
 FOR EACH ROW
DECLARE

   L_item             COMP_SHOP_LIST.ITEM%TYPE;
   O_error_message    VARCHAR2(255);
   O_exist            BOOLEAN := TRUE;
   L_program          VARCHAR2(20);
   L_parent_desc      ITEM_MASTER.ITEM_DESC%TYPE;
   L_grandparent      ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
   L_grandparent_desc ITEM_MASTER.ITEM_DESC%TYPE;
   L_vdate            DATE;
BEGIN
   L_vdate   := DATES_SQL.GET_VDATE;
   L_program := 'COMP_PRICE_HIST';
   if INSERTING or UPDATING then
      --- Check for nulls
      if :new.rec_date is not NULL and
         :new.comp_retail is not NULL and
         :new.comp_retail_type is not NULL then
         --- Check for Item/Ref. Item
         if :new.item is NULL then
            if not ITEM_ATTRIB_SQL.GET_PARENT_INFO(O_error_message,
                                                   L_item,
                                                   L_parent_desc,
                                                   L_grandparent,
                                                   L_grandparent_desc,
                                                   :new.ref_item) then
               RAISE_APPLICATION_ERROR(-20000, O_error_message);
               O_exist := FALSE;
            end if;
         else
            L_item := :new.item;
         end if;
         ---
         if O_exist then
            insert into comp_price_hist (item,
                                         ref_item,
                                         comp_store,
                                         rec_date,
                                         comp_retail,
                                         comp_retail_type,
                                         prom_start_date,
                                         prom_end_date,
                                         offer_type,
                                         multi_units,
                                         multi_unit_retail,
                                         post_date)
                                 values (L_item,
                                         :new.ref_item,
                                         :new.comp_store,
                                         :new.rec_date,
                                         :new.comp_retail,
                                         :new.comp_retail_type,
                                         :new.prom_start_date,
                                         :new.prom_end_date,
                                         :new.offer_type,
                                         :new.multi_units,
                                         :new.multi_unit_retail,
                                         L_vdate);
         end if;   -- if O_exist
      end if;   -- NULL check on 3 key vars
   end if;   -- if INSERTING or UPDATING

EXCEPTION
   when DUP_VAL_ON_INDEX then
      null;
END RMS_COL_CML_RD_CR_CRT_AIUR;
/
