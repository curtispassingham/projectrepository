PROMPT Creating Trigger 'EC_TABLE_BAN_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_BAN_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON BANNER
 FOR EACH ROW
DECLARE

   L_record        BANNER%ROWTYPE := NULL;
   L_action_type   VARCHAR2(1) := NULL;
   L_message       CLOB;
   L_status        VARCHAR2(1) := NULL;
   L_text          VARCHAR2(255) := NULL;
   L_message_type  BANNER_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;

   PROGRAM_ERROR   EXCEPTION;
BEGIN
   if DELETING then
      /* For deletes, the row identifier will be published. */

      L_action_type := 'D';
      L_message_type := 'BannerDel';
      L_record.banner_id := :old.banner_id;
   else
      if INSERTING then
         L_action_type := 'A';
         L_message_type := 'BannerCre';
      else
         L_action_type := 'M';
         L_message_type := 'BannerMod';
      end if;

      /* For inserts and updates, more of the values from the table
         will be published.
      */

      L_record.banner_id := :new.banner_id;
      L_record.banner_name := :new.banner_name;

   end if;

   /* Creates an XML message, puts it in CLOB form, returns the CLOB.
      L_record should be populated with all of the necessary values
      from the table before calling BUILD_MESSAGE.
      L_action_type should specify the type of event ('A'dd, 'M'odify, or 'D'elete)
   */
   if not BANNER_XML.BUILD_MESSAGE(L_status,
                                   L_text,
                                   L_message,
                                   L_record,
                                   L_action_type) then
     raise PROGRAM_ERROR;
   end if;

   /* Takes the CLOB returned from BUILD_MESSAGE and puts it in the
      message queue table.
   */
   RMSMFM_BANNER.ADDTOQ(L_status,
                        L_text,
                        L_message_type,
                        L_record.banner_id,
                        NULL,
                        L_message);

   if L_status = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_text,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_BAN_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_text);

END;
/
