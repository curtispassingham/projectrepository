PROMPT Creating Trigger 'RMS_COL_ITL_UR_AUR'
CREATE OR REPLACE TRIGGER RMS_COL_ITL_UR_AUR
 AFTER UPDATE OF UNIT_RETAIL
 ON ITEM_LOC
 FOR EACH ROW
BEGIN

   update stake_sku_loc ssl
      set ssl.snapshot_unit_retail = NVL(:new.unit_retail,0)
    where ssl.item = :old.item
      and ssl.loc_type = 'S'
      and ssl.location = :old.loc
      and exists (select cycle_count
                                from stake_head sh,
                                     period p,
                                     system_options so
                               where stocktake_date between p.vdate
                                 and (p.vdate+so.stake_lockout_days)
                                 and ssl.cycle_count = sh.cycle_count);

END;
/
