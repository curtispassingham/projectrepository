PROMPT Creating Trigger 'RMS_COL_PAI_PQ_AUR'
CREATE OR REPLACE TRIGGER RMS_COL_PAI_PQ_AUR
 AFTER UPDATE OF PACK_QTY
 ON PACKITEM
 FOR EACH ROW
BEGIN

   if updating then
      if :new.pack_qty != :old.pack_qty then

         insert into repl_item_loc_updates (item,
                                            supplier,
                                            origin_country_id,
                                            location,
                                            loc_type,
                                            change_type)
                                     select :new.pack_no,
                                            NULL,
                                            NULL,
                                            -1,
                                            NULL,
                                            'PQTY'
                                       from dual
                                      where  exists (select 'x'
                                                       from repl_item_loc ril
                                                      where ril.primary_pack_no = :new.pack_no);
      end if;
   end if;

END;
/
