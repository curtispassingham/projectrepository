PROMPT Creating Trigger 'EC_TABLE_CNT_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_CNT_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON COUNTRY
 FOR EACH ROW
DECLARE



   L_error_message  VARCHAR2(255)                     := NULL;
   L_country_id     COUNTRY.COUNTRY_ID%TYPE           := NULL;
   L_country_desc   COUNTRY.COUNTRY_DESC%TYPE         := NULL;
   L_message_type   SEEDOBJ_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;
   L_status_code    VARCHAR2(255)                     := NULL;

   PROGRAM_ERROR   EXCEPTION;
BEGIN


   if DELETING then

      L_country_id    := :old.country_id;
      L_message_type  := RMSMFM_SEEDOBJ.COUNTRY_DEL;

   else

      L_country_id    := :new.country_id;
      L_country_desc  := :new.country_desc;

      if INSERTING then
         L_message_type := RMSMFM_SEEDOBJ.COUNTRY_ADD;

      else  --UPDATING

         if :old.country_desc != :new.country_desc then
            L_message_type := RMSMFM_SEEDOBJ.COUNTRY_UPD;
         end if;

      end if;

   end if;

   if L_message_type IS NOT NULL then

      if RMSMFM_SEEDOBJ.ADDTOQ(L_error_message,
                               L_message_type,
                               L_country_id,
                               NULL,
                               L_country_desc,
                               NULL,
                               NULL,
                               NULL) = FALSE then
         raise PROGRAM_ERROR;
      end if;

   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status_code,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_CNT_AIUDR');
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

END EC_TABLE_CNT_AIUDR;
/
