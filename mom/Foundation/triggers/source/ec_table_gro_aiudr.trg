PROMPT Creating Trigger 'EC_TABLE_GRO_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_GRO_AIUDR
 AFTER DELETE OR INSERT OR UPDATE
 ON GROUPS
 FOR EACH ROW
DECLARE


   L_message_type       MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE;
   L_group_no           GROUPS.GROUP_NO%TYPE;
   L_group_rec          GROUPS%ROWTYPE;
   L_error_msg          VARCHAR2(255) := NULL;
   L_status             VARCHAR2(1);
   program_error EXCEPTION;
BEGIN

  if inserting then
    L_message_type := RMSMFM_MERCHHIER.GRP_ADD;
    L_group_no := :new.group_no;
    L_group_rec.group_no := :new.group_no;
    L_group_rec.group_name := :new.group_name;
    L_group_rec.buyer := :new.buyer;
    L_group_rec.merch := :new.merch;
    L_group_rec.division := :new.division;
  elsif updating then
    L_message_type := RMSMFM_MERCHHIER.GRP_UPD;
    L_group_no := :old.group_no;
    L_group_rec.group_no := :old.group_no;
    L_group_rec.group_name := :new.group_name;
    L_group_rec.buyer := :new.buyer;
    L_group_rec.merch := :new.merch;
    L_group_rec.division := :new.division;
  else -- Deleting
    L_message_type := RMSMFM_MERCHHIER.GRP_DEL;
    L_group_no := :old.group_no;
  end if;

  if NOT RMSMFM_MERCHHIER.ADDTOQ(L_error_msg, 
                                 L_message_type, 
                                 null, 
                                 null, 
                                 L_group_no, 
                                 L_group_rec, 
                                 null, 
                                 null, 
                                 null, 
                                 null, 
                                 null, 
                                 null) THEN
     RAISE program_error;
  end if;

EXCEPTION
   when OTHERS then
      L_status := API_CODES.UNHANDLED_ERROR;
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'EC_TABLE_GRO_AIUDR');

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_msg);
END;
/
/
