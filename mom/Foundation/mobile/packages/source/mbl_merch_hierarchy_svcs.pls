CREATE OR REPLACE PACKAGE MBL_MERCH_HIERARCHY_SVC AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
----------------------------------------------------------------------------
--Function Name : DEPT_SEARCH
--Purpose       : This function will return list of departments based on the input search string,
--                page number and paze size.
--------------------------------------------------------------------------------
FUNCTION DEPT_SEARCH(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_result        IN OUT MBL_MERCH_HIER_DEPT_TBL,
                     IO_pagination   IN OUT MBL_PAGINATION_REC,
                     I_search_string IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
----------------------------------------------------------------------------
--Function Name : DEPT_LOAD
--Purpose       : This function will return list of departments and thier translated name based on 
--                the input list.
--------------------------------------------------------------------------------
FUNCTION DEPT_LOAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result           OUT MBL_MERCH_HIER_DEPT_TBL,
                   I_depts         IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
END MBL_MERCH_HIERARCHY_SVC;
/
