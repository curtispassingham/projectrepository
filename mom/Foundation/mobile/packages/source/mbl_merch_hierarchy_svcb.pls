CREATE OR REPLACE PACKAGE BODY MBL_MERCH_HIERARCHY_SVC AS
--------------------------------------------------------------------------------
FUNCTION DEPT_SEARCH(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_result        IN OUT MBL_MERCH_HIER_DEPT_TBL,
                     IO_pagination   IN OUT MBL_PAGINATION_REC,
                     I_search_string IN     VARCHAR2)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'MBL_MERCH_HIERARCHY_SVC.DEPT_SEARCH';
   L_row_start          NUMBER(10)   := 0;
   L_row_end            NUMBER(10)   := 0;
   L_num_rec_found      NUMBER(10)   := 0;
   L_counter_table      OBJ_NUMERIC_ID_TABLE;

   cursor c_dept is
      select MBL_MERCH_HIER_DEPT_REC(inner.dept,
                                     inner.dept_name),
               inner.total_rows
          from (select d.dept,
                       d.dept_name,
                       rank() over (order by d.dept_name, d.dept) counter,
                       count(*) over () total_rows
                  from v_deps d
                 where (d.dept like('%'||nvl(I_search_string,d.dept)||'%') or
                        lower(d.dept_name) like('%'||lower(nvl(I_search_string,d.dept_name))||'%'))
                 ) inner
         where inner.counter >= L_row_start
           and inner.counter < L_row_end
       order by inner.dept_name;
	
BEGIN

   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   open c_dept;
   fetch c_dept bulk collect into O_result, L_counter_table;
   close c_dept;

   if L_counter_table       is NULL or
      L_counter_table.count <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_table(L_counter_table.first);
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_dept%ISOPEN then
         close c_dept;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END DEPT_SEARCH;
--------------------------------------------------------------------------------
FUNCTION DEPT_LOAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result           OUT MBL_MERCH_HIER_DEPT_TBL,
                   I_depts         IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'MBL_MERCH_HIERARCHY_SVC.DEPT_LOAD';

   cursor c_dept is
      select MBL_MERCH_HIER_DEPT_REC(d.dept,
                                     d.dept_name)
        from v_deps d,
             table(cast(I_depts as OBJ_NUMERIC_ID_TABLE)) input_depts
       where d.dept = value(input_depts)
       order by d.dept_name;
	
BEGIN

   open c_dept;
   fetch c_dept bulk collect into O_result;
   close c_dept;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_dept%ISOPEN then
         close c_dept;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END DEPT_LOAD;
-------------------------------------------------------------------------------------
END MBL_MERCH_HIERARCHY_SVC;
/
