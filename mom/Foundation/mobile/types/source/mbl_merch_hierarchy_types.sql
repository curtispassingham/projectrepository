-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
DROP TYPE MBL_MERCH_HIER_DEPT_TBL FORCE
/
DROP TYPE MBL_MERCH_HIER_DEPT_REC FORCE
/

CREATE OR REPLACE TYPE MBL_MERCH_HIER_DEPT_REC AS OBJECT
(
   DEPT  NUMBER(4),
   NAME  VARCHAR2(120)
)
/

CREATE OR REPLACE TYPE MBL_MERCH_HIER_DEPT_TBL AS TABLE OF MBL_MERCH_HIER_DEPT_REC
/
