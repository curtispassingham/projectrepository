#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_fif_gl_setup.ksh
# Desc: Load the flat file data to staging table fif_gl_setup through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_fif_gl_setup.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_fif_gl_setup.ctl"

inputFile='dc_fif_gl_setup.dat'
#--------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
         return ${OK}
      fi
   fi
}

function LOAD_FIF_GL_SETUP
{
   echo "set feedback off;
      set heading off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      BEGIN

         insert into fif_gl_setup (set_of_books_id,
                                   last_update_id,
                                   sequence1_desc,
                                   sequence2_desc,
                                   sequence3_desc,
                                   sequence4_desc,
                                   sequence5_desc,
                                   sequence6_desc,
                                   sequence7_desc,
                                   sequence8_desc,
                                   sequence9_desc,
                                   sequence10_desc,
                                   category_id,
                                   deliver_to_location_id,
                                   destination_organization_id,
                                   period_name,
                                   set_of_books_desc,
                                   currency_code)
                          select   set_of_books_id,
                                   last_update_id,
                                   sequence1_desc,
                                   sequence2_desc,
                                   sequence3_desc,
                                   sequence4_desc,
                                   sequence5_desc,
                                   sequence6_desc,
                                   sequence7_desc,
                                   sequence8_desc,
                                   sequence9_desc,
                                   sequence10_desc,
                                   category_id,
                                   deliver_to_location_id,
                                   destination_organization_id,
                                   period_name,
                                   set_of_books_desc,
                                   currency_code
                            from   dc_fif_gl_setup;

         COMMIT;

      EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;

      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;

      " | sqlplus -s ${connectStr} >> ${logFile}
   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}

ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"

LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"

# Check configuration file
checkCfg
chk_status=$?
if [[ ${chk_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${chk_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi

# Check the data file
retCode=$(checkFile -f $inputFile)
if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Load the file
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOAD_FIF_GL_SETUP
   ld_fif_status=$?
   if [[ ${ld_fif_status} -ne ${OK} ]]; then
      LOG_ERROR "Load function get failed during loading from dc table to main table" "LOAD_FIF_GL_SETUP" "${ld_fif_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_fif_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
   fi
fi
# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi
LOG_MESSAGE "Job ${pgmName}.ksh completed successfully by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"