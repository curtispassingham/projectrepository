LOAD DATA
REPLACE
INTO TABLE DC_SUP_IMPORT_ATTR
WHEN 
    ( SUPPLIER != BLANKS )
AND ( RELATED_IND != BLANKS )
AND ( BENEFICIARY_IND != BLANKS )
AND ( WITH_RECOURSE_IND != BLANKS )
AND ( REVOCABLE_IND != BLANKS )
FIELDS TERMINATED BY '|'
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( SUPPLIER
 ,AGENT
 ,ADVISING_BANK
 ,ISSUING_BANK
 ,LADING_PORT
 ,DISCHARGE_PORT
 ,MFG_ID
 ,RELATED_IND
 ,BENEFICIARY_IND
 ,WITH_RECOURSE_IND
 ,REVOCABLE_IND
 ,VARIANCE_PCT
 ,LC_NEG_DAYS
 ,PLACE_OF_EXPIRY
 ,DRAFTS_AT
 ,PRESENTATION_TERMS
 ,FACTORY
 ,PARTNER_TYPE_1
 ,PARTNER_1
 ,PARTNER_TYPE_2
 ,PARTNER_2
 ,PARTNER_TYPE_3
 ,PARTNER_3)