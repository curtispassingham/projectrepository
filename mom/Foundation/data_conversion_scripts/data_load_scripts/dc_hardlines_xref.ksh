#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_hardlines_xref.ksh
# Desc: Load the flat file data to staging table through sqlloader and from staging to main tables.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_hardlines_xref.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_hardlines_xref.ctl"
inputFile='dc_hardlines_xref.dat'
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_FILE
# Purpose : Load data from .dat to dc_class table(using sqlloader).
#----------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${OK}
      fi
   fi
}
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_HARDLINES_XREF
# Purpose : load data from data conversion table to base tables.
#----------------------------------------------------------------------------------------------------
function LOAD_HARDLINES_XREF
{
   echo "set feedback off;
      set heading off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      BEGIN

        -- Insert xref items
        insert into item_master(item,
                                item_parent,
                                item_number_type,
                                pack_ind,
                                item_level,
                                tran_level,
                                item_aggregate_ind,
                                diff_1_aggregate_ind,
                                diff_2_aggregate_ind,
                                diff_3_aggregate_ind,
                                diff_4_aggregate_ind,
                                dept,
                                class,
                                subclass,
                                cost_zone_group_id,
                                status,
                                item_desc,
                                short_desc,
                                item_desc_secondary,
                                desc_up,
                                primary_ref_item_ind,
                                standard_uom,
                                uom_conv_factor,
                                merchandise_ind,
                                store_ord_mult,
                                forecast_ind,
                                catch_weight_ind,
                                const_dimen_ind,
                                simple_pack_ind,
                                contains_inner_ind,
                                sellable_ind,
                                orderable_ind,
                                gift_wrap_ind,
                                ship_alone_ind,
                                create_datetime,
                                last_update_id,
                                last_update_datetime,
                                item_xform_ind,
                                inventory_ind,
                                aip_case_type,
                                comments,
								perishable_ind,
                                product_classification,
                                brand_name)
                         select r.item,
                                h.item,
                                r.item_number_type,
                                'N', -- PACK_IND
                                2, -- ITEM_LEVEL
                                1, -- TRAN_LEVEL
                                'N', -- ITEM_AGGREGATE_IND
                                'N', -- DIFF_1_AGGREGATE_IND
                                'N', -- DIFF_2_AGGREGATE_IND
                                'N', -- DIFF_3_AGGREGATE_IND
                                'N', -- DIFF_4_AGGREGATE_IND
                                h.dept,
                                h.class,
                                h.subclass,
                                h.cost_zone_group_id,
                                'A', -- STATUS
                                r.item_desc,
                                nvl(r.short_desc,rtrim(substrb(r.item_desc,1,120))), -- SHORT_DESC
                                r.item_desc_secondary,
                                upper(r.item_desc), -- DESC_UP
                                nvl(r.primary_ref_item_ind,'N'), -- PRIMARY_REF_ITEM_IND
                                h.standard_uom,
                                h.uom_conv_factor,
                                nvl(h.merchandise_ind,'Y'), -- MERCHANDISE_IND
                                h.store_ord_mult,
                                nvl(h.forecast_ind,'Y'), -- FORECAST_IND,
                                'N', -- CATCH_WEIGHT_IND
                                'N', -- CONST_DIMEN_IND
                                'N', -- SIMPLE_PACK_IND
                                'N', -- CONTAINS_INNER_IND
                                'Y', -- SELLABLE_IND
                                'Y', -- ORDERABLE_IND
                                'N', -- GIFT_WRAP_IND
                                'N', -- SHIP_ALONE_IND
                                SYSDATE, -- CREATE_DATETIME
                                USER, -- LAST_UPDATE_ID
                                SYSDATE, -- LAST_UPDATE_DATETIME
                                'N', -- ITEM_XFORM_IND
                                'Y', -- INVENTORY_IND
                                r.aip_case_type,
                                r.comments,
				                'N', --PERISHABLE_IND
                                r.product_classification,
                                r.brand_name
                           from dc_hardlines h,
                                dc_hardlines_xref r
                          where h.item = r.item_parent;

           DECLARE
             CURSOR c_items
             IS
               SELECT item FROM DC_HARDLINES_XREF;
             l_items ITEM_TBL;
             L_error_message rtk_errors.rtk_text%type;
           BEGIN
             OPEN c_items;
             LOOP
               FETCH c_items bulk collect INTO l_items limit 1000;
               IF l_items.count                                                 > 0 THEN
                 IF ITEM_APPROVAL_SQL.SET_ALC_ITEM_TYPE(L_error_message,l_items)=false THEN
                   raise_application_error(-20001,L_error_message);
                 END IF;
               END IF;
               EXIT
             WHEN c_items%notfound;
             END LOOP;
           END;


         COMMIT;

      EXCEPTION
         when OTHERS then
          ROLLBACK;
          :GV_script_error := SQLERRM;
          :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;

      /" | sqlplus -s ${connectStr} >> ${logFile}
   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}.${pgmPID}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

# Check the data file
retCode=$(checkFile -f $inputFile)
if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Load the file using sqlloader
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_status}
else
   LOG_MESSAGE "Sqlloader function completed successfully." "LOAD_FILE" ${OK} ${logFile} ${pgmName} ${pgmPID}

   # Load the data from data conversion to base tables
   LOAD_HARDLINES_XREF
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data from data conversion to base tables." "LOAD_HARDLINES_XREF" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_HARDLINES_XREF" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}
