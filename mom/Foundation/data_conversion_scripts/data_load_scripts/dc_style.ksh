#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_style.ksh
# Desc: Load the flat file data to staging table dc_style through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_style.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

inputFile_style='dc_style.dat'
inputFile_fashion='dc_fashion_sku.dat'
inputFile="dc_style.dat
           dc_fashion_sku.dat"
#--------------------------------------------------------------------------------------------------
function LOAD_STYLE_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile_style%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE_STYLE} log=${sqlldrLog} data=${data_dir}/${inputFile_style} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile_style}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
         return ${OK}
      fi
   fi
}
#--------------------------------------------------------------------------------------------------
function LOAD_FASHION_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile_fashion%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE_FASHION} log=${sqlldrLog} data=${data_dir}/${inputFile_fashion} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile_fashion}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
         return ${OK}
      fi
   fi
}
#-------------------------------------------------------------------------------------------------
function LOAD_STYLE_SKU
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000
     
      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);
      
      EXEC :GV_return_code := 0;
      
      WHENEVER SQLERROR EXIT 1
      
      DECLARE
         -- Defaults not in external table 
         L_item_number_type            ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE := 'ITEM';
         L_status                      ITEM_MASTER.STATUS%TYPE := 'A';                                  
         L_item_level                  ITEM_MASTER.ITEM_LEVEL%TYPE := 1;                    
         L_tran_level                  ITEM_MASTER.TRAN_LEVEL%TYPE := 2; 
         L_sellable_ind                ITEM_MASTER.SELLABLE_IND%TYPE := 'Y';                  
         L_orderable_ind               ITEM_MASTER.ORDERABLE_IND%TYPE := 'Y';                  
         L_inventory_ind               ITEM_MASTER.INVENTORY_IND%TYPE := 'Y'; 

         L_primary_ref_item_ind        ITEM_MASTER.PRIMARY_REF_ITEM_IND%TYPE := 'N';
         L_pack_ind                    ITEM_MASTER.PACK_IND%TYPE := 'N';                
         L_catch_weight_ind            ITEM_MASTER.CATCH_WEIGHT_IND%TYPE := 'N';              
         L_const_dimen_ind             ITEM_MASTER.CONST_DIMEN_IND%TYPE := 'N';               
         L_simple_pack_ind             ITEM_MASTER.SIMPLE_PACK_IND%TYPE := 'N';               
         L_contains_inner_ind          ITEM_MASTER.CONTAINS_INNER_IND%TYPE := 'N';                                            
         L_gift_wrap_ind               ITEM_MASTER.GIFT_WRAP_IND%TYPE := 'N';                 
         L_ship_alone_ind              ITEM_MASTER.SHIP_ALONE_IND%TYPE := 'N';
         L_item_xform_ind              ITEM_MASTER.ITEM_XFORM_IND%TYPE := 'N'; 
                
         L_create_datetime             ITEM_MASTER.CREATE_DATETIME%TYPE := sysdate;               
         L_last_update_id              ITEM_MASTER.LAST_UPDATE_ID%TYPE := user;                
         L_last_update_datetime        ITEM_MASTER.LAST_UPDATE_DATETIME%TYPE := sysdate; 

         -- Defaults for NULL in style and for sku                 
         L_item_aggregate_ind          ITEM_MASTER.ITEM_AGGREGATE_IND%TYPE := 'N';            
         L_diff_aggregate_ind          ITEM_MASTER.DIFF_1_AGGREGATE_IND%TYPE := 'N';          
 
         -- Defaults for NULL in external table
         L_merchandise_ind             ITEM_MASTER.MERCHANDISE_IND%TYPE := 'Y';
         L_forecast_ind                ITEM_MASTER.FORECAST_IND%TYPE := 'Y';

         BEGIN

            -- Insert style level items

         insert into ITEM_MASTER
           (
             item,
             item_number_type,
             pack_ind,
             item_level,
             tran_level,
             item_aggregate_ind,
             diff_1,
             diff_2,
             diff_3,
             diff_4,
             diff_1_aggregate_ind,
             diff_2_aggregate_ind,
             diff_3_aggregate_ind,
             diff_4_aggregate_ind,
             dept,
             class,
             subclass,
             cost_zone_group_id,
             status,
             item_desc,
             short_desc,
             desc_up,
             item_desc_secondary,
             primary_ref_item_ind,
             standard_uom,
             uom_conv_factor,
             merchandise_ind,
             store_ord_mult,
             forecast_ind,
             catch_weight_ind,
             const_dimen_ind,
             simple_pack_ind,
             contains_inner_ind,
             sellable_ind,
             orderable_ind,
             gift_wrap_ind,
             ship_alone_ind,
             create_datetime,
             last_update_id,
             last_update_datetime,
             item_xform_ind,
             inventory_ind,
             aip_case_type,
             comments,
             perishable_ind,
             product_classification,
             brand_name
            )
             select s.item,                                                                                                                                                                                                   
                    L_item_number_type,
                    L_pack_ind,
                    L_item_level,
                    L_tran_level,
                    nvl(s.item_aggregate_ind, L_item_aggregate_ind),
                    coalesce(s.diff_1,s.diff_2,s.diff_3,s.diff_4),  -- diff 1
                    case
                    when nvl(s.diff_1,'-1') != '-1' then
                       coalesce(s.diff_2,s.diff_3,s.diff_4)
                    when nvl(s.diff_2,'-1') != '-1' then
                       nvl(s.diff_3,s.diff_4)
                    when nvl(s.diff_3,'-1') != '-1' then
                       s.diff_4
                    end, -- diff 2
                    case
                    when (nvl(s.diff_1,'-1') != '-1' and nvl(s.diff_2,'-1') != '-1') then
                       nvl(s.diff_3,s.diff_4)
                    when (nvl(s.diff_1,'-1') != '-1' and nvl(s.diff_3,'-1') != '-1') then
                       s.diff_4
                    when (nvl(s.diff_2,'-1') != '-1' and nvl(s.diff_3,'-1') != '-1') then
                       s.diff_4
                    end,  -- diff 3
                    case
                    when (nvl(s.diff_1,'-1') != '-1' and nvl(s.diff_2,'-1') != '-1'
                          and nvl(s.diff_3,'-1') != '-1') then
                       s.diff_4
                    end,  -- diff 4
                    coalesce(s.diff_1_aggregate_ind,s.diff_2_aggregate_ind,s.diff_3_aggregate_ind,s.diff_4_aggregate_ind,L_diff_aggregate_ind),  -- diff_1_aggregate_ind
                    case
                    when nvl(s.diff_1_aggregate_ind,'-1') != '-1' then
                       coalesce(s.diff_2_aggregate_ind,s.diff_3_aggregate_ind,s.diff_4_aggregate_ind,L_diff_aggregate_ind)
                    when nvl(s.diff_2_aggregate_ind,'-1') != '-1' then
                       coalesce(s.diff_3_aggregate_ind,s.diff_4_aggregate_ind,L_diff_aggregate_ind)
                    when nvl(s.diff_3_aggregate_ind,'-1') != '-1' then
                       nvl(s.diff_4_aggregate_ind,L_diff_aggregate_ind)
                    else
                       L_diff_aggregate_ind
                    end, -- diff_2_aggregate_ind
                    case
                    when (nvl(s.diff_1_aggregate_ind,'-1') != '-1' and nvl(s.diff_2_aggregate_ind,'-1') != '-1') then
                       coalesce(s.diff_3_aggregate_ind,s.diff_4_aggregate_ind,L_diff_aggregate_ind)
                    when (nvl(s.diff_1_aggregate_ind,'-1') != '-1' and nvl(s.diff_3_aggregate_ind,'-1') != '-1') then
                       nvl(s.diff_4_aggregate_ind,L_diff_aggregate_ind)
                    when (nvl(s.diff_2_aggregate_ind,'-1') != '-1' and nvl(s.diff_3_aggregate_ind,'-1') != '-1') then
                       nvl(s.diff_4_aggregate_ind,L_diff_aggregate_ind)
                    else
                       L_diff_aggregate_ind
                    end,  -- diff_3_aggregate_ind
                    case
                    when (nvl(s.diff_1_aggregate_ind,'-1') != '-1' and nvl(s.diff_2_aggregate_ind,'-1') != '-1'
                          and nvl(s.diff_3_aggregate_ind,'-1') != '-1') then
                       nvl(s.diff_4_aggregate_ind,L_diff_aggregate_ind)
                    else
                       L_diff_aggregate_ind
                    end,  -- diff_4_aggregate_ind
                    s.dept,
                    s.class,
                    s.subclass,
                    f.cost_zone_group_id,
                    L_status,
                    s.item_desc,
                    nvl(s.short_desc,rtrim(substrb(s.item_desc,1,120))),
                    upper(s.item_desc),
                    s.item_desc_secondary,
                    L_primary_ref_item_ind,
                    f.standard_uom,
                    f.uom_conv_factor,
                    nvl(f.merchandise_ind,L_merchandise_ind),
                    f.store_ord_mult,
                    nvl(f.forecast_ind,L_forecast_ind),
                    L_catch_weight_ind,
                    L_const_dimen_ind,
                    L_simple_pack_ind,
                    L_contains_inner_ind,
                    L_sellable_ind,
                    L_orderable_ind,
                    L_gift_wrap_ind,
                    L_ship_alone_ind,
                    L_create_datetime,
                    L_last_update_id,
                    L_last_update_datetime,
                    L_item_xform_ind,
                    L_inventory_ind,
                    s.aip_case_type,
                    s.comments,
                    nvl(s.perishable_ind,'N'),
                    s.product_classification,
                    s.brand_name
               from dc_fashion_sku f,
                    dc_style s
              where f.item_parent = s.item
                and f.primary_sku_ind = 'Y'; 
   
           DECLARE
             CURSOR c_items
             IS
               SELECT item FROM dc_style;
             l_items ITEM_TBL;
             L_error_message rtk_errors.rtk_text%type;
           BEGIN
             OPEN c_items;
             LOOP
               FETCH c_items bulk collect INTO l_items limit 1000;
               IF l_items.count                                                 > 0 THEN
                 IF ITEM_APPROVAL_SQL.SET_ALC_ITEM_TYPE(L_error_message,l_items)=false THEN
                   raise_application_error(-20001,L_error_message);
                 END IF;
               END IF;
               EXIT
             WHEN c_items%notfound;
             END LOOP;
           END;
           
           L_item_level := 2;
   
           -- Insert tran level(sku) items 
   
           insert into ITEM_MASTER
           (
             item,
             item_parent,
             item_number_type,
             pack_ind,
             item_level,
             tran_level,
             item_aggregate_ind,
             diff_1,
             diff_2,
             diff_3,
             diff_4,
             diff_1_aggregate_ind,
             diff_2_aggregate_ind,
             diff_3_aggregate_ind,
             diff_4_aggregate_ind,
             dept,
             class,
             subclass,
             cost_zone_group_id,
             status,
             item_desc,
             short_desc,
             desc_up,
             item_desc_secondary,
             primary_ref_item_ind,
             standard_uom,
             uom_conv_factor,
             merchandise_ind,
             store_ord_mult,
             forecast_ind,
             catch_weight_ind,
             const_dimen_ind,
             simple_pack_ind,
             contains_inner_ind,
             sellable_ind,
             orderable_ind,
             gift_wrap_ind,
             ship_alone_ind,
             create_datetime,
             last_update_id,
             last_update_datetime,
             item_xform_ind,
             inventory_ind,
             aip_case_type,
             comments,
             perishable_ind, 
             product_classification,
             brand_name
            )
             select f.item,
                    s.item,                                                                                                                                                                                                   
                    L_item_number_type,
                    L_pack_ind,
                    L_item_level,
                    L_tran_level,
                    L_item_aggregate_ind,
                    coalesce(f.diff_1,f.diff_2,f.diff_3,f.diff_4),  -- diff 1
                    case
                    when nvl(f.diff_1,'-1') != '-1' then
                       coalesce(f.diff_2,f.diff_3,f.diff_4)
                    when nvl(f.diff_2,'-1') != '-1' then
                       nvl(f.diff_3,f.diff_4)
                    when nvl(f.diff_3,'-1') != '-1' then
                       f.diff_4
                    end, -- diff 2
                    case
                    when (nvl(f.diff_1,'-1') != '-1' and nvl(f.diff_2,'-1') != '-1') then
                       nvl(f.diff_3,f.diff_4)
                    when (nvl(f.diff_1,'-1') != '-1' and nvl(f.diff_3,'-1') != '-1') then
                       f.diff_4
                    when (nvl(f.diff_2,'-1') != '-1' and nvl(f.diff_3,'-1') != '-1') then
                       f.diff_4
                    end,  -- diff 3
                    case
                    when (nvl(f.diff_1,'-1') != '-1' and nvl(f.diff_2,'-1') != '-1'
                          and nvl(f.diff_3,'-1') != '-1') then
                       f.diff_4
                    end,  -- diff 4
                    L_diff_aggregate_ind,
                    L_diff_aggregate_ind,
                    L_diff_aggregate_ind,
                    L_diff_aggregate_ind,
                    s.dept,
                    s.class,
                    s.subclass,
                    f.cost_zone_group_id,
                    L_status,
                    f.item_desc,
                    nvl(f.short_desc,rtrim(substrb(f.item_desc,1,120))),
                    upper(f.item_desc),
                    f.item_desc_secondary,
                    L_primary_ref_item_ind,
                    f.standard_uom,
                    f.uom_conv_factor,
                    nvl(f.merchandise_ind,L_merchandise_ind),
                    f.store_ord_mult,
                    nvl(f.forecast_ind,L_forecast_ind),
                    L_catch_weight_ind,
                    L_const_dimen_ind,
                    L_simple_pack_ind,
                    L_contains_inner_ind,
                    L_sellable_ind,
                    L_orderable_ind,
                    L_gift_wrap_ind,
                    L_ship_alone_ind,
                    L_create_datetime,
                    L_last_update_id,
                    L_last_update_datetime,
                    L_item_xform_ind,
                    L_inventory_ind,
                    f.aip_case_type,
                    f.comments,
                    nvl(f.perishable_ind,'N'), 
                    f.product_classification,
                    f.brand_name
               from dc_fashion_sku f,
                    dc_style s
              where f.item_parent = s.item;  
              
           DECLARE
             CURSOR c_items
             IS
               SELECT item FROM dc_fashion_sku;
             l_items ITEM_TBL;
             L_error_message rtk_errors.rtk_text%type;
           BEGIN
             OPEN c_items;
             LOOP
               FETCH c_items bulk collect INTO l_items limit 1000;
               IF l_items.count                                                 > 0 THEN
                 IF ITEM_APPROVAL_SQL.SET_ALC_ITEM_TYPE(L_error_message,l_items)=false THEN
                   raise_application_error(-20001,L_error_message);
                 END IF;
               END IF;
               EXIT
             WHEN c_items%notfound;
             END LOOP;
           END;
           

         commit;
      
      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /
      
      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}

ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"

LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"

# Check configuration file
checkCfg
chk_status=$?
if [[ ${chk_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${chk_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi

# Check the data file
for filename in ${inputFile}
do
    retCode=$(checkFile -f ${filename})
    if [[ ${retCode} -eq 2 ]]; then
       LOG_ERROR "Sqlloader data file : ${filename} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
       exit 1
    elif [[ ${retCode} -eq 3 ]]; then
       LOG_ERROR "Sqlloader data file : ${filename} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
       exit 1
    elif [[ ${retCode} -eq 4 ]]; then
       LOG_ERROR "Sqlloader data file : ${filename} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
       exit 1
    fi
done
CTLFILE_STYLE="${scriptDir}/dc_style.ctl"

# Fetching elc indicator from system option.
elc_ind=`${ORACLE_HOME}/bin/sqlplus -s ${connectStr}  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select elc_ind
from system_options;
exit;
EOF`
elc_status=$?

if [[ ${elc_status} -ne ${OK} ]]; then
   LOG_ERROR "Batch failed while fetching the elc_ind from system_options" "ELC_IND" "${elc_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   if [[ ${elc_ind} = "N" ]]; then
      CTLFILE_FASHION="${scriptDir}/dc_fashion_sku_tab_no_elc.ctl"
   else
      CTLFILE_FASHION="${scriptDir}/dc_fashion_sku.ctl"
   fi
fi

# Load the style file
LOAD_STYLE_FILE
ld_styl_status=$?
if [[ ${ld_styl_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the style file" "LOAD_STYLE_FILE" "${ld_styl_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
fi

# Load the fashion file
LOAD_FASHION_FILE
ld_fsh_status=$?
if [[ ${ld_fsh_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the fashion file" "LOAD_FASHION_FILE" "${ld_fsh_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
fi


LOAD_STYLE_SKU
ld_sty_status=$?
if [[ ${ld_sty_status} -ne ${OK} ]]; then
   LOG_ERROR "Load function get failed during loading from dc table to main table" "LOAD_STYLE_SKU" "${ld_sty_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_sty_status}
else
   LOG_MESSAGE "Load Function completed successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

for filename in ${inputFile}
do
  # Move the data file to the processed directory
  if [[ -f "${dataDir}/${filename}" ]]; then
     mv -f ${dataDir}/${filename} ${dataCompDir}/. > /dev/null 2>&1
  fi
done
LOG_MESSAGE "Job ${pgmName}.ksh completed successfully by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"