#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_pack_xref.ksh
# Desc: Load the flat file data to staging table through sqlloader and from staging to main table.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_pack_xref.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_pack_xref.ctl"
inputFile='dc_pack_xref.dat'
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_FILE
# Purpose : Load data from .dat to staging table(using sqlloader).
#----------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = "" 
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${OK}
      fi
   fi
}
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_PACK_XREF
# Purpose : load data from staging table to main table.
#----------------------------------------------------------------------------------------------------
function LOAD_PACK_XREF
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000
     
      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);
      
      EXEC :GV_return_code := 0;
      
      WHENEVER SQLERROR EXIT 1
      
      DECLARE
         -- This script loads the reference pack data into RMS. 

         -- default attributes for orderable packs (not in data file)
         L_item_level                  ITEM_MASTER.ITEM_LEVEL%TYPE := 2;                    
         L_tran_level                  ITEM_MASTER.TRAN_LEVEL%TYPE := 1;                    
         L_status                      ITEM_MASTER.STATUS%TYPE := 'A';

         L_create_datetime             ITEM_MASTER.CREATE_DATETIME%TYPE := sysdate;               
         L_last_update_id              ITEM_MASTER.LAST_UPDATE_ID%TYPE := user;                
         L_last_update_datetime        ITEM_MASTER.LAST_UPDATE_DATETIME%TYPE := sysdate;          

      BEGIN
         -- insert ref items of both orderable and sellable packs
         -- many attributes of the ref item are inherited from its parent item
         insert into ITEM_MASTER(item,
                                 item_number_type,
                                 format_id,
                                 prefix,
                                 item_parent, 
                                 item_level,
                                 tran_level,
                                 item_aggregate_ind,
                                 diff_1,
                                 diff_2,
                                 diff_3,
                                 diff_4,
                                 diff_1_aggregate_ind,
                                 diff_2_aggregate_ind,
                                 diff_3_aggregate_ind,
                                 diff_4_aggregate_ind,
                                 dept,
                                 class,
                                 subclass,
                                 status,
                                 item_desc,
                                 short_desc,
                                 desc_up,
                                 item_desc_secondary,
                                 cost_zone_group_id,
                                 store_ord_mult,
                                 standard_uom,
                                 primary_ref_item_ind,
                                 package_size,
                                 package_uom,
                                 mfg_rec_retail,
                                 retail_label_type,
                                 retail_label_value,
                                 handling_temp,
                                 handling_sensitivity,
                                 order_type, 
                                 sale_type,
                                 pack_ind,
                                 simple_pack_ind,
                                 contains_inner_ind,
                                 sellable_ind,
                                 orderable_ind,
                                 pack_type,
                                 order_as_type,
                                 catch_weight_ind,
                                 item_xform_ind,
                                 inventory_ind,
                                 merchandise_ind,
                                 forecast_ind,
                                 const_dimen_ind,
                                 gift_wrap_ind,
                                 ship_alone_ind,
                                 create_datetime,
                                 last_update_id,
                                 last_update_datetime,
                                 comments,
                                 perishable_ind,
                                 notional_pack_ind,
                                 soh_inquiry_at_pack_ind,
                                 product_classification,
                                 brand_name)
                          select x.item,
                                 x.item_number_type,
                                 x.format_id,
                                 x.prefix,
                                 x.item_parent,
                                 L_item_level,
                                 L_tran_level,
                                 i.item_aggregate_ind,
                                 i.diff_1,
                                 i.diff_2,
                                 i.diff_3,
                                 i.diff_4,
                                 i.diff_1_aggregate_ind,
                                 i.diff_2_aggregate_ind,
                                 i.diff_3_aggregate_ind,
                                 i.diff_4_aggregate_ind,
                                 i.dept,
                                 i.class,
                                 i.subclass,
                                 L_status,
                                 x.item_desc,
                                 nvl(x.short_desc,RTRIM(substrb(x.item_desc,1,120))),
                                 upper(x.item_desc),
                                 x.item_desc_secondary,
                                 i.cost_zone_group_id,
                                 i.store_ord_mult,
                                 i.standard_uom,
                                 nvl(x.primary_ref_item_ind, 'N'),
                                 i.package_size,
                                 i.package_uom,
                                 i.mfg_rec_retail,
                                 i.retail_label_type,
                                 i.retail_label_value,
                                 i.handling_temp,
                                 i.handling_sensitivity,
                                 i.order_type,
                                 i.sale_type,
                                 i.pack_ind,
                                 i.simple_pack_ind,
                                 i.contains_inner_ind,
                                 i.sellable_ind,
                                 i.orderable_ind,
                                 i.pack_type,
                                 i.order_as_type,
                                 i.catch_weight_ind,
                                 i.item_xform_ind,
                                 i.inventory_ind,
                                 i.merchandise_ind,
                                 i.forecast_ind,
                                 i.const_dimen_ind,
                                 i.gift_wrap_ind,
                                 i.ship_alone_ind,
                                 L_create_datetime,
                                 L_last_update_id,
                                 L_last_update_datetime,
                                 x.comments,
                                 nvl(x.perishable_ind,'N'),
                                 nvl(x.notional_pack_ind,'N'),
                                 nvl(x.soh_inquiry_at_pack_ind,'N'),
                                 i.product_classification,
                                 i.brand_name
                            from dc_pack_xref x,
                                 item_master i 
                           where x.item_parent = i.item;  
                           
           DECLARE
             CURSOR c_items
             IS
               SELECT item FROM DC_PACK_XREF;
             l_items ITEM_TBL;
             L_error_message rtk_errors.rtk_text%type;
           BEGIN
             OPEN c_items;
             LOOP
               FETCH c_items bulk collect INTO l_items limit 1000;
               IF l_items.count                                                 > 0 THEN
                 IF ITEM_APPROVAL_SQL.SET_ALC_ITEM_TYPE(L_error_message,l_items)=false THEN
                   raise_application_error(-20001,L_error_message);
                 END IF;
               END IF;
               EXIT
             WHEN c_items%notfound;
             END LOOP;
           END;
           

         commit;
      
      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /
      
      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}.${pgmPID}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

# Check the data file
retCode=$(checkFile -f $inputFile)
if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Load the file using sqlloader
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_status}
else
   LOG_MESSAGE "Sqlloader function completed successfully." "LOAD_FILE" ${OK} ${logFile} ${pgmName} ${pgmPID}
    
   LOAD_PACK_XREF
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data from dc_pack_xref to main tables." "LOAD_PACK_XREF" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_PACK_XREF" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}