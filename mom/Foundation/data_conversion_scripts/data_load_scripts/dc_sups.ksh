#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_sups.ksh
# Desc: Load the flat file data to staging table through sqlloader and from staging to main table .

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_sups.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_sups.ctl"
inputFile='dc_sups.dat'
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_FILE
# Purpose : Load data from .dat to staging table(using sqlloader).
#----------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = "" 
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${OK}
      fi
   fi
}
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_SUPS
# Purpose : load data from staging table to main table.
#----------------------------------------------------------------------------------------------------
function LOAD_SUPS
{
   echo "set feedback off;
      set heading off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE
         L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
         L_system_options_row   SYSTEM_OPTIONS%ROWTYPE; 

         ERROR_IN_FUNCTION EXCEPTION;

      BEGIN
         if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                                  L_system_options_row) = FALSE then
            :GV_script_error := L_error_message;
            raise ERROR_IN_FUNCTION;
         end if;
         insert into sups(supplier,
                          sup_name,
                          sup_name_secondary,
                          contact_name,
                          contact_phone,
                          contact_fax,
                          contact_pager,
                          sup_status,
                          qc_ind,
                          qc_pct,
                          qc_freq,
                          vc_ind,
                          vc_pct,
                          vc_freq,
                          currency_code,
                          lang,
                          terms,
                          freight_terms,
                          ret_allow_ind,
                          ret_auth_req,
                          ret_min_dol_amt,
                          ret_courier,
                          handling_pct,
                          edi_po_ind,
                          edi_po_chg,
                          edi_po_confirm,
                          edi_asn,
                          edi_sales_rpt_freq,
                          edi_supp_available_ind,
                          edi_contract_ind,
                          edi_invc_ind,
                          edi_channel_id,
                          replen_approval_ind,
                          ship_method,
                          payment_method,
                          contact_telex,
                          contact_email,
                          settlement_code,
                          pre_mark_ind,
                          auto_appr_invc_ind,
                          dbt_memo_code,
                          freight_charge_ind,
                          auto_appr_dbt_memo_ind,
                          prepay_invc_ind,
                          backorder_ind,
                          vat_region,
                          inv_mgmt_lvl,
                          service_perf_req_ind,
                          invc_pay_loc,
                          invc_receive_loc,
                          addinvc_gross_net,
                          delivery_policy,
                          comment_desc,
                          default_item_lead_time,
                          duns_number,
                          duns_loc,
                          bracket_costing_ind,
                          vmi_order_status,
                          dsd_ind,
                          supplier_parent,
                          sup_qty_level)
                   select supplier,
                          sup_name,
                          sup_name_secondary,
                          contact_name,
                          contact_phone,
                          contact_fax,
                          contact_pager,
                          'A', -- SUP_STATUS
                          qc_ind,
                          qc_pct,
                          qc_freq,
                          vc_ind,
                          vc_pct,
                          vc_freq,
                          currency_code,
                          lang,
                          terms,
                          freight_terms,
                          ret_allow_ind,
                          ret_auth_req,
                          ret_min_dol_amt,
                          ret_courier,
                          handling_pct,
                          edi_po_ind,
                          edi_po_chg,
                          edi_po_confirm,
                          edi_asn,
                          edi_sales_rpt_freq,
                          edi_supp_available_ind,
                          edi_contract_ind,
                          'Y', -- EDI_INVC_IND
                          edi_channel_id,
                          replen_approval_ind,
                          ship_method,
                          payment_method,
                          contact_telex,
                          contact_email,
                          settlement_code,
                          pre_mark_ind,
                          auto_appr_invc_ind,
                          'Y', -- DBT_MEMO_CODE
                          freight_charge_ind,
                          'Y', -- AUTO_APPR_DBT_MEMO_IND
                          'Y', -- PREPAY_INVC_IND
                          backorder_ind,
                          decode(L_system_options_row.default_tax_type,'SVAT',nvl(vat_region,1000)),
                          inv_mgmt_lvl,
                          service_perf_req_ind,
                          'C', -- INVC_PAY_LOC
                          'C', -- INVC_RECEIVE_LOC
                          'G', -- ADDINVC_GROSS_NET
                          nvl(delivery_policy,'NEXT'),
                          comment_desc,
                          default_item_lead_time,
                          duns_number,
                          duns_loc,
                          'N', -- BRACKET_COSTING_IND
                          vmi_order_status,
                          nvl(dsd_ind,'N'),
                          decode(L_system_options_row.supplier_sites_ind,'Y',supplier_parent,NULL),
                          sup_qty_level
                     from dc_sups;

         COMMIT;

      EXCEPTION
       when ERROR_IN_FUNCTION then
          ROLLBACK;
          :GV_return_code := 1;
       when OTHERS then
          ROLLBACK;
          :GV_script_error := SQLERRM;
          :GV_return_code := 1;
      END;
      /
   
      print :GV_script_error;
      exit  :GV_return_code;
   
      /" | sqlplus -s ${connectStr} >> ${logFile}
   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}.${pgmPID}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

# Check the data file
retCode=$(checkFile -f $inputFile)
if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Load the file using sqlloader
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_status}
else
   LOG_MESSAGE "Sqlloader function completed successfully." "LOAD_FILE" ${OK} ${logFile} ${pgmName} ${pgmPID}
   
   LOAD_SUPS
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data from dc_sups to main tables." "LOAD_SUPS" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_SUPS" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}