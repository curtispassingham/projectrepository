LOAD DATA
REPLACE
INTO TABLE DC_PACK_COMPONENT
WHEN 
    ( PACK_NO != BLANKS )
AND ( ITEM != BLANKS )
AND ( PACK_ITEM_QTY != BLANKS )
FIELDS TERMINATED BY '|'
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( PACK_NO
 ,ITEM
 ,PACK_ITEM_QTY)