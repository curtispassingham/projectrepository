LOAD DATA
REPLACE
INTO TABLE DC_RELATED_ITEM_DETAIL
WHEN  
    ( RELATIONSHIP_ID != BLANKS )
AND ( RELATED_ITEM != BLANKS )
FIELDS TERMINATED BY '|'
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( RELATIONSHIP_ID
 ,RELATED_ITEM
 ,PRIORITY
 ,START_DATE
 ,END_DATE)