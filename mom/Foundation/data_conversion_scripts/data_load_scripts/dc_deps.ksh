#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_deps.ksh
# Desc: Load the flat file data to staging table dc_deps through sqlloader and from staging(dc_deps) to main table(deps).

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_deps.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_deps.ctl"
inputFile='dc_deps.dat'
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_FILE
# Purpose : load department data from .dat to dc_deps table(using sqlloader).
#----------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion deps table" "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${OK}
      fi
   fi
}
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_DEPS
# Purpose : load department data from dc_deps to deps table.
#----------------------------------------------------------------------------------------------------
function LOAD_DEPS
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE
         L_merch_default    DC_MERCH_DEFAULTS%ROWTYPE;

         cursor C_GET_DEFAULTS is
            select *
              from dc_merch_defaults;

      BEGIN

         open C_GET_DEFAULTS;
         fetch C_GET_DEFAULTS into L_merch_default;
         close C_GET_DEFAULTS;

         insert into deps(dept,
                          dept_name,
                          buyer,
                          merch,
                          profit_calc_type,
                          purchase_type,
                          group_no,
                          bud_int,
                          bud_mkup,
                          total_market_amt,
                          markup_calc_type,
                          otb_calc_type,
                          max_avg_counter,
                          avg_tolerance_pct,
                          dept_vat_incl_ind)
                   select dept,
                          dept_name,
                          buyer,
                          merch,
                          nvl(profit_calc_type,L_merch_default.dept_profit_calc_type),
                          nvl(nvl(purchase_type,L_merch_default.dept_purchase_type),0),
                          group_no,
                          case when nvl(markup_calc_type, L_merch_default.dept_markup_calc_type) = 'C' then
                                  round(nvl(mrkup_pct, L_merch_default.dept_mrkup_pct) * 100 / (nvl(mrkup_pct, L_merch_default.dept_mrkup_pct) + 100),4)
                               else
                                  nvl(mrkup_pct,L_merch_default.dept_mrkup_pct)
                          end,
                          case when nvl(markup_calc_type, L_merch_default.dept_markup_calc_type) = 'R' then
                                  round(nvl(mrkup_pct, L_merch_default.dept_mrkup_pct) * 100 / (100 - nvl(mrkup_pct, L_merch_default.dept_mrkup_pct)),4)
                               else
                                  nvl(mrkup_pct,L_merch_default.dept_mrkup_pct)
                          end,
                          total_market_amt,
                          nvl(markup_calc_type, L_merch_default.dept_markup_calc_type),
                          nvl(otb_calc_type, L_merch_default.dept_otb_calc_type),
                          max_avg_counter,
                          avg_tolerance_pct,
                          nvl(dept_vat_incl_ind, L_merch_default.dept_vat_incl_ind)
                     from dc_deps;
         COMMIT;

         if L_merch_default.default_class = 'Y' then
            insert into class(dept,
                              class,
                              class_name,
                              class_vat_ind,
                              class_id)
                       select dept,
                              dept,
                              dept_name,
                              L_merch_default.class_vat_incl_ind,
                              class_id_sequence.nextval
                         from dc_deps;

            insert into subclass(dept,
                                 class,
                                 subclass,
                                 sub_name,
                                 subclass_id)
                          select dept,
                                 dept,
                                 dept,
                                 dept_name,
                                 subclass_id_sequence.nextval
                            from dc_deps;
            COMMIT;
         end if;

      EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

# Check the data file
retCode=$(checkFile -f $inputFile)
if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Load the file using sqlloader
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_status}
else
   LOG_MESSAGE "Sqlloader function completed successfully." "LOAD_FILE" ${OK} ${logFile} ${pgmName} ${pgmPID}

   # Load the data from dc_deps to base tables
   LOAD_DEPS
   ld_status=$?
   if [[ ${ld_status} -ne 0 ]]; then
      LOG_ERROR "Function failed while loading data from dc_deps to main tables." "LOAD_DEPS" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_DEPS" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}
