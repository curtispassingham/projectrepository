#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_stock_ledger_ins.ksh
# Desc: Perform the Stockledger Inserts of merchandise hierarchy.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_stock_ledger_ins.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_STOCK_LEDGER_INS
# Purpose : Insert stock ledger inserts data for merchandise hierarchy.
#----------------------------------------------------------------------------------------------------
function LOAD_STOCK_LEDGER_INS
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1
      DECLARE
         L_rms_async_id STOCK_LEDGER_INSERTS.RMS_ASYNC_ID%TYPE;
         FUNCTION_ERROR    EXCEPTION;
         O_key             VARCHAR2(255);
      BEGIN
         L_rms_async_id := RMS_ASYNC_ID_SEQ.nextval;
         insert into stock_ledger_inserts(type_code,
                                          dept,
                                          class,
                                          subclass,
                                          location,
                                          rms_async_id)
                                   select 'D',
                                          dept,
                                          NULL,
                                          NULL,
                                          NULL,
                                          L_rms_async_id
                                     from dc_deps;

         insert into stock_ledger_inserts(type_code,
                                          dept,
                                          class,
                                          subclass,
                                          location,
                                          rms_async_id)
                                   select 'B',
                                          sc.dept,
                                          sc.class,
                                          sc.subclass,
                                          NULL,
                                          L_rms_async_id
                                     from subclass sc,
                                          dc_deps dd
                                    where sc.dept = dd.dept;
                                    
         IF CORESVC_STKLEDGR_INSERTS_SQL.PROCESS_STKLEDGR_INSERTS(:GV_script_error,L_rms_async_id)=FALSE
         THEN
            raise FUNCTION_ERROR;
         END IF;

         COMMIT;
      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := 255;
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

# Load the data of stockledger inserts 
   LOAD_STOCK_LEDGER_INS
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data of stockledger inserts table " "LOAD_STOCK_LEDGER_INS" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_STOCK_LEDGER_INS" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}