LOAD DATA
REPLACE
INTO TABLE DC_ITEM_SUPPLIER
WHEN 
    ( ITEM != BLANKS )
AND ( SUPPLIER != BLANKS )
FIELDS TERMINATED BY '|'
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( ITEM             
 ,SUPPLIER         
 ,PALLET_NAME      
 ,CASE_NAME        
 ,INNER_NAME       
 ,DIRECT_SHIP_IND  
 ,VPN              
 ,CONCESSION_RATE  
 ,SUPP_LABEL       
 ,CONSIGNMENT_RATE 
 ,PRIMARY_SUPP_IND )