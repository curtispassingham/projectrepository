#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_store_add.ksh
# Desc: Load the flat file data to staging table through sqlloader and from staging to main table .

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_store_add.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_store_add.ctl"
inputFile='dc_store_add.dat'
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_FILE
# Purpose : Load data from .dat to staging table(using sqlloader).
#----------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = "" 
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${OK}
      fi
   fi
}
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_STORE_ADD
# Purpose : load data from staging table to main table.
#----------------------------------------------------------------------------------------------------
function LOAD_STORE_ADD
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1
      DECLARE
         L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
         L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
         L_rms_async_id STORE_ADD.RMS_ASYNC_ID%TYPE;
         O_error_message RTK_ERRORS.RTK_TEXT%TYPE;
         enqueue_exception EXCEPTION;
         PRAGMA EXCEPTION_INIT( enqueue_exception, -20001 );
         ERROR_IN_FUNCTION EXCEPTION;
      BEGIN
         if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                                  L_system_options_row) = FALSE then
            :GV_script_error := L_error_message;
             raise ERROR_IN_FUNCTION;
         end if;
         L_rms_async_id := RMS_ASYNC_ID_SEQ.nextval;
         insert into store_add(store,
                               store_name,
                               store_name10,
                               store_name3,
                               store_name_secondary,
                               store_class,
                               store_mgr_name,
                               store_open_date,
                               store_close_date,
                               acquired_date,
                               remodel_date,
                               fax_number,
                               phone_number,
                               email,
                               total_square_ft,
                               selling_square_ft,
                               linear_distance,
                               vat_region,
                               vat_include_ind,
                               stockholding_ind,
                               channel_id,
                               store_format,
                               mall_name,
                               district,
                               transfer_zone,
                               default_wh,
                               stop_order_days,
                               start_order_days,
                               currency_code,
                               lang,
                               copy_repl_ind,
                               like_store,
                               price_store,
                               cost_location,
                               tran_no_generated,
                               integrated_pos_ind,
                               copy_activity_ind,
                               copy_dlvry_ind,
                               duns_number,
                               duns_loc,
                               sister_store,
                               tsf_entity_id,
                               org_unit_id,
                               store_type,
                               wf_customer_id,
                               auto_rcv,
                               timezone_name,
                               customer_order_loc_ind,
                               rms_async_id,
                               process_mode,
                               process_status)
                        select store,
                               store_name,
                               store_name10,
                               store_name3,
                               store_name_secondary,
                               store_class,
                               store_mgr_name,
                               store_open_date,
                               store_close_date,
                               acquired_date,
                               remodel_date,
                               fax_number,
                               phone_number,
                               email,
                               total_square_ft,
                               selling_square_ft,
                               linear_distance,
                               decode(L_system_options_row.default_tax_type,'SVAT',nvl(vat_region,1000)),
                               NVL(vat_include_ind, 'N'),
                               NVL(stockholding_ind, 'Y'),
                               channel_id,
                               store_format,
                               mall_name,
                               district,
                               transfer_zone,
                               default_wh,
                               stop_order_days,
                               start_order_days,
                               currency_code,
                               lang,
                               NVL(copy_repl_ind, 'N'),
                               NULL,
                               NULL,
                               NULL,
                               tran_no_generated,
                               integrated_pos_ind,
                               NVL(copy_activity_ind, 'N'),
                               NVL(copy_dlvry_ind, 'N'),
                               duns_number,
                               duns_loc,
                               sister_store,
                               tsf_entity_id,
                               org_unit_id, --org_unit_id
                               NVL(store_type, 'C'),
                               wf_customer_id,
                               DECODE(NVL(store_type, 'C'), 'C', 'D','F','D','N'),
                               timezone_name,
                               DECODE(store_type,'C', NVL(customer_order_loc_ind,'N'), 'F', DECODE(STOCKHOLDING_IND, 'Y', NVL(customer_order_loc_ind,'N'), null)),
                               L_rms_async_id,
                               NVL(process_mode,'ASYNC'),
                               NVL(process_status,'00NEW')
                          from dc_store_add;
                          

         COMMIT;

      EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}.${pgmPID}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

# Check the data file
retCode=$(checkFile -f $inputFile)
if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Load the file using sqlloader
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_status}
else
   LOG_MESSAGE "Sqlloader function completed successfully." "LOAD_FILE" ${OK} ${logFile} ${pgmName} ${pgmPID}
   
   LOAD_STORE_ADD
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data from dc_store_add to main tables." "LOAD_STORE_ADD" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_STORE_ADD" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}