#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_product_line.ksh
# Desc: Load the flat file data to staging table dc_product_line through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_product_line.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE_PRODUCT_LINE="${scriptDir}/dc_product_line.ctl"
CTLFILE_PRODUCT="${scriptDir}/dc_product.ctl"

inputFile_product_line='dc_product_line.dat';
inputFile_product='dc_product.dat';

inputFile="dc_product_line.dat
           dc_product.dat"
#--------------------------------------------------------------------------------------------------
function LOAD_PRODUCT_LINE_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile_product_line%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE_PRODUCT_LINE} log=${sqlldrLog} data=${data_dir}/${inputFile_product_line} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile_product_line}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
        LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
         return ${OK}
      fi
   fi
}
#--------------------------------------------------------------------------------------------------
function LOAD_PRODUCT_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile_product%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE_PRODUCT} log=${sqlldrLog} data=${data_dir}/${inputFile_product} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile_product}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
         return ${OK}
      fi
   fi
}

function LOAD_PRODUCT_LINE
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000
     
      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);
      
      EXEC :GV_return_code := 0;
      
      WHENEVER SQLERROR EXIT 1

      DECLARE
         -- default attributes for product line
         L_item_number_type            ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE := 'ITEM';              
         L_item_level                  ITEM_MASTER.ITEM_LEVEL%TYPE := 1;
         L_tran_level                  ITEM_MASTER.TRAN_LEVEL%TYPE := 2;
         L_item_aggregate_ind          ITEM_MASTER.ITEM_AGGREGATE_IND%TYPE := 'N';
         L_diff_1_aggregate_ind        ITEM_MASTER.DIFF_1_AGGREGATE_IND%TYPE := 'N';
         L_diff_2_aggregate_ind        ITEM_MASTER.DIFF_2_AGGREGATE_IND%TYPE := 'N';
         L_diff_3_aggregate_ind        ITEM_MASTER.DIFF_3_AGGREGATE_IND%TYPE := 'N';
         L_diff_4_aggregate_ind        ITEM_MASTER.DIFF_4_AGGREGATE_IND%TYPE := 'N';
         L_status                      ITEM_MASTER.STATUS%TYPE := 'A';

         L_primary_ref_item_ind        ITEM_MASTER.PRIMARY_REF_ITEM_IND%TYPE := 'N';
         L_pack_ind                    ITEM_MASTER.PACK_IND%TYPE := 'N';
         L_simple_pack_ind             ITEM_MASTER.SIMPLE_PACK_IND%TYPE := 'N';
         L_contains_inner_ind          ITEM_MASTER.CONTAINS_INNER_IND%TYPE := 'N';
         L_item_xform_ind              ITEM_MASTER.ITEM_XFORM_IND%TYPE := 'N';
         L_inventory_ind               ITEM_MASTER.INVENTORY_IND%TYPE := 'Y';  
         L_sellable_ind                ITEM_MASTER.SELLABLE_IND%TYPE := 'Y';                  
         L_orderable_ind               ITEM_MASTER.ORDERABLE_IND%TYPE := 'Y';
         L_const_dimen_ind             ITEM_MASTER.CONST_DIMEN_IND%TYPE := 'N';
         L_gift_wrap_ind               ITEM_MASTER.GIFT_WRAP_IND%TYPE := 'N'; 
         L_ship_alone_ind              ITEM_MASTER.SHIP_ALONE_IND%TYPE := 'N';
         L_perishable_ind              ITEM_MASTER.PERISHABLE_IND%TYPE := 'N';
         L_create_datetime             ITEM_MASTER.CREATE_DATETIME%TYPE := sysdate;
         L_last_update_id              ITEM_MASTER.LAST_UPDATE_ID%TYPE := user;
         L_last_update_datetime        ITEM_MASTER.LAST_UPDATE_DATETIME%TYPE := sysdate;
         
         -- The following attributes are defaulted from product (dc_product external table):
         -- cost_zone_group_id
         -- uom_conv_factor
         -- standard_uom
         -- store_ord_mult, plus:
         L_merchandise_ind             ITEM_MASTER.MERCHANDISE_IND%TYPE := 'Y';
         L_forecast_ind                ITEM_MASTER.FORECAST_IND%TYPE := 'Y';
         L_catch_weight_ind            ITEM_MASTER.CATCH_WEIGHT_IND%TYPE := 'N';

      BEGIN
      
         insert into ITEM_MASTER (item,
                                  item_number_type,
                                  item_level,
                                  tran_level,
                                  item_aggregate_ind,
                                  diff_1,
                                  diff_2,
                                  diff_3,
                                  diff_4,
                                  diff_1_aggregate_ind,
                                  diff_2_aggregate_ind,
                                  diff_3_aggregate_ind,
                                  diff_4_aggregate_ind,
                                  dept,
                                  class,
                                  subclass,
                                  cost_zone_group_id,
                                  status,
                                  item_desc,
                                  short_desc,
                                  desc_up,
                                  item_desc_secondary,
                                  store_ord_mult,
                                  standard_uom,
                                  uom_conv_factor,
                                  primary_ref_item_ind,
                                  pack_ind,
                                  simple_pack_ind,
                                  contains_inner_ind,
                                  item_xform_ind,
                                  inventory_ind,
                                  sellable_ind,
                                  orderable_ind,
                                  merchandise_ind,
                                  forecast_ind,
                                  catch_weight_ind,
                                  const_dimen_ind,
                                  gift_wrap_ind,
                                  ship_alone_ind,
                                  create_datetime,
                                  last_update_id,
                                  last_update_datetime,
                                  aip_case_type,
                                  comments,
                                  perishable_ind,
                                  product_classification,
                                  brand_name)
                           select pl.item,
                                  L_item_number_type,
                                  L_item_level,
                                  L_tran_level,
                                  nvl(pl.item_aggregate_ind, L_item_aggregate_ind),
                                  -- fill in diff1, diff2, diff3, diff4
                                  coalesce(pl.diff_1,pl.diff_2,pl.diff_3,pl.diff_4) diff1,
                                  case
                                  when nvl(pl.diff_1,'-1') != '-1' then    -- diff1 is not null, diff1<-diff1
                                     coalesce(pl.diff_2,pl.diff_3,pl.diff_4)
                                  when nvl(pl.diff_2,'-1') != '-1' then    -- diff1 is NULL, but diff2 is not null, diff1<-diff2
                                     nvl(pl.diff_3,pl.diff_4)
                                  when nvl(pl.diff_3,'-1') != '-1' then     -- diff1 is null and diff2 is null, but diff3 is not null, diff1<-diff3
                                     pl.diff_4
                                  end diff2,
                                  case
                                  when (nvl(pl.diff_1,'-1') != '-1' and nvl(pl.diff_2,'-1') != '-1') then
                                     nvl(pl.diff_3,pl.diff_4)
                                  when (nvl(pl.diff_1,'-1') != '-1' and nvl(pl.diff_3,'-1') != '-1') then
                                     pl.diff_4
                                  when (nvl(pl.diff_2,'-1') != '-1' and nvl(pl.diff_3,'-1') != '-1') then
                                     pl.diff_4
                                  end diff3,
                                  case
                                  when (nvl(pl.diff_1,'-1') != '-1' and nvl(pl.diff_2,'-1') != '-1'
                                        and nvl(pl.diff_3,'-1') != '-1') then
                                     pl.diff_4
                                  end diff4,
                                  -- fill in aggregate_ind for diff1-4
                                  coalesce(pl.diff_1_aggregate_ind,
                                           pl.diff_2_aggregate_ind,
                                           pl.diff_3_aggregate_ind,
                                           pl.diff_4_aggregate_ind,
                                           L_diff_1_aggregate_ind) diff1_aggr,
                                  case
                                  when nvl(pl.diff_1_aggregate_ind,'-1') != '-1' then
                                     coalesce(pl.diff_2_aggregate_ind,
                                              pl.diff_3_aggregate_ind,
                                              pl.diff_4_aggregate_ind, 
                                              L_diff_2_aggregate_ind)
                                  when nvl(pl.diff_2_aggregate_ind,'-1') != '-1' then
                                     coalesce(pl.diff_3_aggregate_ind, 
                                              pl.diff_4_aggregate_ind, 
                                              L_diff_2_aggregate_ind)
                                  when nvl(pl.diff_3_aggregate_ind,'-1') != '-1' then
                                     nvl(pl.diff_4_aggregate_ind, L_diff_2_aggregate_ind)
                                  else
                                     L_diff_2_aggregate_ind
                                  end diff2_aggr,
                                  case
                                  when (nvl(pl.diff_1_aggregate_ind,'-1') != '-1' and nvl(pl.diff_2_aggregate_ind,'-1') != '-1') then
                                     coalesce(pl.diff_3_aggregate_ind,
                                              pl.diff_4_aggregate_ind, 
                                              L_diff_3_aggregate_ind)
                                  when (nvl(pl.diff_1_aggregate_ind,'-1') != '-1' and nvl(pl.diff_3_aggregate_ind,'-1') != '-1') then
                                     nvl(pl.diff_4_aggregate_ind, L_diff_3_aggregate_ind)
                                  when (nvl(pl.diff_2_aggregate_ind,'-1') != '-1' and nvl(pl.diff_3_aggregate_ind,'-1') != '-1') then
                                     nvl(pl.diff_4_aggregate_ind, L_diff_3_aggregate_ind)
                                  else
                                     L_diff_3_aggregate_ind
                                  end diff3_aggr,
                                  case
                                  when (nvl(pl.diff_1_aggregate_ind,'-1') != '-1' and nvl(pl.diff_2_aggregate_ind,'-1') != '-1'
                                        and nvl(pl.diff_3_aggregate_ind,'-1') != '-1') then
                                     nvl(pl.diff_4_aggregate_ind, L_diff_4_aggregate_ind)
                                  else
                                     L_diff_4_aggregate_ind
                                  end diff4_aggr,
                                  pl.dept,
                                  pl.class,
                                  pl.subclass,
                                  p.cost_zone_group_id,
                                  L_status,
                                  pl.item_desc,
                                  nvl(pl.short_desc,RTRIM(substrb(pl.item_desc,1,120))),
                                  upper(pl.item_desc),
                                  pl.item_desc_secondary,
                                  p.store_ord_mult,
                                  p.standard_uom,
                                  p.uom_conv_factor,
                                  L_primary_ref_item_ind,
                                  L_pack_ind,
                                  L_simple_pack_ind,
                                  L_contains_inner_ind,
                                  L_item_xform_ind,
                                  L_inventory_ind,
                                  L_sellable_ind,
                                  L_orderable_ind,
                                  nvl(p.merchandise_ind, L_merchandise_ind),
                                  nvl(p.forecast_ind, L_forecast_ind),
                                  nvl(p.catch_weight_ind, L_catch_weight_ind),
                                  L_const_dimen_ind,
                                  L_gift_wrap_ind,
                                  L_ship_alone_ind,
                                  L_create_datetime,
                                  L_last_update_id,
                                  L_last_update_datetime,
                                  pl.aip_case_type,
                                  pl.comments,
                                  NVL(p.perishable_ind,L_perishable_ind),
                                  pl.product_classification,
                                  pl.brand_name
                             from dc_product p,
                                  dc_product_line pl
                            where p.item_parent = pl.item
                              and p.primary_product_ind = 'Y';
                              
           DECLARE
             CURSOR c_items
             IS
               SELECT item FROM dc_product_line;
             l_items ITEM_TBL;
             L_error_message rtk_errors.rtk_text%type;
           BEGIN
             OPEN c_items;
             LOOP
               FETCH c_items bulk collect INTO l_items limit 1000;
               IF l_items.count                                                 > 0 THEN
                 IF ITEM_APPROVAL_SQL.SET_ALC_ITEM_TYPE(L_error_message,l_items)=false THEN
                   raise_application_error(-20001,L_error_message);
                 END IF;
               END IF;
               EXIT
             WHEN c_items%notfound;
             END LOOP;
           END;

         commit;

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}

ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"

LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"

# Check configuration file
checkCfg
chk_status=$?
if [[ ${chk_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${chk_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${LOGFILE}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi

# Check the data file
for filename in ${inputFile}
do
    retCode=$(checkFile -f ${filename})
    if [[ ${retCode} -eq 2 ]]; then
       LOG_ERROR "Sqlloader data file : ${filename} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
       exit 1
    elif [[ ${retCode} -eq 3 ]]; then
       LOG_ERROR "Sqlloader data file : ${filename} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
       exit 1
    elif [[ ${retCode} -eq 4 ]]; then
       LOG_ERROR "Sqlloader data file : ${filename} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
       exit 1
    fi
done

# Load the product line file
LOAD_PRODUCT_LINE_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the product line file" "LOAD_PRODUCT_LINE_FILE" "${ld_status}" "${ERRORFILE}" "${LOGFILE}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
fi

# Load the product file
LOAD_PRODUCT_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the product file" "LOAD_PRODUCT_FILE" "${ld_status}" "${ERRORFILE}" "${LOGFILE}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
fi

LOAD_PRODUCT_LINE
ld_prd_status=$?
if [[ ${ld_prd_status} -ne ${OK} ]]; then
   LOG_ERROR "Load function get failed during loading from dc table to main table" "LOAD_PRODUCT_LINE" "${ld_prd_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_prd_status}
else
   LOG_MESSAGE "Load Function completed successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

for filename in ${inputFile}
do
  # Move the data file to the processed directory
  if [[ -f "${dataDir}/${filename}" ]]; then
     mv -f ${dataDir}/${filename} ${dataCompDir}/. > /dev/null 2>&1
  fi
done
LOG_MESSAGE "Job ${pgmName}.ksh completed successfully by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"