#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_orderable_pack.ksh
# Desc: Load the flat file data to staging table through sqlloader and from staging to main table.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_orderable_pack.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_orderable_pack.ctl"
inputFile='dc_orderable_pack.dat'
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_FILE
# Purpose : Load data from .dat to staging table(using sqlloader).
#----------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = "" 
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${OK}
      fi
   fi
}
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_ORDERABLE_PACK
# Purpose : load data from staging table to main table.
#----------------------------------------------------------------------------------------------------
function LOAD_ORDERABLE_PACK
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000
     
      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);
      
      EXEC :GV_return_code := 0;
      
      WHENEVER SQLERROR EXIT 1
      
      DECLARE
         -- This function loads the orderable pack data into RMS. The orderable pack dataset 
         -- will contain all orderable packs that are either sellable or non-sellable. These 
         -- packs can be simple packs or complex packs in RMS.
      
         -- default attributes for orderable packs (not in data file) 
         L_item_number_type            ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE := 'ITEM';              
         L_item_level                  ITEM_MASTER.ITEM_LEVEL%TYPE := 1;                    
         L_tran_level                  ITEM_MASTER.TRAN_LEVEL%TYPE := 1;                    
         L_item_aggregate_ind          ITEM_MASTER.ITEM_AGGREGATE_IND%TYPE := 'N';            
         L_diff_1_aggregate_ind        ITEM_MASTER.DIFF_1_AGGREGATE_IND%TYPE := 'N';          
         L_diff_2_aggregate_ind        ITEM_MASTER.DIFF_2_AGGREGATE_IND%TYPE := 'N';          
         L_diff_3_aggregate_ind        ITEM_MASTER.DIFF_3_AGGREGATE_IND%TYPE := 'N';          
         L_diff_4_aggregate_ind        ITEM_MASTER.DIFF_4_AGGREGATE_IND%TYPE := 'N';          
         L_status                      ITEM_MASTER.STATUS%TYPE := 'A';

         L_primary_ref_item_ind        ITEM_MASTER.PRIMARY_REF_ITEM_IND%TYPE := 'N';
         L_orderable_ind               ITEM_MASTER.ORDERABLE_IND%TYPE := 'Y';                                   
         L_inventory_ind               ITEM_MASTER.INVENTORY_IND%TYPE := 'Y';  
         L_merchandise_ind             ITEM_MASTER.MERCHANDISE_IND%TYPE := 'Y';
         L_item_xform_ind              ITEM_MASTER.ITEM_XFORM_IND%TYPE := 'N';                
         L_pack_ind                    ITEM_MASTER.PACK_IND%TYPE := 'Y';                      
         L_contains_inner_ind          ITEM_MASTER.CONTAINS_INNER_IND%TYPE := 'N';            
         L_forecast_ind                ITEM_MASTER.FORECAST_IND%TYPE := 'N';                  
         L_standard_uom                ITEM_MASTER.STANDARD_UOM%TYPE := 'EA';

         L_const_dimen_ind             ITEM_MASTER.CONST_DIMEN_IND%TYPE := 'N';               
         L_gift_wrap_ind               ITEM_MASTER.GIFT_WRAP_IND%TYPE := 'N';                 
         L_ship_alone_ind              ITEM_MASTER.SHIP_ALONE_IND%TYPE := 'N';                

         L_create_datetime             ITEM_MASTER.CREATE_DATETIME%TYPE := sysdate;               
         L_last_update_id              ITEM_MASTER.LAST_UPDATE_ID%TYPE := user;                
         L_last_update_datetime        ITEM_MASTER.LAST_UPDATE_DATETIME%TYPE := sysdate;          

         -- conditional defaults
         L_store_ord_mult              ITEM_MASTER.STORE_ORD_MULT%TYPE := 'E';
         L_pack_type                   ITEM_MASTER.PACK_TYPE%TYPE := 'V'; -- default for simple pack

         L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;    

      BEGIN
 
         insert into ITEM_MASTER (item,
                                  item_number_type,
                                  item_level,
                                  tran_level,
                                  item_aggregate_ind,
                                  diff_1,
                                  diff_2,
                                  diff_3,
                                  diff_4,
                                  diff_1_aggregate_ind,
                                  diff_2_aggregate_ind,
                                  diff_3_aggregate_ind,
                                  diff_4_aggregate_ind,
                                  dept,
                                  class,
                                  subclass,
                                  status,
                                  item_desc,
                                  short_desc,
                                  desc_up,
                                  item_desc_secondary,
                                  cost_zone_group_id,
                                  store_ord_mult,
                                  standard_uom,
                                  primary_ref_item_ind,
                                  package_size,
                                  package_uom,
                                  mfg_rec_retail,
                                  retail_label_type,
                                  retail_label_value,
                                  handling_temp,
                                  handling_sensitivity,
                                  pack_ind,
                                  simple_pack_ind,
                                  contains_inner_ind,
                                  sellable_ind,
                                  orderable_ind,
                                  pack_type,
                                  order_as_type,
                                  item_xform_ind,
                                  inventory_ind,
                                  merchandise_ind,
                                  forecast_ind,
                                  const_dimen_ind,
                                  gift_wrap_ind,
                                  ship_alone_ind,
                                  catch_weight_ind,
                                  order_type,
                                  sale_type,
                                  create_datetime,
                                  last_update_id,
                                  last_update_datetime,
                                  comments,
                                  perishable_ind,
                                  notional_pack_ind,
                                  soh_inquiry_at_pack_ind, 
                                  product_classification,
                                  brand_name)
                           select item,
                                  L_item_number_type,
                                  L_item_level,
                                  L_tran_level,
                                  L_item_aggregate_ind,
                                  diff_1,
                                  diff_2,
                                  diff_3,
                                  diff_4,
                                  -- no diff aggregate at product level
                                  L_diff_1_aggregate_ind,
                                  L_diff_2_aggregate_ind,
                                  L_diff_3_aggregate_ind,
                                  L_diff_4_aggregate_ind,
                                  dept,
                                  class,
                                  subclass,
                                  L_status,
                                  item_desc,
                                  nvl(short_desc,RTRIM(substrb(item_desc,1,120))),
                                  upper(item_desc),
                                  item_desc_secondary,
                                  decode(pack_type, 'B', NULL, cost_zone_group_id), -- default to NULL if buyer pack
                                  nvl(store_ord_mult, L_store_ord_mult),  -- default to 'E'aches if NULL
                                  L_standard_uom,
                                  L_primary_ref_item_ind,
                                  package_size,
                                  package_uom,
                                  decode(sellable_ind, 'Y', mfg_rec_retail, NULL),
                                  decode(sellable_ind, 'Y', retail_label_type, NULL),
                                  decode(sellable_ind, 'Y', retail_label_value,NULL),
                                  handling_temp,
                                  handling_sensitivity,
                                  L_pack_ind,
                                  simple_pack_ind,
                                  L_contains_inner_ind,
                                  sellable_ind,
                                  L_orderable_ind,
                                  decode(simple_pack_ind, 'Y', L_pack_type, pack_type), -- default to 'V' for simple pack
                                  decode(pack_type, 'B', order_as_type, NULL), -- only valid for buyer pack
                                  L_item_xform_ind,
                                  L_inventory_ind,
                                  L_merchandise_ind,
                                  L_forecast_ind,
                                  L_const_dimen_ind,
                                  L_gift_wrap_ind,
                                  L_ship_alone_ind,
                                  catch_weight_ind,
                                  order_type,
                                  decode(sellable_ind, 'N', NULL, sale_type),
                                  L_create_datetime,
                                  L_last_update_id,
                                  L_last_update_datetime,
                                  comments,
                                  nvl(perishable_ind,'N'),
                                  nvl(notional_pack_ind,'N'),
                                  nvl(soh_inquiry_at_pack_ind,'N'), 
                                  product_classification,
                                  brand_name
                             from dc_orderable_pack;

           DECLARE
             CURSOR c_items
             IS
               SELECT item FROM DC_ORDERABLE_PACK;
             l_items ITEM_TBL;
             L_error_message rtk_errors.rtk_text%type;
           BEGIN
             OPEN c_items;
             LOOP
               FETCH c_items bulk collect INTO l_items limit 1000;
               IF l_items.count                                                 > 0 THEN
                 IF ITEM_APPROVAL_SQL.SET_ALC_ITEM_TYPE(L_error_message,l_items)=false THEN
                   raise_application_error(-20001,L_error_message);
                 END IF;
               END IF;
               EXIT
             WHEN c_items%notfound;
             END LOOP;
           END;
           

         commit;
      
      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /
      
      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}.${pgmPID}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

# Check the data file
retCode=$(checkFile -f $inputFile)
if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Load the file using sqlloader
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_status}
else
   LOG_MESSAGE "Sqlloader function completed successfully." "LOAD_FILE" ${OK} ${logFile} ${pgmName} ${pgmPID}
   
   LOAD_ORDERABLE_PACK
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data from dc_orderable_pack to main tables." "LOAD_ORDERABLE_PACK" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_ORDERABLE_PACK" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}