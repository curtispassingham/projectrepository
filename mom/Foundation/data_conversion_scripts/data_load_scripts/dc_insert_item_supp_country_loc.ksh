#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_insert_item_supp_country_loc.ksh
# Desc: Load the data from staging to main tables.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_insert_item_supp_country_loc.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#----------------------------------------------------------------------------------------------------
# Function Name : INSERT_ITEM_SUPP_COUNTRY_LOC
# Purpose : load data from data conversion table to base tables.
#----------------------------------------------------------------------------------------------------
function INSERT_ITEM_SUPP_COUNTRY_LOC
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE

         L_date DATE := get_vdate;
         L_default_tax_type     SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;

         cursor C_TAX_TYPE is
            select default_tax_type
              from system_options;
      BEGIN

         open C_TAX_TYPE;
         fetch C_TAX_TYPE into L_default_tax_type;
         close C_TAX_TYPE;

         -- for warehouses, only insert stockholding locations
         insert into item_supp_country_loc(ITEM,
                                           SUPPLIER,
                                           ORIGIN_COUNTRY_ID,
                                           LOC,
                                           LOC_TYPE,
                                           PRIMARY_LOC_IND,
                                           UNIT_COST,
                                           ROUND_LVL,
                                           ROUND_TO_INNER_PCT,
                                           ROUND_TO_CASE_PCT,
                                           ROUND_TO_LAYER_PCT,
                                           ROUND_TO_PALLET_PCT,
                                           SUPP_HIER_TYPE_1,
                                           SUPP_HIER_LVL_1,
                                           SUPP_HIER_TYPE_2,
                                           SUPP_HIER_LVL_2,
                                           SUPP_HIER_TYPE_3,
                                           SUPP_HIER_LVL_3,
                                           PICKUP_LEAD_TIME,
                                           CREATE_DATETIME,
                                           LAST_UPDATE_DATETIME,
                                           LAST_UPDATE_ID,
                                           NEGOTIATED_ITEM_COST,
                                           EXTENDED_BASE_COST,
                                           INCLUSIVE_COST,
                                           BASE_COST)
                                    select il.ITEM,
                                           isc.SUPPLIER,
                                           isc.ORIGIN_COUNTRY_ID,
                                           il.LOCATION,
                                           il.LOC_TYPE,
                                           NVL(il.PRIMARY_LOC_IND, decode(row_number() over(partition by il.ITEM, isc.SUPPLIER,
                                           isc.ORIGIN_COUNTRY_ID order by il.ITEM,isc.SUPPLIER,isc.ORIGIN_COUNTRY_ID,il.LOCATION),1,'Y',
                                               'N')),
                                           isc.UNIT_COST,
                                           isc.ROUND_LVL,
                                           isc.ROUND_TO_INNER_PCT,
                                           isc.ROUND_TO_CASE_PCT,
                                           isc.ROUND_TO_LAYER_PCT,
                                           isc.ROUND_TO_PALLET_PCT,
                                           isc.SUPP_HIER_TYPE_1,
                                           isc.SUPP_HIER_LVL_1,
                                           isc.SUPP_HIER_TYPE_2,
                                           isc.SUPP_HIER_LVL_2,
                                           isc.SUPP_HIER_TYPE_3,
                                           isc.SUPP_HIER_LVL_3,
                                           isc.PICKUP_LEAD_TIME,
                                           SYSDATE,
                                           SYSDATE,
                                           USER,
                                           DECODE(L_default_tax_type,'GTAX',isc.NEGOTIATED_ITEM_COST,NULL),
                                           DECODE(L_default_tax_type,'GTAX',isc.EXTENDED_BASE_COST,NULL),
                                           DECODE(L_default_tax_type,'GTAX',isc.INCLUSIVE_COST,NULL),
                                           DECODE(L_default_tax_type,'GTAX',isc.BASE_COST,NULL)
                                      from dc_item_loc il,
                                           item_supp_country isc,
                                           (select store loc
                                              from store
                                             union all
                                            select wh loc
                                              from wh
                                             where stockholding_ind = 'Y') loc
                                     where il.item = isc.item
                                       and il.location = loc.loc;
         commit;

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

   INSERT_ITEM_SUPP_COUNTRY_LOC
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data of RPM dept aggregation table." "INSERT_ITEM_SUPP_COUNTRY_LOC" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "INSERT_ITEM_SUPP_COUNTRY_LOC" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi


LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}