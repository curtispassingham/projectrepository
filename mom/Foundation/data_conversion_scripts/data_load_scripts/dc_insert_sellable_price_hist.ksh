#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_insert_sellable_price_hist.ksh
# Desc: Load the data from staging to main tables.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_insert_sellable_price_hist.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#----------------------------------------------------------------------------------------------------
# Function Name : INSERT_SELLABLE_PRICE_HIST
# Purpose : load data from data conversion table to base tables.
#----------------------------------------------------------------------------------------------------
function INSERT_SELLABLE_PRICE_HIST
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);
      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1
      --- Need to create the price_hist 0 record for sellable only packs
      --- Other packs are inserted during the item_supplier load script where unit_cost is set
      ---
      DECLARE

         L_vdate                 PERIOD.VDATE%TYPE := GET_VDATE;
         L_error_message         RTK_ERRORS.RTK_TEXT%TYPE;
         L_pack_item             ITEM_MASTER.ITEM%TYPE;
         L_pack_cost_prim        PRICE_HIST.UNIT_COST%TYPE;
         L_pack_retail_prim      PRICE_HIST.UNIT_RETAIL%TYPE;
         L_pack_selling_uom      PRICE_HIST.SELLING_UOM%TYPE := 'EA';

         ERROR_IN_FUNCTION     EXCEPTION;

         cursor C_SELLABLE_PACK is
            select item,
                   unit_retail
              from dc_sellable_pack;

      BEGIN
         FOR rec in C_SELLABLE_PACK LOOP
            L_pack_item := rec.item;

            if PACKITEM_ADD_SQL.BUILD_COMP_COST_RETAIL (L_error_message,
                                                        L_pack_cost_prim,
                                                        L_pack_retail_prim,
                                                        L_pack_item
                                                        ) = FALSE then
                :GV_script_error := L_error_message;
                raise ERROR_IN_FUNCTION;
            end if;

            L_pack_retail_prim := rec.unit_retail;
            insert into price_hist(tran_type,
                                   reason,
                                   item,
                                   loc,
                                   unit_cost,
                                   unit_retail,
                                   selling_unit_retail,
                                   selling_uom,
                                   multi_units,
                                   multi_unit_retail,
                                   multi_selling_uom,
                                   action_date,
                                   post_date)
                            values(0,
                                   0,
                                   L_pack_item,
                                   0,
                                   L_pack_cost_prim,
                                   L_pack_retail_prim,
                                   L_pack_retail_prim,
                                   L_pack_selling_uom,
                                   NULL,
                                   NULL,
                                   NULL,
                                   L_vdate,
                                   L_vdate);
         end LOOP;

         commit;

      EXCEPTION
         when ERROR_IN_FUNCTION then
            rollback;
            :GV_return_code := 1;
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

   INSERT_SELLABLE_PRICE_HIST
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data of RPM dept aggregation table." "INSERT_SELLABLE_PRICE_HIST" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "INSERT_SELLABLE_PRICE_HIST" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi


LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}