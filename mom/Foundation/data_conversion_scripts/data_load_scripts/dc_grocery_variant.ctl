LOAD DATA
REPLACE
INTO TABLE DC_GROCERY_VARIANT
WHEN  
    ( ITEM != BLANKS )
AND ( ITEM_NUMBER_TYPE != BLANKS )
AND ( ITEM_DESC != BLANKS )
AND ( ITEM_PARENT != BLANKS )
AND ( ITEM_GRANDPARENT != BLANKS )
FIELDS TERMINATED BY '|'
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( ITEM
 ,ITEM_NUMBER_TYPE
 ,FORMAT_ID
 ,PREFIX
 ,ITEM_DESC
 ,SHORT_DESC
 ,ITEM_DESC_SECONDARY
 ,ITEM_PARENT
 ,ITEM_GRANDPARENT
 ,PRIMARY_REF_ITEM_IND
 ,AIP_CASE_TYPE
 ,COMMENTS
 ,PRODUCT_CLASSIFICATION
 ,BRAND_NAME)