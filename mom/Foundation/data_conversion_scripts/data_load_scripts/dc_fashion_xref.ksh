#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_fashion_xref.ksh
# Desc: Load the flat file data to staging table dc_fashion_xref through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_fashion_xref.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_fashion_xref.ctl"

inputFile='dc_fashion_xref.dat'
#--------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
         return ${OK}
      fi
   fi
}
#---------------------------------------------------------------------------------------------------
function LOAD_XREF_ITEMS
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE
         -- Defaults not in external table
         L_status                      ITEM_MASTER.STATUS%TYPE := 'A';
         L_item_level                  ITEM_MASTER.ITEM_LEVEL%TYPE := 3;
         L_tran_level                  ITEM_MASTER.TRAN_LEVEL%TYPE := 2;
         L_sellable_ind                ITEM_MASTER.SELLABLE_IND%TYPE := 'Y';
         L_orderable_ind               ITEM_MASTER.ORDERABLE_IND%TYPE := 'Y';
         L_inventory_ind               ITEM_MASTER.INVENTORY_IND%TYPE := 'Y';

         L_primary_ref_item_ind        ITEM_MASTER.PRIMARY_REF_ITEM_IND%TYPE := 'N';
         L_pack_ind                    ITEM_MASTER.PACK_IND%TYPE := 'N';
         L_catch_weight_ind            ITEM_MASTER.CATCH_WEIGHT_IND%TYPE := 'N';
         L_const_dimen_ind             ITEM_MASTER.CONST_DIMEN_IND%TYPE := 'N';
         L_simple_pack_ind             ITEM_MASTER.SIMPLE_PACK_IND%TYPE := 'N';
         L_contains_inner_ind          ITEM_MASTER.CONTAINS_INNER_IND%TYPE := 'N';
         L_gift_wrap_ind               ITEM_MASTER.GIFT_WRAP_IND%TYPE := 'N';
         L_ship_alone_ind              ITEM_MASTER.SHIP_ALONE_IND%TYPE := 'N';
         L_item_xform_ind              ITEM_MASTER.ITEM_XFORM_IND%TYPE := 'N';
         L_item_aggregate_ind          ITEM_MASTER.ITEM_AGGREGATE_IND%TYPE := 'N';
         L_diff_aggregate_ind          ITEM_MASTER.DIFF_1_AGGREGATE_IND%TYPE := 'N';

         L_create_datetime             ITEM_MASTER.CREATE_DATETIME%TYPE := sysdate;
         L_last_update_id              ITEM_MASTER.LAST_UPDATE_ID%TYPE := user;
         L_last_update_datetime        ITEM_MASTER.LAST_UPDATE_DATETIME%TYPE := sysdate;


         -- Defaults for NULL in external table
         L_merchandise_ind             ITEM_MASTER.MERCHANDISE_IND%TYPE := 'Y';
         L_forecast_ind                ITEM_MASTER.FORECAST_IND%TYPE := 'Y';

         BEGIN

            -- Insert cross reference level(UPC,etc) items

           insert into ITEM_MASTER
           (
             item,
             item_parent,
             item_grandparent,
             item_number_type,
             pack_ind,
             item_level,
             tran_level,
             item_aggregate_ind,
             diff_1,
             diff_2,
             diff_3,
             diff_4,
             diff_1_aggregate_ind,
             diff_2_aggregate_ind,
             diff_3_aggregate_ind,
             diff_4_aggregate_ind,
             dept,
             class,
             subclass,
             cost_zone_group_id,
             status,
             item_desc,
             short_desc,
             desc_up,
             item_desc_secondary,
             primary_ref_item_ind,
             standard_uom,
             uom_conv_factor,
             merchandise_ind,
             store_ord_mult,
             forecast_ind,
             catch_weight_ind,
             const_dimen_ind,
             simple_pack_ind,
             contains_inner_ind,
             sellable_ind,
             orderable_ind,
             gift_wrap_ind,
             ship_alone_ind,
             create_datetime,
             last_update_id,
             last_update_datetime,
             item_xform_ind,
             inventory_ind,
             aip_case_type,
             comments,
             perishable_ind,
             product_classification,
             brand_name
            )
             select r.item,
                    f.item,
                    s.item,
                    r.item_number_type,
                    L_pack_ind,
                    L_item_level,
                    L_tran_level,
                    L_item_aggregate_ind,
                    coalesce(f.diff_1,f.diff_2,f.diff_3,f.diff_4),  -- diff 1
                    case
                    when nvl(f.diff_1,'-1') != '-1' then
                       coalesce(f.diff_2,f.diff_3,f.diff_4)
                    when nvl(f.diff_2,'-1') != '-1' then
                       nvl(f.diff_3,f.diff_4)
                    when nvl(f.diff_3,'-1') != '-1' then
                       f.diff_4
                    end, -- diff 2
                    case
                    when (nvl(f.diff_1,'-1') != '-1' and nvl(f.diff_2,'-1') != '-1') then
                       nvl(f.diff_3,f.diff_4)
                    when (nvl(f.diff_1,'-1') != '-1' and nvl(f.diff_3,'-1') != '-1') then
                       f.diff_4
                    when (nvl(f.diff_2,'-1') != '-1' and nvl(f.diff_3,'-1') != '-1') then
                       f.diff_4
                    end,  -- diff 3
                    case
                    when (nvl(f.diff_1,'-1') != '-1' and nvl(f.diff_2,'-1') != '-1'
                          and nvl(f.diff_3,'-1') != '-1') then
                       f.diff_4
                    end,  -- diff 4
                    L_diff_aggregate_ind,
                    L_diff_aggregate_ind,
                    L_diff_aggregate_ind,
                    L_diff_aggregate_ind,
                    s.dept,
                    s.class,
                    s.subclass,
                    f.cost_zone_group_id,
                    L_status,
                    r.item_desc,
                    nvl(r.short_desc,rtrim(substrb(r.item_desc,1,120))),
                    upper(r.item_desc),
                    r.item_desc_secondary,
                    nvl(r.primary_ref_item_ind,L_primary_ref_item_ind),
                    f.standard_uom,
                    f.uom_conv_factor,
                    nvl(f.merchandise_ind,L_merchandise_ind),
                    f.store_ord_mult,
                    nvl(f.forecast_ind,L_forecast_ind),
                    L_catch_weight_ind,
                    L_const_dimen_ind,
                    L_simple_pack_ind,
                    L_contains_inner_ind,
                    L_sellable_ind,
                    L_orderable_ind,
                    L_gift_wrap_ind,
                    L_ship_alone_ind,
                    L_create_datetime,
                    L_last_update_id,
                    L_last_update_datetime,
                    L_item_xform_ind,
                    L_inventory_ind,
                    r.aip_case_type,
                    r.comments,
                    nvl(r.perishable_ind,'N'),
                    r.product_classification,
                    r.brand_name
               from dc_style s,
                    dc_fashion_sku f,
                    dc_fashion_xref r
              where r.item_parent = f.item
                and s.item = f.item_parent;

           DECLARE
             CURSOR c_items
             IS
               SELECT item FROM dc_fashion_xref;
             l_items ITEM_TBL;
             L_error_message rtk_errors.rtk_text%type;
           BEGIN
             OPEN c_items;
             LOOP
               FETCH c_items bulk collect INTO l_items limit 1000;
               IF l_items.count                                                 > 0 THEN
                 IF ITEM_APPROVAL_SQL.SET_ALC_ITEM_TYPE(L_error_message,l_items)=false THEN
                   raise_application_error(-20001,L_error_message);
                 END IF;
               END IF;
               EXIT
             WHEN c_items%notfound;
             END LOOP;
           END;


         commit;

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}

ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"

LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"

# Check configuration file
checkCfg
chk_status=$?
if [[ ${chk_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${chk_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi

# Check the data file
retCode=$(checkFile -f $inputFile)
if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Load the file
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOAD_XREF_ITEMS
   ld_xref_status=$?
   if [[ ${ld_xref_status} -ne ${OK} ]]; then
      LOG_ERROR "Load function get failed during loading from dc table to main table" "LOAD_XREF_ITEMS" "${ld_xref_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_xref_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
   fi
fi
# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi
LOG_MESSAGE "Job ${pgmName}.ksh completed successfully by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"