#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_insert_rpm_future_retail.ksh
# Desc: Load the data from staging to main tables.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_insert_rpm_future_retail.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#----------------------------------------------------------------------------------------------------
# Function Name : INSERT_RPM_FUTURE_RETAIL
# Purpose : load data from data conversion table to base tables.
#----------------------------------------------------------------------------------------------------
function INSERT_RPM_FUTURE_RETAIL
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE

         L_error_message             VARCHAR2(255);
         L_recognize_wh_as_loc       RPM_SYSTEM_OPTIONS.RECOGNIZE_WH_AS_LOCATIONS%TYPE;
         L_date                      PERIOD.VDATE%TYPE := GET_VDATE;
         ERROR_IN_FUNCTION           EXCEPTION;

      BEGIN

         if RPM_SYSTEM_OPTIONS_SQL.GET_RECOGNIZE_WH_AS_LOCATIONS(L_recognize_wh_as_loc,
                                                                 L_error_message) = FALSE then
            :GV_script_error := 1;
            :GV_return_code := L_error_message;
             raise ERROR_IN_FUNCTION;
         end if;

         -- rpm_stage_item_loc are inserted for sellable tran-level items only
         -- RPM batch will take care of inserting into the rpm_future_retail table
            INSERT INTO rpm_stage_item_loc (stage_item_loc_id,
                                            item,
                                            loc,
                                            loc_type,
                                            selling_unit_retail,
                                            selling_uom,
                                            status,
                                            create_date)
                                     SELECT rpm_stage_item_loc_seq.NEXTVAL,
                                            il.item,
                                            il.location,
                                            'S',
                                            il.SELLING_UNIT_RETAIL,
                                            il.SELLING_UOM,
                                            'N',
                                            SYSDATE
                                       FROM DC_ITEM_LOC il,
                                            ITEM_MASTER im,
                                            STORE s
                                            WHERE il.ITEM = im.ITEM
                                            AND il.LOCATION = s.STORE
                                            AND il.LOC_TYPE = 'S'
                                            AND im.SELLABLE_IND = 'Y'
                                            AND im.ITEM_LEVEL = im.TRAN_LEVEL;

         if L_recognize_wh_as_loc = 1 then
         -- for warehouses, only insert stockholding locations
            INSERT INTO rpm_stage_item_loc (stage_item_loc_id,
                                             item,
                                             loc,
                                             loc_type,
                                             selling_unit_retail,
                                             selling_uom,
                                             status,
                                             create_date)
                                      SELECT rpm_stage_item_loc_seq.NEXTVAL,
                                             il.item,
                                             il.location,
                                             'W',
                                             il.SELLING_UNIT_RETAIL,
                                             il.SELLING_UOM,
                                             'N',
                                             SYSDATE
                                        FROM DC_ITEM_LOC il,
                                             ITEM_MASTER im,
                                             WH wh
                                             WHERE il.ITEM = im.ITEM
                                             AND il.LOCATION = wh.WH
                                             AND il.LOC_TYPE = 'W'
                                             AND im.SELLABLE_IND = 'Y'
                                             AND im.ITEM_LEVEL = im.TRAN_LEVEL
                                             AND wh.STOCKHOLDING_IND = 'Y';
         end if;
         commit;

      EXCEPTION
         when ERROR_IN_FUNCTION then
            rollback;
            :GV_return_code := 1;
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

   INSERT_RPM_FUTURE_RETAIL
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data of RPM dept aggregation table." "INSERT_RPM_FUTURE_RETAIL" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "INSERT_RPM_FUTURE_RETAIL" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi


LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}