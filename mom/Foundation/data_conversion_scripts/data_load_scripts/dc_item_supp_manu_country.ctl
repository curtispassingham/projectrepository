LOAD DATA
REPLACE
INTO TABLE DC_ITEM_SUPP_MANU_COUNTRY
WHEN 
     ( ITEM != BLANKS )
AND  ( SUPPLIER != BLANKS )
AND  ( MANU_COUNTRY_ID != BLANKS )
FIELDS TERMINATED BY '|'
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( ITEM                  
 ,SUPPLIER              
 ,MANU_COUNTRY_ID       
 ,PRIMARY_MANU_CTRY_IND )