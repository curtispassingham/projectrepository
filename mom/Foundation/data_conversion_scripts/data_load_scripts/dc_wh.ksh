#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_wh.ksh
# Desc: Load the flat file data to staging table dc_pwh,dc_vwh through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_wh.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
DataFile="dc_pwh
          dc_vwh"
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_FILE
# Purpose : Load data from .dat to d_pwh table(using sqlloader).
#----------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = "" 
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${OK}
      fi
   fi
}
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_WH
# Purpose : load wh data from dc_pwh,dc_vwh to wh table.
#----------------------------------------------------------------------------------------------------
function LOAD_WH
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1
      DECLARE
            L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
            L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
            L_rms_async_id WH_ADD.RMS_ASYNC_ID%TYPE;
            O_error_message RTK_ERRORS.RTK_TEXT%TYPE;
            enqueue_exception EXCEPTION;
            PRAGMA EXCEPTION_INIT( enqueue_exception, -20001 );
            ERROR_IN_FUNCTION   EXCEPTION;
      BEGIN
            if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                                     L_system_options_row) = FALSE then
               :GV_script_error := L_error_message;
                raise ERROR_IN_FUNCTION;
            end if;
            insert into wh(wh,
                        wh_name,
                        wh_name_secondary,
                        email,
                        vat_region,
                        org_hier_type,
                        org_hier_value,
                        currency_code,
                        physical_wh,
                        primary_vwh,
                        channel_id,
                        stockholding_ind,
                        break_pack_ind,
                        redist_wh_ind,
                        delivery_policy,
                        restricted_ind,
                        protected_ind,
                        forecast_wh_ind,
                        rounding_seq,
                        repl_ind,
                        repl_wh_link,
                        repl_src_ord,
                        ib_ind,
                        ib_wh_link,
                        auto_ib_clear,
                        duns_number,
                        duns_loc,
                        tsf_entity_id,
                        finisher_ind,
                        inbound_handling_days,
                        org_unit_id,
                        org_entity_type) 
                 select wh,
                        wh_name,
                        wh_name_secondary,
                        email,
                        decode(L_system_options_row.default_tax_type,'SVAT',nvl(vat_region,1000)),
                        org_hier_type,
                        org_hier_value,
                        currency_code,
                        wh, --physical wh
                        NULL ,
                        NULL,
                        'N', --stockholding_ind
                        NVL(break_pack_ind, 'Y'),
                        NVL(redist_wh_ind, 'Y'),
                        delivery_policy,
                        'N', --restricted_ind
                        'N', --protected_ind
                        NVL(forecast_wh_ind, 'Y'), --forecast_wh_ind
                        NULL,
                        'N', --repl_ind
                        repl_wh_link,
                        NULL,
                        NVL(ib_ind, 'N'),
                        ib_wh_link,
                        NVL(auto_ib_clear, 'N'),
                        duns_number,
                        duns_loc,
                        NULL,   --tsf_entity_id
                        'N',
                        inbound_handling_days,
                        org_unit_id,
                        org_entity_type
                   from dc_pwh;

         insert into wh(wh,
                        wh_name,
                        wh_name_secondary,
                        email,
                        vat_region,
                        org_hier_type,
                        org_hier_value,
                        currency_code,
                        physical_wh,
                        primary_vwh,
                        channel_id,
                        stockholding_ind,
                        break_pack_ind,
                        redist_wh_ind,
                        delivery_policy,
                        restricted_ind,
                        protected_ind,
                        forecast_wh_ind,
                        rounding_seq,
                        repl_ind,
                        repl_wh_link,
                        repl_src_ord,
                        ib_ind,
                        ib_wh_link,
                        auto_ib_clear,
                        duns_number,
                        duns_loc,
                        tsf_entity_id,
                        finisher_ind,
                        inbound_handling_days,
                        org_unit_id,
                        org_entity_type,
                        customer_order_loc_ind,
                        default_wh)
                 select a.wh,
                        a.wh_name,
                        a.wh_name_secondary,
                        b.email,
                        decode(L_system_options_row.default_tax_type,'SVAT',nvl(b.vat_region,1000)),
                        b.org_hier_type,
                        b.org_hier_value,
                        b.currency_code,
                        a.physical_wh,
                        NULL, --primary_vwh
                        a.channel_id,
                        'Y', --stockholding_ind,
                        NVL(b.break_pack_ind, 'Y'),
                        NVL(b.redist_wh_ind, 'Y'),
                        b.delivery_policy,
                        NVL(a.restricted_ind, 'N'),
                        NVL(a.protected_ind, 'N'),
                        NVL(a.forecast_wh_ind, 'Y'),
                        NULL,
                        NVL(a.repl_ind, 'N'),
                        a.repl_wh_link,
                        NULL,
                        NVL(a.ib_ind, 'N'),
                        a.ib_wh_link,
                        NVL(a.auto_ib_clear, 'N'),
                        b.duns_number,
                        b.duns_loc,
                        a.tsf_entity_id,
                        NVL(finisher_ind, 'N'),
                        b.inbound_handling_days,
                        a.org_unit_id,
                        a.org_entity_type,
                        NVL(a.customer_order_loc_ind,'N'),
                        a.default_wh
                   from dc_vwh a,
                        dc_pwh b
                  where a.physical_wh = b.wh;
                  
         L_rms_async_id := RMS_ASYNC_ID_SEQ.nextval;
         insert into wh_add(wh,
                            wh_currency,
                            pricing_location,
                            pricing_loc_curr,
                            rms_async_id)
                     select a.wh,
                            b.currency_code,
                            a.wh,
                            b.currency_code,
                            L_rms_async_id
                       from dc_vwh a,
                            dc_pwh b
                      where a.physical_wh = b.wh;

         update wh
            set primary_vwh = (select primary_vwh
                                 from dc_pwh a
                                where a.wh= wh.wh);

         COMMIT;

      EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}

#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

for filename in ${DataFile}
do
   inputFile=${filename}.dat
   CTLFILE="${scriptDir}/${filename}.ctl"
    # Check the data file
   retCode=$(checkFile -f $inputFile)
   if [[ ${retCode} -eq 2 ]]; then
      LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit 1
   elif [[ ${retCode} -eq 3 ]]; then
      LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit 1
   elif [[ ${retCode} -eq 4 ]]; then
      LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit 1
   fi

   # Load the file using sqlloader
   LOAD_FILE
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "SQLLOADER failed while loading the ${filename} file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Sqlloader function for ${filename} completed successfully." "LOAD_FILE" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi
done

  # Load the data from data conversion to base tables 
   LOAD_WH
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data to main tables." "LOAD_WH" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_WH" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi


# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
for filename in ${DataFile}
do
   inputFile=${filename}.dat
    if [[ -f "${dataDir}/${inputFile}" ]]; then
     mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
    fi
done
LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}
