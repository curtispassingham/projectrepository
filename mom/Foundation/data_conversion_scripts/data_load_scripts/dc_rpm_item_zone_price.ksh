#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_rpm_item_zone_price.ksh
# Desc: Load the flat file data to staging table dc_rpm_item_zone_price through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_rpm_item_zone_price.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_RPM_ITEM_ZONE_PRICE
# Purpose : load RPM item zone price data from data conversion to rpm_item_zone_price table.
#----------------------------------------------------------------------------------------------------
function LOAD_RPM_ITEM_ZONE_PRICE
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE
         L_primary_currency   SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;
         L_item_tbl           ITEM_TBL;
    L_dept_tbl           DEPT_TBL;
         L_zone_group_tbl     DEPT_TBL;

         cursor C_SYSTEM_OPTIONS is
            select currency_code
              from system_options;
         
      BEGIN

         open C_SYSTEM_OPTIONS;
         fetch C_SYSTEM_OPTIONS into L_primary_currency;
         close C_SYSTEM_OPTIONS;

               --- perform the insert where no conversion is necessary (zone currency = primary currency)
               insert into rpm_item_zone_price(item_zone_price_id,
                                               item,
                                               zone_id,
                                               standard_retail,
                                               standard_retail_currency,
                                               standard_uom,
                                               selling_retail,
                                               selling_retail_currency,
                                               selling_uom,
                                               multi_unit_retail_currency)
                                        select RPM_ITEM_ZONE_PRICE_SEQ.nextval,
                                               dph.item,
                                               rz.zone_id,
                                               dph.unit_retail,
                                               rz.currency_code,
                                               dph.selling_uom,
                                               dph.unit_retail,
                                               rz.currency_code,
                                               dph.selling_uom,
                                               rz.currency_code
                                          from dc_price_hist dph,
                                               item_master im,
                                               rpm_merch_retail_def_expl rmrd,
                                               rpm_zone rz
                                         where dph.item = im.item  
                                           and im.dept = rmrd.dept 
                                           and im.class = rmrd.class  
                                           and im.subclass = rmrd.subclass  
                                           and rz.zone_group_id = rmrd.regular_zone_group  
                                           and rz.currency_code = L_primary_currency;

               --- perform the insert where unit retail is converted (zone currency != primary currency)
               insert into rpm_item_zone_price(item_zone_price_id,
                                               item,
                                               zone_id,
                                               standard_retail,
                                               standard_retail_currency,
                                               standard_uom,
                                               selling_retail,
                                               selling_retail_currency,
                                               selling_uom,
                                               multi_unit_retail_currency)
                                        select RPM_ITEM_ZONE_PRICE_SEQ.nextval,
                                               dph.item,
                                               rz.zone_id,
                                               CURRENCY_SQL.CONVERT_VALUE('R',
                                                                          rz.currency_code,
                                                                          L_primary_currency,
                                                                          dph.unit_retail),

                                               rz.currency_code,
                                               dph.selling_uom,
                                               CURRENCY_SQL.CONVERT_VALUE('R',
                                                                          rz.currency_code,
                                                                          L_primary_currency,
                                                                          dph.unit_retail),

                                               rz.currency_code,
                                               dph.selling_uom,
                                               rz.currency_code
                                          from dc_price_hist dph,
                                               item_master im,
                                               rpm_merch_retail_def_expl rmrd,
                                               rpm_zone rz
                                         where dph.item = im.item  
                                           and im.dept = rmrd.dept 
                                           and im.class = rmrd.class  
                                           and im.subclass = rmrd.subclass  
                                           and rz.zone_group_id = rmrd.regular_zone_group
                                           and rz.currency_code != L_primary_currency;

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi
  
# Load the data from data conversion to base tables 
   LOAD_RPM_ITEM_ZONE_PRICE
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data to main tables." "LOAD_RPM_ITEM_ZONE_PRICE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_RPM_ITEM_ZONE_PRICE" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}
