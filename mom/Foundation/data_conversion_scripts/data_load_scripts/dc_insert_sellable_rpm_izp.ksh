#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_insert_sellable_rpm_izp.ksh
# Desc: Load the data from staging to main tables.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_insert_sellable_rpm_izp.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#----------------------------------------------------------------------------------------------------
# Function Name : INSERT_SELLABLE_RPM_IZP
# Purpose : load data from data conversion table to base tables.
#----------------------------------------------------------------------------------------------------
function INSERT_SELLABLE_RPM_IZP
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE
         L_primary_currency   SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;
         L_item_tbl           ITEM_TBL;
	 L_dept_tbl           DEPT_TBL;
         L_zone_group_tbl     DEPT_TBL;

         L_pack_selling_uom   RPM_ITEM_ZONE_PRICE.STANDARD_UOM%TYPE := 'EA';

         cursor C_SYSTEM_OPTIONS is
            select currency_code
              from system_options;

         cursor C_ZONE_GROUP is
            select dsp.item,
                   rmrd.regular_zone_group
              from dc_sellable_pack dsp,
                   rpm_merch_retail_def_expl rmrd
             where dsp.dept = rmrd.dept
               and dsp.class = rmrd.class
               and dsp.subclass = rmrd.subclass;

      BEGIN

         open C_SYSTEM_OPTIONS;
         fetch C_SYSTEM_OPTIONS into L_primary_currency;
         close C_SYSTEM_OPTIONS;

         open C_ZONE_GROUP;
         fetch C_ZONE_GROUP BULK COLLECT into L_item_tbl,
                                              L_zone_group_tbl;
         close C_ZONE_GROUP;

         if L_item_tbl.count > 0 then
            FOR i in L_item_tbl.first .. L_item_tbl.last LOOP

            --- perform the insert where no conversion is necessary (zone currency = primary currency)
               insert into rpm_item_zone_price(item_zone_price_id,
                                               item,
                                               zone_id,
                                               standard_retail,
                                               standard_retail_currency,
                                               standard_uom,
                                               selling_retail,
                                               selling_retail_currency,
                                               selling_uom,
                                               multi_unit_retail_currency)
                                        select RPM_ITEM_ZONE_PRICE_SEQ.nextval,
                                               dsp.item,
                                               rz.zone_id,
                                               dsp.unit_retail, --STANDARD_RETAIL
                                               rz.currency_code,
                                               L_pack_selling_uom,  --STANDARD_UOM
                                               dsp.unit_retail,  --SELLING_RETAIL
                                               rz.currency_code,
                                               L_pack_selling_uom,  --SELLING_UOM
                                               rz.currency_code
                                          from dc_sellable_pack dsp,
                                               rpm_zone rz
                                         where dsp.item = L_item_tbl(i)
                                           and rz.zone_group_id = L_zone_group_tbl(i)
                                           and rz.currency_code = L_primary_currency;

               --- perform the insert where unit retail is converted (zone currency != primary currency)
               insert into rpm_item_zone_price(item_zone_price_id,
                                               item,
                                               zone_id,
                                               standard_retail,
                                               standard_retail_currency,
                                               standard_uom,
                                               selling_retail,
                                               selling_retail_currency,
                                               selling_uom,
                                               multi_unit_retail_currency)
                                        select RPM_ITEM_ZONE_PRICE_SEQ.nextval,
                                               dsp.item,
                                               rz.zone_id,
                                               CURRENCY_SQL.CONVERT_VALUE('R',
                                                                           L_primary_currency,
                                                                           rz.currency_code,
                                                                           dsp.unit_retail), --STANDARD_RETAIL

                                               rz.currency_code,
                                               L_pack_selling_uom, --STANDARD_UOM
                                               CURRENCY_SQL.CONVERT_VALUE('R',
                                                                           L_primary_currency,
                                                                           rz.currency_code,
                                                                           dsp.unit_retail), --SELLING_RETAIL

                                               rz.currency_code,
                                               L_pack_selling_uom,  --SELLING_UOM
                                               rz.currency_code
                                          from dc_sellable_pack dsp,
                                               rpm_zone rz
                                         where dsp.item = L_item_tbl(i)
                                           and rz.zone_group_id = L_zone_group_tbl(i)
                                           and rz.currency_code != L_primary_currency;

               COMMIT;
            END LOOP;
         end if;

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

   INSERT_SELLABLE_RPM_IZP
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data of RPM dept aggregation table." "INSERT_SELLABLE_RPM_IZP" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "INSERT_SELLABLE_RPM_IZP" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi


LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}