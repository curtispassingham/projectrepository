#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_default_grocery.ksh
# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_default_grocery.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#--------------------------------------------------------------------------------------------------
function DEFAULT_GROCERY
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE
         L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
         L_run_report               BOOLEAN;
         L_item_tbl                 ITEM_TBL;
         L_dept_tbl                 DEPT_TBL;
         L_class_tbl                DEPT_TBL;
         L_subclass_tbl             DEPT_TBL;
         L_tax_info_tbl             OBJ_TAX_INFO_TBL   := OBJ_TAX_INFO_TBL();

         ERROR_IN_FUNCTION   EXCEPTION;
         cursor C_MERCH_PRODUCT is
            select OBJ_TAX_INFO_REC(p.item,    --item
                                    NULL,      --item_parent
                                    NULL,      --item_grandparent
                                    'Y',       --pack_ind
                                    4,         --merch_hier_level
                                    pl.dept,   --merch_hier_value
                                    NULL,      --from_tax_region
                                    NULL,      --from_tax_region_desc
                                    NULL,      --to_tax_region
                                    NULL,      --from_entity
                                    NULL,      --from entity_type
                                    NULL,      --to_entity
                                    NULL,      --to_entity_type
                                    NULL,      --amount
                                    NULL,      --cost_retail_ind
                                    NULL,      --tax_incl_ind
                                    NULL,      --tax_amount
                                    NULL,      --tax_rate
                                    NULL,      --tax_code
                                    NULL,      --tax_code_desc
                                    NULL,      --active_date
                                    NULL,      --inventory_ind
                                    NULL,      --ref_no_1
                                    NULL,      --ref_no_2
                                    NULL,      --tran_code
                                    NULL,      --taxable_base
                                    NULL,      --modified_taxable_base
                                    NULL,      --calculation_basis
                                    NULL,      --recoverable_amount
                                    NULL)      --currency
              from dc_product p,
                   dc_product_line pl
             where p.item_parent = pl.item;

         cursor C_MERCH_PRODUCT_LINE_PRODUCT is
            select pl.item,
                   pl.dept,
                   pl.class,
                   pl.subclass
              from dc_product_line pl,
                   dc_product p
             where pl.item = p.item_parent
               and p.primary_product_ind = 'Y'
             union all
            select p.item,
                   pl.dept,
                   pl.class,
                   pl.subclass
              from dc_product p,
                   dc_product_line pl
             where p.item_parent = pl.item;

      BEGIN
         -- default UDA and charges for all product lines and products
         open C_MERCH_PRODUCT_LINE_PRODUCT;
         fetch C_MERCH_PRODUCT_LINE_PRODUCT BULK COLLECT into L_item_tbl,
                                                              L_dept_tbl,
                                                              L_class_tbl,
                                                              L_subclass_tbl;
         close C_MERCH_PRODUCT_LINE_PRODUCT;

         open C_MERCH_PRODUCT;
         fetch C_MERCH_PRODUCT BULK COLLECT into L_tax_info_tbl;

         close C_MERCH_PRODUCT;

         if TAX_SQL.GET_TAX_INFO(L_error_message,
                                 L_tax_info_tbl) = FALSE then
            :GV_script_error := L_error_message;
             raise ERROR_IN_FUNCTION;
         end if;

         if TAX_SQL.INSERT_UPDATE_TAX_SKU(L_error_message,
                                            L_run_report,
                                            L_tax_info_tbl) = FALSE then


            :GV_script_error := L_error_message;
            raise ERROR_IN_FUNCTION;
         end if;

         -- default UDA
         if UDA_SQL.INSERT_DEFAULTS(L_error_message,
                                    L_item_tbl,
                                    L_dept_tbl,
                                    L_class_tbl,
                                    L_subclass_tbl) = FALSE then
            :GV_script_error := L_error_message;
            raise ERROR_IN_FUNCTION;
         end if;

         -- default charges
         if ITEM_CHARGE_SQL.DEFAULT_CHRGS(L_error_message,
                                          L_item_tbl,
                                          L_dept_tbl) = FALSE then
            :GV_script_error := L_error_message;
            raise ERROR_IN_FUNCTION;
         end if;

         commit;

      EXCEPTION
         when ERROR_IN_FUNCTION then
            rollback;
             :GV_return_code := 1;
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}

ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"

LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"

# Check configuration file
checkCfg
chk_status=$?
if [[ ${chk_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${chk_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi

# Load the file
DEFAULT_GROCERY
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Load function get failed during loading from dc table to main table" "DEFAULT_GROCERY" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Load Function completed successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi
LOG_MESSAGE "Job ${pgmName}.ksh completed successfully by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"