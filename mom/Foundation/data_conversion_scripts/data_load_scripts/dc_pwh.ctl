LOAD DATA
REPLACE
INTO TABLE DC_PWH
WHEN 
    ( WH != BLANKS )
AND ( WH_NAME != BLANKS )
AND ( CURRENCY_CODE != BLANKS )
AND ( DELIVERY_POLICY != BLANKS )
AND ( INBOUND_HANDLING_DAYS != BLANKS )
AND ( ORG_ENTITY_TYPE != BLANKS )
FIELDS TERMINATED BY '|'
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( WH                    
 ,WH_NAME               
 ,PRIMARY_VWH           
 ,CURRENCY_CODE         
 ,BREAK_PACK_IND        
 ,REDIST_WH_IND         
 ,DELIVERY_POLICY       
 ,FORECAST_WH_IND       
 ,REPL_IND              
 ,REPL_WH_LINK          
 ,IB_IND                
 ,IB_WH_LINK            
 ,AUTO_IB_CLEAR         
 ,INBOUND_HANDLING_DAYS 
 ,WH_NAME_SECONDARY     
 ,EMAIL                 
 ,VAT_REGION            
 ,ORG_HIER_TYPE         
 ,ORG_HIER_VALUE        
 ,DUNS_LOC              
 ,DUNS_NUMBER           
 ,ORG_UNIT_ID           
 ,ORG_ENTITY_TYPE )