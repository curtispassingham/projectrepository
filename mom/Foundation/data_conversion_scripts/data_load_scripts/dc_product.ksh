#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_product.ksh
# Desc: Load the flat file data to staging table dc_product through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_product.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#--------------------------------------------------------------------------------------------------
function LOAD_PRODUCT
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000
     
      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);
      
      EXEC :GV_return_code := 0;
      
      WHENEVER SQLERROR EXIT 1

      DECLARE
         -- default attributes for product
         L_item_number_type            ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE := 'ITEM';
         L_item_level                  ITEM_MASTER.ITEM_LEVEL%TYPE := 2;
         L_tran_level                  ITEM_MASTER.TRAN_LEVEL%TYPE := 2;
         L_item_aggregate_ind          ITEM_MASTER.ITEM_AGGREGATE_IND%TYPE := 'N';
         L_diff_1_aggregate_ind        ITEM_MASTER.DIFF_1_AGGREGATE_IND%TYPE := 'N';
         L_diff_2_aggregate_ind        ITEM_MASTER.DIFF_2_AGGREGATE_IND%TYPE := 'N';
         L_diff_3_aggregate_ind        ITEM_MASTER.DIFF_3_AGGREGATE_IND%TYPE := 'N';
         L_diff_4_aggregate_ind        ITEM_MASTER.DIFF_4_AGGREGATE_IND%TYPE := 'N';
         L_status                      ITEM_MASTER.STATUS%TYPE := 'A';

         L_primary_ref_item_ind        ITEM_MASTER.PRIMARY_REF_ITEM_IND%TYPE := 'N';
         L_pack_ind                    ITEM_MASTER.PACK_IND%TYPE := 'N';
         L_simple_pack_ind             ITEM_MASTER.SIMPLE_PACK_IND%TYPE := 'N';
         L_contains_inner_ind          ITEM_MASTER.CONTAINS_INNER_IND%TYPE := 'N';
         L_item_xform_ind              ITEM_MASTER.ITEM_XFORM_IND%TYPE := 'N';
         L_inventory_ind               ITEM_MASTER.INVENTORY_IND%TYPE := 'Y';
         L_sellable_ind                ITEM_MASTER.SELLABLE_IND%TYPE := 'Y';
         L_orderable_ind               ITEM_MASTER.ORDERABLE_IND%TYPE := 'Y';

         L_const_dimen_ind             ITEM_MASTER.CONST_DIMEN_IND%TYPE := 'N';
         L_gift_wrap_ind               ITEM_MASTER.GIFT_WRAP_IND%TYPE := 'N';
         L_ship_alone_ind              ITEM_MASTER.SHIP_ALONE_IND%TYPE := 'N';

         L_create_datetime             ITEM_MASTER.CREATE_DATETIME%TYPE := sysdate;
         L_last_update_id              ITEM_MASTER.LAST_UPDATE_ID%TYPE := user;
         L_last_update_datetime        ITEM_MASTER.LAST_UPDATE_DATETIME%TYPE := sysdate;

         L_merchandise_ind             ITEM_MASTER.MERCHANDISE_IND%TYPE := 'Y';
         L_forecast_ind                ITEM_MASTER.FORECAST_IND%TYPE := 'Y';
         L_catch_weight_ind            ITEM_MASTER.CATCH_WEIGHT_IND%TYPE := 'N';
         L_perishable_ind              ITEM_MASTER.PERISHABLE_IND%TYPE := 'N';
         -- The following attributes are defaulted from product line (dc_product_line external table):             
         -- dept
         -- class
         -- subclass

      BEGIN
      
        insert into ITEM_MASTER (item,
                                 item_parent,
                                 item_number_type,
                                 item_level,
                                 tran_level,
                                 item_aggregate_ind,
                                 diff_1,
                                 diff_2,
                                 diff_3,
                                 diff_4,
                                 diff_1_aggregate_ind,
                                 diff_2_aggregate_ind,
                                 diff_3_aggregate_ind,
                                 diff_4_aggregate_ind,
                                 dept,
                                 class,
                                 subclass,
                                 cost_zone_group_id,
                                 status,
                                 item_desc,
                                 short_desc,
                                 desc_up,
                                 item_desc_secondary,
                                 store_ord_mult,
                                 standard_uom,
                                 uom_conv_factor,
                                 primary_ref_item_ind,
                                 pack_ind,
                                 simple_pack_ind,
                                 contains_inner_ind,
                                 item_xform_ind,
                                 inventory_ind,
                                 sellable_ind,
                                 orderable_ind,
                                 merchandise_ind,
                                 forecast_ind,
                                 catch_weight_ind,
                                 const_dimen_ind,
                                 gift_wrap_ind,
                                 ship_alone_ind,
                                 handling_temp,
                                 handling_sensitivity,
                                 waste_type,
                                 waste_pct,
                                 default_waste_pct,
                                 package_size,
                                 package_uom,
                                 deposit_item_type,
                                 container_item,
                                 deposit_in_price_per_uom,
                                 retail_label_type,
                                 retail_label_value,
                                 create_datetime,
                                 last_update_id,
                                 last_update_datetime,
                                 aip_case_type,
                                 comments,
								 perishable_ind,
                                 product_classification,
                                 brand_name)
                          select p.item,
                                 p.item_parent,
                                 L_item_number_type,
                                 L_item_level,
                                 L_tran_level,
                                 L_item_aggregate_ind,
                                 -- fill in diff1, diff2, diff3, diff4
                                 coalesce(p.diff_1,p.diff_2,p.diff_3,p.diff_4) diff1,
                                 case
                                 when nvl(p.diff_1,'-1') != '-1' then    -- diff1 is not null, diff1<-diff1
                                    coalesce(p.diff_2,p.diff_3,p.diff_4)
                                 when nvl(p.diff_2,'-1') != '-1' then    -- diff1 is NULL, but diff2 is not null, diff1<-diff2
                                    nvl(p.diff_3,p.diff_4)
                                 when nvl(p.diff_3,'-1') != '-1' then     -- diff1 is null and diff2 is null, but diff3 is not null, diff1<-diff3
                                    p.diff_4
                                 end diff2, 
                                 case
                                 when (nvl(p.diff_1,'-1') != '-1' and nvl(p.diff_2,'-1') != '-1') then
                                    nvl(p.diff_3,p.diff_4)
                                 when (nvl(p.diff_1,'-1') != '-1' and nvl(p.diff_3,'-1') != '-1') then
                                    p.diff_4
                                 when (nvl(p.diff_2,'-1') != '-1' and nvl(p.diff_3,'-1') != '-1') then
                                    p.diff_4
                                 end diff3,
                                 case
                                 when (nvl(p.diff_1,'-1') != '-1' and nvl(p.diff_2,'-1') != '-1'
                                       and nvl(p.diff_3,'-1') != '-1') then
                                    p.diff_4
                                 end diff4,
                                 -- no diff aggregate at product level
                                 L_diff_1_aggregate_ind,
                                 L_diff_2_aggregate_ind,
                                 L_diff_3_aggregate_ind,
                                 L_diff_4_aggregate_ind,
                                 pl.dept,
                                 pl.class,
                                 pl.subclass,
                                 p.cost_zone_group_id,
                                 L_status,
                                 p.item_desc,
                                 nvl(p.short_desc,RTRIM(substrb(p.item_desc,1,120))),
                                 upper(p.item_desc),
                                 p.item_desc_secondary,
                                 p.store_ord_mult,
                                 p.standard_uom,
                                 p.uom_conv_factor,
                                 L_primary_ref_item_ind,
                                 L_pack_ind,
                                 L_simple_pack_ind,
                                 L_contains_inner_ind,
                                 L_item_xform_ind,
                                 L_inventory_ind,
                                 L_sellable_ind,
                                 L_orderable_ind,
                                 nvl(p.merchandise_ind, L_merchandise_ind),
                                 nvl(p.forecast_ind, L_forecast_ind),
                                 nvl(p.catch_weight_ind, L_catch_weight_ind),
                                 L_const_dimen_ind,
                                 L_gift_wrap_ind,
                                 L_ship_alone_ind,
                                 p.handling_temp,
                                 p.handling_sensitivity,
                                 p.waste_type,
                                 p.waste_pct,
                                 p.default_waste_pct,
                                 p.package_size,
                                 p.package_uom,
                                 p.deposit_item_type,
                                 p.container_item,
                                 p.deposit_in_price_per_uom,
                                 p.retail_label_type,
                                 p.retail_label_value,
                                 L_create_datetime,
                                 L_last_update_id,
                                 L_last_update_datetime,
                                 p.aip_case_type,
                                 p.comments,
                                 NVL(p.perishable_ind,L_perishable_ind),
                                 p.product_classification,
                                 p.brand_name
                            from dc_product p,
                                 dc_product_line pl
                           where p.item_parent = pl.item;  
                           
           DECLARE
             CURSOR c_items
             IS
               SELECT item FROM dc_product;
             l_items ITEM_TBL;
             L_error_message rtk_errors.rtk_text%type;
           BEGIN
             OPEN c_items;
             LOOP
               FETCH c_items bulk collect INTO l_items limit 1000;
               IF l_items.count                                                 > 0 THEN
                 IF ITEM_APPROVAL_SQL.SET_ALC_ITEM_TYPE(L_error_message,l_items)=false THEN
                   raise_application_error(-20001,L_error_message);
                 END IF;
               END IF;
               EXIT
             WHEN c_items%notfound;
             END LOOP;
           END;
           
                           
         commit;

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}

ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"

LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"

# Check configuration file
checkCfg
chk_status=$?
if [[ ${chk_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${chk_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi

# Load the file
LOAD_PRODUCT
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Load function get failed during loading from dc table to main table" "LOAD_PRODUCT" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Load Function completed successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi
LOG_MESSAGE "Job ${pgmName}.ksh completed successfully by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"