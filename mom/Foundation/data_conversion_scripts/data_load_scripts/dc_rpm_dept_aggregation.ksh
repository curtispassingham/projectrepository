#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_rpm_dept_aggregation.ksh
# Desc: Perform the RPM dept aggregation of merchandise hierarchy.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_rpm_dept_aggregation.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_RPM_DEPT_AGGREGATION
# Purpose : Load RPM dept aggregation data from dc_deps.
#----------------------------------------------------------------------------------------------------
function LOAD_RPM_DEPT_AGGREGATION
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      BEGIN

         insert into rpm_dept_aggregation(dept_aggregation_id,
                                          dept,
                                          lowest_strategy_level,
                                          worksheet_level,
                                          historical_sales_level,
                                          regular_sales_ind,
                                          clearance_sales_ind,
                                          promotional_sales_ind,
                                          include_wh_on_hand,
                                          include_wh_on_order,
                                          price_change_amount_calc_type,
                                          retail_chg_highlight_days,
                                          cost_chg_highlight_days,
                                          pend_cost_chg_window_days,
                                          pend_cost_chg_highlight_days,
                                          lock_version)
                                   select RPM_DEPT_AGGREGATION_SEQ.NEXTVAL,
                                          dept,
                                          nvl(lowest_strategy_level,0),
                                          nvl(worksheet_level,0),
                                          nvl(historical_sales_level,0),
                                          nvl(regular_sales_ind,0),
                                          nvl(clearance_sales_ind,0),
                                          nvl(promotional_sales_ind,0),
                                          nvl(include_wh_on_hand,0),
                                          nvl(include_wh_on_order,0),
                                          nvl(price_change_amount_calc_type,0),
                                          retail_chg_highlight_days,
                                          cost_chg_highlight_days,
                                          pend_cost_chg_window_days,
                                          pend_cost_chg_highlight_days,
                                          NULL
                                     from dc_deps;

         COMMIT;

      EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

# Load the data of rpm dept aggregation 
   LOAD_RPM_DEPT_AGGREGATION
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data of RPM dept aggregation table." "LOAD_RPM_DEPT_AGGREGATION" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_RPM_DEPT_AGGREGATION" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}