#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_insert_future_cost.ksh
# Desc: Load the data from staging to main tables.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_insert_future_cost.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#----------------------------------------------------------------------------------------------------
# Function Name : INSERT_FUTURE_COST
# Purpose : load data from data conversion table to base tables.
#----------------------------------------------------------------------------------------------------
function INSERT_FUTURE_COST
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE

         L_date DATE := get_vdate;

      BEGIN
         -- future_cost are inserted for tran-level items with item/supp/country
         -- unit_cost of the primary supplier/country is used in supplier currency
         insert into future_cost(ITEM,
                                 SUPPLIER,
                                 ORIGIN_COUNTRY_ID,
                                 LOCATION,
                                 LOC_TYPE,
                                 ACTIVE_DATE,
                                 BASE_COST,
                                 NET_COST,
                                 NET_NET_COST,
                                 DEAD_NET_NET_COST,
                                 PRICING_COST,
                                 CALC_DATE,
                                 START_IND,
                                 DIVISION,
                                 GROUP_NO,
                                 DEPT,
                                 CLASS,
                                 SUBCLASS)
                          select il.ITEM,
                                 isc.SUPPLIER,
                                 isc.ORIGIN_COUNTRY_ID,
                                 il.LOCATION,
                                 il.LOC_TYPE,
                                 L_date,
                                 isc.UNIT_COST,
                                 isc.UNIT_COST,
                                 isc.UNIT_COST,
                                 isc.UNIT_COST,
                                 isc.UNIT_COST,
                                 L_date,
                                 'Y',
                                 g.DIVISION,
                                 d.GROUP_NO,
                                 im.DEPT,
                                 im.CLASS,
                                 im.SUBCLASS
                            from dc_item_loc il,
                                 item_master im,
                                 deps d,
                                 groups g,
                                 item_supp_country isc,
                                 (select store loc
                                    from store
                                   union all
                                  select wh loc
                                    from wh
                                   where stockholding_ind = 'Y') loc
                           where il.item = im.item
                             and im.item = isc.item
                             and il.location = loc.loc
                             and im.item_level = im.tran_level
                             and im.dept = d.dept
                             and d.group_no = g.group_no;
         commit;

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

   INSERT_FUTURE_COST
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data of RPM dept aggregation table." "INSERT_FUTURE_COST" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "INSERT_FUTURE_COST" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi


LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}