LOAD DATA
REPLACE
INTO TABLE DC_ITEM_SUPP_COUNTRY_DIM
WHEN 
    ( ITEM != BLANKS )
AND ( SUPPLIER != BLANKS )
AND ( ORIGIN_COUNTRY_ID != BLANKS )
AND ( DIM_OBJECT != BLANKS )
FIELDS TERMINATED BY '|'
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( ITEM                     
 ,SUPPLIER                 
 ,ORIGIN_COUNTRY_ID        
 ,DIM_OBJECT               
 ,PRESENTATION_METHOD      
 ,LENGTH                   
 ,WIDTH                    
 ,HEIGHT                   
 ,LWH_UOM                  
 ,WEIGHT                   
 ,NET_WEIGHT               
 ,WEIGHT_UOM               
 ,LIQUID_VOLUME            
 ,LIQUID_VOLUME_UOM        
 ,STAT_CUBE           
 ,TARE_WEIGHT              
 ,TARE_TYPE )               