#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_default_hardlines.ksh
# Desc: Load the data from staging to main tables.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_default_hardlines.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#----------------------------------------------------------------------------------------------------
# Function Name : INSERT_ITEM_DEFAULTS
# Purpose : load data from data conversion table to base tables.
#----------------------------------------------------------------------------------------------------
function INSERT_ITEM_DEFAULTS
{
   echo "set feedback off;
      set heading off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE

         L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
         L_run_report     BOOLEAN;
         L_tax_info_rec   OBJ_TAX_INFO_REC   := OBJ_TAX_INFO_REC();
         L_tax_info_tbl   OBJ_TAX_INFO_TBL   := OBJ_TAX_INFO_TBL();

         ERROR_IN_FUNCTION   EXCEPTION;

         L_item_tbl       ITEM_TBL;
         L_dept_tbl       DEPT_TBL;
         L_class_tbl      DEPT_TBL;
         L_subclass_tbl   DEPT_TBL;

         cursor C_MERCH_SKU is
            select f.item,
                   f.dept,
                   f.class,
                   f.subclass
              from dc_hardlines f;


      BEGIN


         open C_MERCH_SKU;
         fetch C_MERCH_SKU BULK COLLECT into L_item_tbl,
                                             L_dept_tbl,
                                             L_class_tbl,
                                             L_subclass_tbl;
         close C_MERCH_SKU;

         FOR i IN 1..L_item_tbl.COUNT LOOP
               L_tax_info_rec.item := L_item_tbl(i);
               L_tax_info_rec.merch_hier_level := 4;
               L_tax_info_rec.merch_hier_value := L_dept_tbl(i);

               L_tax_info_tbl.EXTEND();
               L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
         END LOOP;

         if TAX_SQL.GET_TAX_INFO(L_error_message,
                                 L_tax_info_tbl) = FALSE then
               :GV_script_error := L_error_message;
               raise ERROR_IN_FUNCTION;
         end if;

         if TAX_SQL.INSERT_UPDATE_TAX_SKU(L_error_message,
                                          L_run_report,
                                          L_tax_info_tbl) = FALSE then
            :GV_script_error := L_error_message;
             raise ERROR_IN_FUNCTION;
         end if;
         if UDA_SQL.INSERT_DEFAULTS(L_error_message,
                                    L_item_tbl,
                                    L_dept_tbl,
                                    L_class_tbl,
                                    L_subclass_tbl) = FALSE then
            :GV_script_error := L_error_message;
            raise ERROR_IN_FUNCTION;
         end if;

         if ITEM_CHARGE_SQL.DEFAULT_CHRGS(L_error_message,
                                          L_item_tbl,
                                          L_dept_tbl) = FALSE then
            :GV_script_error := L_error_message;
            raise ERROR_IN_FUNCTION;
         end if;

         COMMIT;

      EXCEPTION
         when ERROR_IN_FUNCTION then
            ROLLBACK;
           :GV_return_code := 1;
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;

      /" | sqlplus -s ${connectStr} >> ${logFile}
   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

   INSERT_ITEM_DEFAULTS
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data of RPM dept aggregation table." "INSERT_ITEM_DEFAULTS" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "INSERT_ITEM_DEFAULTS" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi


LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}