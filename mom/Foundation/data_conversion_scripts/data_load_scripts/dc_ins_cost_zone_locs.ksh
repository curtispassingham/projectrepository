#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_ins_cost_zone_locs.ksh
# Desc: Load the flat file data to staging table dc_ins_cost_zone_locs through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_ins_cost_zone_locs.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

#----------------------------------------------------------------------------------------------------
# Function Name : INSERT_COST_ZONE_LOCS
# Purpose : load wh cost zone location data from data covnversion to cost_zone tables.
#----------------------------------------------------------------------------------------------------
function INSERT_COST_ZONE_LOCS
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE

         L_loc_zone_group_id   WH.WH%TYPE;

         cursor C_LOC_COST_ZONE_GROUP is
            select zone_group_id
              from cost_zone_group
             where cost_level = 'L';

      BEGIN

         open C_LOC_COST_ZONE_GROUP;
         fetch C_LOC_COST_ZONE_GROUP into L_loc_zone_group_id;

         IF C_LOC_COST_ZONE_GROUP%FOUND then
            insert into cost_zone(zone_group_id,
                                  zone_id,
                                  description,
                                  currency_code,
                                  base_cost_ind)
                           select L_loc_zone_group_id,
                                  wh,
                                  wh_name,
                                  currency_code,
                                  'N'
                             from dc_pwh;

            insert into cost_zone_group_loc(zone_group_id,
                                            location,
                                            loc_type,
                                            zone_id)
                                     select L_loc_zone_group_id,
                                            wh,
                                            'W',
                                            wh
                                       from dc_pwh;

            insert into cost_zone_group_loc(zone_group_id,
                                            location,
                                            loc_type,
                                            zone_id)
                                     select L_loc_zone_group_id,
                                            wh,
                                            'W',
                                            physical_wh
                                       from dc_vwh;
         END IF;
         close C_LOC_COST_ZONE_GROUP;

         COMMIT;

      EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi


   # Load the data from data conversion to base tables
  INSERT_COST_ZONE_LOCS
  ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data to main tables." "INSERT_COST_ZONE_LOCS" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "INSERT_COST_ZONE_LOCS" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi


LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}
