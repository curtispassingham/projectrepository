LOAD DATA
REPLACE
INTO TABLE DC_ITEM_COST_HEAD
WHEN 
    ( ITEM != BLANKS )
AND ( SUPPLIER != BLANKS )
AND ( ORIGIN_COUNTRY_ID != BLANKS )
AND ( DELIVERY_COUNTRY_ID != BLANKS )
AND ( PRIM_DLVY_CTRY_IND != BLANKS )
AND ( NIC_STATIC_IND != BLANKS )
AND ( BASE_COST != BLANKS )
AND ( NEGOTIATED_ITEM_COST != BLANKS )
AND ( EXTENDED_BASE_COST != BLANKS )
AND ( INCLUSIVE_COST != BLANKS )
FIELDS TERMINATED BY '|'
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( ITEM                  
 ,SUPPLIER              
 ,ORIGIN_COUNTRY_ID     
 ,DELIVERY_COUNTRY_ID   
 ,PRIM_DLVY_CTRY_IND    
 ,NIC_STATIC_IND        
 ,BASE_COST             
 ,NEGOTIATED_ITEM_COST  
 ,EXTENDED_BASE_COST    
 ,INCLUSIVE_COST )