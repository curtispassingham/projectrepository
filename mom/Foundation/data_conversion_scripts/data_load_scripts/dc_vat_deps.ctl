LOAD DATA
REPLACE
INTO TABLE DC_VAT_DEPS
WHEN ( DEPT != BLANKS )
 AND ( VAT_REGION != BLANKS )
 AND ( VAT_TYPE != BLANKS )
 AND ( VAT_CODE != BLANKS )
 AND ( REVERSE_VAT_IND != BLANKS )
FIELDS TERMINATED BY '|'
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(DEPT
,VAT_REGION
,VAT_TYPE
,VAT_CODE
,REVERSE_VAT_IND)