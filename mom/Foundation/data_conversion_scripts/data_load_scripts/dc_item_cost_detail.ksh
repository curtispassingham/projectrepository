#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_item_cost_detail.ksh
# Desc: Load the flat file data to staging table dc_item_cost_detail through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_item_cost_detail.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_item_cost_detail.ctl"

inputFile='dc_item_cost_detail.dat'
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_FILE
# Purpose : Load data from .dat to data conversion table(using sqlloader).
#----------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion term detail table" "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${OK}
      fi
   fi
}
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_ITEM_COST_DETAIL
# Purpose : load item cost detail data from dc_item_cost_detail to item_cost_detail table.
#----------------------------------------------------------------------------------------------------
function LOAD_ITEM_COST_DETAIL
{
   echo "set feedback off
         set heading off
         set serveroutput on size 1000000

         VARIABLE GV_return_code    NUMBER;
         VARIABLE GV_script_error   CHAR(255);

         EXEC :GV_return_code := 0;

         WHENEVER SQLERROR EXIT 1

         DECLARE

            L_default_tax_type     SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;

            cursor C_TAX_TYPE is
               select default_tax_type
                 from system_options;
         BEGIN

            open C_TAX_TYPE;
            fetch C_TAX_TYPE into L_default_tax_type;
            close C_TAX_TYPE;
            if L_default_tax_type = 'GTAX' then
               insert into item_cost_detail(item,
                                         supplier,
                                         origin_country_id,
                                         delivery_country_id,
                                         cond_type,
                                         cond_value,
                                         applied_on,
                                         comp_rate,
                                         calculation_basis,
                                         recoverable_amount,
                                         modified_taxable_base)
                                  select icd.item,
                                         icd.supplier,
                                         icd.origin_country_id,
                                         icd.delivery_country_id,
                                         icd.cond_type,
                                         icd.cond_value,
                                         icd.applied_on,
                                         icd.comp_rate,
                                         icd.calculation_basis,
                                         icd.recoverable_amount,
                                         nvl(modified_taxable_base,icd.applied_on)
                                    from dc_item_cost_detail icd,
                                         item_cost_head ich
                                   where icd.item = ich.item
                                     and icd.supplier = ich.supplier;
            else
               :GV_script_error := 'Item_Cost_Detail table is not populated as default_tax_type is not GTAX';
               :GV_return_code := 1;
            end if;

         EXCEPTION
            when OTHERS then
               rollback;
               :GV_script_error := SQLERRM;
               :GV_return_code := 1;
         END;
         /

         print :GV_script_error;
         exit  :GV_return_code;
         /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi


#Determine the default tax to verify item_country file is missing.
DEFAULT_TAX_TYPE=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} << EOF
   set heading off feedback off verify off pause off echo off pages 0
   select default_tax_type from system_options;
   exit
   EOF
   `
if [[ DEFAULT_TAX_TYPE != "SVAT" && DEFAULT_TAX_TYPE != "SALES" ]]; then
# Check the data file
retCode=$(checkFile -f $inputFile)
 if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
 elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
 elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
 fi
fi

# Load the file using sqlloader
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_status}
else
   LOG_MESSAGE "Sqlloader function completed successfully." "LOAD_FILE" ${OK} ${logFile} ${pgmName} ${pgmPID}

   # Load the data from data conversion to base tables
   LOAD_ITEM_COST_DETAIL
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data to main tables." "LOAD_ITEM_COST_DETAIL" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_ITEM_COST_DETAIL" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}