#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_vat_region.ksh
# Desc: Load the flat file data to staging table vat_region through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_vat_region.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_vat_region.ctl"

inputFile='dc_vat_region.dat'
#--------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = "" 
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
         return ${OK}
      fi
   fi
}

function LOAD_VAT_REGION
{
   echo "set feedback off;
      set heading off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE

         L_default_tax_type     SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;

         cursor C_VAT_IND is
            select default_tax_type
              from system_options;

      BEGIN
         open C_VAT_IND;
         fetch C_VAT_IND into L_default_tax_type;
         close C_VAT_IND;

         if L_default_tax_type = 'SVAT' then
            insert into vat_region(vat_region,
                                   vat_region_name,
                                   vat_region_type,
                                   acquisition_vat_ind,
                                   reverse_vat_threshold,
                                   vat_calc_type)
                            select vat_region,
                                   vat_region_name,
                                   vat_region_type,
                                   acquisition_vat_ind,
                                   reverse_vat_threshold,
                                   vat_calc_type
                              from dc_vat_region;

            COMMIT;
         else
            :GV_script_error := 'Vat_region table is not populated as default_tax_type is not SVAT';
            :GV_return_code := 1;
         end if;

      EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;

      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;

      " | sqlplus -s ${connectStr} >> ${logFile}
   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}

ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"

LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"

# Check configuration file
checkCfg
chk_status=$?
if [[ ${chk_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${chk_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi

LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOAD_VAT_REGION
   ld_vat_status=$?
   if [[ ${ld_vat_status} -ne ${OK} ]]; then
      LOG_ERROR "Load function get failed during loading from dc table to main table" "LOAD_VAT_REGION" "${ld_vat_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_vat_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
   fi
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi
LOG_MESSAGE "Job ${pgmName}.ksh completed successfully by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"