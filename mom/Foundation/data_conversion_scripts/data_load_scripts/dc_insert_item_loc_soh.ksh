#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_insert_item_loc_soh.ksh
# Desc: Load the data from staging to main tables.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_insert_item_loc_soh.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#----------------------------------------------------------------------------------------------------
# Function Name : INSERT_ITEM_LOC_SOH
# Purpose : load data from data conversion table to base tables.
#----------------------------------------------------------------------------------------------------
function INSERT_ITEM_LOC_SOH
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      BEGIN
         -- insert into item_loc_soh for transaction level items
         -- for warehouses, only insert records for stockholding locations

         -- case 1: unit_cost of the item's primary_supp/primary_country will be used to
         -- default on ITEM_LOC_SOH, convert from supplier currency to local currency
         insert into item_loc_soh(ITEM,
                                  ITEM_PARENT,
                                  ITEM_GRANDPARENT,
                                  LOC,
                                  LOC_TYPE,
                                  AV_COST,
                                  UNIT_COST,
                                  STOCK_ON_HAND,
                                  SOH_UPDATE_DATETIME,
                                  IN_TRANSIT_QTY,
                                  PACK_COMP_INTRAN,
                                  PACK_COMP_SOH,
                                  TSF_RESERVED_QTY,
                                  PACK_COMP_RESV,
                                  TSF_EXPECTED_QTY,
                                  PACK_COMP_EXP,
                                  RTV_QTY,
                                  NON_SELLABLE_QTY,
                                  CUSTOMER_RESV,
                                  CUSTOMER_BACKORDER,
                                  PACK_COMP_CUST_RESV,
                                  PACK_COMP_CUST_BACK,
                                  PACK_COMP_NON_SELLABLE,
                                  AVERAGE_WEIGHT,
                                  PRIMARY_SUPP,
                                  PRIMARY_CNTRY,
                                  CREATE_DATETIME,
                                  LAST_UPDATE_DATETIME,
                                  LAST_UPDATE_ID)
                           select il.ITEM,
                                  im.ITEM_PARENT,
                                  im.ITEM_GRANDPARENT,
                                  il.LOCATION,
                                  il.LOC_TYPE,
                                  CURRENCY_SQL.CONVERT_VALUE('C',
                                                             loc.currency_code,  --location currency
                                                             s.currency_code,    --supplier currency
                                                             isc.unit_cost),     --supplier unit cost
                                  CURRENCY_SQL.CONVERT_VALUE('C',
                                                             loc.currency_code,  --location currency
                                                             s.currency_code,    --supplier currency
                                                             isc.unit_cost),     --supplier unit cost
                                  0, --STOCK_ON_HAND,
                                  NULL, --SOH_UPDATE_DATETIME,
                                  0, --IN_TRANSIT_QTY,
                                  0, --PACK_COMP_INTRAN,
                                  0, --PACK_COMP_SOH,
                                  0, --TSF_RESERVED_QTY,
                                  0, --PACK_COMP_RESV,
                                  0, --TSF_EXPECTED_QTY,
                                  0, --PACK_COMP_EXP,
                                  0, --RTV_QTY,
                                  0, --NON_SELLABLE_QTY,
                                  0, --CUSTOMER_RESV,
                                  0, --CUSTOMER_BACKORDER,
                                  0, --PACK_COMP_CUST_RESV,
                                  0, --PACK_COMP_CUST_BACK,
                                  0, --PACK_COMP_NON_SELLABLE
                                  case
                                  when im.simple_pack_ind = 'Y' and im.catch_weight_ind = 'Y' then
                                     il.average_weight
                                  end, -- set average weight for simple pack catch weight item only
                                  isc.SUPPLIER, -- primary supplier
                                  isc.ORIGIN_COUNTRY_ID, -- primary country
                                  SYSDATE,
                                  SYSDATE,
                                  USER
                             from dc_item_loc il,
                                  item_supp_country isc,
                                  item_master im,
                                  (select store loc,
                                          currency_code
                                     from store
                                    union all
                                   select wh loc,
                                          currency_code
                                     from wh
                                    where stockholding_ind = 'Y') loc,
                                  sups s
                            where il.item = im.item
                              and im.item = isc.item
                              and im.tran_level = im.item_level
                              and loc.loc = il.location
                              and isc.primary_supp_ind = 'Y'
                              and isc.primary_country_ind = 'Y'
                              and s.supplier = isc.supplier;

         -- case 2: for items that do NOT have item/supp/country
         -- insert item_loc_soh with NULL unit_cost and av_cost
         insert into item_loc_soh(ITEM,
                                  ITEM_PARENT,
                                  ITEM_GRANDPARENT,
                                  LOC,
                                  LOC_TYPE,
                                  AV_COST,
                                  UNIT_COST,
                                  STOCK_ON_HAND,
                                  SOH_UPDATE_DATETIME,
                                  IN_TRANSIT_QTY,
                                  PACK_COMP_INTRAN,
                                  PACK_COMP_SOH,
                                  TSF_RESERVED_QTY,
                                  PACK_COMP_RESV,
                                  TSF_EXPECTED_QTY,
                                  PACK_COMP_EXP,
                                  RTV_QTY,
                                  NON_SELLABLE_QTY,
                                  CUSTOMER_RESV,
                                  CUSTOMER_BACKORDER,
                                  PACK_COMP_CUST_RESV,
                                  PACK_COMP_CUST_BACK,
                                  PACK_COMP_NON_SELLABLE,
                                  AVERAGE_WEIGHT,
                                  PRIMARY_SUPP,
                                  PRIMARY_CNTRY,
                                  CREATE_DATETIME,
                                  LAST_UPDATE_DATETIME,
                                  LAST_UPDATE_ID)
                           select il.ITEM,
                                  im.ITEM_PARENT,
                                  im.ITEM_GRANDPARENT,
                                  il.LOCATION,
                                  il.LOC_TYPE,
                                  NULL, --AV_COST
                                  NULL, --UNIT_COST
                                  0, --STOCK_ON_HAND,
                                  NULL, --SOH_UPDATE_DATETIME,
                                  0, --IN_TRANSIT_QTY,
                                  0, --PACK_COMP_INTRAN,
                                  0, --PACK_COMP_SOH,
                                  0, --TSF_RESERVED_QTY,
                                  0, --PACK_COMP_RESV,
                                  0, --TSF_EXPECTED_QTY,
                                  0, --PACK_COMP_EXP,
                                  0, --RTV_QTY,
                                  0, --NON_SELLABLE_QTY,
                                  0, --CUSTOMER_RESV,
                                  0, --CUSTOMER_BACKORDER,
                                  0, --PACK_COMP_CUST_RESV,
                                  0, --PACK_COMP_CUST_BACK,
                                  0, --PACK_COMP_NON_SELLABLE
                                  case
                                  when im.simple_pack_ind = 'Y' and im.catch_weight_ind = 'Y' then
                                     il.average_weight
                                  end, -- set average weight for simple pack catch weight item only
                                  NULL, --PRIMARY_SUPP
                                  NULL, --PRIMARY_CNTRY
                                  SYSDATE,
                                  SYSDATE,
                                  USER
                             from dc_item_loc il,
                                  item_master im,
                                  (select store loc
                                     from store
                                    union all
                                   select wh loc
                                     from wh
                                    where stockholding_ind = 'Y') loc
                            where il.item = im.item
                              and im.tran_level = im.item_level
                              and loc.loc = il.location
                              and not exists (select 'x'
                                                from item_supp_country isc
                                               where isc.item = il.item
                                                 and rownum = 1);
         commit;

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

   INSERT_ITEM_LOC_SOH
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data of RPM dept aggregation table." "INSERT_ITEM_LOC_SOH" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "INSERT_ITEM_LOC_SOH" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi


LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}