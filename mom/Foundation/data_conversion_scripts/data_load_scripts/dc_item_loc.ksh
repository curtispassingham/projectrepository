#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_item_loc.ksh
# Desc: Load the flat file data to staging table dc_item_loc through sqlloader and from staging(dc_item_loc) to main table(country_attrib).

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_item_loc.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_item_loc.ctl"
inputFile='dc_item_loc.dat'
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_FILE
# Purpose : Load data from .dat to staging table(using sqlloader).
#----------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = "" 
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${OK}
      fi
   fi
}
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_ITEM_LOC
# Purpose : load data from staging table to main table.
#----------------------------------------------------------------------------------------------------
function LOAD_ITEM_LOC
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000




      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      BEGIN
         -- insert item_loc records for all items (tran-level or parent items)
         -- for warehouses, only insert stockholding locations

         -- items that have item/supp/country, default primary supp/primary country
         insert into item_loc(ITEM,
                              LOC,
                              ITEM_PARENT,
                              ITEM_GRANDPARENT,
                              LOC_TYPE,
                              UNIT_RETAIL,
                              REGULAR_UNIT_RETAIL,
                              MULTI_UNITS,
                              MULTI_UNIT_RETAIL,
                              MULTI_SELLING_UOM,
                              SELLING_UNIT_RETAIL,
                              SELLING_UOM,
                              PROMO_RETAIL,
                              PROMO_SELLING_RETAIL,
                              PROMO_SELLING_UOM,
                              CLEAR_IND,
                              TAXABLE_IND,
                              LOCAL_ITEM_DESC,
                              LOCAL_SHORT_DESC,
                              TI,
                              HI,
                              STORE_ORD_MULT,
                              STATUS,
                              STATUS_UPDATE_DATE,
                              DAILY_WASTE_PCT,
                              MEAS_OF_EACH,
                              MEAS_OF_PRICE,
                              UOM_OF_PRICE,
                              PRIMARY_VARIANT,
                              PRIMARY_COST_PACK,
                              PRIMARY_SUPP,
                              PRIMARY_CNTRY,
                              RECEIVE_AS_TYPE,
                              CREATE_DATETIME,
                              LAST_UPDATE_DATETIME,
                              LAST_UPDATE_ID,
                              INBOUND_HANDLING_DAYS,
                              SOURCE_METHOD,
                              SOURCE_WH,
                              STORE_PRICE_IND,
                              RPM_IND)
                       select il.ITEM,
                              il.LOCATION,
                              im.ITEM_PARENT,
                              im.ITEM_GRANDPARENT,
                              il.LOC_TYPE,
                              il.SELLING_UNIT_RETAIL, --UNIT_RETAIL
                              il.SELLING_UNIT_RETAIL, --REGULAR_UNIT_RETAIL
                              il.MULTI_UNITS,
                              il.MULTI_UNIT_RETAIL,
                              il.MULTI_SELLING_UOM,
                              il.SELLING_UNIT_RETAIL,
                              il.SELLING_UOM,
                              NULL,                   -- PROMO_RETAIL
                              NULL,                   --PROMO_SELLING_RETAIL
                              NULL,                   --PROMO_SELLING_UOM
                              'N',                    --CLEAR_IND
                              DECODE(il.LOC_TYPE, 'W', 'N', NVL(il.TAXABLE_IND, 'N')), --TAXABLE_IND, always 'N' for wh, default to 'N'
                              NVL(il.LOCAL_ITEM_DESC, im.ITEM_DESC),
                              NVL(il.LOCAL_SHORT_DESC, DECODE(il.LOCAL_ITEM_DESC, NULL, im.SHORT_DESC, RTRIM(SUBSTRB(il.LOCAL_ITEM_DESC,1,120)))), --LOCAL_SHORT_DESC
                              il.TI,
                              il.HI,
                              il.STORE_ORD_MULT,
                              'A',                    --STATUS
                              SYSDATE,                --STATUS_UPDATE_DATE
                              NULL,                   --DAILY_WASTE_PCT
                              il.MEAS_OF_EACH,
                              il.MEAS_OF_PRICE,
                              il.UOM_OF_PRICE,
                              NULL,                   --PRIMARY_VARIANT
                              il.PRIMARY_COST_PACK,
                              isc.SUPPLIER,           --PRIMARY_SUPP, default to item's primary supplier
                              isc.ORIGIN_COUNTRY_ID,  --PRIMARY_CNTRY, default to item's primary country
                              DECODE(il.LOC_TYPE,'S',NULL,
                                     DECODE(NVL(im.pack_type,'V'), 'B', im.order_as_type, NULL)), --RECEIVE_AS_TYPE, buyer pack only
                              SYSDATE,                --CREATE_DATETIME
                              SYSDATE,                --LAST_UPDATE_DATETIME
                              USER,                   --LAST_UPDATE_ID
                              DECODE(il.LOC_TYPE, 'S', NULL, il.INBOUND_HANDLING_DAYS), --wh only
                              il.SOURCE_METHOD,
                              DECODE(il.SOURCE_METHOD, 'W', il.SOURCE_WH, NULL), --SOURCE_WH
                              'N',                    --STORE_PRICE_IND
                              'N'                     --RPM_IND
                         from dc_item_loc il,
                              item_supp_country isc,
                              item_master im,
                              (select store loc
                                 from store
                                union all
                               select wh loc
                                 from wh
                                where stockholding_ind = 'Y') loc
                        where il.item = isc.item
                          and il.item = im.item
                          and isc.primary_supp_ind = 'Y'
                          and isc.primary_country_ind = 'Y'
                          and il.location = loc.loc;

         -- items that do not have item/supp/country, set primary supp/primary country to NULL
         insert into item_loc(ITEM,
                              LOC,
                              ITEM_PARENT,
                              ITEM_GRANDPARENT,
                              LOC_TYPE,
                              UNIT_RETAIL,
                              REGULAR_UNIT_RETAIL,
                              MULTI_UNITS,
                              MULTI_UNIT_RETAIL,
                              MULTI_SELLING_UOM,
                              SELLING_UNIT_RETAIL,
                              SELLING_UOM,
                              PROMO_RETAIL,
                              PROMO_SELLING_RETAIL,
                              PROMO_SELLING_UOM,
                              CLEAR_IND,
                              TAXABLE_IND,
                              LOCAL_ITEM_DESC,
                              LOCAL_SHORT_DESC,
                              TI,
                              HI,
                              STORE_ORD_MULT,
                              STATUS,
                              STATUS_UPDATE_DATE,
                              DAILY_WASTE_PCT,
                              MEAS_OF_EACH,
                              MEAS_OF_PRICE,
                              UOM_OF_PRICE,
                              PRIMARY_VARIANT,
                              PRIMARY_COST_PACK,
                              PRIMARY_SUPP,
                              PRIMARY_CNTRY,
                              RECEIVE_AS_TYPE,
                              CREATE_DATETIME,
                              LAST_UPDATE_DATETIME,
                              LAST_UPDATE_ID,
                              INBOUND_HANDLING_DAYS,
                              SOURCE_METHOD,
                              SOURCE_WH,
                              STORE_PRICE_IND,
                              RPM_IND)
                       select il.ITEM,
                              il.LOCATION,
                              im.ITEM_PARENT,
                              im.ITEM_GRANDPARENT,
                              il.LOC_TYPE,
                              il.SELLING_UNIT_RETAIL, --UNIT_RETAIL
                              il.SELLING_UNIT_RETAIL, --REGULAR_UNIT_RETAIL
                              il.MULTI_UNITS,
                              il.MULTI_UNIT_RETAIL,
                              il.MULTI_SELLING_UOM,
                              il.SELLING_UNIT_RETAIL,
                              il.SELLING_UOM,
                              NULL,                   -- PROMO_RETAIL
                              NULL,                   --PROMO_SELLING_RETAIL
                              NULL,                   --PROMO_SELLING_UOM
                              'N',                    --CLEAR_IND
                              DECODE(il.LOC_TYPE, 'W', 'N', NVL(il.TAXABLE_IND, 'N')), --TAXABLE_IND, always 'N' for wh, default to 'N'
                              NVL(il.LOCAL_ITEM_DESC, im.ITEM_DESC),
                              NVL(il.LOCAL_SHORT_DESC, DECODE(il.LOCAL_ITEM_DESC, NULL, im.SHORT_DESC,RTRIM(SUBSTRB(il.LOCAL_ITEM_DESC,1,120)))), --LOCAL_SHORT_DESC
                              il.TI,
                              il.HI,
                              il.STORE_ORD_MULT,
                              'A',                    --STATUS
                              SYSDATE,                --STATUS_UPDATE_DATE
                              NULL,                   --DAILY_WASTE_PCT
                              il.MEAS_OF_EACH,
                              il.MEAS_OF_PRICE,
                              il.UOM_OF_PRICE,
                              NULL,                   --PRIMARY_VARIANT
                              il.PRIMARY_COST_PACK,
                              NULL,                   --PRIMARY_SUPP
                              NULL,                   --PRIMARY_CNTRY
                              DECODE(il.LOC_TYPE,'S',NULL,
                                     DECODE(NVL(im.pack_type,'V'), 'B', im.order_as_type, NULL)), --RECEIVE_AS_TYPE, buyer pack only
                              SYSDATE,                --CREATE_DATETIME
                              SYSDATE,                --LAST_UPDATE_DATETIME
                              USER,                   --LAST_UPDATE_ID
                              DECODE(il.LOC_TYPE, 'S', NULL, il.INBOUND_HANDLING_DAYS), --wh only
                              il.SOURCE_METHOD,
                              DECODE(il.SOURCE_METHOD, 'W', il.SOURCE_WH, NULL), --SOURCE_WH
                              'N',                    --STORE_PRICE_IND
                              'N'                     --RPM_IND
                         from dc_item_loc il,
                              item_master im,
                              (select store loc
                                 from store
                                union all
                               select wh loc
                                 from wh
                                where stockholding_ind = 'Y') loc
                        where il.item = im.item
                          and il.location = loc.loc
                          and not exists (select 'x'
                                            from item_supp_country isc
                                           where isc.item = il.item
                                             and rownum = 1);
         commit;

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}.${pgmPID}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

# Check the data file
retCode=$(checkFile -f $inputFile)
if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Load the file using sqlloader
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_status}
else
   LOG_MESSAGE "Sqlloader function completed successfully." "LOAD_FILE" ${OK} ${logFile} ${pgmName} ${pgmPID}
   
   LOAD_ITEM_LOC
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data from dc_item_loc to main tables." "LOAD_ITEM_LOC" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_ITEM_LOC" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}