#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_fashion_sku.ksh
# Desc: Load the flat file data to staging table dc_fashion_sku through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_fashion_sku.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

function INSERT_FASHION_DEFAULTS
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE

         L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
         L_run_report     BOOLEAN;
         L_tax_info_rec   OBJ_TAX_INFO_REC   := OBJ_TAX_INFO_REC();
         L_tax_info_tbl   OBJ_TAX_INFO_TBL   := OBJ_TAX_INFO_TBL();

         ERROR_IN_FUNCTION   EXCEPTION;

         L_item_tbl       ITEM_TBL;
         L_dept_tbl       DEPT_TBL;
         L_class_tbl      DEPT_TBL;
         L_subclass_tbl   DEPT_TBL;

         -- Join style with sku to ensure getting styles that were inserted into item_master
        cursor C_MERCH_ALL is
            select s.item,
                   s.dept,
                   s.class,
                   s.subclass
              from dc_fashion_sku f,
                   dc_style s
             where f.item_parent = s.item
               and f.primary_sku_ind = 'Y'
              union
            select f.item,
                   s.dept,
                   s.class,
                   s.subclass
              from dc_fashion_sku f,
                   dc_style s
             where f.item_parent = s.item;

      BEGIN

         open C_MERCH_ALL;
         fetch C_MERCH_ALL BULK COLLECT into L_item_tbl,
                                             L_dept_tbl,
                                             L_class_tbl,
                                             L_subclass_tbl;
         close C_MERCH_ALL;

         FOR i IN 1..L_item_tbl.COUNT LOOP
               L_tax_info_rec.item := L_item_tbl(i);
               L_tax_info_rec.merch_hier_level := 4;
               L_tax_info_rec.merch_hier_value := L_dept_tbl(i);

               L_tax_info_tbl.EXTEND();
               L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
         END LOOP;

         if TAX_SQL.GET_TAX_INFO(L_error_message,
                                 L_tax_info_tbl) = FALSE then
               :GV_script_error := L_error_message;
               raise ERROR_IN_FUNCTION;
         end if;

         if TAX_SQL.INSERT_UPDATE_TAX_SKU(L_error_message,
                                          L_run_report,
                                          L_tax_info_tbl) = FALSE then
            :GV_script_error := L_error_message;
             raise ERROR_IN_FUNCTION;
         end if;

         if UDA_SQL.INSERT_DEFAULTS(L_error_message,
                                    L_item_tbl,
                                    L_dept_tbl,
                                    L_class_tbl,
                                    L_subclass_tbl) = FALSE then
            :GV_script_error := L_error_message;
            raise ERROR_IN_FUNCTION;
         end if;

         if ITEM_CHARGE_SQL.DEFAULT_CHRGS(L_error_message,
                                          L_item_tbl,
                                          L_dept_tbl) = FALSE then
            :GV_script_error := L_error_message;
            raise ERROR_IN_FUNCTION;
         end if;

         commit;

      EXCEPTION
         when ERROR_IN_FUNCTION then
            rollback;
            :GV_return_code := 1;
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}

ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"

LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"

# Check configuration file
checkCfg
chk_status=$?
if [[ ${chk_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${chk_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi

# Load from dc table to main table.
INSERT_FASHION_DEFAULTS
ld_trm_status=$?
if [[ ${ld_trm_status} -ne ${OK} ]]; then
   LOG_ERROR "Load function get failed during loading from dc table to main table" "INSERT_FASHION_DEFAULTS" "${ld_trm_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_trm_status}
else
   LOG_MESSAGE "Load Function completed successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi
LOG_MESSAGE "Job ${pgmName}.ksh completed successfully by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"