#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_grocery_variant.ksh
# Desc: Load the flat file data to staging table dc_grocery_variant through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_grocery_variant.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_grocery_variant.ctl"

inputFile='dc_grocery_variant.dat'
#--------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
         return ${OK}
      fi
   fi
}

function LOAD_GROCERY_VARIANT
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE
         -- default attributes for grocery variant
         L_item_level                  ITEM_MASTER.ITEM_LEVEL%TYPE := 3;
         L_tran_level                  ITEM_MASTER.TRAN_LEVEL%TYPE := 2;
         L_item_aggregate_ind          ITEM_MASTER.ITEM_AGGREGATE_IND%TYPE := 'N';
         L_diff_1_aggregate_ind        ITEM_MASTER.DIFF_1_AGGREGATE_IND%TYPE := 'N';
         L_diff_2_aggregate_ind        ITEM_MASTER.DIFF_2_AGGREGATE_IND%TYPE := 'N';
         L_diff_3_aggregate_ind        ITEM_MASTER.DIFF_3_AGGREGATE_IND%TYPE := 'N';
         L_diff_4_aggregate_ind        ITEM_MASTER.DIFF_4_AGGREGATE_IND%TYPE := 'N';
         L_status                      ITEM_MASTER.STATUS%TYPE := 'A';

         L_primary_ref_item_ind        ITEM_MASTER.PRIMARY_REF_ITEM_IND%TYPE := 'N';
         L_pack_ind                    ITEM_MASTER.PACK_IND%TYPE := 'N';
         L_simple_pack_ind             ITEM_MASTER.SIMPLE_PACK_IND%TYPE := 'N';
         L_contains_inner_ind          ITEM_MASTER.CONTAINS_INNER_IND%TYPE := 'N';
         L_item_xform_ind              ITEM_MASTER.ITEM_XFORM_IND%TYPE := 'N';
         L_inventory_ind               ITEM_MASTER.INVENTORY_IND%TYPE := 'Y';
         L_sellable_ind                ITEM_MASTER.SELLABLE_IND%TYPE := 'Y';
         L_orderable_ind               ITEM_MASTER.ORDERABLE_IND%TYPE := 'Y';
         L_const_dimen_ind             ITEM_MASTER.CONST_DIMEN_IND%TYPE := 'N';
         L_gift_wrap_ind               ITEM_MASTER.GIFT_WRAP_IND%TYPE := 'N';
         L_ship_alone_ind              ITEM_MASTER.SHIP_ALONE_IND%TYPE := 'N';

         L_create_datetime             ITEM_MASTER.CREATE_DATETIME%TYPE := sysdate;
         L_last_update_id              ITEM_MASTER.LAST_UPDATE_ID%TYPE := user;
         L_last_update_datetime        ITEM_MASTER.LAST_UPDATE_DATETIME%TYPE := sysdate;
         L_perishable_ind              ITEM_MASTER.PERISHABLE_IND%TYPE := 'N';

         -- The following attributes are defaulted from product (dc_product external table):
         -- diff_1
         -- diff_2
         -- diff_3
         -- diff_4
         -- cost_zone_group_id
         -- uom_conv_factor
         -- standard_uom
         -- store_ord_mult, plus:
         L_merchandise_ind             ITEM_MASTER.MERCHANDISE_IND%TYPE := 'Y';
         L_forecast_ind                ITEM_MASTER.FORECAST_IND%TYPE := 'Y';
         L_catch_weight_ind            ITEM_MASTER.CATCH_WEIGHT_IND%TYPE := 'N';

         -- The following attributes are defaulted from product line (dc_product_line external table):
         -- dept
         -- class
         -- subclass

      BEGIN

        insert into ITEM_MASTER (item,
                                 item_number_type,
                                 format_id,
                                 prefix,
                                 item_parent,
                                 item_grandparent,
                                 item_level,
                                 tran_level,
                                 item_aggregate_ind,
                                 diff_1,
                                 diff_2,
                                 diff_3,
                                 diff_4,
                                 diff_1_aggregate_ind,
                                 diff_2_aggregate_ind,
                                 diff_3_aggregate_ind,
                                 diff_4_aggregate_ind,
                                 dept,
                                 class,
                                 subclass,
                                 cost_zone_group_id,
                                 status,
                                 item_desc,
                                 short_desc,
                                 desc_up,
                                 item_desc_secondary,
                                 store_ord_mult,
                                 standard_uom,
                                 uom_conv_factor,
                                 primary_ref_item_ind,
                                 pack_ind,
                                 simple_pack_ind,
                                 contains_inner_ind,
                                 item_xform_ind,
                                 inventory_ind,
                                 sellable_ind,
                                 orderable_ind,
                                 merchandise_ind,
                                 forecast_ind,
                                 catch_weight_ind,
                                 const_dimen_ind,
                                 gift_wrap_ind,
                                 ship_alone_ind,
                                 create_datetime,
                                 last_update_id,
                                 last_update_datetime,
                                 aip_case_type,
                                 comments,
                                 perishable_ind,
                                 product_classification,
                                 brand_name)
                          select v.item,
                                 v.item_number_type,
                                 v.format_id,
                                 v.prefix,
                                 v.item_parent,
                                 v.item_grandparent,
                                 L_item_level,
                                 L_tran_level,
                                 L_item_aggregate_ind,
                                 -- fill in diff1, diff2, diff3, diff4
                                 coalesce(p.diff_1,p.diff_2,p.diff_3,p.diff_4) diff1,
                                 case
                                 when nvl(p.diff_1,'-1') != '-1' then    -- diff1 is not null, diff1<-diff1
                                    coalesce(p.diff_2,p.diff_3,p.diff_4)
                                 when nvl(p.diff_2,'-1') != '-1' then    -- diff1 is NULL, but diff2 is not null, diff1<-diff2
                                    nvl(p.diff_3,p.diff_4)
                                 when nvl(p.diff_3,'-1') != '-1' then     -- diff1 is null and diff2 is null, but diff3 is not null, diff1<-diff3
                                    p.diff_4
                                 end diff2,
                                 case
                                 when (nvl(p.diff_1,'-1') != '-1' and nvl(p.diff_2,'-1') != '-1') then
                                    nvl(p.diff_3,p.diff_4)
                                 when (nvl(p.diff_1,'-1') != '-1' and nvl(p.diff_3,'-1') != '-1') then
                                    p.diff_4
                                 when (nvl(p.diff_2,'-1') != '-1' and nvl(p.diff_3,'-1') != '-1') then
                                    p.diff_4
                                 end diff3,
                                 case
                                 when (nvl(p.diff_1,'-1') != '-1' and nvl(p.diff_2,'-1') != '-1'
                                       and nvl(p.diff_3,'-1') != '-1') then
                                    p.diff_4
                                 end diff4,
                                 -- no diff aggregate at grocery variant level
                                 L_diff_1_aggregate_ind,
                                 L_diff_2_aggregate_ind,
                                 L_diff_3_aggregate_ind,
                                 L_diff_4_aggregate_ind,
                                 pl.dept,
                                 pl.class,
                                 pl.subclass,
                                 p.cost_zone_group_id,
                                 L_status,
                                 v.item_desc,
                                 nvl(v.short_desc,RTRIM(substrb(v.item_desc,1,120))),
                                 upper(v.item_desc),
                                 v.item_desc_secondary,
                                 p.store_ord_mult,
                                 p.standard_uom,
                                 p.uom_conv_factor,
                                 nvl(v.primary_ref_item_ind, L_primary_ref_item_ind),
                                 L_pack_ind,
                                 L_simple_pack_ind,
                                 L_contains_inner_ind,
                                 L_item_xform_ind,
                                 L_inventory_ind,
                                 L_sellable_ind,
                                 L_orderable_ind,
                                 nvl(p.merchandise_ind, L_merchandise_ind),
                                 nvl(p.forecast_ind, L_forecast_ind),
                                 nvl(p.catch_weight_ind, L_catch_weight_ind),
                                 L_const_dimen_ind,
                                 L_gift_wrap_ind,
                                 L_ship_alone_ind,
                                 L_create_datetime,
                                 L_last_update_id,
                                 L_last_update_datetime,
                                 v.aip_case_type,
                                 v.comments,
                                 NVL(p.perishable_ind,L_perishable_ind),
                                 v.product_classification,
                                 v.brand_name
                            from dc_grocery_variant v,
                                 dc_product p,
                                 dc_product_line pl
                           where v.item_parent = p.item
                             and v.item_grandparent = pl.item
                             and p.item_parent = pl.item;

           DECLARE
             CURSOR c_items
             IS
               SELECT item FROM DC_GROCERY_VARIANT;
             l_items ITEM_TBL;
             L_error_message rtk_errors.rtk_text%type;
           BEGIN
             OPEN c_items;
             LOOP
               FETCH c_items bulk collect INTO l_items limit 1000;
               IF l_items.count                                                 > 0 THEN
                 IF ITEM_APPROVAL_SQL.SET_ALC_ITEM_TYPE(L_error_message,l_items)=false THEN
                   raise_application_error(-20001,L_error_message);
                 END IF;
               END IF;
               EXIT
             WHEN c_items%notfound;
             END LOOP;
           END;


         commit;

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}

ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${LOGFILE}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
  LOG_MESSAGE "Connection established successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi

LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"

# Check configuration file
checkCfg
chk_status=$?
if [[ ${chk_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${chk_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the data file
retCode=$(checkFile -f $inputFile)
if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Load the file
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${LOGFILE}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOAD_GROCERY_VARIANT
   ld_trm_status=$?
   if [[ ${ld_trm_status} -ne ${OK} ]]; then
      LOG_ERROR "Load function get failed during loading from dc table to main table" "LOAD_GROCERY_VARIANT" "${ld_trm_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_trm_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
   fi
fi
# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi
LOG_MESSAGE "Job ${pgmName}.ksh completed successfully by user - ${USER}" "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"