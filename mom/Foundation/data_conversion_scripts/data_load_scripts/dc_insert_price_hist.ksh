#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_insert_price_hist.ksh
# Desc: Load the data from staging to main tables.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_insert_price_hist.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#----------------------------------------------------------------------------------------------------
# Function Name : INSERT_PRICE_HIST
# Purpose : load data from data conversion table to base tables.
#----------------------------------------------------------------------------------------------------
function INSERT_PRICE_HIST
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE
         L_vdate              PERIOD.VDATE%TYPE := GET_VDATE;
         L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
         L_system_options_row SYSTEM_OPTIONS%ROWTYPE;

         ERROR_IN_FUNCTION EXCEPTION;

      BEGIN
         if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                                  L_system_options_row) = FALSE then
            :GV_script_error := L_error_message;
            raise ERROR_IN_FUNCTION;
         end if;

         --- use unit_cost on price_hist record for the new item (loc=0), which is in primary currency
         --- perform the insert where no conversion is necessary (local currency = primary currency)
         insert into price_hist(tran_type,
                                reason,
                                item,
                                loc,
                                loc_type,
                                unit_cost,
                                unit_retail,
                                selling_unit_retail,
                                selling_uom,
                                multi_units,
                                multi_unit_retail,
                                multi_selling_uom,
                                action_date,
                                post_date)
                         select 0,
                                0,
                                il.item,
                                il.location,
                                il.loc_type,
                                ph.unit_cost,
                                il.selling_unit_retail,
                                il.selling_unit_retail,
                                il.selling_uom,
                                il.multi_units,
                                il.multi_unit_retail,
                                il.multi_selling_uom,
                                L_vdate,
                                L_vdate
                           from dc_item_loc il,
                                (select store loc,
                                        currency_code
                                   from store
                                  union all
                                 select wh loc,
                                        currency_code
                                   from wh
                                  where stockholding_ind = 'Y') loc,
                                (select distinct item,
                                        unit_cost
                                   from price_hist ph
                                  where tran_type = 0
                                    and reason = 0
                                    and loc = 0) ph
                          where ph.item = il.item
                            and il.location = loc.loc
                            and loc.currency_code = L_system_options_row.currency_code;

         --- perform the insert where unit cost is converted (local curr != primary curr)
         insert into price_hist(tran_type,
                                reason,
                                item,
                                loc,
                                loc_type,
                                unit_cost,
                                unit_retail,
                                selling_unit_retail,
                                selling_uom,
                                multi_units,
                                multi_unit_retail,
                                multi_selling_uom,
                                action_date,
                                post_date)
                         select 0,
                                0,
                                il.item,
                                il.location,
                                il.loc_type,
                                -- unit_cost on price_hist for the 0 record is held in primary currency
                                -- need to convert this to local currency
                                CURRENCY_SQL.CONVERT_VALUE('C',
                                                           loc.currency_code,
                                                           L_system_options_row.currency_code,
                                                           ph.unit_cost),
                                il.selling_unit_retail,
                                il.selling_unit_retail,
                                il.selling_uom,
                                il.multi_units,
                                il.multi_unit_retail,
                                il.multi_selling_uom,
                                L_vdate,
                                L_vdate
                           from dc_item_loc il,
                                (select store loc,
                                        currency_code
                                   from store
                                  union all
                                 select wh loc,
                                        currency_code
                                   from wh
                                  where stockholding_ind = 'Y') loc,
                                (select distinct item,
                                        unit_cost
                                   from price_hist ph
                                  where tran_type = 0
                                    and reason = 0
                                    and loc = 0) ph
                          where ph.item = il.item
                            and il.location = loc.loc
                            and loc.currency_code != L_system_options_row.currency_code;

         commit;

      EXCEPTION
         when ERROR_IN_FUNCTION then
            rollback;
            :GV_return_code := 1;
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

   INSERT_PRICE_HIST
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data of RPM dept aggregation table." "INSERT_PRICE_HIST" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "INSERT_PRICE_HIST" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi


LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}