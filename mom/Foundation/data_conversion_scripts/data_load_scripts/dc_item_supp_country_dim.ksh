#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_item_supp_country_dim.ksh
# Desc: Load the flat file data to staging table dc_item_supp_country_dim through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_item_supp_country_dim.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_item_supp_country_dim.ctl"

inputFile='dc_item_supp_country_dim.dat'
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_FILE
# Purpose : Load data from .dat to data conversion table(using sqlloader).
#----------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = "" 
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion table" "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${OK}
      fi
   fi
}
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_ITEM_SUPP_COUNTRY_DIM
# Purpose : load item supp country dimension data from dc_item_supp_country_dim to item_supp_country_dim table.
#----------------------------------------------------------------------------------------------------
function LOAD_ITEM_SUPP_COUNTRY_DIM
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1


      DECLARE
         L_default_weight_uom         SYSTEM_OPTIONS.DEFAULT_WEIGHT_UOM%TYPE;
         L_default_dimension_uom      SYSTEM_OPTIONS.DEFAULT_DIMENSION_UOM%TYPE;

         cursor C_SYSTEM_OPTIONS is
            select default_weight_uom,
                   default_dimension_uom
              from system_options;

      BEGIN

         open C_SYSTEM_OPTIONS;
         fetch C_SYSTEM_OPTIONS into L_default_weight_uom,
                                     L_default_dimension_uom;
         close C_SYSTEM_OPTIONS;

         insert into item_supp_country_dim(item,
                                           supplier,
                                           origin_country,
                                           dim_object,
                                           presentation_method,
                                           length,
                                           width,
                                           height,
                                           lwh_uom,
                                           weight,
                                           net_weight,
                                           weight_uom,
                                           liquid_volume,
                                           liquid_volume_uom,
                                           stat_cube,
                                           tare_weight,
                                           tare_type,
                                           create_datetime,
                                           last_update_id,
                                           last_update_datetime)
                                    select isc.item,
                                           isc.supplier,
                                           isc.origin_country_id,
                                           isc.dim_object,
                                           isc.presentation_method,
                                           isc.length ,
                                           isc.width,
                                           isc.height,
                                           nvl(isc.lwh_uom, L_default_dimension_uom),
                                           isc.weight,
                                           isc.net_weight,
                                           nvl(isc.weight_uom, L_default_weight_uom),
                                           isc.liquid_volume,
                                           isc.liquid_volume_uom,
                                           isc.stat_cube,
                                           isc.tare_weight,
                                           isc.tare_type,
                                           sysdate,
                                           user,
                                           sysdate
                                      from dc_item_supp_country_dim isc,
                                           sups s
                                     where isc.supplier = s.supplier;

      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

# Check the data file
retCode=$(checkFile -f $inputFile)
if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Load the file using sqlloader
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_status}
else
   LOG_MESSAGE "Sqlloader function completed successfully." "LOAD_FILE" ${OK} ${logFile} ${pgmName} ${pgmPID}
   
   # Load the data from data conversion to base tables 
   LOAD_ITEM_SUPP_COUNTRY_DIM
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data to main tables." "LOAD_ITEM_SUPP_COUNTRY_DIM" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_ITEM_SUPP_COUNTRY_DIM" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}
