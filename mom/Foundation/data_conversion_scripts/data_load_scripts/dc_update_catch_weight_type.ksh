#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_update_catch_weight_type.ksh
# Desc: Load the data from staging to main tables.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_update_catch_weight_type.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#----------------------------------------------------------------------------------------------------
# Function Name : UPDATE_CATCH_WEIGHT_TYPE
# Purpose : load data from data conversion table to base tables.
#----------------------------------------------------------------------------------------------------
function UPDATE_CATCH_WEIGHT_TYPE
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000
     
      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);
      
      EXEC :GV_return_code := 0;
      
      WHENEVER SQLERROR EXIT 1

      DECLARE
         ERROR_IN_FUNCTION EXCEPTION;

      BEGIN
         update item_master i
            set catch_weight_type = '2'
          where simple_pack_ind   = 'Y' 
            and catch_weight_ind  = 'Y'
            and pack_type         = 'V'
            and order_type        = 'V'
            and exists (select 'x'
           from dc_orderable_pack d
          where d.item = i.item
            and rownum = 1)  -- only items from the current load
            and exists (select 'x'
                          from packitem p, item_master i2, uom_class u
                         where p.pack_no       = i.item
                           and i2.item         = p.item
                           and i2.sale_type    = 'L'
                           and i2.standard_uom = u.uom
                           and u.uom_class     = 'MASS'
                           and rownum          = 1);

         update item_master i
            set catch_weight_type = '4'
          where simple_pack_ind   = 'Y' 
            and catch_weight_ind  = 'Y'
            and pack_type         = 'V'
            and order_type        = 'V'
            and exists (select 'x'
           from dc_orderable_pack d
          where d.item = i.item
            and rownum = 1)  -- only items from the current load
            and exists (select 'x'
                          from  packitem p, item_master i2
                         where p.pack_no       = i.item
                           and i2.item         = p.item
                           and i2.sale_type    = 'V'
                           and i2.standard_uom = 'EA'
                           and rownum          = 1);

         commit;
      
      EXCEPTION
         when ERROR_IN_FUNCTION then
            rollback;
            :GV_return_code := 1;
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /
      
      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

   UPDATE_CATCH_WEIGHT_TYPE
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data of RPM dept aggregation table." "UPDATE_CATCH_WEIGHT_TYPE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "UPDATE_CATCH_WEIGHT_TYPE" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi


LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}