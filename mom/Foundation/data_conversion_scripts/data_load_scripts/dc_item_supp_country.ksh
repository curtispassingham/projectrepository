#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_item_supp_country.ksh
# Desc: Load the flat file data to staging table dc_item_supp_country through sqlloader.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_item_supp_country.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
CTLFILE="${scriptDir}/dc_item_supp_country.ctl"
inputFile='dc_item_supp_country.dat'
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_FILE
# Purpose : Load data from .dat to data conversion table(using sqlloader).
#----------------------------------------------------------------------------------------------------
function LOAD_FILE
{
   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${data_dir}/${inputFile%.*}
   sqlldrFile=${sqlldrFile##*/}
   sqlldrLog=${log_dir}/$sqlldrFile.${dtStamp}.log
   sqlldrDsc=${log_dir}/$sqlldrFile.dsc
   sqlldrBad=${log_dir}/$sqlldrFile.bad

   sqlldr userid=${connectStr} control=${CTLFILE} log=${sqlldrLog} data=${data_dir}/${inputFile} bad=${sqlldrBad} discard=${sqlldrDsc}
   sqlldr_status=$?
   if [[ ${sqlldr_status} -eq 1 ||  ${sqlldr_status} -eq 3 ]]; then
      LOG_ERROR "Error while loading file ${inputFile}. See ${sqlldrLog} for details." "LOAD_FILE" "${sqlldr_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      return ${FATAL}
   else
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` = ""
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` = "" 
           || `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all WHEN clauses were failed."` = "" ]]; then
         LOG_MESSAGE "Some records are not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${NON_FATAL}
      else
         LOG_MESSAGE "Completed loading file to staging data conversion term detail table" "LOAD_FILE" ${OK} ${logFile} ${pgmName}
         return ${OK}
      fi
   fi
}
#----------------------------------------------------------------------------------------------------
# Function Name : LOAD_ITEM_SUPP_COUNTRY
# Purpose : load item supp country data from dc_item_supp_country to item_supp_country table.
#----------------------------------------------------------------------------------------------------
function LOAD_ITEM_SUPP_COUNTRY
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1

      DECLARE
         L_default_packing_method     SYSTEM_OPTIONS.DEFAULT_PACKING_METHOD%TYPE;
         L_round_lvl                  SYSTEM_OPTIONS.ROUND_LVL%TYPE;
         L_round_to_case_pct          SYSTEM_OPTIONS.ROUND_TO_CASE_PCT%TYPE;
         L_round_to_inner_pct         SYSTEM_OPTIONS.ROUND_TO_INNER_PCT%TYPE;
         L_round_to_layer_pct         SYSTEM_OPTIONS.ROUND_TO_LAYER_PCT%TYPE;
         L_round_to_pallet_pct        SYSTEM_OPTIONS.ROUND_TO_PALLET_PCT%TYPE;
         L_default_tax_type           SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;

         cursor C_SYSTEM_OPTIONS is
            select default_packing_method,
                   round_lvl,
                   round_to_case_pct,
                   round_to_inner_pct,
                   round_to_layer_pct,
                   round_to_pallet_pct,
                   default_tax_type
              from system_options;

      BEGIN

         open C_SYSTEM_OPTIONS;
         fetch C_SYSTEM_OPTIONS into L_default_packing_method,
                                     L_round_lvl,
                                     L_round_to_case_pct,
                                     L_round_to_inner_pct,
                                     L_round_to_layer_pct,
                                     L_round_to_pallet_pct,
                                     L_default_tax_type;
         close C_SYSTEM_OPTIONS;

         insert into item_supp_country(item,
                                       supplier,
                                       origin_country_id,
                                       unit_cost,
                                       supp_pack_size,
                                       inner_pack_size,
                                       round_lvl,
                                       round_to_inner_pct,
                                       round_to_case_pct,
                                       round_to_layer_pct,
                                       round_to_pallet_pct,
                                       min_order_qty,
                                       max_order_qty,
                                       primary_supp_ind,
                                       primary_country_ind,
                                       ti,
                                       hi,
                                       cost_uom,
                                       lead_time,
                                       packing_method,
                                       default_uop,
                                       create_datetime,
                                       last_update_id,
                                       last_update_datetime,
                                       negotiated_item_cost,
                                       extended_base_cost,
                                       inclusive_cost,
                                       base_cost)
                                select isc.item,
                                       isc.supplier,
                                       isc.origin_country_id,
                                       isc.unit_cost,
                                       isc.supp_pack_size,
                                       isc.inner_pack_size,
                                       nvl(isc.round_lvl,L_round_lvl),
                                       nvl(isc.round_to_inner_pct,L_round_to_inner_pct),
                                       nvl(isc.round_to_case_pct,L_round_to_case_pct),
                                       nvl(isc.round_to_layer_pct,L_round_to_layer_pct),
                                       nvl(isc.round_to_pallet_pct,L_round_to_pallet_pct),
                                       min_order_qty,
                                       max_order_qty,
                                       ms.primary_supp_ind,
                                       nvl(isc.primary_country_ind,decode(row_number() over(partition by ISC.ITEM, ISC.SUPPLIER order by ISC.ORIGIN_COUNTRY_ID),1,'Y','N')),
                                       isc.ti,
                                       isc.hi,
                                       nvl(isc.cost_uom,im.standard_uom),
                                       nvl(isc.lead_time, s.default_item_lead_time),
                                       nvl(isc.packing_method, L_default_packing_method),
                                       nvl(isc.default_uop, 'C'),
                                       sysdate,
                                       user,
                                       sysdate,
                                       DECODE(L_default_tax_type,'GTAX',isc.negotiated_item_cost,NULL),
                                       DECODE(L_default_tax_type,'GTAX',isc.extended_base_cost,NULL),
                                       DECODE(L_default_tax_type,'GTAX',isc.inclusive_cost,NULL),
                                       DECODE(L_default_tax_type,'GTAX',isc.base_cost,NULL)
                                  from dc_item_supp_country isc,
                                       item_supplier ms,
                                       item_master im,
                                       sups s
                                 where im.item = ms.item
                                   and s.supplier = ms.supplier
                                   and isc.item = ms.item
                                   and isc.supplier = ms.supplier;
                                   
      EXCEPTION
         when OTHERS then
            rollback;
            :GV_script_error := SQLERRM;
            :GV_return_code := 1;
      END;
      /

      print :GV_script_error;
      exit  :GV_return_code;
      /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN                                                
#--------------------------------------------------------------------------------------------------
data_dir=${dataDir}
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

# Check the data file
retCode=$(checkFile -f $inputFile)
if [[ ${retCode} -eq 2 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} does not exists." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 3 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} exists with 0(Zero) size." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
elif [[ ${retCode} -eq 4 ]]; then
   LOG_ERROR "Sqlloader data file : ${inputFile} is non-readable." "CHECK_FILE" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Load the file using sqlloader
LOAD_FILE
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "SQLLOADER failed while loading the file" "LOAD_FILE" "${ld_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${ld_status}
else
   LOG_MESSAGE "Sqlloader function completed successfully." "LOAD_FILE" ${OK} ${logFile} ${pgmName} ${pgmPID}
   
   # Load the data from data conversion to base tables 
   LOAD_ITEM_SUPP_COUNTRY
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data to main tables." "LOAD_ITEM_SUPP_COUNTRY" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "LOAD_ITEM_SUPP_COUNTRY" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi
fi

# Create Processed directory if not exists
if [[ ! -d "${dataCompDir}" ]]; then
   mkdir ${dataCompDir} > /dev/null 2>&1
fi

# Move the data file to the processed directory
if [[ -f "${dataDir}/${inputFile}" ]]; then
   mv -f ${dataDir}/${inputFile} ${dataCompDir}/. > /dev/null 2>&1
fi

LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}
