#! /bin/ksh
#--------------------------------------------------------------------------------------------------
# File: dc_insert_item_cost.ksh
# Desc: Load the data from staging to main tables.

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib
. ./dc_load.cfg
. ./dc_load.lib

pgmName='dc_insert_item_cost.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date
#----------------------------------------------------------------------------------------------------
# Function Name : INSERT_ITEM_COST
# Purpose : load data from data conversion table to base tables.
#----------------------------------------------------------------------------------------------------
function INSERT_ITEM_COST
{
   echo "set feedback off
      set heading off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code := 0;

      WHENEVER SQLERROR EXIT 1
      DECLARE
         L_tax_type   SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;

         cursor C_SYSTEM_OPTIONS is
            select default_tax_type
              from system_options;

      BEGIN
         open C_SYSTEM_OPTIONS;
         fetch C_SYSTEM_OPTIONS into L_tax_type;
         close C_SYSTEM_OPTIONS;

         --insert into item_country for all the countries to which the locations have been ranged.
         if L_tax_type = 'GTAX' then
            insert into item_country(item,
                                     country_id)
                              select distinct
                                     il.item,
                                     c.country_id
                                from dc_item_loc il,
                                     addr,
                                     country c
                               where ((il.loc_type = 'S'
                                       and addr.module = 'ST'
                                       and addr.key_value_1 = to_char(il.location)
                                       and addr.addr_type = '01'
                                       and primary_addr_ind = 'Y')
                                      or (il.loc_type = 'E'
                                          and addr.module = 'PTNR'
                                          and addr.key_value_1 = 'E'
                                          and addr.key_value_2 = to_char(il.location)
                                          and addr.addr_type = '01'
                                          and addr.primary_addr_ind = 'Y')
                                      or (il.loc_type = 'W'
                                          and addr.module = 'WH'
                                          and addr.key_value_1 = (select to_char(physical_wh)
                                                                    from wh
                                                                   where wh = il.location)
                                          and addr.addr_type = '01'
                                          and primary_addr_ind = 'Y'))
                                 and addr.country_id = c.country_id
                                 and not exists (select 1
                                                   from item_country ic
                                                  where ic.item = il.item
                                                    and ic.country_id = addr.country_id)
                                UNION
                                select distinct il.item,
                                       so.base_country_id
                                  from dc_item_loc il,
                                       system_config_options so
                                 where not exists (select 1
                                                   from item_country ic
                                                  where ic.item = il.item
                                                    and ic.country_id = so.base_country_id);

            --insert into item_cost_head for the the delivery countries ranged to the item
            insert into item_cost_head(item,
                                       supplier,
                                       origin_country_id,
                                       delivery_country_id,
                                       prim_dlvy_ctry_ind,
                                       nic_static_ind,
                                       base_cost,
                                       negotiated_item_cost,
                                       extended_base_cost,
                                       inclusive_cost)
                                select isc.item,
                                       isc.supplier,
                                       isc.origin_country_id,
                                       ic.country_id,
                                       DECODE(ic.country_id, so.base_country_id, 'Y', 'N') prim_dlvy_ctry_ind,
                                       'Y' nic_static_ind,
                                       isc.base_cost,
                                       isc.negotiated_item_cost,
                                       isc.extended_base_cost,
                                       isc.inclusive_cost
                                  from item_supp_country isc,
                                       item_country ic,
                                       system_config_options so
                                 where isc.item = ic.item
                                   and exists (select 1
                                                 from dc_item_loc il
                                                where il.item = isc.item
                                                  and rownum = 1)
                                   and not exists (select 1
                                                     from item_cost_head ich
                                                    where ich.item = isc.item
                                                      and ich.supplier = isc.supplier
                                                      and ich.origin_country_id = isc.origin_country_id
                                                      and ich.delivery_country_id = ic.country_id);

               insert into item_cost_detail(item,
                                            supplier,
                                            origin_country_id,
                                            delivery_country_id,
                                            cond_type,
                                            cond_value,
                                            applied_on,
                                            comp_rate,
                                            modified_taxable_base)
                                     select ich.item,
                                            ich.supplier,
                                            ich.origin_country_id,
                                            ich.delivery_country_id,
                                            vi.vat_code cond_type,
                                            (ich.base_cost * vi.vat_rate)/100 cond_value,
                                            ich.base_cost applied_on,
                                            vi.vat_rate comp_rate,
                                            ich.base_cost modified_taxable_base
                                       from item_cost_head ich,
                                            vat_item vi,
                                            localization_config_options lo
                                      where ich.item = vi.item
                                        and vi.vat_region = lo.default_vat_region
                                        and vi.vat_type in ('B','C')
                                        and vi.active_date = (select MAX(v.active_date)
                                                                from vat_item v,
                                                                     period p
                                                               where v.item = ich.item
                                                                 and v.vat_region = vi.vat_region
                                                                 and v.vat_type in ('B','C')
                                                                 and v.active_date <= p.vdate)
                                        and exists (select 1
                                                      from dc_item_loc il
                                                     where il.item = ich.item
                                                       and rownum = 1)
                                        and not exists (select 1
                                                          from item_cost_detail icd
                                                         where icd.item = ich.item
                                                           and icd.supplier = ich.supplier
                                                           and icd.origin_country_id = ich.origin_country_id
                                                           and icd.delivery_country_id = ich.delivery_country_id
                                                           and icd.cond_type = vi.vat_code);




         end if;

         EXCEPTION
            when OTHERS then
               rollback;
               :GV_script_error := SQLERRM;
               :GV_return_code := 1;
         END;
         /

         print :GV_script_error;
         exit  :GV_return_code;
         /" | sqlplus -s ${connectStr} >> ${logFile}

   return $?
}
#--------------------------------------------------------------------------------------------------
#                                              MAIN
#--------------------------------------------------------------------------------------------------
log_dir=${logDir}
ERRORFILE="${log_dir}/err.${pgmName}.${exeDate}"
logFile="${log_dir}/${pgmName}_${exeDate}.log"
LOG_MESSAGE "Job ${pgmName}.ksh started by user - ${USER}" ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}

# Check configuration file
checkCfg
ld_status=$?
if [[ ${ld_status} -ne ${OK} ]]; then
   LOG_ERROR "Configuration file(dc_load.cfg) setup is invalid." "CONFIG_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit 1
fi

# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "CONNECTION_CHECK" ${OK} ${logFile} ${pgmName} ${pgmPID}
fi

   INSERT_ITEM_COST
   ld_status=$?
   if [[ ${ld_status} -ne ${OK} ]]; then
      LOG_ERROR "Function failed while loading data of RPM dept aggregation table." "INSERT_ITEM_COST" "${FATAL}" "${ERRORFILE}" "${logFile}" "${pgmName}" "${pgmPID}"
      exit ${ld_status}
   else
      LOG_MESSAGE "Load Function completed successfully." "INSERT_ITEM_COST" ${OK} ${logFile} ${pgmName} ${pgmPID}
   fi


LOG_MESSAGE "Program Completed successfully." ${pgmName} ${OK} ${logFile} ${pgmName} ${pgmPID}