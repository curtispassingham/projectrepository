CREATE OR REPLACE type cost_chg_sch_crit_typ
AS
  object
  (
    cost_change        NUMBER(8,0),
    supplier           NUMBER(10),
    supplier_site      NUMBER(10),
    process_id         NUMBER(20),
    from_date          DATE,
    to_date            DATE,
    last_upd_id        VARCHAR2(30),
	status             VARCHAR2(1),
    supplier_site_name VARCHAR2(240),
    cost_change_desc   VARCHAR2(120),
    reason             NUMBER(2,0),
	from_create_date   DATE,
    to_create_date   DATE
  );
/
