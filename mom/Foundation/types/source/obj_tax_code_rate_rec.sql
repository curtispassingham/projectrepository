--OBJ_TAX_CODE_RATE_REC
DROP TYPE OBJ_TAX_CODE_RATE_TBL FORCE
/
DROP TYPE OBJ_TAX_CODE_RATE_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_TAX_CODE_RATE_REC AS OBJECT (
   I_tax_code             VARCHAR2(6),
   I_tax_type             VARCHAR2(6),     --'R', 'C', 'B'
   I_effective_from_date  DATE,
   --
   O_tax_rate             NUMBER(20,10),
   O_calculation_basis    VARCHAR2(6),     --'P', 'V' 
   O_currency_code        VARCHAR2(3),     --currency of O_tax_rate if O_calculation_basis is 'V'; NULL if 'P'
   --
   CONSTRUCTOR FUNCTION OBJ_TAX_CODE_RATE_REC
   (
      I_tax_code             VARCHAR2 DEFAULT NULL,
      I_tax_type             VARCHAR2 DEFAULT NULL,
      I_effective_from_date  DATE DEFAULT NULL,
      --
      O_tax_rate             NUMBER DEFAULT NULL,
      O_calculation_basis    VARCHAR2 DEFAULT NULL,
      O_currency_code        VARCHAR2 DEFAULT NULL
   ) RETURN SELF AS RESULT
)
/

CREATE OR REPLACE TYPE BODY OBJ_TAX_CODE_RATE_REC AS
 CONSTRUCTOR FUNCTION OBJ_TAX_CODE_RATE_REC 
 (
    I_tax_code             VARCHAR2 DEFAULT NULL,
    I_tax_type             VARCHAR2 DEFAULT NULL,
    I_effective_from_date  DATE DEFAULT NULL,
    --
    O_tax_rate             NUMBER DEFAULT NULL,
    O_calculation_basis    VARCHAR2 DEFAULT NULL,
    O_currency_code        VARCHAR2 DEFAULT NULL
 ) RETURN SELF AS RESULT IS
    BEGIN
       SELF.I_TAX_CODE := I_TAX_CODE;
       SELF.I_TAX_TYPE := I_TAX_TYPE;
       SELF.I_EFFECTIVE_FROM_DATE := I_EFFECTIVE_FROM_DATE;
       SELF.O_TAX_RATE := O_TAX_RATE;
       SELF.O_CALCULATION_BASIS := O_CALCULATION_BASIS;
       SELF.O_CURRENCY_CODE := O_CURRENCY_CODE;
       RETURN;
    END;  
END;
/

CREATE OR REPLACE TYPE OBJ_TAX_CODE_RATE_TBL AS TABLE OF OBJ_TAX_CODE_RATE_REC
/
