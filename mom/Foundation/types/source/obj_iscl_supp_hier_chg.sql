drop type obj_iscl_supp_hier_chg_tbl FORCE
/

drop type obj_iscl_supp_hier_chg_rec FORCE
/

create or replace TYPE obj_iscl_supp_hier_chg_rec AS OBJECT (
ITEM                    VARCHAR2(25),
SUPPLIER                NUMBER(10),
ORIGIN_COUNTRY_ID       VARCHAR2(3),
LOCATION                NUMBER(10),
old_supp_hier_lvl_1     VARCHAR2(10),
old_supp_hier_lvl_2     VARCHAR2(10),
old_supp_hier_lvl_3     VARCHAR2(10),
new_supp_hier_lvl_1     VARCHAR2(10),
new_supp_hier_lvl_2     VARCHAR2(10),
new_supp_hier_lvl_3     VARCHAR2(10)
)
/

create or replace TYPE obj_iscl_supp_hier_chg_tbl AS TABLE OF obj_iscl_supp_hier_chg_rec
/
