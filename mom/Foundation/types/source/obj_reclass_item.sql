DROP Type obj_reclass_item_rec FORCE ;
create or replace TYPE obj_reclass_item_rec AS OBJECT
(
   RECLASS_NO       NUMBER(4),
   ITEM             VARCHAR2(25)
);
/
--Table of reclass item detail recs
DROP Type obj_reclass_item_tbl FORCE;
create or replace TYPE obj_reclass_item_tbl AS TABLE OF obj_reclass_item_rec;
/


