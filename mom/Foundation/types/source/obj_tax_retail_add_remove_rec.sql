--OBJ_TAX_RETAIL_ADD_REMOVE_REC
DROP TYPE OBJ_TAX_RETAIL_ADD_REMOVE_TBL FORCE
/
DROP TYPE OBJ_TAX_RETAIL_ADD_REMOVE_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_TAX_RETAIL_ADD_REMOVE_REC AS OBJECT (
  I_item                  VARCHAR2(25),
  I_dept                  NUMBER(4),
  I_class                 NUMBER(4),
  I_location              NUMBER(10),
  I_loc_type              VARCHAR2(6),  --'S', 'W', 'E'
  I_effective_from_date   DATE,
  I_amount                NUMBER(20,4), -- in location currency
  I_qty                   NUMBER(12,4), 
  --
  O_amount                NUMBER(20,4),  -- in location currency

   CONSTRUCTOR FUNCTION OBJ_TAX_RETAIL_ADD_REMOVE_REC
   (
      I_item                 VARCHAR2 DEFAULT NULL,
      I_dept                 NUMBER DEFAULT NULL,
      I_class                NUMBER DEFAULT NULL,
      I_location             NUMBER DEFAULT NULL,
      I_loc_type             VARCHAR2 DEFAULT NULL,
      I_effective_from_date  DATE DEFAULT NULL,
      I_amount               NUMBER DEFAULT NULL,
      I_qty                  NUMBER DEFAULT NULL,
      --
      O_amount               NUMBER DEFAULT NULL
   ) RETURN SELF AS RESULT
)
/

CREATE OR REPLACE TYPE BODY OBJ_TAX_RETAIL_ADD_REMOVE_REC AS
 CONSTRUCTOR FUNCTION OBJ_TAX_RETAIL_ADD_REMOVE_REC
 (
   I_item                 VARCHAR2 DEFAULT NULL,
   I_dept                 NUMBER DEFAULT NULL,
   I_class                NUMBER DEFAULT NULL,
   I_location             NUMBER DEFAULT NULL,
   I_loc_type             VARCHAR2 DEFAULT NULL,
   I_effective_from_date  DATE DEFAULT NULL,
   I_amount               NUMBER DEFAULT NULL,
   I_qty                  NUMBER DEFAULT NULL,
   --
   O_amount               NUMBER DEFAULT NULL
 ) RETURN SELF AS RESULT IS
    BEGIN
       SELF.I_ITEM := I_ITEM;
       SELF.I_DEPT := I_DEPT;
       SELF.I_CLASS := I_CLASS;
       SELF.I_LOCATION := I_LOCATION;
       SELF.I_LOC_TYPE := I_LOC_TYPE;
       SELF.I_EFFECTIVE_FROM_DATE := I_EFFECTIVE_FROM_DATE;
       SELF.I_AMOUNT := I_AMOUNT;
       SELF.I_QTY := I_QTY;
       SELF.O_AMOUNT := O_AMOUNT;
       RETURN;
    END;  
END;
/

CREATE OR REPLACE TYPE OBJ_TAX_RETAIL_ADD_REMOVE_TBL AS TABLE OF OBJ_TAX_RETAIL_ADD_REMOVE_REC
/
