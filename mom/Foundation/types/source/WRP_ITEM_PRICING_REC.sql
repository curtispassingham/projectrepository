drop type WRP_ITEM_PRICING_TBL FORCE
/

drop type WRP_ITEM_PRICING_REC FORCE
/

create or replace TYPE WRP_ITEM_PRICING_REC AS OBJECT
( -- From pricing system
 ITEM                         VARCHAR2(25),
 RPM_ZONE_GROUP_ID            NUMBER(4),
 ZONE_ID                     NUMBER(10),
 ZONE_DISPLAY_ID             NUMBER(10),
 ZONE_DESCRIPTION            VARCHAR2(120),
 UNIT_RETAIL                  NUMBER(20,4),
 SELLING_UNIT_RETAIL          NUMBER(20,4),
 SELLING_UOM                  VARCHAR2(4),
 MULTI_UNITS                  NUMBER(12,4),
 MULTI_UNIT_RETAIL            NUMBER(20,4),
 MULTI_SELLING_UOM             VARCHAR2(4),
 BASE_RETAIL_IND             VARCHAR2(1),
 CURRENCY_CODE               VARCHAR2(3),
 STANDARD_UOM                 VARCHAR2(4),
 --- Calculated after calling pricing system
 SELLING_MARK_UP             NUMBER,
 MULTI_SELLING_MARK_UP       NUMBER,
 --- Set by item retail form
 DEFAULT_TO_CHILDREN         VARCHAR2(1),
 --- Currency conversion values
 UNIT_RETAIL_PRIM             NUMBER(20,4),
 SELLING_UNIT_RETAIL_PRIM     NUMBER(20,4),
 MULTI_UNIT_RETAIL_PRIM        NUMBER(20,4),
 UNIT_RETAIL_EURO             NUMBER(20,4),
 SELLING_UNIT_RETAIL_EURO     NUMBER(20,4),
 MULTI_UNIT_RETAIL_EURO      NUMBER(20,4),
 --- error handling.  These are necessary because the Form's auto generated query
 ---  triggers do not contain exception handling (meaning they will not correctly
 ---  decode a raised application error.)
 ERROR_MESSAGE                VARCHAR2(255),
 RETURN_CODE                 VARCHAR2(5),
 AREA_DIFF_ZONE_IND          NUMBER(1)
)


/
create or replace TYPE
    WRP_ITEM_PRICING_TBL
    AS TABLE OF WRP_ITEM_PRICING_REC
/
