CREATE OR REPLACE type item_induct_sch_crit_typ
AS
  object
  (
    division      NUMBER(4),
    group_no      NUMBER(4),
    dept          NUMBER(4),
    class         NUMBER(4),
    subclass      NUMBER(4),
    supplier      NUMBER(10),
    supplier_site NUMBER(10),
    process_id    NUMBER(20),
    item          VARCHAR2(25),
    item_desc     VARCHAR2(250),
    status        VARCHAR2(1),
    from_date     DATE,
    to_date       DATE,
    last_upd_id   VARCHAR2(30),
    next_upd_id   VARCHAR2(30),
    item_type     VARCHAR2(1),
    item_level    NUMBER(1),
    tran_level    NUMBER(1),
    orderable_ind VARCHAR2(1),
    inventory_ind VARCHAR2(1),
    sellable_ind  VARCHAR2(1),
    created_date  DATE,
    sup_name      VARCHAR2(240),
    brand_name    VARCHAR2(30),
    skulist       NUMBER(8),
    uda_id        NUMBER(5),
    uda_value     NUMBER(5),
    vpn           VARCHAR2(30));
/