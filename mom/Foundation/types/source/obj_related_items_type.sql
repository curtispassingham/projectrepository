----------------------------------------------------------------------------
--	OBJECT CREATED :                    RELATED_ITEMS_REC 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object RELATED_ITEMS_TBL

DROP TYPE RELATED_ITEMS_TBL FORCE
/

PROMPT Dropping Object RELATED_ITEMS_REC

DROP TYPE RELATED_ITEMS_REC FORCE
/
--------------------------------------
--       Creating Object
--------------------------------------

PROMPT Creating Object RELATED_ITEMS_REC 

CREATE OR REPLACE TYPE RELATED_ITEMS_REC AS OBJECT
(
 Item   Varchar2(25),
 Item_level Number(1),
 Tran_level Number(1)
)
/

PROMPT Creating Object RELATED_ITEMS_TBL

CREATE OR REPLACE TYPE RELATED_ITEMS_TBL AS TABLE OF RELATED_ITEMS_REC
/


 
