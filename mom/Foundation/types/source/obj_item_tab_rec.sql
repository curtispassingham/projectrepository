drop type item_rec force
/
drop type item_tab force
/
create or replace TYPE item_rec as object (item                   VARCHAR2(25),
                                           ref_item               VARCHAR2(25),
                                           vpn                    VARCHAR2(30),
                                           item_desc              VARCHAR2(250),
                                           tl_item_desc           VARCHAR2(250),
                                           dept                   NUMBER(4,0),
                                           class                  NUMBER(4,0),
                                           subclass               NUMBER(4,0),
                                           pack_ind               VARCHAR2(1))
/
create or replace TYPE item_tab AS TABLE OF item_rec
/