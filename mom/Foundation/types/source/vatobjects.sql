drop type OBJ_ITEM_LOCATION_DATE_VAT_TBL force;
drop type OBJ_ITEM_LOCATION_DATE_VAT_REC force;


CREATE OR REPLACE TYPE OBJ_ITEM_LOCATION_DATE_VAT_REC AS OBJECT
(
ITEM            VARCHAR2(25),
LOCATION        NUMBER(10),
ACTION_DATE     DATE,
--
VAT_RATE        NUMBER(20,10),
VAT_VALUE       NUMBER(20,10),
CLASS_VAT_IND   VARCHAR2(1)
);
/

--drop type OBJ_ITEM_LOCATION_DATE_VAT_TBL force;
CREATE OR REPLACE TYPE OBJ_ITEM_LOCATION_DATE_VAT_TBL AS TABLE OF OBJ_ITEM_LOCATION_DATE_VAT_REC;
/
