drop type OBJ_TAX_CALC_FLAT_TBL FORCE
/
drop type OBJ_TAX_CALC_FLAT_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_TAX_CALC_FLAT_REC AS OBJECT (
   I_item                      VARCHAR2(25),
   I_pack_ind                  VARCHAR2(1),
   I_from_entity               VARCHAR2(10),
   I_from_entity_type          VARCHAR2(6),  --'ST', 'WH', 'E', 'SP'
   I_to_entity                 VARCHAR2(10),
   I_to_entity_type            VARCHAR2(6),  --'ST', 'WH', 'E', 'SP'
   I_effective_from_date       DATE,
   I_amount                    NUMBER(20,4),
   I_amount_curr               VARCHAR2(3),
   O_cum_tax_pct               NUMBER(20,10),
   O_cum_tax_value             NUMBER(20,10),
   O_total_tax_amount          NUMBER(20,4),
   O_total_tax_amount_curr     VARCHAR2(3),
   O_total_recover_amount      NUMBER(20,4),
   O_total_recover_amount_curr VARCHAR2(3),
   tax_code                    VARCHAR2(6),
   tax_type                    VARCHAR2(6),
   calculation_basis           VARCHAR2(6),
   taxable_base                NUMBER(20,4),
   modified_taxable_base       NUMBER(20,4),
   tax_rate                    NUMBER(20,10),
   estimated_tax_value         NUMBER(20,4),
   recoverable_amount          NUMBER(20,4),
   currency_code               VARCHAR2(3),
  CONSTRUCTOR FUNCTION OBJ_TAX_CALC_FLAT_REC
   (
     I_item                      VARCHAR2 DEFAULT NULL,
     I_pack_ind                  VARCHAR2 DEFAULT NULL,
     I_from_entity               VARCHAR2 DEFAULT NULL,
     I_from_entity_type          VARCHAR2 DEFAULT NULL,
     I_to_entity                 VARCHAR2 DEFAULT NULL,
     I_to_entity_type            VARCHAR2 DEFAULT NULL,
     I_effective_from_date       DATE DEFAULT NULL,
     I_amount                    NUMBER DEFAULT NULL,
     I_amount_curr               VARCHAR2 DEFAULT NULL,
     O_cum_tax_pct               NUMBER DEFAULT NULL,
     O_cum_tax_value             NUMBER DEFAULT NULL,
     O_total_tax_amount          NUMBER DEFAULT NULL,
     O_total_tax_amount_curr     VARCHAR2 DEFAULT NULL,
     O_total_recover_amount      NUMBER DEFAULT NULL,
     O_total_recover_amount_curr VARCHAR2 DEFAULT NULL,
     tax_code                    VARCHAR2 DEFAULT NULL,
     tax_type                    VARCHAR2 DEFAULT NULL,
     calculation_basis           VARCHAR2 DEFAULT NULL,
     taxable_base                NUMBER DEFAULT NULL,
     modified_taxable_base       NUMBER DEFAULT NULL,	
     tax_rate                    NUMBER DEFAULT NULL,
     estimated_tax_value         NUMBER DEFAULT NULL,
     recoverable_amount          NUMBER DEFAULT NULL,
     currency_code               VARCHAR2 DEFAULT NULL
   ) RETURN SELF AS RESULT
)
/
CREATE OR REPLACE TYPE BODY OBJ_TAX_CALC_FLAT_REC AS
 CONSTRUCTOR FUNCTION OBJ_TAX_CALC_FLAT_REC
 (
     I_item                      VARCHAR2 DEFAULT NULL,
     I_pack_ind                  VARCHAR2 DEFAULT NULL,
     I_from_entity               VARCHAR2 DEFAULT NULL,
     I_from_entity_type          VARCHAR2 DEFAULT NULL,
     I_to_entity                 VARCHAR2 DEFAULT NULL,
     I_to_entity_type            VARCHAR2 DEFAULT NULL,
     I_effective_from_date       DATE DEFAULT NULL,
     I_amount                    NUMBER DEFAULT NULL,
     I_amount_curr               VARCHAR2 DEFAULT NULL,
     O_cum_tax_pct               NUMBER DEFAULT NULL,
     O_cum_tax_value             NUMBER DEFAULT NULL,
     O_total_tax_amount          NUMBER DEFAULT NULL,
     O_total_tax_amount_curr     VARCHAR2 DEFAULT NULL,
     O_total_recover_amount      NUMBER DEFAULT NULL,
     O_total_recover_amount_curr VARCHAR2 DEFAULT NULL,
     tax_code                    VARCHAR2 DEFAULT NULL,
     tax_type                    VARCHAR2 DEFAULT NULL,
     calculation_basis           VARCHAR2 DEFAULT NULL,
     taxable_base                NUMBER DEFAULT NULL,
     modified_taxable_base       NUMBER DEFAULT NULL,
     tax_rate                    NUMBER DEFAULT NULL,
     estimated_tax_value         NUMBER DEFAULT NULL,
     recoverable_amount          NUMBER DEFAULT NULL,
     currency_code               VARCHAR2 DEFAULT NULL
 ) RETURN SELF AS RESULT IS
   BEGIN
      SELF.I_item                      := I_item;
      SELF.I_pack_ind                  := I_pack_ind;
      SELF.I_from_entity               := I_from_entity;
      SELF.I_from_entity_type          := I_from_entity_type;
      SELF.I_to_entity                 := I_to_entity;
      SELF.I_to_entity_type            := I_to_entity_type;
      SELF.I_effective_from_date       := I_effective_from_date;
      SELF.I_amount                    := I_amount;
      SELF.I_amount_curr               := I_amount_curr;
      SELF.O_cum_tax_pct               := O_cum_tax_pct;
      SELF.O_cum_tax_value             := O_cum_tax_value;
      SELF.O_total_tax_amount          := O_total_tax_amount;
      SELF.O_total_tax_amount_curr     := O_total_tax_amount_curr;
      SELF.O_total_recover_amount      := O_total_recover_amount;
      SELF.O_total_recover_amount_curr := O_total_recover_amount_curr;
      SELF.tax_code                    := tax_code;
      SELF.tax_type                    := tax_type;
      SELF.calculation_basis           := calculation_basis;
      SELF.taxable_base                := taxable_base;
      SELF.modified_taxable_base       := modified_taxable_base;
      SELF.tax_rate                    := tax_rate;
      SELF.estimated_tax_value         := estimated_tax_value;
      SELF.recoverable_amount          := recoverable_amount;
      SELF.currency_code               := currency_code;
      RETURN;
   END;
END;
/
CREATE OR REPLACE TYPE OBJ_TAX_CALC_FLAT_TBL AS TABLE OF OBJ_TAX_CALC_FLAT_REC
/
