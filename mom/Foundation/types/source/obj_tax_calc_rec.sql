--OBJ_TAX_CALC_REC
DROP TYPE OBJ_TAX_CALC_TBL FORCE
/
DROP TYPE OBJ_TAX_CALC_REC FORCE
/

create or replace TYPE OBJ_TAX_CALC_REC AS OBJECT (
  I_item                      VARCHAR2(25),
  I_pack_ind                  VARCHAR2(1),
  I_from_entity               VARCHAR2(10),
  I_from_entity_type          VARCHAR2(6),  --'ST', 'WH', 'E', 'SP'
  I_to_entity                 VARCHAR2(10),
  I_to_entity_type            VARCHAR2(6),  --'ST', 'WH', 'E', 'SP'
  I_effective_from_date       DATE,
  I_amount                    NUMBER(20,4),
  I_amount_curr               VARCHAR2(3),
  I_amount_tax_incl_ind       VARCHAR2(1),
  I_origin_country_id         VARCHAR2(3),
  --
  O_cum_tax_pct               NUMBER(20,10),
  O_cum_tax_value             NUMBER(20,10),
  O_total_tax_amount          NUMBER(20,4),
  O_total_tax_amount_curr     VARCHAR2(3),
  O_total_recover_amount      NUMBER(20,4),
  O_total_recover_amount_curr VARCHAR2(3),
  O_tax_detail_tbl            OBJ_TAX_DETAIL_TBL,
  --
  I_tran_type                 VARCHAR2(24),
  I_tran_date                 DATE,
  I_tran_id                   VARCHAR2(120),
  I_cost_retail_ind           VARCHAR2(1),
  I_source_entity             VARCHAR2(10),
  I_source_entity_type        VARCHAR2(6),
  O_tax_exempt_ind            VARCHAR2(1),
  I_vat_code                  VARCHAR2(6),
  I_vat_rate                  NUMBER(20,10),
  

  CONSTRUCTOR FUNCTION OBJ_TAX_CALC_REC
  (
     I_item                      VARCHAR2 DEFAULT NULL,
     I_pack_ind                  VARCHAR2 DEFAULT NULL,
     I_from_entity               VARCHAR2 DEFAULT NULL,
     I_from_entity_type          VARCHAR2 DEFAULT NULL,
     I_to_entity                 VARCHAR2 DEFAULT NULL,
     I_to_entity_type            VARCHAR2 DEFAULT NULL,
     I_effective_from_date       DATE DEFAULT NULL,
     I_amount                    NUMBER DEFAULT NULL,
     I_amount_curr               VARCHAR2 DEFAULT NULL,
     I_amount_tax_incl_ind       VARCHAR2 DEFAULT NULL,
     I_origin_country_id         VARCHAR2 DEFAULT NULL,
     --
     O_cum_tax_pct               NUMBER DEFAULT NULL,
     O_cum_tax_value             NUMBER DEFAULT NULL,
     O_total_tax_amount          NUMBER DEFAULT NULL,
     O_total_tax_amount_curr     VARCHAR2 DEFAULT NULL,
     O_total_recover_amount      NUMBER DEFAULT NULL,
     O_total_recover_amount_curr VARCHAR2 DEFAULT NULL,
     O_tax_detail_tbl            OBJ_TAX_DETAIL_TBL DEFAULT NULL,
     I_tran_type                 VARCHAR2 DEFAULT NULL,
     I_tran_date                 DATE DEFAULT NULL,
     I_tran_id                   VARCHAR2 DEFAULT NULL,
     I_cost_retail_ind           VARCHAR2 DEFAULT NULL,
     I_source_entity             VARCHAR2 DEFAULT NULL,
     I_source_entity_type        VARCHAR2 DEFAULT NULL,
     O_tax_exempt_ind            VARCHAR2 DEFAULT NULL,
     I_vat_code                  VARCHAR2 DEFAULT NULL,
     I_vat_rate                  NUMBER DEFAULT NULL 
  ) RETURN SELF AS RESULT
)
/

CREATE OR REPLACE TYPE BODY OBJ_TAX_CALC_REC AS
 CONSTRUCTOR FUNCTION OBJ_TAX_CALC_REC 
 (
     I_item                      VARCHAR2 DEFAULT NULL,
     I_pack_ind                  VARCHAR2 DEFAULT NULL,
     I_from_entity               VARCHAR2 DEFAULT NULL,
     I_from_entity_type          VARCHAR2 DEFAULT NULL,
     I_to_entity                 VARCHAR2 DEFAULT NULL,
     I_to_entity_type            VARCHAR2 DEFAULT NULL,
     I_effective_from_date       DATE DEFAULT NULL,
     I_amount                    NUMBER DEFAULT NULL,
     I_amount_curr               VARCHAR2 DEFAULT NULL,
     I_amount_tax_incl_ind       VARCHAR2 DEFAULT NULL,
     I_origin_country_id         VARCHAR2 DEFAULT NULL,
     --
     O_cum_tax_pct               NUMBER DEFAULT NULL,
     O_cum_tax_value             NUMBER DEFAULT NULL,
     O_total_tax_amount          NUMBER DEFAULT NULL,
     O_total_tax_amount_curr     VARCHAR2 DEFAULT NULL,
     O_total_recover_amount      NUMBER DEFAULT NULL,
     O_total_recover_amount_curr VARCHAR2 DEFAULT NULL,
     O_tax_detail_tbl            OBJ_TAX_DETAIL_TBL DEFAULT NULL,
     I_tran_type                 VARCHAR2 DEFAULT NULL,
     I_tran_date                 DATE DEFAULT NULL,
     I_tran_id                   VARCHAR2 DEFAULT NULL,
     I_cost_retail_ind           VARCHAR2 DEFAULT NULL,
     I_source_entity             VARCHAR2 DEFAULT NULL,
     I_source_entity_type        VARCHAR2 DEFAULT NULL,
     O_tax_exempt_ind            VARCHAR2 DEFAULT NULL,
     I_vat_code                  VARCHAR2 DEFAULT NULL,
     I_vat_rate                  NUMBER DEFAULT NULL     
 ) RETURN SELF AS RESULT IS
    BEGIN
       SELF.I_ITEM := I_ITEM;
       SELF.I_PACK_IND := I_PACK_IND;
       SELF.I_FROM_ENTITY := I_FROM_ENTITY;
       SELF.I_FROM_ENTITY_TYPE := I_FROM_ENTITY_TYPE;
       SELF.I_TO_ENTITY := I_TO_ENTITY;
       SELF.I_TO_ENTITY_TYPE := I_TO_ENTITY_TYPE;
       SELF.I_EFFECTIVE_FROM_DATE := I_EFFECTIVE_FROM_DATE;
       SELF.I_AMOUNT := I_AMOUNT;
       SELF.I_AMOUNT_CURR := I_AMOUNT_CURR;
       SELF.I_AMOUNT_TAX_INCL_IND := I_AMOUNT_TAX_INCL_IND;
       SELF.I_ORIGIN_COUNTRY_ID := I_ORIGIN_COUNTRY_ID;
       --
       SELF.O_CUM_TAX_PCT := O_CUM_TAX_PCT;
       SELF.O_CUM_TAX_VALUE := O_CUM_TAX_VALUE;
       SELF.O_TOTAL_TAX_AMOUNT := O_TOTAL_TAX_AMOUNT;
       SELF.O_TOTAL_TAX_AMOUNT_CURR := O_TOTAL_TAX_AMOUNT_CURR;
       SELF.O_TOTAL_RECOVER_AMOUNT := O_TOTAL_RECOVER_AMOUNT;
       SELF.O_TOTAL_RECOVER_AMOUNT_CURR := O_TOTAL_RECOVER_AMOUNT_CURR;
       SELF.O_TAX_DETAIL_TBL := O_TAX_DETAIL_TBL;
       --
       SELF.I_TRAN_TYPE := I_TRAN_TYPE;
       SELF.I_TRAN_DATE := I_TRAN_DATE;
       SELF.I_TRAN_ID := I_TRAN_ID;
       SELF.I_COST_RETAIL_IND := I_COST_RETAIL_IND;
       SELF.I_SOURCE_ENTITY := I_SOURCE_ENTITY;
       SELF.I_SOURCE_ENTITY_TYPE  := I_SOURCE_ENTITY_TYPE;
       SELF.O_TAX_EXEMPT_IND := O_TAX_EXEMPT_IND;
       SELF.I_VAT_CODE := I_VAT_CODE;
       SELF.I_VAT_RATE := I_VAT_RATE;   
       --
       RETURN;
    END;  
END;
/

CREATE OR REPLACE TYPE OBJ_TAX_CALC_TBL AS TABLE OF OBJ_TAX_CALC_REC
/
