DROP TYPE OBJ_CANONICAL_TAX_TBL FORCE
/
DROP TYPE OBJ_CANONICAL_TAX_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_CANONICAL_TAX_REC AS OBJECT
(
   item                  VARCHAR2(25),
   pack_item             VARCHAR2(25),
   pack_ind              VARCHAR2(1),
   from_entity           VARCHAR2(10),
   from_entity_type      VARCHAR2(2),
   to_entity             VARCHAR2(10),
   to_entity_type        VARCHAR2(2),
   effective_from        DATE,
   currency_code         VARCHAR2(3),
   tax_type              VARCHAR2(1),
   calculation_basis     VARCHAR2(1),
   tax_code              VARCHAR2(6),
   tax_rate              NUMBER(20,10),
   estimated_tax_value   NUMBER(20,4),
   taxable_base          NUMBER(20,4),
   modified_taxable_base NUMBER(20,4),
   recoverable_amount    NUMBER(20,4)
)
/

CREATE OR REPLACE TYPE OBJ_CANONICAL_TAX_TBL AS TABLE OF OBJ_CANONICAL_TAX_REC
/

