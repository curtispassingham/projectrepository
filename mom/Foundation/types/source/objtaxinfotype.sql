drop type OBJ_TAX_INFO_TBL force
/

drop type OBJ_TAX_INFO_REC force
/

create or replace TYPE OBJ_TAX_INFO_REC as object (
   ITEM                   VARCHAR2(25),
   ITEM_PARENT            VARCHAR2(25),
   ITEM_GRANDPARENT       VARCHAR2(25),
   PACK_IND               VARCHAR2(1),
   MERCH_HIER_LEVEL       NUMBER(2),
   MERCH_HIER_VALUE       NUMBER(4),
   FROM_TAX_REGION        NUMBER(4),
   FROM_TAX_REGION_DESC   VARCHAR2(120),
   TO_TAX_REGION          NUMBER(4),
   FROM_ENTITY            VARCHAR2(10),
   FROM_ENTITY_TYPE       VARCHAR2(6),
   TO_ENTITY              VARCHAR2(10),
   TO_ENTITY_TYPE         VARCHAR2(6),
   AMOUNT                 NUMBER(20,4),
   COST_RETAIL_IND        VARCHAR2(1),
   TAX_INCL_IND           VARCHAR2(1),
   TAX_AMOUNT             NUMBER(20,4),
   TAX_RATE               NUMBER(20,10),
   TAX_CODE               VARCHAR2(6),
   TAX_CODE_DESC          VARCHAR2(120),
   ACTIVE_DATE            DATE,
   INVENTORY_IND          VARCHAR2(1),
   REF_NO_1               NUMBER(12),
   REF_NO_2               NUMBER(12),
   TRAN_CODE              NUMBER(4),
   TAXABLE_BASE           NUMBER(20,4),
   MODIFIED_TAXABLE_BASE  NUMBER(20,4),
   CALCULATION_BASIS      VARCHAR2(1),
   RECOVERABLE_AMOUNT     NUMBER(20,4),
   CURRENCY               VARCHAR2(3),
   REVERSE_VAT_IND        VARCHAR2(1),
   SOURCE_ENTITY          VARCHAR2(10),
   SOURCE_ENTITY_TYPE     VARCHAR2(6),
   TRAN_TYPE              VARCHAR2(10),
   TRAN_DATE              DATE,
   TRAN_ID                VARCHAR2(120),
   TAX_EXEMPT_IND         VARCHAR2(1),
   constructor function OBJ_TAX_INFO_REC
   (
       ITEM                   VARCHAR2   DEFAULT NULL
      ,ITEM_PARENT            VARCHAR2   DEFAULT NULL
      ,ITEM_GRANDPARENT       VARCHAR2   DEFAULT NULL
      ,PACK_IND               VARCHAR2   DEFAULT NULL
      ,MERCH_HIER_LEVEL       NUMBER     DEFAULT NULL
      ,MERCH_HIER_VALUE       NUMBER     DEFAULT NULL
      ,FROM_TAX_REGION        NUMBER     DEFAULT NULL
      ,FROM_TAX_REGION_DESC   VARCHAR2   DEFAULT NULL
      ,TO_TAX_REGION          NUMBER     DEFAULT NULL
      ,FROM_ENTITY            VARCHAR2   DEFAULT NULL
      ,FROM_ENTITY_TYPE       VARCHAR2   DEFAULT NULL
      ,TO_ENTITY              VARCHAR2   DEFAULT NULL
      ,TO_ENTITY_TYPE         VARCHAR2   DEFAULT NULL
      ,AMOUNT                 NUMBER     DEFAULT NULL
      ,COST_RETAIL_IND        VARCHAR2   DEFAULT NULL
      ,TAX_INCL_IND           VARCHAR2   DEFAULT NULL
      ,TAX_AMOUNT             NUMBER     DEFAULT NULL
      ,TAX_RATE               NUMBER     DEFAULT NULL
      ,TAX_CODE               VARCHAR2   DEFAULT NULL
      ,TAX_CODE_DESC          VARCHAR2   DEFAULT NULL
      ,ACTIVE_DATE            DATE       DEFAULT NULL
      ,INVENTORY_IND          VARCHAR2   DEFAULT NULL
      ,REF_NO_1               NUMBER     DEFAULT NULL
      ,REF_NO_2               NUMBER     DEFAULT NULL
      ,TRAN_CODE              NUMBER     DEFAULT NULL
      ,TAXABLE_BASE           NUMBER     DEFAULT NULL
      ,MODIFIED_TAXABLE_BASE  NUMBER     DEFAULT NULL
      ,CALCULATION_BASIS      VARCHAR2   DEFAULT NULL
      ,RECOVERABLE_AMOUNT     NUMBER     DEFAULT NULL
      ,CURRENCY               VARCHAR2   DEFAULT NULL
      ,REVERSE_VAT_IND        VARCHAR2   DEFAULT NULL
      ,SOURCE_ENTITY          VARCHAR2   DEFAULT NULL
      ,SOURCE_ENTITY_TYPE     VARCHAR2   DEFAULT NULL
      ,TRAN_TYPE              VARCHAR2   DEFAULT NULL
      ,TRAN_DATE              DATE       DEFAULT NULL
      ,TRAN_ID                VARCHAR2   DEFAULT NULL
      ,TAX_EXEMPT_IND         VARCHAR2   DEFAULT NULL)
      return self as result
)
/

create or replace type body OBJ_TAX_INFO_REC is
constructor function OBJ_TAX_INFO_REC
(
    ITEM                   VARCHAR2   DEFAULT NULL
   ,ITEM_PARENT            VARCHAR2   DEFAULT NULL
   ,ITEM_GRANDPARENT       VARCHAR2   DEFAULT NULL   
   ,PACK_IND               VARCHAR2   DEFAULT NULL
   ,MERCH_HIER_LEVEL       NUMBER     DEFAULT NULL
   ,MERCH_HIER_VALUE       NUMBER     DEFAULT NULL
   ,FROM_TAX_REGION        NUMBER     DEFAULT NULL
   ,FROM_TAX_REGION_DESC   VARCHAR2   DEFAULT NULL
   ,TO_TAX_REGION          NUMBER     DEFAULT NULL
   ,FROM_ENTITY            VARCHAR2   DEFAULT NULL
   ,FROM_ENTITY_TYPE       VARCHAR2   DEFAULT NULL
   ,TO_ENTITY              VARCHAR2   DEFAULT NULL
   ,TO_ENTITY_TYPE         VARCHAR2   DEFAULT NULL
   ,AMOUNT                 NUMBER     DEFAULT NULL
   ,COST_RETAIL_IND        VARCHAR2   DEFAULT NULL
   ,TAX_INCL_IND           VARCHAR2   DEFAULT NULL
   ,TAX_AMOUNT             NUMBER     DEFAULT NULL
   ,TAX_RATE               NUMBER     DEFAULT NULL
   ,TAX_CODE               VARCHAR2   DEFAULT NULL
   ,TAX_CODE_DESC          VARCHAR2   DEFAULT NULL
   ,ACTIVE_DATE            DATE       DEFAULT NULL
   ,INVENTORY_IND          VARCHAR2   DEFAULT NULL
   ,REF_NO_1               NUMBER     DEFAULT NULL
   ,REF_NO_2               NUMBER     DEFAULT NULL
   ,TRAN_CODE              NUMBER     DEFAULT NULL
   ,TAXABLE_BASE           NUMBER     DEFAULT NULL
   ,MODIFIED_TAXABLE_BASE  NUMBER     DEFAULT NULL
   ,CALCULATION_BASIS      VARCHAR2   DEFAULT NULL
   ,RECOVERABLE_AMOUNT     NUMBER     DEFAULT NULL
   ,CURRENCY               VARCHAR2   DEFAULT NULL
   ,REVERSE_VAT_IND        VARCHAR2   DEFAULT NULL
   ,SOURCE_ENTITY          VARCHAR2   DEFAULT NULL
   ,SOURCE_ENTITY_TYPE     VARCHAR2   DEFAULT NULL
   ,TRAN_TYPE              VARCHAR2   DEFAULT NULL
   ,TRAN_DATE              DATE       DEFAULT NULL
   ,TRAN_ID                VARCHAR2   DEFAULT NULL
   ,TAX_EXEMPT_IND         VARCHAR2   DEFAULT NULL
) return self as result is
   begin
      self.item                  := item;
      self.item_parent           := item_parent;
      self.item_grandparent      := item_grandparent;
      self.pack_ind              := pack_ind;
      self.merch_hier_level      := merch_hier_level;
      self.merch_hier_value      := merch_hier_value;
      self.from_tax_region       := from_tax_region;
      self.from_tax_region_desc  := from_tax_region_desc;
      self.to_tax_region         := to_tax_region;
      self.from_entity           := from_entity;
      self.from_entity_type      := from_entity_type;
      self.to_entity             := to_entity;
      self.to_entity_type        := to_entity_type;
      self.amount                := amount;
      self.cost_retail_ind       := cost_retail_ind;
      self.tax_incl_ind          := tax_incl_ind;
      self.tax_amount            := tax_amount;
      self.tax_rate              := tax_rate;
      self.tax_code              := tax_code;
      self.tax_code_desc         := tax_code_desc;
      self.active_date           := active_date;
      self.inventory_ind         := inventory_ind;
      self.ref_no_1              := ref_no_1;
      self.ref_no_2              := ref_no_2;
      self.tran_code             := tran_code;
      self.taxable_base          := taxable_base;
      self.modified_taxable_base := modified_taxable_base;  
      self.calculation_basis     := calculation_basis;
      self.recoverable_amount    := recoverable_amount;
      self.currency              := currency;
      self.reverse_vat_ind       := reverse_vat_ind;
      self.source_entity         := source_entity;
      self.source_entity_type    := source_entity_type;
      self.tran_type             := tran_type;
      self.tran_date             := tran_date;
      self.tran_id               := tran_id;
      self.tax_exempt_ind        := tax_exempt_ind;
      return;
   end;
end;
/

CREATE OR REPLACE TYPE OBJ_TAX_INFO_TBL AS TABLE OF OBJ_TAX_INFO_REC
/


