drop type OBJ_COMP_ITEM_COST_TBL FORCE
/
drop type OBJ_COMP_ITEM_COST_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_COMP_ITEM_COST_REC AS OBJECT (
   I_item                         VARCHAR2(25),
   I_nic_static_ind               VARCHAR2(1),
   I_supplier                     VARCHAR2(10),
   I_location                     VARCHAR2(10),
   I_loc_type                     VARCHAR2(2),
   I_effective_date               DATE,
   I_calling_form                 VARCHAR2(30),
   I_origin_country_id            VARCHAR2(3),
   I_delivery_country_id          VARCHAR2(3),
   I_prim_dlvy_ctry_ind           VARCHAR2(1),
   I_update_itemcost_ind          VARCHAR2(1),
   I_update_itemcost_child_ind    VARCHAR2(1),
   I_item_cost_tax_incl_ind       VARCHAR2(1),
   O_base_cost                    NUMBER(20,4),
   O_extended_base_cost           NUMBER(20,4),
   O_inclusive_cost               NUMBER(20,4),
   O_negotiated_item_cost         NUMBER(20,4),
   svat_tax_rate                  NUMBER(20,10),
   tax_loc_type                   VARCHAR2(2),
   pack_ind                       VARCHAR2(1),
   pack_type                      VARCHAR2(1),
   dept                           NUMBER(4),
   prim_supp_currency_code        VARCHAR2(3),
   loc_prim_country               VARCHAR2(3),
   loc_prim_country_tax_incl_ind  VARCHAR2(1),
   gtax_total_tax_amount          NUMBER(20,4),
   gtax_total_tax_amount_nic      NUMBER(20,4),
   gtax_total_recover_amount      NUMBER(20,4),
   CONSTRUCTOR FUNCTION OBJ_COMP_ITEM_COST_REC
   (
      I_item                         VARCHAR2 DEFAULT NULL,
      I_nic_static_ind               VARCHAR2 DEFAULT NULL,
      I_supplier                     VARCHAR2 DEFAULT NULL,
      I_location                     VARCHAR2 DEFAULT NULL,
      I_loc_type                     VARCHAR2 DEFAULT NULL,
      I_effective_date               DATE     DEFAULT NULL,
      I_calling_form                 VARCHAR2 DEFAULT NULL,
      I_origin_country_id            VARCHAR2 DEFAULT NULL,
      I_delivery_country_id          VARCHAR2 DEFAULT NULL,
      I_prim_dlvy_ctry_ind           VARCHAR2 DEFAULT NULL,
      I_update_itemcost_ind          VARCHAR2 DEFAULT NULL,
      I_update_itemcost_child_ind    VARCHAR2 DEFAULT NULL,
      I_item_cost_tax_incl_ind       VARCHAR2 DEFAULT NULL,
      O_base_cost                    NUMBER DEFAULT NULL,
      O_extended_base_cost           NUMBER DEFAULT NULL,
      O_inclusive_cost               NUMBER DEFAULT NULL,
      O_negotiated_item_cost         NUMBER DEFAULT NULL,
      svat_tax_rate                  NUMBER DEFAULT NULL,
      tax_loc_type                   VARCHAR2 DEFAULT NULL,
      pack_ind                       VARCHAR2 DEFAULT NULL,
      pack_type                      VARCHAR2 DEFAULT NULL,
      dept                           NUMBER   DEFAULT NULL,
      prim_supp_currency_code        VARCHAR2 DEFAULT NULL,
      loc_prim_country               VARCHAR2 DEFAULT NULL,
      loc_prim_country_tax_incl_ind  VARCHAR2 DEFAULT NULL,
      gtax_total_tax_amount          NUMBER   DEFAULT NULL,
      gtax_total_tax_amount_nic      NUMBER   DEFAULT NULL,
      gtax_total_recover_amount      NUMBER   DEFAULT NULL    
   ) RETURN SELF AS RESULT
)
/
CREATE OR REPLACE TYPE BODY OBJ_COMP_ITEM_COST_REC AS
 CONSTRUCTOR FUNCTION OBJ_COMP_ITEM_COST_REC 
 (
      I_item                         VARCHAR2 DEFAULT NULL,
      I_nic_static_ind               VARCHAR2 DEFAULT NULL,
      I_supplier                     VARCHAR2 DEFAULT NULL,
      I_location                     VARCHAR2 DEFAULT NULL,
      I_loc_type                     VARCHAR2 DEFAULT NULL,
      I_effective_date               DATE     DEFAULT NULL,
      I_calling_form                 VARCHAR2 DEFAULT NULL,
      I_origin_country_id            VARCHAR2 DEFAULT NULL,
      I_delivery_country_id          VARCHAR2 DEFAULT NULL,
      I_prim_dlvy_ctry_ind           VARCHAR2 DEFAULT NULL,
      I_update_itemcost_ind          VARCHAR2 DEFAULT NULL,
      I_update_itemcost_child_ind    VARCHAR2 DEFAULT NULL,
      I_item_cost_tax_incl_ind       VARCHAR2 DEFAULT NULL,
      O_base_cost                    NUMBER DEFAULT NULL,
      O_extended_base_cost           NUMBER DEFAULT NULL,
      O_inclusive_cost               NUMBER DEFAULT NULL,
      O_negotiated_item_cost         NUMBER DEFAULT NULL,
      svat_tax_rate                  NUMBER DEFAULT NULL,
      tax_loc_type                   VARCHAR2 DEFAULT NULL,
      pack_ind                       VARCHAR2 DEFAULT NULL,
      pack_type                      VARCHAR2 DEFAULT NULL,
      dept                           NUMBER DEFAULT NULL,
      prim_supp_currency_code        VARCHAR2 DEFAULT NULL,
      loc_prim_country               VARCHAR2 DEFAULT NULL,
      loc_prim_country_tax_incl_ind  VARCHAR2 DEFAULT NULL,
      gtax_total_tax_amount          NUMBER   DEFAULT NULL,
      gtax_total_tax_amount_nic      NUMBER   DEFAULT NULL,
      gtax_total_recover_amount      NUMBER   DEFAULT NULL     
 ) RETURN SELF AS RESULT IS
    BEGIN
       SELF.I_item := I_item;
       SELF.I_nic_static_ind := I_nic_static_ind;
       SELF.I_supplier := I_supplier;
       SELF.I_location := I_location;
       SELF.I_loc_type := I_loc_type;
       SELF.I_effective_date := I_effective_date;
       SELF.I_calling_form := I_calling_form;
       SELF.I_origin_country_id := I_origin_country_id;
       SELF.I_delivery_country_id := I_delivery_country_id;
       SELF.I_prim_dlvy_ctry_ind := I_prim_dlvy_ctry_ind;
       SELF.I_update_itemcost_ind := I_update_itemcost_ind;
       SELF.I_update_itemcost_child_ind := I_update_itemcost_child_ind;
       SELF.I_item_cost_tax_incl_ind := I_item_cost_tax_incl_ind;
       SELF.O_base_cost := O_base_cost;
       SELF.O_extended_base_cost := O_extended_base_cost;
       SELF.O_inclusive_cost := O_inclusive_cost;
       SELF.O_negotiated_item_cost := O_negotiated_item_cost;
       SELF.svat_tax_rate := svat_tax_rate;
       SELF.tax_loc_type := tax_loc_type;
       SELF.pack_ind := pack_ind;
       SELF.pack_type := pack_type;
       SELF.dept := dept;
       SELF.prim_supp_currency_code := prim_supp_currency_code;
       SELF.loc_prim_country := loc_prim_country;
       SELF.loc_prim_country_tax_incl_ind := loc_prim_country_tax_incl_ind;
       SELF.gtax_total_tax_amount := gtax_total_tax_amount;
       SELF.gtax_total_tax_amount_nic := gtax_total_tax_amount_nic;
       SELF.gtax_total_recover_amount := gtax_total_recover_amount;   
       RETURN;
    END;  
END;
/
CREATE OR REPLACE TYPE OBJ_COMP_ITEM_COST_TBL AS TABLE OF OBJ_COMP_ITEM_COST_REC
/
