--  Table of item_amount_REC
    DROP TYPE item_amount_TBL FORCE
/

--  item_amount_REC
    DROP TYPE item_amount_REC
/

CREATE OR REPLACE TYPE item_amount_REC AS OBJECT (
ITEM                VARCHAR2(25),
ORIGIN_COUNTRY_ID   VARCHAR2(3),
AMOUNT              NUMBER(20,4),
PACK_ITEM           VARCHAR2(25),
TAX_INCL_IND        VARCHAR2(1)
)
/

CREATE OR REPLACE TYPE item_amount_TBL AS TABLE OF item_amount_REC 
/

-- Table of tax_call_REC
   DROP TYPE tax_call_TBL FORCE
/

-- tax_call_REC
   DROP TYPE tax_call_REC 
/

CREATE OR REPLACE TYPE tax_call_REC AS OBJECT (
ITEM_AMOUT_TBL                item_amount_TBL,
SOURCE_ENTITY                 VARCHAR2(10),
SOURCE_TYPE                   VARCHAR2(2),
DEST_ENTITY                   VARCHAR2(10),
DEST_TYPE                     VARCHAR2(2) 
)
/

CREATE OR REPLACE TYPE tax_call_TBL AS TABLE OF tax_call_REC
/
