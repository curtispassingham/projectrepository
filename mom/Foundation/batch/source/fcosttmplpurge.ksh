#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  fcosttmplpurge.ksh
#
#  Desc:  UNIX shell script to purge staging information for cost template upload  
#  parameter tables. The number of retention days will be fetched 
#  from SYSTEM_OPTIONS.FDN_STG_RETENTION_DAYS 
#  
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='fcosttmplpurge.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind

CURR_THREADS=1
OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> &

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.
"
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`
    
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-----------------------------------------------------------------------------
# Function Name: PURGE_CTMPL_PARAM_TABLES
# Purpose      : This will purge Cost Template records from 
#                SVC_WF_COST_TMPL_* parameter tables
#-----------------------------------------------------------------------------
function PURGE_CTMPL_PARAM_TABLES
{
   
   sqlTxt="
      DECLARE
         L_str_error_tst   VARCHAR2(1) := NULL;
         FUNCTION_ERROR    EXCEPTION;
      BEGIN
         if NOT CORESVC_WF_COST_TMPL_UPLD_SQL.PURGE_RECORDS ( :GV_script_error )then
            raise FUNCTION_ERROR;
         end if;
      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt} 
   
   if [[ $? -ne ${OK} ]]; then
       LOG_ERROR "CORESVC_WF_COST_TMPL_UPLD_SQL.PURGE_RECORDS Failed" "PURGE_CTMPL_PARAM_TABLES" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
       return ${FATAL}
   else
      LOG_MESSAGE "CORESVC_WF_COST_TMPL_UPLD_SQL.PURGE_RECORDS - Successfully Completed" "PURGE_CTMPL_PARAM_TABLES" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${OK}
   fi

}


#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   USAGE
   exit ${NON_FATAL}
fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------

connectStr=$1
USER=${connectStr%/*}


LOG_MESSAGE "${pgmName} - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

$ORACLE_HOME/bin/sqlplus -s $connectStr <<EOF >>$ERRORFILE
EOF

#--- Invoke Purge Function
PURGE_CTMPL_PARAM_TABLES

# --  Check for any Oracle errors from the SQLPLUS process
if [ `grep "${pgmPID}: Aborted" $LOGFILE | wc -l` -gt 0 ]
then
   LOG_MESSAGE "Errors encountered. See error file" "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "Program ${pgmName} terminated successfully." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

if [ `wc -l $ERRORFILE | awk '{print $1}'` -eq 0 ]
then
   rm -f $ERRORFILE
fi

exit 0
