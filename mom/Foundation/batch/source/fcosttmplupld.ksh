#! /bin/ksh
#-------------------------------------------------------------------------------------
#  File:  fcosttmplupld.ksh
#
#  Desc:  This shell script uploads cost templates and their relationships from CTMPL files into the
#         SVC_WF_COST_TMPL_UPLD_* tables.
#         These tables serves as the interface by which the RMS core cost template upload
#         process updates the system with the cost template and relationship information
#         CTMPL files.
#
#         The following are done in this script
#         1. Check if the input file exists and have appropriate access.
#         2. SQL Load (sqlldr) the input file into the SVC_WF_COST_TMPL_UPLD_* tables.
#            A fatal error from sqlldr will halt the process. Rejected records
#            is a non-fatal error and will be continue processing.
#         3. Retrieve the process ID from the paramater tables. This will serve
#            as a key for succeeding processing. One file uploaded will have
#            one unique process ID.
#         4. Insert a record into the SVC_WF_COST_TMPL_UPLD_STATUS table with a chunk ID of
#            0. This record will be used to track the status of records loaded
#            for the particular process ID.
#         5. Validate the records on the SVC_WF_COST_TMPL_UPLD_* tables. A file validation failure
#            is a fatal error and will halt processing. A transaction record validation failure
#            is a non fatal error and will reject the record.
#         6. Group or "chunk" the records on the SVC_WF_COST_TMPL_UPLD_* table.
#            This will group the records into chunks whose size is defined on the
#            RMS_PLSQL_BATCH_CONFIG table.
#         7. Create records on the SVC_WF_COST_TMPL_UPLD_STATUS tables for each chunk ID
#            generated. This will allow the succeeding batch (fcosttmplprocess.ksh)
#            to track execution per chunk.
#
#-------------------------------------------------------------------------------------

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='fcosttmplupld.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID

# File locations
CTLFILE="${MMHOME}/oracle/proc/src/fcosttmplupld.ctl"
LOGDIR="${MMHOME}/log"
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"

# Initialize variables and constants
inputfile_ext=0
REJECT="R"

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string> <input file>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <input file>       Input file. Can include directory path.

"
}

#-------------------------------------------------------------------------
# Function Name: CHECK_INPUT_FILE_EXIST
# Purpose      : Check if the input file exists
#-------------------------------------------------------------------------
function CHECK_INPUT_FILE_EXIST
{
   typeset filewithPath=$1
   
   if [[ -n `ls -1 ${filewithPath} 2>/dev/null` ]]; then
      echo ${TRUE}
   else
      LOG_ERROR "Cannot find input file ${filewithPath}." "CHECK_INPUT_FILE_EXIST" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      echo ${FALSE}
      exit ${FATAL}
   fi
  
   return ${OK}

}
#-------------------------------------------------------------------------
# Function Name: CONVERT_FILE
# Purpose      : Reads the input file, generates a seq_no for every Transaction record, appends it to the record
#                and writes it to a new file which will be used for all subsequent processing.
#-------------------------------------------------------------------------
function CONVERT_INPUT_FILE
{
   typeset filewithPath=$1
   awk '
      BEGIN {
               tseq=0;
      }
      {
         if (substr($0,1,5) == "THEAD") tseq = tseq+1;
         if (substr($0,1,5) == "FTAIL") tseq = 0;
         print tseq"|"$0 > var1;
      }
      ' var1=${filewithPath}.new ${filewithPath}
}

#-------------------------------------------------------------------------
# Function Name: GET_PROCESS_ID
# Purpose      : Retrieves the process_id from the paramater tables
#                for file loaded in the LOAD_FILE() function.
#-------------------------------------------------------------------------
function GET_PROCESS_ID
{
   CostTmplProcessId=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select MAX(process_id)
     from svc_wf_cost_tmpl_upld_fhead
    where status = 'N';
   exit;
   EOF`
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${CostTmplProcessId}" "GET_PROCESS_ID" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned Cost Template Upload process id
   if [ -z ${CostTmplProcessId} ]; then
      LOG_ERROR "Cannot retrieve process id for file $filewithPath." "GET_PROCESS_ID" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: LOAD_FILE
# Purpose      : SQL Load the input file into the parameter tables
#-------------------------------------------------------------------------
function LOAD_FILE
{

   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${inputFile##*/}
   sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
   sqlldr ${connectStr} silent=feedback,header \
      control=${CTLFILE} \
      log=${sqlldrLog} \
      data=${inputFile}.new \
      bad=${MMHOME}/log/$sqlldrFile.bad \
      discard=${MMHOME}/log/$sqlldrFile.dsc \
      discardmax=0 \
      errors=0 \
      rows=100000
    
   sqlldr_status=$?

   # Check execution status
   if [[ $sqlldr_status != ${OK} ]]; then
      LOG_ERROR "Error while loading file ${filewithPath}. See ${sqlldrLog} for details." "LOAD_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      echo "Error loading file"
      exit ${FATAL}
   else
      # Check log file for sql loader errors
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` != ""
         && `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` != "" ]];
      then
         LOG_MESSAGE "${inputFileNoPath} - Completed loading file to the cost template upload parameter tables." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      else
         LOG_MESSAGE "${inputFileNoPath} - Some rows not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
         exit ${NON_FATAL}
      fi
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Wrapper for PL/SQL package calls
#-------------------------------------------------------------------------
function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#----------------------------------------------------------------------------
# Function Name: INIT_STATUS
# Purpose      : Insert a record into the SVC_WF_COST_TMPL_UPLD_STATUS table
#----------------------------------------------------------------------------
function INIT_STATUS
{
   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT CORESVC_WF_COST_TMPL_UPLD_SQL.INITIALIZE_PROCESS_STATUS(:GV_script_error,
                                                                   ${CostTmplProcessId},
                                                                   '${inputFileNoPath}') then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}
   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function call INITIALIZE_PROCESS_STATUS Process Id: ${CostTmplProcessId} failed." >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Status record successfully created. Process Id is ${CostTmplProcessId}." "INIT_STATUS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi 
   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: VALIDATE_LOAD# Purpose      : Perform validation on loaded tables.
#              : Validation failure will result in a fatal error.
#-------------------------------------------------------------------------
function VALIDATE_LOAD
{
   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT CORESVC_WF_COST_TMPL_UPLD_SQL.VALIDATE_PARAMETER_TABLES(:GV_script_error,
                                                                   ${CostTmplProcessId}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}
   
   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function call VALIDATE_PARAMETER_TABLES Process Id: ${CostTmplProcessId} failed." >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Successfully validated: Process Id ${CostTmplProcessId}." "VALIDATE_LOAD" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}


#-------------------------------------------------------------------------
# Function Name: CHUNK_COST_TMPL_UPLD
# Purpose      :
#-------------------------------------------------------------------------
function CHUNK_COST_TMPL_UPLD
{
   chunkCount=0

   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT CORESVC_WF_COST_TMPL_UPLD_SQL.CHUNK_COST_TMPL_UPLD(:GV_script_error,
                                                           ${CostTmplProcessId}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function call CHUNK_COST_TMPL_UPLD with Process Id: ${CostTmplProcessId} failed" >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Successfully chunked records for Process Id ${CostTmplProcessId}." "CHUNK_COST_TMPL_UPLD" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: INIT_CHUNK_STATUS
# Purpose      : Perform validation on loaded tables.
#              : Validation failure will result in a fatal error.
#-------------------------------------------------------------------------
function INIT_CHUNK_STATUS
{
   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT CORESVC_WF_COST_TMPL_UPLD_SQL.INITIALIZE_CHUNK_STATUS(:GV_script_error,
                                                                 ${CostTmplProcessId}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}
   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function call INITIALIZE_CHUNK_STATUS with Process Id: ${CostTmplProcessId} failed" >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Status chunk records successfully created for Process Id ${CostTmplProcessId}." "INIT_CHUNK_STATUS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [ $# -lt 2 ]
then
   USAGE
   exit ${NON_FATAL}
fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
USER=${connectStr%/*}
inputFile=$2
inputFileNoPath=${inputFile##*/}    # remove the path
inputfile_ext=${inputFile##*.}
newinputfile=${inputFile}.1

LOG_MESSAGE "fcosttmplupld.ksh started by ${USER} for file ${inputFileNoPath}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

$ORACLE_HOME/bin/sqlplus -s $connectStr <<EOF >>$ERRORFILE
EOF

# Check if input file exists.
CHECK_INPUT_FILE_EXIST $inputFile

#Convert input file
CONVERT_INPUT_FILE $inputFile

# Load the cost template upload parameter tables
LOAD_FILE

# Retrieve the process id for the loaded file
GET_PROCESS_ID

# Insert entry into the status table to track this upload
INIT_STATUS

# Validate the records loaded into the parameter tables
VALIDATE_LOAD

# Chunk the records in the parameter tables
CHUNK_COST_TMPL_UPLD

# Create status records for the chunked records
INIT_CHUNK_STATUS

LOG_MESSAGE "fcosttmplupld.ksh finished for ${USER} for file ${inputFile}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

if [ `wc -l $ERRORFILE | awk '{print $1}'` -eq 0 ]
then
   rm -f $ERRORFILE
fi

exit ${OK}
