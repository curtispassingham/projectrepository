#!/bin/ksh
#################################################################################################
#                        Program Header
#################################################################################################
#
# FILE:         trunctbl.ksh  
#
# DESCRIPTION:  Main script that Truncates a Table or specific partitions.
#
#  USAGE:   trunctable.ksh <Connect String> <Table Name> <Partition Name>
#
#           See USAGE
#
##################################################################################################
 
#################################################################################################
#                     Variable Definitions
#################################################################################################

# Global Constants
GC_cProgramName=`basename ${0}`
GC_cPgmFileBaseName=${GC_cProgramName%.*}
GC_cPgmPID=$$

GC_cConnectString=""
GC_cTableName=""
GC_cPartitionName=""

GC_cError="ERROR"
GC_cLog="LOG"

# TIMESTAMP
exeStamp=`date +"%h_%d"`

# Global File/Directories
GC_cLogFile="${MMHOME}/log/${exeStamp}.log"
GC_cErrorFile="${MMHOME}/error/err.${GC_cPgmFileBaseName}.${exeStamp}.${GC_cPgmPID}"

#################################################################################################
#                     Function Declarations
#################################################################################################

###############################################################################
#
# FUNCTION NAME: LOG_MSG
#
# DESCRIPTION:   This function writes to either error or log. If it writes an error, program aborts.
#
###############################################################################
function LOG_MSG
{

l_cMsg="${1}"
l_cFunc="${2}"
l_cRetCode=${3}

if [[ ${l_cRetCode} = ${GC_cLog} ]]
then
   LOG_MESSAGE "${l_cMsg}" "${l_cFunc}" ${OK} ${GC_cLogFile} ${GC_cPgmFileBaseName} ${GC_cPgmPID}
   return ${OK}
else
   LOG_ERROR "${l_cMsg}" "${l_cFunc}" ${FATAL} ${GC_cErrorFile} ${GC_cLogFile} ${GC_cPgmFileBaseName}
   exit ${FATAL}
fi

}

#################################################################################################
#
# FUNCTION NAME: USAGE
# DESCRIPTION:   This function will display the correct script usage
#
#################################################################################################
function USAGE
{

echo ""
echo " USAGE: ${GC_cPgmFileBaseName} <Connect String> <Table Name> <Partition Name>"
echo "    <Connect String> - Oracle SQL Login."
echo "    <Table Name>     - Table name to be processed."
echo "    <Partition Name> - Partition to be processed. This is optional. All partitions"
echo "                       will be processed if left blank."
echo ""

exit ${FATAL}

}

###############################################################################
#
# FUNCTION NAME: PROCESSARG
#
# DESCRIPTION:   This function parses and checks parameters passed to the program.
#
###############################################################################
function PROCESSARG
{

GC_cConnectString=${1}
GC_cTableName=${2}
GC_cPartitionName=${3}

## Check all required parameters.
if [[ -z "${GC_cConnectString}" || -z "${GC_cTableName}" ]]
then
   USAGE
fi

l_cUser=${GC_cConnectString%/*}

LOG_MSG "Started by ${l_cUser}" "" ${GC_cLog}

${ORACLE_HOME}/bin/sqlplus -s $GC_cConnectString <<EOF >>${GC_cErrorFile}
EOF

## if an error log is generated and is not empty, then connection to oracle failed.
if [[ -s ${GC_cErrorFile} ]]
then
   LOG_MSG "Aborted in function. Error connecting to Oracle." ${0} ${GC_cError}
fi

LOG_MSG "Function Successfully Completed" ${0} ${GC_cLog}
}

###############################################################################
#
# FUNCTION NAME: TRUNCATE_TABLE
#
# DESCRIPTION: This function calls TRUNCATE_TABLE PL/SQL Function to
#              execute the TRUNCATE.
#
###############################################################################
function TRUNCATE_TABLE
{

sqlTxt="
   DECLARE
      L_error_message   VARCHAR2(255);
      L_table_name      VARCHAR2(100)  := '${GC_cTableName}';
      L_partition_name  VARCHAR2(100)  := '${GC_cPartitionName}';
   BEGIN
      IF TRUNCATE_TABLE (L_error_message,
                         L_table_name,
                         L_partition_name) = FALSE THEN
         :GV_script_error := L_error_message;
         :GV_return_code := 255;
      END iF;
   END;"

EXEC_SQL_T "${sqlTxt}" "${GC_cConnectString}" "" ${GC_cErrorFile} ${GC_cLogFile} ${GC_cPgmFileBaseName} ${GC_cPgmPID}

retCode=$?

## if an error occurred while executing the SQL statement, EXEC_SQL_T will do the logging, so we just need to abort.
if [[ ${retCode} -ne ${OK} ]]
then
   exit ${retCode}
fi   

LOG_MSG "Function Successfully Completed" ${0} ${GC_cLog}

}

#################################################################################################
#                     Retek Functional Library
#################################################################################################

. ${MMHOME}/oracle/lib/src/rmsksh.lib
if [[ ${?} -ne 0 ]]
then
   echo "${0}: Error loading rmsksh.lib"
   exit 1
fi

#################################################################################################
#
#                 Start of Program Execution
#
#################################################################################################
GC_iNbrParm=${#}
GC_cCmdLnAll=${*}

if [[ ${GC_iNbrParm} -lt 2 ]]
then
   USAGE
fi

PROCESSARG "${@}"
TRUNCATE_TABLE

LOG_MSG "Program successfully completed." "" ${GC_cLog}

## Perform cleanup.
if [[ ! -s "${GC_cErrorFile}" ]]
then
   rm -f ${GC_cErrorFile}
fi

exit ${OK}
