#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  export_itemvat.ksh
#
#  Desc:  UNIX shell script to extract the itemloc records.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='export_itemvat.ksh'
pgmName=${pgmName##*/}               # get the file name
pgmExt=${pgmName##*.}                # get the extenstion
pgmName=${pgmName%.*}                # remove the file extenstion
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
DIR=`pwd`

OK=0
FATAL=255
#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <mode><thread indicator> <# parallel threads> <store>

   <connect string> is a fully qualified connection string in format:  Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <mode> is the mode of extraction. full or delta

   <thread indicator> indicates if user will provide a thread number. either Y or N.

   <# of parallel threads> is the number of threads to run in parallel if thread indicator is Y. It should be a number between 1-20, optional

   <store> is a specific store number to create a location level file for. This is optional and only valid for a full extract"

}
#-------------------------------------------------------------------------
# Function Name: UPDATE_EXPORT_STG
# Purpose      : Update the item_export_stg with current process_id
#-------------------------------------------------------------------------
function UPDATE_EXPORT_STG
{
process_id=$1
echo "Updating the item_export_stg with process_id: $process_id" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

VARIABLE GV_script_error CHAR(255);
VARIABLE GV_return_code NUMBER;
VARIABLE GV_process_id NUMBER;

EXEC :GV_process_id := ${process_id};
EXEC :GV_script_error := NULL;
EXEC :GV_return_code := 0;

WHENEVER SQLERROR EXIT 1;

  update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where stg.base_extracted_ind = 'N'
     and stg.process_id is null
     and stg.action_type in ('vatitemcre','vatitemmod')
     and exists (select 'Y' from item_master im,item_export_info inf
                  where stg.item = im.item
                    and stg.item = inf.item
                    and im.item = inf.item
                    and im.status = 'A'
                    and im.sellable_ind = 'Y'
                    and inf.base_extracted_ind = 'Y'
                    and inf.process_id is not null);


  update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where stg.base_extracted_ind = 'N'
     and stg.process_id is null
     and stg.action_type in ('vatitemdel');

   COMMIT;
  /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: UPDATE_SINGLE_STORE_STG
# Purpose:  Updating the item_export_stg for single store.
#-------------------------------------------------------------------------
function UPDATE_SINGLE_STORE_STG
{
process_id=$1
str=$2
echo "Updating the item_export_stg with process_id: $process_id for store: $str" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

   VARIABLE GV_store NUMBER;
   VARIABLE GV_script_error CHAR(255);
   VARIABLE GV_process_id NUMBER;
   VARIABLE GV_return_code NUMBER;

   EXEC :GV_store := ${str};
   EXEC :GV_script_error := NULL;
   EXEC :GV_process_id := ${process_id};
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;

  update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where stg.base_extracted_ind = 'N'
     and stg.process_id is null
     and stg.action_type in ('vatitemcre','vatitemmod')
     and exists (select 'Y' from item_master im,item_export_info inf
                  where stg.item = im.item
                    and stg.item = inf.item
                    and im.item = inf.item
                    and im.status = 'A'
                    and im.sellable_ind = 'Y'
                    and inf.base_extracted_ind = 'Y'
                    and inf.process_id is not null)
     and stg.vat_region in (select vat_region from store where store = trim(:GV_store));


  update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where stg.base_extracted_ind = 'N'
     and stg.process_id is null
     and stg.action_type in ('vatitemdel')
     and stg.vat_region in (select vat_region from store where store = trim(:GV_store));

   COMMIT;
  /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      status=${FATAL}
   else
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_STG
# Purpose      : Update the exported records
#-------------------------------------------------------------------------
function PROCESS_STG
{
process_id=$1
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000
VARIABLE GV_script_error CHAR(255);
VARIABLE GV_return_code NUMBER;
VARIABLE GV_process_id NUMBER;

EXEC :GV_process_id := ${process_id};
EXEC :GV_script_error := NULL;
EXEC :GV_return_code := 0;

WHENEVER SQLERROR EXIT 1;

   update item_export_stg
      set base_extracted_ind = 'Y'
    where action_type in ('vatitemcre','vatitemmod','vatitemdel')
      and process_id = trim(:GV_process_id);

   COMMIT;
  /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#-------------------------------------------------------------------------
# Function Name: CREATE_STORE_LIST
# Purpose      : Create store list.
#-------------------------------------------------------------------------
function CREATE_STORE_LIST
{
echo "Creating the store list" >> $LOGFILE
LOC_LIST=$DIR/storelist_${extractDate}.$$
if [[ $1 = "base" ]]; then
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$LOC_LIST
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   set linesize 1000;
    select to_char(st.store)  store,
           st.vat_region      vat_region
      from store st,
           period p
     where p.vdate <= nvl(st.store_close_date,p.vdate)
       and st.store_type in ('C','F');
EOF
fi
if [[ $1 = "stg" ]]; then
process_id=$2
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$LOC_LIST
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0
   VARIABLE GV_process_id NUMBER;
   EXEC :GV_process_id := ${process_id};
   select distinct to_char(lc.store),
                   to_char(stg.vat_region) 
              from item_export_stg stg,
                   vat_region vr,
                   (select to_char(st.store)     store,
                              st.vat_region      vat_region
                         from store st,
                              period p
                        where p.vdate <= nvl(st.store_close_date,p.vdate)
                          and st.store_type in ('C','F')) lc
             where stg.vat_region = lc.vat_region
               and vr.vat_region = lc.vat_region
               and vr.vat_calc_type <> 'E'
               and stg.base_extracted_ind = 'N'
               and stg.action_type in ('vatitemcre','vatitemmod','vatitemdel')
               and process_id = trim(:GV_process_id);
EOF
fi
if [[ `grep "^ORA-" $LOC_LIST | wc -l` -gt 0 ]]; then
   cat $LOC_LIST >> ${ERRORFILE}
   status=${FATAL}
   rm $LOC_LIST
else
   status=${OK}
fi


return $status
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_FULL_CORPS
# Purpose      : Exporting the full vat item for corporate.
#-------------------------------------------------------------------------
function PROCESS_FULL_CORPS
{
echo "Extracting the full vat item records for corporate" >> $LOGFILE
VATITEMCORPFULL=$DIR/vatitem_${extractDate}_corp_full
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$VATITEMCORPFULL
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0
SELECT Family            ||'|'||
       Type              ||'|'||
       Item              ||'|'||
       VatRegion         ||'|'||
       ActiveDate        ||'|'||
       VatType           ||'|'||
       VatCode           ||'|'||
       VatRate           ||'|'||
       ReverseVatInd
 FROM
    (  SELECT 'VATITEM'                                Family
             ,'FULL'                                   Type
             ,vt.item                                  Item
             ,vt.vat_region                            VatRegion
             ,TO_CHAR(vt.active_date, 'DD-MON-YYYY')   ActiveDate
             ,vt.vat_type                              VatType
             ,vt.vat_code                              VatCode
             ,vt.vat_rate                              VatRate
             ,vt.reverse_vat_ind                       ReverseVatInd
         FROM vat_item vt,
              item_master im,
              item_export_info iei
        WHERE vt.item = im.item
          AND im.status = 'A'
          AND im.sellable_ind = 'Y'
          AND im.item = iei.item
          AND iei.base_extracted_ind = 'Y'
          AND iei.item = vt.item
          order by vt.item,
                   vt.active_date
    ) vatfullcorpitem;

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('vatitem',
                                'full',
                                 USER,
                                 SYSDATE);
EOF
   if [[ `grep "^ORA-" $VATITEMCORPFULL | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $VATITEMCORPFULL >> $ERRORFILE
      rm $VATITEMCORPFULL
   else
      COUNTER=`wc -l ${VATITEMCORPFULL}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${VATITEMCORPFULL}" "${VATITEMCORPFULL}"_${COUNTER}.dat
      else
         rm ${VATITEMCORPFULL}
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_FULL
# Purpose      : Exporting the full vat item records.
#-------------------------------------------------------------------------
function PROCESS_FULL
{
store=$1
vat_region=$2
echo "Extracting the full vat item records for store: $store" >> $LOGFILE
VATITEMFULL=$DIR/vatitem_${extractDate}_${store}_full
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$VATITEMFULL
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0
   VARIABLE GV_vat_region NUMBER;
   VARIABLE GV_store NUMBER;
   EXEC :GV_vat_region := ${vat_region};
   EXEC :GV_store := ${store};
SELECT Family            ||'|'||
       Type              ||'|'||
       Item              ||'|'||
       VatRegion         ||'|'||
       ActiveDate        ||'|'||
       VatType           ||'|'||
       VatCode           ||'|'||
       VatRate           ||'|'||
       ReverseVatInd
 FROM
    (  SELECT 'VATITEM'                                Family
             ,'FULL'                                   Type
             ,vt.item                                  Item
             ,vt.vat_region                            VatRegion
             ,TO_CHAR(vt.active_date, 'DD-MON-YYYY')   ActiveDate
             ,vt.vat_type                              VatType
             ,vt.vat_code                              VatCode
             ,vt.vat_rate                              VatRate
             ,vt.reverse_vat_ind                       ReverseVatInd
         FROM vat_item vt,
              item_master im,
              item_loc il,
              item_export_info iei,
              vat_region vr
        WHERE vt.item = im.item
          AND im.item = il.item
          AND il.loc = trim(:GV_store)
          AND im.status = 'A'
          AND im.sellable_ind = 'Y'
          AND im.item = iei.item
          AND iei.base_extracted_ind = 'Y'
          AND iei.item = vt.item
          AND vt.vat_region = trim(:GV_vat_region)
          AND vr.vat_region = vt.vat_region
          AND vr.vat_calc_type <> 'E'
        ORDER BY vt.item,
                 vt.active_date
    ) vatfullitem;
EOF

   if [[ `grep "^ORA-" $VATITEMFULL | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $VATITEMFULL >> $ERRORFILE
      rm $VATITEMFULL
   else
      COUNTER=`wc -l ${VATITEMFULL}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${VATITEMFULL}" "${VATITEMFULL}"_${COUNTER}.dat
      else
         rm ${VATITEMFULL}
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_SINGLE_STORE
# Purpose      : Exporting the vat item records for single store.
#-------------------------------------------------------------------------
function PROCESS_SINGLE_STORE
{
str=$1
echo "Extracting the full vat item records for store: $str" >> $LOGFILE
VATITEMDSTRFULL=$DIR/vatitem_${extractDate}_${str}_full
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$VATITEMDSTRFULL
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   VARIABLE GV_store NUMBER;
   EXEC :GV_store := $str;
SELECT Family            ||'|'||
       Type              ||'|'||
       Item              ||'|'||
       VatRegion         ||'|'||
       ActiveDate        ||'|'||
       VatType           ||'|'||
       VatCode           ||'|'||
       VatRate           ||'|'||
       ReverseVatInd
 FROM
    (  SELECT 'VATITEM'                                Family
             ,'FULL'                                   Type
             ,vt.item                                  Item
             ,vt.vat_region                            VatRegion
             ,TO_CHAR(vt.active_date, 'DD-MON-YYYY')   ActiveDate
             ,vt.vat_type                              VatType
             ,vt.vat_code                              VatCode
             ,vt.vat_rate                              VatRate
             ,vt.reverse_vat_ind                       ReverseVatInd
         FROM vat_item vt,
              item_master im,
              item_loc il,
              item_export_info iei,
              store st,
              vat_region vr
        WHERE vt.item = im.item
          AND im.item = il.item
          AND st.store = trim(:GV_store)
          AND il.loc = st.store
          AND im.status = 'A'
          AND im.sellable_ind = 'Y'
          AND im.item = iei.item
          AND iei.base_extracted_ind = 'Y'
          AND iei.item = vt.item
          AND vt.vat_region = st.vat_region
          AND vr.vat_region = vt.vat_region
          AND vr.vat_calc_type <> 'E'
        ORDER BY vt.item,
                 vt.active_date
    ) vatfullitem;
EOF

   if [[ `grep "^ORA-" $VATITEMDSTRFULL | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $VATITEMDSTRFULL >> $ERRORFILE
      rm $VATITEMDSTRFULL
   else
      COUNTER=`wc -l ${VATITEMDSTRFULL}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${VATITEMDSTRFULL}" "${VATITEMDSTRFULL}"_${COUNTER}.dat
      else
         rm ${VATITEMDSTRFULL}
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_FULL_CORP_DELTA
# Purpose      : Extracting delta item records for corporate.
#-------------------------------------------------------------------------
function PROCESS_FULL_CORP_DELTA
{
process_id=$1
echo "Extracting the delta vat item records for corporate" >> $LOGFILE
VATITEMDELTACORP=$DIR/vatitem_${extractDate}_corp_delta
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$VATITEMDELTACORP
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

VARIABLE GV_process_id NUMBER;

EXEC :GV_process_id := ${process_id};


SELECT Family            ||'|'||
       Type              ||'|'||
       Item              ||'|'||
       VatRegion         ||'|'||
       ActiveDate        ||'|'||
       VatType           ||'|'||
       VatCode           ||'|'||
       VatRate           ||'|'||
       ReverseVatInd
 FROM
    ( SELECT * FROM
      (  SELECT 'VATITEM'                                     Family
               ,upper(stg.action_type)                        Type
               ,stg.item                                      Item
               ,stg.vat_region                                VatRegion
               ,TO_CHAR(stg.vat_active_date, 'DD-MON-YYYY')   ActiveDate
               ,stg.vat_type                                  VatType
               ,stg.vat_code                                  VatCode
               ,vt.vat_rate                                   VatRate
               ,vt.reverse_vat_ind                            ReverseVatInd
               ,stg.seq_no
          FROM item_export_stg stg,
               vat_item vt
         WHERE stg.item = vt.item(+)
           AND stg.process_id = trim(:GV_process_id)
           AND stg.vat_region = vt.vat_region(+)
           AND stg.vat_code = vt.vat_code(+)
           AND stg.vat_type = vt.vat_type(+)
           AND stg.vat_active_date = vt.active_date(+)
           AND stg.base_extracted_ind = 'N'
           AND stg.action_type in ('vatitemcre','vatitemmod','vatitemdel')
           order by stg.seq_no,
                    stg.item,
                    stg.vat_active_date
      )
    ) vatdeltaitemcorp;

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('vatitem',
                                'delta',
                                 USER,
                                 SYSDATE);
EOF

   if [[ `grep "^ORA-" $VATITEMDELTACORP | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $VATITEMDELTACORP >> $ERRORFILE
      rm $VATITEMDELTACORP
   else
      COUNTER=`wc -l ${VATITEMDELTACORP}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${VATITEMDELTACORP}" "${VATITEMDELTACORP}"_${COUNTER}.dat
      else
         rm ${VATITEMDELTACORP}
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_DELTA
# Purpose      : Exporting the delta vat item records.
#-------------------------------------------------------------------------
function PROCESS_DELTA
{
loc=$1
vat_region=$2
process_id=$3
echo "Extracting the delta vat item records for store: $loc" >> $LOGFILE
VATITEMDELTA=$DIR/vatitem_${extractDate}_${loc}_delta
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$VATITEMDELTA
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

VARIABLE GV_process_id NUMBER;
VARIABLE GV_store NUMBER;
VARIABLE GV_vat_region NUMBER;

EXEC :GV_process_id := ${process_id};
EXEC :GV_store := ${loc};
EXEC :GV_vat_region := ${vat_region};


SELECT Family            ||'|'||
       Type              ||'|'||
       Item              ||'|'||
       VatRegion         ||'|'||
       ActiveDate        ||'|'||
       VatType           ||'|'||
       VatCode           ||'|'||
       VatRate           ||'|'||
       ReverseVatInd
 FROM
    ( SELECT * FROM
      (  SELECT 'VATITEM'                                     Family
               ,upper(stg.action_type)                        Type
               ,stg.item                                      Item
               ,stg.vat_region                                VatRegion
               ,TO_CHAR(stg.vat_active_date, 'DD-MON-YYYY')   ActiveDate
               ,stg.vat_type                                  VatType
               ,stg.vat_code                                  VatCode
               ,vt.vat_rate                                   VatRate
               ,vt.reverse_vat_ind                            ReverseVatInd
               ,stg.seq_no                                    seq_no
          FROM item_export_stg stg,
               vat_item vt,
               item_loc il
         WHERE stg.item = vt.item
           AND stg.item = il.item
           AND il.loc = trim(:GV_store)
           AND stg.vat_region = trim(:GV_vat_region)
           AND stg.vat_region = vt.vat_region
           AND stg.vat_code = vt.vat_code
           AND stg.vat_type = vt.vat_type
           AND stg.vat_active_date = vt.active_date
           AND stg.base_extracted_ind = 'N'
           AND stg.action_type in ('vatitemcre','vatitemmod','vatitemdel')
           AND stg.process_id = trim(:GV_process_id)
         UNION
         SELECT 'VATITEM'                                     Family
               ,upper(stg.action_type)                        Type
               ,stg.item                                      Item
               ,stg.vat_region                                VatRegion
               ,TO_CHAR(stg.vat_active_date, 'DD-MON-YYYY')   ActiveDate
               ,stg.vat_type                                  VatType
               ,stg.vat_code                                  VatCode
               ,NULL                                          VatRate
               ,NULL                                          ReverseVatInd
               ,stg.seq_no                                    seq_no
          FROM item_export_stg stg,
               item_export_stg stg1
         WHERE stg.item = stg1.item
           AND stg1.base_extracted_ind = 'N'
           AND stg.base_extracted_ind = 'N'
           AND stg.action_type = 'vatitemdel'
           AND stg1.action_type = 'itemlocdel'
           AND stg1.loc = trim(:GV_store)
           AND stg.vat_region = trim(:GV_vat_region)
           AND stg.process_id = trim(:GV_process_id) 
       )
           order by seq_no,
                    Item,
                    ActiveDate
    ) vatdeltaitem;
EOF

   if [[ `grep "^ORA-" $VATITEMDELTA | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $VATITEMDELTA >> $ERRORFILE
      rm $VATITEMDELTA
   else
      COUNTER=`wc -l ${VATITEMDELTA}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${VATITEMDELTA}" "${VATITEMDELTA}"_${COUNTER}.dat
      else
         rm ${VATITEMDELTA}
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
#           MAIN
#-------------------------------------------------------------------------

if [[ $# -lt 1 || $# -gt 5 || $# -lt 3 ]]; then
   USAGE
   exit 1
fi

CONNECT=$1
connect=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`
conn_status=$?
if [[ ${conn_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${conn_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "export_itemvat.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

current_process_id=`${ORACLE_HOME}/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select export_process_id_seq.nextval
  from dual;
exit;
EOF`
prc_id_status=$?

if [[ ${prc_id_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "GETTING_PROCESS_ID" "${prc_id_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
fi

### Check if no. of threads is passed as arg or not, else default.
if [[ $3 = "Y" ]]; then
   SLOTS=$4
   if [[ -z ${SLOTS} ]]; then
     SLOTS=10
   fi
   STR=$5
   echo $SLOTS | egrep '^[0-9]+$'>/dev/null 2>&1
   status=$?
   if [ "$status" -ne "0" ]
   then
     LOG_ERROR "Thread count should be a Positive Number" "NUM_THREADS" "${status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
     exit 1
   fi
   if [[ ${SLOTS} -gt 20 ]]; then
    SLOTS=10
   fi
else
   if [[ $3 = "N" ]]; then
   STR=$4
   SLOTS=10
   else
     LOG_ERROR "Thread number indicator either should be Y or N" "INDICATOR" "$3" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
     exit 1
   fi
fi


if [[ $2 = "full" ]]; then
   if [[ -z ${STR} ]]; then 
       UPDATE_EXPORT_STG $current_process_id
       upd_exp_st=$?
       if [[ ${upd_exp_st} -ne ${OK} ]]; then
          LOG_ERROR "Error while updating item_export_stg with current process_id" "UPDATE_EXPORT_STG" "${upd_exp_st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
          exit 1
       fi
       tbl="base"
       CREATE_STORE_LIST $tbl
       crt_loc_lst=$?
       if [[ ${crt_loc_lst} -ne ${OK} ]]; then
          LOG_ERROR "Error while updating item_export_stg with current process_id" "CREATE_STORE_LIST" "${crt_loc_lst}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
          exit 1
       fi
       while read store vat_region
       do
         if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
            (
              PROCESS_FULL $store $vat_region 
             )&
             prc_full_status=$?
             if [[ ${prc_full_status} -ne ${OK} ]]; then
                LOG_ERROR "Error while updating item_export_stg with current process_id" "PROCESS_FULL" "${prc_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                exit 1
             fi
         else
            while [ `jobs | wc -l` -ge ${SLOTS} ]
            do
              sleep 1
            done
            (
              PROCESS_FULL $store $vat_region 
             )&
             prc_full_status=$?
             if [[ ${prc_full_status} -ne ${OK} ]]; then
                LOG_ERROR "Error while updating item_export_stg with current process_id" "PROCESS_FULL" "${prc_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                exit 1
             fi
         fi
       done < $LOC_LIST
         ### Wait for all of the threads to complete
            wait
         ### Extracting records for corporate office
          PROCESS_FULL_CORPS
          prc_crp=$?
          if [[ ${prc_crp} -ne ${OK} ]]; then
             LOG_ERROR "Error while updating item_export_stg for processed records" "PROCESS_FULL_CORPS" "${prc_crp}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
          fi
          PROCESS_STG $current_process_id
          prc_stg_status=$?
          if [[ ${prc_stg_status} -ne ${OK} ]]; then
             LOG_ERROR "Error while updating item_export_stg for processed records" "PROCESS_STG" "${prc_stg_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
          else
             rm $LOC_LIST
             LOG_MESSAGE "export_itemvat.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
          fi
   else
      str=${STR}
      UPDATE_SINGLE_STORE_STG ${current_process_id} ${str}
      upd_sin_str_stg=$?
      if [[ ${upd_sin_str_stg} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating item_export_stg with current process_id" "UPDATE_SINGLE_STORE_STG" "${upd_sin_str_stg}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      fi
      PROCESS_FULL_CORPS
      prc_crp=$?
      if [[ ${prc_crp} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating item_export_stg for processed records" "PROCESS_FULL_CORPS" "${prc_crp}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      fi
      PROCESS_SINGLE_STORE $str
      prc_sin_str=$?
      if [[ ${prc_sin_str} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating item_export_stg with current process_id" "PROCESS_SINGLE_STORE" "${prc_sin_str}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      fi
      PROCESS_STG $current_process_id
      prc_stg_status=$?
      if [[ ${prc_stg_status} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating item_export_stg for processed records" "PROCESS_STG" "${prc_stg_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      else
         LOG_MESSAGE "export_itemvat.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      fi      
   fi
fi

if [[ $2 = "delta" ]]; then
   UPDATE_EXPORT_STG $current_process_id
   upd_exp_st=$?
   if [[ ${upd_exp_st} -ne ${OK} ]]; then
      LOG_ERROR "Error while updating item_export_stg with current process_id" "UPDATE_EXPORT_STG" "${upd_exp_st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
      exit 1
   fi
   tbl="stg"
   CREATE_STORE_LIST $tbl $current_process_id
   crt_loc_lst=$?
   if [[ ${crt_loc_lst} -ne ${OK} ]]; then
      LOG_ERROR "Error while updating item_export_stg with current process_id" "CREATE_STORE_LIST" "${crt_loc_lst}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
      exit 1
   fi
   while read loc vat_region
   do
     if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
        (
          PROCESS_DELTA $loc $vat_region $current_process_id
         ) &
        prc_delta_status=$?
        if [[ ${prc_delta_status} -ne ${OK} ]]; then
           LOG_ERROR "Error while updating item_export_stg with current process_id" "PROCESS_DELTA" "${prc_delta_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
           exit 1
        fi
     else
        while [ `jobs | wc -l` -ge ${SLOTS} ]
        do
          sleep 1
        done
        (
          PROCESS_DELTA $loc $vat_region $current_process_id
         ) &
        prc_delta_status=$?
        if [[ ${prc_delta_status} -ne ${OK} ]]; then
           LOG_ERROR "Error while updating item_export_stg with current process_id" "PROCESS_DELTA" "${prc_delta_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
           exit 1
        fi
     fi
   done < $LOC_LIST
   ### Wait for all of the threads to complete
    wait
   ### Extracting records for corporate office
    PROCESS_FULL_CORP_DELTA $current_process_id
    prc_crp_dlt=$?
    if [[ ${prc_crp_dlt} -ne ${OK} ]]; then
       LOG_ERROR "Error while updating item_export_stg for processed records" "PROCESS_FULL_CORP_DELTA" "${prc_crp_dlt}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
       exit 1
    fi
      PROCESS_STG $current_process_id
      prc_stg_status=$?
      if [[ ${prc_stg_status} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating item_export_stg for processed records" "PROCESS_STG" "${prc_stg_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      else
         rm $LOC_LIST
         LOG_MESSAGE "export_itemvat.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      fi
   
fi
