#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  export_itemmaster.ksh
#
#  Desc:  UNIX shell script to extract the item_master records.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='export_itemmaster.ksh'
pgmName=${pgmName##*/}               # get the file name
pgmExt=${pgmName##*.}                # get the extenstion
pgmName=${pgmName%.*}                # remove the file extenstion
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
DIR=`pwd`

OK=0
FATAL=255
#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <mode> <thread indicator> <# parallel threads> <store>

   <connect string> is a fully qualified connection string in format:  Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <mode> is the mode of extraction. full or delta

   <thread indicator> indicates if user will provide a thread number. either Y or N.

   <# of parallel threads> is the number of threads to run in parallel if thread indicator is Y. It should be a number between 1-20, optional

   <store> is a specific store number to create a location level file for. This is optional and only valid for a full extract"
}
#-------------------------------------------------------------------------
# Function: UPDATE_ITEM_EXPORT_STG
# Desc: Updating the item_export_stg with current process id
#-------------------------------------------------------------------------
function UPDATE_ITEM_EXPORT_STG
{
process_id=$1
echo "Updating item_export_stg with process id: $process_id" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

  VARIABLE GV_process_id NUMBER;
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_process_id := ${process_id};
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;

   update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemhdrmod')
     and process_id is null
     and loc is null
     and exists (select 'Y' from item_master im where stg.item = im.item and im.status = 'A');


   update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemhdrdel')
     and loc is null
     and process_id is null;

  update item_export_info inf
     set process_id = trim(:GV_process_id)
    where base_extracted_ind = 'N'
      and exists (select 'Y' from item_master im where inf.item = im.item and im.status = 'A')
      and process_id is null;

    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_STG
# Desc: Updating the exported item_export_stg records
#-------------------------------------------------------------------------
function PROCESS_STG
{
process_id=$1
echo "Updating exported records" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

  VARIABLE GV_process_id NUMBER;
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_process_id := ${process_id};
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;
   update item_export_stg
     set base_extracted_ind = 'Y'
   where base_extracted_ind = 'N'
     and action_type in ('itemhdrmod','itemhdrdel')
     and process_id = trim(:GV_process_id);

   update item_export_info
     set base_extracted_ind = 'Y'
    where base_extracted_ind = 'N'
      and process_id = trim(:GV_process_id);

    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#-----------------------------------------------------------------------------
# Function: UPDATE_SINGLE_STORE_ITEM_EXPORT_STG
# Desc: Updating the item_export_stg with current process id for single store.
#-----------------------------------------------------------------------------
function UPDATE_SINGLE_STORE_ITEM_EXPORT_STG
{
process_id=${current_process_id}
store=$1
echo "Updating item_export_stg with process id for store: $store" >> $LOGFILE
sqlreturn=`echo "set feedback off;
   set heading off;
   set term off;
   set verify off;
   set serveroutput on size 1000000;

  VARIABLE GV_process_id NUMBER;
  VARIABLE GV_store NUMBER;
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_process_id := ${process_id};
   EXEC :GV_store := '$store';
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;

   update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemhdrmod')
     and process_id is null
     and loc is null
     and exists (select 'Y' from item_master im where stg.item = im.item and im.status = 'A')
     and exists (select 'Y' from item_loc il where il.item = stg.item and trim(il.loc) = trim(:GV_store));

   update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemhdrdel')
     and loc is null
     and process_id is null
     and exists (select 'Y' from item_Export_stg stg1 where stg1.item = stg.item and trim(stg1.loc) = trim(:GV_store) and stg1.base_extracted_ind = 'N' and stg1.action_type = 'itemlocdel');

   update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemhdrdel')
     and item_parent is not null
     and process_id is null
     and loc is null
     and exists (select 'Y'
                   from item_export_stg stg1
                  where (stg1.item = stg.item_parent or stg1.item = stg.item_grandparent)
                    and trim(stg1.loc) = trim(:GV_store)
                    and stg1.base_extracted_ind = 'N'
                    and stg1.action_type = 'itemlocdel'
                  union
                 select 'Y'
                   from item_loc il
                  where (il.item = stg.item_parent or il.item = stg.item_grandparent)
                    and trim(il.loc) = trim(:GV_store));

  update item_export_info inf
     set process_id = trim(:GV_process_id)
    where base_extracted_ind = 'N'
      and exists (select 'Y' from item_master im where inf.item = im.item and im.status = 'A')
      and exists (select 'Y' from item_loc il where il.item = inf.item and trim(il.loc) = trim(:GV_store))
      and process_id is null;

    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_SINGLE_STORE_STG
# Desc: Updating the exported item_export_stg records for single store.
#-------------------------------------------------------------------------
function PROCESS_SINGLE_STORE_STG
{
process_id=${current_process_id}
echo "Updating exported records for store: $store" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

  VARIABLE GV_process_id NUMBER;
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_process_id := ${process_id};
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;
   update item_export_stg stg
     set base_extracted_ind = 'Y'
   where base_extracted_ind = 'N'
     and action_type in ('itemhdrmod','itemhdrdel')
     and process_id = trim(:GV_process_id);

   update item_export_info inf
     set base_extracted_ind = 'Y'
    where base_extracted_ind = 'N'
      and process_id = trim(:GV_process_id);

    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: CREATE_STORE_LIST
# Desc: Creating the store list
#-------------------------------------------------------------------------
function CREATE_STORE_LIST
{
STORE_LIST=$DIR/storelist_${extractDate}.$$
echo "Creating the store list" >> $LOGFILE
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$STORE_LIST
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select st.store
     from store st,
          period p
    where p.vdate <= nvl(st.store_close_date,p.vdate)
      and st.store_type in ('C','F')
    order by store;
EOF
}
#-------------------------------------------------------------------------
# Function: CREATE_STG_STORE_LIST
# Desc: Creating the store list
#-------------------------------------------------------------------------
function CREATE_STG_STORE_LIST
{
STG_STORE_LIST=$DIR/storelist_${extractDate}.$$
echo "Creating the store list for delta records." >> $LOGFILE
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$STG_STORE_LIST
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
  
   select il.loc loc
     from item_loc il,
          item_export_stg stg,
          item_master im
    where stg.base_extracted_ind = 'N'
      and stg.action_type = 'itemhdrmod'
      and stg.item = il.item
      and il.loc_type = 'S'
      and im.item = stg.item
      and im.status = 'A'
   union
   select distinct il.loc loc
     from item_loc il,
          item_export_stg stg,
          item_master im
    where stg.base_extracted_ind = 'N'
      and stg.action_type = 'itemhdrmod'
      and stg.item = im.item
      and im.status = 'A'
      and (im.item_parent = il.item or im.item_grandparent = il.item)
      and il.loc_type = 'S'
   union
   select stg1.loc loc
     from item_export_stg stg,
          item_export_stg stg1
   where stg.base_extracted_ind = 'N'
     and stg.action_type = 'itemhdrdel'
     and stg1.action_type = 'itemlocdel'
     and stg1.base_Extracted_ind = 'N'
     and stg1.loc_type = 'S'
     and stg.item = stg1.item
   union
   select il.loc loc
     from item_export_stg stg,
          item_loc il
    where stg.action_type = 'itemhdrdel'
      and (stg.item_parent = il.item or
           stg.item_grandparent = il.item)
      and il.loc_type = 'S'
    union
   select stg1.loc loc
     from item_export_stg stg,
          item_export_stg stg1
    where stg.base_extracted_ind = 'N'
      and stg.action_type = 'itemhdrdel'
      and (stg.item_parent = stg1.item or
           stg.item_grandparent = stg1.item)
      and stg1.action_type = 'itemlocdel'
      and stg1.base_Extracted_ind = 'N'
      and stg1.loc_type = 'S';
EOF
}
#-------------------------------------------------------------------------
# Function: PROCESS_CORP_FULL
# Purpose: Extarcting full records for corporate office.
#-------------------------------------------------------------------------
function PROCESS_CORP_FULL
{
echo "Extracting records for corporate office" >> $LOGFILE
ITEMHDRCORP=$DIR/itemhdr_${extractDate}_corp_full
   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$ITEMHDRCORP
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   SELECT Family                     ||'|'||
       Type                          ||'|'||
       Location                      ||'|'||
       Item                          ||'|'||
       ItemParent                    ||'|'||
       ItemGrandparent               ||'|'||
       PackInd                       ||'|'||
       SimplePackInd                 ||'|'||
       ItemLevel                     ||'|'||
       TranLevel                     ||'|'||
       InventoryInd                  ||'|'||
       Diff1Level                    ||'|'||
       Diff1Type                     ||'|'||
       Diff1                         ||'|'||
       Diff2Level                    ||'|'||
       Diff2Type                     ||'|'||
       Diff2                         ||'|'||
       Diff3Level                    ||'|'||
       Diff3Type                     ||'|'||
       Diff3                         ||'|'||
       Diff4Level                    ||'|'||
       Diff4Type                     ||'|'||
       Diff4                         ||'|'||
       Dept                          ||'|'||
       Class                         ||'|'||
       UniqueClass                   ||'|'||
       Subclass                      ||'|'||
       UniqueSublass                 ||'|'||
       Status                        ||'|'||
       ItemDesc                      ||'|'||
       SecondaryItemDesc             ||'|'||
       ShortDescription              ||'|'||
       BrandName                     ||'|'||
       MerchandiseInd                ||'|'||
       PrimaryRefItemInd             ||'|'||
       CostZoneGroupId               ||'|'||
       StandardUOM                   ||'|'||
       UOMConvFactor                 ||'|'||
       PackageSize                   ||'|'||
       PackageUOM                    ||'|'||
       StoreOrdMulti                 ||'|'||
       ForecastInd                   ||'|'||
       OriginalRetail                ||'|'||
       OriginalRetailCurrencyCode    ||'|'||
       MfgRecRetail                  ||'|'||
       MfgRecRetailCurrencyCode      ||'|'||
       RetailLabelType               ||'|'||
       RetailLabelValue              ||'|'||
       ItemAggregateInd              ||'|'||
       Diff1AggregateInd             ||'|'||
       Diff2AggregateInd             ||'|'||
       Diff3AggregateInd             ||'|'||
       Diff4AggregateInd             ||'|'||
       ItemNumberType                ||'|'||
       FormatID                      ||'|'||
       Prefix                        ||'|'||
       RecHandlingTemp               ||'|'||
       RecHandlingSens               ||'|'||
       PerishableInd                 ||'|'||
       WasteType                     ||'|'||
       WastePct                      ||'|'||
       DefaultWastePct               ||'|'||
       ConstantDimInd                ||'|'||
       ContainsInnerInd              ||'|'||
       SellableInd                   ||'|'||
       OrderableInd                  ||'|'||
       PackType                      ||'|'||
       OrderAsType                   ||'|'||
       ItemServiceLevel              ||'|'||
       GiftWrapInd                   ||'|'||
       ShipAloneInd                  ||'|'||
       ItemXformInd                  ||'|'||
       CatchWeightInd                ||'|'||
       CatchWeightType               ||'|'||
       CatchWeightOrderType          ||'|'||
       CatchWeightSaleType           ||'|'||
       CatchWeightUOM                ||'|'||
       DepositItemType               ||'|'||
       ContainerItem                 ||'|'||
       DepositInPricePerOUM          ||'|'||
       SOHInquiryAtPackInd           ||'|'||
       NotionalPackInd               ||'|'||
       Comments
FROM (
SELECT 'ITEMS'                                                                                 Family
      ,'FULLHDR'                                                                               Type
      ,'CORPORATE'                                                                             Location
      ,im.item                                                                                 Item
      ,im.item_parent                                                                          ItemParent
      ,im.item_grandparent                                                                     ItemGrandparent
      ,im.pack_ind                                                                             PackInd
      ,im.simple_pack_ind                                                                      SimplePackInd
      ,im.item_level                                                                           ItemLevel
      ,im.tran_level                                                                           TranLevel
      ,im.inventory_ind                                                                        InventoryInd
      ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_group_id,NULL,'ID','GROUP'))            Diff1Level
      ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_type,NULL,di1.diff_type,dh1.diff_type)) Diff1Type
      ,im.diff_1                                                                               Diff1
      ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_group_id,NULL,'ID','GROUP'))            Diff2Level
      ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_type,NULL,di2.diff_type,dh2.diff_type)) Diff2Type
      ,im.diff_2                                                                               Diff2
      ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_group_id,NULL,'ID','GROUP'))            Diff3Level
      ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_type,NULL,di3.diff_type,dh3.diff_type)) Diff3Type
      ,im.diff_3                                                                               Diff3
      ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_group_id,NULL,'ID','GROUP'))            Diff4Level
      ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_type,NULL,di4.diff_type,dh4.diff_type)) Diff4Type
      ,im.diff_4                                                                               Diff4
      ,im.DEPT                                                                                 Dept
      ,im.class                                                                                Class
      ,c.class_id                                                                              UniqueClass
      ,im.subclass                                                                             Subclass
      ,s.subclass_id                                                                           UniqueSublass
      ,im.status                                                                               Status
      ,REPLACE(im.item_desc,CHR(10),'')                                                        ItemDesc
      ,REPLACE(im.item_desc_secondary,CHR(10),'')                                              SecondaryItemDesc
      ,REPLACE(im.short_desc,CHR(10),'')                                                       ShortDescription
      ,im.brand_name                                                                           BrandName
      ,im.merchandise_ind                                                                      MerchandiseInd
      ,im.primary_ref_item_ind                                                                 PrimaryRefItemInd
      ,im.cost_zone_group_id                                                                   CostZoneGroupId
      ,im.standard_uom                                                                         StandardUOM
      ,im.uom_conv_factor                                                                      UOMConvFactor
      ,im.package_size                                                                         PackageSize
      ,im.package_uom                                                                          PackageUOM
      ,im.store_ord_mult                                                                       StoreOrdMulti
      ,im.forecast_ind                                                                         ForecastInd
      ,im.original_retail                                                                      OriginalRetail
      ,so.currency_code                                                                        OriginalRetailCurrencyCode
      ,im.mfg_rec_retail                                                                       MfgRecRetail
      ,so.currency_code                                                                        MfgRecRetailCurrencyCode
      ,im.retail_label_type                                                                    RetailLabelType
      ,im.retail_label_value                                                                   RetailLabelValue
      ,im.item_aggregate_ind                                                                   ItemAggregateInd
      ,im.diff_1_aggregate_ind                                                                 Diff1AggregateInd
      ,im.diff_2_aggregate_ind                                                                 Diff2AggregateInd
      ,im.diff_3_aggregate_ind                                                                 Diff3AggregateInd
      ,im.diff_4_aggregate_ind                                                                 Diff4AggregateInd
      ,im.item_number_type                                                                     ItemNumberType
      ,im.format_id                                                                            FormatID
      ,im.prefix                                                                               Prefix
      ,im.handling_temp                                                                        RecHandlingTemp
      ,im.handling_sensitivity                                                                 RecHandlingSens
      ,im.perishable_ind                                                                       PerishableInd
      ,im.waste_type                                                                           WasteType
      ,im.waste_pct                                                                            WastePct
      ,im.default_waste_pct                                                                    DefaultWastePct
      ,im.const_dimen_ind                                                                      ConstantDimInd
      ,im.contains_inner_ind                                                                   ContainsInnerInd
      ,im.sellable_ind                                                                         SellableInd
      ,im.orderable_ind                                                                        OrderableInd
      ,im.pack_type                                                                            PackType
      ,im.order_as_type                                                                        OrderAsType
      ,im.item_service_level                                                                   ItemServiceLevel
      ,im.gift_wrap_ind                                                                        GiftWrapInd
      ,im.ship_alone_ind                                                                       ShipAloneInd
      ,im.item_xform_ind                                                                       ItemXformInd
      ,im.catch_weight_ind                                                                     CatchWeightInd
      ,im.catch_weight_type                                                                    CatchWeightType
      ,im.order_type                                                                           CatchWeightOrderType
      ,im.sale_type                                                                            CatchWeightSaleType
      ,im.catch_weight_uom                                                                     CatchWeightUOM
      ,im.deposit_item_type                                                                    DepositItemType
      ,im.container_item                                                                       ContainerItem
      ,im.deposit_in_price_per_uom                                                             DepositInPricePerOUM
      ,im.soh_inquiry_at_pack_ind                                                              SOHInquiryAtPackInd
      ,im.notional_pack_ind                                                                    NotionalPackInd
      ,im.comments                                                                             Comments
 FROM  item_master im,
       class c,
       subclass s,
       diff_group_head dh1,
       diff_ids di1,
       diff_group_head dh2,
       diff_ids di2,
       diff_group_head dh3,
       diff_ids di3,
       diff_group_head dh4,
       diff_ids di4,
       system_options so
WHERE im.status = 'A'
  AND im.sellable_ind = 'Y'
  AND im.class = c.class
  AND im.dept  = c.dept
  AND c.dept  = s.dept
  AND c.class = s.class
  AND im.subclass = s.subclass
  AND im.diff_1 = dh1.diff_group_id(+)
  AND im.diff_1 = di1.diff_id(+)
  AND im.diff_2 = dh2.diff_group_id(+)
  AND im.diff_2 = di2.diff_id(+)
  AND im.diff_3 = dh3.diff_group_id(+)
  AND im.diff_3 = di3.diff_id(+)
  AND im.diff_4 = dh4.diff_group_id(+)
  AND im.diff_4 = di4.diff_id(+) order by im.item) item_master;

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('item',
                                'full',
                                 USER,
                                 SYSDATE);
EOF
   if [[ `grep "^ORA-" ${ITEMHDRCORP} | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat ${ITEMHDRCORP} >> $ERRORFILE
      rm ${ITEMHDRCORP}
   else
      COUNTER=`wc -l ${ITEMHDRCORP}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${ITEMHDRCORP}" "${ITEMHDRCORP}"_${COUNTER}.dat
      else
         rm $ITEMHDRCORP
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_FULL
# Purpose: Extracting full records
#-------------------------------------------------------------------------
function PROCESS_FULL
{
store=$1
echo "Extracting full records for store: $store" >> $LOGFILE
ITEMHDR=$DIR/itemhdr_${extractDate}_${store}_full
   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$ITEMHDR
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   VARIABLE GV_store NUMBER;
   VARIABLE GV_process_id NUMBER;

   EXEC :GV_store := ${store};
   SELECT Family                     ||'|'||
       Type                          ||'|'||
       Location                      ||'|'||
       Item                          ||'|'||
       ItemParent                    ||'|'||
       ItemGrandparent               ||'|'||
       PackInd                       ||'|'||
       SimplePackInd                 ||'|'||
       ItemLevel                     ||'|'||
       TranLevel                     ||'|'||
       InventoryInd                  ||'|'||
       Diff1Level                    ||'|'||
       Diff1Type                     ||'|'||
       Diff1                         ||'|'||
       Diff2Level                    ||'|'||
       Diff2Type                     ||'|'||
       Diff2                         ||'|'||
       Diff3Level                    ||'|'||
       Diff3Type                     ||'|'||
       Diff3                         ||'|'||
       Diff4Level                    ||'|'||
       Diff4Type                     ||'|'||
       Diff4                         ||'|'||
       Dept                          ||'|'||
       Class                         ||'|'||
       UniqueClass                   ||'|'||
       Subclass                      ||'|'||
       UniqueSublass                 ||'|'||
       Status                        ||'|'||
       ItemDesc                      ||'|'||
       SecondaryItemDesc             ||'|'||
       ShortDescription              ||'|'||
       BrandName                     ||'|'||
       MerchandiseInd                ||'|'||
       PrimaryRefItemInd             ||'|'||
       CostZoneGroupId               ||'|'||
       StandardUOM                   ||'|'||
       UOMConvFactor                 ||'|'||
       PackageSize                   ||'|'||
       PackageUOM                    ||'|'||
       StoreOrdMulti                 ||'|'||
       ForecastInd                   ||'|'||
       OriginalRetail                ||'|'||
       OriginalRetailCurrencyCode    ||'|'||
       MfgRecRetail                  ||'|'||
       MfgRecRetailCurrencyCode      ||'|'||
       RetailLabelType               ||'|'||
       RetailLabelValue              ||'|'||
       ItemAggregateInd              ||'|'||
       Diff1AggregateInd             ||'|'||
       Diff2AggregateInd             ||'|'||
       Diff3AggregateInd             ||'|'||
       Diff4AggregateInd             ||'|'||
       ItemNumberType                ||'|'||
       FormatID                      ||'|'||
       Prefix                        ||'|'||
       RecHandlingTemp               ||'|'||
       RecHandlingSens               ||'|'||
       PerishableInd                 ||'|'||
       WasteType                     ||'|'||
       WastePct                      ||'|'||
       DefaultWastePct               ||'|'||
       ConstantDimInd                ||'|'||
       ContainsInnerInd              ||'|'||
       SellableInd                   ||'|'||
       OrderableInd                  ||'|'||
       PackType                      ||'|'||
       OrderAsType                   ||'|'||
       ItemServiceLevel              ||'|'||
       GiftWrapInd                   ||'|'||
       ShipAloneInd                  ||'|'||
       ItemXformInd                  ||'|'||
       CatchWeightInd                ||'|'||
       CatchWeightType               ||'|'||
       CatchWeightOrderType          ||'|'||
       CatchWeightSaleType           ||'|'||
       CatchWeightUOM                ||'|'||
       DepositItemType               ||'|'||
       ContainerItem                 ||'|'||
       DepositInPricePerOUM          ||'|'||
       SOHInquiryAtPackInd           ||'|'||
       NotionalPackInd               ||'|'||
       Comments
FROM (
SELECT distinct 'ITEMS'                                                                        Family
      ,'FULLHDR'                                                                               Type
      ,il.loc                                                                                  Location
      ,im.item                                                                                 Item
      ,im.item_parent                                                                          ItemParent
      ,im.item_grandparent                                                                     ItemGrandparent
      ,im.pack_ind                                                                             PackInd
      ,im.simple_pack_ind                                                                      SimplePackInd
      ,im.item_level                                                                           ItemLevel
      ,im.tran_level                                                                           TranLevel
      ,im.inventory_ind                                                                        InventoryInd
      ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_group_id,NULL,'ID','GROUP'))            Diff1Level
      ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_type,NULL,di1.diff_type,dh1.diff_type)) Diff1Type
      ,im.diff_1                                                                               Diff1
      ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_group_id,NULL,'ID','GROUP'))            Diff2Level
      ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_type,NULL,di2.diff_type,dh2.diff_type)) Diff2Type
      ,im.diff_2                                                                               Diff2
      ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_group_id,NULL,'ID','GROUP'))            Diff3Level
      ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_type,NULL,di3.diff_type,dh3.diff_type)) Diff3Type
      ,im.diff_3                                                                               Diff3
      ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_group_id,NULL,'ID','GROUP'))            Diff4Level
      ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_type,NULL,di4.diff_type,dh4.diff_type)) Diff4Type
      ,im.diff_4                                                                               Diff4
      ,im.DEPT                                                                                 Dept
      ,im.class                                                                                Class
      ,c.class_id                                                                              UniqueClass
      ,im.subclass                                                                             Subclass
      ,s.subclass_id                                                                           UniqueSublass
      ,im.status                                                                               Status
      ,REPLACE(im.item_desc,CHR(10),'')                                                        ItemDesc
      ,REPLACE(im.item_desc_secondary,CHR(10),'')                                              SecondaryItemDesc
      ,REPLACE(im.short_desc,CHR(10),'')                                                       ShortDescription
      ,im.brand_name                                                                           BrandName
      ,im.merchandise_ind                                                                      MerchandiseInd
      ,im.primary_ref_item_ind                                                                 PrimaryRefItemInd
      ,im.cost_zone_group_id                                                                   CostZoneGroupId
      ,im.standard_uom                                                                         StandardUOM
      ,im.uom_conv_factor                                                                      UOMConvFactor
      ,im.package_size                                                                         PackageSize
      ,im.package_uom                                                                          PackageUOM
      ,im.store_ord_mult                                                                       StoreOrdMulti
      ,im.forecast_ind                                                                         ForecastInd
      ,im.original_retail                                                                      OriginalRetail
      ,so.currency_code                                                                        OriginalRetailCurrencyCode
      ,im.mfg_rec_retail                                                                       MfgRecRetail
      ,so.currency_code                                                                        MfgRecRetailCurrencyCode
      ,im.retail_label_type                                                                    RetailLabelType
      ,im.retail_label_value                                                                   RetailLabelValue
      ,im.item_aggregate_ind                                                                   ItemAggregateInd
      ,im.diff_1_aggregate_ind                                                                 Diff1AggregateInd
      ,im.diff_2_aggregate_ind                                                                 Diff2AggregateInd
      ,im.diff_3_aggregate_ind                                                                 Diff3AggregateInd
      ,im.diff_4_aggregate_ind                                                                 Diff4AggregateInd
      ,im.item_number_type                                                                     ItemNumberType
      ,im.format_id                                                                            FormatID
      ,im.prefix                                                                               Prefix
      ,im.handling_temp                                                                        RecHandlingTemp
      ,im.handling_sensitivity                                                                 RecHandlingSens
      ,im.perishable_ind                                                                       PerishableInd
      ,im.waste_type                                                                           WasteType
      ,im.waste_pct                                                                            WastePct
      ,im.default_waste_pct                                                                    DefaultWastePct
      ,im.const_dimen_ind                                                                      ConstantDimInd
      ,im.contains_inner_ind                                                                   ContainsInnerInd
      ,im.sellable_ind                                                                         SellableInd
      ,im.orderable_ind                                                                        OrderableInd
      ,im.pack_type                                                                            PackType
      ,im.order_as_type                                                                        OrderAsType
      ,im.item_service_level                                                                   ItemServiceLevel
      ,im.gift_wrap_ind                                                                        GiftWrapInd
      ,im.ship_alone_ind                                                                       ShipAloneInd
      ,im.item_xform_ind                                                                       ItemXformInd
      ,im.catch_weight_ind                                                                     CatchWeightInd
      ,im.catch_weight_type                                                                    CatchWeightType
      ,im.order_type                                                                           CatchWeightOrderType
      ,im.sale_type                                                                            CatchWeightSaleType
      ,im.catch_weight_uom                                                                     CatchWeightUOM
      ,im.deposit_item_type                                                                    DepositItemType
      ,im.container_item                                                                       ContainerItem
      ,im.deposit_in_price_per_uom                                                             DepositInPricePerOUM
      ,im.soh_inquiry_at_pack_ind                                                              SOHInquiryAtPackInd
      ,im.notional_pack_ind                                                                    NotionalPackInd
      ,im.comments                                                                             Comments
 FROM  item_master im,
       item_loc il,
       class c,
       subclass s,
       diff_group_head dh1,
       diff_ids di1,
       diff_group_head dh2,
       diff_ids di2,
       diff_group_head dh3,
       diff_ids di3,
       diff_group_head dh4,
       diff_ids di4,
       system_options so,
      ( select item,
               item_parent,
               item_grandparent
          from item_master 
         where status = 'A' 
           and sellable_ind = 'Y' 
           and item_level > tran_level
       ) upc
WHERE im.status = 'A'
  AND im.sellable_ind = 'Y'
  AND il.item = DECODE(upc.item,null,im.item,im.item_parent)
  AND im.item = upc.item(+)
  AND im.item_parent = upc.item_parent(+)
  AND il.loc = trim(:GV_store)
  AND il.loc_type = 'S'
  AND im.class = c.class
  AND im.dept  = c.dept
  AND c.dept  = s.dept
  AND c.class = s.class
  AND im.subclass = s.subclass
  AND im.diff_1 = dh1.diff_group_id(+)
  AND im.diff_1 = di1.diff_id(+)
  AND im.diff_2 = dh2.diff_group_id(+)
  AND im.diff_2 = di2.diff_id(+)
  AND im.diff_3 = dh3.diff_group_id(+)
  AND im.diff_3 = di3.diff_id(+)
  AND im.diff_4 = dh4.diff_group_id(+)
  AND im.diff_4 = di4.diff_id(+)) item_master;

EOF
   if [[ `grep "^ORA-" ${ITEMHDR} | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat ${ITEMHDR} >> $ERRORFILE
      rm ${ITEMHDR}
   else
      COUNTER=`wc -l ${ITEMHDR}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${ITEMHDR}" "${ITEMHDR}"_${COUNTER}.dat
      else
         rm $ITEMHDR
      fi
      status=${OK}
   fi
return ${status}
}
#--------------------------------------------------------------------------------
# Function: PROCESS_CORP_DELTA
# Purpose: Extracting delta records for corporate office.
#-------------------------------------------------------------------------
function PROCESS_CORP_DELTA
{
process_id=$1
echo "Extracting delta records for corporate office" >> $LOGFILE
ITEMHDRCORPDELTA=$DIR/itemhdr_${extractDate}_corp_delta
   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$ITEMHDRCORPDELTA
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   VARIABLE GV_store NUMBER;
   VARIABLE GV_process_id NUMBER;


   EXEC :GV_process_id := ${process_id};
   SELECT Family                     ||'|'||
       Type                          ||'|'||
       Location                      ||'|'||
       Item                          ||'|'||
       ItemParent                    ||'|'||
       ItemGrandparent               ||'|'||
       PackInd                       ||'|'||
       SimplePackInd                 ||'|'||
       ItemLevel                     ||'|'||
       TranLevel                     ||'|'||
       InventoryInd                  ||'|'||
       Diff1Level                    ||'|'||
       Diff1Type                     ||'|'||
       Diff1                         ||'|'||
       Diff2Level                    ||'|'||
       Diff2Type                     ||'|'||
       Diff2                         ||'|'||
       Diff3Level                    ||'|'||
       Diff3Type                     ||'|'||
       Diff3                         ||'|'||
       Diff4Level                    ||'|'||
       Diff4Type                     ||'|'||
       Diff4                         ||'|'||
       Dept                          ||'|'||
       Class                         ||'|'||
       UniqueClass                   ||'|'||
       Subclass                      ||'|'||
       UniqueSublass                 ||'|'||
       Status                        ||'|'||
       ItemDesc                      ||'|'||
       SecondaryItemDesc             ||'|'||
       ShortDescription              ||'|'||
       BrandName                     ||'|'||
       MerchandiseInd                ||'|'||
       PrimaryRefItemInd             ||'|'||
       CostZoneGroupId               ||'|'||
       StandardUOM                   ||'|'||
       UOMConvFactor                 ||'|'||
       PackageSize                   ||'|'||
       PackageUOM                    ||'|'||
       StoreOrdMulti                 ||'|'||
       ForecastInd                   ||'|'||
       OriginalRetail                ||'|'||
       OriginalRetailCurrencyCode    ||'|'||
       MfgRecRetail                  ||'|'||
       MfgRecRetailCurrencyCode      ||'|'||
       RetailLabelType               ||'|'||
       RetailLabelValue              ||'|'||
       ItemAggregateInd              ||'|'||
       Diff1AggregateInd             ||'|'||
       Diff2AggregateInd             ||'|'||
       Diff3AggregateInd             ||'|'||
       Diff4AggregateInd             ||'|'||
       ItemNumberType                ||'|'||
       FormatID                      ||'|'||
       Prefix                        ||'|'||
       RecHandlingTemp               ||'|'||
       RecHandlingSens               ||'|'||
       PerishableInd                 ||'|'||
       WasteType                     ||'|'||
       WastePct                      ||'|'||
       DefaultWastePct               ||'|'||
       ConstantDimInd                ||'|'||
       ContainsInnerInd              ||'|'||
       SellableInd                   ||'|'||
       OrderableInd                  ||'|'||
       PackType                      ||'|'||
       OrderAsType                   ||'|'||
       ItemServiceLevel              ||'|'||
       GiftWrapInd                   ||'|'||
       ShipAloneInd                  ||'|'||
       ItemXformInd                  ||'|'||
       CatchWeightInd                ||'|'||
       CatchWeightType               ||'|'||
       CatchWeightOrderType          ||'|'||
       CatchWeightSaleType           ||'|'||
       CatchWeightUOM                ||'|'||
       DepositItemType               ||'|'||
       ContainerItem                 ||'|'||
       DepositInPricePerOUM          ||'|'||
       SOHInquiryAtPackInd           ||'|'||
       NotionalPackInd               ||'|'||
       Comments
FROM (
SELECT * FROM (
               SELECT 'ITEMS'                                                                                 Family
                     ,upper(stg.action_type)                                                                  Type
                     ,'CORPORATE'                                                                             Location
                     ,stg.item                                                                                Item
                     ,im.item_parent                                                                          ItemParent
                     ,im.item_grandparent                                                                     ItemGrandparent
                     ,im.pack_ind                                                                             PackInd
                     ,im.simple_pack_ind                                                                      SimplePackInd
                     ,im.item_level                                                                           ItemLevel
                     ,im.tran_level                                                                           TranLevel
                     ,im.inventory_ind                                                                        InventoryInd
                     ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_group_id,NULL,'ID','GROUP'))            Diff1Level
                     ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_type,NULL,di1.diff_type,dh1.diff_type)) Diff1Type
                     ,im.diff_1                                                                               Diff1
                     ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_group_id,NULL,'ID','GROUP'))            Diff2Level
                     ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_type,NULL,di2.diff_type,dh2.diff_type)) Diff2Type
                     ,im.diff_2                                                                               Diff2
                     ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_group_id,NULL,'ID','GROUP'))            Diff3Level
                     ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_type,NULL,di3.diff_type,dh3.diff_type)) Diff3Type
                     ,im.diff_3                                                                               Diff3
                     ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_group_id,NULL,'ID','GROUP'))            Diff4Level
                     ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_type,NULL,di4.diff_type,dh4.diff_type)) Diff4Type
                     ,im.diff_4                                                                               Diff4
                     ,im.DEPT                                                                                 Dept
                     ,im.class                                                                                Class
                     ,c.class_id                                                                              UniqueClass
                     ,im.subclass                                                                             Subclass
                     ,s.subclass_id                                                                           UniqueSublass
                     ,im.status                                                                               Status
                     ,REPLACE(im.item_desc,CHR(10),'')                                                        ItemDesc
                     ,REPLACE(im.item_desc_secondary,CHR(10),'')                                              SecondaryItemDesc
                     ,REPLACE(im.short_desc,CHR(10),'')                                                       ShortDescription
                     ,im.brand_name                                                                           BrandName
                     ,im.merchandise_ind                                                                      MerchandiseInd
                     ,im.primary_ref_item_ind                                                                 PrimaryRefItemInd
                     ,im.cost_zone_group_id                                                                   CostZoneGroupId
                     ,im.standard_uom                                                                         StandardUOM
                     ,im.uom_conv_factor                                                                      UOMConvFactor
                     ,im.package_size                                                                         PackageSize
                     ,im.package_uom                                                                          PackageUOM
                     ,im.store_ord_mult                                                                       StoreOrdMulti
                     ,im.forecast_ind                                                                         ForecastInd
                     ,im.original_retail                                                                      OriginalRetail
                     ,so.currency_code                                                                        OriginalRetailCurrencyCode
                     ,im.mfg_rec_retail                                                                       MfgRecRetail
                     ,so.currency_code                                                                        MfgRecRetailCurrencyCode
                     ,im.retail_label_type                                                                    RetailLabelType
                     ,im.retail_label_value                                                                   RetailLabelValue
                     ,im.item_aggregate_ind                                                                   ItemAggregateInd
                     ,im.diff_1_aggregate_ind                                                                 Diff1AggregateInd
                     ,im.diff_2_aggregate_ind                                                                 Diff2AggregateInd
                     ,im.diff_3_aggregate_ind                                                                 Diff3AggregateInd
                     ,im.diff_4_aggregate_ind                                                                 Diff4AggregateInd
                     ,im.item_number_type                                                                     ItemNumberType
                     ,im.format_id                                                                            FormatID
                     ,im.prefix                                                                               Prefix
                     ,im.handling_temp                                                                        RecHandlingTemp
                     ,im.handling_sensitivity                                                                 RecHandlingSens
                     ,im.perishable_ind                                                                       PerishableInd
                     ,im.waste_type                                                                           WasteType
                     ,im.waste_pct                                                                            WastePct
                     ,im.default_waste_pct                                                                    DefaultWastePct
                     ,im.const_dimen_ind                                                                      ConstantDimInd
                     ,im.contains_inner_ind                                                                   ContainsInnerInd
                     ,im.sellable_ind                                                                         SellableInd
                     ,im.orderable_ind                                                                        OrderableInd
                     ,im.pack_type                                                                            PackType
                     ,im.order_as_type                                                                        OrderAsType
                     ,im.item_service_level                                                                   ItemServiceLevel
                     ,im.gift_wrap_ind                                                                        GiftWrapInd
                     ,im.ship_alone_ind                                                                       ShipAloneInd
                     ,im.item_xform_ind                                                                       ItemXformInd
                     ,im.catch_weight_ind                                                                     CatchWeightInd
                     ,im.catch_weight_type                                                                    CatchWeightType
                     ,im.order_type                                                                           CatchWeightOrderType
                     ,im.sale_type                                                                            CatchWeightSaleType
                     ,im.catch_weight_uom                                                                     CatchWeightUOM
                     ,im.deposit_item_type                                                                    DepositItemType
                     ,im.container_item                                                                       ContainerItem
                     ,im.deposit_in_price_per_uom                                                             DepositInPricePerOUM
                     ,im.soh_inquiry_at_pack_ind                                                              SOHInquiryAtPackInd
                     ,im.notional_pack_ind                                                                    NotionalPackInd
                     ,im.comments                                                                             Comments
                     ,stg.seq_no
                FROM  item_export_stg stg,
                      item_master im,
                      class c,
                      subclass s,
                      diff_group_head dh1,
                      diff_ids di1,
                      diff_group_head dh2,
                      diff_ids di2,
                      diff_group_head dh3,
                      diff_ids di3,
                      diff_group_head dh4,
                      diff_ids di4,
                      system_options so
               WHERE stg.item = im.item
                 AND stg.base_extracted_ind = 'N'
                 AND stg.action_type in ('itemhdrmod')
                 AND stg.process_id = trim(:GV_process_id)
                 AND im.class = c.class
                 AND im.dept  = c.dept
                 AND c.dept  = s.dept
                 AND c.class = s.class
                 AND im.subclass = s.subclass
                 AND im.diff_1 = dh1.diff_group_id(+)
                 AND im.diff_1 = di1.diff_id(+)
                 AND im.diff_2 = dh2.diff_group_id(+)
                 AND im.diff_2 = di2.diff_id(+)
                 AND im.diff_3 = dh3.diff_group_id(+)
                 AND im.diff_3 = di3.diff_id(+)
                 AND im.diff_4 = dh4.diff_group_id(+)
                 AND im.diff_4 = di4.diff_id(+)
               UNION ALL
               SELECT 'ITEMS'                Family
                     ,upper(stg.action_type) Type
                     ,'CORPORATE'            Location
                     ,stg.item               Item
                     ,NULL                   ItemParent
                     ,NULL                   ItemGrandparent
                     ,NULL                   PackInd
                     ,NULL                   SimplePackInd
                     ,stg.item_level         ItemLevel
                     ,stg.tran_level         TranLevel
                     ,NULL                   InventoryInd
                     ,NULL                   Diff1Level
                     ,NULL                   Diff1Type
                     ,NULL                   Diff1
                     ,NULL                   Diff2Level
                     ,NULL                   Diff2Type
                     ,NULL                   Diff2
                     ,NULL                   Diff3Level
                     ,NULL                   Diff3Type
                     ,NULL                   Diff3
                     ,NULL                   Diff4Level
                     ,NULL                   Diff4Type
                     ,NULL                   Diff4
                     ,NULL                   Dept
                     ,NULL                   Class
                     ,NULL                   UniqueClass
                     ,NULL                   Subclass
                     ,NULL                   UniqueSublass
                     ,NULL                   Status
                     ,NULL                   ItemDesc
                     ,NULL                   SecondaryItemDesc
                     ,NULL                   ShortDescription
                     ,NULL                   BrandName
                     ,NULL                   MerchandiseInd
                     ,NULL                   PrimaryRefItemInd
                     ,NULL                   CostZoneGroupId
                     ,NULL                   StandardUOM
                     ,NULL                   UOMConvFactor
                     ,NULL                   PackageSize
                     ,NULL                   PackageUOM
                     ,NULL                   StoreOrdMulti
                     ,NULL                   ForecastInd
                     ,NULL                   OriginalRetail
                     ,NULL                   OriginalRetailCurrencyCode
                     ,NULL                   MfgRecRetail
                     ,NULL                   MfgRecRetailCurrencyCode
                     ,NULL                   RetailLabelType
                     ,NULL                   RetailLabelValue
                     ,NULL                   ItemAggregateInd
                     ,NULL                   Diff1AggregateInd
                     ,NULL                   Diff2AggregateInd
                     ,NULL                   Diff3AggregateInd
                     ,NULL                   Diff4AggregateInd
                     ,NULL                   ItemNumberType
                     ,NULL                   FormatID
                     ,NULL                   Prefix
                     ,NULL                   RecHandlingTemp
                     ,NULL                   RecHandlingSens
                     ,NULL                   PerishableInd
                     ,NULL                   WasteType
                     ,NULL                   WastePct
                     ,NULL                   DefaultWastePct
                     ,NULL                   ConstantDimInd
                     ,NULL                   ContainsInnerInd
                     ,NULL                   SellableInd
                     ,NULL                   OrderableInd
                     ,NULL                   PackType
                     ,NULL                   OrderAsType
                     ,NULL                   ItemServiceLevel
                     ,NULL                   GiftWrapInd
                     ,NULL                   ShipAloneInd
                     ,NULL                   ItemXformInd
                     ,NULL                   CatchWeightInd
                     ,NULL                   CatchWeightType
                     ,NULL                   CatchWeightOrderType
                     ,NULL                   CatchWeightSaleType
                     ,NULL                   CatchWeightUOM
                     ,NULL                   DepositItemType
                     ,NULL                   ContainerItem
                     ,NULL                   DepositInPricePerOUM
                     ,NULL                   SOHInquiryAtPackInd
                     ,NULL                   NotionalPackInd
                     ,NULL                   Comments
                     ,stg.seq_no
                FROM  item_export_stg stg
               WHERE stg.base_extracted_ind = 'N'
                 AND stg.action_type in ('itemhdrdel')
                 AND stg.process_id = trim(:GV_process_id)
                UNION ALL
               SELECT 'ITEMS'                                                                                 Family
                     ,'ITEMHDRCRE'                                                                            Type
                     ,'CORPORATE'                                                                             Location
                     ,inf.item                                                                                Item
                     ,im.item_parent                                                                          ItemParent
                     ,im.item_grandparent                                                                     ItemGrandparent
                     ,im.pack_ind                                                                             PackInd
                     ,im.simple_pack_ind                                                                      SimplePackInd
                     ,im.item_level                                                                           ItemLevel
                     ,im.tran_level                                                                           TranLevel
                     ,im.inventory_ind                                                                        InventoryInd
                     ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_group_id,NULL,'ID','GROUP'))            Diff1Level
                     ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_type,NULL,di1.diff_type,dh1.diff_type)) Diff1Type
                     ,im.diff_1                                                                               Diff1
                     ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_group_id,NULL,'ID','GROUP'))            Diff2Level
                     ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_type,NULL,di2.diff_type,dh2.diff_type)) Diff2Type
                     ,im.diff_2                                                                               Diff2
                     ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_group_id,NULL,'ID','GROUP'))            Diff3Level
                     ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_type,NULL,di3.diff_type,dh3.diff_type)) Diff3Type
                     ,im.diff_3                                                                               Diff3
                     ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_group_id,NULL,'ID','GROUP'))            Diff4Level
                     ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_type,NULL,di4.diff_type,dh4.diff_type)) Diff4Type
                     ,im.diff_4                                                                               Diff4
                     ,im.DEPT                                                                                 Dept
                     ,im.class                                                                                Class
                     ,c.class_id                                                                              UniqueClass
                     ,im.subclass                                                                             Subclass
                     ,s.subclass_id                                                                           UniqueSublass
                     ,im.status                                                                               Status
                     ,REPLACE(im.item_desc,CHR(10),'')                                                        ItemDesc
                     ,REPLACE(im.item_desc_secondary,CHR(10),'')                                              SecondaryItemDesc
                     ,REPLACE(im.short_desc,CHR(10),'')                                                       ShortDescription
                     ,im.brand_name                                                                           BrandName
                     ,im.merchandise_ind                                                                      MerchandiseInd
                     ,im.primary_ref_item_ind                                                                 PrimaryRefItemInd
                     ,im.cost_zone_group_id                                                                   CostZoneGroupId
                     ,im.standard_uom                                                                         StandardUOM
                     ,im.uom_conv_factor                                                                      UOMConvFactor
                     ,im.package_size                                                                         PackageSize
                     ,im.package_uom                                                                          PackageUOM
                     ,im.store_ord_mult                                                                       StoreOrdMulti
                     ,im.forecast_ind                                                                         ForecastInd
                     ,im.original_retail                                                                      OriginalRetail
                     ,so.currency_code                                                                        OriginalRetailCurrencyCode
                     ,im.mfg_rec_retail                                                                       MfgRecRetail
                     ,so.currency_code                                                                        MfgRecRetailCurrencyCode
                     ,im.retail_label_type                                                                    RetailLabelType
                     ,im.retail_label_value                                                                   RetailLabelValue
                     ,im.item_aggregate_ind                                                                   ItemAggregateInd
                     ,im.diff_1_aggregate_ind                                                                 Diff1AggregateInd
                     ,im.diff_2_aggregate_ind                                                                 Diff2AggregateInd
                     ,im.diff_3_aggregate_ind                                                                 Diff3AggregateInd
                     ,im.diff_4_aggregate_ind                                                                 Diff4AggregateInd
                     ,im.item_number_type                                                                     ItemNumberType
                     ,im.format_id                                                                            FormatID
                     ,im.prefix                                                                               Prefix
                     ,im.handling_temp                                                                        RecHandlingTemp
                     ,im.handling_sensitivity                                                                 RecHandlingSens
                     ,im.perishable_ind                                                                       PerishableInd
                     ,im.waste_type                                                                           WasteType
                     ,im.waste_pct                                                                            WastePct
                     ,im.default_waste_pct                                                                    DefaultWastePct
                     ,im.const_dimen_ind                                                                      ConstantDimInd
                     ,im.contains_inner_ind                                                                   ContainsInnerInd
                     ,im.sellable_ind                                                                         SellableInd
                     ,im.orderable_ind                                                                        OrderableInd
                     ,im.pack_type                                                                            PackType
                     ,im.order_as_type                                                                        OrderAsType
                     ,im.item_service_level                                                                   ItemServiceLevel
                     ,im.gift_wrap_ind                                                                        GiftWrapInd
                     ,im.ship_alone_ind                                                                       ShipAloneInd
                     ,im.item_xform_ind                                                                       ItemXformInd
                     ,im.catch_weight_ind                                                                     CatchWeightInd
                     ,im.catch_weight_type                                                                    CatchWeightType
                     ,im.order_type                                                                           CatchWeightOrderType
                     ,im.sale_type                                                                            CatchWeightSaleType
                     ,im.catch_weight_uom                                                                     CatchWeightUOM
                     ,im.deposit_item_type                                                                    DepositItemType
                     ,im.container_item                                                                       ContainerItem
                     ,im.deposit_in_price_per_uom                                                             DepositInPricePerOUM
                     ,im.soh_inquiry_at_pack_ind                                                              SOHInquiryAtPackInd
                     ,im.notional_pack_ind                                                                    NotionalPackInd
                     ,im.comments                                                                             Comments
                     ,NULL                                                                                    seq_no
                FROM  item_export_info inf,
                      item_master im,
                      class c,
                      subclass s,
                      diff_group_head dh1,
                      diff_ids di1,
                      diff_group_head dh2,
                      diff_ids di2,
                      diff_group_head dh3,
                      diff_ids di3,
                      diff_group_head dh4,
                      diff_ids di4,
                      system_options so
               WHERE inf.item = im.item
                 AND inf.base_extracted_ind = 'N'
                 AND inf.process_id = trim(:GV_process_id)
                 AND im.class = c.class
                 AND im.dept  = c.dept
                 AND c.dept  = s.dept
                 AND c.class = s.class
                 AND im.subclass = s.subclass
                 AND im.diff_1 = dh1.diff_group_id(+)
                 AND im.diff_1 = di1.diff_id(+)
                 AND im.diff_2 = dh2.diff_group_id(+)
                 AND im.diff_2 = di2.diff_id(+)
                 AND im.diff_3 = dh3.diff_group_id(+)
                 AND im.diff_3 = di3.diff_id(+)
                 AND im.diff_4 = dh4.diff_group_id(+)
                 AND im.diff_4 = di4.diff_id(+) ) order by seq_no) itemhdrcorpdelta;

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('item',
                                'delta',
                                 USER,
                                 SYSDATE);
EOF
   if [[ `grep "^ORA-" ${ITEMHDRCORPDELTA} | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat ${ITEMHDRCORPDELTA} >> $ERRORFILE
      rm ${ITEMHDRCORPDELTA}
   else
      COUNTER=`wc -l ${ITEMHDRCORPDELTA}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${ITEMHDRCORPDELTA}" "${ITEMHDRCORPDELTA}"_${COUNTER}.dat
      else
         rm $ITEMHDRCORPDELTA
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
#              MAIN
#-------------------------------------------------------------------------
if [[ $# -lt 1 || $# -gt 5 || $# -lt 3 ]]; then
    USAGE
    exit 1
fi

CONNECT=$1
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${CONNECT}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${con_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "export_itemmaster.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#Getting process_id
current_process_id=`${ORACLE_HOME}/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select export_process_id_seq.nextval
  from dual;
exit;
EOF`
process_id_status=$?

if [[ ${process_id_status} -ne ${OK} ]]; then
         LOG_ERROR "invalid username/password; logon denied" "GETTING_PROCESS_ID" "${process_id_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
      exit 1
fi
### Check if no. of threads is passed as arg or not, else default.
if [[ $3 = "Y" ]]; then
   SLOTS=$4
   if [[ -z ${SLOTS} ]]; then
     SLOTS=10
   fi
   STR=$5
   echo $SLOTS | egrep '^[0-9]+$'>/dev/null 2>&1
   status=$?
   if [ "$status" -ne "0" ]
   then
     LOG_ERROR "Thread count should be a Positive Number" "NUM_THREADS" "${status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
     exit 1
   fi
   if [[ ${SLOTS} -gt 20 ]]; then
    SLOTS=10
   fi
else
   if [[ $3 = "N" ]]; then
   STR=$4
   SLOTS=10
   else
     LOG_ERROR "Thread number indicator either should be Y or N" "INDICATOR" "$3" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
     exit 1
   fi
fi

if [[ $2 = "full" ]]; then
   if [[ -z ${STR} ]]; then
       UPDATE_ITEM_EXPORT_STG ${current_process_id}
       upd_item_st=$?
       if [[ ${upd_item_st} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating item_export_stg and item_export_info" "UPDATE_ITEM_EXPORT_STG" "${upd_item_st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
       fi
      CREATE_STORE_LIST
      crt_sts=$?
      if [[ ${crt_sts} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating item_export_stg and item_export_info" "CREATE_STORE_LIST" "${crt_sts}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      else
         while read store
         do
           if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
              (
                PROCESS_FULL $store
               ) &
               process_full_status=$?
                 if [[ ${process_full_status} -ne ${OK} ]]; then
                     LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL" "${process_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                     exit 1
                 fi
           # Loop until a thread becomes available
           else 
              while [ `jobs | wc -l` -ge ${SLOTS} ]
              do
                sleep 1
              done
              (
                PROCESS_FULL $store
               ) &
               process_full_status=$?
                 if [[ ${process_full_status} -ne ${OK} ]]; then
                     LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL" "${process_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                     exit 1
                 fi
           fi
         done < $STORE_LIST
        ### Wait for all of the threads to complete
        wait
        ### Extracting records for corporate office
         PROCESS_CORP_FULL
         if [[ $? -eq ${OK} ]]; then
            PROCESS_STG ${current_process_id}
            process_stg_status=$?
            if [[ ${process_stg_status} -ne ${OK} ]]; then
               LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" "${process_stg_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
               exit 1
            else
               rm -r $STORE_LIST
               LOG_MESSAGE "export_itemmaster.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
            fi
         fi
      fi
   else
     str=${STR}
     echo "Updating the item_export_stg with process id:" ${current_process_id} >> $LOGFILE
     UPDATE_SINGLE_STORE_ITEM_EXPORT_STG ${str}
     upd_sgl_str=$?
     if [[ ${upd_sgl_str} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "UPDATE_SINGLE_STORE_ITEM_EXPORT_STG" "${upd_sgl_str}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
     else
         PROCESS_CORP_FULL
         prc_corp_st=$?
         if [[ ${prc_corp_st} -ne ${OK} ]]; then
            LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_CORP_FULL" "${prc_corp_st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
            exit 1
         fi
         PROCESS_FULL ${str}
             process_full_status=$?
               if [[ ${process_full_status} -ne ${OK} ]]; then
                   LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL" "${process_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                   exit 1
               else
                 PROCESS_SINGLE_STORE_STG
                 prc_sin_stg=$?
                  if [[ ${prc_sin_stg} -ne ${OK} ]]; then
                     LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_SINGLE_STORE_STG" "${prc_sin_stg}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                     exit 1
                  fi
                  LOG_MESSAGE "export_itemmaster.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
               fi
     fi
   fi
fi
if [[ $2 = "delta" ]]; then

       UPDATE_ITEM_EXPORT_STG ${current_process_id}
       upd_item_st=$?
       if [[ ${upd_item_st} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating item_export_stg and item_export_info" "UPDATE_ITEM_EXPORT_STG" "${upd_item_st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
       fi
       
        PROCESS_CORP_DELTA ${current_process_id}
        if [[ $? -eq ${OK} ]]; then
            PROCESS_STG ${current_process_id}
            process_stg_status=$?
            if [[ ${process_stg_status} -ne ${OK} ]]; then
               LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" "${process_stg_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
               exit 1
        fi
    fi
fi
