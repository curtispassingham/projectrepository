LOAD DATA
APPEND
INTO TABLE SVC_FCUSTUPLD_FHEAD
WHEN FILE_RECORD_DESCRIPTOR = 'FHEAD'
FIELDS TERMINATED BY '|' 
OPTIONALLY ENCLOSED BY '"' TRAILING NULLCOLS 
  (
   PROCESS_ID                "FCUSTUPLD_PROCESS_ID_SEQ.NEXTVAL",
   FCUST_SEQ_NO               position(1),
   FILE_RECORD_DESCRIPTOR               ,
   FILE_LINE_ID              "nvl (:FILE_LINE_ID,1)",
   FILE_TYPE                 "nvl (:FILE_TYPE,'FCUST')"  ,
   FILE_CREATE_DATE          "nvl (:FILE_CREATE_DATE,sysdate)",
   LAST_UPDATE_DATETIME       SYSDATE ,
   STATUS                     constant "N",
   ERROR_MSG                  constant ""
  )
INTO TABLE SVC_FCUSTUPLD_THEAD
WHEN FILE_RECORD_DESCRIPTOR = 'THEAD'
FIELDS TERMINATED BY '|' 
OPTIONALLY ENCLOSED BY '"' TRAILING NULLCOLS 
  (
   PROCESS_ID                "FCUSTUPLD_PROCESS_ID_SEQ.CURRVAL",
   CHUNK_ID                   constant 1,
   FCUST_SEQ_NO               position(1),
   FILE_RECORD_DESCRIPTOR               ,
   FILE_LINE_ID              "nvl (:FILE_LINE_ID,1)",
   MESSAGE_TYPE              "lower(:MESSAGE_TYPE)",  
   F_CUSTOMER_GROUP_ID                  ,   
   F_CUSTOMER_GROUP_NAME                ,
   STATUS                     constant "N" ,
   ERROR_MSG                  constant ""     
 )
INTO TABLE SVC_FCUSTUPLD_TDETL
WHEN FILE_RECORD_DESCRIPTOR = 'TDETL'
FIELDS TERMINATED BY '|' 
OPTIONALLY ENCLOSED BY '"' TRAILING NULLCOLS 
  (
   PROCESS_ID                "FCUSTUPLD_PROCESS_ID_SEQ.CURRVAL", 
   FCUST_SEQ_NO               position(1),
   FILE_RECORD_DESCRIPTOR              ,
   FILE_LINE_ID              "nvl (:FILE_LINE_ID,1)",
   MESSAGE_TYPE              "lower(:MESSAGE_TYPE)"  ,  
   F_CUSTOMER_ID                       ,
   F_CUSTOMER_NAME                     ,
   CREDIT_IND                "upper(nvl (:CREDIT_IND,'N'))",
   AUTO_APPROVE_IND          "upper(nvl (:AUTO_APPROVE_IND,'N'))",
   STATUS                     constant "N" ,
   ERROR_MSG                  constant "" 
 )
INTO TABLE SVC_FCUSTUPLD_TTAIL
WHEN FILE_RECORD_DESCRIPTOR = 'TTAIL'
FIELDS TERMINATED BY '|' 
OPTIONALLY ENCLOSED BY '"' TRAILING NULLCOLS 
  (
   PROCESS_ID                  "FCUSTUPLD_PROCESS_ID_SEQ.CURRVAL",   
   FCUST_SEQ_NO                 position(1),
   FILE_RECORD_DESCRIPTOR                  ,
   FILE_LINE_ID                "nvl (:FILE_LINE_ID,1)",
   TRAN_RECORD_COUNTER                     ,   
   STATUS                       constant "N" ,
   ERROR_MSG                    constant ""  
 )
INTO TABLE SVC_FCUSTUPLD_FTAIL
WHEN FILE_RECORD_DESCRIPTOR = 'FTAIL'
FIELDS TERMINATED BY '|' 
OPTIONALLY ENCLOSED BY '"' TRAILING NULLCOLS 
  (
   PROCESS_ID                  "FCUSTUPLD_PROCESS_ID_SEQ.CURRVAL", 
   FCUST_SEQ_NO                 position(1),
   FILE_RECORD_DESCRIPTOR                  ,
   FILE_LINE_ID                "nvl (:FILE_LINE_ID,1)",
   FILE_RECORD_COUNTER                     ,   
   STATUS                       constant "N" ,
   ERROR_MSG                    constant ""  
 )
 
  
  
