#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  export_merchhier.ksh
#
#  Desc:  UNIX shell script to extract the merchandise hierarchy.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='export_merchhier.ksh'
pgmName=${pgmName##*/}               # remove the path
pgmExt=${pgmName##*.}                # get the extension
pgmName=${pgmName%.*}                # get the program name
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
DIR=`pwd`

OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <mode>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.
   
   <mode> mode of extraction. full or delta"
}
#-------------------------------------------------------------------------
# Function Name: UPDATE_MERCHHIER_EXPORT_STG
# Purpose      : update merchhier_export_stg with process_id for export
#-------------------------------------------------------------------------
function UPDATE_MERCHHIER_EXPORT_STG
{
prcs_id=$1
echo "Updating merchhier_export_stg with process id: $prcs_id" >> $LOGFILE
    sqlReturn=`echo "set feedback off
      set heading off
      set term off
      set verify off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);
      VARIABLE GV_exportprocess_id NUMBER;

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      EXEC :GV_exportprocess_id := $prcs_id;

      WHENEVER SQLERROR EXIT 1
      update merchhier_export_stg
         set process_id = trim(:GV_exportprocess_id)
       where base_extracted_ind = 'N'
         and process_id is null;

      COMMIT;
      /

      print :GV_script_error;
      exit  :GV_return_code;
     "  | sqlplus -s ${CONNECT}`
   
   if [[ $? -ne ${OK} ]]; then
      status=${FATAL}
   else
      status=${OK}
   fi
   return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_STG
# Purpose      : Update the exported records.
#-------------------------------------------------------------------------
function PROCESS_STG
{
process_id=$1
   echo "Updating exported records" >> $LOGFILE
   sqlReturn=`echo "set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0
   
   VARIABLE GV_return_code    NUMBER;
   VARIABLE GV_script_error   CHAR(255);
   VARIABLE GV_exportprocess_id NUMBER;
   
   EXEC :GV_return_code  := 0;
   EXEC :GV_script_error := NULL;
   EXEC :GV_exportprocess_id := $process_id;
   
   update merchhier_export_stg
      set base_extracted_ind = 'Y'
    where base_extracted_ind = 'N'
      and process_id = trim(:GV_exportprocess_id);

   commit;
   /
   print :GV_script_error
   exit  :GV_return_code
   " | sqlplus -s $CONNECT`


   if [[ $? -ne ${OK} ]]; then
      status=${FATAL}
   else
      status=${OK}
   fi
return ${status}

}
#-------------------------------------------------------------------------
# Function Name: PROCESS_FULL
# Purpose      : Export full records
#-------------------------------------------------------------------------
function PROCESS_FULL
{
 echo "Extracting full merchandising hierarchy records" >> $LOGFILE
 MERCH_HIERAR=$DIR/merchhierarchy_${extractDate}_full
   
   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$MERCH_HIERAR
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0
 SELECT  family                    ||'|'||
         action                    ||'|'||
         heirarchy_level           ||'|'||
         hierarchyNodeId           ||'|'||
         heirarchy_name            ||'|'||
         parent_level              ||'|'||
         parent_id                 ||'|'||
         GrandparentMerchDisplayId ||'|'||
         parentMerchDisplayId      ||'|'||
         merchDisplayId
   FROM
        (SELECT 'MERCHHIERARCHY' family,
                'FULL'     action,
                'DIVISION' heirarchy_level,
                d.division hierarchyNodeId,
                d.div_name heirarchy_name,
                'COMPANY'  parent_level,
                co.company parent_id,
                NULL       GrandparentMerchDisplayId,
                NULL       parentMerchDisplayId,
                d.division merchDisplayId
        FROM division d,
             comphead co
        UNION ALL
        SELECT 'MERCHHIERARCHY'   family,
               'FULL'        action,
               'GROUP'       heirarchy_level,
               g.group_no    hierarchyNodeId,
               g.group_name  heirarchy_name,
               'DIVISION'    parent_level,
               g.division    parent_id,
               NULL          GrandparentMerchDisplayId,
               NULL          parentMerchDisplayId,
               g.group_no    merchDisplayId
        FROM groups g
        UNION ALL
        SELECT 'MERCHHIERARCHY'  family,
               'FULL'       action,
               'DEPARTMENT' heirarchy_level,
               dp.dept      hierarchyNodeId,
               dp.dept_name heirarchy_name,
               'GROUP'      parent_level,
               dp.group_no  parent_id,
               NULL         GrandparentMerchDisplayId,
               NULL         parentMerchDisplayId,
               dp.dept      merchDisplayId
        FROM deps dp
        UNION ALL
        SELECT 'MERCHHIERARCHY'   family,
               'FULL'        action,
               'CLASS'       heirarchy_level,
               c.class_id    hierarchyNodeId,
               c.class_name  heirarchy_name,
               'DEPARTMENT'  parent_level,
               c.dept        parent_id,
               NULL          GrandparentMerchDisplayId,
               c.dept        parentMerchDisplayId,
               c.class       merchDisplayId
        FROM class c
        UNION ALL
        SELECT 'MERCHHIERARCHY'   family,
               'FULL'        action,
               'SUBCLASS'    heirarchy_level,
               s.subclass_id hierarchyNodeId,
               s.sub_name    heirarchy_name,
               'CLASS'       parent_level,
               c1.class_id   parent_id,
               s.dept        GrandparentMerchDisplayId,
               s.class       parentMerchDisplayId,
               s.subclass    merchDisplayId
        FROM subclass s,
             class c1
       WHERE c1.class = s.class
         AND c1.dept  = s.dept
        ) merchhier;

       insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('merchhierarchy',
                                'full',
                                 USER,
                                 SYSDATE);

EOF
err=$?
   if [[ `grep "^ORA-" $MERCH_HIERAR | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $MERCH_HIERAR >> $ERRORFILE
      rm $MERCH_HIERAR
   else
      COUNTER=`wc -l ${MERCH_HIERAR}| awk '{print $1}'`
      mv "${MERCH_HIERAR}" "${MERCH_HIERAR}"_${COUNTER}.dat
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_DELTA
# Purpose      : Export the records,which are not exported last time
#-------------------------------------------------------------------------
function PROCESS_DELTA
{
Process_id=$1
echo "Extracting delta merchandising hierarchy records." >> $LOGFILE
MERCH_HIERAR_DELTA=$DIR/merchhierarchy_${extractDate}_delta
   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF > $MERCH_HIERAR_DELTA
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000
   set pages 0
   
   VARIABLE GV_exportprocess_id NUMBER;
   EXEC :GV_exportprocess_id := $Process_id;   
   WHENEVER SQLERROR EXIT 1
   SELECT  family                    ||'|'||
           action                    ||'|'||
           heirarchy_level           ||'|'||
           hierarchyNodeId           ||'|'||
           heirarchy_name            ||'|'||
           parent_level              ||'|'||
           parent_id                 ||'|'||
           GrandparentMerchDisplayId ||'|'||
           parentMerchDisplayId      ||'|'||
           merchDisplayId
   FROM
        (SELECT 'MERCHHIERARCHY'                                             family,
                upper(stg.action_type)                                       action,
                'DIVISION'                                                   heirarchy_level,
                stg.division                                                 hierarchyNodeId,
                d.div_name                                                   heirarchy_name,
                UPPER(stg.parent_level)                                      parent_level,
                TO_NUMBER(DECODE(stg.action_type,'divdel',NULL,co.company))  parent_id,
                NULL                                                         GrandparentMerchDisplayId,
                NULL                                                         parentMerchDisplayId,
                d.division                                                   merchDisplayId,
                stg.seq_no
        FROM division d,
             comphead co,
             merchhier_export_stg stg
       WHERE stg.division = d.division(+)
         AND stg.base_extracted_ind = 'N'
         AND stg.division is not null
         AND stg.process_id = trim(:GV_exportprocess_id)
        UNION ALL
        SELECT 'MERCHHIERARCHY'         family,
               upper(stg.action_type)   action,
               'GROUP'                  heirarchy_level,
               stg.group_no             hierarchyNodeId,
               g.group_name             heirarchy_name,
               upper(stg.parent_level)  parent_level,
               g.division               parent_id,
               NULL                     GrandparentMerchDisplayId,
               NULL                     parentMerchDisplayId,
               stg.group_no             merchDisplayId,
               stg.seq_no
          FROM groups g,
               merchhier_export_stg stg
         WHERE stg.group_no = g.group_no(+)
           AND stg.base_extracted_ind = 'N'
           AND stg.group_no is not null
           AND stg.process_id = trim(:GV_exportprocess_id)
        UNION ALL
        SELECT 'MERCHHIERARCHY'         family,
               upper(stg.action_type)   action,
               'DEPARTMENT'             heirarchy_level,
               stg.dept                 hierarchyNodeId,
               dp.dept_name             heirarchy_name,
               upper(stg.parent_level)  parent_level,
               dp.group_no              parent_id,
               NULL                     GrandparentMerchDisplayId,
               NULL                     parentMerchDisplayId,
               stg.dept                 merchDisplayId,
               stg.seq_no
          FROM deps dp,
               merchhier_export_stg stg
         WHERE stg.dept = dp.dept(+)
           AND stg.dept is not null
           AND stg.base_extracted_ind = 'N'
           AND stg.process_id = trim(:GV_exportprocess_id)
        UNION ALL
        SELECT 'MERCHHIERARCHY'          family,
               upper(stg.action_type)    action,
               'CLASS'                   heirarchy_level,
               stg.class_id              hierarchyNodeId,
               c.class_name              heirarchy_name,
               upper(stg.parent_level)   parent_level,
               c.dept                    parent_id,
               NULL                      GrandparentMerchDisplayId,
               c.dept                    parentMerchDisplayId,
               c.class                   merchDisplayId,
               stg.seq_no
          FROM class c,
               merchhier_export_stg stg
         WHERE stg.class_id = c.class_id(+)
           AND stg.class_id is not null
           AND stg.base_extracted_ind = 'N'
           AND stg.process_id = trim(:GV_exportprocess_id)
        UNION ALL
       SELECT  'MERCHHIERARCHY'          family,
               upper(stg.action_type)    action,
               'SUBCLASS'                heirarchy_level,
               stg.subclass_id           hierarchyNodeId,
               s.sub_name                heirarchy_name,
               upper(stg.parent_level)   parent_level,
               (select class_id from class c where c.class = s.class and c.dept = s.dept) parent_id,
               s.dept                    GrandparentMerchDisplayId,
               s.class                   parentMerchDisplayId,
               s.subclass                merchDisplayId,
               stg.seq_no
          FROM subclass s,
               merchhier_export_stg stg
         WHERE stg.subclass_id = s.subclass_id(+)
           AND stg.subclass_id is not null
           AND stg.base_extracted_ind = 'N'
           AND stg.process_id = trim(:GV_exportprocess_id)
        ) merchhier order by merchhier.seq_no;

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('merchhierarchy',
                                'delta',
                                 USER,
                                 SYSDATE);

EOF
err=$?

if [[ `grep "^ORA-" $MERCH_HIERAR_DELTA | wc -l` -gt 0 ]]; then
   status=${FATAL}
   cat $MERCH_HIERAR_DELTA >> $ERRORFILE
   rm $MERCH_HIERAR_DELTA
else
   COUNTER=`wc -l ${MERCH_HIERAR_DELTA}| awk '{print $1}'`
   mv "${MERCH_HIERAR_DELTA}" "${MERCH_HIERAR_DELTA}"_${COUNTER}.dat
   status=${OK}
fi    

return ${status}

}

#-------------------------------------------------------------------------
#     MAIN
#-------------------------------------------------------------------------
# Validate the number of arguments

if [[ $# -lt 1 || $# -gt 2 || $# -lt 2 ]]; then
   USAGE
   exit 1
fi

CONNECT=$1
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${CONNECT}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${con_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "export_merchhier.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#Getting process_id
current_process_id=`${ORACLE_HOME}/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select export_process_id_seq.nextval
  from dual;
EOF`
process_id_status=$?

if [[ $process_id_status -ne ${OK} ]]; then
    LOG_ERROR "Error while retrieveing the process_id" "GETTING_PROCESS_ID" "${process_id_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
    exit 1
else
    UPDATE_MERCHHIER_EXPORT_STG $current_process_id
    st=$?
    if [[ ${st} -ne ${OK} ]]; then
       LOG_ERROR "Error while updating the process_id" "UPDATE_MERCHHIER_EXPORT_STG" "${FATAL}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
       exit 1 
    fi
fi

if [[ $2 = "full" ]]; then
    PROCESS_FULL
    process_full_status=$?
    if [[ ${process_full_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL" "${process_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    else 
        PROCESS_STG $current_process_id
        process_stg_status=$?
        
        if [[ ${process_stg_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" ${process_stg_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
        else
            LOG_MESSAGE "export_merchhier.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
    fi
elif [[ $2 = "delta" ]]; then
    PROCESS_DELTA $current_process_id
    process_delta_status=$?
    if [[ ${process_delta_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA" ${process_delta_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    else 
        PROCESS_STG $current_process_id
        process_stg_status=$?
        if [[ ${process_stg_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" ${process_stg_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
        else
            LOG_MESSAGE "export_merchhier.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
    fi
fi

exit 0
