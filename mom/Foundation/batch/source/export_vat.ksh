#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  export_vat.ksh
#
#  Desc:  UNIX shell script to extract the itemloc records.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='export_vat.ksh'
pgmName=${pgmName##*/}               # get the file name
pgmExt=${pgmName##*.}                # get the extenstion
pgmName=${pgmName%.*}                # remove the file extenstion
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
DIR=`pwd`

OK=0
FATAL=255
#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <mode>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <mode> mode of extraction. full or delta"
}
#-------------------------------------------------------------------------
# Function Name: UPDATE_EXPORT_STG
# Purpose      : Updating the vat_export_stg with process id.
#-------------------------------------------------------------------------
function UPDATE_EXPORT_STG
{
process_id=$1
echo "Updating the vat_export_stg with process_id: $process_id" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

   VARIABLE GV_script_error CHAR(255);
   VARIABLE GV_return_code NUMBER;
   VARIABLE GV_process_id NUMBER;

   EXEC :GV_process_id := $process_id;
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;
     update vat_export_stg
        set process_id = trim(:GV_process_id)
      where base_extracted_ind = 'N'
        and process_id is null;

      COMMIT;
     /

   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
       status=${FATAL}
   else
       status=${OK}
   fi
   return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_STG
# Purpose      : Updating the exported records of vat_export_stg
#-------------------------------------------------------------------------
function PROCESS_STG
{
process_id=$1
echo "Updating the exported records of vat_export_stg" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

VARIABLE GV_script_error CHAR(255);
VARIABLE GV_return_code NUMBER;
VARIABLE GV_process_id NUMBER;

EXEC :GV_process_id := $process_id;
EXEC :GV_script_error := NULL;
EXEC :GV_return_code := 0;

WHENEVER SQLERROR EXIT 1;
  update vat_export_stg
     set base_extracted_ind = 'Y'
   where base_extracted_ind = 'N'
     and process_id = trim(:GV_process_id);
   COMMIT;
  /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#------------------------------------------------------------------------
# Function: PROCESS_FULL
# Purpose : Extracting the full vat records.
#------------------------------------------------------------------------
function PROCESS_FULL
{
echo "Extracting the full vat records." >> $LOGFILE
VAT_FULL=$DIR/vat_${extractDate}_full
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$VAT_FULL
 set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   SELECT family        ||'|'||
       type             ||'|'||
       vat_region       ||'|'||
       vat_region_name  ||'|'||
       vat_code         ||'|'||
       vat_code_desc    ||'|'||
       active_date      ||'|'||
       vat_rate
FROM (
SELECT 'VAT'                                 family,
       'FULL'                                type,
       vat_region                            vat_region,
       vat_region_name                       vat_region_name,
       vr.vat_code                           vat_code,
       vc.vat_code_desc                      vat_code_desc,
       TO_CHAR(vr.active_date,'DD-MON-YYYY') active_date,
       vr.vat_rate                           vat_rate
  FROM vat_codes vc,
       vat_code_rates vr,
       vat_region
 WHERE vc.vat_code = vr.vat_code
   AND vat_calc_type <> 'E'
  ORDER BY vat_region,
           vat_code);

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('vat',
                                'full',
                                 USER,
                                 SYSDATE);
EOF
   if [[ `grep "^ORA-" $VAT_FULL | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $VAT_FULL >> $ERRORFILE
      rm $VAT_FULL
   else
      COUNTER=`wc -l ${VAT_FULL}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${VAT_FULL}" "${VAT_FULL}"_${COUNTER}.dat
      else
         rm ${VAT_FULL}
      fi
      status=${OK}
   fi
return ${status}
}
#------------------------------------------------------------------------
# Function: PROCESS_DELTA
# Purpose : Extracting the delta vat records.
#------------------------------------------------------------------------
function PROCESS_DELTA
{
process_id=$1
echo "Extracting the delta vat records." >> $LOGFILE
VAT_DELTA=$DIR/vat_${extractDate}_delta
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$VAT_DELTA
 set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0
   VARIABLE GV_process_id NUMBER;
   EXEC :GV_process_id := $process_id;

   SELECT family           ||'|'||
          type             ||'|'||
          vat_region       ||'|'||
          vat_region_name  ||'|'||
          vat_code         ||'|'||
          vat_code_desc    ||'|'||
          active_date      ||'|'||
          vat_rate
   FROM (
   SELECT 'VAT'                                                         family,
          UPPER(stg.action_type)                                        type,
          stg.vat_region                                                vat_region,
          DECODE(stg.action_type, 'vatdel', NULL, vr.vat_region_name)   vat_region_name,
          stg.vat_code                                                  vat_code,
          DECODE(stg.action_type, 'vatdel', NULL, vc.vat_code_desc)     vat_code_desc,
          TO_CHAR(stg.active_date,'DD-MON-YYYY')                        active_date,
          DECODE(stg.action_type, 'vatdel', NULL, vcr.vat_rate)         vat_rate,
          stg.seq_no                                                    seq_no
     FROM vat_export_stg stg,
          vat_codes vc,
          vat_code_rates vcr,
          vat_region vr
    WHERE stg.vat_region = vr.vat_region(+)
      AND stg.vat_code   = vc.vat_code(+)
      AND stg.vat_code   = vcr.vat_code(+)
      AND stg.active_date = vcr.active_date(+)
      AND stg.base_extracted_ind = 'N'
      AND stg.process_id = trim(:GV_process_id)
     ORDER BY stg.seq_no,
              stg.vat_region,
              stg.vat_code);

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('vat',
                                'delta',
                                 USER,
                                 SYSDATE);
EOF
   if [[ `grep "^ORA-" $VAT_DELTA | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $VAT_DELTA >> $ERRORFILE
      rm $VAT_DELTA
   else
      COUNTER=`wc -l ${VAT_DELTA}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${VAT_DELTA}" "${VAT_DELTA}"_${COUNTER}.dat
      else
         rm ${VAT_DELTA}
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
#           MAIN
#-------------------------------------------------------------------------
if [[ $# -ne 2 ]]; then
   USAGE
   exit 1
fi

CONNECT=$1
connect=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`
conn_status=$?
if [[ ${conn_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${conn_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "export_itemvat.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

current_process_id=`${ORACLE_HOME}/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select export_process_id_seq.nextval
  from dual;
EOF`
prc_id_status=$?

if [[ ${prc_id_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "GETTING_PROCESS_ID" "${prc_id_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   UPDATE_EXPORT_STG $current_process_id
   upd_exp_st=$?
   if [[ ${upd_exp_st} -ne ${OK} ]]; then
      LOG_ERROR "Error while updating vat_export_stg with current process_id" "UPDATE_EXPORT_STG" "${upd_exp_st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
      exit 1
   fi
fi

if [[ $2 = "full" ]]; then
   PROCESS_FULL
   prc_ful_sts=$?
   if [[ ${prc_ful_sts} -ne ${OK} ]]; then
      LOG_ERROR "Error while updating vat_export_stg with current process_id" "PROCESS_FULL" "${prc_ful_sts}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
      exit 1
   else
      PROCESS_STG $current_process_id
      prc_stg_sts=$?
      if [[ ${prc_stg_sts} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating vat_export_stg with current process_id" "PROCESS_STG" "${prc_stg_sts}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      else
         LOG_MESSAGE "export_itemvat.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      fi
   fi
fi
if [[ $2 = "delta" ]]; then
   PROCESS_DELTA $current_process_id
   prc_delta_sts=$?
   if [[ ${prc_delta_sts} -ne ${OK} ]]; then
      LOG_ERROR "Error while updating vat_export_stg with current process_id" "PROCESS_DELTA" "${prc_delta_sts}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
      exit 1
   else
      PROCESS_STG $current_process_id
      prc_stg_sts=$?
      if [[ ${prc_stg_sts} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating vat_export_stg with current process_id" "PROCESS_STG" "${prc_stg_sts}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      else
         LOG_MESSAGE "export_itemvat.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      fi
   fi
fi