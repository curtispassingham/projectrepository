#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  export_stg_purge.ksh
#
#  Desc:  UNIX shell script to extract the differentiator.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='export_stg_purge.ksh'
pgmName=${pgmName##*/}               # get the file name
pgmExt=${pgmName##*.}                # get the extenstion
pgmName=${pgmName%.*}                # remove the file extenstion
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
DIR=`pwd`

OK=0
FATAL=255
NONFATAL=1
#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet."
}
#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Execute the SQL and PL/SQL statements.
#-------------------------------------------------------------------------
function EXEC_SQL
{
sqlTxt=$*

  sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_result         NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      EXEC :GV_result := 0;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`
    
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
      status=${FATAL}
   else
      status=${OK}
   fi
   return ${status}
}
#------------------------------------------------------------------------
# Function Name: PURGE_EXPORT_STG
# Purpose      : Purging the stg table of all export
#------------------------------------------------------------------------
function PURGE_EXPORT_STG
{
sqltxt="
     DECLARE
        FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if DATA_EXPORT_SQL.PURGE_STG(:GV_script_error) = FALSE then
            raise FUNCTION_ERROR;
         end if;

         if :GV_script_error is NOT NULL then
            :GV_return_code := ${NONFATAL};
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
EXEC_SQL $sqltxt
exec_sql_st=$?
if [[ ${exec_sql_st} -ne ${OK} ]]; then
   status=${FATAL}
else
   status=${OK}
fi
return ${status}
}
#-------------------------------------------------------------------------
#               MAIN
#-------------------------------------------------------------------------

if [[ $# -lt 1 || $# -gt 1 ]]; then
   USAGE
   exit 1
fi
CONNECT=$1
connn_chk=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s ${CONNECT}`
conn_status=$?

if [[ ${conn_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${conn_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "export_stg_purge.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   PURGE_EXPORT_STG
   prg_status=$?
   if [[ ${prg_status} -ne ${OK} ]]; then
      LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PURGE_EXPORT_STG" "${prg_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
      exit $prg_status
   else 
      LOG_MESSAGE "export_stg_purge.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi
fi

