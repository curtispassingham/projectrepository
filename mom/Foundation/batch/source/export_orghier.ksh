#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  export_orghier.ksh
#
#  Desc:  UNIX shell script to extract organizational hierarchy data.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='export_orghier.ksh'
pgmName=${pgmName##*/}               # remove the path
pgmExt=${pgmName##*.}                # get the extension
pgmName=${pgmName%.*}                # get the program name
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the execution date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
DIR=`pwd`

OK=0
FATAL=255

#---------------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#---------------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <mode>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.
   
   <mode> mode of extraction. full or delta"
}

#---------------------------------------------------------------------------------
# Function Name: UPDATE_ORGHIER_EXPORT_STG
# Purpose:       Initiate records to be extracted from the orghier_export_stg
#                table. Set process_id for those with base_extracted_ind = 'N'.
#---------------------------------------------------------------------------------

function UPDATE_ORGHIER_EXPORT_STG
{

prcs_id=$1
echo "Updating orghier_export_stg with process id: $prcs_id" >> $LOGFILE

     sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);
      VARIABLE GV_exportprocess_id NUMBER;

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      EXEC :GV_exportprocess_id := $prcs_id;

      WHENEVER SQLERROR EXIT ${FATAL}
      update orghier_export_stg
         set process_id = trim(:GV_exportprocess_id)
       where base_extracted_ind = 'N';

      COMMIT;
      /

      print :GV_script_error;
      exit  :GV_return_code;
     "  | sqlplus -s ${CONNECT} `

   if [[ $? -ne ${OK} ]]; then
      status=${FATAL}
   else
      status=${OK}
   fi
   return ${status}
}

#-------------------------------------------------------------------------
# Function Name: PROCESS_FULL
# Purpose      :
#-------------------------------------------------------------------------

function PROCESS_FULL
{
   echo "Extracting Full Organizational Hierarchy..." >>$LOGFILE

    # Set filename to contain this organizational hierarchy
   ORGHIER=$DIR/orghierarchy_${extractDate}_full

    # Get the organizational hierarchy to be processed
#   [ -f $ORGHIER ] && rm $ORGHIER
   $ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >$ORGHIER
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   select family      || '|' ||
          action      || '|' ||
          hierlevel   || '|' ||
          hierid      || '|' ||
          hiername    || '|' ||
          parentlevel || '|' ||
          parentid    || '|' ||
          mgr_name    || '|' ||
          currency_code
    from (select 'ORGHIERARCHY' as family,
                 'FULL' as action,
                 'AREA' as hierlevel,
                 a.area as hierid,
                 a.area_name as hiername,
                 'CHAIN' as parentlevel,
                 a.chain as parentid,
                 a.mgr_name as mgr_name,
                 a.currency_code as currency_code
            from area a
           union all
          select 'ORGHIERARCHY' as family,
                 'FULL' as action,
                 'CHAIN' as hierlevel,
                 c.chain as hierid,
                 c.chain_name as hiername,
                 'COMPANY' as parentlevel,
                 co.company as parentid,
                 c.mgr_name as mgr_name,
                 c.currency_code as currency_code
            from chain c,
                 comphead co
           union all
          select 'ORGHIERARCHY' as family,
                 'FULL' as action,
                 'COMPANY' as hierlevel,
                 co.company as hierid,
                 co.co_name as hiername,
                 NULL as parentlevel,
                 NULL as parentid,
                 NULL as mgr_name,
                 NULL as currency_code
            from comphead co
           union all
          select 'ORGHIERARCHY' as family,
                 'FULL' as action,
                 'DISTRICT' as hierlevel,
                 d.district as hierid,
                 d.district_name as hiername,
                 'REGION' as parentlevel,
                 d.region as parentid,
                 d.mgr_name as mgr_name,
                 d.currency_code as currency_code
            from district d
           union all
          select 'ORGHIERARCHY' as family,
                 'FULL' as action,
                 'REGION' as hierlevel,
                 r.region as hierid,
                 r.region_name as hiername,
                 'AREA' as parentlevel,
                 r.area as parentid,
                 r.mgr_name as mgr_name,
                 r.currency_code as currency_code
            from region r
           union all
          select 'ORGHIERARCHY' as family,
                 'FULL' as action,
                 'STORE' as hierlevel,
                 s.store as hierid,
                 REPLACE(s.store_name,CHR(10),'') as hiername,
                 'DISTRICT' as parentlevel,
                 s.district as parentid,
                 s.store_mgr_name as mgr_name,
                 s.currency_code as currency_code
            from store s
           union all
          select 'ORGHIERARCHY' as family,
                 'FULL' as action,
                 'WAREHOUSE' as hierlevel,
                 w.wh as hierid,
                 REPLACE(w.wh_name,CHR(10),'') as hiername,
                 'COMPANY' as parentlevel,
                 co.company as parentid,
                 NULL as mgr_name,
                 w.currency_code as currency_code
            from wh w,
                 comphead co) orghier;

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('orghierarchy',
                                'full',
                                 USER,
                                 SYSDATE);

EOF
err=$?
   if [[ `grep "^ORA-" $ORGHIER | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $ORGHIER >> $ERRORFILE
      rm $ORGHIER
   else
      COUNTER=`wc -l ${ORGHIER}| awk '{print $1}'`
      mv "${ORGHIER}" "${ORGHIER}"_${COUNTER}.dat
      status=${OK}
   fi

   return ${status}
}

#-------------------------------------------------------------------------
# Function Name: PROCESS_DELTA
# Purpose      :
#-------------------------------------------------------------------------

function PROCESS_DELTA
{
   process_id=$1
   echo "Extracting Delta Organizational Hierarchy..." >>$LOGFILE

    # Set filename to contain this organizational hierarchy
   ORGHIER=$DIR/orghierarchy_${extractDate}_delta

    # Create the flatfile
   [ -f $ORGHIER ] && rm $ORGHIER
   $ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >$ORGHIER
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   VARIABLE GV_exportprocess_id NUMBER;

   EXEC :GV_exportprocess_id := $process_id;
   WHENEVER SQLERROR EXIT 1

   select family      || '|' ||
          action      || '|' ||
          hierlevel   || '|' ||
          hierid      || '|' ||
          hiername    || '|' ||
          parentlevel || '|' ||
          parentid    || '|' ||
          mgr_name    || '|' ||
          currency_code
    from (/* area */
          select oes.seq_no,'ORGHIERARCHY' as family,
                 UPPER(oes.action_type) as action,
                 'AREA' as hierlevel,
                 oes.area as hierid,
                 a.area_name as hiername,
                 UPPER(oes.parent_level) as parentlevel,
                 a.chain as parentid,
                 a.mgr_name as mgr_name,
                 a.currency_code as currency_code
            from orghier_export_stg oes,
                 area a
           where oes.area = a.area(+)
             and oes.action_type in ('areacre', 'areamod', 'areadel')
             and process_id = trim(:GV_exportprocess_id)
           union all
          /* chain */
          select oes.seq_no,'ORGHIERARCHY' as family,
                 UPPER(oes.action_type) as action,
                 'CHAIN' as hierlevel,
                 oes.chain as hierid,
                 c.chain_name as hiername,
                 UPPER(oes.parent_level) as parentlevel,
                 TO_NUMBER(DECODE(oes.action_type, 'chaindel', NULL, co.company)) as parentid,
                 c.mgr_name as mgr_name,
                 c.currency_code as currency_code
            from orghier_export_stg oes,
                 chain c,
                 comphead co
           where oes.chain = c.chain(+)
             and oes.action_type in ('chaincre', 'chainmod', 'chaindel')
             and process_id = trim(:GV_exportprocess_id)
           union all
          /* company */
          select oes.seq_no,'ORGHIERARCHY' as family,
                 UPPER(oes.action_type) as action,
                 'COMPANY' as hierlevel,
                 oes.company as hierid,
                 co.co_name as hiername,
                 UPPER(oes.parent_level) as parentlevel,
                 NULL as parentid,
                 NULL as mgr_name,
                 NULL as currency_code
            from orghier_export_stg oes,
                 comphead co
           where oes.company = co.company(+)
             and oes.action_type in ('compcre', 'compmod')
             and process_id = trim(:GV_exportprocess_id)
           union all
          /* district */
          select oes.seq_no,'ORGHIERARCHY' as family,
                 UPPER(oes.action_type) as action,
                 'DISTRICT' as hierlevel,
                 oes.district as hierid,
                 d.district_name as hiername,
                 UPPER(oes.parent_level) as parentlevel,
                 d.region as parentid,
                 d.mgr_name as mgr_name,
                 d.currency_code as currency_code
            from orghier_export_stg oes,
                 district d
           where oes.district = d.district(+)
             and oes.action_type in ('districtcre', 'districtmod', 'districtdel')
             and process_id = trim(:GV_exportprocess_id)
           union all
          /* region */
          select oes.seq_no,'ORGHIERARCHY' as family,
                 UPPER(oes.action_type) as action,
                 'REGION' as hierlevel,
                 oes.region as hierid,
                 r.region_name as hiername,
                 UPPER(oes.parent_level) as parentlevel,
                 r.area as parentid,
                 r.mgr_name as mgr_name,
                 r.currency_code as currency_code
            from orghier_export_stg oes,
                 region r
           where oes.region = r.region(+)
             and oes.action_type in ('regioncre', 'regionmod', 'regiondel')
             and process_id = trim(:GV_exportprocess_id)
           union all
          /* store */
          select oes.seq_no,'ORGHIERARCHY' as family,
                 UPPER(oes.action_type) as action,
                 'STORE' as hierlevel,
                 oes.store as hierid,
                 REPLACE(s.store_name,CHR(10),'') as hiername,
                 UPPER(oes.parent_level) as parentlevel,
                 s.district as parentid,
                 s.store_mgr_name as mgr_name,
                 s.currency_code as currency_code
            from orghier_export_stg oes,
                 store s
           where oes.store = s.store(+)
             and oes.action_type in ('storecre', 'storemod', 'storedel')
             and process_id = trim(:GV_exportprocess_id)
           union all
          /* warehouse */
          select oes.seq_no,'ORGHIERARCHY' as family,
                 UPPER(oes.action_type) as action,
                 'WAREHOUSE' as hierlevel,
                 oes.wh as hierid,
                 REPLACE(w.wh_name,CHR(10),'') as hiername,
                 UPPER(oes.parent_level) as parentlevel,
                 co.company as parentid,
                 NULL as mgr_name,
                 w.currency_code as currency_code
            from orghier_export_stg oes,
                 wh w,
                 comphead co
           where oes.wh = w.wh(+)
             and oes.action_type in ('whcre', 'whmod', 'whdel')
             and process_id = trim(:GV_exportprocess_id)
           order by hierlevel, seq_no) orghier;

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('orghierarchy',
                                'delta',
                                 USER,
                                 SYSDATE);

EOF
err=$?

if [[ `grep "^ORA-" $ORGHIER | wc -l` -gt 0 ]]; then
   status=${FATAL}
   cat $ORGHIER >> $ERRORFILE
   rm $ORGHIER
else
   COUNTER=`wc -l ${ORGHIER}| awk '{print $1}'`
   mv "${ORGHIER}" "${ORGHIER}"_${COUNTER}.dat
   status=${OK}
fi

return ${status}

}

#-------------------------------------------------------------------------
# Function Name: PROCESS_STG
# Purpose      :
#-------------------------------------------------------------------------

function PROCESS_STG
{
process_id=$1
   echo "Updating exported records" >> $LOGFILE
   sqlReturn=`echo "set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   VARIABLE GV_return_code    NUMBER;
   VARIABLE GV_script_error   CHAR(255);
   VARIABLE GV_exportprocess_id NUMBER;

   EXEC :GV_return_code  := 0;
   EXEC :GV_script_error := NULL;
   EXEC :GV_exportprocess_id := $process_id;

   update orghier_export_stg
      set base_extracted_ind = 'Y'
    where base_extracted_ind = 'N'
      and process_id = trim(:GV_exportprocess_id);

   commit;
   /
   print :GV_script_error
   exit  :GV_return_code
   " | sqlplus -s $CONNECT`

   if [[ $? -ne ${OK} ]]; then
      status=${FATAL}
   else
      status=${OK}
   fi
return ${status}

}
#-----------------------------------------------
# Main program starts
# Parse the command line
#-----------------------------------------------

# Validate the number of arguments
if [[ $# -lt 1 || $# -gt 2 || $# -lt 2 ]]; then
   USAGE
   exit 1
fi

CONNECT=$1

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${con_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "export_orghier.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#Retrieve Process ID
current_process_id=`$ORACLE_HOME/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select export_process_id_seq.nextval
  from dual;
exit;
EOF`

#Call Update table

process_id_status=$?

if [[ $process_id_status -ne ${OK} ]]; then
    LOG_ERROR "Error while retrieveing the process_id" "GETTING_PROCESS_ID" "${process_id_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
    exit 1
else
    UPDATE_ORGHIER_EXPORT_STG $current_process_id
    st=$?
    if [[ ${st} -ne ${OK} ]]; then
       LOG_ERROR "Error while updating the process_id" "UPDATE_ORGHIER_EXPORT_STG" "${FATAL}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
       exit 1 
    fi
fi

#Call data extract
if [[ $2 = "full" ]]; then
    PROCESS_FULL
    process_full_status=$?
    if [[ ${process_full_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL" "${process_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    else 
        PROCESS_STG $current_process_id
        process_stg_status=$?
        
        if [[ ${process_stg_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" ${process_stg_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
        else
            LOG_MESSAGE "export_orghier.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
    fi
elif [[ $2 = "delta" ]]; then
    PROCESS_DELTA $current_process_id
    process_delta_status=$?
    if [[ ${process_delta_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA" ${process_delta_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    else 
        PROCESS_STG $current_process_id
        process_stg_status=$?
        if [[ ${process_stg_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" ${process_stg_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
        else
            LOG_MESSAGE "export_orghier.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
    fi
fi

exit 0