#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  export_diffgrp.ksh
#
#  Desc:  UNIX shell script to extract the differentiator.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='export_diffgrp.ksh'
pgmName=${pgmName##*/}               #get the file name
pgmExt=${pgmName##*.}                #get the extenstion
pgmName=${pgmName%.*}                #remove the file extenstion
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
DIR=`pwd`

OK=0
FATAL=255
#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <mode>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <mode> mode of extraction. full or delta"
}
#-------------------------------------------------------------------------
# Function: UPDATE_DIFFGRP_EXPORT_STG
# Desc: Updating the diffgrp_export_stg with current process id
#-------------------------------------------------------------------------
function UPDATE_DIFFGRP_EXPORT_STG
{
process_id=$1
echo "Updating diffgrp_export_stg with process id: $process_id" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

   VARIABLE GV_process_id NUMBER;
   VARIABLE GV_script_error CHAR(255);
   VARIABLE GV_return_code NUMBER;

   EXEC :GV_process_id := ${process_id};
   EXEC :GV_return_code  := 0;
   EXEC :GV_script_error := NULL;

   WHENEVER SQLERROR EXIT 1;
   update diffgrp_export_stg
      set process_id = trim(:GV_process_id)
    where base_extracted_ind = 'N'
      and process_id is null;

   COMMIT;
   /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`


   if [[ $? -ne ${OK} ]]; then
      status=${FATAL}
   else
      status=${OK}
   fi
   return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_STG
# Purpose      : Update the exported records.
#-------------------------------------------------------------------------
function PROCESS_STG
{
process_id=$1
   echo "Updating exported records" >> $LOGFILE
   sqlReturn=`echo "set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   VARIABLE GV_return_code    NUMBER;
   VARIABLE GV_script_error   CHAR(255);
   VARIABLE GV_exportprocess_id NUMBER;

   EXEC :GV_return_code  := 0;
   EXEC :GV_script_error := NULL;
   EXEC :GV_exportprocess_id := $process_id;

   update diffgrp_export_stg
      set base_extracted_ind = 'Y'
    where base_extracted_ind = 'N'
      and process_id = trim(:GV_exportprocess_id);

   commit;
   /
   print :GV_script_error
   exit  :GV_return_code
   " | sqlplus -s $CONNECT`


   if [[ $? -ne ${OK} ]]; then
      status=${FATAL}
   else
      status=${OK}
   fi
return ${status}

}
#-------------------------------------------------------------------------
# Function Name: PROCESS_FULL_HEAD
# Purpose      : Export full header records
#-------------------------------------------------------------------------
function PROCESS_FULL_HEAD
{
 echo "Extracting full diff group header records" >> $LOGFILE
 DIFFGRPHD=$DIR/diffgrphdr_${extractDate}_full

   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$DIFFGRPHD
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0
   SELECT family       ||'|'||
       type            ||'|'||
       diff_group_id   ||'|'||
       diff_group_desc ||'|'||
       diff_type       ||'|'||
       diff_type_desc
   FROM (
      SELECT 'DIFFGRPHDR'       family,
             'FULL'             type,
             dh.diff_group_id   diff_group_id,
             dh.diff_group_desc diff_group_desc,
             dh.diff_type       diff_type,
             dt.diff_type_desc  diff_type_desc
        FROM diff_group_head dh,
             diff_type dt
      WHERE dh.diff_type = dt.diff_type) diffgrphead;
EOF
err=$?
   if [[ `grep "^ORA-" $DIFFGRPHD | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $DIFFGRPHD >> $ERRORFILE
      rm $DIFFGRPHD
   else
      COUNTER=`wc -l ${DIFFGRPHD}| awk '{print $1}'`
      mv "${DIFFGRPHD}" "${DIFFGRPHD}"_${COUNTER}.dat
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_FULL_DETAIL
# Purpose      : Export full detail records
#-------------------------------------------------------------------------
function PROCESS_FULL_DETAIL
{
   echo "Extracting full diff group detail records" >> $LOGFILE
   DIFFGRPDT=$DIR/diffgrpdtl_${extractDate}_full

   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$DIFFGRPDT
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   SELECT  family         ||'|'||
           type            ||'|'||
           diff_group_id   ||'|'||
           diff_id         ||'|'||
           display_seq
    FROM (
       SELECT 'DIFFGRPDTL'       family,
              'FULL'             type,
              dd.diff_group_id   diff_group_id,
              dd.diff_id         diff_id,
              dd.display_seq     display_seq
         FROM diff_group_head dh,
              diff_group_detail dd,
              diff_ids d
        WHERE dh.diff_group_id = dd.diff_group_id
          AND dd.diff_id = d.diff_id) diffgrpdtl;

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                         values('diffgrp',
                                'full',
                                USER,
                                SYSDATE);

EOF
err=$?
   if [[ `grep "^ORA-" $DIFFGRPDT | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $DIFFGRPDT >> $ERRORFILE
      rm $DIFFGRPDT
   else
      COUNTER=`wc -l ${DIFFGRPDT}| awk '{print $1}'`
      mv "${DIFFGRPDT}" "${DIFFGRPDT}"_${COUNTER}.dat
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_DELTA_HEAD
# Purpose      : Export the records,which are not exported last time
#-------------------------------------------------------------------------
function PROCESS_DELTA_HEAD
{
Process_id=$1
echo "Extracting delta diff group header records." >> $LOGFILE
DIFFHD_DELTA=$DIR/diffgrphdr_${extractDate}_delta
   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF > $DIFFHD_DELTA
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000
   set pages 0

   VARIABLE GV_exportprocess_id NUMBER;
   EXEC :GV_exportprocess_id := $Process_id;
   WHENEVER SQLERROR EXIT 1
   SELECT family          ||'|'||
          type            ||'|'||
          diff_group_id   ||'|'||
          diff_group_desc ||'|'||
          diff_type       ||'|'||
          diff_type_desc
   FROM (
         SELECT 'DIFFGRPHDR'          family,
                upper(stg.action_type)type,
                stg.diff_group_id     diff_group_id,
                dh.diff_group_desc    diff_group_desc,
                dh.diff_type          diff_type,
                (select diff_type_desc from diff_type dt where dt.diff_type = dh.diff_type) diff_type_desc,
                stg.seq_no
           FROM diff_group_head dh,
                diffgrp_export_stg stg
          WHERE stg.diff_group_id = dh.diff_group_id(+)
            AND stg.action_type in ('diffgrphdrcre','diffgrphdrmod','diffgrphdrdel')
            AND stg.process_id = trim(:GV_exportprocess_id)
            AND stg.base_extracted_ind = 'N') diffgrphead order by diffgrphead.seq_no;
EOF
err=$?

if [[ `grep "^ORA-" $DIFFHD_DELTA | wc -l` -gt 0 ]]; then
   status=${FATAL}
   cat $DIFFHD_DELTA >> $ERRORFILE
   rm $DIFFHD_DELTA
else
   COUNTER=`wc -l ${DIFFHD_DELTA}| awk '{print $1}'`
   mv "${DIFFHD_DELTA}" "${DIFFHD_DELTA}"_${COUNTER}.dat
   status=${OK}
fi

return ${status}

}
#-------------------------------------------------------------------------
# Function Name: PROCESS_DELTA_DETAIL
# Purpose      : Export the records,which are not exported last time
#-------------------------------------------------------------------------
function PROCESS_DELTA_DETAIL
{
Process_id=$1
echo "Extracting delta diff group detail records." >> $LOGFILE
DIFFDT_DELTA=$DIR/diffgrpdtl_${extractDate}_delta
   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF > $DIFFDT_DELTA
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000
   set pages 0

   VARIABLE GV_exportprocess_id NUMBER;
   EXEC :GV_exportprocess_id := $Process_id;
   WHENEVER SQLERROR EXIT 1
   SELECT family          ||'|'||
          type            ||'|'||
          diff_group_id   ||'|'||
          diff_id         ||'|'||
          display_seq
   FROM (
         SELECT 'DIFFGRPDTL'                                    family,
                upper(stg.action_type)                          type,
                stg.diff_group_id                               diff_group_id,
                stg.diff_id                                     diff_id,
                dd.display_seq                                  display_seq,
                stg.seq_no
           FROM diff_group_detail dd,
                diffgrp_export_stg stg
          WHERE stg.diff_group_id = dd.diff_group_id(+)
            AND stg.diff_id       = dd.diff_id(+)
            AND stg.action_type in ('diffgrpdtlcre','diffgrpdtlmod','diffgrpdtldel')
            AND stg.process_id = trim(:GV_exportprocess_id)
            AND stg.base_extracted_ind = 'N' order by seq_no ) diffgrpdtl;

         insert into data_export_hist(family,
                                      export_type,
                                      export_id,
                                      exported_datetime)
                              values ('diffgrp',
                                      'delta',
                                       USER,
                                       SYSDATE);
EOF
err=$?

if [[ `grep "^ORA-" $DIFFDT_DELTA | wc -l` -gt 0 ]]; then
   status=${FATAL}
   cat $DIFFDT_DELTA >> $ERRORFILE
   rm $DIFFDT_DELTA
else
   COUNTER=`wc -l ${DIFFDT_DELTA}| awk '{print $1}'`
   mv "${DIFFDT_DELTA}" "${DIFFDT_DELTA}"_${COUNTER}.dat
   status=${OK}
fi

return ${status}

}
#-------------------------------------------------------------------------
# MAIN
#-------------------------------------------------------------------------
# Validate the number of arguments
if [[ $# -lt 1 || $# -gt 2 || $# -lt 2 ]]; then
   USAGE
   exit 1
fi

CONNECT=$1
concheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${CONNECT}`
constatus=$?

if [[ ${constatus} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${constatus}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "export_diffgrp.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#Get the process id
current_process_id=`${ORACLE_HOME}/bin/sqlplus -s ${CONNECT}  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select export_process_id_seq.nextval
  from dual;
EOF`
process_id_status=$?

if [[ ${process_id_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "GETTING_PROCESS_ID" "${process_id_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
    UPDATE_DIFFGRP_EXPORT_STG $current_process_id
    st=$?
    if [[ ${st} -ne ${OK} ]]; then
       LOG_ERROR "Error while updating the process_id" "UPDATE_DIFFGRP_EXPORT_STG" "${FATAL}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
       exit 1
    fi
fi

if [[ $2 = "full" ]]; then
    PROCESS_FULL_HEAD
    process_fullhd_status=$?
    if [[ ${process_fullhd_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL_HEAD" "${process_fullhd_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    fi
    PROCESS_FULL_DETAIL
    process_fulldt_status=$?
    if [[ ${process_fulldt_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL_DETAIL" "${process_fulldt_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    fi
    if [[ ${process_fullhd_status} -eq ${OK} && ${process_fulldt_status} -eq ${OK} ]]; then
        PROCESS_STG $current_process_id
        process_stg_status=$?

        if [[ ${process_stg_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" ${process_stg_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
        else
            LOG_MESSAGE "export_diffgrp.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
    fi
elif [[ $2 = "delta" ]]; then
    PROCESS_DELTA_HEAD $current_process_id
    process_deltahd_status=$?
    if [[ ${process_deltahd_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA_HEAD" ${process_deltahd_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    fi
    PROCESS_DELTA_DETAIL $current_process_id
    process_deltadt_status=$?
    if [[ ${process_deltadt_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA_DETAIL" ${process_deltadt_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    fi
    if [[ ${process_deltahd_status} -eq ${OK} && ${process_deltadt_status} -eq ${OK} ]]; then
        PROCESS_STG $current_process_id
        process_stg_status=$?
        if [[ ${process_stg_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" ${process_stg_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
        else
            LOG_MESSAGE "export_diffgrp.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
    fi
fi

exit 0