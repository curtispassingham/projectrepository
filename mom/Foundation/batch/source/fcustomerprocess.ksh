#! /bin/ksh
#-------------------------------------------------------------------------------------
#  File:  fcustomerprocess.ksh
#
#  Desc:  This shell script updates the RMS franchise customer tables with uploaded franchise customer information.
#          This information is read from the SVC_FCUSTUPLD_* tables.
#          These tables were populated by a previous batch (fcustomerupload.ksh).
#
#         The following are done in this script
#         1. Get the process ID for the file uploaded in the previous batch.
#            This is retrieved from the SVC_FCUSTUPLD_STATUS table.
#         3. Call the core franchise customer upload PL/SQL package for each process id.
#-------------------------------------------------------------------------------------

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='fcustomerprocess.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string> <input file>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <input file>       Input file. Can include directory path.
"
}

#-------------------------------------------------------------------------
# Function Name: GET_PROCESS_ID
# Purpose      : Retrieves the process id from the SVC_FCUSTUPLD_STATUS table
#-------------------------------------------------------------------------
function GET_PROCESS_ID
{
   fcustProcessId=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select max(process_id)
     from svc_fcustupld_status
    where upper(trim(reference_id)) = upper(trim('${inputFileNoPath}'))
      and status = 'N';
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${fcustProcessId}" "GET_PROCESS_ID" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned process ID
   if [ -z ${fcustProcessId} ]; then
      LOG_ERROR "Unable to retrieve the process ID for the file ${inputFileNoPath}." "GET_PROCESS_ID" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Process ID is ${fcustProcessId}." "GET_PROCESS_ID" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}


#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: PROCESS_FCUSTOMER_UPLOAD
# Purpose      :
#-------------------------------------------------------------------------
function PROCESS_FCUSTOMER_UPLOAD
{
   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN
         if NOT CORESVC_FCUSTOMER_UPLOAD_SQL.GET_TRANSACTION(:GV_script_error,
                                                              ${fcustProcessId} ) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "${inputFile} - Function call PROCESS_FCUSTOMER_UPLOAD Process Id: ${fcustProcessId} failed." >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFile} - Process Id: ${fcustProcessId} successfully processed." "PROCESS_FCUSTOMER_UPLOAD" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      echo "successfully processed."
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [ $# -lt 2 ]
then
   USAGE
   exit ${NON_FATAL}
fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
USER=${connectStr%/*}
inputFile=$2
inputFileNoPath=${inputFile##*/}    # remove the path
inputfile_ext=${inputFile##*.}
LOG_MESSAGE "fcustomerprocess.ksh started by ${USER} for file ${inputFileNoPath}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

$ORACLE_HOME/bin/sqlplus -s $connectStr <<EOF >>$ERRORFILE
EOF

# Get the process id from the status table for the input file
GET_PROCESS_ID

#Process the valid transactions
PROCESS_FCUSTOMER_UPLOAD
         
LOG_MESSAGE "fcustomerprocess.ksh finished for ${USER} for file ${inputFile}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

if [ `wc -l $ERRORFILE | awk '{print $1}'` -eq 0 ]
then
   rm -f $ERRORFILE
fi
#-----------End of Processing -------------------------------------------------

exit ${OK}
