
#include <retek.h>
#include <std_rest.h>

EXEC SQL INCLUDE SQLCA.H;
long   SQLCODE;

/* standard restart declarations */
char restart_start_array[1][255];
char restart_application_image_array[2][255];

char ps_domain[NULL_DOMAIN_ID];
char ps_partition_name[31];

/******* FUNCTION PROTOTYPES  *********/

int  init(char* argv[]);
int  process(void);

int  truncate_table(char is_table_name[NULL_TABLE_NAME],
                    char is_table_owner[NULL_TABLE_OWNER],
                    int  ii_partition_table_ind);

int  partition_check(char is_table[NULL_TABLE_NAME],
                     char is_table_owner[NULL_TABLE_OWNER],
                     int  *ioi_partition_ind);
int  final(void);

/*****END FUNCTION PROTOTYPES********/
/*___________________________________*/

int main(int argc, char* argv[])
{
   char* function = "main";
   int   li_init_results;   
   char  ls_log_message[NULL_ERROR_MESSAGE];
   strcpy(PROGRAM, "fcstprg");
   strcpy(g_s_restart_name, "fcstprg");
   
   if (argc < 3)
   {
      fprintf(stderr,"Usage: %s userid/passwd domain\n",argv[0]);
      return(FAILED);
   }

   if (LOGON(argc, argv) < 0)
      return(FAILED);

   if ((li_init_results = init(argv)) < 0)
      gi_error_flag = 2;
   
   if (li_init_results != NO_THREAD_AVAILABLE)
   {
      if (li_init_results == OK)
      {
         if (process() < 0)
            gi_error_flag = 1;
      }

      if (final() < 0)
      {
         if (gi_error_flag == 0)
            gi_error_flag = 3;
      }
   }

   if (gi_error_flag == 2)
   {
      LOG_MESSAGE("Aborted in init");
      return(FAILED);
   }
   else if (gi_error_flag == 1)
   {
      sprintf(ls_log_message,"Thread %s - Aborted in process",g_s_restart_thread_val);
      LOG_MESSAGE(ls_log_message);
      return (FAILED);
   }
   else if (gi_error_flag == 3)
   {
      sprintf(ls_log_message,"Thread %s - Aborted in final",g_s_restart_thread_val);
      LOG_MESSAGE(ls_log_message);
      return (FAILED);
   }
   else if (li_init_results == NO_THREAD_AVAILABLE)
   {
      LOG_MESSAGE("Terminated - No threads available");
      return(NO_THREADS);
   }
   else
   {
      sprintf(ls_log_message,"Thread %s - Terminated Successfully",g_s_restart_thread_val);
      LOG_MESSAGE(ls_log_message);
   }

   return (SUCCEEDED);
}  /* End of main() */


int init(char* argv[])
{
    char *function = "init";
    int  restart_init_results;

    /* standard restart and recovery startup - only used for logging purposes */
    restart_init_results =
       restart_init(restart_start_array, restart_application_image_array,
          restart_num_threads, restart_driver_name);
    if (restart_init_results != 0) return(restart_init_results);

    memset(ps_domain,'\0',sizeof(ps_domain));
    strncpy(ps_domain, argv[2],sizeof(ps_domain)-1);

    return(0);
}  /* end of init()   */

int process(void)
{
    char *function = "process";
    char ls_table_owner[NULL_TABLE_OWNER];
    char ls_table_name[NULL_TABLE_NAME]= ""; 
    char ls_table_temp[NULL_TABLE_NAME];
    int  li_partition_ind;
    int  li_partition_ind_temp = 0;


    /******* ITEM_FORECAST ********/  

    strcpy(ls_table_temp, "item_forecast");
    if (synonym_trace(ls_table_temp, ls_table_owner, ls_table_name) < 0) return(-1);
    
    if (partition_check(ls_table_name, ls_table_owner, &li_partition_ind_temp) < 0) return(-1);
    else
       li_partition_ind = li_partition_ind_temp;

    if (truncate_table(ls_table_name, ls_table_owner, li_partition_ind) < 0) return(-1);


    /******* DEPT_SALES_FORECAST ********/  

    strcpy(ls_table_temp, "dept_sales_forecast");
    if (synonym_trace(ls_table_temp, ls_table_owner, ls_table_name) < 0) return(-1);

    if (partition_check(ls_table_name, ls_table_owner, &li_partition_ind_temp) < 0) return(-1);
    else
       li_partition_ind = li_partition_ind_temp;

    if (truncate_table(ls_table_name, ls_table_owner, li_partition_ind) < 0) return(-1);


    /******* CLASS_SALES_FORECAST ********/  

    strcpy(ls_table_temp, "class_sales_forecast");
    if (synonym_trace(ls_table_temp, ls_table_owner, ls_table_name) < 0) return(-1);

    if (partition_check(ls_table_name, ls_table_owner, &li_partition_ind_temp) < 0) return(-1);
    else
       li_partition_ind = li_partition_ind_temp;

    if (truncate_table(ls_table_name, ls_table_owner, li_partition_ind) < 0) return(-1);

    /******* SUBCLASS_SALES_FORECAST ********/  

    strcpy(ls_table_temp, "subclass_sales_forecast");
    if (synonym_trace(ls_table_temp, ls_table_owner, ls_table_name) < 0) return(-1);

    if (partition_check(ls_table_name, ls_table_owner, &li_partition_ind_temp) < 0) return(-1);
    else
       li_partition_ind = li_partition_ind_temp;

    if (truncate_table(ls_table_name, ls_table_owner, li_partition_ind) < 0) return(-1);

    return(0);

} /* end of process()  */



int truncate_table(char is_table_name[NULL_TABLE_NAME],
                   char is_table_owner[NULL_TABLE_OWNER],
                   int  ii_partition_table_ind)
{
   char *function = "truncate_table";
   char ls_dynamic_statement[255];

   if (ii_partition_table_ind == 1)
   {
      snprintf(ls_dynamic_statement, sizeof(ls_dynamic_statement), "ALTER TABLE %s.%s TRUNCATE PARTITION %s DROP STORAGE",
                is_table_owner, is_table_name, ps_partition_name);
   }
   else if (ii_partition_table_ind == 0) 
   {
        snprintf(ls_dynamic_statement, sizeof(ls_dynamic_statement), "DELETE %s.%s WHERE DOMAIN_ID = %s",is_table_owner, is_table_name, ps_domain);      
   }     

   EXEC SQL EXECUTE IMMEDIATE :ls_dynamic_statement;
   if SQL_ERROR_FOUND
   {
      snprintf(err_data, sizeof(err_data), "Execute Immediate statement:  %s", ls_dynamic_statement);
      strcpy(table, is_table_name);
      WRITE_ERROR(SQLCODE, function, table, err_data);
      return(-1);
   }

   return(0);
} /* end truncate_table()  */


int partition_check(char is_table[NULL_TABLE_NAME], 
                    char is_table_owner[NULL_TABLE_OWNER], 
                    int  *ioi_partition_ind) 
{
    char *function = "partition_check"; 
    int  li_part_ind_temp = 0;
    ps_partition_name[0] = '\0';
    
    EXEC SQL VAR ps_partition_name IS STRING(31);
    
    EXEC SQL EXECUTE
       DECLARE

          CURSOR c_partition_info is
          SELECT partition_name, high_value
            FROM all_tab_partitions
           WHERE table_name = UPPER(:is_table)
             AND table_owner = UPPER(:is_table_owner);

       BEGIN
         FOR rec IN c_partition_info 
            LOOP
            IF rec.high_value = :ps_domain
            THEN
               :li_part_ind_temp := 1;
               :ps_partition_name := rec.partition_name;
               EXIT;
            END IF;
         END LOOP;

       END;
    END-EXEC;
   
    if (SQL_ERROR_FOUND)
    {
       snprintf(err_data, sizeof(err_data), "EXECUTE get partition info, table_to_check: %s domain= %s", is_table, ps_domain);
       strcpy(table,"all_tab_partitions");
       WRITE_ERROR(SQLCODE, function, table, err_data);
       return(-1);
    }

    *ioi_partition_ind = li_part_ind_temp;


    return(0);
} /* end of partition_check() */


int final(void)
{
    char *function = "final";
    return(restart_close(restart_err_msg));
} /* end of final() */


