
#include <retek_2.h>
#include <intrface.h>

#define FATAL                            -1
#define NON_FATAL                         1
#define OK                                0
#define NUM_INIT_PARAMETERS               2
#define NUM_COMMIT_PARAMETERS             1
#define NULL_LOG_MESSAGE                255
#define NULL_FORMAT_PARAM               255
#define LEN_YEAR                          4
#define LEN_HALF                          1
#define LEN_QUARTER                       1
#define LEN_MONTH                         2
#define LEN_WEEK                          2
#define LEN_DAY                           1
#define LEN_SHORT_DATE                    8

EXEC SQL INCLUDE SQLCA.H;
long SQLCODE;

init_parameter parameter[NUM_INIT_PARAMETERS] =
{
   "thread_val",      "string",          "",
   "out_file",        "rtk_file",        "O"
};

typedef struct
{
   long l_year;
   long l_half;
   long l_quarter;
   long l_month;
   long l_week;
   long l_yweek;
   long l_day;
   long l_date;
} date_struct;

char ps_thread_val[NULL_THREAD];

/* function prototypes */
int main(int argc, char* argv[]);
int init(rtk_file* of_out);
int process(rtk_file* of_out);
int final(void);
int format_buffer(char* os_format_fdetl);
int get_dates(long* ol_first_year,
	      long* ol_first_month,
	      long* ol_first_day,
	      long* ol_last_year,
	      long* ol_last_month,
	      long* ol_last_day);
int increment_date(long* oi_year,
		   long* oi_month,
		   long* oi_day);
int increment_454(date_struct* io_454_date);
int init_454(date_struct* io_454_date);
int write_fdetl(char* is_format_fdetl,
		date_struct* i_date_454,
		rtk_file* of_out);

int main(int argc, char* argv[])
{
   char* function = "main";
   int   li_init_results;   
   char  ls_log_message[NULL_ERROR_MESSAGE];
   rtk_file lf_out;
   
   if (argc < 2)
   {
      fprintf(stderr,"Usage: %s userid/passwd\n",argv[0]);
      return(FAILED);
   }

   if (LOGON(argc, argv) < 0)
      return(FAILED);

   if ((li_init_results = init(&lf_out)) < 0)
      gi_error_flag = 2;
   
   if (li_init_results != NO_THREAD_AVAILABLE)
   {
      if (li_init_results == OK)
      {
         if (process(&lf_out) < 0)
            gi_error_flag = 1;
      }

      if (final() < 0)
      {
         if (gi_error_flag == 0)
            gi_error_flag = 3;
      }
   }

   if (gi_error_flag == 2)
   {
      LOG_MESSAGE("Aborted in init");
      return(FAILED);
   }
   else if (gi_error_flag == 1)
   {
      sprintf(ls_log_message,"Thread %s - Aborted in process",ps_thread_val);
      LOG_MESSAGE(ls_log_message);
      return (FAILED);
   }
   else if (gi_error_flag == 3)
   {
      sprintf(ls_log_message,"Thread %s - Aborted in final",ps_thread_val);
      LOG_MESSAGE(ls_log_message);
      return (FAILED);
   }
   else if (li_init_results == NO_THREAD_AVAILABLE)
   {
      LOG_MESSAGE("Terminated - No threads available");
      return(NO_THREADS);
   }
   else
   {
      if (g_l_rej_cnt)
      {
         sprintf(ls_log_message,"Thread %s - Terminated Successfully with non-fatal errors",
                 ps_thread_val);
         LOG_MESSAGE(ls_log_message);
         return(WARNING);
      }
      else
      {
         sprintf(ls_log_message,"Thread %s - Terminated Successfully",ps_thread_val);
         LOG_MESSAGE(ls_log_message);
      }
   }

   return (SUCCEEDED);
}  /* End of main() */

int init(rtk_file* of_out)
{ 
   char* function = "init";
   int   li_init_return;

   set_filename(of_out, "rmse_rpas_clndmstr.dat", NOT_PAD);

   li_init_return = retek_init(NUM_INIT_PARAMETERS,
                               parameter,
                               ps_thread_val,
                               of_out);
   if (li_init_return != 0)
      return(li_init_return);

   /* Check if new start; if not, return fatal */
   if (!is_new_start())
   {
      return(FATAL);
   }

   return(OK);

} /* end of init() */

int process(rtk_file* of_out)
{
   char* function = "process";
   int   li_loop = 0;

   char ls_create_date[NULL_DATE];
   char ls_format_fdetl[NULL_FORMAT_PARAM];

   long ll_curr_year;
   long ll_curr_month;
   long ll_curr_day;
   long ll_last_year;
   long ll_last_month;
   long ll_last_day;
   date_struct date_454;

   if (get_dates(&ll_curr_year,
		 &ll_curr_month,
		 &ll_curr_day,
		 &ll_last_year,
		 &ll_last_month,
		 &ll_last_day) < 0)
      return(FATAL);

   date_454.l_date = (ll_curr_year*100+ll_curr_month)*100+ll_curr_day;

   format_buffer(ls_format_fdetl);
   
   if(init_454(&date_454) < 0)
   {
      sprintf(err_data, "454 date init error, %.02ld%.02ld%.04ld",
	      ll_curr_day, ll_curr_month, ll_curr_year);
      WRITE_ERROR(RET_FUNCTION_ERR,function,"",err_data);
      return(FATAL);
   }

   if(ll_curr_year       < ll_last_year)
      li_loop = 1;
   else if(ll_curr_month < ll_last_month)
      li_loop = 1;
   else if(ll_curr_day   <= ll_last_day)
      li_loop = 1;

   while(li_loop)
   {
      if(write_fdetl(ls_format_fdetl,
		     &date_454,
		     of_out) < 0)
	 return(FATAL);

      if(increment_454(&date_454) < 0)
      {
         sprintf(err_data, "454 date increment error, %.02ld%.02ld%.04ld",
		 ll_curr_day, ll_curr_month, ll_curr_year);
	 WRITE_ERROR(RET_FUNCTION_ERR,function,"",err_data);
	 return(FATAL);
      }

      if(increment_date(&ll_curr_year,
			&ll_curr_month,
			&ll_curr_day) < 0)
      {
	 sprintf(err_data, "Cal date incrementation error, %.02ld%.02ld%.04ld",
		 ll_curr_day, ll_curr_month, ll_curr_year);
	 WRITE_ERROR(RET_FUNCTION_ERR,function,"",err_data);
	 return(FATAL);
      }

      date_454.l_date = (ll_curr_year*100+ll_curr_month)*100+ll_curr_day;

      if(ll_curr_year       < ll_last_year)
	 li_loop = 1;
      else if(ll_curr_month < ll_last_month && ll_curr_year <= ll_last_year)
	 li_loop = 1;
      else if(ll_curr_day   <= ll_last_day && ll_curr_month <= ll_last_month && ll_curr_year <= ll_last_year)
	 li_loop = 1;
      else
	 li_loop = 0;
   }

   if (retek_force_commit(NUM_COMMIT_PARAMETERS,
			  of_out) < 0)
     return(FATAL);

   return(OK);

} /* end of process() */


int final(void)
{
   char* function = "final";
   int li_final_return;

   li_final_return = retek_close();

   return(li_final_return);

} /* end of final() */

int format_buffer(char* os_format_fdetl)
{

  /* set up format string for fdetl */
  strcpy(os_format_fdetl, "\0");
  strcat(os_format_fdetl, "%.*ld");        /* year */
  strcat(os_format_fdetl, "%.*ld");        /* half */
  strcat(os_format_fdetl, "%.*ld");        /* quarter */
  strcat(os_format_fdetl, "%-*ld");        /* month */
  strcat(os_format_fdetl, "%-*ld");        /* week */
  strcat(os_format_fdetl, "%.*ld");        /* day */
  strcat(os_format_fdetl, "%.*ld");        /* date */
  strcat(os_format_fdetl, "\n");

  return(OK);

} /* end of format_buffer() */

int write_fdetl(char* is_format_fdetl,
		date_struct* i_date_454,
		rtk_file* of_out)
{
   char *function="write_fdetl";

   if (rtk_print(of_out, is_format_fdetl,
                 LEN_YEAR,       i_date_454->l_year,
                 LEN_HALF,       i_date_454->l_half,
                 LEN_QUARTER,    i_date_454->l_quarter,
                 LEN_MONTH,      i_date_454->l_month,
                 LEN_WEEK,       i_date_454->l_yweek,
                 LEN_DAY,        i_date_454->l_day,
                 LEN_SHORT_DATE, i_date_454->l_date) < 0)
      return(FATAL);
  
   return(OK);

} /* end of write_fdetl() */

int get_dates(long* ol_first_year,
	      long* ol_first_month,
	      long* ol_first_day,
	      long* ol_last_year,
	      long* ol_last_month,
	      long* ol_last_day)
{

   char* function = "get_dates";

   int  li_first_year;
   int  li_first_month;
   int  li_first_day;
   int  li_last_year;
   int  li_last_month;
   int  li_last_day;

   EXEC SQL DECLARE c_get_first_date CURSOR FOR
      SELECT TO_NUMBER(TO_CHAR(c.first_day, 'YYYY')),
             TO_NUMBER(TO_CHAR(c.first_day, 'MM')),
             TO_NUMBER(TO_CHAR(c.first_day, 'DD'))
        FROM calendar c,
             system_options s
       WHERE ABS(s.start_of_half_month) = c.month_454
    ORDER BY first_day;

   EXEC SQL DECLARE c_get_last_date CURSOR FOR
      SELECT TO_NUMBER(TO_CHAR(first_day, 'YYYY')),
             TO_NUMBER(TO_CHAR(first_day, 'MM')),
             TO_NUMBER(TO_CHAR(first_day, 'DD'))
        FROM calendar
    ORDER BY first_day DESC;

   EXEC SQL OPEN c_get_first_date;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Opening c_get_first_date");
      strcpy(table, "calendar, system_options");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   EXEC SQL OPEN c_get_last_date;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Opening c_get_last_date");
      strcpy(table, "calendar");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   EXEC SQL FETCH c_get_first_date INTO :li_first_year,
                                        :li_first_month,
                                        :li_first_day;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Fetching c_get_first_date");
      strcpy(table, "calendar, system_options");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   EXEC SQL FETCH c_get_last_date INTO :li_last_year,
                                       :li_last_month,
                                       :li_last_day;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Fetching c_get_last_date");
      strcpy(table, "calendar");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   EXEC SQL CLOSE c_get_first_date;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Closing c_get_first_date");
      strcpy(table, "calendar, system_options");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   EXEC SQL CLOSE c_get_last_date;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Closing c_get_last_date");
      strcpy(table, "calendar");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   *ol_first_year  = li_first_year;
   *ol_first_month = li_first_month;
   *ol_first_day   = li_first_day;
   *ol_last_year   = li_last_year;
   *ol_last_month  = li_last_month;
   *ol_last_day    = li_last_day;

   return(OK);

} /* end of get_dates() */

int increment_date(long* ol_year,
		   long* ol_month,
		   long* ol_day)
{
   char *function="increment_date";

   if((*ol_day) < 28)
   {
      (*ol_day)++;
      return(OK);
   }

/*    Note: ol_day is >= 28. */

   if((*ol_month) == 2)
   {
/*       Note: it is February. */

      if(((*ol_year)%4 == 0) && ((*ol_year)%100 != 0) ||
        ((*ol_year)%4 == 0) && ((*ol_year)%400 == 0))
      {
/*       Note: it is a leap year.  */
	 
         if((*ol_day) < 29)
         {
            (*ol_day)++;
            return(OK);
         }
         else
         {
            (*ol_day) = 1;
            (*ol_month)++;
            return(OK);
         }
      }
      else
      {
/* 	 Note: it is not a leap year or it is but it is the 29th of Feb. */
	 
         (*ol_day) = 1;
         (*ol_month)++;
         return(OK);
      }
   }

   if((*ol_month) == 4 ||
      (*ol_month) == 6 ||
      (*ol_month) == 9 ||
      (*ol_month) == 11)
   {
/*       Note: it is a 30 day month. */
      if((*ol_day) < 30)
      {
         (*ol_day)++;
         return(OK);
      }
      else
      {
         (*ol_day) = 1;
         (*ol_month)++;
         return(OK);
      }
   }

/*    Note: we have a 31 day month. */

   if((*ol_day) < 31)
   {
      (*ol_day)++;
      return(OK);
   }

/*    Note: we have a 31 day month and the io_day is 31. */

   if((*ol_month) == 12)
   {
      (*ol_day)   = 1;
      (*ol_month) = 1;
      (*ol_year)++;
      return(OK);
   }
   else
   {
      (*ol_day) = 1;
      (*ol_month)++;
      return(OK);
   }

/* Note: we should never reach this statement! */
   return(FATAL);
} /* end of increment_date() */

int increment_454(date_struct* io_454_date)
{

   char* function = "increment_454";

   long ll_no_weeks;

   EXEC SQL DECLARE c_get_weeks CURSOR FOR
      SELECT no_of_weeks
        FROM calendar
     WHERE TO_NUMBER(TO_DATE(first_day, 'YYYYMMDD')) <= :io_454_date->l_date
    ORDER BY first_day DESC;

   EXEC SQL OPEN c_get_weeks;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Opening c_get_weeks");
      strcpy(table, "calendar");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   EXEC SQL FETCH c_get_weeks INTO :ll_no_weeks;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Fetching c_get_weeks");
      strcpy(table, "calendar");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   EXEC SQL CLOSE c_get_weeks;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Closing c_get_weeks");
      strcpy(table, "calendar");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   /* Increment day */
   io_454_date->l_day++;
   if(io_454_date->l_day == 8)
      io_454_date->l_day = 1;

   /* Increment week */
   if(io_454_date->l_day  == 1)
      io_454_date->l_week++;
   if(io_454_date->l_week > ll_no_weeks)
      io_454_date->l_week = 1;

   /* Increment month */
   if(io_454_date->l_day   == 1 &&
      io_454_date->l_week  == 1)
      io_454_date->l_month++;
   if(io_454_date->l_month == 13)
      io_454_date->l_month = 1;

   /* Increment yweek */
   if(io_454_date->l_day   == 1)
      io_454_date->l_yweek++;
   if(io_454_date->l_month == 1 &&
      io_454_date->l_day   == 1 &&
      io_454_date->l_week  == 1)
      io_454_date->l_yweek = 1;

   /* Increment quarter */
   if(io_454_date->l_day           == 1 &&
      io_454_date->l_week          == 1 &&
      ((io_454_date->l_month-1) % 3) == 0)
      io_454_date->l_quarter++;
   if(io_454_date->l_quarter       == 5)
      io_454_date->l_quarter = 1;

   /* Increment half */
   if(io_454_date->l_day           == 1 &&
      io_454_date->l_week          == 1 &&
      ((io_454_date->l_month-1) % 6) == 0)
      io_454_date->l_half++;
   if(io_454_date->l_half          == 3)
      io_454_date->l_half = 1;

   /* Increment year */
   if(io_454_date->l_day      == 1 &&
      io_454_date->l_yweek    == 1)
      io_454_date->l_year++;

   return(OK);
} /* end of increment_454() */

int init_454(date_struct* io_454_date)
{

   char* function = "init_454";

   long  ll_start_454_year;
   long  ll_start_of_falf_month;

/*    Remarks: if the system_options.start_of_half_month is negative, */
/* 		     we need to choose the largest year for starting */
/* 		     454 year that appears in the 12 month window */
/* 		     beginning in the month the */
/* 		     system_options.start_of_half_month indicated */

/* 	    otherwise we need to choose the smallest year value for */
/* 	             starting 454 year that appears in the 12 month */
/*                      window beginning in the month the */
/* 		     system_options.start_of_half_month indicated */

   EXEC SQL DECLARE c_get_first_454_year CURSOR FOR
      SELECT c.year_454,
             s.start_of_half_month
        FROM calendar c,
             system_options s
       WHERE ABS(s.start_of_half_month) = c.month_454
    ORDER BY first_day;

   EXEC SQL OPEN c_get_first_454_year;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Opening c_get_first_454_year");
      strcpy(table, "calendar, system_options");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   EXEC SQL FETCH c_get_first_454_year INTO :ll_start_454_year,
                                            :ll_start_of_falf_month;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Fetching c_get_first_454_year");
      strcpy(table, "calendar, system_options");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   EXEC SQL CLOSE c_get_first_454_year;
   if (SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Closing c_get_first_454_year");
      strcpy(table, "calendar, system_options");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   io_454_date->l_day      = 0;
   io_454_date->l_week     = 0;
   io_454_date->l_yweek    = 0;
   io_454_date->l_month    = 0;
   io_454_date->l_quarter  = 0;
   io_454_date->l_half     = 0;

   /* NOTE: initial 454 date incrementation will increment all 454 date values by 1 */
   if(ll_start_of_falf_month < 0)
      io_454_date->l_year     = ll_start_454_year;
   else
      io_454_date->l_year     = ll_start_454_year-1;

   if(increment_454(io_454_date) < 0)
   {
      sprintf(err_data, "Initial 454 date increment error.");
      WRITE_ERROR(RET_FUNCTION_ERR,function,"",err_data);
      return(FATAL);
   }

   return(OK);

} /* end of init_454() */
