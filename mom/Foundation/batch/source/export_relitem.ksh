#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  export_relitem.ksh
#
#  Desc:  UNIX shell script to extract the item_master records.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='export_relitem.ksh'
pgmName=${pgmName##*/}               # get the file name
pgmExt=${pgmName##*.}                # get the extenstion
pgmName=${pgmName%.*}                # remove the file extenstion
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
DIR=`pwd`

OK=0
FATAL=255
#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
echo "<connect string> is a fully qualified connection string in format:  Username/password@db. Use "'$UP'" if using Oracle Wallet.

     <mode> is the mode of extraction. full or delta

     <thread indicator> indicates if user will provide a thread number. either Y or N.

     <# of parallel threads> is the number of threads to run in parallel if thread indicator is Y. It should be a number between 1-20, optional

     <store> is a specific store number to create a location level file for. This is optional and only valid for a full extract"

}
#-------------------------------------------------------------------------
# Function: UPDATE_RELITEM_EXP_STG
# Purpose:  Updating the relitem_export_stg with process id.
#-------------------------------------------------------------------------
function UPDATE_RELITEM_EXP_STG
{
process_id=$1
echo "Updating the relitem_export_stg with process_id: $process_id" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

   VARIABLE GV_process_id NUMBER;
   VARIABLE GV_script_error CHAR(255);
   VARIABLE GV_return_code NUMBER;
   EXEC :GV_process_id := $process_id;
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;

   update relitem_export_stg stg
      set process_id = trim(:GV_process_id)
    where stg.base_extracted_ind = 'N'
      and stg.process_id is null
      and exists (select 'Y' from item_master im,item_export_info iei where im.item = iei.item and im.item = stg.item and im.status = 'A' and iei.base_extracted_ind = 'Y')
      and stg.action_type in ('relitemheadcre','relitemheadmod','relitemheaddel');

   update relitem_export_stg stg
      set process_id = trim(:GV_process_id)
    where stg.base_extracted_ind = 'N'
      and stg.action_type = 'relitemheaddel'
      and exists (select 'Y' from item_export_stg ies where ies.item = stg.item and ies.action_type = 'itemhdrdel')
      and process_id is null;

   update relitem_export_stg stg
      set process_id = trim(:GV_process_id)
    where stg.base_extracted_ind = 'N'
      and stg.process_id is null
      and stg.action_type in ('relitemdetcre','relitemdetmod')
      and exists (select 'Y' from related_item_detail rd,
                                  item_master im,
                                  item_export_info iei 
                            where stg.relationship_id = rd.relationship_id
                              and im.item = rd.related_item
                              and iei.item = im.item
                              and im.status = 'A'
                              and iei.base_extracted_ind = 'Y');
   
   update relitem_export_stg stg
      set process_id = trim(:GV_process_id)
    where stg.base_extracted_ind = 'N'
      and stg.process_id is null
      and stg.action_type = 'relitemdetdel'
      and exists (select 'Y' 
                    from item_export_info iei
                   where iei.item = stg.related_item
                     and iei.base_extracted_ind = 'Y'
                     and iei.process_id is not null );

   update relitem_export_stg stg
      set process_id = trim(:GV_process_id)
    where stg.base_extracted_ind = 'N'
      and stg.process_id is null
      and stg.action_type in ('relitemdetdel')
      and exists (select 'Y' from relitem_export_stg stg1
                            where stg1.relationship_id = stg.relationship_id
                              and stg1.action_type = 'relitemheaddel');

   COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
       status=${FATAL}
   else
       status=${OK}
   fi
return ${status}   
}
#-------------------------------------------------------------------------
# Function: UPDATE_SINGLE_STR_STG
# Purpose : Updating the relitem_export_stg with process_id.
#-------------------------------------------------------------------------
function UPDATE_SINGLE_STR_STG
{
process_id=$1
store=$2
echo "Updating the relitem_Export_stg with process_id: $process_id" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

   VARIABLE GV_process_id NUMBER;
   VARIABLE GV_store NUMBER;
   VARIABLE GV_script_error CHAR(255);
   VARIABLE GV_return_code NUMBER;

   EXEC :GV_process_id := $process_id;
   EXEC :GV_store := $store;
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;
   
   WHENEVER SQLERROR EXIT 1;

   update relitem_export_stg stg
      set process_id = trim(:GV_process_id)
    where stg.base_extracted_ind = 'N'
      and stg.process_id is null
      and exists (select 'Y' from item_master im,
                                  item_export_info iei,
                                  item_loc il 
                            where im.item = iei.item 
                              and im.item = stg.item 
                              and im.status = 'A' 
                              and iei.base_extracted_ind = 'Y'
                              and im.item = il.item
                              and il.loc = trim(:GV_store))
      and stg.action_type in ('relitemheadcre','relitemheadmod','relitemheaddel');

   update relitem_export_stg stg
      set process_id = trim(:GV_process_id)
    where stg.base_extracted_ind = 'N'
      and stg.action_type = 'relitemheaddel'
      and exists (select 'Y' from item_export_stg ies
                            where ies.item = stg.item 
                              and ies.action_type = 'itemlocdel'
                              and ies.loc = trim(:GV_store))
      and process_id is null;   
   
   update relitem_export_stg stg
      set process_id = trim(:GV_process_id)
    where stg.base_extracted_ind = 'N'
      and stg.process_id is null
      and stg.action_type in ('relitemdetcre','relitemdetmod','relitemdetdel')
      and exists (select 'Y' from related_item_detail rd,
                                  related_item_head rh,
                                  item_master im,
                                  item_master im1,
                                  item_export_info iei,
                                  item_loc il,
                                  item_loc il1
                            where stg.relationship_id = rd.relationship_id
                              and rd.relationship_id = rh.relationship_id
                              and im.item = rd.related_item
                              and im1.item = rh.item
                              and im1.item = il1.item
                              and il.loc = il1.loc
                              and iei.item = im.item
                              and im.item = il.item
                              and il.loc = trim(:GV_store)
                              and im.status = 'A'
                              and iei.base_extracted_ind = 'Y');

   update relitem_export_stg stg
      set process_id = trim(:GV_process_id)
    where stg.base_extracted_ind = 'N'
      and stg.process_id is null
      and stg.action_type in ('relitemdetdel')
      and exists (select 'Y' from item_export_stg ies
                            where ies.item = stg.related_item 
                              and ies.action_type = 'itemlocdel'
                              and ies.loc = trim(:GV_store));

   COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
       status=${FATAL}
   else
       status=${OK}
   fi
return ${status} 
}
#-------------------------------------------------------------------------
# Function: PROCESS_STG
# Purpose:  Updating the exported records from relitem_export_stg.
#-------------------------------------------------------------------------
function PROCESS_STG
{
process_id=$1
echo "Updating the exported records" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

   VARIABLE GV_process_id NUMBER;
   VARIABLE GV_script_error CHAR(255);
   VARIABLE GV_return_code NUMBER;
 
   EXEC :GV_process_id := $process_id;
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;
   
   WHENEVER SQLERROR EXIT 1;
   update relitem_export_stg
      set base_extracted_ind = 'Y'
    where process_id =  trim(:GV_process_id)
      and base_extracted_ind = 'N';
   COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
       status=${FATAL}
   else
       status=${OK}
   fi
return ${status} 
}
#-------------------------------------------------------------------------
# Function: CREATE_STORE_LIST
# Purpose : Crealting a store list.
#-------------------------------------------------------------------------
function CREATE_STORE_LIST
{
echo "Creating a store list" >> $LOGFILE
STORE_LIST=$DIR/storelist_${extractDate}.$$
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} << EOF >$STORE_LIST
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select st.store
     from store st,
          period p
    where p.vdate <= nvl(st.store_close_date,p.vdate)
      and st.store_type in ('C','F')
    order by store;
EOF

   if [[ $? -ne ${OK} ]]; then
       status=${FATAL}
   else
       status=${OK}
   fi
return ${status}  
}
#-------------------------------------------------------------------------
# Function: PROCESS_CORP_FULL_HDR
# Purpose : Extracting full header level records of related item.
#-------------------------------------------------------------------------
function PROCESS_CORP_FULL_HDR
{
echo "Extracting full header level related item records for corporate." >> $LOGFILE
RELITMHDRFULL_CORP=$DIR/relitemhead_${extractDate}_corp_full
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$RELITMHDRFULL_CORP
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

SELECT Family                    ||'|'||
       Type                      ||'|'||
       RelationshipID            ||'|'||
       Item                      ||'|'||
       Location                  ||'|'||
       RelationshipName          ||'|'||
       RelationshipType          ||'|'||
       MandatoryInd
FROM (
      SELECT 'ITEMS'              Family,
             'FULLRELITEMHDR'     Type,
             rh.relationship_id   RelationshipId,
             rh.item              Item,
             'CORPORATE'          Location,
             rh.relationship_name RelationshipName,
             rh.relationship_type RelationshipType,
             rh.mandatory_ind     MandatoryInd
        FROM related_item_head rh,
             item_master im,
             item_export_info iei
       WHERE rh.item = im.item
         AND im.item = iei.item
         AND im.status = 'A'
         AND im.sellable_ind = 'Y'
         AND iei.base_extracted_ind = 'Y'
      ) relitmfulhdrcorp;

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('related items',
                                'full',
                                 USER,
                                 SYSDATE);
EOF

   if [[ `grep "^ORA" ${RELITMHDRFULL_CORP} | wc -l` -gt 0 ]]; then
       status=${FATAL}
       rm ${RELITMHDRFULL_CORP}
   else
      COUNTER=`wc -l ${RELITMHDRFULL_CORP} | awk '{print $1}'`
      status=${OK}
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${RELITMHDRFULL_CORP}" "${RELITMHDRFULL_CORP}"_${COUNTER}.dat
      else
         rm ${RELITMHDRFULL_CORP}
      fi
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_FULL_HDR
# Purpose : Extracting full header level records of related item.
#-------------------------------------------------------------------------
function PROCESS_FULL_HDR
{
store=$1
echo "Extracting full header level related item records for store: $store" >> $LOGFILE
RELITMHDRFULL=$DIR/relitemhead_${extractDate}_${store}_full
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$RELITMHDRFULL
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   VARIABLE GV_store NUMBER;
   EXEC :GV_store := $store;
SELECT Family                    ||'|'||
       Type                      ||'|'||
       RelationshipID            ||'|'||
       Item                      ||'|'||
       Location                  ||'|'||
       RelationshipName          ||'|'||
       RelationshipType          ||'|'||
       MandatoryInd
FROM (
      SELECT distinct 'ITEMS'              Family,
                      'FULLRELITEMHDR'     Type,
                      rh.relationship_id   RelationshipId,
                      rh.item              Item,
                      il.loc               Location,
                      rh.relationship_name RelationshipName,
                      rh.relationship_type RelationshipType,
                      rh.mandatory_ind     MandatoryInd
                 FROM related_item_head rh,
                      related_item_detail rd,
                      item_master im,
                      item_loc il,
                      item_loc il1,
                      item_master im1,
                      item_export_info iei,
                      item_export_info iei1
                WHERE rh.relationship_id = rd.relationship_id
                  AND im.item = rd.related_item
                  AND im.item = il.item
                  AND iei.item = im.item
                  AND il.loc = trim(:GV_store)
                  AND il.loc_type = 'S'
                  AND iei.base_extracted_ind = 'Y'
                  AND im1.item = rh.item
                  AND il1.item = im1.item
                  AND il1.loc = il.loc
                  AND im1.status = 'A'
                  AND im1.sellable_ind = 'Y'
                  AND iei1.item = im1.item
                  AND iei1.base_extracted_ind = 'Y'
      ) relitmfulhdr;
EOF

   if [[ `grep "^ORA-" ${RELITMHDRFULL} | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat ${RELITMHDRFULL} >> $ERRORFILE
      rm ${RELITMHDRFULL}
   else
      COUNTER=`wc -l ${RELITMHDRFULL}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${RELITMHDRFULL}" "${RELITMHDRFULL}"_${COUNTER}.dat
      else
         rm $RELITMHDRFULL
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_CORP_FULL_DTL
# Purpose : Extracting full detail records of related item for corporate.
#-------------------------------------------------------------------------
function PROCESS_CORP_FULL_DTL
{
echo "Extracting full detail level related item records for corporate" >> $LOGFILE
RELITMDTLFULL_CORP=$DIR/relitemdet_${extractDate}_corp_full
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$RELITMDTLFULL_CORP
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

SELECT Family           ||'|'||
       Type             ||'|'||
       RelationshipID   ||'|'||
       RelatedItem      ||'|'||
       Location         ||'|'||
       Priority         ||'|'||
       StartDate        ||'|'||
       EndDate   
FROM (   
      SELECT 'ITEMS'                                        Family
             ,'FULLRELITEMDET'                              Type
             ,rd.relationship_id                            RelationshipID
             ,rd.related_item                               RelatedItem
             ,'CORPORATE'                                   Location
             ,rd.priority                                   Priority
             ,TO_CHAR(rd.start_date,'DD-MON-YYYY')          StartDate
             ,TO_CHAR(rd.end_date,'DD-MON-YYYY')            EndDate
        FROM related_item_detail rd,
             item_master im, 
             item_export_info iei
       WHERE rd.related_item = im.item
         AND im.status ='A'
         AND im.sellable_ind = 'Y'
         AND iei.item = im.item
         AND iei.base_extracted_ind = 'Y'
      ) relitemfulldtlcorp;
   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('related items',
                                'full',
                                 USER,
                                 SYSDATE);
EOF
   if [[ `grep "^ORA-" ${RELITMDTLFULL_CORP} | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat ${RELITMDTLFULL_CORP} >> $ERRORFILE
      rm ${RELITMDTLFULL_CORP}
   else
      COUNTER=`wc -l ${RELITMDTLFULL_CORP}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${RELITMDTLFULL_CORP}" "${RELITMDTLFULL_CORP}"_${COUNTER}.dat
      else
         rm $RELITMDTLFULL_CORP
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_FULL_DTL
# Purpose : Extracting full detail records of related item.
#-------------------------------------------------------------------------
function PROCESS_FULL_DTL
{
store=$1
echo "Extracting full detail level related item records for store: $store" >> $LOGFILE
RELITMDTLFULL=$DIR/relitemdet_${extractDate}_${store}_full
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$RELITMDTLFULL
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   VARIABLE GV_store NUMBER;
   EXEC :GV_store := $store;
SELECT Family           ||'|'||
       Type             ||'|'||
       RelationshipID   ||'|'||
       RelatedItem      ||'|'||
       Location         ||'|'||
       Priority         ||'|'||
       StartDate        ||'|'||
       EndDate   
FROM (   
      SELECT 'ITEMS'                                          Family
             ,'FULLRELITEMDET'                                Type
             ,rd.relationship_id                              RelationshipID
             ,rd.related_item                                 RelatedItem
             ,il.loc                                          Location
             ,rd.priority                                     Priority
             ,TO_CHAR(rd.start_date,'DD-MON-YYYY')            StartDate
             ,TO_CHAR(rd.end_date,'DD-MON-YYYY')              EndDate
        FROM related_item_head rh,
             related_item_detail rd,
             item_master im, 
             item_master im1,
             item_loc il,
             item_loc il1,
             item_export_info iei,
             item_export_info iei1
       WHERE rd.relationship_id = rh.relationship_id
         AND rh.item = im.item
         AND il1.item = im.item
         ANd il1.loc = il.loc
         AND im.status ='A'
         AND im.sellable_ind = 'Y'
         AND iei.item = im.item
         AND iei.base_extracted_ind = 'Y'
         AND rd.related_item = im1.item
         AND im1.item = il.item
         AND il.loc = trim(:GV_store)
         ANd il.loc_type = 'S'
         AND iei1.item = im1.item
         AND iei1.base_extracted_ind = 'Y'
      ) relitemfulldtl;
EOF
   if [[ `grep "^ORA-" ${RELITMDTLFULL} | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat ${RELITMDTLFULL} >> $ERRORFILE
      rm ${RELITMDTLFULL}
   else
      COUNTER=`wc -l ${RELITMDTLFULL}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${RELITMDTLFULL}" "${RELITMDTLFULL}"_${COUNTER}.dat
      else
         rm $RELITMDTLFULL
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_DELTA_HDR
# Purpose : Extracting full delta records.
#-------------------------------------------------------------------------
function PROCESS_DELTA_HDR
{
process_id=${current_process_id}
store=$1
echo "Extracting the delta header item related records for store: $store" >> $LOGFILE
RELITMHDRDELTA=$DIR/relitemhead_${extractDate}_${store}_delta
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} << EOF >$RELITMHDRDELTA
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   VARIABLE GV_store NUMBER;
   VARIABLE GV_process_id NUMBER;
   EXEC :GV_store := $store;
   EXEC :GV_process_id := $process_id;
SELECT Family                    ||'|'||
       Type                      ||'|'||
       RelationshipID            ||'|'||
       Item                      ||'|'||
       Location                  ||'|'||
       RelationshipName          ||'|'||
       RelationshipType          ||'|'||
       MandatoryInd
FROM (
      SELECT 'ITEMS'                    Family,
             UPPER(stg.action_type)     Type,
             stg.relationship_id        RelationshipId,
             stg.item                   Item,
             il.loc                     Location,
             rh.relationship_name       RelationshipName,
             rh.relationship_type       RelationshipType,
             rh.mandatory_ind           MandatoryInd
        FROM relitem_export_stg stg,
             related_item_head rh,
             related_item_detail rd,
             item_loc il,
             item_loc il1
       WHERE stg.relationship_id = rh.relationship_id
         AND stg.relationship_id = rd.relationship_id
         AND stg.item = il.item
         AND rd.related_item = il1.item
         AND il1.loc = il.loc 
         AND il.loc = trim(:GV_store)
         AND stg.action_type in('relitemheadcre','relitemheadmod')
         AND stg.base_extracted_ind = 'N'
         AND stg.process_id = trim(:GV_process_id)
      UNION
      SELECT 'ITEMS'                    Family,
              UPPER(stg.action_type)     Type,
              stg.relationship_id        RelationshipId,
              stg.item                   Item,
              il.loc                     Location,
              rh.relationship_name       RelationshipName,
              rh.relationship_type       RelationshipType,
              rh.mandatory_ind           MandatoryInd
         FROM relitem_export_stg stg,
              related_item_head rh,
              item_loc il
        WHERE stg.relationship_id = rh.relationship_id(+)
          AND stg.item = il.item
          AND il.loc = trim(:GV_store)
          AND stg.action_type = 'relitemheaddel'
          AND stg.base_extracted_ind = 'N'
          AND stg.process_id = trim(:GV_process_id)
      UNION
      SELECT 'ITEMS'                    Family,
              UPPER(stg.action_type)     Type,
              stg.relationship_id        RelationshipId,
              stg.item                   Item,
              stg1.loc                   Location,
              NULL                       RelationshipName,
              NULL                       RelationshipType,
              NULL                       MandatoryInd
         FROM relitem_export_stg stg,
              item_export_stg stg1
        WHERE stg.item = stg1.item
          AND stg1.action_type = 'itemlocdel'
          AND stg1.loc = trim(:GV_store)
          AND stg.action_type = 'relitemheaddel'
          AND stg.base_extracted_ind = 'N'
          AND stg.process_id = trim(:GV_process_id)
      ) relitmhdrdelta;
EOF
   if [[ `grep "^ORA-" ${RELITMHDRDELTA} | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat ${RELITMHDRDELTA} >> $ERRORFILE
      rm ${RELITMHDRDELTA}
   else
      COUNTER=`wc -l ${RELITMHDRDELTA}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${RELITMHDRDELTA}" "${RELITMHDRDELTA}"_${COUNTER}.dat
      else
         rm $RELITMHDRDELTA
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_DELTA_DTL
# Purpose : Extracting delta records.
#-------------------------------------------------------------------------
function PROCESS_DELTA_DTL
{
process_id=${current_process_id}
store=$1
echo "Extracting delta detail item related records for store: $store" >> $LOGFILE
RELITMDTLDELTA=$DIR/relitemdet_${extractDate}_${store}_delta
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} << EOF >$RELITMDTLDELTA
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   VARIABLE GV_store NUMBER;
   VARIABLE GV_process_id NUMBER;
   EXEC :GV_store := $store;
   EXEC :GV_process_id := $process_id;
SELECT Family           ||'|'||
       Type             ||'|'||
       RelationshipID   ||'|'||
       RelatedItem      ||'|'||
       Location         ||'|'||
       Priority         ||'|'||
       StartDate        ||'|'||
       EndDate   
FROM ( 
      SELECT 'ITEMS'                                       Family,
             UPPER(stg.action_type)                        Type,
             stg.relationship_id                           RelationshipID,
             stg.related_item                              RelatedItem,
             il.loc                                        Location,
             rd.priority                                   Priority,
             TO_CHAR(rd.start_date,'DD-MON-YYYY')          StartDate,
             TO_CHAR(rd.end_date,'DD-MON-YYYY')            EndDate
        FROM relitem_export_stg stg,
             related_item_head rh,
             related_item_detail rd,
             item_loc il,
             item_loc il1
       WHERE stg.relationship_id = rh.relationship_id
         AND stg.relationship_id = rd.relationship_id
         AND stg.related_item = il.item
         AND stg.related_item = rd.related_item
         AND rh.item = il1.item
         AND il.loc = il1.loc
         AND il.loc = trim(:GV_store)
         AND stg.base_extracted_ind = 'N'
         AND stg.process_id = trim(:GV_process_id)
         AND stg.action_type in ('relitemdetcre','relitemdetmod')
      UNION
      SELECT 'ITEMS'                 Family,
             UPPER(stg.action_type)  Type,
             stg.relationship_id     RelationshipID,
             stg.related_item        RelatedItem,
             il.loc                  Location,
             NULL                    Priority,
             NULL                    StartDate,
             NULL                    EndDate
        FROM relitem_export_stg stg,
             related_item_head rh,
             item_loc il,
             item_loc il1
       WHERE stg.relationship_id = rh.relationship_id
         AND stg.related_item = il.item
         AND rh.item = il1.item
         AND il.loc = il1.loc
         AND il.loc = trim(:GV_store)
         AND stg.base_extracted_ind = 'N'
         AND stg.process_id = trim(:GV_process_id)
         AND stg.action_type in ('relitemdetdel')
      UNION
      SELECT 'ITEMS'                 Family,
             UPPER(stg.action_type)  Type,
             stg.relationship_id     RelationshipID,
             stg.related_item        RelatedItem,
             il.loc                  Location,
             NULL                    Priority,
             NULL                    StartDate,
             NULL                    EndDate
        FROM relitem_export_stg stg,
             relitem_export_stg stg1,
             item_loc il,
             item_loc il1
       WHERE stg.relationship_id = stg1.relationship_id
         AND stg.related_item = il.item
         AND stg1.item = il1.item
         AND il.loc = il1.loc
         AND stg1.action_type = 'relitemheaddel'
         AND il.loc = trim(:GV_store)
         AND stg.base_extracted_ind = 'N'
         AND stg.process_id = trim(:GV_process_id)
         AND stg.action_type in ('relitemdetdel')
      UNION
     SELECT 'ITEMS'                 Family,
             UPPER(stg.action_type)  Type,
             stg.relationship_id     RelationshipID,
             stg.related_item        RelatedItem,
             stg2.loc                  Location,
             NULL                    Priority,
             NULL                    StartDate,
             NULL                    EndDate
        FROM relitem_export_stg stg,
             relitem_export_stg stg1,
             item_Export_stg stg2
       WHERE stg.relationship_id = stg1.relationship_id
         AND stg1.item = stg2.item
         AND stg1.action_type = 'relitemheaddel'
         AND stg2.action_type = 'itemlocdel'
         AND stg2.loc = trim(:GV_store)
         AND stg.base_extracted_ind = 'N'
         AND stg.process_id = trim(:GV_process_id)
         AND stg.action_type in ('relitemdetdel')
      ) relitmdtldelta;
EOF
   if [[ `grep "^ORA-" ${RELITMDTLDELTA} | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat ${RELITMDTLDELTA} >> $ERRORFILE
      rm ${RELITMDTLDELTA}
   else
      COUNTER=`wc -l ${RELITMDTLDELTA}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${RELITMDTLDELTA}" "${RELITMDTLDELTA}"_${COUNTER}.dat
      else
         rm $RELITMDTLDELTA
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_CORP_DELTA_HDR
# Purpose : Extracting the delta header level related item for corporate.
#-------------------------------------------------------------------------
function PROCESS_CORP_DELTA_HDR
{
process_id=${current_process_id}
echo "Extracting delta header level related item records for corporate." >> $LOGFILE
RELITMHDRDELTA_CORP=$DIR/relitemhead_${extractDate}_corp_delta
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} << EOF >$RELITMHDRDELTA_CORP
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0
   VARIABLE GV_process_id NUMBER;
   EXEC :GV_process_id := $process_id;

SELECT Family                    ||'|'||
       Type                      ||'|'||
       RelationshipID            ||'|'||
       Item                      ||'|'||
       Location                  ||'|'||
       RelationshipName          ||'|'||
       RelationshipType          ||'|'||
       MandatoryInd
FROM (
      SELECT 'ITEMS'                 Family,
             UPPER(stg.action_type)  Type,
             stg.relationship_id     RelationshipId,
             stg.item                Item,
             'CORPORATE'             Location,
             rh.relationship_name    RelationshipName,
             rh.relationship_type    RelationshipType,
             rh.mandatory_ind        MandatoryInd,
             stg.seq_no              seq_no
        FROM relitem_export_stg stg,
             related_item_head rh
       WHERE stg.relationship_id = rh.relationship_id(+)
         AND stg.base_extracted_ind = 'N'
         AND stg.process_id = trim(:GV_process_id)
         AND stg.action_type in ('relitemheadcre','relitemheadmod','relitemheaddel')
         order by seq_no
      ) relitmdtlhdrcorp;
   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('related items',
                                'delta',
                                 USER,
                                 SYSDATE);
EOF
   if [[ `grep "^ORA-" ${RELITMHDRDELTA_CORP} | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat ${RELITMHDRDELTA_CORP} >> $ERRORFILE
      rm ${RELITMHDRDELTA_CORP}
   else
      COUNTER=`wc -l ${RELITMHDRDELTA_CORP}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${RELITMHDRDELTA_CORP}" "${RELITMHDRDELTA_CORP}"_${COUNTER}.dat
      else
         rm $RELITMHDRDELTA_CORP
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_CORP_DELTA_DTL
# Purpose : Extracting the delta detail level related item for corporate.
#-------------------------------------------------------------------------
function PROCESS_CORP_DELTA_DTL
{
process_id=${current_process_id}
echo "Extracting delta detail level related item records for corporate." >> $LOGFILE
RELITMDTLDELTA_CORP=$DIR/relitemdet_${extractDate}_corp_delta
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} << EOF >$RELITMDTLDELTA_CORP
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   VARIABLE GV_process_id NUMBER;
   EXEC :GV_process_id := $process_id;

SELECT Family           ||'|'||
       Type             ||'|'||
       RelationshipID   ||'|'||
       RelatedItem      ||'|'||
       Location         ||'|'||
       Priority         ||'|'||
       StartDate        ||'|'||
       EndDate   
FROM ( 
      SELECT 'ITEMS'                                        Family,
             UPPER(stg.action_type)                         Type,
             stg.relationship_id                            RelationshipID,
             stg.related_item                               RelatedItem,
             'CORPORATE'                                    Location,
             rd.priority                                    Priority,
             TO_CHAR(rd.start_date,'DD-MON-YYYY')           StartDate,
             TO_CHAR(rd.end_date,'DD-MON-YYYY')             EndDate,
             stg.seq_no            seq_no
        FROM relitem_export_stg stg,
             related_item_detail rd
       WHERE stg.relationship_id = rd.relationship_id(+)
         AND stg.related_item = rd.related_item (+)
         AND stg.base_extracted_ind = 'N'
         AND stg.process_id = trim(:GV_process_id)
         AND stg.action_type in ('relitemdetcre','relitemdetmod','relitemdetdel')
         order by seq_no
      ) relitmdtlhdrcorp;
   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('related items',
                                'delta',
                                 USER,
                                 SYSDATE);
EOF
   if [[ `grep "^ORA-" ${RELITMDTLDELTA_CORP} | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat ${RELITMDTLDELTA_CORP} >> $ERRORFILE
      rm ${RELITMDTLDELTA_CORP}
   else
      COUNTER=`wc -l ${RELITMDTLDELTA_CORP}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${RELITMDTLDELTA_CORP}" "${RELITMDTLDELTA_CORP}"_${COUNTER}.dat
      else
         rm $RELITMDTLDELTA_CORP
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
#               MAIN
#-------------------------------------------------------------------------
if [[ $# -lt 1 || $# -gt 5 || $# -lt 3 ]]; then
    USAGE
    exit 1
fi

CONNECT=$1
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${CONNECT}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${con_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "export_relitem.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#Getting process_id
current_process_id=`${ORACLE_HOME}/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select export_process_id_seq.nextval
  from dual;
exit;
EOF`
process_id_status=$?

if [[ ${process_id_status} -ne ${OK} ]]; then
         LOG_ERROR "invalid username/password; logon denied" "GETTING_PROCESS_ID" "${process_id_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
      exit 1
fi

### Check if no. of threads is passed as arg or not, else default.
if [[ $3 = "Y" ]]; then
   SLOTS=$4
   if [[ -z ${SLOTS} ]]; then
     SLOTS=10
   fi
   STR=$5
   echo $SLOTS | egrep '^[0-9]+$'>/dev/null 2>&1
   status=$?
   if [ "$status" -ne "0" ]
   then
     LOG_ERROR "Thread count should be a Positive Number" "NUM_THREADS" "${status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
     exit 1
   fi
   if [[ ${SLOTS} -gt 20 ]]; then
    SLOTS=10
   fi
else
   if [[ $3 = "N" ]]; then
   STR=$4
   SLOTS=10
   else
     LOG_ERROR "Thread number indicator either should be Y or N" "INDICATOR" "$3" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
     exit 1
   fi
fi

if [[ $2 = "full" ]]; then
   if [[ -z ${STR} ]]; then
      UPDATE_RELITEM_EXP_STG ${current_process_id}
      upd_rlit_stg=$?
      if [[ ${upd_rlit_stg} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating relitem_export_stg" "UPDATE_RELITEM_EXP_STG" "${upd_rlit_stg}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      fi
      CREATE_STORE_LIST
      cr_str_lst=$?
      if [[ ${cr_str_lst} -ne ${OK} ]]; then
         LOG_ERROR "Error while creating store list" "CREATE_STORE_LIST" "${cr_str_lst}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      fi   
      while read store
      do
        if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
           (
             PROCESS_FULL_HDR $store 
            ) &
           process_full_hdr_status=$?
           if [[ ${process_full_hdr_status} -ne ${OK} ]]; then
               LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL_HDR" "${process_full_hdr_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
               exit 1
           fi
        else
           while [ `jobs | wc -l` -ge ${SLOTS} ]
           do
              sleep 1
           done
           (
             PROCESS_FULL_HDR $store 
            ) &
           process_full_hdr_status=$?
           if [[ ${process_full_hdr_status} -ne ${OK} ]]; then
               LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL_HDR" "${process_full_hdr_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
               exit 1
           fi
        fi
      done < $STORE_LIST 
    ### Wait for all of the threads to complete
        wait
    ### Extracting records for corporate office
        PROCESS_CORP_FULL_HDR   
        prc_crp_hdr_full=$?
        if [[ ${prc_crp_hdr_full} -ne ${OK} ]]; then
            LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_CORP_FULL_HDR" "${prc_crp_hdr_full}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
            exit 1
        fi
      while read store
      do
        if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
           (  
            PROCESS_FULL_DTL $store 
            ) &
            process_full_dtl_status=$?
            if [[ ${process_full_dtl_status} -ne ${OK} ]]; then
                LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL_DTL" "${process_full_dtl_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                exit 1
            fi
        else
            while [ `jobs | wc -l` -ge ${SLOTS} ]
            do
              sleep 1
            done
           (  
            PROCESS_FULL_DTL $store 
            ) &
            process_full_dtl_status=$?
            if [[ ${process_full_dtl_status} -ne ${OK} ]]; then
                LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL_DTL" "${process_full_dtl_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                exit 1
            fi
        fi
      done < $STORE_LIST 
    ### Wait for all of the threads to complete
        wait
    ### Extracting records for corporate office
        PROCESS_CORP_FULL_DTL
        prc_crp_dtl_full=$?
        if [[ ${prc_crp_dtl_full} -ne ${OK} ]]; then
            LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_CORP_FULL_DTL" "${prc_crp_dtl_full}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
            exit 1
        fi
        PROCESS_STG ${current_process_id}
        prc_stg=$?
        if [[ ${prc_stg} -ne ${OK} ]]; then
            LOG_ERROR "Job terminated with fatal error.Error recorded in ${PROCESS_STG}" "PROCESS_CORP_FULL_DTL" "${prc_stg}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
            exit 1
        else
           rm -r $STORE_LIST
           LOG_MESSAGE "export_relitem.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
   else
     str=${STR}
     UPDATE_SINGLE_STR_STG ${current_process_id} ${str}
     upd_sin_str_stg=$?
      if [[ ${upd_sin_str_stg} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating relitem_export_stg" "UPDATE_SINGLE_STR_STG" "${upd_sin_str_stg}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      fi   
      PROCESS_FULL_HDR $str
       process_full_hdr_status=$?
         if [[ ${process_full_hdr_status} -ne ${OK} ]]; then
             LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL_HDR" "${process_full_hdr_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
         fi
      PROCESS_CORP_FULL_HDR   
        prc_crp_hdr_full=$?
        if [[ ${prc_crp_hdr_full} -ne ${OK} ]]; then
            LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_CORP_FULL_HDR" "${prc_crp_hdr_full}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
            exit 1
        fi
      PROCESS_FULL_DTL $str
       process_full_dtl_status=$?
         if [[ ${process_full_dtl_status} -ne ${OK} ]]; then
             LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL_DTL" "${process_full_dtl_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
         fi
      PROCESS_CORP_FULL_DTL
        prc_crp_dtl_full=$?
        if [[ ${prc_crp_dtl_full} -ne ${OK} ]]; then
            LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_CORP_FULL_DTL" "${prc_crp_dtl_full}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
            exit 1
        fi
      PROCESS_STG ${current_process_id}
        prc_stg=$?
        if [[ ${prc_stg} -ne ${OK} ]]; then
            LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" "${prc_stg}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
            exit 1
        else
           LOG_MESSAGE "export_relitem.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
    fi
fi
if [[ $2 = "delta" ]]; then
      UPDATE_RELITEM_EXP_STG ${current_process_id}
      upd_rlit_stg=$?
      if [[ ${upd_rlit_stg} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating relitem_export_stg" "UPDATE_RELITEM_EXP_STG" "${upd_rlit_stg}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      fi   
      CREATE_STORE_LIST
      cr_str_lst=$?
      if [[ ${cr_str_lst} -ne ${OK} ]]; then
         LOG_ERROR "Error while creating store list" "CREATE_STORE_LIST" "${cr_str_lst}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      fi
      while read store
      do
        if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
           (
             PROCESS_DELTA_HDR $store 
            ) &
             process_delta_hdr_status=$?
             if [[ ${process_delta_hdr_status} -ne ${OK} ]]; then
                 LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA_HDR" "${process_delta_hdr_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                 exit 1
             fi
        else
           while [ `jobs | wc -l` -ge ${SLOTS} ]
           do
             sleep 1
           done
            (
             PROCESS_DELTA_HDR $store 
             ) &
             process_delta_hdr_status=$?
             if [[ ${process_delta_hdr_status} -ne ${OK} ]]; then
                 LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA_HDR" "${process_delta_hdr_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                 exit 1
             fi
        fi
      done < $STORE_LIST
    ### Wait for all of the threads to complete
        wait
    ### Extracting records for corporate office
        PROCESS_CORP_DELTA_HDR   
        prc_crp_hdr_delta=$?
        if [[ ${prc_crp_hdr_delta} -ne ${OK} ]]; then
            LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_CORP_DELTA_HDR" "${prc_crp_hdr_delta}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
            exit 1
        fi   
      while read store
      do
        if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
           ( 
             PROCESS_DELTA_DTL $store 
            )&
            process_delta_dtl_status=$?
            if [[ ${process_delta_dtl_status} -ne ${OK} ]]; then
                LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA_DTL" "${process_delta_hdr_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                exit 1
            fi
        else
           while [ `jobs | wc -l` -ge ${SLOTS} ]
           do
             sleep 1
           done
           ( 
             PROCESS_DELTA_DTL $store 
            )&
            process_delta_dtl_status=$?
            if [[ ${process_delta_dtl_status} -ne ${OK} ]]; then
                LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA_DTL" "${process_delta_hdr_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
                exit 1
            fi
        fi
      done < $STORE_LIST
    ### Wait for all of the threads to complete
        wait
    ### Extracting records for corporate office   
        PROCESS_CORP_DELTA_DTL
        prc_crp_dtl_delta=$?
        if [[ ${prc_crp_dtl_delta} -ne ${OK} ]]; then
            LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_CORP_DELTA_DTL" "${prc_crp_dtl_delta}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
            exit 1
        fi
        PROCESS_STG ${current_process_id}
        prc_stg=$?
        if [[ ${prc_stg} -ne ${OK} ]]; then
            LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" "${prc_stg}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
            exit 1
        else
           rm -r $STORE_LIST
           LOG_MESSAGE "export_relitem.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
fi