#! /bin/ksh
#----------------------------------------------------------------------------------------
#  File:  pricingeventprocess.ksh
#
#  Desc:  This shell script uploads pricing events from SVC_PRICING_EVENT_HEAD table
#  to SVC_PRICING_EVENT_TEMP table. Wrapper script for CORESVC_XPRICE_SQL.PROCESS_DETAILS
#
#        The following are done in this script
#        1. Explode the pricing events from SVC_PRICING_EVENT_HEAD into SVC_PRICING_EVENT_TEMP
#        2. Group the records on the SVC_PRICING_EVENT_TEMP table.
#           This will group the records into threads based on location and chunks whose
#           size is defined on the RMS_PLSQL_BATCH_CONFIG table
#        3. Create records on the SVC_PRICING_EVENT_TEMP tables for each chunk ID generated
#        4. Validate the records on the SVC_PRICING_EVENT_TEMP table. A validation failure
#           is a fatal error and will halt processing.
#        5. Get the chunk Ids to be processed and process the events from temporary tables
#------------------------------------------------------------------------------------------
. ${RETAIL_HOME}/oracle/lib/src/rmsksh.lib
pgmName='pricingeventprocess.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

LOGFILE="${RETAIL_HOME}/log/$exeDate.log"
ERRORFILE="${RETAIL_HOME}/error/err.$pgmName.$exeDate"

OK=0
FATAL=255
#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

"
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_result         NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      EXEC :GV_result := 0;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL " ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${FATAL}
   fi

   return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: GET_EVENT_COUNT
# Purpose      : Retrieves the count of records to be processed
#-------------------------------------------------------------------------
function GET_EVENT_COUNT
{
   getEvntCnt=`$ORACLE_HOME/bin/sqlplus -s ${CONNECT}  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select count(*)
     from svc_pricing_event_head
    where process_status = 'N'
      and trunc(effective_date) = get_vdate + 1;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${getEvntCnt} - Error in getting the new record count to be processed." "GET_EVENT_COUNT" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: EXPLODE_EVENT
# Purpose      : Update the status of events to I in SVC_PRICING_EVENT_HEAD
#                and explode the records into SVC_PRICING_EVENT_TEMP.
#-------------------------------------------------------------------------
function EXPLODE_EVENT
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
         O_key             VARCHAR2(255);
         L_vdate           DATE := get_vdate + 1;
      BEGIN
         if NOT CORESVC_XPRICE_SQL.EXPLODE_DETAILS(:GV_script_error, L_vdate) then
            raise FUNCTION_ERROR;
         end if;
      EXCEPTION
         when FUNCTION_ERROR then
         ROLLBACK;
         if SQL_LIB.PARSE_MSG(:GV_script_error,
                              O_key) = FALSE then
            NULL;
         end if;
         :GV_return_code := ${FATAL};
      when OTHERS then
         ROLLBACK;
         :GV_script_error := SQLERRM;
         :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "Function call CORESVC_XPRICE_SQL.EXPLODE_DETAILS failed." "EXPLODE_EVENT" "${FATAL}" "${ERRORFILE}" "${LOGFILE}" "${pgmName}" "${pgmPID}"
      exit ${FATAL}
   else
      LOG_MESSAGE "Pricing Events successfully exploded." "EXPLODE_EVENT" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}

}
#-------------------------------------------------------------------------
# Function Name: GET_EXPLODE_COUNT
# Purpose      : Retrieves the count of records exploded
#-------------------------------------------------------------------------
function GET_EXPLODE_COUNT
{
   getExpCnt=`$ORACLE_HOME/bin/sqlplus -s ${CONNECT}  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select count(*)
     from svc_pricing_event_head
    where process_status = 'X'
      and trunc(effective_date) = get_vdate + 1;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${getExpCnt} - Error in getting the exploded record count." "GET_EXPLODE_COUNT" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: GET_CHUNKS
# Purpose      : create the chunks for temporary table
#-------------------------------------------------------------------------
function GET_CHUNKS
{
   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT CORESVC_XPRICE_SQL.CHUNK_PRICE_EVENT(:GV_script_error) then
            raise FUNCTION_ERROR;
         end if;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "Function call CORESVC_XPRICE_SQL.CHUNK_PRICE_EVENT failed." "GET_CHUNKS" "${FATAL}" "${ERRORFILE}" "${LOGFILE}" "${pgmName}" "${pgmPID}"
      exit ${FATAL}
   else
      LOG_MESSAGE "Chunking successfully created for price event temp table." "GET_CHUNKS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}

}

#-------------------------------------------------------------------------
# Function Name: PROCESS
# Purpose      :
#-------------------------------------------------------------------------
function PROCESS
{
   threadVal=$1

   sqlTxt="
   DECLARE
      cursor C_GET_CHUNKS is
         select distinct
                chunk_id
           from svc_pricing_event_temp
          where trunc(effective_date) = GET_VDATE + 1
            and process_status = 'N'
            and thread_val = ${threadVal};

      TYPE c_recs_type IS TABLE OF C_GET_CHUNKS%ROWTYPE;
      L_get_chunks c_recs_type;
      FUNCTION_ERROR   EXCEPTION;
      L_vdate          DATE := get_vdate + 1;

   BEGIN
      open C_GET_CHUNKS;
      fetch C_GET_CHUNKS BULK COLLECT into L_get_chunks;
      close C_GET_CHUNKS;

      if L_get_chunks.COUNT > 0 then
         FOR i in 1..L_get_chunks.COUNT
         LOOP
            BEGIN
         
                if NOT CORESVC_XPRICE_SQL.PROCESS_DETAILS(:GV_script_error,
                                                          ${threadVal},
                                                          L_get_chunks(i).chunk_id) then
                  raise FUNCTION_ERROR;
                end if;
         
             EXCEPTION
               when FUNCTION_ERROR then
                  ROLLBACK;
                  /*Update the process_status to 'I'*/
                  update svc_pricing_event_temp
                     set process_status = 'E',
                         error_message = sql_lib.parse_msg(:GV_script_error)
                   where thread_val = ${threadVal}
                     and chunk_id   = L_get_chunks(i).chunk_id
                     and trunc(effective_date) = L_vdate;
                  COMMIT;
               when OTHERS then
                  ROLLBACK;
                  :GV_script_error := SQLERRM;
         
                   /*Update the process_status to 'I'*/
                  update svc_pricing_event_temp
                     set process_status = 'E',
                         error_message = sql_lib.parse_msg(:GV_script_error)
                   where thread_val = ${threadVal}
                     and chunk_id   = L_get_chunks(i).chunk_id
                     and trunc(effective_date) = L_vdate;
                  COMMIT;
            END;
         END LOOP;
      end if;
   END;"

      EXEC_SQL "${sqlTxt}"
      if [[ $? -ne ${OK} ]]; then
         LOG_ERROR "Function call CORESVC_XPRICE_SQL.PROCESS_DETAILS failed." "PROCESS" "${FATAL}" "${ERRORFILE}" "${LOGFILE}" "${pgmName}" "${pgmPID}"
         exit ${FATAL}
      else
         LOG_MESSAGE "CORESVC_XPRICE_SQL.PROCESS_DETAILS function completed successfully." "PROCESS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      fi
return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: POST_PROCESS
# Purpose      :
#-------------------------------------------------------------------------
function POST_PROCESS
{
  sqlTxt="
   BEGIN
      update svc_pricing_event_head sph
         set process_status = 'E'
       where exists (select 'x'
                       from svc_pricing_event_temp spt
                      where spt.process_id = sph.process_id
                        and process_status = 'E'
                        and trunc(effective_date) = get_vdate + 1)
         and process_status = 'X'
         and trunc(effective_date) = get_vdate + 1;

      update svc_pricing_event_head sph
         set process_status = 'P'
       where exists (select 'x'
                       from svc_pricing_event_temp spt
                      where spt.process_id = sph.process_id
                        and process_status in ('P','S')
                        and trunc(effective_date) = get_vdate + 1)
         and process_status = 'X'
         and trunc(effective_date) = get_vdate + 1;
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
   END;"

   EXEC_SQL "${sqlTxt}"
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "Error while updating the pricing header table" "POST_PROCESS" "${FATAL}" "${ERRORFILE}" "${LOGFILE}" "${pgmName}" "${pgmPID}"
      exit ${FATAL}
   else
      LOG_MESSAGE "Pricing header table updated successfully." "POST_PROCESS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   getErrorInfo=`$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF
   set pages 0 feedback off;
   set linesize 2000
   whenever sqlerror exit -1
   select 'Price change of '||item||' at location '||location||' could not be processed because '||error_message
     from svc_pricing_event_temp
    where trunc(effective_date) = get_vdate + 1
      and process_status = 'E';
   exit;
EOF`

   echo "${getErrorInfo}" >> ${ERRORFILE}

return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: GET_MAX_THREADS
# Purpose      : Retrieves the maximum number of concurrent threads
#                that can be run for this batch.
#-------------------------------------------------------------------------
function GET_MAX_THREADS
{
   parallelThreads=`$ORACLE_HOME/bin/sqlplus -s ${CONNECT}  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select max_concurrent_threads
     from rms_plsql_batch_config
    where program_name = 'CORESVC_XPRICE_SQL';
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${parallelThreads} - Error in getting the maximum thread." "GET_MAX_THREADS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned number of parallel threads
   if [ -z ${parallelThreads} ]; then
      LOG_ERROR "Unable to retrieve the number of threads from the RMS_PLSQL_BATCH_CONFIG table." "GET_MAX_THREADS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      LOG_MESSAGE "The number of concurrent threads for processing is ${parallelThreads}." "GET_MAX_THREADS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}
#-----------------------------------------------
# Main program starts
# Parse the command line
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   USAGE
   exit 1
fi

CONNECT=$1
if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "pricingeventprocess.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

GET_EVENT_COUNT
if [[ $? -ne ${OK} ]]; then
   LOG_ERROR "Failed in GET_EVENT_COUNT" "GET_EVENT_COUNT" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
   exit ${FATAL}
fi
# Check the returned number of new records to be processed
if [[ ${getEvntCnt} -eq 0 ]]; then
   LOG_MESSAGE "No new price event records found to be processed" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit ${OK}
else
   LOG_MESSAGE "The number of price events to be processed is ${getEvntCnt}." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

# Call the explode function
EXPLODE_EVENT

if [[ $? -ne ${OK} ]]; then
   LOG_ERROR "Failed in EXPLODE_EVENT" "EXPLODE_EVENT" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
   exit ${FATAL}
fi

GET_EXPLODE_COUNT
if [[ $? -ne ${OK} ]]; then
   LOG_ERROR "Failed in GET_EXPLODE_COUNT" "GET_EXPLODE_COUNT" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
   exit ${FATAL}
fi

# Check the returned number of exploded records
if [[ ${getExpCnt} -eq 0 ]]; then
   LOG_MESSAGE "No records exploded from the header table for processing the price event" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit ${OK}
else
   LOG_MESSAGE "The number of price events exploded is ${getExpCnt}." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

GET_MAX_THREADS
if [[ $? -ne ${OK} ]]; then
   LOG_ERROR "Failed in GET_MAX_THREADS" "GET_MAX_THREADS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
   exit ${FATAL}
fi

i=1
maxThreads=`expr $parallelThreads + 1`

GET_CHUNKS
if [[ $? -ne ${OK} ]]; then
   LOG_ERROR "Failed in GET_CHUNKS" "GET_CHUNKS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
   exit ${FATAL}
fi

while [ $i -lt $maxThreads ]
do
   (
   PROCESS $i;
   )&
   i=`expr $i + 1`
done

# Wait for all threads to complete
wait

# Check for any Oracle errors from the SQLPLUS process
if [ `grep "${pgmPID}: Aborted" $LOGFILE | wc -l` -gt 0 ]
then
   LOG_MESSAGE "Errors encountered. Check the error file." "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   POST_PROCESS
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "Failed in POST_PROCESS" "POST_PROCESS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
   LOG_MESSAGE "Program pricingeventprocess.ksh terminated successfully." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#-----------End of Processing -------------------------------------------------

exit 0
