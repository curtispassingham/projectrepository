#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  itm_indctn_purge.ksh
#
#  Desc:  UNIX shell script to purge data from item induction SVC tables.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='itm_indctn_purge.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind

CURR_THREADS=1
OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.
  "
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: PURGE_SVC_TABLES
# Purpose      : This will call CORESVC_ITEM.PURGE_SVC_ITEM_TABLES who is
#                responsible in filtering all valid process_ids for purging.
#-------------------------------------------------------------------------
function PURGE_SVC_TABLES
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
         L_error_message   VARCHAR2(100);
      BEGIN
         if CORESVC_ITEM.PURGE_SVC_ITEM_TABLES (:GV_script_error) = FALSE then
            raise FUNCTION_ERROR;
         end if;
      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
       LOG_ERROR "CORESVC_ITEM.PURGE_SVC_ITEM_TABLES Failed" "PURGE_SVC_TABLES" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
       return ${FATAL}
   else
      LOG_MESSAGE "CORESVC_ITEM.PURGE_SVC_ITEM_TABLES - Successfully Completed" "PURGE_SVC_ITEM_TABLES" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      return ${OK}
   fi

}


#-----------------------------------------------
# Main program starts
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   USAGE
   exit ${NON_FATAL}
fi

#Validate input parameters
CONNECT=$1

if [[ -n ${CONNECT%/*} ]]; then
   USER=${CONNECT%/*}
else
   USER="default user"
fi

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
   echo $ConnCheck
   LOG_MESSAGE "${ConnCheck}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "itm_indctn_purge.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#--- Invoke Purge Function
PURGE_SVC_TABLES

#--  Check for any Oracle errors from the SQLPLUS process
if [[ `grep "${pgmPID}: Aborted" $LOGFILE | wc -l` -gt 0 ]]; then
   LOG_MESSAGE "Errors encountered. See error file" "" ${FATAL} ${LOGFILE} ${pgmName} ${pgmPID}
   exit 1
else
   LOG_MESSAGE "Program itm_indctn_purge.ksh terminated successfully." "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

exit 0
