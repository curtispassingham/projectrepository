#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  export_stores.ksh
#
#  Desc:  UNIX shell script to extract organizational hierarchy data.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='export_stores.ksh'
pgmName=${pgmName##*/}               # remove the path
pgmExt=${pgmName##*.}                # get the extension
pgmName=${pgmName%.*}                # get the program name
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the execution date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
DIR=`pwd`

OK=0
FATAL=255

#--------------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#--------------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <mode>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.
   
   <mode> mode of extraction. full or delta"
}

#--------------------------------------------------------------------------------
# Function Name: UPDATE_STORE_EXPORT_STG
# Purpose:       Initiate records to be extracted from the store_export_stg
#                table. Set process_id for those with base_extracted_ind = 'N'.
#--------------------------------------------------------------------------------

function UPDATE_STORE_EXPORT_STG
{
process_id=$1
echo "Updating store_export_stg with process id: $process_id" >> $LOGFILE

   sqlReturn=`echo "set feedback off;
    set heading off;
    set term off;
    set verify off;
    set serveroutput on size 1000000;

    VARIABLE GV_return_code    NUMBER;
    VARIABLE GV_script_error   CHAR(255);
    VARIABLE GV_exportprocess_id NUMBER;

    EXEC :GV_return_code  := 0;
    EXEC :GV_script_error := NULL;
    EXEC :GV_exportprocess_id := $process_id;

    WHENEVER SQLERROR EXIT ${FATAL}
    update store_export_stg
       set process_id = trim(:GV_exportprocess_id)
     where base_extracted_ind = 'N';

    COMMIT;
    /

    print :GV_script_error;
    exit  :GV_return_code;
   "  | sqlplus -s ${CONNECT} `

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "UPDATE_STORE_EXPORT_STG" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName} ${pgmPID}
      status=${FATAL}
   else
      status=${OK}
   fi

   return ${status}
}

#-------------------------------------------------------------------------
# Function Name: PROCESS_FULL_STORE
# Purpose      : Export full store records
#-------------------------------------------------------------------------

function PROCESS_FULL_STORE
{
   echo "Extracting full store records" >> $LOGFILE

    # Set filename to contain the stores
   STORES=$DIR/store_${extractDate}_full

    # Get the stores to be processed
   $ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >$STORES
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   select 'STORES'                                            || '|' ||
          'FULL'                                              || '|' ||
          s.store                                             || '|' ||
          REPLACE(s.store_name,CHR(10),'')                                        || '|' ||
          s.store_name10                                      || '|' ||
          s.store_name3                                       || '|' ||
          REPLACE(s.store_name_secondary,CHR(10),'')                              || '|' ||
          s.store_class                                       || '|' ||
          cd.code_desc                                        || '|' ||
          s.store_mgr_name                                    || '|' ||
          TO_CHAR(s.store_open_date, 'YYYYMMDD')              || '|' ||
          TO_CHAR(s.store_close_date,'YYYYMMDD')              || '|' ||
          TO_CHAR(s.acquired_date,'YYYYMMDD')                 || '|' ||
          TO_CHAR(s.remodel_date, 'YYYYMMDD')                 || '|' ||
          s.fax_number                                        || '|' ||
          s.phone_number                                      || '|' ||
          s.email                                             || '|' ||
          s.total_square_ft                                   || '|' ||
          s.selling_square_ft                                 || '|' ||
          s.linear_distance                                   || '|' ||
          decode(vr.vat_calc_type,'E',null,s.vat_region)      || '|' ||
          s.vat_include_ind                                   || '|' ||
          s.stockholding_ind                                  || '|' ||
          s.channel_id                                        || '|' ||
          c.channel_name                                      || '|' ||
          s.store_format                                      || '|' ||
          sf.format_name                                      || '|' ||
          REPLACE(s.mall_name,CHR(10),'')                                         || '|' ||
          s.district                                          || '|' ||
          s.transfer_zone                                     || '|' ||
          s.default_wh                                        || '|' ||
          s.stop_order_days                                   || '|' ||
          s.start_order_days                                  || '|' ||
          s.currency_code                                     || '|' ||
          l.iso_code                                          || '|' ||
          s.tran_no_generated                                 || '|' ||
          s.integrated_pos_ind                                || '|' ||
          s.duns_number                                       || '|' ||
          s.duns_loc                                          || '|' ||
          s.sister_store                                      || '|' ||
          s.tsf_entity_id                                     || '|' ||
          s.org_unit_id                                       || '|' ||
          s.auto_rcv                                          || '|' ||
          s.remerch_ind                                       || '|' ||
          s.store_type                                        || '|' ||
          s.wf_customer_id                                    || '|' ||
          s.timezone_name                                     || '|' ||
          s.customer_order_loc_ind
     from store s,
          code_detail cd,
          channels c,
          store_format sf,
          lang l,
          vat_region vr
    where cd.code_type = 'CSTR'
      and cd.code = s.store_class
      and s.channel_id = c.channel_id (+)
      and s.store_format = sf.store_format (+)
      and s.lang = l.lang
      and s.vat_region = vr.vat_region(+)
    order by s.store;

EOF
   err=$?

   if [[ `grep "^ORA-" $STORES | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $STORES >> $ERRORFILE
      rm $STORES
   else
      COUNTER=`wc -l ${STORES}| awk '{print $1}'`
	  if [[ ${COUNTER} -gt 0 ]]; then
         mv "${STORES}" "${STORES}"_${COUNTER}.dat
	  else
	     rm ${STORES}
	  fi
      status=${OK}
   fi

   return ${status}

}

#-------------------------------------------------------------------------
# Function Name: PROCESS_FULL_STOREADDR
# Purpose      : Export full store detail (addresses) records
#-------------------------------------------------------------------------

function PROCESS_FULL_STOREADDR
{
   echo "Extracting full store detail records" >> $LOGFILE

    # Set filename to contain the stores
   STOREADDR=$DIR/storeaddr_${extractDate}_full

    # Get the stores to be processed
   $ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >$STOREADDR
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   select 'STORES'              || '|' ||
          'FULL'                || '|' ||
          a.key_value_1         || '|' ||
          a.addr_type           || '|' ||
          vatt.type_desc        || '|' ||
          a.primary_addr_ind    || '|' ||
          a.add_1               || '|' ||
          a.add_2               || '|' ||
          a.add_3               || '|' ||
          a.city                || '|' ||
          a.county              || '|' ||
          a.state               || '|' ||
          a.country_id          || '|' ||
          a.post                || '|' ||
          a.jurisdiction_code   || '|' ||
          a.contact_name        || '|' ||
          a.contact_phone       || '|' ||
          a.contact_fax         || '|' ||
          a.contact_email
     from addr a,
          v_add_type_tl vatt
    where a.module in ('ST', 'WFST')
      and a.addr_type = vatt.address_type
    order by a.key_value_1, addr_type;

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                         values('stores',
                                'full',
                                 USER,
                                 SYSDATE);

EOF
   err=$?

   if [[ `grep "^ORA-" $STOREADDR | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $STOREADDR >> $ERRORFILE
      rm $STOREADDR
   else
      COUNTER=`wc -l ${STOREADDR}| awk '{print $1}'`
	  if [[ ${COUNTER} -gt 0 ]]; then
	     mv "${STOREADDR}" "${STOREADDR}"_${COUNTER}.dat
      else
	     rm ${STOREADDR}
	  fi
	  status=${OK}
   fi

   return ${status}
}

#-------------------------------------------------------------------------
# Function Name: PROCESS_DELTA_STORE
# Purpose      : Export the records,which are not exported last time
#-------------------------------------------------------------------------

function PROCESS_DELTA_STORE
{
   Process_id=$1
   echo "Extracting delta store records." >> $LOGFILE

    # Set filename to contain this organizational hierarchy
   STORES=$DIR/store_${extractDate}_delta

    # Create the flatfile
   $ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >$STORES
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   VARIABLE GV_exportprocess_id NUMBER;

   EXEC :GV_exportprocess_id := $Process_id;

   select 'STORES'                    || '|' ||
          UPPER(ses.action_type)      || '|' ||
          ses.store                   || '|' ||
          inner.storename             || '|' ||
          inner.storename10           || '|' ||
          inner.storenameabbr         || '|' ||
          inner.storenamesec          || '|' ||
          inner.storeclassid          || '|' ||
          inner.storeclassname        || '|' ||
          inner.manager               || '|' ||
          inner.opendate              || '|' ||
          inner.closedate             || '|' ||
          inner.acquiredate           || '|' ||
          inner.remodeldate           || '|' ||
          inner.faxnumber             || '|' ||
          inner.phonenumber           || '|' ||
          inner.email                 || '|' ||
          inner.totalsqfeet           || '|' ||
          inner.sellingsqfeet         || '|' ||
          inner.lineardistance        || '|' ||
          inner.vatregion             || '|' ||
          inner.vatinclind            || '|' ||
          inner.stockholdingind       || '|' ||
          inner.channelid             || '|' ||
          inner.channelname           || '|' ||
          inner.storeformat           || '|' ||
          inner.storeformatname       || '|' ||
          inner.mallname              || '|' ||
          inner.district              || '|' ||
          inner.transferzone          || '|' ||
          inner.defaultwh             || '|' ||
          inner.stoporderdays         || '|' ||
          inner.startorderdays        || '|' ||
          inner.currencycode          || '|' ||
          inner.storelangisocode      || '|' ||
          inner.trannogenerate        || '|' ||
          inner.intposind             || '|' ||
          inner.dunsnumber            || '|' ||
          inner.dunsloc               || '|' ||
          inner.sisterstore           || '|' ||
          inner.tsfentityid           || '|' ||
          inner.orgunitid             || '|' ||
          inner.autorcv               || '|' ||
          inner.remerchind            || '|' ||
          inner.storetype             || '|' ||
          inner.wfcustomer            || '|' ||
          inner.timezone              || '|' ||
          inner.customerorderlocind
     from store_export_stg ses,
          (select s.store                                           as storeid,
                  REPLACE(s.store_name,CHR(10),'')                                      as storename,
                  s.store_name10                                    as storename10,
                  s.store_name3                                     as storenameabbr,
                  REPLACE(s.store_name_secondary,CHR(10),'')                            as storenamesec,
                  s.store_class                                     as storeclassid,
                  cd.code_desc                                      as storeclassname,
                  s.store_mgr_name                                  as manager,
                  TO_CHAR(s.store_open_date, 'YYYYMMDD')            as opendate,
                  TO_CHAR(s.store_close_date,'YYYYMMDD')            as closedate,
                  TO_CHAR(s.acquired_date,'YYYYMMDD')               as acquiredate,
                  TO_CHAR(s.remodel_date, 'YYYYMMDD')               as remodeldate,
                  s.fax_number                                      as faxnumber,
                  s.phone_number                                    as phonenumber,
                  s.email                                           as email,
                  s.total_square_ft                                 as totalsqfeet,
                  s.selling_square_ft                               as sellingsqfeet,
                  s.linear_distance                                 as lineardistance,
                  decode(vr.vat_calc_type,'E',null,s.vat_region)    as vatregion,
                  s.vat_include_ind                                 as vatinclind,
                  s.stockholding_ind                                as stockholdingind,
                  s.channel_id                                      as channelid,
                  c.channel_name                                    as channelname,
                  s.store_format                                    as storeformat,
                  sf.format_name                                    as storeformatname,
                  REPLACE(s.mall_name,CHR(10),'')                                      as mallname,
                  s.district                                        as district,
                  s.transfer_zone                                   as transferzone,
                  s.default_wh                                      as defaultwh,
                  s.stop_order_days                                 as stoporderdays,
                  s.start_order_days                                as startorderdays,
                  s.currency_code                                   as currencycode,
                  l.iso_code                                        as storelangisocode,
                  s.tran_no_generated                               as trannogenerate,
                  s.integrated_pos_ind                              as intposind,
                  s.duns_number                                     as dunsnumber,
                  s.duns_loc                                        as dunsloc,
                  s.sister_store                                    as sisterstore,
                  s.tsf_entity_id                                   as tsfentityid,
                  s.org_unit_id                                     as orgunitid,
                  s.auto_rcv                                        as autorcv,
                  s.remerch_ind                                     as remerchind,
                  s.store_type                                      as storetype,
                  s.wf_customer_id                                  as wfcustomer,
                  s.timezone_name                                   as timezone,
                  s.customer_order_loc_ind                          as customerorderlocind
             from store s,
                  code_detail cd,
                  channels c,
                  store_format sf,
                  lang l,
                  vat_region vr
            where cd.code_type = 'CSTR'
              and cd.code(+) = s.store_class
              and s.channel_id = c.channel_id(+)
              and s.store_format = sf.store_format(+)
              and s.lang = l.lang(+)
              and s.vat_region = vr.vat_region(+)) inner
    where ses.action_type in ('storecre', 'storemod', 'storedel')
      and ses.process_id = trim(:GV_exportprocess_id)
      and ses.store = inner.storeid(+)
    order by seq_no;

EOF

   err=$?

   if [[ `grep "^ORA-" $STORES | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $STORES >> $ERRORFILE
      rm $STORES
   else
      COUNTER=`wc -l ${STORES}| awk '{print $1}'`
	  if [[ ${COUNTER} -gt 0 ]]; then
         mv "${STORES}" "${STORES}"_${COUNTER}.dat
      else
	     rm ${STORES}
	  fi
	  status=${OK}
   fi

   return ${status}

}

#-------------------------------------------------------------------------
# Function Name: PROCESS_DELTA_STADDR
# Purpose      : Export the records,which are not exported last time
#-------------------------------------------------------------------------

function PROCESS_DELTA_STADDR
{
   Process_id=$1
   echo "Extracting delta diff group detail records." >> $LOGFILE

    # Set filename to contain this organizational hierarchy
   STOREADDR=$DIR/storeaddr_${extractDate}_delta

    # Create the flatfile
   $ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >$STOREADDR
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   VARIABLE GV_exportprocess_id NUMBER;

   EXEC :GV_exportprocess_id := $Process_id;

   select 'STORES'                 || '|' ||
          UPPER(action_type)       || '|' ||
          ses.store                || '|' ||
          ses.addr_type            || '|' ||
          vatt.type_desc           || '|' ||
          ses.primary_addr_ind     || '|' ||
          a.add_1                  || '|' ||
          a.add_2                  || '|' ||
          a.add_3                  || '|' ||
          a.city                   || '|' ||
          a.county                 || '|' ||
          a.state                  || '|' ||
          a.country_id             || '|' ||
          a.post                   || '|' ||
          a.jurisdiction_code      || '|' ||
          a.contact_name           || '|' ||
          a.contact_phone          || '|' ||
          a.contact_fax            || '|' ||
          a.contact_email
     from store_export_stg ses,
          addr a,
          v_add_type_tl vatt
    where ses.action_type in ('storedtlcre', 'storedtlmod', 'storedtldel')
      and ses.process_id = trim(:GV_exportprocess_id)
      and TO_CHAR(ses.store) = a.key_value_1 (+)
      and ses.addr_key = a.addr_key(+)
      and a.module(+) in ('ST', 'WFST')
      and ses.addr_type = a.addr_type(+)
      and ses.addr_type = vatt.address_type
    order by ses.seq_no;

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                         values('stores',
                                'delta',
                                 USER,
                                 SYSDATE);

EOF
   err=$?

   if [[ `grep "^ORA-" $STOREADDR | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $STOREADDR >> $ERRORFILE
      rm $STOREADDR
   else
      COUNTER=`wc -l ${STOREADDR}| awk '{print $1}'`
	  if [[ ${COUNTER} -gt 0 ]]; then
         mv "${STOREADDR}" "${STOREADDR}"_${COUNTER}.dat
      else
	     rm ${STOREADDR}
	  fi
	  status=${OK}
   fi

   return ${status}
}

#-------------------------------------------------------------------------
# Function Name: PROCESS_STG
# Purpose      : Update the exported records.
#-------------------------------------------------------------------------

function PROCESS_STG
{
process_id=$1
   echo "Updating exported records" >> $LOGFILE
   sqlReturn=`echo "set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   VARIABLE GV_return_code    NUMBER;
   VARIABLE GV_script_error   CHAR(255);
   VARIABLE GV_exportprocess_id NUMBER;

   EXEC :GV_return_code  := 0;
   EXEC :GV_script_error := NULL;
   EXEC :GV_exportprocess_id := $process_id;

   update store_export_stg
      set base_extracted_ind = 'Y'
    where base_extracted_ind = 'N'
      and process_id = trim(:GV_exportprocess_id);

   commit;
   /
   print :GV_script_error
   exit  :GV_return_code
   " | sqlplus -s $CONNECT`


   if [[ $? -ne ${OK} ]]; then
      status=${FATAL}
   else
      status=${OK}
   fi
return ${status}

}
#-----------------------------------------------
# Main program starts
# Parse the command line
#-----------------------------------------------

# Test for the number of input arguments
if [[ $# -lt 1 || $# -gt 2 || $# -lt 2 ]]; then
   USAGE
   exit 1
fi

CONNECT=$1

#Validate that DB connection is valid and available
ConnCheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`
constatus=$?

if [[ ${constatus} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${constatus}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "export_stores.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#Retrieve Process ID
current_process_id=`$ORACLE_HOME/bin/sqlplus -s $CONNECT  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select export_process_id_seq.nextval
  from dual;
exit;
EOF`

process_id_status=$?

#Call Update export table
if [[ ${process_id_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "GETTING_PROCESS_ID" "${process_id_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
    UPDATE_STORE_EXPORT_STG $current_process_id
    st=$?
    if [[ ${st} -ne ${OK} ]]; then
       LOG_ERROR "Error while updating the process_id" "UPDATE_STORE_EXPORT_STG" "${FATAL}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
       exit 1
    fi
fi

#Call data extract
if [[ $2 = "full" ]]; then
    PROCESS_FULL_STORE
    process_fullhd_status=$?
    if [[ ${process_fullhd_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL_STORE" "${process_fullhd_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    fi
    PROCESS_FULL_STOREADDR
    process_fulldt_status=$?
    if [[ ${process_fulldt_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL_STOREADDR" "${process_fulldt_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    fi
    if [[ ${process_fullhd_status} -eq ${OK} && ${process_fulldt_status} -eq ${OK} ]]; then
        PROCESS_STG $current_process_id
        process_stg_status=$?

        if [[ ${process_stg_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" ${process_stg_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
        else
            LOG_MESSAGE "export_stores.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
    fi
elif [[ $2 = "delta" ]]; then
    PROCESS_DELTA_STORE $current_process_id
    process_deltahd_status=$?
    if [[ ${process_deltahd_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA_STORE" ${process_deltahd_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    fi
    PROCESS_DELTA_STADDR $current_process_id
    process_deltadt_status=$?
    if [[ ${process_deltadt_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA_STADDR" ${process_deltadt_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    fi
    if [[ ${process_deltahd_status} -eq ${OK} && ${process_deltadt_status} -eq ${OK} ]]; then
        PROCESS_STG $current_process_id
        process_stg_status=$?
        if [[ ${process_stg_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" ${process_stg_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
        else
            LOG_MESSAGE "export_stores.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
    fi
fi

exit 0