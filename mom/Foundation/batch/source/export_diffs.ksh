#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  export_diffs.ksh
#
#  Desc:  UNIX shell script to extract the differentiator.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='export_diffs.ksh'
pgmName=${pgmName##*/}               # remove the path
pgmExt=${pgmName##*.}                # get the extension
pgmName=${pgmName%.*}                # get the program name
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
DIR=`pwd`

OK=0
FATAL=255

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <mode>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.
   
   <mode> mode of extraction. full or delta"
}
#-------------------------------------------------------------------------
# Function Name: UPDATE_DIFFS_EXPORT_STG
# Purpose      : update diffs_export_stg with process_id for export
#-------------------------------------------------------------------------
function UPDATE_DIFFS_EXPORT_STG
{
prcs_id=$1
echo "Updating diffs_export_stg with process id: $prcs_id" >> $LOGFILE
    sqlReturn=`echo "set feedback off
      set heading off
      set term off
      set verify off
      set serveroutput on size 1000000

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);
      VARIABLE GV_exportprocess_id NUMBER;

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;
      EXEC :GV_exportprocess_id := $prcs_id;

      WHENEVER SQLERROR EXIT 1
      update diffs_export_stg
         set process_id = trim(:GV_exportprocess_id)
       where base_extracted_ind = 'N'
         and process_id is null;

      COMMIT;
      /

      print :GV_script_error;
      exit  :GV_return_code;
     "  | sqlplus -s ${CONNECT}`
   
   if [[ $? -ne ${OK} ]]; then
      status=${FATAL}
   else
      status=${OK}
   fi
   return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_STG
# Purpose      : Update the exported records.
#-------------------------------------------------------------------------
function PROCESS_STG
{
process_id=$1
   echo "Updating exported records" >> $LOGFILE
   sqlReturn=`echo "set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0
   
   VARIABLE GV_return_code    NUMBER;
   VARIABLE GV_script_error   CHAR(255);
   VARIABLE GV_exportprocess_id NUMBER;
   
   EXEC :GV_return_code  := 0;
   EXEC :GV_script_error := NULL;
   EXEC :GV_exportprocess_id := $process_id;
   
   update diffs_export_stg
      set base_extracted_ind = 'Y'
    where base_extracted_ind = 'N'
      and process_id = trim(:GV_exportprocess_id);

   commit;
   /
   print :GV_script_error
   exit  :GV_return_code
   " | sqlplus -s $CONNECT`


   if [[ $? -ne ${OK} ]]; then
      status=${FATAL}
   else
      status=${OK}
   fi
return ${status}

}
#-------------------------------------------------------------------------
# Function Name: PROCESS_FULL
# Purpose      : Export full records
#-------------------------------------------------------------------------
function PROCESS_FULL
{
 echo "Extracting full differentiator records" >> $LOGFILE
 DIFFID=$DIR/diffs_${extractDate}_full
   
   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$DIFFID
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0
   SELECT family          ||'|'|| 
          type            ||'|'|| 
          DiffId          ||'|'|| 
          DiffDesc        ||'|'|| 
          DiffType        ||'|'|| 
          DiffTypeDesc    ||'|'|| 
          IndustryCode    ||'|'|| 
          IndustrySubgroup
   FROM
     (SELECT 'DIFFS'             family,
             'FULL'              type,
             d.diff_id           DiffId,
             d.diff_desc         DiffDesc,
             dt.diff_type        DiffType,
             dt.diff_type_desc   DiffTypeDesc,
             d.industry_code     IndustryCode,
             d.industry_subgroup IndustrySubgroup
     FROM diff_ids d,
          diff_type dt
    WHERE d.diff_type = dt.diff_type) diffs;

       insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('diffs',
                                'full',
                                 USER,
                                 SYSDATE);

EOF
err=$?
   if [[ `grep "^ORA-" $DIFFID | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $DIFFID >> $ERRORFILE
      rm $DIFFID
   else
      COUNTER=`wc -l ${DIFFID}| awk '{print $1}'`
      mv "${DIFFID}" "${DIFFID}"_${COUNTER}.dat
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_DELTA
# Purpose      : Export the records,which are not exported last time
#-------------------------------------------------------------------------
function PROCESS_DELTA
{
Process_id=$1
echo "Extracting delta differentiator records." >> $LOGFILE
DIFFID_DELTA=$DIR/diffs_${extractDate}_delta
   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF > $DIFFID_DELTA
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000
   set pages 0
   
   VARIABLE GV_exportprocess_id NUMBER;
   EXEC :GV_exportprocess_id := $Process_id;   
   WHENEVER SQLERROR EXIT 1
   SELECT family          ||'|'|| 
          type            ||'|'|| 
          DiffId          ||'|'|| 
          DiffDesc        ||'|'|| 
          DiffType        ||'|'|| 
          DiffTypeDesc    ||'|'|| 
          IndustryCode    ||'|'|| 
          IndustrySubgroup
   FROM
     (SELECT 'DIFFS'                family,
             upper(stg.action_type) type,
             stg.diff_id            DiffId,
             d.diff_desc            DiffDesc,
             d.diff_type            DiffType,
             (select dt.diff_type_desc 
                from diff_type dt 
               where dt.diff_type = d.diff_type)  DiffTypeDesc,
             d.industry_code     IndustryCode,
             d.industry_subgroup IndustrySubgroup,
             stg.seq_no
        FROM diff_ids d,
             diffs_export_stg stg
       WHERE stg.diff_id  = d.diff_id(+)
         AND stg.process_id = trim(:GV_exportprocess_id)
         AND stg.base_extracted_ind = 'N') diffs order by diffs.seq_no;

   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('diffs',
                                'delta',
                                 USER,
                                 SYSDATE);

EOF
err=$?

if [[ `grep "^ORA-" $DIFFID_DELTA | wc -l` -gt 0 ]]; then
   status=${FATAL}
   cat $DIFFID_DELTA >> $ERRORFILE
   rm $DIFFID_DELTA
else
   COUNTER=`wc -l ${DIFFID_DELTA}| awk '{print $1}'`
   mv "${DIFFID_DELTA}" "${DIFFID_DELTA}"_${COUNTER}.dat
   status=${OK}
fi    

return ${status}

}

#-------------------------------------------------------------------------
#     MAIN
#-------------------------------------------------------------------------
# Validate the number of arguments

if [[ $# -lt 1 || $# -gt 2 || $# -lt 2 ]]; then
   USAGE
   exit 1
fi

CONNECT=$1
conncheck=`echo "exit" | $ORACLE_HOME/bin/sqlplus -s -l ${CONNECT}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${con_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "export_diffs.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

#Getting process_id
current_process_id=`${ORACLE_HOME}/bin/sqlplus -s ${CONNECT}  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select export_process_id_seq.nextval
  from dual;
EOF`
process_id_status=$?

if [[ ${process_id_status} -ne ${OK} ]]; then
    LOG_ERROR "Error while retrieveing the process_id" "GETTING_PROCESS_ID" "${process_id_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
    exit 1
else
    UPDATE_DIFFS_EXPORT_STG $current_process_id
    st=$?
    if [[ ${st} -ne ${OK} ]]; then
       LOG_ERROR "Error while updating the process_id" "UPDATE_DIFFS_EXPORT_STG" "${FATAL}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
       exit 1 
    fi
fi

if [[ $2 = "full" ]]; then
    PROCESS_FULL
    process_full_status=$?
    if [[ ${process_full_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL" "${process_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    else 
        PROCESS_STG $current_process_id
        process_stg_status=$?
        if [[ ${process_stg_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" ${process_stg_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
        else
            LOG_MESSAGE "export_diffs.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
    fi
elif [[ $2 = "delta" ]]; then
    PROCESS_DELTA $current_process_id
    process_delta_status=$?
    if [[ ${process_delta_status} -ne ${OK} ]]; then
        LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA" ${process_delta_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
        exit 1
    else 
        PROCESS_STG $current_process_id
        process_stg_status=$?
        if [[ ${process_stg_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" ${process_stg_status} "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
        else
            LOG_MESSAGE "export_diffs.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
        fi
    fi
fi

exit 0