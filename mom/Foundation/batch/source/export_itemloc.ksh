#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  export_itemloc.ksh
#
#  Desc:  UNIX shell script to extract the itemloc records.
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='export_itemloc.ksh'
pgmName=${pgmName##*/}               # get the file name
pgmExt=${pgmName##*.}                # get the extenstion
pgmName=${pgmName%.*}                # remove the file extenstion
pgmPID=$$                            # get the process ID
exeDate=`date +"%h_%d"`              # get the execution date
extractDate=`date +"%G%m%d%H%M%S"`   # get the extraction date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind
DIR=`pwd`

OK=0
FATAL=255
#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: . $pgmName <connect string> <mode><thread indicator> <# parallel threads> <location>

   <connect string> is a fully qualified connection string in format:   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <mode> is the mode of extraction. full or delta

   <thread indicator> indicates if user will provide a thread number. either Y or N.

   <# of parallel threads> is the number of threads to run in parallel if thread indicator is Y. It should be a number between 1-20, optional

   <location> is a specific location number to create the flat file for. Optional"
}
#-------------------------------------------------------------------------
# Function: UPDATE_ITEM_EXPORT_STG
# Desc: Updating the item_export_stg with current process id
#-------------------------------------------------------------------------
function UPDATE_ITEM_EXPORT_STG
{
process_id=$1
echo "Updating item_export_stg with process id: $process_id" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

  VARIABLE GV_process_id NUMBER;
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_process_id := ${process_id};
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;
   
  update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemhdrmod','itemupccre')
     and process_id is null
     and loc is not null
     and exists (select 'Y' from item_master im where stg.item = im.item and im.status = 'A');

  update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemloccre','itemlocmod')
     and process_id is null
     and exists (select 'Y' from item_master im where stg.item = im.item and im.status = 'A'
     and exists (select 'Y' from item_export_info inf where inf.item = stg.item and inf.base_extracted_ind = 'Y' and inf.process_id is not null));

   update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemlocdel','itemupcdel')
     and process_id is null;

    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_STG
# Desc: Updating the exported item_export_stg records
#-------------------------------------------------------------------------
function PROCESS_STG
{
process_id=$1
echo "Updating exported records" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

  VARIABLE GV_process_id NUMBER;
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_process_id := ${process_id};
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;
   update item_export_stg
     set base_extracted_ind = 'Y'
   where base_extracted_ind = 'N'
     and action_type in ('itemloccre','itemlocmod','itemlocdel','itemupccre','itemhdrmod','itemupcdel')
     and process_id = trim(:GV_process_id);
    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: UPDATE_SINGLE_STORE_ITEM_EXPORT_STG
# Desc: Updating the item_export_stg with current process id for single store
#-------------------------------------------------------------------------
function UPDATE_SINGLE_STORE_ITEM_EXPORT_STG
{
process_id=${current_process_id}
store=$1
echo "Updating item_export_stg with process id for location: $store" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

  VARIABLE GV_process_id NUMBER;
  VARIABLE GV_store CHAR(255);
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_process_id := ${process_id};
   EXEC :GV_store      := '$store';
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;
    update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemhdrmod')
     and process_id is null
     and loc is not null
     and exists (select 'Y' from item_master im where stg.item = im.item and im.status = 'A')
     and exists (select 'Y' from item_loc il where il.item = stg.item and trim(il.loc) = trim(:GV_store));

  update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemupccre')
     and item_parent is not null
     and process_id is null
     and loc is not null
     and trim(stg.loc)=trim(:GV_store)
     and exists (select 'Y' from item_master im where stg.item = im.item and im.status = 'A');
    
   
   update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemupcdel')
     and item_parent is not null
     and process_id is null
     and exists (select 'Y'
                   from item_export_stg stg1
                  where (stg1.item = stg.item_parent or stg1.item = stg.item_grandparent)
                    and trim(stg1.loc) = trim(:GV_store)
                    and stg1.base_extracted_ind = 'N'
                    and stg1.action_type = 'itemlocdel'
                 union
                    select 'Y'
                   from item_loc il
                  where (il.item = stg.item_parent or il.item = stg.item_grandparent)
                    and trim(il.loc) = trim(:GV_store));
                
  
   
   update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemloccre','itemlocmod')
     and process_id is null
     and loc = trim(:GV_store)
     and exists (select 'Y' from item_master im where stg.item = im.item and im.status = 'A')
     and exists (select 'Y' from item_export_info inf where stg.item = inf.item and inf.base_extracted_ind = 'Y' and inf.process_id is not null);

   update item_export_stg stg
     set process_id = trim(:GV_process_id)
   where base_extracted_ind = 'N'
     and action_type in ('itemlocdel')
     and loc = trim(:GV_store)
     and process_id is null;

    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_SINGLE_STORE_STG
# Desc: Updating the exported item_export_stg records for single store
#-------------------------------------------------------------------------
function PROCESS_SINGLE_STORE_STG
{
process_id=${current_process_id}
 echo "Processing exported records for location: $opt_loc" >> $LOGFILE
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000

  VARIABLE GV_process_id NUMBER;
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_process_id := ${process_id};
   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;

   WHENEVER SQLERROR EXIT 1;
   update item_export_stg
     set base_extracted_ind = 'Y'
   where base_extracted_ind = 'N'
     and action_type in ('itemloccre','itemlocmod','itemlocdel','itemupccre','itemhdrmod','itemupcdel')
     and process_id = trim(:GV_process_id);

    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`

if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: CREATE_LOCATION_LIST
# Desc: Creating the location list
#-------------------------------------------------------------------------
function CREATE_LOCATION_LIST
{
echo "Creating the location list" >> $LOGFILE
if [[ $1 = "base" ]]; then
LOC_LIST=$DIR/loclist_${extractDate}.$$
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$LOC_LIST
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
    select to_char(st.store)  loc,
           'S'                loc_type
      from store st,
           period p
     where p.vdate <= nvl(st.store_close_date,p.vdate)
       and st.store_type in ('C','F')
    union all
    select to_char(wh.wh)      loc,
           'W'                 loc_type
      from wh wh
     where wh.wh <> wh.physical_wh
    union all
    select p.partner_id        loc,
           p.partner_type      loc_type
      from partner p
     where p.partner_type = 'E';
EOF
fi

if [[ $1 = "stg" ]]; then
process_id=$2
LOC_LIST=$DIR/loclist_${extractDate}.$$
$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$LOC_LIST
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   
   select stg.loc loc,
          stg.loc_type loc_type
     from item_export_stg stg,
          item_master im
    where stg.base_extracted_ind = 'N'
      and stg.action_type in('itemhdrmod','itemupccre')
      and stg.item = im.item
      and stg.loc_type = 'S'
      and im.status = 'A'
   union
   select stg1.loc loc,
          stg1.loc_type loc_type
     from item_export_stg stg,
          item_export_stg stg1
   where stg.base_extracted_ind = 'N'
     and stg.action_type = 'itemhdrdel'
     and stg1.action_type = 'itemlocdel'
     and stg1.base_Extracted_ind = 'N'
     and stg1.loc_type = 'S'
     and stg.item = stg1.item
   union
   select il.loc loc,
          il.loc_type loc_type   
     from item_export_stg stg,
          item_loc il
    where stg.action_type = 'itemupcdel'
      and (stg.item_parent = il.item or
           stg.item_grandparent = il.item)
      and il.loc_type = 'S'
    union
   select stg1.loc loc,
          stg1.loc_type loc_type
     from item_export_stg stg,
          item_export_stg stg1
    where stg.base_extracted_ind = 'N'
      and stg.action_type = 'itemupcdel'
      and (stg.item_parent = stg1.item or
           stg.item_grandparent = stg1.item)
      and stg1.action_type = 'itemlocdel'
      and stg1.base_Extracted_ind = 'N'
      and stg1.loc_type = 'S'
   union
   select distinct loc,
          loc_type
      from item_export_stg
     where base_extracted_ind = 'N'
       and action_type in ('itemloccre','itemlocmod','itemlocdel')
       order by loc;
EOF
fi

if [[ `grep "^ORA-" $LOC_LIST | wc -l` -gt 0 ]]; then
   cat $LOC_LIST >> ${ERRORFILE}
   status=${FATAL}
   rm $LOC_LIST
else
   status=${OK}
fi


return $status
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_FULL
# Purpose      : Export full item loc records
#-------------------------------------------------------------------------
function PROCESS_FULL
{
loc=$1
loc_type=$2
if [[ -z ${loc_type} ]]; then
loc_type=`${ORACLE_HOME}/bin/sqlplus -s ${CONNECT} <<EOF

set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0

VARIABLE GV_loc NUMBER;
EXEC :GV_loc := ${loc};
select loc_type from (select 'S'   loc_type
  from store st,
       period p
 where p.vdate <= nvl(st.store_close_date,p.vdate)
   and st.store_type in ('C','F')
   and st.store = trim(:GV_loc)
union all
select 'W' loc_type
  from wh wh
   where wh.wh = trim(:GV_loc)
     and wh.wh <> wh.physical_wh
union all
select 'E'          loc_type
  from partner p
 where p.partner_type = 'E'
   and p.partner_id = trim(:GV_loc));
exit;
EOF`
fi

echo "Extracting full item location records for Location: ${loc}" >> $LOGFILE
 ITEMSTORE=$DIR/itemloc_${extractDate}_${loc_type}_${loc}_full

   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$ITEMSTORE
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   VARIABLE GV_loc NUMBER;
   VARIABLE GV_script_error CHAR(255);
   VARIABLE GV_return_code NUMBER;

   EXEC :GV_loc := ${loc};


select Family                     ||'|'||
       Type                       ||'|'||
       LocType                    ||'|'||
       Location                   ||'|'||
       Item                       ||'|'||
       ItemParent                 ||'|'||
       ItemGrandparent            ||'|'||
       InitialUnitRetail          ||'|'||
       SellingUnitRetail          ||'|'||
       CurrencyCode               ||'|'||
       SellingUOM                 ||'|'||
       TaxableInd                 ||'|'||
       LocalItemDesc              ||'|'||
       LocalShortDesc             ||'|'||
       Ti                         ||'|'||
       Hi                         ||'|'||
       StoreOrderMultiple         ||'|'||
       Status                     ||'|'||
       DailyWastePct              ||'|'||
       MeasureOfEach              ||'|'||
       MeasureOfPrice             ||'|'||
       UomOfPrice                 ||'|'||
       PrimaryVariant             ||'|'||
       PrimaryCostPack            ||'|'||
       PrimarySupplier            ||'|'||
       PrimaryOriginCountry       ||'|'||
       ReceiveAsType              ||'|'||
       InboundHandlingDays        ||'|'||
       SourceMethod               ||'|'||
       SourceWh                   ||'|'||
       UinType                    ||'|'||
       UinLabel                   ||'|'||
       CaptureTimeInProc          ||'|'||
       ExtUinInd                  ||'|'||
       IntentionallyRangedInd     ||'|'||
       CostingLocation            ||'|'||
       CostingLocType             ||'|'||
       LaunchDate                 ||'|'||
       QtyKeyOptions              ||'|'||
       ManualPriceEntry           ||'|'||
       DepositCode                ||'|'||
       FoodStampInd               ||'|'||
       WicInd                     ||'|'||
       ProportionalTarePct        ||'|'||
       FixedTareValue             ||'|'||
       FixedTareUom               ||'|'||
       RewardEligibleInd          ||'|'||
       NatlBrandCompItem          ||'|'||
       ReturnPolicy               ||'|'||
       StopSaleInd                ||'|'||
       ElectMtkClubs              ||'|'||
       ReportCode                 ||'|'||
       ReqShelfLifeOnSelection    ||'|'||
       ReqShelfLifeOnReceipt      ||'|'||
       IBShelfLife                ||'|'||
       StoreReorderableInd        ||'|'||
       RackSize                   ||'|'||
       FullPalletItem             ||'|'||
       InStoreMarketBasket        ||'|'||
       StorageLocation            ||'|'||
       AltStorageLocation         ||'|'||
       ReturnableInd              ||'|'||
       RefundableInd              ||'|'||
       BackOrderInd               ||'|'||
       MerchandiseInd
  from ( select 'ITEMLOC'                                Family
               ,'FULLITEMLOC'                            Type
               ,il.loc_type                              LocType
               ,il.loc                                   Location
               ,il.item                                  Item
               ,il.item_parent                           ItemParent
               ,il.item_grandparent                      ItemGrandparent
               ,il.unit_retail                           InitialUnitRetail
               ,il.selling_unit_retail                   SellingUnitRetail
               ,loc.currency_code                        CurrencyCode
               ,il.selling_uom                           SellingUOM
               ,il.taxable_ind                           TaxableInd
               ,REPLACE(il.local_item_desc,CHR(10),'')   LocalItemDesc
               ,REPLACE(il.local_short_desc,CHR(10),'')  LocalShortDesc
               ,il.ti                                    Ti
               ,il.hi                                    Hi
               ,il.store_ord_mult                        StoreOrderMultiple
               ,il.status                                Status
               ,il.daily_waste_pct                       DailyWastePct
               ,il.meas_of_each                          MeasureOfEach
               ,il.meas_of_price                         MeasureOfPrice
               ,il.uom_of_price                          UomOfPrice
               ,il.primary_variant                       PrimaryVariant
               ,il.primary_cost_pack                     PrimaryCostPack
               ,il.primary_supp                          PrimarySupplier
               ,il.primary_cntry                         PrimaryOriginCountry
               ,il.receive_as_type                       ReceiveAsType
               ,il.inbound_handling_days                 InboundHandlingDays
               ,il.source_method                         SourceMethod
               ,il.source_wh                             SourceWh
               ,il.uin_type                              UinType
               ,il.uin_label                             UinLabel
               ,il.capture_time                          CaptureTimeInProc
               ,il.ext_uin_ind                           ExtUinInd
               ,il.ranged_ind                            IntentionallyRangedInd
               ,il.costing_loc                           CostingLocation
               ,il.costing_loc_type                      CostingLocType
               ,TO_CHAR(ilt.launch_date, 'DD-MON-YYYY')  LaunchDate
               ,ilt.qty_key_options                      QtyKeyOptions
               ,ilt.manual_price_entry                   ManualPriceEntry
               ,ilt.deposit_code                         DepositCode
               ,ilt.food_stamp_ind                       FoodStampInd
               ,ilt.wic_ind                              WicInd
               ,ilt.proportional_tare_pct                ProportionalTarePct
               ,ilt.fixed_tare_value                     FixedTareValue
               ,ilt.fixed_tare_uom                       FixedTareUom
               ,ilt.reward_eligible_ind                  RewardEligibleInd
               ,ilt.natl_brand_comp_item                 NatlBrandCompItem
               ,ilt.return_policy                        ReturnPolicy
               ,ilt.stop_sale_ind                        StopSaleInd
               ,ilt.elect_mtk_clubs                      ElectMtkClubs
               ,ilt.report_code                          ReportCode
               ,ilt.req_shelf_life_on_selection          ReqShelfLifeOnSelection
               ,ilt.req_shelf_life_on_receipt            ReqShelfLifeOnReceipt
               ,ilt.ib_shelf_life                        IBShelfLife
               ,ilt.store_reorderable_ind                StoreReorderableInd
               ,ilt.rack_size                            RackSize
               ,ilt.full_pallet_item                     FullPalletItem
               ,ilt.in_store_market_basket               InStoreMarketBasket
               ,ilt.storage_location                     StorageLocation
               ,ilt.alt_storage_location                 AltStorageLocation
               ,ilt.returnable_ind                       ReturnableInd
               ,ilt.refundable_ind                       RefundableInd
               ,ilt.back_order_ind                       BackOrderInd
               ,im.merchandise_ind                       MerchandiseInd
          from item_loc il,
               item_loc_traits ilt,
               item_master im,
               item_export_info iei,
               (select store  location,
                       currency_code currency_code
                  from store
                 union
                select wh     location,
                       currency_code currency_code
                  from wh
                 union
                select TO_NUMBER(partner_id) location,
                       currency_code
                  from partner
                 where partner_type = 'E') loc
         where il.item = im.item
           and im.sellable_ind = 'Y'
           and im.status = 'A'
           and im.item = iei.item
           and iei.base_extracted_ind = 'Y'
           and il.item = iei.item
           and il.item = ilt.item(+)
           and il.loc  = ilt.loc(+)
           and il.loc  = loc.location
           and il.loc = trim(:GV_loc)
           order by item,location) itemloc;
EOF
err=$?
   if [[ `grep "^ORA-" $ITEMSTORE | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $ITEMSTORE >> $ERRORFILE
      rm $ITEMSTORE
   else
      COUNTER=`wc -l ${ITEMSTORE}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${ITEMSTORE}" "${ITEMSTORE}"_${COUNTER}.dat
      else
         rm ${ITEMSTORE}
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: PROCESS_HDR_DELTA
# Purpose: Extracting delta records
#-------------------------------------------------------------------------
function PROCESS_HDR_DELTA
{
store=$1
process_id=$2

echo "Extracting delta records for store: $store" >> $LOGFILE
ITEMHDRDELTA=$DIR/itemhdr_${extractDate}_${store}_delta
   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$ITEMHDRDELTA
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 2500;
   set pages 0

   VARIABLE GV_store NUMBER;
   VARIABLE GV_process_id NUMBER;

   EXEC :GV_store := ${store};
   EXEC :GV_process_id := ${process_id};

SELECT Family                        ||'|'||
       Type                          ||'|'||
       Location                      ||'|'||
       Item                          ||'|'||
       ItemParent                    ||'|'||
       ItemGrandparent               ||'|'||
       PackInd                       ||'|'||
       SimplePackInd                 ||'|'||
       ItemLevel                     ||'|'||
       TranLevel                     ||'|'||
       InventoryInd                  ||'|'||
       Diff1Level                    ||'|'||
       Diff1Type                     ||'|'||
       Diff1                         ||'|'||
       Diff2Level                    ||'|'||
       Diff2Type                     ||'|'||
       Diff2                         ||'|'||
       Diff3Level                    ||'|'||
       Diff3Type                     ||'|'||
       Diff3                         ||'|'||
       Diff4Level                    ||'|'||
       Diff4Type                     ||'|'||
       Diff4                         ||'|'||
       Dept                          ||'|'||
       Class                         ||'|'||
       UniqueClass                   ||'|'||
       Subclass                      ||'|'||
       UniqueSublass                 ||'|'||
       Status                        ||'|'||
       ItemDesc                      ||'|'||
       SecondaryItemDesc             ||'|'||
       ShortDescription              ||'|'||
       BrandName                     ||'|'||
       MerchandiseInd                ||'|'||
       PrimaryRefItemInd             ||'|'||
       CostZoneGroupId               ||'|'||
       StandardUOM                   ||'|'||
       UOMConvFactor                 ||'|'||
       PackageSize                   ||'|'||
       PackageUOM                    ||'|'||
       StoreOrdMulti                 ||'|'||
       ForecastInd                   ||'|'||
       OriginalRetail                ||'|'||
       OriginalRetailCurrencyCode    ||'|'||
       MfgRecRetail                  ||'|'||
       MfgRecRetailCurrencyCode      ||'|'||
       RetailLabelType               ||'|'||
       RetailLabelValue              ||'|'||
       ItemAggregateInd              ||'|'||
       Diff1AggregateInd             ||'|'||
       Diff2AggregateInd             ||'|'||
       Diff3AggregateInd             ||'|'||
       Diff4AggregateInd             ||'|'||
       ItemNumberType                ||'|'||
       FormatID                      ||'|'||
       Prefix                        ||'|'||
       RecHandlingTemp               ||'|'||
       RecHandlingSens               ||'|'||
       PerishableInd                 ||'|'||
       WasteType                     ||'|'||
       WastePct                      ||'|'||
       DefaultWastePct               ||'|'||
       ConstantDimInd                ||'|'||
       ContainsInnerInd              ||'|'||
       SellableInd                   ||'|'||
       OrderableInd                  ||'|'||
       PackType                      ||'|'||
       OrderAsType                   ||'|'||
       ItemServiceLevel              ||'|'||
       GiftWrapInd                   ||'|'||
       ShipAloneInd                  ||'|'||
       ItemXformInd                  ||'|'||
       CatchWeightInd                ||'|'||
       CatchWeightType               ||'|'||
       CatchWeightOrderType          ||'|'||
       CatchWeightSaleType           ||'|'||
       CatchWeightUOM                ||'|'||
       DepositItemType               ||'|'||
       ContainerItem                 ||'|'||
       DepositInPricePerOUM          ||'|'||
       SOHInquiryAtPackInd           ||'|'||
       NotionalPackInd               ||'|'||
       Comments
FROM (
SELECT * FROM (
     SELECT  distinct 'ITEMS'                                                                                 Family
                     ,upper(stg.action_type)                                                                  Type
                     ,il.loc                                                                                  Location
                     ,stg.item                                                                                 Item
                     ,im.item_parent                                                                          ItemParent
                     ,im.item_grandparent                                                                     ItemGrandparent
                     ,im.pack_ind                                                                             PackInd
                     ,im.simple_pack_ind                                                                      SimplePackInd
                     ,im.item_level                                                                           ItemLevel
                     ,im.tran_level                                                                           TranLevel
                     ,im.inventory_ind                                                                        InventoryInd
                     ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_group_id,NULL,'ID','GROUP'))            Diff1Level
                     ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_type,NULL,di1.diff_type,dh1.diff_type)) Diff1Type
                     ,im.diff_1                                                                               Diff1
                     ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_group_id,NULL,'ID','GROUP'))            Diff2Level
                     ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_type,NULL,di2.diff_type,dh2.diff_type)) Diff2Type
                     ,im.diff_2                                                                               Diff2
                     ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_group_id,NULL,'ID','GROUP'))            Diff3Level
                     ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_type,NULL,di3.diff_type,dh3.diff_type)) Diff3Type
                     ,im.diff_3                                                                               Diff3
                     ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_group_id,NULL,'ID','GROUP'))            Diff4Level
                     ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_type,NULL,di4.diff_type,dh4.diff_type)) Diff4Type
                     ,im.diff_4                                                                               Diff4
                     ,im.DEPT                                                                                 Dept
                     ,im.class                                                                                Class
                     ,c.class_id                                                                              UniqueClass
                     ,im.subclass                                                                             Subclass
                     ,s.subclass_id                                                                           UniqueSublass
                     ,im.status                                                                               Status
                     ,im.item_desc                                                                            ItemDesc
                     ,im.item_desc_secondary                                                                  SecondaryItemDesc
                     ,im.short_desc                                                                           ShortDescription
                     ,im.brand_name                                                                           BrandName
                     ,im.merchandise_ind                                                                      MerchandiseInd
                     ,im.primary_ref_item_ind                                                                 PrimaryRefItemInd
                     ,im.cost_zone_group_id                                                                   CostZoneGroupId
                     ,im.standard_uom                                                                         StandardUOM
                     ,im.uom_conv_factor                                                                      UOMConvFactor
                     ,im.package_size                                                                         PackageSize
                     ,im.package_uom                                                                          PackageUOM
                     ,im.store_ord_mult                                                                       StoreOrdMulti
                     ,im.forecast_ind                                                                         ForecastInd
                     ,im.original_retail                                                                      OriginalRetail
                     ,so.currency_code                                                                        OriginalRetailCurrencyCode
                     ,im.mfg_rec_retail                                                                       MfgRecRetail
                     ,so.currency_code                                                                        MfgRecRetailCurrencyCode
                     ,im.retail_label_type                                                                    RetailLabelType
                     ,im.retail_label_value                                                                   RetailLabelValue
                     ,im.item_aggregate_ind                                                                   ItemAggregateInd
                     ,im.diff_1_aggregate_ind                                                                 Diff1AggregateInd
                     ,im.diff_2_aggregate_ind                                                                 Diff2AggregateInd
                     ,im.diff_3_aggregate_ind                                                                 Diff3AggregateInd
                     ,im.diff_4_aggregate_ind                                                                 Diff4AggregateInd
                     ,im.item_number_type                                                                     ItemNumberType
                     ,im.format_id                                                                            FormatID
                     ,im.prefix                                                                               Prefix
                     ,im.handling_temp                                                                        RecHandlingTemp
                     ,im.handling_sensitivity                                                                 RecHandlingSens
                     ,im.perishable_ind                                                                       PerishableInd
                     ,im.waste_type                                                                           WasteType
                     ,im.waste_pct                                                                            WastePct
                     ,im.default_waste_pct                                                                    DefaultWastePct
                     ,im.const_dimen_ind                                                                      ConstantDimInd
                     ,im.contains_inner_ind                                                                   ContainsInnerInd
                     ,im.sellable_ind                                                                         SellableInd
                     ,im.orderable_ind                                                                        OrderableInd
                     ,im.pack_type                                                                            PackType
                     ,im.order_as_type                                                                        OrderAsType
                     ,im.item_service_level                                                                   ItemServiceLevel
                     ,im.gift_wrap_ind                                                                        GiftWrapInd
                     ,im.ship_alone_ind                                                                       ShipAloneInd
                     ,im.item_xform_ind                                                                       ItemXformInd
                     ,im.catch_weight_ind                                                                     CatchWeightInd
                     ,im.catch_weight_type                                                                    CatchWeightType
                     ,im.order_type                                                                           CatchWeightOrderType
                     ,im.sale_type                                                                            CatchWeightSaleType
                     ,im.catch_weight_uom                                                                     CatchWeightUOM
                     ,im.deposit_item_type                                                                    DepositItemType
                     ,im.container_item                                                                       ContainerItem
                     ,im.deposit_in_price_per_uom                                                             DepositInPricePerOUM
                     ,im.soh_inquiry_at_pack_ind                                                              SOHInquiryAtPackInd
                     ,im.notional_pack_ind                                                                    NotionalPackInd
                     ,im.comments                                                                             Comments
                     ,stg.seq_no                                                                              seq_no
                FROM  item_export_stg stg,
                      item_master im,
                      item_loc il,
                      class c,
                      subclass s,
                      diff_group_head dh1,
                      diff_ids di1,
                      diff_group_head dh2,
                      diff_ids di2,
                      diff_group_head dh3,
                      diff_ids di3,
                      diff_group_head dh4,
                      diff_ids di4,
                      system_options so,
                      (select item,
                              item_parent,
                              item_grandparent
                         from item_master 
                       where status = 'A' 
                         and sellable_ind = 'Y' 
                         and item_level > tran_level
                       ) upc 
               WHERE stg.item = im.item
                 AND il.item = DECODE(upc.item,null,stg.item,im.item_parent)
                 AND im.item = upc.item(+)
                 AND im.item_parent = upc.item_parent(+)
                 AND stg.base_extracted_ind = 'N'
                 AND stg.action_type in ('itemhdrmod')
                 AND stg.loc=il.loc
                 AND il.loc = trim (:GV_store)
                 AND stg.process_id = trim(:GV_process_id)
                 AND im.class = c.class
                 AND im.dept  = c.dept
                 AND c.dept  = s.dept
                 AND c.class = s.class
                 AND im.subclass = s.subclass
                 AND im.diff_1 = dh1.diff_group_id(+)
                 AND im.diff_1 = di1.diff_id(+)
                 AND im.diff_2 = dh2.diff_group_id(+)
                 AND im.diff_2 = di2.diff_id(+)
                 AND im.diff_3 = dh3.diff_group_id(+)
                 AND im.diff_3 = di3.diff_id(+)
                 AND im.diff_4 = dh4.diff_group_id(+)
                 AND im.diff_4 = di4.diff_id(+)
               UNION ALL
               SELECT 'ITEMS'                Family
                     ,'ITEMHDRDEL'           Type
                     ,inner.loc              Location
                     ,inner.item             Item
                     ,NULL                   ItemParent
                     ,NULL                   ItemGrandparent
                     ,NULL                   PackInd
                     ,NULL                   SimplePackInd
                     ,inner.item_level       ItemLevel
                     ,inner.tran_level       TranLevel
                     ,NULL                   InventoryInd
                     ,NULL                   Diff1Level
                     ,NULL                   Diff1Type
                     ,NULL                   Diff1
                     ,NULL                   Diff2Level
                     ,NULL                   Diff2Type
                     ,NULL                   Diff2
                     ,NULL                   Diff3Level
                     ,NULL                   Diff3Type
                     ,NULL                   Diff3
                     ,NULL                   Diff4Level
                     ,NULL                   Diff4Type
                     ,NULL                   Diff4
                     ,NULL                   Dept
                     ,NULL                   Class
                     ,NULL                   UniqueClass
                     ,NULL                   Subclass
                     ,NULL                   UniqueSublass
                     ,NULL                   Status
                     ,NULL                   ItemDesc
                     ,NULL                   SecondaryItemDesc
                     ,NULL                   ShortDescription
                     ,NULL                   BrandName
                     ,NULL                   MerchandiseInd
                     ,NULL                   PrimaryRefItemInd
                     ,NULL                   CostZoneGroupId
                     ,NULL                   StandardUOM
                     ,NULL                   UOMConvFactor
                     ,NULL                   PackageSize
                     ,NULL                   PackageUOM
                     ,NULL                   StoreOrdMulti
                     ,NULL                   ForecastInd
                     ,NULL                   OriginalRetail
                     ,NULL                   OriginalRetailCurrencyCode
                     ,NULL                   MfgRecRetail
                     ,NULL                   MfgRecRetailCurrencyCode
                     ,NULL                   RetailLabelType
                     ,NULL                   RetailLabelValue
                     ,NULL                   ItemAggregateInd
                     ,NULL                   Diff1AggregateInd
                     ,NULL                   Diff2AggregateInd
                     ,NULL                   Diff3AggregateInd
                     ,NULL                   Diff4AggregateInd
                     ,NULL                   ItemNumberType
                     ,NULL                   FormatID
                     ,NULL                   Prefix
                     ,NULL                   RecHandlingTemp
                     ,NULL                   RecHandlingSens
                     ,NULL                   PerishableInd
                     ,NULL                   WasteType
                     ,NULL                   WastePct
                     ,NULL                   DefaultWastePct
                     ,NULL                   ConstantDimInd
                     ,NULL                   ContainsInnerInd
                     ,NULL                   SellableInd
                     ,NULL                   OrderableInd
                     ,NULL                   PackType
                     ,NULL                   OrderAsType
                     ,NULL                   ItemServiceLevel
                     ,NULL                   GiftWrapInd
                     ,NULL                   ShipAloneInd
                     ,NULL                   ItemXformInd
                     ,NULL                   CatchWeightInd
                     ,NULL                   CatchWeightType
                     ,NULL                   CatchWeightOrderType
                     ,NULL                   CatchWeightSaleType
                     ,NULL                   CatchWeightUOM
                     ,NULL                   DepositItemType
                     ,NULL                   ContainerItem
                     ,NULL                   DepositInPricePerOUM
                     ,NULL                   SOHInquiryAtPackInd
                     ,NULL                   NotionalPackInd
                     ,NULL                   Comments
                     ,inner.seqno            seq_no
                FROM  (select stg.item item,
                              stg.item_level,
                              stg.tran_level,
                              stg1.loc loc,
                              stg1.seq_no seqno
                         from item_export_stg stg,
                              item_export_stg stg1
                        where stg.action_type in ('itemhdrdel')
                          and stg.item = stg1.item
                          and stg1.action_type in ('itemlocdel')
                          and stg1.process_id = trim(:GV_process_id)
                          and stg1.loc = trim (:GV_store)
                        union
                       select stg.item item,
                              stg.item_level,
                              stg.tran_level,
                              il.loc loc,
                              stg.seq_no seqno
                         from item_export_stg stg,
                              item_loc il
                        where stg.base_extracted_ind = 'N'
                          and stg.action_type in ('itemupcdel')
                          and (stg.item_parent = il.item or
                               stg.item_grandparent = il.item)
                          and stg.process_id = trim(:GV_process_id)
                          and il.loc = trim (:GV_store)
                        union
                       select stg.item item,
                              stg.item_level,
                              stg.tran_level,
                              stg1.loc loc,
                              stg1.seq_no seqno
                         from item_export_stg stg,
                              item_export_stg stg1
                        where stg.action_type in ('itemupcdel')
                          and (stg.item_parent = stg1.item or
                               stg.item_grandparent = stg1.item)
                          and stg1.action_type in ('itemlocdel')
                          and stg1.process_id = trim(:GV_process_id)
                          and stg1.loc = trim (:GV_store)
                          and stg1.base_extracted_ind = 'N') inner
            UNION ALL
               SELECT distinct 'ITEMS'                                                                                 Family
                     ,'ITEMHDRCRE'                                                                            Type
                     ,il.loc                                                                                  Location
                     ,im.item                                                                                 Item
                     ,im.item_parent                                                                          ItemParent
                     ,im.item_grandparent                                                                     ItemGrandparent
                     ,im.pack_ind                                                                             PackInd
                     ,im.simple_pack_ind                                                                      SimplePackInd
                     ,im.item_level                                                                           ItemLevel
                     ,im.tran_level                                                                           TranLevel
                     ,im.inventory_ind                                                                        InventoryInd
                     ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_group_id,NULL,'ID','GROUP'))            Diff1Level
                     ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_type,NULL,di1.diff_type,dh1.diff_type)) Diff1Type
                     ,im.diff_1                                                                               Diff1
                     ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_group_id,NULL,'ID','GROUP'))            Diff2Level
                     ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_type,NULL,di2.diff_type,dh2.diff_type)) Diff2Type
                     ,im.diff_2                                                                               Diff2
                     ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_group_id,NULL,'ID','GROUP'))            Diff3Level
                     ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_type,NULL,di3.diff_type,dh3.diff_type)) Diff3Type
                     ,im.diff_3                                                                               Diff3
                     ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_group_id,NULL,'ID','GROUP'))            Diff4Level
                     ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_type,NULL,di4.diff_type,dh4.diff_type)) Diff4Type
                     ,im.diff_4                                                                               Diff4
                     ,im.DEPT                                                                                 Dept
                     ,im.class                                                                                Class
                     ,c.class_id                                                                              UniqueClass
                     ,im.subclass                                                                             Subclass
                     ,s.subclass_id                                                                           UniqueSublass
                     ,im.status                                                                               Status
                     ,im.item_desc                                                                            ItemDesc
                     ,im.item_desc_secondary                                                                  SecondaryItemDesc
                     ,im.short_desc                                                                           ShortDescription
                     ,im.brand_name                                                                           BrandName
                     ,im.merchandise_ind                                                                      MerchandiseInd
                     ,im.primary_ref_item_ind                                                                 PrimaryRefItemInd
                     ,im.cost_zone_group_id                                                                   CostZoneGroupId
                     ,im.standard_uom                                                                         StandardUOM
                     ,im.uom_conv_factor                                                                      UOMConvFactor
                     ,im.package_size                                                                         PackageSize
                     ,im.package_uom                                                                          PackageUOM
                     ,im.store_ord_mult                                                                       StoreOrdMulti
                     ,im.forecast_ind                                                                         ForecastInd
                     ,im.original_retail                                                                      OriginalRetail
                     ,so.currency_code                                                                        OriginalRetailCurrencyCode
                     ,im.mfg_rec_retail                                                                       MfgRecRetail
                     ,so.currency_code                                                                        MfgRecRetailCurrencyCode
                     ,im.retail_label_type                                                                    RetailLabelType
                     ,im.retail_label_value                                                                   RetailLabelValue
                     ,im.item_aggregate_ind                                                                   ItemAggregateInd
                     ,im.diff_1_aggregate_ind                                                                 Diff1AggregateInd
                     ,im.diff_2_aggregate_ind                                                                 Diff2AggregateInd
                     ,im.diff_3_aggregate_ind                                                                 Diff3AggregateInd
                     ,im.diff_4_aggregate_ind                                                                 Diff4AggregateInd
                     ,im.item_number_type                                                                     ItemNumberType
                     ,im.format_id                                                                            FormatID
                     ,im.prefix                                                                               Prefix
                     ,im.handling_temp                                                                        RecHandlingTemp
                     ,im.handling_sensitivity                                                                 RecHandlingSens
                     ,im.perishable_ind                                                                       PerishableInd
                     ,im.waste_type                                                                           WasteType
                     ,im.waste_pct                                                                            WastePct
                     ,im.default_waste_pct                                                                    DefaultWastePct
                     ,im.const_dimen_ind                                                                      ConstantDimInd
                     ,im.contains_inner_ind                                                                   ContainsInnerInd
                     ,im.sellable_ind                                                                         SellableInd
                     ,im.orderable_ind                                                                        OrderableInd
                     ,im.pack_type                                                                            PackType
                     ,im.order_as_type                                                                        OrderAsType
                     ,im.item_service_level                                                                   ItemServiceLevel
                     ,im.gift_wrap_ind                                                                        GiftWrapInd
                     ,im.ship_alone_ind                                                                       ShipAloneInd
                     ,im.item_xform_ind                                                                       ItemXformInd
                     ,im.catch_weight_ind                                                                     CatchWeightInd
                     ,im.catch_weight_type                                                                    CatchWeightType
                     ,im.order_type                                                                           CatchWeightOrderType
                     ,im.sale_type                                                                            CatchWeightSaleType
                     ,im.catch_weight_uom                                                                     CatchWeightUOM
                     ,im.deposit_item_type                                                                    DepositItemType
                     ,im.container_item                                                                       ContainerItem
                     ,im.deposit_in_price_per_uom                                                             DepositInPricePerOUM
                     ,im.soh_inquiry_at_pack_ind                                                              SOHInquiryAtPackInd
                     ,im.notional_pack_ind                                                                    NotionalPackInd
                     ,im.comments                                                                             Comments
                     ,NULL                                                                                    seq_no
                FROM  item_export_stg stg,
                      item_master im,
                      item_loc il,
                      class c,
                      subclass s,
                      diff_group_head dh1,
                      diff_ids di1,
                      diff_group_head dh2,
                      diff_ids di2,
                      diff_group_head dh3,
                      diff_ids di3,
                      diff_group_head dh4,
                      diff_ids di4,
                      system_options so
               WHERE stg.item = im.item_parent
                 and im.status = 'A' 
                 and im.sellable_ind = 'Y' 
                 and im.item_level > im.tran_level 
                 AND il.item=  stg.item
                 AND stg.base_extracted_ind = 'N'
                 AND stg.loc=il.loc
                 AND stg.process_id = trim(:GV_process_id)
                 AND stg.action_type in ('itemloccre')
                 AND il.loc = trim (:GV_store)
                 AND im.class = c.class
                 AND im.dept  = c.dept
                 AND c.dept  = s.dept
                 AND c.class = s.class
                 AND im.subclass = s.subclass
                 AND im.diff_1 = dh1.diff_group_id(+)
                 AND im.diff_1 = di1.diff_id(+)
                 AND im.diff_2 = dh2.diff_group_id(+)
                 AND im.diff_2 = di2.diff_id(+)
                 AND im.diff_3 = dh3.diff_group_id(+)
                 AND im.diff_3 = di3.diff_id(+)
                 AND im.diff_4 = dh4.diff_group_id(+)
                 AND im.diff_4 = di4.diff_id(+)          
              UNION 
              SELECT distinct 'ITEMS'                                                                                 Family
                     ,'ITEMHDRCRE'                                                                            Type
                     ,stg.loc                                                                                  Location
                     ,stg.item                                                                                 Item
                     ,im.item_parent                                                                          ItemParent
                     ,im.item_grandparent                                                                     ItemGrandparent
                     ,im.pack_ind                                                                             PackInd
                     ,im.simple_pack_ind                                                                      SimplePackInd
                     ,im.item_level                                                                           ItemLevel
                     ,im.tran_level                                                                           TranLevel
                     ,im.inventory_ind                                                                        InventoryInd
                     ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_group_id,NULL,'ID','GROUP'))            Diff1Level
                     ,decode(im.diff_1,NULL,im.diff_1,decode(dh1.diff_type,NULL,di1.diff_type,dh1.diff_type)) Diff1Type
                     ,im.diff_1                                                                               Diff1
                     ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_group_id,NULL,'ID','GROUP'))            Diff2Level
                     ,decode(im.diff_2,NULL,im.diff_2,decode(dh2.diff_type,NULL,di2.diff_type,dh2.diff_type)) Diff2Type
                     ,im.diff_2                                                                               Diff2
                     ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_group_id,NULL,'ID','GROUP'))            Diff3Level
                     ,decode(im.diff_3,NULL,im.diff_3,decode(dh3.diff_type,NULL,di3.diff_type,dh3.diff_type)) Diff3Type
                     ,im.diff_3                                                                               Diff3
                     ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_group_id,NULL,'ID','GROUP'))            Diff4Level
                     ,decode(im.diff_4,NULL,im.diff_4,decode(dh4.diff_type,NULL,di4.diff_type,dh4.diff_type)) Diff4Type
                     ,im.diff_4                                                                               Diff4
                     ,im.DEPT                                                                                 Dept
                     ,im.class                                                                                Class
                     ,c.class_id                                                                              UniqueClass
                     ,im.subclass                                                                             Subclass
                     ,s.subclass_id                                                                           UniqueSublass
                     ,im.status                                                                               Status
                     ,im.item_desc                                                                            ItemDesc
                     ,im.item_desc_secondary                                                                  SecondaryItemDesc
                     ,im.short_desc                                                                           ShortDescription
                     ,im.brand_name                                                                           BrandName
                     ,im.merchandise_ind                                                                      MerchandiseInd
                     ,im.primary_ref_item_ind                                                                 PrimaryRefItemInd
                     ,im.cost_zone_group_id                                                                   CostZoneGroupId
                     ,im.standard_uom                                                                         StandardUOM
                     ,im.uom_conv_factor                                                                      UOMConvFactor
                     ,im.package_size                                                                         PackageSize
                     ,im.package_uom                                                                          PackageUOM
                     ,im.store_ord_mult                                                                       StoreOrdMulti
                     ,im.forecast_ind                                                                         ForecastInd
                     ,im.original_retail                                                                      OriginalRetail
                     ,so.currency_code                                                                        OriginalRetailCurrencyCode
                     ,im.mfg_rec_retail                                                                       MfgRecRetail
                     ,so.currency_code                                                                        MfgRecRetailCurrencyCode
                     ,im.retail_label_type                                                                    RetailLabelType
                     ,im.retail_label_value                                                                   RetailLabelValue
                     ,im.item_aggregate_ind                                                                   ItemAggregateInd
                     ,im.diff_1_aggregate_ind                                                                 Diff1AggregateInd
                     ,im.diff_2_aggregate_ind                                                                 Diff2AggregateInd
                     ,im.diff_3_aggregate_ind                                                                 Diff3AggregateInd
                     ,im.diff_4_aggregate_ind                                                                 Diff4AggregateInd
                     ,im.item_number_type                                                                     ItemNumberType
                     ,im.format_id                                                                            FormatID
                     ,im.prefix                                                                               Prefix
                     ,im.handling_temp                                                                        RecHandlingTemp
                     ,im.handling_sensitivity                                                                 RecHandlingSens
                     ,im.perishable_ind                                                                       PerishableInd
                     ,im.waste_type                                                                           WasteType
                     ,im.waste_pct                                                                            WastePct
                     ,im.default_waste_pct                                                                    DefaultWastePct
                     ,im.const_dimen_ind                                                                      ConstantDimInd
                     ,im.contains_inner_ind                                                                   ContainsInnerInd
                     ,im.sellable_ind                                                                         SellableInd
                     ,im.orderable_ind                                                                        OrderableInd
                     ,im.pack_type                                                                            PackType
                     ,im.order_as_type                                                                        OrderAsType
                     ,im.item_service_level                                                                   ItemServiceLevel
                     ,im.gift_wrap_ind                                                                        GiftWrapInd
                     ,im.ship_alone_ind                                                                       ShipAloneInd
                     ,im.item_xform_ind                                                                       ItemXformInd
                     ,im.catch_weight_ind                                                                     CatchWeightInd
                     ,im.catch_weight_type                                                                    CatchWeightType
                     ,im.order_type                                                                           CatchWeightOrderType
                     ,im.sale_type                                                                            CatchWeightSaleType
                     ,im.catch_weight_uom                                                                     CatchWeightUOM
                     ,im.deposit_item_type                                                                    DepositItemType
                     ,im.container_item                                                                       ContainerItem
                     ,im.deposit_in_price_per_uom                                                             DepositInPricePerOUM
                     ,im.soh_inquiry_at_pack_ind                                                              SOHInquiryAtPackInd
                     ,im.notional_pack_ind                                                                    NotionalPackInd
                     ,im.comments                                                                             Comments
                     ,NULL                                                                                    seq_no
                 FROM item_export_stg stg,
                      item_master im,
                      item_loc il,
                      class c,
                      subclass s,
                      diff_group_head dh1,
                      diff_ids di1,
                      diff_group_head dh2,
                      diff_ids di2,
                      diff_group_head dh3,
                      diff_ids di3,
                      diff_group_head dh4,
                      diff_ids di4,
                      system_options so
               WHERE stg.item = im.item
                 AND stg.item= il.item(+)
                 AND stg.base_extracted_ind = 'N'
                 AND stg.loc=il.loc(+)
                 AND stg.process_id = trim(:GV_process_id)
                 AND stg.action_type in ('itemloccre','itemupccre')
                 AND stg.loc = trim (:GV_store)
                 AND im.class = c.class
                 AND im.dept  = c.dept
                 AND c.dept  = s.dept
                 AND c.class = s.class
                 AND im.subclass = s.subclass
                 AND im.diff_1 = dh1.diff_group_id(+)
                 AND im.diff_1 = di1.diff_id(+)
                 AND im.diff_2 = dh2.diff_group_id(+)
                 AND im.diff_2 = di2.diff_id(+)
                 AND im.diff_3 = dh3.diff_group_id(+)
                 AND im.diff_3 = di3.diff_id(+)
                 AND im.diff_4 = dh4.diff_group_id(+)
                 AND im.diff_4 = di4.diff_id(+)) order by seq_no
   ) itemdhdrelta;

EOF
   if [[ `grep "^ORA-" ${ITEMHDRDELTA} | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat ${ITEMHDRDELTA} >> $ERRORFILE
      rm ${ITEMHDRDELTA}
   else
      COUNTER=`wc -l ${ITEMHDRDELTA}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${ITEMHDRDELTA}" "${ITEMHDRDELTA}"_${COUNTER}.dat
      else
         rm $ITEMHDRDELTA
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function Name: PROCESS_DELTA
# Purpose      : Export item location delta records
#-------------------------------------------------------------------------
function PROCESS_DELTA
{
loc=$1
loc_type=$2
process_id=$3
if [[ $2 = "single" ]]; then
loc_type=`$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   VARIABLE GV_loc NUMBER;
   VARIABLE GV_process_id NUMBER;

   EXEC :GV_process_id := ${process_id};
   EXEC :GV_loc := ${loc};

   select distinct loc_type
     from item_export_stg
    where loc = trim(:GV_loc)
      and base_Extracted_ind = 'N'
      and process_id = trim(:GV_process_id);

exit;
EOF`
fi
 echo "Extracting delta item location records for location: ${loc}" >> $LOGFILE
 ITMSTRSTG=$DIR/itemloc_${extractDate}_${loc_type}_${loc}_delta

   $ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF >$ITMSTRSTG
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set linesize 1000;
   set pages 0

   VARIABLE GV_loc NUMBER;
   VARIABLE GV_process_id NUMBER;

   EXEC :GV_loc := ${loc};
   EXEC :GV_process_id := ${process_id};

select Family                     ||'|'||
       Type                       ||'|'||
       LocType                    ||'|'||
       Location                   ||'|'||
       Item                       ||'|'||
       ItemParent                 ||'|'||
       ItemGrandparent            ||'|'||
       InitialUnitRetail          ||'|'||
       SellingUnitRetail          ||'|'||
       CurrencyCode               ||'|'||
       SellingUOM                 ||'|'||
       TaxableInd                 ||'|'||
       LocalItemDesc              ||'|'||
       LocalShortDesc             ||'|'||
       Ti                         ||'|'||
       Hi                         ||'|'||
       StoreOrderMultiple         ||'|'||
       Status                     ||'|'||
       DailyWastePct              ||'|'||
       MeasureOfEach              ||'|'||
       MeasureOfPrice             ||'|'||
       UomOfPrice                 ||'|'||
       PrimaryVariant             ||'|'||
       PrimaryCostPack            ||'|'||
       PrimarySupplier            ||'|'||
       PrimaryOriginCountry       ||'|'||
       ReceiveAsType              ||'|'||
       InboundHandlingDays        ||'|'||
       SourceMethod               ||'|'||
       SourceWh                   ||'|'||
       UinType                    ||'|'||
       UinLabel                   ||'|'||
       CaptureTimeInProc          ||'|'||
       ExtUinInd                  ||'|'||
       IntentionallyRangedInd     ||'|'||
       CostingLocation            ||'|'||
       CostingLocType             ||'|'||
       LaunchDate                 ||'|'||
       QtyKeyOptions              ||'|'||
       ManualPriceEntry           ||'|'||
       DepositCode                ||'|'||
       FoodStampInd               ||'|'||
       WicInd                     ||'|'||
       ProportionalTarePct        ||'|'||
       FixedTareValue             ||'|'||
       FixedTareUom               ||'|'||
       RewardEligibleInd          ||'|'||
       NatlBrandCompItem          ||'|'||
       ReturnPolicy               ||'|'||
       StopSaleInd                ||'|'||
       ElectMtkClubs              ||'|'||
       ReportCode                 ||'|'||
       ReqShelfLifeOnSelection    ||'|'||
       ReqShelfLifeOnReceipt      ||'|'||
       IBShelfLife                ||'|'||
       StoreReorderableInd        ||'|'||
       RackSize                   ||'|'||
       FullPalletItem             ||'|'||
       InStoreMarketBasket        ||'|'||
       StorageLocation            ||'|'||
       AltStorageLocation         ||'|'||
       ReturnableInd              ||'|'||
       RefundableInd              ||'|'||
       BackOrderInd               ||'|'||
       MerchandiseInd
  from
     ( select * from
      (  select 'ITEMLOC'                                Family
               ,upper(stg.action_type)                   Type
               ,stg.loc_type                             LocType
               ,stg.loc                                  Location
               ,stg.item                                 Item
               ,il.item_parent                           ItemParent
               ,il.item_grandparent                      ItemGrandparent
               ,DECODE(stg.action_type, 'itemloccre', il.unit_retail, NULL)           InitialUnitRetail
               ,DECODE(stg.action_type, 'itemloccre', il.selling_unit_retail, NULL)   SellingUnitRetail
               ,DECODE(stg.action_type, 'itemloccre', loc.currency_code, NULL)        CurrencyCode
               ,DECODE(stg.action_type, 'itemloccre', il.selling_uom, NULL)           SellingUOM
               ,il.taxable_ind                           TaxableInd
               ,REPLACE(il.local_item_desc,CHR(10),'')   LocalItemDesc
               ,REPLACE(il.local_short_desc,CHR(10),'')  LocalShortDesc
               ,il.ti                                    Ti
               ,il.hi                                    Hi
               ,il.store_ord_mult                        StoreOrderMultiple
               ,il.status                                Status
               ,il.daily_waste_pct                       DailyWastePct
               ,il.meas_of_each                          MeasureOfEach
               ,il.meas_of_price                         MeasureOfPrice
               ,il.uom_of_price                          UomOfPrice
               ,il.primary_variant                       PrimaryVariant
               ,il.primary_cost_pack                     PrimaryCostPack
               ,il.primary_supp                          PrimarySupplier
               ,il.primary_cntry                         PrimaryOriginCountry
               ,il.receive_as_type                       ReceiveAsType
               ,il.inbound_handling_days                 InboundHandlingDays
               ,il.source_method                         SourceMethod
               ,il.source_wh                             SourceWh
               ,il.uin_type                              UinType
               ,il.uin_label                             UinLabel
               ,il.capture_time                          CaptureTimeInProc
               ,il.ext_uin_ind                           ExtUinInd
               ,il.ranged_ind                            IntentionallyRangedInd
               ,il.costing_loc                           CostingLocation
               ,il.costing_loc_type                      CostingLocType
               ,TO_CHAR(ilt.launch_date, 'DD-MON-YYYY')  LaunchDate
               ,ilt.qty_key_options                      QtyKeyOptions
               ,ilt.manual_price_entry                   ManualPriceEntry
               ,ilt.deposit_code                         DepositCode
               ,ilt.food_stamp_ind                       FoodStampInd
               ,ilt.wic_ind                              WicInd
               ,ilt.proportional_tare_pct                ProportionalTarePct
               ,ilt.fixed_tare_value                     FixedTareValue
               ,ilt.fixed_tare_uom                       FixedTareUom
               ,ilt.reward_eligible_ind                  RewardEligibleInd
               ,ilt.natl_brand_comp_item                 NatlBrandCompItem
               ,ilt.return_policy                        ReturnPolicy
               ,ilt.stop_sale_ind                        StopSaleInd
               ,ilt.elect_mtk_clubs                      ElectMtkClubs
               ,ilt.report_code                          ReportCode
               ,ilt.req_shelf_life_on_selection          ReqShelfLifeOnSelection
               ,ilt.req_shelf_life_on_receipt            ReqShelfLifeOnReceipt
               ,ilt.ib_shelf_life                        IBShelfLife
               ,ilt.store_reorderable_ind                StoreReorderableInd
               ,ilt.rack_size                            RackSize
               ,ilt.full_pallet_item                     FullPalletItem
               ,ilt.in_store_market_basket               InStoreMarketBasket
               ,ilt.storage_location                     StorageLocation
               ,ilt.alt_storage_location                 AltStorageLocation
               ,ilt.returnable_ind                       ReturnableInd
               ,ilt.refundable_ind                       RefundableInd
               ,ilt.back_order_ind                       BackOrderInd
               ,stg.merchandise_ind                      MerchandiseInd
               ,stg.seq_no                               seq_no
          from item_loc il,
               item_loc_traits ilt,
               item_export_stg stg,
               (select store  location,
                       currency_code currency_code
                  from store
                 union
                select wh     location,
                       currency_code currency_code
                  from wh
                 union
                select TO_NUMBER(partner_id) location,
                       currency_code
                  from partner
                 where partner_type = 'E') loc
         where stg.item  = il.item(+)
           and stg.loc = il.loc(+)
           and stg.item  = ilt.item(+)
           and stg.loc = ilt.loc(+)
           and stg.loc = loc.location
           and stg.loc = trim(:GV_loc)
           and stg.process_id = trim(:GV_process_id)
           and stg.action_type in ('itemloccre','itemlocmod','itemlocdel')
           and stg.base_extracted_ind = 'N') order by seq_no
       ) itemlocstg;
EOF
err=$?
   if [[ `grep "^ORA-" $ITMSTRSTG | wc -l` -gt 0 ]]; then
      status=${FATAL}
      cat $ITMSTRSTG >> $ERRORFILE
      rm $ITMSTRSTG
   else
      COUNTER=`wc -l ${ITMSTRSTG}| awk '{print $1}'`
      if [[ ${COUNTER} -gt 0 ]]; then
         mv "${ITMSTRSTG}" "${ITMSTRSTG}"_${COUNTER}.dat
      else
         rm $ITMSTRSTG
      fi
      status=${OK}
   fi
return ${status}
}
#-------------------------------------------------------------------------
# Function: INS_DATA_EXP_HIST
# Purpose : Insert into data_export_hist
#-------------------------------------------------------------------------
function INS_DATA_EXP_HIST
{
mode=$1
if [[ ${mode} = "full" ]]; then
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000
  
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;
   
   WHENEVER SQLERROR EXIT 1;
   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('itemloc',
                                'full',
                                 USER,
                                 SYSDATE);
    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`
fi
if [[ ${mode} = "delta" ]]; then
sqlreturn=`echo "set feedback off
   set heading off
   set term off
   set verify off
   set serveroutput on size 1000000
  VARIABLE GV_script_error CHAR(255);
  VARIABLE GV_return_code NUMBER;

   EXEC :GV_script_error := NULL;
   EXEC :GV_return_code := 0;   
   WHENEVER SQLERROR EXIT 1;   
   insert into data_export_hist(family,
                                export_type,
                                export_id,
                                exported_datetime)
                        values ('itemloc',
                                'delta',
                                 USER,
                                 SYSDATE);
    COMMIT;
 /
   print :GV_script_error;
   exit  :GV_return_code;
   " | sqlplus -s ${CONNECT}`
fi
if [[ $? -ne ${OK} ]]; then
    status=${FATAL}
else
    status=${OK}
fi
return ${status}
}
#-------------------------------------------------------------------------
#                             MAIN
#-------------------------------------------------------------------------
if [[ $# -lt 1 || $# -gt 5 || $# -lt 3 ]]; then
   USAGE
   exit 1
fi

CONNECT=$1
mode=$2
concheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${CONNECT}`
constatus=$?

if [[ ${constatus} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "CONNECTION_FAILED" "${constatus}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
else
   LOG_MESSAGE "export_itemloc.ksh - Started by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

current_process_id=`$ORACLE_HOME/bin/sqlplus -s ${CONNECT} <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select export_process_id_seq.nextval
 from dual;
exit;
EOF`
process_id_status=$?
if [[ ${process_id_status} -ne ${OK} ]]; then
   LOG_ERROR "invalid username/password; logon denied" "GETTING_PROCESS_ID" "${process_id_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
   exit 1
fi

### Check if no. of threads is passed as arg or not, else default.
if [[ $3 = "Y" ]]; then
   SLOTS=$4
   if [[ -z ${SLOTS} ]]; then
     SLOTS=10
   fi
   STR=$5
   echo $SLOTS | egrep '^[0-9]+$'>/dev/null 2>&1
   status=$?
   if [ "$status" -ne "0" ]
   then
     LOG_ERROR "Thread count should be a Positive Number" "NUM_THREADS" "${status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
     exit 1
   fi
   if [[ ${SLOTS} -gt 20 ]]; then
    SLOTS=10
   fi
else
   if [[ $3 = "N" ]]; then
   STR=$4
   SLOTS=10
   else
     LOG_ERROR "Thread number indicator either should be Y or N" "INDICATOR" "$3" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
     exit 1
   fi
fi
if [[ -z ${STR} ]]; then
      UPDATE_ITEM_EXPORT_STG ${current_process_id}
      st=$?
      if [[ ${st} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating item_export_stg with current process_id" "UPDATE_ITEM_EXPORT_STG" "${st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      fi
   if [[ $2 = "full" ]]; then
      tbl="base"
      CREATE_LOCATION_LIST $tbl
      crt_status=$?
      if [[ ${crt_status} -ne ${OK} ]]; then
         LOG_ERROR "ORA Error while fetching store" "CREATE_LOCATION_LIST" "${crt_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      fi
      while read loc loc_type
       do
        if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
            ( 
              PROCESS_FULL $loc $loc_type 
             )&
            process_full_status=$?
            if [[ ${process_full_status} -ne ${OK} ]]; then
             LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL" "${process_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
            fi
        else
           while [ `jobs | wc -l` -ge ${SLOTS} ]
           do
             sleep 1
           done
            ( 
              PROCESS_FULL $loc $loc_type 
             )&
            process_full_status=$?
            if [[ ${process_full_status} -ne ${OK} ]]; then
             LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL" "${process_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
            fi
        fi
       done < $LOC_LIST
   ### Wait for all of the threads to complete
        wait
       if [[ $? -eq ${OK} ]]; then
          INS_DATA_EXP_HIST ${mode}
          ins_dt_exp_st=$?
          if [[ ${ins_dt_exp_st} -ne ${OK} ]]; then
             LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "INS_DATA_EXP_HIST" "${ins_dt_exp_st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
          fi
          PROCESS_STG ${current_process_id}
          process_stg_status=$?
          if [[ ${process_stg_status} -ne ${OK} ]]; then
             LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" "${process_stg_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
          else
             rm $LOC_LIST
             LOG_MESSAGE "export_itemloc.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
          fi
       fi
   fi

   if [[ $2 = "delta" ]]; then
      tbl="stg"
      CREATE_LOCATION_LIST $tbl ${current_process_id}
      crt_status=$?
      if [[ ${crt_status} -ne ${OK} ]]; then
         LOG_ERROR "ORA Error while fetching store" "CREATE_LOCATION_LIST" "${crt_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      fi
      while read loc loc_type
       do
         if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
          (
      
            
            PROCESS_HDR_DELTA $loc ${current_process_id} 
           ) &
          process_delta_status=$?
          if [[ ${process_delta_status} -ne ${OK} ]]; then
           LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_HDR_DELTA" "${process_delta_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
           exit 1
          fi
         else
            while [ `jobs | wc -l` -ge ${SLOTS} ]
            do
              sleep 1
            done
             (
               PROCESS_HDR_DELTA $loc ${current_process_id} 
              ) &
             process_delta_status=$?
             if [[ ${process_delta_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_HDR_DELTA" "${process_delta_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
             fi
         fi
       done < $LOC_LIST
   ### Wait for all of the threads to complete
        wait
        while read loc loc_type
       do
         if [[ `jobs | wc -l` -lt ${SLOTS} ]]; then
          (
            PROCESS_DELTA $loc $loc_type ${current_process_id} 
           ) &
          process_delta_status=$?
          if [[ ${process_delta_status} -ne ${OK} ]]; then
           LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA" "${process_delta_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
           exit 1
          fi
         else
            while [ `jobs | wc -l` -ge ${SLOTS} ]
            do
              sleep 1
            done
             (
               PROCESS_DELTA $loc $loc_type ${current_process_id} 
              ) &
             process_delta_status=$?
             if [[ ${process_delta_status} -ne ${OK} ]]; then
              LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA" "${process_delta_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
              exit 1
             fi
         fi
       done < $LOC_LIST
       if [[ $? -eq ${OK} ]]; then
          INS_DATA_EXP_HIST ${mode}
          ins_dt_exp_st=$?
          if [[ ${ins_dt_exp_st} -ne ${OK} ]]; then
             LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "INS_DATA_EXP_HIST" "${ins_dt_exp_st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
          fi
          PROCESS_STG ${current_process_id}
          process_stg_status=$?
          if [[ ${process_stg_status} -ne ${OK} ]]; then
             LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_STG" "${process_stg_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
          else
             rm $LOC_LIST
             LOG_MESSAGE "export_itemloc.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
          fi
       fi
   fi
else
   opt_loc=${STR}
     echo "Updating the item_export_stg with process id:" ${current_process_id} >> $LOGFILE
      UPDATE_SINGLE_STORE_ITEM_EXPORT_STG ${opt_loc}
      st=$?
      if [[ ${st} -ne ${OK} ]]; then
         LOG_ERROR "Error while updating item_export_stg with current process_id" "UPDATE_SINGLE_STORE_ITEM_EXPORT_STG" "${st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      fi
   if [[ $2 = "full" ]]; then
      PROCESS_FULL $opt_loc
      process_full_status=$?
      if [[ ${process_full_status} -ne ${OK} ]]; then
         LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_FULL" "${process_full_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
      else
          INS_DATA_EXP_HIST ${mode}
          ins_dt_exp_st=$?
          if [[ ${ins_dt_exp_st} -ne ${OK} ]]; then
             LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "INS_DATA_EXP_HIST" "${ins_dt_exp_st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
          fi
          PROCESS_SINGLE_STORE_STG
          process_single_store_stg_status=$?
          if [[ ${process_single_store_stg_status} -ne ${OK} ]]; then
             LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_SINGLE_STORE_STG" "${process_single_store_stg_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
          else
             LOG_MESSAGE "export_itemloc.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
          fi
      fi
   fi
   if [[ $2 = "delta" ]]; then
      location_type="single"
      PROCESS_HDR_DELTA $opt_loc ${current_process_id}
      process_delta_status=$?
      if [[ ${process_delta_status} -ne ${OK} ]]; then
         LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA" "${process_delta_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
         exit 1
      else
         PROCESS_DELTA $opt_loc $location_type ${current_process_id}
         process_delta_status=$?
         if [[ ${process_delta_status} -ne ${OK} ]]; then
            LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_DELTA" "${process_delta_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
        else
          INS_DATA_EXP_HIST ${mode}
          ins_dt_exp_st=$?
          if [[ ${ins_dt_exp_st} -ne ${OK} ]]; then
             LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "INS_DATA_EXP_HIST" "${ins_dt_exp_st}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
          fi
          PROCESS_SINGLE_STORE_STG
          process_single_store_stg_status=$?
          if [[ ${process_single_store_stg_status} -ne ${OK} ]]; then
             LOG_ERROR "Job terminated with fatal error.Error recorded in ${ERRORFILE}" "PROCESS_SINGLE_STORE_STG" "${process_single_store_stg_status}" "${ERRORFILE}" "${LOGFILE}" $pgmName $pgmPID
             exit 1
          else
             LOG_MESSAGE "export_itemloc.ksh - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
          fi
        fi
      
      fi
   fi
fi