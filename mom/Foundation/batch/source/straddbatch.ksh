#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  straddbatch.ksh
#  Desc:  Store Add Batch
#
#-------------------------------------------------------------------------
. ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='runoraproc.ksh'
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

# File locations
LOGDIR="${MMHOME}/log"
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate

ERRINDFILE=err.ind
FATAL=255
NON_FATAL=1
OK=0
TRUE=1
FALSE=0

USAGE="Usage: $pgmName.$pgmExt <connect>"
#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------
# Test for the number of input arguments
if [ $# -lt 1 ]
then
   echo $USAGE
   exit ${NON_FATAL}
fi

connectStr=$1
USER=${connectStr%/*}
PROC_COMMAND="coresvc_store_add_sql.add_store_batch"
echo "Executing following proc ..."
echo $PROC_COMMAND
#Disabling automatic shell expansion of asterisks etc so that
#when sqlplus returns asterisk it is not automatically expanded
set -f
sqlReturn=`echo "set feedback off;
   set heading off;
   set term off;
   set verify off;
   set serveroutput on size 1000000;
   WHENEVER SQLERROR EXIT ${FATAL}
   DECLARE
   BEGIN
      ${PROC_COMMAND};
      COMMIT;
   END;
   /
   "  | sqlplus -s ${connectStr}`

if [[ $? -ne ${OK} ]]; then
   LOG_ERROR "${sqlReturn}" ${pgmName} ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
   return ${FATAL}
else
   LOG_MESSAGE "Successfully executed proc : ${PROC_COMMAND}" ${pgmName} ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
fi

return ${OK}
