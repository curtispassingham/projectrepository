LOAD DATA
APPEND
INTO TABLE svc_wf_cost_tmpl_upld_fhead
WHEN FILE_RECORD_DESCRIPTOR = 'FHEAD'
FIELDS TERMINATED BY '|'
OPTIONALLY ENCLOSED BY '"' TRAILING NULLCOLS
  
  (
     PROCESS_ID              "COSTTMPL_UPLD_PROCESS_ID_SEQ.NEXTVAL",
     SEQ_NO                  position(1),
     FILE_RECORD_DESCRIPTOR,
     FILE_LINE_ID            "nvl (:FILE_LINE_ID,1)",
     FILE_TYPE,
     FILE_CREATE_DATE        "nvl (:FILE_CREATE_DATE,sysdate)",      
     STATUS                  constant "N",
     ERROR_MSG               constant ""
  )
  
INTO TABLE svc_wf_cost_tmpl_upld_thead
WHEN FILE_RECORD_DESCRIPTOR = 'THEAD'
  FIELDS TERMINATED BY '|'
  OPTIONALLY ENCLOSED BY '"' TRAILING NULLCOLS
  
  (
     PROCESS_ID              "COSTTMPL_UPLD_PROCESS_ID_SEQ.CURRVAL",
     SEQ_NO                  position(1),
     CHUNK_ID                constant "",
     FILE_RECORD_DESCRIPTOR,
     FILE_LINE_ID            "nvl (:FILE_LINE_ID,1)",        
     MESSAGE_TYPE            "lower (:MESSAGE_TYPE)",
     TEMPLATE_ID,
     TEMPLATE_DESC,
     FIRST_APPLIED,
     PERCENTAGE,
     COST,
     FINAL_COST,
     STATUS                  constant "N",
     ERROR_MSG               constant ""
  )
INTO TABLE svc_wf_cost_tmpl_upld_tdetl
WHEN FILE_RECORD_DESCRIPTOR = 'TDETL'
  FIELDS TERMINATED BY '|'
  OPTIONALLY ENCLOSED BY '"' TRAILING NULLCOLS
  
  (
     PROCESS_ID              "COSTTMPL_UPLD_PROCESS_ID_SEQ.CURRVAL",
     SEQ_NO                  position(1),
     FILE_RECORD_DESCRIPTOR,
     FILE_LINE_ID            "nvl (:FILE_LINE_ID,1)",
     MESSAGE_TYPE            "lower (:MESSAGE_TYPE)",
     DEPT,
     CLASS,
     SUBCLASS,
     ITEM,
     LOCATION,
     START_DATE,
     END_DATE,
     NEW_START_DATE,
     NEW_END_DATE,
     COST_COMP_ID,
     STATUS                  constant "N",
     ERROR_MSG               constant ""
  )
INTO TABLE svc_wf_cost_tmpl_upld_ttail
WHEN FILE_RECORD_DESCRIPTOR = 'TTAIL'
  FIELDS TERMINATED BY '|'
  OPTIONALLY ENCLOSED BY '"' TRAILING NULLCOLS
  
  (
     PROCESS_ID              "COSTTMPL_UPLD_PROCESS_ID_SEQ.CURRVAL",
     SEQ_NO                  position(1),
     FILE_RECORD_DESCRIPTOR,
     FILE_LINE_ID            "nvl (:FILE_LINE_ID,1)",
     TRAN_RECORD_COUNTER,
     STATUS                  constant "N",
     ERROR_MSG               constant ""
  )
  
INTO TABLE svc_wf_cost_tmpl_upld_ftail
WHEN FILE_RECORD_DESCRIPTOR = 'FTAIL'
  FIELDS TERMINATED BY '|'
  OPTIONALLY ENCLOSED BY '"' TRAILING NULLCOLS
  
  (
     PROCESS_ID              "COSTTMPL_UPLD_PROCESS_ID_SEQ.CURRVAL",
     SEQ_NO                  position(1),
     FILE_RECORD_DESCRIPTOR,
     FILE_LINE_ID            "nvl (:FILE_LINE_ID,1)",
     FILE_RECORD_COUNTER,
     STATUS                  constant "N",
     ERROR_MSG               constant ""
  )