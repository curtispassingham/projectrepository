#! /bin/ksh
#---------------------------------------------------------------------------------------------
#  File:  batch_rfmvcurrconv.ksh
#
#  Desc:  UNIX shell script to Refresh the Materialized
#         view MV_CURRENCY_CONVERSION_RATES
#---------------------------------------------------------------------------------------------
USAGE="Usage: `basename $0` <connect>"

# Test for the number of input arguments
if [ $# -ne 1 ]
then
   echo $USAGE
   exit 1
fi

CONNECT=$1

logfile="${MMHOME}/log/`basename $0 .ksh`_`date +"%h_%d"`.log"

[ -f $logfile ] && rm $logfile

$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >$logfile
SET HEAD OFF
SET AUTOPRINT OFF
SET SERVEROUTPUT ON

WHENEVER SQLERROR EXIT 255
WHENEVER OSERROR  EXIT 255

variable errcode number ;

DECLARE
   
   I_error_message  VARCHAR2(2000);

BEGIN

   :errcode := 0;

   -- Refresh the mv_currency_conversion_rate materialized view
   if REFRESH_MV_CURR_CONV_RATES(I_error_message) = FALSE then
      DBMS_OUTPUT.PUT_LINE(I_error_message);   
      :errcode:= 255;
   end if;

EXCEPTION
   WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('SQLCODE,SQLERRM'||SQLCODE ||','||SQLERRM);   
      :errcode:= 255;
   
END;
/
exit :errcode;

EOF

return_code=$?

if [ ${return_code} -ne 0 ]
then
   echo "" >> ${logfile}
   echo "ERROR while Refreshing the Materialized view MV_CURRENCY_CONVERSION_RATES" >> ${logfile}
   echo "" >> ${logfile}
   exit 1
fi

# Remove the temporary log file
rm -f ${logfile}

exit 0
