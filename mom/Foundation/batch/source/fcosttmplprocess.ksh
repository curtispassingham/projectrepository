#! /bin/ksh
#-------------------------------------------------------------------------------------
#  File:  fcosttmplprocess.ksh
#
#  Desc:  This shell script updates the RMS cost template table tables with uploaded 
#         cost templates and their relationships. The cost template information is read
#         from SVC_WF_COST_TMPL_UPLD_* tables.
#         These tables were populated by a previous batch (fcosttmplupld.ksh).
#
#         The following are done in this script
#         1. Retrieve the maximum number of concurrent threads that can run for this
#            batch. This is retrieved from the RMS_PLSQL_BATCH_CONFIG table.
#         2. Get the process ID for the file uploaded in the previous batch.
#            This is retrieved from the SVC_WF_COST_TMPL_UPLD_STATUS table.
#         3. Get the chunk IDs to be processed. This is retrieved from the
#            SVC_WF_COST_TMPL_UPLD_STATUS table.
#         4. Call the core cost template process PL/SQL package for each chunk ID.
#-------------------------------------------------------------------------------------

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='fcosttmplprocess.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string> <input file>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <input file>       Input file. Can include directory path.
"
}

#-------------------------------------------------------------------------
# Function Name: GET_MAX_THREADS
# Purpose      : Retrieves the maximum number of concurrent threads
#                that can be run for this batch.
#-------------------------------------------------------------------------
function GET_MAX_THREADS
{
   parallelThreads=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select max_concurrent_threads
     from rms_plsql_batch_config
    where program_name = 'CORESVC_WF_COST_TMPL_UPLD_SQL';
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${parallelThreads}" "GET_MAX_THREADS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned number of parallel threads
   if [ -z ${parallelThreads} ]; then
      LOG_ERROR "Unable to retrieve the number of threads from the RMS_PLSQL_BATCH_CONFIG table." "GET_MAX_THREADS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - The number of concurrent threads for processing is ${parallelThreads}." "GET_MAX_THREADS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------------------
# Function Name: GET_PROCESS_ID
# Purpose      : Retrieves the process id from the SVC_WF_COST_TMPL_UPLD_STATUS table
#-------------------------------------------------------------------------------------
function GET_PROCESS_ID
{  
   typeset filewithPath=$1
   
   CostTmplProcessId=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
set pages 0
   select max(process_id)
     from svc_wf_cost_tmpl_upld_status
    where upper(trim(reference_id)) = upper(trim('${filewithPath}'))
      and status = 'N';
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${CostTmplProcessId}" "GET_PROCESS_ID" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned process ID
   if [ -z ${CostTmplProcessId} ]; then
      LOG_ERROR "Unable to retrieve the process ID for the file ${inputFileNoPath}." "GET_PROCESS_ID" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Process ID is ${CostTmplProcessId}." "GET_PROCESS_ID" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: GET_CHUNKS
# Purpose      : populates thre restartArr array..
#-------------------------------------------------------------------------
function GET_CHUNKS
{
   # Retrieve chunks to process into an indexed array
   set -A restartArr `$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select distinct chunk_id
     from svc_wf_cost_tmpl_upld_status
    where process_id = ${CostTmplProcessId}
      and status = 'N'
      and chunk_id <> 0
      order by chunk_id;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${restartArr}" "GET_CHUNKS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned restart array
   if [ -z ${restartArr} ]; then
      LOG_ERROR "Unable to retrieve the chunk IDs for the file ${inputFileNoPath}." "GET_CHUNKS" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Chunk IDs retrieved." "GET_CHUNKS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#---------------------------------------------------------------------------------
# Function Name: PROCESS_COST_TMPL_UPLD
# Purpose      : Function to process the validated records in the parameter tables
#                Processing is done chunk by chunk
#---------------------------------------------------------------------------------
function PROCESS_COST_TMPL_UPLD
{
   chunkVal=$1

   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN
         if NOT CORESVC_WF_COST_TMPL_UPLD_SQL.PROCESS_COST_TMPL_UPLD(:GV_script_error,
                                                             ${CostTmplProcessId},
                                                             ${chunkVal}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "${inputFile} - Function call PROCESS_COST_TMPL_UPLD Process Id: ${CostTmplProcessId}, Chunk ID ${chunkVal} failed." >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFile} - Process Id: ${CostTmplProcessId}, Chunk ID ${chunkVal} successfully processed." "PROCESS_COST_TMPL_UPLD" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: WRITE_FILE_PROCESS_STATUS
# Purpose      : Update the status of the processed file records to 'P'
#-------------------------------------------------------------------------
function WRITE_FILE_PROCESS_STATUS
{
   ProcessId=$1

   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN
         if NOT CORESVC_WF_COST_TMPL_UPLD_SQL.WRITE_FILE_PROCESS_STATUS(:GV_script_error,
                                                             ${ProcessId}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "${inputFile} - Function call WRITE_FILE_PROCESS_STATUS Process Id: ${CostTmplProcessId}, Chunk ID ${chunkVal} failed." >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFile} - Process Id: ${CostTmplProcessId}, Chunk ID ${chunkVal} successfully processed." "WRITE_FILE_PROCESS_STATUS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [ $# -lt 2 ]
then
   USAGE
   exit ${NON_FATAL}
fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
USER=${connectStr%/*}
inputFile=$2
inputFileNoPath=${inputFile##*/}    # remove the path
inputfile_ext=${inputFile##*.}
LOG_MESSAGE "fcosttmplprocess.ksh started by ${USER} for file ${inputFileNoPath}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

$ORACLE_HOME/bin/sqlplus -s $connectStr <<EOF >>$ERRORFILE
EOF

# Get the maximum concurrent threads from the configuration table
GET_MAX_THREADS

# Get the process id from the status table for the input file
GET_PROCESS_ID ${inputFileNoPath}

# Retrieve chunks to processinto an indexed array
GET_CHUNKS

# Iterate through the chunk IDs and call the core logic for each
arr_index=0
while [ ${arr_index} -lt  ${#restartArr[*]} ]
do
   if [ `jobs | wc -l` -lt ${parallelThreads} ]
   then
      (
         PROCESS_COST_TMPL_UPLD ${restartArr[${arr_index}]}
      ) &
   else
      # Loop until a thread becomes available
      while [ `jobs | wc -l` -ge ${parallelThreads} ]
      do
         sleep 1
      done
      (
         PROCESS_COST_TMPL_UPLD ${restartArr[${arr_index}]}
      ) &
   fi
   let arr_index=${arr_index}+1;
done

# Wait for all of the threads to complete
wait

WRITE_FILE_PROCESS_STATUS ${CostTmplProcessId}

LOG_MESSAGE "fcosttmplprocess.ksh finished for ${USER} for file ${inputFile}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

if [ `wc -l $ERRORFILE | awk '{print $1}'` -eq 0 ]
then
   rm -f $ERRORFILE
fi

exit ${OK}
