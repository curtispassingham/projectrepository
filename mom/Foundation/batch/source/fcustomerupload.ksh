#! /bin/ksh
#-------------------------------------------------------------------------------------
#  File:  fcustomerupload.ksh
#
#  Desc:  This shell script uploads franchise customer groups and franchise customers 
#         information from FCUST files into the SVC_FCUSTUPLD_* tables.
#         These tables serves as the interface by which the RMS core franchise 
#         customer upload process updates the system with the inserts/updates and
#         deletes of franchise customer groups and customers from the FCUST files.
#
#         The following are done in this script
#         1. Check if the input file exists and have appropriate access.
#         2. Creates a new input file from the existing with the addition
#            of fcust_seq_no. 
#            The fcust_seq_no is a unique sequence number given to each 
#            THEAD record in the FCUST file.The corresponding TDETL and 
#            TTAIL will have the same sequence number.
#            This is used to uniquely identify a transaction in the 
#            franchise customer upload process.
#         3. SQL Load (sqlldr) loads the input file into the SVC_FCUSTUPLD_* tables.
#            A fatal error from sqlldr will halt the process. Rejected records
#            is a non-fatal error and will be continue processing.
#         3. Retrieve the process ID from the paramater tables. This will 
#            serve as a key for succeeding processing. One file uploaded will have
#            one unique process ID.
#         4. Insert a record into the SVC_FCUSTUPLD_STATUS table with a chunk ID of
#            1. This record will be used to track the status of records loaded
#            for the particular process ID.
#         5. Validate the records on the SVC_FCUSTUPLD_* tables. 
#            A file level validation failure is a fatal error and will halt processing 
#            and the transcation level validation failure is a non fatal error and 
#            the validation will be continued with the other transcations.
#         6. For the FATAL errors, the return value will be 255 and for the non fatal 
#            rejections, the return value will be 1.
#-------------------------------------------------------------------------------------

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/rmsksh.lib

pgmName='fcustomerupload.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID

# File locations
CTLFILE="${MMHOME}/oracle/proc/src/fcustomerupload.ctl"
LOGDIR="${MMHOME}/log"
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"

# Initialize variables and constants
inputfile_ext=0
REJECT="R"

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string> <input file>

   <connect string>   Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <input file>       Input file. Can include directory path.

  "
}

#-------------------------------------------------------------------------
# Function Name: CHECK_INPUT_FILE_EXIST
# Purpose      : Check if the input file exists
#-------------------------------------------------------------------------
function CHECK_INPUT_FILE_EXIST
{

 typeset filewithPath=$1
   
 if [[ -n `ls -1 ${filewithPath} 2>/dev/null` ]]; then
    echo ${TRUE}
 else
    LOG_ERROR "Cannot find input file ${filewithPath}." "CHECK_INPUT_FILE_EXIST" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
    exit ${FATAL}
 fi
   
 return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: CREATE_NEW_INPUT_FILE
# Purpose      : Creates a new input file with an extra input fcust_seq_no. 
#-------------------------------------------------------------------------
function CREATE_NEW_INPUT_FILE
{
typeset filewithPath=$1
awk '
BEGIN {
fcust_seq_no=0;
}
{
if (substr($0,1,5) == "THEAD") fcust_seq_no = fcust_seq_no+1;
if (substr($0,1,5) == "FTAIL") fcust_seq_no = 0;
  print fcust_seq_no"|"$0 > var1;
}
' var1=${filewithPath}.new ${filewithPath}
}
  
#-------------------------------------------------------------------------
# Function Name: GET_PROCESS_ID
# Purpose      : Retrieves the process_id from the paramater tables
#                for file loaded in the LOAD_FILE() function.
#-------------------------------------------------------------------------
function GET_PROCESS_ID
{
   fcustProcessId=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select MAX(process_id)
     from svc_fcustupld_fhead
    where status = 'N';
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${fcustProcessId}" "GET_PROCESS_ID" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned franchise customer process id
   if [ -z ${fcustProcessId} ]; then
      LOG_ERROR "FHEAD Missing in the input file.Cannot retrieve process id for file $filewithPath." "GET_PROCESS_ID" ${FATAL} ${ERRORFILE} ${pgmName}
      exit ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: LOAD_FILE
# Purpose      : SQL Load the input file into the parameter tables
#-------------------------------------------------------------------------
function LOAD_FILE
{

   dtStamp=`date +"%G%m%d%H%M%S"`
   sqlldrFile=${inputFile##*/}
   sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
   sqlldr ${connectStr} silent=feedback,header \
      control=${CTLFILE} \
      log=${sqlldrLog} \
      data=${inputFile}.new \
      bad=${MMHOME}/log/$sqlldrFile.bad \
      discard=${MMHOME}/log/$sqlldrFile.dsc \
      errors=0 \
      DISCARDMAX=1 \
      ROWS=100000
   sqlldr_status=$?
   
   # Check execution status
   if [[ $sqlldr_status != ${OK} ]]; then
      LOG_ERROR "Error while loading file ${filewithPath}. See ${sqlldrLog} for details." "LOAD_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      # Check log file for sql loader errors
      if [[ `cat ${sqlldrLog} | grep -x "  0 Rows not loaded due to data errors."` != ""
         && `cat ${sqlldrLog} | grep -x "  0 Rows not loaded because all fields were null."` != "" ]];
      then
         LOG_MESSAGE "${inputFileNoPath} - Completed loading file to the franchise customer upload parameter tables." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
      else
         LOG_MESSAGE "${inputFileNoPath} - Some rows not loaded. See ${sqlldrLog} for details." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
         exit ${NON_FATAL}
      fi
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Wrapper for PL/SQL package calls
#-------------------------------------------------------------------------
function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      return ${FATAL}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: INIT_STATUS
# Purpose      : Insert a record into the svc_fcustupld_status table
#-------------------------------------------------------------------------
function INIT_STATUS
{
   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT CORESVC_FCUSTOMER_UPLOAD_SQL.INITIALIZE_PROCESS_STATUS(:GV_script_error,
                                                                       ${fcustProcessId},
                                                                      '${inputFileNoPath}') then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                 O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}
   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function call INITIALIZE_PROCESS_STATUS Process Id: ${fcustProcessId} failed." >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Status record successfully created. Process Id is ${fcustProcessId}." "INIT_STATUS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: VALIDATE_FATAL
# Purpose      : Perform validation on loaded tables.
#              : Validation failure will result in a fatal error.
#-------------------------------------------------------------------------
function VALIDATE_FATAL
{
   sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN

         if NOT CORESVC_FCUSTOMER_UPLOAD_SQL.VALIDATE_FATAL(:GV_script_error,
                                                            ${fcustProcessId}) then
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                  O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function call VALIDATE_FATAL Process Id: ${fcustProcessId} failed." >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Successfully validated: Process Id ${fcustProcessId}." "VALIDATE_FATAL" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: CHECK_FOR_REJECTS
# Purpose      : Logs any non-fatal validation failures
#-------------------------------------------------------------------------
function CHECK_FOR_REJECTS
{
   Invtransactions=`$ORACLE_HOME/bin/sqlplus -s $connectStr  <<EOF
   set pause off
   set echo off
   set heading off
   set feedback off
   set verify off
   set pages 0
   select 'Y'
     from svc_fcustupld_thead
    where process_id = ${fcustProcessId}
      and status = '${REJECT}'
      and rownum = 1;
   exit;
   EOF`
   
   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${fcustProcessId}" "GET_PROCESS_ID" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
   
   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: VALIDATE_TRANSACTIONS
# Purpose      : Perform validation on loaded tables.
#              : Validation failure will result in a non fatal rejection.
#-------------------------------------------------------------------------
function VALIDATE_TRANSACTIONS
{
  sqlTxt="
      DECLARE
         O_key             VARCHAR2(255);
         FUNCTION_ERROR    EXCEPTION;
      BEGIN
      
        if NOT CORESVC_FCUSTOMER_UPLOAD_SQL.VALIDATE_PARAMETER_TABLES(:GV_script_error,
                                                                      ${fcustProcessId}) then
             
            
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            if SQL_LIB.PARSE_MSG(:GV_script_error,
                                  O_key) = FALSE then
               NULL;
            end if;
            :GV_return_code := ${FATAL};
         when OTHERS then
             ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   
    
     EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "${inputFileNoPath} - Function call VALIDATE_TRANSACTIONS Process Id: ${fcustProcessId} failed." >>${ERRORFILE}
      exit ${FATAL}
   else
      LOG_MESSAGE "${inputFileNoPath} - Successfully validated: Process Id ${fcustProcessId}." "VALIDATE_TRANSACTIONS" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi

   return ${OK}
}

#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [ $# -lt 2 ]
then
   USAGE
   exit ${NON_FATAL}
fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
USER=${connectStr%/*}
inputFile=$2
inputFileNoPath=${inputFile##*/}    # remove the path
inputfile_ext=${inputFile##*.}
LOG_MESSAGE "fcustomerupload.ksh started by ${USER} for file ${inputFileNoPath}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

$ORACLE_HOME/bin/sqlplus -s $connectStr <<EOF >>$ERRORFILE
EOF

# Check if there are input files to process, if so load the staging tables.
#if [[ $(CHECK_INPUT_FILE_EXIST ${inputFile}) -eq ${TRUE} ]]; then

#   CONVERT_FILE $inputFile
   
 #  LOAD_FILE
 #  if [[ $? -ne ${OK} ]]; then
  #    exit ${FATAL}
 #  fi
#fi

# Check if input file exists.
  CHECK_INPUT_FILE_EXIST $inputFile

#Create new input file with extra identifier fcust_seq_no.
  CREATE_NEW_INPUT_FILE $inputFile
 
# Load the franchise customer upload parameter tables
  LOAD_FILE

# Retrieve the process id for the loaded file
  GET_PROCESS_ID

# Insert entry into the status table to track this upload
  INIT_STATUS

# Validate the records loaded into the parameter tables for FATAL errors.
  VALIDATE_FATAL
  
#Validate the records loaded into the parameter tables for NON FATAL rejections.
  VALIDATE_TRANSACTIONS

# Check if there are invalid records
  CHECK_FOR_REJECTS

# Exit with NON FATAL error in case of Rejects.
  if [[ ${Invtransactions} = 'Y' ]]; then
     echo "Terminated with rejected records. Please check the parameter tables."
     LOG_MESSAGE "fcustomerupload.ksh terminated with rejected records for ${USER} for file ${inputFileNoPath}.Refer to the bad files and parameter tables. " "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
     exit ${NON_FATAL}
   else
     echo "fcustomerupload.ksh finished successfully for file ${inputFileNoPath}."
     LOG_MESSAGE "fcustomerupload.ksh finished for ${USER} for file ${inputFileNoPath}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
  fi

if [ `wc -l $ERRORFILE | awk '{print $1}'` -eq 0 ]
then
   rm -f $ERRORFILE
fi
exit ${OK}
