/*----------------------------------------------------------------------------------*\
 | CONVERT_TO_PRIMARY() will convert a given amount from the idnt's currency
 |                      into primary currency.
 |
 | Parameters:
 |          ls_idnt : The location, vendor, or external finisher whose
 |                    curreny is to be converted.
 |      ls_idnt_type: Indicator telling the program what kind of
 |                    location/transaction we're dealing with:
 |                       "S" = store
 |                       "W" = warehouse
                         "E" = external finisher 
 |                       "V" = vendor (supplier)
 |          ls_date: The date of the transaction being processed, to be used in
 |                   determining the effective date for the rate to be returned.
 |        ld_amount: The amount to be converted.  This is passed as a pointer so
 |                   that the value can be directly manipulated by the function.
\*----------------------------------------------------------------------------------*/
int convert_to_primary(char    *ls_idnt,
                       char    *ls_idnt_type,
                       char    *ls_date,
                       double  *ld_amount);
/*----------------------------------------------------------------------------------*\
 | These functions return currency rates for specific idnts, based on type.
 | If the calling program needs to get a rate rather than directly modifying an
 | amount, one of these functions should be used.
\*----------------------------------------------------------------------------------*/
int convert_store(char    *ls_store,
                  char    *ls_st_date,
                  double  *ld_st_rate);

int convert_wh(char    *ls_wh,
               char    *ls_wh_date,
               double  *ld_wh_rate);

int convert_external_finisher(char    *ls_external_finisher,
                              char    *ls_ex_date,
                              double  *ld_ex_rate);                              

int convert_vendor(char    *ls_vendor,
                   char    *ls_v_date,
                   double  *ld_v_rate);


