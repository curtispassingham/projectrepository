--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE ADDED:				CHAIN_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TYPE
--------------------------------------
PROMPT Creating Table 'CHAIN_TL'
CREATE TABLE CHAIN_TL(
LANG NUMBER(6) NOT NULL,
CHAIN NUMBER(10) NOT NULL,
CHAIN_NAME VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE CHAIN_TL is 'This is the translation table for CHAIN table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN CHAIN_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN CHAIN_TL.CHAIN is 'Contains the number which uniquely identifies the chain.'
/

COMMENT ON COLUMN CHAIN_TL.CHAIN_NAME is 'Contains the name of the chain which, along with the chain number, identifies the chain.'
/

COMMENT ON COLUMN CHAIN_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN CHAIN_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN CHAIN_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN CHAIN_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE CHAIN_TL ADD CONSTRAINT PK_CHAIN_TL PRIMARY KEY (
LANG,
CHAIN
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE CHAIN_TL
 ADD CONSTRAINT CHNT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE CHAIN_TL ADD CONSTRAINT CHNT_CHN_FK FOREIGN KEY (
CHAIN
) REFERENCES CHAIN (
CHAIN
)
/

