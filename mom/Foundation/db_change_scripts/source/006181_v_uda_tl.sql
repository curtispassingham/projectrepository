CREATE OR REPLACE FORCE VIEW V_UDA_TL (UDA_ID, UDA_DESC, LANG ) AS
SELECT  b.uda_id,
        case when tl.lang is not null then tl.uda_desc else b.uda_desc end uda_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  UDA b,
        UDA_TL tl
 WHERE  b.uda_id = tl.uda_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_UDA_TL is 'This is the translation view for base table UDA. This view fetches data in user langauge either from translation table UDA_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_UDA_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_UDA_TL.UDA_ID is 'This field contains a unique number identifying the User Defined Attribute.'
/

COMMENT ON COLUMN V_UDA_TL.UDA_DESC is 'This field contains a description of the User-Defined Attribute.'
/

