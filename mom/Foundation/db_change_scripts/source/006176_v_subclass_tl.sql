CREATE OR REPLACE FORCE VIEW V_SUBCLASS_TL (DEPT, CLASS, SUBCLASS, SUB_NAME, LANG ) AS
SELECT  b.dept,
        b.class,
        b.subclass,
        case when tl.lang is not null then tl.sub_name else b.sub_name end sub_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SUBCLASS b,
        SUBCLASS_TL tl
 WHERE  b.dept = tl.dept (+)
   AND  b.class = tl.class (+)
   AND  b.subclass = tl.subclass (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SUBCLASS_TL is 'This is the translation view for base table SUBCLASS. This view fetches data in user langauge either from translation table SUBCLASS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SUBCLASS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SUBCLASS_TL.DEPT is 'Contains the department number of which the subclass is a member.'
/

COMMENT ON COLUMN V_SUBCLASS_TL.CLASS is 'Contains the class number of which the subclass is a member.'
/

COMMENT ON COLUMN V_SUBCLASS_TL.SUBCLASS is 'Contains the number which uniquely identifies the subclass.'
/

COMMENT ON COLUMN V_SUBCLASS_TL.SUB_NAME is 'Contains the name of the subclass which, along with the subclass number, uniquely identifies the subclass.'
/

