--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW CREATED:              V_IINDUCT_SEARCH_RMS 
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT Creating View 'V_IINDUCT_SEARCH_RMS'
CREATE OR REPLACE FORCE EDITIONABLE VIEW V_IINDUCT_SEARCH_RMS ("ITEM", "ITEM_DESC", "ITEM_TYPE", "SUPPLIER", "DEPT", "CLASS", "SUBCLASS", "STATUS", "ITEM_LEVEL", "TRAN_LEVEL", "SELLABLE_IND", "ORDERABLE_IND", "INVENTORY_IND", "BRAND_NAME", "DEPT_NAME", "CLASS_NAME", "SUBCLASS_NAME", "SUPPLIER_NAME", "VPN") AS 
SELECT t.item item
      ,t.item_desc item_desc
      ,t.item_type
      ,psv.supplier
      ,t.dept dept
      ,t.class class
      ,t.subclass subclass
      ,t.status status
      ,t.item_level item_level
      ,t.tran_level tran_level
      ,t.sellable_ind sellable_ind
      ,t.orderable_ind orderable_ind
      ,t.inventory_ind inventory_ind
      ,t.brand_name  brand_name
      ,t.dept_name   dept_name
      ,t.class_name  class_name
      ,t.subclass_name subclass_name
      ,psv.sup_name supplier_name
      ,psv.vpn         vpn
 FROM ( select iem.item item
                    ,iem.item_desc item_desc
                    ,CASE
                       WHEN iem.simple_pack_ind = 'Y' THEN
                            'S'
                       WHEN iem.pack_ind = 'Y' AND iem.simple_pack_ind = 'N' THEN
                            'C'
                       WHEN iem.deposit_item_type is not null THEN
                            iem.deposit_item_type
                       WHEN iem.item_xform_ind = 'Y' AND iem.orderable_ind = 'Y' THEN
                            'O'
                       WHEN iem.item_xform_ind = 'Y' AND iem.orderable_ind = 'N' THEN
                            'L'
                       WHEN dep.purchase_type IN (1, 2) THEN
                            'I'
                       ELSE 'R'
                     END item_type
                    ,dep.dept dept
                    ,iem.class class
                    ,iem.subclass subclass
                    ,iem.status status
                    ,iem.item_level item_level
                    ,iem.tran_level tran_level
                    ,iem.sellable_ind sellable_ind
                    ,iem.orderable_ind orderable_ind
                    ,iem.inventory_ind inventory_ind
                    ,iem.brand_name  brand_name
                    ,dtl.dept_name   dept_name
                    ,cls.class_name  class_name
                    ,scls.sub_name   subclass_name
               FROM deps dep,
                    v_deps_tl dtl,
                    v_class_tl cls,
                    v_subclass_tl scls,
                    v_item_master iem
              WHERE iem.dept = dep.dept
                AND dep.dept = dtl.dept
                AND iem.class = cls.class
                AND iem.subclass = scls.subclass
                AND dtl.dept      = cls.dept
                AND cls.dept      = scls.dept
                AND cls.class     = scls.class) t
        , (
      select item,supplier,vpn,primary_supp_ind, sup_name 
      from 
      (
      select ies.item,ies.supplier,ies.vpn,ies.primary_supp_ind, s.sup_name
      , rank() over(partition by item order by primary_supp_ind desc) rank 
      from item_supplier ies, v_sups s
      where s.supplier(+) = ies.supplier
      )
      where rank = 1
      ) psv
WHERE t.item = psv.item (+)
/