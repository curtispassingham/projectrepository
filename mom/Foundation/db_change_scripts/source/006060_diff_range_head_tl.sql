--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       DIFF_RANGE_HEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE DIFF_RANGE_HEAD_TL(
LANG NUMBER(6) NOT NULL,
DIFF_RANGE NUMBER(4) NOT NULL,
DIFF_RANGE_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE DIFF_RANGE_HEAD_TL is 'This is the translation table for DIFF_RANGE_HEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN DIFF_RANGE_HEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN DIFF_RANGE_HEAD_TL.DIFF_RANGE is 'Contains the number used to uniquely identify the differentiator range.'
/

COMMENT ON COLUMN DIFF_RANGE_HEAD_TL.DIFF_RANGE_DESC is 'Contains the differentiator range description.'
/

COMMENT ON COLUMN DIFF_RANGE_HEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN DIFF_RANGE_HEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN DIFF_RANGE_HEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN DIFF_RANGE_HEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE DIFF_RANGE_HEAD_TL ADD CONSTRAINT PK_DIFF_RANGE_HEAD_TL PRIMARY KEY (
LANG,
DIFF_RANGE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE DIFF_RANGE_HEAD_TL
 ADD CONSTRAINT DRGHT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE DIFF_RANGE_HEAD_TL ADD CONSTRAINT DRGHT_DRGH_FK FOREIGN KEY (
DIFF_RANGE
) REFERENCES DIFF_RANGE_HEAD (
DIFF_RANGE
)
/

