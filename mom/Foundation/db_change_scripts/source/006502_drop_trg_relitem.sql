--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

PROMPT dropping Trigger 'TR_RELATED_ITEM_HEAD'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'TR_RELATED_ITEM_HEAD'
   AND OBJECT_TYPE ='TRIGGER';

if (L_table_exists != 0) then
      execute immediate 'DROP TRIGGER TR_RELATED_ITEM_HEAD';
  end if;
end;
/

PROMPT dropping Trigger 'TR_RELATED_ITEM_DETAIL'
DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'TR_RELATED_ITEM_DETAIL'
   AND OBJECT_TYPE ='TRIGGER';
   
if (L_table_exists != 0) then
      execute immediate 'DROP TRIGGER TR_RELATED_ITEM_DETAIL';
  end if;
end;
/
