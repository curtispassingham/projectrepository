--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_ITEM_SUPPLIER
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_ITEM_SUPPLIER"
CREATE TABLE DC_ITEM_SUPPLIER
(
   ITEM                          VARCHAR2(25),
   SUPPLIER                      NUMBER(10),
   PALLET_NAME                   VARCHAR2(6),
   CASE_NAME                     VARCHAR2(6),
   INNER_NAME                    VARCHAR2(6),
   DIRECT_SHIP_IND               VARCHAR2(1),
   VPN                           VARCHAR2(30),
   CONCESSION_RATE               NUMBER(12,4),
   SUPP_LABEL                    VARCHAR2(15),
   CONSIGNMENT_RATE              NUMBER(12,4),
   PRIMARY_SUPP_IND              VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_ITEM_SUPPLIER is 'This table is a staging table for data conversion and will hold data of ITEM_SUPPLIER.'
/
COMMENT ON COLUMN DC_ITEM_SUPPLIER.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_ITEM_SUPPLIER.SUPPLIER is 'This field contains the number of the supplier of the item.'
/
COMMENT ON COLUMN DC_ITEM_SUPPLIER.PALLET_NAME is 'Code referencing the name used by supplier to refer to the pallet. Valid codes are defined in the PALN code type.'
/
COMMENT ON COLUMN DC_ITEM_SUPPLIER.CASE_NAME is 'Code referencing the name used by supplier to refer to the case. Valid codes are defined in the CASN code type.'
/
COMMENT ON COLUMN DC_ITEM_SUPPLIER.INNER_NAME is 'Code referencing the name used by supplier to refer to the inner. Valid codes are defined in the INRN code type.'
/
COMMENT ON COLUMN DC_ITEM_SUPPLIER.DIRECT_SHIP_IND is 'This field will contain a value of Yes to indicate that any item asssociated with this supplier is eligible for a direct shipment from the supplier to the customer.'
/
COMMENT ON COLUMN DC_ITEM_SUPPLIER.VPN is 'This field contains the Vendor Product Number associated with this item.'
/
COMMENT ON COLUMN DC_ITEM_SUPPLIER.CONCESSION_RATE is 'The concession rate is the margin that a particular supplier receives for the sale of a concession item.'
/
COMMENT ON COLUMN DC_ITEM_SUPPLIER.SUPP_LABEL is 'This field will hold the supplier label for an item.'
/
COMMENT ON COLUMN DC_ITEM_SUPPLIER.CONSIGNMENT_RATE is 'This field contains the consignment rate for this item for the supplier.'
/
COMMENT ON COLUMN DC_ITEM_SUPPLIER.PRIMARY_SUPP_IND is 'This field Indicates whether this supplier is the primary supplier for the item.'
/