--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_VAT_CODES
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_VAT_CODES"
CREATE TABLE DC_VAT_CODES
(
   VAT_CODE             VARCHAR2(6),
   VAT_CODE_DESC        VARCHAR2(120)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_VAT_CODES is 'This table is a staging table for data conversion which will hold one row for each VAT code. The VAT code is used to determine which items are
subject to VAT tax.'
/
COMMENT ON COLUMN DC_VAT_CODES.VAT_CODE is 'This field contains the alphanumeric identification for the VAT code.'
/
COMMENT ON COLUMN DC_VAT_CODES.VAT_CODE_DESC is 'Contains a description identifying the VAT code.'
/
