--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	SEQUENCE ADDED:				PRICE_SEQUENCE
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       ADDING SEQUENCE
--------------------------------------
PROMPT Creating Sequence 'PRICE_SEQUENCE'
CREATE SEQUENCE PRICE_SEQUENCE 
INCREMENT BY 1
MAXVALUE 99999999999
MINVALUE 0
CACHE 5000
/
