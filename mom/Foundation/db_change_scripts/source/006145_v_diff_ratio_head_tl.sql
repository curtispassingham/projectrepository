CREATE OR REPLACE FORCE VIEW V_DIFF_RATIO_HEAD_TL (DIFF_RATIO_ID, DESCRIPTION, LANG ) AS
SELECT  b.diff_ratio_id,
        case when tl.lang is not null then tl.description else b.description end description,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  DIFF_RATIO_HEAD b,
        DIFF_RATIO_HEAD_TL tl
 WHERE  b.diff_ratio_id = tl.diff_ratio_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_DIFF_RATIO_HEAD_TL is 'This is the translation view for base table DIFF_RATIO_HEAD. This view fetches data in user langauge either from translation table DIFF_RATIO_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_DIFF_RATIO_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_DIFF_RATIO_HEAD_TL.DIFF_RATIO_ID is 'This field holds the identifier for the diff_ratio_head record.'
/

COMMENT ON COLUMN V_DIFF_RATIO_HEAD_TL.DESCRIPTION is 'This field holds the description of the size_ratio_id.'
/

