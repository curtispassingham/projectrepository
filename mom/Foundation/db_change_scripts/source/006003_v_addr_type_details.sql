--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_ADDR_TYPE_DETAILS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_ADDR_TYPE_DETAILS'
CREATE OR REPLACE FORCE VIEW V_ADDR_TYPE_DETAILS
("MODULE",
 "ADDRESS_TYPE",
 "TYPE_DESC",
 "PRIMARY_IND",
 "MANDATORY_IND")
AS (SELECT atm.module,
           aty.address_type,
           vatl.type_desc,
           atm.primary_ind,
           atm.mandatory_ind
      FROM add_type_module atm,
           add_type aty,
           v_add_type_tl vatl
     WHERE atm.address_type  = aty.address_type
       and vatl.address_type = aty.address_type)
/

COMMENT ON COLUMN V_ADDR_TYPE_DETAILS."MODULE" IS 'This column contains the code for the module that the address is attached to.'
/

COMMENT ON COLUMN V_ADDR_TYPE_DETAILS."ADDRESS_TYPE" IS 'This column contains the unique ID identifying the address type within Oracle.'
/

COMMENT ON COLUMN V_ADDR_TYPE_DETAILS."TYPE_DESC" IS 'This column contains the description of the address type. Values include: Returns, Business, Postal, Order, and Billing/Acct Payable.'
/

COMMENT ON COLUMN V_ADDR_TYPE_DETAILS."PRIMARY_IND" IS 'This column indicates if this address type is the primary address type for the module.'
/

COMMENT ON COLUMN V_ADDR_TYPE_DETAILS."MANDATORY_IND" IS 'This column indicates if this address type is the mandatory address type for the module.'
/


