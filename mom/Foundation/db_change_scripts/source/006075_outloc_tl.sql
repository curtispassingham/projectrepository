--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       OUTLOC_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE OUTLOC_TL(
LANG NUMBER(6) NOT NULL,
OUTLOC_TYPE VARCHAR2(6) NOT NULL,
OUTLOC_ID VARCHAR2(5) NOT NULL,
OUTLOC_DESC VARCHAR2(150) NOT NULL,
OUTLOC_ADD1 VARCHAR2(240) ,
OUTLOC_ADD2 VARCHAR2(240) ,
OUTLOC_CITY VARCHAR2(120) ,
CONTACT_NAME VARCHAR2(120) ,
OUTLOC_NAME_SECONDARY VARCHAR2(150) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE OUTLOC_TL is 'This is the translation table for OUTLOC table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN OUTLOC_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN OUTLOC_TL.OUTLOC_TYPE is 'This field holds the type of location, e.g., Discharge Port (DP) or Port of Lading (PL)'
/

COMMENT ON COLUMN OUTLOC_TL.OUTLOC_ID is 'This is a unique location identification used so that the location can be referenced in other modules.'
/

COMMENT ON COLUMN OUTLOC_TL.OUTLOC_DESC is 'A description or name of the location.'
/

COMMENT ON COLUMN OUTLOC_TL.OUTLOC_ADD1 is 'The street address of the location.'
/

COMMENT ON COLUMN OUTLOC_TL.OUTLOC_ADD2 is 'The second line of a street address.'
/

COMMENT ON COLUMN OUTLOC_TL.OUTLOC_CITY is 'This field holds the name of the city where the location exists.'
/

COMMENT ON COLUMN OUTLOC_TL.CONTACT_NAME is 'The name of a person that may be contacted at the particular location.'
/

COMMENT ON COLUMN OUTLOC_TL.OUTLOC_NAME_SECONDARY is 'Contains the secondary name of the outside location.'
/

COMMENT ON COLUMN OUTLOC_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN OUTLOC_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN OUTLOC_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN OUTLOC_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE OUTLOC_TL ADD CONSTRAINT PK_OUTLOC_TL PRIMARY KEY (
LANG,
OUTLOC_TYPE,
OUTLOC_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE OUTLOC_TL
 ADD CONSTRAINT OLCT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE OUTLOC_TL ADD CONSTRAINT OLCT_OLC_FK FOREIGN KEY (
OUTLOC_TYPE,
OUTLOC_ID
) REFERENCES OUTLOC (
OUTLOC_TYPE,
OUTLOC_ID
)
/

