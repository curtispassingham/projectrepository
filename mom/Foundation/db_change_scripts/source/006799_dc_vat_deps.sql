-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_VAT_DEPS
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table DC_VAT_DEPS
CREATE TABLE DC_VAT_DEPS
(
   DEPT                          NUMBER(4),
   VAT_REGION                    NUMBER(4),
   VAT_TYPE                      VARCHAR2(1),
   VAT_CODE                      VARCHAR2(6),
   REVERSE_VAT_IND               VARCHAR2(1)
)
 INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_VAT_DEPS is 'This table is a staging table for data conversion and will hold data for vat region and department.'
/
COMMENT ON COLUMN DC_VAT_DEPS.DEPT is 'Contains the number which uniquely identifies the department.'
/
COMMENT ON COLUMN DC_VAT_DEPS.VAT_REGION is 'Contains the number of the Value Added Tax region in which this store is contained.'
/
COMMENT ON COLUMN DC_VAT_DEPS.VAT_TYPE is 'Refer to VAT_ITEM.VAT_TYPE.'
/
COMMENT ON COLUMN DC_VAT_DEPS.VAT_CODE is 'The VAT code.'
/
COMMENT ON COLUMN DC_VAT_DEPS.REVERSE_VAT_IND is 'Indicates if items in the department are subject  to reverse charge VAT at the vat region. It is used to default the value to VAT_ITEM only.'
/