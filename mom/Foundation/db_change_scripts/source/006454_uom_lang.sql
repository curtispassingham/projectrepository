--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Dropping Table :                               UOM_LANG
----------------------------------------------------------------------------

whenever sqlerror exit failure

PROMPT Copying data from 'UOM_LANG' to 'UOM_CLASS_TL'
MERGE INTO UOM_CLASS_TL UCT USING ( select ul.lang, 
                                           ul.uom, 
                                           ul.uom_trans,
                                           ul.uom_desc_trans,
                                           ul.orig_lang_ind,
                                           ul.reviewed_ind
                                      from uom_lang ul,
                                           uom_class uc
                                     where ul.uom = uc.uom) USE_THIS
ON (    uct.lang = use_this.lang
    and uct.uom = use_this.uom)
WHEN MATCHED THEN 
    UPDATE  set uct.uom_trans = use_this.uom_trans, 
                uct.uom_desc_trans = use_this.uom_desc_trans, 
                uct.orig_lang_ind = use_this.orig_lang_ind, 
                uct.reviewed_ind = use_this.reviewed_ind, 
                uct.last_update_datetime = sysdate, 
                uct.last_update_id = user 
WHEN NOT MATCHED THEN 
    INSERT (lang, 
            uom, 
            uom_trans, 
            uom_desc_trans, 
            orig_lang_ind, 
            reviewed_ind, 
            create_datetime,
            create_id,
            last_update_datetime,
            last_update_id)
    VALUES (use_this.lang, 
            use_this.uom, 
            use_this.uom_trans, 
            use_this.uom_desc_trans, 
            use_this.orig_lang_ind, 
            use_this.reviewed_ind, 
            sysdate, 
            user, 
            sysdate, 
            user)
/

commit
/

PROMPT Dropping table 'UOM_LANG'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'UOM_LANG';

  if (L_table_exists != 0) then
      execute immediate 'DROP TABLE UOM_LANG';
  end if;
end;
/
