--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_STATE_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_STATE_TL'
CREATE TABLE SVC_STATE_TL
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
    DESCRIPTION VARCHAR2(120) ,
    COUNTRY_ID VARCHAR2(3) ,
    STATE VARCHAR2(3) ,
    LANG NUMBER(6,0) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE 
	)
	INITRANS 6
	TABLESPACE RETAIL_DATA
/


COMMENT ON TABLE SVC_STATE_TL IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in STATE_TL.'
/

COMMENT ON COLUMN SVC_STATE_TL.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_STATE_TL.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_STATE_TL.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_STATE_TL.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_STATE_TL.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_STATE_TL.STATE IS 'This column contains the state abbreviation for the address.'
/

COMMENT ON COLUMN SVC_STATE_TL.COUNTRY_ID IS 'Contains the unique code which identifies the country where the state belongs.'
/

COMMENT ON COLUMN SVC_STATE_TL.DESCRIPTION IS 'Contains the full name of the state.'
/

COMMENT ON COLUMN SVC_STATE_TL.LANG IS 'Contains the number which uniquely identifies a language.'
/

COMMENT ON COLUMN SVC_STATE_TL.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_STATE_TL.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_STATE_TL.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_STATE_TL.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_STATE_TL'           
ALTER TABLE SVC_STATE_TL
ADD CONSTRAINT SVC_STATE_TL_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX   
/

PROMPT Creating Unique Key on 'SVC_STATE_TL'
ALTER TABLE SVC_STATE_TL
ADD CONSTRAINT SVC_STATE_TL_UK UNIQUE ( COUNTRY_ID,STATE,LANG )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX   
/


	
	
	
