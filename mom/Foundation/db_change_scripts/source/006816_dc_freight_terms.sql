--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_FREIGHT_TERMS
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_FREIGHT_TERMS"
CREATE TABLE DC_FREIGHT_TERMS
(
   FREIGHT_TERMS             VARCHAR2(30),
   TERM_DESC                 VARCHAR2(240),
   START_DATE_ACTIVE         DATE,
   END_DATE_ACTIVE           DATE,
   ENABLED_FLAG              VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_FREIGHT_TERMS is 'This table is a staging table for data conversion and will hold data for FREIGHT_TERMS table.'
/
COMMENT ON COLUMN DC_FREIGHT_TERMS.FREIGHT_TERMS is 'Contains a number that uniquely identifies the freight terms.'
/
COMMENT ON COLUMN DC_FREIGHT_TERMS.TERM_DESC is 'Contains a description of the freight terms used in the system.'
/
COMMENT ON COLUMN DC_FREIGHT_TERMS.START_DATE_ACTIVE is 'Indicates the date for assigning an active date to the Freight Terms.'
/
COMMENT ON COLUMN DC_FREIGHT_TERMS.END_DATE_ACTIVE is 'Indicates the date for assigning an inactive date to the Freight Terms.'
/
COMMENT ON COLUMN DC_FREIGHT_TERMS.ENABLED_FLAG is 'Indicates whether the freight terms are valid or invalid within the respective application. The values would be either (Y)es or (N)o.'
/