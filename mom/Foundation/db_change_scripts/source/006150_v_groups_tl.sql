CREATE OR REPLACE FORCE VIEW V_GROUPS_TL (GROUP_NO, GROUP_NAME, LANG ) AS
SELECT  b.group_no,
        case when tl.lang is not null then tl.group_name else b.group_name end group_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  GROUPS b,
        GROUPS_TL tl
 WHERE  b.group_no = tl.group_no (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_GROUPS_TL is 'This is the translation view for base table GROUPS. This view fetches data in user langauge either from translation table GROUPS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_GROUPS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_GROUPS_TL.GROUP_NO is 'Contains the number which uniquely identifies the group.'
/

COMMENT ON COLUMN V_GROUPS_TL.GROUP_NAME is 'Contains the description which, along with the group number, identifies the group.'
/

