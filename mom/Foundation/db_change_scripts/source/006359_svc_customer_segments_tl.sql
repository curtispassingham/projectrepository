--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_CUSTOMER_SEGMENTS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

CREATE TABLE SVC_CUSTOMER_SEGMENTS_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, CUSTOMER_SEGMENT_ID NUMBER(10)
, CUSTOMER_SEGMENT_DESC VARCHAR2(120)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_CUSTOMER_SEGMENTS_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in CUSTOMER_SEGMENTS_TL.'
/

COMMENT ON COLUMN SVC_CUSTOMER_SEGMENTS_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_CUSTOMER_SEGMENTS_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_CUSTOMER_SEGMENTS_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_CUSTOMER_SEGMENTS_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_CUSTOMER_SEGMENTS_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_CUSTOMER_SEGMENTS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_CUSTOMER_SEGMENTS_TL.CUSTOMER_SEGMENT_ID is 'The customer segment id of a given customer segment type, referenced and attached to promotions in RPM. They will be available in RPM promotion UI screen for customer segment promotions.'
/

COMMENT ON COLUMN SVC_CUSTOMER_SEGMENTS_TL.CUSTOMER_SEGMENT_DESC is 'The description of customer segment id.'
/

ALTER TABLE SVC_CUSTOMER_SEGMENTS_TL
ADD CONSTRAINT SVC_CUSTOMER_SEGMENTS_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SVC_CUSTOMER_SEGMENTS_TL
ADD CONSTRAINT SVC_CUSTOMER_SEGMENTS_TL_UK UNIQUE
(LANG, CUSTOMER_SEGMENT_ID)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

