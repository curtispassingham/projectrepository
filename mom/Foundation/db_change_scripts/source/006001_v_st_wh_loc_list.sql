--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_ST_WH_LOC_LIST
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_ST_WH_LOC_LIST'
CREATE OR REPLACE FORCE VIEW V_ST_WH_LOC_LIST
("ID",
 "DESCRIPTION",
 "LOC_TYPE")
AS (SELECT loc_list as ID,
           loc_list_desc as DESCRIPTION,
           'SL' as LOC_TYPE
      FROM v_loc_list_head v
    UNION ALL
    SELECT store as ID,
           store_name as DESCRIPTION,
           'ST' as LOC_TYPE
      FROM v_store
    UNION ALL
    SELECT wh as ID,
           wh_name as DESCRIPTION,
           'WH' as LOC_TYPE
      FROM v_wh)
/

COMMENT ON COLUMN V_ST_WH_LOC_LIST."ID" IS 'The id that identifies the location.'
/

COMMENT ON COLUMN V_ST_WH_LOC_LIST."DESCRIPTION" IS 'The name of the location id.'
/

COMMENT ON COLUMN V_ST_WH_LOC_LIST."LOC_TYPE" IS 'Identifies the type of location the Id belongs to. Valid values are: SL, ST, WH.'
/
