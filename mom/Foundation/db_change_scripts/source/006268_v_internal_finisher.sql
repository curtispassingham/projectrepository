--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_INTERNAL_FINISHER
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_INTERNAL_FINISHER'

CREATE OR REPLACE FORCE VIEW V_INTERNAL_FINISHER ( FINISHER_ID, PHYSICAL_WH, TSF_ENTITY_ID, FINISHER_DESC, CURRENCY_CODE, ORG_UNIT_ID ) AS
SELECT WAH.WH FINISHER_ID
      ,WAH.PHYSICAL_WH PHYSICAL_WH
      ,WAH.TSF_ENTITY_ID TSF_ENTITY_ID
      ,V.WH_NAME FINISHER_DESC
      ,WAH.CURRENCY_CODE CURRENCY_CODE
      ,WAH.ORG_UNIT_ID ORG_UNIT_ID 
  FROM WH WAH,
       V_WH_TL V
 WHERE finisher_ind = 'Y'
   AND WAH.WH = V.WH
/

COMMENT ON TABLE V_INTERNAL_FINISHER IS 'This view extracts all the details for internal finishers. The finisher description is translated based on user language.'
/
