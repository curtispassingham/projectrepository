--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

----------------------------------------------------------
--       Drop Package         RELATED_ITEM_ORPOS_EXTRACT
----------------------------------------------------------
PROMPT dropping Package 'RELATED_ITEM_ORPOS_EXTRACT'

DECLARE
  L_table_exists number := 0;
BEGIN
   SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'RELATED_ITEM_ORPOS_EXTRACT'
   AND OBJECT_TYPE ='PACKAGE';
   
if (L_table_exists != 0) then
      execute immediate 'DROP PACKAGE RELATED_ITEM_ORPOS_EXTRACT';
  end if;
end;
/
