--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       COUNTRY_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE COUNTRY_TL(
LANG NUMBER(6) NOT NULL,
COUNTRY_ID VARCHAR2(3) NOT NULL,
COUNTRY_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE COUNTRY_TL is 'This is the translation table for COUNTRY table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN COUNTRY_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN COUNTRY_TL.COUNTRY_ID is 'Contains a number which uniquely identifies the country.'
/

COMMENT ON COLUMN COUNTRY_TL.COUNTRY_DESC is 'Contains the name of the country.'
/

COMMENT ON COLUMN COUNTRY_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN COUNTRY_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN COUNTRY_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN COUNTRY_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE COUNTRY_TL ADD CONSTRAINT PK_COUNTRY_TL PRIMARY KEY (
LANG,
COUNTRY_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE COUNTRY_TL
 ADD CONSTRAINT CTRYT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE COUNTRY_TL ADD CONSTRAINT CTRYT_CTRY_FK FOREIGN KEY (
COUNTRY_ID
) REFERENCES COUNTRY (
COUNTRY_ID
)
/

