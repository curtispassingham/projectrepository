--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       UDA_VALUES_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE UDA_VALUES_TL(
LANG NUMBER(6) NOT NULL,
UDA_ID NUMBER(5) NOT NULL,
UDA_VALUE NUMBER(5) NOT NULL,
UDA_VALUE_DESC VARCHAR2(250) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE UDA_VALUES_TL is 'This is the translation table for UDA_VALUES table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN UDA_VALUES_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN UDA_VALUES_TL.UDA_ID is 'This field contains a unique number identifying the User Defined Attribute.'
/

COMMENT ON COLUMN UDA_VALUES_TL.UDA_VALUE is 'This field contains a unique number identifying the User Defined Attribute value for the UDA. A UDA can have multiple values. For example, Color can be a UDA and it can have different values like Green, Red, Blue, etc.'
/

COMMENT ON COLUMN UDA_VALUES_TL.UDA_VALUE_DESC is 'This field contains a description of the UDA value.'
/

COMMENT ON COLUMN UDA_VALUES_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN UDA_VALUES_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN UDA_VALUES_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN UDA_VALUES_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE UDA_VALUES_TL ADD CONSTRAINT PK_UDA_VALUES_TL PRIMARY KEY (
LANG,
UDA_ID,
UDA_VALUE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE UDA_VALUES_TL
 ADD CONSTRAINT UVT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE UDA_VALUES_TL ADD CONSTRAINT UVT_UV_FK FOREIGN KEY (
UDA_ID,
UDA_VALUE
) REFERENCES UDA_VALUES (
UDA_ID,
UDA_VALUE
)
/

