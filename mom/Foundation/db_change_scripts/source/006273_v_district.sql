--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_DISTRICT
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_DISTRICT'
CREATE OR REPLACE FORCE VIEW V_DISTRICT
 (CHAIN
 ,AREA
 ,REGION
 ,DISTRICT
 ,DISTRICT_NAME)
 AS SELECT ARE.CHAIN CHAIN
          ,REG.AREA AREA
          ,DIS.REGION REGION
          ,DIS.DISTRICT DISTRICT
          ,V.DISTRICT_NAME DISTRICT_NAME
FROM AREA ARE
    ,REGION REG
    ,DISTRICT DIS
    ,V_DISTRICT_TL V
  WHERE reg.region=dis.region
    AND are.area=reg.area
    AND v.district = dis.district
/


COMMENT ON TABLE V_DISTRICT IS 'This view will be used to display the District LOVs using a security policy to filter User access. The district name is translated based on user language.'
/

COMMENT ON COLUMN V_DISTRICT."CHAIN" IS 'This column contains the chain number of which the area is a member.'
/

COMMENT ON COLUMN V_DISTRICT."AREA" IS 'Contains the number of the area of which the region is a member.'
/

COMMENT ON COLUMN V_DISTRICT."REGION" IS 'Contains the number of the region of which the district is a member.'
/

COMMENT ON COLUMN V_DISTRICT."DISTRICT" IS 'Contains the number which uniquely identifies the district.'
/

