CREATE OR REPLACE FORCE VIEW V_COMPHEAD_TL (COMPANY, CO_NAME, CO_ADD1, CO_ADD2, CO_ADD3, CO_CITY, CO_NAME_SECONDARY, LANG ) AS
SELECT  b.company,
        case when tl.lang is not null then tl.co_name else b.co_name end co_name,
        case when tl.lang is not null then tl.co_add1 else b.co_add1 end co_add1,
        case when tl.lang is not null then tl.co_add2 else b.co_add2 end co_add2,
        case when tl.lang is not null then tl.co_add3 else b.co_add3 end co_add3,
        case when tl.lang is not null then tl.co_city else b.co_city end co_city,
        case when tl.lang is not null then tl.co_name_secondary else b.co_name_secondary end co_name_secondary,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  COMPHEAD b,
        COMPHEAD_TL tl
 WHERE  b.company = tl.company (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_COMPHEAD_TL is 'This is the translation view for base table COMPHEAD. This view fetches data in user langauge either from translation table COMPHEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_COMPHEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_COMPHEAD_TL.COMPANY is 'Contains the unique number identifying the company the for which the system is running.  This is the highest level in the merchandise hierarchy.'
/

COMMENT ON COLUMN V_COMPHEAD_TL.CO_NAME is 'Contains the company name for which the system is running.'
/

COMMENT ON COLUMN V_COMPHEAD_TL.CO_ADD1 is 'The address of the company headquarters.'
/

COMMENT ON COLUMN V_COMPHEAD_TL.CO_ADD2 is 'The second line of the company headquarters address.'
/

COMMENT ON COLUMN V_COMPHEAD_TL.CO_ADD3 is 'The third line of the company headquarters address.'
/

COMMENT ON COLUMN V_COMPHEAD_TL.CO_CITY is 'The city of the company headquarters.'
/

COMMENT ON COLUMN V_COMPHEAD_TL.CO_NAME_SECONDARY is 'Contains the secondary name of the company.'
/

