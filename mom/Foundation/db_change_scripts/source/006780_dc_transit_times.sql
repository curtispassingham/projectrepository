--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_TRANSIT_TIMES
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_TRANSIT_TIMES"
CREATE TABLE DC_TRANSIT_TIMES
(
   TRANSIT_TIMES_ID           NUMBER(10),
   DEPT                       NUMBER(4),
   ORIGIN                     NUMBER(10),
   DESTINATION                NUMBER(10),
   ORIGIN_TYPE                VARCHAR2(2),
   DESTINATION_TYPE           VARCHAR2(2),
   TRANSIT_TIME               NUMBER(4),
   CLASS                      NUMBER(4),
   SUBCLASS                   NUMBER(4)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_TRANSIT_TIMES is 'This table is a staging table for data conversion and holds transit time in number of days between an origin and destination location.'
/
COMMENT ON COLUMN DC_TRANSIT_TIMES.TRANSIT_TIMES_ID is 'This field holds the unique id for the origin and destination location transit time combination.'
/
COMMENT ON COLUMN DC_TRANSIT_TIMES.DEPT is 'The field holds the unique identifier for the department for which the transit time is defined.'
/
COMMENT ON COLUMN DC_TRANSIT_TIMES.ORIGIN is 'This field holds the unique identifier for the origin location for which transit time is defined.'
/
COMMENT ON COLUMN DC_TRANSIT_TIMES.DESTINATION is 'This field holds the unique identifier for the destination location for which transit time is defined.'
/
COMMENT ON COLUMN DC_TRANSIT_TIMES.ORIGIN_TYPE is 'This field holds the type of origin location for which transit time is defined.'
/
COMMENT ON COLUMN DC_TRANSIT_TIMES.DESTINATION_TYPE is 'This field holds the type of destination location for which transit time is defined.'
/
COMMENT ON COLUMN DC_TRANSIT_TIMES.TRANSIT_TIME is 'This field holds the transit time in days.'
/
COMMENT ON COLUMN DC_TRANSIT_TIMES.CLASS is 'The field holds the unique identifier for the class for which the transit time is defined.'
/
COMMENT ON COLUMN DC_TRANSIT_TIMES.SUBCLASS is 'The field holds the unique identifier for the subclass for which the transit time is defined.'
/