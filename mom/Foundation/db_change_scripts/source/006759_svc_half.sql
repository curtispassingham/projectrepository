--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_HALF
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_HALF'
CREATE TABLE SVC_HALF
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
    HALF_DATE VARCHAR2(120) ,
    HALF_NAME VARCHAR2(120) ,
    HALF_NO NUMBER(5,0) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE 
	)
	INITRANS 6	
	TABLESPACE RETAIL_DATA
/


COMMENT ON TABLE SVC_HALF IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in HALF.'
/

COMMENT ON COLUMN SVC_HALF.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_HALF.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_HALF.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_HALF.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_HALF.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_HALF.HALF_NO IS 'This field contains the year and the half number (1 or 2) for each half. For example: 20131 or 20132.'
/

COMMENT ON COLUMN SVC_HALF.HALF_NAME IS 'This field contains the season name and year for the half. For example: Summer 2013.'
/

COMMENT ON COLUMN SVC_HALF.HALF_DATE IS 'This field contains the description of the month span for the half. For example: Aug 2012 to Jan 2013.'
/

COMMENT ON COLUMN SVC_HALF.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_HALF.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_HALF.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_HALF.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/

PROMPT Creating Primary Key on 'SVC_HALF'   
ALTER TABLE SVC_HALF
ADD CONSTRAINT SVC_HALF_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX	     
/

PROMPT Creating Unique Key on 'SVC_HALF'   
ALTER TABLE SVC_HALF
ADD CONSTRAINT SVC_HALF_UK UNIQUE ( HALF_NO )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX	   
/
