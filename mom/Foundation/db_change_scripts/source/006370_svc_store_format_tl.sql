--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_STORE_FORMAT_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

CREATE TABLE SVC_STORE_FORMAT_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, STORE_FORMAT NUMBER(4)
, FORMAT_NAME VARCHAR2(60)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_STORE_FORMAT_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in STORE_FORMAT_TL.'
/

COMMENT ON COLUMN SVC_STORE_FORMAT_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_STORE_FORMAT_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_STORE_FORMAT_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_STORE_FORMAT_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_STORE_FORMAT_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_STORE_FORMAT_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_STORE_FORMAT_TL.STORE_FORMAT is 'Contains the number which uniquely identifies the store format.'
/

COMMENT ON COLUMN SVC_STORE_FORMAT_TL.FORMAT_NAME is 'Contains the name or description of the store format.'
/

ALTER TABLE SVC_STORE_FORMAT_TL
ADD CONSTRAINT SVC_STORE_FORMAT_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SVC_STORE_FORMAT_TL
ADD CONSTRAINT SVC_STORE_FORMAT_TL_UK UNIQUE
(LANG, STORE_FORMAT)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

