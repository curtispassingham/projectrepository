--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

---------------------------------------------------------------------------
-- Name:    GET_ITEM_STATUS
-- Purpose: This function is used as a function based index for ITEM_MASTER.
--
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION GET_ITEM_STATUS(I_item               IN   ITEM_MASTER.ITEM%TYPE,
                                           I_item_parent        IN   ITEM_MASTER.ITEM_PARENT%TYPE,
                                           I_item_grandparent   IN   ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                                           I_item_status        IN   ITEM_MASTER.STATUS%TYPE)
RETURN VARCHAR2 DETERMINISTIC
IS 

   L_exists        VARCHAR2(1) := NULL;
   L_status        VARCHAR2(1) := NULL;

   cursor C_EXISTS is
      select 'Y' 
        from daily_purge
       where upper(table_name)='ITEM_MASTER'
         AND (key_value = I_item
          OR key_value = I_item_parent 
          OR key_value = I_item_grandparent);

BEGIN
   if (I_item is NOT NULL or I_item_parent is NOT NULL or I_item_grandparent is NOT NULL) then
      open C_EXISTS;
      fetch C_EXISTS into L_exists;
      close C_EXISTS;

      if L_exists = 'Y' then
         L_status := 'D';
      else 
         L_status := I_item_status;
      end if;
   end if;
  
   return L_status;
EXCEPTION
   -- ignore errors because a null will be returned
   when OTHERS then
      NULL;
END;
/

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT CREATING INDEX 'ITEM_MASTER_I15'
CREATE INDEX ITEM_MASTER_I15 ON ITEM_MASTER(GET_ITEM_STATUS(ITEM,ITEM_PARENT,ITEM_GRANDPARENT,STATUS))
/
