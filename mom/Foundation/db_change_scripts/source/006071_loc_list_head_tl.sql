--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       LOC_LIST_HEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE LOC_LIST_HEAD_TL(
LANG NUMBER(6) NOT NULL,
LOC_LIST NUMBER(10) NOT NULL,
LOC_LIST_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE LOC_LIST_HEAD_TL is 'This is the translation table for LOC_LIST_HEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN LOC_LIST_HEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN LOC_LIST_HEAD_TL.LOC_LIST is 'Contains a number to uniquely identify a location list.'
/

COMMENT ON COLUMN LOC_LIST_HEAD_TL.LOC_LIST_DESC is 'Contains the description of the location list.'
/

COMMENT ON COLUMN LOC_LIST_HEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN LOC_LIST_HEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN LOC_LIST_HEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN LOC_LIST_HEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE LOC_LIST_HEAD_TL ADD CONSTRAINT PK_LOC_LIST_HEAD_TL PRIMARY KEY (
LANG,
LOC_LIST
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE LOC_LIST_HEAD_TL
 ADD CONSTRAINT LLHT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE LOC_LIST_HEAD_TL ADD CONSTRAINT LLHT_LLH_FK FOREIGN KEY (
LOC_LIST
) REFERENCES LOC_LIST_HEAD (
LOC_LIST
)
/

