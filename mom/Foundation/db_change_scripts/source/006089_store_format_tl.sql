--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       STORE_FORMAT_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE STORE_FORMAT_TL(
LANG NUMBER(6) NOT NULL,
STORE_FORMAT NUMBER(4) NOT NULL,
FORMAT_NAME VARCHAR2(60) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE STORE_FORMAT_TL is 'This is the translation table for STORE_FORMAT table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN STORE_FORMAT_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN STORE_FORMAT_TL.STORE_FORMAT is 'Contains the number which uniquely identifies the store format.'
/

COMMENT ON COLUMN STORE_FORMAT_TL.FORMAT_NAME is 'Contains the name or description of the store format.'
/

COMMENT ON COLUMN STORE_FORMAT_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN STORE_FORMAT_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN STORE_FORMAT_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN STORE_FORMAT_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE STORE_FORMAT_TL ADD CONSTRAINT PK_STORE_FORMAT_TL PRIMARY KEY (
LANG,
STORE_FORMAT
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE STORE_FORMAT_TL
 ADD CONSTRAINT SFT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE STORE_FORMAT_TL ADD CONSTRAINT SFT_SF_FK FOREIGN KEY (
STORE_FORMAT
) REFERENCES STORE_FORMAT (
STORE_FORMAT
)
/

