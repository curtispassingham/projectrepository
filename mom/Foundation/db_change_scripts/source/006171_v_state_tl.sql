CREATE OR REPLACE FORCE VIEW V_STATE_TL (STATE, COUNTRY_ID, DESCRIPTION, LANG ) AS
SELECT  b.state,
        b.country_id,
        case when tl.lang is not null then tl.description else b.description end description,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  STATE b,
        STATE_TL tl
 WHERE  b.state = tl.state (+)
   AND  b.country_id = tl.country_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_STATE_TL is 'This is the translation view for base table STATE. This view fetches data in user langauge either from translation table STATE_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_STATE_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_STATE_TL.STATE is 'Contains the postal abbreviation for the state.'
/

COMMENT ON COLUMN V_STATE_TL.DESCRIPTION is 'Contains the full name of the state.'
/

COMMENT ON COLUMN V_STATE_TL.COUNTRY_ID is 'Contains the unique code which identifies the country where the state belongs.'
/

