CREATE OR REPLACE FORCE VIEW V_SUPS_TL (SUPPLIER, SUP_NAME, SUP_NAME_SECONDARY, CONTACT_NAME, LANG ) AS
SELECT  b.supplier,
        case when tl.lang is not null then tl.sup_name else b.sup_name end sup_name,
        case when tl.lang is not null then tl.sup_name_secondary else b.sup_name_secondary end sup_name_secondary,
        case when tl.lang is not null then tl.contact_name else b.contact_name end contact_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SUPS b,
        SUPS_TL tl
 WHERE  b.supplier = tl.supplier (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SUPS_TL is 'This is the translation view for base table SUPS. This view fetches data in user langauge either from translation table SUPS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SUPS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SUPS_TL.SUPPLIER is 'Unique identifying number for a supplier within the system.  The user determines this number when a new supplier is first added to the system.'
/

COMMENT ON COLUMN V_SUPS_TL.SUP_NAME is 'Contains the suppliers trading name.'
/

COMMENT ON COLUMN V_SUPS_TL.SUP_NAME_SECONDARY is 'Secondary name of the supplier.'
/

COMMENT ON COLUMN V_SUPS_TL.CONTACT_NAME is 'Contains the name of the suppliers representative contact.'
/

