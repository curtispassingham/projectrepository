--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_STYLE
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_STYLE"
CREATE TABLE DC_STYLE
(
  ITEM                      VARCHAR2(25),
  ITEM_DESC                 VARCHAR2(250),
  SHORT_DESC                VARCHAR2(120),
  ITEM_DESC_SECONDARY       VARCHAR2(250),
  DEPT                      NUMBER(4),
  CLASS                     NUMBER(4),
  SUBCLASS                  NUMBER(4),
  DIFF_1                    VARCHAR2(10),
  DIFF_2                    VARCHAR2(10),
  DIFF_3                    VARCHAR2(10),
  DIFF_4                    VARCHAR2(10),
  ITEM_AGGREGATE_IND        VARCHAR2(1),
  DIFF_1_AGGREGATE_IND      VARCHAR2(1),
  DIFF_2_AGGREGATE_IND      VARCHAR2(1),
  DIFF_3_AGGREGATE_IND      VARCHAR2(1),
  DIFF_4_AGGREGATE_IND      VARCHAR2(1),
  AIP_CASE_TYPE             VARCHAR2(6),
  COMMENTS                  VARCHAR2(2000),
  PERISHABLE_IND            VARCHAR2(1),
  PRODUCT_CLASSIFICATION    VARCHAR2(6),
  BRAND_NAME                VARCHAR2(30)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_STYLE is 'This table is a staging table for data conversion and will hold style item data of ITEM_MASTER table.'
/
COMMENT ON COLUMN DC_STYLE.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_STYLE.ITEM_DESC is 'Long description of the item.'
/
COMMENT ON COLUMN DC_STYLE.SHORT_DESC is 'Shortened description of the item.'
/
COMMENT ON COLUMN DC_STYLE.ITEM_DESC_SECONDARY is 'Secondary descriptions of the item.'
/
COMMENT ON COLUMN DC_STYLE.DEPT is 'Number identifying the department to which the item is attached.'
/
COMMENT ON COLUMN DC_STYLE.CLASS is 'Number identifying the class to which the item is attached.'
/
COMMENT ON COLUMN DC_STYLE.SUBCLASS is 'Number identifying the subclass to which the item is attached.'
/
COMMENT ON COLUMN DC_STYLE.DIFF_1 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_STYLE.DIFF_2 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_STYLE.DIFF_3 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_STYLE.DIFF_4 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_STYLE.ITEM_AGGREGATE_IND is 'Indicator to aggregate inventory and sales for the item.'
/
COMMENT ON COLUMN DC_STYLE.DIFF_1_AGGREGATE_IND is 'Indicator for the corresponding diff. Indicator to aggregate inventory and sales for an item at Parent/Diff level (e.g Style/Color or Style/Size ). This indicator is currently used by allocation and MFP.'
/
COMMENT ON COLUMN DC_STYLE.DIFF_2_AGGREGATE_IND is 'Indicator for the corresponding diff. Indicator to aggregate inventory and sales for an item at Parent/Diff level (e.g Style/Color or Style/Size ). This indicator is currently used by allocation and MFP.'
/
COMMENT ON COLUMN DC_STYLE.DIFF_3_AGGREGATE_IND is 'Indicator for the corresponding diff. Indicator to aggregate inventory and sales for an item at Parent/Diff level (e.g Style/Color or Style/Size ). This indicator is currently used by allocation and MFP.'
/
COMMENT ON COLUMN DC_STYLE.DIFF_4_AGGREGATE_IND is 'Indicator for the corresponding diff. Indicator to aggregate inventory and sales for an item at Parent/Diff level (e.g Style/Color or Style/Size ). This indicator is currently used by allocation and MFP.'
/
COMMENT ON COLUMN DC_STYLE.AIP_CASE_TYPE is 'Only used if AIP is integrated. Determines which case sizes to extract against an item in the AIP interface.'
/
COMMENT ON COLUMN DC_STYLE.COMMENTS is 'Holds any comments associated with the item.'
/
COMMENT ON COLUMN DC_STYLE.PERISHABLE_IND is 'A grocery item attribute used to indicate whether an item is perishable or not.'
/
COMMENT ON COLUMN DC_STYLE.PRODUCT_CLASSIFICATION is 'Product classification is informational only in RMS, but is used by RWMS to determine how to pack customer orders : such as to determine products that may not be able to be packaged together.'
/
COMMENT ON COLUMN DC_STYLE.BRAND_NAME is 'This field contains the brand associated to an item.'
/