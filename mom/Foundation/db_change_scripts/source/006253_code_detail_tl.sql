--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 CODE_DETAIL_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'CODE_DETAIL_TL'
CREATE TABLE CODE_DETAIL_TL
 (LANG NUMBER(6) NOT NULL,
  CODE_TYPE VARCHAR2(4 ) NOT NULL,
  CODE VARCHAR2(6 ) NOT NULL,
  CODE_DESC VARCHAR2(250 ) NOT NULL,
  CREATE_DATETIME DATE NOT NULL,
  CREATE_ID VARCHAR2(30 ) NOT NULL,
  LAST_UPDATE_DATETIME DATE NOT NULL,
  LAST_UPDATE_ID VARCHAR2(30 ) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE CODE_DETAIL_TL is 'This is the translation table for CODE_DETAIL table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN CODE_DETAIL_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN CODE_DETAIL_TL.CODE_TYPE is 'This field will contain a valid code type for the row.  The valid code types are defined on the CODE_HEAD table.'
/

COMMENT ON COLUMN CODE_DETAIL_TL.CODE is 'This column holds the code that is translated.'
/

COMMENT ON COLUMN CODE_DETAIL_TL.CODE_DESC is 'This field contains the description associated with the code and code type.'
/

COMMENT ON COLUMN CODE_DETAIL_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN CODE_DETAIL_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN CODE_DETAIL_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN CODE_DETAIL_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/


PROMPT Creating Primary Key on 'CODE_DETAIL_TL'
ALTER TABLE CODE_DETAIL_TL
 ADD CONSTRAINT PK_CODE_DETAIL_TL PRIMARY KEY
  (LANG,
   CODE_TYPE,
   CODE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'CODE_DETAIL_TL'
 ALTER TABLE CODE_DETAIL_TL
  ADD CONSTRAINT CDT_LANG_FK
  FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/


PROMPT Creating FK on 'CODE_DETAIL_TL'
 ALTER TABLE CODE_DETAIL_TL
  ADD CONSTRAINT CDT_CD_FK
  FOREIGN KEY (CODE_TYPE, CODE)
 REFERENCES CODE_DETAIL (CODE_TYPE, CODE)
/

