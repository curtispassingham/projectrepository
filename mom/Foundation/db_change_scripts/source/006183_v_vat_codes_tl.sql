CREATE OR REPLACE FORCE VIEW V_VAT_CODES_TL (VAT_CODE, VAT_CODE_DESC, LANG ) AS
SELECT  b.vat_code,
        case when tl.lang is not null then tl.vat_code_desc else b.vat_code_desc end vat_code_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  VAT_CODES b,
        VAT_CODES_TL tl
 WHERE  b.vat_code = tl.vat_code (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_VAT_CODES_TL is 'This is the translation view for base table VAT_CODES. This view fetches data in user langauge either from translation table VAT_CODES_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_VAT_CODES_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_VAT_CODES_TL.VAT_CODE is 'This field contains the alphanumeric identification for the VAT code. Valid values include: S - Standard C - Composite Z - Zero E - Exempt Other values may also be entered. These are the default VAT Rates that are set-up upon installation of the RMS.'
/

COMMENT ON COLUMN V_VAT_CODES_TL.VAT_CODE_DESC is 'Contains a description identifying the VAT code.'
/

