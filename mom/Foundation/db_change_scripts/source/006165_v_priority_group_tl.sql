CREATE OR REPLACE FORCE VIEW V_PRIORITY_GROUP_TL (PRIORITY_GROUP_ID, PRIORITY_GROUP_DESC, LANG ) AS
SELECT  b.priority_group_id,
        case when tl.lang is not null then tl.priority_group_desc else b.priority_group_desc end priority_group_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  PRIORITY_GROUP b,
        PRIORITY_GROUP_TL tl
 WHERE  b.priority_group_id = tl.priority_group_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_PRIORITY_GROUP_TL is 'This is the translation view for base table PRIORITY_GROUP. This view fetches data in user langauge either from translation table PRIORITY_GROUP_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_PRIORITY_GROUP_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_PRIORITY_GROUP_TL.PRIORITY_GROUP_ID is 'System generated numeric Column. It is the primary key of the table. It will be unique for each priority. This is a non-editable column.'
/

COMMENT ON COLUMN V_PRIORITY_GROUP_TL.PRIORITY_GROUP_DESC is 'User entered description for the priority group. Description can be changed'
/

