--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       STORE_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE STORE_TL(
LANG NUMBER(6) NOT NULL,
STORE NUMBER(10) NOT NULL,
STORE_NAME VARCHAR2(150) NOT NULL,
STORE_NAME_SECONDARY VARCHAR2(150) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE STORE_TL is 'This is the translation table for STORE table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN STORE_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN STORE_TL.STORE is 'Contains the number which uniquely identifies the store.'
/

COMMENT ON COLUMN STORE_TL.STORE_NAME is 'Contains the name of the store which, along with the store number, identifies the store.'
/

COMMENT ON COLUMN STORE_TL.STORE_NAME_SECONDARY is 'Secondary name of the store.'
/

COMMENT ON COLUMN STORE_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN STORE_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN STORE_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN STORE_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE STORE_TL ADD CONSTRAINT PK_STORE_TL PRIMARY KEY (
LANG,
STORE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE STORE_TL
 ADD CONSTRAINT STRT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE STORE_TL ADD CONSTRAINT STRT_STR_FK FOREIGN KEY (
STORE
) REFERENCES STORE (
STORE
)
/

