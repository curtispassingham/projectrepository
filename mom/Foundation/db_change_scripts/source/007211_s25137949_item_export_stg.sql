--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ITEM_EXPORT_STG'

PROMPT Creating Index 'ITEM_EXPORT_STG_I1'
CREATE INDEX ITEM_EXPORT_STG_I1 on ITEM_EXPORT_STG
  (ITEM,
   LOC,
   LOC_TYPE,
   ACTION_TYPE
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

