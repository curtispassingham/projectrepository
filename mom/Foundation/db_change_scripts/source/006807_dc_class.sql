--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_CLASS
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table DC_CLASS
CREATE TABLE DC_CLASS
(
   DEPT            NUMBER(4),
   CLASS           NUMBER(4),
   CLASS_NAME      VARCHAR2(120),
   CLASS_VAT_IND   VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_CLASS is 'This table is a staging table for data conversion and will hold data for class table..'
/
COMMENT ON COLUMN DC_CLASS.DEPT is 'Contains the number which uniquely identifies the department.'
/
COMMENT ON COLUMN DC_CLASS.CLASS is 'Contains the number which uniquely identifies the class within the system.'
/
COMMENT ON COLUMN DC_CLASS.CLASS_NAME is 'Contains the name of the class which, along with the class number, identifies the class.'
/
COMMENT ON COLUMN DC_CLASS.CLASS_VAT_IND is 'This field determines if retail is displayed and held with or with out vat. This field is only editable when vat is turned on in the system and defined at the class level.'
/