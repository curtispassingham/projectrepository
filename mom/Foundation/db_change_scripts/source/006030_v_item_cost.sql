--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_ITEM_COST
----------------------------------------------------------------------------

whenever sqlerror exit
CREATE OR REPLACE FORCE VIEW V_ITEM_COST ("ITEM", "PACK_IND", "ORDERABLE_IND", "SELLABLE_IND", "UNIT_COST", "CURRENCY_CODE")
AS
  (
  -- unit_cost of orderable (packs and non-packs)
  SELECT isc.item,
    im.pack_ind,
    im.orderable_ind,
    im.sellable_ind,
    isc.unit_cost * mv.exchange_rate unit_cost,
    mv.to_currency currency_code
  FROM item_master im,
    item_supp_country isc,
    sups,
    -- view of the most recent effective exchange rate to primary currency
    (
    SELECT from_currency,
      to_currency,
      exchange_rate
    FROM
      (SELECT rank() over (partition BY from_currency, to_currency, exchange_type order by effective_date DESC) rank,
        from_currency,
        to_currency,
        exchange_rate
      FROM mv_currency_conversion_rates,
        system_options sop,
        period p
      WHERE exchange_type = 'C'
      AND effective_date <= p.vdate
      AND to_currency     = sop.currency_code
      )
    WHERE rank = 1
    ) mv
  WHERE im.orderable_ind      = 'Y'
  AND im.item                 = isc.item
  AND isc.primary_supp_ind    = 'Y'
  AND isc.primary_country_ind = 'Y'
  AND sups.supplier           = isc.supplier
  AND mv.from_currency        = sups.currency_code
  UNION ALL
  -- unit_cost of non-orderable packs as a sum of its component cost
  SELECT DISTINCT im.item,
    im.pack_ind,
    im.orderable_ind,
    im.sellable_ind,
    SUM(isc.unit_cost * mv.exchange_rate * vpq.qty) over (partition BY vpq.pack_no) unit_cost,
    mv.to_currency currency_code
  FROM v_packsku_qty vpq,
    item_master im,
    item_supp_country isc,
    sups,
    -- view of the most recent effective exchange rate to primary currency
    (
    SELECT from_currency,
      to_currency,
      exchange_rate
    FROM
      (SELECT rank() over (partition BY from_currency, to_currency, exchange_type order by effective_date DESC) rank,
        from_currency,
        to_currency,
        exchange_rate
      FROM mv_currency_conversion_rates,
        system_options sop,
        period p
      WHERE exchange_type = 'C'
      AND effective_date <= p.vdate
      AND to_currency     = sop.currency_code
      )
    WHERE rank = 1
    ) mv
  WHERE im.pack_ind           = 'Y'
  AND im.orderable_ind        = 'N'
  AND im.item                 = vpq.pack_no
  AND vpq.item                = isc.item
  AND isc.primary_supp_ind    = 'Y'
  AND isc.primary_country_ind = 'Y'
  AND sups.supplier           = isc.supplier
  AND mv.from_currency        = sups.currency_code
  ) ;
  COMMENT ON TABLE V_ITEM_COST
IS
  'This view is used for the item search screen in RMS Alloy. It includes all item unit cost converted to the primary currency.' ;
  COMMENT ON COLUMN V_ITEM_COST.ITEM
IS
  'Item Id. It can be an orderable item or a non-orderable pack.' ;
  COMMENT ON COLUMN V_ITEM_COST.PACK_IND
IS
  'Indicates if the item is a pack.' ;
  COMMENT ON COLUMN V_ITEM_COST.ORDERABLE_IND
IS
  'Indicates if the item is orderable.' ;
  COMMENT ON COLUMN V_ITEM_COST.SELLABLE_IND
IS
  'Indicates if the item is sellable.' ;
  COMMENT ON COLUMN V_ITEM_COST.UNIT_COST
IS
  'Unit cost of item''s primary supplier and country converted to the primary currency. For a non-orderable pack, it is the total cost of the pack component items.' ;
  COMMENT ON COLUMN V_ITEM_COST.CURRENCY_CODE
IS
  'Currency code of the unit cost.' ;
