--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_UDA
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_UDA'
CREATE OR REPLACE FORCE VIEW V_UDA ( UDA_ID, UDA_DESC, DISPLAY_TYPE, FILTER_MERCH_ID, FILTER_ORG_ID, SINGLE_VALUE_IND, FILTER_MERCH_ID_CLASS, FILTER_MERCH_ID_SUBCLASS ) AS
SELECT UDA.UDA_ID UDA_ID
      ,V.UDA_DESC UDA_DESC
      ,UDA.DISPLAY_TYPE DISPLAY_TYPE
      ,UDA.FILTER_MERCH_ID FILTER_MERCH_ID
      ,UDA.FILTER_ORG_ID FILTER_ORG_ID
      ,UDA.SINGLE_VALUE_IND SINGLE_VALUE_IND
      ,UDA.FILTER_MERCH_ID_CLASS FILTER_MERCH_ID_CLASS
      ,UDA.FILTER_MERCH_ID_SUBCLASS FILTER_MERCH_ID_SUBCLASS
FROM UDA UDA,
     V_UDA_TL V
WHERE UDA.UDA_ID = V.UDA_ID
/

COMMENT ON TABLE V_UDA IS 'This view will be used to display the UDA LOVs using a security policy to filter User access.'
/

