--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

---------------------------------------------------------------------------
-- Name:    GET_PRIMARY_ITEM_SUPPLIER_VPN
-- Purpose: This function is used as a function based index for ITEM_MASTER.
--
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION GET_PRIMARY_ITEM_SUPPLIER_VPN(I_item   IN   ITEM_MASTER.ITEM%TYPE)
RETURN VARCHAR2 DETERMINISTIC
IS 

   L_vpn             ITEM_SUPPLIER.VPN%TYPE := NULL;
   L_exists          BOOLEAN;
   L_error_message   VARCHAR2(255);
   
   cursor C_GET_VPN is
      select vpn
        from item_supplier
       where item = I_item
         and primary_supp_ind = 'Y';
     
BEGIN

   open C_GET_VPN;
   fetch C_GET_VPN into L_vpn;
   close C_GET_VPN;
  
   return L_vpn;
  
EXCEPTION
   -- ignore errors because a null will be returned
   when OTHERS then
      NULL;
END;
/

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT CREATING INDEX 'ITEM_MASTER_I16'
CREATE INDEX ITEM_MASTER_I16 ON ITEM_MASTER(GET_PRIMARY_ITEM_SUPPLIER_VPN(ITEM))
/
