--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 LIKE_STORE_DEPT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'LIKE_STORE_DEPT'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_TABLES
   WHERE TABLE_NAME='LIKE_STORE_DEPT';

  if (L_table_exists = 0) then
      execute immediate 'CREATE TABLE LIKE_STORE_DEPT(STORE NUMBER(10,0) NOT NULL,DEPT NUMBER(4,0) NOT NULL) INITRANS 6 TABLESPACE RETAIL_DATA';
	  execute immediate 'COMMENT ON TABLE LIKE_STORE_DEPT is ''Will hold values for stores and departments that have been successfully processed during likestore processing.''';
	  execute immediate 'COMMENT ON COLUMN LIKE_STORE_DEPT.STORE is ''Contains the number which uniquely identifies the store.''';
	  execute immediate 'COMMENT ON COLUMN LIKE_STORE_DEPT.DEPT is ''Contains the numeric identifier of the department.''';
  end if;
end;
/

