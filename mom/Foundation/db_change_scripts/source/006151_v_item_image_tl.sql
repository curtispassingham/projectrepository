CREATE OR REPLACE FORCE VIEW V_ITEM_IMAGE_TL (ITEM, IMAGE_NAME, IMAGE_DESC, LANG ) AS
SELECT  b.item,
        b.image_name,
        case when tl.lang is not null then tl.image_desc else b.image_desc end image_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  ITEM_IMAGE b,
        ITEM_IMAGE_TL tl
 WHERE  b.item = tl.item (+)
   AND  b.image_name = tl.image_name (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_ITEM_IMAGE_TL is 'This is the translation view for base table ITEM_IMAGE. This view fetches data in user langauge either from translation table ITEM_IMAGE_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_ITEM_IMAGE_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_ITEM_IMAGE_TL.ITEM is 'This field contains the unique alphanumeric identifier for the item, the image is for.'
/

COMMENT ON COLUMN V_ITEM_IMAGE_TL.IMAGE_NAME is 'This field contains the name of the image of the item.'
/

COMMENT ON COLUMN V_ITEM_IMAGE_TL.IMAGE_DESC is 'This field contains the description associated with the image of the item.'
/

