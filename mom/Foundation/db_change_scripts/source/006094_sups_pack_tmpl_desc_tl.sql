--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       SUPS_PACK_TMPL_DESC_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE SUPS_PACK_TMPL_DESC_TL(
LANG NUMBER(6) NOT NULL,
SUPPLIER NUMBER(10) NOT NULL,
PACK_TMPL_ID NUMBER(8) NOT NULL,
SUPP_PACK_DESC VARCHAR2(250) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SUPS_PACK_TMPL_DESC_TL is 'This is the translation table for SUPS_PACK_TMPL_DESC table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN SUPS_PACK_TMPL_DESC_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SUPS_PACK_TMPL_DESC_TL.SUPPLIER is 'This field holds the supplier number.'
/

COMMENT ON COLUMN SUPS_PACK_TMPL_DESC_TL.PACK_TMPL_ID is 'This field holds the pack template ID.'
/

COMMENT ON COLUMN SUPS_PACK_TMPL_DESC_TL.SUPP_PACK_DESC is 'This field contains the pack description of the suppliers pack template.'
/

COMMENT ON COLUMN SUPS_PACK_TMPL_DESC_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN SUPS_PACK_TMPL_DESC_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN SUPS_PACK_TMPL_DESC_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN SUPS_PACK_TMPL_DESC_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE SUPS_PACK_TMPL_DESC_TL ADD CONSTRAINT PK_SUPS_PACK_TMPL_DESC_TL PRIMARY KEY (
LANG,
SUPPLIER,
PACK_TMPL_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SUPS_PACK_TMPL_DESC_TL
 ADD CONSTRAINT SPTDT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE SUPS_PACK_TMPL_DESC_TL ADD CONSTRAINT SPTDT_SPTD_FK FOREIGN KEY (
SUPPLIER,
PACK_TMPL_ID
) REFERENCES SUPS_PACK_TMPL_DESC (
SUPPLIER,
PACK_TMPL_ID
)
/

