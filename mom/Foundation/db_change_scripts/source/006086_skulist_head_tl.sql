--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       SKULIST_HEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE SKULIST_HEAD_TL(
LANG NUMBER(6) NOT NULL,
SKULIST NUMBER(8) NOT NULL,
SKULIST_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SKULIST_HEAD_TL is 'This is the translation table for SKULIST_HEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN SKULIST_HEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SKULIST_HEAD_TL.SKULIST is 'This field contains the number which uniquely identifies the Item List'
/

COMMENT ON COLUMN SKULIST_HEAD_TL.SKULIST_DESC is 'This field contains the description which corresponds to the SKU list number.'
/

COMMENT ON COLUMN SKULIST_HEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN SKULIST_HEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN SKULIST_HEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN SKULIST_HEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE SKULIST_HEAD_TL ADD CONSTRAINT PK_SKULIST_HEAD_TL PRIMARY KEY (
LANG,
SKULIST
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SKULIST_HEAD_TL
 ADD CONSTRAINT STET_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE SKULIST_HEAD_TL ADD CONSTRAINT STET_STE_FK FOREIGN KEY (
SKULIST
) REFERENCES SKULIST_HEAD (
SKULIST
)
/

