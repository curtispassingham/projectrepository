--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_STYLE
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_ITEM_TICKET"
CREATE TABLE DC_ITEM_TICKET
(
   ITEM                   VARCHAR2(25),
   TICKET_TYPE_ID         VARCHAR2(4),
   PRINT_ON_PC_IND        VARCHAR2(1),
   PO_PRINT_TYPE          VARCHAR2(1),
   TICKET_OVER_PCT        NUMBER(12,4)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_ITEM_TICKET is 'This table is a staging table for data conversion and will hold style item data of ITEM_MASTER table.'
/
COMMENT ON COLUMN DC_ITEM_TICKET.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_ITEM_TICKET.TICKET_TYPE_ID is 'Long description of the item.'
/
COMMENT ON COLUMN DC_ITEM_TICKET.PRINT_ON_PC_IND is 'Shortened description of the item.'
/
COMMENT ON COLUMN DC_ITEM_TICKET.PO_PRINT_TYPE is 'Secondary descriptions of the item.'
/
COMMENT ON COLUMN DC_ITEM_TICKET.TICKET_OVER_PCT is 'Number identifying the department to which the item is attached.'
/