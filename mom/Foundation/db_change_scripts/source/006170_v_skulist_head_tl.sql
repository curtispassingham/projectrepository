CREATE OR REPLACE FORCE VIEW V_SKULIST_HEAD_TL (SKULIST, SKULIST_DESC, LANG ) AS
SELECT  b.skulist,
        case when tl.lang is not null then tl.skulist_desc else b.skulist_desc end skulist_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SKULIST_HEAD b,
        SKULIST_HEAD_TL tl
 WHERE  b.skulist = tl.skulist (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SKULIST_HEAD_TL is 'This is the translation view for base table SKULIST_HEAD. This view fetches data in user langauge either from translation table SKULIST_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SKULIST_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SKULIST_HEAD_TL.SKULIST is 'This field contains the number which uniquely identifies the Item List'
/

COMMENT ON COLUMN V_SKULIST_HEAD_TL.SKULIST_DESC is 'This field contains the description which corresponds to the SKU list number.'
/

