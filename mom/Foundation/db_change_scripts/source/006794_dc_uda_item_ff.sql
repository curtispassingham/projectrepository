--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_UDA_ITEM_FF
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_UDA_ITEM_FF"
CREATE TABLE DC_UDA_ITEM_FF
(
   ITEM                   VARCHAR2(25),
   UDA_ID                 NUMBER(5),
   UDA_TEXT               VARCHAR2(250)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_UDA_ITEM_FF is 'This table is a staging table for data conversion and will hold one row for each item/attribute combination for UDAs with display type of Free
Form (FF) and contains the information about item attribute relationship, create date, update date, etc.'
/
COMMENT ON COLUMN DC_UDA_ITEM_FF.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_UDA_ITEM_FF.UDA_ID is 'This field contains a number uniquely identifying the User-Defined Attribute.'
/
COMMENT ON COLUMN DC_UDA_ITEM_FF.UDA_TEXT is 'This field contains the text value of the Used Defined attribute for the item.'
/