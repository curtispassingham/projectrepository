--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_ITEM_LOC
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_ITEM_LOC"
CREATE TABLE DC_ITEM_LOC
(
   ITEM                       VARCHAR2(25),
   LOCATION                   NUMBER(10),
   LOC_TYPE                   VARCHAR2(1),
   PRIMARY_LOC_IND            VARCHAR2(1),
   SELLING_UNIT_RETAIL        NUMBER(20,4),
   SELLING_UOM                VARCHAR2(4),
   TAXABLE_IND                VARCHAR2(1),
   LOCAL_ITEM_DESC            VARCHAR2(250),
   LOCAL_SHORT_DESC           VARCHAR2(120),
   TI                         NUMBER(12,4),
   HI                         NUMBER(12,4),
   STORE_ORD_MULT             VARCHAR2(1),
   MEAS_OF_EACH               NUMBER(12,4),
   MEAS_OF_PRICE              NUMBER(12,4),
   UOM_OF_PRICE               VARCHAR2(4),
   PRIMARY_COST_PACK          VARCHAR2(25),
   INBOUND_HANDLING_DAYS      NUMBER(2),
   SOURCE_WH                  NUMBER(10),
   SOURCE_METHOD              VARCHAR2(1),
   MULTI_UNITS                NUMBER(12,4),
   MULTI_UNIT_RETAIL          NUMBER(20,4),
   MULTI_SELLING_UOM          VARCHAR2(4),
   AVERAGE_WEIGHT             NUMBER(12,4),
   UIN_TYPE                   VARCHAR2(6),
   UIN_LABEL                  VARCHAR2(6),
   CAPTURE_TIME               VARCHAR2(6),
   EXT_UIN_IND                VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_ITEM_LOC is 'This table is a staging table for data conversion and will hold data of item_loc table.'
/
COMMENT ON COLUMN DC_ITEM_LOC.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_ITEM_LOC.LOCATION is 'Numeric identifier of the location in which the item is to be found.'
/
COMMENT ON COLUMN DC_ITEM_LOC.LOC_TYPE is 'Type of location in the location field. Valid values are S (store), W (warehouse), and E (external finisher).'
/
COMMENT ON COLUMN DC_ITEM_LOC.PRIMARY_LOC_IND is 'Store/Warehouse locations of the retailer in a country where a given supplier is the primary or main supplier of an item.'
/
COMMENT ON COLUMN DC_ITEM_LOC.SELLING_UNIT_RETAIL is 'Contains the unit retail price in the selling unit of measure for the item/location combination. This field is stored in the local currency.'
/
COMMENT ON COLUMN DC_ITEM_LOC.SELLING_UOM is 'Contains the selling unit of measure for an items single-unit retail.'
/
COMMENT ON COLUMN DC_ITEM_LOC.TAXABLE_IND is 'Indicates if item is taxable at the store(Y/N).'
/
COMMENT ON COLUMN DC_ITEM_LOC.LOCAL_ITEM_DESC is 'Contains the local description of the item.'
/
COMMENT ON COLUMN DC_ITEM_LOC.LOCAL_SHORT_DESC is 'Contains the local short description of the item.'
/
COMMENT ON COLUMN DC_ITEM_LOC.TI is 'Number of shipping units (cases) that make up one tier of a pallet.'
/
COMMENT ON COLUMN DC_ITEM_LOC.HI is 'Number of tiers that make up a complete pallet (height).'
/
COMMENT ON COLUMN DC_ITEM_LOC.STORE_ORD_MULT is 'This column contains the multiple in which the item needs to be shipped from a warehouse to the location.'
/
COMMENT ON COLUMN DC_ITEM_LOC.MEAS_OF_EACH is 'Size of an each in terms of the uom_of_price.'
/
COMMENT ON COLUMN DC_ITEM_LOC.MEAS_OF_PRICE is 'Size to be used on the ticket in terms of the uom_of_price.'
/
COMMENT ON COLUMN DC_ITEM_LOC.UOM_OF_PRICE is 'Unit of measure that will be used on the ticket for this item.'
/
COMMENT ON COLUMN DC_ITEM_LOC.PRIMARY_COST_PACK is 'This field contains an item number that is a simple pack containing the item in the item column for this record.'
/
COMMENT ON COLUMN DC_ITEM_LOC.INBOUND_HANDLING_DAYS is 'This field indicates the number of inbound handling days for an item at a warehouse type location.'
/
COMMENT ON COLUMN DC_ITEM_LOC.SOURCE_WH is 'This value will be used by the ad-hoc PO/Transfer creation process to determine which warehouse to fill the stores request from.'
/
COMMENT ON COLUMN DC_ITEM_LOC.SOURCE_METHOD is 'This value will be used to specify how the adhoc PO/TSF creation process should source the item/location request.'
/
COMMENT ON COLUMN DC_ITEM_LOC.MULTI_UNITS is 'This field contains the multi-units for the item/location (zone) combination.'
/
COMMENT ON COLUMN DC_ITEM_LOC.MULTI_UNIT_RETAIL is 'This field holds the multi-unit retail in the multi-selling unit of measure for the item/location (zone) combination.'
/
COMMENT ON COLUMN DC_ITEM_LOC.MULTI_SELLING_UOM is 'This field holds the selling unit of measure for this item/location (zone) combinations multiunit retail.'
/
COMMENT ON COLUMN DC_ITEM_LOC.AVERAGE_WEIGHT is 'A new field to hold the average simple pack weight for a catch weight simple pack.'
/
COMMENT ON COLUMN DC_ITEM_LOC.UIN_TYPE is 'This column will contain the unique identification number (UIN) used to identify the instances of the item at the location.'
/
COMMENT ON COLUMN DC_ITEM_LOC.UIN_LABEL is 'This column will contain the label for the UIN when displayed in SIM.'
/
COMMENT ON COLUMN DC_ITEM_LOC.CAPTURE_TIME is 'This column will indicate when the UIN should be captured for an item during transaction processing.'
/
COMMENT ON COLUMN DC_ITEM_LOC.EXT_UIN_IND is 'This Yes/No indicator indicates if UIN is being generated in the external system.'
/