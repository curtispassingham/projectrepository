CREATE OR REPLACE FORCE VIEW V_SEASONS_TL (SEASON_ID, SEASON_DESC, LANG ) AS
SELECT  b.season_id,
        case when tl.lang is not null then tl.season_desc else b.season_desc end season_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SEASONS b,
        SEASONS_TL tl
 WHERE  b.season_id = tl.season_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SEASONS_TL is 'This is the translation view for base table SEASONS. This view fetches data in user langauge either from translation table SEASONS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SEASONS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SEASONS_TL.SEASON_ID is 'This field contains the unique identifier for season.'
/

COMMENT ON COLUMN V_SEASONS_TL.SEASON_DESC is 'This field contains the description associated with the season.'
/

