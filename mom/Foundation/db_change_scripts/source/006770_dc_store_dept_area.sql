--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_STORE_DEPT_AREA
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_STORE_DEPT_AREA"
CREATE TABLE DC_STORE_DEPT_AREA
(
   STORE                      NUMBER(10),
   DEPT                       NUMBER(4),
   EFFECTIVE_DATE             DATE,
   AREA                       NUMBER(12,4)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_STORE_DEPT_AREA is 'This table is a staging table for data conversion and will contain the area dedicated to a given department within a given store as of the effective date.'
/
COMMENT ON COLUMN DC_STORE_DEPT_AREA.STORE is 'Contains the number which uniquely identifies the store.'
/
COMMENT ON COLUMN DC_STORE_DEPT_AREA.DEPT is 'This column contains the numeric identifier of the department.'
/
COMMENT ON COLUMN DC_STORE_DEPT_AREA.EFFECTIVE_DATE is 'This column contains the date that the given area for the store/department is effective.'
/
COMMENT ON COLUMN DC_STORE_DEPT_AREA.AREA is 'Secondary name of the store.'
/