--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'UDA_ITEM_FF'
ALTER TABLE UDA_ITEM_FF ADD UDA_TEXT_DESC generated always AS (UDA_TEXT || null) virtual
/
COMMENT ON COLUMN UDA_ITEM_FF.UDA_TEXT_DESC IS 'This field is is a virtual column and will have a value same UDA_TEXT.'
/
