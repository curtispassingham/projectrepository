--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_VAT_REGION
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_VAT_REGION"
CREATE TABLE DC_VAT_REGION
(
   VAT_REGION               NUMBER(4),
   VAT_REGION_NAME          VARCHAR2(120),
   VAT_REGION_TYPE          VARCHAR2(6),
   ACQUISITION_VAT_IND      VARCHAR2(1),
   REVERSE_VAT_THRESHOLD    NUMBER(20,4),
   VAT_CALC_TYPE            VARCHAR2(6)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_VAT_REGION is 'This table is a staging table for data conversion and will hold one row for each VAT region and is maintained by the VAT Region Maintenance.'
/
COMMENT ON COLUMN DC_VAT_REGION.VAT_REGION is 'Contains the unique identifying number for the VAT region in the system.'
/
COMMENT ON COLUMN DC_VAT_REGION.VAT_REGION_NAME is 'Contains the name associated with the VAT region.'
/
COMMENT ON COLUMN DC_VAT_REGION.VAT_REGION_TYPE is 'Will hold the type of VAT region.'
/
COMMENT ON COLUMN DC_VAT_REGION.ACQUISITION_VAT_IND is 'Indicates if acquisition VAT is applicable to the vat region. Valid values are Y and N.'
/
COMMENT ON COLUMN DC_VAT_REGION.REVERSE_VAT_THRESHOLD is 'This holds the invoice-level total value limit.'
/
COMMENT ON COLUMN DC_VAT_REGION.VAT_CALC_TYPE is 'This column holds the tax calculation type and can be ''S''imple, ''E''xempt or ''C''ustom.'
/