--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_ITEM_COUNTRY
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_ITEM_COUNTRY"
CREATE TABLE DC_ITEM_COUNTRY
(
   ITEM                     VARCHAR2(25),
   COUNTRY_ID               VARCHAR2(3)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_ITEM_COUNTRY is 'This table is a staging table for data conversion and will hold data of ITEM_COUNTRY.'
/
COMMENT ON COLUMN DC_ITEM_COUNTRY.ITEM is 'Alphanumeric value that identifies either the item or item parent.'
/
COMMENT ON COLUMN DC_ITEM_COUNTRY.COUNTRY_ID is 'This column contains the unique code that identifies the country, which is associated with the item.'
/