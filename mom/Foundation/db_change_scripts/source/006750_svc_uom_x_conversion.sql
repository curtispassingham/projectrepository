-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_UOM_X_CONVERSION
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_UOM_X_CONVERSION'
CREATE TABLE SVC_UOM_X_CONVERSION
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
    CONVERT_SQL VARCHAR2(2000) ,
    TO_UOM_CLASS VARCHAR2(6) ,
    FROM_UOM_CLASS VARCHAR2(6) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE 
	)
	INITRANS 6
	TABLESPACE RETAIL_DATA
/



COMMENT ON TABLE SVC_UOM_X_CONVERSION IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in UOM_X_CONVERSION.'
/

COMMENT ON COLUMN SVC_UOM_X_CONVERSION.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_UOM_X_CONVERSION.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_UOM_X_CONVERSION.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_UOM_X_CONVERSION.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_UOM_X_CONVERSION.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_UOM_X_CONVERSION.TO_UOM_CLASS IS 'Contains the type of the UOM that is required for the product in question, and to which the conversion takes place (in the above example, the To_Class is Volume).'
/

COMMENT ON COLUMN SVC_UOM_X_CONVERSION.FROM_UOM_CLASS IS 'Contains the class of the UOM currently used on the product in question (for example, in a conversion from weight to volume, the From_Class is weight).'
/

COMMENT ON COLUMN SVC_UOM_X_CONVERSION.CONVERT_SQL IS 'Contains the actual select statement to perform the conversion.'
/

COMMENT ON COLUMN SVC_UOM_X_CONVERSION.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_UOM_X_CONVERSION.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_UOM_X_CONVERSION.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_UOM_X_CONVERSION.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/
PROMPT Creating Primary Key on 'SVC_UOM_X_CONVERSION'                      
ALTER TABLE SVC_UOM_X_CONVERSION
ADD CONSTRAINT SVC_UOM_X_CONVERSION_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/
	
PROMPT Creating Unique Key on 'SVC_UOM_X_CONVERSION'
ALTER TABLE SVC_UOM_X_CONVERSION
ADD CONSTRAINT SVC_UOM_X_CONVERSION_UK UNIQUE ( TO_UOM_CLASS,FROM_UOM_CLASS ) 
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX 
/
