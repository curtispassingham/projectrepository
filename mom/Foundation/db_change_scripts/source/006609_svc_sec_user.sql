--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_SEC_USER
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_SEC_USER'
CREATE TABLE SVC_SEC_USER
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  APPLICATION_USER_ID VARCHAR2(30 ),
  DATABASE_USER_ID VARCHAR2(30 ),
  USER_SEQ NUMBER(15,0),
  ALLOCATION_USER_IND VARCHAR2(1 ),
  REIM_USER_IND VARCHAR2(1 ),
  RESA_USER_IND VARCHAR2(1 ),
  RMS_USER_IND VARCHAR2(1 ),
  MANAGER NUMBER(15,0),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_SEC_USER is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in SEC_USER.'
/

COMMENT ON COLUMN SVC_SEC_USER.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_SEC_USER.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_SEC_USER.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_SEC_USER.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_SEC_USER.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_SEC_USER.APPLICATION_USER_ID is 'This column holds the application user id set up in enterprise LDAP for the security user.'
/

COMMENT ON COLUMN SVC_SEC_USER.DATABASE_USER_ID is 'This column holds the database user login id assigned to the security user. It is used to login to applications like RMS. All database user ids must be defined on the USER_ATTRIB table.'
/

COMMENT ON COLUMN SVC_SEC_USER.USER_SEQ is 'This is a sequence generated number that uniquely identifies a security user.'
/

COMMENT ON COLUMN SVC_SEC_USER.ALLOCATION_USER_IND is 'This field will indicate whether the user is an Allocation User. Valid values are Y/N.'
/

COMMENT ON COLUMN SVC_SEC_USER.REIM_USER_IND is 'This field will indicate whether the user is a ReIM User. Valid values are Y/N.'
/

COMMENT ON COLUMN SVC_SEC_USER.RESA_USER_IND is 'This field will indicate whether the user is a ReSA User. Valid values are Y/N.'
/

COMMENT ON COLUMN SVC_SEC_USER.RMS_USER_IND is 'This field will indicate whether the user is a RMS User. Valid values are Y/N.'
/

COMMENT ON COLUMN SVC_SEC_USER.MANAGER is 'This field holds the user_seq of the manager for the application. Manager should be entered as a user earlier on.'
/

COMMENT ON COLUMN SVC_SEC_USER.CREATE_ID is 'User who created the record.'
/

COMMENT ON COLUMN SVC_SEC_USER.CREATE_DATETIME is 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_SEC_USER.LAST_UPD_ID is 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_SEC_USER.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_SEC_USER'
ALTER TABLE SVC_SEC_USER
 ADD CONSTRAINT SVC_SEC_USER_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_SEC_USER'
ALTER TABLE SVC_SEC_USER
 ADD CONSTRAINT SVC_SEC_USER_UK UNIQUE
  (USER_SEQ,
   APPLICATION_USER_ID,
   DATABASE_USER_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

