--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_UDA_ITEM_DEFAULTS
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table DC_UDA_ITEM_DEFAULTS
CREATE TABLE DC_UDA_ITEM_DEFAULTS
(
   UDA_ID            NUMBER(5),
   DEPT              NUMBER(4),
   CLASS             NUMBER(4),
   SUBCLASS          NUMBER(4),
   UDA_VALUE         NUMBER(5)
)
 INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_UDA_ITEM_DEFAULTS is 'This table is a staging table for data conversion and will hold data  for an item attribute'
/
COMMENT ON COLUMN DC_UDA_ITEM_DEFAULTS.UDA_ID is 'Contains the number which uniquely identifies the department.'
/
COMMENT ON COLUMN DC_UDA_ITEM_DEFAULTS.DEPT is 'This field contains the department for which the default is being set-up.'
/
COMMENT ON COLUMN DC_UDA_ITEM_DEFAULTS.CLASS is 'This field contains the class for which the default is being set-up.'
/
COMMENT ON COLUMN DC_UDA_ITEM_DEFAULTS.SUBCLASS is 'This field contains the subclass for which the default is being set-up.'
/
COMMENT ON COLUMN DC_UDA_ITEM_DEFAULTS.UDA_VALUE is 'This is an optional field in which a default value can be assigned, such that all items created in the hierarchy level will be assigned this UDA value initially.'
/