--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_PARTNER
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_PARTNER'
CREATE OR REPLACE FORCE VIEW V_PARTNER
 ("PARTNER_TYPE"
 ,"PARTNER_ID"
 ,"PARTNER_DESC"
 ,"CURRENCY_CODE"
 ,"LANG"
 ,"STATUS"
 ,"CONTACT_NAME"
 ,"CONTACT_PHONE"
 ,"CONTACT_FAX"
 ,"CONTACT_TELEX"
 ,"CONTACT_EMAIL"
 ,"MFG_ID"
 ,"PRINCIPLE_COUNTRY_ID"
 ,"LINE_OF_CREDIT"
 ,"OUTSTAND_CREDIT"
 ,"OPEN_CREDIT"
 ,"YTD_CREDIT"
 ,"YTD_DRAWDOWNS"
 ,"TAX_ID"
 ,"TERMS"
 ,"SERVICE_PERF_REQ_IND"
 ,"INVC_PAY_LOC"
 ,"INVC_RECEIVE_LOC"
 ,"IMPORT_COUNTRY_ID"
 ,"PRIMARY_IA_IND"
 ,"COMMENT_DESC"
 ,"TSF_ENTITY_ID"
 ,"VAT_REGION"
 ,"ORG_UNIT_ID"
 ,"PARTNER_NAME_SECONDARY"
 ,"AUTO_RCV_STOCK_IND")
 AS SELECT pr.PARTNER_TYPE        PARTNER_TYPE   ,
pr.PARTNER_ID          PARTNER_ID   ,
v.PARTNER_DESC         PARTNER_DESC   ,
pr.CURRENCY_CODE       URRENCY_CODE   ,
pr.LANG                LANG   ,
pr.STATUS              STATUS   ,
pr.CONTACT_NAME        CONTACT_NAME   ,
pr.CONTACT_PHONE       CONTACT_PHONE   ,
pr.CONTACT_FAX         CONTACT_FAX   ,
pr.CONTACT_TELEX       CONTACT_TELEX   ,
pr.CONTACT_EMAIL       CONTACT_EMAIL   ,
pr.MFG_ID              MFG_ID   ,
pr.PRINCIPLE_COUNTRY_ID PRINCIPLE_COUNTRY_ID   ,
pr.LINE_OF_CREDIT      LINE_OF_CREDIT   ,
pr.OUTSTAND_CREDIT     OUTSTAND_CREDIT   ,
pr.OPEN_CREDIT         OPEN_CREDIT   ,
pr.YTD_CREDIT          YTD_CREDIT   ,
pr.YTD_DRAWDOWNS       YTD_DRAWDOWNS   ,
pr.TAX_ID              TAX_ID   ,
pr.TERMS               TERMS   ,
pr.SERVICE_PERF_REQ_IND SERVICE_PERF_REQ_IND   ,
pr.INVC_PAY_LOC          INVC_PAY_LOC ,
pr.INVC_RECEIVE_LOC      INVC_RECEIVE_LOC ,
pr.IMPORT_COUNTRY_ID     IMPORT_COUNTRY_ID ,
pr.PRIMARY_IA_IND        PRIMARY_IA_IND ,
pr.COMMENT_DESC          COMMENT_DESC ,
pr.TSF_ENTITY_ID         TSF_ENTITY_ID ,
pr.VAT_REGION            VAT_REGION ,
pr.ORG_UNIT_ID           ORG_UNIT_ID,
v.PARTNER_NAME_SECONDARY PARTNER_NAME_SECONDARY,
pr.AUTO_RCV_STOCK_IND     AUTO_RCV_STOCK_IND
FROM partner pr,
     v_partner_tl v
WHERE pr.partner_id = v.partner_id
/


COMMENT ON TABLE V_PARTNER IS 'This view based on partner table is used to get translated value of partner description'
/

COMMENT ON COLUMN V_PARTNER."PARTNER_TYPE" IS 'Specifies the type of partner.  Valid values are Bank BK, Agent AG, Freight Forwarder FF, Importer IM, Broker BR, Factory FA, Applicant AP, Consolidator CO, and Consignee CN, Supplier hierarchy level 1 S1, Supplier hierarchy level 2 S2, Supplier hierarchy level 3 S3.'
/

COMMENT ON COLUMN V_PARTNER."PARTNER_ID" IS 'Unique identifying number for a partner within the system.  The user determines this number when a new partner is first added to the system.'
/

COMMENT ON COLUMN V_PARTNER."CURRENCY_CODE" IS 'Contains a code identifying the currency the partner uses for business transactions.'
/

COMMENT ON COLUMN V_PARTNER."LANG" IS 'This field contains the partners preferred language.'
/

COMMENT ON COLUMN V_PARTNER."STATUS" IS 'Determines whether the partner is currently active. Valid values include: A for an active partner or I for an inactive partner.  The status of a partner will be checked when an order is being created to make certain the partner is active.'
/

COMMENT ON COLUMN V_PARTNER."CONTACT_NAME" IS 'Contains the name of the partners representative contract.'
/

COMMENT ON COLUMN V_PARTNER."CONTACT_PHONE" IS 'Contains the phone number of the partners representative contact.'
/

COMMENT ON COLUMN V_PARTNER."CONTACT_FAX" IS 'Contains the fax number of the partners representative contact.'
/

COMMENT ON COLUMN V_PARTNER."CONTACT_TELEX" IS 'Contains the telex number of the partners representative contact.'
/

COMMENT ON COLUMN V_PARTNER."CONTACT_EMAIL" IS 'Contains the e-mail address of the partners representative contact.'
/

COMMENT ON COLUMN V_PARTNER."MFG_ID" IS 'Contains the manufacturers tax identification number.  This field is NULL when the Partner Type is Bank (BK).'
/

COMMENT ON COLUMN V_PARTNER."PRINCIPLE_COUNTRY_ID" IS 'Contains the country id to which the partner is assigned.  This field is NULL when the Partner Type is Bank (BK).'
/

COMMENT ON COLUMN V_PARTNER."LINE_OF_CREDIT" IS 'Contains the line of credit the company has at the Bank in the Partners currency.  This field is not NULL when the Partner Type is Bank (BK).'
/

COMMENT ON COLUMN V_PARTNER."OUTSTAND_CREDIT" IS 'Contains the total amount of credit that the company has used or has charged against  in the Partners currency.  This field is not NULL when the Partner Type is Bank (BK).'
/

COMMENT ON COLUMN V_PARTNER."OPEN_CREDIT" IS 'Contains the total amount that the company can still charge against in the Partners currency.  This field is not NULL when the Partner Type is Bank (BK).'
/

COMMENT ON COLUMN V_PARTNER."YTD_CREDIT" IS 'Contains the total amount of credit the company has used this year to date in the Partners currency.  This field is not NULL when the Partner Type is Bank (BK).'
/

COMMENT ON COLUMN V_PARTNER."YTD_DRAWDOWNS" IS 'Contains the year to date payments the bank has made on behalf of the company in the Partners currency.  This field is not NULL when the Partner Type is Bank (BK).'
/

COMMENT ON COLUMN V_PARTNER."TAX_ID" IS 'Contains the unique tax identification number of the partner.  This will be used for reporting during the Customs Entry process.'
/

COMMENT ON COLUMN V_PARTNER."TERMS" IS 'Payment terms for the partner.  These terms specify when payment is due and if any discounts exist for early payment.  If populated, they will default on any invoice entered for this partner.'
/

COMMENT ON COLUMN V_PARTNER."SERVICE_PERF_REQ_IND" IS 'Indicates if the expense vendors services (e.g. snowplowing, window washing) must be confirmed as performed before paying an invoice from that expense vendor.  Valid values are Y (all service non-merchandise lines on an invoice from this expense vendor must be confirmed before the invoice can be paid) and N (services do not need to be confirmed or partner is not an expense vendor).'
/

COMMENT ON COLUMN V_PARTNER."INVC_PAY_LOC" IS 'Indicates where invoices from this expense vendor are paid - at the store or centrally through corporate accounting.  Valid values are S (paid at the store) and C (paid centrally).  This field will only be populated for expense vendors, and should only be S if using ReSA to accept payment at the store.'
/

COMMENT ON COLUMN V_PARTNER."INVC_RECEIVE_LOC" IS 'Indicates where invoices from this expense vendor are received - at the store or centrally through corporate accounting.  Valid values are S (received at the store) and C (received centrally).  This field should only be populated when using invoice matching.'
/

COMMENT ON COLUMN V_PARTNER."IMPORT_COUNTRY_ID" IS 'Import country of the Import Authority.  This field is not populated for other partner types.'
/

COMMENT ON COLUMN V_PARTNER."PRIMARY_IA_IND" IS 'Indicates if an Import Authority is the primary Import Authority for an import country.  This field will always be N for other partner types.  There must be one and only one primary Import Authority for each country associated with an Import Authority on the partner table.'
/

COMMENT ON COLUMN V_PARTNER."COMMENT_DESC" IS 'Contains any comments associated with the Partner.'
/

COMMENT ON COLUMN V_PARTNER."TSF_ENTITY_ID" IS 'ID of the transfer entity with which an external finisher (partner_type = E) is associated.  Valid values are found on the TSF_ENTITY table.  A transfer entity is a group of locations that share legal requirements around product management.  If SYSTEM_OPTIONS.INTERCOMPANY_TRANSFER_IND = Y, then each external finisher will be required to have an associated TSF_ENTITY_ID.  If SYSTEM_OPTIONS.INTERCOMPANY_TRANSFER_IND = N, then transfer entity functionality is not used and PARTNER.TSF_ENTITY_ID will not be required.'
/

COMMENT ON COLUMN V_PARTNER."VAT_REGION" IS 'VAT (value added tax) region with which a partner is associated. Valid values will be found on the VAT_REGION table. If SYSTEM_OPTIONS.DEFAULT_TAX_TYPE is SVAT, then each partner will be required to have an associated VAT_REGION. For other default_tax_type, then VAT functionality is not used and PARTNER.VAT_REGION will not be required.'
/

COMMENT ON COLUMN V_PARTNER."ORG_UNIT_ID" IS 'Org Unit Id'
/

COMMENT ON COLUMN V_PARTNER."PARTNER_NAME_SECONDARY" IS 'This wil hold the secondary name of the partner.'
/

COMMENT ON COLUMN V_PARTNER."AUTO_RCV_STOCK_IND" IS 'This will indicate whether the system will update the stock for the external finisher when the 1st leg of the transfer is shipped. Valid values are Yes or No'
/

