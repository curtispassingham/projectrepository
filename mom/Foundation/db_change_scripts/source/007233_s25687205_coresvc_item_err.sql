--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'CORESVC_ITEM_ERR'
ALTER TABLE CORESVC_ITEM_ERR ADD LOC NUMBER (10,0) NULL
/

COMMENT ON COLUMN CORESVC_ITEM_ERR.LOC is 'The Location to which error belongs.'
/

