--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_PWH
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_PWH"
CREATE TABLE DC_PWH
(
   WH                         NUMBER(10),
   WH_NAME                    VARCHAR2(150),
   PRIMARY_VWH                NUMBER(10),
   CURRENCY_CODE              VARCHAR2(3),
   BREAK_PACK_IND             VARCHAR2(1),
   REDIST_WH_IND              VARCHAR2(1),
   DELIVERY_POLICY            VARCHAR2(6),
   FORECAST_WH_IND            VARCHAR2(1),
   REPL_IND                   VARCHAR2(1),
   REPL_WH_LINK               NUMBER(10),
   IB_IND                     VARCHAR2(1),
   IB_WH_LINK                 NUMBER(10),
   AUTO_IB_CLEAR              VARCHAR2(1),
   INBOUND_HANDLING_DAYS      NUMBER(2),
   WH_NAME_SECONDARY          VARCHAR2(150),
   EMAIL                      VARCHAR2(100),
   VAT_REGION                 NUMBER(4),
   ORG_HIER_TYPE              NUMBER(4),
   ORG_HIER_VALUE             NUMBER(10),
   DUNS_LOC                   VARCHAR2(4),
   DUNS_NUMBER                VARCHAR2(9),
   ORG_UNIT_ID                NUMBER(15),
   ORG_ENTITY_TYPE            VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_PWH is 'This table is a staging table for data conversion and will hold the physical warehouse information.'
/
COMMENT ON COLUMN DC_PWH.WH is 'Contains the number which uniquely identifies the warehouse.'
/
COMMENT ON COLUMN DC_PWH.WH_NAME is 'Contains the name of the warehouse which, along with the warehouse number, identifies the warehouse.'
/
COMMENT ON COLUMN DC_PWH.PRIMARY_VWH is 'This field holds the virtual warehouse that will be used as the basis for all transactions for which only a physical warehouse and not a virtual warehouse has not been specified.'
/
COMMENT ON COLUMN DC_PWH.CURRENCY_CODE is 'This field contains the currency code under which the warehouse operates.'
/
COMMENT ON COLUMN DC_PWH.BREAK_PACK_IND is 'Indicates whether or not the warehouse is capable of distributing less than the supplier case quantity.'
/
COMMENT ON COLUMN DC_PWH.REDIST_WH_IND is 'Indicates that the warehouse is a Re- Distribution warehouse.'
/
COMMENT ON COLUMN DC_PWH.DELIVERY_POLICY is 'Contains the delivery policy of the warehouse. Next Day indicates that if a location is closed, the warehouse will deliver on the next day.'
/
COMMENT ON COLUMN DC_PWH.FORECAST_WH_IND is 'This indicator determines if a warehouse is forecastable.'
/
COMMENT ON COLUMN DC_PWH.REPL_IND is 'This indicator determines if a warehouse is replenishable.'
/
COMMENT ON COLUMN DC_PWH.REPL_WH_LINK is 'This field holds the replenishable warehouse that is linked to this virtual warehouse.'
/
COMMENT ON COLUMN DC_PWH.IB_IND is 'This field indicates if the warehouse is an investment buy warehouse.'
/
COMMENT ON COLUMN DC_PWH.IB_WH_LINK is 'This field contains the investment buy warehouse that is linked to the virtual warehouse.'
/
COMMENT ON COLUMN DC_PWH.AUTO_IB_CLEAR is 'This indicator determines if the investment buys inventory should be automatically transferred to the turn (replenishable) warehouse when an order is received by the turn warehouse.'
/
COMMENT ON COLUMN DC_PWH.INBOUND_HANDLING_DAYS is 'Warehouse inbound handling days are defined as the number of days that the warehouse requires to receive any item and get it to the shelf so that it is ready to pick.'
/
COMMENT ON COLUMN DC_PWH.WH_NAME_SECONDARY is 'Secondary name of the warehouse.'
/
COMMENT ON COLUMN DC_PWH.EMAIL is 'Holds the email address for the location.'
/
COMMENT ON COLUMN DC_PWH.VAT_REGION is 'Contains the number of the Value Added Tax region in which this warehouse is located.'
/
COMMENT ON COLUMN DC_PWH.ORG_HIER_TYPE is 'Contains the organization type that will be used in reporting purposes for the warehouse. The type comes from the organizational hierarchy.'
/
COMMENT ON COLUMN DC_PWH.ORG_HIER_VALUE is 'Contains the code associated with the specific organizational hierarchy type.'
/
COMMENT ON COLUMN DC_PWH.DUNS_LOC is 'This field holds the Dun and Bradstreet number to identify the location.'
/
COMMENT ON COLUMN DC_PWH.DUNS_NUMBER is 'This field holds the Dun and Bradstreet number to identify the warehouse'
/
COMMENT ON COLUMN DC_PWH.ORG_UNIT_ID is 'This column will hold the oracle organizational unit id value.'
/
COMMENT ON COLUMN DC_PWH.ORG_ENTITY_TYPE is 'This is the new column that will specify if the warehouse is a legal entity (Importer, Exporter) or a regular warehouse.'
/
