CREATE OR REPLACE FORCE VIEW V_STORE_ADD_TL (STORE, STORE_NAME, STORE_NAME_SECONDARY, LANG ) AS
SELECT  b.store,
        case when tl.lang is not null then tl.store_name else b.store_name end store_name,
        case when tl.lang is not null then tl.store_name_secondary else b.store_name_secondary end store_name_secondary,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  STORE_ADD b,
        STORE_ADD_TL tl
 WHERE  b.store = tl.store (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_STORE_ADD_TL is 'This is the translation view for base table STORE_ADD. This view fetches data in user langauge either from translation table STORE_ADD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_STORE_ADD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_STORE_ADD_TL.STORE is 'Contains the number which uniquely identifies the store.'
/

COMMENT ON COLUMN V_STORE_ADD_TL.STORE_NAME is 'Contains the name of the store which, along with the store number, identifies the store.'
/

COMMENT ON COLUMN V_STORE_ADD_TL.STORE_NAME_SECONDARY is 'Secondary name of the store.'
/

