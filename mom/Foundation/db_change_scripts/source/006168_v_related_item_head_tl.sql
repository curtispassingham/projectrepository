CREATE OR REPLACE FORCE VIEW V_RELATED_ITEM_HEAD_TL (RELATIONSHIP_ID, RELATIONSHIP_NAME, LANG ) AS
SELECT  b.relationship_id,
        case when tl.lang is not null then tl.relationship_name else b.relationship_name end relationship_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  RELATED_ITEM_HEAD b,
        RELATED_ITEM_HEAD_TL tl
 WHERE  b.relationship_id = tl.relationship_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_RELATED_ITEM_HEAD_TL is 'This is the translation view for base table RELATED_ITEM_HEAD. This view fetches data in user langauge either from translation table RELATED_ITEM_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_RELATED_ITEM_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_RELATED_ITEM_HEAD_TL.RELATIONSHIP_ID is 'Unique identifier for each relationship header.'
/

COMMENT ON COLUMN V_RELATED_ITEM_HEAD_TL.RELATIONSHIP_NAME is 'Name given to the relationship.'
/

