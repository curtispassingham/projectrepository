--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_PO_TYPE_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_PO_TYPE_TL'
CREATE TABLE SVC_PO_TYPE_TL
(	
PROCESS_ID        NUMBER(10,0) NOT NULL,
CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
ROW_SEQ           NUMBER(20,0) NOT NULL,
ACTION            VARCHAR2(10),
PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
PO_TYPE_DESC VARCHAR2(120) ,
PO_TYPE VARCHAR2(4) ,
LANG NUMBER(6,0) ,
CREATE_ID         VARCHAR2(30) DEFAULT USER ,
CREATE_DATETIME   DATE DEFAULT SYSDATE ,
LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
LAST_UPD_DATETIME   DATE DEFAULT SYSDATE
)
INITRANS 6	
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_PO_TYPE_TL IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in PO_TYPE_TL.'
/
COMMENT ON COLUMN SVC_PO_TYPE_TL.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/
COMMENT ON COLUMN SVC_PO_TYPE_TL.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/
COMMENT ON COLUMN SVC_PO_TYPE_TL.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/
COMMENT ON COLUMN SVC_PO_TYPE_TL.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/
COMMENT ON COLUMN SVC_PO_TYPE_TL.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/
COMMENT ON COLUMN SVC_PO_TYPE_TL.PO_TYPE IS 'This field contains the unique identifier for the order type.'
/
COMMENT ON COLUMN SVC_PO_TYPE_TL.PO_TYPE_DESC IS 'This field contains a description for the specific order type.'
/
COMMENT ON COLUMN SVC_PO_TYPE_TL.LANG IS 'This field contains valid Language for the specific order type.'
/
COMMENT ON COLUMN SVC_PO_TYPE_TL.CREATE_ID IS 'User who created the record.'
/
COMMENT ON COLUMN SVC_PO_TYPE_TL.CREATE_DATETIME IS 'Date time when record was inserted.'
/
COMMENT ON COLUMN SVC_PO_TYPE_TL.LAST_UPD_ID IS 'User who last updated the record.'
/
COMMENT ON COLUMN SVC_PO_TYPE_TL.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/

PROMPT Creating Primary Key on 'SVC_PO_TYPE_TL'   
ALTER TABLE SVC_PO_TYPE_TL
ADD CONSTRAINT SVC_PO_TYPE_TL_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX	
/

PROMPT Creating Unique Key on 'SVC_PO_TYPE_TL'   
ALTER TABLE SVC_PO_TYPE_TL
ADD CONSTRAINT SVC_PO_TYPE_TL_UK UNIQUE ( PO_TYPE,LANG )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX	
/


	
	
	
