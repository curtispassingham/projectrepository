--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE ADDED:				ADDR_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TYPE
--------------------------------------
PROMPT Creating Table 'ADDR_TL'
CREATE TABLE ADDR_TL(
LANG NUMBER(6) NOT NULL,
ADDR_KEY NUMBER(11) NOT NULL,
ADD_1 VARCHAR2(240) NOT NULL,
ADD_2 VARCHAR2(240) ,
ADD_3 VARCHAR2(240) ,
CITY VARCHAR2(120) NOT NULL,
CONTACT_NAME VARCHAR2(120) ,
COUNTY VARCHAR2(250) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE ADDR_TL is 'This is the translation table for ADDR table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN ADDR_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN ADDR_TL.ADDR_KEY is 'This column contains a unique number used to distinguish between different addresses.'
/

COMMENT ON COLUMN ADDR_TL.ADD_1 is 'This column contains the first line of the address.'
/

COMMENT ON COLUMN ADDR_TL.ADD_2 is 'This column contains the second line of the address.'
/

COMMENT ON COLUMN ADDR_TL.ADD_3 is 'This column contains the third line of the address.'
/

COMMENT ON COLUMN ADDR_TL.CITY is 'This column contains the name of the city that is associated with the address.'
/

COMMENT ON COLUMN ADDR_TL.CONTACT_NAME is 'This column contains the name of the contact for the supplier at this address.'
/

COMMENT ON COLUMN ADDR_TL.COUNTY is 'This column holds the county name for the location.'
/

COMMENT ON COLUMN ADDR_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN ADDR_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN ADDR_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN ADDR_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE ADDR_TL ADD CONSTRAINT PK_ADDR_TL PRIMARY KEY (
LANG,
ADDR_KEY
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE ADDR_TL
 ADD CONSTRAINT ADRT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE ADDR_TL ADD CONSTRAINT ADRT_ADR_FK FOREIGN KEY (
ADDR_KEY
) REFERENCES ADDR (
ADDR_KEY
)
/

