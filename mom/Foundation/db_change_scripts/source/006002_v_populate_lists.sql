--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_POPULATE_LISTS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_POPULATE_LISTS'
CREATE OR REPLACE FORCE VIEW "V_POPULATE_LISTS"
("CODE",
 "CODE_TYPE",
 "CODE_SEQ",
 "CODE_DESC")
 AS (SELECT cd.code,
            cd.code_type,
            cd.code_seq,
            trans.code_desc
       FROM code_detail cd
 INNER JOIN code_detail_trans trans
         ON cd.code_type  = trans.code_type
        AND cd.code      = trans.code
      WHERE trans.lang = (SELECT lang
                            FROM lang
                           WHERE ISO_code = UPPER(SYS_CONTEXT('retail_ctx', 'APP_ISO_LANGUAGE')))
    )

/


COMMENT ON COLUMN V_POPULATE_LISTS."CODE" IS 'This field contains the code used in Oracle Retail which must be decoded for display in the on-line forms.'
/

COMMENT ON COLUMN V_POPULATE_LISTS."CODE_TYPE" IS 'This field will contain a valid code type for the row.'
/

COMMENT ON COLUMN V_POPULATE_LISTS."CODE_SEQ" IS 'This is a number used to order the elements so that they appear consistently when using them to populate a list.'
/

COMMENT ON COLUMN V_POPULATE_LISTS."CODE_DESC" IS 'This field contains the description associated with the code and code type.'
/



