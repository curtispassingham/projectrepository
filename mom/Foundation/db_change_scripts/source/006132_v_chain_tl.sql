CREATE OR REPLACE FORCE VIEW V_CHAIN_TL (CHAIN, CHAIN_NAME, LANG ) AS
SELECT  b.chain,
        case when tl.lang is not null then tl.chain_name else b.chain_name end chain_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  CHAIN b,
        CHAIN_TL tl
 WHERE  b.chain = tl.chain (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_CHAIN_TL is 'This is the translation view for base table CHAIN. This view fetches data in user langauge either from translation table CHAIN_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_CHAIN_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_CHAIN_TL.CHAIN is 'Contains the number which uniquely identifies the chain.'
/

COMMENT ON COLUMN V_CHAIN_TL.CHAIN_NAME is 'Contains the name of the chain which, along with the chain number, identifies the chain.'
/

