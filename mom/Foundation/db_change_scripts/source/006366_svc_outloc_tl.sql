--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_OUTLOC_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

CREATE TABLE SVC_OUTLOC_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, OUTLOC_TYPE VARCHAR2(6)
, OUTLOC_ID VARCHAR2(5)
, OUTLOC_DESC VARCHAR2(150)
, OUTLOC_ADD1 VARCHAR2(240)
, OUTLOC_ADD2 VARCHAR2(240)
, OUTLOC_CITY VARCHAR2(120)
, CONTACT_NAME VARCHAR2(120)
, OUTLOC_NAME_SECONDARY VARCHAR2(150)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_OUTLOC_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in OUTLOC_TL.'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_OUTLOC_TL.OUTLOC_TYPE is 'This field holds the type of location, e.g., Discharge Port (DP) or Port of Lading (PL)'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.OUTLOC_ID is 'This is a unique location identification used so that the location can be referenced in other modules.'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.OUTLOC_DESC is 'A description or name of the location.'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.OUTLOC_ADD1 is 'The street address of the location.'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.OUTLOC_ADD2 is 'The second line of a street address.'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.OUTLOC_CITY is 'This field holds the name of the city where the location exists.'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.CONTACT_NAME is 'The name of a person that may be contacted at the particular location.'
/

COMMENT ON COLUMN SVC_OUTLOC_TL.OUTLOC_NAME_SECONDARY is 'Contains the secondary name of the outside location.'
/

ALTER TABLE SVC_OUTLOC_TL
ADD CONSTRAINT SVC_OUTLOC_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SVC_OUTLOC_TL
ADD CONSTRAINT SVC_OUTLOC_TL_UK UNIQUE
(LANG, OUTLOC_TYPE, OUTLOC_ID)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

