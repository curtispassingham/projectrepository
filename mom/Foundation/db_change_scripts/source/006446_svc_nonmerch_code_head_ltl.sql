--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_NONMERCH_CODE_HEAD_LTL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_NONMERCH_CODE_HEAD_LTL'
CREATE TABLE SVC_NONMERCH_CODE_HEAD_LTL
 (PROCESS_ID NUMBER(10) NOT NULL,
  CHUNK_ID NUMBER(10) NOT NULL,
  ROW_SEQ NUMBER(20) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ),
  LANG NUMBER(6),
  NON_MERCH_CODE VARCHAR2(6 ),
  NON_MERCH_CODE_DESC VARCHAR2(120 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_NONMERCH_CODE_HEAD_LTL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded to NON_MERCH_CODE_HEAD_TL.'
/

COMMENT ON COLUMN SVC_NONMERCH_CODE_HEAD_LTL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_NONMERCH_CODE_HEAD_LTL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_NONMERCH_CODE_HEAD_LTL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_NONMERCH_CODE_HEAD_LTL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_NONMERCH_CODE_HEAD_LTL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_NONMERCH_CODE_HEAD_LTL.LANG is 'This field contains the language in which the translated text is maintained.'
/

COMMENT ON COLUMN SVC_NONMERCH_CODE_HEAD_LTL.NON_MERCH_CODE is 'Code identifying a non-merchandise cost that can be added to an invoice.'
/

COMMENT ON COLUMN SVC_NONMERCH_CODE_HEAD_LTL.NON_MERCH_CODE_DESC is 'Description of the non-merchandise cost code.'
/


PROMPT Creating Primary Key on 'SVC_NONMERCH_CODE_HEAD_LTL'
ALTER TABLE SVC_NONMERCH_CODE_HEAD_LTL
 ADD CONSTRAINT SVC_NMERCH_CODE_HEAD_LTL_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_NONMERCH_CODE_HEAD_LTL'
ALTER TABLE SVC_NONMERCH_CODE_HEAD_LTL
 ADD CONSTRAINT SVC_NMERCH_CODE_HEAD_LTL_UK UNIQUE
  (LANG,
   NON_MERCH_CODE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

