--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       PHASES_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE PHASES_TL(
LANG NUMBER(6) NOT NULL,
SEASON_ID NUMBER(3) NOT NULL,
PHASE_ID NUMBER(3) NOT NULL,
PHASE_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE PHASES_TL is 'This is the translation table for PHASES table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN PHASES_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN PHASES_TL.SEASON_ID is 'This field contains the unique identifier for season that the phase is part of.'
/

COMMENT ON COLUMN PHASES_TL.PHASE_ID is 'This field contains the identifier for phase. This number is not unique without the associated season identifier.'
/

COMMENT ON COLUMN PHASES_TL.PHASE_DESC is 'This field holds the description of the phase.'
/

COMMENT ON COLUMN PHASES_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN PHASES_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN PHASES_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN PHASES_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE PHASES_TL ADD CONSTRAINT PK_PHASES_TL PRIMARY KEY (
LANG,
SEASON_ID,
PHASE_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE PHASES_TL
 ADD CONSTRAINT PHST_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE PHASES_TL ADD CONSTRAINT PHST_PHS_FK FOREIGN KEY (
SEASON_ID,
PHASE_ID
) REFERENCES PHASES (
SEASON_ID,
PHASE_ID
)
/

