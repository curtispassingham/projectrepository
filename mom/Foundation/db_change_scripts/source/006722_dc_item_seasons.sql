--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_ITEM_SEASONS
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_ITEM_SEASONS"
CREATE TABLE DC_ITEM_SEASONS
(
   ITEM                   VARCHAR2(25),
   SEASON_ID              NUMBER(3),
   PHASE_ID               NUMBER(3)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_ITEM_SEASONS is 'This table is a staging table for data conversion and will hold data of ITEM_SEASONS.'
/
COMMENT ON COLUMN DC_ITEM_SEASONS.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_ITEM_SEASONS.SEASON_ID is 'The season identifier.'
/
COMMENT ON COLUMN DC_ITEM_SEASONS.PHASE_ID is 'The phase identifier.'
/