--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_PARTNER_ORG_UNIT
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_PARTNER_ORG_UNIT"
CREATE TABLE DC_PARTNER_ORG_UNIT
(
   PARTNER                NUMBER(10),
   ORG_UNIT_ID            NUMBER(15),
   PARTNER_TYPE           VARCHAR2(1),
   PRIMARY_PAY_SITE       VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_PARTNER_ORG_UNIT is 'This table is a staging table for data conversion and will hold data for PARTNER_ORG_UNIT.'
/
COMMENT ON COLUMN DC_PARTNER_ORG_UNIT.PARTNER is 'contains either Suppler or Supplier Site.'
/
COMMENT ON COLUMN DC_PARTNER_ORG_UNIT.ORG_UNIT_ID is 'Contains org_unit_id'
/
COMMENT ON COLUMN DC_PARTNER_ORG_UNIT.PARTNER_TYPE is 'Identifies the type of the partner. S for Supplier and U for Supplier Site.'
/
COMMENT ON COLUMN DC_PARTNER_ORG_UNIT.PRIMARY_PAY_SITE is 'Primary payment site indicator.'
/