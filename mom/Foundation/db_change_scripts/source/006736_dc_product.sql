--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_PRODUCT
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_PRODUCT"
CREATE TABLE DC_PRODUCT
(
   ITEM                      VARCHAR2(25),
   PRIMARY_PRODUCT_IND       VARCHAR2(1),
   ITEM_PARENT               VARCHAR2(25),
   ITEM_DESC                 VARCHAR2(250),
   SHORT_DESC                VARCHAR2(120),
   ITEM_DESC_SECONDARY       VARCHAR2(250),
   COST_ZONE_GROUP_ID        NUMBER(4),
   UOM_CONV_FACTOR           NUMBER(20,10),
   STANDARD_UOM              VARCHAR2(4),
   STORE_ORD_MULT            VARCHAR2(1),
   MERCHANDISE_IND           VARCHAR2(1),
   FORECAST_IND              VARCHAR2(1),
   DIFF_1                    VARCHAR2(10),
   DIFF_2                    VARCHAR2(10),
   DIFF_3                    VARCHAR2(10),
   DIFF_4                    VARCHAR2(10),
   CATCH_WEIGHT_IND          VARCHAR2(1),
   HANDLING_TEMP             VARCHAR2(6),
   HANDLING_SENSITIVITY      VARCHAR2(6),
   WASTE_TYPE                VARCHAR2(6),
   WASTE_PCT                 NUMBER(12,4),
   DEFAULT_WASTE_PCT         NUMBER(12,4),
   PACKAGE_SIZE              NUMBER(12,4),
   PACKAGE_UOM               VARCHAR2(4),
   DEPOSIT_ITEM_TYPE         VARCHAR2(6),
   CONTAINER_ITEM            VARCHAR2(25),
   DEPOSIT_IN_PRICE_PER_UOM  VARCHAR2(6),
   RETAIL_LABEL_TYPE         VARCHAR2(6),
   RETAIL_LABEL_VALUE        NUMBER(20,4),
   COMMENTS                  VARCHAR2(2000),
   AIP_CASE_TYPE             VARCHAR2(6),
   PERISHABLE_IND            VARCHAR2(1),
   PRODUCT_CLASSIFICATION    VARCHAR2(6),
   BRAND_NAME                VARCHAR2(30)  
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_PRODUCT is 'This table is a staging table for data conversion and will hold style item data of ITEM_MASTER table.'
/
COMMENT ON COLUMN DC_PRODUCT.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_PRODUCT.PRIMARY_PRODUCT_IND is 'Identify primary product item.'
/
COMMENT ON COLUMN DC_PRODUCT.ITEM_PARENT is 'Alphanumeric value that uniquely identifies the item/group at the level above the item. This value must exist as an item in another row on the item_master table.'
/
COMMENT ON COLUMN DC_PRODUCT.ITEM_DESC is 'Long description of the item.'
/
COMMENT ON COLUMN DC_PRODUCT.SHORT_DESC is 'Shortened description of the item.'
/
COMMENT ON COLUMN DC_PRODUCT.ITEM_DESC_SECONDARY is 'Secondary descriptions of the item.'
/
COMMENT ON COLUMN DC_PRODUCT.COST_ZONE_GROUP_ID is 'Cost zone group associated with the item. This field is only required when elc_ind (landed cost indicator) is set to Y on the system_options table.'
/
COMMENT ON COLUMN DC_PRODUCT.UOM_CONV_FACTOR is 'Conversion factor between an Each and the standard_uom when the standard_uom is not in the quantity class.'
/
COMMENT ON COLUMN DC_PRODUCT.STANDARD_UOM is 'Unit of measure in which stock of the item is tracked at a corporate level.'
/
COMMENT ON COLUMN DC_PRODUCT.STORE_ORD_MULT is 'Merchandise shipped from the warehouses to the stores must be specified in this unit type. Valid values are: C = Cases I = Inner E = Eaches.'
/
COMMENT ON COLUMN DC_PRODUCT.MERCHANDISE_IND is 'Indicates if the item is a merchandise item (Y, N).'
/
COMMENT ON COLUMN DC_PRODUCT.FORECAST_IND is 'Indicates if this item will be interfaced to an external forecasting system (Y, N).'
/
COMMENT ON COLUMN DC_PRODUCT.DIFF_1 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_PRODUCT.DIFF_2 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_PRODUCT.DIFF_3 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_PRODUCT.DIFF_4 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_PRODUCT.CATCH_WEIGHT_IND is 'Indiactes whether the item should be weighed when it  verage at a location. Valid values for this field are Y and N.'
/
COMMENT ON COLUMN DC_PRODUCT.HANDLING_TEMP is 'Holds the temperature information associated with the item. Valid values for this field are in the code type HTMP on the code_head and code_detail tables.'
/
COMMENT ON COLUMN DC_PRODUCT.HANDLING_SENSITIVITY is 'Holds the sensitivity information associated with the item.'
/
COMMENT ON COLUMN DC_PRODUCT.WASTE_TYPE is 'Identifies the wastage type as either sales or spoilage wastage. Sales wastage occurs during processes that make an item saleable'
/
COMMENT ON COLUMN DC_PRODUCT.WASTE_PCT is 'Average percent of wastage for the item over its shelf life. Used in inflating the retail price for wastage items.'
/
COMMENT ON COLUMN DC_PRODUCT.DEFAULT_WASTE_PCT is 'Default daily wastage percent for spoilage type wastage items.'
/
COMMENT ON COLUMN DC_PRODUCT.PACKAGE_SIZE is 'Holds the size of the product printed on any packaging.'
/
COMMENT ON COLUMN DC_PRODUCT.PACKAGE_UOM is 'Holds the unit of measure associated with the package size.'
/
COMMENT ON COLUMN DC_PRODUCT.DEPOSIT_ITEM_TYPE is 'This is the deposit item component type. A NULL value in this field indicates that this item is not part of a deposit item relationship.'
/
COMMENT ON COLUMN DC_PRODUCT.CONTAINER_ITEM is 'This holds the container item number for a contents item. This field is only populated and required if the DEPOSIT_ITEM_TYPE = E.'
/
COMMENT ON COLUMN DC_PRODUCT.DEPOSIT_IN_PRICE_PER_UOM is 'This field indicates if the deposit amount is included in the price per UOM calculation for a contents item ticket.'
/
COMMENT ON COLUMN DC_PRODUCT.RETAIL_LABEL_TYPE is 'This field indicates any special lable type assoctiated with an item.'
/
COMMENT ON COLUMN DC_PRODUCT.RETAIL_LABEL_VALUE is 'This field represents the value associated with the retail label type.'
/
COMMENT ON COLUMN DC_PRODUCT.AIP_CASE_TYPE is 'Only used if AIP is integrated. Determines which case sizes to extract against an item in the AIP interface.'
/
COMMENT ON COLUMN DC_PRODUCT.COMMENTS is 'Holds any comments associated with the item.'
/
COMMENT ON COLUMN DC_PRODUCT.PRODUCT_CLASSIFICATION is 'Product classification is informational only in RMS, but is used by RWMS to determine how to pack customer orders : such as to determine products that may not be able to be packaged together.'
/
COMMENT ON COLUMN DC_PRODUCT.PERISHABLE_IND is 'A grocery item attribute used to indicate whether an item is perishable or not.'
/
COMMENT ON COLUMN DC_PRODUCT.BRAND_NAME is 'This field contains the brand associated to an item.'
/