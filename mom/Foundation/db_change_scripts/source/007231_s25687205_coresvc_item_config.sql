--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'CORESVC_ITEM_CONFIG'
ALTER TABLE CORESVC_ITEM_CONFIG ADD DEFAULT_ALL_LOCS_ISCL VARCHAR (1 ) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN CORESVC_ITEM_CONFIG.DEFAULT_ALL_LOCS_ISCL is 'This indicates whether all ranged locations should be defaulted to supplier/countries or not upon the uploading an Item-Supplier-Country record into RMS.If ''Y'' default all locations ranged otherwise locations passed in file/xml would be defaulted.'
/

COMMENT ON COLUMN CORESVC_ITEM_CONFIG.CASCADE_IUD_ITEM_SUPP_COUNTRY is 'This field indicates whether the inserts, updates or deletes on item/supplier/country and item/supplier/country/loc details should be cascaded to the child items or not'
/

