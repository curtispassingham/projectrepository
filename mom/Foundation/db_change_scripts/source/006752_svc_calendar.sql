--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_CALENDAR
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_CALENDAR'
CREATE TABLE SVC_CALENDAR
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
    NO_OF_WEEKS NUMBER(1,0) ,
    MONTH_454 NUMBER(2,0) ,
    YEAR_454 NUMBER(4,0) ,
    FIRST_DAY DATE ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE
	)	
	INITRANS 6
	TABLESPACE RETAIL_DATA
/


COMMENT ON TABLE SVC_CALENDAR IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in CALENDAR.'
/

COMMENT ON COLUMN SVC_CALENDAR.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_CALENDAR.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_CALENDAR.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_CALENDAR.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_CALENDAR.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_CALENDAR.NO_OF_WEEKS IS 'Indicates whether there are 4 or 5 calendar weeks in the 4-5-4 month.'
/

COMMENT ON COLUMN SVC_CALENDAR.MONTH_454 IS 'Contains the number ranging from 1 to 12 which indicates the 4-5-4 month.'
/

COMMENT ON COLUMN SVC_CALENDAR.YEAR_454 IS 'Contains the year that the 4-5-4 month falls in. The 4-5-4 year begins on the first day of the first 4-5-4 month regardless of when the calendar year begins.'
/

COMMENT ON COLUMN SVC_CALENDAR.FIRST_DAY IS 'Contains the first day of the 4-5-4 month.'
/

COMMENT ON COLUMN SVC_CALENDAR.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_CALENDAR.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_CALENDAR.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_CALENDAR.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/

PROMPT Creating Primary Key on 'SVC_CALENDAR'       
ALTER TABLE SVC_CALENDAR
ADD CONSTRAINT SVC_CALENDAR_PK PRIMARY KEY 
( 
  PROCESS_ID,
  ROW_SEQ 
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX	     
/

PROMPT Creating Unique Key on 'SVC_CALENDAR'
ALTER TABLE SVC_CALENDAR
ADD CONSTRAINT SVC_CALENDAR_UK UNIQUE ( YEAR_454,MONTH_454 )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX	   
/
