CREATE OR REPLACE FORCE VIEW V_OUTLOC_TL (OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_ADD1, OUTLOC_ADD2, OUTLOC_CITY, CONTACT_NAME, OUTLOC_NAME_SECONDARY, LANG ) AS
SELECT  b.outloc_type,
        b.outloc_id,
        case when tl.lang is not null then tl.outloc_desc else b.outloc_desc end outloc_desc,
        case when tl.lang is not null then tl.outloc_add1 else b.outloc_add1 end outloc_add1,
        case when tl.lang is not null then tl.outloc_add2 else b.outloc_add2 end outloc_add2,
        case when tl.lang is not null then tl.outloc_city else b.outloc_city end outloc_city,
        case when tl.lang is not null then tl.contact_name else b.contact_name end contact_name,
        case when tl.lang is not null then tl.outloc_name_secondary else b.outloc_name_secondary end outloc_name_secondary,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  OUTLOC b,
        OUTLOC_TL tl
 WHERE  b.outloc_type = tl.outloc_type (+)
   AND  b.outloc_id = tl.outloc_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_OUTLOC_TL is 'This is the translation view for base table OUTLOC. This view fetches data in user langauge either from translation table OUTLOC_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_OUTLOC_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_OUTLOC_TL.OUTLOC_TYPE is 'This field holds the type of location, e.g., Discharge Port (DP) or Port of Lading (PL)'
/

COMMENT ON COLUMN V_OUTLOC_TL.OUTLOC_ID is 'This is a unique location identification used so that the location can be referenced in other modules.'
/

COMMENT ON COLUMN V_OUTLOC_TL.OUTLOC_DESC is 'A description or name of the location.'
/

COMMENT ON COLUMN V_OUTLOC_TL.OUTLOC_ADD1 is 'The street address of the location.'
/

COMMENT ON COLUMN V_OUTLOC_TL.OUTLOC_ADD2 is 'The second line of a street address.'
/

COMMENT ON COLUMN V_OUTLOC_TL.OUTLOC_CITY is 'This field holds the name of the city where the location exists.'
/

COMMENT ON COLUMN V_OUTLOC_TL.CONTACT_NAME is 'The name of a person that may be contacted at the particular location.'
/

COMMENT ON COLUMN V_OUTLOC_TL.OUTLOC_NAME_SECONDARY is 'Contains the secondary name of the outside location.'
/

