--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


-----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
---------------------------------------------------------------------------------------
--Materialized View Created:                               SUBCLASS_SALES_FORECAST
---------------------------------------------------------------------------------------

whenever sqlerror exit
PROMPT create materialized view SUBCLASS_SALES_FORECAST
CREATE MATERIALIZED VIEW SUBCLASS_SALES_FORECAST
ON PREBUILT TABLE 
WITH REDUCED PRECISION
REFRESH FORCE ON DEMAND 
ENABLE QUERY REWRITE
           AS SELECT im.dept dept,
                     im.class class,
                     im.subclass subclass,
                     1 domain_id,
                     f.loc loc,
                     f.eow_date eow_date,
                     SUM(f.forecast_sales) forecast_sales
                FROM item_master im, 
                     item_forecast f,
                     store s
               WHERE im.item = f.item
                 AND f.loc  = s.store
            GROUP BY im.dept, im.class, im.subclass, f.loc, f.eow_date
/