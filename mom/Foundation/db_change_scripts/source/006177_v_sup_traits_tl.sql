CREATE OR REPLACE FORCE VIEW V_SUP_TRAITS_TL (SUP_TRAIT, DESCRIPTION, LANG ) AS
SELECT  b.sup_trait,
        case when tl.lang is not null then tl.description else b.description end description,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SUP_TRAITS b,
        SUP_TRAITS_TL tl
 WHERE  b.sup_trait = tl.sup_trait (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SUP_TRAITS_TL is 'This is the translation view for base table SUP_TRAITS. This view fetches data in user langauge either from translation table SUP_TRAITS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SUP_TRAITS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SUP_TRAITS_TL.SUP_TRAIT is 'This field contains the number that uniquely identifies the supplier trait.'
/

COMMENT ON COLUMN V_SUP_TRAITS_TL.DESCRIPTION is 'This field contains the description associated with the supplier trait.'
/

