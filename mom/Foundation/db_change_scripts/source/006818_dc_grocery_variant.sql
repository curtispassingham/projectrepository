--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_GROCERY_VARIANT
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_GROCERY_VARIANT"
CREATE TABLE DC_GROCERY_VARIANT
(
   ITEM                     VARCHAR2(25),
   ITEM_NUMBER_TYPE         VARCHAR2(6),
   FORMAT_ID                VARCHAR2(1),
   PREFIX                   NUMBER(2),
   ITEM_DESC                VARCHAR2(250),
   SHORT_DESC               VARCHAR2(120),
   ITEM_DESC_SECONDARY      VARCHAR2(250),   
   ITEM_PARENT              VARCHAR2(25),
   ITEM_GRANDPARENT         VARCHAR2(25),
   PRIMARY_REF_ITEM_IND     VARCHAR2(1),
   AIP_CASE_TYPE            VARCHAR2(6),
   COMMENTS                 VARCHAR2(2000),
   PRODUCT_CLASSIFICATION   VARCHAR2(6),
   BRAND_NAME               VARCHAR2(30)  
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
 COMMENT ON TABLE DC_GROCERY_VARIANT is 'This table is a staging table for data conversion and will hold product line variant data of ITEM_MASTER table.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.ITEM_NUMBER_TYPE is 'Code specifying what type the item is. Valid values for this field are in the code type UPCT on the code_head and code_detail tables.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.FORMAT_ID is 'This field will hold the format ID that corresponds to the items variable UPC.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.PREFIX is 'This column holds the prefix for variable weight UPCs.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.ITEM_DESC is 'Long description of the item.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.SHORT_DESC is 'Shortened description of the item.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.ITEM_DESC_SECONDARY is 'Secondary descriptions of the item.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.ITEM_PARENT is 'Alphanumeric value that uniquely identifies the item/group at the level above the item. This value must exist as an item in another row on the item_master table.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.ITEM_GRANDPARENT is 'Alphanumeric value that uniquely identifies the item/group at the level above the item. This value must exist as both an item and an item parent inanother row on the item_master table.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.PRIMARY_REF_ITEM_IND is 'Indicates if the sub-transation level item is designated as the primary sub-transaction level item. For transaction level items and above the value in this field will be No.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.AIP_CASE_TYPE is 'Only used if AIP is integrated. Determines which case sizes to extract against an item in the AIP interface.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.COMMENTS is 'Holds any comments associated with the item.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.PRODUCT_CLASSIFICATION is 'Product classification is informational only in RMS, but is used by RWMS to determine how to pack customer orders : such as to determine products that may not be able to be packaged together.'
/
COMMENT ON COLUMN DC_GROCERY_VARIANT.BRAND_NAME is 'This field contains the brand associated to an item.'
/