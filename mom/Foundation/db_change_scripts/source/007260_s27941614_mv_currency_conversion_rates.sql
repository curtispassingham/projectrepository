--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


-----------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to 
--   ensure data is preserved as desired.
--
-----------------------------------------------------------------
--	MVIEW MODIFIED:			MV_CURRENCY_CONVERSION_RATES
--	INDEX MODIFIED:			MV_CURR_CONVER_RATES_I1
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       MODIFYING INDEX
--------------------------------------
PROMPT DROPPING INDEX 'MV_CURR_CONVER_RATES_I1'
DECLARE
  L_index_exists number := 0;
BEGIN
 SELECT count(*) INTO L_index_exists
   FROM USER_INDEXES
  WHERE INDEX_NAME ='MV_CURR_CONVER_RATES_I1'
    AND TABLE_NAME ='MV_CURRENCY_CONVERSION_RATES';
	
  if (L_index_exists != 0)then
      execute immediate 'DROP INDEX MV_CURR_CONVER_RATES_I1' ;
 end if;
end;
/

PROMPT CREATING INDEX 'MV_CURR_CONVER_RATES_I1'
CREATE INDEX MV_CURR_CONVER_RATES_I1 ON MV_CURRENCY_CONVERSION_RATES
(
FROM_CURRENCY,
TO_CURRENCY,
EXCHANGE_TYPE,
EFFECTIVE_DATE,
EXCHANGE_RATE
)
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/
