--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_PARTNER
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_PARTNER"
CREATE TABLE DC_PARTNER
(
   PARTNER_TYPE             VARCHAR2(6),
   PARTNER_ID               VARCHAR2(10),
   PARTNER_DESC             VARCHAR2(240),
   CURRENCY_CODE            VARCHAR2(3),
   LANG                     NUMBER(6),
   STATUS                   VARCHAR2(1),
   CONTACT_NAME             VARCHAR2(120),
   CONTACT_PHONE            VARCHAR2(20),
   CONTACT_FAX              VARCHAR2(20),
   CONTACT_TELEX            VARCHAR2(20),
   CONTACT_EMAIL            VARCHAR2(100),
   MFG_ID                   VARCHAR2(18),
   PRINCIPLE_COUNTRY_ID     VARCHAR2(3),
   LINE_OF_CREDIT           NUMBER(20,4),
   OUTSTAND_CREDIT          NUMBER(20,4),
   OPEN_CREDIT              NUMBER(20,4),
   YTD_CREDIT               NUMBER(20,4),
   YTD_DRAWDOWNS            NUMBER(20,4),
   TAX_ID                   VARCHAR2(18),
   TERMS                    VARCHAR2(15),
   SERVICE_PERF_REQ_IND     VARCHAR2(1),
   INVC_PAY_LOC             VARCHAR2(6),
   INVC_RECEIVE_LOC         VARCHAR2(6),
   IMPORT_COUNTRY_ID        VARCHAR2(3),
   PRIMARY_IA_IND           VARCHAR2(1),
   COMMENT_DESC             VARCHAR2(2000),
   TSF_ENTITY_ID            NUMBER(10),
   VAT_REGION               NUMBER(4),
   ORG_UNIT_ID              NUMBER(15),
   PARTNER_NAME_SECONDARY   VARCHAR2(240),
   AUTO_RECEIVE_IND         VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /

COMMENT ON TABLE DC_PARTNER is 'This table is a staging table for data conversion and will hold the partner information.'
/
COMMENT ON COLUMN DC_PARTNER.PARTNER_TYPE is 'Specifies the type of partner.'
/
COMMENT ON COLUMN DC_PARTNER.PARTNER_ID is 'Unique identifying number for a partner within the system.'
/
COMMENT ON COLUMN DC_PARTNER.PARTNER_DESC is 'Contains the partners description or name.'
/
COMMENT ON COLUMN DC_PARTNER.CURRENCY_CODE is 'Contains a code identifying the currency the partner uses for business transactions.'
/
COMMENT ON COLUMN DC_PARTNER.LANG is 'This field contains the partners preferred language.'
/
COMMENT ON COLUMN DC_PARTNER.STATUS is 'Determines whether the partner is currently active.'
/
COMMENT ON COLUMN DC_PARTNER.CONTACT_NAME is 'Contains the name of the partners representative contract.'
/
COMMENT ON COLUMN DC_PARTNER.CONTACT_PHONE is 'Contains the phone number of the partners representative contact.'
/
COMMENT ON COLUMN DC_PARTNER.CONTACT_FAX is 'Contains the fax number of the partners representative contact.'
/
COMMENT ON COLUMN DC_PARTNER.CONTACT_TELEX is 'Contains the telex number of the partners representative contact.'
/
COMMENT ON COLUMN DC_PARTNER.CONTACT_EMAIL is 'Contains the e-mail address of the partners representative contact.'
/
COMMENT ON COLUMN DC_PARTNER.MFG_ID is 'Contains the manufacturers tax identification number.'
/
COMMENT ON COLUMN DC_PARTNER.PRINCIPLE_COUNTRY_ID is 'Contains the country id to which the partner is assigned.'
/
COMMENT ON COLUMN DC_PARTNER.LINE_OF_CREDIT is 'Contains the line of credit the company has at the Bank in the Partners currency.'
/
COMMENT ON COLUMN DC_PARTNER.OUTSTAND_CREDIT is 'Contains the total amount of credit that the company has used or has charged against in the Partners currency.'
/
COMMENT ON COLUMN DC_PARTNER.OPEN_CREDIT is 'Contains the total amount that the company can still charge against in the Partners currency.'
/
COMMENT ON COLUMN DC_PARTNER.YTD_CREDIT is 'Contains the total amount of credit the company has used this year to date in the Partners currency.'
/
COMMENT ON COLUMN DC_PARTNER.YTD_DRAWDOWNS is 'Contains the year to date payments the bank has made on behalf of the company in the Partners currency.'
/
COMMENT ON COLUMN DC_PARTNER.TAX_ID is 'Contains the unique tax identification number of the partner.'
/
COMMENT ON COLUMN DC_PARTNER.TERMS is 'Payment terms for the partner.'
/
COMMENT ON COLUMN DC_PARTNER.SERVICE_PERF_REQ_IND is 'Indicates if the expense vendors services (e.g. snowplowing, window washing) must be confirmed as performed before paying an invoice from that expense vendor.'
/
COMMENT ON COLUMN DC_PARTNER.INVC_PAY_LOC is 'Indicates where invoices from this expense vendor are paid - at the store or centrally through corporate accounting.'
/
COMMENT ON COLUMN DC_PARTNER.INVC_RECEIVE_LOC is 'Indicates where invoices from this expense vendor are received - at the store or centrally through corporate accounting.'
/
COMMENT ON COLUMN DC_PARTNER.IMPORT_COUNTRY_ID is 'Import country of the Import Authority.'
/
COMMENT ON COLUMN DC_PARTNER.PRIMARY_IA_IND is 'Indicates if an Import Authority is the primary Import Authority for an import country.'
/
COMMENT ON COLUMN DC_PARTNER.COMMENT_DESC is 'Contains any comments associated with the Partner.'
/
COMMENT ON COLUMN DC_PARTNER.TSF_ENTITY_ID is 'ID of the transfer entity with which an external finisher (partner_type = E) is associated.'
/
COMMENT ON COLUMN DC_PARTNER.VAT_REGION is 'VAT (value added tax) region with which a partner is associated. Valid values will be found on the VAT_REGION table.'
/
COMMENT ON COLUMN DC_PARTNER.ORG_UNIT_ID is 'Org Unit Id'
/
COMMENT ON COLUMN DC_PARTNER.PARTNER_NAME_SECONDARY is 'This wil hold the secondary name of the partner.'
/
COMMENT ON COLUMN DC_PARTNER.AUTO_RECEIVE_IND is 'This will indicate whether the system will update the stock for the external finisher when the 1st leg of the transfer is shipped.'
/