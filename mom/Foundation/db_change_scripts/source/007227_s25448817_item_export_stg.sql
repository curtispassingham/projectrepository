--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ITEM_EXPORT_STG'

PROMPT MODIFYING CONSTRAINT 'CHK_ITEM_EXPORT_STG_ACTN_TYPE'
DECLARE
  L_con_exists number := 0;
BEGIN
  SELECT count(*) INTO L_con_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ITEM_EXPORT_STG_ACTN_TYPE'
     AND CONSTRAINT_TYPE = 'C';
   
  if (L_con_exists != 0) then
      execute immediate 'ALTER TABLE ITEM_EXPORT_STG DROP CONSTRAINT  CHK_ITEM_EXPORT_STG_ACTN_TYPE';
  end if;
end;
/

ALTER TABLE ITEM_EXPORT_STG ADD CONSTRAINT
 CHK_ITEM_EXPORT_STG_ACTN_TYPE CHECK (ACTION_TYPE IN ( 'itemhdrmod', 'itemhdrdel', 'itemloccre', 'itemlocmod', 'itemlocdel', 'itemupccre','itemupcdel','vatitemcre', 'vatitemmod', 'vatitemdel'))
/
