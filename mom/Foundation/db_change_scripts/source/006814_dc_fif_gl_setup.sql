--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_FIF_GL_SETUP
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_FIF_GL_SETUP"
CREATE TABLE DC_FIF_GL_SETUP
(
   SET_OF_BOOKS_ID                           NUMBER(15),
   LAST_UPDATE_ID                            NUMBER(15),
   SEQUENCE1_DESC                            VARCHAR2(20),
   SEQUENCE2_DESC                            VARCHAR2(20),
   SEQUENCE3_DESC                            VARCHAR2(20),
   SEQUENCE4_DESC                            VARCHAR2(20),
   SEQUENCE5_DESC                            VARCHAR2(20),
   SEQUENCE6_DESC                            VARCHAR2(20),
   SEQUENCE7_DESC                            VARCHAR2(20),
   SEQUENCE8_DESC                            VARCHAR2(20),
   SEQUENCE9_DESC                            VARCHAR2(20),
   SEQUENCE10_DESC                           VARCHAR2(20),
   CATEGORY_ID                               NUMBER(38),
   DELIVER_TO_LOCATION_ID                    NUMBER(15),
   DESTINATION_ORGANIZATION_ID               NUMBER(38),
   PERIOD_NAME                               VARCHAR2(15),
   SET_OF_BOOKS_DESC                         VARCHAR2(120),
   CURRENCY_CODE                             VARCHAR2(3)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_FIF_GL_SETUP is 'This table is a staging table for data conversion and will hold all the intial set-up information pertaining to Financial Application.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.SET_OF_BOOKS_ID is 'Oracle set of books for Oracle Retail transactions.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.LAST_UPDATE_ID is 'Oracle last update ID, default for all Oracle Retail transactions.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.SEQUENCE1_DESC is 'Contains description for sequence columns on the interface cross reference form.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.SEQUENCE2_DESC is 'Contains description for sequence columns on the interface cross reference form.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.SEQUENCE3_DESC is 'Contains description for sequence columns on the interface cross reference form.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.SEQUENCE4_DESC is 'Contains description for sequence columns on the interface cross reference form.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.SEQUENCE5_DESC is 'Contains description for sequence columns on the interface cross reference form.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.SEQUENCE6_DESC is 'Contains description for sequence columns on the interface cross reference form.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.SEQUENCE7_DESC is 'Contains description for sequence columns on the interface cross reference form.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.SEQUENCE8_DESC is 'Contains description for sequence columns on the interface cross reference form.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.SEQUENCE9_DESC is 'Contains description for sequence columns on the interface cross reference form.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.SEQUENCE10_DESC is 'Contains description for sequence columns on the interface cross reference form.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.CATEGORY_ID is 'Oracle category ID, default for Oracle Retail purchase order feed.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.DELIVER_TO_LOCATION_ID is 'Oracle location_id, default for Oracle Retail purchase order feed.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.DESTINATION_ORGANIZATION_ID is 'Oracle organization_id, default for Oracle Retail purchase order feed.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.PERIOD_NAME is 'This denotes the user entered accounting period name as defined in Financial Applications'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.SET_OF_BOOKS_DESC is 'Set Of Books Description.'
/
COMMENT ON COLUMN DC_FIF_GL_SETUP.CURRENCY_CODE is 'Currency code for the Set Of Book ID.'
/