--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_BRAND
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_BRAND"
CREATE TABLE DC_BRAND
(
   BRAND_NAME                 VARCHAR2(30),
   BRAND_DESCRIPTION          VARCHAR2(120)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_BRAND is 'This table is a staging table for data conversion and will hold to display brand information of an item.'
/
COMMENT ON COLUMN DC_BRAND.BRAND_NAME is 'This column holds the Brand name.'
/
COMMENT ON COLUMN DC_BRAND.BRAND_DESCRIPTION is 'This column holds the description of the Brand.'
/