--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE ADDED:				CHANNELS_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TYPE
--------------------------------------
PROMPT Creating Table 'CHANNELS_TL'
CREATE TABLE CHANNELS_TL(
LANG NUMBER(6) NOT NULL,
CHANNEL_ID NUMBER(4) NOT NULL,
CHANNEL_NAME VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE CHANNELS_TL is 'This is the translation table for CHANNELS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN CHANNELS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN CHANNELS_TL.CHANNEL_ID is 'This column will contain the number that uniquely identifies the channel.'
/

COMMENT ON COLUMN CHANNELS_TL.CHANNEL_NAME is 'Contains the name of the channel.'
/

COMMENT ON COLUMN CHANNELS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN CHANNELS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN CHANNELS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN CHANNELS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE CHANNELS_TL ADD CONSTRAINT PK_CHANNELS_TL PRIMARY KEY (
LANG,
CHANNEL_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE CHANNELS_TL
 ADD CONSTRAINT CHNLT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE CHANNELS_TL ADD CONSTRAINT CHNLT_CHNL_FK FOREIGN KEY (
CHANNEL_ID
) REFERENCES CHANNELS (
CHANNEL_ID
)
/

