--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	VIEW Added: 		 V_PEND_MERCH_HIER
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating VIEW               
--------------------------------------
PROMPT Creating View 'V_PEND_MERCH_HIER'
CREATE OR REPLACE FORCE VIEW V_PEND_MERCH_HIER AS
(
   SELECT PEND.*,
          NVL(DI.DIVISION, PEND.MERCH_HIER_ID) AS DIVISION,
          NVL(DI.DIV_NAME, PEND.MERCH_HIER_NAME) AS DIV_NAME,
          NULL          AS GROUP_NO,
          NULL          AS GROUP_NAME,
          NULL          AS DEPT,
          NULL          AS DEPT_NAME,
          NULL          AS CLASS,
          NULL          AS CLASS_NAME,
          NULL          AS SUBCLASS,
          NULL          AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          DIVISION DI
    WHERE PEND.HIER_TYPE = 'V'
      AND PEND.MERCH_HIER_ID = DI.DIVISION (+)
   UNION ALL
   SELECT PEND.*,
          NVL(DI.DIVISION, P2.MERCH_HIER_ID)   AS DIVISION,
          NVL(DI.DIV_NAME, P2.MERCH_HIER_NAME) AS DIV_NAME,
          NVL(GS.GROUP_NO, PEND.MERCH_HIER_ID) AS GROUP_NO,
          NVL(GS.GROUP_NAME, PEND.MERCH_HIER_NAME) AS GROUP_NAME,
          NULL          AS DEPT,
          NULL          AS DEPT_NAME,
          NULL          AS CLASS,
          NULL          AS CLASS_NAME,
          NULL          AS SUBCLASS,
          NULL          AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          PEND_MERCH_HIER P2,
          DIVISION DI,
          GROUPS GS
    WHERE PEND.HIER_TYPE = 'G'
      AND PEND.MERCH_HIER_ID = GS.GROUP_NO (+)
      AND PEND.MERCH_HIER_PARENT_ID = DI.DIVISION (+)
      AND DI.DIVISION        = GS.DIVISION (+)
      AND P2.HIER_TYPE = 'V'
      AND PEND.MERCH_HIER_PARENT_ID = P2.MERCH_HIER_ID
   UNION ALL
   SELECT PEND.*,
          NVL(HIER.DIVISION, P2.MERCH_HIER_PARENT_ID)   AS DIVISION,
          NVL(HIER.DIV_NAME, P3.MERCH_HIER_NAME)   AS DIV_NAME,
          NVL(HIER.GROUP_NO, P2.MERCH_HIER_ID)  AS GROUP_NO,
          NVL(HIER.GROUP_NAME, P2.MERCH_HIER_NAME) AS GROUP_NAME,
          NVL(DS.DEPT, PEND.MERCH_HIER_ID) AS DEPT,
          NVL(DS.DEPT_NAME, PEND.MERCH_HIER_NAME) AS DEPT_NAME,
          NULL          AS CLASS,
          NULL          AS CLASS_NAME,
          NULL          AS SUBCLASS,
          NULL          AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          (SELECT MERCH_HIER_ID, MERCH_HIER_PARENT_ID, MERCH_HIER_NAME
             FROM PEND_MERCH_HIER
            WHERE HIER_TYPE = 'G'
              AND ACTION_TYPE = 'A') P2,
          (SELECT MERCH_HIER_ID, MERCH_HIER_NAME
             FROM PEND_MERCH_HIER
            WHERE HIER_TYPE = 'V'
              AND ACTION_TYPE = 'A') P3,
          (SELECT DI.DIVISION, DI.DIV_NAME, GS.GROUP_NO, GS.GROUP_NAME
             FROM DIVISION DI,
                  GROUPS GS
            WHERE DI.DIVISION = GS.DIVISION ) HIER,
          DEPS DS
    WHERE PEND.HIER_TYPE = 'D'
      AND PEND.MERCH_HIER_ID = DS.DEPT (+)
      AND PEND.MERCH_HIER_PARENT_ID = HIER.GROUP_NO (+)
      AND HIER.GROUP_NO = DS.GROUP_NO (+)
      AND PEND.MERCH_HIER_PARENT_ID = P2.MERCH_HIER_ID (+)
      AND P2.MERCH_HIER_PARENT_ID = P3.MERCH_HIER_ID (+)
   UNION ALL
   SELECT PEND.*,
          NVL(HIER.DIVISION, P2.DIVISION)   AS DIVISION,
          NVL(HIER.DIV_NAME, P2.DIV_NAME)   AS DIV_NAME,
          NVL(HIER.GROUP_NO, P2.GROUP_NO)   AS GROUP_NO,
          NVL(HIER.GROUP_NAME, P2.GROUP_NAME) AS GROUP_NAME,
          NVL(HIER.DEPT, P2.MERCH_HIER_ID)       AS DEPT,
          NVL(HIER.DEPT_NAME, P2.MERCH_HIER_NAME)  AS DEPT_NAME,
          NVL(CL.CLASS, PEND.MERCH_HIER_ID) AS CLASS,
          NVL(CL.CLASS_NAME, PEND.MERCH_HIER_NAME) AS CLASS_NAME,
          NULL          AS SUBCLASS,
          NULL          AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,          
          (SELECT PEND.*,
                  NVL(HIER.DIVISION, P2.MERCH_HIER_PARENT_ID)   AS DIVISION,
                  NVL(HIER.DIV_NAME, P3.MERCH_HIER_NAME)   AS DIV_NAME,
                  NVL(HIER.GROUP_NO, P2.MERCH_HIER_ID)  AS GROUP_NO,
                  NVL(HIER.GROUP_NAME, P2.MERCH_HIER_NAME) AS GROUP_NAME,
                  NVL(DS.DEPT, PEND.MERCH_HIER_ID) AS DEPT,
                  NVL(DS.DEPT_NAME, PEND.MERCH_HIER_NAME) AS DEPT_NAME
             FROM PEND_MERCH_HIER PEND,
                   (SELECT MERCH_HIER_ID, MERCH_HIER_PARENT_ID, MERCH_HIER_NAME
                      FROM PEND_MERCH_HIER
                     WHERE HIER_TYPE = 'G'
                       AND ACTION_TYPE = 'A') P2,
                   (SELECT MERCH_HIER_ID, MERCH_HIER_NAME
                      FROM PEND_MERCH_HIER
                     WHERE HIER_TYPE = 'V'
                       AND ACTION_TYPE = 'A') P3,
                   (SELECT DI.DIVISION, DI.DIV_NAME, GS.GROUP_NO, GS.GROUP_NAME
                      FROM DIVISION DI,
                           GROUPS GS
                     WHERE DI.DIVISION = GS.DIVISION ) HIER,
                  DEPS DS
            WHERE PEND.HIER_TYPE = 'D'
              AND PEND.MERCH_HIER_ID = DS.DEPT (+)
              AND PEND.MERCH_HIER_PARENT_ID = HIER.GROUP_NO (+)
              AND HIER.GROUP_NO = DS.GROUP_NO (+)
              AND PEND.MERCH_HIER_PARENT_ID = P2.MERCH_HIER_ID (+)
              AND P2.MERCH_HIER_PARENT_ID = P3.MERCH_HIER_ID (+)
              AND ACTION_TYPE = 'A') P2,
          (SELECT DI.DIVISION, DI.DIV_NAME, GS.GROUP_NO, GS.GROUP_NAME, DS.DEPT, DS.DEPT_NAME
             FROM DIVISION DI,
                  GROUPS GS,
                  DEPS DS
            WHERE DI.DIVISION = GS.DIVISION 
              AND GS.GROUP_NO = DS.GROUP_NO) HIER,
          CLASS CL
    WHERE PEND.HIER_TYPE = 'C'
      AND PEND.MERCH_HIER_ID        = CL.CLASS (+)
      AND PEND.MERCH_HIER_PARENT_ID = HIER.DEPT (+)
      AND HIER.DEPT = CL.DEPT (+)
      AND PEND.MERCH_HIER_PARENT_ID = P2.MERCH_HIER_ID (+)
   UNION ALL
   SELECT PEND.*,
          NVL(HIER.DIVISION, P2.DIVISION)   AS DIVISION,
          NVL(HIER.DIV_NAME, P2.DIV_NAME)   AS DIV_NAME,
          NVL(HIER.GROUP_NO, P2.GROUP_NO)   AS GROUP_NO,
          NVL(HIER.GROUP_NAME, P2.GROUP_NAME) AS GROUP_NAME,
          NVL(HIER.DEPT, P2.DEPT)       AS DEPT,
          NVL(HIER.DEPT_NAME, P2.DEPT_NAME)  AS DEPT_NAME,
          NVL(HIER.CLASS, P2.CLASS)      AS CLASS,
          NVL(HIER.CLASS_NAME, P2.CLASS_NAME) AS CLASS_NAME,
          NVL(SC.SUBCLASS, PEND.MERCH_HIER_ID) AS SUBCLASS,
          NVL(SC.SUB_NAME, PEND.MERCH_HIER_NAME) AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          (SELECT DI.DIVISION, DI.DIV_NAME, GS.GROUP_NO, GS.GROUP_NAME, DS.DEPT, DS.DEPT_NAME, CL.CLASS, CL.CLASS_NAME
             FROM DIVISION DI,
                  GROUPS GS,
                  DEPS DS,
                  CLASS CL
            WHERE DI.DIVISION = GS.DIVISION 
              AND GS.GROUP_NO = DS.GROUP_NO
              AND DS.DEPT  = CL.DEPT) HIER,
          SUBCLASS SC,
          (SELECT PEND.MERCH_HIER_ID, PEND.MERCH_HIER_NAME,
                   NVL(HIER.DIVISION, P2.DIVISION)   AS DIVISION,
                   NVL(HIER.DIV_NAME, P2.DIV_NAME)   AS DIV_NAME,
                   NVL(HIER.GROUP_NO, P2.GROUP_NO)   AS GROUP_NO,
                   NVL(HIER.GROUP_NAME, P2.GROUP_NAME) AS GROUP_NAME,
                   NVL(HIER.DEPT, P2.MERCH_HIER_ID)       AS DEPT,
                   NVL(HIER.DEPT_NAME, P2.MERCH_HIER_NAME)  AS DEPT_NAME,
                   NVL(CL.CLASS, PEND.MERCH_HIER_ID) AS CLASS,
                   NVL(CL.CLASS_NAME, PEND.MERCH_HIER_NAME) AS CLASS_NAME
              FROM PEND_MERCH_HIER PEND,          
                   (SELECT PEND.*,
                           NVL(HIER.DIVISION, P2.MERCH_HIER_PARENT_ID)   AS DIVISION,
                           NVL(HIER.DIV_NAME, P3.MERCH_HIER_NAME)   AS DIV_NAME,
                           NVL(HIER.GROUP_NO, P2.MERCH_HIER_ID)  AS GROUP_NO,
                           NVL(HIER.GROUP_NAME, P2.MERCH_HIER_NAME) AS GROUP_NAME,
                           NVL(DS.DEPT, PEND.MERCH_HIER_ID) AS DEPT,
                           NVL(DS.DEPT_NAME, PEND.MERCH_HIER_NAME) AS DEPT_NAME
                      FROM PEND_MERCH_HIER PEND,
                            (SELECT MERCH_HIER_ID, MERCH_HIER_PARENT_ID, MERCH_HIER_NAME
                               FROM PEND_MERCH_HIER
                              WHERE HIER_TYPE = 'G'
                                AND ACTION_TYPE = 'A') P2,
                            (SELECT MERCH_HIER_ID, MERCH_HIER_NAME
                               FROM PEND_MERCH_HIER
                              WHERE HIER_TYPE = 'V'
                                AND ACTION_TYPE = 'A') P3,
                            (SELECT DI.DIVISION, DI.DIV_NAME, GS.GROUP_NO, GS.GROUP_NAME
                               FROM DIVISION DI,
                                    GROUPS GS
                              WHERE DI.DIVISION = GS.DIVISION ) HIER,
                           DEPS DS
                     WHERE PEND.HIER_TYPE = 'D'
                       AND PEND.MERCH_HIER_ID = DS.DEPT (+)
                       AND PEND.MERCH_HIER_PARENT_ID = HIER.GROUP_NO (+)
                       AND HIER.GROUP_NO = DS.GROUP_NO (+)
                       AND PEND.MERCH_HIER_PARENT_ID = P2.MERCH_HIER_ID (+)
                       AND P2.MERCH_HIER_PARENT_ID = P3.MERCH_HIER_ID (+)
                       AND ACTION_TYPE = 'A') P2,
                   (SELECT DI.DIVISION, DI.DIV_NAME, GS.GROUP_NO, GS.GROUP_NAME, DS.DEPT, DS.DEPT_NAME
                      FROM DIVISION DI,
                           GROUPS GS,
                           DEPS DS
                     WHERE DI.DIVISION = GS.DIVISION 
                       AND GS.GROUP_NO = DS.GROUP_NO) HIER,
                   CLASS CL
             WHERE PEND.HIER_TYPE = 'C'
               AND PEND.MERCH_HIER_ID        = CL.CLASS (+)
               AND PEND.MERCH_HIER_PARENT_ID = HIER.DEPT (+)
               AND HIER.DEPT = CL.DEPT (+)
               AND PEND.MERCH_HIER_PARENT_ID = P2.MERCH_HIER_ID (+)
               AND PEND.ACTION_TYPE = 'A') P2
    WHERE PEND.HIER_TYPE = 'S'
      AND PEND.MERCH_HIER_ID             = SC.SUBCLASS (+)
      AND PEND.MERCH_HIER_PARENT_ID      = SC.CLASS (+)
      AND PEND.MERCH_HIER_GRANDPARENT_ID = SC.DEPT (+)
      AND PEND.MERCH_HIER_PARENT_ID      = HIER.CLASS (+)
      AND PEND.MERCH_HIER_GRANDPARENT_ID = HIER.DEPT (+)
      AND HIER.DEPT  = SC.DEPT (+)
      AND HIER.CLASS = SC.CLASS (+)
      AND PEND.MERCH_HIER_PARENT_ID = P2.MERCH_HIER_ID (+)
)
/
