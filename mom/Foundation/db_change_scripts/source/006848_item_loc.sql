--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ITEM_LOC'

PROMPT Modifying Index 'ITEM_LOC_I7'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_INDEXES
   WHERE INDEX_NAME = 'ITEM_LOC_I7';

  if (L_table_exists != 0) then
      execute immediate 'DROP INDEX ITEM_LOC_I7';
  end if;
end;
/

PROMPT Creating Index on 'ITEM_LOC'
CREATE INDEX ITEM_LOC_I7 on ITEM_LOC
  (LOC,
   STATUS,
   RANGED_IND
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

