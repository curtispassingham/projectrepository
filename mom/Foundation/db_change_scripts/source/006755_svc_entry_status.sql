  --------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_ENTRY_STATUS
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_ENTRY_STATUS'
CREATE TABLE SVC_ENTRY_STATUS
   (	
   PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
    ENTRY_STATUS_DESC VARCHAR2(250) ,
    IMPORT_COUNTRY_ID VARCHAR2(3) ,
    ENTRY_STATUS VARCHAR2(6) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE 
	)
	INITRANS 6	
	TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_ENTRY_STATUS IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in ENTRY_STATUS.'
/

COMMENT ON COLUMN SVC_ENTRY_STATUS.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_ENTRY_STATUS.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_ENTRY_STATUS.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_ENTRY_STATUS.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_ENTRY_STATUS.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_ENTRY_STATUS.ENTRY_STATUS_DESC IS 'This column will hold the description of the entry status.'
/

COMMENT ON COLUMN SVC_ENTRY_STATUS.ENTRY_STATUS IS 'This will hold the unique identifier for the custom defined entry status.'
/

COMMENT ON COLUMN SVC_ENTRY_STATUS.IMPORT_COUNTRY_ID IS 'This column will hold the import country.'
/

COMMENT ON COLUMN SVC_ENTRY_STATUS.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_ENTRY_STATUS.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_ENTRY_STATUS.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_ENTRY_STATUS.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_ENTRY_STATUS'   
ALTER TABLE SVC_ENTRY_STATUS
ADD CONSTRAINT SVC_ENTRY_STATUS_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX	    
/

PROMPT Creating Unique Key on 'SVC_ENTRY_STATUS'   
ALTER TABLE SVC_ENTRY_STATUS
ADD CONSTRAINT SVC_ENTRY_STATUS_UK UNIQUE ( IMPORT_COUNTRY_ID,ENTRY_STATUS )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX	
/
