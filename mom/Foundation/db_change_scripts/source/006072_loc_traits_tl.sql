--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       LOC_TRAITS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE LOC_TRAITS_TL(
LANG NUMBER(6) NOT NULL,
LOC_TRAIT NUMBER(4) NOT NULL,
DESCRIPTION VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE LOC_TRAITS_TL is 'This is the translation table for LOC_TRAITS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN LOC_TRAITS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN LOC_TRAITS_TL.LOC_TRAIT is 'Contains a number which uniquely identifies a location trait.'
/

COMMENT ON COLUMN LOC_TRAITS_TL.DESCRIPTION is 'Contains a description which corresponds with the location trait number.'
/

COMMENT ON COLUMN LOC_TRAITS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN LOC_TRAITS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN LOC_TRAITS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN LOC_TRAITS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE LOC_TRAITS_TL ADD CONSTRAINT PK_LOC_TRAITS_TL PRIMARY KEY (
LANG,
LOC_TRAIT
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE LOC_TRAITS_TL
 ADD CONSTRAINT LOTT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE LOC_TRAITS_TL ADD CONSTRAINT LOTT_LOT_FK FOREIGN KEY (
LOC_TRAIT
) REFERENCES LOC_TRAITS (
LOC_TRAIT
)
/

