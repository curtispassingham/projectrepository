--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_ITEM_COST_HEAD
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_ITEM_COST_HEAD"
CREATE TABLE DC_ITEM_COST_HEAD
(
   ITEM                          VARCHAR2(25),
   SUPPLIER                      NUMBER(10),
   ORIGIN_COUNTRY_ID             VARCHAR2(3),
   DELIVERY_COUNTRY_ID           VARCHAR2(3),
   PRIM_DLVY_CTRY_IND            VARCHAR2(1),
   NIC_STATIC_IND                VARCHAR2(1),
   BASE_COST                     NUMBER(20,4),
   NEGOTIATED_ITEM_COST          NUMBER(20,4),
   EXTENDED_BASE_COST            NUMBER(20,4),
   INCLUSIVE_COST                NUMBER(20,4)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_ITEM_COST_HEAD is 'This table is a staging table for data conversion and will hold data of ITEM_COST_HEAD table.'
/
COMMENT ON COLUMN DC_ITEM_COST_HEAD.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_ITEM_COST_HEAD.SUPPLIER is 'The unique identifier for the supplier.'
/
COMMENT ON COLUMN DC_ITEM_COST_HEAD.ORIGIN_COUNTRY_ID is 'The country where the item was manufactured or significantly altered.'
/
COMMENT ON COLUMN DC_ITEM_COST_HEAD.DELIVERY_COUNTRY_ID is 'Country to which the item will be delivered to.'
/
COMMENT ON COLUMN DC_ITEM_COST_HEAD.PRIM_DLVY_CTRY_IND is 'Indicates if the country is the primary delivery country of the item.'
/
COMMENT ON COLUMN DC_ITEM_COST_HEAD.NIC_STATIC_IND is 'Indicates if the Negotiated Item Cost (NIC) is static or not.'
/
COMMENT ON COLUMN DC_ITEM_COST_HEAD.BASE_COST is 'This will hold the tax exclusive cost of the item.'
/
COMMENT ON COLUMN DC_ITEM_COST_HEAD.NEGOTIATED_ITEM_COST is 'This will hold the supplier negotiated item cost.'
/
COMMENT ON COLUMN DC_ITEM_COST_HEAD.EXTENDED_BASE_COST is 'This will hold the extended base cost of the item.'
/
COMMENT ON COLUMN DC_ITEM_COST_HEAD.INCLUSIVE_COST is 'This will hold the inclusive cost of the item.'
/