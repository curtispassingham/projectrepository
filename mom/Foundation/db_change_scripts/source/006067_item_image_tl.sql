--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       ITEM_IMAGE_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE ITEM_IMAGE_TL(
LANG NUMBER(6) NOT NULL,
ITEM VARCHAR2(25) NOT NULL,
IMAGE_NAME VARCHAR2(120) NOT NULL,
IMAGE_DESC VARCHAR2(40) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE ITEM_IMAGE_TL is 'This is the translation table for ITEM_IMAGE table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN ITEM_IMAGE_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN ITEM_IMAGE_TL.ITEM is 'This field contains the unique alphanumeric identifier for the item, the image is for.'
/

COMMENT ON COLUMN ITEM_IMAGE_TL.IMAGE_NAME is 'This field contains the name of the image of the item.'
/

COMMENT ON COLUMN ITEM_IMAGE_TL.IMAGE_DESC is 'This field contains the description associated with the image of the item.'
/

COMMENT ON COLUMN ITEM_IMAGE_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN ITEM_IMAGE_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN ITEM_IMAGE_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN ITEM_IMAGE_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE ITEM_IMAGE_TL ADD CONSTRAINT PK_ITEM_IMAGE_TL PRIMARY KEY (
LANG,
ITEM,
IMAGE_NAME
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE ITEM_IMAGE_TL
 ADD CONSTRAINT IMGT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE ITEM_IMAGE_TL ADD CONSTRAINT IMGT_IMG_FK FOREIGN KEY (
ITEM,
IMAGE_NAME
) REFERENCES ITEM_IMAGE (
ITEM,
IMAGE_NAME
)
/

