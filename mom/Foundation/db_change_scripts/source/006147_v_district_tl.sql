CREATE OR REPLACE FORCE VIEW V_DISTRICT_TL (DISTRICT, DISTRICT_NAME, LANG ) AS
SELECT  b.district,
        case when tl.lang is not null then tl.district_name else b.district_name end district_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  DISTRICT b,
        DISTRICT_TL tl
 WHERE  b.district = tl.district (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_DISTRICT_TL is 'This is the translation view for base table DISTRICT. This view fetches data in user langauge either from translation table DISTRICT_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_DISTRICT_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_DISTRICT_TL.DISTRICT is 'Contains the number which uniquely identifies the district.'
/

COMMENT ON COLUMN V_DISTRICT_TL.DISTRICT_NAME is 'Contains the name of the district which, along with the district number, identifies the district.'
/

