--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_DEPS
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table DC_DEPS
CREATE TABLE DC_DEPS
(
   DEPT                          NUMBER(4),
   DEPT_NAME                     VARCHAR2(120),
   BUYER                         NUMBER(4),
   MERCH                         NUMBER(4),
   PROFIT_CALC_TYPE              NUMBER(1),
   PURCHASE_TYPE                 NUMBER(1),
   GROUP_NO                      NUMBER(4),
   MRKUP_PCT                     NUMBER(12,4),
   TOTAL_MARKET_AMT              NUMBER(24,4),
   MARKUP_CALC_TYPE              VARCHAR2(2),
   OTB_CALC_TYPE                 VARCHAR2(1),
   MAX_AVG_COUNTER               NUMBER(5),
   AVG_TOLERANCE_PCT             NUMBER(12,4),
   DEPT_VAT_INCL_IND             VARCHAR2(1),
   LOWEST_STRATEGY_LEVEL         NUMBER(6),
   WORKSHEET_LEVEL               NUMBER(6),
   HISTORICAL_SALES_LEVEL        NUMBER(6),
   REGULAR_SALES_IND             NUMBER(6),
   CLEARANCE_SALES_IND           NUMBER(6),
   PROMOTIONAL_SALES_IND         NUMBER(6),
   INCLUDE_WH_ON_HAND            NUMBER(6),
   INCLUDE_WH_ON_ORDER           NUMBER(6),
   PRICE_CHANGE_AMOUNT_CALC_TYPE NUMBER(6),
   RETAIL_CHG_HIGHLIGHT_DAYS     NUMBER(4),
   COST_CHG_HIGHLIGHT_DAYS       NUMBER(4),
   PEND_COST_CHG_WINDOW_DAYS     NUMBER(4),
   PEND_COST_CHG_HIGHLIGHT_DAYS  NUMBER(4)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_DEPS is 'This table is a staging table for data conversion and will hold data for department table..'
/
COMMENT ON COLUMN DC_DEPS.DEPT is 'Contains the number which uniquely identifies the department.'
/
COMMENT ON COLUMN DC_DEPS.DEPT_NAME is 'Contains the name of the department.'
/
COMMENT ON COLUMN DC_DEPS.BUYER is 'Contains the number which uniquely identifies the buyer for that department.'
/
COMMENT ON COLUMN DC_DEPS.MERCH is 'Contains the number which uniquely describes the merchandiser for that department.'
/
COMMENT ON COLUMN DC_DEPS.PROFIT_CALC_TYPE is 'Contains a number which indicates whether profit will be calculated by Direct Cost or by Retail Inventory.'
/
COMMENT ON COLUMN DC_DEPS.PURCHASE_TYPE is 'Contains a code which indicates whether items in this department are normal merchandise, consignment stock or concession items.'
/
COMMENT ON COLUMN DC_DEPS.GROUP_NO is 'Contains the number of the group in which the department exists.'
/
COMMENT ON COLUMN DC_DEPS.MRKUP_PCT is 'Contains the default markup percentage.'
/
COMMENT ON COLUMN DC_DEPS.TOTAL_MARKET_AMT is 'Contains the total market amount that is expected for the department. The field can be used for analytics or reporting.'
/
COMMENT ON COLUMN DC_DEPS.MARKUP_CALC_TYPE is 'Contains the code letter which determines how markup is calculated in this department.'
/
COMMENT ON COLUMN DC_DEPS.OTB_CALC_TYPE is 'Contains the code letter which determines how OTB is calculated in this department.'
/
COMMENT ON COLUMN DC_DEPS.MAX_AVG_COUNTER is 'A maximum average counter will hold the maximum count of days with acceptable data to include in an Average for items within the department.'
/
COMMENT ON COLUMN DC_DEPS.AVG_TOLERANCE_PCT is 'A tolerance percentage value used in averaging for items within this value. This value will set up a range for appropriate data and constrain outliers.'
/
COMMENT ON COLUMN DC_DEPS.DEPT_VAT_INCL_IND is 'This indicator will be be used only to default to the class level indicator when classes are initially set up for the department and will only be available when the system level class vat option is on.'
/
COMMENT ON COLUMN DC_DEPS.LOWEST_STRATEGY_LEVEL is 'The lowest level at which a strategy may be defined.'
/
COMMENT ON COLUMN DC_DEPS.WORKSHEET_LEVEL is 'The value in this field identifies what merchandise level should be used to build the default worksheet. This level will be either at or above the value in the lowest_strategy_level.'
/
COMMENT ON COLUMN DC_DEPS.HISTORICAL_SALES_LEVEL is 'Determines what period should be used by the merchandise extract when extracting historical sales.'
/
COMMENT ON COLUMN DC_DEPS.REGULAR_SALES_IND is 'The value in this field indicates if regular price sales should be included as part of the historical sales extracted by the merchandise extract.'
/
COMMENT ON COLUMN DC_DEPS.CLEARANCE_SALES_IND is 'The value in this field indicates if clearance price sales should be included as part of the historical sales extracted by the merchandise extract.'
/
COMMENT ON COLUMN DC_DEPS.PROMOTIONAL_SALES_IND is 'The value in this field indicates if promotional price sales should be included as part of the historical sales extracted by the merchandise extract.'
/
COMMENT ON COLUMN DC_DEPS.INCLUDE_WH_ON_HAND is 'This indicator will be used by the merchandise extract to determine whether or not to include the warehouse on hand when calculating sell thru and price change impact.'
/
COMMENT ON COLUMN DC_DEPS.INCLUDE_WH_ON_ORDER is 'This indicator will be used by the merchandise extract to determine whether or not to include the warehouse on order when calculating the total on order quantity.'
/
COMMENT ON COLUMN DC_DEPS.PRICE_CHANGE_AMOUNT_CALC_TYPE is 'The value in this field determines the calculation method for the price change amount column on the worksheet and worksheet status screens.'
/
COMMENT ON COLUMN DC_DEPS.RETAIL_CHG_HIGHLIGHT_DAYS is 'Defines a window of recent price changes the worksheet will highlight past price changes that fall within this window.'
/
COMMENT ON COLUMN DC_DEPS.COST_CHG_HIGHLIGHT_DAYS is 'Defines a window of recent cost changes the worksheet will highlight past cost changes that fall within this window.'
/
COMMENT ON COLUMN DC_DEPS.PEND_COST_CHG_WINDOW_DAYS is 'Defines how many days forward the worksheet will look to find upcoming cost changes.'
/
COMMENT ON COLUMN DC_DEPS.PEND_COST_CHG_HIGHLIGHT_DAYS is 'Defines a window of upcoming cost changes the worksheet will highlight upcoming cost changes that fall within this window'
/