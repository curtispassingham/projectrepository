--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RTK_ROLE_PRIVS'
ALTER TABLE RTK_ROLE_PRIVS MODIFY ORD_APPR_AMT NUMBER (20,4) NULL
/

COMMENT ON COLUMN RTK_ROLE_PRIVS.ORD_APPR_AMT is 'This field contains the upper limit that the role is able to approve on an order.  If not defined, then the role can approve any order amount. This value is expressed in primary currency.'
/

