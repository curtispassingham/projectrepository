--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_RELATED_ITEM_DETAIL
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_RELATED_ITEM_DETAIL"
CREATE TABLE DC_RELATED_ITEM_DETAIL
(
   RELATIONSHIP_ID        NUMBER(20),
   RELATED_ITEM           VARCHAR2(25),
   PRIORITY               NUMBER(4),
   START_DATE             DATE,
   END_DATE               DATE
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_RELATED_ITEM_DETAIL is 'This table is a staging table for data conversion and will hold related item information which needs to be passed to ORPOS.'
/
COMMENT ON COLUMN DC_RELATED_ITEM_DETAIL.RELATIONSHIP_ID is 'The relationship id for the item.'
/
COMMENT ON COLUMN DC_RELATED_ITEM_DETAIL.RELATED_ITEM is 'Item id of the related item.'
/
COMMENT ON COLUMN DC_RELATED_ITEM_DETAIL.PRIORITY is 'In case of multiple related substitute items, this column could be used (optional) to define relative priority.'
/
COMMENT ON COLUMN DC_RELATED_ITEM_DETAIL.START_DATE is 'From this date related item can be used on transactions.'
/
COMMENT ON COLUMN DC_RELATED_ITEM_DETAIL.END_DATE is 'Till this date related item can be used on transactions.'
/