--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	View CREATED:						V_ITEM_SEARCH 
---------------------------------------------------------------------------
CREATE OR REPLACE FORCE VIEW V_ITEM_SEARCH
("ITEM", 
 "ITEM_DESC", 
 "ITEM_DESC_SECONDARY", 
 "ITEM_LEVEL", 
 "ITEM_LEVEL_DESC", 
 "TRAN_LEVEL", 
 "TRAN_LEVEL_DESC", 
 "ITEM_TYPE", 
 "SUPPLIER", 
 "SUPPLIER_PARENT", 
 "VPN", 
 "DEPT1", 
 "DEPT_NAME", 
 "CLASS1", 
 "CLASS_NAME", 
 "SUBCLASS1", 
 "SUB_NAME", 
 "STATUS", 
 "DIVISION", 
 "DIV_NAME", 
 "GROUP_NO", 
 "GROUP_NAME", 
 "ITEM_GRANDPARENT", 
 "ITEM_PARENT", 
 "SHORT_DESC", 
 "ITEM_NUMBER_TYPE", 
 "SELLABLE_IND", 
 "ORDERABLE_IND", 
 "INVENTORY_IND", 
 "PACK_IND", 
 "PACK_TYPE", 
 "ORDER_AS_TYPE", 
 "DIFF_1", 
 "DIFF_2", 
 "DIFF_3", 
 "DIFF_4", 
 "COST_ZONE_GROUP_ID", 
 "STANDARD_UOM", 
 "UOM_CONV_FACTOR", 
 "PACKAGE_SIZE", 
 "PACKAGE_UOM", 
 "ORIGINAL_RETAIL", 
 "MFG_REC_RETAIL", 
 "RETAIL_LABEL_TYPE", 
 "RETAIL_LABEL_VALUE", 
 "MERCHANDISE_IND", 
 "STORE_ORD_MULT", 
 "FORECAST_IND", 
 "CONTAINS_INNER_IND", 
 "COMMENTS", 
 "ITEM_XFORM_IND", 
 "CATCH_WEIGHT_IND", 
 "CONST_DIMEN_IND", 
 "HANDLING_TEMP", 
 "HANDLING_SENSITIVITY", 
 "WASTE_TYPE", 
 "WASTE_PCT", 
 "DEFAULT_WASTE_PCT", 
 "DEPOSIT_ITEM_TYPE", 
 "PACK_TYPE_DESC", 
 "ORDER_AS_TYPE_DESC", 
 "SUPPLIER_CALC", 
 "SUPPLIER_PARENT_CALC", 
 "VPN_CALC", 
 "DIFF1_DESC", 
 "DIFF1_TYPE", 
 "DIFF1_TYPE_DESC", 
 "DIFF2_DESC", 
 "DIFF2_TYPE", 
 "DIFF2_TYPE_DESC", 
 "DIFF3_DESC", 
 "DIFF3_TYPE", 
 "DIFF3_TYPE_DESC", 
 "DIFF4_DESC", 
 "DIFF4_TYPE", 
 "DIFF4_TYPE_DESC", 
 "COST_ZONE_GROUP_DESC", 
 "WASTE_TYPE_DESC", 
 "HANDLING_SENS_DESC", 
 "HANDLING_TEMP_DESC", 
 "STORE_ORD_MULT_DESC", 
 "RETAIL_LABEL_TYPE_DESC", 
 "DEPOSIT_ITEM_TYPE_DESC") 
AS (
  select item.item,
         item.item_desc,
         item.item_desc_secondary,
         item.item_level,
         (select code_desc from v_code_detail ilvs where item.item_level = ilvs.code and ilvs.code_type = 'ILVS') as item_level_desc,
         item.tran_level,
         (select code_desc from v_code_detail tlvl where item.tran_level=tlvl.code and tlvl.code_type='TLVL') as tran_level_desc,
         case
            when item.simple_pack_ind = 'Y' then
               'S'
            when item.pack_ind = 'Y' and item.simple_pack_ind = 'N' then
               'C'
            when item.deposit_item_type is not null then
               item.deposit_item_type
            when item.item_xform_ind = 'Y' and item.orderable_ind = 'Y' then
               'O'
            when item.item_xform_ind = 'Y' and item.orderable_ind = 'N' then
               'L'
            when deps.purchase_type in (1, 2) then
               'I'
            else 'R'
            end item_type,
         supp.supplier,
         supp.supplier_parent,
         supp.vpn,
         mhier.dept as dept1,
         mhier.dept_name,
         mhier.class as class1,
         mhier.class_name,
         mhier.subclass as subclass1,
         mhier.sub_name,
         decode((select 'Y' 
                   from daily_purge
                  where upper(table_name) = 'ITEM_MASTER'
                    and (key_value = item.item or key_value = item.item_parent or key_value = item.item_grandparent) 
                    and rownum=1), 'Y', 'D', item.status) status,
         mhier.division,
         mhier.div_name,
         mhier.group_no,
         mhier.group_name,
         item.item_grandparent,
         item.item_parent,
         item.short_desc,
         item.item_number_type,        
         item.sellable_ind,
         item.orderable_ind,
         item.inventory_ind,
         item.pack_ind,
         item.pack_type,
         item.order_as_type,
         item.diff_1,
         item.diff_2,
         item.diff_3,
         item.diff_4,
         item.cost_zone_group_id,        
         item.standard_uom,
         item.uom_conv_factor,
         item.package_size,
         item.package_uom,
         item.original_retail,
         item.mfg_rec_retail,
         item.retail_label_type,
         item.retail_label_value,
         item.merchandise_ind,
         item.store_ord_mult,
         item.forecast_ind,
         item.contains_inner_ind,
         item.comments,
         item.item_xform_ind,
         item.catch_weight_ind,
         item.const_dimen_ind,
         item.handling_temp,
         item.handling_sensitivity,
         item.waste_type,
         item.waste_pct,
         item.default_waste_pct,
         item.deposit_item_type,
         (select code_desc from v_code_detail pctp where item.pack_type = pctp.code and pctp.code_type = 'PATP') pack_type_desc,
         (select code_desc from v_code_detail parc where item.order_as_type = parc.code and parc.code_type = 'PARC') order_as_type_desc,
         -- vpn
         decode(supp.vpn, null, null, supp.supplier) supplier_calc,   
         decode(supp.vpn, null, null, supp.supplier_parent) supplier_parent_calc,
         decode(supp.vpn, null, 
                          (select code_desc from v_code_detail labl 
                            where labl.code = 'ITM' and labl.code_type = 'LABL' )||': '||item.item, 
                          supp.vpn) vpn_calc,
         -- diff_1
         (select vdg.description from v_diff_id_group_type vdg where vdg.id_group = item.diff_1) diff1_desc,
         (select vdg.diff_type from v_diff_id_group_type vdg where vdg.id_group = item.diff_1) diff1_type,
         (select dtp.diff_type_desc  
            from diff_type dtp, v_diff_id_group_type vdg 
           where vdg.diff_type = dtp.diff_type 
             and vdg.id_group = item.diff_1) diff1_type_desc,
         -- diff_2
         (select vdg.description from v_diff_id_group_type vdg where vdg.id_group = item.diff_2) diff2_desc,
         (select vdg.diff_type from v_diff_id_group_type vdg where vdg.id_group = item.diff_2) diff2_type,
         (select dtp.diff_type_desc  
            from diff_type dtp, v_diff_id_group_type vdg 
           where vdg.diff_type = dtp.diff_type 
             and vdg.id_group = item.diff_2) diff2_type_desc,
         -- diff_3
         (select vdg.description from v_diff_id_group_type vdg where vdg.id_group = item.diff_3) diff3_desc,
         (select vdg.diff_type from v_diff_id_group_type vdg where vdg.id_group = item.diff_3) diff3_type,
         (select dtp.diff_type_desc  
            from diff_type dtp, v_diff_id_group_type vdg 
           where vdg.diff_type = dtp.diff_type 
             and vdg.id_group = item.diff_3) diff3_type_desc,
         -- diff_4
         (select vdg.description from v_diff_id_group_type vdg where vdg.id_group = item.diff_4) diff4_desc,
         (select vdg.diff_type from v_diff_id_group_type vdg where vdg.id_group = item.diff_4) diff4_type,
         (select dtp.diff_type_desc  
            from diff_type dtp, v_diff_id_group_type vdg 
           where vdg.diff_type = dtp.diff_type 
             and vdg.id_group = item.diff_4) diff4_type_desc,
         (select code_desc 
            from cost_zone_group cg,      
                 v_code_detail cd 
           where cg.cost_level = cd.code 
             and cd.code_type = 'CZGP' and cg.zone_group_id  = item.cost_zone_group_id)  cost_zone_group_desc,
         (select code_desc from v_code_detail wstg where item.waste_type = wstg.code and wstg.code_type = 'WSTG') waste_type_desc,        
         (select code_desc from v_code_detail hsen where item.handling_sensitivity = hsen.code and hsen.code_type = 'HSEN') handling_sens_desc, 
         (select code_desc from v_code_detail htmp where item.handling_temp = htmp.code and htmp.code_type = 'HTMP') handling_temp_desc,
         (select code_desc from v_code_detail orml where item.store_ord_mult = orml.code and orml.code_type = 'ORML') store_ord_mult_desc,       
         (select code_desc from v_code_detail rtlt where item.retail_label_type = rtlt.code and rtlt.code_type = 'RTLT') retail_label_type_desc,
         (select code_desc from v_code_detail itmt where item.deposit_item_type=itmt.code and itmt.code_type='ITMT') as deposit_item_type_desc
   from v_item_master item, 
        (select item_supplier.*,
                v_sups.supplier_parent
           from item_supplier, v_sups
          where item_supplier.supplier = v_sups.supplier
            and primary_supp_ind = 'Y') supp, 
        v_merch_hierarchy mhier,
        deps
  where item.item = supp.item(+)
    and item.dept   = mhier.dept
    and item.class = mhier.class
    and item.subclass = mhier.subclass
    and deps.dept = item.dept)
/

COMMENT ON TABLE V_ITEM_SEARCH IS 'This view is used for the item search screen in RMS Alloy. It includes all fields that can be used as item search criteria along with their descriptions to be displayed in the search result.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ITEM" IS 'Item Id.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ITEM_DESC" IS 'Item description.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ITEM_DESC_SECONDARY" IS 'Item secondary description.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ITEM_LEVEL" IS 'Item level.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ITEM_LEVEL_DESC" IS 'Translated description of the item level.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."TRAN_LEVEL" IS 'Transaction level.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."TRAN_LEVEL_DESC" IS 'Translated description of the transaction level.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ITEM_TYPE" IS 'Derived attribute of item type based on item attributes to indicate if the item is a regular item, pack, simple pack, transform orderable, transform sellable, a consignment or concession item, or a type of deposit item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."SUPPLIER" IS 'The primary supplier of an item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."SUPPLIER_PARENT" IS 'The primary supplier parent of an item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."VPN" IS 'The vpn associated with the primary supplier of an item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DEPT1" IS 'The merchandise department of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DEPT_NAME" IS 'The merchandise department name of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."CLASS1" IS 'The merchandise class of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."CLASS_NAME" IS 'The merchandise class name of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."SUBCLASS1" IS 'The merchandise subclass of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."SUB_NAME" IS 'The merchandise subclass name of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."STATUS" IS 'Item status. If an item or its parent or grandparent is on daily purge, the item is in D (deleted) status.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIVISION" IS 'The merchandise division of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIV_NAME" IS 'The merchandise division name of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."GROUP_NO" IS 'The merchandise group of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."GROUP_NAME" IS 'The merchandise group name of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ITEM_GRANDPARENT" IS 'Item grandparent.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ITEM_PARENT" IS 'Item parent.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."SHORT_DESC" IS 'Item short description.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ITEM_NUMBER_TYPE" IS 'Item number type.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."SELLABLE_IND" IS 'Item sellable indicator.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ORDERABLE_IND" IS 'Item orderable indicator.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."INVENTORY_IND" IS 'Item inventory indicator.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."PACK_IND" IS 'Item pack indicator.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."PACK_TYPE" IS 'Pack type of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ORDER_AS_TYPE" IS 'Order as type of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF_1" IS 'Differentiator 1 of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF_2" IS 'Differentiator 2 of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF_3" IS 'Differentiator 3 of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF_4" IS 'Differentiator 4 of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."COST_ZONE_GROUP_ID" IS 'The cost zone group of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."STANDARD_UOM" IS 'The standard unit of measure of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."UOM_CONV_FACTOR" IS 'The unit of measure conversion factor between the standard uom and eaches.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."PACKAGE_SIZE" IS 'The package size of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."PACKAGE_UOM" IS 'The unit of measure of the package size.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ORIGINAL_RETAIL" IS 'The original retail of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."MFG_REC_RETAIL" IS 'The manufacturer recommended retail of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."RETAIL_LABEL_TYPE" IS 'The retail label type.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."RETAIL_LABEL_VALUE" IS 'The retail label value.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."MERCHANDISE_IND" IS 'The merchandise indicator.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."STORE_ORD_MULT" IS 'The store order multiple.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."FORECAST_IND" IS 'The forecast indicator.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."CONTAINS_INNER_IND" IS 'The contains inner indicator.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."COMMENTS" IS 'The comments.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ITEM_XFORM_IND" IS 'The transformable item indicator.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."CATCH_WEIGHT_IND" IS 'The catch weight item indicator.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."CONST_DIMEN_IND" IS 'The constant dimension indicator.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."HANDLING_TEMP" IS 'The handling temperature.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."HANDLING_SENSITIVITY" IS 'The handling sensitivity.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."WASTE_TYPE" IS 'The wastage type of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."WASTE_PCT" IS 'The wastage percentage of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DEFAULT_WASTE_PCT" IS 'The default wastage percentage of the item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DEPOSIT_ITEM_TYPE" IS 'The deposit type of a deposit item.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."PACK_TYPE_DESC" IS 'The translated description of the pack type.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."ORDER_AS_TYPE_DESC" IS 'The translated description of the order as type.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."SUPPLIER_CALC" IS 'The supplier of the item''s primary vpn.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."SUPPLIER_PARENT_CALC" IS 'The parent supplier of the item''s primary vpn.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."VPN_CALC" IS 'The vpn associated with the primary supplier of an item. If the item does not have any vpn, use item id with translated label.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF1_DESC" IS 'The translated description of item''s diff1.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF1_TYPE" IS 'The diff type of diff1.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF1_TYPE_DESC" IS 'The translated diff type description of item''s diff1.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF2_DESC" IS 'The translated description of item''s diff2.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF2_TYPE" IS 'The diff type of diff2.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF2_TYPE_DESC" IS 'The translated diff type description of item''s diff2.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF3_DESC" IS 'The translated description of item''s diff3.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF3_TYPE" IS 'The diff type of diff3.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF3_TYPE_DESC" IS 'The translated diff type description of item''s diff3.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF4_DESC" IS 'The translated description of item''s diff4.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF4_TYPE" IS 'The diff type of diff4.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DIFF4_TYPE_DESC" IS 'The translated diff type description of item''s diff4.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."COST_ZONE_GROUP_DESC" IS 'The translated description of item''s cost zone group.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."WASTE_TYPE_DESC" IS 'The translated description of item''s wastage type.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."HANDLING_SENS_DESC" IS 'The translated description of item''s handling sensitivity.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."HANDLING_TEMP_DESC" IS 'The translated description of item''s handling temperature.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."STORE_ORD_MULT_DESC" IS 'The translated description of item''s store order multiple.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."RETAIL_LABEL_TYPE_DESC" IS 'The translated description of item''s retail label type.'
/

COMMENT ON COLUMN V_ITEM_SEARCH."DEPOSIT_ITEM_TYPE_DESC" IS 'The translated description of item''s deposit item type.'
/

