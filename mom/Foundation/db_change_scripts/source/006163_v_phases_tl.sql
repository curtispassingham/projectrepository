CREATE OR REPLACE FORCE VIEW V_PHASES_TL (SEASON_ID, PHASE_ID, PHASE_DESC, LANG ) AS
SELECT  b.season_id,
        b.phase_id,
        case when tl.lang is not null then tl.phase_desc else b.phase_desc end phase_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  PHASES b,
        PHASES_TL tl
 WHERE  b.season_id = tl.season_id (+)
   AND  b.phase_id = tl.phase_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_PHASES_TL is 'This is the translation view for base table PHASES. This view fetches data in user langauge either from translation table PHASES_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_PHASES_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_PHASES_TL.SEASON_ID is 'This field contains the unique identifier for season that the phase is part of.'
/

COMMENT ON COLUMN V_PHASES_TL.PHASE_ID is 'This field contains the identifier for phase. This number is not unique without the associated season identifier.'
/

COMMENT ON COLUMN V_PHASES_TL.PHASE_DESC is 'This field holds the description of the phase.'
/

