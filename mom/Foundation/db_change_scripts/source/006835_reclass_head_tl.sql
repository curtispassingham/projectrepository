--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RECLASS_HEAD_TL'

PROMPT Creating Index 'RECLASS_HEAD_TL_I1'
CREATE INDEX RECLASS_HEAD_TL_I1 on RECLASS_HEAD_TL
  (RECLASS_NO
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

