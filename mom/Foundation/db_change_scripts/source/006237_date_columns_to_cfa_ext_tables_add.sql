-------------------------------------------------------------------------
-- Copyright (c) 2015, Oracle and/or its affiliates.
-- All rights reserved.
-- $HeadURL$
-- $Revision$
-- $Date$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--     TABLES UPDATED:			ADDR_CFA_EXT
--					CLASS_CFA_EXT
--					DEPS_CFA_EXT
--					ITEM_LOC_CFA_EXT
--					ITEM_MASTER_CFA_EXT
--					ITEM_SUPP_COUNTRY_CFA_EXT
--					ITEM_SUPP_COUNTRY_LOC_CFA_EXT
--					ITEM_SUPPLIER_CFA_EXT
--					STORE_ADD_CFA_EXT
--					STORE_CFA_EXT
--					SUBCLASS_CFA_EXT
--					SUPS_CFA_EXT
--					VAT_CODES_CFA_EXT
--					WH_CFA_EXT
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ALTERING TABLE
--------------------------------------
ALTER TABLE ADDR_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN ADDR_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ADDR_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ADDR_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

ALTER TABLE CLASS_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN CLASS_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN CLASS_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN CLASS_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

ALTER TABLE DEPS_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN DEPS_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN DEPS_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN DEPS_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

ALTER TABLE ITEM_LOC_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN ITEM_LOC_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ITEM_LOC_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ITEM_LOC_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

ALTER TABLE ITEM_MASTER_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN ITEM_MASTER_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ITEM_MASTER_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ITEM_MASTER_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/


ALTER TABLE ITEM_SUPP_COUNTRY_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN ITEM_SUPP_COUNTRY_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ITEM_SUPP_COUNTRY_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ITEM_SUPP_COUNTRY_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

ALTER TABLE ITEM_SUPP_COUNTRY_LOC_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN ITEM_SUPP_COUNTRY_LOC_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ITEM_SUPP_COUNTRY_LOC_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ITEM_SUPP_COUNTRY_LOC_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/


ALTER TABLE ITEM_SUPPLIER_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN ITEM_SUPPLIER_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ITEM_SUPPLIER_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ITEM_SUPPLIER_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/


ALTER TABLE STORE_ADD_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN STORE_ADD_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN STORE_ADD_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN STORE_ADD_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/


ALTER TABLE STORE_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE);

COMMENT ON COLUMN STORE_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN STORE_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN STORE_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/


ALTER TABLE SUBCLASS_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN SUBCLASS_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN SUBCLASS_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN SUBCLASS_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/


ALTER TABLE SUPS_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE);

COMMENT ON COLUMN SUPS_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN SUPS_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN SUPS_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/


ALTER TABLE VAT_CODES_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN VAT_CODES_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN VAT_CODES_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN VAT_CODES_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/


ALTER TABLE WH_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN WH_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN WH_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN WH_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

