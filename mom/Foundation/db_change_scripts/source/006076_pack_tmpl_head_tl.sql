--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       PACK_TMPL_HEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE PACK_TMPL_HEAD_TL(
LANG NUMBER(6) NOT NULL,
PACK_TMPL_ID NUMBER(8) NOT NULL,
PACK_TMPL_DESC VARCHAR2(250) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE PACK_TMPL_HEAD_TL is 'This is the translation table for PACK_TMPL_HEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN PACK_TMPL_HEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN PACK_TMPL_HEAD_TL.PACK_TMPL_ID is 'This field contains the unique identification code for a pack template.'
/

COMMENT ON COLUMN PACK_TMPL_HEAD_TL.PACK_TMPL_DESC is 'This field contains a description of the pack template.'
/

COMMENT ON COLUMN PACK_TMPL_HEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN PACK_TMPL_HEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN PACK_TMPL_HEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN PACK_TMPL_HEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE PACK_TMPL_HEAD_TL ADD CONSTRAINT PK_PACK_TMPL_HEAD_TL PRIMARY KEY (
LANG,
PACK_TMPL_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE PACK_TMPL_HEAD_TL
 ADD CONSTRAINT PTHT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE PACK_TMPL_HEAD_TL ADD CONSTRAINT PTHT_PTH_FK FOREIGN KEY (
PACK_TMPL_ID
) REFERENCES PACK_TMPL_HEAD (
PACK_TMPL_ID
)
/

