--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_RTK_ROLE_PRIVS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_RTK_ROLE_PRIVS'
CREATE TABLE SVC_RTK_ROLE_PRIVS
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  TSF_APPR_IND VARCHAR2(1 ) DEFAULT 'N',
  ORD_APPR_AMT NUMBER(20,4),
  ROLE VARCHAR2(30 ),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_RTK_ROLE_PRIVS is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in RTK_ROLE_PRIVS.'
/

COMMENT ON COLUMN SVC_RTK_ROLE_PRIVS.PROCESS_ID is 'Uniquely identifies a process in   SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_RTK_ROLE_PRIVS.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1'
/

COMMENT ON COLUMN SVC_RTK_ROLE_PRIVS.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_RTK_ROLE_PRIVS.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_RTK_ROLE_PRIVS.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_RTK_ROLE_PRIVS.TSF_APPR_IND is 'This field indicates whether or not the role is able to approve transfers.  If this indicator is set to Y, then users in this role will create transfers in approved status, otherwise the transfer will remain in input status and need to be approved at a later time.  Valid values for this field are Y and N.'
/

COMMENT ON COLUMN SVC_RTK_ROLE_PRIVS.ORD_APPR_AMT is 'This field contains the upper limit that the role is able to approve on an order.  This value is expressed in primary currency.'
/

COMMENT ON COLUMN SVC_RTK_ROLE_PRIVS.ROLE is 'This field contains the Oracle role for which the record will pertain to.'
/

COMMENT ON COLUMN SVC_RTK_ROLE_PRIVS.CREATE_ID is 'This column holds the user id created the record.'
/

COMMENT ON COLUMN SVC_RTK_ROLE_PRIVS.CREATE_DATETIME is 'This column holds the timestamp when the record is created.'
/

COMMENT ON COLUMN SVC_RTK_ROLE_PRIVS.LAST_UPD_ID is 'This column holds the user id Last Updated the record.'
/

COMMENT ON COLUMN SVC_RTK_ROLE_PRIVS.LAST_UPD_DATETIME is 'This column holds the timestamp when the record is Last Updated.'
/


PROMPT Creating Primary Key on 'SVC_RTK_ROLE_PRIVS'
ALTER TABLE SVC_RTK_ROLE_PRIVS
 ADD CONSTRAINT SVC_RTK_ROLE_PRIVS_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_RTK_ROLE_PRIVS'
ALTER TABLE SVC_RTK_ROLE_PRIVS
 ADD CONSTRAINT SVC_RTK_ROLE_PRIVS_UK UNIQUE
  (ROLE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

