CREATE OR REPLACE FORCE VIEW V_BRAND_TL (BRAND_NAME, BRAND_DESCRIPTION, LANG ) AS
SELECT  b.brand_name,
        case when tl.lang is not null then tl.brand_description else b.brand_description end brand_description,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  BRAND b,
        BRAND_TL tl
 WHERE  b.brand_name = tl.brand_name (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_BRAND_TL is 'This is the translation view for base table BRAND. This view fetches data in user langauge either from translation table BRAND_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_BRAND_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_BRAND_TL.BRAND_NAME is 'This column holds the Brand name.'
/

COMMENT ON COLUMN V_BRAND_TL.BRAND_DESCRIPTION is 'This column holds the description of the Brand.'
/

