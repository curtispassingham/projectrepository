--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_SVC_ORDER_PARAMETER_CONFIG
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_SVC_ORDER_PARAMETER_CONFIG'
CREATE TABLE SVC_SVC_ORDER_PARAMETER_CONFIG
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
    APPLY_DEALS           VARCHAR2(1) ,
    APPLY_SCALING VARCHAR2(1) ,
    SKIP_OPEN_SHIPMENT VARCHAR2(1) ,
    CANCEL_ALLOC VARCHAR2(1) ,
    RECALC_REPLENISHMENT VARCHAR2(1) ,
    OTB_OVERRIDE VARCHAR2(1) ,
    MAX_ORDER_EXPIRY_DAYS NUMBER(4,0) ,
    MAX_ORDER_NO_QTY NUMBER(4,0) ,
    WAIT_BTWN_THREADS NUMBER(10,0) ,
    MAX_THREADS NUMBER(10,0) ,
    MAX_CHUNK_SIZE NUMBER(10,0) ,
    APPLY_BRACKETS VARCHAR2(1) ,
    OVERRIDE_MANL_COST_SRC VARCHAR2(1) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE
	)	
	INITRANS 6
	TABLESPACE RETAIL_DATA
/


COMMENT ON TABLE SVC_SVC_ORDER_PARAMETER_CONFIG IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in SVC_CORESVC_ITEM_CONFIG.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.APPLY_DEALS IS 'Indicates whether or not deals are to be applied on the purchase order. Valid values are Y or N.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.APPLY_SCALING IS 'Indicates whether or not scaling are to be performed on the quantities in the purchase order. Valid values are Y or N.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.SKIP_OPEN_SHIPMENT IS 'Indicates whether or not open shipments linked to the PO will be cancelled when the order is cancelled. Cancelled shipments cannot be reinstated. Valid values are Y or N.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.CANCEL_ALLOC IS 'Indicates whether or not PO linked allocations will be cancelled when the order is cancelled or set to worksheet status.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.RECALC_REPLENISHMENT IS 'Indicates whether or not replenishment results are to be recalculated. Valid values are Y or N.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.OTB_OVERRIDE IS 'Indicates whether or not PO approval will be allowed even if OTB limits have been exceeded. Valid values are Y or N.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.MAX_ORDER_EXPIRY_DAYS IS 'Contains the maximum number of days that can be set for the order expiry days.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.MAX_ORDER_NO_QTY IS 'The maximum number of order numbers that can be preissued in one request.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.WAIT_BTWN_THREADS IS 'This is the number of milliseconds between submission of two threads.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.MAX_THREADS IS 'This is the maximum number of threads that should be spawned for the coresvc_po package.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.MAX_CHUNK_SIZE IS 'The maximum number of orders that should be processed in one chunk.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.APPLY_BRACKETS IS 'Indicates whether or not brackets are to be applied to the purchase order. Valid values are Y or N.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.OVERRIDE_MANL_COST_SRC IS 'Indicates whether or not manual costs will be overridden when deals are applied. Valid values are Y or N.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_SVC_ORDER_PARAMETER_CONFIG.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/

PROMPT Creating primary key on 'SVC_SVC_ORDER_PARAMETER_CONFIG'    
ALTER TABLE SVC_SVC_ORDER_PARAMETER_CONFIG
ADD CONSTRAINT SVC_SVC_ORDER_PM_CONFIG_PK 
PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX    
/
