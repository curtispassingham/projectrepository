--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_UDA_ITEM_LOV
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_UDA_ITEM_LOV"
CREATE TABLE DC_UDA_ITEM_LOV
(
   ITEM                   VARCHAR2(25),
   UDA_ID                 NUMBER(5),
   UDA_VALUE              NUMBER(5)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_UDA_ITEM_LOV is 'This table is a staging table for data conversion and will hold sone row for each item/attribute combination for UDAs with display type of List of
Values (LV) and contains the information about item attribute relationship, create date, update date, etc.'
/
COMMENT ON COLUMN DC_UDA_ITEM_LOV.ITEM is 'This field contains unique alphanumeric identifier for the item.'
/
COMMENT ON COLUMN DC_UDA_ITEM_LOV.UDA_ID is 'This field contains a number uniquely identifying the User-Defined Attribute.'
/
COMMENT ON COLUMN DC_UDA_ITEM_LOV.UDA_VALUE is 'This field contains value of the Used Defined attribute for the item.'
/