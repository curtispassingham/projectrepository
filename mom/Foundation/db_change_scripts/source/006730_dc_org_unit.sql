--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_ORG_UNIT
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_ORG_UNIT"
CREATE TABLE DC_ORG_UNIT
(
   ORG_UNIT_ID            NUMBER(15),
   DESCRIPTION            VARCHAR2(120),
   SET_OF_BOOKS_ID        NUMBER(15)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_ORG_UNIT is 'This table is a staging table for data conversion and will hold the oracle organizational unit values.'
/
COMMENT ON COLUMN DC_ORG_UNIT.ORG_UNIT_ID is 'holds the oracle organizational unit ID.'
/
COMMENT ON COLUMN DC_ORG_UNIT.DESCRIPTION is 'holds the organizational unit description.'
/
COMMENT ON COLUMN DC_ORG_UNIT.SET_OF_BOOKS_ID is 'Set of Books Id.'
/