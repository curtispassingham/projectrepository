--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_PRICING_EVENT_LOCS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_PRICING_EVENT_LOCS'
CREATE TABLE SVC_PRICING_EVENT_LOCS
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  HIER_VALUE NUMBER(10,0) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_PRICING_EVENT_LOCS is 'This is a table used for explosion of the hierarchy level to the lowest possible level at store or warehouse'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_LOCS.PROCESS_ID is 'This corresponds to the unique value in svc_pricing_event_head table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_LOCS.HIER_VALUE is 'This field contains the store or warehouse value.'
/


PROMPT Creating Primary Key on 'SVC_PRICING_EVENT_LOCS'
ALTER TABLE SVC_PRICING_EVENT_LOCS
 ADD CONSTRAINT SVC_PRICING_EVENT_LOCS_PK PRIMARY KEY
  (PROCESS_ID,
   HIER_VALUE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

