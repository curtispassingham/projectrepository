--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_AREA
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_AREA'
CREATE OR REPLACE FORCE VIEW V_AREA
 (CHAIN
 ,AREA
 ,AREA_NAME)
 AS SELECT ARE.CHAIN CHAIN
          ,ARE.AREA AREA
          ,V.AREA_NAME AREA_NAME
FROM AREA ARE,
     V_AREA_TL V
WHERE ARE.AREA = V.AREA
/


COMMENT ON TABLE V_AREA IS 'This view will be used to display the Area LOVs using a security policy to filter User access. The area name will be translated based on user language'
/

