--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 NIL_INPUT_WORKING
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
-- Creating Table
--------------------------------------

SET SERVEROUTPUT ON

RENAME NIL_INPUT_WORKING TO NIL_INPUT_WORKING_BKP
/

CREATE TABLE NIL_INPUT_WORKING
  (
    PROCESS_ID                     NUMBER(20, 0) NOT NULL ,
    ITEM                           VARCHAR2(25 ) ,
    ITEM_PARENT                    VARCHAR2(25 ) ,
    ITEM_GRANDPARENT               VARCHAR2(25 ) ,
    ITEM_DESC                      VARCHAR2(250 ) ,
    ITEM_SHORT_DESC                VARCHAR2(120 ) ,
    ITEM_NUMBER_TYPE               VARCHAR2(6 ) ,
    FORMAT_ID                      VARCHAR2(1 ) ,
    PREFIX                         NUMBER(2, 0) ,
    DEPT                           NUMBER(4, 0) ,
    CLASS                          NUMBER(4, 0) ,
    SUBCLASS                       NUMBER(4, 0) ,
    ITEM_LEVEL                     NUMBER(1, 0) ,
    TRAN_LEVEL                     NUMBER(1, 0) ,
    ITEM_STATUS                    VARCHAR2(1 ) ,
    WASTE_TYPE                     VARCHAR2(6 ) ,
    WASTE_PCT                      NUMBER(12, 4) ,
    DEFAULT_WASTE_PCT              NUMBER(12, 4) ,
    SELLABLE_IND                   VARCHAR2(1 ) ,
    ORDERABLE_IND                  VARCHAR2(1 ) ,
    PACK_IND                       VARCHAR2(1 ) ,
    PACK_TYPE                      VARCHAR2(1 ) ,
    SIMPLE_PACK_IND                VARCHAR2(1 ) ,
    DIFF_1                         VARCHAR2(10 ) ,
    DIFF_2                         VARCHAR2(10 ) ,
    DIFF_3                         VARCHAR2(10 ) ,
    DIFF_4                         VARCHAR2(10 ) ,
    ORDER_AS_TYPE                  VARCHAR2(1 ) ,
    CONTAINS_INNER_IND             VARCHAR2(1 ) ,
    STORE_ORD_MULT                 VARCHAR2(1 ) ,
    LOC                            NUMBER(10, 0) ,
    LOC_TYPE                       VARCHAR2(1 ) ,
    DAILY_WASTE_PCT                NUMBER(12, 4) ,
    UNIT_COST_LOC                  NUMBER(20, 4) ,
    UNIT_RETAIL_LOC                NUMBER(20, 4) ,
    SELLING_UNIT_RETAIL            NUMBER(20, 4) ,
    SELLING_UOM                    VARCHAR2(4 ) ,
    ITEM_LOC_STATUS                VARCHAR2(1 ) ,
    TAXABLE_IND                    VARCHAR2(1 ) ,
    TI                             NUMBER(12, 4) ,
    HI                             NUMBER(12, 4) ,
    MEAS_OF_EACH                   NUMBER(12, 4) ,
    MEAS_OF_PRICE                  NUMBER(12, 4) ,
    UOM_OF_PRICE                   VARCHAR2(4 ) ,
    PRIMARY_VARIANT                VARCHAR2(25 ) ,
    PRIMARY_SUPP                   NUMBER(10, 0) ,
    PRIMARY_CNTRY                  VARCHAR2(3 ) ,
    PRIMARY_COST_PACK              VARCHAR2(25 ) ,
    LOCAL_ITEM_DESC                VARCHAR2(250 ) ,
    LOCAL_SHORT_DESC               VARCHAR2(120 ) ,
    RECEIVE_AS_TYPE                VARCHAR2(1 ) ,
    STORE_PRICE_IND                VARCHAR2(1 ) ,
    UIN_TYPE                       VARCHAR2(6 ) ,
    UIN_LABEL                      VARCHAR2(6 ) ,
    EXT_UIN_IND                    VARCHAR2(1 ) ,
    CAPTURE_TIME                   VARCHAR2(6 ) ,
    SOURCE_METHOD                  VARCHAR2(1 ) ,
    SOURCE_WH                      NUMBER(10, 0) ,
    INBOUND_HANDLING_DAYS          NUMBER(2, 0) ,
    CURRENCY_CODE                  VARCHAR2(3 ) ,
    LIKE_STORE                     NUMBER(10, 0) ,
    DEFAULT_TO_CHILDREN_IND        VARCHAR2(1 ) DEFAULT 'N' ,
    LANG                           NUMBER(6, 0) ,
    CLASS_VAT_IND                  VARCHAR2(1 ) ,
    HIER_LEVEL                     VARCHAR2(6 ) ,
    HIER_NUM_VALUE                 NUMBER(20, 0) ,
    HIER_CHAR_VALUE                VARCHAR2(6 ) ,
    STORE_TYPE                     VARCHAR2(6 ) ,
    ORG_UNIT_ID                    NUMBER(15, 0) ,
    SOURCE_WH_SUPP                 NUMBER(10, 0) ,
    SOURCE_WH_ORG_UNIT_ID          NUMBER(15, 0) ,
    UNIT_COST_SUP                  NUMBER(20, 4) ,
    CHILD_FROM_INPUT               VARCHAR2(1 ) DEFAULT 'N' ,
    ITEM_FROM_INPUT_PACK           VARCHAR2(1 ) DEFAULT 'N' ,
    VAT_CODE                       VARCHAR2(6 ) ,
    VAT_RATE                       NUMBER(20, 4) ,
    VAT_REGION                     NUMBER(4, 0) ,
    CATCH_WEIGHT_IND               VARCHAR2(1 ) ,
    SALE_TYPE                      VARCHAR2(6 ) ,
    CONTAINER_ITEM                 VARCHAR2(25 ) ,
    IMPORT_COUNTRY_ID              VARCHAR2(3 ) ,
    SUPP_CURRENCY_CODE             VARCHAR2(3 ) ,
    AV_COST                        NUMBER(20, 4) ,
    ITEM_XFORM_IND                 VARCHAR2(1 ) ,
    MULTI_UNITS                    NUMBER(12, 4) ,
    MULTI_UNIT_RETAIL              NUMBER(20, 4) ,
    MULTI_SELLING_UOM              VARCHAR2(4 ) ,
    STANDARD_UOM                   VARCHAR2(4 ) ,
    UOM_CONV_FACTOR                NUMBER(20, 10) ,
    CATCH_WEIGHT_UOM               VARCHAR2(4 ) ,
    AV_WEIGHT                      NUMBER(12, 4) ,
    UOM_ISCD                       VARCHAR2(4 ) ,
    WH_UNIT_COST                   NUMBER(20, 4) ,
    DELIVERY_COUNTRY_ID            VARCHAR2(3 ) ,
    DELIVERY_COUNTRY_LOCALIZED_IND VARCHAR2(1 ) DEFAULT 'N' ,
    DEFAULT_LOC_IND                VARCHAR2(1 ) DEFAULT 'N' ,
    DEFAULT_PO_COST                VARCHAR2(6 ) ,
    COSTING_LOC                    NUMBER(10, 0) ,
    COSTING_LOC_TYPE               VARCHAR2(1 ) ,
    RANGED_IND                     VARCHAR2(1 ) ,
    DEFAULT_WH                     NUMBER(10, 0) ,
    ITEM_LOC_IND                   VARCHAR2(1 ) ,
    DEPOSIT_ITEM_TYPE              VARCHAR2(1 )
  )
  LOGGING TABLESPACE RETAIL_DATA PCTFREE 10 INITRANS 6 STORAGE
  (
    INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS UNLIMITED BUFFER_POOL DEFAULT
  )
  NOCOMPRESS PARTITION BY HASH
  (PROCESS_ID)
  (
    PARTITION NIL_INPUT_WORKING_P001 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P002 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P003 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P004 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P005 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P006 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P007 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P008 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P009 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P010 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P011 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P012 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P013 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P014 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P015 TABLESPACE RETAIL_DATA ,
    PARTITION NIL_INPUT_WORKING_P016 TABLESPACE RETAIL_DATA );
  COMMENT ON TABLE NIL_INPUT_WORKING IS 'This a working table to be used by NEW_ITEM_LOC_SQL package to speed bulk processing of new item location ranging.';
  
  COMMENT ON COLUMN NIL_INPUT_WORKING.PROCESS_ID IS 'the unique identifier of the records processed in a single transaction';

COMMENT ON COLUMN NIL_INPUT_WORKING.ITEM
IS
  'unique alphanumeric value that identifies the item.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ITEM_PARENT
IS
  'alphanumeric value that uniquely identifies the item/group at the level above the item.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ITEM_GRANDPARENT
IS
  'alphanumeric value that uniquely identifies the item/group two levels above the item.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ITEM_DESC
IS
  'long description of the item.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ITEM_SHORT_DESC
IS
  'shortened description of the item.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ITEM_NUMBER_TYPE
IS
  'code specifying what type the item is.  valid values for this field are in the code type upct on the code_head and code_detail tables.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.FORMAT_ID
IS
  'this field will hold the format id that corresponds to the items variable  upc.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.PREFIX
IS
  'this column holds the prefix for variable weight upcs.  the prefix determines the format of the eventual upc and will be used to decode variable weight upcs that are uploaded from the pos.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DEPT
IS
  'NUMBER identifying the department to which the item is attached.  the items department will be the same as that of its parent (and, by transitivity, to that of its grandparent).';
  COMMENT ON COLUMN NIL_INPUT_WORKING.CLASS
IS
  'NUMBER identifying the class to which the item is attached.  the items class will be the same as that of its parent (and, by transitivity, to that of its grandparent).';
  COMMENT ON COLUMN NIL_INPUT_WORKING.SUBCLASS
IS
  'NUMBER identifying the subclass to which the item is attached.  the items subclass will be the same as that of its parent (and, by transitivity, to that of its grandparent).';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ITEM_LEVEL
IS
  'NUMBER indicating which of the three levels the item resides.  the item level determines if the item stands alone or if it is part of a family of related items.  the item level also determines how the item may be used throughout the system.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.TRAN_LEVEL
IS
  'NUMBER indicating which of the three levels transactions occur for the items group.  the transaction level is the level at which the items inventory is tracked in the system.  the transaction level item will be counted, transferred, shipped, etc.  the transaction level may be at the current item or up to 2 levels above or below the current item.  only one level of the hierarchy of an item family may contain transaction level items.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ITEM_STATUS
IS
  'status of the item.  valid values are: w = worksheet: item setup in progress, cannot be used in system s = submitted: item setup complete and awaiting approval, cannot be use in system a = approved: item is approved and can now be used throughout the system';
  COMMENT ON COLUMN NIL_INPUT_WORKING.WASTE_TYPE
IS
  'identifies the wastage type as either sales or spoilage wastage.  sales wastage occurs during processes that make an item saleable (i.e. fat is trimmed off at customer request).  spoilage wastage occurs during the products shelf life (i.e. evaporation causes the product to weigh less after a period of time).  valid values are: sp = spoilage sl = sales  wastage is not applicable to pack items.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.WASTE_PCT
IS
  'average percent of wastage for the item over its shelf life.  used in inflating the retail price for wastage items.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DEFAULT_WASTE_PCT
IS
  'default daily wastage percent for spoilage type wastage items.  this value will default to all item locations and represents the average amount of wastage that occurs on a daily basis.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.SELLABLE_IND
IS
  'indicates if pack item may be sold as a unit.  if it is y then the pack will have its own unique unit retail.  if it is n then the packs unit retail is the sum of each individual items total retail within the pack. this field will only be available if the item is a pack item.  valid values are: y = yes, this pack may be sold as a unit, n = no, this pack may not be sold as a unit';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ORDERABLE_IND
IS
  'indicates if pack item is orderable.  if it is y then the suppliers of the pack must supply all components in the pack.  if it is n then the components may have different suppliers. this field will only be available if the item is a pack item.  valid values are: y = yes, this pack may be ordered, n = no, this pack may not be ordered.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.PACK_IND
IS
  'indicates if the item is a pack.  a pack item is a collection of items that may be either ordered or sold as a unit.  packs require details (i.e. component items and qtys, etc.) that other items do not.  this field is required by the database.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.PACK_TYPE
IS
  'indicates if pack item is a vendor pack or a buyer pack.  a vendor pack is a pack that the vendor or supplier recognizes and sells to the retailer.  if the pack item is a vendor pack, communication with the supplier will use the vendor pack number.  a buyer pack is a pack that a buyer has created for internal ease of use.  if the pack item is a buyer pack, communication with the supplier will explode the pack out to its component items.  this field will only be available if the item is a pack item.  if the pack item is not orderable this field must be null.  valid values are: v = vendor, b = buyer.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.SIMPLE_PACK_IND
IS
  'indicates if pack item is a simple pack or not.  this field will only be available if the item is a pack item.  a simple pack is an item whose components are all the same item (i.e. a six pack of cola, etc).  valid values are: y = yes, this item is a simple pack n = no, this item is not a simple pack';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DIFF_1
IS
  'diff_group or diff_id that differentiates the current item from its item_parent.  for an item that is a parent, this field may be either a group (i.e. mens pant sizes) or a value (6 oz).  for an item that is not a parent, this field may contain a value  (34x34, red, etc.)  valid values are found on the diff_group and diff_id tables.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DIFF_2
IS
  'diff_group or diff_id that differentiates the current item from its item_parent.  for an item that is a parent, this field may be either a group (i.e. mens pant sizes) or a value (6 oz).  for an item that is not a parent, this field may contain a value  (34x34, red, etc.)  valid values are found on the diff_group and diff_id tables.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DIFF_3
IS
  'diff_group or diff_id that differentiates the current item from its item_parent.  for an item that is a parent, this field may be either a group (i.e. mens pant sizes) or a value (6 oz).  for an item that is not a parent, this field may contain a value  (34x34, red, etc.)  valid values are found on the diff_group and diff_id tables.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DIFF_4
IS
  'diff_group or diff_id that differentiates the current item from its item_parent.  for an item that is a parent, this field may be either a group (i.e. mens pant sizes) or a value (6 oz).  for an item that is not a parent, this field may contain a value  (34x34, red, etc.)  valid values are found on the diff_group and diff_id tables.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ORDER_AS_TYPE
IS
  'indicates if pack item is receivable at the component level or at the pack level (for a buyer pack only).  this field is required if pack item is an orderable buyer pack.  this field must be null if the pack is sellable only or a vendor pack. this field will only be available if the item is a pack item.  valid values are: e = eaches (component level) p = pack (buyer pack only)';
  COMMENT ON COLUMN NIL_INPUT_WORKING.CONTAINS_INNER_IND
IS
  'indicates if pack item contains inner packs.  vendor packs will never contain inner packs and this field will be defaulted to n.  this field will only be available if the item is a pack item.  valid values are: y = yes, this pack contains inner packs n = no, this pack does not contain inner packs';
  COMMENT ON COLUMN NIL_INPUT_WORKING.STORE_ORD_MULT
IS
  'merchandise shipped from the warehouses to the stores must be specified in this unit type.  valid values are: c = cases i = inner e = eaches';
  COMMENT ON COLUMN NIL_INPUT_WORKING.LOC
IS
  'numeric identifier of the location in which the item is to be found.  this field may contain a store, warehouse, or external finisher.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.LOC_TYPE
IS
  'type of location in the location field.  valid values are s (store), w (warehouse), and e (external finisher).';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DAILY_WASTE_PCT
IS
  'average percentage lost from inventory on a daily basis due to natural wastage.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.UNIT_COST_LOC
IS
  'if system_options.std_av_ind = a and elc_ind = y then this field holds the estimated landed cost when item is initially set up.  it is then updated with pos estimated landed cost each time this item is received at this location.  otherwise, this field holds the primary supplier cost.  this field is stored in the local currency.  pack items will not have a unit cost stored on item_loc.  stock of a pack item is valued at the component level and therefore a unit cost is not applicable for pack';
  COMMENT ON COLUMN NIL_INPUT_WORKING.UNIT_RETAIL_LOC
IS
  'contains the unit retail price in the standard unit of measure for the item/location combination.  this field is stored in the local currency.  stores retail should match the retail on item_zone_price unless the item is on clearance.  warehouse retails should match the retail on item_zone_price for the base zone.  pack items will not have a unit cost stored on item_loc.  stock of a pack item is valued at the component level and therefore a unit cost is not applicable for pack items.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.SELLING_UNIT_RETAIL
IS
  'contains the unit retail price in the selling unit of measure for the item/location combination.  this field is stored in the local currency.  store retails should match the retail on item_zone_price unless the item is on clearance.  warehouse retails should match the retail on item_zone_price for the base zone.  pack items will not have a unit cost stored on item_loc.  stock of a pack item is valued at the component level and therefore a unit cost is not applicable for pack items.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.SELLING_UOM
IS
  'contains the selling unit of measure for an items single-unit retail.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ITEM_LOC_STATUS
IS
  'current status of item at the store.  valid values are: a = active, item is valid and can be ordered and sold i = inactive, item is valid but cannot be ordered or sold c = discontinued, item is valid and sellable but no longer orderable d = delete, item is invalid and cannot be ordered or sold';
  COMMENT ON COLUMN NIL_INPUT_WORKING.TAXABLE_IND
IS
  'indicates if item is taxable at the store.  valid values are: y = yes, the item is taxable n = no, the item is not taxable';
  COMMENT ON COLUMN NIL_INPUT_WORKING.TI
IS
  'NUMBER of shipping units (cases) that make up one tier of a pallet.  multiply ti x hi to get total NUMBER of cases for a pallet.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.HI
IS
  'NUMBER of tiers that make up a complete pallet (height).  multiply ti x hi to get total NUMBER of cases for a pallet.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.MEAS_OF_EACH
IS
  'size of an each in terms of the uom_of_price.  for example 12 oz.  used in ticketing.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.MEAS_OF_PRICE
IS
  'size to be used on the ticket in terms of the uom_of_price.  for example, if the user wants the ticket to have the label print the price per ounce, this value would be 1.  if the user wanted the price per 100 grams this value would be 100.  used in ticketing.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.UOM_OF_PRICE
IS
  'unit of measure that will be used on the ticket for this item.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.PRIMARY_VARIANT
IS
  'this field is used to address sales of plus (i.e. above transaction level items) when inventory is tracked at a lower level (i.e. upc).  this field will only contain a value for items one level higher than the transaction level.  valid choices will be any transaction level item that is a child of this item.  in order to select a transaction level item as the primary variant, the item/location relationship must exist at the transaction level.  when a transaction level item is specified as a primary variant for an item higher than the transaction level, an extra pos_mods record will be written.  both the transaction level item (i.e. upc) and the higher than transcation level item (i.e. plu) will be sent to the pos to allow the store to sell the plu.  the information sent for the plu will be the same information sent for the transaction level item (i.e. upc).';
  COMMENT ON COLUMN NIL_INPUT_WORKING.PRIMARY_SUPP
IS
  'numeric identifier of the supplier who will be considered the primary supplier for the specified item/loc.  the supplier/origin country combination will determine the value of the unit cost field on item_loc.  if the supplier is changed and elc = n, the unit cost field on item_loc will be updated with the new suppliers cost.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.PRIMARY_CNTRY
IS
  'contains the identifier of the origin country which will be considered the primary country for the specified item/location.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.PRIMARY_COST_PACK
IS
  'this field contains an item NUMBER that is a simple pack containing the item in the item column for this record.  if populated, the cost of the future cost table will be driven from the simple pack and the deals and cost changes for the simple pack.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.LOCAL_ITEM_DESC
IS
  'contains the local description of the item. this field will default to the items description but will be over-ridable. this value will be downloaded to the pos.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.LOCAL_SHORT_DESC
IS
  'contains the local short description of the item.  this field will default to the items short description but will be over-ridable.  this value will be downloaded to the pos.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.RECEIVE_AS_TYPE
IS
  'this column determines whether the stock on hand for a pack component item or the buyer pack itself will be updated when a buyer pack is received at a warehouse. valid values are each or pack.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.STORE_PRICE_IND
IS
  'this field indicates if an item at a particular store location can have the unit retail marked down by the store.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.UIN_TYPE
IS
  'this column will contain the unique identification NUMBER (uin) used to identify the instances of the item at the location.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.UIN_LABEL
IS
  'this column will contain the label for the uin when displayed in sim.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.EXT_UIN_IND
IS
  'this yes/no indicator indicates if uin is being generated in the external system.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.CAPTURE_TIME
IS
  'this column will indicate when the uin should be captured for an item during transaction processing.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.SOURCE_METHOD
IS
  'this value will be used to specify how the ad-hoc po/tsf creation process should source the item/stores request. if the value is warehouse, the process will attempt to fill the request by creating a transfer from the item/locations primary sourcing warehouse. if this warehouse doesnt have enough inventory to fill the request a purchase order will be created for the item/locations primary supplier.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.SOURCE_WH
IS
  'this value will be used by the ad-hoc po/transfer creation process to determine which warehouse to fill the stores request from. a value will be required in this field if the sourcing method is warehouse.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.INBOUND_HANDLING_DAYS
IS
  'this field indicates the NUMBER of inbound handling days for an item at a warehouse type location.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.CURRENCY_CODE
IS
  'Location currency code';
  COMMENT ON COLUMN NIL_INPUT_WORKING.LIKE_STORE
IS
  'contains the store in which to copy items to the new store.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DEFAULT_TO_CHILDREN_IND
IS
  'This indicates to copy the item/loc to the input item children.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.LANG
IS
  'this column identifies the language to be used for the given store.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.CLASS_VAT_IND
IS
  'this field determines if retail is displayed and held with or with out vat.  this field is only editable when vat is turned on in the system and defined at the class level, when that is the case the field can vary between y and n by class.  when vat is turned on in the system and not defined at the class level, this field defaults to y.  when vat is turned off in the system, this field defaults to n.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.HIER_LEVEL
IS
  'The organizational hierarchy level to which to range the item to.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.HIER_NUM_VALUE
IS
  'The NUMBER value of the organizational hierarchy level to which to range the item to.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.HIER_CHAR_VALUE
IS
  'The char value of the organizational hierarchy level to which to range the item to.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.STORE_TYPE
IS
  'this will indicate whether a particular store is a wholesale, franchise or company store.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ORG_UNIT_ID
IS
  'column will contain the organizational unit id value.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.SOURCE_WH_SUPP
IS
  'numeric identifier of the supplier who is the supplier for the source wh.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.SOURCE_WH_ORG_UNIT_ID
IS
  ' The organizational unit id value for the source wh.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.UNIT_COST_SUP
IS
  'The supplier unit cost';
  COMMENT ON COLUMN NIL_INPUT_WORKING.CHILD_FROM_INPUT
IS
  'This indicates if the records is a child of an input item';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ITEM_FROM_INPUT_PACK
IS
  'This indicates if the item is a component of an input pack item';
  COMMENT ON COLUMN NIL_INPUT_WORKING.VAT_CODE
IS
  'this field contains the alphanumeric identification for the vat code. valid values include: s - standard c - composite z - zero e - exempt other values may also be entered.  these are the default vat rates that are set-up upon installation of the rms.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.VAT_RATE
IS
  'vat rate associated with a given vat code.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.VAT_REGION
IS
  'contains the unique identifying NUMBER for the vat region in the system.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.CATCH_WEIGHT_IND
IS
  'indiactes whether the item should be weighed when it arives at a location.  valid values for this field are y and n.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.SALE_TYPE
IS
  'this indicates the method of how catch weight items are sold in store locations. valid values are: v - variable weight each l - loose weight valid values are held on the code_detail table with a code type = stpe';
  COMMENT ON COLUMN NIL_INPUT_WORKING.CONTAINER_ITEM
IS
  'this holds the container item NUMBER for a contents item. this field is only populated and required if the deposit_item_type = e.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.IMPORT_COUNTRY_ID
IS
  'The import country ID';
  COMMENT ON COLUMN NIL_INPUT_WORKING.SUPP_CURRENCY_CODE
IS
  'Supplier currency code.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.AV_COST
IS
  'calculated using pos estimated landed cost during po receiving process if elc_ind = y.  this field is stored in the local currency.  pack items will not have a average cost stored on item_loc.  stock of a pack item is valued at the component level and therefore an average cost is not applicable for pack items.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ITEM_XFORM_IND
IS
  'this indicator will show that an item is associated an item transformation. the item will be either the sellable item  or orderable item in the transformation process.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.MULTI_UNITS
IS
  'this field contains the multi-units for the item/location (zone) combination.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.MULTI_UNIT_RETAIL
IS
  'this field holds the multi-unit retail in the multi-selling unit of measure for the item/location (zone) combination.  this field is stored in teh local currency.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.MULTI_SELLING_UOM
IS
  'this field holds the selling unit of measure for this item/location (zone) combinations multi-unit retail.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.STANDARD_UOM
IS
  'unit of measure in which stock of the item is tracked at a corporate level.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.UOM_CONV_FACTOR
IS
  'conversion factor between an each and the standard_uom when the standard_uom is not in the quantity class (e.g. if standard_uom = lb and 1 lb = 10 eaches, this factor will be 10).  this factor will be used to convert sales and stock data when an item is retailed in eaches but does not have eaches as its standard unit of measure.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.CATCH_WEIGHT_UOM
IS
  'uom for catchweight items.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.AV_WEIGHT
IS
  'a new field to hold the average simple pack weight for a catch weight simple pack.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.UOM_ISCD
IS
  'Item supplier country weight UOM';
  COMMENT ON COLUMN NIL_INPUT_WORKING.WH_UNIT_COST
IS
  'Wh unit cost';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DELIVERY_COUNTRY_ID
IS
  'country to which the item  will be delivered to.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DELIVERY_COUNTRY_LOCALIZED_IND
IS
  'this indicate if the country is localized or not. this will determine if localized attributes will be required. valid values are yes or no. default value is no.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DEFAULT_LOC_IND
IS
  'default location, whose fiscal attributes would be used to to set the initial item retail when no locations have been ranged for the item. the location should belong to the given country. this can either be a store or a warehouse.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DEFAULT_PO_COST
IS
  'this will indicate at which cost the purchasing would be done in a country. this is the cost at which the purchase orders would be raised. default value is bc - base cost. valid values are, base cost (bc) and negotiated item cost (nic).';
  COMMENT ON COLUMN NIL_INPUT_WORKING.COSTING_LOC
IS
  'this will hold the costing location at which Franchise locations will base their costs.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.COSTING_LOC_TYPE
IS
  'this will hold the location type of the costing location.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.RANGED_IND
IS
  'this will indicate if the location is ranged intentionally by the user for replenishment/selling or incidentally ranged by the RMS programs when item is not ranged to a specific location on the transaction.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DEFAULT_WH
IS
  'this will hold the default WH for a store. This can come from the input source wh, store default wh or default wh from system options.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.ITEM_LOC_IND
IS
  'this will indicate if item loc is initiated from the item location form.';
  COMMENT ON COLUMN NIL_INPUT_WORKING.DEPOSIT_ITEM_TYPE
IS
  'this will indicate if the item is a content or container for deposit items.';
  
INSERT INTO NIL_INPUT_WORKING
(SELECT * FROM NIL_INPUT_WORKING_BKP)
/

COMMIT
/

PROMPT Adding Constraints to Table 'NIL_INPUT_WORKING'
ALTER TABLE NIL_INPUT_WORKING_BKP DROP CONSTRAINT CHK_CATCH_WEIGHT_IND
/
ALTER TABLE NIL_INPUT_WORKING_BKP DROP CONSTRAINT CHK_CHILD_FROM_INPUT
/
ALTER TABLE NIL_INPUT_WORKING_BKP DROP CONSTRAINT CHK_CLASS_VAT_IND
/
ALTER TABLE NIL_INPUT_WORKING_BKP DROP CONSTRAINT CHK_DEFAULT_LOC_IND
/
ALTER TABLE NIL_INPUT_WORKING_BKP DROP CONSTRAINT CHK_SELLABLE_IND
/
ALTER TABLE NIL_INPUT_WORKING_BKP DROP CONSTRAINT CHK_ITEM_XFORM_IND
/
ALTER TABLE NIL_INPUT_WORKING_BKP DROP CONSTRAINT CHK_ITEM_FROM_INPUT_PACK
/
ALTER TABLE NIL_INPUT_WORKING_BKP DROP CONSTRAINT CHK_DELIVERY_COUNTRY_LOCAL_IND
/
ALTER TABLE NIL_INPUT_WORKING_BKP DROP CONSTRAINT CHK_DEFAULT_TO_CHILDREN_IND
/

ALTER TABLE NIL_INPUT_WORKING ADD CONSTRAINT CHK_CATCH_WEIGHT_IND CHECK
  (
    CATCH_WEIGHT_IND IN ('N','Y')
  )
ENABLE 
/

ALTER TABLE NIL_INPUT_WORKING ADD CONSTRAINT CHK_CHILD_FROM_INPUT CHECK 
(CHILD_FROM_INPUT IN ('N','Y')) ENABLE 
/

ALTER TABLE NIL_INPUT_WORKING ADD CONSTRAINT CHK_CLASS_VAT_IND CHECK 
(CLASS_VAT_IND IN ('N','Y')) ENABLE
/

ALTER TABLE NIL_INPUT_WORKING ADD CONSTRAINT CHK_DEFAULT_LOC_IND CHECK 
(DEFAULT_LOC_IND IN ('N','Y')) ENABLE 
/

ALTER TABLE NIL_INPUT_WORKING ADD CONSTRAINT CHK_DEFAULT_TO_CHILDREN_IND CHECK 
(DEFAULT_TO_CHILDREN_IND IN ('N','Y')) ENABLE 
/

ALTER TABLE NIL_INPUT_WORKING ADD CONSTRAINT CHK_DELIVERY_COUNTRY_LOCAL_IND CHECK 
(DELIVERY_COUNTRY_LOCALIZED_IND IN ('N','Y')) ENABLE 
/

ALTER TABLE NIL_INPUT_WORKING ADD CONSTRAINT CHK_ITEM_FROM_INPUT_PACK CHECK 
(ITEM_FROM_INPUT_PACK IN ('N','Y')) ENABLE
/

ALTER TABLE NIL_INPUT_WORKING ADD CONSTRAINT CHK_ITEM_XFORM_IND CHECK 
(ITEM_XFORM_IND IN ('N','Y')) ENABLE
/

ALTER TABLE NIL_INPUT_WORKING ADD CONSTRAINT CHK_SELLABLE_IND CHECK 
(SELLABLE_IND IN ('N','Y')) ENABLE
/

PROMPT Adding Indexes to Table 'NIL_INPUT_WORKING'
DECLARE
	l_exists 		NUMBER(1);
	l_partitioned 	NUMBER(1);
	l_local_string	VARCHAR2(10) := '';
BEGIN
	for i in 
		(SELECT INDEX_NAME FROM USER_INDEXES WHERE TABLE_NAME = 'NIL_INPUT_WORKING_BKP') loop
			execute immediate 'DROP INDEX '|| i.INDEX_NAME ;
			dbms_output.put_line('Index Dropped ' || i.INDEX_NAME);
	end loop;
	
	SELECT COUNT(*) INTO l_partitioned 
		FROM USER_PART_TABLES 
		WHERE TABLE_NAME = 'NIL_INPUT_WORKING' ;
		
	IF l_partitioned > 0 THEN
		l_local_string := 'LOCAL';
	END IF;

	SELECT COUNT(*) INTO l_exists 
		FROM USER_INDEXES 
		WHERE TABLE_NAME = 'NIL_INPUT_WORKING' 
		AND INDEX_NAME = 'NIL_INPUT_WORKING_I2';
	IF l_exists = 0 THEN
		execute immediate 'CREATE INDEX NIL_INPUT_WORKING_I2 on NIL_INPUT_WORKING(ITEM,PROCESS_ID, LOC) ' || l_local_string ||' INITRANS 12 TABLESPACE RETAIL_INDEX ' ;
		dbms_output.put_line('Index Created NIL_INPUT_WORKING_I2');
	END IF;
		
	SELECT COUNT(*) INTO l_exists 
		FROM USER_INDEXES 
		WHERE TABLE_NAME = 'NIL_INPUT_WORKING' 
		AND INDEX_NAME = 'NIL_INPUT_WORKING_I1';
	IF l_exists = 0 THEN
		execute immediate 'CREATE INDEX NIL_INPUT_WORKING_I1 on NIL_INPUT_WORKING(PROCESS_ID, LOC, LOC_TYPE, DEPT, CLASS, SUBCLASS) ' || l_local_string ||' INITRANS 12 TABLESPACE RETAIL_INDEX';
		dbms_output.put_line('Index Created NIL_INPUT_WORKING_I1');
	END IF;

END;
/

DROP TABLE NIL_INPUT_WORKING_BKP
/

