--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       POS_COUPON_HEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE POS_COUPON_HEAD_TL(
LANG NUMBER(6) NOT NULL,
COUPON_ID NUMBER(6) NOT NULL,
COUPON_DESC VARCHAR2(250) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE POS_COUPON_HEAD_TL is 'This is the translation table for POS_COUPON_HEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN POS_COUPON_HEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN POS_COUPON_HEAD_TL.COUPON_ID is 'Contains the number that uniquely identifies the coupon.'
/

COMMENT ON COLUMN POS_COUPON_HEAD_TL.COUPON_DESC is 'Contains the description of the coupon associated with the coupon number.'
/

COMMENT ON COLUMN POS_COUPON_HEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN POS_COUPON_HEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN POS_COUPON_HEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN POS_COUPON_HEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE POS_COUPON_HEAD_TL ADD CONSTRAINT PK_POS_COUPON_HEAD_TL UNIQUE (
LANG,
COUPON_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE POS_COUPON_HEAD_TL
 ADD CONSTRAINT PCHT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE POS_COUPON_HEAD_TL ADD CONSTRAINT PCHT_PCH_FK FOREIGN KEY (
COUPON_ID
) REFERENCES POS_COUPON_HEAD (
COUPON_ID
)
/

