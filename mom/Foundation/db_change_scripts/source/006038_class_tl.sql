--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE ADDED:				CLASS_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TYPE
--------------------------------------
PROMPT Creating Table 'CLASS_TL'
CREATE TABLE CLASS_TL(
LANG NUMBER(6) NOT NULL,
DEPT NUMBER(4) NOT NULL,
CLASS NUMBER(4) NOT NULL,
CLASS_NAME VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE CLASS_TL is 'This is the translation table for CLASS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN CLASS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN CLASS_TL.DEPT is 'Contains the number of the department of which the class is a member.'
/

COMMENT ON COLUMN CLASS_TL.CLASS is 'Contains the number which uniquely identifies the class within the system.'
/

COMMENT ON COLUMN CLASS_TL.CLASS_NAME is 'Contains the name of the class which, along with the class number, identifies the class.'
/

COMMENT ON COLUMN CLASS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN CLASS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN CLASS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN CLASS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE CLASS_TL ADD CONSTRAINT PK_CLASS_TL PRIMARY KEY (
LANG,
DEPT,
CLASS
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE CLASS_TL
 ADD CONSTRAINT CLST_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE CLASS_TL ADD CONSTRAINT CLST_CLS_FK FOREIGN KEY (
DEPT,
CLASS
) REFERENCES CLASS (
DEPT,
CLASS
)
/

