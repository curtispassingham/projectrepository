--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_UOM_CLASS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_UOM_CLASS'
CREATE TABLE SVC_UOM_CLASS
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
    UOM_DESC_TRANS VARCHAR2(120) ,
    UOM_TRANS VARCHAR2(4) ,
    UOM_CLASS VARCHAR2(6) ,
    UOM VARCHAR2(4) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE
	)
	INITRANS 6	
	TABLESPACE RETAIL_DATA
/


COMMENT ON TABLE SVC_UOM_CLASS IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in UOM_CLASS.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.UOM IS 'Contains a string that uniquely identifies the unit of measure. Example: LBS for pounds.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.UOM_CLASS IS 'Contains the unit of measure type used as a grouping mechanism for the many UOM options. When converting from one UOM to another, the class is used to determine how the system proceeds with the conversion, whether it is an in-class or across-class conversion.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.UOM_TRANS IS 'Contains UOM Translation.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.UOM_DESC_TRANS IS 'Contains UOM Description.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_UOM_CLASS.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/

PROMPT Creating Primary Key on 'SVC_UOM_CLASS'           
ALTER TABLE SVC_UOM_CLASS
ADD CONSTRAINT SVC_UOM_CLASS_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

PROMPT Creating Unique Key on 'SVC_UOM_CLASS'
ALTER TABLE SVC_UOM_CLASS
ADD CONSTRAINT SVC_UOM_CLASS_UK UNIQUE ( UOM )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX   
/
