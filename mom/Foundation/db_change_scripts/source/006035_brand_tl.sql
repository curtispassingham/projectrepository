--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE ADDED:				BRAND_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TYPE
--------------------------------------
PROMPT Creating Table 'BRAND_TL'
CREATE TABLE BRAND_TL(
LANG NUMBER(6) NOT NULL,
BRAND_NAME VARCHAR2(30) NOT NULL,
BRAND_DESCRIPTION VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE BRAND_TL is 'This is the translation table for BRAND table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN BRAND_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN BRAND_TL.BRAND_NAME is 'This column holds the Brand name.'
/

COMMENT ON COLUMN BRAND_TL.BRAND_DESCRIPTION is 'This column holds the description of the Brand.'
/

COMMENT ON COLUMN BRAND_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN BRAND_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN BRAND_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN BRAND_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE BRAND_TL ADD CONSTRAINT PK_BRAND_TL PRIMARY KEY (
LANG,
BRAND_NAME
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE BRAND_TL
 ADD CONSTRAINT BRDT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE BRAND_TL ADD CONSTRAINT BRDT_BRD_FK FOREIGN KEY (
BRAND_NAME
) REFERENCES BRAND (
BRAND_NAME
)
/

