CREATE OR REPLACE FORCE VIEW V_CUSTOMER_SEGMENT_TYPES_TL (CUSTOMER_SEGMENT_TYPE, CUSTOMER_SEGMENT_TYPE_DESC, LANG ) AS
SELECT  b.customer_segment_type,
        case when tl.lang is not null then tl.customer_segment_type_desc else b.customer_segment_type_desc end customer_segment_type_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  CUSTOMER_SEGMENT_TYPES b,
        CUSTOMER_SEGMENT_TYPES_TL tl
 WHERE  b.customer_segment_type = tl.customer_segment_type (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_CUSTOMER_SEGMENT_TYPES_TL is 'This is the translation view for base table CUSTOMER_SEGMENT_TYPES. This view fetches data in user langauge either from translation table CUSTOMER_SEGMENT_TYPES_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_CUSTOMER_SEGMENT_TYPES_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_CUSTOMER_SEGMENT_TYPES_TL.CUSTOMER_SEGMENT_TYPE is 'The customer segment type of a given customer segment, referenced and attached to promotions in RPM. They will be available in RPM promotion UI screen for customer segment promotions. For example: Electrician.'
/

COMMENT ON COLUMN V_CUSTOMER_SEGMENT_TYPES_TL.CUSTOMER_SEGMENT_TYPE_DESC is 'The customer segment type description.'
/

