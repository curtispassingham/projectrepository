--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ITEM_MASTER'
ALTER TABLE ITEM_MASTER ADD CURR_SELLING_UNIT_RETAIL NUMBER (20,4) NULL
/

COMMENT ON COLUMN ITEM_MASTER.CURR_SELLING_UNIT_RETAIL is 'This field contains the current selling unit retail of the item.'
/

ALTER TABLE ITEM_MASTER ADD CURR_SELLING_UOM VARCHAR2 (4 ) NULL
/

COMMENT ON COLUMN ITEM_MASTER.CURR_SELLING_UOM is 'This field contains the current selling UOM of the item.'
/

