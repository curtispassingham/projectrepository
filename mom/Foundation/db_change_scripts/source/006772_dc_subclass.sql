--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_SUBCLASS
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------


prompt Creating table DC_SUBCLASS
CREATE TABLE DC_SUBCLASS
(
   DEPT            NUMBER(4),
   CLASS           NUMBER(4),
   SUBCLASS        NUMBER(4),
   SUB_NAME        VARCHAR2(120)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_SUBCLASS is 'This table is a staging table for data conversion and will hold data for subclass table.'
/
COMMENT ON COLUMN DC_SUBCLASS.DEPT is 'Contains the number which uniquely identifies the department.'
/
COMMENT ON COLUMN DC_SUBCLASS.CLASS is 'Contains the number which uniquely identifies the class within the system.'
/
COMMENT ON COLUMN DC_SUBCLASS.SUBCLASS is 'Contains the name of the class which, along with the class number, identifies the subclass.'
/
COMMENT ON COLUMN DC_SUBCLASS.SUB_NAME is 'Contains the name of the subclass which, along with the subclass number, uniquely identifies the subclass.'
/