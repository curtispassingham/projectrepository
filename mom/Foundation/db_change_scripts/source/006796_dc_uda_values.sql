--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_UDA_VALUES
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_UDA_VALUES"
CREATE TABLE DC_UDA_VALUES
(
   UDA_ID                    NUMBER(5),
   UDA_VALUE_DESC            VARCHAR2(250)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_UDA_VALUES is 'This table is a staging table for data conversion and will hold data for UDA_VALUES.'
/
COMMENT ON COLUMN DC_UDA_VALUES.UDA_ID is 'This field contains a unique number identifying the User Defined Attribute.'
/
COMMENT ON COLUMN DC_UDA_VALUES.UDA_VALUE_DESC is 'This field contains a description of the UDA value.'
/