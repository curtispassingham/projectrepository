--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_UDA_ITEM_FF_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT Creating View 'V_UDA_ITEM_FF_TL'
CREATE OR REPLACE FORCE VIEW V_UDA_ITEM_FF_TL (ITEM, UDA_ID, UDA_TEXT, UDA_TEXT_DESC, LANG ) AS
SELECT  b.item,
        b.uda_id,
        b.uda_text,
        case when tl.lang is not null then tl.uda_text_desc else b.uda_text_desc end uda_text_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  UDA_ITEM_FF b,
        UDA_ITEM_FF_TL tl
 WHERE  b.item = tl.item (+)
   AND  b.uda_id = tl.uda_id (+)
   AND  b.uda_text = tl.uda_text (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_UDA_ITEM_FF_TL is 'This is the translation view for base table UDA_ITEM_FF. This view fetches data in user langauge either from translation table UDA_ITEM_FF_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_UDA_ITEM_FF_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_UDA_ITEM_FF_TL.ITEM is 'This field contains unique alphanumeric identifier for the item.'
/

COMMENT ON COLUMN V_UDA_ITEM_FF_TL.UDA_ID is 'This field contains a number uniquely identifying the User-Defined Attribute.'
/

COMMENT ON COLUMN V_UDA_ITEM_FF_TL.UDA_TEXT is 'This field contains the text value of the Used Defined attribute for the item.'
/

COMMENT ON COLUMN V_UDA_ITEM_FF_TL.UDA_TEXT_DESC is 'This field contains the uda description in user language.'
/

