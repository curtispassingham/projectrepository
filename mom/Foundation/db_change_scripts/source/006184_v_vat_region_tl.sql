CREATE OR REPLACE FORCE VIEW V_VAT_REGION_TL (VAT_REGION, VAT_REGION_NAME, LANG ) AS
SELECT  b.vat_region,
        case when tl.lang is not null then tl.vat_region_name else b.vat_region_name end vat_region_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  VAT_REGION b,
        VAT_REGION_TL tl
 WHERE  b.vat_region = tl.vat_region (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_VAT_REGION_TL is 'This is the translation view for base table VAT_REGION. This view fetches data in user langauge either from translation table VAT_REGION_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_VAT_REGION_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_VAT_REGION_TL.VAT_REGION is 'Contains the unique identifying number for the VAT region in the system.'
/

COMMENT ON COLUMN V_VAT_REGION_TL.VAT_REGION_NAME is 'Contains the name associated with the VAT region.'
/

