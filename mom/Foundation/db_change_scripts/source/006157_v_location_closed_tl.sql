CREATE OR REPLACE FORCE VIEW V_LOCATION_CLOSED_TL (LOCATION, CLOSE_DATE, REASON, LANG ) AS
SELECT  b.location,
        b.close_date,
        case when tl.lang is not null then tl.reason else b.reason end reason,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  LOCATION_CLOSED b,
        LOCATION_CLOSED_TL tl
 WHERE  b.location = tl.location (+)
   AND  b.close_date = tl.close_date (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_LOCATION_CLOSED_TL is 'This is the translation view for base table LOCATION_CLOSED. This view fetches data in user langauge either from translation table LOCATION_CLOSED_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_LOCATION_CLOSED_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_LOCATION_CLOSED_TL.LOCATION is 'Contain the location number'
/

COMMENT ON COLUMN V_LOCATION_CLOSED_TL.CLOSE_DATE is 'This field contains the date of the closing.'
/

COMMENT ON COLUMN V_LOCATION_CLOSED_TL.REASON is 'Contains the reason why the store is closed.'
/

