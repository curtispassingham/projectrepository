--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Modify View: 		 V_SVC_ITEM_EXIST
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       Modify V_SVC_ITEM_EXIST               
--------------------------------------
PROMPT Creating View 'V_SVC_ITEM_EXIST'
CREATE OR REPLACE FORCE VIEW "V_SVC_ITEM_EXIST"
("ITEM"
 ,"PROCESS_ID"
 ,"CREATE_DATE"
 ,"LAST_UPD_ID")
 AS ( SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_uda_item_lov                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_item_master                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_update_id 
        FROM svc_item_master_tl                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id 
        FROM svc_item_supplier                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_item_country                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_item_country_l10n_ext                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_item_master_cfa_ext                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_item_supplier_cfa_ext                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_item_supp_country                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_item_supp_country_cfa_ext                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_item_supp_country_dim                  
      UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id 
        FROM svc_item_supp_manu_country                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_item_supp_uom                  
       UNION                  
      SELECT head_item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_item_xform_detail                  
       UNION                  
      SELECT head_item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id 
        FROM svc_item_xform_head                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_packitem                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_rpm_item_zone_price                   
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_uda_item_ff                     
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_uda_item_date                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_upd_id  
        FROM svc_vat_item                  
       UNION                  
      SELECT item,
             process_id,
             trunc(create_datetime) create_date,
             last_update_id  
        FROM svc_item_image)
/

COMMENT ON TABLE V_SVC_ITEM_EXIST IS 'This view is used for checking whether any item exists on the item induction staging tables for a given process id, create date and last update id.'
/

COMMENT ON COLUMN V_SVC_ITEM_EXIST.ITEM IS 'Item Id.'
/

COMMENT ON COLUMN V_SVC_ITEM_EXIST.PROCESS_ID IS 'Process Id.'
/

COMMENT ON COLUMN V_SVC_ITEM_EXIST.CREATE_DATE IS 'Holds the date the record was created.'
/

COMMENT ON COLUMN V_SVC_ITEM_EXIST.LAST_UPD_ID IS 'Last Update Id.'
/
