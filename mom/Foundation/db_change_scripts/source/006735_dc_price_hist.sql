--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_PRICE_HIST
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_PRICE_HIST"
CREATE TABLE DC_PRICE_HIST
(
   ITEM                          VARCHAR2(25),
   UNIT_RETAIL                   NUMBER(20,4),
   SELLING_UOM                   VARCHAR2(4)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_PRICE_HIST is 'This table is a staging table for data conversion and will hold data of PRICE_HIST.'
/
COMMENT ON COLUMN DC_PRICE_HIST.ITEM is 'Alphanumeric value that identifies either the item or item parent.'
/
COMMENT ON COLUMN DC_PRICE_HIST.UNIT_RETAIL is 'Contains the current single unit retail in the standard unit of measure.'
/
COMMENT ON COLUMN DC_PRICE_HIST.SELLING_UOM is 'Contains the selling unit of measure for an items single-unit retail.'
/