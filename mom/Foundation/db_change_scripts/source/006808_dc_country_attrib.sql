--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_COUNTRY_ATTRIB
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_COUNTRY_ATTRIB"
CREATE TABLE DC_COUNTRY_ATTRIB
(
   COUNTRY_ID           VARCHAR2(3)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_COUNTRY_ATTRIB is 'This table is a staging table for data conversion and will hold data for COUNTRY_ATTRIB.'
/
COMMENT ON COLUMN DC_COUNTRY_ATTRIB.COUNTRY_ID is 'Contains a number which uniquely identifies the country.'
/