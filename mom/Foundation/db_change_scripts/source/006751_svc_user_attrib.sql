-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_USER_ATTRIB
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_USER_ATTRIB'
CREATE TABLE SVC_USER_ATTRIB
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
    USER_ID VARCHAR2(30) ,
    USER_NAME VARCHAR2(120) ,
    LANG NUMBER(6,0) ,
    STORE_DEFAULT NUMBER(10,0) ,
    USER_PHONE VARCHAR2(20) ,
    USER_FAX VARCHAR2(20) ,
    USER_PAGER VARCHAR2(20) ,
    USER_EMAIL VARCHAR2(250) ,
    DEFAULT_PRINTER VARCHAR2(20) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE 
	)
	INITRANS 6	
	TABLESPACE RETAIL_DATA
/



COMMENT ON TABLE SVC_USER_ATTRIB IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in USER_ATTRIB.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.USER_ID IS 'Contains the unique identifier for the user.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.USER_NAME IS 'Contains the name of the user.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.LANG IS 'Contains the language that the Oracle user prefers to use in Oracle Retail.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.STORE_DEFAULT IS 'Contains the default store for the user.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.USER_PHONE IS 'Contains the telephone number of the user.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.USER_FAX IS 'Contains the fax number of the user.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.USER_PAGER IS 'Contains the pager number of the user.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.USER_EMAIL IS 'Contains the email address for the user.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.DEFAULT_PRINTER IS 'stores users default printer. valid values found on printer_head.printer'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_USER_ATTRIB.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/

PROMPT Creating Primary Key on 'SVC_USER_ATTRIB'                          
ALTER TABLE SVC_USER_ATTRIB
ADD CONSTRAINT SVC_USER_ATTRIB_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX    
/

PROMPT Creating Unique Key on 'SVC_USER_ATTRIB'
ALTER TABLE SVC_USER_ATTRIB
ADD CONSTRAINT SVC_USER_ATTRIB_UK UNIQUE ( USER_ID )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX  
/
	
	
	
