--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       ITEM_MASTER_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE ITEM_MASTER_TL(
LANG NUMBER(6) NOT NULL,
ITEM VARCHAR2(25) NOT NULL,
ITEM_DESC VARCHAR2(250) NOT NULL,
ITEM_DESC_SECONDARY VARCHAR2(250) ,
SHORT_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE ITEM_MASTER_TL is 'This is the translation table for ITEM_MASTER table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN ITEM_MASTER_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN ITEM_MASTER_TL.ITEM is 'Unique alphanumeric value that identifies the item.'
/

COMMENT ON COLUMN ITEM_MASTER_TL.ITEM_DESC is 'Long description of the item.  This description is used through out the system to help online users identify the item.  For items that have parents, this description will default to the parents description plus any differentiators.  For items without parents, this description will default to null.'
/

COMMENT ON COLUMN ITEM_MASTER_TL.ITEM_DESC_SECONDARY is 'Secondary descriptions of the item.  This field can only be populated when system_options.secondary_desc_ind = Y.'
/

COMMENT ON COLUMN ITEM_MASTER_TL.SHORT_DESC is 'Shortened description of the item.  This description is the default for downloading to the POS.  For items that have parents, this description will default to the parents short description.  For items without parents, this description will default to null.'
/

COMMENT ON COLUMN ITEM_MASTER_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN ITEM_MASTER_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN ITEM_MASTER_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN ITEM_MASTER_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE ITEM_MASTER_TL ADD CONSTRAINT PK_ITEM_MASTER_TL PRIMARY KEY (
LANG,
ITEM
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE ITEM_MASTER_TL
 ADD CONSTRAINT IMAT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE ITEM_MASTER_TL ADD CONSTRAINT IMAT_IMA_FK FOREIGN KEY (
ITEM
) REFERENCES ITEM_MASTER (
ITEM
)
/

