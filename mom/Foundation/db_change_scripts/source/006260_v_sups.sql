--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_SUPS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_SUPS'

CREATE OR REPLACE FORCE VIEW V_SUPS (SUPPLIER
   ,SUP_NAME
   ,SUP_NAME_SECONDARY
   ,SUPPLIER_PARENT
   ,CONTACT_NAME
   ,CONTACT_PHONE
   ,CONTACT_FAX
   ,CONTACT_PAGER
   ,SUP_STATUS
   ,QC_IND
   ,QC_PCT
   ,QC_FREQ
   ,VC_IND
   ,VC_PCT
   ,VC_FREQ
   ,CURRENCY_CODE
   ,LANG
   ,TERMS
   ,FREIGHT_TERMS
   ,RET_ALLOW_IND
   ,RET_AUTH_REQ
   ,RET_MIN_DOL_AMT
   ,RET_COURIER
   ,HANDLING_PCT
   ,EDI_PO_IND
   ,EDI_PO_CHG
   ,EDI_PO_CONFIRM
   ,EDI_ASN
   ,EDI_SALES_RPT_FREQ
   ,EDI_SUPP_AVAILABLE_IND
   ,EDI_CONTRACT_IND
   ,EDI_INVC_IND
   ,EDI_CHANNEL_ID
   ,COST_CHG_PCT_VAR
   ,COST_CHG_AMT_VAR
   ,REPLEN_APPROVAL_IND
   ,SHIP_METHOD
   ,PAYMENT_METHOD
   ,CONTACT_TELEX
   ,CONTACT_EMAIL
   ,SETTLEMENT_CODE
   ,PRE_MARK_IND
   ,AUTO_APPR_INVC_IND
   ,DBT_MEMO_CODE
   ,FREIGHT_CHARGE_IND
   ,AUTO_APPR_DBT_MEMO_IND
   ,PREPAY_INVC_IND
   ,BACKORDER_IND
   ,VAT_REGION
   ,INV_MGMT_LVL
   ,SERVICE_PERF_REQ_IND
   ,INVC_PAY_LOC
   ,INVC_RECEIVE_LOC
   ,ADDINVC_GROSS_NET
   ,DELIVERY_POLICY
   ,COMMENT_DESC
   ,DEFAULT_ITEM_LEAD_TIME
   ,DUNS_NUMBER
   ,DUNS_LOC
   ,BRACKET_COSTING_IND
   ,VMI_ORDER_STATUS
   ,DSD_IND
   ,SCALE_AIP_ORDERS
   ,FINAL_DEST_IND
   ,SUP_QTY_LEVEL
   ,EXTERNAL_REF_ID
   ,CREATE_ID
   ,CREATE_DATETIME
   ,STATUS_UPD_BY_RMS)
   AS SELECT SUP.SUPPLIER
           , V.SUP_NAME
           , V.SUP_NAME_SECONDARY
           , SUP.SUPPLIER_PARENT
           , SUP.CONTACT_NAME
           , SUP.CONTACT_PHONE
           , SUP.CONTACT_FAX
           , SUP.CONTACT_PAGER
           , SUP.SUP_STATUS
           , SUP.QC_IND
           , SUP.QC_PCT
           , SUP.QC_FREQ
           , SUP.VC_IND
           , SUP.VC_PCT
           , SUP.VC_FREQ
           , SUP.CURRENCY_CODE
           , SUP.LANG
           , SUP.TERMS
           , SUP.FREIGHT_TERMS
           , SUP.RET_ALLOW_IND
           , SUP.RET_AUTH_REQ
           , SUP.RET_MIN_DOL_AMT
           , SUP.RET_COURIER
           , SUP.HANDLING_PCT
           , SUP.EDI_PO_IND
           , SUP.EDI_PO_CHG
           , SUP.EDI_PO_CONFIRM
           , SUP.EDI_ASN
           , SUP.EDI_SALES_RPT_FREQ
           , SUP.EDI_SUPP_AVAILABLE_IND
           , SUP.EDI_CONTRACT_IND
           , SUP.EDI_INVC_IND
           , SUP.EDI_CHANNEL_ID
           , SUP.COST_CHG_PCT_VAR
           , SUP.COST_CHG_AMT_VAR
           , SUP.REPLEN_APPROVAL_IND
           , SUP.SHIP_METHOD
           , SUP.PAYMENT_METHOD
           , SUP.CONTACT_TELEX
           , SUP.CONTACT_EMAIL
           , SUP.SETTLEMENT_CODE
           , SUP.PRE_MARK_IND
           , SUP.AUTO_APPR_INVC_IND
           , SUP.DBT_MEMO_CODE
           , SUP.FREIGHT_CHARGE_IND
           , SUP.AUTO_APPR_DBT_MEMO_IND
           , SUP.PREPAY_INVC_IND
           , SUP.BACKORDER_IND
           , SUP.VAT_REGION
           , SUP.INV_MGMT_LVL
           , SUP.SERVICE_PERF_REQ_IND
           , SUP.INVC_PAY_LOC
           , SUP.INVC_RECEIVE_LOC
           , SUP.ADDINVC_GROSS_NET
           , SUP.DELIVERY_POLICY
           , SUP.COMMENT_DESC
           , SUP.DEFAULT_ITEM_LEAD_TIME
           , SUP.DUNS_NUMBER
           , SUP.DUNS_LOC
           , SUP.BRACKET_COSTING_IND
           , SUP.VMI_ORDER_STATUS
           , SUP.DSD_IND
           , SUP.SCALE_AIP_ORDERS
           , SUP.FINAL_DEST_IND
           , SUP.SUP_QTY_LEVEL
           , SUP.EXTERNAL_REF_ID
           , SUP.CREATE_ID
           , SUP.CREATE_DATETIME
           , SUP.STATUS_UPD_BY_RMS STATUS_UPD_BY_RMS
FROM SUPS SUP,
     V_SUPS_TL V
WHERE SUP.SUPPLIER = V.SUPPLIER
/

COMMENT ON TABLE V_SUPS IS 'This view will be used to display the supplier information using a security policy to filter user access. The supplier name and secondary name are translated based on user language.'
/

COMMENT ON COLUMN V_SUPS.STATUS_UPD_BY_RMS IS 'Column indicates that Supplier is inactivated from RMS. Suppliers inactivated in RMS will have this column updated as Y. Otherwise this column will be null.'
/


