CREATE OR REPLACE FORCE VIEW V_POS_COUPON_HEAD_TL (COUPON_ID, COUPON_DESC, LANG ) AS
SELECT  b.coupon_id,
        case when tl.lang is not null then tl.coupon_desc else b.coupon_desc end coupon_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  POS_COUPON_HEAD b,
        POS_COUPON_HEAD_TL tl
 WHERE  b.coupon_id = tl.coupon_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_POS_COUPON_HEAD_TL is 'This is the translation view for base table POS_COUPON_HEAD. This view fetches data in user langauge either from translation table POS_COUPON_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_POS_COUPON_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_POS_COUPON_HEAD_TL.COUPON_ID is 'Contains the number that uniquely identifies the coupon.'
/

COMMENT ON COLUMN V_POS_COUPON_HEAD_TL.COUPON_DESC is 'Contains the description of the coupon associated with the coupon number.'
/

