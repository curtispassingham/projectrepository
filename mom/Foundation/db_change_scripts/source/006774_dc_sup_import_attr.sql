--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_SUP_IMPORT_ATTR
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_SUP_IMPORT_ATTR"
CREATE TABLE DC_SUP_IMPORT_ATTR
(
   SUPPLIER             NUMBER(10),
   AGENT                VARCHAR2(10),
   ADVISING_BANK        VARCHAR2(10),
   ISSUING_BANK         VARCHAR2(10),
   LADING_PORT          VARCHAR2(5),
   DISCHARGE_PORT       VARCHAR2(5),
   MFG_ID               VARCHAR2(18),
   RELATED_IND          VARCHAR2(1),
   BENEFICIARY_IND      VARCHAR2(1),
   WITH_RECOURSE_IND    VARCHAR2(1),
   REVOCABLE_IND        VARCHAR2(1),
   VARIANCE_PCT         NUMBER(12,4),
   LC_NEG_DAYS          NUMBER(3),
   PLACE_OF_EXPIRY      VARCHAR2(6),
   DRAFTS_AT            VARCHAR2(6),
   PRESENTATION_TERMS   VARCHAR2(6),
   FACTORY              VARCHAR2(10),
   PARTNER_TYPE_1       VARCHAR2(6),
   PARTNER_1            VARCHAR2(10),
   PARTNER_TYPE_2       VARCHAR2(6),
   PARTNER_2            VARCHAR2(10),
   PARTNER_TYPE_3       VARCHAR2(6),
   PARTNER_3            VARCHAR2(10)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_SUP_IMPORT_ATTR is 'This table is a staging table for data conversion and will hold to import specific attributes associated with a supplier.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.SUPPLIER is 'Unique identifying number for a supplier within the system.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.AGENT is 'The agent associated with the supplier.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.ADVISING_BANK is 'Code for the bank advising the Letter of Credit.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.ISSUING_BANK is 'issuing_bank'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.LADING_PORT is 'Contains the identification number of the suppliers Lading Port.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.DISCHARGE_PORT is 'Contains the identification number of the suppliers Discharge Port.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.MFG_ID is 'Contains the manufacturers tax identification number.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.RELATED_IND is 'This field will indicate if the Supplier is related to the company or not.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.BENEFICIARY_IND is 'This field will indicate whether this supplier can be a beneficiary or not.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.WITH_RECOURSE_IND is 'Indicates conditional payment on the part of the bank as instructed by the buyer.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.REVOCABLE_IND is 'Indicates if the Letter of Credit is revocable or not.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.VARIANCE_PCT is 'Allowed currency variance percentage for the Letter of Credit.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.LC_NEG_DAYS is 'The number of days to negotiate documents.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.PLACE_OF_EXPIRY is 'Contains the place where the Letter of Credit will expire.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.DRAFTS_AT is 'This field specifies the terms of draft (or when payment is to be made) for the Letter of Credit.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.PRESENTATION_TERMS is 'Presentation terms in (P,A,N)'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.FACTORY is 'Contains the factory partner id for the factory partner type.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.PARTNER_TYPE_1 is 'Contains the partner type of the first additional partner.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.PARTNER_1 is 'Contains the partner id of the first additional partner.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.PARTNER_TYPE_2 is 'Contains the partner type of the second additional partner.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.PARTNER_2 is 'Contains the partner id of the second additional partner.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.PARTNER_TYPE_3 is 'Contains the partner type of the third additional partner.'
/
COMMENT ON COLUMN DC_SUP_IMPORT_ATTR.PARTNER_3 is 'Contains the partner id of the third additional partner.'
/