--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_TERMS_DETAIL
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_TERMS_DETAIL"
CREATE TABLE DC_TERMS_DETAIL
(
   TERMS                VARCHAR2(15),
   TERMS_SEQ            NUMBER(10),
   DUEDAYS              NUMBER(3),
   DUE_MAX_AMOUNT       NUMBER(12,4),
   DUE_DOM              NUMBER(2),
   DUE_MM_FWD           NUMBER(3),
   DISCDAYS             NUMBER(3),
   PERCENT              NUMBER(12,4),
   DISC_DOM             NUMBER(2),
   DISC_MM_FWD          NUMBER(3),
   ENABLED_FLAG         VARCHAR2(1),
   CUTOFF_DAY           NUMBER(2),
   FIXED_DATE           DATE,
   START_DATE_ACTIVE    DATE,
   END_DATE_ACTIVE      DATE
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_TERMS_DETAIL is 'This table is a staging table for data conversion and will hold data for TERM_DETAIL table.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.TERMS is 'Contains a number uniquely identifying the supplier terms.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.TERMS_SEQ is 'Order sequence in which to apply the discount percent.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.DUEDAYS is 'Contains the number of days until payment is due.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.DUE_MAX_AMOUNT is 'Maximum payment amound due by a certain date.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.DUE_DOM is 'Day of month used to calculate due date of invoice payment line.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.DUE_MM_FWD is 'Number of months ahead used to calculate due date of invoice payment line.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.DISCDAYS is 'Contains the number of days in which payment must be made in order to receive the discount.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.PERCENT is 'Contains the percent of discount if payment is made within the specified time frame.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.DISC_DOM is 'Day of month used to calculate discount date for invoice payment line.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.DISC_MM_FWD is 'Number of months ahead to calculate discount date for invoice payment line.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.ENABLED_FLAG is 'Indicates whether the Payment terms are valid or invalid within the respective application. The values would be either (Y)es or (N)o.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.CUTOFF_DAY is 'Day of the month after which Oracle Payables schedules payment using the day after the current month.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.FIXED_DATE is 'Fixed due date.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.START_DATE_ACTIVE is 'Indicates the date for assigning an active date to the Payment Terms.'
/
COMMENT ON COLUMN DC_TERMS_DETAIL.END_DATE_ACTIVE is 'Indicates the date for assigning an inactive date to the Payment Terms.'
/