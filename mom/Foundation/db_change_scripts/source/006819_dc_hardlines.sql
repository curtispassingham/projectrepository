--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_HARDLINES
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_HARDLINES"
CREATE TABLE DC_HARDLINES
(
   ITEM                     VARCHAR2(25),
   ITEM_DESC                VARCHAR2(250),
   SHORT_DESC               VARCHAR2(120),
   ITEM_DESC_SECONDARY      VARCHAR2(250),
   DEPT                     NUMBER(4),
   CLASS                    NUMBER(4),
   SUBCLASS                 NUMBER(4),
   COST_ZONE_GROUP_ID       NUMBER(4),
   UOM_CONV_FACTOR          NUMBER(20,10),
   STANDARD_UOM             VARCHAR2(4),
   STORE_ORD_MULT           VARCHAR2(1),
   COMMENTS                 VARCHAR2(2000),
   MERCHANDISE_IND          VARCHAR2(1),
   FORECAST_IND             VARCHAR2(1),
   AIP_CASE_TYPE            VARCHAR2(6),
   PRODUCT_CLASSIFICATION   VARCHAR2(6),
   BRAND_NAME               VARCHAR2(30)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_HARDLINES is 'This table is a staging table for data conversion and will hold hardlines item.'
/
COMMENT ON COLUMN DC_HARDLINES.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_HARDLINES.ITEM_DESC is 'Long description of the item.'
/
COMMENT ON COLUMN DC_HARDLINES.SHORT_DESC is 'Shortened description of the item.'
/
COMMENT ON COLUMN DC_HARDLINES.ITEM_DESC_SECONDARY is 'Secondary descriptions of the item.'
/
COMMENT ON COLUMN DC_HARDLINES.DEPT is 'Number identifying the department to which the item is attached.'
/
COMMENT ON COLUMN DC_HARDLINES.CLASS is 'Number identifying the class to which the item is attached.'
/
COMMENT ON COLUMN DC_HARDLINES.SUBCLASS is 'Number identifying the subclass to which the item is attached.'
/
COMMENT ON COLUMN DC_HARDLINES.COST_ZONE_GROUP_ID is 'Cost zone group associated with the item. This field is only required when elc_ind (landed cost indicator) is set to Y on the system_options table.'
/
COMMENT ON COLUMN DC_HARDLINES.UOM_CONV_FACTOR is 'Conversion factor between an Each and the standard_uom when the standard_uom is not in the quantity class.'
/
COMMENT ON COLUMN DC_HARDLINES.STANDARD_UOM is 'Unit of measure in which stock of the item is tracked at a corporate level.'
/
COMMENT ON COLUMN DC_HARDLINES.STORE_ORD_MULT is 'Merchandise shipped from the warehouses to the stores must be specified in this unit type. Valid values are: C = Cases I = Inner E = Eaches.'
/
COMMENT ON COLUMN DC_HARDLINES.COMMENTS is 'Holds any comments associated with the item.'
/
COMMENT ON COLUMN DC_HARDLINES.MERCHANDISE_IND is 'Indicates if the item is a merchandise item (Y, N).'
/
COMMENT ON COLUMN DC_HARDLINES.FORECAST_IND is 'Indicates if this item will be interfaced to an external forecasting system (Y, N).'
/
COMMENT ON COLUMN DC_HARDLINES.AIP_CASE_TYPE is 'Only used if AIP is integrated. Determines which case sizes to extract against an item in the AIP interface.'
/
COMMENT ON COLUMN DC_HARDLINES.PRODUCT_CLASSIFICATION is 'Product classification is informational only in RMS, but is used by RWMS to determine how to pack customer orders : such as to determine products that may not be able to be packaged together.'
/
COMMENT ON COLUMN DC_HARDLINES.BRAND_NAME is 'This field contains the brand associated to an item.'
/