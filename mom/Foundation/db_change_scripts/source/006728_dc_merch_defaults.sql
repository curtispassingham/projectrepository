--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_MERCH_DEFAULTS
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table DC_MERCH_DEFAULTS
CREATE TABLE DC_MERCH_DEFAULTS
(
   DEFAULT_CLASS            VARCHAR2(1),
   DEFAULT_SUBCLASS         VARCHAR2(1),
   DEPT_PROFIT_CALC_TYPE    NUMBER(1),
   DEPT_PURCHASE_TYPE       NUMBER(1),
   DEPT_MRKUP_PCT           NUMBER(12,4),
   DEPT_MARKUP_CALC_TYPE    VARCHAR2(2),
   DEPT_OTB_CALC_TYPE       VARCHAR2(1),
   DEPT_VAT_INCL_IND        VARCHAR2(1),
   CLASS_VAT_INCL_IND       VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_MERCH_DEFAULTS is 'This table is a staging table for data conversion and will hold  default data for merchandising details..'
/
COMMENT ON COLUMN DC_MERCH_DEFAULTS.DEFAULT_CLASS is 'Default class indicator either Y or N'
/
COMMENT ON COLUMN DC_MERCH_DEFAULTS.DEFAULT_SUBCLASS is 'Default sub class indicator either Y or N'
/
COMMENT ON COLUMN DC_MERCH_DEFAULTS.DEPT_PROFIT_CALC_TYPE is 'Contains a number which indicates whether profit will be calculated by Direct Cost or by Retail Inventory.'
/
COMMENT ON COLUMN DC_MERCH_DEFAULTS.DEPT_PURCHASE_TYPE is 'Contains a code which indicates whether items in this department are normal merchandise, consignment stock or concession items.'
/
COMMENT ON COLUMN DC_MERCH_DEFAULTS.DEPT_MRKUP_PCT is 'Contains the default markup percentage.'
/
COMMENT ON COLUMN DC_MERCH_DEFAULTS.DEPT_MARKUP_CALC_TYPE is 'Contains the code letter which determines how markup is calculated in this department.'
/
COMMENT ON COLUMN DC_MERCH_DEFAULTS.DEPT_OTB_CALC_TYPE is 'Contains the code letter which determines how OTB is calculated in this department.'
/
COMMENT ON COLUMN DC_MERCH_DEFAULTS.DEPT_VAT_INCL_IND is 'This indicator will be be used only to default to the class level indicator when classes are initially set up for the department and will only be available when the system level class vat option is on. When vat is turned on in the system and not defined at the class level, this field defaults to Y. When vat is turned off in the system, this field defaults to N.'
/
COMMENT ON COLUMN DC_MERCH_DEFAULTS.CLASS_VAT_INCL_IND is 'This field determines if retail is displayed and held with or without vat. This field is only editable when vat is turned on in the system and defined at the class level, when that is the case the field can vary between Y and N by class. When vat is turned on in the system and not defined at the class level, this field defaults to Y. When vat is turned off in the system, this field defaults to N.'
/