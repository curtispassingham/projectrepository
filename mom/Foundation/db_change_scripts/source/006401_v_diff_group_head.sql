--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_DIFF_GROUP_HEAD
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Updating View 'V_DIFF_GROUP_HEAD'
CREATE OR REPLACE FORCE VIEW V_DIFF_GROUP_HEAD ("DIFF_GROUP_ID", "DIFF_TYPE", "DIFF_GROUP_DESC", "CREATE_DATETIME", "LAST_UPDATE_ID", "LAST_UPDATE_DATETIME", "FILTER_ORG_ID", "FILTER_MERCH_ID", "FILTER_MERCH_ID_CLASS", "FILTER_MERCH_ID_SUBCLASS") AS
  SELECT dgh.DIFF_GROUP_ID,
         dgh.DIFF_TYPE,
         v.DIFF_GROUP_DESC,
         dgh.CREATE_DATETIME,
         dgh.LAST_UPDATE_ID,
         dgh.LAST_UPDATE_DATETIME,
         dgh.FILTER_ORG_ID,
         dgh.FILTER_MERCH_ID,
         dgh.FILTER_MERCH_ID_CLASS,
         dgh.FILTER_MERCH_ID_SUBCLASS 
    from diff_group_head dgh,
         v_diff_group_head_tl v
   where v.diff_group_id = dgh.diff_group_id
/

COMMENT ON TABLE V_DIFF_GROUP_HEAD IS 'This view will be used to display the Diff Group LOVs using a security policy to filter User access.'
/

