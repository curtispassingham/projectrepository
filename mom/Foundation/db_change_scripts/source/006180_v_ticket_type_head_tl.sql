CREATE OR REPLACE FORCE VIEW V_TICKET_TYPE_HEAD_TL (TICKET_TYPE_ID, TICKET_TYPE_DESC, LANG ) AS
SELECT  b.ticket_type_id,
        case when tl.lang is not null then tl.ticket_type_desc else b.ticket_type_desc end ticket_type_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  TICKET_TYPE_HEAD b,
        TICKET_TYPE_HEAD_TL tl
 WHERE  b.ticket_type_id = tl.ticket_type_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_TICKET_TYPE_HEAD_TL is 'This is the translation view for base table TICKET_TYPE_HEAD. This view fetches data in user langauge either from translation table TICKET_TYPE_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_TICKET_TYPE_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_TICKET_TYPE_HEAD_TL.TICKET_TYPE_ID is 'This field contains a character string which uniquely identifies the ticket or label type.'
/

COMMENT ON COLUMN V_TICKET_TYPE_HEAD_TL.TICKET_TYPE_DESC is 'This field contains a description of the ticket or label type.'
/

