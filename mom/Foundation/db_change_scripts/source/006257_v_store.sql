--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW Added:        v_store
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating VIEW               
--------------------------------------
PROMPT Creating VIEW 'v_store'
CREATE OR REPLACE FORCE VIEW v_store AS 
SELECT SHA.CHAIN CHAIN ,
    SHA.AREA AREA ,
    SHA.REGION REGION ,
    SHA.DISTRICT DISTRICT ,
    STR.STORE STORE ,
    V.STORE_NAME STORE_NAME ,
    V.STORE_NAME_SECONDARY STORE_NAME_SECONDARY ,
    STR.STORE_CLOSE_DATE STORE_CLOSE_DATE ,
    STR.STOP_ORDER_DAYS STOP_ORDER_DAYS ,
    STR.STOCKHOLDING_IND STOCKHOLDING_IND ,
    STR.CURRENCY_CODE CURRENCY_CODE ,
    STR.STORE_CLASS STORE_CLASS ,
    STR.TRANSFER_ZONE TRANSFER_ZONE ,
    STR.DEFAULT_WH DEFAULT_WH ,
    STR.STORE_TYPE STORE_TYPE ,
    STR.WF_CUSTOMER_ID WF_CUSTOMER_ID ,
    STR.ORG_UNIT_ID ORG_UNIT_ID,
    STR.TSF_ENTITY_ID,
    STR.VAT_REGION,
    STR.CHANNEL_ID,
    STR.CUSTOMER_ORDER_LOC_IND,
    STR.EMAIL,
    STR.DUNS_NUMBER,
    STR.DUNS_LOC,
    STR.CREATE_ID,
    STR.CREATE_DATETIME
  FROM STORE_HIERARCHY SHA ,
       STORE STR,
       V_STORE_TL V
  WHERE sha.store = str.store
    AND str.store = v.store
/
  
COMMENT ON TABLE v_store IS 'This view will be used to display the Store LOVs using a security policy to filter User access. The store name and secondary name are translated based on user language.'

comment on column v_store.chain IS 'Contains the chain the store belongs to.'
/
comment on column v_store.area IS 'Contains the area the store belongs to.'
/
comment on column v_store.region IS 'Contains the region the store belongs to.'
/
comment on column v_store.district IS 'Contains the district the store belongs to.'
/
comment on column v_store.STORE IS 'Contains the number which uniquely identifies the store.'
/
comment on column v_store.STORE_NAME IS 'Contains the translated name of the store which, along with the store number, identifies the store.'
/
comment on column v_store.STORE_NAME_SECONDARY IS 'Translated Secondary name of the store.'
/
comment on column v_store.STORE_CLOSE_DATE IS 'Contains the date on which the store closed.'
/
comment on column v_store.STOP_ORDER_DAYS IS 'Contains the number of days before a store closing that the store will stop accepting orders.  This column will be used when the store_close_date is defined.'
/
comment on column v_store.STOCKHOLDING_IND IS 'This column indicates whether the store can hold stock.  In a non-multichannel environment this will always be Y.'
/
comment on column v_store.CURRENCY_CODE IS 'This field contains the currency code under which the store operates.'
/
comment on column v_store.STORE_CLASS IS 'Contains the code letter indicating the class of which the store is a member.  Valid values are A through E.'
/
comment on column v_store.TRANSFER_ZONE IS 'Contains the transfer zone in which the store is located.  Valid values are located on the tsfzone table.'
/
comment on column v_store.DEFAULT_WH IS 'Contains the number of the warehouse that may be used as the default for creating cross-dock masks.  This determines which stores are associated with or sourced from a warehouse. Will hold only virtual warehouses in a multi-channel environment.'
/
comment on column v_store.STORE_TYPE IS 'This will indicate whether a particular store is a franchise or company store.'
/
comment on column v_store.WF_CUSTOMER_ID IS 'Numeric Id of the customer.'
/
comment on column v_store.ORG_UNIT_ID IS 'Column will contain the organizational unit ID value.'
/
comment on column v_store.VAT_REGION IS 'Contains the number of the Value Added Tax region in which this store is contained.'
/
comment on column v_store.CHANNEL_ID IS 'In a multichannel environment this will contain the channel with which the store is associated.  Valid values can be found on the channels table.'
/
comment on column v_store.CUSTOMER_ORDER_LOC_IND IS 'This Column determines whether the location is customer order location or not.If the indicator is Y then the location can be used by OMS for sourcing/ fulfillment or both else it  cannot be used.It is enabled only for the company stores .'
/
comment on column v_store.EMAIL IS 'Holds the email address for the location'
/
comment on column v_store.DUNS_NUMBER IS 'This field holds the Dun and Bradstreet number to identify the store.'
/
comment on column v_store.DUNS_LOC IS 'This field holds the Dun and Bradstreet number to identify the location'
/
comment on column v_store.CREATE_ID IS 'This column holds the User id of the user who created the record.'
/
comment on column v_store.CREATE_DATETIME IS 'This column holds the record creation date.'
/
