--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_STATE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_STATE'
CREATE TABLE SVC_STATE
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
    COUNTRY_ID VARCHAR2(3) ,
    DESCRIPTION VARCHAR2(120) ,
    STATE VARCHAR2(3) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE 
	)
	INITRANS 6	
	TABLESPACE RETAIL_DATA
/


COMMENT ON TABLE SVC_STATE IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in STATE.'
/

COMMENT ON COLUMN SVC_STATE.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_STATE.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_STATE.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_STATE.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_STATE.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_STATE.STATE IS 'This column contains the state abbreviation for the address.'
/

COMMENT ON COLUMN SVC_STATE.COUNTRY_ID IS 'Contains the unique code which identifies the country where the state belongs.'
/

COMMENT ON COLUMN SVC_STATE.DESCRIPTION IS 'Contains the full name of the state.'
/

COMMENT ON COLUMN SVC_STATE.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_STATE.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_STATE.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_STATE.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/



PROMPT Creating Primary Key on 'SVC_STATE'               
ALTER TABLE SVC_STATE
ADD CONSTRAINT SVC_STATE_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX   
/

PROMPT Creating Unique Key on 'SVC_STATE'
ALTER TABLE SVC_STATE
ADD CONSTRAINT SVC_STATE_UK UNIQUE ( COUNTRY_ID,STATE )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/
	