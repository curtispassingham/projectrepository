--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Updating View  V_LOC_TRAITS
--------------------------------------
PROMPT Creating View 'V_LOC_TRAITS'
CREATE OR REPLACE FORCE VIEW V_LOC_TRAITS (LOC_TRAIT, DESCRIPTION, FILTER_ORG_ID)
 AS SELECT LOT.LOC_TRAIT LOC_TRAIT
          ,V.DESCRIPTION DESCRIPTION
          ,LOT.FILTER_ORG_ID FILTER_ORG_ID
FROM LOC_TRAITS LOT,
     V_LOC_TRAITS_TL V
WHERE LOT.LOC_TRAIT = V.LOC_TRAIT
/

COMMENT ON TABLE V_LOC_TRAITS IS 'This view will be used to display the Location List LOVs using a security policy to filter User access. The description is translated based on user language.'
/
