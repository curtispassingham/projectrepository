--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_PRICING_EVENT_TEMP
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_PRICING_EVENT_TEMP'
CREATE TABLE SVC_PRICING_EVENT_TEMP
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  THREAD_VAL NUMBER(10,0),
  CHUNK_ID NUMBER(10,0) DEFAULT 1,
  PROCESS_STATUS VARCHAR2(6 ) DEFAULT 'N' NOT NULL,
  EVENT_TYPE VARCHAR2(6 ) NOT NULL,
  EVENT_ID NUMBER(12,0) NOT NULL,
  EFFECTIVE_DATE DATE,
  CURRENCY_CODE VARCHAR2(6 ),
  ITEM_RANK NUMBER(2,0),
  ITEM VARCHAR2(25 ),
  ITEM_PARENT VARCHAR2(25 ),
  ITEM_GRANDPARENT VARCHAR2(25 ),
  DEPT NUMBER(4,0),
  CLASS NUMBER(4,0),
  SUBCLASS NUMBER(4,0),
  ITEM_LEVEL NUMBER(1,0),
  TRAN_LEVEL NUMBER(1,0),
  STATUS VARCHAR2(1 ),
  PACK_IND VARCHAR2(1 ),
  SELLABLE_IND VARCHAR2(1 ),
  ORDERABLE_IND VARCHAR2(1 ),
  CATCH_WEIGHT_IND VARCHAR2(1 ),
  PACK_TYPE VARCHAR2(1 ),
  STANDARD_UOM VARCHAR2(4 ),
  HIER_RANK NUMBER(2,0),
  LOCATION NUMBER(10,0),
  LOC_TYPE VARCHAR2(1 ),
  ITEM_SOH NUMBER(12,4),
  CLEAR_IND VARCHAR2(1 ),
  IL_PROMO_SELLING_RETAIL NUMBER(20,4),
  PRIMARY_SUPP NUMBER(10,0),
  IL_SELLING_UNIT_RETAIL NUMBER(20,4),
  IL_SELLING_UOM VARCHAR2(4 ),
  ORIG_UNIT_RETAIL NUMBER(20,4),
  OLD_UNIT_RETAIL NUMBER(20,4),
  NEW_UNIT_RETAIL NUMBER(20,4),
  SELLING_UNIT_RETAIL NUMBER(20,4),
  SELLING_UOM VARCHAR2(4 ),
  MULTI_UNITS NUMBER(12,4),
  MULTI_UNIT_RETAIL NUMBER(20,4),
  MULTI_SELLING_UOM VARCHAR2(4 ),
  PROMO_SELLING_RETAIL NUMBER(20,4),
  PROMO_SELLING_UOM VARCHAR2(4 ),
  PROMO_RETAIL NUMBER(20,4),
  LOC_CURRENCY_CODE VARCHAR2(6 ),
  EVENT_SIMILARITY NUMBER(2,0),
  PRICE_HIST_TRAN_TYPE NUMBER(2,0),
  ERROR_MESSAGE VARCHAR2(2000 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_PRICING_EVENT_TEMP is 'This is a table used for explosion and Processing of External Pricing Events in RMS'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.PROCESS_ID is 'This corresponds to the unique value in svc_pricing_event_head table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.THREAD_VAL is 'This contains the thead number to be used during multithreading'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1 for online processing or calculated based on item'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.PROCESS_STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error , S - Skipped , or I - In Progress'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.EVENT_TYPE is 'Event Type for Price Change Event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.EVENT_ID is 'Event_Id for Price Change Event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.EFFECTIVE_DATE is 'Effective date for Price Change Event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.CURRENCY_CODE is 'Currency code for Price Change Event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.ITEM_RANK is 'Rank Value calculated based on Item Level for that process'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.ITEM is 'Alphanumeric value that identifies the item.'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.ITEM_PARENT is 'Item Parent Value fetched from Item_master'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.ITEM_GRANDPARENT is 'Item Grandparent Value from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.DEPT is 'Dept value from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.CLASS is 'Class Value  from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.SUBCLASS is 'Subclass Value from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.ITEM_LEVEL is 'Item level from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.TRAN_LEVEL is 'Tran Level  from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.STATUS is 'Item status from  from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.PACK_IND is 'Pack Indicator  from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.SELLABLE_IND is 'Sellable Indicator  from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.ORDERABLE_IND is 'Orderable Indicator from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.CATCH_WEIGHT_IND is 'Catch Weight Indicator from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.PACK_TYPE is 'Pack Type value from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.STANDARD_UOM is 'Standard Uom of Item from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.HIER_RANK is 'Hierachy rank value calculated based in Hier Level for that process'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.LOCATION is 'Location to which Price change is applicable'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.LOC_TYPE is 'Location type for the location id , can be ''S''tore or ''W''arehouse'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.ITEM_SOH is 'Sum of Item''s Stock_on_hand, in_transit_qty, pack_comp_soh and pack_comp_intran at the location.This column is used during processing of Pricing Event.'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.CLEAR_IND is 'Clearance Indicator from Item_loc table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.IL_PROMO_SELLING_RETAIL is 'Current Promo_selling_retail of Item from item_loc table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.PRIMARY_SUPP is 'Primary supplier from Item_loc table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.IL_SELLING_UNIT_RETAIL is 'Current Selling_Unit_retail value from Item_loc table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.IL_SELLING_UOM is 'Current Selling_UOM value from Item_loc table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.ORIG_UNIT_RETAIL is 'Original Unit Retail value used in Tran Data postings.This column is used during processing of Pricing Event.'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.OLD_UNIT_RETAIL is 'Current Value of Unit_retail from Item_loc table.'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.NEW_UNIT_RETAIL is 'Selling_unit_retail value in Standard UOM.This column is used during processing of Pricing Event.'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.SELLING_UNIT_RETAIL is 'New Selling Unit Retail for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.SELLING_UOM is 'New Selling UOM for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.MULTI_UNITS is 'New Multi Units for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.MULTI_UNIT_RETAIL is 'New Multi Unit Retail for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.MULTI_SELLING_UOM is 'New Multi Selling UOM for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.PROMO_SELLING_RETAIL is 'New Promo Selling Retail for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.PROMO_SELLING_UOM is 'New Promo Selling UOM for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.PROMO_RETAIL is 'Promo_selling_retail value in standard UOM.This column is used during processing of Pricing Event.'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.LOC_CURRENCY_CODE is 'Location Currency code from store or wh table. This column is used during processing of Pricing Event.'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.EVENT_SIMILARITY is 'Similarity of event_types to identify processing path. This column is used during processing of Pricing Event.'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.PRICE_HIST_TRAN_TYPE is 'Tran type value to be inserted in Price_Hist table.'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_TEMP.ERROR_MESSAGE is 'Error Message populated during the validation phase of Processing.'
/

