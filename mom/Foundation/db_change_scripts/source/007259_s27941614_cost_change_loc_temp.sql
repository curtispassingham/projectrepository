--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


-----------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to 
--   ensure data is preserved as desired.
--
-----------------------------------------------------------------
--	INDEX ADDED:			COST_CHANGE_LOC_TEMP_I1
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING INDEX
--------------------------------------

PROMPT CREATING INDEX 'COST_CHANGE_LOC_TEMP_I1'
CREATE INDEX COST_CHANGE_LOC_TEMP_I1 ON COST_CHANGE_LOC_TEMP
(
ITEM,
SUPPLIER, 
LOCATION,
ORIGIN_COUNTRY_ID,
LOC_TYPE,
COST_CHANGE
)
/
