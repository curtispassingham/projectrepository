--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_PACK_XREF
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_PACK_XREF"
CREATE TABLE DC_PACK_XREF
(
   ITEM                      VARCHAR2(25),
   ITEM_DESC                 VARCHAR2(250),
   SHORT_DESC                VARCHAR2(120),
   ITEM_DESC_SECONDARY       VARCHAR2(250),
   ITEM_PARENT               VARCHAR2(25),
   COMMENTS                  VARCHAR2(2000),
   PRIMARY_REF_ITEM_IND      VARCHAR2(1),
   ITEM_NUMBER_TYPE          VARCHAR2(6),
   FORMAT_ID                 VARCHAR2(1),
   PREFIX                    NUMBER(2),
   PERISHABLE_IND            VARCHAR2(1),
   NOTIONAL_PACK_IND         VARCHAR2(1),
   SOH_INQUIRY_AT_PACK_IND   VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_PACK_XREF is 'This table is a staging table for data conversion and will hold the pack xref information'
/
COMMENT ON COLUMN DC_PACK_XREF.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_PACK_XREF.ITEM_DESC is 'Long description of the item.'
/
COMMENT ON COLUMN DC_PACK_XREF.SHORT_DESC is 'Shortened description of the item.'
/
COMMENT ON COLUMN DC_PACK_XREF.ITEM_DESC_SECONDARY is 'Secondary descriptions of the item.'
/
COMMENT ON COLUMN DC_PACK_XREF.ITEM_PARENT is 'Alphanumeric value that uniquely identifies the item/group at the level above the item.'
/
COMMENT ON COLUMN DC_PACK_XREF.COMMENTS is 'Holds any comments associated with the item.'
/
COMMENT ON COLUMN DC_PACK_XREF.PRIMARY_REF_ITEM_IND is 'Indicates if the sub-transation level item is designated as the primary sub-transaction level item.'
/
COMMENT ON COLUMN DC_PACK_XREF.ITEM_NUMBER_TYPE is 'Code specifying what type the item is.'
/
COMMENT ON COLUMN DC_PACK_XREF.FORMAT_ID is 'This field will hold the format ID that corresponds to the items variable UPC.'
/
COMMENT ON COLUMN DC_PACK_XREF.PREFIX is 'This column holds the prefix for variable weight UPCs.'
/
COMMENT ON COLUMN DC_PACK_XREF.PERISHABLE_IND is 'A grocery item attribute used to indicate whether an item is perishable or not.'
/
COMMENT ON COLUMN DC_PACK_XREF.NOTIONAL_PACK_IND is 'This is to indicate that the pack item should post the transaction at pack level in SIM. '
/
COMMENT ON COLUMN DC_PACK_XREF.SOH_INQUIRY_AT_PACK_IND is 'This indicates to show the stock on hand at pack level in downstream applications when it is called in POS from SIM.'
/