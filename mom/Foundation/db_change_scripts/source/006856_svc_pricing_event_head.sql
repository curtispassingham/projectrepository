--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_PRICING_EVENT_HEAD
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_PRICING_EVENT_HEAD'
CREATE TABLE SVC_PRICING_EVENT_HEAD
 (EVENT_ID NUMBER(12,0) NOT NULL,
  EVENT_TYPE VARCHAR2(6 ) NOT NULL,
  PROCESS_ID NUMBER(10,0) NOT NULL,
  PROCESS_STATUS VARCHAR2(6 ) DEFAULT 'N' NOT NULL,
  ITEM_LEVEL NUMBER(1,0),
  ITEM VARCHAR2(25 ),
  DIFF_ID VARCHAR2(6 ),
  HIER_LEVEL VARCHAR2(4 ),
  EFFECTIVE_DATE DATE,
  CURRENCY_CODE VARCHAR2(6 ),
  SELLING_UNIT_RETAIL NUMBER(20,4),
  SELLING_UOM VARCHAR2(4 ),
  MULTI_UNITS NUMBER(12,4),
  MULTI_UNIT_RETAIL NUMBER(20,4),
  MULTI_SELLING_UOM VARCHAR2(4 ),
  PROMO_SELLING_RETAIL NUMBER(20,4),
  PROMO_SELLING_UOM VARCHAR2(4 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_PRICING_EVENT_HEAD is 'This is a table used for staging the pricing events from the external system'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.EVENT_ID is 'Event_Id for Price Change Event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.EVENT_TYPE is 'Event Type for Price Change Event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.PROCESS_ID is 'This corresponds to the unique value in svc_pricing_event_head table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.PROCESS_STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error , S - Skipped , or I - In Progress'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.ITEM_LEVEL is 'Item level from Item_master table'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.ITEM is 'Alphanumeric value that identifies the item.'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.DIFF_ID is 'Diff_id that differentiates the current item from its item_parent.'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.HIER_LEVEL is 'This field indicates the merchandise hierarchy level'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.EFFECTIVE_DATE is 'Effective date for Price Change Event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.CURRENCY_CODE is 'Currency code for Price Change Event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.SELLING_UNIT_RETAIL is 'New Selling Unit Retail for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.SELLING_UOM is 'New Selling UOM for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.MULTI_UNITS is 'New Multi Units for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.MULTI_UNIT_RETAIL is 'New Multi Unit Retail for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.MULTI_SELLING_UOM is 'New Multi Selling UOM for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.PROMO_SELLING_RETAIL is 'New Promo Selling Retail for the price change event'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.PROMO_SELLING_UOM is 'New Promo Selling UOM for the price change event'
/


PROMPT Creating Primary Key on 'SVC_PRICING_EVENT_HEAD'
ALTER TABLE SVC_PRICING_EVENT_HEAD
 ADD CONSTRAINT SVC_PRICING_EVENT_HEAD_PK PRIMARY KEY
  (EVENT_ID,
   EVENT_TYPE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

