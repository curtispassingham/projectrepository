--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- View Updated:      V_ITEM_MASTER_TL 
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Updating View
--------------------------------------
PROMPT UPDATING VIEW 'V_ITEM_MASTER_TL'
CREATE OR REPLACE FORCE VIEW V_ITEM_MASTER_TL (ITEM, ITEM_DESC, ITEM_DESC_SECONDARY, SHORT_DESC, LANG ) AS
SELECT  b.item,
        case when tl.lang is not null then tl.item_desc else b.item_desc end item_desc,
        case when tl.lang is not null then tl.item_desc_secondary else b.item_desc_secondary end item_desc_secondary,
        case when tl.lang is not null then tl.short_desc else b.short_desc end short_desc,
        NVL(tl.lang,lng.prim_lang) lang
  FROM  ITEM_MASTER b,
        ITEM_MASTER_TL tl,
        (select GET_PRIMARY_LANG() prim_lang, LANGUAGE_SQL.GET_USER_LANGUAGE() user_lang from dual where rownum = 1) lng
 WHERE  b.item = tl.item (+)
   AND  tl.lang (+) = lng.user_lang
/

COMMENT ON TABLE V_ITEM_MASTER_TL is 'This is the translation view for base table ITEM_MASTER. This view fetches data in user langauge either from translation table ITEM_MASTER_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_ITEM_MASTER_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_ITEM_MASTER_TL.ITEM is 'Unique alphanumeric value that identifies the item.'
/

COMMENT ON COLUMN V_ITEM_MASTER_TL.ITEM_DESC is 'Long description of the item.  This description is used through out the system to help online users identify the item.  For items that have parents, this description will default to the parents description plus any differentiators.  For items without parents, this description will default to null.'
/

COMMENT ON COLUMN V_ITEM_MASTER_TL.ITEM_DESC_SECONDARY is 'Secondary descriptions of the item.  This field can only be populated when system_options.secondary_desc_ind = Y.'
/

COMMENT ON COLUMN V_ITEM_MASTER_TL.SHORT_DESC is 'Shortened description of the item.  This description is the default for downloading to the POS.  For items that have parents, this description will default to the parents short description.  For items without parents, this description will default to null.'
/

