--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_SEASONS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Updating View 'V_SEASONS'
CREATE OR REPLACE FORCE VIEW V_SEASONS
 AS SELECT SSN.SEASON_ID SEASON_ID
          ,V.SEASON_DESC SEASON_DESC
          ,SSN.FILTER_ORG_ID FILTER_ORG_ID
          ,SSN.FILTER_MERCH_ID FILTER_MERCH_ID
          ,SSN.FILTER_MERCH_ID_CLASS FILTER_MERCH_ID_CLASS
          ,SSN.FILTER_MERCH_ID_SUBCLASS FILTER_MERCH_ID_SUBCLASS
          ,SSN.START_DATE START_DATE
          ,SSN.END_DATE END_DATE
FROM SEASONS SSN,
     V_SEASONS_TL V
WHERE V.SEASON_ID = SSN.SEASON_ID
/

COMMENT ON TABLE V_SEASONS IS 'This view will be used to display the Seasons LOVs using a security policy to filter User access.'
/

