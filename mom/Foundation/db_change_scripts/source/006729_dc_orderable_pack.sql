--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_ORDERABLE_PACK
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_ORDERABLE_PACK"
CREATE TABLE DC_ORDERABLE_PACK
(
   ITEM                      VARCHAR2(25),
   DIFF_1                    VARCHAR2(10),
   DIFF_2                    VARCHAR2(10),
   DIFF_3                    VARCHAR2(10),
   DIFF_4                    VARCHAR2(10),
   DEPT                      NUMBER(4),
   CLASS                     NUMBER(4),
   SUBCLASS                  NUMBER(4),
   ITEM_DESC                 VARCHAR2(250),
   SHORT_DESC                VARCHAR2(120),
   ITEM_DESC_SECONDARY       VARCHAR2(250),
   COST_ZONE_GROUP_ID        NUMBER(4),
   PACKAGE_SIZE              NUMBER(12,4),
   PACKAGE_UOM               VARCHAR2(4),
   STORE_ORD_MULT            VARCHAR2(1),
   MFG_REC_RETAIL            NUMBER(20,4),
   RETAIL_LABEL_TYPE         VARCHAR2(6),
   RETAIL_LABEL_VALUE        NUMBER(20,4),
   HANDLING_TEMP             VARCHAR2(6),
   HANDLING_SENSITIVITY      VARCHAR2(6),
   CATCH_WEIGHT_IND          VARCHAR2(1),
   SIMPLE_PACK_IND           VARCHAR2(1),
   SELLABLE_IND              VARCHAR2(1),
   PACK_TYPE                 VARCHAR2(1),
   ORDER_AS_TYPE             VARCHAR2(1),
   COMMENTS                  VARCHAR2(2000),
   ORDER_TYPE                VARCHAR2(6),
   SALE_TYPE                 VARCHAR2(6),
   PERISHABLE_IND            VARCHAR2(1),
   NOTIONAL_PACK_IND         VARCHAR2(1),
   SOH_INQUIRY_AT_PACK_IND   VARCHAR2(1),
   PRODUCT_CLASSIFICATION    VARCHAR2(6),
   BRAND_NAME                VARCHAR2(30)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_ORDERABLE_PACK is 'This table is a staging table for data conversion and will hold the orderable pack item information.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.DIFF_1 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.DIFF_2 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.DIFF_3 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.DIFF_4 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.DEPT is 'Number identifying the department to which the item is attached.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.CLASS is 'Number identifying the class to which the item is attached.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.SUBCLASS is 'Number identifying the subclass to which the item is attached.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.ITEM_DESC is 'Long description of the item.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.SHORT_DESC is 'Shortened description of the item.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.ITEM_DESC_SECONDARY is 'Secondary descriptions of the item.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.COST_ZONE_GROUP_ID is 'Cost zone group associated with the item.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.PACKAGE_SIZE is 'Holds the size of the product printed on any packaging.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.PACKAGE_UOM is 'Holds the unit of measure associated with the package size.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.STORE_ORD_MULT is 'Merchandise shipped from the warehouses to the stores must be specified in this unit type.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.MFG_REC_RETAIL is 'Manufacturers recommended retail price for the item.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.RETAIL_LABEL_TYPE is 'This field indicates any special lable type associated with an item'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.RETAIL_LABEL_VALUE is 'This field represents the value associated with the retail label type.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.HANDLING_TEMP is 'Holds the temperature information associated with the item.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.HANDLING_SENSITIVITY is 'Holds the sensitivity information associated with the item.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.CATCH_WEIGHT_IND is 'Indiactes whether the item should be weighed when it arrives at a location.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.SIMPLE_PACK_IND is 'Indicates if pack item is a simple pack or not.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.SELLABLE_IND is 'Indicates if pack item may be sold as a unit.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.PACK_TYPE is 'Indicates if pack item is a vendor pack or a buyer pack.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.ORDER_AS_TYPE is 'Indicates if pack item is receivable at the component level or at the pack level.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.COMMENTS is 'Holds any comments associated with the item.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.ORDER_TYPE is 'This determines how catch weight items are ordered.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.SALE_TYPE is 'This indicates the method of how catch weight items are sold in store locations.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.PERISHABLE_IND is 'A grocery item attribute used to indicate whether an item is perishable or not.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.NOTIONAL_PACK_IND is 'This is to indicate that the pack item should post the transaction at pack level in SIM. '
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.SOH_INQUIRY_AT_PACK_IND is 'This indicates to show the stock on hand at pack level in downstream applications when it is called in POS from SIM.'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.PRODUCT_CLASSIFICATION is 'Product classification is informational only in RMS, but is used by RWMS to determine how to pack customer orders'
/
COMMENT ON COLUMN DC_ORDERABLE_PACK.BRAND_NAME is 'This field contains the brand associated to an item'
/