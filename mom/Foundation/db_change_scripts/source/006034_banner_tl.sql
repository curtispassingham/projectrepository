--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE ADDED:				BANNER_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TYPE
--------------------------------------
PROMPT Creating Table 'BANNER_TL'
CREATE TABLE BANNER_TL(
LANG NUMBER(6) NOT NULL,
BANNER_ID NUMBER(4) NOT NULL,
BANNER_NAME VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE BANNER_TL is 'This is the translation table for BANNER table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN BANNER_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN BANNER_TL.BANNER_ID is 'The number to uniquely identify a Banner.'
/

COMMENT ON COLUMN BANNER_TL.BANNER_NAME is 'The name of the Banner for which channels are associated.'
/

COMMENT ON COLUMN BANNER_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN BANNER_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN BANNER_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN BANNER_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE BANNER_TL ADD CONSTRAINT PK_BANNER_TL PRIMARY KEY (
LANG,
BANNER_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE BANNER_TL
 ADD CONSTRAINT BANT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE BANNER_TL ADD CONSTRAINT BANT_BAN_FK FOREIGN KEY (
BANNER_ID
) REFERENCES BANNER (
BANNER_ID
)
/

