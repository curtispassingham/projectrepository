--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_REGION
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_REGION"
CREATE TABLE DC_REGION
(
   REGION                     NUMBER(10),
   REGION_NAME                VARCHAR2(120),
   AREA                       NUMBER(10),
   MGR_NAME                   VARCHAR2(120),
   CURRENCY_CODE              VARCHAR2(3)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_REGION is 'This table is a staging table for data conversion and will hold data for REGION.'
/
COMMENT ON COLUMN DC_REGION.REGION is 'Contains the number which uniquely identifies the region.'
/
COMMENT ON COLUMN DC_REGION.REGION_NAME is 'Contains the name of the region which, along with the region number, identifies the region.'
/
COMMENT ON COLUMN DC_REGION.AREA is 'Contains the number of the area of which the region is a member.'
/
COMMENT ON COLUMN DC_REGION.MGR_NAME is 'Contains the name of the manager of the region.'
/
COMMENT ON COLUMN DC_REGION.CURRENCY_CODE is 'This field contains the currency code under which the region operates.'
/