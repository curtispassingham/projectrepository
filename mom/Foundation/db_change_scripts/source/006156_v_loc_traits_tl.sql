CREATE OR REPLACE FORCE VIEW V_LOC_TRAITS_TL (LOC_TRAIT, DESCRIPTION, LANG ) AS
SELECT  b.loc_trait,
        case when tl.lang is not null then tl.description else b.description end description,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  LOC_TRAITS b,
        LOC_TRAITS_TL tl
 WHERE  b.loc_trait = tl.loc_trait (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_LOC_TRAITS_TL is 'This is the translation view for base table LOC_TRAITS. This view fetches data in user langauge either from translation table LOC_TRAITS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_LOC_TRAITS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_LOC_TRAITS_TL.LOC_TRAIT is 'Contains a number which uniquely identifies a location trait.'
/

COMMENT ON COLUMN V_LOC_TRAITS_TL.DESCRIPTION is 'Contains a description which corresponds with the location trait number.'
/

