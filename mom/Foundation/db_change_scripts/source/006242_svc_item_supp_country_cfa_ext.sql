--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_ITEM_SUPP_COUNTRY_CFA_EXT'
ALTER TABLE SVC_ITEM_SUPP_COUNTRY_CFA_EXT ADD DATE_23 DATE  NULL
/

COMMENT ON COLUMN SVC_ITEM_SUPP_COUNTRY_CFA_EXT.DATE_23 is 'Refer to SVC_ITEM_SUPP_COUNTRY_CFA_EXT.DATE_23.'
/

ALTER TABLE SVC_ITEM_SUPP_COUNTRY_CFA_EXT ADD DATE_24 DATE  NULL
/

COMMENT ON COLUMN SVC_ITEM_SUPP_COUNTRY_CFA_EXT.DATE_24 is 'Refer to SVC_ITEM_SUPP_COUNTRY_CFA_EXT.DATE_24.'
/

ALTER TABLE SVC_ITEM_SUPP_COUNTRY_CFA_EXT ADD DATE_25 DATE  NULL
/

COMMENT ON COLUMN SVC_ITEM_SUPP_COUNTRY_CFA_EXT.DATE_25 is 'Refer to SVC_ITEM_SUPP_COUNTRY_CFA_EXT.DATE_25.'
/

