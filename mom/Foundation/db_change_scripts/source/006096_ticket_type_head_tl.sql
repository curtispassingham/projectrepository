--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       TICKET_TYPE_HEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE TICKET_TYPE_HEAD_TL(
LANG NUMBER(6) NOT NULL,
TICKET_TYPE_ID VARCHAR2(4) NOT NULL,
TICKET_TYPE_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE TICKET_TYPE_HEAD_TL is 'This is the translation table for TICKET_TYPE_HEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN TICKET_TYPE_HEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN TICKET_TYPE_HEAD_TL.TICKET_TYPE_ID is 'This field contains a character string which uniquely identifies the ticket or label type.'
/

COMMENT ON COLUMN TICKET_TYPE_HEAD_TL.TICKET_TYPE_DESC is 'This field contains a description of the ticket or label type.'
/

COMMENT ON COLUMN TICKET_TYPE_HEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN TICKET_TYPE_HEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN TICKET_TYPE_HEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN TICKET_TYPE_HEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE TICKET_TYPE_HEAD_TL ADD CONSTRAINT PK_TICKET_TYPE_HEAD_TL PRIMARY KEY (
LANG,
TICKET_TYPE_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE TICKET_TYPE_HEAD_TL
 ADD CONSTRAINT TTHT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE TICKET_TYPE_HEAD_TL ADD CONSTRAINT TTHT_TTH_FK FOREIGN KEY (
TICKET_TYPE_ID
) REFERENCES TICKET_TYPE_HEAD (
TICKET_TYPE_ID
)
/

