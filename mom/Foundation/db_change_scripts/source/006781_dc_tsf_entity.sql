--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_TSF_ENTITY
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_TSF_ENTITY"
CREATE TABLE DC_TSF_ENTITY
(
   TSF_ENTITY_ID              NUMBER(10),
   TSF_ENTITY_DESC            VARCHAR2(120)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_TSF_ENTITY is 'This table is a staging table for data conversion and will holds all the transfer entities created within the company.'
/
COMMENT ON COLUMN DC_TSF_ENTITY.TSF_ENTITY_ID is 'This field contains the number which uniquely identifies the transfer entity.'
/
COMMENT ON COLUMN DC_TSF_ENTITY.TSF_ENTITY_DESC is 'This field contains the name of the transfer entity.'
/