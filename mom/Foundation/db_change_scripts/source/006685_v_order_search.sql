--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  View CREATED:                       V_ORDER_SEARCH
---------------------------------------------------------------------------
CREATE OR REPLACE FORCE VIEW V_ORDER_SEARCH
(ORDER_NO,
 STATUS,
 SUPPLIER_SITE,
 SUPPLIER_SITE_NAME,
 SUPPLIER_PARENT,
 SUPPLIER_PARENT_NAME,
 NOT_BEFORE_DATE,
 NOT_AFTER_DATE,
 OTB_EOW_DATE,
 WRITTEN_DATE,
 ORDER_TYPE,
 CURRENCY_CODE,
 IMPORT_ORDER_IND,
 IMPORT_COUNTRY_ID,
 CONTRACT_NO,
 ORIG_IND,
 SPLIT_REF_ORDNO,
 VENDOR_ORDER_NO,
 PO_TYPE,
 PROMOTION,
 BUYER,
 POOL_SUP_SITE,
 POOL_SUP_SITE_NAME,
 POOL_SUP_PARENT,
 POOL_SUP_PARENT_NAME,
 ITEM,
 ITEM_DESC,
 LOCATION,
 LOCATION_TYPE,
 PHYSICAL_LOC,
 DIVISION,
 GROUP_NO,
 DEPT,
 CLASS,
 SUBCLASS,
 CHAIN,
 AREA,
 REGION,
 DISTRICT,
 TSF_PO_LINK_NO,
 MASTER_PO_NO)
AS
SELECT o.order_no,
       o.status,
       o.supplier supplier_site,
       ss.sup_name supplier_site_name,
       ss.supplier_parent,
       (select sup_name from v_sups where supplier = ss.supplier_parent) supplier_parent_name,
       o.NOT_BEFORE_DATE,
       o.NOT_AFTER_DATE,
       o.OTB_EOW_DATE,
       o.written_date,
       o.order_type,
       o.currency_code,
       o.import_order_ind,
       o.import_country_id,
       o.contract_no,
       o.orig_ind ,
       o.SPLIT_REF_ORDNO,
       o.vendor_order_no,
       o.po_type,
       o.Promotion,
       o.buyer,
       oim.pool_sup_site,
       oim.pool_sup_site_name,
       oim.pool_sup_parent,
       (select sup_name from v_sups where supplier =  oim.pool_sup_parent) pool_sup_parent_name,
       null item,
       null item_desc,
       o.location location,
       o.loc_type location_type,
       vw.physical_wh  physical_loc,
       g.division  division,
       d.group_no group_no,
       o.dept dept,
       null class,
       null subclass,
       vs.chain chain,
       vs.area area,
       vs.region region,
       vs.district district,
       null tsf_po_link_no,
       o.master_po_no
  FROM ordhead o,
       (select oi.order_no,
               oi.pool_supplier pool_sup_site,
               ss2.sup_name pool_sup_site_name,
               ss2.supplier_parent pool_sup_parent
          from ord_inv_mgmt oi,
               v_sups ss2
         where oi.pool_supplier = ss2.supplier) oim,
       v_sups ss,
       v_store vs,
       v_wh vw,
       deps d,
       groups g
 WHERE o.order_no = oim.order_no(+)
   and o.supplier = ss.supplier
   and o.location = vs.store(+)
   and o.location = vw.wh(+)
   and o.dept = d.dept
   and d.group_no = g.group_no
   and not exists (select 'x'
                    from ordloc ol
                   where o.order_no = ol.order_no
                     and rownum = 1)
 union all
SELECT o.order_no,
       o.status,
       o.supplier supplier_site,
       ss.sup_name supplier_site_name,
       ss.supplier_parent,
       (select sup_name from v_sups where supplier = ss.supplier_parent) supplier_parent_name,
       o.NOT_BEFORE_DATE,
       o.NOT_AFTER_DATE,
       o.OTB_EOW_DATE,
       o.written_date,
       o.order_type,
       o.currency_code,
       o.import_order_ind,
       o.import_country_id,
       o.contract_no,
       o.orig_ind ,
       o.SPLIT_REF_ORDNO,
       o.vendor_order_no,
       o.po_type,
       o.Promotion,
       o.buyer,
       oim.pool_sup_site,
       oim.pool_sup_site_name,
       oim.pool_sup_parent,
       (select sup_name from v_sups where supplier =  oim.pool_sup_parent) pool_sup_parent_name,
       im.item item,
       im.item_desc item_desc,
       ol.location location,
       ol.loc_type location_type,
       vw.physical_wh  physical_loc,
       g.division  division,
       d.group_no group_no,
       d.dept dept,
       c.class class,
       s.subclass subclass,
       vs.chain chain,
       vs.area area,
       vs.region region,
       vs.district district,
       ol.tsf_po_link_no tsf_po_link_no,
       o.master_po_no
  from ordloc ol,
       v_item_master im,
       ordhead o,
       (select oi.order_no,
               oi.pool_supplier pool_sup_site,
               ss2.sup_name pool_sup_site_name,
               ss2.supplier_parent pool_sup_parent
          from ord_inv_mgmt oi,
               v_sups ss2
         where oi.pool_supplier = ss2.supplier) oim,
       v_sups ss,
       v_store vs,
       v_wh vw,
       deps d,
       class c,
       subclass s,
       groups g
 where im.item = ol.item
   and ol.order_no = o.order_no
   and o.order_no = oim.order_no(+)
   and o.supplier = ss.supplier
   and ol.location = vs.store(+)
   and ol.location = vw.wh(+)
   and d.dept = im.dept
   and c.dept = im.dept
   and c.class = im.class
   and s.dept = im.dept
   and s.class = im.class
   and s.subclass = im.subclass
   and d.group_no = g.group_no
/

COMMENT ON TABLE V_ORDER_SEARCH IS 'This view is used for the order search screen in RMS Alloy. It includes all fields that can be used as item search criteria.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.ORDER_NO IS 'Purchase Order Number.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.STATUS IS 'The current status of the Purchase Order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.SUPPLIER_SITE IS 'The supplier site where the Purchase Order is placed.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.SUPPLIER_SITE_NAME IS 'Translated name of the supplier site.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.SUPPLIER_PARENT IS 'Parent Supplier of the Supplier Site.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.SUPPLIER_PARENT_NAME IS 'Translated name of the Parent Supplier.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.NOT_BEFORE_DATE IS 'Contains the first date that delivery of the Purchase order will be accepted.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.NOT_AFTER_DATE IS 'Contains the last date that delivery of the Purchase order will be accepted.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.OTB_EOW_DATE IS 'Contains the OTB budget bucket the order amount should be placed into.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.WRITTEN_DATE IS 'Contains the date the Purchase order was created within the system.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.ORDER_TYPE IS 'Indicates which Open To Buy bucket would be updated.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.CURRENCY_CODE IS 'Currency of the Purchase Order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.IMPORT_ORDER_IND IS 'Indicates if the purchase order is an import order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.IMPORT_COUNTRY_ID IS 'The identifier of the country into which the items on the order are being imported.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.CONTRACT_NO IS 'Contains the contract number associated with This order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.ORIG_IND IS 'Indicates where the Purchase Order originated.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.SPLIT_REF_ORDNO IS 'This column will store the original order number from which the split orders were generated from.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.VENDOR_ORDER_NO IS 'Contains the vendor number who will provide the merchandise specified in the order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.PO_TYPE IS 'Contains the context of the Purchase Order creation.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.PROMOTION IS 'Contains the promotion number associated with the order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.BUYER IS 'Contains the number associated with the buyer for the order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.POOL_SUP_SITE IS 'This field will contain the ID for a pooled supplier site if the order was created as a pooled order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.POOL_SUP_SITE_NAME IS 'This field will contain the translated name of the pooled supplier site.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.POOL_SUP_PARENT IS 'This field will contain the parent of the pooled supplier site.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.POOL_SUP_PARENT_NAME IS 'This field will contain the translated name of the pooled supplier parent.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.ITEM IS 'The item for which the Purchase Order IS created.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.ITEM_DESC IS 'Description of the Item.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.LOCATION IS 'The location for which the Purchase Order is created.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.LOCATION_TYPE IS 'This indicates the type of location.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.PHYSICAL_LOC IS 'This contains the physical location of the location.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.DIVISION IS 'This contains the division to which the item belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.GROUP_NO IS 'This contains the group to which the item belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.DEPT IS 'This contains the department to which the item belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.CLASS IS 'This contains the class to which the item belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.SUBCLASS IS 'This contains the subclass to which the item belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.CHAIN IS 'This contains the chain to which the store location belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.AREA IS 'This contains the area to which the store location belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.REGION IS 'This contains the region to which the store location belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.DISTRICT IS 'This contains the district to which the store location belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.TSF_PO_LINK_NO IS 'Reference number to link the item on the purchase orders to transfer.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.MASTER_PO_NO IS 'This field will contain the master order number from which child records were created.'
/
