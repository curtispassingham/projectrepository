--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 S9T_TMPL_WKSHT_DEF_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'S9T_TMPL_WKSHT_DEF_TL'
CREATE TABLE S9T_TMPL_WKSHT_DEF_TL
 (TEMPLATE_KEY VARCHAR2(255 ) NOT NULL,
  WKSHT_KEY VARCHAR2(255 ) NOT NULL,
  LANG NUMBER(6) NOT NULL,
  WKSHT_NAME VARCHAR2(255 ) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE S9T_TMPL_WKSHT_DEF_TL is 'This table contains translations by language for worksheet definition contained in S9T_TMPL_WKSHT_DEF table.'
/

COMMENT ON COLUMN S9T_TMPL_WKSHT_DEF_TL.TEMPLATE_KEY is 'The template key. Maps to s9t_template.template_key.'
/

COMMENT ON COLUMN S9T_TMPL_WKSHT_DEF_TL.WKSHT_KEY is 'The worksheet key for the error. Maps to S9t_tmpl_wksht_def.wksht_key.'
/

COMMENT ON COLUMN S9T_TMPL_WKSHT_DEF_TL.LANG is 'Language'
/

COMMENT ON COLUMN S9T_TMPL_WKSHT_DEF_TL.WKSHT_NAME is 'The translated worksheet name in the LANG.'
/


PROMPT Creating Primary Key on 'S9T_TMPL_WKSHT_DEF_TL'
ALTER TABLE S9T_TMPL_WKSHT_DEF_TL
 ADD CONSTRAINT S9T_TMPL_WKSHT_DEF_TL_PK PRIMARY KEY
  (TEMPLATE_KEY,
   WKSHT_KEY,
   LANG
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'S9T_TMPL_WKSHT_DEF_TL'
 ALTER TABLE S9T_TMPL_WKSHT_DEF_TL
  ADD CONSTRAINT S9T_TMPL_WKSHT_DEF_TL_FK1
  FOREIGN KEY (TEMPLATE_KEY, WKSHT_KEY)
 REFERENCES S9T_TMPL_WKSHT_DEF (TEMPLATE_KEY, WKSHT_KEY)
/

