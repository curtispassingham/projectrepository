--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_FASHION_SKU
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_FASHION_SKU"
CREATE TABLE DC_FASHION_SKU
(
  ITEM                      VARCHAR2(25),
  PRIMARY_SKU_IND           VARCHAR2(1),
  ITEM_PARENT               VARCHAR2(25),
  ITEM_DESC                 VARCHAR2(250),
  SHORT_DESC                VARCHAR2(120),
  ITEM_DESC_SECONDARY       VARCHAR2(250),
  COST_ZONE_GROUP_ID        NUMBER(4),
  STANDARD_UOM              VARCHAR2(4),
  UOM_CONV_FACTOR           NUMBER(20,10),
  STORE_ORD_MULT            VARCHAR2(1),
  COMMENTS                  VARCHAR2(2000),
  MERCHANDISE_IND           VARCHAR2(1),
  FORECAST_IND              VARCHAR2(1),
  DIFF_1                    VARCHAR2(10),
  DIFF_2                    VARCHAR2(10),
  DIFF_3                    VARCHAR2(10),
  DIFF_4                    VARCHAR2(10),
  AIP_CASE_TYPE             VARCHAR2(6),
  PERISHABLE_IND            VARCHAR2(1),
  PRODUCT_CLASSIFICATION    VARCHAR2(6),
  BRAND_NAME                VARCHAR2(30)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_FASHION_SKU is 'This table is a staging table for data conversion and will hold style item data of ITEM_MASTER table.'
/
COMMENT ON COLUMN DC_FASHION_SKU.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_FASHION_SKU.PRIMARY_SKU_IND is 'Identify primary sku item.'
/
COMMENT ON COLUMN DC_FASHION_SKU.ITEM_PARENT is 'Alphanumeric value that uniquely identifies the item/group at the level above the item. This value must exist as an item in another row on the item_master table.'
/
COMMENT ON COLUMN DC_FASHION_SKU.ITEM_DESC is 'Long description of the item.'
/
COMMENT ON COLUMN DC_FASHION_SKU.SHORT_DESC is 'Shortened description of the item.'
/
COMMENT ON COLUMN DC_FASHION_SKU.ITEM_DESC_SECONDARY is 'Secondary descriptions of the item.'
/
COMMENT ON COLUMN DC_FASHION_SKU.COST_ZONE_GROUP_ID is 'Cost zone group associated with the item. This field is only required when elc_ind (landed cost indicator) is set to Y on the system_options table.'
/
COMMENT ON COLUMN DC_FASHION_SKU.STANDARD_UOM is 'Unit of measure in which stock of the item is tracked at a corporate level.'
/
COMMENT ON COLUMN DC_FASHION_SKU.UOM_CONV_FACTOR is 'Conversion factor between an Each and the standard_uom when the standard_uom is not in the quantity class.'
/
COMMENT ON COLUMN DC_FASHION_SKU.STORE_ORD_MULT is 'Merchandise shipped from the warehouses to the stores must be specified in this unit type. Valid values are: C = Cases I = Inner E = Eaches.'
/
COMMENT ON COLUMN DC_FASHION_SKU.COMMENTS is 'Holds any comments associated with the item.'
/
COMMENT ON COLUMN DC_FASHION_SKU.MERCHANDISE_IND is 'Indicates if the item is a merchandise item (Y, N).'
/
COMMENT ON COLUMN DC_FASHION_SKU.FORECAST_IND is 'Indicates if this item will be interfaced to an external forecasting system (Y, N).'
/
COMMENT ON COLUMN DC_FASHION_SKU.DIFF_1 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_FASHION_SKU.DIFF_2 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_FASHION_SKU.DIFF_3 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_FASHION_SKU.DIFF_4 is 'Diff_group or diff_id that differentiates the current item from its item_parent.'
/
COMMENT ON COLUMN DC_FASHION_SKU.AIP_CASE_TYPE is 'Only used if AIP is integrated. Determines which case sizes to extract against an item in the AIP interface.'
/
COMMENT ON COLUMN DC_FASHION_SKU.PERISHABLE_IND is 'A grocery item attribute used to indicate whether an item is perishable or not.'
/
COMMENT ON COLUMN DC_FASHION_SKU.PRODUCT_CLASSIFICATION is 'Product classification is informational only in RMS, but is used by RWMS to determine how to pack customer orders : such as to determine products that may not be able to be packaged together.'
/
COMMENT ON COLUMN DC_FASHION_SKU.BRAND_NAME is 'This field contains the brand associated to an item.'
/