--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--View Updated:  V_S9T_TMPL_WKSHT_DEF
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       UPDATING VIEW               
--------------------------------------
PROMPT CREATING VIEW 'V_S9T_TMPL_WKSHT_DEF';
CREATE OR REPLACE FORCE  VIEW V_S9T_TMPL_WKSHT_DEF ("TEMPLATE_KEY", "WKSHT_KEY", "WKSHT_NAME", "MANDATORY", "SEQ_NO") AS 
  SELECT wd.template_key,
    wd.wksht_key,
    NVL(wds.wksht_name,wd.wksht_name) AS wksht_name,
    wd.mandatory,
    wd.seq_no
  FROM s9t_tmpl_wksht_def wd,
    s9t_tmpl_wksht_def_tl wds
  WHERE wd.template_key = wds.template_key(+)
  AND wd.wksht_key      = wds.wksht_key(+)
  AND wds.lang (+)      = get_user_lang ;
/
