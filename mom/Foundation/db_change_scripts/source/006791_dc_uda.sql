--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_UDA
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_UDA"
CREATE TABLE DC_UDA
(
   UDA_ID              NUMBER(5),
   UDA_DESC            VARCHAR2(120),
   DISPLAY_TYPE        VARCHAR2(2),
   DATA_TYPE           VARCHAR2(12),
   DATA_LENGTH         NUMBER(3),
   SINGLE_VALUE_IND    VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_UDA is 'This table is a staging table for data conversion and will hold data for UDA table.'
/
COMMENT ON COLUMN DC_UDA.UDA_ID is 'This field contains a unique number identifying the User Defined Attribute.'
/
COMMENT ON COLUMN DC_UDA.UDA_DESC is 'This field contains a description of the User-Defined Attribute.'
/
COMMENT ON COLUMN DC_UDA.DISPLAY_TYPE is 'This field is used to store the display type (how the UDA values will be displayed to the user) for the given UDA.'
/
COMMENT ON COLUMN DC_UDA.DATA_TYPE is 'This field contains the data type of any valid values for the UDA.'
/
COMMENT ON COLUMN DC_UDA.DATA_LENGTH is 'This field contains the data length of any valid values for the UDA.'
/
COMMENT ON COLUMN DC_UDA.SINGLE_VALUE_IND is 'This field indicates whether or not the UDA should be constrained to having at most one value when assigned to a specific module.'
/