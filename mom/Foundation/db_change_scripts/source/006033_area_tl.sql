--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE ADDED:				AREA_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TYPE
--------------------------------------
PROMPT Creating Table 'AREA_TL'
CREATE TABLE AREA_TL(
LANG NUMBER(6) NOT NULL,
AREA NUMBER(10) NOT NULL,
AREA_NAME VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE AREA_TL is 'This is the translation table for AREA table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN AREA_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN AREA_TL.AREA is 'This column contains the number which uniquely identifies an area within the system.'
/

COMMENT ON COLUMN AREA_TL.AREA_NAME is 'This column contains the name of the area which, along with the area number, identifies the area.'
/

COMMENT ON COLUMN AREA_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN AREA_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN AREA_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN AREA_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE AREA_TL ADD CONSTRAINT PK_AREA_TL PRIMARY KEY (
LANG,
AREA
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE AREA_TL
 ADD CONSTRAINT ARAT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE AREA_TL ADD CONSTRAINT ARAT_ARA_FK FOREIGN KEY (
AREA
) REFERENCES AREA (
AREA
)
/

