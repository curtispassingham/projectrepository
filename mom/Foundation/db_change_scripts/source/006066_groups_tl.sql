--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       GROUPS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE GROUPS_TL(
LANG NUMBER(6) NOT NULL,
GROUP_NO NUMBER(4) NOT NULL,
GROUP_NAME VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE GROUPS_TL is 'This is the translation table for GROUPS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN GROUPS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN GROUPS_TL.GROUP_NO is 'Contains the number which uniquely identifies the group.'
/

COMMENT ON COLUMN GROUPS_TL.GROUP_NAME is 'Contains the description which, along with the group number, identifies the group.'
/

COMMENT ON COLUMN GROUPS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN GROUPS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN GROUPS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN GROUPS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE GROUPS_TL ADD CONSTRAINT PK_GROUPS_TL PRIMARY KEY (
LANG,
GROUP_NO
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE GROUPS_TL
 ADD CONSTRAINT GRPT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE GROUPS_TL ADD CONSTRAINT GRPT_GRP_FK FOREIGN KEY (
GROUP_NO
) REFERENCES GROUPS (
GROUP_NO
)
/

