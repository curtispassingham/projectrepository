--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_SEC_GROUP_LOC_MATRIX
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_SEC_GROUP_LOC_MATRIX'
CREATE TABLE SVC_SEC_GROUP_LOC_MATRIX
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  UPDATE_IND VARCHAR2(1 ),
  SELECT_IND VARCHAR2(1 ),
  WH NUMBER(10,0),
  STORE NUMBER(10,0),
  DISTRICT NUMBER(10,0),
  REGION NUMBER(10,0),
  GROUP_ID NUMBER(4,0),
  COLUMN_CODE VARCHAR2(6 ),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_SEC_GROUP_LOC_MATRIX is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in SEC_GROUP_LOC_MATRIX.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.UPDATE_IND is 'Value of this column indicates whether the user has insert, update and delete privileges.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.SELECT_IND is 'Value of this column indicates whether the user has select privileges.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.WH is 'Contains the unique number identifying the warehouse in the organisational hierarchy.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.STORE is 'Contains the unique number identifying the store in the organisational hierarchy.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.DISTRICT is 'Contains the unique number identifying the district in the organisational hierarchy.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.REGION is 'Contains the unique number identifying the region in the organisational hierarchy.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.GROUP_ID is 'Contains the unique identifier associated with the group.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.COLUMN_CODE is 'Contains a 6 digit code which identifies the functional area to which the security applies.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.CREATE_ID is 'User who created the record.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.CREATE_DATETIME is 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.LAST_UPD_ID is 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_SEC_GROUP_LOC_MATRIX.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_SEC_GROUP_LOC_MATRIX'
ALTER TABLE SVC_SEC_GROUP_LOC_MATRIX
 ADD CONSTRAINT SVC_SEC_GROUP_LOC_MATRIX_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_SEC_GROUP_LOC_MATRIX'
ALTER TABLE SVC_SEC_GROUP_LOC_MATRIX
 ADD CONSTRAINT SVC_SEC_GROUP_LOC_MATRIX_UK UNIQUE
  (GROUP_ID,
   COLUMN_CODE,
   WH,
   STORE,
   DISTRICT,
   REGION
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

