--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--View Updated:  V_S9T_TMPL_WKSHT_COMP
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       UPDATING VIEW               
--------------------------------------
PROMPT CREATING VIEW 'V_S9T_TMPL_WKSHT_COMP';
CREATE OR REPLACE FORCE  VIEW V_S9T_TMPL_WKSHT_COMP ("TEMPLATE_KEY", "WKSHT_KEY", "WKSHT_NAME", "MANDATORY", "SEQ_NO","LANG") AS 
  SELECT wd.template_key,
    wd.wksht_key,
    NVL(wds.wksht_name,wd.wksht_name) AS wksht_name,
    wd.mandatory,
    wd.seq_no,
    NVL(wds.lang,get_user_lang) AS lang
  FROM s9t_tmpl_wksht_def wd,
    s9t_tmpl_wksht_def_tl wds
  WHERE wd.template_key = wds.template_key(+)
  AND wd.wksht_key      = wds.wksht_key(+);
/
