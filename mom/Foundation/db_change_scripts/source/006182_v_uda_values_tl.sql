CREATE OR REPLACE FORCE VIEW V_UDA_VALUES_TL (UDA_ID, UDA_VALUE, UDA_VALUE_DESC, LANG ) AS
SELECT  b.uda_id,
        b.uda_value,
        case when tl.lang is not null then tl.uda_value_desc else b.uda_value_desc end uda_value_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  UDA_VALUES b,
        UDA_VALUES_TL tl
 WHERE  b.uda_id = tl.uda_id (+)
   AND  b.uda_value = tl.uda_value (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_UDA_VALUES_TL is 'This is the translation view for base table UDA_VALUES. This view fetches data in user langauge either from translation table UDA_VALUES_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_UDA_VALUES_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_UDA_VALUES_TL.UDA_ID is 'This field contains a unique number identifying the User Defined Attribute.'
/

COMMENT ON COLUMN V_UDA_VALUES_TL.UDA_VALUE is 'This field contains a unique number identifying the User Defined Attribute value for the UDA. A UDA can have multiple values. For example, Color can be a UDA and it can have different values like Green, Red, Blue, etc.'
/

COMMENT ON COLUMN V_UDA_VALUES_TL.UDA_VALUE_DESC is 'This field contains a description of the UDA value.'
/

