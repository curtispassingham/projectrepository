--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       PRIORITY_GROUP_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE PRIORITY_GROUP_TL(
LANG NUMBER(6) NOT NULL,
PRIORITY_GROUP_ID NUMBER(4) NOT NULL,
PRIORITY_GROUP_DESC VARCHAR2(100) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE PRIORITY_GROUP_TL is 'This is the translation table for PRIORITY_GROUP table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN PRIORITY_GROUP_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN PRIORITY_GROUP_TL.PRIORITY_GROUP_ID is 'System generated numeric Column. It is the primary key of the table. It will be unique for each priority. This is a non-editable column.'
/

COMMENT ON COLUMN PRIORITY_GROUP_TL.PRIORITY_GROUP_DESC is 'User entered description for the priority group. Description can be changed'
/

COMMENT ON COLUMN PRIORITY_GROUP_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN PRIORITY_GROUP_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN PRIORITY_GROUP_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN PRIORITY_GROUP_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE PRIORITY_GROUP_TL ADD CONSTRAINT PK_PRIORITY_GROUP_TL PRIMARY KEY (
LANG,
PRIORITY_GROUP_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE PRIORITY_GROUP_TL
 ADD CONSTRAINT PGT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE PRIORITY_GROUP_TL ADD CONSTRAINT PGT_PG_FK FOREIGN KEY (
PRIORITY_GROUP_ID
) REFERENCES PRIORITY_GROUP (
PRIORITY_GROUP_ID
)
/

