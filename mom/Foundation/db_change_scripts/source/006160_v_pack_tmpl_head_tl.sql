CREATE OR REPLACE FORCE VIEW V_PACK_TMPL_HEAD_TL (PACK_TMPL_ID, PACK_TMPL_DESC, LANG ) AS
SELECT  b.pack_tmpl_id,
        case when tl.lang is not null then tl.pack_tmpl_desc else b.pack_tmpl_desc end pack_tmpl_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  PACK_TMPL_HEAD b,
        PACK_TMPL_HEAD_TL tl
 WHERE  b.pack_tmpl_id = tl.pack_tmpl_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_PACK_TMPL_HEAD_TL is 'This is the translation view for base table PACK_TMPL_HEAD. This view fetches data in user langauge either from translation table PACK_TMPL_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_PACK_TMPL_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_PACK_TMPL_HEAD_TL.PACK_TMPL_ID is 'This field contains the unique identification code for a pack template.'
/

COMMENT ON COLUMN V_PACK_TMPL_HEAD_TL.PACK_TMPL_DESC is 'This field contains a description of the pack template.'
/

