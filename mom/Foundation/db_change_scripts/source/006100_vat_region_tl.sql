--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       VAT_REGION_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE VAT_REGION_TL(
LANG NUMBER(6) NOT NULL,
VAT_REGION NUMBER(4) NOT NULL,
VAT_REGION_NAME VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE VAT_REGION_TL is 'This is the translation table for VAT_REGION table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN VAT_REGION_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN VAT_REGION_TL.VAT_REGION is 'Contains the unique identifying number for the VAT region in the system.'
/

COMMENT ON COLUMN VAT_REGION_TL.VAT_REGION_NAME is 'Contains the name associated with the VAT region.'
/

COMMENT ON COLUMN VAT_REGION_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN VAT_REGION_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN VAT_REGION_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN VAT_REGION_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE VAT_REGION_TL ADD CONSTRAINT PK_VAT_REGION_TL PRIMARY KEY (
LANG,
VAT_REGION
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE VAT_REGION_TL
 ADD CONSTRAINT VRT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE VAT_REGION_TL ADD CONSTRAINT VRT_VR_FK FOREIGN KEY (
VAT_REGION
) REFERENCES VAT_REGION (
VAT_REGION
)
/

