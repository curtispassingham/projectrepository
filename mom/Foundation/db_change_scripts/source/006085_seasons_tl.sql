--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       SEASONS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE SEASONS_TL(
LANG NUMBER(6) NOT NULL,
SEASON_ID NUMBER(3) NOT NULL,
SEASON_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SEASONS_TL is 'This is the translation table for SEASONS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN SEASONS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SEASONS_TL.SEASON_ID is 'This field contains the unique identifier for season.'
/

COMMENT ON COLUMN SEASONS_TL.SEASON_DESC is 'This field contains the description associated with the season.'
/

COMMENT ON COLUMN SEASONS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN SEASONS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN SEASONS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN SEASONS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE SEASONS_TL ADD CONSTRAINT PK_SEASONS_TL PRIMARY KEY (
LANG,
SEASON_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SEASONS_TL
 ADD CONSTRAINT SENT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE SEASONS_TL ADD CONSTRAINT SENT_SEN_FK FOREIGN KEY (
SEASON_ID
) REFERENCES SEASONS (
SEASON_ID
)
/

