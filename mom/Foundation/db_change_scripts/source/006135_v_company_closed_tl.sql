CREATE OR REPLACE FORCE VIEW V_COMPANY_CLOSED_TL (CLOSE_DATE, CLOSE_DESC, LANG ) AS
SELECT  b.close_date,
        case when tl.lang is not null then tl.close_desc else b.close_desc end close_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  COMPANY_CLOSED b,
        COMPANY_CLOSED_TL tl
 WHERE  b.close_date = tl.close_date (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_COMPANY_CLOSED_TL is 'This is the translation view for base table COMPANY_CLOSED. This view fetches data in user langauge either from translation table COMPANY_CLOSED_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_COMPANY_CLOSED_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_COMPANY_CLOSED_TL.CLOSE_DATE is 'This field contains the date that the company is closed.'
/

COMMENT ON COLUMN V_COMPANY_CLOSED_TL.CLOSE_DESC is 'This field contains a description of the close.'
/

