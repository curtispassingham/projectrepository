--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_ITEM_SUPP_MANU_COUNTRY
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_ITEM_SUPP_MANU_COUNTRY"
CREATE TABLE DC_ITEM_SUPP_MANU_COUNTRY
(
   ITEM                          VARCHAR2(25),
   SUPPLIER                      NUMBER(10),
   MANU_COUNTRY_ID               VARCHAR2(3),
   PRIMARY_MANU_CTRY_IND         VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_ITEM_SUPP_MANU_COUNTRY is 'This table is a staging table for data conversion and will hold data of ITEM_SUPP_MANU_COUNTRY.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_MANU_COUNTRY.ITEM is 'Alphanumeric value that identifies either the item or item parent.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_MANU_COUNTRY.SUPPLIER is 'The unique identifier for the supplier.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID is 'The country where the item was manufactured.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_MANU_COUNTRY.PRIMARY_MANU_CTRY_IND is 'This field indicates whether this country is the primary country of manufacture for the item/supplier.'
/