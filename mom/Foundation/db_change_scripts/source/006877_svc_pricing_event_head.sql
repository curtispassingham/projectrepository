--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_PRICING_EVENT_HEAD'
ALTER TABLE SVC_PRICING_EVENT_HEAD MODIFY EVENT_ID NUMBER (15,0)
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.EVENT_ID is 'Event Id for the Price Change Event.'
/

ALTER TABLE SVC_PRICING_EVENT_HEAD MODIFY CURRENCY_CODE VARCHAR2 (3)
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.CURRENCY_CODE is 'Currency code for the Price Change Event.'
/

COMMENT ON COLUMN SVC_PRICING_EVENT_HEAD.HIER_LEVEL is 'This field indicates the organization hierarchy level.'
/

