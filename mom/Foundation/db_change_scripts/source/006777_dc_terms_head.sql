--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_TERMS_HEAD
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_TERMS_HEAD"
CREATE TABLE DC_TERMS_HEAD
(
   TERMS                     VARCHAR2(15),
   TERMS_CODE                VARCHAR2(50),
   TERMS_DESC                VARCHAR2(240),
   RANK                      NUMBER(10)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_TERMS_HEAD is 'This table is a staging table for data conversion and will hold data for TERM_HEAD table.'
/
COMMENT ON COLUMN DC_TERMS_HEAD.TERMS is 'Contains a number uniquely identifying the supplier terms.'
/
COMMENT ON COLUMN DC_TERMS_HEAD.TERMS_CODE is 'Indicates the Alphanumeric representation of Term Name which acts as the Term code in Oracle Financials.'
/
COMMENT ON COLUMN DC_TERMS_HEAD.TERMS_DESC is 'Contains a description of the supplier terms. For example: 2.5% 30 days.'
/
COMMENT ON COLUMN DC_TERMS_HEAD.RANK is 'Unique rank to rate invoice payment terms against purchase order terms.'
/