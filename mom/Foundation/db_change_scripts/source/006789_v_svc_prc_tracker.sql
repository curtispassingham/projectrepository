--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW CREATE:           V_SVC_PRC_TRACKER
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT CREATING View V_SVC_PRC_TRACKER
CREATE OR REPLACE FORCE VIEW V_SVC_PRC_TRACKER
 (PROCESS_ID
 ,PROCESS_DESC
 ,FILE_ID
 ,FILE_NAME
 ,TEMPLATE_TYPE
 ,TEMPLATE_KEY
 ,TEMPLATE_NAME
 ,TEMPLATE_CATEGORY
 ,ACTION_TYPE
 ,PROCESS_SOURCE
 ,PROCESS_SOURCE_DESC
 ,PROCESS_DESTINATION
 ,PROCESS_DESTINATION_DESC
 ,ACTION_DATE
 ,STATUS
 ,STATUS_DESC
 ,USER_ID
 ,FILE_PATH
 ,MODULE_TYPE)
 AS SELECT SPT.PROCESS_ID PROCESS_ID,
    SPT.PROCESS_DESC PROCESS_DESC,
    SPT.FILE_ID FILE_ID,
    SF.FILE_NAME FILE_NAME,
    ST.TEMPLATE_TYPE TEMPLATE_TYPE,
    SPT.TEMPLATE_KEY TEMPLATE_KEY,
    NVL(STS.TEMPLATE_NAME,ST.TEMPLATE_NAME) AS TEMPLATE_NAME,
    ST.TEMPLATE_CATEGORY AS TEMPLATE_CATEGORY,
    SPT.ACTION_TYPE ACTION_TYPE,
    SPT.PROCESS_SOURCE PROCESS_SOURCE,
    SRC.CODE_DESC AS PROCESS_SOURCE_DESC,
    SPT.PROCESS_DESTINATION PROCESS_DESTINATION,
    DST.CODE_DESC AS PROCESS_DESTINATION_DESC,
    SPT.ACTION_DATE ACTION_DATE,
    SPT.STATUS STATUS,
    STATUS_CODE.CODE_DESC AS STATUS_DESC,
    USER_ID,
    SPT.FILE_PATH FILE_PATH,
    SPT.MODULE_TYPE
  FROM SVC_PROCESS_TRACKER SPT,
    S9T_FOLDER SF,
    S9T_TEMPLATE ST,
    V_S9T_TEMPLATE STS,
    V_CODE_DETAIL_TL SRC,
    V_CODE_DETAIL_TL DST,
    V_CODE_DETAIL_TL STATUS_CODE
  WHERE SPT.TEMPLATE_KEY        = ST.TEMPLATE_KEY(+)
  AND ST.TEMPLATE_KEY           = STS.TEMPLATE_KEY(+)
  AND SPT.FILE_ID               = SF.FILE_ID (+)
  AND SRC.CODE_TYPE             = 'IISL'
  AND SRC.CODE                  = SPT.PROCESS_SOURCE
  AND DST.CODE_TYPE             = 'IISL'
  AND DST.CODE                  = SPT.PROCESS_DESTINATION
  AND STATUS_CODE.CODE_TYPE (+) = 'IIPS'
  AND STATUS_CODE.CODE (+)      = SPT.STATUS;
/
