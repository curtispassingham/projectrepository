CREATE OR REPLACE FORCE VIEW V_CUSTOMER_SEGMENTS_TL (CUSTOMER_SEGMENT_ID, CUSTOMER_SEGMENT_DESC, LANG ) AS
SELECT  b.customer_segment_id,
        case when tl.lang is not null then tl.customer_segment_desc else b.customer_segment_desc end customer_segment_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  CUSTOMER_SEGMENTS b,
        CUSTOMER_SEGMENTS_TL tl
 WHERE  b.customer_segment_id = tl.customer_segment_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_CUSTOMER_SEGMENTS_TL is 'This is the translation view for base table CUSTOMER_SEGMENTS. This view fetches data in user langauge either from translation table CUSTOMER_SEGMENTS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_CUSTOMER_SEGMENTS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_CUSTOMER_SEGMENTS_TL.CUSTOMER_SEGMENT_ID is 'The customer segment id of a given customer segment type, referenced and attached to promotions in RPM. They will be available in RPM promotion UI screen for customer segment promotions.'
/

COMMENT ON COLUMN V_CUSTOMER_SEGMENTS_TL.CUSTOMER_SEGMENT_DESC is 'The description of customer segment id.'
/

