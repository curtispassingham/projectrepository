--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_VAT_CODES_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

PROMPT CREATING TABLE 'SVC_VAT_CODES_TL'
CREATE TABLE SVC_VAT_CODES_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, VAT_CODE VARCHAR2(6)
, VAT_CODE_DESC VARCHAR2(120)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_VAT_CODES_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in VAT_CODES_TL.'
/

COMMENT ON COLUMN SVC_VAT_CODES_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_VAT_CODES_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_VAT_CODES_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_VAT_CODES_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_VAT_CODES_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_VAT_CODES_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_VAT_CODES_TL.VAT_CODE is 'This field contains the alphanumeric identification for the VAT code. Valid values include: S - Standard C - Composite Z - Zero E - Exempt Other values may also be entered. These are the default VAT Rates that are set-up upon installation of the RMS.'
/

COMMENT ON COLUMN SVC_VAT_CODES_TL.VAT_CODE_DESC is 'Contains a description identifying the VAT code.'
/

PROMPT CREATING PRIMARY KEY ON 'SVC_VAT_CODES_TL'
ALTER TABLE SVC_VAT_CODES_TL
ADD CONSTRAINT SVC_VAT_CODES_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

PROMPT CREATING UNIQUE KEY ON 'SVC_VAT_CODES_TL'
ALTER TABLE SVC_VAT_CODES_TL
ADD CONSTRAINT SVC_VAT_CODES_TL_UK UNIQUE
(LANG, VAT_CODE)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

