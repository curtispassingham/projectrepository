--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 DIFF_TYPE_CFA_EXT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'DIFF_TYPE_CFA_EXT'
CREATE TABLE DIFF_TYPE_CFA_EXT
 (DIFF_TYPE VARCHAR2(6 ) NOT NULL,
  GROUP_ID NUMBER(10) NOT NULL,
  VARCHAR2_1 VARCHAR2(250 ),
  VARCHAR2_2 VARCHAR2(250 ),
  VARCHAR2_3 VARCHAR2(250 ),
  VARCHAR2_4 VARCHAR2(250 ),
  VARCHAR2_5 VARCHAR2(250 ),
  VARCHAR2_6 VARCHAR2(250 ),
  VARCHAR2_7 VARCHAR2(250 ),
  VARCHAR2_8 VARCHAR2(250 ),
  VARCHAR2_9 VARCHAR2(250 ),
  VARCHAR2_10 VARCHAR2(250 ),
  NUMBER_11 NUMBER,
  NUMBER_12 NUMBER,
  NUMBER_13 NUMBER,
  NUMBER_14 NUMBER,
  NUMBER_15 NUMBER,
  NUMBER_16 NUMBER,
  NUMBER_17 NUMBER,
  NUMBER_18 NUMBER,
  NUMBER_19 NUMBER,
  NUMBER_20 NUMBER,
  DATE_21 DATE,
  DATE_22 DATE,
  DATE_23 DATE,
  DATE_24 DATE,
  DATE_25 DATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE DIFF_TYPE_CFA_EXT is 'This is the custom attribute extension table for the entity DIFF_TYPE.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.DIFF_TYPE is 'Contains the code used to uniquely identify a differentiator type.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.GROUP_ID is 'This column holds the attribute group id that this extended data is associated with.  The logical business meaning of the VARCHAR_ , NUMBER_ and DATE_ columns on this table are determined by the metadata defined for this attribute.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.VARCHAR2_1 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_1 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.VARCHAR2_2 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_2 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.VARCHAR2_3 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_3 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.VARCHAR2_4 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references  VARCHAR2_4 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.VARCHAR2_5 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references  VARCHAR2_5 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.VARCHAR2_6 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references  VARCHAR2_6 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.VARCHAR2_7 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references  VARCHAR2_7 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.VARCHAR2_8 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_8 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.VARCHAR2_9 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_9 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.VARCHAR2_10 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_10 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.NUMBER_11 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_11 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.NUMBER_12 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_12 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.NUMBER_13 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_13 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.NUMBER_14 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_14 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.NUMBER_15 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_15 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.NUMBER_16 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_16 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.NUMBER_17 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_17 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.NUMBER_18 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_18 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.NUMBER_19 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_19 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.NUMBER_20 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_20 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.DATE_21 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_21 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.DATE_22 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_22 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.DATE_23 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_22 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.DATE_24 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN DIFF_TYPE_CFA_EXT.DATE_25 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/


PROMPT Creating Primary Key on 'DIFF_TYPE_CFA_EXT'
ALTER TABLE DIFF_TYPE_CFA_EXT
 ADD CONSTRAINT PK_DIFF_TYPE_CFA_EXT PRIMARY KEY
  (DIFF_TYPE,
   GROUP_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'DIFF_TYPE_CFA_EXT'
 ALTER TABLE DIFF_TYPE_CFA_EXT
  ADD CONSTRAINT DTE_DIF_FK
  FOREIGN KEY (DIFF_TYPE)
 REFERENCES DIFF_TYPE (DIFF_TYPE)
/

