--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_GROUPS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_GROUPS'
CREATE OR REPLACE FORCE VIEW V_GROUPS
 (DIVISION
 ,GROUP_NO
 ,GROUP_NAME)
 AS SELECT GRO.DIVISION DIVISION
          ,GRO.GROUP_NO GROUP_NO
          ,V.GROUP_NAME GROUP_NAME
FROM GROUPS GRO,
     V_GROUPS_TL V
WHERE GRO.GROUP_NO = V.GROUP_NO     
/


COMMENT ON TABLE V_GROUPS IS 'This view will be used to display the Merchandising Group LOVs using a security policy to filter User access. The group name is translated based on user language.'
/

COMMENT ON COLUMN V_GROUPS."DIVISION" IS 'Contains the number of the division of which the group is a member.'
/

COMMENT ON COLUMN V_GROUPS."GROUP_NO" IS 'Contains the number which uniquely identifies the group.'
/

