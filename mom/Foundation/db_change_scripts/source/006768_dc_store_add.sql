--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_STORE_ADD
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_STORE_ADD"
CREATE TABLE DC_STORE_ADD
(
   STORE                      NUMBER(10),
   STORE_NAME                 VARCHAR2(150),
   STORE_NAME10               VARCHAR2(10),
   STORE_NAME3                VARCHAR2(3),
   STORE_CLASS                VARCHAR2(1),
   STORE_MGR_NAME             VARCHAR2(120),
   STORE_OPEN_DATE            DATE,
   STOCKHOLDING_IND           VARCHAR2(1),
   DISTRICT                   NUMBER(10),
   START_ORDER_DAYS           NUMBER(3),
   CURRENCY_CODE              VARCHAR2(3),
   LANG                       NUMBER(6),
   COPY_REPL_IND              VARCHAR2(1),
   TRAN_NO_GENERATED          VARCHAR2(6),
   INTEGRATED_POS_IND         VARCHAR2(1),
   COPY_ACTIVITY_IND          VARCHAR2(1),
   COPY_DLVRY_IND             VARCHAR2(1),
   STORE_NAME_SECONDARY       VARCHAR2(150),
   STORE_CLOSE_DATE           DATE,
   ACQUIRED_DATE              DATE,
   REMODEL_DATE               DATE,
   FAX_NUMBER                 VARCHAR2(20),
   PHONE_NUMBER               VARCHAR2(20),
   EMAIL                      VARCHAR2(100),
   TOTAL_SQUARE_FT            NUMBER(8),
   SELLING_SQUARE_FT          NUMBER(8),
   LINEAR_DISTANCE            NUMBER(8),
   VAT_REGION                 NUMBER(4),
   VAT_INCLUDE_IND            VARCHAR2(1),
   CHANNEL_ID                 NUMBER(4),
   STORE_FORMAT               NUMBER(4),
   MALL_NAME                  VARCHAR2(120),
   TRANSFER_ZONE              NUMBER(4),
   DEFAULT_WH                 NUMBER(10),
   STOP_ORDER_DAYS            NUMBER(3),
   DUNS_NUMBER                VARCHAR2(9),
   DUNS_LOC                   VARCHAR2(4),
   SISTER_STORE               NUMBER(10),
   TSF_ENTITY_ID              NUMBER(10),
   ORG_UNIT_ID                NUMBER(15),
   STORE_TYPE                 VARCHAR2(6),
   WF_CUSTOMER_ID             NUMBER(10),
   AUTO_APPROVE_ORDERS_IND    VARCHAR2(1),
   TIMEZONE_NAME              VARCHAR2(64),
   CUSTOMER_ORDER_LOC_IND     VARCHAR2(1),
   PROCESS_MODE               VARCHAR2(5),
   PROCESS_STATUS             VARCHAR2(25)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_STORE_ADD is 'This table is a staging table for data conversion and will hold one row for each new store to be created in the merchandising system.'
/
COMMENT ON COLUMN DC_STORE_ADD.STORE is 'Contains the number which uniquely identifies the store.'
/
COMMENT ON COLUMN DC_STORE_ADD.STORE_NAME is 'Contains the name of the store which, along  with the store number, identifies the store.'
/
COMMENT ON COLUMN DC_STORE_ADD.STORE_NAME10 is 'Contains a ten character abbreviation of the store name.'
/
COMMENT ON COLUMN DC_STORE_ADD.STORE_NAME3 is 'Contains a three character abbreviation of the store name.'
/
COMMENT ON COLUMN DC_STORE_ADD.STORE_CLASS is 'Contains the code letter indicating the class of which the store is a member.'
/
COMMENT ON COLUMN DC_STORE_ADD.STORE_MGR_NAME is 'Contains the name of the store manager.'
/
COMMENT ON COLUMN DC_STORE_ADD.STORE_OPEN_DATE is 'Contains the date on which the store opened.'
/
COMMENT ON COLUMN DC_STORE_ADD.STOCKHOLDING_IND is 'This column indicates whether the store can hold stock.'
/
COMMENT ON COLUMN DC_STORE_ADD.DISTRICT is 'Contains the number of the district in which the store is a member.'
/
COMMENT ON COLUMN DC_STORE_ADD.START_ORDER_DAYS is 'Contains the number of days before the store_open_date that the store will begin accepting orders.'
/
COMMENT ON COLUMN DC_STORE_ADD.CURRENCY_CODE is 'This field contains the currency code under which the store operates.'
/
COMMENT ON COLUMN DC_STORE_ADD.LANG is 'This column identifies the language to be used for the given store.'
/
COMMENT ON COLUMN DC_STORE_ADD.COPY_REPL_IND is 'Indicator to determine if replenishment info should be copied to the new store.'
/
COMMENT ON COLUMN DC_STORE_ADD.TRAN_NO_GENERATED is 'Contains the level at which unique POS transaction numbers are generated.'
/
COMMENT ON COLUMN DC_STORE_ADD.INTEGRATED_POS_IND is 'Indicates whether or not the POS system at the Store is integrated.'
/
COMMENT ON COLUMN DC_STORE_ADD.COPY_ACTIVITY_IND is 'This field indicates if the like stores closing date schedule should be copied in the creation of a new store based on a like store.'
/
COMMENT ON COLUMN DC_STORE_ADD.COPY_DLVRY_IND is 'This field indicates if the like stores delivery schedule should be copied in the creation of a new store based on a like store.'
/
COMMENT ON COLUMN DC_STORE_ADD.STORE_NAME_SECONDARY is 'Secondary name of the store.'
/
COMMENT ON COLUMN DC_STORE_ADD.STORE_CLOSE_DATE is 'Contains the date on which the store closed.'
/
COMMENT ON COLUMN DC_STORE_ADD.ACQUIRED_DATE is 'Contains the date on which the store was acquired.'
/
COMMENT ON COLUMN DC_STORE_ADD.REMODEL_DATE is 'Contains the date on which the store was last remodeled.'
/
COMMENT ON COLUMN DC_STORE_ADD.FAX_NUMBER is 'Contains the fax number for the store.'
/
COMMENT ON COLUMN DC_STORE_ADD.PHONE_NUMBER is 'Contains the phone number for the store.'
/
COMMENT ON COLUMN DC_STORE_ADD.EMAIL is 'Holds the email address for the location'
/
COMMENT ON COLUMN DC_STORE_ADD.TOTAL_SQUARE_FT is 'Contains the total square footage of the store.'
/
COMMENT ON COLUMN DC_STORE_ADD.SELLING_SQUARE_FT is 'Contains the total square footage of the stores selling area.'
/
COMMENT ON COLUMN DC_STORE_ADD.LINEAR_DISTANCE is 'Holds the total merchandisable space of the location.'
/
COMMENT ON COLUMN DC_STORE_ADD.VAT_REGION is 'Contains the number of the Value Added Tax region in which this store is contained.'
/
COMMENT ON COLUMN DC_STORE_ADD.VAT_INCLUDE_IND is 'Indicates whether or not Value Added Tax will be included in the retail prices for the store.'
/
COMMENT ON COLUMN DC_STORE_ADD.CHANNEL_ID is 'In a multichannel environment this will contain the channel with which the store is associated.'
/
COMMENT ON COLUMN DC_STORE_ADD.STORE_FORMAT is 'Contains the number indicating the format of the store.'
/
COMMENT ON COLUMN DC_STORE_ADD.MALL_NAME is 'Contains the name of the mall in which the store is located.'
/
COMMENT ON COLUMN DC_STORE_ADD.TRANSFER_ZONE is 'Contains the transfer zone in which the store is located.'
/
COMMENT ON COLUMN DC_STORE_ADD.DEFAULT_WH is 'Contains the number of the warehouse that may be used as the default for creating crossdock masks.'
/
COMMENT ON COLUMN DC_STORE_ADD.STOP_ORDER_DAYS is 'Contains the number of days before a store closing that the store will stop accepting orders.'
/
COMMENT ON COLUMN DC_STORE_ADD.DUNS_NUMBER is 'This field holds the Dun and Bradstreet number to identify the store.'
/
COMMENT ON COLUMN DC_STORE_ADD.DUNS_LOC is 'This field holds the Dun and Bradstreet number to identify the location'
/
COMMENT ON COLUMN DC_STORE_ADD.SISTER_STORE is 'This field will hold a store number which will be used to relate the current store to the historical data of an existing store.'
/
COMMENT ON COLUMN DC_STORE_ADD.TSF_ENTITY_ID is 'Transfer Entity ID'
/
COMMENT ON COLUMN DC_STORE_ADD.ORG_UNIT_ID is 'Column will contain the organizational unit ID value.'
/
COMMENT ON COLUMN DC_STORE_ADD.STORE_TYPE is 'This will indicate whether a particular store is a franchise or company store.'
/
COMMENT ON COLUMN DC_STORE_ADD.WF_CUSTOMER_ID is 'Numeric Id of the customer.'
/
COMMENT ON COLUMN DC_STORE_ADD.AUTO_APPROVE_ORDERS_IND is 'This column will indicate whether the client is allowing automatic receipt for the store. Valid values are Y (Yes), N (No), D (System Default). Default value is D.'
/
COMMENT ON COLUMN DC_STORE_ADD.TIMEZONE_NAME is 'Indicates the time zone of the store.'
/
COMMENT ON COLUMN DC_STORE_ADD.CUSTOMER_ORDER_LOC_IND is 'This Column determines whether the location is customer order location or not.'
/
COMMENT ON COLUMN DC_STORE_ADD.PROCESS_MODE is 'The mode (ASYNC or BATCH) in which the like-store part of this store needs to be processed.'
/
COMMENT ON COLUMN DC_STORE_ADD.PROCESS_STATUS is 'The status of this store add process.'
/