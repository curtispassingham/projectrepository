--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

---------------------------------------------------------------------------
-- Name:    GET_PRIMARY_ITEM_SUPPLIER
-- Purpose: This function is used as a function based index for ITEM_MASTER.
--
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION GET_PRIMARY_ITEM_SUPPLIER(I_item   IN   ITEM_MASTER.ITEM%TYPE)
RETURN NUMBER DETERMINISTIC
IS 

   L_supplier        ITEM_SUPPLIER.SUPPLIER%TYPE := NULL;
   
      cursor C_GET_SUPPLIER is
         select supplier
           from item_supplier
          where item = I_item
         and primary_supp_ind = 'Y';
BEGIN
   open C_GET_SUPPLIER;
   fetch C_GET_SUPPLIER into L_supplier;
   close C_GET_SUPPLIER;
   
   
   return L_supplier;
   
EXCEPTION
   -- ignore errors because a null will be returned
   when OTHERS then
      NULL;
END;
/

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT CREATING INDEX 'ITEM_MASTER_I14'
CREATE INDEX ITEM_MASTER_I14 ON ITEM_MASTER(GET_PRIMARY_ITEM_SUPPLIER(ITEM))
/
