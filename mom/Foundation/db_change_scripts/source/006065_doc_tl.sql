--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       DOC_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE DOC_TL(
LANG NUMBER(6) NOT NULL,
DOC_ID NUMBER(6) NOT NULL,
DOC_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE DOC_TL is 'This is the translation table for DOC table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN DOC_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN DOC_TL.DOC_ID is 'An assigned sequence number to distinguish between the different documents.'
/

COMMENT ON COLUMN DOC_TL.DOC_DESC is 'A full description or name of the document type.'
/

COMMENT ON COLUMN DOC_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN DOC_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN DOC_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN DOC_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE DOC_TL ADD CONSTRAINT PK_DOC_TL UNIQUE (
LANG,
DOC_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE DOC_TL
 ADD CONSTRAINT DOCT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE DOC_TL ADD CONSTRAINT DOCT_DOC_FK FOREIGN KEY (
DOC_ID
) REFERENCES DOC (
DOC_ID
)
/

