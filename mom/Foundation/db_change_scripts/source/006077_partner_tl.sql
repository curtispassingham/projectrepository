--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       PARTNER_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE PARTNER_TL(
LANG NUMBER(6) NOT NULL,
PARTNER_TYPE VARCHAR2(6) NOT NULL,
PARTNER_ID VARCHAR2(10) NOT NULL,
PARTNER_DESC VARCHAR2(240) NOT NULL,
PARTNER_NAME_SECONDARY VARCHAR2(240) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE PARTNER_TL is 'This is the translation table for PARTNER table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN PARTNER_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN PARTNER_TL.PARTNER_TYPE is 'Specifies the type of partner.  Valid values are Bank BK, Agent AG, Freight Forwarder FF, Importer IM, Broker BR, Factory FA, Applicant AP, Consolidator CO, and Consignee CN, Supplier hierarchy level 1 S1, Supplier hierarchy level 2 S2, Supplier hierarchy level 3 S3.'
/

COMMENT ON COLUMN PARTNER_TL.PARTNER_ID is 'Unique identifying number for a partner within the system.  The user determines this number when a new partner is first added to the system.'
/

COMMENT ON COLUMN PARTNER_TL.PARTNER_DESC is 'Contains the partners description or name.'
/

COMMENT ON COLUMN PARTNER_TL.PARTNER_NAME_SECONDARY is 'This wil hold the secondary name of the partner.'
/

COMMENT ON COLUMN PARTNER_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN PARTNER_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN PARTNER_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN PARTNER_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE PARTNER_TL ADD CONSTRAINT PK_PARTNER_TL PRIMARY KEY (
LANG,
PARTNER_TYPE,
PARTNER_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE PARTNER_TL
 ADD CONSTRAINT PTNRT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE PARTNER_TL ADD CONSTRAINT PTNRT_PTNR_FK FOREIGN KEY (
PARTNER_TYPE,
PARTNER_ID
) REFERENCES PARTNER (
PARTNER_TYPE,
PARTNER_ID
)
/

