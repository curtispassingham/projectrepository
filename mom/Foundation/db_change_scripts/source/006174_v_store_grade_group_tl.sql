CREATE OR REPLACE FORCE VIEW V_STORE_GRADE_GROUP_TL (STORE_GRADE_GROUP_ID, STORE_GRADE_GROUP_DESC, LANG ) AS
SELECT  b.store_grade_group_id,
        case when tl.lang is not null then tl.store_grade_group_desc else b.store_grade_group_desc end store_grade_group_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  STORE_GRADE_GROUP b,
        STORE_GRADE_GROUP_TL tl
 WHERE  b.store_grade_group_id = tl.store_grade_group_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_STORE_GRADE_GROUP_TL is 'This is the translation view for base table STORE_GRADE_GROUP. This view fetches data in user langauge either from translation table STORE_GRADE_GROUP_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_STORE_GRADE_GROUP_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_STORE_GRADE_GROUP_TL.STORE_GRADE_GROUP_ID is 'This column holds the unique store grade group identification number.  The id is a unique system generated number.'
/

COMMENT ON COLUMN V_STORE_GRADE_GROUP_TL.STORE_GRADE_GROUP_DESC is 'This column will hold the store group description associated with the identification number.'
/

