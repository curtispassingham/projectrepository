--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW MODIFIED:          V_ITEM_XFORM_DETAIL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_ITEM_XFORM_DETAIL'
CREATE OR REPLACE FORCE VIEW V_ITEM_XFORM_DETAIL
 AS select ixd.*
      from item_xform_detail ixd
     where exists (select 1 
                     from v_item_master vim 
                    where ixd.detail_item = vim.item)
/

COMMENT ON TABLE V_ITEM_XFORM_DETAIL IS 'This view extracts the transformation detail record for the transformed orderable items.'
/

