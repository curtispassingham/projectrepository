CREATE OR REPLACE FORCE VIEW V_STORE_FORMAT_TL (STORE_FORMAT, FORMAT_NAME, LANG ) AS
SELECT  b.store_format,
        case when tl.lang is not null then tl.format_name else b.format_name end format_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  STORE_FORMAT b,
        STORE_FORMAT_TL tl
 WHERE  b.store_format = tl.store_format (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_STORE_FORMAT_TL is 'This is the translation view for base table STORE_FORMAT. This view fetches data in user langauge either from translation table STORE_FORMAT_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_STORE_FORMAT_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_STORE_FORMAT_TL.STORE_FORMAT is 'Contains the number which uniquely identifies the store format.'
/

COMMENT ON COLUMN V_STORE_FORMAT_TL.FORMAT_NAME is 'Contains the name or description of the store format.'
/

