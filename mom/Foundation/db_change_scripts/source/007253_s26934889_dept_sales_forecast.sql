--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


-----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
---------------------------------------------------------------------------------------
--Materialized View Created:                               DEPT_SALES_FORECAST
---------------------------------------------------------------------------------------

whenever sqlerror exit
PROMPT create materialized view DEPT_SALES_FORECAST
CREATE MATERIALIZED VIEW DEPT_SALES_FORECAST
ON PREBUILT TABLE 
WITH REDUCED PRECISION
REFRESH FORCE ON DEMAND 
ENABLE QUERY REWRITE
           AS SELECT fsc.dept dept,
                     1 domain_id,
                     fsc.loc loc,
                     fsc.eow_date eow_date,
                     SUM(fsc.forecast_sales) forecast_sales
                FROM class_sales_forecast fsc,
                     store s
               WHERE fsc.loc       = s.store
            GROUP BY fsc.dept, fsc.loc, fsc.eow_date
/