--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit
---------------------------------------------------------------------------
-- Name:    GET_ITEM_TYPE
-- Purpose: This function is used as a function based index for ITEM_MASTER.
--
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION GET_ITEM_TYPE(I_simple_pack_ind     IN   ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                                         I_pack_ind            IN   ITEM_MASTER.PACK_IND%TYPE,
                                         I_deposit_item_type   IN   ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE,
                                         I_item_xform_ind      IN   ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                                         I_orderable_ind       IN   ITEM_MASTER.ORDERABLE_IND%TYPE,
                                         I_dept                IN   DEPS.DEPT%TYPE)
RETURN VARCHAR2 DETERMINISTIC
IS 

   L_item_type       VARCHAR2(1) := NULL;
   L_purchase_type   DEPS.PURCHASE_TYPE%TYPE; 

   cursor C_GET_PURCHASE_TYPE is
      select purchase_type
        from deps
       where dept = I_dept;

BEGIN
  if I_dept is not null then 
     open C_GET_PURCHASE_TYPE;
     fetch C_GET_PURCHASE_TYPE into L_purchase_type;
     close C_GET_PURCHASE_TYPE;
  end if;

  if I_simple_pack_ind = 'Y' then
      L_item_type := 'S';
  elsif I_pack_ind = 'Y' and I_simple_pack_ind = 'N' then
      L_item_type := 'C';
  elsif I_deposit_item_type is not null then
     L_item_type := I_deposit_item_type;
  elsif I_item_xform_ind = 'Y' and I_orderable_ind = 'Y' then
     L_item_type := 'O';
  elsif I_item_xform_ind = 'Y' and I_orderable_ind = 'N' then
     L_item_type := 'L';
  elsif L_purchase_type IN (1,2) then 
     L_item_type := 'I';
  else 
     L_item_type := 'R';
  end if;
  
  return L_item_type;
EXCEPTION
   -- ignore errors because a null will be returned
   when OTHERS then
      NULL;
END;
/

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT CREATING INDEX 'ITEM_MASTER_I13'
CREATE INDEX ITEM_MASTER_I13 ON ITEM_MASTER(GET_ITEM_TYPE(SIMPLE_PACK_IND,PACK_IND, DEPOSIT_ITEM_TYPE,ITEM_XFORM_IND,ORDERABLE_IND,DEPT))
/
