CREATE OR REPLACE FORCE VIEW V_DIFF_TYPE_TL (DIFF_TYPE, DIFF_TYPE_DESC, LANG ) AS
SELECT  b.diff_type,
        case when tl.lang is not null then tl.diff_type_desc else b.diff_type_desc end diff_type_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  DIFF_TYPE b,
        DIFF_TYPE_TL tl
 WHERE  b.diff_type = tl.diff_type (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_DIFF_TYPE_TL is 'This is the translation view for base table DIFF_TYPE. This view fetches data in user langauge either from translation table DIFF_TYPE_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_DIFF_TYPE_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_DIFF_TYPE_TL.DIFF_TYPE is 'Contains the code used to uniquely identify a differentiator type.'
/

COMMENT ON COLUMN V_DIFF_TYPE_TL.DIFF_TYPE_DESC is 'Contains the description of the differentiator type.'
/

