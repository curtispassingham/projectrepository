CREATE OR REPLACE FORCE VIEW V_DOC_TL (DOC_ID, DOC_DESC, LANG ) AS
SELECT  b.doc_id,
        case when tl.lang is not null then tl.doc_desc else b.doc_desc end doc_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  DOC b,
        DOC_TL tl
 WHERE  b.doc_id = tl.doc_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_DOC_TL is 'This is the translation view for base table DOC. This view fetches data in user langauge either from translation table DOC_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_DOC_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_DOC_TL.DOC_ID is 'An assigned sequence number to distinguish between the different documents.'
/

COMMENT ON COLUMN V_DOC_TL.DOC_DESC is 'A full description or name of the document type.'
/

