--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_ORG_UNIT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_ORG_UNIT'
CREATE TABLE SVC_ORG_UNIT
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
	ORG_UNIT_ID NUMBER(15,0) ,
    DESCRIPTION VARCHAR2(120) ,
    SET_OF_BOOKS_ID NUMBER(15,0) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE
	)
	INITRANS 6	
	TABLESPACE RETAIL_DATA
/


COMMENT ON TABLE SVC_ORG_UNIT IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in ORG_UNIT.'
/

COMMENT ON COLUMN SVC_ORG_UNIT.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_ORG_UNIT.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_ORG_UNIT.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_ORG_UNIT.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_ORG_UNIT.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_ORG_UNIT.ORG_UNIT_ID IS 'Holds the oracle organizational unit ID.'
/

COMMENT ON COLUMN SVC_ORG_UNIT.DESCRIPTION IS 'Holds the organizational unit description.'
/

COMMENT ON COLUMN SVC_ORG_UNIT.SET_OF_BOOKS_ID IS 'Set of Books Id.'
/

COMMENT ON COLUMN SVC_ORG_UNIT.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_ORG_UNIT.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_ORG_UNIT.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_ORG_UNIT.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/

PROMPT Creating Primary Key on 'SVC_ORG_UNIT'   
ALTER TABLE SVC_ORG_UNIT
ADD CONSTRAINT SVC_ORG_UNIT_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX	   
    
/
PROMPT Creating Unique Key on 'SVC_ORG_UNIT'   
ALTER TABLE SVC_ORG_UNIT
ADD CONSTRAINT SVC_ORG_UNIT_UK UNIQUE ( ORG_UNIT_ID )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX	   
/

