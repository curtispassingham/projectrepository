--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ITEM_SUPP_COUNTRY'
COMMENT ON COLUMN ITEM_SUPP_COUNTRY.UNIT_COST is 'This field contains the current corporate unit cost for the SKU from the supplier/origin country.  This field is stored in item''s standard uom and in supplier currency.  This value will match the cost field for the primary location on item_supp_country_loc.  This field may be edited while the item is in worksheet status.  If edited, the user will have the choice of changing the cost of all locations, or only the primary location.  This cost is the cost that will be written to item_supp_country_loc when new locations are added to an item.'
/

COMMENT ON COLUMN ITEM_SUPP_COUNTRY.COST_UOM is 'A cost UOM is held to allow costs to be managed in a separate UOM than the standard UOM. The unit cost stored in standard UOM is converted to cost UOM for display in the Item Supplier Country screen. Likewise, if the user enters or updates the unit cost via the Item Supplier Country screen, it is converted to standard UOM before being saved to the table.'
/

