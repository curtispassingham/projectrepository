CREATE OR REPLACE FORCE VIEW V_BANNER_TL (BANNER_ID, BANNER_NAME, LANG ) AS
SELECT  b.banner_id,
        case when tl.lang is not null then tl.banner_name else b.banner_name end banner_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  BANNER b,
        BANNER_TL tl
 WHERE  b.banner_id = tl.banner_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_BANNER_TL is 'This is the translation view for base table BANNER. This view fetches data in user langauge either from translation table BANNER_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_BANNER_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_BANNER_TL.BANNER_ID is 'The number to uniquely identify a Banner.'
/

COMMENT ON COLUMN V_BANNER_TL.BANNER_NAME is 'The name of the Banner for which channels are associated.'
/

