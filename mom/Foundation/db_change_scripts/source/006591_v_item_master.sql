--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_ITEM_MASTER
----------------------------------------------------------------------------
PROMPT Creating View 'V_ITEM_MASTER'

CREATE OR REPLACE FORCE VIEW V_ITEM_MASTER (DIVISION, GROUP_NO, DEPT, CLASS, SUBCLASS, ITEM, STATUS, ITEM_LEVEL, TRAN_LEVEL, ITEM_DESC, ITEM_DESC_SECONDARY, DESC_UP, PACK_TYPE, PACK_IND, SELLABLE_IND, ORDERABLE_IND, SIMPLE_PACK_IND, ITEM_NUMBER_TYPE, ITEM_PARENT, ITEM_GRANDPARENT, DIFF_1, DIFF_2, DIFF_3, DIFF_4, CONTAINS_INNER_IND, INVENTORY_IND, STANDARD_UOM, ITEM_XFORM_IND, CATCH_WEIGHT_IND, DEPOSIT_ITEM_TYPE, CONTAINER_ITEM, UOM_CONV_FACTOR, PERISHABLE_IND, PACKAGE_UOM, CATCH_WEIGHT_UOM, MERCHANDISE_IND, PREFIX, NOTIONAL_PACK_IND, HANDLING_SENSITIVITY, SHIP_ALONE_IND, DIFF_1_AGGREGATE_IND, PACKAGE_SIZE, COST_ZONE_GROUP_ID, DEPOSIT_IN_PRICE_PER_UOM, ORDER_AS_TYPE, FORECAST_IND, GIFT_WRAP_IND, FORMAT_ID, PRODUCT_CLASSIFICATION, WASTE_TYPE, SOH_INQUIRY_AT_PACK_IND, CATCH_WEIGHT_TYPE, DIFF_2_AGGREGATE_IND, WASTE_PCT, SALE_TYPE, CONST_DIMEN_IND, DEFAULT_WASTE_PCT, LAST_UPDATE_ID, ORDER_TYPE, CHECK_UDA_IND, ITEM_AGGREGATE_IND, ITEM_SERVICE_LEVEL, STORE_ORD_MULT, CREATE_DATETIME, RETAIL_LABEL_VALUE, ORIGINAL_RETAIL, AIP_CASE_TYPE, SHORT_DESC, HANDLING_TEMP, RETAIL_LABEL_TYPE, DIFF_3_AGGREGATE_IND, LAST_UPDATE_DATETIME, MFG_REC_RETAIL, PRIMARY_REF_ITEM_IND, BRAND_NAME, COMMENTS, CREATE_ID, DIFF_4_AGGREGATE_IND)
AS
WITH lng AS (select LANGUAGE_SQL.GET_USER_LANGUAGE() user_lang from dual where rownum = 1)
    SELECT (select division from groups g,deps d where g.group_no = d.group_no and d.dept = iem.dept) division 
          ,(select group_no from deps where dept = iem.dept) GROUP_NO
          ,iem.DEPT DEPT
          ,IEM.CLASS CLASS
          ,IEM.SUBCLASS SUBCLASS
          ,IEM.ITEM ITEM
          ,IEM.STATUS STATUS
          ,IEM.ITEM_LEVEL ITEM_LEVEL
          ,IEM.TRAN_LEVEL TRAN_LEVEL
          , NVL( (select item_desc 
                    from item_master_tl tl, lng 
                   where item = iem.item and tl.lang = lng.user_lang ), iem.item_desc) item_desc
          ,NVL( (select item_desc_secondary 
                   from item_master_tl tl, lng 
                  where item = iem.item and tl.lang = lng.user_lang), iem.item_desc_secondary) item_desc_secondary
          ,IEM.DESC_UP DESC_UP
          ,IEM.PACK_TYPE PACK_TYPE
          ,IEM.PACK_IND PACK_IND
          ,IEM.SELLABLE_IND SELLABLE_IND
          ,IEM.ORDERABLE_IND ORDERABLE_IND
          ,IEM.SIMPLE_PACK_IND SIMPLE_PACK_IND
          ,IEM.ITEM_NUMBER_TYPE ITEM_NUMBER_TYPE
          ,IEM.ITEM_PARENT ITEM_PARENT
          ,IEM.ITEM_GRANDPARENT ITEM_GRANDPARENT
          ,IEM.DIFF_1 DIFF_1
          ,IEM.DIFF_2 DIFF_2
          ,IEM.DIFF_3 DIFF_3
          ,IEM.DIFF_4 DIFF_4
          ,IEM.CONTAINS_INNER_IND CONTAINS_INNER_IND
          ,IEM.INVENTORY_IND INVENTORY_IND
          ,IEM.STANDARD_UOM STANDARD_UOM
          ,IEM.ITEM_XFORM_IND ITEM_XFORM_IND
          ,IEM.CATCH_WEIGHT_IND CATCH_WEIGHT_IND
          ,IEM.DEPOSIT_ITEM_TYPE DEPOSIT_ITEM_TYPE
          ,IEM.CONTAINER_ITEM CONTAINER_ITEM
          ,IEM.UOM_CONV_FACTOR UOM_CONV_FACTOR
          ,IEM.PERISHABLE_IND
          ,IEM.PACKAGE_UOM
          ,IEM.CATCH_WEIGHT_UOM
          ,IEM.MERCHANDISE_IND
          ,IEM.PREFIX
          ,IEM.NOTIONAL_PACK_IND
          ,IEM.HANDLING_SENSITIVITY
          ,IEM.SHIP_ALONE_IND
          ,IEM.DIFF_1_AGGREGATE_IND
          ,IEM.PACKAGE_SIZE
          ,IEM.COST_ZONE_GROUP_ID
          ,IEM.DEPOSIT_IN_PRICE_PER_UOM
          ,IEM.ORDER_AS_TYPE
          ,IEM.FORECAST_IND
          ,IEM.GIFT_WRAP_IND
          ,IEM.FORMAT_ID
          ,IEM.PRODUCT_CLASSIFICATION
          ,IEM.WASTE_TYPE
          ,IEM.SOH_INQUIRY_AT_PACK_IND
          ,IEM.CATCH_WEIGHT_TYPE
          ,IEM.DIFF_2_AGGREGATE_IND
          ,IEM.WASTE_PCT
          ,IEM.SALE_TYPE
          ,IEM.CONST_DIMEN_IND
          ,IEM.DEFAULT_WASTE_PCT
          ,IEM.LAST_UPDATE_ID
          ,IEM.ORDER_TYPE
          ,IEM.CHECK_UDA_IND
          ,IEM.ITEM_AGGREGATE_IND
          ,IEM.ITEM_SERVICE_LEVEL
          ,IEM.STORE_ORD_MULT
          ,IEM.CREATE_DATETIME
          ,IEM.RETAIL_LABEL_VALUE
          ,IEM.ORIGINAL_RETAIL
          ,IEM.AIP_CASE_TYPE
          ,NVL( (select short_desc 
                  from item_master_tl tl, lng 
                  where item = iem.item and tl.lang = lng.user_lang), iem.short_desc) short_desc
          ,IEM.HANDLING_TEMP
          ,IEM.RETAIL_LABEL_TYPE
          ,IEM.DIFF_3_AGGREGATE_IND
          ,IEM.LAST_UPDATE_DATETIME
          ,IEM.MFG_REC_RETAIL
          ,IEM.PRIMARY_REF_ITEM_IND
          ,IEM.BRAND_NAME
          ,IEM.COMMENTS
          ,IEM.CREATE_ID
          ,IEM.DIFF_4_AGGREGATE_IND
     FROM ITEM_MASTER IEM
/

COMMENT ON TABLE V_ITEM_MASTER IS 'This view will be used to display the Item LOVs using a security policy to filter User access'
/