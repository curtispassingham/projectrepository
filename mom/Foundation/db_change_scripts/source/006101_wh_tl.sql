--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       WH_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE WH_TL(
LANG NUMBER(6) NOT NULL,
WH NUMBER(10) NOT NULL,
WH_NAME VARCHAR2(150) NOT NULL,
WH_NAME_SECONDARY VARCHAR2(150) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE WH_TL is 'This is the translation table for WH table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN WH_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN WH_TL.WH is 'Contains the number which uniquely identifies the warehouse. The wh table stores all warehouses in the system.  Both virtual and physical warehouses will be stored on this table.  The addition of the new column, physical_wh, helps determine which warehouses are physical and which are virtual.  All physical warehouses will have a physical_wh column value equal to their wh number.  Virtual warehouses will have a valid physical warehouse in this column.'
/

COMMENT ON COLUMN WH_TL.WH_NAME is 'Contains the name of the warehouse which, along with the warehouse number, identifies the warehouse.'
/

COMMENT ON COLUMN WH_TL.WH_NAME_SECONDARY is 'Secondary name of the warehouse.'
/

COMMENT ON COLUMN WH_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN WH_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN WH_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN WH_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE WH_TL ADD CONSTRAINT PK_WH_TL PRIMARY KEY (
LANG,
WH
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE WH_TL
 ADD CONSTRAINT WHT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE WH_TL ADD CONSTRAINT WHT_WH_FK FOREIGN KEY (
WH
) REFERENCES WH (
WH
)
/

