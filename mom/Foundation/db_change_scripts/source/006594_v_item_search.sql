--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	View Modified:						v_item_search
---------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------
--       MODIFYING VIEW
-------------------------------------------
CREATE OR REPLACE FORCE VIEW V_ITEM_SEARCH
("ITEM", 
 "ITEM_DESC", 
 "ITEM_DESC_SECONDARY", 
 "ITEM_LEVEL", 
 "ITEM_LEVEL_DESC", 
 "TRAN_LEVEL", 
 "TRAN_LEVEL_DESC", 
 "ITEM_TYPE", 
 "SUPPLIER", 
 "VPN", 
 "DEPT1", 
 "DEPT_NAME", 
 "CLASS1", 
 "CLASS_NAME", 
 "SUBCLASS1", 
 "SUB_NAME", 
 "STATUS", 
 "DIVISION", 
 "GROUP_NO", 
 "ITEM_GRANDPARENT", 
 "ITEM_PARENT", 
 "SHORT_DESC", 
 "ITEM_NUMBER_TYPE", 
 "ITEM_NUMBER_TYPE_DESC", 
 "SELLABLE_IND", 
 "ORDERABLE_IND", 
 "INVENTORY_IND", 
 "PACK_IND", 
 "PACK_TYPE", 
 "DIFF_1", 
 "DIFF_2", 
 "DIFF_3", 
 "DIFF_4", 
 "ORIGINAL_RETAIL", 
 "MFG_REC_RETAIL", 
 "MERCHANDISE_IND", 
 "FORECAST_IND", 
 "ITEM_XFORM_IND", 
 "CATCH_WEIGHT_IND", 
 "DEPOSIT_ITEM_TYPE",
 "PACK_TYPE_DESC", 
 "DEPOSIT_ITEM_TYPE_DESC")
AS 
WITH lng AS (select LANGUAGE_SQL.GET_USER_LANGUAGE() user_lang from dual where rownum = 1)
SELECT  item.item,
          item.item_desc,
          item.item_desc_secondary,
          item.item_level,
          (SELECT code_desc FROM v_code_detail ilvs WHERE item.item_level = ilvs.code AND ilvs.code_type = 'ILVS') AS item_level_desc,
          item.tran_level,
          (SELECT code_desc FROM v_code_detail tlvl WHERE item.tran_level=tlvl.code AND tlvl.code_type ='TLVL') AS tran_level_desc,
          CASE
             WHEN item.simple_pack_ind = 'Y' THEN
                  'S'
             WHEN item.pack_ind = 'Y' AND item.simple_pack_ind = 'N' THEN
                  'C'
             WHEN item.deposit_item_type IS NOT NULL THEN
                  item.deposit_item_type
             WHEN item.item_xform_ind = 'Y' AND item.orderable_ind = 'Y' THEN
                  'O'
             WHEN item.item_xform_ind = 'Y' AND item.orderable_ind = 'N' THEN
                  'L'
             WHEN (select purchase_type from deps where dept = item.dept) IN (1, 2) THEN
                  'I'
             ELSE 'R'
           END item_type,
           (select supplier from item_supplier where item = item.item and primary_supp_ind = 'Y') supplier,
           (select vpn from item_supplier where item = item.item and primary_supp_ind = 'Y') vpn,
           item.dept ,
           (select dept_name from v_deps_tl, lng where dept = item.dept and lang = lng.user_lang) AS dept_name,
           item.class AS class1,
          (select class_name from v_class_tl,lng where dept = item.dept and class = item.class and lang = lng.user_lang) AS class_name,
           item.subclass AS subclass1,
          (select sub_name from v_subclass_tl,lng where dept = item.dept and class = item.class and subclass = item.subclass and lang = lng.user_lang) AS sub_name,
           DECODE((SELECT 'Y'
                     FROM daily_purge WHERE upper(table_name) = 'ITEM_MASTER'
                      AND (key_value = item.item
                       OR key_value = item.item_parent
                       OR key_value = item.item_grandparent)
                      AND rownum =1), 'Y', 'D', item.status) status,
           (select division from groups where group_no = (select group_no from deps where dept = item.dept)) division,
           (select group_no from deps where dept = item.dept) AS group_no,
           item.item_grandparent,
           item.item_parent,
           item.short_desc,
           item.item_number_type,
           (SELECT code_desc FROM v_code_detail upct WHERE item.item_number_type = upct.code AND upct.code_type = 'UPCT') item_number_type_desc,
           item.sellable_ind,
           item.orderable_ind,
           item.inventory_ind,
           item.pack_ind,
           item.pack_type,
           item.diff_1,
           item.diff_2,
           item.diff_3,
           item.diff_4,
           item.original_retail,
           item.mfg_rec_retail,
           item.merchandise_ind,
           item.forecast_ind,
           item.item_xform_ind,
           item.catch_weight_ind,
           item.deposit_item_type,
           (SELECT code_desc FROM v_code_detail pctp WHERE item.pack_type = pctp.code AND pctp.code_type = 'PATP') pack_type_desc,
           (SELECT code_desc FROM v_code_detail itmt WHERE item.deposit_item_type=itmt.code AND itmt.code_type ='ITMT') AS deposit_item_type_desc
  FROM v_item_master item;

  COMMENT ON TABLE V_ITEM_SEARCH
IS
  'This view is used for the item search screen in RMS Alloy. It includes all fields that can be used as item search criteria along with their descriptions to be displayed in the search result except for unit_cost.' ;

