--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_TSF_ENTITY_ORG_UNIT_SOB
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_TSF_ENTITY_ORG_UNIT_SOB"
CREATE TABLE DC_TSF_ENTITY_ORG_UNIT_SOB
(
   TSF_ENTITY_ID                 NUMBER(10),
   ORG_UNIT_ID                   NUMBER(15),
   SET_OF_BOOKS_ID               NUMBER(15)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_TSF_ENTITY_ORG_UNIT_SOB is 'This table is a staging table for data conversion and will hold data for TSF_ENTITY_ORG_UNIT_SOB.'
/
COMMENT ON COLUMN DC_TSF_ENTITY_ORG_UNIT_SOB.TSF_ENTITY_ID is 'This field contains the number which uniquely identifies a transfer entity.'
/
COMMENT ON COLUMN DC_TSF_ENTITY_ORG_UNIT_SOB.ORG_UNIT_ID is 'This field contains the number which uniquely identifies a org unit.'
/
COMMENT ON COLUMN DC_TSF_ENTITY_ORG_UNIT_SOB.SET_OF_BOOKS_ID is 'This field contains the number which uniquely identifies a set of books. One set of books can be shared among multiple transfer entities and org units however each transfer entity and org unit can be associated with only one set of books.'
/