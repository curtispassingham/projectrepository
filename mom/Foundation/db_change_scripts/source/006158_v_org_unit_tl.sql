CREATE OR REPLACE FORCE VIEW V_ORG_UNIT_TL (ORG_UNIT_ID, DESCRIPTION, LANG ) AS
SELECT  b.org_unit_id,
        case when tl.lang is not null then tl.description else b.description end description,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  ORG_UNIT b,
        ORG_UNIT_TL tl
 WHERE  b.org_unit_id = tl.org_unit_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_ORG_UNIT_TL is 'This is the translation view for base table ORG_UNIT. This view fetches data in user langauge either from translation table ORG_UNIT_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_ORG_UNIT_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_ORG_UNIT_TL.ORG_UNIT_ID is 'holds the oracle organizational unit ID'
/

COMMENT ON COLUMN V_ORG_UNIT_TL.DESCRIPTION is 'holds the organizational unit description'
/

