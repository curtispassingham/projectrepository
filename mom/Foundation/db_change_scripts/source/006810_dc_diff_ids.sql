--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_DIFF_IDS
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_DIFF_IDS"
CREATE TABLE DC_DIFF_IDS
(
   DIFF_ID             VARCHAR2(10),
   DIFF_TYPE           VARCHAR2(6),
   DIFF_DESC           VARCHAR2(120),
   INDUSTRY_CODE       VARCHAR2(10),
   INDUSTRY_SUBGROUP   VARCHAR2(10)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_DIFF_IDS is 'This table is a staging table for data conversion and will holds all the unique differentiators defined for a differentiator type.'
/
COMMENT ON COLUMN DC_DIFF_IDS.DIFF_ID is 'Contains the code to uniquely identify a differentiator.'
/
COMMENT ON COLUMN DC_DIFF_IDS.DIFF_TYPE is 'This field will hold a value of the types of differentiators contained in this differentiator group.'
/
COMMENT ON COLUMN DC_DIFF_IDS.DIFF_DESC is 'Description of the differentiator.'
/
COMMENT ON COLUMN DC_DIFF_IDS.INDUSTRY_CODE is 'Can be used to hold the unique code used by industry standards to identify the differentiator.'
/
COMMENT ON COLUMN DC_DIFF_IDS.INDUSTRY_SUBGROUP is 'Can be used to hold a sub-grouping code used by industry standards to further identify the differentiator.'
/