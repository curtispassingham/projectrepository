--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_DISTRICT
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_DISTRICT"
CREATE TABLE DC_DISTRICT
(
   DISTRICT                   NUMBER(10),
   DISTRICT_NAME              VARCHAR2(120),
   REGION                     NUMBER(10),
   MGR_NAME                   VARCHAR2(120),
   CURRENCY_CODE              VARCHAR2(3)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_DISTRICT is 'This table is a staging table for data conversion and will hold data for DISTRICT.'
/
COMMENT ON COLUMN DC_DISTRICT.DISTRICT is 'Contains the number which uniquely identifies the district.'
/
COMMENT ON COLUMN DC_DISTRICT.DISTRICT_NAME is 'Contains the name of the district which, along with the district number, identifies the district.'
/
COMMENT ON COLUMN DC_DISTRICT.REGION is 'Contains the number of the region of which the district is a member.'
/
COMMENT ON COLUMN DC_DISTRICT.MGR_NAME is 'Contains the name of the manager of the district.'
/
COMMENT ON COLUMN DC_DISTRICT.CURRENCY_CODE is 'This field contains the currency code under which the district operates.'
/