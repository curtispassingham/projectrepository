--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
---------------------------------------------------------------------------
--  Table Dropped :				POS_MODS
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Dropping Table               
--------------------------------------

PROMPT Dropping Table 'POS_MODS' 
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'POS_MODS'
     AND OBJECT_TYPE = 'TABLE';

  if (L_table_exists != 0) then
      execute immediate 'DROP TABLE POS_MODS';
  end if;
end;
/
