PROMPT Creating View 'V_IINDUCT_SEARCH_STG'
CREATE OR REPLACE FORCE VIEW "V_IINDUCT_SEARCH_STG" 
("ITEM",
"ITEM_DESC", 
"ITEM_TYPE",
"DEPT", 
"CLASS",
"SUBCLASS",
"STATUS", 
"ITEM_LEVEL", 
"TRAN_LEVEL", 
"ORDERABLE_IND", 
"INVENTORY_IND",
"SELLABLE_IND", 
"LAST_UPD_ID",
"NEXT_UPD_ID", 
"PROCESS_ID", 
"SUPPLIER", 
"CREATE_DATETIME", 
"BRAND_NAME", 
"VPN",
"DEPT_NAME", 
"CLASS_NAME", 
"SUBCLASS_NAME",
"SUPPLIER_NAME",
"PROCESS_DESC" ) AS
with il as (select item,process_id,create_datetime 
              from svc_item_master
      union select item,process_id,create_datetime
             from svc_item_master_tl
      union select item,process_id,create_datetime
              from svc_item_supplier
      union select item,process_id,create_datetime
              from svc_item_country
      union select item,process_id,create_datetime
              from svc_item_country_l10n_ext
      union select item,process_id,create_datetime
              from svc_ITEM_MASTER_CFA_EXT
      union select item,process_id,create_datetime
              from svc_ITEM_SUPPLIER_CFA_EXT
      union select item,process_id,create_datetime
              from svc_ITEM_SUPP_COUNTRY
      union select item,process_id,create_datetime
              from svc_ITEM_SUPP_COUNTRY_CFA_EXT
      union select item,process_id,create_datetime
              from svc_ITEM_SUPP_COUNTRY_DIM
      union select item,process_id,create_datetime
              from svc_ITEM_SUPP_MANU_COUNTRY
      union select item,process_id,create_datetime
              from svc_ITEM_SUPP_UOM
      union select head_item,process_id,create_datetime
              from svc_ITEM_XFORM_DETAIL
      union select head_item,process_id,create_datetime
              from svc_ITEM_XFORM_HEAD
      union select item,process_id,create_datetime
              from svc_PACKITEM
      union select item,process_id,create_datetime
              from svc_RPM_ITEM_ZONE_PRICE
      union select item,process_id,create_datetime
              from svc_UDA_ITEM_FF
      union select item,process_id,create_datetime
              from svc_UDA_ITEM_LOV
      union select item,process_id,create_datetime
              from svc_UDA_ITEM_DATE
      union select item,process_id,create_datetime
              from svc_VAT_ITEM
      union select item,process_id,create_datetime
              from svc_item_image),
t as (select a.item, nvl(b.item_desc,a.item_desc) item_desc
        from svc_item_master a, (select item, item_desc from svc_item_master_tl where lang = get_user_lang) b
       where a.item = b.item(+)
      union
      select b.item, b.item_desc
        from svc_item_master_tl b
       where lang = get_user_lang
         and not exists(select 'x' from svc_item_master a where a.item = b.item and rownum = 1)
      union all 
      select b.item, b.item_desc 
        from (select item, item_desc, lang, rank() over (partition by item order by lang) as rank from svc_item_master_tl) b
       where not exists(select 'x'
                         from dual
                        where b.lang = get_user_lang)
         and not exists(select 'x'
                         from svc_item_master a 
                        where a.item = b.item)
         and b.rank = 1),
im as (select il.item,max(il.process_id) as maxpid,max(trunc(il.create_datetime))as maxcd
         from il
     group by il.item)
select distinct im.item,
       t.item_desc,
       CASE
        WHEN sim.simple_pack_ind = 'Y' THEN 
             'S'
        WHEN sim.pack_ind = 'Y' AND sim.simple_pack_ind = 'N' THEN
             'C'
        WHEN sim.deposit_item_type is not null THEN
             sim.deposit_item_type
        WHEN sim.item_xform_ind = 'Y' AND sim.orderable_ind = 'Y' THEN 
             'O'
        WHEN sim.item_xform_ind = 'Y' AND sim.orderable_ind = 'N' THEN 
             'L'
        WHEN dep.purchase_type IN (1, 2) THEN 
             'I'
        ELSE 'R'
       END item_type,
       sim.dept,
       sim.class,
       sim.subclass,
       sim.status,
       sim.item_level,
       sim.tran_level,
       sim.orderable_ind,
       sim.inventory_ind,
       sim.sellable_ind,
       (select user_id from svc_process_tracker where process_id = maxpid) as last_upd_id,
       sim.next_upd_id,
       im.maxpid as process_id,
       sis.supplier,
       im.maxcd as create_datetime,
       sim.brand_name,
       sis.vpn,
       (select dept_name from v_deps_tl dtl where dtl.dept = sim.dept) dept_name,
       (select cls.class_name from v_class_tl cls where cls.class = sim.class and cls.dept = sim.dept) class_name,
       (select scls.sub_name from v_subclass_tl scls where scls.subclass= sim.subclass and scls.class = sim.class and scls.dept = sim.dept) subclass_name,
       (select sup_name from v_sups_tl s where s.supplier = sis.supplier) supplier_name,
       (select process_desc from svc_process_tracker spt where spt.process_id = im.maxpid) process_desc
  from im,
       t,
       svc_item_master sim,
       (select item,supplier,vpn,primary_supp_ind,create_datetime,last_upd_id,process_id, rank() over(partition by item order by primary_supp_ind desc) rank from svc_item_supplier) sis,
       deps dep
 where im.item = t.item (+)
   and im.item = sim.item (+)
   and im.item = sis.item(+)
   and im.maxpid = sim.process_id (+)
   and im.maxpid = sis.process_id (+)
   and sis.rank(+) = 1
   and sim.dept = dep.dept(+);
   
   
COMMENT ON TABLE V_IINDUCT_SEARCH_STG IS 'This view is used for the item induct screen in RMS Alloy. It contains fields to be displayed in the search result when Item Upload/Download from staging option is selected.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.ITEM IS 'Item Id.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.ITEM_DESC IS 'Item description.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.ITEM_TYPE IS 'Derived attribute of item type based on item attributes to indicate if the item is a regular item, pack, simple pack, transform orderable, transform sellable, a consignment or concession item, or a type of deposit item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.DEPT IS 'The merchandise department of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.CLASS IS 'The merchandise class of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.SUBCLASS IS 'The merchandise subclass of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.STATUS IS 'Item status. If an item or its parent or grandparent is on daily purge, the item is in D (deleted) status.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.ITEM_LEVEL IS 'Item level.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.TRAN_LEVEL IS 'Transaction level.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.ORDERABLE_IND IS 'Item orderable indicator.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.INVENTORY_IND IS 'Item inventory indicator.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.SELLABLE_IND IS 'Item sellable indicator.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.LAST_UPD_ID IS 'User who last updated the record.';  
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.NEXT_UPD_ID IS 'User who is expected to work next on this item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.SUPPLIER IS 'The supplier of the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.CREATE_DATETIME IS 'Date time when record was inserted.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.BRAND_NAME IS 'Brand associated to the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.VPN IS 'The Vendor Product Number associated with the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.DEPT_NAME IS 'The merchandise department name of the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.CLASS_NAME IS 'The merchandise class name of the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.SUBCLASS_NAME IS 'The merchandise subclass name of the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.SUPPLIER_NAME IS 'The name of the supplier.';   
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.PROCESS_DESC IS 'The process description.';