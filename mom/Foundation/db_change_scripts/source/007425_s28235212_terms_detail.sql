--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'TERMS_DETAIL'

PROMPT MODIFYING CONSTRAINT 'CHK_TERMS_DETAIL_DUE_DOM'
ALTER TABLE TERMS_DETAIL DROP CONSTRAINT  CHK_TERMS_DETAIL_DUE_DOM
/
ALTER TABLE TERMS_DETAIL ADD CONSTRAINT
 CHK_TERMS_DETAIL_DUE_DOM CHECK (DUE_DOM BETWEEN 0 AND 31)
/

PROMPT MODIFYING CONSTRAINT 'CHK_TERMS_DETAIL_DISC_DOM'
ALTER TABLE TERMS_DETAIL DROP CONSTRAINT  CHK_TERMS_DETAIL_DISC_DOM
/
ALTER TABLE TERMS_DETAIL ADD CONSTRAINT
 CHK_TERMS_DETAIL_DISC_DOM CHECK (DISC_DOM BETWEEN 0 AND 31)
/
