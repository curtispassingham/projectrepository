--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       RELATED_ITEM_HEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE RELATED_ITEM_HEAD_TL(
LANG NUMBER(6) NOT NULL,
RELATIONSHIP_ID NUMBER(20) NOT NULL,
RELATIONSHIP_NAME VARCHAR2(255) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RELATED_ITEM_HEAD_TL is 'This is the translation table for RELATED_ITEM_HEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN RELATED_ITEM_HEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN RELATED_ITEM_HEAD_TL.RELATIONSHIP_ID is 'Unique identifier for each relationship header.'
/

COMMENT ON COLUMN RELATED_ITEM_HEAD_TL.RELATIONSHIP_NAME is 'Name given to the relationship.'
/

COMMENT ON COLUMN RELATED_ITEM_HEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN RELATED_ITEM_HEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN RELATED_ITEM_HEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN RELATED_ITEM_HEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE RELATED_ITEM_HEAD_TL ADD CONSTRAINT PK_RELATED_ITEM_HEAD_TL PRIMARY KEY (
LANG,
RELATIONSHIP_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE RELATED_ITEM_HEAD_TL
 ADD CONSTRAINT RIHT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE RELATED_ITEM_HEAD_TL ADD CONSTRAINT RIHT_RIH_FK FOREIGN KEY (
RELATIONSHIP_ID
) REFERENCES RELATED_ITEM_HEAD (
RELATIONSHIP_ID
)
/

