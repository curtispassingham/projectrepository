--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_UDA_VALUES_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

PROMPT CREATING TABLE 'SVC_UDA_VALUES_TL'
CREATE TABLE SVC_UDA_VALUES_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, UDA_ID NUMBER(5)
, UDA_VALUE NUMBER(5)
, UDA_VALUE_DESC VARCHAR2(250)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_UDA_VALUES_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in UDA_VALUES_TL.'
/

COMMENT ON COLUMN SVC_UDA_VALUES_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_UDA_VALUES_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_UDA_VALUES_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_UDA_VALUES_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_UDA_VALUES_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_UDA_VALUES_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_UDA_VALUES_TL.UDA_ID is 'This field contains a unique number identifying the User Defined Attribute.'
/

COMMENT ON COLUMN SVC_UDA_VALUES_TL.UDA_VALUE is 'This field contains a unique number identifying the User Defined Attribute value for the UDA. A UDA can have multiple values. For example, Color can be a UDA and it can have different values like Green, Red, Blue, etc.'
/

COMMENT ON COLUMN SVC_UDA_VALUES_TL.UDA_VALUE_DESC is 'This field contains a description of the UDA value.'
/

PROMPT CREATING PRIMARY KEY ON 'SVC_UDA_VALUES_TL'
ALTER TABLE SVC_UDA_VALUES_TL
ADD CONSTRAINT SVC_UDA_VALUES_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

PROMPT CREATING UNIQUE KEY ON 'SVC_UDA_VALUES_TL'
ALTER TABLE SVC_UDA_VALUES_TL
ADD CONSTRAINT SVC_UDA_VALUES_TL_UK UNIQUE
(LANG, UDA_ID, UDA_VALUE)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

