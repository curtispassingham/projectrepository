--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_SEC_USER_GROUP
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_SEC_USER_GROUP'
CREATE TABLE SVC_SEC_USER_GROUP
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  USER_SEQ NUMBER(15,0),
  GROUP_ID NUMBER(4,0),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_SEC_USER_GROUP is 'This is a staging table used for Admin API spreadsheet upload process.It is used to   temporarily hold data before it is uploaded/updated in SEC_USER_GROUP.'
/

COMMENT ON COLUMN SVC_SEC_USER_GROUP.PROCESS_ID is 'Uniquely identifies a process in   SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_SEC_USER_GROUP.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1'
/

COMMENT ON COLUMN SVC_SEC_USER_GROUP.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_SEC_USER_GROUP.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_SEC_USER_GROUP.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_SEC_USER_GROUP.USER_SEQ is 'This column holds the security user assigned to the security group. It references the user sequence defined on the SEC_USER table.'
/

COMMENT ON COLUMN SVC_SEC_USER_GROUP.GROUP_ID is 'This column holds the security group the security user belongs to. It references the group id defined on the SEC_GROUP table.'
/

COMMENT ON COLUMN SVC_SEC_USER_GROUP.CREATE_ID is 'This column holds the user id created the record.'
/

COMMENT ON COLUMN SVC_SEC_USER_GROUP.CREATE_DATETIME is 'This column holds the timestamp when the record is created.'
/

COMMENT ON COLUMN SVC_SEC_USER_GROUP.LAST_UPD_ID is 'This column holds the user id Last Updated the record.'
/

COMMENT ON COLUMN SVC_SEC_USER_GROUP.LAST_UPD_DATETIME is 'This column holds the timestamp when the record is Last Updated.'
/


PROMPT Creating Primary Key on 'SVC_SEC_USER_GROUP'
ALTER TABLE SVC_SEC_USER_GROUP
 ADD CONSTRAINT SVC_SEC_USER_GROUP_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_SEC_USER_GROUP'
ALTER TABLE SVC_SEC_USER_GROUP
 ADD CONSTRAINT SVC_SEC_USER_GROUP_UK UNIQUE
  (GROUP_ID,
   USER_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

