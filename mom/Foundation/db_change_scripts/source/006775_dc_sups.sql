--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_SUPS
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_SUPS"
CREATE TABLE DC_SUPS
(
   SUPPLIER                  NUMBER(10),
   SUP_NAME                  VARCHAR2(240),
   SUP_NAME_SECONDARY        VARCHAR2(240),
   CONTACT_NAME              VARCHAR2(120),
   CONTACT_PHONE             VARCHAR2(20),
   CONTACT_FAX               VARCHAR2(20),
   CONTACT_PAGER             VARCHAR2(20),
   QC_IND                    VARCHAR2(1),
   QC_PCT                    NUMBER(12,4),
   QC_FREQ                   NUMBER(2),
   VC_IND                    VARCHAR2(1),
   VC_PCT                    NUMBER(12,4),
   VC_FREQ                   NUMBER(2),
   CURRENCY_CODE             VARCHAR2(3),
   LANG                      NUMBER(6),
   TERMS                     VARCHAR2(15),
   FREIGHT_TERMS             VARCHAR2(30),
   RET_ALLOW_IND             VARCHAR2(1),
   RET_AUTH_REQ              VARCHAR2(1),
   RET_MIN_DOL_AMT           NUMBER(20,4),
   RET_COURIER               VARCHAR2(250),
   HANDLING_PCT              NUMBER(12,4),
   EDI_PO_IND                VARCHAR2(1),
   EDI_PO_CHG                VARCHAR2(1),
   EDI_PO_CONFIRM            VARCHAR2(1),
   EDI_ASN                   VARCHAR2(1),
   EDI_SALES_RPT_FREQ        VARCHAR2(1),
   EDI_SUPP_AVAILABLE_IND    VARCHAR2(1),
   EDI_CONTRACT_IND          VARCHAR2(1),
   EDI_CHANNEL_ID            NUMBER(4),
   REPLEN_APPROVAL_IND       VARCHAR2(1),
   SHIP_METHOD               VARCHAR2(6),
   PAYMENT_METHOD            VARCHAR2(6),
   CONTACT_TELEX             VARCHAR2(20),
   CONTACT_EMAIL             VARCHAR2(100),
   SETTLEMENT_CODE           VARCHAR2(1),
   PRE_MARK_IND              VARCHAR2(1),
   AUTO_APPR_INVC_IND        VARCHAR2(1),
   FREIGHT_CHARGE_IND        VARCHAR2(1),
   BACKORDER_IND             VARCHAR2(1),
   VAT_REGION                NUMBER(4),
   INV_MGMT_LVL              VARCHAR2(6),
   SERVICE_PERF_REQ_IND      VARCHAR2(1),
   DELIVERY_POLICY           VARCHAR2(6),
   COMMENT_DESC              VARCHAR2(2000),
   DEFAULT_ITEM_LEAD_TIME    NUMBER(4),
   DUNS_NUMBER               VARCHAR2(9),
   DUNS_LOC                  VARCHAR2(4),
   VMI_ORDER_STATUS          VARCHAR2(6),
   DSD_IND                   VARCHAR2(1),
   SUPPLIER_PARENT           NUMBER(10),
   SUP_QTY_LEVEL             VARCHAR2(6)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /

COMMENT ON TABLE DC_SUPS is 'This table is a staging table for data conversion and will hold the supplier information.'
/
COMMENT ON COLUMN DC_SUPS.SUPPLIER is 'Unique identifying number for a supplier within the system.'
/
COMMENT ON COLUMN DC_SUPS.SUP_NAME is 'Contains the suppliers trading name.'
/
COMMENT ON COLUMN DC_SUPS.SUP_NAME_SECONDARY is 'Secondary name of the supplier.'
/
COMMENT ON COLUMN DC_SUPS.CONTACT_NAME is 'Contains the name of the suppliers representative contact.'
/
COMMENT ON COLUMN DC_SUPS.CONTACT_PHONE is 'Contains a telephone number for the suppliers representative contact.'
/
COMMENT ON COLUMN DC_SUPS.CONTACT_FAX is 'Contains a fax number for the suppliers representative contact.'
/
COMMENT ON COLUMN DC_SUPS.CONTACT_PAGER is 'Contains the number for the pager of the supplier�s representative contact.'
/
COMMENT ON COLUMN DC_SUPS.QC_IND is 'Determines whether orders from this supplier will default as requiring quality control.'
/
COMMENT ON COLUMN DC_SUPS.QC_PCT is 'Indicates the percentage of items per receipt that will be marked for quality checking.'
/
COMMENT ON COLUMN DC_SUPS.QC_FREQ is 'Indicates the frequency in which items per receipt will be marked for quality checking.'
/
COMMENT ON COLUMN DC_SUPS.VC_IND is 'Determines whether orders from this supplier will default as requiring vendor control.'
/
COMMENT ON COLUMN DC_SUPS.VC_PCT is 'Indicates the percentage of items per receipt that will be marked for vendor checking.'
/
COMMENT ON COLUMN DC_SUPS.VC_FREQ is 'Indicates the frequency in which items per receipt will be marked for vendor checking.'
/
COMMENT ON COLUMN DC_SUPS.CURRENCY_CODE is 'Contains a code identifying the currency the supplier uses for business transactions.'
/
COMMENT ON COLUMN DC_SUPS.LANG is 'This field contains the suppliers preferred language.'
/
COMMENT ON COLUMN DC_SUPS.TERMS is 'Indicator identifying the sales terms that will default when an order is created for the supplier.'
/
COMMENT ON COLUMN DC_SUPS.FREIGHT_TERMS is 'Indicator that references what freight terms will default when a order is created for the supplier.'
/
COMMENT ON COLUMN DC_SUPS.RET_ALLOW_IND is 'Indicates whether or not the supplier will accept returns.'
/
COMMENT ON COLUMN DC_SUPS.RET_AUTH_REQ is 'Indicates if returns must be accompanied by an authorization number when sent back to the vendors.'
/
COMMENT ON COLUMN DC_SUPS.RET_MIN_DOL_AMT is 'Contains a value if the supplier requires a minimum dollar amount to be returned in order to accept the return.'
/
COMMENT ON COLUMN DC_SUPS.RET_COURIER is 'Contains the name of the courier that should be used for all returns to the supplier.'
/
COMMENT ON COLUMN DC_SUPS.HANDLING_PCT is 'Percentage multiplied by the total order cost to determine the handling cost for the return.'
/
COMMENT ON COLUMN DC_SUPS.EDI_PO_IND is 'Indicates whether purchase orders will be sent to the supplier via Electronic Data Interchange.'
/
COMMENT ON COLUMN DC_SUPS.EDI_PO_CHG is 'Indicates whether purchase order changes will be sent to the supplier via Electronic Data Interchange.'
/
COMMENT ON COLUMN DC_SUPS.EDI_PO_CONFIRM is 'Indicates whether this supplier will send acknowledgment of a purchase orders sent via Electronic Data Interchange.'
/
COMMENT ON COLUMN DC_SUPS.EDI_ASN is 'Indicates whether this supplier will send Advance Shipment Notifications electronically.'
/
COMMENT ON COLUMN DC_SUPS.EDI_SALES_RPT_FREQ is 'This field contains the EDI sales report frequency for this supplier.'
/
COMMENT ON COLUMN DC_SUPS.EDI_SUPP_AVAILABLE_IND is 'This field indicates whether the supplier will send availability via EDI.'
/
COMMENT ON COLUMN DC_SUPS.EDI_CONTRACT_IND is 'This field indicates whether contracts will be sent to the supplier via EDI.'
/
COMMENT ON COLUMN DC_SUPS.EDI_CHANNEL_ID is 'The supplier is an EDI supplier and supports vendor initiated ordering, this field will contain the channel ID for the channel to which all inventory for these types of orders will flow.'
/
COMMENT ON COLUMN DC_SUPS.REPLEN_APPROVAL_IND is 'Indicates whether contract orders for the supplier should be created in Approved status.'
/
COMMENT ON COLUMN DC_SUPS.SHIP_METHOD is 'The method used to ship the items on the purchase order from the country of origin to the country of import.'
/
COMMENT ON COLUMN DC_SUPS.PAYMENT_METHOD is 'LC(Letter of Credit)'
/
COMMENT ON COLUMN DC_SUPS.CONTACT_TELEX is 'This field contains the telex number of the partner or suppliers representative contact.'
/
COMMENT ON COLUMN DC_SUPS.CONTACT_EMAIL is 'This field contains the email address of the partner or suppliers representative contact.'
/
COMMENT ON COLUMN DC_SUPS.SETTLEMENT_CODE is 'This field indicates which payment process method is used for this supplier.'
/
COMMENT ON COLUMN DC_SUPS.PRE_MARK_IND is 'This field indicates whether or not the supplier has agreed to break an order into separate boxes (and mark them) that can be shipped directly to the stores.'
/
COMMENT ON COLUMN DC_SUPS.AUTO_APPR_INVC_IND is 'Indicates whether or not the suppliers invoice matches can be automatically approved for payment.'
/
COMMENT ON COLUMN DC_SUPS.FREIGHT_CHARGE_IND is 'Indicates if a supplier is allowed to charge freight costs to the client. This field will only be populated if invoice matching is installed.'
/
COMMENT ON COLUMN DC_SUPS.BACKORDER_IND is 'Indicates if backorders or partial shipments will be accepted.'
/
COMMENT ON COLUMN DC_SUPS.VAT_REGION is 'Contains the unique identifying number for the VAT region in the system.'
/
COMMENT ON COLUMN DC_SUPS.INV_MGMT_LVL is 'Indicator that determines whether supplier inventory management information can be set up at the supplier/department level or just at the supplier level.'
/
COMMENT ON COLUMN DC_SUPS.SERVICE_PERF_REQ_IND is 'Indicates if the suppliers services (e.g. shelf stocking) must be confirmed as performed before paying an invoice from that supplier.'
/
COMMENT ON COLUMN DC_SUPS.DELIVERY_POLICY is 'Contains the delivery policy of the supplier. Next Day indicates that the if a location is closed, the supplier will deliver on the next day.'
/
COMMENT ON COLUMN DC_SUPS.COMMENT_DESC is 'Any miscellaneous comments associated with the supplier.'
/
COMMENT ON COLUMN DC_SUPS.DEFAULT_ITEM_LEAD_TIME is 'Holds the default lead time for the supplier. The lead time is the time the supplier needs between receiving an order and having the order ready to ship.'
/
COMMENT ON COLUMN DC_SUPS.DUNS_NUMBER is 'This field holds the Dun and Bradstreet number to identify the supplier.'
/
COMMENT ON COLUMN DC_SUPS.DUNS_LOC is 'This field holds the Dun and Bradstreet number to identify the location of the supplier.'
/
COMMENT ON COLUMN DC_SUPS.VMI_ORDER_STATUS is 'This column determines the status in which any inbound POs from this supplier are created.'
/
COMMENT ON COLUMN DC_SUPS.DSD_IND is 'Indicates whether the supplier can ship direct to store.'
/
COMMENT ON COLUMN DC_SUPS.SUPPLIER_PARENT is 'PARENT_SUPPLIER field will store supplier number for the supplier sites.'
/
COMMENT ON COLUMN DC_SUPS.SUP_QTY_LEVEL is 'This will hold the level at which quantity is ordered at.'
/