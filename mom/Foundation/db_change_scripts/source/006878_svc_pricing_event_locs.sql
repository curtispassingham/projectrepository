--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_PRICING_EVENT_LOCS'
COMMENT ON COLUMN SVC_PRICING_EVENT_LOCS.HIER_VALUE is 'Contains the organization hierarchy value at which the price is changing. It can be a store, warehouse, district, region, area, or chain.'
/

