CREATE OR REPLACE FORCE VIEW V_DIFF_GROUP_HEAD_TL (DIFF_GROUP_ID, DIFF_GROUP_DESC, LANG ) AS
SELECT  b.diff_group_id,
        case when tl.lang is not null then tl.diff_group_desc else b.diff_group_desc end diff_group_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  DIFF_GROUP_HEAD b,
        DIFF_GROUP_HEAD_TL tl
 WHERE  b.diff_group_id = tl.diff_group_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_DIFF_GROUP_HEAD_TL is 'This is the translation view for base table DIFF_GROUP_HEAD. This view fetches data in user langauge either from translation table DIFF_GROUP_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_DIFF_GROUP_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_DIFF_GROUP_HEAD_TL.DIFF_GROUP_ID is 'This field will hold a unique number (identifier) for the differentiator group.'
/

COMMENT ON COLUMN V_DIFF_GROUP_HEAD_TL.DIFF_GROUP_DESC is 'Description of the differentiator group (for example: Mens Shirt Sizes, Womens Shoe Sizes, Girls Dress Sizes, Shower Gel Scents, Yogurt Flavors, etc.).'
/

