--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_CHAIN
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_CHAIN'
CREATE OR REPLACE FORCE VIEW V_CHAIN
 (CHAIN
 ,CHAIN_NAME)
 AS SELECT CHA.CHAIN CHAIN
        ,V.CHAIN_NAME CHAIN_NAME
FROM CHAIN CHA,
     V_CHAIN_TL V
WHERE CHA.CHAIN = V.CHAIN    
/


COMMENT ON TABLE V_CHAIN IS 'This view will be used to display the Chain LOVs using a security policy to filter User access. The Chain name is translated based on user language'
/

