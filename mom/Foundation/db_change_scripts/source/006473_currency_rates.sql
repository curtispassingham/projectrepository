--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'CURRENCY_RATES'
COMMENT ON COLUMN CURRENCY_RATES.EXCHANGE_TYPE is 'Identifies the type of exchange rate the history exists for.  Valid values are defined under code type ''EXTP'': C (Consolidation) O (Operational) L (Letter of Credit/Bank) P (Purchase Order) U (Customs Entry) T (Transportation)'
/

