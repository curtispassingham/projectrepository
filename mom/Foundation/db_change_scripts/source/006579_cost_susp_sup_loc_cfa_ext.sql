--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 COST_SUSP_SUP_LOC_CFA_EXT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'COST_SUSP_SUP_LOC_CFA_EXT'
CREATE TABLE COST_SUSP_SUP_LOC_CFA_EXT
 (COST_CHANGE NUMBER(8) NOT NULL,
  ITEM VARCHAR2(25 ) NOT NULL,
  SUPPLIER NUMBER(10) NOT NULL,
  ORIGIN_COUNTRY_ID VARCHAR2(3 ) NOT NULL,
  LOC NUMBER(10) NOT NULL,
  BRACKET_VALUE1 NUMBER(12,4),
  GROUP_ID NUMBER(10) NOT NULL,
  VARCHAR2_1 VARCHAR2(250 ),
  VARCHAR2_2 VARCHAR2(250 ),
  VARCHAR2_3 VARCHAR2(250 ),
  VARCHAR2_4 VARCHAR2(250 ),
  VARCHAR2_5 VARCHAR2(250 ),
  VARCHAR2_6 VARCHAR2(250 ),
  VARCHAR2_7 VARCHAR2(250 ),
  VARCHAR2_8 VARCHAR2(250 ),
  VARCHAR2_9 VARCHAR2(250 ),
  VARCHAR2_10 VARCHAR2(250 ),
  NUMBER_11 NUMBER,
  NUMBER_12 NUMBER,
  NUMBER_13 NUMBER,
  NUMBER_14 NUMBER,
  NUMBER_15 NUMBER,
  NUMBER_16 NUMBER,
  NUMBER_17 NUMBER,
  NUMBER_18 NUMBER,
  NUMBER_19 NUMBER,
  NUMBER_20 NUMBER,
  DATE_21 DATE,
  DATE_22 DATE,
  DATE_23 DATE,
  DATE_24 DATE,
  DATE_25 DATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE COST_SUSP_SUP_LOC_CFA_EXT is 'This table is CFAS extension table for base COST_SUSP_SUP_DETAIL_LOC base table'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.COST_CHANGE is 'Contains the code used to uniquely identify a cost change.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.ITEM is 'Contains the code used to uniquely identify a item.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.SUPPLIER is 'Contains the code used to uniquely identify a supplier.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.ORIGIN_COUNTRY_ID is 'Contains the code used to uniquely identify a origin country.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.LOC is 'Contains the code used to uniquely identify a location.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.BRACKET_VALUE1 is 'Contains the code used to uniquely identify a bracket value.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.GROUP_ID is 'This column holds the attribute group id that this extended data is associated with.  The logical business meaning of the VARCHAR_ , NUMBER_ and DATE_ columns on this table are determined by the metadata defined for this attribute.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.VARCHAR2_1 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_1 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.VARCHAR2_2 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_2 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.VARCHAR2_3 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_3 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.VARCHAR2_4 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references  VARCHAR2_4 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.VARCHAR2_5 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references  VARCHAR2_5 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.VARCHAR2_6 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references  VARCHAR2_6 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.VARCHAR2_7 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references  VARCHAR2_7 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.VARCHAR2_8 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_8 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.VARCHAR2_9 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_9 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.VARCHAR2_10 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references VARCHAR2_10 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.NUMBER_11 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_11 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.NUMBER_12 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_12 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.NUMBER_13 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_13 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.NUMBER_14 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_14 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.NUMBER_15 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_15 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.NUMBER_16 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_16 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.NUMBER_17 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_17 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.NUMBER_18 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_18 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.NUMBER_19 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_19 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.NUMBER_20 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references NUMBER_20 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.DATE_21 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_21 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.DATE_22 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_22 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.DATE_23 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_22 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.DATE_24 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

COMMENT ON COLUMN COST_SUSP_SUP_LOC_CFA_EXT.DATE_25 is 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/


PROMPT Creating Unique Key on 'COST_SUSP_SUP_LOC_CFA_EXT'
ALTER TABLE COST_SUSP_SUP_LOC_CFA_EXT
 ADD CONSTRAINT UK_COST_SUSP_SUP_LOC_CFA_EXT UNIQUE
  (COST_CHANGE,
   ITEM,
   SUPPLIER,
   ORIGIN_COUNTRY_ID,
   LOC,
   BRACKET_VALUE1,
   GROUP_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'COST_SUSP_SUP_LOC_CFA_EXT'
 ALTER TABLE COST_SUSP_SUP_LOC_CFA_EXT
  ADD CONSTRAINT CPD_ISC_FK_EXT_FK
  FOREIGN KEY (ITEM, SUPPLIER, ORIGIN_COUNTRY_ID, LOC)
 REFERENCES ITEM_SUPP_COUNTRY_LOC (ITEM, SUPPLIER, ORIGIN_COUNTRY_ID, LOC)
/

