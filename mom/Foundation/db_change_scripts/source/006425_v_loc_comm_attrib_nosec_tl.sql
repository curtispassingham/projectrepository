--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--      UPDATING VIEW
--------------------------------------
PROMPT Creating View V_LOC_COMM_ATTRIB_NOSEC_TL
CREATE OR REPLACE FORCE VIEW V_LOC_COMM_ATTRIB_NOSEC_TL AS
   SELECT wh.WH LOC,
          'W' LOC_TYPE,
          tl.WH_NAME LOC_NAME,
          tl.WH_NAME_SECONDARY LOC_NAME_SECONDARY,
          wh.CURRENCY_CODE,
          wh.STOCKHOLDING_IND,
          wh.VAT_REGION,
          wh.CHANNEL_ID,
          wh.ORG_UNIT_ID,
          wh.TSF_ENTITY_ID,
          wh.CUSTOMER_ORDER_LOC_IND,
          wh.EMAIL,
          wh.DUNS_NUMBER,
          wh.DUNS_LOC,
          wh.CREATE_ID,
          wh.CREATE_DATETIME
     FROM WH wh, v_wh_tl tl
    WHERE wh.wh = tl.wh
    UNION ALL
   SELECT s.STORE, 
          'S' LOC_TYPE,
          tl.STORE_NAME,
          tl.STORE_NAME_SECONDARY,
          s.CURRENCY_CODE, 
          s.STOCKHOLDING_IND, 
          s.VAT_REGION, 
          s.CHANNEL_ID, 
          s.ORG_UNIT_ID, 
          s.TSF_ENTITY_ID, 
          s.CUSTOMER_ORDER_LOC_IND, 
          s.EMAIL, 
          s.DUNS_NUMBER, 
          s.DUNS_LOC, 
          s.CREATE_ID, 
          s.CREATE_DATETIME
     FROM STORE s, v_store_tl tl
    WHERE s.store = tl.store
/

COMMENT ON TABLE V_LOC_COMM_ATTRIB_NOSEC_TL IS 'This view combines all store and wh records in the system along with all the attributes that are common between them.  The view does not apply data security.'
/
