--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       RECLASS_HEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE RECLASS_HEAD_TL(
LANG NUMBER(6) NOT NULL,
RECLASS_NO NUMBER(4) NOT NULL,
RECLASS_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RECLASS_HEAD_TL is 'This is the translation table for RECLASS_HEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN RECLASS_HEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN RECLASS_HEAD_TL.RECLASS_NO is 'This field contains a unique number representing the reclassification number.'
/

COMMENT ON COLUMN RECLASS_HEAD_TL.RECLASS_DESC is 'This field contains a description of why the SKUs in the RECLASS_SKU table are being reclassified.'
/

COMMENT ON COLUMN RECLASS_HEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN RECLASS_HEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN RECLASS_HEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN RECLASS_HEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE RECLASS_HEAD_TL ADD CONSTRAINT PK_RECLASS_HEAD_TL PRIMARY KEY (
LANG,
RECLASS_NO
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE RECLASS_HEAD_TL
 ADD CONSTRAINT RHT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE RECLASS_HEAD_TL ADD CONSTRAINT RHT_RH_FK FOREIGN KEY (
RECLASS_NO
) REFERENCES RECLASS_HEAD (
RECLASS_NO
)
/

