CREATE OR REPLACE FORCE VIEW V_DEPS_TL (DEPT, DEPT_NAME, LANG ) AS
SELECT  b.dept,
        case when tl.lang is not null then tl.dept_name else b.dept_name end dept_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  DEPS b,
        DEPS_TL tl
 WHERE  b.dept = tl.dept (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_DEPS_TL is 'This is the translation view for base table DEPS. This view fetches data in user langauge either from translation table DEPS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_DEPS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_DEPS_TL.DEPT is 'Contains the number which uniquely identifies the department.'
/

COMMENT ON COLUMN V_DEPS_TL.DEPT_NAME is 'Contains the name of the department.'
/

