--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       ORG_UNIT_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE ORG_UNIT_TL(
LANG NUMBER(6) NOT NULL,
ORG_UNIT_ID NUMBER(15) NOT NULL,
DESCRIPTION VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE ORG_UNIT_TL is 'This is the translation table for ORG_UNIT table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN ORG_UNIT_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN ORG_UNIT_TL.ORG_UNIT_ID is 'holds the oracle organizational unit ID'
/

COMMENT ON COLUMN ORG_UNIT_TL.DESCRIPTION is 'holds the organizational unit description'
/

COMMENT ON COLUMN ORG_UNIT_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN ORG_UNIT_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN ORG_UNIT_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN ORG_UNIT_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE ORG_UNIT_TL ADD CONSTRAINT PK_ORG_UNIT_TL PRIMARY KEY (
LANG,
ORG_UNIT_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE ORG_UNIT_TL
 ADD CONSTRAINT OGUT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE ORG_UNIT_TL ADD CONSTRAINT OGUT_OGU_FK FOREIGN KEY (
ORG_UNIT_ID
) REFERENCES ORG_UNIT (
ORG_UNIT_ID
)
/

