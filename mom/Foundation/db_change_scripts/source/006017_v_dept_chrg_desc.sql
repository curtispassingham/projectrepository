--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_DEPT_CHRG_DESC
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_DEPT_CHRG_DESC'
CREATE OR REPLACE FORCE VIEW "V_DEPT_CHRG_DESC"
("DESCR",
 "ID",
 "CODE")
AS (
  SELECT area_name AS DESCR,
         area AS ID ,
         'A' AS CODE
   FROM v_area
  UNION ALL
  SELECT district_name DESCR,
         district AS ID,
         'D' AS CODE
    FROM v_district
  UNION ALL
  SELECT wh.wh_name AS DESCR,
         wh.wh AS ID,
         'X' AS CODE
    FROM wh,
         v_wh vw
   WHERE vw.wh = wh.wh
     AND wh.org_entity_type = 'X'
     AND finisher_ind       = 'N'    
  UNION ALL
  SELECT wh.wh_name AS DESCR,
         wh.wh AS ID,
         'M' AS CODE
    FROM wh,
         v_wh vw
   WHERE vw.wh = wh.wh
     AND wh.org_entity_type = 'M'
     AND finisher_ind = 'N'
  UNION ALL
  SELECT loc_list_desc AS DESCR,
         loc_list AS ID,
         'LL' AS CODE
    FROM v_loc_list_head
  UNION ALL
  SELECT wh_name AS DESCR,
         wh AS ID,
         'PW' AS CODE
    FROM v_wh
   WHERE stockholding_ind = 'N'
     AND wh = physical_wh
  UNION ALL
  SELECT region_name AS DESCR,
         region AS ID,
         'R' AS CODE
    FROM v_region
  UNION ALL
  SELECT store_name AS DESCR,
         store AS ID,
         'S' AS CODE
    FROM v_store
   WHERE stockholding_ind = 'Y'
  UNION ALL
  SELECT description AS DESCR,
         transfer_zone AS ID,
         'T' AS CODE
    FROM tsfzone
  UNION ALL
  SELECT wh_name DESCR,
         wh ID,
         'W' AS CODE
    FROM v_wh
   WHERE stockholding_ind = 'Y'
     AND wh != physical_wh
  UNION ALL
  SELECT description AS DESCR,
         zone_id AS ID,
         'CZ' AS CODE
    FROM cost_zone
  UNION ALL
  SELECT finisher_desc AS DESCR,
         finisher_id AS ID,
         'E' AS CODE
  FROM v_external_finisher
  UNION ALL
  SELECT finisher_desc AS DESCR,
         finisher_id AS ID,
         'I' AS CODE
  FROM v_internal_finisher)
/

COMMENT ON TABLE V_DEPT_CHRG_DESC IS 'This view returns the ID and description of the organization hierarchies used in the department upcharges screen.'
/

COMMENT ON COLUMN V_DEPT_CHRG_DESC."DESCR" IS 'The specific description of the Org Hierarchy Id.'
/

COMMENT ON COLUMN V_DEPT_CHRG_DESC."ID" IS 'The unique Id the identifies the particular Org Hierarchy Level value.'
/

COMMENT ON COLUMN V_DEPT_CHRG_DESC."CODE" IS 'The code that identifies the Org Hierarhcy Level.'
/