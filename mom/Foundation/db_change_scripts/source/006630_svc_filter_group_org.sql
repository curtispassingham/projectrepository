--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_FILTER_GROUP_ORG
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_FILTER_GROUP_ORG'
CREATE TABLE SVC_FILTER_GROUP_ORG
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  FILTER_ORG_ID NUMBER(15,0),
  FILTER_ORG_LEVEL VARCHAR2(1 ),
  SEC_GROUP_ID NUMBER(4,0),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_FILTER_GROUP_ORG is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in FILTER_GROUP_ORG.'
/

COMMENT ON COLUMN SVC_FILTER_GROUP_ORG.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_FILTER_GROUP_ORG.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_FILTER_GROUP_ORG.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_FILTER_GROUP_ORG.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_FILTER_GROUP_ORG.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_FILTER_GROUP_ORG.FILTER_ORG_ID is 'ID of the Organization hierarchy level
assigned to the User Security Group.'
/

COMMENT ON COLUMN SVC_FILTER_GROUP_ORG.FILTER_ORG_LEVEL is 'The Organization hierarchy level assigned to the User Security Group.  Valid values are contained in the CODE_DETIAL table with a CODE_TYPE of FLOW.'
/

COMMENT ON COLUMN SVC_FILTER_GROUP_ORG.SEC_GROUP_ID is 'ID of the User Security group'
/

COMMENT ON COLUMN SVC_FILTER_GROUP_ORG.CREATE_ID is 'User who created the record.'
/

COMMENT ON COLUMN SVC_FILTER_GROUP_ORG.CREATE_DATETIME is 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_FILTER_GROUP_ORG.LAST_UPD_ID is 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_FILTER_GROUP_ORG.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_FILTER_GROUP_ORG'
ALTER TABLE SVC_FILTER_GROUP_ORG
 ADD CONSTRAINT SVC_FILTER_GROUP_ORG_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_FILTER_GROUP_ORG'
ALTER TABLE SVC_FILTER_GROUP_ORG
 ADD CONSTRAINT SVC_FILTER_GROUP_ORG_UK UNIQUE
  (FILTER_ORG_ID,
   FILTER_ORG_LEVEL,
   SEC_GROUP_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

