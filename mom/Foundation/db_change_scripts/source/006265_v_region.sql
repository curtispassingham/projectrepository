--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_REGION
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW 
--------------------------------------
PROMPT Creating View 'V_REGION'
CREATE OR REPLACE FORCE VIEW V_REGION (CHAIN, AREA, REGION, REGION_NAME)
 AS SELECT ARE.CHAIN CHAIN
          ,REG.AREA AREA
          ,REG.REGION REGION
          ,V.REGION_NAME REGION_NAME
FROM REGION REG
    ,AREA ARE
    ,V_REGION_TL V
WHERE are.area = reg.area 
  AND reg.region = v.region
/

COMMENT ON TABLE V_REGION IS 'This view will be used to display the Region LOVs using a security policy to filter User access. The region name is translated based on user language.'
/
