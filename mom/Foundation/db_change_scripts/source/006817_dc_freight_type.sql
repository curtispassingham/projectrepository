--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_FREIGHT_TYPE
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_FREIGHT_TYPE"
CREATE TABLE DC_FREIGHT_TYPE
(
   FREIGHT_TYPE        VARCHAR2(6),
   FREIGHT_TYPE_DESC   VARCHAR2(250)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_FREIGHT_TYPE is 'This table is a staging table for data conversion and will hold data for FREIGHT_TYPE which is attached to transportation records.'
/
COMMENT ON COLUMN DC_FREIGHT_TYPE.FREIGHT_TYPE is 'Contains the unique key that identifies the freight type record.'
/
COMMENT ON COLUMN DC_FREIGHT_TYPE.FREIGHT_TYPE_DESC is 'Contains the description of the freight type.'
/