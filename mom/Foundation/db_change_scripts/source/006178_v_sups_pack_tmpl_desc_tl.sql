CREATE OR REPLACE FORCE VIEW V_SUPS_PACK_TMPL_DESC_TL (SUPPLIER, PACK_TMPL_ID, SUPP_PACK_DESC, LANG ) AS
SELECT  b.supplier,
        b.pack_tmpl_id,
        case when tl.lang is not null then tl.supp_pack_desc else b.supp_pack_desc end supp_pack_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SUPS_PACK_TMPL_DESC b,
        SUPS_PACK_TMPL_DESC_TL tl
 WHERE  b.supplier = tl.supplier (+)
   AND  b.pack_tmpl_id = tl.pack_tmpl_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SUPS_PACK_TMPL_DESC_TL is 'This is the translation view for base table SUPS_PACK_TMPL_DESC. This view fetches data in user langauge either from translation table SUPS_PACK_TMPL_DESC_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SUPS_PACK_TMPL_DESC_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SUPS_PACK_TMPL_DESC_TL.SUPPLIER is 'This field holds the supplier number.'
/

COMMENT ON COLUMN V_SUPS_PACK_TMPL_DESC_TL.PACK_TMPL_ID is 'This field holds the pack template ID.'
/

COMMENT ON COLUMN V_SUPS_PACK_TMPL_DESC_TL.SUPP_PACK_DESC is 'This field contains the pack description of the suppliers pack template.'
/

