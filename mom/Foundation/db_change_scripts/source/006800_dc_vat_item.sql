--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_VAT_ITEM
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_VAT_ITEM"
CREATE TABLE DC_VAT_ITEM
(
   ITEM                   VARCHAR2(25),
   VAT_REGION             NUMBER(4),
   VAT_TYPE               VARCHAR2(1),
   VAT_CODE               VARCHAR2(6),
   VAT_RATE               NUMBER(20,10),
   ACTIVE_DATE            DATE,
   REVERSE_VAT_IND        VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_VAT_ITEM is 'This table is a staging table for data conversion and will hold item vat relationship.'
/
COMMENT ON COLUMN DC_VAT_ITEM.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_VAT_ITEM.VAT_REGION is 'Contains the number of the Value Added Tax region in which this store is contained.'
/
COMMENT ON COLUMN DC_VAT_ITEM.VAT_TYPE is 'Holds the type of vat either Retail or Cost or Both.'
/
COMMENT ON COLUMN DC_VAT_ITEM.VAT_CODE is 'This field contains the alphanumeric identification for the VAT code.'
/
COMMENT ON COLUMN DC_VAT_ITEM.VAT_RATE is 'VAT rate associated with a given VAT code.'
/
COMMENT ON COLUMN DC_VAT_ITEM.ACTIVE_DATE is 'Holds the active date of vat.'
/
COMMENT ON COLUMN DC_VAT_ITEM.REVERSE_VAT_IND is 'Indicates if the item is subject to reverse charge VAT at the vat region.'
/