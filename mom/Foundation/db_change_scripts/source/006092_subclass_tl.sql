--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       SUBCLASS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE SUBCLASS_TL(
LANG NUMBER(6) NOT NULL,
DEPT NUMBER(4) NOT NULL,
CLASS NUMBER(4) NOT NULL,
SUBCLASS NUMBER(4) NOT NULL,
SUB_NAME VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SUBCLASS_TL is 'This is the translation table for SUBCLASS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN SUBCLASS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SUBCLASS_TL.DEPT is 'Contains the department number of which the subclass is a member.'
/

COMMENT ON COLUMN SUBCLASS_TL.CLASS is 'Contains the class number of which the subclass is a member.'
/

COMMENT ON COLUMN SUBCLASS_TL.SUBCLASS is 'Contains the number which uniquely identifies the subclass.'
/

COMMENT ON COLUMN SUBCLASS_TL.SUB_NAME is 'Contains the name of the subclass which, along with the subclass number, uniquely identifies the subclass.'
/

COMMENT ON COLUMN SUBCLASS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN SUBCLASS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN SUBCLASS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN SUBCLASS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE SUBCLASS_TL ADD CONSTRAINT PK_SUBCLASS_TL PRIMARY KEY (
LANG,
DEPT,
CLASS,
SUBCLASS
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SUBCLASS_TL
 ADD CONSTRAINT SCLT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE SUBCLASS_TL ADD CONSTRAINT SCLT_SCL_FK FOREIGN KEY (
DEPT,
CLASS,
SUBCLASS
) REFERENCES SUBCLASS (
DEPT,
CLASS,
SUBCLASS
)
/

