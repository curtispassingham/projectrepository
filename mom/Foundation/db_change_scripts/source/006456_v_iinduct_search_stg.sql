--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Modify View: 		 V_IINDUCT_SEARCH_STG
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       Modify V_IINDUCT_SEARCH_STG               
--------------------------------------
PROMPT Creating View 'V_IINDUCT_SEARCH_STG'
CREATE OR REPLACE FORCE VIEW "V_IINDUCT_SEARCH_STG" 
("ITEM",
"ITEM_DESC", 
"ITEM_TYPE",
"DEPT", 
"CLASS",
"SUBCLASS",
"STATUS", 
"ITEM_LEVEL", 
"TRAN_LEVEL", 
"ORDERABLE_IND", 
"INVENTORY_IND",
"SELLABLE_IND", 
"LAST_UPD_ID",
"NEXT_UPD_ID", 
"PROCESS_ID", 
"SUPPLIER", 
"CREATE_DATETIME", 
"BRAND_NAME", 
"VPN",
"DEPT_NAME", 
"CLASS_NAME", 
"SUBCLASS_NAME",
"SUPPLIER_NAME",
"PROCESS_DESC" ) AS
WITH il AS (SELECT item,process_id,create_datetime 
              FROM svc_item_master                 
         UNION ALL
            SELECT item,process_id,create_datetime 
              FROM svc_item_master_tl             
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_item_supplier
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_item_country
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_item_country_l10n_ext
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_item_master_cfa_ext
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_item_supplier_cfa_ext
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_item_supp_country
         UNION ALL
            SELECT item,process_id,create_datetime
             FROM svc_item_supp_country_cfa_ext
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_item_supp_country_dim
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_item_supp_manu_country
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_item_supp_uom
         UNION ALL
            SELECT head_item,process_id,create_datetime
              FROM svc_item_xform_detail
         UNION ALL
            SELECT head_item,process_id,create_datetime
              FROM svc_item_xform_head
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_packitem
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_rpm_item_zone_price
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_uda_item_ff
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_uda_item_lov
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_uda_item_date
         UNION ALL
            SELECT item,process_id,create_datetime
              FROM svc_vat_item
         UNION ALL
           SELECT item,process_id,create_datetime
             FROM svc_item_image),                     
     im AS (SELECT il.item,
                   MAX(il.process_id) AS maxpid,
                   MAX(il.create_datetime)AS maxcd
               FROM il
           GROUP BY il.item),
      t AS (SELECT * 
              FROM (SELECT item,
                           item_desc,
                           row_number() over (partition by item order by source_rank) item_desc_rank
                      FROM (
                            SELECT b.item,
                                   b.item_desc,
                                   1 source_rank
                              FROM svc_item_master_tl b
                             WHERE lang = get_user_lang
                               AND NOT EXISTS (SELECT 'x' FROM svc_item_master a WHERE a.item = b.item AND rownum = 1)
                             UNION
                            SELECT a.item,
                                   NVL(b.item_desc,a.item_desc) item_desc,
                                   2 source_rank
                              FROM svc_item_master a,
                                  (SELECT item, item_desc FROM svc_item_master_tl WHERE lang = get_user_lang) b
                             WHERE a.item = b.item(+)
                             UNION
                            SELECT b.item,
                                   b.item_desc,
                                   3 source_rank 
                              FROM svc_item_master_tl b
                             WHERE lang != get_user_lang    
                               AND NOT EXISTS (SELECT 'x' FROM svc_item_master a WHERE a.item = b.item AND rownum = 1)
                             UNION
                            SELECT item,
                                   item_desc,
                                   4 source_rank
                              FROM v_item_master_tl a          
                             WHERE a.lang = get_user_lang
                               AND EXISTS (SELECT 'x' from im WHERE im.item = a.item AND rownum = 1)))
             WHERE item_desc_rank = 1)  
SELECT DISTINCT im.item,
       t.item_desc,
       CASE
        WHEN sim.simple_pack_ind = 'Y' THEN 
             'S'
        WHEN sim.pack_ind = 'Y' AND sim.simple_pack_ind = 'N' THEN
             'C'
        WHEN sim.deposit_item_type is not null THEN
             sim.deposit_item_type
        WHEN sim.item_xform_ind = 'Y' AND sim.orderable_ind = 'Y' THEN 
             'O'
        WHEN sim.item_xform_ind = 'Y' AND sim.orderable_ind = 'N' THEN 
             'L'
        WHEN dep.purchase_type IN (1, 2) THEN 
             'I'
        ELSE 'R'
       END item_type,
       sim.dept,
       sim.class,
       sim.subclass,
       sim.status,
       sim.item_level,
       sim.tran_level,
       sim.orderable_ind,
       sim.inventory_ind,
       sim.sellable_ind,
       (select user_id from svc_process_tracker where process_id = maxpid) as last_upd_id,
       sim.next_upd_id,
       im.maxpid as process_id,
       sis.supplier,    
       im.maxcd as create_datetime,
       sim.brand_name,
       sis.vpn,
       (SELECT dept_name from v_deps_tl dtl where dtl.dept = sim.dept) dept_name,
       (SELECT cls.class_name from v_class_tl cls where cls.class = sim.class and cls.dept = sim.dept) class_name,
       (SELECT scls.sub_name from v_subclass_tl scls where scls.subclass= sim.subclass and scls.class = sim.class and scls.dept = sim.dept) subclass_name,
       (SELECT sup_name from v_sups_tl s where s.supplier = sis.supplier) supplier_name,
       (SELECT process_desc from svc_process_tracker spt where spt.process_id = im.maxpid) process_desc
  FROM im,
       t,
       svc_item_master sim,
       (select item,    
               supplier,
               vpn,
               primary_supp_ind,
               create_datetime,
               last_upd_id,
               process_id, 
               rank() over(partition by item order by primary_supp_ind desc) rank 
          from svc_item_supplier) sis,
       deps dep
 WHERE im.item = t.item (+)  
   AND im.item = sim.item (+)
   AND im.item = sis.item(+)
   AND im.maxpid = sim.process_id (+)
   AND im.maxpid = sis.process_id (+)
   AND sis.rank(+) = 1
   AND sim.dept = dep.dept(+);
   
   
COMMENT ON TABLE V_IINDUCT_SEARCH_STG IS 'This view is used for the item induct screen in RMS Alloy. It contains fields to be displayed in the search result when Item Upload/Download from staging option is selected.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.ITEM IS 'Item Id.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.ITEM_DESC IS 'Item description.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.ITEM_TYPE IS 'Derived attribute of item type based on item attributes to indicate if the item is a regular item, pack, simple pack, transform orderable, transform sellable, a consignment or concession item, or a type of deposit item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.DEPT IS 'The merchandise department of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.CLASS IS 'The merchandise class of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.SUBCLASS IS 'The merchandise subclass of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.STATUS IS 'Item status. If an item or its parent or grandparent is on daily purge, the item is in D (deleted) status.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.ITEM_LEVEL IS 'Item level.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.TRAN_LEVEL IS 'Transaction level.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.ORDERABLE_IND IS 'Item orderable indicator.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.INVENTORY_IND IS 'Item inventory indicator.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.SELLABLE_IND IS 'Item sellable indicator.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.LAST_UPD_ID IS 'User who last updated the record.';  
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.NEXT_UPD_ID IS 'User who is expected to work next on this item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.SUPPLIER IS 'The supplier of the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.CREATE_DATETIME IS 'Date time when record was inserted.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.BRAND_NAME IS 'Brand associated to the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.VPN IS 'The Vendor Product Number associated with the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.DEPT_NAME IS 'The merchandise department name of the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.CLASS_NAME IS 'The merchandise class name of the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.SUBCLASS_NAME IS 'The merchandise subclass name of the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.SUPPLIER_NAME IS 'The name of the supplier.';   
COMMENT ON COLUMN V_IINDUCT_SEARCH_STG.PROCESS_DESC IS 'The process description.';