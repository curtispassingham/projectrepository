--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_ITEM_INDUCT_CONFIG
----------------------------------------------------------------------------
whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_ITEM_INDUCT_CONFIG'
CREATE TABLE SVC_ITEM_INDUCT_CONFIG
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
    MAX_ITEMS_FOR_DNLD NUMBER(10,0) ,
    MAX_ITEMS_FOR_SYNC_DNLD NUMBER(10,0) ,
    MAX_FILE_SIZE_FOR_UPLD NUMBER(10,0) ,
    MAX_FILE_SIZE_FOR_SYNC_UPLD NUMBER(10,0) ,
    MAX_CC_FOR_SYNC_DNLD NUMBER(10,0) ,
    MAX_CC_FOR_DNLD NUMBER(10,0) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE 
	)
	INITRANS 6
	TABLESPACE RETAIL_DATA
/


COMMENT ON TABLE SVC_ITEM_INDUCT_CONFIG IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in ITEM_INDUCT_CONFIG.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.MAX_ITEMS_FOR_DNLD IS 'Maximum number of items that can be downloaded using spreadsheet.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.MAX_ITEMS_FOR_SYNC_DNLD IS 'Maximum number of items that can be downloaded using spreadsheet in synchronous fashion. Beyond this threshold the process is submitted as an asynchronous process.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.MAX_FILE_SIZE_FOR_UPLD IS 'Maximum file size that can be uploaded.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.MAX_FILE_SIZE_FOR_SYNC_UPLD IS 'Maximum file size that can be uploaded in synchronous fashion. Beyond this threshold the process is submitted as an asynchronous process.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.MAX_CC_FOR_SYNC_DNLD IS 'Maximum number of cost changes allowed for synchronous download.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.MAX_CC_FOR_DNLD IS 'Maximum number of cost changes allowed for download.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_ITEM_INDUCT_CONFIG.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/

PROMPT Creating Primary Key on 'SVC_ITEM_INDUCT_CONFIG'           
ALTER TABLE SVC_ITEM_INDUCT_CONFIG
ADD CONSTRAINT SVC_ITEM_INDUCT_CONFIG_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX 
/


	
	
	
