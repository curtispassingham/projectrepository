--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	View Modified:						v_item_search
---------------------------------------------------------------------------

whenever sqlerror exit

-------------------------------------------
--       MODIFYING VIEW
-------------------------------------------
CREATE OR REPLACE FORCE VIEW V_ITEM_SEARCH
("ITEM", 
 "ITEM_DESC", 
 "ITEM_DESC_SECONDARY", 
 "ITEM_LEVEL", 
 "ITEM_LEVEL_DESC", 
 "TRAN_LEVEL", 
 "TRAN_LEVEL_DESC", 
 "ITEM_TYPE", 
 "SUPPLIER", 
 "VPN", 
 "DEPT1", 
 "DEPT_NAME", 
 "CLASS1", 
 "CLASS_NAME", 
 "SUBCLASS1", 
 "SUB_NAME", 
 "STATUS", 
 "DIVISION", 
 "GROUP_NO", 
 "ITEM_GRANDPARENT", 
 "ITEM_PARENT", 
 "SHORT_DESC", 
 "ITEM_NUMBER_TYPE", 
 "ITEM_NUMBER_TYPE_DESC", 
 "SELLABLE_IND", 
 "ORDERABLE_IND", 
 "INVENTORY_IND", 
 "PACK_IND", 
 "PACK_TYPE", 
 "DIFF_1", 
 "DIFF_2", 
 "DIFF_3", 
 "DIFF_4", 
 "ORIGINAL_RETAIL", 
 "MFG_REC_RETAIL", 
 "MERCHANDISE_IND", 
 "FORECAST_IND", 
 "ITEM_XFORM_IND", 
 "CATCH_WEIGHT_IND", 
 "DEPOSIT_ITEM_TYPE",
 "PACK_TYPE_DESC", 
 "DEPOSIT_ITEM_TYPE_DESC")
AS 
  (SELECT item.item,
          item.item_desc,
          item.item_desc_secondary,
          item.item_level,
          (SELECT code_desc FROM v_code_detail ilvs WHERE item.item_level = ilvs.code AND ilvs.code_type = 'ILVS') AS item_level_desc,
          item.tran_level,
          (SELECT code_desc FROM v_code_detail tlvl WHERE item.tran_level=tlvl.code AND tlvl.code_type ='TLVL') AS tran_level_desc,
          CASE
             WHEN item.simple_pack_ind = 'Y' THEN 
                  'S'
             WHEN item.pack_ind = 'Y' AND item.simple_pack_ind = 'N' THEN
                  'C'
             WHEN item.deposit_item_type IS NOT NULL THEN
                  item.deposit_item_type
             WHEN item.item_xform_ind = 'Y' AND item.orderable_ind = 'Y' THEN 
                  'O'
             WHEN item.item_xform_ind = 'Y' AND item.orderable_ind = 'N' THEN 
                  'L'
             WHEN deps.purchase_type IN (1, 2) THEN 
                  'I'
             ELSE 'R'
           END item_type,
           supp.supplier,
           supp.vpn,
           dtl.dept AS dept1,
           dtl.dept_name,
           ctl.class AS class1,
           ctl.class_name,
           stl.subclass AS subclass1,
           stl.sub_name,
           DECODE((SELECT 'Y' 
                     FROM daily_purge WHERE upper(table_name) = 'ITEM_MASTER'
                      AND (key_value = item.item
                       OR key_value = item.item_parent
                       OR key_value = item.item_grandparent)
                      AND rownum =1), 'Y', 'D', item.status) status,
           g.division,
           deps.group_no,
           item.item_grandparent,
           item.item_parent,
           item.short_desc,
           item.item_number_type,
           (SELECT code_desc FROM v_code_detail upct WHERE item.item_number_type = upct.code AND upct.code_type = 'UPCT') item_number_type_desc,
           item.sellable_ind,
           item.orderable_ind,
           item.inventory_ind,
           item.pack_ind,
           item.pack_type,
           item.diff_1,
           item.diff_2,
           item.diff_3,
           item.diff_4,
           item.original_retail,
           item.mfg_rec_retail,
           item.merchandise_ind,
           item.forecast_ind,
           item.item_xform_ind,
           item.catch_weight_ind,
           item.deposit_item_type,
           (SELECT code_desc FROM v_code_detail pctp WHERE item.pack_type = pctp.code AND pctp.code_type = 'PATP') pack_type_desc,
           (SELECT code_desc FROM v_code_detail itmt WHERE item.deposit_item_type=itmt.code AND itmt.code_type ='ITMT') AS deposit_item_type_desc
  FROM v_item_master item,
       (SELECT item_supplier.*
          FROM item_supplier,
               v_sups
         WHERE item_supplier.supplier = v_sups.supplier
           AND primary_supp_ind = 'Y') supp,
       v_deps_tl dtl,
       v_class_tl ctl,
       v_subclass_tl stl,
       deps,
       groups g
  WHERE item.item = supp.item(+)
    AND item.dept = dtl.dept
    AND item.dept = ctl.dept
    AND item.class = ctl.class
    AND item.dept = stl.dept
    AND item.class = stl.class
    AND item.subclass = stl.subclass
    AND deps.dept = item.dept
    AND deps.group_no = g.group_no
  );

  COMMENT ON TABLE V_ITEM_SEARCH
IS
  'This view is used for the item search screen in RMS Alloy. It includes all fields that can be used as item search criteria along with their descriptions to be displayed in the search result except for unit_cost.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ITEM
IS
  'Item Id.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ITEM_DESC
IS
  'Item description.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ITEM_DESC_SECONDARY
IS
  'Item secondary description.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ITEM_LEVEL
IS
  'Item level.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ITEM_LEVEL_DESC
IS
  'Translated description of the item level.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.TRAN_LEVEL
IS
  'Transaction level.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.TRAN_LEVEL_DESC
IS
  'Translated description of the transaction level.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ITEM_TYPE
IS
  'Derived attribute of item type based on item attributes to indicate if the item is a regular item, pack, simple pack, transform orderable, transform sellable, a consignment or concession item, or a type of deposit item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.SUPPLIER
IS
  'The primary supplier of an item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.VPN
IS
  'The vpn associated with the primary supplier of an item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.DEPT1
IS
  'The merchandise department of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.DEPT_NAME
IS
  'The merchandise department name of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.CLASS1
IS
  'The merchandise class of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.CLASS_NAME
IS
  'The merchandise class name of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.SUBCLASS1
IS
  'The merchandise subclass of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.SUB_NAME
IS
  'The merchandise subclass name of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.STATUS
IS
  'Item status. If an item or its parent or grandparent is on daily purge, the item is in D (deleted) status.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.DIVISION
IS
  'The merchandise division of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.GROUP_NO
IS
  'The merchandise group of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ITEM_GRANDPARENT
IS
  'Item grandparent.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ITEM_PARENT
IS
  'Item parent.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.SHORT_DESC
IS
  'Item short description.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ITEM_NUMBER_TYPE
IS
  'Item number type.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ITEM_NUMBER_TYPE_DESC
IS
  'Item number type description.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.SELLABLE_IND
IS
  'Item sellable indicator.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ORDERABLE_IND
IS
  'Item orderable indicator.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.INVENTORY_IND
IS
  'Item inventory indicator.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.PACK_IND
IS
  'Item pack indicator.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.PACK_TYPE
IS
  'Pack type of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.DIFF_1
IS
  'Differentiator 1 of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.DIFF_2
IS
  'Differentiator 2 of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.DIFF_3
IS
  'Differentiator 3 of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.DIFF_4
IS
  'Differentiator 4 of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ORIGINAL_RETAIL
IS
  'The original retail of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.MFG_REC_RETAIL
IS
  'The manufacturer recommended retail of the item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.MERCHANDISE_IND
IS
  'The merchandise indicator.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.FORECAST_IND
IS
  'The forecast indicator.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.ITEM_XFORM_IND
IS
  'The transformable item indicator.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.CATCH_WEIGHT_IND
IS
  'The catch weight item indicator.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.DEPOSIT_ITEM_TYPE
IS
  'The deposit type of a deposit item.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.PACK_TYPE_DESC
IS
  'The translated description of the pack type.' ;
  COMMENT ON COLUMN V_ITEM_SEARCH.DEPOSIT_ITEM_TYPE_DESC
IS
  'The translated description of item''s deposit item type.' ;

