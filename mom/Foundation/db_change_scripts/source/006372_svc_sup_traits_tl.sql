--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_SUP_TRAITS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

CREATE TABLE SVC_SUP_TRAITS_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, SUP_TRAIT NUMBER(4)
, DESCRIPTION VARCHAR2(120)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_SUP_TRAITS_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in SUP_TRAITS_TL.'
/

COMMENT ON COLUMN SVC_SUP_TRAITS_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_SUP_TRAITS_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_SUP_TRAITS_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_SUP_TRAITS_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_SUP_TRAITS_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_SUP_TRAITS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_SUP_TRAITS_TL.SUP_TRAIT is 'This field contains the number that uniquely identifies the supplier trait.'
/

COMMENT ON COLUMN SVC_SUP_TRAITS_TL.DESCRIPTION is 'This field contains the description associated with the supplier trait.'
/

ALTER TABLE SVC_SUP_TRAITS_TL
ADD CONSTRAINT SVC_SUP_TRAITS_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SVC_SUP_TRAITS_TL  
ADD CONSTRAINT SVC_SUP_TRAITS_TL_UK UNIQUE
(LANG, SUP_TRAIT)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

