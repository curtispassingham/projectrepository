--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_WF_CUSTOMER
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_WF_CUSTOMER"
CREATE TABLE DC_WF_CUSTOMER
(
   WF_CUSTOMER_ID         NUMBER(10),
   WF_CUSTOMER_NAME       VARCHAR2(120),
   CREDIT_IND             VARCHAR2(1),
   WF_CUSTOMER_GROUP_ID   NUMBER(10),
   AUTO_APPROVE_IND       VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_WF_CUSTOMER is 'This table is a staging table for data conversion and will hold data for WF_CUSTOMER.'
/
COMMENT ON COLUMN DC_WF_CUSTOMER.WF_CUSTOMER_ID is 'This field will hold the unique identifier for the customer.'
/
COMMENT ON COLUMN DC_WF_CUSTOMER.WF_CUSTOMER_NAME is 'This field will hold the customer description.'
/
COMMENT ON COLUMN DC_WF_CUSTOMER.CREDIT_IND is 'This field will determine if the customer has good credit.'
/
COMMENT ON COLUMN DC_WF_CUSTOMER.WF_CUSTOMER_GROUP_ID is 'This field will indicate which customer group this customer belongs to.'
/
COMMENT ON COLUMN DC_WF_CUSTOMER.AUTO_APPROVE_IND is 'This indicator is used to auto approve the externally uploaded orders and returns if all the validations are passed. Valid values are Y and N'
/