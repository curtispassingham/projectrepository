CREATE OR REPLACE FORCE VIEW V_WH_TL (WH, WH_NAME, WH_NAME_SECONDARY, LANG ) AS
SELECT  b.wh,
        case when tl.lang is not null then tl.wh_name else b.wh_name end wh_name,
        case when tl.lang is not null then tl.wh_name_secondary else b.wh_name_secondary end wh_name_secondary,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  WH b,
        WH_TL tl
 WHERE  b.wh = tl.wh (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_WH_TL is 'This is the translation view for base table WH. This view fetches data in user langauge either from translation table WH_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_WH_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_WH_TL.WH is 'Contains the number which uniquely identifies the warehouse. The wh table stores all warehouses in the system.  Both virtual and physical warehouses will be stored on this table.  The addition of the new column, physical_wh, helps determine which warehouses are physical and which are virtual.  All physical warehouses will have a physical_wh column value equal to their wh number.  Virtual warehouses will have a valid physical warehouse in this column.'
/

COMMENT ON COLUMN V_WH_TL.WH_NAME is 'Contains the name of the warehouse which, along with the warehouse number, identifies the warehouse.'
/

COMMENT ON COLUMN V_WH_TL.WH_NAME_SECONDARY is 'Secondary name of the warehouse.'
/

