--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_ITEM_SUPP_COUNTRY_DIM
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_ITEM_SUPP_COUNTRY_DIM"
CREATE TABLE DC_ITEM_SUPP_COUNTRY_DIM
(
   ITEM                          VARCHAR2(25),
   SUPPLIER                      NUMBER(10),
   ORIGIN_COUNTRY_ID             VARCHAR2(3),
   DIM_OBJECT                    VARCHAR2(6),
   PRESENTATION_METHOD           VARCHAR2(6),
   LENGTH                        NUMBER(12,4),
   WIDTH                         NUMBER(12,4),
   HEIGHT                        NUMBER(12,4),
   LWH_UOM                       VARCHAR2(4),
   WEIGHT                        NUMBER(12,4),
   NET_WEIGHT                    NUMBER(12,4),
   WEIGHT_UOM                    VARCHAR2(4),
   LIQUID_VOLUME                 NUMBER(12,4),
   LIQUID_VOLUME_UOM             VARCHAR2(4),
   STAT_CUBE                     NUMBER(12,4),
   TARE_WEIGHT                   NUMBER(12,4),
   TARE_TYPE                     VARCHAR2(6)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_ITEM_SUPP_COUNTRY_DIM is 'This table is a staging table for data conversion and will hold data of ITEM_SUPP_COUNTRY_DIM table.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.SUPPLIER is 'The unique identifier for the supplier.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY_ID is 'The country where the item was manufactured or significantly altered.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT is 'Specific object whose dimensions are specified in this record (e.g. case, pallet, each).'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.PRESENTATION_METHOD is 'Describes the packaging (if any) being taken into consideration in the specified dimensions.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.LENGTH is 'Length of dim_object measured in units specified in lwh_uom.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.WIDTH is 'Width of dim_object measured in units specified in lwh_uom.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.HEIGHT is 'Height of dim_object measured in units specified in lwh_uom.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.LWH_UOM is 'Unit of measurement for length, width, and height (e.g. inches, centimeters, feet).'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.WEIGHT is 'Weight of dim_object measured in units specified in weight_uom.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT is 'Net weight of the dim_object (weight without packaging) measured in units specified in weight_uom.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM is 'Unit of measurement for weight (e.g. pounds, kilograms).'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME is 'Liquid volume, or capacity, of dim_object measured in units specified in volume_uom.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM is 'Unit of measurement for liquid_volume (e.g. ounces, liters). Liquid volumes are only convertible to other liquid volumes.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.STAT_CUBE is 'Statistical value of the dim_objects dimensions to be used for loading purposes.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.TARE_WEIGHT is 'Amount of weight to be subtracted for packaging materials.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY_DIM.TARE_TYPE is 'Indicates if tare weight for this dim_object is wet or dry.'
/