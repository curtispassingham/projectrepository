--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Creating View  V_LOC_LIST_HEAD 
--------------------------------------
PROMPT Creating View 'V_LOC_LIST_HEAD'
CREATE OR REPLACE FORCE VIEW V_LOC_LIST_HEAD ("LOC_LIST", "LOC_LIST_DESC" ,"FILTER_ORG_ID")
 AS SELECT LLH.LOC_LIST "LOC_LIST"
          ,V.LOC_LIST_DESC "LOC_LIST_DESC"
          ,LLH.FILTER_ORG_ID "FILTER_ORG_ID"
     FROM LOC_LIST_HEAD LLH,
          V_LOC_LIST_HEAD_TL V
    WHERE LLH.LOC_LIST = V.LOC_LIST
      AND (    EXISTS (SELECT 1
                         FROM LOC_LIST_DETAIL LLD
                        WHERE LLD.LOC_LIST = LLH.LOC_LIST
                          AND LLD.LOCATION IN (SELECT STORE FROM V_STORE UNION SELECT WH FROM V_WH))
        OR NOT EXISTS (SELECT 1
                         FROM LOC_LIST_DETAIL LLD
                        WHERE LLD.LOC_LIST = LLH.LOC_LIST))
/

COMMENT ON TABLE V_LOC_LIST_HEAD IS 'This view will be used to display the Location List LOVs using a security policy to filter User access.'
/

COMMENT ON COLUMN V_LOC_LIST_HEAD.LOC_LIST IS 'Contains a number to uniquely identify a location list.'
/

COMMENT ON COLUMN V_LOC_LIST_HEAD.LOC_LIST_DESC IS 'Contains the location list description translated based on user language.'
/

COMMENT ON COLUMN V_LOC_LIST_HEAD.FILTER_ORG_ID IS 'The ID of the Organizational Hierarchy that the Location List is assigned to.   This field will be used to control the Location Lists a user can see in the Location List LOV when the link between the user and the Organizational Hierarchy has been established.'
/

