--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW ADDED:          V_BANNER
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING VIEW
--------------------------------------
PROMPT Creating View 'V_BANNER'
CREATE OR REPLACE FORCE VIEW V_BANNER
 (BANNER_ID
 ,BANNER_NAME)
 AS SELECT BAN.BANNER_ID BANNER_ID
        ,V.BANNER_NAME BANNER_NAME
FROM BANNER BAN,
     V_BANNER_TL V
WHERE BAN.BANNER_ID = V.BANNER_ID     
/


COMMENT ON TABLE V_BANNER IS 'This view will be used to display the Banner LOVs with banner name translated based on user language.'
/

