--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_CORESVC_ITEM_CONFIG
----------------------------------------------------------------------------
whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_CORESVC_ITEM_CONFIG'
CREATE TABLE SVC_CORESVC_ITEM_CONFIG
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
    CASCADE_IUD_ITEM_SUPPLIER VARCHAR2(1) ,
    MAX_THREADS NUMBER(10,0) ,
    WAIT_BTWN_THREADS NUMBER(10,0) ,
    CASCADE_IIM_DETAILS VARCHAR2(1) ,
    MAX_CHUNK_SIZE NUMBER(10,0) ,
    PROC_ERR_RETENTION_DAYS NUMBER(5,0) ,
    CASCADE_UDA_DETAILS VARCHAR2(1) ,
    CASCADE_IUD_ITEM_SUPP_COUNTRY VARCHAR2(1) ,
    CASCADE_IUD_ISC_DIMENSIONS VARCHAR2(1) ,
    CASCADE_IUD_ISMC VARCHAR2(1) ,
    ISC_UPDATE_ALL_LOCS VARCHAR2(1) ,
    ISC_UPDATE_ALL_CHILD_LOCS VARCHAR2(1) ,
    CASCADE_VAT_ITEM VARCHAR2(1) ,
    MAX_ITEM_RESV_QTY NUMBER(4,0) ,
    MAX_ITEM_EXPIRY_DAYS NUMBER(4,0) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE
	)
	INITRANS 6
	TABLESPACE RETAIL_DATA
/


COMMENT ON TABLE SVC_CORESVC_ITEM_CONFIG IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in CORESVC_ITEM_CONFIG.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.MAX_CHUNK_SIZE IS 'The maximum number of items that should be processed in one chunk.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.PROC_ERR_RETENTION_DAYS IS 'Number of days the errors for a process should be retained before purge.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.CASCADE_UDA_DETAILS IS 'This field indicates whether the inserts, updates or deletes on uda details should be cascaded to the child items or not.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.CASCADE_IUD_ITEM_SUPP_COUNTRY IS 'This field indicates whether the inserts, updates or deletes on item/supplier/country details should be cascaded to the child items or not.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.CASCADE_IUD_ISC_DIMENSIONS IS 'This field indicates whether the inserts, updates or deletes on item/supplier/country/dimension details should be cascaded to the child items or not.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.CASCADE_IUD_ISMC IS 'This field indicates whether the inserts, updates or deletes on item/supplier/manufacturing country details should be cascaded to the child items or not.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.ISC_UPDATE_ALL_LOCS IS 'This field indicates whether the updated default location information should be cascaded to all locations for the item/supplier/country relationship or not.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.ISC_UPDATE_ALL_CHILD_LOCS IS 'This field indicates whether the updated default locaiton ifnormation should be cascaded to all child item locations for the item/supplier/country relationship or not.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.CASCADE_VAT_ITEM IS 'This field indicates whether the inserts or deletes of item VAT information should be cascaded to the child items or not.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.MAX_THREADS IS 'This is the maximum number of threads that should be spawned for coresvc_item package.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.MAX_ITEM_RESV_QTY IS 'The maximum number of item numbers that can be reserved.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.MAX_ITEM_EXPIRY_DAYS IS 'The number of days before the reserved item number expires.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.CASCADE_IUD_ITEM_SUPPLIER IS 'This field indicates whether the inserts, updates or deletes on item/supplier details should be cascaded to the child items or not.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_CORESVC_ITEM_CONFIG.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/

PROMPT Creating Primary Key on 'SVC_CORESVC_ITEM_CONFIG'       
ALTER TABLE SVC_CORESVC_ITEM_CONFIG
ADD CONSTRAINT SVC_CORESVC_ITEM_CONFIG_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX 
/
