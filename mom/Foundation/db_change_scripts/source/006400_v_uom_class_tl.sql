--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      VIEW ADDED:                             V_UOM_CLASS_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING VIEW
--------------------------------------
PROMPT Creating View 'V_UOM_CLASS_TL'
CREATE OR REPLACE FORCE VIEW V_UOM_CLASS_TL
 (UOM,
  UOM_CLASS,
  UOM_TRANS,
  UOM_DESC_TRANS,
  LANG,
  ISO_CODE)
  AS
  SELECT USE_THIS.UOM,
         USE_THIS.UOM_CLASS,
         USE_THIS.UOM_TRANS,
         USE_THIS.UOM_DESC_TRANS,
         USE_THIS.LANG,
         USE_THIS.ISO_CODE
    FROM (SELECT B.UOM UOM,
                 B.UOM_CLASS UOM_CLASS,
                 T.UOM_TRANS UOM_TRANS,
                 T.UOM_DESC_TRANS UOM_DESC_TRANS,
                 T.LANG LANG,
                 RANK() OVER(PARTITION BY B.UOM ORDER BY L.PRIORITY) RANK,
                 L.ISO_CODE ISO_CODE
            FROM UOM_CLASS B,
                 UOM_CLASS_TL T,
                 (SELECT LANG,
                         1 PRIORITY,
                         ISO_CODE
                    FROM LANG
                   WHERE LANG = GET_USER_LANG()
                   UNION
                  SELECT LANG,
                         2 PRIORITY,
                         ISO_CODE 
                    FROM LANG
                   WHERE LANG = GET_PRIMARY_LANG()) L
           WHERE B.UOM = T.UOM
             AND L.LANG   = T.LANG) USE_THIS
 WHERE RANK = 1
/

COMMENT ON COLUMN V_UOM_CLASS_TL.UOM IS 'Contains a string that uniquely identifies the unit of measure. Example:  LBS for pounds.'
/
COMMENT ON COLUMN V_UOM_CLASS_TL.UOM_CLASS IS 'Contains the unit of measure type used as a grouping mechanism for the many UOM options.  When converting from one UOM to another, the class is used to determine how the system proceeds with the conversion, whether it is an in-class or across-class conversion.'
/
COMMENT ON COLUMN V_UOM_CLASS_TL.UOM_TRANS IS 'Translated Unit Of Measurement.'
/
COMMENT ON COLUMN V_UOM_CLASS_TL.UOM_DESC_TRANS IS 'Translated UOM description.'
/
COMMENT ON COLUMN V_UOM_CLASS_TL.LANG IS 'Contains the number which uniquely identifies a language in RMS.'
/
COMMENT ON COLUMN V_UOM_CLASS_TL.ISO_CODE IS 'This field holds the ISO code associated with the given language.'
/
COMMENT ON TABLE  V_UOM_CLASS_TL IS 'This translation view returns descriptions of Unit Of Measurement within Oracle Retail in user language. If the description is not defined in user language, it returns the description in the system data integration language.'
/
