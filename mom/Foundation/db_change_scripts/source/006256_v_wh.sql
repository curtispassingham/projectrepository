--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               v_wh
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_WH'
CREATE OR REPLACE FORCE VIEW V_WH ( WH_NAME, WH_NAME_SECONDARY, WH, CURRENCY_CODE, PHYSICAL_WH, PRIMARY_WH, STOCKHOLDING_IND, REPL_IND, IB_IND, ORG_UNIT_ID, VAT_REGION, CHANNEL_ID, TSF_ENTITY_ID, CUSTOMER_ORDER_LOC_IND, EMAIL, DUNS_NUMBER, DUNS_LOC, CREATE_ID, CREATE_DATETIME )
AS
  SELECT V.WH_NAME WH_NAME ,
    V.WH_NAME_SECONDARY WH_NAME_SECONDARY ,
    WAH.WH WH ,
    WAH.CURRENCY_CODE CURRENCY_CODE ,
    WAH.PHYSICAL_WH PHYSICAL_WH ,
    WAH.PRIMARY_VWH PRIMARY_WH ,
    WAH.STOCKHOLDING_IND STOCKHOLDING_IND ,
    WAH.REPL_IND REPL_IND ,
    WAH.IB_IND IB_IND ,
    WAH.ORG_UNIT_ID ORG_UNIT_ID
    --
    ,
    WAH.VAT_REGION ,
    WAH.CHANNEL_ID ,
    WAH.TSF_ENTITY_ID ,
    WAH.CUSTOMER_ORDER_LOC_IND ,
    WAH.EMAIL ,
    WAH.DUNS_NUMBER ,
    WAH.DUNS_LOC ,
    WAH.CREATE_ID ,
    WAH.CREATE_DATETIME
  FROM WH WAH,
       V_WH_TL V
  WHERE WAH.FINISHER_IND = 'N' 
    AND V.WH = WAH.WH;
/

  COMMENT ON TABLE V_WH
IS
  'This view will be used to display the Warehouse LOVs using a security policy to filter User access.' 
  /
  COMMENT ON COLUMN V_WH.WH_NAME
IS
  'Contains the name of the warehouse which, along with the warehouse number, identifies the warehouse.' 
  /
  COMMENT ON COLUMN V_WH.WH_NAME_SECONDARY
IS
  'Secondary name of the warehouse.' 
  /
  COMMENT ON COLUMN V_WH.WH
IS
  'Contains the number which uniquely identifies the warehouse. The wh table stores all warehouses in the system.  Both virtual and physical warehouses will be stored on this table.  The addition of the new column, physical_wh, helps determine which warehouses are physical and which are virtual.  All physical warehouses will have a physical_wh column value equal to their wh number.  Virtual warehouses will have a valid physical warehouse in this column.' 
  /
  COMMENT ON COLUMN V_WH.CURRENCY_CODE
IS
  'This field contains the currency code under which the warehouse operates.' 
  /
  COMMENT ON COLUMN V_WH.PHYSICAL_WH
IS
  'This column will contain the number of the physical warehouse that is assigned to the virtual warehouse.' 
  /
  COMMENT ON COLUMN V_WH.PRIMARY_WH
IS
  'This field holds the virtual warehouse that will used as the basis for all transactions for which only a physical warehouse and not a virtual warehouse has not been specified.' 
  /
  COMMENT ON COLUMN V_WH.STOCKHOLDING_IND
IS
  'This column will indicate if the warehouse is a stock holding location.  In a non-multichannel environment, this will always be Y.     In a multichannel environment it will be N for a physical warehouse and Y for a virtual warehouse.' 
  /
  COMMENT ON COLUMN V_WH.REPL_IND
IS
  'This indicator determines if a warehouse is replenishable.' 
  /
  COMMENT ON COLUMN V_WH.IB_IND
IS
  'This field indicates if the warehouse is an investment buy warehouse.' 
  /
  COMMENT ON COLUMN V_WH.ORG_UNIT_ID
IS
  'this column will hold the oracle oraganizational unit id value.' 
  /
  COMMENT ON COLUMN V_WH.VAT_REGION
IS
  'warehouse is located.' 
  /
  COMMENT ON COLUMN V_WH.CHANNEL_ID
IS
  'This column will contain the channel for which the virtual warehouse will be assigned.' 
  /
  COMMENT ON COLUMN V_WH.TSF_ENTITY_ID
IS
  'ID of the transfer entity with which this warehouse is associated. Valid values are found on the TSF_ENTITY table. A transfer entity is a group of locations that share legal requirements around product management.' 
  /
  COMMENT ON COLUMN V_WH.CUSTOMER_ORDER_LOC_IND
IS
  'This Column determines if the location is customer order location or not, i.e. if the indicator is checked then the location can be used by OMS for sourcing/ fulfillment or both else it cannot be used.   It is enabled only for virtual warehouses. For a physical warehouse, this column would be NULL. For virtual warehouses, the valid values are ''Y'' or ''N''.' 
  /
  COMMENT ON COLUMN V_WH.EMAIL
IS
  'Holds the email address for the location' 
  /
  COMMENT ON COLUMN V_WH.DUNS_NUMBER
IS
  'This field holds the Dun and Bradstreet number to identify the warehouse' 
  /
  COMMENT ON COLUMN V_WH.DUNS_LOC
IS
  'This field holds the Dun and Bradstreet number to identify the location' 
  /
  COMMENT ON COLUMN V_WH.CREATE_ID
IS
  'This column holds the User id of the user who created the record.' 
  /
  COMMENT ON COLUMN V_WH.CREATE_DATETIME
IS
  'This column holds the record creation date.' 
  /
