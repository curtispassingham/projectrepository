--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


-----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
---------------------------------------------------------------------------------------
--Materialized View Created:                               CLASS_SALES_FORECAST
---------------------------------------------------------------------------------------

whenever sqlerror exit
PROMPT create materialized view CLASS_SALES_FORECAST
CREATE MATERIALIZED VIEW CLASS_SALES_FORECAST
ON PREBUILT TABLE 
WITH REDUCED PRECISION
REFRESH FORCE ON DEMAND 
ENABLE QUERY REWRITE
           AS SELECT fss.dept dept,
                     fss.class class,
                     1 domain_id,
                     fss.loc loc,
                     fss.eow_date eow_date,
                     SUM(fss.forecast_sales) forecast_sales
                FROM subclass_sales_forecast fss,
                     store s
               WHERE fss.loc       = s.store
            GROUP BY fss.dept, fss.class, fss.loc, fss.eow_date
/