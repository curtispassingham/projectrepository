--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_MERCH_HIER
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_MERCH_HIER'
CREATE OR REPLACE FORCE VIEW V_MERCH_HIER ("EFFECTIVE_DATE", "HIER_TYPE", "HIERARCHY_ID", "HIERARCHY_NAME", "PARENT_HIERARCHY_ID", "GRANDPARENT_HIERARCHY_ID") AS 
  SELECT p.vdate EFFECTIVE_DATE,
       'V' hier_type,
       div.division hierarchy_id,
       v.div_name hierarchy_name,
       NULL parent_hierarchy_id,
       NULL grandparent_hierarchy_id
  FROM division div,
       v_division_tl v,
       period p
 WHERE v.division = div.division
 UNION ALL
SELECT p.vdate EFFECTIVE_DATE,
       'G' hier_type,
       grp.group_no hierarchy_id,
       v.group_name hierarchy_name,
       grp.division parent_hierarchy_id,
       NULL grandparent_hierarchy_id
  FROM GROUPS grp,
       v_groups_tl v,
       period p
 WHERE v.group_no = grp.group_no
 UNION ALL
SELECT p.vdate EFFECTIVE_DATE,
       'D' hier_type,
       dps.dept hierarchy_id,
       v.dept_name hierarchy_name,
       dps.group_no parent_hierarchy_id,
       NULL grandparent_hierarchy_id
  FROM deps dps,
       period p,
       v_deps_tl v
 WHERE v.dept = dps.dept
 UNION ALL
SELECT p.vdate EFFECTIVE_DATE,
       'C' hier_type,
       cls.CLASS hierarchy_id,
       v.class_name hierarchy_name,
       cls.dept parent_hierarchy_id,
       NULL grandparent_hierarchy_id
  FROM CLASS cls,
       period p,
       v_class_tl v
 WHERE v.dept = cls.dept
   AND v.class = cls.class
 UNION ALL
 SELECT p.vdate EFFECTIVE_DATE,
        'S' hier_type,
        sub.subclass hierarchy_id,
        v.sub_name hierarchy_name,
        sub.CLASS parent_hierarchy_id,
        sub.dept grandparent_hierarchy_id
   FROM subclass sub,
        period p,
        v_subclass_tl v
  WHERE v.dept = sub.dept
    AND v.class = sub.class
    AND v.subclass = sub.subclass
  UNION ALL
 SELECT pmhs.effective_date EFFECTIVE_DATE,
        pmhs.hier_type hier_type,
        pmhs.merch_hier_id hierarchy_id,
        v.merch_hier_name hierarchy_name,
        pmhs.merch_hier_parent_id parent_hierarchy_id,
        pmhs.merch_hier_grandparent_id  grandparent_hierarchy_id
  FROM PEND_MERCH_HIER pmhs,
       V_PEND_MERCH_HIER_TL v
 WHERE action_type = 'A'
   AND pmhs.hier_type = v.hier_type
   AND pmhs.merch_hier_id = v.merch_hier_id
   AND NVL(pmhs.merch_hier_parent_id, -1) = NVL(v.merch_hier_parent_id, -1)
   AND NVL(pmhs.merch_hier_grandparent_id, -1) = NVL(v.merch_hier_grandparent_id, -1);

comment on table V_MERCH_HIER is 'This view will combine the new shadow table (PEND_MERCH_HIER) and the existing merchandise hierarchy tables (division, groups, deps, class, and subclass). The view will be used to support the validation of reclassification events until they are moved directly to the production tables concurrently with the execution of the scheduled reclassification events. In house reporting may also make use of the new view to visibility to the to-be structures for both hierarchy changes and pending item reclassification.';
