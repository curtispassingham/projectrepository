CREATE OR REPLACE FORCE VIEW V_REGION_TL (REGION, REGION_NAME, LANG ) AS
SELECT  b.region,
        case when tl.lang is not null then tl.region_name else b.region_name end region_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  REGION b,
        REGION_TL tl
 WHERE  b.region = tl.region (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_REGION_TL is 'This is the translation view for base table REGION. This view fetches data in user langauge either from translation table REGION_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_REGION_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_REGION_TL.REGION is 'Contains the number which uniquely identifies the region.'
/

COMMENT ON COLUMN V_REGION_TL.REGION_NAME is 'Contains the name of the region which, along with the region number, identifies the region.'
/

