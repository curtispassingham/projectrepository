--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_VWH
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_VWH"
CREATE TABLE DC_VWH
(
   WH                         NUMBER(10),
   WH_NAME                    VARCHAR2(150),
   PHYSICAL_WH                NUMBER(10),
   RESTRICTED_IND             VARCHAR2(1),
   PROTECTED_IND              VARCHAR2(1),
   FORECAST_WH_IND            VARCHAR2(1),
   REPL_IND                   VARCHAR2(1),
   REPL_WH_LINK               NUMBER(10), 
   IB_IND                     VARCHAR2(1),
   IB_WH_LINK                 NUMBER(10),
   AUTO_IB_CLEAR              VARCHAR2(1),
   FINISHER_IND               VARCHAR2(1),
   WH_NAME_SECONDARY          VARCHAR2(150),
   CHANNEL_ID                 NUMBER(4),
   TSF_ENTITY_ID              NUMBER(10),
   ORG_UNIT_ID                NUMBER(15),
   ORG_ENTITY_TYPE            VARCHAR2(1), 
   CUSTOMER_ORDER_LOC_IND     VARCHAR2(1),
   DEFAULT_WH                 NUMBER(10)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_VWH is 'This table is a staging table for data conversion and will hold the virtual warehouse information.'
/
COMMENT ON COLUMN DC_VWH.WH is 'Contains the number which uniquely identifies the warehouse.'
/
COMMENT ON COLUMN DC_VWH.WH_NAME is 'Contains the name of the warehouse which, along with the warehouse number, identifies the warehouse.'
/
COMMENT ON COLUMN DC_VWH.PHYSICAL_WH is 'This column will contain the number of the physical warehouse that is assigned to the virtual warehouse.'
/
COMMENT ON COLUMN DC_VWH.RESTRICTED_IND is 'Indicator used to restrict virtual warehouses from receiving stock during an inbound type transaction when stock needs to be prorated across virtual warehouses within a physical warehouse because a virtual warehouse in the physical warehouse has not been identified for the transaction.'
/
COMMENT ON COLUMN DC_VWH.PROTECTED_IND is 'Indicator used to determine if the virtual warehouse is affected last in transactions where inventory is removed or affected first in short-shipment type transactions where inventory is being added.'
/
COMMENT ON COLUMN DC_VWH.FORECAST_WH_IND is 'This indicator determines if a warehouse is forecastable.'
/
COMMENT ON COLUMN DC_VWH.REPL_IND is 'This indicator determines if a warehouse is replenishable.'
/
COMMENT ON COLUMN DC_VWH.REPL_WH_LINK is 'This field holds the replenishable warehouse that is linked to this virtual warehouse.'
/
COMMENT ON COLUMN DC_VWH.IB_IND is 'This field indicates if the warehouse is an investment buy warehouse.'
/
COMMENT ON COLUMN DC_VWH.IB_WH_LINK is 'This field contains the investment buy warehouse that is linked to the virtual warehouse.'
/
COMMENT ON COLUMN DC_VWH.AUTO_IB_CLEAR is 'This indicator determines if the investment buys inventory should be automatically transferred to the turn (replenishable) warehouse when an order is received by the turn warehouse.'
/
COMMENT ON COLUMN DC_VWH.FINISHER_IND is 'Yes/No value which indicates if this virtual warehouse is an internal finisher.'
/
COMMENT ON COLUMN DC_VWH.WH_NAME_SECONDARY is 'Secondary name of the warehouse.'
/
COMMENT ON COLUMN DC_VWH.CHANNEL_ID is 'This column will contain the channel for which the virtual warehouse will be assigned.'
/
COMMENT ON COLUMN DC_VWH.TSF_ENTITY_ID is 'ID of the transfer entity with which this warehouse is associated.'
/
COMMENT ON COLUMN DC_VWH.ORG_UNIT_ID  is 'This column will hold the oracle organizational unit id value.'
/
COMMENT ON COLUMN DC_VWH.ORG_ENTITY_TYPE is 'This is the new column that will specify if the warehouse is a legal entity (Importer, Exporter) or a regular warehouse.'
/
COMMENT ON COLUMN DC_VWH.CUSTOMER_ORDER_LOC_IND is 'This column determines if the location is customer order location or not, i.e. if the indicator is checked then the location can be used by OMS for sourcing/ fulfillment or both else it cannot be used.'
/
COMMENT ON COLUMN DC_VWH.DEFAULT_WH is 'This field contains the default warehouse linked to the virtual warehouse.'
/