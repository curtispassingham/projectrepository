--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_DEPS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View V_DEPS
CREATE OR REPLACE FORCE VIEW V_DEPS
 (DIVISION
 ,GROUP_NO
 ,DEPT
 ,DEPT_NAME
 ,PURCHASE_TYPE
 ,MARKUP_CALC_TYPE
 ,OTB_CALC_TYPE)
 AS SELECT GRO.DIVISION DIVISION
          ,DEP.GROUP_NO GROUP_NO
          ,DEP.DEPT DEPT
          ,V.DEPT_NAME DEPT_NAME
          ,DEP.PURCHASE_TYPE PURCHASE_TYPE
          ,DEP.MARKUP_CALC_TYPE
          ,DEP.OTB_CALC_TYPE
FROM DEPS DEP
    ,GROUPS GRO
    ,V_DEPS_TL V
  WHERE gro.group_no = dep.group_no
    AND v.dept = dep.dept
/


COMMENT ON TABLE V_DEPS IS 'This view will be used to display the Department LOVs using a security policy to filter User access.'
/

COMMENT ON COLUMN V_DEPS."DIVISION" IS 'Contains the number of the division of which the group is a member.'
/

COMMENT ON COLUMN V_DEPS."GROUP_NO" IS 'Contains the number of the group in which the department exists.'
/

COMMENT ON COLUMN V_DEPS."DEPT" IS 'Contains the number which uniquely identifies the department.'
/

COMMENT ON COLUMN V_DEPS."PURCHASE_TYPE" IS 'Contains a code which indicates whether items in this department are normal merchandise, consignment stock or concession items. Valid values are: 0 = Normal Merchandise,  1 = Consignment Stock, 2 = Concession Items'
/

COMMENT ON COLUMN V_DEPS."MARKUP_CALC_TYPE" IS 'Contains the code letter which determines how markup is calculated in this department.  Valid values are: C = Cost, R = Retail'
/

COMMENT ON COLUMN V_DEPS."OTB_CALC_TYPE" IS 'Contains the code letter which determines how OTB is calculated in this department.  Valid values are: C = Cost, R = Retail'
/