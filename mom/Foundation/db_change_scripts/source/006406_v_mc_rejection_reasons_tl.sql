--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW CREATE:           V_MC_REJECTION_REASONS_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATING VIEW
--------------------------------------
PROMPT Creating View 'V_MC_REJECTION_REASONS_TL'
CREATE OR REPLACE FORCE VIEW V_MC_REJECTION_REASONS_TL (REASON_KEY, REJECTION_REASON, LANG ) AS
SELECT  b.reason_key,
        case when tl.lang is not null then tl.rejection_reason else b.rejection_reason end rejection_reason,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  mc_rejection_reasons b,
        mc_rejection_reasons_tl tl
 WHERE  b.reason_key = tl.reason_key (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_MC_REJECTION_REASONS_TL is 'This is the translation view for base table MC_REJECTION_REASONS. This view fetches data in user langauge either from translation table MC_REJECTION_REASONS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_MC_REJECTION_REASONS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_MC_REJECTION_REASONS_TL.REASON_KEY is 'Contains the reason key.'
/

COMMENT ON COLUMN V_MC_REJECTION_REASONS_TL.REJECTION_REASON is 'This Column holds the rejection reason.'
/

