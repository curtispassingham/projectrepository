--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_INV_STATUS_CODES_LANG_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_INV_STATUS_CODES_LANG_TL'
CREATE TABLE SVC_INV_STATUS_CODES_LANG_TL
 (PROCESS_ID NUMBER(10) NOT NULL,
  CHUNK_ID NUMBER(10) NOT NULL,
  ROW_SEQ NUMBER(20) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ),
  LANG NUMBER(6),
  INV_STATUS_CODE VARCHAR2(10 ),
  INV_STATUS_CODE_DESC VARCHAR2(120 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_INV_STATUS_CODES_LANG_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded to INV_STATUS_CODES_TL'
/

COMMENT ON COLUMN SVC_INV_STATUS_CODES_LANG_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_INV_STATUS_CODES_LANG_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_INV_STATUS_CODES_LANG_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_INV_STATUS_CODES_LANG_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_INV_STATUS_CODES_LANG_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_INV_STATUS_CODES_LANG_TL.LANG is 'This field contains the language in which the translated text is maintained.'
/

COMMENT ON COLUMN SVC_INV_STATUS_CODES_LANG_TL.INV_STATUS_CODE is 'Contains a unique inventory status code.'
/

COMMENT ON COLUMN SVC_INV_STATUS_CODES_LANG_TL.INV_STATUS_CODE_DESC is 'Contains an inventory status code description for the inventory status code.'
/


PROMPT Creating Primary Key on 'SVC_INV_STATUS_CODES_LANG_TL'
ALTER TABLE SVC_INV_STATUS_CODES_LANG_TL
 ADD CONSTRAINT SVC_INV_STATUS_CODES_LTL_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_INV_STATUS_CODES_LANG_TL'
ALTER TABLE SVC_INV_STATUS_CODES_LANG_TL
 ADD CONSTRAINT SVC_INV_STATUS_CODES_LTL_UK UNIQUE
  (LANG,
   INV_STATUS_CODE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

