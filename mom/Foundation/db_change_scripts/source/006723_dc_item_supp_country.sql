--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_ITEM_SUPP_COUNTRY
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_ITEM_SUPP_COUNTRY"
CREATE TABLE DC_ITEM_SUPP_COUNTRY
(
   ITEM                          VARCHAR2(25),
   SUPPLIER                      NUMBER(10),
   ORIGIN_COUNTRY_ID             VARCHAR2(3),
   UNIT_COST                     NUMBER(20,4),
   SUPP_PACK_SIZE                NUMBER(12,4),
   INNER_PACK_SIZE               NUMBER(12,4),
   ROUND_LVL                     VARCHAR2(6),
   ROUND_TO_INNER_PCT            NUMBER(12,4),
   ROUND_TO_CASE_PCT             NUMBER(12,4),
   ROUND_TO_LAYER_PCT            NUMBER(12,4),
   ROUND_TO_PALLET_PCT           NUMBER(12,4),
   MIN_ORDER_QTY                 NUMBER(12,4),
   MAX_ORDER_QTY                 NUMBER(12,4),
   PRIMARY_COUNTRY_IND           VARCHAR2(1),
   TI                            NUMBER(12,4),
   HI                            NUMBER(12,4),
   COST_UOM                      VARCHAR2(4),
   LEAD_TIME                     NUMBER(4),
   PACKING_METHOD                VARCHAR2(6),
   DEFAULT_UOP                   VARCHAR2(6),
   NEGOTIATED_ITEM_COST          NUMBER(20,4),
   EXTENDED_BASE_COST            NUMBER(20,4),
   INCLUSIVE_COST                NUMBER(20,4),
   BASE_COST                     NUMBER(20,4)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_ITEM_SUPP_COUNTRY is 'This table is a staging table for data conversion and will hold data of ITEM_SUPP_COUNTRY table.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.SUPPLIER is 'The unique identifier for the supplier.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID is 'The country where the item was manufactured or significantly altered.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.UNIT_COST is 'This field contains the current corporate unit cost for the SKU from the supplier/origin country. This field is stored in the suppliers currency.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE is 'This field contains the quantity that orders must be placed in multiples of for the supplier for the item.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.INNER_PACK_SIZE is 'This field contains the units of an item contained in an inner pack supplied by the supplier.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.ROUND_LVL is 'This column will be used to determine how order quantities will be rounded to Case, Layer and Pallet.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.ROUND_TO_INNER_PCT is 'This column will hold the Inner Rounding Threshold value.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT is 'This column will hold the Case Rounding Threshold value.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.ROUND_TO_LAYER_PCT is 'This column will hold the Layer Rounding Threshold value.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.ROUND_TO_PALLET_PCT is 'This column will hold the Pallet Rounding Threshold value.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.MIN_ORDER_QTY is 'This field contains the minimum quantity that can be ordered at one time from the supplier for the item.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.MAX_ORDER_QTY is 'This field contains the maximum quantity that can be ordered at one time from the supplier for the item.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.PRIMARY_COUNTRY_IND is 'This field indicates whether this country is the primary country for the item/supplier.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.TI is 'Number of shipping units (cases) that make up one tier of a pallet.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.HI is 'Number of tiers that make up a complete pallet (height).'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.COST_UOM is 'A cost UOM is held to allow costs to be managed in a separate UOM to the standard UOM.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.LEAD_TIME is 'This field indicates the number of days that will elapse between the date an order is written for the item and the date it is ready for shipment from the supplier.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.PACKING_METHOD is 'This field indicates whether the packing method of the item in the container is Flat or Hanging.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.DEFAULT_UOP is 'Contains the default unit of purchase for the item/supplier/country.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST is 'This will hold the supplier negotiated item cost for the primary delivery country of the item.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST is 'This will hold the extended base cost for the primary delivery country of the item.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.INCLUSIVE_COST is 'This will hold the inclusive cost for the primary delivery country of the item.'
/
COMMENT ON COLUMN DC_ITEM_SUPP_COUNTRY.BASE_COST is 'This field will hold the tax exclusive cost of the item.'
/