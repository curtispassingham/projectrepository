--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       DIFF_TYPE_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE DIFF_TYPE_TL(
LANG NUMBER(6) NOT NULL,
DIFF_TYPE VARCHAR2(6) NOT NULL,
DIFF_TYPE_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE DIFF_TYPE_TL is 'This is the translation table for DIFF_TYPE table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN DIFF_TYPE_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN DIFF_TYPE_TL.DIFF_TYPE is 'Contains the code used to uniquely identify a differentiator type.'
/

COMMENT ON COLUMN DIFF_TYPE_TL.DIFF_TYPE_DESC is 'Contains the description of the differentiator type.'
/

COMMENT ON COLUMN DIFF_TYPE_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN DIFF_TYPE_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN DIFF_TYPE_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN DIFF_TYPE_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE DIFF_TYPE_TL ADD CONSTRAINT PK_DIFF_TYPE_TL PRIMARY KEY (
LANG,
DIFF_TYPE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE DIFF_TYPE_TL
 ADD CONSTRAINT DTT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE DIFF_TYPE_TL ADD CONSTRAINT DTT_DT_FK FOREIGN KEY (
DIFF_TYPE
) REFERENCES DIFF_TYPE (
DIFF_TYPE
)
/

