--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       STORE_GRADE_GROUP_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE STORE_GRADE_GROUP_TL(
LANG NUMBER(6) NOT NULL,
STORE_GRADE_GROUP_ID NUMBER(8) NOT NULL,
STORE_GRADE_GROUP_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE STORE_GRADE_GROUP_TL is 'This is the translation table for STORE_GRADE_GROUP table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN STORE_GRADE_GROUP_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN STORE_GRADE_GROUP_TL.STORE_GRADE_GROUP_ID is 'This column holds the unique store grade group identification number.  The id is a unique system generated number.'
/

COMMENT ON COLUMN STORE_GRADE_GROUP_TL.STORE_GRADE_GROUP_DESC is 'This column will hold the store group description associated with the identification number.'
/

COMMENT ON COLUMN STORE_GRADE_GROUP_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN STORE_GRADE_GROUP_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN STORE_GRADE_GROUP_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN STORE_GRADE_GROUP_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE STORE_GRADE_GROUP_TL ADD CONSTRAINT PK_STORE_GRADE_GROUP_TL PRIMARY KEY (
LANG,
STORE_GRADE_GROUP_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE STORE_GRADE_GROUP_TL
 ADD CONSTRAINT SGGT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE STORE_GRADE_GROUP_TL ADD CONSTRAINT SGGT_SGG_FK FOREIGN KEY (
STORE_GRADE_GROUP_ID
) REFERENCES STORE_GRADE_GROUP (
STORE_GRADE_GROUP_ID
)
/

