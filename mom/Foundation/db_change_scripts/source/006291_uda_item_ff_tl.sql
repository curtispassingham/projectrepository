--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 UDA_ITEM_FF_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'UDA_ITEM_FF_TL'
CREATE TABLE UDA_ITEM_FF_TL
 (LANG NUMBER(6) NOT NULL,
  ITEM VARCHAR2(25 ) NOT NULL,
  UDA_ID NUMBER(5) NOT NULL,
  UDA_TEXT VARCHAR2(250 ) NOT NULL,
  UDA_TEXT_DESC VARCHAR2(250 ),
  CREATE_DATETIME DATE NOT NULL,
  CREATE_ID VARCHAR2(30 ) NOT NULL,
  LAST_UPDATE_DATETIME DATE NOT NULL,
  LAST_UPDATE_ID VARCHAR2(30 ) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE UDA_ITEM_FF_TL is 'This is the translation table for UDA_ITEM_FF_TL table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN UDA_ITEM_FF_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN UDA_ITEM_FF_TL.ITEM is 'This field contains unique alphanumeric identifier for the item.'
/

COMMENT ON COLUMN UDA_ITEM_FF_TL.UDA_ID is 'This field contains a number uniquely identifying the User-Defined Attribute.'
/

COMMENT ON COLUMN UDA_ITEM_FF_TL.UDA_TEXT is 'This field contains the text value of the Used Defined attribute for the item.'
/

COMMENT ON COLUMN UDA_ITEM_FF_TL.UDA_TEXT_DESC is 'This field holds the uda text in different language. '
/

COMMENT ON COLUMN UDA_ITEM_FF_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN UDA_ITEM_FF_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN UDA_ITEM_FF_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN UDA_ITEM_FF_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/


PROMPT Creating Primary Key on 'UDA_ITEM_FF_TL'
ALTER TABLE UDA_ITEM_FF_TL
 ADD CONSTRAINT PK_UDA_ITEM_FF_TL PRIMARY KEY
  (LANG,
   ITEM,
   UDA_ID,
   UDA_TEXT
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'UDA_ITEM_FF_TL'
 ALTER TABLE UDA_ITEM_FF_TL
  ADD CONSTRAINT UIFT_LANG_FK
  FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/


PROMPT Creating FK on 'UDA_ITEM_FF_TL'
 ALTER TABLE UDA_ITEM_FF_TL
  ADD CONSTRAINT UIFT_UIF_FK
  FOREIGN KEY (ITEM, UDA_ID, UDA_TEXT)
 REFERENCES UDA_ITEM_FF (ITEM, UDA_ID, UDA_TEXT)
/

