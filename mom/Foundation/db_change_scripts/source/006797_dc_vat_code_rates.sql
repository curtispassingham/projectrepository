--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_VAT_CODE_RATES
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_VAT_CODE_RATES"
CREATE TABLE DC_VAT_CODE_RATES
(
   VAT_CODE                  VARCHAR2(6),
   ACTIVE_DATE               DATE,
   VAT_RATE                  NUMBER(20,10)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_VAT_CODE_RATES is 'This table is a staging table for data conversion and will hold VAT rates and active dates for each VAT code in the system.'
/
COMMENT ON COLUMN DC_VAT_CODE_RATES.VAT_CODE is 'Code to uniquely identify a VAT rate.'
/
COMMENT ON COLUMN DC_VAT_CODE_RATES.ACTIVE_DATE is 'Date on which the VAT rate becomes active.'
/
COMMENT ON COLUMN DC_VAT_CODE_RATES.VAT_RATE is 'VAT rate associated with a given VAT code.'
/