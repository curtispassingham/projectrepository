CREATE OR REPLACE FORCE VIEW V_CURRENCIES_TL (CURRENCY_CODE, CURRENCY_DESC, LANG ) AS
SELECT  b.currency_code,
        case when tl.lang is not null then tl.currency_desc else b.currency_desc end currency_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  CURRENCIES b,
        CURRENCIES_TL tl
 WHERE  b.currency_code = tl.currency_code (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_CURRENCIES_TL is 'This is the translation view for base table CURRENCIES. This view fetches data in user langauge either from translation table CURRENCIES_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_CURRENCIES_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_CURRENCIES_TL.CURRENCY_CODE is 'Contains a number which uniquely identifies the type of currency.'
/

COMMENT ON COLUMN V_CURRENCIES_TL.CURRENCY_DESC is 'Contains a description of the currency.'
/

