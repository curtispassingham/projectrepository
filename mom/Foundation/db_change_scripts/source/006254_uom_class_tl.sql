--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 UOM_CLASS_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'UOM_CLASS_TL'
CREATE TABLE UOM_CLASS_TL
 (LANG NUMBER(6) NOT NULL,
  UOM VARCHAR2(4 ) NOT NULL,
  UOM_TRANS VARCHAR2(4 ) NOT NULL,
  UOM_DESC_TRANS VARCHAR2(120 ) NOT NULL,
  ORIG_LANG_IND VARCHAR2(1 ) NOT NULL,
  REVIEWED_IND VARCHAR2(1 ) NOT NULL,
  CREATE_DATETIME DATE NOT NULL,
  CREATE_ID VARCHAR2(30 ) NOT NULL,
  LAST_UPDATE_DATETIME DATE NOT NULL,
  LAST_UPDATE_ID VARCHAR2(30 ) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE UOM_CLASS_TL is 'This is the translation table for UOM_CLASS_TL table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN UOM_CLASS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN UOM_CLASS_TL.UOM is 'Unit of Measurement'
/

COMMENT ON COLUMN UOM_CLASS_TL.UOM_TRANS is 'Translated UOM'
/

COMMENT ON COLUMN UOM_CLASS_TL.UOM_DESC_TRANS is 'Translated UOM description'
/

COMMENT ON COLUMN UOM_CLASS_TL.ORIG_LANG_IND is 'Indicates if the description is in the original language entered for the UOM. It is set to Y when the first record is written to the table for the UOM.'
/

COMMENT ON COLUMN UOM_CLASS_TL.REVIEWED_IND is 'Indicates if the description needs to be reviewed for translation. It is set to N when the description in the original language is inserted or updated. We assume that clients will regularly '
/

COMMENT ON COLUMN UOM_CLASS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN UOM_CLASS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN UOM_CLASS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN UOM_CLASS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/


PROMPT Creating Primary Key on 'UOM_CLASS_TL'
ALTER TABLE UOM_CLASS_TL
 ADD CONSTRAINT PK_UOM_CLASS_TL PRIMARY KEY
  (LANG,
   UOM
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating FK on 'UOM_CLASS_TL'
 ALTER TABLE UOM_CLASS_TL
  ADD CONSTRAINT UCT_LANG_FK
  FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/


PROMPT Creating FK on 'UOM_CLASS_TL'
 ALTER TABLE UOM_CLASS_TL
  ADD CONSTRAINT UCT_UC_FK
  FOREIGN KEY (UOM)
 REFERENCES UOM_CLASS (UOM)
/

