--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_ITEM_COST_DETAIL
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_ITEM_COST_DETAIL"
CREATE TABLE DC_ITEM_COST_DETAIL
(
   ITEM                               VARCHAR2(25),
   SUPPLIER                           NUMBER(10),
   ORIGIN_COUNTRY_ID                  VARCHAR2(3),
   DELIVERY_COUNTRY_ID                VARCHAR2(3),
   COND_TYPE                          VARCHAR2(10),
   COND_VALUE                         NUMBER(20,4),
   APPLIED_ON                         NUMBER(20,4),
   COMP_RATE                          NUMBER(20,10),
   CALCULATION_BASIS                  VARCHAR2(1),
   RECOVERABLE_AMOUNT                 NUMBER(20,4),
   MODIFIED_TAXABLE_BASE              NUMBER(20,4)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_ITEM_COST_DETAIL is 'This table is a staging table for data conversion and will hold data of ITEM_COST_DETAIL table.'
/
COMMENT ON COLUMN DC_ITEM_COST_DETAIL.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_ITEM_COST_DETAIL.SUPPLIER is 'The unique identifier for the supplier.'
/
COMMENT ON COLUMN DC_ITEM_COST_DETAIL.ORIGIN_COUNTRY_ID is 'The country where the item was manufactured or significantly altered.'
/
COMMENT ON COLUMN DC_ITEM_COST_DETAIL.DELIVERY_COUNTRY_ID is 'Country to which the item will be delivered to.'
/
COMMENT ON COLUMN DC_ITEM_COST_DETAIL.COND_TYPE is 'This will hold the condition type applicable on the items cost.'
/
COMMENT ON COLUMN DC_ITEM_COST_DETAIL.COND_VALUE is 'This will hold the condition value or tax amount per of the corresponding condition.'
/
COMMENT ON COLUMN DC_ITEM_COST_DETAIL.APPLIED_ON is 'This will hold the cost on which a particular condition should be applied.'
/
COMMENT ON COLUMN DC_ITEM_COST_DETAIL.COMP_RATE is 'This will hold the rate of the condition applied.'
/
COMMENT ON COLUMN DC_ITEM_COST_DETAIL.CALCULATION_BASIS is 'Indicates if the comp_rate is a percentage or an amount value. Valid values are: P - percent, V - amount value.'
/
COMMENT ON COLUMN DC_ITEM_COST_DETAIL.RECOVERABLE_AMOUNT is 'Hold the recoverable amount of tax applied on the item.'
/
COMMENT ON COLUMN DC_ITEM_COST_DETAIL.MODIFIED_TAXABLE_BASE is 'This column will hold the cost on which the taxes were applied.'
/