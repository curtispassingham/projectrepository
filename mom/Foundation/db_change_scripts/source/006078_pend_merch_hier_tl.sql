--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       PEND_MERCH_HIER_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE PEND_MERCH_HIER_TL(
LANG NUMBER(6) NOT NULL,
HIER_TYPE VARCHAR2(1) NOT NULL,
MERCH_HIER_ID NUMBER(4) NOT NULL,
MERCH_HIER_PARENT_ID NUMBER(4) ,
MERCH_HIER_GRANDPARENT_ID NUMBER(4) ,
MERCH_HIER_NAME VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE PEND_MERCH_HIER_TL is 'This is the translation table for PEND_MERCH_HIER table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN PEND_MERCH_HIER_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN PEND_MERCH_HIER_TL.HIER_TYPE is 'This column will identify which hierarchy is being added or updated.  The values for this column will be V- Division, G- Group D dept, C class and S subclass'
/

COMMENT ON COLUMN PEND_MERCH_HIER_TL.MERCH_HIER_ID is 'This column will contain the division, group, dept, class or the subclass number depending on the hier_type. If the action_type is _A_, then this id will be generated using the current sequence number generator for division, group, dept, class and subclass depending on the hier_type.'
/

COMMENT ON COLUMN PEND_MERCH_HIER_TL.MERCH_HIER_PARENT_ID is 'This column will contain the division_id if hier_type = group, group_id if hier_type= dept, dept number if hier_type= class and class number if hier_type= subclass.'
/

COMMENT ON COLUMN PEND_MERCH_HIER_TL.MERCH_HIER_GRANDPARENT_ID is 'This column will only be populated if the hier_type = subclass. If hier_type = subclass the column will contain the dept number.'
/

COMMENT ON COLUMN PEND_MERCH_HIER_TL.MERCH_HIER_NAME is 'This column will contain name of the division, group, department, class or subclass depending on the hier_type'
/

COMMENT ON COLUMN PEND_MERCH_HIER_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN PEND_MERCH_HIER_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN PEND_MERCH_HIER_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN PEND_MERCH_HIER_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE PEND_MERCH_HIER_TL ADD CONSTRAINT UK_PEND_MERCH_HIER_TL UNIQUE (
LANG,
HIER_TYPE,
MERCH_HIER_ID,
MERCH_HIER_PARENT_ID,
MERCH_HIER_GRANDPARENT_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE PEND_MERCH_HIER_TL
 ADD CONSTRAINT PHMHT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE PEND_MERCH_HIER_TL ADD CONSTRAINT PHMHT_PHMH_FK FOREIGN KEY (
HIER_TYPE,
MERCH_HIER_ID,
MERCH_HIER_PARENT_ID,
MERCH_HIER_GRANDPARENT_ID
) REFERENCES PEND_MERCH_HIER (
HIER_TYPE,
MERCH_HIER_ID,
MERCH_HIER_PARENT_ID,
MERCH_HIER_GRANDPARENT_ID
)
/

