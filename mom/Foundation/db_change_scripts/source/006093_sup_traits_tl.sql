--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       SUP_TRAITS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE SUP_TRAITS_TL(
LANG NUMBER(6) NOT NULL,
SUP_TRAIT NUMBER(4) NOT NULL,
DESCRIPTION VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SUP_TRAITS_TL is 'This is the translation table for SUP_TRAITS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN SUP_TRAITS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SUP_TRAITS_TL.SUP_TRAIT is 'This field contains the number that uniquely identifies the supplier trait.'
/

COMMENT ON COLUMN SUP_TRAITS_TL.DESCRIPTION is 'This field contains the description associated with the supplier trait.'
/

COMMENT ON COLUMN SUP_TRAITS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN SUP_TRAITS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN SUP_TRAITS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN SUP_TRAITS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE SUP_TRAITS_TL ADD CONSTRAINT PK_SUP_TRAITS_TL UNIQUE (
LANG,
SUP_TRAIT
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SUP_TRAITS_TL
 ADD CONSTRAINT STTT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE SUP_TRAITS_TL ADD CONSTRAINT STTT_STT_FK FOREIGN KEY (
SUP_TRAIT
) REFERENCES SUP_TRAITS (
SUP_TRAIT
)
/

