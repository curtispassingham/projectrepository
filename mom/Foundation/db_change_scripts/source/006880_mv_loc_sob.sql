--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  MATERIALIZED VIEW UPDATED:               MV_LOC_SOB
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING MATERIALIZED VIEW
--------------------------------------
PROMPT Dropping Materialized View 'MV_LOC_SOB'
DROP MATERIALIZED VIEW MV_LOC_SOB
/

PROMPT Creating Materialized View 'MV_LOC_SOB'
CREATE MATERIALIZED VIEW MV_LOC_SOB
TABLESPACE "RETAIL_DATA" 
PCTFREE 10 
INITRANS 6 
STORAGE 
( 
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1 
  MAXEXTENTS 2147483645 
  BUFFER_POOL DEFAULT 
) 
USING INDEX 
REFRESH ON DEMAND FORCE 
DISABLE QUERY REWRITE AS 
SELECT loc.location                AS   location,
           loc.location_type       AS   location_type,
           loc.location_name       AS   location_name,
           sob.set_of_books_id     AS   set_of_books_id,
           sob.set_of_books_desc   AS   set_of_books_desc,
           loc.loc_currency        AS   loc_currency,
           sob.period_name         AS   period_name,
           sob.last_update_id      AS   last_update_id
    from
     (select TO_NUMBER(partner_id) location,
             partner_desc location_name,
             partner_type location_type,
             currency_code loc_currency,
             NVL(tsf_entity_id, -1) tsf_entity_id,
             NVL(org_unit_id, -1) org_unit_id,
             'C' store_type
        from partner
       where partner_type='E'
      UNION ALL
      select store location,
             store_name location_name,
             'S' location_type,
             currency_code loc_currency,
             NVL(tsf_entity_id, -1) tsf_entity_id,
             NVL(org_unit_id, -1) org_unit_id,
             store_type
        from store
      UNION ALL
      select wh location,
             wh_name location_name,
             'W' location_type,
             currency_code loc_currency,
             NVL(tsf_entity_id, -1) tsf_entity_id,
             NVL(org_unit_id, -1) org_unit_id,
             'C' store_type
        from wh) loc,
     --location's set of books id should be based just on the ORG_UNIT_ID of the location
     (select org_unit_id,
             fgs.set_of_books_id,
             fgs.set_of_books_desc,
             fgs.period_name,
             fgs.last_update_id
        from org_unit ou,
             fif_gl_setup fgs
       where fgs.set_of_books_id = ou.set_of_books_id) sob
WHERE loc.org_unit_id = NVL(sob.org_unit_id(+), loc.org_unit_id)
/
