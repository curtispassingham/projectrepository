--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_LOCATION_CLOSED_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

CREATE TABLE SVC_LOCATION_CLOSED_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, LOCATION NUMBER(10)
, CLOSE_DATE DATE
, REASON VARCHAR2(250)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_LOCATION_CLOSED_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in LOCATION_CLOSED_TL.'
/

COMMENT ON COLUMN SVC_LOCATION_CLOSED_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_LOCATION_CLOSED_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_LOCATION_CLOSED_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_LOCATION_CLOSED_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_LOCATION_CLOSED_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_LOCATION_CLOSED_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_LOCATION_CLOSED_TL.LOCATION is 'Contain the location number'
/

COMMENT ON COLUMN SVC_LOCATION_CLOSED_TL.CLOSE_DATE is 'This field contains the date of the closing.'
/

COMMENT ON COLUMN SVC_LOCATION_CLOSED_TL.REASON is 'Contains the reason why the store is closed.'
/

ALTER TABLE SVC_LOCATION_CLOSED_TL
ADD CONSTRAINT SVC_LOCATION_CLOSED_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SVC_LOCATION_CLOSED_TL
ADD CONSTRAINT SVC_LOCATION_CLOSED_TL_UK UNIQUE
(LANG, LOCATION, CLOSE_DATE)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

