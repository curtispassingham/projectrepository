CREATE OR REPLACE FORCE VIEW V_ITEM_SUPPLIER_TL (ITEM, SUPPLIER, SUPP_LABEL, SUPP_DIFF_1, SUPP_DIFF_2, SUPP_DIFF_3, SUPP_DIFF_4, LANG ) AS
SELECT  b.item,
        b.supplier,
        case when tl.lang is not null then tl.supp_label else b.supp_label end supp_label,
        case when tl.lang is not null then tl.supp_diff_1 else b.supp_diff_1 end supp_diff_1,
        case when tl.lang is not null then tl.supp_diff_2 else b.supp_diff_2 end supp_diff_2,
        case when tl.lang is not null then tl.supp_diff_3 else b.supp_diff_3 end supp_diff_3,
        case when tl.lang is not null then tl.supp_diff_4 else b.supp_diff_4 end supp_diff_4,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  ITEM_SUPPLIER b,
        ITEM_SUPPLIER_TL tl
 WHERE  b.item = tl.item (+)
   AND  b.supplier = tl.supplier (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_ITEM_SUPPLIER_TL is 'This is the translation view for base table ITEM_SUPPLIER. This view fetches data in user langauge either from translation table ITEM_SUPPLIER_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_ITEM_SUPPLIER_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_ITEM_SUPPLIER_TL.ITEM is 'Alphanumeric value that identifies the item'
/

COMMENT ON COLUMN V_ITEM_SUPPLIER_TL.SUPPLIER is 'This field contains the number of the supplier of the item.'
/

COMMENT ON COLUMN V_ITEM_SUPPLIER_TL.SUPP_LABEL is 'This field will hold the supplier laber for an item (Parent/Child)'
/

COMMENT ON COLUMN V_ITEM_SUPPLIER_TL.SUPP_DIFF_1 is 'This field contains the first supplier differentiator and/or description.  This field may only contain a value for items with an item_parent.'
/

COMMENT ON COLUMN V_ITEM_SUPPLIER_TL.SUPP_DIFF_2 is 'This field contains the second supplier differentiator and/or description.  This field may only contain a value for items with an item_parent.'
/

COMMENT ON COLUMN V_ITEM_SUPPLIER_TL.SUPP_DIFF_3 is 'This field contains the third supplier differentiator and/or description.  This field may only contain a value for items with an item_parent.'
/

COMMENT ON COLUMN V_ITEM_SUPPLIER_TL.SUPP_DIFF_4 is 'This field contains the fourth supplier differentiator and/or description.  This field may only contain a value for items with an item_parent.'
/

