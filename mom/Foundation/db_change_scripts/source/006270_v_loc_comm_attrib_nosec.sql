--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Creating View  V_LOC_COMM_ATTRIB_NOSEC 
--------------------------------------
create or replace force view V_LOC_COMM_ATTRIB_NOSEC as
   select w.wh loc,
          'W' loc_type,
          v.wh_name loc_name,
          v.wh_name_secondary loc_name_secondary,
          w.currency_code,
          w.stockholding_ind,
          w.vat_region,
          w.channel_id,
          w.org_unit_id,
          w.tsf_entity_id,
          w.customer_order_loc_ind,
          w.email,
          w.duns_number,
          w.duns_loc,
          w.create_id,
          w.create_datetime
     from wh w,
          v_wh_tl v
    where w.wh = v.wh
    union all
   select s.store, 
          'S' loc_type,
          v.store_name,
          v.store_name_secondary,
          s.currency_code, 
          s.stockholding_ind, 
          s.vat_region, 
          s.channel_id, 
          s.org_unit_id, 
          s.tsf_entity_id, 
          s.customer_order_loc_ind, 
          s.email, 
          s.duns_number, 
          s.duns_loc, 
          s.create_id, 
          s.create_datetime
     from store s,
          v_store_tl v
    where s.store = v.store
/

comment on table v_loc_comm_attrib_nosec is 'This view combines all store and wh records in the system along with all the attributes that are common between them.  The view does not apply data security but applies translation.'
/


