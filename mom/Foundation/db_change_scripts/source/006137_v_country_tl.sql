CREATE OR REPLACE FORCE VIEW V_COUNTRY_TL (COUNTRY_ID, COUNTRY_DESC, LANG ) AS
SELECT  b.country_id,
        case when tl.lang is not null then tl.country_desc else b.country_desc end country_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  COUNTRY b,
        COUNTRY_TL tl
 WHERE  b.country_id = tl.country_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_COUNTRY_TL is 'This is the translation view for base table COUNTRY. This view fetches data in user langauge either from translation table COUNTRY_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_COUNTRY_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_COUNTRY_TL.COUNTRY_ID is 'Contains a number which uniquely identifies the country.'
/

COMMENT ON COLUMN V_COUNTRY_TL.COUNTRY_DESC is 'Contains the name of the country.'
/

