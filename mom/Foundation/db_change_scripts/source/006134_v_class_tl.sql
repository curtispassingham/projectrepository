CREATE OR REPLACE FORCE VIEW V_CLASS_TL (DEPT, CLASS, CLASS_NAME, LANG ) AS
SELECT  b.dept,
        b.class,
        case when tl.lang is not null then tl.class_name else b.class_name end class_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  CLASS b,
        CLASS_TL tl
 WHERE  b.dept = tl.dept (+)
   AND  b.class = tl.class (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_CLASS_TL is 'This is the translation view for base table CLASS. This view fetches data in user langauge either from translation table CLASS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_CLASS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_CLASS_TL.DEPT is 'Contains the number of the department of which the class is a member.'
/

COMMENT ON COLUMN V_CLASS_TL.CLASS is 'Contains the number which uniquely identifies the class within the system.'
/

COMMENT ON COLUMN V_CLASS_TL.CLASS_NAME is 'Contains the name of the class which, along with the class number, identifies the class.'
/

