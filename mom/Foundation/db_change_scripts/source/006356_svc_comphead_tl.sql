--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_COMPHEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

CREATE TABLE SVC_COMPHEAD_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, COMPANY NUMBER(4)
, CO_NAME VARCHAR2(120)
, CO_ADD1 VARCHAR2(240)
, CO_ADD2 VARCHAR2(240)
, CO_ADD3 VARCHAR2(240)
, CO_CITY VARCHAR2(120)
, CO_NAME_SECONDARY VARCHAR2(120)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_COMPHEAD_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in COMPHEAD_TL.'
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.COMPANY is 'Contains the unique number identifying the company the for which the system is running.  This is the highest level in the merchandise hierarchy.'
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.CO_NAME is 'Contains the company name for which the system is running.'
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.CO_ADD1 is 'The address of the company headquarters.'
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.CO_ADD2 is 'The second line of the company headquarters address.'
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.CO_ADD3 is 'The third line of the company headquarters address.'
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.CO_CITY is 'The city of the company headquarters.'
/

COMMENT ON COLUMN SVC_COMPHEAD_TL.CO_NAME_SECONDARY is 'Contains the secondary name of the company.'
/

ALTER TABLE SVC_COMPHEAD_TL
ADD CONSTRAINT SVC_COMPHEAD_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SVC_COMPHEAD_TL
ADD CONSTRAINT SVC_COMPHEAD_TL_UK UNIQUE
(LANG, COMPANY)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

