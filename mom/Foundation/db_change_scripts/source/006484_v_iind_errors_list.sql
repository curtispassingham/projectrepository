--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Modify View: 		 V_IIND_ERRORS_LIST
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       Modify V_IIND_ERRORS_LIST               
--------------------------------------
PROMPT Modifying VIEW 'V_IIND_ERRORS_LIST'
CREATE OR REPLACE FORCE VIEW V_IIND_ERRORS_LIST
 (PROCESS_ID
 ,TEMPLATE_KEY
 ,ERROR_SEQ
 ,TABLE_NAME
 ,TABLE_DESC
 ,COLUMN_NAME
 ,COLUMN_DESC
 ,ROW_SEQ
 ,ERROR_TYPE
 ,ERROR_KEY
 ,ERROR_MSG
 ,ITEM
 ,ITEM_DESC
 ,ITEM_DESC_TRANS
 ,SUPPLIER
 ,SUP_NAME
 ,SUPPLIER_PARENT
 ,SUP_NAME_TRANS
 ,COUNTRY_ID
 ,COUNTRY_DESC
 ,COUNTRY_DESC_TRANS)
AS SELECT ed.process_id process_id,
    ed.template_key template_key,
    ed.error_seq error_seq,
    ed.table_name table_name,
    nvl(wd.wksht_name,ed.table_name) AS table_desc,
    ed.column_name column_name,
    cd.column_name AS column_desc,
    ed.row_seq row_seq,
    NVL(ed.pr_error_type, 'E') error_type,
    ed.error_key error_key,
    item_induct_sql.get_error_display(ed.error_key) AS error_msg,
    ed.item item,
    im.item_desc item_desc,
    vim.item_desc item_desc_trans,
    ed.supplier supplier,
    sups.sup_name sup_name,
    sups.supplier_parent supplier_parent,
    vst.sup_name sup_name_trans,
    ed.country_id country_id,
    country.country_desc country_desc,
    vct.country_desc country_desc_trans
  FROM
    (
    -- Get errors occured during spreadsheet to staging processing
    SELECT 'S9T2STG' AS error_type,
      pt.process_id,
      pt.template_key,
      se.error_seq_no AS error_seq,
      se.wksht_key  AS table_name,
      se.column_key AS column_name,
      se.row_seq,
      se.error_type AS pr_error_type,
      error_key,
      NULL AS item,
      NULL AS supplier,
      NULL AS country_id
    FROM svc_process_tracker pt,
      s9t_errors se
    WHERE pt.file_id = se.file_id
    UNION ALL
    -- Get errors occured during staging to RMS processing
    SELECT 'STG2RMS' AS error_type,
      pt.process_id,
      pt.template_key,
      se.error_seq,
      NVL(tbl_map.table_name,se.table_name) AS table_name,
      se.column_name                        AS column_name,
      se.row_seq,
      se.error_type AS pr_error_type,
      se.error_msg  AS error_key,
      se.item       AS item,
      se.supplier   AS supplier,
      se.country_id AS country_id
    FROM svc_process_tracker pt,
      (select process_id,
              table_name,
              country_id,
              supplier,
              item,
              error_type,
              error_msg,
              row_seq,
              column_name,
              error_seq
         from coresvc_costchg_err
      union
      select process_id,
              table_name,
              country_id,
              supplier,
              item,
              error_type,
              error_msg,
              row_seq,
              column_name,
              error_seq
         from coresvc_item_err) se,
      (SELECT 'ITEM_COST_DETAIL' AS table_name,
        'SVC_ITEM_COST_DETAIL'   AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_COST_HEAD' AS table_name,
        'SVC_ITEM_COST_HEAD'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_COUNTRY' AS table_name,
        'SVC_ITEM_COUNTRY'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_COUNTRY_L10N_EXT' AS table_name,
        'SVC_ITEM_COUNTRY_L10N_EXT'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_IMAGE' AS table_name,
        'SVC_ITEM_IMAGE'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_IMAGE_TL' AS table_name,
        'SVC_ITEM_IMAGE_TL'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_MASTER' AS table_name,
        'SVC_ITEM_MASTER'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_MASTER_TL' AS table_name,
        'SVC_ITEM_MASTER_TL'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_MASTER_CFA_EXT' AS table_name,
        'SVC_ITEM_MASTER_CFA_EXT'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_SUPPLIER' AS table_name,
        'SVC_ITEM_SUPPLIER'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_SUPPLIER_TL' AS table_name,
        'SVC_ITEM_SUPPLIER_TL'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_SUPPLIER_CFA_EXT' AS table_name,
        'SVC_ITEM_SUPPLIER_CFA_EXT'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_SUPP_COUNTRY' AS table_name,
        'SVC_ITEM_SUPP_COUNTRY'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_SUPP_COUNTRY_CFA_EXT' AS table_name,
        'SVC_ITEM_SUPP_COUNTRY_CFA_EXT'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_SUPP_COUNTRY_DIM' AS table_name,
        'SVC_ITEM_SUPP_COUNTRY_DIM'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_SUPP_MANU_COUNTRY' AS table_name,
        'SVC_ITEM_SUPP_MANU_COUNTRY'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_SUPP_UOM' AS table_name,
        'SVC_ITEM_SUPP_UOM'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_XFORM_DETAIL' AS table_name,
        'SVC_ITEM_XFORM_DETAIL'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_XFORM_HEAD' AS table_name,
        'SVC_ITEM_XFORM_HEAD'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'ITEM_XFORM_HEAD_TL' AS table_name,
        'SVC_ITEM_XFORM_HEAD_TL'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'PACKITEM' AS table_name, 'SVC_PACKITEM' AS svc_table_name FROM dual
      UNION ALL
      SELECT 'RPM_ITEM_ZONE_PRICE' AS table_name,
        'SVC_RPM_ITEM_ZONE_PRICE'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'UDA_ITEM_DATE' AS table_name,
        'SVC_UDA_ITEM_DATE'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'UDA_ITEM_FF' AS table_name,
        'SVC_UDA_ITEM_FF'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'UDA_ITEM_LOV' AS table_name,
        'SVC_UDA_ITEM_LOV'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'VAT_ITEM' AS table_name, 'SVC_VAT_ITEM' AS svc_table_name FROM dual
      UNION ALL
      SELECT 'COST_SUSP_SUP_HEAD' AS table_name,
        'SVC_COST_SUSP_SUP_HEAD'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'COST_SUSP_SUP_DETAIL_LOC' AS table_name,
        'SVC_COST_SUSP_SUP_DETAIL_LOC'  AS svc_table_name
      FROM dual
      UNION ALL
      SELECT 'COST_SUSP_SUP_DETAIL' AS table_name,
        'SVC_COST_SUSP_SUP_DETAIL'  AS svc_table_name
      FROM dual
      ) tbl_map
    WHERE pt.process_id = se.process_id
    AND se.table_name   = tbl_map.svc_table_name(+)
    ) ed,
    v_s9t_tmpl_wksht_def wd,
    v_s9t_tmpl_cols_def cd,
    item_master im,
    v_item_master_tl vim,
    sups,
    v_sups_tl vst,
    country,
    v_country_tl vct
  WHERE ed.template_key = wd.template_key (+)
  AND ed.table_name     = wd.wksht_key (+)
  AND ed.column_name    = cd.column_key (+)
  AND ed.template_key   = cd.template_key (+)
  AND ed.table_name     = cd.wksht_key (+)
  AND ed.item           = im.item (+)
  AND ed.item           = vim.item (+)
  AND ed.supplier       = sups.supplier (+)
  AND ed.supplier       = vst.supplier (+)
  AND ed.country_id     = country.country_id (+)
  AND ed.country_id     = vct.country_id (+)
  /

COMMENT ON TABLE V_IIND_ERRORS_LIST IS 'This view lists the errors caught as part of item induction process.'
/
