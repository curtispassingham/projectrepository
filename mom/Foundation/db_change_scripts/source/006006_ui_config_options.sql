--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'UI_CONFIG_OPTIONS'
ALTER TABLE UI_CONFIG_OPTIONS ADD MIN_QTY_PCT_DEC NUMBER (1) DEFAULT 0 NOT NULL
/

COMMENT ON COLUMN UI_CONFIG_OPTIONS.MIN_QTY_PCT_DEC is 'Holds the mininum decimal digits for quantity or percentage precision. The value should be between 0 and 4.'
/

ALTER TABLE UI_CONFIG_OPTIONS ADD MAX_QTY_PCT_DEC NUMBER (1) DEFAULT 4 NOT NULL
/

COMMENT ON COLUMN UI_CONFIG_OPTIONS.MAX_QTY_PCT_DEC is 'Holds the maximum decimal digits for quantity or percentage precision. The value should be between 0 and 4.'
/

