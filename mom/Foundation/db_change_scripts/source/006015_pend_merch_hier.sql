
--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  TABLE ALTER:               PEND_MERCH_HIER
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING TABLE
--------------------------------------
PROMPT Delet CONSTRAINT 'CHK_PEND_MERCH_HIER_PURCHASE'
ALTER TABLE PEND_MERCH_HIER DROP CONSTRAINT CHK_PEND_MERCH_HIER_PURCHASE;


PROMPT ADD CONSTRAINT 'CHK_PEND_MERCH_HIER_PURCHASE'
ALTER TABLE PEND_MERCH_HIER ADD CONSTRAINT CHK_PEND_MERCH_HIER_PURCHASE CHECK (PURCHASE_TYPE IN (0,1,2));