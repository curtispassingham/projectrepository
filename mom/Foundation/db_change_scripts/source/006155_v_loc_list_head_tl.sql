CREATE OR REPLACE FORCE VIEW V_LOC_LIST_HEAD_TL (LOC_LIST, LOC_LIST_DESC, LANG ) AS
SELECT  b.loc_list,
        case when tl.lang is not null then tl.loc_list_desc else b.loc_list_desc end loc_list_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  LOC_LIST_HEAD b,
        LOC_LIST_HEAD_TL tl
 WHERE  b.loc_list = tl.loc_list (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_LOC_LIST_HEAD_TL is 'This is the translation view for base table LOC_LIST_HEAD. This view fetches data in user langauge either from translation table LOC_LIST_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_LOC_LIST_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_LOC_LIST_HEAD_TL.LOC_LIST is 'Contains a number to uniquely identify a location list.'
/

COMMENT ON COLUMN V_LOC_LIST_HEAD_TL.LOC_LIST_DESC is 'Contains the description of the location list.'
/

