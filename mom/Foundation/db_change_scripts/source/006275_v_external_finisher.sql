--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_EXTERNAL_FINISHER
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_EXTERNAL_FINISHER'

CREATE OR REPLACE FORCE VIEW V_EXTERNAL_FINISHER ( FINISHER_ID, FINISHER_DESC, TSF_ENTITY_ID, CURRENCY_CODE, ORG_UNIT_ID ) AS
SELECT TO_NUMBER(PRT.PARTNER_ID) FINISHER_ID
          ,V.PARTNER_DESC FINISHER_DESC
          ,PRT.TSF_ENTITY_ID TSF_ENTITY_ID
          ,PRT.CURRENCY_CODE CURRENCY_CODE
          ,PRT.ORG_UNIT_ID ORG_UNIT_ID
     FROM PARTNER PRT,
          V_PARTNER_TL V
    WHERE PRT.partner_type = 'E'
      AND PRT.partner_id = v.partner_id
/

COMMENT ON TABLE V_EXTERNAL_FINISHER IS 'This view extracts all the details for external finishers.'
/