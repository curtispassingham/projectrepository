--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_TICKET_TYPE_HEAD
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_TICKET_TYPE_HEAD"
CREATE TABLE DC_TICKET_TYPE_HEAD
(
   TICKET_TYPE_ID     VARCHAR2(4),
   TICKET_TYPE_DESC   VARCHAR2(120),
   SEL_IND            VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_TICKET_TYPE_HEAD is 'This table is a staging table for data conversion and will hold data for each ticket type that exists in the system.'
/
COMMENT ON COLUMN DC_TICKET_TYPE_HEAD.TICKET_TYPE_ID is 'This field contains a character string which uniquely identifies the ticket or label type.'
/
COMMENT ON COLUMN DC_TICKET_TYPE_HEAD.TICKET_TYPE_DESC is 'This field contains a description of the ticketor label type.'
/
COMMENT ON COLUMN DC_TICKET_TYPE_HEAD.SEL_IND is 'Indicates if the ticket type is a shelf edge label.'
/