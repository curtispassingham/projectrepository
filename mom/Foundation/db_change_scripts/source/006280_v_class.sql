--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_CLASS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_CLASS'
CREATE OR REPLACE FORCE VIEW V_CLASS
 ("DEPT"
 ,"CLASS"
 ,"CLASS_NAME")
 AS SELECT CLA.DEPT DEPT ,
         CLA.CLASS CLASS ,
         V.CLASS_NAME CLASS_NAME
   FROM CLASS CLA,
        DEPS DEP,
        V_CLASS_TL V
  WHERE DEP.DEPT = CLA.DEPT
    AND CLA.DEPT = V.DEPT
    AND CLA.CLASS = V.CLASS
/


COMMENT ON TABLE V_CLASS IS 'This vew will be used to display the Class LOVs using a security policy to filter User access. The class name is translated based on user language.'
/

COMMENT ON COLUMN V_CLASS."DEPT" IS 'Contains the number of the department of which the class is a member.'
/

COMMENT ON COLUMN V_CLASS."CLASS" IS 'Contains the number which uniquely identifies the class within the system.'
/

