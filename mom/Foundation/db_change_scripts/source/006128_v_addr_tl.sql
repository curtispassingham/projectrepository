CREATE OR REPLACE FORCE VIEW V_ADDR_TL (ADDR_KEY, ADD_1, ADD_2, ADD_3, CITY, CONTACT_NAME, COUNTY, LANG ) AS
SELECT  b.addr_key,
        case when tl.lang is not null then tl.add_1 else b.add_1 end add_1,
        case when tl.lang is not null then tl.add_2 else b.add_2 end add_2,
        case when tl.lang is not null then tl.add_3 else b.add_3 end add_3,
        case when tl.lang is not null then tl.city else b.city end city,
        case when tl.lang is not null then tl.contact_name else b.contact_name end contact_name,
        case when tl.lang is not null then tl.county else b.county end county,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  ADDR b,
        ADDR_TL tl
 WHERE  b.addr_key = tl.addr_key (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_ADDR_TL is 'This is the translation view for base table ADDR. This view fetches data in user langauge either from translation table ADDR_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_ADDR_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_ADDR_TL.ADDR_KEY is 'This column contains a unique number used to distinguish between different addresses.'
/

COMMENT ON COLUMN V_ADDR_TL.ADD_1 is 'This column contains the first line of the address.'
/

COMMENT ON COLUMN V_ADDR_TL.ADD_2 is 'This column contains the second line of the address.'
/

COMMENT ON COLUMN V_ADDR_TL.ADD_3 is 'This column contains the third line of the address.'
/

COMMENT ON COLUMN V_ADDR_TL.CITY is 'This column contains the name of the city that is associated with the address.'
/

COMMENT ON COLUMN V_ADDR_TL.CONTACT_NAME is 'This column contains the name of the contact for the supplier at this address.'
/

COMMENT ON COLUMN V_ADDR_TL.COUNTY is 'This column holds the county name for the location.'
/

