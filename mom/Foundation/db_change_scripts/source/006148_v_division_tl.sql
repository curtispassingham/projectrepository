CREATE OR REPLACE FORCE VIEW V_DIVISION_TL (DIVISION, DIV_NAME, LANG ) AS
SELECT  b.division,
        case when tl.lang is not null then tl.div_name else b.div_name end div_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  DIVISION b,
        DIVISION_TL tl
 WHERE  b.division = tl.division (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_DIVISION_TL is 'This is the translation view for base table DIVISION. This view fetches data in user langauge either from translation table DIVISION_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_DIVISION_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_DIVISION_TL.DIVISION is 'Contains the number which uniquely identifies the division of the company.'
/

COMMENT ON COLUMN V_DIVISION_TL.DIV_NAME is 'Contains the name which, along with the division number, identifies the division of the company.'
/

