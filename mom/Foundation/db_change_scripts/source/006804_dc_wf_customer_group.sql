--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_WF_CUSTOMER_GROUP
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_WF_CUSTOMER_GROUP"
CREATE TABLE DC_WF_CUSTOMER_GROUP
(
   WF_CUSTOMER_GROUP_ID       NUMBER(10),
   WF_CUSTOMER_GROUP_NAME     VARCHAR2(120)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_WF_CUSTOMER_GROUP is 'This table is a staging table for data conversion and will hold data for WF_CUSTOMER_GROUP.'
/
COMMENT ON COLUMN DC_WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_ID is 'This field will hold the unique identifier for the customer group.'
/
COMMENT ON COLUMN DC_WF_CUSTOMER_GROUP.WF_CUSTOMER_GROUP_NAME is 'This field will hold the customer group description'
/