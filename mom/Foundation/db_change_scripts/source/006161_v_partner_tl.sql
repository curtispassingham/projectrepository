CREATE OR REPLACE FORCE VIEW V_PARTNER_TL (PARTNER_TYPE, PARTNER_ID, PARTNER_DESC, PARTNER_NAME_SECONDARY, LANG ) AS
SELECT  b.partner_type,
        b.partner_id,
        case when tl.lang is not null then tl.partner_desc else b.partner_desc end partner_desc,
        case when tl.lang is not null then tl.partner_name_secondary else b.partner_name_secondary end partner_name_secondary,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  PARTNER b,
        PARTNER_TL tl
 WHERE  b.partner_type = tl.partner_type (+)
   AND  b.partner_id = tl.partner_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_PARTNER_TL is 'This is the translation view for base table PARTNER. This view fetches data in user langauge either from translation table PARTNER_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_PARTNER_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_PARTNER_TL.PARTNER_TYPE is 'Specifies the type of partner.  Valid values are Bank BK, Agent AG, Freight Forwarder FF, Importer IM, Broker BR, Factory FA, Applicant AP, Consolidator CO, and Consignee CN, Supplier hierarchy level 1 S1, Supplier hierarchy level 2 S2, Supplier hierarchy level 3 S3.'
/

COMMENT ON COLUMN V_PARTNER_TL.PARTNER_ID is 'Unique identifying number for a partner within the system.  The user determines this number when a new partner is first added to the system.'
/

COMMENT ON COLUMN V_PARTNER_TL.PARTNER_DESC is 'Contains the partners description or name.'
/

COMMENT ON COLUMN V_PARTNER_TL.PARTNER_NAME_SECONDARY is 'This wil hold the secondary name of the partner.'
/

