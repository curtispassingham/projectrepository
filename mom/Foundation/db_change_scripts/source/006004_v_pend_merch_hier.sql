
--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_PEND_MERCH_HIER
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_PEND_MERCH_HIER'
CREATE OR REPLACE FORCE VIEW V_PEND_MERCH_HIER
("HIER_TYPE",
 "MERCH_HIER_ID",
 "MERCH_HIER_PARENT_ID",
 "MERCH_HIER_GRANDPARENT_ID",
 "EFFECTIVE_DATE",
 "ACTION_TYPE",
 "MERCH_HIER_NAME",
 "DOMAIN",
 "BUYER",
 "MERCH",
 "PROFIT_CALC_TYPE",
 "PURCHASE_TYPE",
 "BUD_INT",
 "BUD_MKUP",
 "TOTAL_MARKET_AMT",
 "MARKUP_CALC_TYPE",
 "OTB_CALC_TYPE",
 "MAX_AVG_COUNTER",
 "AVG_TOLERANCE_PCT",
 "DEPT_VAT_INCL_IND",
 "CLASS_VAT_INCL_IND",
 "DIVISION",
 "DIV_NAME",
 "GROUP_NO",
 "GROUP_NAME",
 "DEPT",
 "DEPT_NAME",
 "CLASS",
 "CLASS_NAME",
 "SUBCLASS",
 "SUB_NAME")
AS
(
   SELECT PEND.*,
          DI.DIVISION   AS DIVISION,
          DI.DIV_NAME   AS DIV_NAME,
          NULL          AS GROUP_NO,
          NULL          AS GROUP_NAME,
          NULL          AS DEPT,
          NULL          AS DEPT_NAME,
          NULL          AS CLASS,
          NULL          AS CLASS_NAME,
          NULL          AS SUBCLASS,
          NULL          AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          DIVISION DI
    WHERE PEND.HIER_TYPE = 'V'
      AND PEND.MERCH_HIER_ID = DI.DIVISION
   UNION ALL
   SELECT PEND.*,
          DI.DIVISION   AS DIVISION,
          DI.DIV_NAME   AS DIV_NAME,
          GS.GROUP_NO   AS GROUP_NO,
          GS.GROUP_NAME AS GROUP_NAME,
          NULL          AS DEPT,
          NULL          AS DEPT_NAME,
          NULL          AS CLASS,
          NULL          AS CLASS_NAME,
          NULL          AS SUBCLASS,
          NULL          AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          DIVISION DI,
          GROUPS GS
    WHERE PEND.HIER_TYPE = 'G'
      AND PEND.MERCH_HIER_ID = GS.GROUP_NO
      AND DI.DIVISION = GS.DIVISION
   UNION ALL
   SELECT PEND.*,
          DI.DIVISION   AS DIVISION,
          DI.DIV_NAME   AS DIV_NAME,
          GS.GROUP_NO   AS GROUP_NO,
          GS.GROUP_NAME AS GROUP_NAME,
          DS.DEPT       AS DEPT,
          DS.DEPT_NAME  AS DEPT_NAME,
          NULL          AS CLASS,
          NULL          AS CLASS_NAME,
          NULL          AS SUBCLASS,
          NULL          AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          DIVISION DI,
          GROUPS GS,
          DEPS DS
    WHERE PEND.HIER_TYPE = 'D'
      AND PEND.MERCH_HIER_ID = DS.DEPT
      AND DI.DIVISION = GS.DIVISION
      AND GS.GROUP_NO = DS.GROUP_NO
   UNION ALL
   SELECT PEND.*,
          DI.DIVISION   AS DIVISION,
          DI.DIV_NAME   AS DIV_NAME,
          GS.GROUP_NO   AS GROUP_NO,
          GS.GROUP_NAME AS GROUP_NAME,
          DS.DEPT       AS DEPT,
          DS.DEPT_NAME  AS DEPT_NAME,
          CL.CLASS      AS CLASS,
          CL.CLASS_NAME AS CLASS_NAME,
          NULL          AS SUBCLASS,
          NULL          AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          DIVISION DI,
          GROUPS GS,
          DEPS DS,
          CLASS CL
    WHERE PEND.HIER_TYPE = 'C'
      AND PEND.MERCH_HIER_ID = CL.CLASS
      AND PEND.MERCH_HIER_PARENT_ID = CL.DEPT
      AND DI.DIVISION = GS.DIVISION
      AND GS.GROUP_NO = DS.GROUP_NO
      AND DS.DEPT = CL.DEPT
   UNION ALL
   SELECT PEND.*,
          DI.DIVISION   AS DIVISION,
          DI.DIV_NAME   AS DIV_NAME,
          GS.GROUP_NO   AS GROUP_NO,
          GS.GROUP_NAME AS GROUP_NAME,
          DS.DEPT       AS DEPT,
          DS.DEPT_NAME  AS DEPT_NAME,
          CL.CLASS      AS CLASS,
          CL.CLASS_NAME AS CLASS_NAME,
          SC.SUBCLASS   AS SUBCLASS,
          SC.SUB_NAME   AS SUB_NAME
     FROM PEND_MERCH_HIER PEND,
          DIVISION DI,
          GROUPS GS,
          DEPS DS,
          CLASS CL,
          SUBCLASS SC
    WHERE PEND.HIER_TYPE = 'S'
      AND PEND.MERCH_HIER_ID = SC.SUBCLASS
      AND PEND.MERCH_HIER_PARENT_ID = SC.CLASS
      AND PEND.MERCH_HIER_GRANDPARENT_ID = SC.DEPT
      AND DI.DIVISION = GS.DIVISION
      AND GS.GROUP_NO = DS.GROUP_NO
      AND DS.DEPT = CL.DEPT
      AND CL.DEPT = SC.DEPT
      AND CL.CLASS = SC.CLASS
)
/

 COMMENT ON COLUMN V_PEND_MERCH_HIER."HIER_TYPE" IS 'This column will identify which hierarchy is being added or updated.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."MERCH_HIER_ID" IS 'This column will contain the division, group, dept, class or the subclass number.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."MERCH_HIER_PARENT_ID" IS 'This column will contain the division_id if hier_type = group, group_id if hier_type= dept, dept number if hier_type= class and class number if hier_type= subclass.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."MERCH_HIER_GRANDPARENT_ID" IS 'This column will only be populated if the hier_type = subclass. If hier_type = subclass the column will contain the dept number.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."EFFECTIVE_DATE" IS 'This column will contain the effective date when the records from shadow table will be moved to the actual RMS merchandise hierarchy tables.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."ACTION_TYPE" IS 'This column will identify whether this is a new addition of division, group, dept, class or subclass or changes for existing division, group, dept, class or subclass.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."MERCH_HIER_NAME" IS 'This column will contain name of the division, group, department, class or subclass depending on the hier_type.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."DOMAIN" IS 'This column will contain the unique number representing the domain. This domain number is used when interfacing data to external systems using domains.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."BUYER" IS 'This column will contain the number that uniquely identifies the buyer for the hierarchy type.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."MERCH" IS 'This column will contain the number that uniquely identifies the merchandiser for the hierarchy type.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."PROFIT_CALC_TYPE" IS 'This column will contain a number that indicates whether profit will be calculated by Direct Cost or by Retail Inventory.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."PURCHASE_TYPE" IS 'This column will contain a code that indicates whether items in this department are normal merchandise or consignment stock.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."BUD_INT" IS 'This column will contain the Budgeted Intake percentage.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."BUD_MKUP" IS 'This column will contain the Budgeted Markup percentage.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."TOTAL_MARKET_AMT" IS 'This column will contain the total market amount that is expected for the hierarchy type.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."MARKUP_CALC_TYPE" IS 'This column will contain the code letter that determines how markup is calculated in this hierarchy type.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."OTB_CALC_TYPE" IS 'Otb_calc_type This column will contain the code letter that determines how OTB is calculated in this hierarchy type.'
/
 COMMENT ON COLUMN V_PEND_MERCH_HIER."MAX_AVG_COUNTER" IS 'This column will contain the maximum count of days with acceptable data to include in an average for items within the hierarchy type.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."AVG_TOLERANCE_PCT" IS 'This column will contain a tolerance percentage value used in averaging for items within this value.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."DEPT_VAT_INCL_IND" IS 'This column will contain an indicator that will be used to default the class level indicator when classes are initially set up for the department and will only be available when the system level class vat option is on.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."CLASS_VAT_INCL_IND" IS 'This column will contain an indicator that is used to determine whether retail is displayed and held with or without vat.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."DIVISION" IS 'Contains the number which uniquely identifies the division of the company.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."DIV_NAME" IS 'Contains the name which, along with the division number, identifies the division of the company.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."GROUP_NO" IS 'Contains the number which uniquely identifies the Group.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."GROUP_NAME" IS 'Contains the name which, along with the group number, identifies the group of the company.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."DEPT" IS 'Contains the number which uniquely identifies the Department.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."DEPT_NAME" IS 'Contains the name which, along with the department number, identifies the department of the company.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."CLASS" IS 'Contains the number which uniquely identifies the class within a department.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."CLASS_NAME" IS 'Contains the name which, along with the class number, identifies the class of the company.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."SUBCLASS" IS 'Contains the number which uniquely identifies the subclass within a specific department and class.'
/
COMMENT ON COLUMN V_PEND_MERCH_HIER."SUB_NAME" IS 'Contains the name which, along with the subclass number, identifies the subclass of the company.'
/




