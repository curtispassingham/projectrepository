CREATE OR REPLACE FORCE VIEW V_DIFF_RANGE_HEAD_TL (DIFF_RANGE, DIFF_RANGE_DESC, LANG ) AS
SELECT  b.diff_range,
        case when tl.lang is not null then tl.diff_range_desc else b.diff_range_desc end diff_range_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  DIFF_RANGE_HEAD b,
        DIFF_RANGE_HEAD_TL tl
 WHERE  b.diff_range = tl.diff_range (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_DIFF_RANGE_HEAD_TL is 'This is the translation view for base table DIFF_RANGE_HEAD. This view fetches data in user langauge either from translation table DIFF_RANGE_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_DIFF_RANGE_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_DIFF_RANGE_HEAD_TL.DIFF_RANGE is 'Contains the number used to uniquely identify the differentiator range.'
/

COMMENT ON COLUMN V_DIFF_RANGE_HEAD_TL.DIFF_RANGE_DESC is 'Contains the differentiator range description.'
/

