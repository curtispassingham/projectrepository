--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE ADDED:				COMPANY_CLOSED_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TYPE
--------------------------------------
PROMPT Creating Table 'COMPANY_CLOSED_TL'
CREATE TABLE COMPANY_CLOSED_TL(
LANG NUMBER(6) NOT NULL,
CLOSE_DATE DATE NOT NULL,
CLOSE_DESC VARCHAR2(120) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE COMPANY_CLOSED_TL is 'This is the translation table for COMPANY_CLOSED table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN COMPANY_CLOSED_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN COMPANY_CLOSED_TL.CLOSE_DATE is 'This field contains the date that the company is closed.'
/

COMMENT ON COLUMN COMPANY_CLOSED_TL.CLOSE_DESC is 'This field contains a description of the close.'
/

COMMENT ON COLUMN COMPANY_CLOSED_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN COMPANY_CLOSED_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN COMPANY_CLOSED_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN COMPANY_CLOSED_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE COMPANY_CLOSED_TL ADD CONSTRAINT PK_COMPANY_CLOSED_TL PRIMARY KEY (
LANG,
CLOSE_DATE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE COMPANY_CLOSED_TL
 ADD CONSTRAINT CCDT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE COMPANY_CLOSED_TL ADD CONSTRAINT CCDT_CCD_FK FOREIGN KEY (
CLOSE_DATE
) REFERENCES COMPANY_CLOSED (
CLOSE_DATE
)
/

