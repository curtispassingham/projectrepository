--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ITEM_COST_DETAIL'

PROMPT Creating Index 'ITEM_COST_DETAIL_I1'
CREATE INDEX ITEM_COST_DETAIL_I1 on ITEM_COST_DETAIL
  (ITEM,
   SUPPLIER,
   ORIGIN_COUNTRY_ID,
   DELIVERY_COUNTRY_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

