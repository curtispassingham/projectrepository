--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'PRICE_HIST'

PROMPT Creating Index 'PRICE_HIST_I4'
CREATE INDEX PRICE_HIST_I4 ON PRICE_HIST 
( 
 LOC
)
 LOCAL
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

