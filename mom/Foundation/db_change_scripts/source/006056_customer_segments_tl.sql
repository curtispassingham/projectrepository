--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       CUSTOMER_SEGMENTS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE CUSTOMER_SEGMENTS_TL(
LANG NUMBER(6) NOT NULL,
CUSTOMER_SEGMENT_ID NUMBER(10) NOT NULL,
CUSTOMER_SEGMENT_DESC VARCHAR2(120) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE CUSTOMER_SEGMENTS_TL is 'This is the translation table for CUSTOMER_SEGMENTS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN CUSTOMER_SEGMENTS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN CUSTOMER_SEGMENTS_TL.CUSTOMER_SEGMENT_ID is 'The customer segment id of a given customer segment type, referenced and attached to promotions in RPM. They will be available in RPM promotion UI screen for customer segment promotions.'
/

COMMENT ON COLUMN CUSTOMER_SEGMENTS_TL.CUSTOMER_SEGMENT_DESC is 'The description of customer segment id.'
/

COMMENT ON COLUMN CUSTOMER_SEGMENTS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN CUSTOMER_SEGMENTS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN CUSTOMER_SEGMENTS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN CUSTOMER_SEGMENTS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE CUSTOMER_SEGMENTS_TL ADD CONSTRAINT PK_CUSTOMER_SEGMENTS_TL PRIMARY KEY (
LANG,
CUSTOMER_SEGMENT_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE CUSTOMER_SEGMENTS_TL
 ADD CONSTRAINT CST_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE CUSTOMER_SEGMENTS_TL ADD CONSTRAINT CST_CS_FK FOREIGN KEY (
CUSTOMER_SEGMENT_ID
) REFERENCES CUSTOMER_SEGMENTS (
CUSTOMER_SEGMENT_ID
)
/

