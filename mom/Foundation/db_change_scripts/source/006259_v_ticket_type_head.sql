--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_TICKET_TYPE_HEAD
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_TICKET_TYPE_HEAD'

CREATE OR REPLACE FORCE VIEW V_TICKET_TYPE_HEAD ( TICKET_TYPE_ID, TICKET_TYPE_DESC, FILTER_ORG_ID, FILTER_MERCH_ID, FILTER_MERCH_ID_CLASS, FILTER_MERCH_ID_SUBCLASS ) AS
SELECT TTH.TICKET_TYPE_ID TICKET_TYPE_ID
       ,V.TICKET_TYPE_DESC TICKET_TYPE_DESC
       ,TTH.FILTER_ORG_ID FILTER_ORG_ID
       ,TTH.FILTER_MERCH_ID FILTER_MERCH_ID
       ,TTH.FILTER_MERCH_ID_CLASS FILTER_MERCH_ID_CLASS
       ,TTH.FILTER_MERCH_ID_SUBCLASS FILTER_MERCH_ID_SUBCLASS
  FROM TICKET_TYPE_HEAD TTH,
       V_TICKET_TYPE_HEAD_TL V
 WHERE TTH.TICKET_TYPE_ID = V.TICKET_TYPE_ID
/

COMMENT ON TABLE V_TICKET_TYPE_HEAD IS 'This view will be used to display the Ticket Type LOVs using a security policy to filter User access. The description is translated based on user language.'
/
