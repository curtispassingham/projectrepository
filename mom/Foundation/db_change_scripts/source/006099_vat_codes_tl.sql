--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       VAT_CODES_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE VAT_CODES_TL(
LANG NUMBER(6) NOT NULL,
VAT_CODE VARCHAR2(6) NOT NULL,
VAT_CODE_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE VAT_CODES_TL is 'This is the translation table for VAT_CODES table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN VAT_CODES_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN VAT_CODES_TL.VAT_CODE is 'This field contains the alphanumeric identification for the VAT code. Valid values include: S - Standard C - Composite Z - Zero E - Exempt Other values may also be entered. These are the default VAT Rates that are set-up upon installation of the RMS.'
/

COMMENT ON COLUMN VAT_CODES_TL.VAT_CODE_DESC is 'Contains a description identifying the VAT code.'
/

COMMENT ON COLUMN VAT_CODES_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN VAT_CODES_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN VAT_CODES_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN VAT_CODES_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE VAT_CODES_TL ADD CONSTRAINT PK_VAT_CODES_TL PRIMARY KEY (
LANG,
VAT_CODE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE VAT_CODES_TL
 ADD CONSTRAINT VCT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE VAT_CODES_TL ADD CONSTRAINT VCT_VC_FK FOREIGN KEY (
VAT_CODE
) REFERENCES VAT_CODES (
VAT_CODE
)
/

