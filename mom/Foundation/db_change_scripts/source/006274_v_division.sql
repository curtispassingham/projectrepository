--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_DIVISION
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_DIVISION'

CREATE OR REPLACE FORCE VIEW V_DIVISION
 (DIVISION 
 ,DIV_NAME)
 AS SELECT DIV.DIVISION DIVISION
          ,V.DIV_NAME DIV_NAME
FROM DIVISION DIV,
     V_DIVISION_TL V
WHERE V.DIVISION = DIV.DIVISION     
/

COMMENT ON TABLE V_DIVISION IS 'This view will be used to display the division information using a security policy to filter user access.'
/

COMMENT ON COLUMN V_DIVISION."DIVISION" IS 'Contains the number which uniquely identifies the division.'
/

COMMENT ON COLUMN V_DIVISION."DIV_NAME" IS 'Contains the translated name of the division.'
/