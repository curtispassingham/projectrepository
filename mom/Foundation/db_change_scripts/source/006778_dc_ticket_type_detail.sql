--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_TICKET_TYPE_DETAIL
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_TICKET_TYPE_DETAIL"
CREATE TABLE DC_TICKET_TYPE_DETAIL
(
   TICKET_TYPE_ID        VARCHAR2(4),
   TICKET_ITEM_ID        VARCHAR2(4),
   UDA_ID                NUMBER(5)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_TICKET_TYPE_DETAIL is 'This table is a staging table for data conversion and will hold one row for each item which will be on the ticket.'
/
COMMENT ON COLUMN DC_TICKET_TYPE_DETAIL.TICKET_TYPE_ID is 'This field contains a character string which uniquely identifies the ticket type.'
/
COMMENT ON COLUMN DC_TICKET_TYPE_DETAIL.TICKET_ITEM_ID is 'This field contains a character string which uniquely identifies an attribute which will appear on a ticket or label such as retail price or price per unit of measure.'
/
COMMENT ON COLUMN DC_TICKET_TYPE_DETAIL.UDA_ID is 'This field contains a number which uniquely defines a user-defined attribute which is to be printed on this ticket type.'
/
