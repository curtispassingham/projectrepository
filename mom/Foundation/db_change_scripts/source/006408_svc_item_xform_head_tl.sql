--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

----------------------------------------------------------
--       Modifying Table        SVC_ITEM_XFORM_HEAD_TL   
----------------------------------------------------------

PROMPT Dropping Constraint 'SVC_ITEM_XFORM_HEAD_TL_UK'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'SVC_ITEM_XFORM_HEAD_TL_UK'
     AND CONSTRAINT_TYPE = 'U';

  if (L_table_exists != 0) then
      execute immediate 'ALTER TABLE SVC_ITEM_XFORM_HEAD_TL DROP CONSTRAINT SVC_ITEM_XFORM_HEAD_TL_UK';
  end if;
end;
/

PROMPT Modifying Table 'SVC_ITEM_XFORM_HEAD_TL'
ALTER TABLE SVC_ITEM_XFORM_HEAD_TL RENAME COLUMN ITEM to HEAD_ITEM
/

COMMENT ON COLUMN SVC_ITEM_XFORM_HEAD_TL.HEAD_ITEM is 'Refer to ITEM_XFORM_HEAD.HEAD_ITEM.'
/

ALTER TABLE SVC_ITEM_XFORM_HEAD_TL ADD CONSTRAINT SVC_ITEM_XFORM_HEAD_TL_UK UNIQUE 
 (LANG, 
  HEAD_ITEM) 
  USING INDEX 
  INITRANS 12 
  TABLESPACE RETAIL_INDEX
  /
