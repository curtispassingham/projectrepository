--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_PEND_MERCH_HIER
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_PEND_MERCH_HIER'
DROP VIEW V_PEND_MERCH_HIER;

CREATE VIEW V_PEND_MERCH_HIER AS
(
   select pend.*,
          NVL(pend.merch_hier_id,   di.division) as division,
          NVL(pend.merch_hier_name, di.div_name) as div_name,
          NULL as group_no,
          NULL as group_name,
          NULL as dept,
          NULL as dept_name,
          NULL as class,
          NULL as class_name,
          NULL as subclass,
          NULL as sub_name
     from pend_merch_hier pend,
          division di
    where pend.hier_type     = 'V'
      and pend.merch_hier_id = di.division (+)
   UNION ALL
   select pend.*,
          NVL(divparent.merch_hier_id,   di.division)   as division,
          NVL(divparent.merch_hier_name, di.div_name)   as div_name,
          NVL(pend.merch_hier_id,        gs.group_no)   as group_no,
          NVL(pend.merch_hier_name,      gs.group_name) as group_name,
          NULL as dept,
          NULL as dept_name,
          NULL as class,
          NULL as class_name,
          NULL as subclass,
          NULL as sub_name
     from pend_merch_hier pend,
          (select merch_hier_id, merch_hier_name
             from pend_merch_hier
            where hier_type = 'V') divparent,
          division di,
          groups gs
    where pend.hier_type            = 'G'
      and pend.merch_hier_id        = gs.group_no (+)
      and pend.merch_hier_parent_id = di.division (+)
      and pend.merch_hier_parent_id = divparent.merch_hier_id (+)
      and di.division               = gs.division (+)
   UNION ALL
   select pend.*,
          NVL(p2.merch_hier_parent_id, hier.division)   as division,
          NVL(p3.merch_hier_name,      hier.div_name)   as div_name,
          NVL(p2.merch_hier_id,        hier.group_no)   as group_no,
          NVL(p2.merch_hier_name,      hier.group_name) as group_name,
          NVL(pend.merch_hier_id,      ds.dept)         as dept,
          NVL(pend.merch_hier_name,    ds.dept_name)    as dept_name,
          NULL as class,
          NULL as class_name,
          NULL as subclass,
          NULL as sub_name
     from pend_merch_hier pend,
          deps ds,
          (select merch_hier_id, merch_hier_parent_id, merch_hier_name
             from pend_merch_hier
            where hier_type = 'G') p2,
          (select merch_hier_id, merch_hier_name
             from pend_merch_hier
            where hier_type = 'V') p3,
          (select NVL(gs.new_division, gs.old_division) division, div.new_div_name div_name,
                  NVL(gs.new_group_no, gs.old_group_no) group_no,
                  NVL(gs.new_group_name, gs.old_group_name) group_name
            from (select grppend.merch_hier_parent_id new_division, grp.division old_division,
                         grppend.merch_hier_id new_group_no, grp.group_no old_group_no,
                         grppend.merch_hier_name new_group_name, grp.group_name old_group_name
                    from (select merch_hier_id, merch_hier_parent_id, merch_hier_name
                            from pend_merch_hier
                           where hier_type = 'G'
                             and action_type = 'M') grppend,
                         groups grp
                   where grp.group_no = grppend.merch_hier_id (+)) gs,
                  (select div.division orig_div, div.div_name orig_name,
                          NVL(divpend.merch_hier_id, div.division) new_division,
                          NVL(divpend.merch_hier_name, div.div_name) new_div_name
                     from division div,
                         (select merch_hier_id, merch_hier_name
                            from pend_merch_hier
                           where hier_type = 'V'
                             and action_type = 'M') divpend
                    where div.division = divpend.merch_hier_id (+)) div
            where gs.old_division = div.new_division (+)) hier
    where pend.hier_type            = 'D'
      and pend.merch_hier_id        = ds.dept (+)
      and pend.merch_hier_parent_id = hier.group_no (+)
      and hier.group_no             = ds.group_no (+)
      and pend.merch_hier_parent_id = p2.merch_hier_id (+)
      and p2.merch_hier_parent_id   = p3.merch_hier_id (+)
   UNION ALL
   select pend.*,
          NVL(dept.division,        hier.division)   as division,
          NVL(dept.div_name,        hier.div_name)   as div_name,
          NVL(dept.group_no,        hier.group_no)   as group_no,
          NVL(dept.group_name,      hier.group_name) as group_name,
          NVL(dept.dept,            hier.dept)       as dept,
          NVL(dept.dept_name,       hier.dept_name)  as dept_name,
          NVL(pend.merch_hier_id,   cl.class)        as class,
          NVL(pend.merch_hier_name, cl.class_name)   as class_name,
          NULL as subclass,
          NULL as sub_name
     from pend_merch_hier pend,
          class cl,
          (select NVL(p2.merch_hier_parent_id, hier.division)   as division,
                  NVL(p3.merch_hier_name,      hier.div_name)   as div_name,
                  NVL(p2.merch_hier_id,        hier.group_no)   as group_no,
                  NVL(p2.merch_hier_name,      hier.group_name) as group_name,
                  NVL(pend.merch_hier_id,      ds.dept)         as dept,
                  NVL(pend.merch_hier_name,    ds.dept_name)    as dept_name
             from pend_merch_hier pend,
                  deps ds,
                  (select merch_hier_id, merch_hier_parent_id, merch_hier_name
                     from pend_merch_hier
                    where hier_type = 'G') p2,
                  (select merch_hier_id, merch_hier_name
                     from pend_merch_hier
                    where hier_type = 'V') p3,
                  (select NVL(gs.new_division, gs.old_division) division, div.new_div_name div_name,
                          NVL(gs.new_group_no, gs.old_group_no) group_no,
                          NVL(gs.new_group_name, gs.old_group_name) group_name
                    from (select grppend.merch_hier_parent_id new_division, grp.division old_division,
                                 grppend.merch_hier_id new_group_no, grp.group_no old_group_no,
                                 grppend.merch_hier_name new_group_name, grp.group_name old_group_name
                            from (select merch_hier_id, merch_hier_parent_id, merch_hier_name
                                    from pend_merch_hier
                                   where hier_type = 'G'
                                     and action_type = 'M') grppend,
                                 groups grp
                           where grp.group_no = grppend.merch_hier_id (+)) gs,
                          (select div.division orig_div, div.div_name orig_name,
                                  nvl(divpend.merch_hier_id, div.division) new_division,
                                  nvl(divpend.merch_hier_name, div.div_name) new_div_name
                             from division div,
                                 (select merch_hier_id, merch_hier_name
                                    from pend_merch_hier
                                   where hier_type = 'V'
                                     and action_type = 'M') divpend
                            where div.division = divpend.merch_hier_id (+)) div
                    where gs.old_division = div.new_division (+)) hier
            where pend.hier_type            = 'D'
              and pend.merch_hier_id        = ds.dept (+)
              and pend.merch_hier_parent_id = hier.group_no (+)
              and hier.group_no             = ds.group_no (+)
              and pend.merch_hier_parent_id = p2.merch_hier_id (+)
              and p2.merch_hier_parent_id   = p3.merch_hier_id (+)) dept,
          (select di.division, di.div_name,
                  gs.group_no, gs.group_name,
                  ds.dept, ds.dept_name
             from division di,
                  groups gs,
                  deps ds
            where di.division = gs.division
              and gs.group_no = ds.group_no) hier
    where pend.hier_type            = 'C'
      and pend.merch_hier_id        = cl.class (+)
      and pend.merch_hier_parent_id = hier.dept (+)
      and hier.dept                 = cl.dept (+)
      and pend.merch_hier_parent_id = dept.dept (+)
   UNION ALL
   select pend.*,
          NVL(deptgparent.division,   hier.division)   as division,
          NVL(deptgparent.div_name,   hier.div_name)   as div_name,
          NVL(deptgparent.group_no,   hier.group_no)   as group_no,
          NVL(deptgparent.group_name, hier.group_name) as group_name,
          NVL(deptgparent.dept,       hier.dept)       as dept,
          NVL(deptgparent.dept_name,  hier.dept_name)  as dept_name,
          NVL(clparent.class,         hier.class)      as class,
          NVL(clparent.class_name,    hier.class_name) as class_name,
          NVL(pend.merch_hier_id,     sc.subclass)     as subclass,
          NVL(pend.merch_hier_name,   sc.sub_name)     as sub_name
     from pend_merch_hier pend,
          subclass sc,
          (select di.division, di.div_name,
                  gs.group_no, gs.group_name,
                  ds.dept, ds.dept_name,
                  cl.class, cl.class_name
             from division di,
                  groups gs,
                  deps ds,
                  class cl
            where di.division = gs.division
              and gs.group_no = ds.group_no
              and ds.dept  = cl.dept) hier,
          (select nvl(dept.division,        hier.division)   as division,
                  nvl(dept.div_name,        hier.div_name)   as div_name,
                  nvl(dept.group_no,        hier.group_no)   as group_no,
                  nvl(dept.group_name,      hier.group_name) as group_name,
                  nvl(dept.dept,            hier.dept)       as dept,
                  nvl(dept.dept_name,       hier.dept_name)  as dept_name,
                  nvl(pend.merch_hier_id,   cl.class)        as class,
                  nvl(pend.merch_hier_name, cl.class_name)   as class_name
             from pend_merch_hier pend,
                  class cl,
                  (select nvl(p2.merch_hier_parent_id, hier.division)        as division,
                          nvl(p3.merch_hier_name,      hier.div_name)        as div_name,
                          nvl(p2.merch_hier_id,        hier.group_no)        as group_no,
                          nvl(p2.merch_hier_name,      hier.group_name)      as group_name,
                          nvl(ds.dept,                 pend.merch_hier_id)   as dept,
                          nvl(ds.dept_name,            pend.merch_hier_name) as dept_name
                     from pend_merch_hier pend,
                          deps ds,
                          (select merch_hier_id, merch_hier_parent_id, merch_hier_name
                             from pend_merch_hier
                            where hier_type = 'G') p2,
                          (select merch_hier_id, merch_hier_name
                             from pend_merch_hier
                            where hier_type = 'V') p3,
                          (select nvl(gs.new_division, gs.old_division) division, div.new_div_name div_name,
                                  nvl(gs.new_group_no, gs.old_group_no) group_no,
                                  nvl(gs.new_group_name, gs.old_group_name) group_name
                            from (select grppend.merch_hier_parent_id new_division, grp.division old_division,
                                         grppend.merch_hier_id new_group_no, grp.group_no old_group_no,
                                         grppend.merch_hier_name new_group_name, grp.group_name old_group_name
                                    from (select merch_hier_id, merch_hier_parent_id, merch_hier_name
                                            from pend_merch_hier
                                           where hier_type = 'G'
                                             and action_type = 'M') grppend,
                                         groups grp
                                   where grp.group_no = grppend.merch_hier_id (+)) gs,
                                  (select div.division orig_div, div.div_name orig_name,
                                          nvl(divpend.merch_hier_id, div.division) new_division,
                                          nvl(divpend.merch_hier_name, div.div_name) new_div_name
                                     from division div,
                                         (select merch_hier_id, merch_hier_name
                                            from pend_merch_hier
                                           where hier_type = 'V'
                                             and action_type = 'M') divpend
                                    where div.division = divpend.merch_hier_id (+)) div
                            where gs.old_division = div.new_division (+)) hier
                    where pend.hier_type            = 'D'
                      and pend.merch_hier_id        = ds.dept (+)
                      and pend.merch_hier_parent_id = hier.group_no (+)
                      and hier.group_no             = ds.group_no (+)
                      and pend.merch_hier_parent_id = p2.merch_hier_id (+)
                      and p2.merch_hier_parent_id   = p3.merch_hier_id (+)) dept,
                  (select di.division, di.div_name,
                          gs.group_no, gs.group_name,
                          ds.dept, ds.dept_name
                     from division di,
                          groups gs,
                          deps ds
                    where di.division = gs.division
                      and gs.group_no = ds.group_no) hier
            where pend.hier_type            = 'C'
              and pend.merch_hier_id        = cl.class (+)
              and pend.merch_hier_parent_id = hier.dept (+)
              and hier.dept                 = cl.dept (+)
              and pend.merch_hier_parent_id = dept.dept (+)) clparent,
        (select nvl(p2.merch_hier_parent_id, hier.division)   as division,
                nvl(p3.merch_hier_name,      hier.div_name)   as div_name,
                nvl(p2.merch_hier_id,        hier.group_no)   as group_no,
                nvl(p2.merch_hier_name,      hier.group_name) as group_name,
                nvl(pend.merch_hier_id,      ds.dept)         as dept,
                nvl(pend.merch_hier_name,    ds.dept_name)    as dept_name
           from pend_merch_hier pend,
                deps ds,
                (select merch_hier_id, merch_hier_parent_id, merch_hier_name
                   from pend_merch_hier
                  where hier_type = 'G') p2,
                (select merch_hier_id, merch_hier_name
                   from pend_merch_hier
                  where hier_type = 'V') p3,
                (select nvl(gs.new_division, gs.old_division) division, div.new_div_name div_name,
                        nvl(gs.new_group_no, gs.old_group_no) group_no,
                        nvl(gs.new_group_name, gs.old_group_name) group_name
                  from (select grppend.merch_hier_parent_id new_division, grp.division old_division,
                               grppend.merch_hier_id new_group_no, grp.group_no old_group_no,
                               grppend.merch_hier_name new_group_name, grp.group_name old_group_name
                          from (select merch_hier_id, merch_hier_parent_id, merch_hier_name
                                  from pend_merch_hier
                                 where hier_type = 'G'
                                   and action_type = 'M') grppend,
                               groups grp
                         where grp.group_no = grppend.merch_hier_id (+)) gs,
                        (select div.division orig_div, div.div_name orig_name,
                                nvl(divpend.merch_hier_id, div.division) new_division,
                                nvl(divpend.merch_hier_name, div.div_name) new_div_name
                           from division div,
                               (select merch_hier_id, merch_hier_name
                                  from pend_merch_hier
                                 where hier_type = 'V'
                                   and action_type = 'M') divpend
                          where div.division = divpend.merch_hier_id (+)) div
                  where gs.old_division = div.new_division (+)) hier
          where pend.hier_type            = 'D'
            and pend.merch_hier_id        = ds.dept (+)
            and pend.merch_hier_parent_id = hier.group_no (+)
            and hier.group_no             = ds.group_no (+)
            and pend.merch_hier_parent_id = p2.merch_hier_id (+)
            and p2.merch_hier_parent_id   = p3.merch_hier_id (+)) deptgparent
    where pend.hier_type                 = 'S'
      and pend.merch_hier_id             = sc.subclass (+)
      and pend.merch_hier_parent_id      = sc.class (+)
      and pend.merch_hier_grandparent_id = sc.dept (+)
      and pend.merch_hier_parent_id      = hier.class (+)
      and pend.merch_hier_grandparent_id = hier.dept (+)
      and hier.dept                      = sc.dept (+)
      and hier.class                     = sc.class (+)
      and pend.merch_hier_parent_id      = clparent.class (+)
      and pend.merch_hier_grandparent_id = clparent.dept (+)
      and pend.merch_hier_grandparent_id = deptgparent.dept (+)
)
;
