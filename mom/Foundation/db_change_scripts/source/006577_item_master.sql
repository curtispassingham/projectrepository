--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ITEM_MASTER'

PROMPT Creating Index 'ITEM_MASTER_I12'
CREATE INDEX ITEM_MASTER_I12 on ITEM_MASTER
  (ITEM_AGGREGATE_IND,
   DEPT
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

