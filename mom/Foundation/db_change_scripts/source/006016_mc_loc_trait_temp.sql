--------------------------------------------------------
-- Copyright (c) 2010, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision: 1.6 $
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE UPDATED:				MC_LOC_TRAIT_TEMP
--	COLUMNS UPDATED:				LOC_LIST
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Altering Table
--------------------------------------
PROMPT Altering table MC_LOC_TRAIT_TEMP
ALTER TABLE MC_LOC_TRAIT_TEMP MODIFY LOC_LIST NUMBER(10,0)
/