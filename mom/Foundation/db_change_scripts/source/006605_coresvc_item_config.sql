--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'CORESVC_ITEM_CONFIG'
ALTER TABLE CORESVC_ITEM_CONFIG ADD CASCADE_IUD_ITEM_SUPPLIER VARCHAR2 (1 ) DEFAULT 'Y' NOT NULL
/

COMMENT ON COLUMN CORESVC_ITEM_CONFIG.CASCADE_IUD_ITEM_SUPPLIER is 'This field indicates whether the inserts, updates or deletes on item/supplier details should be cascaded to the child items or not.'
/


PROMPT ADDING CONSTRAINT 'CHK_CORESVC_ITEM_CONFIG_9'
ALTER TABLE CORESVC_ITEM_CONFIG ADD CONSTRAINT
 CHK_CORESVC_ITEM_CONFIG_9 CHECK (CASCADE_IUD_ITEM_SUPPLIER IN ('Y','N'))
/
