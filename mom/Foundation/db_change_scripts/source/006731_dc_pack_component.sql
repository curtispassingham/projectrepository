--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_PACK_COMPONENT
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_PACK_COMPONENT"
CREATE TABLE DC_PACK_COMPONENT
(
   PACK_NO                   VARCHAR2(25),
   ITEM                      VARCHAR2(25),
   PACK_ITEM_QTY             NUMBER(12,4)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_PACK_COMPONENT is 'This table is a staging table for data conversion and will holds the pack and its component.'
/
COMMENT ON COLUMN DC_PACK_COMPONENT.PACK_NO is 'Alphanumeric value that uniquely identifies the pack for which details are held in this table.'
/
COMMENT ON COLUMN DC_PACK_COMPONENT.ITEM is 'Alphanumeric value that identifies the component item within the pack.'
/
COMMENT ON COLUMN DC_PACK_COMPONENT.PACK_ITEM_QTY is 'Contains the quantity of component items within the pack.'
/