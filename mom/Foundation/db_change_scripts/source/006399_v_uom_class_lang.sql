--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      VIEW ADDED:                             V_UOM_CLASS_LANG
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING VIEW
--------------------------------------
PROMPT Creating View 'V_UOM_CLASS_LANG'
CREATE OR REPLACE FORCE VIEW V_UOM_CLASS_LANG
 (UOM,
  UOM_CLASS,
  UOM_TRANS,
  UOM_DESC_TRANS,
  LANG,
  ISO_CODE)
  AS
  SELECT B.UOM UOM,
         B.UOM_CLASS UOM_CLASS,
         T.UOM_TRANS UOM_TRANS,
         T.UOM_DESC_TRANS UOM_DESC_TRANS,
         T.LANG LANG,
         L.ISO_CODE ISO_CODE
    FROM UOM_CLASS B,
         UOM_CLASS_TL T,
         LANG L
   WHERE B.UOM = T.UOM
     AND L.LANG   = T.LANG
/

COMMENT ON COLUMN V_UOM_CLASS_LANG.UOM IS 'Contains a string that uniquely identifies the unit of measure. Example:  LBS for pounds.'
/
COMMENT ON COLUMN V_UOM_CLASS_LANG.UOM_CLASS IS 'Contains the unit of measure type used as a grouping mechanism for the many UOM options.  When converting from one UOM to another, the class is used to determine how the system proceeds with the conversion, whether it is an in-class or across-class conversion.'
/
COMMENT ON COLUMN V_UOM_CLASS_LANG.UOM_TRANS IS 'Translated Unit Of Measurement.'
/
COMMENT ON COLUMN V_UOM_CLASS_LANG.UOM_DESC_TRANS IS 'Translated UOM description.'
/
COMMENT ON COLUMN V_UOM_CLASS_LANG.LANG IS 'Contains the number which uniquely identifies a language in RMS.'
/
COMMENT ON COLUMN V_UOM_CLASS_LANG.ISO_CODE IS 'This field holds the ISO code associated with the given language.'
/
COMMENT ON TABLE  V_UOM_CLASS_LANG IS 'This translation view returns Unit Of Measurement along with its descriptions in multiple languages. It is used by RPM.'
/
