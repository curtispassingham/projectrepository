--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_S9T_LIST_VALS
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_S9T_LIST_VALS'
CREATE TABLE SVC_S9T_LIST_VALS
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS    VARCHAR2(10) DEFAULT 'N' ,
    CODE              VARCHAR2(255) ,
    COLUMN_NAME       VARCHAR2(255) ,
    SHEET_NAME        VARCHAR2(255) ,
    TEMPLATE_CATEGORY VARCHAR2(255) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE 
	)	
	INITRANS 6
	TABLESPACE RETAIL_DATA
/


COMMENT ON TABLE SVC_S9T_LIST_VALS IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in S9T_LIST_VALS.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.CODE IS 'The code_detail code from which the drop down values should be obtained from.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.COLUMN_NAME IS 'The column-name to which this drop downmapping belongs to.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.SHEET_NAME IS 'The sheet-name to which this drop down mapping belongs to.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.TEMPLATE_CATEGORY IS 'The template category. Valid values are stored against code-type S9TC.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_S9T_LIST_VALS.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/

PROMPT Creating Primary Key on 'SVC_S9T_LIST_VALS'   
ALTER TABLE SVC_S9T_LIST_VALS
ADD CONSTRAINT SVC_S9T_LIST_VALS_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX 
/

PROMPT Creating Unique Key on 'SVC_S9T_LIST_VALS'
ALTER TABLE SVC_S9T_LIST_VALS
ADD CONSTRAINT SVC_S9T_LIST_VALS_UK UNIQUE ( TEMPLATE_CATEGORY,SHEET_NAME,COLUMN_NAME ) 
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX  
/	
	
	
