CREATE OR REPLACE FORCE VIEW V_PEND_MERCH_HIER_TL (HIER_TYPE, MERCH_HIER_ID, MERCH_HIER_PARENT_ID, MERCH_HIER_GRANDPARENT_ID, MERCH_HIER_NAME, LANG ) AS
SELECT  b.hier_type,
        b.merch_hier_id,
        b.merch_hier_parent_id,
        b.merch_hier_grandparent_id,
        case when tl.lang is not null then tl.merch_hier_name else b.merch_hier_name end merch_hier_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  PEND_MERCH_HIER b,
        PEND_MERCH_HIER_TL tl
 WHERE  b.hier_type = tl.hier_type (+)
   AND  b.merch_hier_id = tl.merch_hier_id (+)
   AND  b.merch_hier_parent_id = tl.merch_hier_parent_id (+)
   AND  b.merch_hier_grandparent_id = tl.merch_hier_grandparent_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_PEND_MERCH_HIER_TL is 'This is the translation view for base table PEND_MERCH_HIER. This view fetches data in user langauge either from translation table PEND_MERCH_HIER_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_PEND_MERCH_HIER_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_PEND_MERCH_HIER_TL.HIER_TYPE is 'This column will identify which hierarchy is being added or updated.  The values for this column will be V- Division, G- Group D dept, C class and S subclass'
/

COMMENT ON COLUMN V_PEND_MERCH_HIER_TL.MERCH_HIER_ID is 'This column will contain the division, group, dept, class or the subclass number depending on the hier_type. If the action_type is _A_, then this id will be generated using the current sequence number generator for division, group, dept, class and subclass depending on the hier_type.'
/

COMMENT ON COLUMN V_PEND_MERCH_HIER_TL.MERCH_HIER_PARENT_ID is 'This column will contain the division_id if hier_type = group, group_id if hier_type= dept, dept number if hier_type= class and class number if hier_type= subclass.'
/

COMMENT ON COLUMN V_PEND_MERCH_HIER_TL.MERCH_HIER_GRANDPARENT_ID is 'This column will only be populated if the hier_type = subclass. If hier_type = subclass the column will contain the dept number.'
/

COMMENT ON COLUMN V_PEND_MERCH_HIER_TL.MERCH_HIER_NAME is 'This column will contain name of the division, group, department, class or subclass depending on the hier_type'
/

