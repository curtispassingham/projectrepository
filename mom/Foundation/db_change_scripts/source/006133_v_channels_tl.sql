CREATE OR REPLACE FORCE VIEW V_CHANNELS_TL (CHANNEL_ID, CHANNEL_NAME, LANG ) AS
SELECT  b.channel_id,
        case when tl.lang is not null then tl.channel_name else b.channel_name end channel_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  CHANNELS b,
        CHANNELS_TL tl
 WHERE  b.channel_id = tl.channel_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_CHANNELS_TL is 'This is the translation view for base table CHANNELS. This view fetches data in user langauge either from translation table CHANNELS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_CHANNELS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_CHANNELS_TL.CHANNEL_ID is 'This column will contain the number that uniquely identifies the channel.'
/

COMMENT ON COLUMN V_CHANNELS_TL.CHANNEL_NAME is 'Contains the name of the channel.'
/

