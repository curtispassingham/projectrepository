--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_ITEM_SUPPLIER_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_ITEM_SUPPLIER_TL'
CREATE TABLE SVC_ITEM_SUPPLIER_TL
 (PROCESS_ID NUMBER(10) NOT NULL,
  CHUNK_ID NUMBER(10) NOT NULL,
  ROW_SEQ NUMBER(20) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ),
  LANG NUMBER(6),
  ITEM VARCHAR2(25 ),
  SUPPLIER NUMBER(10),
  SUPP_LABEL VARCHAR2(15 ),
  SUPP_DIFF_1 VARCHAR2(120 ),
  SUPP_DIFF_2 VARCHAR2(120 ),
  SUPP_DIFF_3 VARCHAR2(120 ),
  SUPP_DIFF_4 VARCHAR2(120 ),
  CREATE_ID VARCHAR2(30 ),
  CREATE_DATETIME DATE,
  LAST_UPDATE_ID VARCHAR2(30 ),
  LAST_UPDATE_DATETIME DATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_ITEM_SUPPLIER_TL is 'This is a staging table used for item induction spreadsheet upload process. It is used to temporarily hold data before it is uploaded/updated in ITEM_SUPPLIER_TL.'''
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.LANG is 'Refer to ITEM_SUPPLIER_TL.LANG column.'
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.ITEM is 'Refer to ITEM_SUPPLIER_TL.ITEM column.'
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.SUPPLIER is 'Refer to ITEM_SUPPLIER_TL.SUPPLIER column.'
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.SUPP_LABEL is 'Refer to ITEM_SUPPLIER_TL.SUPP_LABEL column. '
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.SUPP_DIFF_1 is 'Refer to ITEM_SUPPLIER_TL.SUPP_DIFF_1 column. '
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.SUPP_DIFF_2 is 'Refer to ITEM_SUPPLIER_TL.SUPP_DIFF_2 column. '
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.SUPP_DIFF_3 is 'Refer to ITEM_SUPPLIER_TL.SUPP_DIFF_3 column. '
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.SUPP_DIFF_4 is 'Refer to ITEM_SUPPLIER_TL.SUPP_DIFF_4 column. '
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.CREATE_ID is 'User who created the record.'
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.CREATE_DATETIME is 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.LAST_UPDATE_ID is 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_ITEM_SUPPLIER_TL.LAST_UPDATE_DATETIME is 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_ITEM_SUPPLIER_TL'
ALTER TABLE SVC_ITEM_SUPPLIER_TL
 ADD CONSTRAINT SVC_ITEM_SUPPLIER_TL_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_ITEM_SUPPLIER_TL'
ALTER TABLE SVC_ITEM_SUPPLIER_TL
 ADD CONSTRAINT SVC_ITEM_SUPPLIER_TL_UK UNIQUE
  (LANG,
   ITEM,
   SUPPLIER
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

