--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------------
--	Tables Dropped: 		 COST_SUSP_SUP_ITEM_CFA_EXT,COST_SUSP_SUP_LOC_CFA_EXT
----------------------------------------------------------------------------------

whenever sqlerror exit failure
--------------------------------------
--       Dropping Tables               
--------------------------------------
PROMPT Dropping Table 'COST_SUSP_SUP_ITEM_CFA_EXT'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'COST_SUSP_SUP_ITEM_CFA_EXT'
     AND OBJECT_TYPE = 'TABLE';

  if (L_table_exists != 0) then
      execute immediate 'DROP table COST_SUSP_SUP_ITEM_CFA_EXT';
  end if;
end;
/


PROMPT Dropping Table 'COST_SUSP_SUP_LOC_CFA_EXT'
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'COST_SUSP_SUP_LOC_CFA_EXT'
     AND OBJECT_TYPE = 'TABLE';

  if (L_table_exists != 0) then
      execute immediate 'DROP table COST_SUSP_SUP_LOC_CFA_EXT';
  end if;
end;
/
