--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  Modify View:         V_IINDUCT_SEARCH_RMS
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       Modify V_IINDUCT_SEARCH_RMS               
--------------------------------------
PROMPT Creating View 'V_IINDUCT_SEARCH_RMS'
CREATE OR REPLACE FORCE VIEW "V_IINDUCT_SEARCH_RMS"
("ITEM",
 "ITEM_DESC",
 "ITEM_TYPE",
 "SUPPLIER",
 "DIVISION",
 "GROUP_NO",
 "DEPT", 
 "CLASS",
 "SUBCLASS", 
 "STATUS", 
 "ITEM_LEVEL",
 "TRAN_LEVEL",
 "SELLABLE_IND", 
 "ORDERABLE_IND", 
 "INVENTORY_IND",  
 "BRAND_NAME",
 "DIV_NAME" ,
 "GROUP_NAME",
 "DEPT_NAME", 
 "CLASS_NAME", 
 "SUBCLASS_NAME", 
 "SUPPLIER_NAME", 
 "VPN") 
 AS 
 with t as ( select iem.item item
                    ,iem.item_desc item_desc
                    ,CASE
                       WHEN iem.simple_pack_ind = 'Y' THEN 
                            'S'
                       WHEN iem.pack_ind = 'Y' AND iem.simple_pack_ind = 'N' THEN
                            'C'
                       WHEN iem.deposit_item_type is not null THEN
                            iem.deposit_item_type
                       WHEN iem.item_xform_ind = 'Y' AND iem.orderable_ind = 'Y' THEN 
                            'O'
                       WHEN iem.item_xform_ind = 'Y' AND iem.orderable_ind = 'N' THEN 
                            'L'
                       WHEN dep.purchase_type IN (1, 2) THEN 
                            'I'
                       ELSE 'R'
                     END item_type
					,iem.division division
                    ,iem.group_no group_no					
                    ,dep.dept dept
                    ,iem.class class
                    ,iem.subclass subclass
                    ,iem.status status
                    ,iem.item_level item_level
                    ,iem.tran_level tran_level
                    ,iem.sellable_ind sellable_ind
                    ,iem.orderable_ind orderable_ind
                    ,iem.inventory_ind inventory_ind
                    ,iem.brand_name  brand_name
                    ,divs.div_name div_name		
                    ,grpt.group_name group_name					
                    ,dtl.dept_name   dept_name
                    ,cls.class_name  class_name
                    ,scls.sub_name   subclass_name
               FROM deps dep,
                    groups grp,
                    v_deps_tl dtl,
                    v_class_tl cls,
                    v_subclass_tl scls,
                    v_item_master iem,
                    v_groups_tl grpt,
                    v_division_tl divs
              WHERE iem.division = divs.division
                AND iem.group_no = grp.group_no
                AND iem.dept = dep.dept
                AND grp.division = divs.division
                AND grp.group_no = grpt.group_no                
                AND grpt.group_no = dep.group_no
                AND dep.dept = dtl.dept
                AND iem.class = cls.class
                AND iem.subclass = scls.subclass
                AND dtl.dept      = cls.dept   
                AND cls.dept      = scls.dept     
                AND cls.class     = scls.class)
SELECT t.item item
      ,t.item_desc item_desc
      ,t.item_type
      ,ies.supplier
      ,t.division division
	  ,t.group_no group_no
      ,t.dept dept
      ,t.class class
      ,t.subclass subclass
      ,t.status status
      ,t.item_level item_level
      ,t.tran_level tran_level
      ,t.sellable_ind sellable_ind
      ,t.orderable_ind orderable_ind
      ,t.inventory_ind inventory_ind
      ,t.brand_name  brand_name
	  ,t.div_name div_name
	  ,t.group_name group_name
      ,t.dept_name   dept_name
      ,t.class_name  class_name
      ,t.subclass_name subclass_name
      ,(select sup_name from v_sups s where s.supplier = ies.supplier) supplier_name
      ,ies.vpn         vpn
 FROM t,
      (select item,supplier,vpn,primary_supp_ind, rank() over(partition by item order by primary_supp_ind desc) rank from item_supplier) ies
WHERE t.item = ies.item (+)
  AND ies.rank(+) = 1
/
  
COMMENT ON TABLE V_IINDUCT_SEARCH_RMS IS 'This view is used for the item induct screen in RMS Alloy. It is contains all fields that can be used as item search criteria along with their descriptions to be displayed in the search result when Item Download from RMS option is selected.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.ITEM IS 'Item Id.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.ITEM_DESC IS 'Item description.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.ITEM_TYPE IS 'Derived attribute of item type based on item attributes to indicate if the item is a regular item, pack, simple pack, transform orderable, transform sellable, a consignment or concession item, or a type of deposit item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.SUPPLIER IS 'The supplier of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.DIVISION IS 'The merchandise Division of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.GROUP_NO IS 'The merchandise Group of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.DEPT IS 'The merchandise department of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.CLASS IS 'The merchandise class of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.SUBCLASS IS 'The merchandise subclass of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.STATUS IS 'Item status. If an item or its parent or grandparent is on daily purge, the item is in D (deleted) status.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.ITEM_LEVEL IS 'Item level.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.TRAN_LEVEL IS 'Transaction level.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.SELLABLE_IND IS 'Item sellable indicator.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.ORDERABLE_IND IS 'Item orderable indicator.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.INVENTORY_IND IS 'Item inventory indicator.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.BRAND_NAME IS 'Brand associated to the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.DIV_NAME IS 'The merchandise Division name of the item.';
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.GROUP_NAME IS 'The merchandise Group name of the item.'; 
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.DEPT_NAME IS 'The merchandise department name of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.CLASS_NAME IS 'The merchandise class name of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.SUBCLASS_NAME IS 'The merchandise subclass name of the item.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.SUPPLIER_NAME IS 'The name of the supplier.' ;
COMMENT ON COLUMN V_IINDUCT_SEARCH_RMS.VPN IS 'The Vendor Product Number associated with the item.' ; 
