--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_FREIGHT_SIZE
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_FREIGHT_SIZE"
CREATE TABLE DC_FREIGHT_SIZE
(
   FREIGHT_SIZE         VARCHAR2(6),
   FREIGHT_SIZE_DESC    VARCHAR2(250)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_FREIGHT_SIZE is 'This table is a staging table for data conversion and will hold data for FREIGHT_SIZE which is attached to transportation records.'
/
COMMENT ON COLUMN DC_FREIGHT_SIZE.FREIGHT_SIZE is 'Contains the unique key that identifies the freight size record.'
/
COMMENT ON COLUMN DC_FREIGHT_SIZE.FREIGHT_SIZE_DESC is 'Contains the description of the freight size.'
/