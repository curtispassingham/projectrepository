--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_DIFF_ID_GROUP_TYPE
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_DIFF_ID_GROUP_TYPE'
CREATE OR REPLACE FORCE VIEW V_DIFF_ID_GROUP_TYPE (ID_GROUP, DESCRIPTION, DIFF_TYPE, ID_GROUP_IND) AS
SELECT DID.DIFF_ID ID_GROUP, 
       V.DIFF_DESC DESCRIPTION, 
       DID.DIFF_TYPE DIFF_TYPE, 
       'ID' ID_GROUP_IND
FROM DIFF_IDS DID,
     V_DIFF_IDS_TL V
WHERE DID.DIFF_ID = V.DIFF_ID
UNION ALL
SELECT DGH.DIFF_GROUP_ID ID_GROUP, 
       V.DIFF_GROUP_DESC DESCRIPTION, 
       DGH.DIFF_TYPE DIFF_TYPE, 
       'GROUP' ID_GROUP_IND
FROM DIFF_GROUP_HEAD DGH,
     V_DIFF_GROUP_HEAD_TL V
WHERE DGH.DIFF_GROUP_ID = V.DIFF_GROUP_ID
  

/
COMMENT ON TABLE V_DIFF_ID_GROUP_TYPE IS 'This view extracts the ids and description for all differential types.'
/
