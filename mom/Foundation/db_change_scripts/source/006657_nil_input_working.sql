--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'NIL_INPUT_WORKING'

SET SERVEROUTPUT ON
DECLARE
	l_exists 		NUMBER(1);
	l_partitioned 	NUMBER(1);
	l_local_string	VARCHAR2(10) := '';
BEGIN
	for i in 
		(SELECT INDEX_NAME FROM USER_INDEXES WHERE TABLE_NAME = 'NIL_INPUT_WORKING') loop
			execute immediate 'DROP INDEX '|| i.INDEX_NAME ;
			dbms_output.put_line('Index Dropped ' || i.INDEX_NAME);
	end loop;
	
	SELECT COUNT(*) INTO l_partitioned 
		FROM USER_PART_TABLES 
		WHERE TABLE_NAME = 'NIL_INPUT_WORKING' ;
		
	IF l_partitioned > 0 THEN
		l_local_string := 'LOCAL';
	END IF;

	SELECT COUNT(*) INTO l_exists 
		FROM USER_INDEXES 
		WHERE TABLE_NAME = 'NIL_INPUT_WORKING' 
		AND INDEX_NAME = 'NIL_INPUT_WORKING_I2';
	IF l_exists = 0 THEN
		execute immediate 'CREATE INDEX NIL_INPUT_WORKING_I2 on NIL_INPUT_WORKING(ITEM,PROCESS_ID, LOC) ' || l_local_string;
		dbms_output.put_line('Index Created NIL_INPUT_WORKING_I2');
	END IF;
		
	SELECT COUNT(*) INTO l_exists 
		FROM USER_INDEXES 
		WHERE TABLE_NAME = 'NIL_INPUT_WORKING' 
		AND INDEX_NAME = 'NIL_INPUT_WORKING_I1';
	IF l_exists = 0 THEN
		execute immediate 'CREATE INDEX NIL_INPUT_WORKING_I1 on NIL_INPUT_WORKING(PROCESS_ID, LOC, LOC_TYPE, DEPT, CLASS, SUBCLASS) ' || l_local_string;
		dbms_output.put_line('Index Created NIL_INPUT_WORKING_I1');
	END IF;

END;
/
