--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_RELATED_ITEM_HEAD
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_RELATED_ITEM_HEAD"
CREATE TABLE DC_RELATED_ITEM_HEAD
(
   RELATIONSHIP_ID        NUMBER(20),
   ITEM                   VARCHAR2(25),
   RELATIONSHIP_NAME      VARCHAR2(255),
   RELATIONSHIP_TYPE      VARCHAR2(6),
   MANDATORY_IND          VARCHAR2(1)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_RELATED_ITEM_HEAD is 'This table is a staging table for data conversion and will contain one row for each item and relationship type (Cross Sell, Sub Sell, Substitute etc).'
/
COMMENT ON COLUMN DC_RELATED_ITEM_HEAD.ITEM is 'Unique alphanumeric value that identifies the item.'
/
COMMENT ON COLUMN DC_RELATED_ITEM_HEAD.RELATIONSHIP_ID is 'Unique identifier for each relationship header.'
/
COMMENT ON COLUMN DC_RELATED_ITEM_HEAD.RELATIONSHIP_NAME is 'Name given to the relationship.'
/
COMMENT ON COLUMN DC_RELATED_ITEM_HEAD.RELATIONSHIP_TYPE is 'Describes the type of relationship. Values are configured in code_detail table under code_type IREL.'
/
COMMENT ON COLUMN DC_RELATED_ITEM_HEAD.MANDATORY_IND is 'Indicates whether the relationship is mandatory.'
/