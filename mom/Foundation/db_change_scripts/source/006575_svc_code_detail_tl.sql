--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_CODE_DETAIL_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_CODE_DETAIL_TL'
CREATE TABLE SVC_CODE_DETAIL_TL
 (PROCESS_ID NUMBER(10) NOT NULL,
  CHUNK_ID NUMBER(10) NOT NULL,
  ROW_SEQ NUMBER(20) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ),
  LANG NUMBER(6),
  CODE_TYPE VARCHAR2(4 ),
  CODE VARCHAR2(6 ),
  CODE_DESC VARCHAR2(250 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_CODE_DETAIL_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded to CODE_DETAIL_TL'
/

COMMENT ON COLUMN SVC_CODE_DETAIL_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_CODE_DETAIL_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_CODE_DETAIL_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_CODE_DETAIL_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_CODE_DETAIL_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_CODE_DETAIL_TL.LANG is 'This field contains the language in which the translated text is maintained.'
/

COMMENT ON COLUMN SVC_CODE_DETAIL_TL.CODE_TYPE is 'This field will contain a valid code type for the row.  The valid code types are defined on the CODE_HEAD table.'
/

COMMENT ON COLUMN SVC_CODE_DETAIL_TL.CODE is 'This field contains the code used in Oracle Retail which must be decoded for display in the on-line forms.'
/

COMMENT ON COLUMN SVC_CODE_DETAIL_TL.CODE_DESC is 'This field contains the description associated with the code and code type.'
/


PROMPT Creating Primary Key on 'SVC_CODE_DETAIL_TL'
ALTER TABLE SVC_CODE_DETAIL_TL
 ADD CONSTRAINT SVC_CODE_DTL_TL_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_CODE_DETAIL_TL'
ALTER TABLE SVC_CODE_DETAIL_TL
 ADD CONSTRAINT SVC_CODE_DTL_TL_UK UNIQUE
  (LANG,
   CODE_TYPE,
   CODE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

