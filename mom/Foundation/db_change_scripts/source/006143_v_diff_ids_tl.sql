CREATE OR REPLACE FORCE VIEW V_DIFF_IDS_TL (DIFF_ID, DIFF_DESC, LANG ) AS
SELECT  b.diff_id,
        case when tl.lang is not null then tl.diff_desc else b.diff_desc end diff_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  DIFF_IDS b,
        DIFF_IDS_TL tl
 WHERE  b.diff_id = tl.diff_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_DIFF_IDS_TL is 'This is the translation view for base table DIFF_IDS. This view fetches data in user langauge either from translation table DIFF_IDS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_DIFF_IDS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_DIFF_IDS_TL.DIFF_ID is 'Contains the code to uniquely identify a differentiator.'
/

COMMENT ON COLUMN V_DIFF_IDS_TL.DIFF_DESC is 'Description of the differentiator (for example, Blueberry, Shower Fresh, Red, etc.)'
/

