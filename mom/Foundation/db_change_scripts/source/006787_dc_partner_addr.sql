--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  DC_PARTNER_ADDR
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       Creating Table               
--------------------------------------

prompt Creating table "DC_PARTNER_ADDR"
CREATE TABLE DC_PARTNER_ADDR
(
   KEY_VALUE_1                VARCHAR2(20),
   KEY_VALUE_2                VARCHAR2(20),
   ADDR_TYPE                  VARCHAR2(2),
   PRIMARY_ADDR_IND           VARCHAR2(1),
   CONTACT_NAME               VARCHAR2(120),
   CONTACT_PHONE              VARCHAR2(20),
   CONTACT_FAX                VARCHAR2(20),
   CONTACT_EMAIL              VARCHAR2(100),
   CONTACT_TELEX              VARCHAR2(20),
   ADD_1                      VARCHAR2(240),
   ADD_2                      VARCHAR2(240),
   ADD_3                      VARCHAR2(240),
   CITY                       VARCHAR2(120),
   COUNTY                     VARCHAR2(250),
   STATE                      VARCHAR2(3),
   POST                       VARCHAR2(30),
   COUNTRY_ID                 VARCHAR2(3),
   JURISDICTION_CODE          VARCHAR2(10)
)
  INITRANS 6
 TABLESPACE RETAIL_DATA
 /
COMMENT ON TABLE DC_PARTNER_ADDR is 'This table is a staging table for data conversion and will hold the partner address information.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.KEY_VALUE_1 is 'This column contains specific ID or type that the address is attached to.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.KEY_VALUE_2 is 'If the module is Partner (PTNR), then this field will contain the partners ID, else this field will be null.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.ADDR_TYPE is 'This column indicates the type for the address.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.PRIMARY_ADDR_IND is 'This column indicates whether the address is the primary address for the address type.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.CONTACT_NAME is 'Contains the name of the partners representative contract.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.CONTACT_PHONE is 'Contains the phone number of the partners representative contact.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.CONTACT_FAX is 'Contains the fax number of the partners representative contact.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.CONTACT_EMAIL is 'Contains the e-mail address of the partners representative contact.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.CONTACT_TELEX is 'Contains the telex number of the partners representative contact.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.ADD_1 is 'This column contains the first line of the address.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.ADD_2 is 'This column contains the second line of the address.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.ADD_3 is 'This column contains the third line of the address.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.CITY is 'This column contains the name of the city that is associated with the address.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.COUNTY is 'This column holds the county name for the location.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.STATE is 'This column contains the state abbreviation for the address.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.POST is 'This column contains the zip code for the address.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.COUNTRY_ID is 'This column contains the country where the address exists.'
/
COMMENT ON COLUMN DC_PARTNER_ADDR.JURISDICTION_CODE is 'Identifies the jurisdiction code for the country-state relationship.'
/