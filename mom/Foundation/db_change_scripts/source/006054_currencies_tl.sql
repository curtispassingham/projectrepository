--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       CURRENCIES_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE CURRENCIES_TL(
LANG NUMBER(6) NOT NULL,
CURRENCY_CODE VARCHAR2(3) NOT NULL,
CURRENCY_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE CURRENCIES_TL is 'This is the translation table for CURRENCIES table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN CURRENCIES_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN CURRENCIES_TL.CURRENCY_CODE is 'Contains a number which uniquely identifies the type of currency.'
/

COMMENT ON COLUMN CURRENCIES_TL.CURRENCY_DESC is 'Contains a description of the currency.'
/

COMMENT ON COLUMN CURRENCIES_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN CURRENCIES_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN CURRENCIES_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN CURRENCIES_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE CURRENCIES_TL ADD CONSTRAINT PK_CURRENCIES_TL PRIMARY KEY (
LANG,
CURRENCY_CODE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE CURRENCIES_TL
 ADD CONSTRAINT CURRT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE CURRENCIES_TL ADD CONSTRAINT CURRT_CURR_FK FOREIGN KEY (
CURRENCY_CODE
) REFERENCES CURRENCIES (
CURRENCY_CODE
)
/

