--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE ADDED:				COMPHEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TYPE
--------------------------------------
PROMPT Creating Table 'COMPHEAD_TL'
CREATE TABLE COMPHEAD_TL(
LANG NUMBER(6) NOT NULL,
COMPANY NUMBER(4) NOT NULL,
CO_NAME VARCHAR2(120) NOT NULL,
CO_ADD1 VARCHAR2(240) NOT NULL,
CO_ADD2 VARCHAR2(240) ,
CO_ADD3 VARCHAR2(240) ,
CO_CITY VARCHAR2(120) NOT NULL,
CO_NAME_SECONDARY VARCHAR2(120) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE COMPHEAD_TL is 'This is the translation table for COMPHEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN COMPHEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN COMPHEAD_TL.COMPANY is 'Contains the unique number identifying the company the for which the system is running.  This is the highest level in the merchandise hierarchy.'
/

COMMENT ON COLUMN COMPHEAD_TL.CO_NAME is 'Contains the company name for which the system is running.'
/

COMMENT ON COLUMN COMPHEAD_TL.CO_ADD1 is 'The address of the company headquarters.'
/

COMMENT ON COLUMN COMPHEAD_TL.CO_ADD2 is 'The second line of the company headquarters address.'
/

COMMENT ON COLUMN COMPHEAD_TL.CO_ADD3 is 'The third line of the company headquarters address.'
/

COMMENT ON COLUMN COMPHEAD_TL.CO_CITY is 'The city of the company headquarters.'
/

COMMENT ON COLUMN COMPHEAD_TL.CO_NAME_SECONDARY is 'Contains the secondary name of the company.'
/

COMMENT ON COLUMN COMPHEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN COMPHEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN COMPHEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN COMPHEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE COMPHEAD_TL ADD CONSTRAINT PK_COMPHEAD_TL PRIMARY KEY (
LANG,
COMPANY
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE COMPHEAD_TL
 ADD CONSTRAINT COHT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE COMPHEAD_TL ADD CONSTRAINT COHT_COH_FK FOREIGN KEY (
COMPANY
) REFERENCES COMPHEAD (
COMPANY
)
/

