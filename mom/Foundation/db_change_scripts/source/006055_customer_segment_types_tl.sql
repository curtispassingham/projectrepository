--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       CUSTOMER_SEGMENT_TYPES_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE CUSTOMER_SEGMENT_TYPES_TL(
LANG NUMBER(6) NOT NULL,
CUSTOMER_SEGMENT_TYPE VARCHAR2(6) NOT NULL,
CUSTOMER_SEGMENT_TYPE_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE CUSTOMER_SEGMENT_TYPES_TL is 'This is the translation table for CUSTOMER_SEGMENT_TYPES table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN CUSTOMER_SEGMENT_TYPES_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN CUSTOMER_SEGMENT_TYPES_TL.CUSTOMER_SEGMENT_TYPE is 'The customer segment type of a given customer segment, referenced and attached to promotions in RPM. They will be available in RPM promotion UI screen for customer segment promotions. For example: Electrician.'
/

COMMENT ON COLUMN CUSTOMER_SEGMENT_TYPES_TL.CUSTOMER_SEGMENT_TYPE_DESC is 'The customer segment type description.'
/

COMMENT ON COLUMN CUSTOMER_SEGMENT_TYPES_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN CUSTOMER_SEGMENT_TYPES_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN CUSTOMER_SEGMENT_TYPES_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN CUSTOMER_SEGMENT_TYPES_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE CUSTOMER_SEGMENT_TYPES_TL ADD CONSTRAINT PK_CUSTOMER_SEGMENT_TYPES_TL PRIMARY KEY (
LANG,
CUSTOMER_SEGMENT_TYPE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE CUSTOMER_SEGMENT_TYPES_TL
 ADD CONSTRAINT CSTT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE CUSTOMER_SEGMENT_TYPES_TL ADD CONSTRAINT CSTT_CST_FK FOREIGN KEY (
CUSTOMER_SEGMENT_TYPE
) REFERENCES CUSTOMER_SEGMENT_TYPES (
CUSTOMER_SEGMENT_TYPE
)
/

