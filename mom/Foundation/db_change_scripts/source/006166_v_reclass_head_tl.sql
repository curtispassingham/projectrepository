CREATE OR REPLACE FORCE VIEW V_RECLASS_HEAD_TL (RECLASS_NO, RECLASS_DESC, LANG ) AS
SELECT  b.reclass_no,
        case when tl.lang is not null then tl.reclass_desc else b.reclass_desc end reclass_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  RECLASS_HEAD b,
        RECLASS_HEAD_TL tl
 WHERE  b.reclass_no = tl.reclass_no (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_RECLASS_HEAD_TL is 'This is the translation view for base table RECLASS_HEAD. This view fetches data in user langauge either from translation table RECLASS_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_RECLASS_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_RECLASS_HEAD_TL.RECLASS_NO is 'This field contains a unique number representing the reclassification number.'
/

COMMENT ON COLUMN V_RECLASS_HEAD_TL.RECLASS_DESC is 'This field contains a description of why the SKUs in the RECLASS_SKU table are being reclassified.'
/

