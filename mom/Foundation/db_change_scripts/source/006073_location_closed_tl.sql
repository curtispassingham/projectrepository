--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       LOCATION_CLOSED_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE LOCATION_CLOSED_TL(
LANG NUMBER(6) NOT NULL,
LOCATION NUMBER(10) NOT NULL,
CLOSE_DATE DATE NOT NULL,
REASON VARCHAR2(250) ,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE LOCATION_CLOSED_TL is 'This is the translation table for LOCATION_CLOSED table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN LOCATION_CLOSED_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN LOCATION_CLOSED_TL.LOCATION is 'Contain the location number'
/

COMMENT ON COLUMN LOCATION_CLOSED_TL.CLOSE_DATE is 'This field contains the date of the closing.'
/

COMMENT ON COLUMN LOCATION_CLOSED_TL.REASON is 'Contains the reason why the store is closed.'
/

COMMENT ON COLUMN LOCATION_CLOSED_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN LOCATION_CLOSED_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN LOCATION_CLOSED_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN LOCATION_CLOSED_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE LOCATION_CLOSED_TL ADD CONSTRAINT PK_LOCATION_CLOSED_TL PRIMARY KEY (
LANG,
LOCATION,
CLOSE_DATE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE LOCATION_CLOSED_TL
 ADD CONSTRAINT LCDT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE LOCATION_CLOSED_TL ADD CONSTRAINT LCDT_LCD_FK FOREIGN KEY (
LOCATION,
CLOSE_DATE
) REFERENCES LOCATION_CLOSED (
LOCATION,
CLOSE_DATE
)
/

