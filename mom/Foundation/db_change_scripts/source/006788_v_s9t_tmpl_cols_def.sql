--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
--The customer DBA is responsible to review this script to ensure
--data is preserved as desired.
--
----------------------------------------------------------------------------
--Table Added:  V_S9T_TMPL_COLS_DEF
----------------------------------------------------------------------------


whenever sqlerror exit  

--------------------------------------
--       UPDATING VIEW               
--------------------------------------
PROMPT CREATING VIEW 'V_S9T_TMPL_COLS_DEF';
CREATE OR REPLACE FORCE  VIEW V_S9T_TMPL_COLS_DEF ("TEMPLATE_KEY", "WKSHT_KEY", "COLUMN_KEY", "COLUMN_NAME", "MANDATORY") AS 
  SELECT cd.template_key template_key,
    cd.wksht_key wksht_key,
    cd.column_key column_key,
    NVL(cds.column_name,cd.column_name) AS column_name,
    cd.mandatory mandatory
  FROM s9t_tmpl_cols_def cd,
    s9t_tmpl_cols_def_tl cds
  WHERE cd.template_key = cds.template_key(+)
  AND cd.wksht_key      = cds.wksht_key(+)
  AND cd.column_key     = cds.column_key(+)
  AND cds.lang (+)      = get_user_lang ;
/
