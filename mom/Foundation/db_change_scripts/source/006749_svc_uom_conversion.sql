--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_UOM_CONVERSION
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_UOM_CONVERSION' 
 CREATE TABLE SVC_UOM_CONVERSION
   (	
    PROCESS_ID        NUMBER(10,0) NOT NULL,
    CHUNK_ID          NUMBER(10,0) DEFAULT 1 NOT NULL,
    ROW_SEQ           NUMBER(20,0) NOT NULL,
    ACTION            VARCHAR2(10),
    PROCESS$STATUS            VARCHAR2(10) DEFAULT 'N' ,
    OPERATOR VARCHAR2(1) ,
    FACTOR NUMBER(20,10) ,
    TO_UOM VARCHAR2(4) ,
    FROM_UOM VARCHAR2(4) ,
    CREATE_ID         VARCHAR2(30) DEFAULT USER ,
    CREATE_DATETIME   DATE DEFAULT SYSDATE ,
    LAST_UPD_ID         VARCHAR2(30) DEFAULT USER ,
    LAST_UPD_DATETIME   DATE DEFAULT SYSDATE 
	)	
 INITRANS 6
 TABLESPACE RETAIL_DATA
/


COMMENT ON TABLE SVC_UOM_CONVERSION IS 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in UOM_CONVERSION.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.PROCESS_ID IS 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.CHUNK_ID IS 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.ROW_SEQ IS 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.ACTION IS 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.PROCESS$STATUS IS 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.TO_UOM IS 'Contains a string that uniquely identifies the unit of measure to which the conversion is to occur.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.FROM_UOM IS 'Contains a string that uniquely identifies the unit of measure from which the conversion is to occur.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.FACTOR IS 'Contains a number that is the conversion factor from the from_UOM to the to_UOM.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.OPERATOR IS 'Contains a character that identifies the operation required to convert from the from_UOM to the to_UOM. Valid Values are: M = Multiply and D= Divide.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.CREATE_ID IS 'User who created the record.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.CREATE_DATETIME IS 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.LAST_UPD_ID IS 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_UOM_CONVERSION.LAST_UPD_DATETIME IS 'Date time when record was last updated.'
/
 
ALTER TABLE SVC_UOM_CONVERSION
ADD CONSTRAINT SVC_UOM_CONVERSION_PK PRIMARY KEY ( PROCESS_ID,ROW_SEQ )
  USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/   

ALTER TABLE SVC_UOM_CONVERSION
ADD CONSTRAINT SVC_UOM_CONVERSION_UK UNIQUE ( TO_UOM,FROM_UOM )
  USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/   
