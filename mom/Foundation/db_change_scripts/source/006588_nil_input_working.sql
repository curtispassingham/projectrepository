--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'NIL_INPUT_WORKING'
COMMENT ON COLUMN NIL_INPUT_WORKING.PRIMARY_VARIANT is 'This field is used to address sales of PLUs (i.e. above transaction level items) when inventory is tracked at a lower level (i.e. UPC).  This field will only contain a value for items one level higher than the transaction level.  Valid choices will be any transaction level item that is a child of this item.  In order to select a transaction level item as the primary variant, the item/location relationship must exist at the transaction level.  Both the transaction level item (i.e. UPC) and the higher than transcation level item (i.e. PLU) will be sent to the POS to allow the store to sell the PLU.  The information sent for the PLU will be the same information sent for the transaction level item (i.e. UPC).'
/

