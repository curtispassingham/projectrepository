CREATE OR REPLACE FORCE VIEW V_AREA_TL (AREA, AREA_NAME, LANG ) AS
SELECT  b.area,
        case when tl.lang is not null then tl.area_name else b.area_name end area_name,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  AREA b,
        AREA_TL tl
 WHERE  b.area = tl.area (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_AREA_TL is 'This is the translation view for base table AREA. This view fetches data in user langauge either from translation table AREA_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_AREA_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_AREA_TL.AREA is 'This column contains the number which uniquely identifies an area within the system.'
/

COMMENT ON COLUMN V_AREA_TL.AREA_NAME is 'This column contains the name of the area which, along with the area number, identifies the area.'
/

