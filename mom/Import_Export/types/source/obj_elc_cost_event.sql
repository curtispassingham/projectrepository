drop type obj_elc_cost_event_tbl FORCE
/

drop type obj_elc_cost_event_rec FORCE
/

create or replace TYPE obj_elc_cost_event_rec AS OBJECT (
ITEM                    VARCHAR2(25),
SUPPLIER                NUMBER(10),
ORIGIN_COUNTRY_ID       VARCHAR2(3),
COST_ZONE_GROUP         NUMBER(4),
COST_ZONE               NUMBER(10)
)
/

create or replace TYPE obj_elc_cost_event_tbl AS TABLE OF obj_elc_cost_event_rec
/
