--WRP_TRANSPORTATION_REC
--------------------------------------
--       Dropping TYPE
--------------------------------------
PROMPT Dropping TYPE 'WRP_TRANSPORTATION_REC'

DROP TYPE WRP_TRANSPORTATION_REC FORCE
/
--------------------------------------
--       Creating TYPE
--------------------------------------
PROMPT Creating TYPE 'WRP_TRANSPORTATION_REC'
CREATE OR REPLACE TYPE WRP_TRANSPORTATION_REC AS OBJECT (
TRANSPORTATION_ID          NUMBER(10),
VESSEL_ID                  VARCHAR2(20),
VOYAGE_FLT_ID              VARCHAR2(10),
ESTIMATED_DEPART_DATE      DATE,
ACTUAL_DEPART_DATE         DATE,
ORDER_NO                   NUMBER(12),
ITEM                       VARCHAR2(25),
BL_AWB_ID                  VARCHAR2(30),
CONTAINER_ID               VARCHAR2(20),
INVOICE_ID                 VARCHAR2(30),
INVOICE_DATE               DATE,
SHIPMENT_NO                VARCHAR2(20),
ACTUAL_ARRIVAL_DATE        DATE,
DELIVERY_DATE              DATE,
STATUS                     VARCHAR2(6),
TRAN_MODE_ID               VARCHAR2(6),
VESSEL_SCAC_CODE           VARCHAR2(6),
CONTAINER_SCAC_CODE        VARCHAR2(6),
SEAL_ID                    VARCHAR2(15),
FREIGHT_TYPE               VARCHAR2(6),
FREIGHT_SIZE               VARCHAR2(6),
ORIGIN_COUNTRY_ID          VARCHAR2(3),
CONSOLIDATION_COUNTRY_ID   VARCHAR2(3),
EXPORT_COUNTRY_ID          VARCHAR2(3),
ESTIMATED_ARRIVAL_DATE     DATE,
LADING_PORT                VARCHAR2(5),
DISCHARGE_PORT             VARCHAR2(5),
RECEIPT_ID                 VARCHAR2(30),
FCR_ID                     VARCHAR2(15),
FCR_DATE                   DATE,
SERVICE_CONTRACT_NO        VARCHAR2(15),
IN_TRANSIT_NO              VARCHAR2(15),
IN_TRANSIT_DATE            DATE,
LOT_NO                     VARCHAR2(15),
INVOICE_AMT                NUMBER(20,4),
CURRENCY_CODE              VARCHAR2(3),
EXCHANGE_RATE              NUMBER(20,10),
CARTON_QTY                 NUMBER(12,4),
CARTON_UOM                 VARCHAR2(4),
ITEM_QTY                   NUMBER(12,4),
ITEM_QTY_UOM               VARCHAR2(4),
GROSS_WT                   NUMBER(12,4),
GROSS_WT_UOM               VARCHAR2(4),
NET_WT                     NUMBER(12,4),
NET_WT_UOM                 VARCHAR2(4),
CUBIC                      NUMBER(12,4),
CUBIC_UOM                  VARCHAR2(4),
PACKING_METHOD             VARCHAR2(6),
RUSH_IND                   VARCHAR2(1),
CANDIDATE_IND              VARCHAR2(1),
COMMENTS                   VARCHAR2(2000),
SELECTED_IND               VARCHAR2(1),
TRANS_PARTNER_TYPE         VARCHAR2(6),
TRANS_PARTNER_ID           VARCHAR2(10))
/