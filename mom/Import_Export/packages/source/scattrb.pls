
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SUBCLASS_ATTRIB_SQL AS
  
--------------------------------------------------------------------
   FUNCTION GET_NAME(	O_error_message IN OUT 	VARCHAR2,
			I_dept		IN	NUMBER,
			I_class		IN	NUMBER,
                        I_subclass      IN     	NUMBER,
                        O_sub_name 	IN OUT 	VARCHAR2)
   RETURN BOOLEAN IS

      L_program	VARCHAR2(64)	:= 'SUBCLASS_ATTRIB_SQL.GET_NAME';

      cursor C_SUBCLASS IS
             select sub_name
             from   v_subclass_tl
             where  subclass = I_subclass
	     and    class = I_class
             and    dept = I_dept;

   BEGIN
      open C_SUBCLASS;
      fetch C_SUBCLASS into O_sub_name;
      if C_SUBCLASS%NOTFOUND then
         close C_SUBCLASS;
         O_error_message := sql_lib.create_msg('INV_SUBCLASS',
						null,null,null);
         RETURN FALSE;
      else
         close C_SUBCLASS;
         RETURN TRUE;
      end if;
   EXCEPTION
      when OTHERS then
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
						SQLERRM,
						L_program,
						null);
         RETURN FALSE;
   END GET_NAME;
---
END SUBCLASS_ATTRIB_SQL;
/


