
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY OGA_SQL AS
-------------------------------------------------------------------------
FUNCTION GET_INFO (O_error_message IN OUT VARCHAR2,
                   O_exists        IN OUT	BOOLEAN,
                   O_oga_desc	     IN OUT OGA.OGA_DESC%TYPE,
                   O_req_form      IN OUT OGA.REQ_FORM%TYPE,
                   I_oga_code      IN     OGA.OGA_CODE%TYPE)
RETURN BOOLEAN IS
   
   L_program       VARCHAR2(64)    :='OGA_SQL.GET_INFO';
   
   cursor C_GET_INFO is
      select vog.oga_desc,
             og.req_form
        from v_oga_tl vog, 
             oga og
       where vog.oga_code = og.oga_code
         and og.oga_code = I_oga_code;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_INFO', 'OGA', 
                    'oga code: ' || I_oga_code);
   open C_GET_INFO;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_INFO', 'OGA', 
                    'oga code: ' || I_oga_code);
   fetch C_GET_INFO into O_oga_desc,
                         O_req_form;

   if C_GET_INFO%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_INFO', 'OGA', 
                       'oga code: ' || I_oga_code);      
      close C_GET_INFO;
      O_error_message := SQL_LIB.CREATE_MSG('OGA_CODE_NOTFOUND',
                                            NULL,NULL,NULL);

      O_exists := FALSE;
      return TRUE;
   end if;   

   O_exists := TRUE;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_INFO', 'OGA', 
                    'oga code: ' || I_oga_code);
   close C_GET_INFO;
 

   return TRUE;
 
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_INFO;
END;
/















