
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TARIFF_SQL AUTHID CURRENT_USER AS

------------------------------------------------------------------------
-- Function Name : GET_DESC
-- Purpose       : This function will retrieve description fields from  
--                 the tariff_treat_reference table that matches a given 
--                 tariff code.
------------------------------------------------------------------------
   FUNCTION GET_DESC (O_error_message    IN OUT VARCHAR2,
                      O_desc             IN OUT TARIFF_TREATMENT.TARIFF_TREATMENT_DESC%TYPE,
                      I_tariff_treatment IN     TARIFF_TREATMENT.TARIFF_TREATMENT%TYPE)
      RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name : GET_CONDITIONAL_IND 
-- Purpose       : This function returns the conditional indicator
--                 for a tariff treatment.
------------------------------------------------------------------------
   FUNCTION GET_CONDITIONAL_IND (O_error_message      IN OUT VARCHAR2,
                                 O_conditional_ind    IN OUT TARIFF_TREATMENT.CONDITIONAL_IND%TYPE,
                                 I_tariff_treatment   IN TARIFF_TREATMENT.TARIFF_TREATMENT%TYPE)                     
	RETURN BOOLEAN;
------------------------------------------------------------------------
END TARIFF_SQL;
/
 


