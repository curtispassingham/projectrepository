
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY TARIFF_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION GET_DESC (O_error_message    IN OUT  VARCHAR2,
                   O_desc             IN OUT  TARIFF_TREATMENT.TARIFF_TREATMENT_DESC%TYPE,
                   I_tariff_treatment IN      TARIFF_TREATMENT.TARIFF_TREATMENT%TYPE) 
         RETURN BOOLEAN IS

      L_program                     VARCHAR2(64) := 'TARIFF_SQL.GET_DESC';    
            
   cursor C_TARIFF_TREATMENT_DESC is
      select tt.tariff_treatment_desc
        from v_tariff_treatment_tl tt
       where tt.tariff_treatment = I_tariff_treatment;
      
BEGIN

   SQL_LIB.SET_MARK('OPEN','C_TARIFF_TREATMENT_DESC','V_TARIFF_TREATMENT_TL ','Tariff treatment: '||(I_tariff_treatment));
   open C_TARIFF_TREATMENT_DESC;

   SQL_LIB.SET_MARK('FETCH','C_TARIFF_TREATMENT_DESC','V_TARIFF_TREATMENT_TL ','Tariff treatment: '||(I_tariff_treatment));
   fetch C_TARIFF_TREATMENT_DESC into O_desc;

   if C_TARIFF_TREATMENT_DESC%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TARIFF_TREATMENT',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE','C_TARIFF_TREATMENT_DESC','V_TARIFF_TREATMENT_TL ','Tariff treatment: '||(I_tariff_treatment));
      close C_TARIFF_TREATMENT_DESC;
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_TARIFF_TREATMENT_DESC','V_TARIFF_TREATMENT_TL ','Tariff treatment: '||(I_tariff_treatment));
   close C_TARIFF_TREATMENT_DESC;

  

   return TRUE;
 
EXCEPTION   
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                          SQLERRM, 
                                          L_program, 
                                          to_char(SQLCODE));
   return FALSE;

END GET_DESC;
------------------------------------------------------------------------------------------------
FUNCTION GET_CONDITIONAL_IND (O_error_message	   IN OUT VARCHAR2,
                              O_conditional_ind    IN OUT TARIFF_TREATMENT.CONDITIONAL_IND%TYPE,
                              I_tariff_treatment   IN     TARIFF_TREATMENT.TARIFF_TREATMENT%TYPE)
                           
	RETURN BOOLEAN IS


   L_program           VARCHAR2(64) := 'TARIFF_SQL.GET_CONDITIONAL_IND'; 

   cursor C_CHECK_CONDITIONAL is
      select tt.conditional_ind
        from tariff_treatment tt
       where tt.tariff_treatment = I_tariff_treatment;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_CHECK_CONDITIONAL','TARIFF_TREATMENT','Tariff treatment: '||(I_tariff_treatment));
   open C_CHECK_CONDITIONAL;

   SQL_LIB.SET_MARK('FETCH','C_CHECK_CONDITIONAL','TARIFF_TREATMENT','Tariff treatment: '||(I_tariff_treatment));
   fetch C_CHECK_CONDITIONAL into O_conditional_ind;
   
   if C_CHECK_CONDITIONAL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TARIFF_TREATMENT',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_CONDITIONAL','TARIFF_TREATMENT','Tariff treatment: '||(I_tariff_treatment));
      close C_CHECK_CONDITIONAL;
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_CONDITIONAL','TARIFF_TREATMENT','Tariff treatment: '||(I_tariff_treatment));
   close C_CHECK_CONDITIONAL;

   return TRUE;
   
EXCEPTION
   when OTHERS then
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
   return FALSE;
END GET_CONDITIONAL_IND;
---------------------------------------------------------------------------------------------
END TARIFF_SQL;
/


