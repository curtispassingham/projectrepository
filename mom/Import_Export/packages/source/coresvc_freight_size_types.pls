CREATE OR REPLACE PACKAGE CORESVC_FREIGHT_SIZES_TYPES AUTHID CURRENT_USER AS
   template_key           CONSTANT VARCHAR2(255) := 'FREIGHT_SIZE_TYPE_DATA';
   template_category      CONSTANT VARCHAR2(255) := 'RMSIMP';
   
   action_new                      VARCHAR2(25)  := 'NEW';
   action_mod                      VARCHAR2(25)  := 'MOD';
   action_del                      VARCHAR2(25)  := 'DEL';
   
   FREIGHT_SIZE_sheet              VARCHAR2(64)  := 'FREIGHT_SIZE';
   FREIGHT_SIZE$Action             NUMBER        :=1;
   FREIGHT_SIZE$FREIGHT_SIZE       NUMBER        :=2;
   FREIGHT_SIZE$FREIGHT_SIZE_DESC  NUMBER        :=3;
   
   FREIGHT_SIZE_TL_sheet              VARCHAR2(64)  := 'FREIGHT_SIZE_TL';
   FREIGHT_SIZE_TL$Action             NUMBER        :=1;
   FREIGHT_SIZE_TL$LANG               NUMBER        :=2;
   FREIGHT_SIZE_TL$FREIGHT_SIZE       NUMBER        :=3;
   FREIGHT_SIZE_TL$DESC               NUMBER        :=4;
   
   FREIGHT_TYPE_sheet               VARCHAR2(25) := 'FREIGHT_TYPE';
   FREIGHT_TYPE$Action              NUMBER       := 1;
   FREIGHT_TYPE$FREIGHT_TYPE        NUMBER       := 2;
   FREIGHT_TYPE$FREIGHT_TYPE_DESC   NUMBER       := 3;
   
   FREIGHT_TYPE_TL_sheet            VARCHAR2(25) := 'FREIGHT_TYPE_TL';
   FREIGHT_TYPE_TL$Action           NUMBER       := 1;
   FREIGHT_TYPE_TL$LANG             NUMBER       := 2;
   FREIGHT_TYPE_TL$FREIGHT_TYPE     NUMBER       := 3;
   FREIGHT_TYPE_TL$FRGHT_TYP_DESC   NUMBER       := 4;
   
--------------------------------------------------------------------------------
   sheet_name_trans                S9T_PKG.trans_map_typ;
   action_column                   VARCHAR2(255) := 'ACTION';
--------------------------------------------------------------------------------
   FUNCTION CREATE_S9T( O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                        I_template_only_ind IN     CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count   IN OUT NUMBER,
                        I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id    IN     NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------
END CORESVC_FREIGHT_SIZES_TYPES;
/
