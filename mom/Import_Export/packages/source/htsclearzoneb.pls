CREATE OR REPLACE PACKAGE BODY HTS_CLEAR_ZONE_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION TAX_ZONE_EXISTS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists             IN OUT   BOOLEAN,
                         I_hts                IN       HTS_TAX_ZONE.HTS%TYPE,
                         I_import_country_id  IN       HTS_TAX_ZONE.IMPORT_COUNTRY_ID%TYPE,
                         I_effect_from        IN       HTS_TAX_ZONE.EFFECT_FROM%TYPE,
                         I_effect_to          IN       HTS_TAX_ZONE.EFFECT_TO%TYPE,
                         I_tax_type           IN       HTS_TAX_ZONE.TAX_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)  := 'HTS_CLEAR_ZONE_SQL.TAX_ZONE_EXISTS';
   L_rec_exists     VARCHAR2(1)   := NULL;

   cursor C_CHECK_REC_EXISTS is
      select 'x'
        from hts_tax_zone htz
       where htz.hts               = I_hts
         and htz.import_country_id = I_import_country_id
         and htz.effect_from       = I_effect_from
         and htz.effect_to         = I_effect_to
         and htz.tax_type          = I_tax_type
         and rownum                = 1;

BEGIN
   O_exists := TRUE;
   ---
   if I_hts is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hts',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_from is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_from',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_to is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_to',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_tax_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tax_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_REC_EXISTS',
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type);
   open C_CHECK_REC_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_REC_EXISTS',
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type);
   fetch C_CHECK_REC_EXISTS into L_rec_exists;

   if C_CHECK_REC_EXISTS%NOTFOUND then 
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_REC_EXISTS',
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type);
   close C_CHECK_REC_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END TAX_ZONE_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION FEE_ZONE_EXISTS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists            IN OUT   BOOLEAN,
                         I_hts               IN       HTS_FEE_ZONE.HTS%TYPE,
                         I_import_country_id IN       HTS_FEE_ZONE.IMPORT_COUNTRY_ID%TYPE,
                         I_effect_from       IN       HTS_FEE_ZONE.EFFECT_FROM%TYPE,
                         I_effect_to         IN       HTS_FEE_ZONE.EFFECT_TO%TYPE,
                         I_fee_type          IN       HTS_FEE_ZONE.FEE_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)  := 'HTS_CLEAR_ZONE_SQL.FEE_ZONE_EXISTS';
   L_rec_exists     VARCHAR2(1)   := NULL;

   cursor C_CHECK_REC_EXISTS is
      select 'x'
        from hts_fee_zone
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and fee_type          = I_fee_type
         and rownum            = 1;

BEGIN
   O_exists := TRUE;
   ---
   if I_hts is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hts',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_from is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_from',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_to is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_to',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_fee_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fee_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_REC_EXISTS',
                    'HTS_FEE_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' fee_type: '||I_fee_type);
   open C_CHECK_REC_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_REC_EXISTS',
                    'HTS_FEE_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' fee_type: '||I_fee_type);
   fetch C_CHECK_REC_EXISTS into L_rec_exists;

   if C_CHECK_REC_EXISTS%NOTFOUND then 
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_REC_EXISTS',
                    'HTS_FEE_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' fee_type: '||I_fee_type);
   close C_CHECK_REC_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END FEE_ZONE_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION TARIFF_TREATMENT_ZONE_EXISTS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists              IN OUT   BOOLEAN,
                                      I_hts                 IN       HTS_TARIFF_TREATMENT_ZONE.HTS%TYPE,
                                      I_import_country_id   IN       HTS_TARIFF_TREATMENT_ZONE.IMPORT_COUNTRY_ID%TYPE,
                                      I_effect_from         IN       HTS_TARIFF_TREATMENT_ZONE.EFFECT_FROM%TYPE,
                                      I_effect_to           IN       HTS_TARIFF_TREATMENT_ZONE.EFFECT_TO%TYPE,
                                      I_tariff_treatment    IN       HTS_TARIFF_TREATMENT_ZONE.TARIFF_TREATMENT%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)  := 'HTS_CLEAR_ZONE_SQL.TARIFF_TREATMENT_ZONE_EXISTS';
   L_rec_exists     VARCHAR2(1)   := NULL;

   cursor C_CHECK_REC_EXISTS is
      select 'x'
        from hts_tariff_treatment_zone
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and tariff_treatment  = I_tariff_treatment
         and rownum            = 1;

BEGIN
   O_exists := TRUE;
   ---
   if I_hts is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hts',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_from is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_from',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_to is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_to',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_tariff_treatment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tariff_treatment',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_REC_EXISTS',
                    'HTS_TARIFF_TREATMENT_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tariff_treatment: '||I_tariff_treatment);
   open C_CHECK_REC_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_REC_EXISTS',
                    'HTS_TARIFF_TREATMENT_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tariff_treatment: '||I_tariff_treatment);
   fetch C_CHECK_REC_EXISTS into L_rec_exists;

   if C_CHECK_REC_EXISTS%NOTFOUND then 
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_REC_EXISTS',
                    'HTS_TARIFF_TREATMENT_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tariff_treatment: '||I_tariff_treatment);
   close C_CHECK_REC_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END TARIFF_TREATMENT_ZONE_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_TAX_ZONE_DUP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_dup_exists        IN OUT   BOOLEAN,
                            I_hts               IN       HTS_TAX_ZONE.HTS%TYPE,
                            I_import_country_id IN       HTS_TAX_ZONE.IMPORT_COUNTRY_ID%TYPE,
                            I_effect_from       IN       HTS_TAX_ZONE.EFFECT_FROM%TYPE,
                            I_effect_to         IN       HTS_TAX_ZONE.EFFECT_TO%TYPE,
                            I_tax_type          IN       HTS_TAX_ZONE.TAX_TYPE%TYPE,
                            I_clearing_zone_id  IN       HTS_TAX_ZONE.CLEARING_ZONE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)  := 'HTS_CLEAR_ZONE_SQL.CHECK_TAX_ZONE_DUP';
   L_rec_exists     VARCHAR2(1)   := NULL;

   cursor C_CHECK_REC_EXISTS is
      select 'x'
        from hts_tax_zone
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and tax_type          = I_tax_type
         and clearing_zone_id  = I_clearing_zone_id
         and rownum            = 1;

BEGIN
   O_dup_exists := TRUE;
   ---
   if I_hts is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hts',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_from is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_from',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_to is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_to',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_tax_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tax_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_clearing_zone_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_clearing_zone_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_REC_EXISTS',
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type||' clearing_zone_id: '||I_clearing_zone_id);
   open C_CHECK_REC_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_REC_EXISTS',
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type||' clearing_zone_id: '||I_clearing_zone_id);
   fetch C_CHECK_REC_EXISTS into L_rec_exists;

   if C_CHECK_REC_EXISTS%NOTFOUND then 
      O_dup_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_REC_EXISTS',
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type||' clearing_zone_id: '||I_clearing_zone_id);
   close C_CHECK_REC_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_TAX_ZONE_DUP;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_FEE_ZONE_DUP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_dup_exists        IN OUT   BOOLEAN,
                            I_hts               IN       HTS_FEE_ZONE.HTS%TYPE,
                            I_import_country_id IN       HTS_FEE_ZONE.IMPORT_COUNTRY_ID%TYPE,
                            I_effect_from       IN       HTS_FEE_ZONE.EFFECT_FROM%TYPE,
                            I_effect_to         IN       HTS_FEE_ZONE.EFFECT_TO%TYPE,
                            I_fee_type          IN       HTS_FEE_ZONE.FEE_TYPE%TYPE,
                            I_clearing_zone_id  IN       HTS_FEE_ZONE.CLEARING_ZONE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)  := 'HTS_CLEAR_ZONE_SQL.CHECK_FEE_ZONE_DUP';
   L_rec_exists     VARCHAR2(1)   := NULL;

   cursor C_CHECK_REC_EXISTS is
      select 'x'
        from hts_fee_zone
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and fee_type          = I_fee_type
         and clearing_zone_id  = I_clearing_zone_id
         and rownum            = 1;

BEGIN
   O_dup_exists := TRUE;
   ---
   if I_hts is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hts',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_from is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_from',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_to is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_to',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_fee_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fee_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_clearing_zone_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_clearing_zone_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_REC_EXISTS',
                    'HTS_FEE_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' fee_type: '||I_fee_type||' clearing_zone_id: '||I_clearing_zone_id);
   open C_CHECK_REC_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_REC_EXISTS',
                    'HTS_FEE_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' fee_type: '||I_fee_type||' clearing_zone_id: '||I_clearing_zone_id);
   fetch C_CHECK_REC_EXISTS into L_rec_exists;

   if C_CHECK_REC_EXISTS%NOTFOUND then 
      O_dup_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_REC_EXISTS',
                    'HTS_FEE_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' fee_type: '||I_fee_type||' clearing_zone_id: '||I_clearing_zone_id);
   close C_CHECK_REC_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_FEE_ZONE_DUP;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_TARIFF_TREATMENT_DUP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_dup_exists         IN OUT   BOOLEAN,
                                    I_hts                IN       HTS_TARIFF_TREATMENT_ZONE.HTS%TYPE,
                                    I_import_country_id  IN       HTS_TARIFF_TREATMENT_ZONE.IMPORT_COUNTRY_ID%TYPE,
                                    I_effect_from        IN       HTS_TARIFF_TREATMENT_ZONE.EFFECT_FROM%TYPE,
                                    I_effect_to          IN       HTS_TARIFF_TREATMENT_ZONE.EFFECT_TO%TYPE,
                                    I_tariff_treatment   IN       HTS_TARIFF_TREATMENT_ZONE.TARIFF_TREATMENT%TYPE,
                                    I_clearing_zone_id   IN       HTS_TARIFF_TREATMENT_ZONE.CLEARING_ZONE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)  := 'HTS_CLEAR_ZONE_SQL.CHECK_TARIFF_TREATMENT_DUP';
   L_rec_exists     VARCHAR2(1)   := NULL;

   cursor C_CHECK_REC_EXISTS is
      select 'x'
        from hts_tariff_treatment_zone
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and tariff_treatment  = I_tariff_treatment
         and clearing_zone_id  = I_clearing_zone_id
         and rownum            = 1;

BEGIN
   O_dup_exists := TRUE;
   ---
   if I_hts is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hts',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_from is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_from',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_to is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_to',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_tariff_treatment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tariff_treatment',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_clearing_zone_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_clearing_zone_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_REC_EXISTS',
                    'HTS_TARIFF_TREATMENT_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tariff_treatment: '||I_tariff_treatment||' clearing_zone_id: '||I_clearing_zone_id);
   open C_CHECK_REC_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_REC_EXISTS',
                    'HTS_TARIFF_TREATMENT_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tariff_treatment: '||I_tariff_treatment||' clearing_zone_id: '||I_clearing_zone_id);
   fetch C_CHECK_REC_EXISTS into L_rec_exists;

   if C_CHECK_REC_EXISTS%NOTFOUND then 
      O_dup_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_REC_EXISTS',
                    'HTS_TARIFF_TREATMENT_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tariff_treatment: '||I_tariff_treatment||' clearing_zone_id: '||I_clearing_zone_id);
   close C_CHECK_REC_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_TARIFF_TREATMENT_DUP;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_TAX_ZONE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts               IN       HTS_TAX_ZONE.HTS%TYPE,
                         I_import_country_id IN       HTS_TAX_ZONE.IMPORT_COUNTRY_ID%TYPE,
                         I_effect_from       IN       HTS_TAX_ZONE.EFFECT_FROM%TYPE,
                         I_effect_to         IN       HTS_TAX_ZONE.EFFECT_TO%TYPE,
                         I_tax_type          IN       HTS_TAX_ZONE.TAX_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)  := 'HTS_CLEAR_ZONE_SQL.DELETE_TAX_ZONE';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_locked, -54);

   cursor C_LOCK_RECS is
      select 'x'
        from hts_tax_zone
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and tax_type          = I_tax_type
         for update nowait;

BEGIN
   ---
   if I_hts is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hts',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_from is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_from',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_to is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_to',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_tax_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tax_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_RECS',
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type);
   open C_LOCK_RECS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_LOCK_RECS',
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type);

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_RECS',
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type);
   close C_LOCK_RECS;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type);

   delete hts_tax_zone
    where hts               = I_hts
      and import_country_id = I_import_country_id
      and effect_from       = I_effect_from
      and effect_to         = I_effect_to
      and tax_type          = I_tax_type;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', 
                                            'HTS_TAX_ZONE',
                                            I_hts, 
                                            I_tax_type);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_TAX_ZONE;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_FEE_ZONE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts               IN       HTS_FEE_ZONE.HTS%TYPE,
                         I_import_country_id IN       HTS_FEE_ZONE.IMPORT_COUNTRY_ID%TYPE,
                         I_effect_from       IN       HTS_FEE_ZONE.EFFECT_FROM%TYPE,
                         I_effect_to         IN       HTS_FEE_ZONE.EFFECT_TO%TYPE,
                         I_fee_type          IN       HTS_FEE_ZONE.FEE_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)  := 'HTS_CLEAR_ZONE_SQL.DELETE_FEE_ZONE';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_locked, -54);

   cursor C_LOCK_RECS is
      select 'x'
        from hts_fee_zone
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and fee_type          = I_fee_type
         for update nowait;

BEGIN
   ---
   if I_hts is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hts',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_from is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_from',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_to is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_to',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_fee_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fee_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_RECS',
                    'HTS_FEE_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' fee_type: '||I_fee_type);
   open C_LOCK_RECS;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_RECS',
                    'HTS_FEE_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' fee_type: '||I_fee_type);
   close C_LOCK_RECS;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'HTS_FEE_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' fee_type: '||I_fee_type);

   delete hts_fee_zone
    where hts               = I_hts
      and import_country_id = I_import_country_id
      and effect_from       = I_effect_from
      and effect_to         = I_effect_to
      and fee_type          = I_fee_type;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', 
                                            'HTS_FEE_ZONE',
                                            I_hts, 
                                            I_fee_type);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_FEE_ZONE;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_TARIFF_TREATMENT_ZONE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_hts                 IN       HTS_TARIFF_TREATMENT_ZONE.HTS%TYPE,
                                      I_import_country_id   IN       HTS_TARIFF_TREATMENT_ZONE.IMPORT_COUNTRY_ID%TYPE,
                                      I_effect_from         IN       HTS_TARIFF_TREATMENT_ZONE.EFFECT_FROM%TYPE,
                                      I_effect_to           IN       HTS_TARIFF_TREATMENT_ZONE.EFFECT_TO%TYPE,
                                      I_tariff_treatment    IN       HTS_TARIFF_TREATMENT_ZONE.TARIFF_TREATMENT%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)  := 'HTS_CLEAR_ZONE_SQL.DELETE_TARIFF_TREATMENT_ZONE';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_locked, -54);

   cursor C_LOCK_RECS is
      select 'x'
        from hts_tariff_treatment_zone
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and tariff_treatment  = I_tariff_treatment
         for update nowait;

BEGIN
   ---
   if I_hts is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hts',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_from is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_from',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_to is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_to',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_tariff_treatment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tariff_treatment',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_RECS',
                    'HTS_TARIFF_TREATMENT_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tariff_treatment: '||I_tariff_treatment);
   open C_LOCK_RECS;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_RECS',
                    'HTS_TARIFF_TREATMENT_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tariff_treatment: '||I_tariff_treatment);
   close C_LOCK_RECS;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'HTS_TARIFF_TREATMENT_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tariff_treatment: '||I_tariff_treatment);

   delete hts_tariff_treatment_zone
    where hts               = I_hts
      and import_country_id = I_import_country_id
      and effect_from       = I_effect_from
      and effect_to         = I_effect_to
      and tariff_treatment  = I_tariff_treatment;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', 
                                            'HTS_TARIFF_TREATMENT_ZONE',
                                            I_hts, 
                                            I_tariff_treatment);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_TARIFF_TREATMENT_ZONE;
---------------------------------------------------------------------------------------------
FUNCTION LOCK_HTS_TAX(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_hts               IN       HTS_TAX.HTS%TYPE,
                      I_import_country_id IN       HTS_TAX.IMPORT_COUNTRY_ID%TYPE,
                      I_effect_from       IN       HTS_TAX.EFFECT_FROM%TYPE,
                      I_effect_to         IN       HTS_TAX.EFFECT_TO%TYPE,
                      I_tax_type          IN       HTS_TAX.TAX_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)  := 'HTS_CLEAR_ZONE_SQL.LOCK_HTS_TAX';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_locked, -54);

   cursor C_LOCK_RECS is
      select 'x'
        from hts_tax
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and tax_type          = I_tax_type
         for update nowait;

BEGIN
   ---
   if I_hts is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hts',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_from is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_from',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_to is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_to',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_tax_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tax_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_RECS',
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type);
   open C_LOCK_RECS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_LOCK_RECS',
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type);

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_RECS',
                    'HTS_TAX_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tax_type: '||I_tax_type);
   close C_LOCK_RECS;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', 
                                            'HTS_TAX',
                                            I_hts, 
                                            I_tax_type);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_HTS_TAX;
---------------------------------------------------------------------------------------------
FUNCTION LOCK_HTS_FEE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_hts               IN       HTS_FEE.HTS%TYPE,
                      I_import_country_id IN       HTS_FEE.IMPORT_COUNTRY_ID%TYPE,
                      I_effect_from       IN       HTS_FEE.EFFECT_FROM%TYPE,
                      I_effect_to         IN       HTS_FEE.EFFECT_TO%TYPE,
                      I_fee_type          IN       HTS_FEE.FEE_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)  := 'HTS_CLEAR_ZONE_SQL.LOCK_HTS_FEE';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_locked, -54);

   cursor C_LOCK_RECS is
      select 'x'
        from hts_fee
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and fee_type          = I_fee_type
         for update nowait;

BEGIN
   ---
   if I_hts is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hts',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_from is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_from',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_to is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_to',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_fee_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_fee_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_RECS',
                    'HTS_FEE_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' fee_type: '||I_fee_type);
   open C_LOCK_RECS;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_RECS',
                    'HTS_FEE_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' fee_type: '||I_fee_type);
   close C_LOCK_RECS;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', 
                                            'HTS_FEE',
                                            I_hts, 
                                            I_fee_type);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_HTS_FEE;
---------------------------------------------------------------------------------------------
FUNCTION LOCK_HTS_TARIFF_TREATMENT(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_hts                 IN       HTS_TARIFF_TREATMENT.HTS%TYPE,
                                   I_import_country_id   IN       HTS_TARIFF_TREATMENT.IMPORT_COUNTRY_ID%TYPE,
                                   I_effect_from         IN       HTS_TARIFF_TREATMENT.EFFECT_FROM%TYPE,
                                   I_effect_to           IN       HTS_TARIFF_TREATMENT.EFFECT_TO%TYPE,
                                   I_tariff_treatment    IN       HTS_TARIFF_TREATMENT.TARIFF_TREATMENT%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)  := 'HTS_CLEAR_ZONE_SQL.LOCK_HTS_TARIFF_TREATMENT';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_locked, -54);

   cursor C_LOCK_RECS is
      select 'x'
        from hts_tariff_treatment
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and tariff_treatment  = I_tariff_treatment
         for update nowait;

BEGIN
   ---
   if I_hts is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_hts',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_from is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_from',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_effect_to is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effect_to',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_tariff_treatment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tariff_treatment',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_RECS',
                    'HTS_TARIFF_TREATMENT_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tariff_treatment: '||I_tariff_treatment);
   open C_LOCK_RECS;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_RECS',
                    'HTS_TARIFF_TREATMENT_ZONE',
                    'HTS: '||I_hts||' import_country_id: '||I_import_country_id||' tariff_treatment: '||I_tariff_treatment);
   close C_LOCK_RECS;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', 
                                            'HTS_TARIFF_TREATMENT',
                                            I_hts, 
                                            I_tariff_treatment);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_HTS_TARIFF_TREATMENT;
---------------------------------------------------------------------------------------------
END;
/