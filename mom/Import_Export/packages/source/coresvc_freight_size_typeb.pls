CREATE OR REPLACE PACKAGE BODY CORESVC_FREIGHT_SIZES_TYPES AS
   cursor C_SVC_FREIGHT_SIZE(I_process_id   NUMBER,
                             I_chunk_id     NUMBER) IS
      select pk_freight_size.rowid  as pk_freight_size_rid,
             st.freight_size_desc,
             UPPER(st.freight_size) as freight_size,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)       as action,
             st.process$status,
             st.rowid               as st_rid
        from svc_freight_size st,
             freight_size pk_freight_size,
             dual
       where st.process_id           = I_process_id
         and st.chunk_id             = I_chunk_id
         and UPPER(st.freight_size)  = pk_freight_size.freight_size (+);
         
   cursor C_SVC_FREIGHT_TYPE(I_process_id   NUMBER,
                             I_chunk_id     NUMBER) is
      select pk_freight_type.rowid  as pk_freight_type_rid,
             st.rowid               as st_rid,
             st.freight_type_desc,
             UPPER(st.freight_type) as freight_type,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)       as action,
             st.process$status
        from svc_freight_type st,
             freight_type pk_freight_type,
             dual
       where st.process_id          = I_process_id
         and st.chunk_id            = I_chunk_id
         and UPPER(st.freight_type) = pk_freight_type.freight_type (+);         

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab     errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;

   Type FREIGHT_SIZE_TL_TAB IS TABLE OF FREIGHT_SIZE_TL%ROWTYPE;
   Type FTYPE_TL_TAB IS TABLE OF FREIGHT_TYPE_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   LP_primary_lang    LANG.LANG%TYPE;
   
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.EXISTS(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet   IN VARCHAR2,
                          I_row_seq IN NUMBER,
                          I_col     IN VARCHAR2,
                          I_sqlcode IN NUMBER,
                          I_sqlerrm IN VARCHAR2)IS
BEGIN
   Lp_s9t_errors_tab.EXTEND();
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id  IN SVC_ADMIN_UPLD_ER.process_id%TYPE,
                      I_error_seq   IN SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                      I_chunk_id    IN SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                      I_table_name  IN SVC_ADMIN_UPLD_ER.table_name%TYPE,
                      I_row_seq     IN SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                      I_column_name IN SVC_ADMIN_UPLD_ER.column_name%TYPE,
                      I_error_msg   IN SVC_ADMIN_UPLD_ER.error_msg%TYPE,
                      I_error_type  IN SVC_ADMIN_UPLD_ER.error_type%TYPE default 'E') IS
BEGIN
   Lp_errors_tab.EXTEND();
   Lp_errors_tab(Lp_errors_tab.COUNT()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.COUNT()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.COUNT()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.COUNT()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.COUNT()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.COUNT()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.COUNT()).error_type  := I_error_type;
END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets               S9T_PKG.names_map_typ;
   FREIGHT_SIZE_cols      S9T_PKG.names_map_typ;
   FREIGHT_SIZE_TL_cols   S9T_PKG.names_map_typ;
   FREIGHT_TYPE_cols      S9T_PKG.names_map_typ;
   FREIGHT_TYPE_TL_cols   S9T_PKG.names_map_typ;
BEGIN
   L_sheets                         := s9t_pkg.get_sheet_names(I_file_id);
   FREIGHT_SIZE_cols                := s9t_pkg.get_col_names(I_file_id,FREIGHT_SIZE_sheet);
   FREIGHT_SIZE$Action              := FREIGHT_SIZE_cols('ACTION');
   FREIGHT_SIZE$FREIGHT_SIZE        := FREIGHT_SIZE_cols('FREIGHT_SIZE');
   FREIGHT_SIZE$FREIGHT_SIZE_DESC   := FREIGHT_SIZE_cols('FREIGHT_SIZE_DESC');

   FREIGHT_SIZE_TL_cols             := s9t_pkg.get_col_names(I_file_id,FREIGHT_SIZE_TL_sheet);
   FREIGHT_SIZE_TL$Action           := FREIGHT_SIZE_TL_cols('ACTION');
   FREIGHT_SIZE_TL$LANG             := FREIGHT_SIZE_TL_cols('LANG');
   FREIGHT_SIZE_TL$FREIGHT_SIZE     := FREIGHT_SIZE_TL_cols('FREIGHT_SIZE');
   FREIGHT_SIZE_TL$DESC             := FREIGHT_SIZE_TL_cols('FREIGHT_SIZE_DESC');
   
   FREIGHT_TYPE_cols                := s9t_pkg.get_col_names(I_file_id,FREIGHT_TYPE_sheet);
   FREIGHT_TYPE$Action              := FREIGHT_TYPE_cols('ACTION');
   FREIGHT_TYPE$FREIGHT_TYPE_DESC   := FREIGHT_TYPE_cols('FREIGHT_TYPE_DESC');
   FREIGHT_TYPE$FREIGHT_TYPE        := FREIGHT_TYPE_cols('FREIGHT_TYPE');

   FREIGHT_TYPE_TL_cols             := s9t_pkg.get_col_names(I_file_id,FREIGHT_TYPE_TL_sheet);
   FREIGHT_TYPE_TL$Action           := FREIGHT_TYPE_TL_cols('ACTION');
   FREIGHT_TYPE_TL$LANG             := FREIGHT_TYPE_TL_cols('LANG');
   FREIGHT_TYPE_TL$FREIGHT_TYPE     := FREIGHT_TYPE_TL_cols('FREIGHT_TYPE');
   FREIGHT_TYPE_TL$FRGHT_TYP_DESC   := FREIGHT_TYPE_TL_cols('FREIGHT_TYPE_DESC');
   
END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_FREIGHT_SIZE(I_file_id IN NUMBER)IS
BEGIN
   insert into TABLE (select  ss.s9t_rows
                        from  s9t_folder sf,
                              TABLE  (sf.s9t_file_obj.sheets) ss
                       where  sf.file_id  = I_file_id
                         and  ss.sheet_name = FREIGHT_SIZE_sheet)
            select S9T_ROW(S9T_CELLS( CORESVC_FREIGHT_SIZES_TYPES.action_mod,
                                      t1.FREIGHT_SIZE,
                                      t.FREIGHT_SIZE_DESC))
              from FREIGHT_SIZE t1, FREIGHT_SIZE_TL t
             where t1.freight_size = t.freight_size
               and t.lang = LP_primary_lang;
END POPULATE_FREIGHT_SIZE;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_FREIGHT_SIZE_TL(I_file_id IN NUMBER)IS
BEGIN
   insert into TABLE (select  ss.s9t_rows
                        from  s9t_folder sf,
                              TABLE  (sf.s9t_file_obj.sheets) ss
                       where  sf.file_id  = I_file_id
                         and  ss.sheet_name = FREIGHT_SIZE_TL_sheet)
            select S9T_ROW(S9T_CELLS( CORESVC_FREIGHT_SIZES_TYPES.action_mod,
                                      lang,
                                      freight_size,
                                      freight_size_desc))
              from freight_size_tl
             where lang <> LP_primary_lang;

END POPULATE_FREIGHT_SIZE_TL;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_FREIGHT_TYPE(I_file_id   IN   NUMBER) IS
BEGIN
   insert into table (select ss.s9t_rows
                        from s9t_folder sf,
                       table (sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = FREIGHT_TYPE_sheet )
   select s9t_row(s9t_cells(CORESVC_FREIGHT_SIZES_TYPES.action_mod,
                            ft.freight_type,
                            tl.freight_type_desc ))
     from freight_type    ft,
          freight_type_tl tl
    where ft.freight_type = tl.freight_type
      and tl.lang = LP_primary_lang;

END POPULATE_FREIGHT_TYPE;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_FREIGHT_TYPE_TL(I_file_id   IN   NUMBER) IS
BEGIN
   insert into table (select ss.s9t_rows
                        from s9t_folder sf,
                       table (sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = FREIGHT_TYPE_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_FREIGHT_SIZES_TYPES.action_mod,
                            lang,
                            freight_type,
                            freight_type_desc ))
     from freight_type_tl tl
    where lang <> LP_primary_lang;

END POPULATE_FREIGHT_TYPE_TL;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id IN OUT NUMBER)IS
   L_file      S9T_FILE;
   L_file_name S9T_FOLDER.file_name%type;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;

   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(FREIGHT_SIZE_sheet);
   L_file.sheets(L_file.get_sheet_index(FREIGHT_SIZE_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                          'FREIGHT_SIZE',
                                                                                          'FREIGHT_SIZE_DESC');

   L_file.add_sheet(FREIGHT_SIZE_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(FREIGHT_SIZE_TL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                             'LANG',
                                                                                             'FREIGHT_SIZE',
                                                                                             'FREIGHT_SIZE_DESC');
   
   L_file.add_sheet(FREIGHT_TYPE_sheet);
   L_file.sheets(L_file.GET_SHEET_INDEX(FREIGHT_TYPE_sheet)).column_headers := s9t_cells('ACTION',
                                                                                         'FREIGHT_TYPE',
                                                                                         'FREIGHT_TYPE_DESC');

   L_file.add_sheet(FREIGHT_TYPE_TL_sheet);
   L_file.sheets(L_file.GET_SHEET_INDEX(FREIGHT_TYPE_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                            'LANG',
                                                                                            'FREIGHT_TYPE',
                                                                                            'FREIGHT_TYPE_DESC');
   
   S9T_PKG.save_obj(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind IN     CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_FREIGHT_SIZES_TYPES.CREATE_S9T';
   L_file    s9t_file;
BEGIN
   INIT_S9T(O_file_id);
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_FREIGHT_SIZE(O_file_id);
      POPULATE_FREIGHT_SIZE_TL(O_file_id);
      POPULATE_FREIGHT_TYPE(O_file_id);
      POPULATE_FREIGHT_TYPE_TL(O_file_id);
      Commit;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,template_key);
   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------
---  Name: PROCESS_S9T_FREIGHT_SIZE
--- Purpose: Load data to staging table SVC_FREIGHT_SIZE
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_FREIGHT_SIZE( I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                                    I_process_id IN SVC_FREIGHT_SIZE.PROCESS_ID%TYPE)IS

   TYPE svc_FREIGHT_SIZE_col_typ IS TABLE OF SVC_FREIGHT_SIZE%ROWTYPE;
   L_temp_rec           SVC_FREIGHT_SIZE%ROWTYPE;
   SVC_FREIGHT_SIZE_COL SVC_FREIGHT_SIZE_col_typ :=NEW svc_FREIGHT_SIZE_col_typ();
   L_process_id         SVC_FREIGHT_SIZE.process_id%type;
   L_error              BOOLEAN:=FALSE;
   L_default_rec        SVC_FREIGHT_SIZE%rowtype;

   cursor C_MANDATORY_IND IS
      select  FREIGHT_SIZE_DESC_mi,
              FREIGHT_SIZE_mi,
              1 as dummy
        from  (select column_key,
                      mandatory
                 from s9t_tmpl_cols_def
                where template_key  = CORESVC_FREIGHT_SIZES_TYPES.template_key
                  and wksht_key    = 'FREIGHT_SIZE')
        PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ('FREIGHT_SIZE_DESC' as FREIGHT_SIZE_DESC,
                                                         'FREIGHT_SIZE'      as FREIGHT_SIZE,
                                                                  null       as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_FREIGHT_SIZE';
   L_pk_columns    VARCHAR2(255)  := 'Freight Size';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select  FREIGHT_SIZE_DESC_dv,
                       FREIGHT_SIZE_dv,
                       null as dummy
                 from  (select column_key,
                               default_value
                          from s9t_tmpl_cols_def
                         where template_key  = CORESVC_FREIGHT_SIZES_TYPES.template_key
                           and wksht_key    = 'FREIGHT_SIZE')
                PIVOT (MAX(default_value) as dv FOR (column_key) IN ('FREIGHT_SIZE_DESC' as FREIGHT_SIZE_DESC,
                                                                     'FREIGHT_SIZE'      as FREIGHT_SIZE,
                                                                      NULL               as dummy)))
   LOOP
      BEGIN
         L_default_rec.FREIGHT_SIZE_DESC := rec.FREIGHT_SIZE_DESC_dv;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FREIGHT_SIZE',
                            NULL,
                           'FREIGHT_SIZE_DESC',
                           'INV_DEFAULT',
                            SQLERRM);
      END;

      BEGIN
         L_default_rec.FREIGHT_SIZE := UPPER(rec.FREIGHT_SIZE_dv);
      EXCEPTION
         when OTHERS then

            WRITE_S9T_ERROR(I_file_id,
                           'FREIGHT_SIZE',
                            NULL,
                           'FREIGHT_SIZE',
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

 --Get mandatory indicators
   open  C_mandatory_ind;
   fetch C_mandatory_ind
    INTO L_mi_rec;
   close C_mandatory_ind;
   FOR rec IN  (select UPPER(r.get_cell(freight_size$action))        as action,
                       r.get_cell(freight_size$freight_size_desc)    as freight_size_desc,
                       UPPER(r.get_cell(freight_size$freight_size))  as freight_size,
                       r.get_row_seq()                               as row_seq
                  from s9t_folder sf,
                       TABLE(sf.s9t_file_obj.sheets) ss,
                       TABLE(ss.s9t_rows) r
                 where sf.file_id  = I_file_id
                   and ss.sheet_name = SHEET_NAME_TRANS(FREIGHT_SIZE_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;

      BEGIN
         L_temp_rec.Action := rec.Action;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               FREIGHT_SIZE_sheet,
                               rec.row_seq,
                               action_column,
                               SQLCODE,
                               SQLERRM);
                               L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.FREIGHT_SIZE_DESC := rec.FREIGHT_SIZE_DESC;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               FREIGHT_SIZE_sheet,
                               rec.row_seq,
                               'FREIGHT_SIZE_DESC',
                               SQLCODE,
                               SQLERRM);
                               L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.FREIGHT_SIZE := UPPER(rec.FREIGHT_SIZE);
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               FREIGHT_SIZE_sheet,
                               rec.row_seq,
                               'FREIGHT_SIZE',
                               SQLCODE,
                               SQLERRM);
                               L_error := TRUE;
      END;


      if rec.action = CORESVC_FREIGHT_SIZES_TYPES.action_new then
         L_temp_rec.FREIGHT_SIZE_DESC := NVL( L_temp_rec.FREIGHT_SIZE_DESC,
                                              L_default_rec.FREIGHT_SIZE_DESC);
         L_temp_rec.FREIGHT_SIZE      := NVL( L_temp_rec.FREIGHT_SIZE,
                                              L_default_rec.FREIGHT_SIZE);
      end if;


      if NOT (L_temp_rec.FREIGHT_SIZE is NOT NULL and 1 = 1) then
         WRITE_S9T_ERROR( I_file_id,
                          FREIGHT_SIZE_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));

         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_FREIGHT_SIZE_col.EXTEND();
         svc_FREIGHT_SIZE_col(svc_FREIGHT_SIZE_col.count()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_FREIGHT_SIZE_col.count SAVE EXCEPTIONS
      Merge into SVC_FREIGHT_SIZE st USING
            (select (case
                     when l_mi_rec.FREIGHT_SIZE_DESC_mi    = 'N'
                      and svc_FREIGHT_SIZE_col(i).action   = CORESVC_FREIGHT_SIZES_TYPES.action_mod
                      and s1.FREIGHT_SIZE_DESC  IS NULL
                     then mtl.FREIGHT_SIZE_DESC
                     else s1.FREIGHT_SIZE_DESC
                     end) as FREIGHT_SIZE_DESC,
                    (case
                     when l_mi_rec.FREIGHT_SIZE_mi         = 'N'
                      and svc_FREIGHT_SIZE_col(i).action   = CORESVC_FREIGHT_SIZES_TYPES.action_mod
                      and s1.FREIGHT_SIZE       IS NULL
                     then mtl.FREIGHT_SIZE
                     else s1.FREIGHT_SIZE
                     end) as FREIGHT_SIZE,
                     NULL as dummy
              from (select svc_FREIGHT_SIZE_col(i).FREIGHT_SIZE_DESC as FREIGHT_SIZE_DESC,
                           svc_FREIGHT_SIZE_col(i).FREIGHT_SIZE      as FREIGHT_SIZE,
                           NULL as dummy                            
                     from  dual) s1,
                     FREIGHT_SIZE_TL mtl
             where  mtl.FREIGHT_SIZE (+)    = s1.FREIGHT_SIZE   and
                    mtl.LANG (+)            = LP_primary_lang)
                    sq ON (st.FREIGHT_SIZE  = sq.FREIGHT_SIZE   and  
                    svc_FREIGHT_SIZE_col(i).action in (CORESVC_FREIGHT_SIZES_TYPES.action_mod,CORESVC_FREIGHT_SIZES_TYPES.action_del))

   when matched then
      update
         set    process_id         = svc_freight_size_col(i).process_id,
                chunk_id           = svc_freight_size_col(i).chunk_id,
                row_seq            = svc_freight_size_col(i).row_seq,
                action             = svc_freight_size_col(i).action,
                process$status     = svc_freight_size_col(i).process$status,
                freight_size_desc  = sq.freight_size_desc,
                create_id          = svc_freight_size_col(i).create_id,
                create_datetime    = svc_freight_size_col(i).create_datetime,
                last_upd_id        = svc_freight_size_col(i).last_upd_id,
                last_upd_datetime  = svc_freight_size_col(i).last_upd_datetime
   when NOT matched then
      insert   (process_id,
                chunk_id,
                row_seq,
                action,
                process$status,
                freight_size_desc,
                freight_size,
                create_id,
                create_datetime,
                last_upd_id,
                last_upd_datetime)
      values  (svc_freight_size_col(i).process_id,
               svc_freight_size_col(i).chunk_id,
               svc_freight_size_col(i).row_seq,
               svc_freight_size_col(i).action,
               svc_freight_size_col(i).process$status,
               sq.freight_size_desc,
               sq.freight_size,
               svc_freight_size_col(i).create_id,
               svc_freight_size_col(i).create_datetime,
               svc_freight_size_col(i).last_upd_id,
               svc_freight_size_col(i).last_upd_datetime);

   EXCEPTION
      when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.count
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          FREIGHT_SIZE_sheet,
                          svc_FREIGHT_SIZE_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
      when OTHERS then
         if C_MANDATORY_IND%ISOPEN then
            close C_MANDATORY_IND;
         end if;
         ROLLBACK;
   END;
   
END PROCESS_S9T_FREIGHT_SIZE;
--------------------------------------------------------------------------------
---  Name: PROCESS_S9T_FREIGHT_SIZE_TL
--- Purpose: Load data to staging table SVC_FREIGHT_SIZE_TL
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_FREIGHT_SIZE_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                      I_process_id   IN   SVC_FREIGHT_SIZE_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_FREIGHT_SIZE_TL_COL_TYP IS TABLE OF SVC_FREIGHT_SIZE_TL%ROWTYPE;
   L_temp_rec                SVC_FREIGHT_SIZE_TL%ROWTYPE;
   SVC_FREIGHT_SIZE_TL_col   SVC_FREIGHT_SIZE_TL_COL_TYP := NEW SVC_FREIGHT_SIZE_TL_COL_TYP();
   L_process_id              SVC_FREIGHT_SIZE_TL.PROCESS_ID%TYPE;
   L_error                   BOOLEAN                     := FALSE;
   L_default_rec             SVC_FREIGHT_SIZE_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select lang_mi,
             freight_size_mi,
             freight_size_desc_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = FREIGHT_SIZE_TL_sheet)
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('LANG'                as lang,
                                       'FREIGHT_SIZE'        as freight_size,
                                       'FREIGHT_SIZE_DESC'   as freight_size_desc));

   L_mi_rec                  C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS                EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table                   VARCHAR2(30)                := 'SVC_FREIGHT_SIZE_TL';
   L_pk_columns              VARCHAR2(255)               := 'Lang, Freight Size';
   L_error_code              NUMBER;
   L_error_msg               RTK_ERRORS.RTK_TEXT%type;

BEGIN
  -- Get default values.
   FOR rec IN (select lang_dv,
                      freight_size_dv,
                      freight_size_desc_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key = template_key
                          and wksht_key    = FREIGHT_SIZE_TL_sheet)
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('LANG'                as lang,
                                                'FREIGHT_SIZE'        as freight_size,
                                                'FREIGHT_SIZE_DESC'   as freight_size_desc)))
   LOOP
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_SIZE_TL_sheet ,
                            NULL,
                           'LANG' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.freight_size := rec.freight_size_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_SIZE_TL_sheet ,
                            NULL,
                           'FREIGHT_SIZE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.freight_size_desc := rec.freight_size_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_SIZE_TL_sheet ,
                            NULL,
                           'FREIGHT_SIZE_DESC' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select upper(r.get_cell(FREIGHT_SIZE_TL$Action))   as action,
                      r.get_cell(FREIGHT_SIZE_TL$LANG)            as lang,
                      r.get_cell(FREIGHT_SIZE_TL$FREIGHT_SIZE)    as freight_size,
                      r.get_cell(FREIGHT_SIZE_TL$DESC)            as freight_size_desc,
                      r.get_row_seq()                             as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(FREIGHT_SIZE_TL_sheet))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_SIZE_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_SIZE_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.freight_size := rec.freight_size;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_SIZE_TL_sheet,
                            rec.row_seq,
                            'FREIGHT_SIZE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.freight_size_desc := rec.freight_size_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_SIZE_TL_sheet,
                            rec.row_seq,
                            'FREIGHT_SIZE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_FREIGHT_SIZES_TYPES.action_new then
         L_temp_rec.lang              := NVL( L_temp_rec.lang,L_default_rec.lang);
         L_temp_rec.freight_size      := NVL( L_temp_rec.freight_size,l_default_rec.freight_size);
         L_temp_rec.freight_size_desc := NVL( L_temp_rec.freight_size_desc,l_default_rec.freight_size_desc);
      end if;
      if NOT (L_temp_rec.lang is NOT NULL and L_temp_rec.freight_size is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         FREIGHT_SIZE_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_FREIGHT_SIZE_TL_col.extend();
         SVC_FREIGHT_SIZE_TL_col(SVC_FREIGHT_SIZE_TL_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..SVC_FREIGHT_SIZE_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_FREIGHT_SIZE_TL st
      using(select(case
                   when L_mi_rec.lang_mi = 'N'
                    and SVC_FREIGHT_SIZE_TL_col(i).action = CORESVC_FREIGHT_SIZES_TYPES.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when L_mi_rec.freight_size_mi = 'N'
                    and SVC_FREIGHT_SIZE_TL_col(i).action = CORESVC_FREIGHT_SIZES_TYPES.action_mod
                    and s1.freight_size IS NULL then
                        mt.freight_size
                   else s1.freight_size
                   end) as freight_size,
                  (case
                   when L_mi_rec.freight_size_desc_mi = 'N'
                    and SVC_FREIGHT_SIZE_TL_col(i).action = CORESVC_FREIGHT_SIZES_TYPES.action_mod
                    and s1.freight_size_desc IS NULL then
                        mt.freight_size_desc
                   else s1.freight_size_desc
                   end) as freight_size_desc,
                   null as dummy
              from (select SVC_FREIGHT_SIZE_TL_col(i).lang as lang,
                           SVC_FREIGHT_SIZE_TL_col(i).freight_size as freight_size,
                           SVC_FREIGHT_SIZE_TL_col(i).freight_size_desc as freight_size_desc,
                           null as dummy
                      from dual ) s1,
                    freight_size_tl mt
             where mt.freight_size (+) = s1.freight_size
               and mt.lang (+) = s1.lang)sq
               ON (st.freight_size   = sq.freight_size and
                   st.lang     = sq.lang and
                   SVC_FREIGHT_SIZE_TL_col(i).action IN (CORESVC_FREIGHT_SIZES_TYPES.action_mod,CORESVC_FREIGHT_SIZES_TYPES.action_del))
      when matched then
      update
         set process_id      = SVC_FREIGHT_SIZE_TL_col(i).process_id ,
             chunk_id        = SVC_FREIGHT_SIZE_TL_col(i).chunk_id ,
             row_seq         = SVC_FREIGHT_SIZE_TL_col(i).row_seq ,
             action          = SVC_FREIGHT_SIZE_TL_col(i).action ,
             process$status  = SVC_FREIGHT_SIZE_TL_col(i).process$status ,
             freight_size_desc = sq.freight_size_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             lang ,
             freight_size ,
             freight_size_desc)
      values(SVC_FREIGHT_SIZE_TL_col(i).process_id ,
             SVC_FREIGHT_SIZE_TL_col(i).chunk_id ,
             SVC_FREIGHT_SIZE_TL_col(i).row_seq ,
             SVC_FREIGHT_SIZE_TL_col(i).action ,
             SVC_FREIGHT_SIZE_TL_col(i).process$status ,
             sq.lang ,
             sq.freight_size ,
             sq.freight_size_desc);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP

         L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_SIZE_TL_SHEET,
                            SVC_FREIGHT_SIZE_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);

         END LOOP;
   END;
END PROCESS_S9T_FREIGHT_SIZE_TL;
--------------------------------------------------------------------------------
---  Name: PROCESS_S9T_FREIGHT_TYPE
--- Purpose: Load data to staging table SVC_FREIGHT_TYPE
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_FREIGHT_TYPE( I_file_id    IN s9t_folder.file_id%TYPE,
                                    I_process_id IN SVC_FREIGHT_TYPE.process_id%TYPE) IS

   TYPE svc_FREIGHT_TYPE_col_typ IS TABLE OF svc_freight_type%ROWTYPE;
   L_temp_rec       SVC_FREIGHT_TYPE%ROWTYPE;
   svc_FREIGHT_TYPE_col svc_FREIGHT_TYPE_col_typ :=NEW svc_FREIGHT_TYPE_col_typ();
   L_process_id     SVC_FREIGHT_TYPE.process_id%TYPE;
   L_error          BOOLEAN:=FALSE;
   L_default_rec    SVC_FREIGHT_TYPE%ROWTYPE;
   cursor c_mandatory_ind is
      select FREIGHT_TYPE_DESC_mi,
             FREIGHT_TYPE_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = CORESVC_FREIGHT_SIZES_TYPES.template_key
                 and wksht_key    = 'FREIGHT_TYPE')
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('FREIGHT_TYPE_DESC' as FREIGHT_TYPE_DESC,
                                                                'FREIGHT_TYPE'      as FREIGHT_TYPE));
   L_mi_rec        c_mandatory_ind%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_FREIGHT_TYPE';
   L_pk_columns    VARCHAR2(255)  := 'Freight Type';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
   --Get default values.
   FOR rec IN (select FREIGHT_TYPE_DESC_dv,
                      FREIGHT_TYPE_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = CORESVC_FREIGHT_SIZES_TYPES.template_key
                          and wksht_key    = 'FREIGHT_TYPE' )
                        pivot (MAX(default_value) as dv FOR (column_key) IN ( 'FREIGHT_TYPE_DESC' as FREIGHT_TYPE_DESC,
                                                                              'FREIGHT_TYPE'      as FREIGHT_TYPE)))
   LOOP
      BEGIN
      L_default_rec.FREIGHT_TYPE_DESC := rec.FREIGHT_TYPE_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'FREIGHT_TYPE',
                             NULL,
                            'FREIGHT_TYPE_DESC',
                            'INV_DEFAULT',
                             SQLERRM);
      END;

      BEGIN
      L_default_rec.FREIGHT_TYPE := rec.FREIGHT_TYPE_dv;
      EXCEPTION
         WHEN OTHERS THEN
            WRITE_S9T_ERROR( I_file_id,
                            'FREIGHT_TYPE',
                             NULL,
                            'FREIGHT_TYPE',
                            'INV_DEFAULT',
                             SQLERRM);
      END;
   END LOOP;

   --Get mandatory indicators
   OPEN C_mandatory_ind;
   FETCH C_mandatory_ind
   INTO L_mi_rec;
   CLOSE C_mandatory_ind;
   FOR rec IN (select UPPER(r.get_cell(FREIGHT_TYPE$Action))       as Action,
                      r.get_cell(FREIGHT_TYPE$FREIGHT_TYPE_DESC)   as FREIGHT_TYPE_DESC,
                      UPPER(r.get_cell(FREIGHT_TYPE$FREIGHT_TYPE)) as FREIGHT_TYPE,
                      r.get_row_seq()                              as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = SHEET_NAME_TRANS(FREIGHT_TYPE_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               FREIGHT_TYPE_sheet,
                               rec.row_seq,
                               action_column,
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.FREIGHT_TYPE_DESC := rec.FREIGHT_TYPE_DESC;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               FREIGHT_TYPE_sheet,
                               rec.row_seq,
                               'FREIGHT_TYPE_DESC',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.FREIGHT_TYPE := rec.FREIGHT_TYPE;
            EXCEPTION
               when OTHERS then
                  WRITE_S9T_ERROR(I_file_id,
                                  FREIGHT_TYPE_sheet,
                                  rec.row_seq,
                                  'FREIGHT_TYPE',
                                  SQLCODE,
                                  SQLERRM);
                  L_error := TRUE;
      END;
      if rec.action = CORESVC_FREIGHT_SIZES_TYPES.action_new then
         L_temp_rec.FREIGHT_TYPE_DESC := NVL( L_temp_rec.FREIGHT_TYPE_DESC,
                                              L_default_rec.FREIGHT_TYPE_DESC);
         L_temp_rec.FREIGHT_TYPE      := NVL( L_temp_rec.FREIGHT_TYPE,
                                              L_default_rec.FREIGHT_TYPE);
      end if;

      if NOT ( L_temp_rec.FREIGHT_TYPE is NOT NULL) then
         WRITE_S9T_ERROR( I_file_id,
                          FREIGHT_TYPE_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_FREIGHT_TYPE_col.EXTEND();
         svc_FREIGHT_TYPE_col(svc_FREIGHT_TYPE_col.COUNT()):=L_temp_rec;
      end if;

      L_temp_rec := NULL;

   END LOOP;

   BEGIN

   FORALL i IN 1..svc_FREIGHT_TYPE_col.count
      SAVE EXCEPTIONS Merge INTO SVC_FREIGHT_TYPE st USING
         (select(case
                    when L_mi_rec.FREIGHT_TYPE_mi    = 'N'
                     and svc_FREIGHT_TYPE_col(i).action = CORESVC_FREIGHT_SIZES_TYPES.action_mod
                     and s1.FREIGHT_TYPE             IS NULL
                    then mtl.FREIGHT_TYPE
                    else s1.FREIGHT_TYPE
                    end) as FREIGHT_TYPE,
                (case
                    when L_mi_rec.FREIGHT_TYPE_DESC_mi    = 'N'
                     and svc_FREIGHT_TYPE_col(i).action = CORESVC_FREIGHT_SIZES_TYPES.action_mod
                     and s1.FREIGHT_TYPE_DESC             IS NULL
                    then mtl.FREIGHT_TYPE_DESC
                    else s1.FREIGHT_TYPE_DESC
                    end) as FREIGHT_TYPE_DESC,
                null as dummy
           from (select svc_FREIGHT_TYPE_col(i).FREIGHT_TYPE as FREIGHT_TYPE,
                        svc_FREIGHT_TYPE_col(i).FREIGHT_TYPE_DESC as FREIGHT_TYPE_DESC,
                        NULL as dummy
                   from dual
                 )s1,
                freight_type_tl mtl
          where mtl.freight_type (+) = s1.freight_type
            and mtl.lang (+) = LP_primary_lang) sq
            ON ( st.FREIGHT_TYPE      = sq.FREIGHT_TYPE and
                 svc_FREIGHT_TYPE_col(i).action in (CORESVC_FREIGHT_SIZES_TYPES.action_mod,CORESVC_FREIGHT_SIZES_TYPES.action_del))
         when matched then
            update set process_id        = svc_FREIGHT_TYPE_col(i).PROCESS_ID ,
                       chunk_id          = svc_FREIGHT_TYPE_col(i).CHUNK_ID ,
                       row_seq           = svc_FREIGHT_TYPE_col(i).ROW_SEQ ,
                       action            = svc_FREIGHT_TYPE_col(i).ACTION,
                       process$status    = svc_FREIGHT_TYPE_col(i).PROCESS$STATUS ,
                       freight_type_desc = sq.FREIGHT_TYPE_DESC,
                       create_id         = svc_FREIGHT_TYPE_col(i).CREATE_ID ,
                       create_datetime   = svc_FREIGHT_TYPE_col(i).CREATE_DATETIME ,
                       last_upd_id       = svc_FREIGHT_TYPE_col(i).LAST_UPD_ID ,
                       last_upd_datetime = svc_FREIGHT_TYPE_col(i).LAST_UPD_DATETIME
         when NOT matched then
            insert(process_id ,
                   chunk_id ,
                   row_seq ,
                   action ,
                   process$status ,
                   freight_type_desc ,
                   freight_type ,
                   create_id ,
                   create_datetime ,
                   last_upd_id ,
                   last_upd_datetime)
            values(svc_FREIGHT_TYPE_col(i).PROCESS_ID ,
                   svc_FREIGHT_TYPE_col(i).CHUNK_ID ,
                   svc_FREIGHT_TYPE_col(i).ROW_SEQ ,
                   svc_FREIGHT_TYPE_col(i).ACTION ,
                   svc_FREIGHT_TYPE_col(i).PROCESS$STATUS ,
                   sq.FREIGHT_TYPE_DESC,
                   sq.FREIGHT_TYPE,
                   svc_FREIGHT_TYPE_col(i).CREATE_ID ,
                   svc_FREIGHT_TYPE_col(i).CREATE_DATETIME ,
                   svc_FREIGHT_TYPE_col(i).LAST_UPD_ID ,
                   svc_FREIGHT_TYPE_col(i).LAST_UPD_DATETIME);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             FREIGHT_TYPE_sheet,
                             svc_FREIGHT_TYPE_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
      when OTHERS then
         if C_MANDATORY_IND%ISOPEN then
            close C_MANDATORY_IND;
         end if;
         ROLLBACK;
   END;
END PROCESS_S9T_FREIGHT_TYPE;
--------------------------------------------------------------------------------
---  Name: PROCESS_S9T_FREIGHT_TYPE_TL
--- Purpose: Load data to staging table SVC_FREIGHT_TYPE_TL
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_FREIGHT_TYPE_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                      I_process_id   IN   SVC_FREIGHT_TYPE_TL.PROCESS_ID%TYPE) IS

   TYPE svc_frght_typ_tl_col_typ is TABLE OF SVC_FREIGHT_TYPE_TL%ROWTYPE;
   L_temp_rec             SVC_FREIGHT_TYPE_TL%ROWTYPE;
   svc_frght_typ_tl_col   svc_frght_typ_tl_col_typ := NEW svc_frght_typ_tl_col_typ();
   L_process_id           SVC_FREIGHT_TYPE_TL.PROCESS_ID%TYPE;
   L_error                BOOLEAN                  := FALSE;
   L_default_rec          SVC_FREIGHT_TYPE_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select lang_mi,
             freight_type_mi,
             freight_type_desc_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key =  template_key
                 and wksht_key    = 'FREIGHT_TYPE_TL'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('LANG'              as lang,
                                            'FREIGHT_TYPE'      as freight_type,
                                            'FREIGHT_TYPE_DESC' as freight_type_desc));

   L_mi_rec                C_MANDATORY_IND%ROWTYPE;
   dml_errors              EXCEPTION;
   PRAGMA                  exception_init(dml_errors, -24381);
   L_table                 VARCHAR2(30)   := 'SVC_FREIGHT_TYPE_TL';
   L_pk_columns            VARCHAR2(255)  := 'Freight Type, Lang';
   L_error_code            NUMBER;
   L_error_msg             RTK_ERRORS.RTK_TEXT%type;

BEGIN
   FOR rec IN (select lang_dv,
                      freight_type_dv,
                      freight_type_desc_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key =  template_key
                          and wksht_key    = 'FREIGHT_TYPE_TL'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'LANG'              as lang,
                                                      'FREIGHT_TYPE'      as freight_type,
                                                      'FREIGHT_TYPE_DESC' as freight_type_desc)))
   LOOP
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TYPE_TL_sheet,
                            NULL,
                            'LANG',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.freight_type_desc := rec.freight_type_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TYPE_TL_sheet,
                            NULL,
                            'FREIGHT_TYPE_DESC',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.freight_type := rec.freight_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TYPE_TL_sheet,
                            NULL,
                            'FREIGHT_TYPE',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(freight_type_tl$action)                as action,
                     r.get_cell(freight_type_tl$lang)                  as lang,
                     UPPER(r.get_cell(freight_type_tl$freight_type))   as freight_type,
                     r.get_cell(freight_type_tl$frght_typ_desc)        as freight_type_desc,
                     r.get_row_seq()                                   as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(FREIGHT_TYPE_TL_sheet)
             )
   LOOP
      L_temp_rec                := NULL;
      L_temp_rec.process_id     := I_process_id;
      L_temp_rec.chunk_id       := 1;
      L_temp_rec.row_seq        := rec.row_seq;
      L_temp_rec.process$status := 'N';
      L_error := FALSE;

      BEGIN
        L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TYPE_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
        L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TYPE_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
        L_temp_rec.freight_type_desc := rec.freight_type_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TYPE_TL_sheet,
                            rec.row_seq,
                            'FREIGHT_TYPE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
        L_temp_rec.freight_type := rec.freight_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FREIGHT_TYPE_TL_sheet,
                            rec.row_seq,
                            'FREIGHT_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_FREIGHT_SIZES_TYPES.action_new then
        L_temp_rec.freight_type_desc := NVL(L_temp_rec.freight_type_desc, L_default_rec.freight_type_desc);
        L_temp_rec.freight_type      := NVL(L_temp_rec.freight_type,L_default_rec.freight_type);
        L_temp_rec.lang              := NVL(L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.freight_type is NOT NULL and L_temp_rec.lang is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         FREIGHT_TYPE_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_frght_typ_tl_col.extend();
         svc_frght_typ_tl_col(svc_frght_typ_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_frght_typ_tl_col.COUNT SAVE EXCEPTIONS
         merge into SVC_FREIGHT_TYPE_TL st
         using(select (case
                       when L_mi_rec.freight_type_mi    = 'N'
                       and svc_frght_typ_tl_col(i).action   = CORESVC_FREIGHT_SIZES_TYPES.action_mod
                          and s1.freight_type             is NULL
                       then mt.freight_type
                       else s1.freight_type
                       end) as freight_type,
                      (case
                       when L_mi_rec.lang_mi    = 'N'
                       and svc_frght_typ_tl_col(i).action   = CORESVC_FREIGHT_SIZES_TYPES.action_mod
                          and s1.lang             is NULL
                       then mt.lang
                       else s1.lang
                       end) as lang,
                      (case
                       when L_mi_rec.freight_type_desc_mi = 'N'
                       and svc_frght_typ_tl_col(i).action     = CORESVC_FREIGHT_SIZES_TYPES.action_mod
                       and s1.freight_type_desc             is NULL
                       then mt.freight_type_desc
                       else s1.freight_type_desc
                        end) as freight_type_desc,
                        NULL as dummy
                 from (select svc_frght_typ_tl_col(i).lang as lang,
                              svc_frght_typ_tl_col(i).freight_type as freight_type,
                              svc_frght_typ_tl_col(i).freight_type_desc as freight_type_desc,
                              NULL as dummy
                         from dual) s1,
                       freight_type_tl mt
                where mt.lang (+)   = s1.lang
                  and mt.freight_type (+) = s1.freight_type) sq
                  on (st.freight_type = sq.freight_type
                      and st.lang     = sq.lang
                      and svc_frght_typ_tl_col(i).action in (CORESVC_FREIGHT_SIZES_TYPES.action_mod,
                                                             CORESVC_FREIGHT_SIZES_TYPES.action_del))
         when matched then
            update
               set process_id        = svc_frght_typ_tl_col(i).process_id,
                   chunk_id          = svc_frght_typ_tl_col(i).chunk_id,
                   row_seq           = svc_frght_typ_tl_col(i).row_seq,
                   action            = svc_frght_typ_tl_col(i).action,
                   process$status    = svc_frght_typ_tl_col(i).process$status,
                   freight_type_desc = sq.freight_type_desc
         when NOT matched then
            insert(process_id,
                   chunk_id,
                   row_seq,
                   action,
                   process$status,
                   lang,
                   freight_type,
                   freight_type_desc)
            values(svc_frght_typ_tl_col(i).process_id,
                   svc_frght_typ_tl_col(i).chunk_id,
                   svc_frght_typ_tl_col(i).row_seq,
                   svc_frght_typ_tl_col(i).action,
                   svc_frght_typ_tl_col(i).process$status,
                   sq.lang,
                   sq.freight_type,
                   sq.freight_type_desc);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..SQL%BULK_EXCEPTIONS.COUNT
            LOOP
               L_error_code:=sql%bulk_exceptions(i).error_code;
               if L_error_code=1 then
                  L_error_code := NULL;
                  L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
               end if;
               WRITE_S9T_ERROR(I_file_id,
                               FREIGHT_TYPE_TL_sheet,
                               svc_frght_typ_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                               NULL,
                               L_error_code,
                               L_error_msg);
            END LOOP;
   END;

END PROCESS_S9T_FREIGHT_TYPE_TL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_error_count   IN OUT NUMBER,
                      I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id    IN     NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_FREIGHT_SIZES_TYPES.PROCESS_S9T';
   L_file           S9T_FILE;
   L_sheets         S9T_PKG.NAMES_MAP_TYP;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR         EXCEPTION;
   PRAGMA           EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN

   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   commit;--to ensure that the record in s9t_folder is commited
      S9T_PKG.ODS2OBJ(I_file_id);
   commit;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);

   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.sheet_trans(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_FREIGHT_SIZE(I_file_id,
                               I_process_id);
      PROCESS_S9T_FREIGHT_SIZE_TL(I_file_id,
                                  I_process_id);
      PROCESS_S9T_FREIGHT_TYPE(I_file_id,
                               I_process_id);
      PROCESS_S9T_FREIGHT_TYPE_TL(I_file_id,
                                  I_process_id);                                  
   end if;

   O_error_count := LP_s9t_errors_tab.COUNT();

   forall i IN 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   commit;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
    ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   update svc_process_tracker
      set status       = 'PE',
          file_id      = I_file_id
    where process_id   = I_process_id;
   commit;
   return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
      insert into s9t_errors
           values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_SIZE_POST_INS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_rec           IN     C_SVC_FREIGHT_SIZE%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_FREIGHT_SIZES_TYPES.EXEC_FREIGHT_SIZE_POST_INS';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FREIGHT_SIZE_TL';
   L_exists    BOOLEAN;
BEGIN
   --Insert/merge into Freight_size_tl table
   if TRANSPORTATION_SQL.MERGE_FREIGHT_SIZE_TL(O_error_message,
                                               I_rec.freight_size,
                                               I_rec.freight_size_desc,
                                               LP_primary_lang) = FALSE then
      WRITE_ERROR(I_rec.PROCESS_ID,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.CHUNK_ID,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      ROLLBACK TO successful_inv_status;
   end if;

   return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));
         return FALSE;
END EXEC_FREIGHT_SIZE_POST_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_SIZE_INS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_freight_size_temp_rec IN     FREIGHT_SIZE%ROWTYPE,
                               I_rec                   IN     C_SVC_FREIGHT_SIZE%ROWTYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(64)                      := 'CORESVC_FREIGHT_SIZES_TYPES.EXEC_FREIGHT_SIZE_INS';
   L_table    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FREIGHT_SIZE';
BEGIN
   BEGIN
      insert into freight_size
           values I_freight_size_temp_rec;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END;
   if EXEC_FREIGHT_SIZE_POST_INS(O_error_message,
                                 I_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      return FALSE;
   end if;
return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));
         return FALSE;
END EXEC_FREIGHT_SIZE_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_SIZE_UPD(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_freight_size_temp_rec IN     FREIGHT_SIZE%ROWTYPE)
RETURN BOOLEAN IS
   L_table    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FREIGHT_SIZE';
   L_program  VARCHAR2(64)                      := 'CORESVC_FREIGHT_SIZES_TYPES.EXEC_FREIGHT_SIZE_UPD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   cursor C_LOCK_FREIGHT_SIZE_UPD is
      select 'x'
        from freight_size
       where freight_size = I_freight_size_temp_rec.freight_size
         for update nowait;

BEGIN
   open  C_LOCK_FREIGHT_SIZE_UPD;
   close C_LOCK_FREIGHT_SIZE_UPD;

   update freight_size
      set row = I_freight_size_temp_rec
    where 1 = 1
      and freight_size = I_freight_size_temp_rec.freight_size;

return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_freight_size_temp_rec.freight_size,
                                             NULL);
      close C_LOCK_FREIGHT_SIZE_UPD;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_FREIGHT_SIZE_UPD%ISOPEN   then
         close C_LOCK_FREIGHT_SIZE_UPD;
      end if;
      return FALSE;
END EXEC_FREIGHT_SIZE_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_SIZE_DEL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_freight_size_temp_rec IN     FREIGHT_SIZE%ROWTYPE)
RETURN BOOLEAN IS
   L_table    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FREIGHT_SIZE';
   L_program  VARCHAR2(64)                      := 'CORESVC_FREIGHT_SIZES_TYPES.EXEC_FREIGHT_SIZE_DEL';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);
   cursor C_LOCK_FREIGHT_SIZE_DEL is
      select 'x'
        from freight_size
       where freight_size = I_freight_size_temp_rec.freight_size
         for update nowait;

BEGIN
   open  C_LOCK_FREIGHT_SIZE_DEL;
   close C_LOCK_FREIGHT_SIZE_DEL;

   --Delete in freight_size_tl table
   if TRANSPORTATION_SQL.DEL_FREIGHT_SIZE_TL(O_error_message,
                                             I_freight_size_temp_rec.freight_size) = FALSE then
      return FALSE;
   end if;

   delete from freight_size
    where freight_size = I_freight_size_temp_rec.freight_size;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_freight_size_temp_rec.freight_size,
                                             NULL);
      close C_LOCK_FREIGHT_SIZE_DEL;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_FREIGHT_SIZE_DEL%ISOPEN   then
         close C_LOCK_FREIGHT_SIZE_DEL;
      end if;
      return FALSE;
END EXEC_FREIGHT_SIZE_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_SIZE_TL_INS(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_freight_size_tl_ins_tab   IN       FREIGHT_SIZE_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_FREIGHT_SIZES_TYPES.EXEC_FREIGHT_SIZE_TL_INS';

BEGIN
   if I_freight_size_tl_ins_tab is NOT NULL and I_freight_size_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_freight_size_tl_ins_tab.COUNT()
         insert into freight_size_tl
              values I_freight_size_tl_ins_tab(i);

      FORALL i IN 1..I_freight_size_tl_ins_tab.COUNT()
         update freight_size_tl
            set reviewed_ind = 'Y'
          where reviewed_ind = 'N'
            and freight_size = I_freight_size_tl_ins_tab(i).freight_size
            and lang = LP_primary_lang;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_FREIGHT_SIZE_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_SIZE_TL_UPD(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_freight_size_tl_upd_tab  IN       FREIGHT_SIZE_TL_TAB,
                                  I_freight_size_tl_upd_rst  IN       ROW_SEQ_TAB,
                                  I_process_id               IN       SVC_FREIGHT_SIZE_TL.PROCESS_ID%TYPE,
                                  I_chunk_id                 IN       SVC_FREIGHT_SIZE_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_FREIGHT_SIZES_TYPES.EXEC_FREIGHT_SIZE_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FREIGHT_SIZE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_FREIGHT_SIZE_TL_UPD(I_freight_size  FREIGHT_SIZE_TL.FREIGHT_SIZE%TYPE,
                                I_lang          FREIGHT_SIZE_TL.LANG%TYPE) is
      select 'x'
        from freight_size_tl
       where freight_size = I_freight_size
         and lang = I_lang
         for update nowait;

BEGIN
   if I_freight_size_tl_upd_tab is NOT NULL and I_freight_size_tl_upd_tab.count > 0 then
      for i in I_freight_size_tl_upd_tab.FIRST..I_freight_size_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_freight_size_tl_upd_tab(i).lang);
            L_key_val2 := 'Freight Size: '||to_char(I_freight_size_tl_upd_tab(i).freight_size);
            open C_FREIGHT_SIZE_TL_UPD(I_freight_size_tl_upd_tab(i).freight_size,
                                       I_freight_size_tl_upd_tab(i).lang);
            close C_FREIGHT_SIZE_TL_UPD;

            update freight_size_tl
               set freight_size_desc = I_freight_size_tl_upd_tab(i).freight_size_desc,
                   last_update_id = I_freight_size_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_freight_size_tl_upd_tab(i).last_update_datetime
             where lang = I_freight_size_tl_upd_tab(i).lang
               and freight_size = I_freight_size_tl_upd_tab(i).freight_size;
            
            update freight_size_tl
               set reviewed_ind = 'Y'
             where reviewed_ind = 'N'
               and freight_size = I_freight_size_tl_upd_tab(i).freight_size
               and lang = LP_primary_lang;
               
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_FREIGHT_SIZE_TL',
                           I_freight_size_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_FREIGHT_SIZE_TL_UPD%ISOPEN then
         close C_FREIGHT_SIZE_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_FREIGHT_SIZE_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_SIZE_TL_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_freight_size_tl_del_tab  IN       FREIGHT_SIZE_TL_TAB,
                                  I_freight_size_tl_del_rst  IN       ROW_SEQ_TAB,
                                  I_process_id               IN       SVC_FREIGHT_SIZE_TL.PROCESS_ID%TYPE,
                                  I_chunk_id                 IN       SVC_FREIGHT_SIZE_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_FREIGHT_SIZES_TYPES.EXEC_FREIGHT_SIZE_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FREIGHT_SIZE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_FREIGHT_SIZE_TL_DEL(I_freight_size  FREIGHT_SIZE_TL.FREIGHT_SIZE%TYPE,
                                I_lang          FREIGHT_SIZE_TL.LANG%TYPE) is
      select 'x'
        from freight_size_tl
       where freight_size = I_freight_size
         and lang = I_lang
         for update nowait;

BEGIN
   if I_freight_size_tl_del_tab is NOT NULL and I_freight_size_tl_del_tab.count > 0 then
      for i in I_freight_size_tl_del_tab.FIRST..I_freight_size_tl_del_tab.LAST loop
         BEGIN
         L_key_val1 := 'Lang: '||to_char(I_freight_size_tl_del_tab(i).lang);
         L_key_val2 := 'Freight Size: '||to_char(I_freight_size_tl_del_tab(i).freight_size);
         open C_FREIGHT_SIZE_TL_DEL(I_freight_size_tl_del_tab(i).freight_size,
                                    I_freight_size_tl_del_tab(i).lang);
         close C_FREIGHT_SIZE_TL_DEL;

         delete freight_size_tl
          where lang = I_freight_size_tl_del_tab(i).lang
            and freight_size = I_freight_size_tl_del_tab(i).freight_size;

         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_FREIGHT_SIZE_TL',
                           I_freight_size_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_FREIGHT_SIZE_TL_DEL%ISOPEN then
         close C_FREIGHT_SIZE_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_FREIGHT_SIZE_TL_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_FREIGHT_SIZE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error         IN OUT BOOLEAN,
                                  I_rec           IN     C_SVC_FREIGHT_SIZE%ROWTYPE)
RETURN BOOLEAN IS
   L_table    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_FREIGHT_SIZE';
   L_program  VARCHAR2(64)                      :='CORESVC_FREIGHT_SIZES_TYPES.PROCESS_VAL_FREIGHT_SIZE';
   L_exists   BOOLEAN;
BEGIN
   --To Check if record exists in the transportation table
   if I_rec.action = action_del then
      if TRANSPORTATION_SQL.CHECK_DELETE_FREIGHT_SIZE(O_error_message,
                                                      L_exists,
                                                      I_rec.freight_size)= FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;

      if L_exists then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FREIGHT_SIZE',
                     'CANNOT_DELETE_FREIGHT_SIZ');
         O_error := TRUE;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_VAL_FREIGHT_SIZE;
--------------------------------------------------------------------------------
FUNCTION PROCESS_FREIGHT_SIZE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id     IN     SVC_FREIGHT_SIZE.PROCESS_ID%TYPE,
                              I_chunk_id       IN     SVC_FREIGHT_SIZE.CHUNK_ID%TYPE )

RETURN BOOLEAN IS
   L_program               VARCHAR2(64)                      :='CORESVC_FREIGHT_SIZES_TYPES.PROCESS_FREIGHT_SIZE';
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_FREIGHT_SIZE';
   L_error                 BOOLEAN;
   L_process_error         BOOLEAN;
   L_exists                BOOLEAN;
   L_freight_size_temp_rec FREIGHT_SIZE%ROWTYPE;
BEGIN
   FOR rec IN c_svc_FREIGHT_SIZE(I_process_id,I_chunk_id)
   LOOP
   SAVEPOINT successful_inv_status;
     L_error := FALSE;

      -- check if the action is NULL or if it is not amoung NEW, MOD,DEL
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del)   then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      -- check if the given freight_size is already present
      if rec.action = action_new
         and rec.PK_FREIGHT_SIZE_rid is NOT NULL   then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     l_table,
                     rec.row_seq,
                     'FREIGHT_SIZE',
                     'DUP_FREIGHT_SIZE');
         L_error :=TRUE;
      end if;

      -- Check if the freight_size is NULL when action is MOD or DEL
      if rec.action IN (action_mod,action_del)
         and rec.PK_FREIGHT_SIZE_rid is NULL   then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     l_table,
                     rec.row_seq,
                     'FREIGHT_SIZE',
                     'FREIGHT_SIZE_MISSING');
         L_error :=TRUE;
      end if;

      -- Check if given freight size is null when action is NEW or MOD
      if rec.action IN (action_new,action_mod)   then
         if NOT( rec.FREIGHT_SIZE_DESC is NOT NULL )   then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'FREIGHT_SIZE_DESC',
                        'ENTER_FREIGHT_SIZE_DESC');
            L_error :=TRUE;
         end if;
      end if;

      --Validation for delete
      if PROCESS_VAL_FREIGHT_SIZE(O_error_message,
                                  L_error,
                                  rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_freight_size_temp_rec.FREIGHT_SIZE    := rec.FREIGHT_SIZE;
         L_freight_size_temp_rec.CREATE_ID       := GET_USER;
         L_freight_size_temp_rec.CREATE_DATETIME := SYSDATE;

            --If there is no error then do the processing
            if rec.action = action_new then
               if EXEC_FREIGHT_SIZE_INS(O_error_message,
                                        L_freight_size_temp_rec,
                                        rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,rec.row_seq,
                              NULL,
                              O_error_message);
                  L_process_error := TRUE;
               end if;
            end if;

            if rec.action = action_mod then
               -- update in the freight_size_tl table on modification
               if TRANSPORTATION_SQL.MERGE_FREIGHT_SIZE_TL(O_error_message,
                                                           rec.freight_size,
                                                           rec.freight_size_desc,
                                                           LP_primary_lang) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              NULL,
                              O_error_message);
               end if;
               if EXEC_FREIGHT_SIZE_UPD(O_error_message,
                                        L_freight_size_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              NULL,
                              O_error_message);
                  L_process_error := TRUE;
               end if;
            end if;

            if rec.action = action_del then
               if EXEC_FREIGHT_SIZE_DEL(O_error_message,
                                        L_freight_size_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              NULL,
                              O_error_message);
                   L_process_error := TRUE;
               end if;
            end if;
      end if;

   END LOOP;
   return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));
         return FALSE;
END PROCESS_FREIGHT_SIZE;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_FREIGHT_SIZE_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id      IN       SVC_FREIGHT_SIZE_TL.PROCESS_ID%TYPE,
                                 I_chunk_id        IN       SVC_FREIGHT_SIZE_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                   VARCHAR2(64) := 'CORESVC_FREIGHT_SIZES_TYPES.PROCESS_FREIGHT_SIZE_TL';
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FREIGHT_SIZE_TL';
   L_base_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FREIGHT_SIZE';
   L_table                     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FREIGHT_SIZE_TL';
   L_error                     BOOLEAN := FALSE;
   L_process_error             BOOLEAN := FALSE;
   L_freight_size_tl_temp_rec  FREIGHT_SIZE_TL%ROWTYPE;
   L_freight_size_tl_upd_rst   ROW_SEQ_TAB;
   L_freight_size_tl_del_rst   ROW_SEQ_TAB;

   cursor C_SVC_FREIGHT_SIZE_TL(I_process_id NUMBER,
                                I_chunk_id NUMBER) is
      select pk_freight_size_tl.rowid   as pk_freight_size_tl_rid,
             fk_freight_size.rowid      as fk_freight_size_rid,
             fk_lang.rowid              as fk_lang_rid,
             freight_size_tl_prim.rowid as freight_size_tl_prim_rid,
             st.lang,
             st.freight_size,
             st.freight_size_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_freight_size_tl  st,
             freight_size         fk_freight_size,
             freight_size_tl      pk_freight_size_tl,
             (select freight_size
                from freight_size_tl
               where lang = LP_primary_lang) freight_size_tl_prim,
             lang                 fk_lang
       where st.process_id   =  I_process_id
         and st.chunk_id     =  I_chunk_id
         and st.freight_size =  fk_freight_size.freight_size (+)
         and st.lang         =  pk_freight_size_tl.lang (+)
         and st.freight_size =  pk_freight_size_tl.freight_size (+)
         and st.lang         =  fk_lang.lang (+)
         and st.freight_size =  freight_size_tl_prim.freight_size (+);

   TYPE SVC_FREIGHT_SIZE_TL is TABLE OF C_SVC_FREIGHT_SIZE_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_freight_size_tl_tab         SVC_FREIGHT_SIZE_TL;

   L_freight_size_tl_ins_tab         freight_size_tl_tab         := NEW freight_size_tl_tab();
   L_freight_size_tl_upd_tab         freight_size_tl_tab         := NEW freight_size_tl_tab();
   L_freight_size_tl_del_tab         freight_size_tl_tab         := NEW freight_size_tl_tab();

BEGIN
   if C_SVC_FREIGHT_SIZE_TL%ISOPEN then
      close C_SVC_FREIGHT_SIZE_TL;
   end if;

   open C_SVC_FREIGHT_SIZE_TL(I_process_id,
                              I_chunk_id);
   LOOP
      fetch C_SVC_FREIGHT_SIZE_TL bulk collect into L_svc_freight_size_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_freight_size_tl_tab.COUNT > 0 then
         FOR i in L_svc_freight_size_tl_tab.FIRST..L_svc_freight_size_tl_tab.LAST LOOP
         L_error := FALSE;

            --check for primary_lang
            if L_svc_freight_size_tl_tab(i).lang = LP_primary_lang and L_svc_freight_size_tl_tab(i).action = action_new then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_freight_size_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               L_error :=TRUE;
            end if;

            -- check if action is valid
            if L_svc_freight_size_tl_tab(i).action is NULL
               or L_svc_freight_size_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_freight_size_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_freight_size_tl_tab(i).action = action_new
               and L_svc_freight_size_tl_tab(i).pk_freight_size_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_freight_size_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_freight_size_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_freight_size_tl_tab(i).lang is NOT NULL
               and L_svc_freight_size_tl_tab(i).freight_size is NOT NULL
               and L_svc_freight_size_tl_tab(i).pk_freight_size_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_freight_size_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- ensure that the primary language record exists before adding a new record in non-primary lang
            if L_svc_freight_size_tl_tab(i).action = action_new
               and L_svc_freight_size_tl_tab(i).freight_size is NOT NULL
               and L_svc_freight_size_tl_tab(i).lang <> LP_primary_lang
               and L_svc_freight_size_tl_tab(i).freight_size_tl_prim_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_freight_size_tl_tab(i).row_seq,
                            'LANG',
                            'PRIMARY_LANG_REQ');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_freight_size_tl_tab(i).action = action_new
               and L_svc_freight_size_tl_tab(i).freight_size is NOT NULL
               and L_svc_freight_size_tl_tab(i).fk_freight_size_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_freight_size_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_freight_size_tl_tab(i).action = action_new
               and L_svc_freight_size_tl_tab(i).lang is NOT NULL
               and L_svc_freight_size_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_freight_size_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_freight_size_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_freight_size_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if L_svc_freight_size_tl_tab(i).freight_size is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_freight_size_tl_tab(i).row_seq,
                           'FREIGHT_SIZE',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_freight_size_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_freight_size_tl_tab(i).freight_size_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_freight_size_tl_tab(i).row_seq,
                              'FREIGHT_SIZE_DESC',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if NOT L_error then
               L_freight_size_tl_temp_rec.lang := L_svc_freight_size_tl_tab(i).lang;
               L_freight_size_tl_temp_rec.freight_size := L_svc_freight_size_tl_tab(i).freight_size;
               L_freight_size_tl_temp_rec.freight_size_desc := L_svc_freight_size_tl_tab(i).freight_size_desc;
               L_freight_size_tl_temp_rec.orig_lang_ind := 'N';
               L_freight_size_tl_temp_rec.reviewed_ind := 'Y';
               L_freight_size_tl_temp_rec.create_datetime := SYSDATE;
               L_freight_size_tl_temp_rec.create_id := GET_USER;
               L_freight_size_tl_temp_rec.last_update_datetime := SYSDATE;
               L_freight_size_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_freight_size_tl_tab(i).action = action_new then
                  L_freight_size_tl_ins_tab.extend;
                  L_freight_size_tl_ins_tab(L_freight_size_tl_ins_tab.count()) := L_freight_size_tl_temp_rec;
               end if;

               if L_svc_freight_size_tl_tab(i).action = action_mod then
                  L_freight_size_tl_upd_tab.extend;
                  L_freight_size_tl_upd_tab(L_freight_size_tl_upd_tab.count()) := L_freight_size_tl_temp_rec;
                  L_freight_size_tl_upd_rst(L_freight_size_tl_upd_tab.count()) := L_svc_freight_size_tl_tab(i).row_seq;
               end if;

               if L_svc_freight_size_tl_tab(i).action = action_del then
                  L_freight_size_tl_del_tab.extend;
                  L_freight_size_tl_del_tab(L_freight_size_tl_del_tab.count()) := L_freight_size_tl_temp_rec;
                  L_freight_size_tl_del_rst(L_freight_size_tl_del_tab.count()) := L_svc_freight_size_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_FREIGHT_SIZE_TL%NOTFOUND;
   END LOOP;
   close C_SVC_FREIGHT_SIZE_TL;

   if EXEC_FREIGHT_SIZE_TL_INS(O_error_message,
                               L_freight_size_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_FREIGHT_SIZE_TL_UPD(O_error_message,
                               L_freight_size_tl_upd_tab,
                               L_freight_size_tl_upd_rst,
                               I_process_id,
                               I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_FREIGHT_SIZE_TL_DEL(O_error_message,
                               L_freight_size_tl_del_tab,
                               L_freight_size_tl_del_rst,
                               I_process_id,
                               I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_FREIGHT_SIZE_TL;
--------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_TYPE_POST_INS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_rec           IN     C_SVC_FREIGHT_TYPE%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      :='CORESVC_FREIGHT_SIZES_TYPES.EXEC_FREIGHT_TYPE_POST_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='FREIGHT_TYPE_TL';
   L_exists  BOOLEAN;
BEGIN
   if TRANSPORTATION_SQL.MERGE_FREIGHT_TYPE_TL(O_error_message,
                                               I_rec.freight_type,
                                               I_rec.freight_type_desc,
                                               LP_primary_lang) = FALSE then
      WRITE_ERROR(I_rec.PROCESS_ID,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.CHUNK_ID,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      ROLLBACK TO successful_inv_status;
   end if;

   return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));
         return FALSE;
END EXEC_FREIGHT_TYPE_POST_INS;
---------------------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_TYPE_INS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_freight_type_temp_rec IN       FREIGHT_TYPE%ROWTYPE,
                               I_rec                   IN       C_SVC_FREIGHT_TYPE%ROWTYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(64) := 'CORESVC_FREIGHT_SIZES_TYPES.EXEC_FREIGHT_TYPE_INS';
BEGIN
   BEGIN
   insert into freight_type
      values I_freight_type_temp_rec;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));
         return FALSE;
   END;

   if EXEC_FREIGHT_TYPE_POST_INS(O_error_message,
                                 I_rec) = FALSE then
      return FALSE;
   end if;
   return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));
         return FALSE;
END EXEC_FREIGHT_TYPE_INS;
-----------------------------------------------------------------------------------------------------
FUNCTION EXEC_FREIGHT_TYPE_DEL( O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rec                  IN      C_SVC_FREIGHT_TYPE%ROWTYPE )
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_FREIGHT_SIZES_TYPES.EXEC_FREIGHT_TYPE_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FREIGHT_TYPE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the FREIGHT_TYPE record
   cursor C_LOCK_FREIGHT_TYPE_DEL is
      select 'x'
        from FREIGHT_TYPE
       where freight_type = I_rec.freight_type
         for update nowait;

   --Cursor to lock the FREIGHT_TYPE_TL record
   cursor C_LOCK_FREIGHT_TYPE_TL_DEL is
      select 'x'
        from FREIGHT_TYPE_TL
       where freight_type = I_rec.freight_type
         for update nowait;

BEGIN
   open  C_LOCK_FREIGHT_TYPE_DEL;
   close C_LOCK_FREIGHT_TYPE_DEL;

   if TRANSPORTATION_SQL.DEL_FREIGHT_TYPE_TL(O_error_message,
                                             I_rec.freight_type) = FALSE then
      return FALSE;
   end if;

   open  C_LOCK_FREIGHT_TYPE_TL_DEL;
   close C_LOCK_FREIGHT_TYPE_TL_DEL;

   delete from freight_type
    where freight_type = I_rec.freight_type;

   return TRUE;
   EXCEPTION
      when RECORD_LOCKED then
         if C_LOCK_FREIGHT_TYPE_DEL%ISOPEN then
            close C_LOCK_FREIGHT_TYPE_DEL;
         end if;
         if C_LOCK_FREIGHT_TYPE_TL_DEL%ISOPEN then
            close C_LOCK_FREIGHT_TYPE_TL_DEL;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                L_table,
                                                I_rec.freight_type,
                                                NULL);
         return FALSE;

      when OTHERS then
         if C_LOCK_FREIGHT_TYPE_DEL%ISOPEN then
            close C_LOCK_FREIGHT_TYPE_DEL;
         end if;
         if C_LOCK_FREIGHT_TYPE_TL_DEL%ISOPEN then
            close C_LOCK_FREIGHT_TYPE_TL_DEL;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));
         return FALSE;
END EXEC_FREIGHT_TYPE_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_FREIGHT_TYPE (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_error          IN OUT  BOOLEAN,
                                   I_rec            IN      C_SVC_FREIGHT_TYPE%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_FREIGHT_SIZES_TYPES.PROCESS_VAL_FREIGHT_TYPE';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FREIGHT_TYPE';
   L_exists    BOOLEAN;
BEGIN
   --To check on delete
   if I_rec.action = action_del then
      if TRANSPORTATION_SQL.CHECK_DELETE_FREIGHT_TYPE(O_error_message,
                                                      L_exists,
                                                      I_rec.freight_type)= FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;

      if L_exists then
         WRITE_ERROR( I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'FREIGHT_TYPE',
                     'CANNOT_DELETE_FREIGHT_TYP');
         O_error := TRUE;
      end if;

   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_FREIGHT_TYPE;
-----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_FREIGHT_TYPE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id    IN     SVC_FREIGHT_TYPE.PROCESS_ID%TYPE,
                              I_chunk_id      IN     SVC_FREIGHT_TYPE.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program               VARCHAR2(64)                      :='CORESVC_FREIGHT_SIZES_TYPES.PROCESS_FREIGHT_TYPE';
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_FREIGHT_TYPE';
   L_freight_type_temp_rec FREIGHT_TYPE%ROWTYPE;
   L_error                 BOOLEAN;
   L_process_error         BOOLEAN;
   L_exists                BOOLEAN;
BEGIN
   FOR rec IN C_SVC_FREIGHT_TYPE(I_process_id,I_chunk_id)
   LOOP
      SAVEPOINT successful_inv_status;
      L_error := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      if rec.action = action_new
         and rec.PK_FREIGHT_TYPE_rid IS NOT NULL then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'FREIGHT_TYPE',
                     'DUP_FREIGHT_TYPE');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_FREIGHT_TYPE_rid IS NULL then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'FREIGHT_TYPE',
                     'FREIGHT_TYPE_MISSING');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_new) then
         if NOT(  rec.FREIGHT_TYPE_DESC  IS NOT NULL ) then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'FREIGHT_TYPE_DESC',
                        'ENTER_FREIGHT_TYPE_DESC');
            L_error :=TRUE;
         end if;
      end if;

      --Validation for foreign key constraints
      if PROCESS_VAL_FREIGHT_TYPE(O_error_message,
                                  L_error,
                                  rec) = FALSE then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      NULL,
                      O_error_message);
         L_error := TRUE;
      end if;

      --This updates process status column in the staging table entry with 'E' for a negative path
      if NOT L_error then
         L_process_error                      := FALSE;
         L_freight_type_temp_rec.FREIGHT_TYPE := rec.FREIGHT_TYPE;

         if rec.action = action_new then
            if EXEC_FREIGHT_TYPE_INS(O_error_message,
                                     L_freight_type_temp_rec,
                                     rec) = FALSE then

               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if TRANSPORTATION_SQL.MERGE_FREIGHT_TYPE_TL(O_error_message,
                                                        rec.freight_type,
                                                        rec.freight_type_desc,
                                                        LP_primary_lang) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_FREIGHT_TYPE_DEL(O_error_message,
                                     rec) = FALSE then

               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
                L_process_error := TRUE;
            end if;
         end if;
      end if;

   END LOOP;

   return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                TO_CHAR(SQLCODE));
         return FALSE;
END PROCESS_FREIGHT_TYPE;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_FTYPE_TL_INS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_frght_typ_tl_ins_tab   IN       FTYPE_TL_TAB)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_FREIGHT_SIZES_TYPES.EXEC_FTYPE_TL_INS';

BEGIN
   if I_frght_typ_tl_ins_tab is NOT NULL and I_frght_typ_tl_ins_tab.COUNT > 0 then
      FORALL i IN 1..I_frght_typ_tl_ins_tab.COUNT()
         insert into freight_type_tl
              values I_frght_typ_tl_ins_tab(i);

      FORALL i IN 1..I_frght_typ_tl_ins_tab.COUNT()
         update freight_type_tl
            set reviewed_ind = 'Y'
          where reviewed_ind = 'N'
            and freight_type = I_frght_typ_tl_ins_tab(i).freight_type
            and lang = LP_primary_lang;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_FTYPE_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_FTYPE_TL_UPD(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_frght_typ_tl_upd_tab   IN       FTYPE_TL_TAB,
                           I_frght_typ_tl_upd_rst  IN       ROW_SEQ_TAB,
                           I_process_id             IN       SVC_FREIGHT_TYPE_TL.PROCESS_ID%TYPE,
                           I_chunk_id               IN       SVC_FREIGHT_TYPE_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_FREIGHT_SIZES_TYPES.EXEC_FTYPE_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FREIGHT_TYPE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30)                      := NULL;
   L_key_val2      VARCHAR2(30)                      := NULL;

    --Cursor to lock the record
   cursor C_LOCK_FRGHT_TYP_TL_UPD(I_freight_type   FREIGHT_TYPE_TL.FREIGHT_TYPE%TYPE,
                                  I_lang           FREIGHT_TYPE_TL.LANG%TYPE) is
      select 'x'
        from freight_type_tl
       where freight_type = I_freight_type
         and lang = I_lang
         for update nowait;

BEGIN
   if I_frght_typ_tl_upd_tab is NOT NULL and I_frght_typ_tl_upd_tab.count > 0 then
      for i in I_frght_typ_tl_upd_tab.FIRST..I_frght_typ_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_frght_typ_tl_upd_tab(i).lang);
            L_key_val2 := 'Freight Type: '||to_char(I_frght_typ_tl_upd_tab(i).freight_type);
            open C_LOCK_FRGHT_TYP_TL_UPD(I_frght_typ_tl_upd_tab(i).freight_type,
                                         I_frght_typ_tl_upd_tab(i).lang);
            close C_LOCK_FRGHT_TYP_TL_UPD;
            
            update freight_type_tl
               set freight_type_desc = I_frght_typ_tl_upd_tab(i).freight_type_desc,
                   last_update_id = I_frght_typ_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_frght_typ_tl_upd_tab(i).last_update_datetime
             where lang = I_frght_typ_tl_upd_tab(i).lang
               and freight_type = I_frght_typ_tl_upd_tab(i).freight_type;
            
            update freight_type_tl
               set reviewed_ind = 'Y'
             where reviewed_ind = 'N'
               and freight_type = I_frght_typ_tl_upd_tab(i).freight_type
               and lang = LP_primary_lang;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_FREIGHT_TYPE_TL',
                           I_frght_typ_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_FRGHT_TYP_TL_UPD%ISOPEN then
         close C_LOCK_FRGHT_TYP_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_FTYPE_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_FTYPE_TL_DEL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_frght_typ_tl_del_tab   IN       FTYPE_TL_TAB,
                           I_frght_typ_tl_del_rst   IN       ROW_SEQ_TAB,
                           I_process_id             IN       SVC_FREIGHT_TYPE_TL.PROCESS_ID%TYPE,
                           I_chunk_id               IN       SVC_FREIGHT_TYPE_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_FREIGHT_SIZES_TYPES.EXEC_FTYPE_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FREIGHT_TYPE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30)                      := NULL;
   L_key_val2      VARCHAR2(30)                      := NULL;

    --Cursor to lock the record
   cursor C_LOCK_FRGHT_TYP_TL_DEL(I_freight_type   FREIGHT_TYPE_TL.FREIGHT_TYPE%TYPE,
                                  I_lang           FREIGHT_TYPE_TL.LANG%TYPE) is
      select 'x'
        from freight_type_tl
       where freight_type = I_freight_type
         and lang = I_lang
         for update nowait;

BEGIN
   if I_frght_typ_tl_del_tab is NOT NULL and I_frght_typ_tl_del_tab.COUNT > 0 then
      for i in I_frght_typ_tl_del_tab.FIRST..I_frght_typ_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_frght_typ_tl_del_tab(i).lang);
            L_key_val2 := 'Freight Type: '||to_char(I_frght_typ_tl_del_tab(i).freight_type);
            open C_LOCK_FRGHT_TYP_TL_DEL(I_frght_typ_tl_del_tab(i).freight_type,
                                         I_frght_typ_tl_del_tab(i).lang);
            close C_LOCK_FRGHT_TYP_TL_DEL;
            
            delete freight_type_tl
             where lang = I_frght_typ_tl_del_tab(i).lang
               and freight_type = I_frght_typ_tl_del_tab(i).freight_type;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_FREIGHT_TYPE_TL',
                           I_frght_typ_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_FRGHT_TYP_TL_DEL%ISOPEN then
         close C_LOCK_FRGHT_TYP_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_FTYPE_TL_DEL;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_FREIGHT_TYPE_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN       SVC_FREIGHT_TYPE_TL.PROCESS_ID%TYPE,
                                I_chunk_id        IN       SVC_FREIGHT_TYPE_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)                      := 'CORESVC_FREIGHT_SIZES_TYPES.PROCESS_FREIGHT_TYPE_TL';
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FREIGHT_TYPE_TL';
   L_base_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'FREIGHT_TYPE';
   L_table                    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FREIGHT_TYPE_TL';
   L_error                    BOOLEAN                           := FALSE;
   L_process_error            BOOLEAN                           := FALSE;
   L_frght_typ_tl_temp_rec    FREIGHT_TYPE_TL%ROWTYPE;
   L_frght_typ_tl_upd_rst     ROW_SEQ_TAB;
   L_frght_typ_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_FRGHT_TYP_TL(I_process_id   NUMBER,
                             I_chunk_id     NUMBER) is
      select pk_frght_typ_tl.rowid             as pk_frght_typ_tl_rid,
             fk_frght_typ.rowid                as fk_frght_typ_rid,
             fk_lang.rowid                     as fk_lang_rid,
             pk_frght_typ_tl_prim.freight_type as pk_frght_typ_tl_prim_rec,
             st.lang,
             UPPER(st.freight_type)            as freight_type,
             st.freight_type_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)                  as action,
             st.process$status
        from svc_freight_type_tl             st,
             freight_type                    fk_frght_typ,
             freight_type_tl                 pk_frght_typ_tl,
             (select freight_type
                from freight_type_tl
               where lang = LP_primary_lang) pk_frght_typ_tl_prim,
             lang                            fk_lang
       where st.process_id          = I_process_id
         and st.chunk_id            = I_chunk_id
         and UPPER(st.freight_type) = fk_frght_typ.freight_type (+)
         and st.lang                = pk_frght_typ_tl.lang (+)
         and UPPER(st.freight_type) = pk_frght_typ_tl.freight_type (+)
         and UPPER(st.freight_type) = pk_frght_typ_tl_prim.freight_type (+)
         and st.lang                = fk_lang.lang (+);

   TYPE SVC_FRGHT_TYP_TL is TABLE OF C_SVC_FRGHT_TYP_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_frght_typ_tl_tab    SVC_FRGHT_TYP_TL;

   L_frght_typ_tl_ins_tab    ftype_tl_tab                      := NEW ftype_tl_tab();
   L_frght_typ_tl_upd_tab    ftype_tl_tab                      := NEW ftype_tl_tab();
   L_frght_typ_tl_del_tab    ftype_tl_tab                      := NEW ftype_tl_tab();

BEGIN
   if C_SVC_FRGHT_TYP_TL%ISOPEN then
      close C_SVC_FRGHT_TYP_TL;
   end if;

   open C_SVC_FRGHT_TYP_TL(I_process_id,
                           I_chunk_id);
   LOOP
      fetch C_SVC_FRGHT_TYP_TL bulk collect into L_svc_frght_typ_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_frght_typ_tl_tab.COUNT > 0 then
         FOR i in L_svc_frght_typ_tl_tab.FIRST..L_svc_frght_typ_tl_tab.LAST LOOP
         L_error := FALSE;

            --check for primary_lang
            if L_svc_frght_typ_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_frght_typ_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;
            if L_svc_frght_typ_tl_tab(i).action = action_new and L_svc_frght_typ_tl_tab(i).lang <> LP_primary_lang and L_svc_frght_typ_tl_tab(i).pk_frght_typ_tl_rid is NULL then
               if L_svc_frght_typ_tl_tab(i).pk_frght_typ_tl_prim_rec is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_frght_typ_tl_tab(i).row_seq,
                              'LANG',
                              'PRIMARY_LANG_REQ');
                  L_error := TRUE;
               end if;
            end if;

            -- check if action is valid
            if L_svc_frght_typ_tl_tab(i).action is NULL
               or L_svc_frght_typ_tl_tab(i).action NOT IN (action_new, action_mod, action_del) then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_frght_typ_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error := TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_frght_typ_tl_tab(i).action = action_new
               and L_svc_frght_typ_tl_tab(i).pk_frght_typ_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_frght_typ_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_frght_typ_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_frght_typ_tl_tab(i).lang is NOT NULL
               and L_svc_frght_typ_tl_tab(i).freight_type is NOT NULL
               and L_svc_frght_typ_tl_tab(i).pk_frght_typ_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_frght_typ_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error := TRUE;
            end if;

            -- check for FK
            if L_svc_frght_typ_tl_tab(i).action = action_new
               and L_svc_frght_typ_tl_tab(i).freight_type is NOT NULL
               and L_svc_frght_typ_tl_tab(i).fk_frght_typ_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_frght_typ_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_frght_typ_tl_tab(i).action = action_new
               and L_svc_frght_typ_tl_tab(i).lang is NOT NULL
               and L_svc_frght_typ_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_frght_typ_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error := TRUE;
            end if;

            --check for required fields
            if L_svc_frght_typ_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_frght_typ_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error := TRUE;
            end if;

            if L_svc_frght_typ_tl_tab(i).freight_type is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_frght_typ_tl_tab(i).row_seq,
                           'FREIGHT_TYPE',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_frght_typ_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_frght_typ_tl_tab(i).freight_type_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_frght_typ_tl_tab(i).row_seq,
                              'FREIGHT_TYPE_DESC',
                              'MUST_ENTER_FIELD');
                  L_error := TRUE;
               end if;
            end if;

            if NOT L_error then
               L_frght_typ_tl_temp_rec.lang := L_svc_frght_typ_tl_tab(i).lang;
               L_frght_typ_tl_temp_rec.freight_type := L_svc_frght_typ_tl_tab(i).freight_type;
               L_frght_typ_tl_temp_rec.freight_type_desc := L_svc_frght_typ_tl_tab(i).freight_type_desc;
               L_frght_typ_tl_temp_rec.orig_lang_ind := 'N';
               L_frght_typ_tl_temp_rec.reviewed_ind := 'Y';
               L_frght_typ_tl_temp_rec.create_datetime := SYSDATE;
               L_frght_typ_tl_temp_rec.create_id := GET_USER;
               L_frght_typ_tl_temp_rec.last_update_datetime := SYSDATE;
               L_frght_typ_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_frght_typ_tl_tab(i).action = action_new then
                  L_frght_typ_tl_ins_tab.extend;
                  L_frght_typ_tl_ins_tab(L_frght_typ_tl_ins_tab.count()) := L_frght_typ_tl_temp_rec;
               end if;

               if L_svc_frght_typ_tl_tab(i).action = action_mod then
                  L_frght_typ_tl_upd_tab.extend;
                  L_frght_typ_tl_upd_tab(L_frght_typ_tl_upd_tab.count()) := L_frght_typ_tl_temp_rec;
                  L_frght_typ_tl_upd_rst(L_frght_typ_tl_upd_tab.count()) := L_svc_frght_typ_tl_tab(i).row_seq;
               end if;

               if L_svc_frght_typ_tl_tab(i).action = action_del then
                  L_frght_typ_tl_del_tab.extend;
                  L_frght_typ_tl_del_tab(L_frght_typ_tl_del_tab.count()) := L_frght_typ_tl_temp_rec;
                  L_frght_typ_tl_del_rst(L_frght_typ_tl_del_tab.count()) := L_svc_frght_typ_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_FRGHT_TYP_TL%NOTFOUND;
   END LOOP;
   close C_SVC_FRGHT_TYP_TL;

   if EXEC_FTYPE_TL_INS(O_error_message,
                        L_frght_typ_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_FTYPE_TL_UPD(O_error_message,
                        L_frght_typ_tl_upd_tab,
                        L_frght_typ_tl_upd_rst,
                        I_process_id,
                        I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_FTYPE_TL_DEL(O_error_message,
                        L_frght_typ_tl_del_tab,
                        L_frght_typ_tl_del_rst,
                        I_process_id,
                        I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_FREIGHT_TYPE_TL;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id  IN  NUMBER) IS
BEGIN
   delete from svc_freight_size_tl
      where process_id = I_process_id;

   delete from svc_freight_size
      where process_id = I_process_id;

   delete
     from svc_freight_type_tl
    where process_id = I_process_id;

   delete
     from svc_freight_type
    where process_id = I_process_id;         
END;
--------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%type,
                 O_error_count   IN OUT NUMBER,
                 I_process_id    IN     NUMBER,
                 I_chunk_id      IN     NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    :='CORESVC_FREIGHT_SIZES_TYPES.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_FREIGHT_SIZE(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_FREIGHT_SIZE_TL(O_error_message,
                              I_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_FREIGHT_TYPE(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;
   if PROCESS_FREIGHT_TYPE_TL(O_error_message,
                              I_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;
      
   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);

   LP_errors_tab := NEW errors_tab_typ();

   open  C_GET_ERR_COUNT;
   fetch C_GET_ERR_COUNT into L_err_count;
   close C_GET_ERR_COUNT;

   open  C_GET_WARN_COUNT;
   fetch C_GET_WARN_COUNT into L_warn_count;
   close C_GET_WARN_COUNT;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------
END CORESVC_FREIGHT_SIZES_TYPES;
/
