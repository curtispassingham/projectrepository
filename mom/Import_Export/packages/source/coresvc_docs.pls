-- File Name : CORESVC_DOC_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_DOC AUTHID CURRENT_USER AS

   template_key  CONSTANT    VARCHAR2(255)  := 'DOC_DATA';
   action_new                VARCHAR2(25)   := 'NEW';
   action_mod                VARCHAR2(25)   := 'MOD';
   action_del                VARCHAR2(25)   := 'DEL';
   DOC_sheet                 VARCHAR2(255)  := 'DOC';
   DOC$Action                NUMBER         := 1;
   DOC$DOC_ID                NUMBER         := 2;
   DOC$DOC_DESC              NUMBER         := 3;
   DOC$DOC_TYPE              NUMBER         := 4;
   DOC$LC_IND                NUMBER         := 5;
   DOC$SEQ_NO                NUMBER         := 6;
   DOC$TEXT                  NUMBER         := 7;

   DOC_TL_sheet              VARCHAR2(255)  := 'DOC_TL';
   DOC_TL$Action             NUMBER         := 1;
   DOC_TL$LANG               NUMBER         := 2;
   DOC_TL$DOC_ID             NUMBER         := 3;
   DOC_TL$DOC_DESC           NUMBER         := 4;

   sheet_name_trans    S9T_PKG.TRANS_MAP_TYP;
   TYPE DOC_rec_tab IS TABLE OF DOC%ROWTYPE;
   action_column       VARCHAR2(255)          := 'ACTION';
   template_category   CODE_DETAIL.CODE%TYPE  := 'RMSFND';
--------------------------------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------------------------------
END CORESVC_DOC;
/
