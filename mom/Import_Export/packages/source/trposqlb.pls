CREATE OR REPLACE PACKAGE BODY TRANSPORTATION_PO_SQL AS
----------------------------------------------------------------------------------------------------
AT_TRANSACTION_LEVEL      CONSTANT   CODE_DETAIL.CODE%TYPE := 'T';
ABOVE_TRANSACTION_LEVEL   CONSTANT   CODE_DETAIL.CODE%TYPE := 'A';
----------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_QTY_AND_INVOICE_AMT(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_item_qty          IN OUT TRANSPORTATION.ITEM_QTY%TYPE,
                                      O_invoice_amt       IN OUT TRANSPORTATION.INVOICE_AMT%TYPE,
                                      I_transportation_id IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE,
                                      I_order_no          IN     TRANSPORTATION.ORDER_NO%TYPE,
                                      I_item              IN     TRANSPORTATION.ITEM%TYPE,
                                      I_mode              IN     NUMBER,
                                      I_item_qty          IN     TRANSPORTATION.ITEM_QTY%TYPE DEFAULT 0)

RETURN BOOLEAN IS

   FUNCTION_NAME                   CONSTANT   VARCHAR2(61) := 'TRANSPORTATION_PO_SQL.GET_ITEM_QTY_AND_INVOICE_AMT';

   ROLL_UP_CHILD_RECORDS           CONSTANT   NUMBER(1)    := 1;
   NEW_AT_TRAN_LEVEL               CONSTANT   NUMBER(1)    := 2;
   NEW_ABOVE_TRAN_LEVEL_NO_CHILD   CONSTANT   NUMBER(1)    := 3;
   NEW_ABOVE_TRAN_LEVEL_CHILD      CONSTANT   NUMBER(1)    := 4;
   CALCULATE_PARENT_INVOICE_AMT    CONSTANT   NUMBER(1)    := 5;

   L_invoice_amt          TRANSPORTATION.INVOICE_AMT%TYPE;
   L_item_qty             TRANSPORTATION.ITEM_QTY%TYPE;
   L_olo_item_qty         ORDLOC.QTY_ORDERED%TYPE;
   L_olo_item_amt         ORDLOC.UNIT_COST%TYPE;
   L_olo_parent_item_qty  ORDLOC.QTY_ORDERED%TYPE;
   L_olo_parent_item_amt  ORDLOC.UNIT_COST%TYPE;
   L_tra_amt              TRANSPORTATION.INVOICE_AMT%TYPE;
   L_tra_qty              TRANSPORTATION.ITEM_QTY%TYPE;
   L_trs_order_item_qty   TRANS_SKU.QUANTITY%TYPE;
   L_trs_tranpo_id_amt    ORDLOC.UNIT_COST%TYPE;
   L_trs_tranpo_id_qty    TRANS_SKU.QUANTITY%TYPE;

   cursor C_TRA
       is
   select Nvl(Sum(tra.item_qty),0) qty,
          Nvl(Sum(tra.invoice_amt),0)
                                   amt
     from transportation           tra
    where tra.order_no             = I_order_no
      and tra.item                 = I_item;

   cursor C_TRS_TRANPO_ID
       is
   select Nvl(Sum(trs.quantity),0) qty,
          Nvl(Sum(trs.quantity * uc.unit_cost),0)
                                   amt
     from (select olo.order_no,
                  olo.item,
                  Min(olo.unit_cost) unit_cost
             from ordloc olo
         group by olo.order_no,
                  olo.item)     uc,
          trans_sku             trs
    where trs.transportation_id = I_transportation_id
      and trs.item              = uc.item
      and uc.order_no           = I_order_no;

   cursor C_TRS_ORDER_ITEM
       is
   select Nvl(Sum(trs.quantity),0) qty
     from trans_sku                trs,
          transportation           tra
    where tra.order_no             = I_order_no
      and tra.transportation_id    = trs.transportation_id
      and trs.item                 = I_item;

   cursor C_OLO_ITEM
       is
   select Nvl(Sum(olo.qty_ordered),0) qty,
          Nvl(Sum(olo.qty_ordered * olo.unit_cost),0)
                                      amt
     from ordloc                      olo
    where olo.order_no                = I_order_no
      and olo.item                    = I_item;

   cursor C_OLO_PARENT_ITEM
       is
   select Nvl(Sum(olo.qty_ordered),0) qty,
          Nvl(Sum(olo.qty_ordered * olo.unit_cost),0)
                                      amt
     from ordloc                      olo,
          v_item_master               viem
    where viem.item_parent            = I_item
      and olo.order_no                = I_order_no
      and viem.item                   = olo.item;

BEGIN

   L_item_qty    := 0;
   L_invoice_amt := 0;

   if (I_mode = NEW_AT_TRAN_LEVEL
   or  I_mode = NEW_ABOVE_TRAN_LEVEL_CHILD
   or  I_mode = NEW_ABOVE_TRAN_LEVEL_NO_CHILD) then
      open  C_TRA;
      fetch C_TRA
      into  L_tra_qty,
            L_tra_amt;
      close C_TRA;
   end if;

   if I_mode = ROLL_UP_CHILD_RECORDS then
      open  C_TRS_TRANPO_ID;
      fetch C_TRS_TRANPO_ID
      into  L_trs_tranpo_id_qty,
            L_trs_tranpo_id_amt;
      close C_TRS_TRANPO_ID;
   end if;

   if (I_mode = NEW_ABOVE_TRAN_LEVEL_CHILD
   or  I_mode = NEW_ABOVE_TRAN_LEVEL_NO_CHILD) then
      open  C_TRS_ORDER_ITEM;
      fetch C_TRS_ORDER_ITEM
      into  L_trs_order_item_qty;
      close C_TRS_ORDER_ITEM;
   end if;

   if I_mode = NEW_AT_TRAN_LEVEL then
      open  C_OLO_ITEM;
      fetch C_OLO_ITEM
      into  L_olo_item_qty,
            L_olo_item_amt;
      close C_OLO_ITEM;
   elsif (I_mode = NEW_ABOVE_TRAN_LEVEL_NO_CHILD
   or     I_mode = NEW_ABOVE_TRAN_LEVEL_CHILD
   or     I_mode = CALCULATE_PARENT_INVOICE_AMT) then
      open  C_OLO_PARENT_ITEM;
      fetch C_OLO_PARENT_ITEM
      into  L_olo_parent_item_qty,
            L_olo_parent_item_amt;
      close C_OLO_PARENT_ITEM;
   end if;

   if     I_mode = ROLL_UP_CHILD_RECORDS          then
      L_item_qty    := L_trs_tranpo_id_qty;
      L_invoice_amt := L_trs_tranpo_id_amt;
   elsif  I_mode = NEW_AT_TRAN_LEVEL              then
      L_item_qty    := L_olo_item_qty - L_tra_qty;
      L_invoice_amt := L_olo_item_amt - L_tra_amt;
   elsif (I_mode = NEW_ABOVE_TRAN_LEVEL_CHILD
   or     I_mode = NEW_ABOVE_TRAN_LEVEL_NO_CHILD) then
      L_item_qty    := L_olo_parent_item_qty - L_tra_qty - L_trs_order_item_qty;
      L_invoice_amt := L_olo_parent_item_amt - L_tra_amt;
   elsif  I_mode = CALCULATE_PARENT_INVOICE_AMT then
      L_item_qty    := L_olo_parent_item_qty - L_tra_qty - L_trs_order_item_qty;
      L_invoice_amt := L_olo_parent_item_amt * (I_item_qty / L_olo_parent_item_qty);
   end if;

   if L_item_qty < 0 then
      O_item_qty := 0;
   else
      O_item_qty := Nvl(L_item_qty,0);
   end if;

   if L_invoice_amt < 0 then
      O_invoice_amt := 0;
   else
      O_invoice_amt := Nvl(L_invoice_amt,0);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            FUNCTION_NAME,
                                            To_Char(SQLCODE));
      return FALSE;
END GET_ITEM_QTY_AND_INVOICE_AMT;
-----------------------------------------------------------------------------------------------------
FUNCTION CREATE_CHILD_DIFFS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item                   IN     ITEM_MASTER.ITEM%TYPE,
                            I_order_no               IN     ORDHEAD.ORDER_NO%TYPE,
                            I_transportation_id      IN     TRANS_SKU.TRANSPORTATION_ID%TYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(50) := 'TRANSPORTATION_PO_SQL.CREATE_CHILD_DIFFS';
   L_seq_no                 TRANS_SKU.SEQ_NO%TYPE;
   L_exists                 BOOLEAN;
   L_trs_qty                TRANS_SKU.QUANTITY%TYPE;
   L_child_item             TRANS_SKU.ITEM%TYPE;

   cursor C_PARENT_ITEMS is
      select  viem.standard_uom     standard_uom,
              olo.item,
              Sum(olo.qty_ordered)  qty_ordered
        from  ordloc                olo,
              v_item_master         viem
       where (viem.item_parent      = I_item
           or viem.item_grandparent = I_item)
         and  olo.order_no          = I_order_no
         and  olo.item              = viem.item
    group by  viem.standard_uom,
              olo.item;

   cursor C_TRS
       is
   select Nvl(Sum(trs.quantity),0) qty
     from trans_sku                trs,
          transportation           tra
    where tra.order_no             = I_order_no
      and tra.transportation_id   != I_transportation_id
      and tra.transportation_id    = trs.transportation_id
      and trs.item                 = L_child_item;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   elsif I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                             L_program,
                                             NULL);
      return FALSE;
   elsif I_transportation_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_transportation_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   FOR C_PARENT_ITEMS_REC in C_PARENT_ITEMS LOOP

      if TRANSPORTATION_SQL.DUP_TRANS_ITEM(O_error_message,
                                           L_exists,
                                           I_transportation_id,
                                           c_parent_items_rec.item) = FALSE then
         return FALSE;
      end if;

      if L_exists then
         O_error_message:= SQL_LIB.CREATE_MSG('INV_ITEM_TRANS_ID',
                                              NULL,
                                              NULL,
                                              NULL);
         return FALSE;
      end if;

      L_child_item := c_parent_items_rec.item;

      open  C_TRS;
      fetch C_TRS
      into  L_trs_qty;
      close C_TRS;

      if TRANSPORTATION_SQL.TRANS_SKU_SEQ(O_error_message,
                                          L_seq_no,
                                          I_transportation_id) = FALSE then
         return FALSE;
      end if;

      insert into trans_sku(transportation_id,
                            seq_no,
                            item,
                            quantity,
                            quantity_uom)
                     values(I_transportation_id,
                            L_seq_no,
                            c_parent_items_rec.item,
                            Greatest(c_parent_items_rec.qty_ordered - L_trs_qty,0),
                            c_parent_items_rec.standard_uom);
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_CHILD_DIFFS;
-----------------------------------------------------------------------------------------------------
FUNCTION CREATE_TRANSPORTATION(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no               IN     TRANSPORTATION.ORDER_NO%TYPE,
                               I_vessel_id              IN     TRANSPORTATION.VESSEL_ID%TYPE,
                               I_voyage_flt_id          IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                               I_estimated_depart_date  IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                               I_item_default_level     IN     CODE_DETAIL.CODE%TYPE,
                               I_create_child_diffs     IN     VARCHAR2 DEFAULT NULL)

RETURN BOOLEAN IS

   FUNCTION_NAME            VARCHAR2(50) := 'TRANSPORTATION_PO_SQL.CREATE_TRANSPORTATION';

   L_exists                 BOOLEAN;
   L_transportation_id      TRANSPORTATION.TRANSPORTATION_ID%TYPE;
   L_invoice_amt            TRANSPORTATION.INVOICE_AMT%TYPE;
   L_item_qty               TRANSPORTATION.ITEM_QTY%TYPE;

   cursor C_AT_TL
       is
   select distinct
          vohe.currency_code,
          vohe.exchange_rate,
          osk.item,
          Nvl(osh.origin_country_id,osk.origin_country_id) origin_country_id ,
          viem.standard_uom
     from v_item_master    viem,
          ordsku           osk,
          (select distinct item, 
                           origin_country_id,
                           order_no
                      from ordsku_hts 
                      unpivot(item for col_name in (item, pack_item))) osh,
          v_ordhead        vohe
    where vohe.order_no    = I_order_no
      and vohe.order_no    = osk.order_no
      and osk.order_no     = osh.order_no(+)
      and osk.item         = viem.item
      and osk.item         = osh.item(+)
      and viem.item_level  = viem.tran_level;

   cursor C_ABOVE_TL
       is
   select distinct
          vohe.currency_code,
          vohe.exchange_rate,
          Nvl(osh.origin_country_id,osk.origin_country_id) origin_country_id ,
          Nvl(viem_i.item_parent,viem_i.item)
                             tranpo_item,
          Decode(viem_i.item_parent,
                 NULL,viem_i.standard_uom,
                      viem_ip.standard_uom)
                             standard_uom
     from v_item_master      viem_ip,
          v_item_master      viem_i,
          ordsku             osk,
          ordsku_hts         osh,
          v_ordhead          vohe
    where vohe.order_no      = I_order_no
      and vohe.order_no      = osk.order_no
      and osk.order_no       = osh.order_no(+)
      and osk.item           = viem_i.item
      and osk.item           = osh.item(+)
      and viem_i.item_parent = viem_ip.item(+) -- make sure the parent item is security-ok
-- make sure whichever item is used (item or parent), that the item is Above Transaction Level
      and Decode(viem_i.item_parent,
                 NULL,viem_i.item_level -viem_i.tran_level,
                      viem_ip.item_level-viem_ip.tran_level)
                             < 0
 order by tranpo_item;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_order_no',
                                            'NULL');
      return FALSE;
   end if;

   if I_vessel_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_vessel_id',
                                            'NULL');
      return FALSE;
   end if;

   if I_voyage_flt_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_voyage_flt_id',
                                            'NULL');
      return FALSE;
   end if;

   if I_estimated_depart_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_estimated_depart_date',
                                            'NULL');
      return FALSE;
   end if;

   if I_item_default_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_item_default_level',
                                            'NULL');
      return FALSE;
   end if;

   if (I_create_child_diffs is NULL
   or  I_create_child_diffs in ('Y','N')) then
      NULL;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_create_child_diffs',
                                            Nvl(I_create_child_diffs,'NULL'));
      return FALSE;
   end if;

   if I_item_default_level = AT_TRANSACTION_LEVEL then

      for c_at_tl_row in C_AT_TL loop

         if TRANSPORTATION_PO_SQL.GET_ITEM_QTY_AND_INVOICE_AMT(O_error_message,
                                                               L_item_qty,
                                                               L_invoice_amt,
                                                               NULL,
                                                               I_order_no,
                                                               c_at_tl_row.item,
                                                               2) = FALSE then
            return FALSE;
         end if;
         if (L_item_qty > 0) then
            if TRANSPORTATION_SQL.GET_NEXT_ID(O_error_message,
                                              L_transportation_id) = FALSE then
               return FALSE;
            end if;

            insert into transportation(transportation_id,
                                       vessel_id,
                                       voyage_flt_id,
                                       estimated_depart_date,
                                       order_no,
                                       item,
                                       origin_country_id,
                                       invoice_amt,
                                       currency_code,
                                       exchange_rate,
                                       item_qty,
                                       item_qty_uom,
                                       rush_ind,
                                       candidate_ind)
                                values(L_transportation_id,
                                       I_vessel_id,
                                       I_voyage_flt_id,
                                       I_estimated_depart_date,
                                       I_order_no,
                                       c_at_tl_row.item,
                                       c_at_tl_row.origin_country_id,
                                       L_invoice_amt,
                                       c_at_tl_row.currency_code,
                                       c_at_tl_row.exchange_rate,
                                       L_item_qty,
                                       c_at_tl_row.standard_uom,
                                       'N',
                                       'N');
         end if;
      end loop;

   elsif I_item_default_level = ABOVE_TRANSACTION_LEVEL then

      for c_above_tl_row in C_ABOVE_TL loop

         if I_create_child_diffs = 'Y' then
            L_item_qty    := 0;
            L_invoice_amt := 0;
         else
            if TRANSPORTATION_PO_SQL.GET_ITEM_QTY_AND_INVOICE_AMT(O_error_message,
                                                                  L_item_qty,
                                                                  L_invoice_amt,
                                                                  NULL,
                                                                  I_order_no,
                                                                  c_above_tl_row.tranpo_item,
                                                                  3) = FALSE then
               return FALSE;
            end if;
         end if;

         if TRANSPORTATION_SQL.GET_NEXT_ID(O_error_message,
                                           L_transportation_id) = FALSE then
            return FALSE;
         end if;

         insert into transportation(transportation_id,
                                    vessel_id,
                                    voyage_flt_id,
                                    estimated_depart_date,
                                    order_no,
                                    item,
                                    origin_country_id,
                                    invoice_amt,
                                    currency_code,
                                    exchange_rate,
                                    item_qty,
                                    item_qty_uom,
                                    rush_ind,
                                    candidate_ind)
                            values (L_transportation_id,
                                    I_vessel_id,
                                    I_voyage_flt_id,
                                    I_estimated_depart_date,
                                    I_order_no,
                                    c_above_tl_row.tranpo_item,
                                    c_above_tl_row.origin_country_id,
                                    L_invoice_amt,
                                    c_above_tl_row.currency_code,
                                    c_above_tl_row.exchange_rate,
                                    L_item_qty,
                                    c_above_tl_row.standard_uom,
                                    'N',
                                    'N');

         if I_create_child_diffs = 'Y' then

            if TRANSPORTATION_PO_SQL.CREATE_CHILD_DIFFS(O_error_message,
                                                        c_above_tl_row.tranpo_item,
                                                        I_order_no,
                                                        L_transportation_id) = FALSE then
               return FALSE;
            end if;

            if TRANSPORTATION_PO_SQL.GET_ITEM_QTY_AND_INVOICE_AMT(O_error_message,
                                                                  L_item_qty,
                                                                  L_invoice_amt,
                                                                  L_transportation_id,
                                                                  I_order_no,
                                                                  c_above_tl_row.tranpo_item,
                                                                  1) = FALSE then
               return FALSE;
            end if;

            update transportation        tra
               set tra.item_qty          = L_item_qty,
                   tra.invoice_amt       = L_invoice_amt
             where tra.transportation_id = L_transportation_id;

         end if;

      end loop;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             FUNCTION_NAME,
                                             to_char(SQLCODE));
      return FALSE;

END CREATE_TRANSPORTATION;
-----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_LOCK(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_transportation_id IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE)

RETURN BOOLEAN

IS

   FUNCTION_NAME CONSTANT VARCHAR2(61) := 'TRANSPORTATION_PO_SQL.TRANPO_LOCK';

   record_locked       EXCEPTION;
   pragma              EXCEPTION_INIT(record_locked,-54);

   cursor C_LOCK_TRA
       is
   select 1
     from transportation        tra
    where tra.transportation_id = I_transportation_id
      for update nowait;

BEGIN

   if I_transportation_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_transportation_id',
                                            'NULL');
      return FALSE;
   end if;

   open  C_LOCK_TRA;

   close C_LOCK_TRA;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG ('TABLE_LOCKED',
                                             'TRANSPORTATION',
                                             'I_transportation_id='||I_transportation_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             FUNCTION_NAME,
                                             SQLCODE);
      return FALSE;
END TRANPO_LOCK;
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_INSERT(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tra            IN     TRANSPORTATION%ROWTYPE)

RETURN BOOLEAN IS

   FUNCTION_NAME CONSTANT VARCHAR2(61) := 'TRANSPORTATION_PO_SQL.TRANPO_INSERT';

   L_transportation_id    TRANSPORTATION.TRANSPORTATION_ID%TYPE;

BEGIN

   if I_tra.rush_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_tra.rush_ind',
                                            'NULL');
      return FALSE;
   end if;

   if I_tra.candidate_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_tra.candidate_ind',
                                            'NULL');
      return FALSE;
   end if;

   if TRANSPORTATION_SQL.GET_NEXT_ID(O_error_message,
                                     L_transportation_id) = FALSE then
      return FALSE;
   end if;

   insert into transportation (transportation_id,
                               vessel_id,
                               voyage_flt_id,
                               estimated_depart_date,
                               actual_depart_date,
                               order_no,
                               item,
                               bl_awb_id,
                               container_id,
                               invoice_id,
                               invoice_date,
                               shipment_no,
                               actual_arrival_date,
                               delivery_date,
                               status,
                               tran_mode_id,
                               vessel_scac_code,
                               container_scac_code,
                               seal_id,
                               freight_type,
                               freight_size,
                               origin_country_id,
                               consolidation_country_id,
                               export_country_id,
                               estimated_arrival_date,
                               lading_port,
                               discharge_port,
                               trans_partner_type,
                               trans_partner_id,
                               receipt_id,
                               fcr_id,
                               fcr_date,
                               service_contract_no,
                               in_transit_no,
                               in_transit_date,
                               lot_no,
                               invoice_amt,
                               currency_code,
                               exchange_rate,
                               carton_qty,
                               carton_uom,
                               item_qty,
                               item_qty_uom,
                               gross_wt,
                               gross_wt_uom,
                               net_wt,
                               net_wt_uom,
                               cubic,
                               cubic_uom,
                               packing_method,
                               rush_ind,
                               candidate_ind,
                               comments,
                               selected_ind)
                       values (L_transportation_id,
                               I_tra.vessel_id,
                               I_tra.voyage_flt_id,
                               I_tra.estimated_depart_date,
                               I_tra.actual_depart_date,
                               I_tra.order_no,
                               I_tra.item,
                               I_tra.bl_awb_id,
                               I_tra.container_id,
                               I_tra.invoice_id,
                               I_tra.invoice_date,
                               I_tra.shipment_no,
                               I_tra.actual_arrival_date,
                               I_tra.delivery_date,
                               I_tra.status,
                               I_tra.tran_mode_id,
                               I_tra.vessel_scac_code,
                               I_tra.container_scac_code,
                               I_tra.seal_id,
                               I_tra.freight_type,
                               I_tra.freight_size,
                               I_tra.origin_country_id,
                               I_tra.consolidation_country_id,
                               I_tra.export_country_id,
                               I_tra.estimated_arrival_date,
                               I_tra.lading_port,
                               I_tra.discharge_port,
                               I_tra.trans_partner_type,
                               I_tra.trans_partner_id,
                               I_tra.receipt_id,
                               I_tra.fcr_id,
                               I_tra.fcr_date,
                               I_tra.service_contract_no,
                               I_tra.in_transit_no,
                               I_tra.in_transit_date,
                               I_tra.lot_no,
                               I_tra.invoice_amt,
                               I_tra.currency_code,
                               I_tra.exchange_rate,
                               I_tra.carton_qty,
                               I_tra.carton_uom,
                               I_tra.item_qty,
                               I_tra.item_qty_uom,
                               I_tra.gross_wt,
                               I_tra.gross_wt_uom,
                               I_tra.net_wt,
                               I_tra.net_wt_uom,
                               I_tra.cubic,
                               I_tra.cubic_uom,
                               I_tra.packing_method,
                               I_tra.rush_ind,
                               I_tra.candidate_ind,
                               I_tra.comments,
                               I_tra.selected_ind);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            FUNCTION_NAME,
                                            To_Char(SQLCODE));
      return FALSE;
END TRANPO_INSERT;
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_ITEM_UPDATE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tra            IN     TRANSPORTATION%ROWTYPE)

RETURN BOOLEAN IS

   FUNCTION_NAME CONSTANT VARCHAR2(61) := 'TRANSPORTATION_PO_SQL.TRANPO_ITEM_UPDATE';

   record_locked          EXCEPTION;
   pragma                 EXCEPTION_INIT(record_locked,-54);

   cursor C_LOCK_TRA
       is
   select 1
     from transportation        tra
    where tra.transportation_id = I_tra.transportation_id
      for update nowait;

BEGIN

   if I_tra.transportation_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_tra.transportation_id',
                                            'NULL');
      return FALSE;
   end if;

   open  C_LOCK_TRA;

   close C_LOCK_TRA;

   update transportation        tra
      set tra.bl_awb_id         = Decode(tra.bl_awb_id   ,NULL,I_tra.bl_awb_id   ,tra.bl_awb_id),
          tra.container_id      = Decode(tra.container_id,NULL,I_tra.container_id,tra.container_id),
          tra.invoice_id        = Decode(tra.invoice_id  ,NULL,I_tra.invoice_id  ,tra.invoice_id),
          tra.invoice_date      = I_tra.invoice_date,
          tra.invoice_amt       = I_tra.invoice_amt,
          tra.carton_qty        = I_tra.carton_qty,
          tra.carton_uom        = I_tra.carton_uom,
          tra.item_qty          = I_tra.item_qty,
          tra.item_qty_uom      = I_tra.item_qty_uom,
          tra.gross_wt          = I_tra.gross_wt,
          tra.gross_wt_uom      = I_tra.gross_wt_uom,
          tra.net_wt            = I_tra.net_wt,
          tra.net_wt_uom        = I_tra.net_wt_uom,
          tra.cubic             = I_tra.cubic,
          tra.cubic_uom         = I_tra.cubic_uom
    where tra.transportation_id = I_tra.transportation_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'TRANSPORTATION',
                                            'I_tra.transportation_id='||I_tra.transportation_id,
                                            'NULL');
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            FUNCTION_NAME,
                                            To_Char(SQLCODE));
      return FALSE;
END TRANPO_ITEM_UPDATE;
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_MASS_UPDATE(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cb_x_status                 IN     VARCHAR2,
                            I_cb_x_container_id           IN     VARCHAR2,
                            I_cb_x_bl_awb_id              IN     VARCHAR2,
                            I_cb_x_invoice_id             IN     VARCHAR2,
                            I_cb_x_invoice_date           IN     VARCHAR2,
                            I_cb_x_actual_depart_date     IN     VARCHAR2,
                            I_cb_x_estimated_arrival_date IN     VARCHAR2,
                            I_cb_x_actual_arrival_date    IN     VARCHAR2,
                            I_cb_x_delivery_date          IN     VARCHAR2,
                            I_cb_x_vessel_scac_code       IN     VARCHAR2,
                            I_cb_x_tran_mode_id           IN     VARCHAR2,
                            I_cb_x_freight_type           IN     VARCHAR2,
                            I_cb_x_trans_partner_type     IN     VARCHAR2,
                            I_cb_x_trans_partner_id       IN     VARCHAR2,
                            I_cb_x_consolidation_cntry_id IN     VARCHAR2,
                            I_cb_x_origin_country_id      IN     VARCHAR2,
                            I_cb_x_export_country_id      IN     VARCHAR2,
                            I_cb_x_lading_port            IN     VARCHAR2,
                            I_cb_x_discharge_port         IN     VARCHAR2,
                            I_cb_x_fcr_id                 IN     VARCHAR2,
                            I_cb_x_fcr_date               IN     VARCHAR2,
                            I_cb_x_shipment_no            IN     VARCHAR2,
                            I_cb_x_receipt_id             IN     VARCHAR2,
                            I_cb_x_seal_id                IN     VARCHAR2,
                            I_cb_x_container_scac_code    IN     VARCHAR2,
                            I_cb_x_freight_size           IN     VARCHAR2,
                            I_cb_x_packing_method         IN     VARCHAR2,
                            I_cb_x_service_contract_no    IN     VARCHAR2,
                            I_cb_x_in_transit_no          IN     VARCHAR2,
                            I_cb_x_in_transit_date        IN     VARCHAR2,
                            I_cb_x_lot_no                 IN     VARCHAR2,
                            I_cb_x_rush_ind               IN     VARCHAR2,
                            I_cb_x_candidate_ind          IN     VARCHAR2,
                            I_cb_x_comments               IN     VARCHAR2,
                            I_tra                         IN     TRANSPORTATION%ROWTYPE)

RETURN BOOLEAN IS

   FUNCTION_NAME CONSTANT VARCHAR2(61) := 'TRANSPORTATION_PO_SQL.TRANPO_MASS_UPDATE';

   record_locked          EXCEPTION;
   pragma                 EXCEPTION_INIT(record_locked,-54);

   cursor C_LOCK_TRA
       is
   select 1
     from transportation            tra
    where tra.order_no              = I_tra.order_no
      and tra.vessel_id             = I_tra.vessel_id
      and tra.voyage_flt_id         = I_tra.voyage_flt_id
      and tra.estimated_depart_date = I_tra.estimated_depart_date
      and tra.selected_ind          = 'Y'
      for update nowait;

BEGIN

   if I_tra.order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_tra.order_no',
                                            'NULL');
      return FALSE;
   end if;

   if I_tra.vessel_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_tra.vessel_id',
                                            'NULL');
      return FALSE;
   end if;

   if I_tra.voyage_flt_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_tra.voyage_flt_id',
                                            'NULL');
      return FALSE;
   end if;

   if I_tra.estimated_depart_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_tra.estimated_depart_date',
                                            'NULL');
      return FALSE;
   end if;

   if I_cb_x_rush_ind = 'Y' then
      if I_tra.rush_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               FUNCTION_NAME,
                                               'I_tra.rush_ind',
                                               'NULL');
         return FALSE;
      end if;
   end if;

   if I_cb_x_candidate_ind = 'Y' then
      if I_tra.candidate_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               FUNCTION_NAME,
                                               'I_tra.candidate_ind',
                                               'NULL');
         return FALSE;
      end if;
   end if;

   open  C_LOCK_TRA;

   close C_LOCK_TRA;

   update transportation               tra
      set tra.actual_depart_date       = Decode(I_cb_x_actual_depart_date    ,'Y',I_tra.actual_depart_date      ,tra.actual_depart_date),
          tra.bl_awb_id                = Decode(tra.bl_awb_id,
                                                NULL,Decode(I_cb_x_bl_awb_id,
                                                            'Y',I_tra.bl_awb_id,
                                                            tra.bl_awb_id),
                                                tra.bl_awb_id),
          tra.container_id             = Decode(tra.container_id,
                                                NULL,Decode(I_cb_x_container_id,
                                                            'Y',I_tra.container_id,
                                                            tra.container_id),
                                                tra.container_id),
          tra.invoice_id               = Decode(tra.invoice_id,
                                                NULL,Decode(I_cb_x_invoice_id,
                                                            'Y',I_tra.invoice_id,
                                                            tra.invoice_id),
                                                tra.invoice_id),
          tra.invoice_date             = Decode(I_cb_x_invoice_date          ,'Y',I_tra.invoice_date            ,tra.invoice_date),
          tra.shipment_no              = Decode(I_cb_x_shipment_no           ,'Y',I_tra.shipment_no             ,tra.shipment_no),
          tra.actual_arrival_date      = Decode(I_cb_x_actual_arrival_date   ,'Y',I_tra.actual_arrival_date     ,tra.actual_arrival_date),
          tra.delivery_date            = Decode(I_cb_x_delivery_date         ,'Y',I_tra.delivery_date           ,tra.delivery_date),
          tra.status                   = Decode(I_cb_x_status                ,'Y',I_tra.status                  ,tra.status),
          tra.tran_mode_id             = Decode(I_cb_x_tran_mode_id          ,'Y',I_tra.tran_mode_id            ,tra.tran_mode_id),
          tra.vessel_scac_code         = Decode(I_cb_x_vessel_scac_code      ,'Y',I_tra.vessel_scac_code        ,tra.vessel_scac_code),
          tra.container_scac_code      = Decode(I_cb_x_container_scac_code   ,'Y',I_tra.container_scac_code     ,tra.container_scac_code),
          tra.seal_id                  = Decode(I_cb_x_seal_id               ,'Y',I_tra.seal_id                 ,tra.seal_id),
          tra.freight_type             = Decode(I_cb_x_freight_type          ,'Y',I_tra.freight_type            ,tra.freight_type),
          tra.freight_size             = Decode(I_cb_x_freight_size          ,'Y',I_tra.freight_size            ,tra.freight_size),
          tra.origin_country_id        = Decode(I_cb_x_origin_country_id     ,'Y',I_tra.origin_country_id       ,tra.origin_country_id),
          tra.consolidation_country_id = Decode(I_cb_x_consolidation_cntry_id,'Y',I_tra.consolidation_country_id,tra.consolidation_country_id),
          tra.export_country_id        = Decode(I_cb_x_export_country_id     ,'Y',I_tra.export_country_id       ,tra.export_country_id),
          tra.estimated_arrival_date   = Decode(I_cb_x_estimated_arrival_date,'Y',I_tra.estimated_arrival_date  ,tra.estimated_arrival_date),
          tra.lading_port              = Decode(I_cb_x_lading_port           ,'Y',I_tra.lading_port             ,tra.lading_port),
          tra.discharge_port           = Decode(I_cb_x_discharge_port        ,'Y',I_tra.discharge_port          ,tra.discharge_port),
          tra.trans_partner_type       = Decode(I_cb_x_trans_partner_type    ,'Y',I_tra.trans_partner_type      ,tra.trans_partner_type),
          tra.trans_partner_id         = Decode(I_cb_x_trans_partner_id      ,'Y',I_tra.trans_partner_id        ,tra.trans_partner_id),
          tra.receipt_id               = Decode(I_cb_x_receipt_id            ,'Y',I_tra.receipt_id              ,tra.receipt_id),
          tra.fcr_id                   = Decode(I_cb_x_fcr_id                ,'Y',I_tra.fcr_id                  ,tra.fcr_id),
          tra.fcr_date                 = Decode(I_cb_x_fcr_date              ,'Y',I_tra.fcr_date                ,tra.fcr_date),
          tra.service_contract_no      = Decode(I_cb_x_service_contract_no   ,'Y',I_tra.service_contract_no     ,tra.service_contract_no),
          tra.in_transit_no            = Decode(I_cb_x_in_transit_no         ,'Y',I_tra.in_transit_no           ,tra.in_transit_no),
          tra.in_transit_date          = Decode(I_cb_x_in_transit_date       ,'Y',I_tra.in_transit_date         ,tra.in_transit_date),
          tra.lot_no                   = Decode(I_cb_x_lot_no                ,'Y',I_tra.lot_no                  ,tra.lot_no),
          tra.packing_method           = Decode(I_cb_x_packing_method        ,'Y',I_tra.packing_method          ,tra.packing_method),
          tra.rush_ind                 = Decode(I_cb_x_rush_ind              ,'Y',I_tra.rush_ind                ,tra.rush_ind),
          tra.candidate_ind            = Decode(I_cb_x_candidate_ind         ,'Y',I_tra.candidate_ind           ,tra.candidate_ind),
          tra.comments                 = Decode(I_cb_x_comments              ,'Y',I_tra.comments                ,tra.comments)
    where tra.order_no                 = I_tra.order_no
      and tra.vessel_id                = I_tra.vessel_id
      and tra.voyage_flt_id            = I_tra.voyage_flt_id
      and tra.estimated_depart_date    = I_tra.estimated_depart_date
      and tra.selected_ind             = 'Y';

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'TRANSPORTATION',
                                            'I_tra.order_no='             ||I_tra.order_no     ||
                                            ', I_tra.vessel_id='          ||I_tra.vessel_id    ||
                                            ', I_tra.voyage_flt_id='      ||I_tra.voyage_flt_id,
                                            'I_tra.estimated_depart_date='||I_tra.estimated_depart_date);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            FUNCTION_NAME,
                                            To_Char(SQLCODE));
      return FALSE;
END TRANPO_MASS_UPDATE;
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_SELECTION(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_transportation_id     IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE,
                          I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                          I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                          I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                          I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                          I_selected_ind          IN     TRANSPORTATION.SELECTED_IND%TYPE)

RETURN BOOLEAN IS

   FUNCTION_NAME CONSTANT VARCHAR2(61) := 'TRANSPORTATION_PO_SQL.TRANPO_SELECTION';

   record_locked          EXCEPTION;
   pragma                 EXCEPTION_INIT(record_locked,-54);

   cursor C_LOCK_TRA_KEY_ITEMS
       is
   select 1
     from transportation            tra
    where tra.order_no              = I_order_no
      and tra.vessel_id             = I_vessel_id
      and tra.voyage_flt_id         = I_voyage_flt_id
      and tra.estimated_depart_date = I_estimated_depart_date
      and tra.selected_ind          = 'Y'
      for update nowait;

   cursor C_LOCK_TRA_ID
       is
   select 1
     from transportation            tra
    where tra.transportation_id     = I_transportation_id
      for update nowait;

BEGIN

   if I_transportation_id is NULL then
      if I_order_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               FUNCTION_NAME,
                                               'I_order_no',
                                               'NULL');
         return FALSE;
      end if;
      if I_vessel_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               FUNCTION_NAME,
                                               'I_vessel_id',
                                               'NULL');
         return FALSE;
      end if;
      if I_voyage_flt_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               FUNCTION_NAME,
                                               'I_voyage_flt_id',
                                               'NULL');
         return FALSE;
      end if;
      if I_estimated_depart_date is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               FUNCTION_NAME,
                                               'I_estimated_depart_date',
                                               'NULL');
         return FALSE;
      end if;
   else
      if I_order_no is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               FUNCTION_NAME,
                                               'I_order_no',
                                               I_order_no);
         return FALSE;
      end if;
      if I_vessel_id is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               FUNCTION_NAME,
                                               'I_vessel_id',
                                               I_vessel_id);
         return FALSE;
      end if;
      if I_voyage_flt_id is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               FUNCTION_NAME,
                                               'I_voyage_flt_id',
                                               I_voyage_flt_id);
         return FALSE;
      end if;
      if I_estimated_depart_date is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               FUNCTION_NAME,
                                               'I_estimated_depart_date',
                                               I_estimated_depart_date);
         return FALSE;
      end if;
   end if;

   if I_transportation_id is NULL then

      open  C_LOCK_TRA_KEY_ITEMS;

      close C_LOCK_TRA_KEY_ITEMS;

      update transportation            tra
         set tra.selected_ind          = I_selected_ind
       where tra.order_no              = I_order_no
         and tra.vessel_id             = I_vessel_id
         and tra.voyage_flt_id         = I_voyage_flt_id
         and tra.estimated_depart_date = I_estimated_depart_date;

   else

      open  C_LOCK_TRA_ID;

      close C_LOCK_TRA_ID;

      update transportation            tra
         set tra.selected_ind          = I_selected_ind
       where tra.transportation_id     = I_transportation_id;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'TRANSPORTATION',
                                            'I_order_no='             ||I_order_no     ||
                                            ', I_vessel_id='          ||I_vessel_id    ||
                                            ', I_voyage_flt_id='      ||I_voyage_flt_id,
                                            'I_estimated_depart_date='||I_estimated_depart_date);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            FUNCTION_NAME,
                                            To_Char(SQLCODE));
      return FALSE;
END TRANPO_SELECTION;
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_DELETE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_transportation_id IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE)

RETURN BOOLEAN IS

   FUNCTION_NAME CONSTANT VARCHAR2(61) := 'TRANSPORTATION_PO_SQL.TRANPO_DELETE';

   record_locked             EXCEPTION;
   pragma                    EXCEPTION_INIT(record_locked,-54);
   L_return_code             BOOLEAN;
   L_vessel_id               TRANSPORTATION.VESSEL_ID%TYPE;
   L_voyage_flt_id           TRANSPORTATION.VOYAGE_FLT_ID%TYPE;
   L_estimated_depart_date   TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE;
   L_order_no                TRANSPORTATION.ORDER_NO%TYPE;
   L_item                    TRANSPORTATION.ITEM%TYPE;
   L_container_id            TRANSPORTATION.CONTAINER_ID%TYPE;
   L_bl_awb_id               TRANSPORTATION.BL_AWB_ID%TYPE;
   L_invoice_id              TRANSPORTATION.INVOICE_ID%TYPE;
   L_status                  TRANSPORTATION.STATUS%TYPE;

   cursor C_LOCK_TRA_ID
       is
   select 1
     from trans_lic_visa        tlv,
          trans_packing         trp,
          trans_delivery        trd,
          trans_claims          trc,
          trans_sku             trs,
          missing_doc           mdc,
          transportation        tra
    where tra.transportation_id = I_transportation_id
      and tra.transportation_id = mdc.transportation_id(+)
      and tra.transportation_id = trs.transportation_id(+)
      and tra.transportation_id = trc.transportation_id(+)
      and tra.transportation_id = trd.transportation_id(+)
      and tra.transportation_id = trp.transportation_id(+)
      and tra.transportation_id = tlv.transportation_id(+)
      for update nowait;

   cursor C_LOCK_TIMELINE1 is
      select 'Y'
        from timeline
       where timeline_type in ('TRPI','TRPOBL')
         and key_value_1 = to_char(L_order_no)
         and key_value_2 = decode(timeline_type,'TRPI',L_item,'TRPOBL',L_bl_awb_id)
         for update nowait;

   cursor C_LOCK_TIMELINE2 is
      select 'Y'
        from timeline
       where timeline_type in ('TR','TRBL','TRCO','TRCI')
         and key_value_1 = decode(timeline_type,'TR',to_char(I_transportation_id),
                                                'TRBL',L_bl_awb_id,
                                                'TRCO',L_container_id,
                                                'TRCI',L_invoice_id)
         and key_value_2 is NULL
         for update nowait;

   cursor C_LOCK_TRAN_SHIPMENT is
      select 'Y'
        from transportation_shipment ts
       where exists(select 'Y'
                      from transportation t
                     where t.vessel_id = ts.vessel_id
                       and t.voyage_flt_id   = ts.voyage_flt_id
                       and t.estimated_depart_date = ts.estimated_depart_date
                       and t.order_no = ts.order_no
                       and t.transportation_id = I_transportation_id)
         and not exists(select 'Y'
                          from transportation t
                         where t.vessel_id = ts.vessel_id
                           and t.voyage_flt_id   = ts.voyage_flt_id
                           and t.estimated_depart_date = ts.estimated_depart_date
                           and t.order_no = ts.order_no
                           and t.transportation_id != I_transportation_id)
         for update nowait;

BEGIN

   if I_transportation_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_transportation_id',
                                            'NULL');
      return FALSE;
   end if;

   if TRANSPORTATION_SQL.GET_MAIN_INFO(O_error_message,
                                       L_return_code,
                                       L_vessel_id,
                                       L_voyage_flt_id,
                                       L_estimated_depart_date,
                                       L_order_no,
                                       L_item,
                                       L_container_id,
                                       L_bl_awb_id,
                                       L_invoice_id,
                                       L_status,
                                       I_transportation_id) = FALSE then
      return FALSE;
   end if;

   open C_LOCK_TIMELINE1;
   close C_LOCK_TIMELINE1;

   delete from timeline tl
    where tl.timeline_type in ('TRPI','TRPOBL')
      and tl.key_value_1 = to_char(L_order_no)
      and tl.key_value_2 = decode(tl.timeline_type,'TRPI',L_item,L_bl_awb_id)
      and not exists(select 'Y'
                       from transportation t
                      where t.order_no = L_order_no
                        and decode(tl.timeline_type,'TRPI',t.item,t.bl_awb_id) = decode(tl.timeline_type,'TRPI',L_item,L_bl_awb_id)
                        and t.transportation_id != I_transportation_id);

   open C_LOCK_TIMELINE2;
   close C_LOCK_TIMELINE2;

   delete from timeline t2
    where t2.timeline_type in ('TR','TRBL','TRCO','TRCI')
      and t2.key_value_1 = decode(t2.timeline_type,'TR',to_char(I_transportation_id),    
                                                   'TRBL',L_bl_awb_id,
                                                   'TRCO',L_container_id,
                                                    L_invoice_id)
      and t2.key_value_2 is NULL
      and not exists(select 'Y' 
                       from transportation t
                      where decode(t2.timeline_type,'TR',to_char(t.transportation_id),
                                                    'TRBL',t.bl_awb_id,
                                                    'TRCO',t.container_id,
                                                    t.invoice_id) = decode(t2.timeline_type,'TR',to_char(I_transportation_id),
                                                                                            'TRBL',L_bl_awb_id,
                                                                                            'TRCO',L_container_id,
                                                                                            L_invoice_id)
                        and t.transportation_id != I_transportation_id);

   open C_LOCK_TRAN_SHIPMENT;
   close C_LOCK_TRAN_SHIPMENT;

   delete from transportation_shipment ts
    where exists(select 'Y'
                   from transportation t
                  where t.vessel_id = ts.vessel_id
                    and t.voyage_flt_id   = ts.voyage_flt_id
                    and t.estimated_depart_date = ts.estimated_depart_date
                    and t.order_no = ts.order_no
                    and t.transportation_id = I_transportation_id)
      and not exists(select 'Y'
                      from transportation t
                     where t.vessel_id = ts.vessel_id
                       and t.voyage_flt_id   = ts.voyage_flt_id
                       and t.estimated_depart_date = ts.estimated_depart_date
                       and t.order_no = ts.order_no
                       and t.transportation_id != I_transportation_id);

   open  C_LOCK_TRA_ID;

   close C_LOCK_TRA_ID;

   delete
     from trans_lic_visa        tlv
    where tlv.transportation_id = I_transportation_id;

   delete
     from trans_packing         trp
    where trp.transportation_id = I_transportation_id;

   delete
     from trans_delivery        trd
    where trd.transportation_id = I_transportation_id;

   delete
     from trans_claims          trc
    where trc.transportation_id = I_transportation_id;

   delete
     from trans_sku             trs
    where trs.transportation_id = I_transportation_id;

   delete
     from missing_doc           mdc
    where mdc.transportation_id = I_transportation_id;

   delete
     from transportation        tra
    where tra.transportation_id = I_transportation_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'TRANSPORTATION',
                                            'I_transportation_id='||I_transportation_id,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            FUNCTION_NAME,
                                            To_Char(SQLCODE));
      return FALSE;
END TRANPO_DELETE;
----------------------------------------------------------------------------------------------------
FUNCTION TRANS_SKU_EXISTS(O_error_message     IN OUT VARCHAR2,
                          O_exists            IN OUT BOOLEAN,
                          I_transportation_id IN     TRANS_SKU.TRANSPORTATION_ID%TYPE)

RETURN BOOLEAN IS

   FUNCTION_NAME   CONSTANT   VARCHAR2(61) := 'TRANSPORTATION_PO_SQL.TRANS_SKU_EXISTS';

   cursor   C_TRS
       is
   select   1
     from   trans_sku             trs
    where   trs.transportation_id = I_transportation_id;

   c_trs_row   C_TRS%ROWTYPE;

BEGIN

   if I_transportation_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_PARM_PROG',
                                             FUNCTION_NAME,
                                             'I_transportation_id',
                                             'NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK ('OPEN',
                     'C_TRS',
                     'TRANS_SKU',
                     'I_transportation_id='||I_transportation_id);

   open  C_TRS;

   SQL_LIB.SET_MARK ('FETCH',
                     'C_TRS',
                     'TRANS_SKU',
                     'I_transportation_id='||I_transportation_id);

   fetch C_TRS
    into c_trs_row;

   O_exists := C_TRS%FOUND;

   SQL_LIB.SET_MARK ('CLOSE',
                     'C_TRS',
                     'TRANS_SKU',
                     'I_transportation_id='||I_transportation_id);

   close C_TRS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             FUNCTION_NAME,
                                             SQLCODE);
      return FALSE;
END TRANS_SKU_EXISTS;
----------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TRANPO_ITEM(O_error_message         IN OUT VARCHAR2,
                              O_valid                 IN OUT BOOLEAN,
                              O_v_item_master         IN OUT V_ITEM_MASTER%ROWTYPE,
                              I_item                  IN     TRANSPORTATION.ITEM%TYPE,
                              I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                              I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                              I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                              I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                              I_item_default_level    IN     VARCHAR2)

RETURN BOOLEAN IS

   FUNCTION_NAME   CONSTANT   VARCHAR2(61) := 'TRANSPORTATION_PO_SQL.VALIDATE_TRANPO_ITEM';

   cursor C_ITEM
       is
   select viem.*
     from v_item_master        viem,
          ordsku               osk
    where osk.order_no         = I_order_no
      and osk.item             = I_item
      and osk.item             = viem.item
      and viem.status          = 'A'
      and I_item_default_level = 'T'
union all
   select viem_ip.*
     from v_item_master        viem_ip,
          v_item_master        viem_i,
          ordsku               osk
    where osk.order_no         = I_order_no
      and osk.item             = viem_i.item
      and viem_i.status        = 'A'
      and viem_i.item_parent   = viem_ip.item
      and viem_ip.status       = 'A'
      and viem_ip.item         = I_item
      and I_item_default_level = 'A';

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_item',
                                            'NULL');
      return FALSE;
   end if;

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_order_no',
                                            'NULL');
      return FALSE;
   end if;

   if I_vessel_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_vessel_id',
                                            'NULL');
      return FALSE;
   end if;

   if I_voyage_flt_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_voyage_flt_id',
                                            'NULL');
      return FALSE;
   end if;

   if I_estimated_depart_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_estimated_depart_date',
                                            'NULL');
      return FALSE;
   end if;

   if I_item_default_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_item_default_level',
                                            'NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK ('OPEN',
                     'C_ITEM',
                     'V_ITEM_MASTER/ORDSKU/V_TRANSPORTATION',
                     'I_item='                  ||I_item||
                     '/I_order_no='             ||I_order_no);

   open  C_ITEM;

   SQL_LIB.SET_MARK ('FETCH',
                     'C_ITEM',
                     'V_ITEM_MASTER/ORDSKU/V_TRANSPORTATION',
                     'I_item='                  ||I_item||
                     '/I_order_no='             ||I_order_no);

   fetch C_ITEM
    into O_v_item_master;

   O_valid := C_ITEM%FOUND;

   SQL_LIB.SET_MARK ('CLOSE',
                     'C_ITEM',
                     'V_ITEM_MASTER/ORDSKU/V_TRANSPORTATION',
                     'I_item='                  ||I_item||
                     '/I_order_no='             ||I_order_no);

   close C_ITEM;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             FUNCTION_NAME,
                                             SQLCODE);
      return FALSE;
END VALIDATE_TRANPO_ITEM;
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_VALIDATE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN     TRANSPORTATION.ORDER_NO%TYPE)

RETURN BOOLEAN IS
   L_program_name            VARCHAR2(100) := 'TRANSPORTATION_PO_SQL.TRANPO_VALIDATE';
   L_obl_exists              VARCHAR2(1);
   L_ce_exists               VARCHAR2(1);

   cursor C_VALIDATE_OBL is
      select 'x'
        from obligation
       where to_char(I_order_no) = key_value_1
         and rownum = 1;

   cursor C_VALIDATE_CE is
      select 'x'
        from ce_ord_item
       where I_order_no = order_no
         and rownum = 1;

BEGIN

   SQL_LIB.SET_MARK ('OPEN',
                     'C_VALIDATE_OBL',
                     'OBLIGATION',
                     'I_order_no ='||I_order_no);

   open  C_VALIDATE_OBL;

   SQL_LIB.SET_MARK ('FETCH',
                     'C_VALIDATE_OBL',
                     'OBLIGATION',
                     'I_order_no ='||I_order_no);

   fetch C_VALIDATE_OBL into L_obl_exists;

   SQL_LIB.SET_MARK ('CLOSE',
                     'C_VALIDATE_OBL',
                     'OBLIGATION',
                     'I_order_no ='||I_order_no);

   close C_VALIDATE_OBL;

   if L_obl_exists is NOT NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('OBL_TRANPO_EXISTS',
                                           NULL,
                                           NULL,
                                           NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK ('OPEN',
                     'C_VALIDATE_CE',
                     'CE_ORD_ITEM',
                     'I_order_no ='||I_order_no);

   open  C_VALIDATE_CE;

   SQL_LIB.SET_MARK ('FETCH',
                     'C_VALIDATE_CE',
                     'CE_ORD_ITEM',
                     'I_order_no ='||I_order_no);

   fetch C_VALIDATE_CE into L_ce_exists;

   SQL_LIB.SET_MARK ('CLOSE',
                     'C_VALIDATE_CE',
                     'CE_ORD_ITEM',
                     'I_order_no ='||I_order_no);

   close C_VALIDATE_CE;

   if L_ce_exists is NOT NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('CE_TRANPO_EXISTS',
                                           NULL,
                                           NULL,
                                           NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END TRANPO_VALIDATE;
----------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_APPLY_REC(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid                   IN OUT   BOOLEAN,
                            I_item                    IN       TRANSPORTATION.ITEM%TYPE,
                            I_container_id            IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                            I_bl_awb_id               IN       TRANSPORTATION.BL_AWB_ID%TYPE,
                            I_invoice_id              IN       TRANSPORTATION.INVOICE_ID%TYPE,
                            I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE,
                            I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                            I_voyage_flt_id           IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                            I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                            I_item_default_level      IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program_name   VARCHAR2(100) := 'TRANSPORTATION_PO_SQL.VALIDATE_APPLY_REC';
   L_dummy          VARCHAR2(1);
   L_found          BOOLEAN;

   cursor C_VALID_REC is
      select 'Y'
        from v_item_master viem
       where viem.item            = I_item
         and viem.status          = 'A'
         and I_item_default_level = 'T'
         and not exists (select 'Y'
                           from v_transportation vtra
                          where vtra.order_no              = I_order_no
                            and vtra.vessel_id             = I_vessel_id
                            and vtra.voyage_flt_id         = I_voyage_flt_id
                            and vtra.estimated_depart_date = I_estimated_depart_date
                            and vtra.item                  = viem.item
                            and ((vtra.container_id        = I_container_id
                                  and vtra.bl_awb_id       = I_bl_awb_id
                                  and vtra.invoice_id      = I_invoice_id)
                                      or (vtra.container_id is NULL
                                          or vtra.bl_awb_id is NULL
                                          or vtra.invoice_id is NULL))
                            and rownum = 1)
         and rownum = 1
      UNION ALL
      select 'Y'
        from v_item_master viem_ip,
             v_item_master viem_i
       where viem_i.status        = 'A'
         and viem_i.item_parent   = viem_ip.item
         and viem_ip.status       = 'A'
         and viem_ip.item         = I_item
         and not exists (select 'Y'
                           from v_transportation vtra
                          where vtra.order_no              = I_order_no
                            and vtra.vessel_id             = I_vessel_id
                            and vtra.voyage_flt_id         = I_voyage_flt_id
                            and vtra.estimated_depart_date = I_estimated_depart_date
                            and vtra.item                  = viem_ip.item
                            and ((vtra.container_id        = I_container_id
                                  and vtra.bl_awb_id       = I_bl_awb_id
                                  and vtra.invoice_id      = I_invoice_id)
                                      or (vtra.container_id is NULL
                                          or vtra.bl_awb_id is NULL
                                          or vtra.invoice_id is NULL))
                            and rownum = 1)
         and rownum = 1;

BEGIN

   SQL_LIB.SET_MARK ('OPEN',
                     'C_VALID_REC',
                     'V_ITEM_MASTER/V_TRANSPORTATION',
                     'I_item='                  ||I_item||
                     '/I_order_no='             ||I_order_no);

   open C_VALID_REC;

   SQL_LIB.SET_MARK ('FETCH',
                     'C_VALID_REC',
                     'V_ITEM_MASTER/V_TRANSPORTATION',
                     'I_item='                  ||I_item||
                     '/I_order_no='             ||I_order_no);

   fetch C_VALID_REC into L_dummy;

   SQL_LIB.SET_MARK ('CLOSE',
                     'C_VALID_REC',
                     'V_ITEM_MASTER/V_TRANSPORTATION',
                     'I_item='                  ||I_item||
                     '/I_order_no='             ||I_order_no);

   close C_VALID_REC;

   if L_dummy is NOT NULL then
      L_found := TRUE;
   else
      L_found := FALSE;
   end if;

   return L_found;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             SQLCODE);
      return FALSE;
END VALIDATE_APPLY_REC;
----------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_VVE_SHIP(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_status                  IN OUT   VARCHAR2,
                           I_shipment                IN       SHIPMENT.SHIPMENT%TYPE,
                           I_asn                     IN       SHIPMENT.ASN%TYPE,
                           I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE,
                           I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                           I_voyage_flt_id           IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                           I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                           I_add_remove_ind          IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program_name   VARCHAR2(100) := 'TRANSPORTATION_PO_SQL.VALIDATE_VVE_SHIP';
   L_shipment     SHIPMENT.SHIPMENT%TYPE;
   L_order_no     TRANSPORTATION.ORDER_NO%TYPE;

   CURSOR C_MISSING_PO is
      select s.order_no
        from shipment s
       where asn = I_asn
         and s.order_no != I_order_no
         and not exists (select 'x' 
                           from transportation t
                          where t.order_no = s.order_no
                            and t.vessel_id = I_vessel_id
                            and t.voyage_flt_id = I_voyage_flt_id
                            and t.estimated_depart_date = I_estimated_depart_date);

   CURSOR C_SPLIT_ASN is
      select shipment, order_no
        from transportation_shipment
       where asn = I_asn
         and (vessel_id != I_vessel_id
              or voyage_flt_id != I_voyage_flt_id
              or estimated_depart_date != I_estimated_depart_date);

   CURSOR C_CHECK_EXISTING is
      select shipment
        from transportation_shipment
       where shipment = I_shipment
         and (vessel_id != I_vessel_id
              or voyage_flt_id != I_voyage_flt_id
              or estimated_depart_date != I_estimated_depart_date);
BEGIN
   open c_check_existing;
   fetch c_check_existing into L_shipment;
   close c_check_existing;
   if L_shipment is not null then
      O_error_message := SQL_LIB.CREATE_MSG ('?DIFF_SHIP_VVE',
                                             L_shipment,
                                             NULL,
                                             NULL);
      O_status := '?';
      return true;
   end if;
   if I_add_remove_ind = 'A' then
      open c_missing_po;
      fetch c_missing_po into L_order_no;
      close c_missing_po;
      if L_order_no is not null then 
         O_error_message := SQL_LIB.CREATE_MSG ('?VVE_MISS_ASN_PO',
                                                I_asn,
                                                L_order_no,
                                                NULL);
         O_status := '?';
         return true;
      end if;
      open c_split_asn;
      fetch c_split_asn into L_shipment, L_order_no;
      close c_split_asn;
      if L_order_no is not null then
         O_error_message := SQL_LIB.CREATE_MSG ('?VVE_SPLIT_ASN',
                                                I_asn,
                                                L_shipment,
                                                L_order_no);
         O_status := '?';
         return true;
      end if;
   end if;
   O_status := 'S';
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             SQLCODE);
      return FALSE;
END VALIDATE_VVE_SHIP;
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_MASS_UPDATE(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cb_x_status                   IN       VARCHAR2,
                            I_cb_x_actual_depart_date       IN       VARCHAR2,
                            I_cb_x_estimated_arrival_date   IN       VARCHAR2,
                            I_cb_x_actual_arrival_date      IN       VARCHAR2,
                            I_cb_x_delivery_date            IN       VARCHAR2,
                            I_cb_x_shipment_no              IN       VARCHAR2,
                            I_cb_x_receipt_id               IN       VARCHAR2,
                            I_cb_x_fcr_id                   IN       VARCHAR2,
                            I_cb_x_fcr_date                 IN       VARCHAR2,
                            I_cb_x_in_transit_no            IN       VARCHAR2,
                            I_cb_x_in_transit_date          IN       VARCHAR2,
                            I_cb_x_service_contract_no      IN       VARCHAR2,
                            I_cb_x_lot_no                   IN       VARCHAR2,
                            I_cb_x_seal_id                  IN       VARCHAR2,
                            I_cb_x_packing_method           IN       VARCHAR2,
                            I_cb_x_trans_partner_id         IN       VARCHAR2,
                            I_cb_x_lading_port              IN       VARCHAR2,
                            I_cb_x_discharge_port           IN       VARCHAR2,
                            I_cb_x_container_scac_code      IN       VARCHAR2,
                            I_cb_x_vessel_scac_code         IN       VARCHAR2,
                            I_cb_x_freight_type             IN       VARCHAR2,
                            I_cb_x_freight_size             IN       VARCHAR2,
                            I_cb_x_consolidation_cntry_id   IN       VARCHAR2,
                            I_cb_x_origin_country_id        IN       VARCHAR2,
                            I_cb_x_export_country_id        IN       VARCHAR2,
                            I_cb_x_tran_mode_id             IN       VARCHAR2,
                            I_cb_x_rush_ind                 IN       VARCHAR2,
                            I_cb_x_candidate_ind            IN       VARCHAR2,
                            I_cb_x_comments                 IN       VARCHAR2,
                            I_initial_container_id          IN       VARCHAR2,
                            I_initial_bl_awb_id             IN       VARCHAR2,
                            I_initial_invoice_id            IN       VARCHAR2,
                            I_tra                           IN       WRP_TRANSPORTATION_REC)
RETURN INTEGER IS

   FUNCTION_NAME CONSTANT VARCHAR2(61) := 'TRANSPORTATION_PO_SQL.TRANPO_MASS_UPDATE';

   L_hts_details_missing  BOOLEAN;
   record_locked          EXCEPTION;
   pragma                 EXCEPTION_INIT(record_locked,-54);

   cursor C_TRA is
      select tra.item,
             tra.invoice_id,
             tra.invoice_date,
             tra.transportation_id
        from transportation            tra
       where tra.order_no              = I_tra.order_no
         and tra.vessel_id             = I_tra.vessel_id
         and tra.voyage_flt_id         = I_tra.voyage_flt_id
         and tra.estimated_depart_date = I_tra.estimated_depart_date
         and tra.item                  = NVL(I_tra.item, tra.item);

   cursor C_LOCK_TRA is
      select 1
        from transportation            tra
       where tra.order_no              = I_tra.order_no
         and tra.vessel_id             = I_tra.vessel_id
         and tra.voyage_flt_id         = I_tra.voyage_flt_id
         and tra.estimated_depart_date = I_tra.estimated_depart_date
         and (tra.item                 = NVL(I_tra.item, tra.item) or tra.item is NULL)
         and (tra.container_id         = NVL(I_tra.container_id, tra.container_id) or tra.container_id is NULL)
         and (tra.bl_awb_id            = NVL(I_tra.bl_awb_id, tra.bl_awb_id) or tra.bl_awb_id is NULL)
         and (tra.invoice_id           = NVL(I_tra.invoice_id, tra.invoice_id) or tra.invoice_id is NULL)
         for update nowait;

BEGIN

   if I_tra.order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_tra.order_no',
                                            'NULL');
      return 0;
   end if;

   if I_tra.vessel_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_tra.vessel_id',
                                            'NULL');
      return 0;
   end if;

   if I_tra.voyage_flt_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_tra.voyage_flt_id',
                                            'NULL');
      return 0;
   end if;

   if I_tra.estimated_depart_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            FUNCTION_NAME,
                                            'I_tra.estimated_depart_date',
                                            'NULL');
      return 0;
   end if;

   if I_cb_x_rush_ind = 'Y' then
      if I_tra.rush_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               FUNCTION_NAME,
                                               'I_tra.rush_ind',
                                               'NULL');
         return 0;
      end if;
   end if;

   if I_cb_x_candidate_ind = 'Y' then
      if I_tra.candidate_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                               FUNCTION_NAME,
                                               'I_tra.candidate_ind',
                                               'NULL');
         return 0;
      end if;
   end if;
   
   if I_tra.candidate_ind = 'Y' then
      FOR C_TRA_REC in C_TRA LOOP
        if TRANSPORTATION_SQL.APPROVED_HTS_EXISTS(O_error_message,
                                                  L_hts_details_missing,
                                                  I_tra.order_no,
                                                  c_tra_rec.item,
                                                  c_tra_rec.transportation_id) = FALSE then
           return 0;
        end if;

        if L_hts_details_missing then
           O_error_message:= SQL_LIB.CREATE_MSG('TRANPO_CAND_IND_NO_HTS',
                                                NULL,
                                                NULL,
                                                NULL);
           return 0;
        end if;

        if (((I_initial_invoice_id IS NULL) AND (c_tra_rec.invoice_id IS NULL)) 
            OR ((I_tra.invoice_date IS NULL) AND (c_tra_rec.invoice_date IS NULL))) then
           O_error_message:= SQL_LIB.CREATE_MSG('INV_TRANPO_INVC_DTL',
                                                NULL,
                                                NULL,
                                                NULL);
           return 0;
        end if;  
      END LOOP;
   end if;

   open  C_LOCK_TRA;

   close C_LOCK_TRA;

   update transportation               tra
      set tra.bl_awb_id                = DECODE(tra.bl_awb_id                ,NULL,I_initial_bl_awb_id     ,tra.bl_awb_id),
          tra.container_id             = DECODE(tra.container_id             ,NULL,I_initial_container_id  ,tra.container_id),
          tra.invoice_id               = DECODE(tra.invoice_id               ,NULL,I_initial_invoice_id    ,tra.invoice_id),
          tra.status                   = DECODE(I_cb_x_status                ,'Y' ,NULL                    ,NVL(I_tra.status                   ,tra.status)),
          tra.invoice_date             = NVL(I_tra.invoice_date ,tra.invoice_date),                                                   
          tra.actual_depart_date       = DECODE(I_cb_x_actual_depart_date    ,'Y' ,NULL                    ,NVL(I_tra.actual_depart_date       ,tra.actual_depart_date)),
          tra.actual_arrival_date      = DECODE(I_cb_x_actual_arrival_date   ,'Y' ,NULL                    ,NVL(I_tra.actual_arrival_date      ,tra.actual_arrival_date)),
          tra.estimated_arrival_date   = DECODE(I_cb_x_estimated_arrival_date,'Y' ,NULL                    ,NVL(I_tra.estimated_arrival_date   ,tra.estimated_arrival_date)),
          tra.delivery_date            = DECODE(I_cb_x_delivery_date         ,'Y' ,NULL                    ,NVL(I_tra.delivery_date            ,tra.delivery_date)),
          tra.shipment_no              = DECODE(I_cb_x_shipment_no           ,'Y' ,NULL                    ,NVL(I_tra.shipment_no              ,tra.shipment_no)),
          tra.receipt_id               = DECODE(I_cb_x_receipt_id            ,'Y' ,NULL                    ,NVL(I_tra.receipt_id               ,tra.receipt_id)),
          tra.fcr_id                   = DECODE(I_cb_x_fcr_id                ,'Y' ,NULL                    ,NVL(I_tra.fcr_id                   ,tra.fcr_id)),
          tra.fcr_date                 = DECODE(I_cb_x_fcr_date              ,'Y' ,NULL                    ,NVL(I_tra.fcr_date                 ,tra.fcr_date)),
          tra.in_transit_no            = DECODE(I_cb_x_in_transit_no         ,'Y' ,NULL                    ,NVL(I_tra.in_transit_no            ,tra.in_transit_no)),
          tra.in_transit_date          = DECODE(I_cb_x_in_transit_date       ,'Y' ,NULL                    ,NVL(I_tra.in_transit_date          ,tra.in_transit_date)),
          tra.service_contract_no      = DECODE(I_cb_x_service_contract_no   ,'Y' ,NULL                    ,NVL(I_tra.service_contract_no      ,tra.service_contract_no)),
          tra.lot_no                   = DECODE(I_cb_x_lot_no                ,'Y' ,NULL                    ,NVL(I_tra.lot_no                   ,tra.lot_no)),
          tra.seal_id                  = DECODE(I_cb_x_seal_id               ,'Y' ,NULL                    ,NVL(I_tra.seal_id                  ,tra.seal_id)),
          tra.packing_method           = DECODE(I_cb_x_packing_method        ,'Y' ,NULL                    ,NVL(I_tra.packing_method           ,tra.packing_method)),
          tra.trans_partner_type       = DECODE(I_cb_x_trans_partner_id      ,'Y' ,NULL                    ,NVL(I_tra.trans_partner_type       ,tra.trans_partner_type)),
          tra.trans_partner_id         = DECODE(I_cb_x_trans_partner_id      ,'Y' ,NULL                    ,NVL(I_tra.trans_partner_id         ,tra.trans_partner_id)),
          tra.lading_port              = DECODE(I_cb_x_lading_port           ,'Y' ,NULL                    ,NVL(I_tra.lading_port              ,tra.lading_port)),
          tra.discharge_port           = DECODE(I_cb_x_discharge_port        ,'Y' ,NULL                    ,NVL(I_tra.discharge_port           ,tra.discharge_port)),
          tra.container_scac_code      = DECODE(I_cb_x_container_scac_code   ,'Y' ,NULL                    ,NVL(I_tra.container_scac_code      ,tra.container_scac_code)),
          tra.vessel_scac_code         = DECODE(I_cb_x_vessel_scac_code      ,'Y' ,NULL                    ,NVL(I_tra.vessel_scac_code         ,tra.vessel_scac_code)),
          tra.freight_type             = DECODE(I_cb_x_freight_type          ,'Y' ,NULL                    ,NVL(I_tra.freight_type             ,tra.freight_type)),
          tra.freight_size             = DECODE(I_cb_x_freight_size          ,'Y' ,NULL                    ,NVL(I_tra.freight_size             ,tra.freight_size)),
          tra.consolidation_country_id = DECODE(I_cb_x_consolidation_cntry_id,'Y' ,NULL                    ,NVL(I_tra.consolidation_country_id ,tra.consolidation_country_id)),
          tra.origin_country_id        = DECODE(I_cb_x_origin_country_id     ,'Y' ,NULL                    ,NVL(I_tra.origin_country_id        ,tra.origin_country_id)),
          tra.export_country_id        = DECODE(I_cb_x_export_country_id     ,'Y' ,NULL                    ,NVL(I_tra.export_country_id        ,tra.export_country_id)),
          tra.tran_mode_id             = DECODE(I_cb_x_tran_mode_id          ,'Y' ,NULL                    ,NVL(I_tra.tran_mode_id             ,tra.tran_mode_id)),
          tra.rush_ind                 = DECODE(I_cb_x_rush_ind              ,'Y' ,'N'                     ,NVL(I_tra.rush_ind                 ,tra.rush_ind)),
          tra.candidate_ind            = DECODE(I_cb_x_candidate_ind         ,'Y' ,'N'                     ,NVL(I_tra.candidate_ind            ,tra.candidate_ind)),
          tra.comments                 = DECODE(I_cb_x_comments              ,'Y' ,NULL                    ,NVL(I_tra.comments                 ,tra.comments))
    where tra.order_no                 = I_tra.order_no
      and tra.vessel_id                = I_tra.vessel_id
      and tra.voyage_flt_id            = I_tra.voyage_flt_id
      and tra.estimated_depart_date    = I_tra.estimated_depart_date
      and (tra.item                    = NVL(I_tra.item, tra.item) or tra.item is NULL)
      and (tra.container_id            = NVL(I_tra.container_id, tra.container_id) or tra.container_id is NULL)
      and (tra.bl_awb_id               = NVL(I_tra.bl_awb_id, tra.bl_awb_id) or tra.bl_awb_id is NULL)
      and (tra.invoice_id              = NVL(I_tra.invoice_id, tra.invoice_id) or tra.invoice_id is NULL);

   return 1;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'TRANSPORTATION',
                                            'I_tra.order_no='             ||I_tra.order_no     ||
                                            ', I_tra.vessel_id='          ||I_tra.vessel_id    ||
                                            ', I_tra.voyage_flt_id='      ||I_tra.voyage_flt_id,
                                            'I_tra.estimated_depart_date='||I_tra.estimated_depart_date);
      return 0;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            FUNCTION_NAME,
                                            To_Char(SQLCODE));
      return 0;
END TRANPO_MASS_UPDATE;
----------------------------------------------------------------------------------------------------
END TRANSPORTATION_PO_SQL;
/
