
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SUBCLASS_ATTRIB_SQL AUTHID CURRENT_USER AS
  
--------------------------------------------------------------------
--- Function Name:  GET_NAME
--- Purpose:        Looks up a SUBCLASS description from SUBCLASS
--- Called By:      P_POPULATE_DESC in trandata.fmb
--- Calls:	    <none>
--- Input Values:   O_error_message,
---		    I_subclass,
---		    O_sub_name
--- 
--- Created:	    by Chad Whipple
--- Modified:	    01-AUG-96 by Matt Sniffen
---		    changed name,package to meet new standards
--------------------------------------------------------------------
   FUNCTION GET_NAME(	O_error_message IN OUT 	VARCHAR2,
			I_dept		IN	NUMBER,
			I_class		IN	NUMBER,
                        I_subclass      IN     	NUMBER,
                        O_sub_name 	IN OUT 	VARCHAR2)
   RETURN BOOLEAN;
---
END SUBCLASS_ATTRIB_SQL;
/


