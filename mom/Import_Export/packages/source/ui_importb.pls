CREATE OR REPLACE PACKAGE Body IMPORT_SQL IS
  ---------------------------------------------------------------------------------------------
  -- Module                   : htsfind (Form Module)
  -- Source Object            : FUNCTION   -> P_CHECK_EXISTS
  -- NEW ARGUMENTS (0)
  ---------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTS(O_error_message   IN OUT   VARCHAR2,
                      I_country         IN OUT   hts.import_country_id%TYPE,
                      I_hts             IN OUT   hts.hts%TYPE,
                      I_effect_from     IN OUT   hts.effect_from%TYPE,
                      I_effect_to       IN OUT   hts.effect_to%TYPE)
  return BOOLEAN
is
  L_exists    VARCHAR2(1);
  L_program   VARCHAR2(64) := 'IMPORT_SQL.CHECK_EXISTS';
  cursor C_EXISTS is
    select 'x'
      from hts
     where hts = I_hts
       and import_country_id = I_country
       and effect_from = I_effect_from
       and effect_to = I_effect_to;
BEGIN
  open C_EXISTS;
  fetch C_EXISTS into L_exists;
  if C_EXISTS%NOTFOUND then
    O_error_message := SQL_LIB.CREATE_MSG('ENTITY_DOES_NOT_EXIST',
                                          NULL,
                                          NULL,
                                          NULL);
    close C_EXISTS;
    return FALSE;
  end if;
  close C_EXISTS;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : tranpo (Form Module)
-- Source Object            : PROCEDURE  -> P_POST_QUERY
-- NEW ARGUMENTS (4)
-- ITEM          :B_SHIPMENT.SHIPMENT.................... -> I_SHIPMENT
-- ITEM          :B_SHIPMENT.TI_TO_LOC_DESC.............. -> I_TI_TO_LOC_DESC
-- ITEM          :B_SHIPMENT.TO_LOC...................... -> I_TO_LOC
-- ITEM          :B_SHIPMENT.TO_LOC_TYPE................. -> I_TO_LOC_TYPE
---------------------------------------------------------------------------------------------
FUNCTION POST_QUERY(O_error_message    IN OUT   VARCHAR2,
                    I_SHIPMENT         IN       NUMBER ,
                    I_TI_TO_LOC_DESC   IN OUT   VARCHAR2 ,
                    I_TO_LOC           IN       NUMBER ,
                    I_TO_LOC_TYPE      IN       VARCHAR2 )
  return BOOLEAN
is
  L_error_message   VARCHAR2(255):= NULL;
  L_loc_exist       BOOLEAN := NULL;
  L_wh              WH%ROWTYPE;
  L_program         VARCHAR2(64) := 'IMPORT_SQL.POST_QUERY';
  cursor C_GET_VVE is
    select vessel_id ,
           voyage_flt_id ,
           estimated_depart_date
      from transportation_shipment
     where shipment = I_SHIPMENT ;
BEGIN
  --- Get to_loc name
  if I_TO_LOC_TYPE = 'S' then
    if STORE_ATTRIB_SQL.GET_NAME(L_error_message,
                                 I_TO_LOC,
                                 I_TI_TO_LOC_DESC) = FALSE then
       I_TI_TO_LOC_DESC := NULL;
       O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
  elsif I_TO_LOC_TYPE = 'W' then
    if WH_ATTRIB_SQL.GET_ROW(L_error_message,
                             L_loc_exist,
                             L_wh,
                             I_TO_LOC) = FALSE
                             OR L_loc_exist = FALSE then
       I_TI_TO_LOC_DESC := NULL;
       O_error_message  := SQL_LIB.CREATE_MSG(L_error_message,
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
    end if;
    I_TI_TO_LOC_DESC := L_wh.wh_name;
  elsif I_TO_LOC_TYPE = 'E' then
    if LOCATION_ATTRIB_SQL.GET_FINISHER_NAME(L_error_message,
                                             L_loc_exist,
                                             I_TI_TO_LOC_DESC,
                                             I_TO_LOC,
                                             I_TO_LOC_TYPE) = FALSE Or L_loc_exist = FALSE then
        O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
    end if;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
  return FALSE;
end POST_QUERY;
end IMPORT_SQL;
/
