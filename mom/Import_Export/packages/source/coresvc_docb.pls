CREATE OR REPLACE PACKAGE BODY CORESVC_DOC as
   cursor C_SVC_DOC(I_process_id NUMBER,
                    I_chunk_id   NUMBER) is
      select pk_doc.rowid     as pk_doc_rid,
             st.rowid         as st_rid,
             st.text,
             st.seq_no,
             UPPER(st.lc_ind) as lc_ind,
             pk_doc.lc_ind    as old_lc_ind,
             st.doc_type,
             st.doc_desc,
             st.doc_id,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action) as action,
             st.process$status
        from svc_doc st,
             doc pk_doc
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.doc_id     = pk_doc.doc_id (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab     errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;

   Type DOC_TL_TAB IS TABLE OF DOC_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
------------------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if SHEET_NAME_TRANS.EXISTS(I_sheet_name) then
      return SHEET_NAME_TRANS(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id  IN  S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet    IN  VARCHAR2,
                          I_row_seq  IN  NUMBER,
                          I_col      IN  VARCHAR2,
                          I_sqlcode  IN  NUMBER,
                          I_sqlerrm  IN  VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).file_id              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_seq_no         := S9T_ERRORS_SEQ.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).template_key         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).wksht_key            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).column_key           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).row_seq              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_key            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_id            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_datetime      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_id       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_datetime := SYSDATE;
END WRITE_S9T_ERROR;
-------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
-----------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets      S9T_PKG.NAMES_MAP_TYP;
   DOC_cols      S9T_PKG.NAMES_MAP_TYP;
   DOC_TL_cols   S9T_PKG.NAMES_MAP_TYP;
BEGIN
   L_sheets     := S9T_PKG.GET_SHEET_NAMES(I_file_id);
   DOC_cols     := S9T_PKG.GET_COL_NAMES(I_file_id, DOC_SHEET);
   DOC$Action   := DOC_cols('ACTION');
   DOC$TEXT     := DOC_cols('TEXT');
   DOC$SEQ_NO   := DOC_cols('SEQ_NO');
   DOC$LC_IND   := DOC_cols('LC_IND');
   DOC$DOC_TYPE := DOC_cols('DOC_TYPE');
   DOC$DOC_DESC := DOC_cols('DOC_DESC');
   DOC$DOC_ID   := DOC_cols('DOC_ID');

   DOC_TL_cols     := S9T_PKG.GET_COL_NAMES(I_file_id, DOC_TL_SHEET);
   DOC_TL$Action   := DOC_TL_cols('ACTION');
   DOC_TL$LANG     := DOC_TL_cols('LANG');
   DOC_TL$DOC_ID   := DOC_TL_cols('DOC_ID');
   DOC_TL$DOC_DESC := DOC_TL_cols('DOC_DESC');
END POPULATE_NAMES;
-----------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_DOC(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                      from s9t_folder sf,
                           TABLE(sf.s9t_file_obj.sheets) ss
                     where sf.file_id  = I_file_id
                       and ss.sheet_name = DOC_SHEET)
   select s9t_row(s9t_cells(CORESVC_DOC.action_mod,
                            doc_id,
                            doc_desc,
                            doc_type,
                            seq_no,
                            text,
                            lc_ind))
     from doc;
END POPULATE_DOC;
-----------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_DOC_TL(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                      from s9t_folder sf,
                           TABLE(sf.s9t_file_obj.sheets) ss
                     where sf.file_id  = I_file_id
                       and ss.sheet_name = DOC_TL_SHEET)
   select s9t_row(s9t_cells(CORESVC_DOC.action_mod,
                            lang,
                            doc_id,
                            doc_desc))
     from doc_tl;
END POPULATE_DOC_TL;
-----------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file       s9t_file;
   L_file_name  s9t_folder.file_name%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(DOC_SHEET);
   L_file.sheets(l_file.get_sheet_index(DOC_SHEET)).column_headers := s9t_cells('ACTION',
                                                                                'DOC_ID',
                                                                                'DOC_DESC',
                                                                                'DOC_TYPE',
                                                                                'SEQ_NO',
                                                                                'TEXT',
                                                                                'LC_IND');
   S9T_PKG.SAVE_OBJ(L_file);

   L_file.add_sheet(DOC_TL_SHEET);
   L_file.sheets(l_file.get_sheet_index(DOC_TL_SHEET)).column_headers := s9t_cells('ACTION',
                                                                                   'LANG',
                                                                                   'DOC_ID',
                                                                                   'DOC_DESC');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program  VARCHAR2(64):='CORESVC_DOC.CREATE_S9T';
   L_file     s9t_file;

BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE   then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_DOC(O_file_id);
      POPULATE_DOC_TL(O_file_id);
      COMMIT;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);

   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);

   L_file := S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE   then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-----------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_DOC(I_file_id     IN  S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id  IN  SVC_DOC.PROCESS_ID%TYPE) IS
   TYPE svc_doc_col_typ IS TABLE OF SVC_DOC%ROWTYPE;
   svc_doc_col   svc_doc_col_typ := NEW svc_doc_col_typ();
   L_error       BOOLEAN         := FALSE;
   L_temp_rec    SVC_DOC%ROWTYPE;
   L_default_rec SVC_DOC%ROWTYPE;
   L_process_id  SVC_DOC.PROCESS_ID%TYPE;

   cursor C_MANDATORY_IND is
      select TEXT_mi,
             SEQ_NO_mi,
             LC_IND_mi,
             DOC_TYPE_mi,
             DOC_DESC_mi,
             DOC_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key              = CORESVC_DOC.template_key
                 and wksht_key                 = 'DOC'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) in ('TEXT'     as TEXT,
                                            'SEQ_NO'   as SEQ_NO,
                                            'LC_IND'   as LC_IND,
                                            'DOC_TYPE' as DOC_TYPE,
                                            'DOC_DESC' as DOC_DESC,
                                            'DOC_ID'   as DOC_ID,
                                             null      as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_DOC';
   L_pk_columns    VARCHAR2(255)  := 'Document';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec in (select text_dv,
                      seq_no_dv,
                      lc_ind_dv,
                      doc_type_dv,
                      doc_desc_dv,
                      doc_id_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key      = CORESVC_DOC.template_key
                          and wksht_key         = 'DOC'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) in ( 'TEXT'     as TEXT,
                                                      'SEQ_NO'   as SEQ_NO,
                                                      'LC_IND'   as LC_IND,
                                                      'DOC_TYPE' as DOC_TYPE,
                                                      'DOC_DESC' as DOC_DESC,
                                                      'DOC_ID'   as DOC_ID,
                                                         NULL    as dummy)))
   LOOP
      BEGIN
         L_default_rec.text := rec.text_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DOC ' ,
                            NULL,
                           'TEXT ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.seq_no := rec.seq_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DOC ' ,
                            NULL,
                           'SEQ_NO ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lc_ind := rec.lc_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DOC ' ,
                            NULL,
                           'LC_IND ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.doc_type := rec.doc_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DOC ' ,
                            NULL,
                           'DOC_TYPE ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.doc_desc := rec.doc_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DOC ' ,
                            NULL,
                           'DOC_DESC ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.doc_id := rec.doc_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DOC ' ,
                            NULL,
                           'DOC_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;

   FOR rec in (select UPPER(r.get_cell(DOC$Action)) as Action,
                      r.get_cell(DOC$TEXT)          as TEXT,
                      r.get_cell(DOC$SEQ_NO)        as SEQ_NO,
                      UPPER(r.get_cell(DOC$LC_IND)) as LC_IND,
                      r.get_cell(DOC$DOC_TYPE)      as DOC_TYPE,
                      r.get_cell(DOC$DOC_DESC)      as DOC_DESC,
                      r.get_cell(DOC$DOC_ID)        as DOC_ID,
                      r.get_row_seq()               as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                 where sf.file_id  = I_file_id
                   and ss.sheet_name = sheet_name_trans(DOC_SHEET))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;

      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DOC_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.text := rec.text;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DOC_SHEET,
                            rec.row_seq,
                            'TEXT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.seq_no := rec.seq_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           DOC_SHEET,
                            rec.row_seq,
                            'SEQ_NO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.lc_ind := rec.lc_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DOC_SHEET,
                            rec.row_seq,
                            'LC_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.doc_type := rec.doc_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DOC_SHEET,
                            rec.row_seq,
                            'DOC_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.doc_desc := rec.doc_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DOC_SHEET,
                            rec.row_seq,
                            'DOC_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.doc_id := rec.doc_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DOC_SHEET,
                            rec.row_seq,
                            'DOC_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_DOC.action_new then
         L_temp_rec.text      := NVL(L_temp_rec.text,
                                     L_default_rec.text);
         L_temp_rec.seq_no    := NVL(L_temp_rec.seq_no,
                                     L_default_rec.seq_no);
         L_temp_rec.lc_ind    := NVL(L_temp_rec.lc_ind,
                                     L_default_rec.lc_ind);
         L_temp_rec.doc_type  := NVL(L_temp_rec.doc_type,
                                     L_default_rec.doc_type);
         L_temp_rec.doc_desc  := NVL(L_temp_rec.doc_desc,
                                     L_default_rec.doc_desc);
         L_temp_rec.doc_id    := NVL(L_temp_rec.doc_id,
                                     L_default_rec.doc_id);
      end if;

      if rec.action IN (CORESVC_DOC.action_mod, CORESVC_DOC.action_del)
         and L_temp_rec.doc_id is NULL   then
         WRITE_S9T_ERROR(I_file_id,
                         DOC_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_doc_col.extend();
         svc_doc_col(svc_doc_col.COUNT()) := L_temp_rec;
      end if;

   END LOOP;
   BEGIN
      FORALL i in 1..svc_doc_col.COUNT SAVE EXCEPTIONS
         merge into svc_doc st
         using(select
                     (case
                      when L_mi_rec.text_mi        = 'N'
                       and svc_doc_col(i).action   = CORESVC_DOC.action_mod
                       and s1.text is NULL
                      then mt.text
                      else s1.text
                      end) as text,
                     (case
                      when L_mi_rec.seq_no_mi      = 'N'
                       and svc_doc_col(i).action   = CORESVC_DOC.action_mod
                       and s1.seq_no is NULL
                      then mt.seq_no
                      else s1.seq_no
                      end) as seq_no,
                     (case
                      when L_mi_rec.lc_ind_mi      = 'N'
                       and svc_doc_col(i).action   = CORESVC_DOC.action_mod
                       and s1.lc_ind is NULL
                      then mt.lc_ind
                      else s1.lc_ind
                      end) as lc_ind,
                     (case
                      when L_mi_rec.doc_type_mi    = 'N'
                       and svc_doc_col(i).action   = CORESVC_DOC.action_mod
                       and s1.doc_type is NULL
                      then mt.doc_type
                      else s1.doc_type
                      end) as doc_type,
                     (case
                      when L_mi_rec.doc_desc_mi    = 'N'
                       and svc_doc_col(i).action   = CORESVC_DOC.action_mod
                       and s1.doc_desc is NULL
                      then mt.doc_desc
                      else s1.doc_desc
                      end) as doc_desc,
                     (case
                      when L_mi_rec.doc_id_mi      = 'N'
                       and svc_doc_col(i).action   = CORESVC_DOC.action_mod
                       and s1.doc_id is NULL
                      then mt.doc_id
                      else s1.doc_id
                      end) as doc_id,
                     null as dummy
                 from (select
                             svc_doc_col(i).text     as TEXT,
                             svc_doc_col(i).seq_no   as SEQ_NO,
                             svc_doc_col(i).lc_ind   as LC_IND,
                             svc_doc_col(i).doc_type as DOC_TYPE,
                             svc_doc_col(i).doc_desc as DOC_DESC,
                             svc_doc_col(i).doc_id   as DOC_ID,
                             null                    as dummy
                         from dual ) s1,
               DOC mt
                where
                     mt.doc_id (+) =  s1.doc_id   and
                     1 = 1 )sq
                   on (st.doc_id   =  sq.doc_id and
                       svc_doc_col(i).action in (CORESVC_DOC.action_mod,CORESVC_DOC.action_del))
         when matched then
         update
            set process_id        = svc_doc_col(i).process_id,
                chunk_id          = svc_doc_col(i).chunk_id,
                row_seq           = svc_doc_col(i).row_seq,
                action            = svc_doc_col(i).action,
                process$status    = svc_doc_col(i).process$status,
                lc_ind            = sq.lc_ind,
                seq_no            = sq.seq_no,
                doc_type          = sq.doc_type,
                doc_desc          = sq.doc_desc,
                text              = sq.text,
                create_id         = svc_doc_col(i).create_id,
                create_datetime   = svc_doc_col(i).create_datetime,
                last_upd_id       = svc_doc_col(i).last_upd_id,
                last_upd_datetime = svc_doc_col(i).last_upd_datetime
         when NOT matched then
         insert(process_id,
                chunk_id,
                row_seq,
                action,
                process$status,
                text,
                seq_no,
                lc_ind,
                doc_type,
                doc_desc,
                doc_id,
                create_id,
                create_datetime,
                last_upd_id,
                last_upd_datetime)
         values(svc_doc_col(i).process_id,
                svc_doc_col(i).chunk_id,
                svc_doc_col(i).row_seq,
                svc_doc_col(i).action,
                svc_doc_col(i).process$status,
                sq.text,
                sq.seq_no,
                sq.lc_ind,
                sq.doc_type,
                sq.doc_desc,
                sq.doc_id,
                svc_doc_col(i).create_id,
                svc_doc_col(i).create_datetime,
                svc_doc_col(i).last_upd_id,
                svc_doc_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i in 1..SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             DOC_SHEET,
                             svc_doc_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_DOC;
-----------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_DOC_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                             I_process_id   IN   SVC_DOC_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_DOC_TL_COL_TYP IS TABLE OF SVC_DOC_TL%ROWTYPE;
   L_temp_rec         SVC_DOC_TL%ROWTYPE;
   SVC_DOC_TL_COL     SVC_DOC_TL_COL_TYP := NEW SVC_DOC_TL_COL_TYP();
   L_process_id       SVC_DOC_TL.PROCESS_ID%TYPE;
   L_error            BOOLEAN := FALSE;
   L_default_rec      SVC_DOC_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select doc_desc_mi,
             doc_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'DOC_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('DOC_DESC' as doc_desc,
                                       'DOC_ID'   as doc_id,
                                       'LANG'     as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_DOC_TL';
   L_pk_columns    VARCHAR2(255)  := 'Doc ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select doc_desc_dv,
                      doc_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'DOC_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('DOC_DESC' as doc_desc,
                                                'DOC_ID'   as doc_id,
                                                'LANG'     as lang)))
   LOOP
      BEGIN
         L_default_rec.doc_desc := rec.doc_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DOC_TL_SHEET ,
                            NULL,
                           'DOC_DESC' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.doc_id := rec.doc_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           DOC_TL_SHEET ,
                            NULL,
                           'DOC_ID' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DOC_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(doc_tl$action))   as action,
                      r.get_cell(doc_tl$doc_desc)        as doc_desc,
                      r.get_cell(doc_tl$doc_id)          as doc_id,
                      r.get_cell(doc_tl$lang)            as lang,
                      r.get_row_seq()                    as row_seq
                 from s9t_folder sf,
                      table(sf.s9t_file_obj.sheets) ss,
                      table(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(DOC_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DOC_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.doc_desc := rec.doc_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DOC_TL_SHEET,
                            rec.row_seq,
                            'DOC_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.doc_id := rec.doc_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DOC_TL_SHEET,
                            rec.row_seq,
                            'DOC_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DOC_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_DOC.action_new then
         L_temp_rec.doc_desc  := NVL( L_temp_rec.doc_desc,L_default_rec.doc_desc);
         L_temp_rec.doc_id    := NVL( L_temp_rec.doc_id,L_default_rec.doc_id);
         L_temp_rec.lang      := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.doc_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         DOC_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_DOC_TL_col.extend();
         SVC_DOC_TL_col(SVC_DOC_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..SVC_DOC_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_DOC_TL st
      using(select
                  (case
                   when l_mi_rec.doc_desc_mi = 'N'
                    and SVC_DOC_TL_col(i).action = CORESVC_DOC.action_mod
                    and s1.doc_desc IS NULL then
                        mt.doc_desc
                   else s1.doc_desc
                   end) as doc_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and SVC_DOC_TL_col(i).action = CORESVC_DOC.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.doc_id_mi = 'N'
                    and SVC_DOC_TL_col(i).action = CORESVC_DOC.action_mod
                    and s1.doc_id IS NULL then
                        mt.doc_id
                   else s1.doc_id
                   end) as doc_id
              from (select SVC_DOC_TL_col(i).doc_desc as doc_desc,
                           SVC_DOC_TL_col(i).doc_id   as doc_id,
                           SVC_DOC_TL_col(i).lang     as lang
                      from dual) s1,
                   doc_tl mt
             where mt.doc_id (+) = s1.doc_id
               and mt.lang (+)   = s1.lang) sq
                on (st.doc_id = sq.doc_id and
                    st.lang = sq.lang and
                    SVC_DOC_TL_col(i).ACTION IN (CORESVC_DOC.action_mod,CORESVC_DOC.action_del))
      when matched then
      update
         set process_id        = SVC_DOC_TL_col(i).process_id ,
             chunk_id          = SVC_DOC_TL_col(i).chunk_id ,
             row_seq           = SVC_DOC_TL_col(i).row_seq ,
             action            = SVC_DOC_TL_col(i).action ,
             process$status    = SVC_DOC_TL_col(i).process$status ,
             doc_desc = sq.doc_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             doc_desc ,
             doc_id ,
             lang)
      values(SVC_DOC_TL_col(i).process_id ,
             SVC_DOC_TL_col(i).chunk_id ,
             SVC_DOC_TL_col(i).row_seq ,
             SVC_DOC_TL_col(i).action ,
             SVC_DOC_TL_col(i).process$status ,
             sq.doc_desc ,
             sq.doc_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            DOC_TL_SHEET,
                            SVC_DOC_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_DOC_TL;
-----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count    IN OUT  NUMBER,
                     I_file_id        IN      S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id     IN      NUMBER)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_DOC.PROCESS_S9T';
   INVALID_FORMAT    EXCEPTION;
   PRAGMA            EXCEPTION_INIT(INVALID_FORMAT, -31011);
   L_file            s9t_file;
   L_sheets          S9T_PKG.NAMES_MAP_TYP;
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE;
   MAX_CHAR      EXCEPTION;
   PRAGMA        EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE   then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_DOC(I_file_id,
                      I_process_id);
      PROCESS_S9T_DOC_TL(I_file_id,
                         I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   FORALL i in 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;

   COMMIT;
   return TRUE;
EXCEPTION

   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      FORALL i in 1..O_error_count
         insert
           into s9t_errors
         values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DOC_VAL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error          IN OUT  BOOLEAN,
                         I_rec            IN      C_SVC_DOC%ROWTYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(64)                      :='CORESVC_DOC.PROCESS_DOC_VAL';
   L_exists   BOOLEAN                           := FALSE;
   L_table    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_DOC';
BEGIN

   if I_rec.action = action_del   then
      if DOCUMENTS_SQL.REQ_DOCS_EXIST_ID(O_error_message,
                                         L_exists,
                                         I_rec.doc_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DOC_ID',
                     O_error_message);
         O_error := TRUE;
      end if;
      if L_exists
         and O_error = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DOC_ID',
                     'NO_DELETE_DOC');
          O_error := TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DOC_VAL;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_DOC_INS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_doc_temp_rec   IN      DOC%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_DOC.EXEC_DOC_INS';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DOC';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

BEGIN

   insert into doc
        values I_doc_temp_rec;

   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DOC_INS;
-------------------------------------------------------------------------------------------------------
FUNCTION EXEC_DOC_UPD(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_doc_temp_rec   IN      DOC%ROWTYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)                      := 'CORESVC_DOC.EXEC_DOC_UPD';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DOC';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_DOC_LOCK is
      select 'X'
        from DOC
       where doc_id = I_doc_temp_rec.doc_id
       for update nowait;

BEGIN
   open C_DOC_LOCK;
   close C_DOC_LOCK;

   update doc
      set row = I_doc_temp_rec
    where doc_id = I_doc_temp_rec.doc_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      if C_DOC_LOCK%ISOPEN then
         close C_DOC_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'DOC',
                                             I_doc_temp_rec.doc_id,
                                             NULL);
      return FALSE;

   when OTHERS then
      if C_DOC_LOCK%ISOPEN then
         close C_DOC_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DOC_UPD;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_DOC_DEL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_doc_temp_rec   IN      DOC%ROWTYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)                      := 'CORESVC_DOC.EXEC_DOC_DEL';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DOC';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_DOC_LOCK is
      select 'X'
        from DOC
       where doc_id = I_doc_temp_rec.doc_id
       for update nowait;

   cursor C_DOC_TL_LOCK is
      select 'x'
        from doc_tl
       where doc_id = i_doc_temp_rec.doc_id
       for update nowait;
BEGIN
   open C_DOC_TL_LOCK;
   close C_DOC_TL_LOCK;

   open C_DOC_LOCK;
   close C_DOC_LOCK;

   delete from doc_tl
    where doc_id = I_doc_temp_rec.doc_id;

   delete from doc
    where doc_id = I_doc_temp_rec.doc_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'DOC',
                                             I_doc_temp_rec.doc_id,
                                             NULL);
      close C_DOC_LOCK;
      return FALSE;

   when OTHERS then
      if C_DOC_TL_LOCK%ISOPEN then
         close C_DOC_TL_LOCK;
      end if;
      if C_DOC_LOCK%ISOPEN then
         close C_DOC_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DOC_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_DOC_TL_INS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_doc_tl_ins_tab    IN       DOC_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_DOC.EXEC_DOC_TL_INS';

BEGIN
   if I_doc_tl_ins_tab is NOT NULL and I_doc_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_doc_tl_ins_tab.COUNT()
         insert into doc_tl
              values I_doc_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_DOC_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_DOC_TL_UPD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_doc_tl_upd_tab     IN       DOC_TL_TAB,
                         I_doc_tl_upd_rst     IN       ROW_SEQ_TAB,
                         I_process_id         IN       SVC_DOC_TL.PROCESS_ID%TYPE,
                         I_chunk_id           IN       SVC_DOC_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_DOC_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DOC_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_DOC_TL_UPD(I_doc_id  DOC_TL.DOC_ID%TYPE,
                            I_lang    DOC_TL.LANG%TYPE) is
      select 'x'
        from doc_tl
       where doc_id = I_doc_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_doc_tl_upd_tab is NOT NULL and I_doc_tl_upd_tab.count > 0 then
      for i in I_doc_tl_upd_tab.FIRST..I_doc_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_doc_tl_upd_tab(i).lang);
            L_key_val2 := 'Doc ID: '||to_char(I_doc_tl_upd_tab(i).doc_id);
            open C_LOCK_DOC_TL_UPD(I_doc_tl_upd_tab(i).doc_id,
                                     I_doc_tl_upd_tab(i).lang);
            close C_LOCK_DOC_TL_UPD;
            
            update doc_tl
               set doc_desc = I_doc_tl_upd_tab(i).doc_desc,
                   last_update_id = I_doc_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_doc_tl_upd_tab(i).last_update_datetime
             where lang = I_doc_tl_upd_tab(i).lang
               and doc_id = I_doc_tl_upd_tab(i).doc_id;

         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_DOC_TL',
                           I_doc_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_DOC_TL_UPD%ISOPEN then
         close C_LOCK_DOC_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_DOC_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_DOC_TL_DEL(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_doc_tl_del_tab   IN       DOC_TL_TAB,
                         I_doc_tl_del_rst   IN       ROW_SEQ_TAB,
                         I_process_id       IN       SVC_DOC_TL.PROCESS_ID%TYPE,
                         I_chunk_id         IN       SVC_DOC_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_DOC.EXEC_DOC_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DOC_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_DOC_TL_DEL(I_doc_id  DOC_TL.DOC_ID%TYPE,
                            I_lang    DOC_TL.LANG%TYPE) is
      select 'x'
        from doc_tl
       where doc_id = I_doc_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_doc_tl_del_tab is NOT NULL and I_doc_tl_del_tab.count > 0 then
      for i in I_doc_tl_del_tab.FIRST..I_doc_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_doc_tl_del_tab(i).lang);
            L_key_val2 := 'Doc ID: '||to_char(I_doc_tl_del_tab(i).doc_id);
            open C_LOCK_DOC_TL_DEL(I_doc_tl_del_tab(i).doc_id,
                                   I_doc_tl_del_tab(i).lang);
            close C_LOCK_DOC_TL_DEL;
           
            delete doc_tl
             where lang = I_doc_tl_del_tab(i).lang
               and doc_id = I_doc_tl_del_tab(i).doc_id;

         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_DOC_TL',
                           I_doc_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key_val1,
                                             L_key_val2);
      return FALSE;
   when OTHERS then
      if C_LOCK_DOC_TL_DEL%ISOPEN then
         close C_LOCK_DOC_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_DOC_TL_DEL;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DOC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_process_id    IN     SVC_DOC.PROCESS_ID%TYPE,
                     I_chunk_id      IN     SVC_DOC.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      :='CORESVC_DOC.PROCESS_DOC';
   L_error          BOOLEAN;
   L_doc_temp_rec   DOC%ROWTYPE;
   L_process_error  BOOLEAN                           := FALSE;
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_DOC';
   L_next_doc_id    DOC.DOC_ID%TYPE := NULL;
BEGIN
   FOR rec in C_SVC_DOC(I_process_id,
                        I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      if rec.action IN (action_mod,action_new)   then
         /*if rec.action = action_new
            and rec.pk_doc_rid is NOT NULL   then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DOC_ID',
                        'DOC_EXISTS');
            L_error := TRUE;
         end if;*/
         if rec.doc_desc is NULL then
             WRITE_ERROR(I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                         'DOC_DESC',
                         'DOC_DESCR');
            L_error := TRUE;
         end if;
         if rec.doc_type is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DOC_TYPE',
                        'DOC_ID_TYPE');
            L_error := TRUE;
         end if;

         if rec.lc_ind is NULL
            or rec.lc_ind NOT IN ('Y','N') then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'LC_IND',
                        'INV_Y_N_IND');
            L_error := TRUE;
         end if;
         if rec.text is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'TEXT',
                        'DOC_TEXT');
            L_error := TRUE;
         end if;
      end if;

      if rec.action = action_mod
         and rec.old_lc_ind = 'Y'
         and rec.lc_ind = 'N' then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LC_IND',
                     'NO_MOD_LC');
         L_error := TRUE;
      end if;

      if rec.action IN (action_mod,action_del) then

         if rec.pk_doc_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DOC_ID',
                        'NO_RECORD');
            L_error := TRUE;
         end if;

         if PROCESS_DOC_VAL(O_error_message,
                            L_error,
                            rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_error := TRUE;
         end if;
      end if;

      if rec.action = action_new
         and L_error = FALSE
         and L_next_doc_id is NULL then
         if DOCUMENTS_SQL.NEXT_DOC_NO(O_error_message,
                                      L_next_doc_id) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DOC_ID',
                        O_error_message);
            L_error := TRUE;
          else
            L_doc_temp_rec.doc_id             :=L_next_doc_id;
            update svc_doc_tl
              set doc_id = L_next_doc_id
              where process_id =I_process_id
                and chunk_id =I_chunk_id
                and action = action_new
                and doc_id = rec.doc_id;
                L_next_doc_id := NULL;
               
         end if;
      end if;

      if NOT L_error then
         if rec.action IN (action_new, action_mod) then
            if rec.doc_type = 'CS'      then
               L_doc_temp_rec.swift_tag       := '71B';
            elsif rec.doc_type = 'BI'   then
               L_doc_temp_rec.swift_tag       := '78';
            elsif rec.doc_type = 'SI'   then
               L_doc_temp_rec.swift_tag       := '72';
            elsif rec.doc_type = 'AI'   then
               L_doc_temp_rec.swift_tag       := '47B';
            elsif rec.doc_type = 'REQ'  then
               L_doc_temp_rec.swift_tag       := '46B';
            else
               L_doc_temp_rec.swift_tag       := '';
            end if;
         end if;
         if rec.action IN (action_mod,action_del)then
            L_doc_temp_rec.doc_id             :=rec.doc_id;
         end if;

         L_doc_temp_rec.text            := rec.text;
         L_doc_temp_rec.seq_no          := rec.seq_no;
         L_doc_temp_rec.lc_ind          := rec.lc_ind;
         L_doc_temp_rec.doc_type        := rec.doc_type;
         L_doc_temp_rec.doc_desc        := rec.doc_desc;
         L_doc_temp_rec.create_id       := GET_USER;
         L_doc_temp_rec.create_datetime := SYSDATE;

         if rec.action = action_new then
            if EXEC_DOC_INS(O_error_message,
                            L_doc_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_DOC_UPD(O_error_message,
                            L_doc_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;

            end if;
         end if;
         if rec.action = action_del then
            if EXEC_DOC_DEL(O_error_message,
                            L_doc_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DOC;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DOC_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_DOC_TL.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_DOC_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_DOC.PROCESS_DOC_TL';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DOC_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DOC_TL';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_doc_tl_temp_rec      DOC_TL%ROWTYPE;
   L_doc_tl_upd_rst       ROW_SEQ_TAB;
   L_doc_tl_del_rst       ROW_SEQ_TAB;

   cursor C_SVC_DOC_TL(I_process_id NUMBER,
                       I_chunk_id NUMBER) is
      select pk_doc_tl.rowid  as pk_doc_tl_rid,
             fk_doc.rowid     as fk_doc_rid,
             fk_lang.rowid    as fk_lang_rid,
             st.lang,
             st.doc_id,
             st.doc_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_doc_tl  st,
             doc         fk_doc,
             doc_tl      pk_doc_tl,
             lang        fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.doc_id      =  fk_doc.doc_id (+)
         and st.lang        =  pk_doc_tl.lang (+)
         and st.doc_id      =  pk_doc_tl.doc_id (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_DOC_TL_TAB is TABLE OF C_SVC_DOC_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_doc_tl_tab        SVC_DOC_TL_TAB;

   L_doc_tl_ins_tab         doc_tl_tab         := NEW doc_tl_tab();
   L_doc_tl_upd_tab         doc_tl_tab         := NEW doc_tl_tab();
   L_doc_tl_del_tab         doc_tl_tab         := NEW doc_tl_tab();

BEGIN
   if C_SVC_DOC_TL%ISOPEN then
      close C_SVC_DOC_TL;
   end if;

   open C_SVC_DOC_TL(I_process_id,
                     I_chunk_id);
   LOOP
      fetch C_SVC_DOC_TL bulk collect into L_svc_doc_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_doc_tl_tab.COUNT > 0 then
         FOR i in L_svc_doc_tl_tab.FIRST..L_svc_doc_tl_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_doc_tl_tab(i).action is NULL
               or L_svc_doc_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_doc_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_doc_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_doc_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_doc_tl_tab(i).action = action_new
               and L_svc_doc_tl_tab(i).pk_doc_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_doc_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_doc_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_doc_tl_tab(i).lang is NOT NULL
               and L_svc_doc_tl_tab(i).doc_id is NOT NULL
               and L_svc_doc_tl_tab(i).pk_doc_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_doc_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_doc_tl_tab(i).action = action_new
               and L_svc_doc_tl_tab(i).doc_id is NOT NULL
               and L_svc_doc_tl_tab(i).fk_doc_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_doc_tl_tab(i).row_seq,
                            'DOC_ID',
                            'NO_DOC_ID_FOUND');
               L_error :=TRUE;
            end if;

            if L_svc_doc_tl_tab(i).action = action_new
               and L_svc_doc_tl_tab(i).lang is NOT NULL
               and L_svc_doc_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_doc_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_doc_tl_tab(i).action in (action_new, action_mod) and L_svc_doc_tl_tab(i).doc_desc is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_doc_tl_tab(i).row_seq,
                           'DOC_DESC',
                           'DOC_DESCR');
               L_error :=TRUE;
            end if;

            if L_svc_doc_tl_tab(i).doc_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_doc_tl_tab(i).row_seq,
                           'DOC_ID',
                           'MUST_ENTER_DOC_ID');
               L_error :=TRUE;
            end if;

            if L_svc_doc_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_doc_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANG');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_doc_tl_temp_rec.lang := L_svc_doc_tl_tab(i).lang;
               L_doc_tl_temp_rec.doc_id := L_svc_doc_tl_tab(i).doc_id;
               L_doc_tl_temp_rec.doc_desc := L_svc_doc_tl_tab(i).doc_desc;
               L_doc_tl_temp_rec.create_datetime := SYSDATE;
               L_doc_tl_temp_rec.create_id := GET_USER;
               L_doc_tl_temp_rec.last_update_datetime := SYSDATE;
               L_doc_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_doc_tl_tab(i).action = action_new then
                  L_doc_tl_ins_tab.extend;
                  L_doc_tl_ins_tab(L_doc_tl_ins_tab.count()) := L_doc_tl_temp_rec;
               end if;

               if L_svc_doc_tl_tab(i).action = action_mod then
                  L_doc_tl_upd_tab.extend;
                  L_doc_tl_upd_tab(L_doc_tl_upd_tab.count()) := L_doc_tl_temp_rec;
                  L_doc_tl_upd_rst(L_doc_tl_upd_tab.count()) := L_svc_doc_tl_tab(i).row_seq;
               end if;

               if L_svc_doc_tl_tab(i).action = action_del then
                  L_doc_tl_del_tab.extend;
                  L_doc_tl_del_tab(L_doc_tl_del_tab.count()) := L_doc_tl_temp_rec;
                  L_doc_tl_del_rst(L_doc_tl_del_tab.count()) := L_svc_doc_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_DOC_TL%NOTFOUND;
   END LOOP;
   close C_SVC_DOC_TL;
   if EXEC_DOC_TL_INS(O_error_message,
                      L_doc_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_DOC_TL_UPD(O_error_message,
                      L_doc_tl_upd_tab,
                      L_doc_tl_upd_rst,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_DOC_TL_DEL(O_error_message,
                      L_doc_tl_del_tab,
                      L_doc_tl_del_rst,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DOC_TL;
-----------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN SVC_DOC.PROCESS_ID%TYPE) IS
BEGIN
   delete
     from svc_doc_tl
    where process_id = I_process_id;

   delete
     from svc_doc
    where process_id = I_process_id;
END CLEAR_STAGING_DATA;

------------------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64) := 'CORESVC_DOC.PROCESS';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_DOC(O_error_message,
                  I_process_id,
                  I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_DOC_TL(O_error_message,
                     I_process_id,
                     I_chunk_id) = FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                    when status = 'PE'
                    then 'PE'
                    else L_process_status
                    END),
          action_date = sysdate
    where process_id = I_process_id;

   CLEAR_STAGING_DATA(I_process_id);


   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
-----------------------------------------------------------------------------------------------------
END CORESVC_DOC;
/
