CREATE OR REPLACE PACKAGE BODY OBLIGATION_SQL AS
--------------------------------------------------------------------------------
FUNCTION GET_NEXT_OBLIGATION (O_error_message    IN OUT   VARCHAR2,
                              O_obligation_key   IN OUT   OBLIGATION.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN IS

   L_first_time               VARCHAR2(3) := 'YES';
   L_wrap_seq_no              OBLIGATION.OBLIGATION_KEY%TYPE;
   L_exists                   VARCHAR2(1) := NULL;

   cursor C_OBL_EXISTS is
      select 'x'
        from obligation
       where obligation_key  = O_obligation_key;

   cursor C_SELECT_NEXTVAL is
      select obligation_sequence.NEXTVAL
        from dual;

BEGIN

   LOOP
      SQL_LIB.SET_MARK('open','C_SELECT_NEXTVAL','DUAL',NULL);
      open C_SELECT_NEXTVAL;
      SQL_LIB.SET_MARK('FETCH','C_SELECT_NEXTVAL','DUAL',NULL);
      fetch C_SELECT_NEXTVAL into O_obligation_key;
      if L_first_time = 'YES' then
         L_wrap_seq_no   := O_obligation_key;
         L_first_time    := 'NO';
      elsif O_obligation_key = L_wrap_seq_no then
         O_error_message := SQL_LIB.CREATE_MSG('NO_AVAIL_OBLIG_SEQ', NULL, NULL, NULL);
         SQL_LIB.SET_MARK('CLOSE','C_SELECT_NEXTVAL','DUAL',NULL);
         close C_SELECT_NEXTVAL;
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_SELECT_NEXTVAL','DUAL',NULL);
      close C_SELECT_NEXTVAL;

      SQL_LIB.SET_MARK('open','C_OBL_EXISTS','OBLIGATION',
                      'OBLIGATION KEY: '||to_char(O_obligation_key));
      open C_OBL_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_OBL_EXISTS','OBLIGATION',
                      'OBLIGATION KEY: '||to_char(O_obligation_key));
      fetch C_OBL_EXISTS into L_exists;
      if C_OBL_EXISTS%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_OBL_EXISTS','OBLIGATION',
                      'OBLIGATION KEY: '||to_char(O_obligation_key));
         close C_OBL_EXISTS;
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_OBL_EXISTS','OBLIGATION',
                      'OBLIGATION KEY: '||to_char(O_obligation_key));
      close C_OBL_EXISTS;
   END LOOP;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'OBLIGATION_SQL.GET_NEXT_OBLIGATION',
                                            to_char(SQLCODE));
   return FALSE;
END GET_NEXT_OBLIGATION;
--------------------------------------------------------------------------------
FUNCTION GET_INFO (O_error_message      IN OUT   VARCHAR2,
                   O_exists             IN OUT   BOOLEAN,
                   O_obligation_level   IN OUT   OBLIGATION.OBLIGATION_LEVEL%TYPE,
                   O_key_value_1        IN OUT   OBLIGATION.KEY_VALUE_1%TYPE,
                   O_key_value_2        IN OUT   OBLIGATION.KEY_VALUE_2%TYPE,
                   O_key_value_3        IN OUT   OBLIGATION.KEY_VALUE_3%TYPE,
                   O_key_value_4        IN OUT   OBLIGATION.KEY_VALUE_4%TYPE,
                   O_key_value_5        IN OUT   OBLIGATION.KEY_VALUE_5%TYPE,
                   O_key_value_6        IN OUT   OBLIGATION.KEY_VALUE_6%TYPE,
                   O_partner_type       IN OUT   PARTNER.PARTNER_TYPE%TYPE,
                   O_partner_id         IN OUT   PARTNER.PARTNER_ID%TYPE,
                   O_supplier           IN OUT   OBLIGATION.SUPPLIER%TYPE,
                   O_payment_method     IN OUT   OBLIGATION.PAYMENT_METHOD%TYPE,
                   O_ext_invc_no        IN OUT   OBLIGATION.EXT_INVC_NO%TYPE,
                   O_ext_invc_date      IN OUT   OBLIGATION.EXT_INVC_DATE%TYPE,
                   O_paid_date          IN OUT   OBLIGATION.PAID_DATE%TYPE,
                   O_paid_amt           IN OUT   OBLIGATION.PAID_AMT%TYPE,
                   O_currency_code      IN OUT   CURRENCIES.CURRENCY_CODE%TYPE,
                   O_exchange_rate      IN OUT   CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                   O_check_auth_no      IN OUT   OBLIGATION.CHECK_AUTH_NO%TYPE,
                   O_status             IN OUT   OBLIGATION.STATUS%TYPE,
                   I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN IS

   cursor C_OBL_INFO is
      select obligation_level,
             key_value_1,
             key_value_2,
             key_value_3,
             key_value_4,
             key_value_5,
             key_value_6,
             partner_type,
             partner_id,
             supplier,
             ext_invc_no,
             ext_invc_date,
             paid_date,
             paid_amt,
             payment_method,
             currency_code,
             exchange_rate,
             check_auth_no,
             status
        from obligation
       where obligation_key = I_obligation_key;

BEGIN
   if I_obligation_key is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',NULL,NULL,NULL);
      return FALSE;
   end if;

   O_exists := FALSE;
   SQL_LIB.SET_MARK('open','C_OBL_INFO','OBLIGATION',
                    'OBLIGATION KEY: '||to_char(I_obligation_key));
   open C_OBL_INFO;
   SQL_LIB.SET_MARK('FETCH','C_OBL_INFO','OBLIGATION',
                    'OBLIGATION KEY: '||to_char(I_obligation_key));
   fetch C_OBL_INFO into O_obligation_level,
                         O_key_value_1,
                         O_key_value_2,
                         O_key_value_3,
                         O_key_value_4,
                         O_key_value_5,
                         O_key_value_6,
                         O_partner_type,
                         O_partner_id,
                         O_supplier,
                         O_ext_invc_no,
                         O_ext_invc_date,
                         O_paid_date,
                         O_paid_amt,
                         O_payment_method,
                         O_currency_code,
                         O_exchange_rate,
                         O_check_auth_no,
                         O_status;
   if C_OBL_INFO%NOTFOUND then
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_OBLIGATION',to_char(I_obligation_key),NULL,NULL);
   else
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_OBL_INFO','OBLIGATION',
                    'OBLIGATION KEY: '||to_char(I_obligation_key));
   close C_OBL_INFO;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'OBLIGATION_SQL.GET_INFO',
                                            to_char(SQLCODE));
   return FALSE;
END GET_INFO;
--------------------------------------------------------------------------------
FUNCTION EXT_INVC_VALIDATE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   BOOLEAN,
                            I_partner_id      IN       OBLIGATION.PARTNER_ID%TYPE,
                            I_supplier        IN       OBLIGATION.SUPPLIER%TYPE,
                            I_ext_invc_no     IN       OBLIGATION.EXT_INVC_NO%TYPE)
   return BOOLEAN is

      L_exists   VARCHAR2(1) := NULL;

      cursor C_EXISTS is
         select 'x'
           from obligation
          where ext_invc_no    = NVL(TO_CHAR(I_ext_invc_no), -1)
            and (   partner_id = I_partner_id
                 OR supplier   = I_supplier);

BEGIN

   O_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN','C_EXISTS','OBLIGATION',
                    'EXT INVC NO: '||I_ext_invc_no);
   open C_EXISTS;

   SQL_LIB.SET_MARK('FETCH','C_EXISTS','OBLIGATION',
                    'EXT INVC NO: '||I_ext_invc_no);
   fetch C_EXISTS into L_exists;

   if L_exists is NOT NULL then
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','OBLIGATION',
                    'EXT INVC NO: '||I_ext_invc_no);
   close C_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'OBLIGATION_SQL.EXT_INVC_VALIDATE',
                                            to_char(SQLCODE));
   return FALSE;
END EXT_INVC_VALIDATE;
--------------------------------------------------------------------------------
FUNCTION EXT_INVC_EXISTS (O_error_message      IN OUT   VARCHAR2,
                          O_exists             IN OUT   BOOLEAN,
                          I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                          I_obligation_level   IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                          I_key_value_1        IN       OBLIGATION.KEY_VALUE_1%TYPE,
                          I_key_value_2        IN       OBLIGATION.KEY_VALUE_2%TYPE,
                          I_key_value_3        IN       OBLIGATION.KEY_VALUE_3%TYPE,
                          I_key_value_4        IN       OBLIGATION.KEY_VALUE_4%TYPE,
                          I_key_value_5        IN       OBLIGATION.KEY_VALUE_5%TYPE,
                          I_key_value_6        IN       OBLIGATION.KEY_VALUE_6%TYPE,
                          I_ext_invc_no        IN       OBLIGATION.EXT_INVC_NO%TYPE)
   RETURN BOOLEAN IS

   L_exists   VARCHAR2(1) := NULL;

   cursor C_EXISTS_1 is
      select 'X'
        from obligation
       where ext_invc_no = I_ext_invc_no;

   cursor C_EXISTS_2 is
      select 'X'
        from obligation
       where obligation_key = I_obligation_key
         and ext_invc_no     = I_ext_invc_no;




   cursor C_EXISTS_3 is
      select 'X'
        from obligation
       where ext_invc_no = I_ext_invc_no
         and ((obligation_level in ('PO','CUST','POT')
               and key_value_1  = I_key_value_1
               and key_value_2 is NULL
               and key_value_3 is NULL
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level = 'POIT'
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3 is NULL
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level = 'TRVV'
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level in ('TRBL','TRCO','TRVVEP')
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4  = I_key_value_4
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level in ('TRVP','TRCPO','TRBLP')
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4  = I_key_value_4
               and key_value_5  = I_key_value_5
               and key_value_6 is NULL)
           or (obligation_level in ('TRCP','TRBP','CEDT')
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4  = I_key_value_4
               and key_value_5  = I_key_value_5
               and key_value_6  = I_key_value_6));

BEGIN

   O_exists := FALSE;
   if I_obligation_key is NULL and I_obligation_level is NULL then
      SQL_LIB.SET_MARK('open','C_EXISTS_1','OBLIGATION',
                       'EXT INVC NO: '||I_ext_invc_no);
      open C_EXISTS_1;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS_1','OBLIGATION',
                       'EXT INVC NO: '||I_ext_invc_no);
      fetch C_EXISTS_1 into L_exists;
      if C_EXISTS_1%FOUND then
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS_1','OBLIGATION',
                       'EXT INVC NO: '||I_ext_invc_no);
      close C_EXISTS_1;
   elsif I_obligation_key is not NULL then
      SQL_LIB.SET_MARK('open','C_EXISTS_2','OBLIGATION',
                       'EXT INVC NO: '||I_ext_invc_no);
      open C_EXISTS_2;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS_2','OBLIGATION',
                       'EXT INVC NO: '||I_ext_invc_no);
      fetch C_EXISTS_2 into L_exists;
      if C_EXISTS_2%FOUND then
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS_2','OBLIGATION',
                       'EXT INVC NO: '||I_ext_invc_no);
      close C_EXISTS_2;
   elsif I_obligation_level is not NULL then
      SQL_LIB.SET_MARK('open','C_EXISTS_3','OBLIGATION',
                       'EXT INVC NO: '||I_ext_invc_no);
      open C_EXISTS_3;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS_3','OBLIGATION',
                       'EXT INVC NO: '||I_ext_invc_no);
      fetch C_EXISTS_3 into L_exists;
      if C_EXISTS_3%FOUND then
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS_3','OBLIGATION',
                       'EXT INVC NO: '||I_ext_invc_no);
      close C_EXISTS_3;
   end if;

   if O_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('INV_REF_NO',NULL,NULL,NULL);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'OBLIGATION_SQL.EXT_INVC_EXISTS',
                                            to_char(SQLCODE));
   return FALSE;
END EXT_INVC_EXISTS;
--------------------------------------------------------------------------------
FUNCTION AUTH_EXISTS (O_error_message      IN OUT   VARCHAR2,
                      O_exists             IN OUT   BOOLEAN,
                      I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                      I_obligation_level   IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                      I_key_value_1        IN       OBLIGATION.KEY_VALUE_1%TYPE,
                      I_key_value_2        IN       OBLIGATION.KEY_VALUE_2%TYPE,
                      I_key_value_3        IN       OBLIGATION.KEY_VALUE_3%TYPE,
                      I_key_value_4        IN       OBLIGATION.KEY_VALUE_4%TYPE,
                      I_key_value_5        IN       OBLIGATION.KEY_VALUE_5%TYPE,
                      I_key_value_6        IN       OBLIGATION.KEY_VALUE_6%TYPE,
                      I_check_auth_no      IN       OBLIGATION.CHECK_AUTH_NO%TYPE)
   RETURN BOOLEAN IS

   L_exists   VARCHAR2(1) := NULL;

   cursor C_EXISTS_1 is
      select 'X'
        from obligation
       where check_auth_no = I_check_auth_no;

   cursor C_EXISTS_2 is
      select 'X'
        from obligation
       where obligation_key = I_obligation_key
         and check_auth_no  = I_check_auth_no;


   cursor C_EXISTS_3 is
      select 'X'
        from obligation
       where check_auth_no = I_check_auth_no
         and ((obligation_level in ('PO','CUST','POT')
               and key_value_1  = I_key_value_1
               and key_value_2 is NULL
               and key_value_3 is NULL
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level = 'POIT'
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3 is NULL
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level = 'TRVV'
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level in ('TRBL','TRCO','TRVVEP')
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4  = I_key_value_4
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level in ('TRVP','TRCPO','TRBLP')
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4  = I_key_value_4
               and key_value_5  = I_key_value_5
               and key_value_6 is NULL)
           or (obligation_level in ('TRCP','TRBP','CEDT')
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4  = I_key_value_4
               and key_value_5  = I_key_value_5
               and key_value_6  = I_key_value_6));


BEGIN

   O_exists := FALSE;
   if I_obligation_key is NULL and I_obligation_level is NULL then
      SQL_LIB.SET_MARK('open','C_EXISTS_1','OBLIGATION',
                       'CHECK AUTH NO: '||I_check_auth_no);
      open C_EXISTS_1;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS_1','OBLIGATION',
                       'CHECK AUTH NO: '||I_check_auth_no);
      fetch C_EXISTS_1 into L_exists;
      if C_EXISTS_1%FOUND then
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS_1','OBLIGATION',
                       'CHECK AUTH NO: '||I_check_auth_no);
      close C_EXISTS_1;
   elsif I_obligation_key is not NULL then
      SQL_LIB.SET_MARK('open','C_EXISTS_2','OBLIGATION',
                       'CHECK AUTH NO: '||I_check_auth_no);
      open C_EXISTS_2;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS_2','OBLIGATION',
                       'CHECK AUTH NO: '||I_check_auth_no);
      fetch C_EXISTS_2 into L_exists;
      if C_EXISTS_2%FOUND then
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS_2','OBLIGATION',
                       'CHECK AUTH NO: '||I_check_auth_no);
      close C_EXISTS_2;
   elsif I_obligation_level is not NULL then
      SQL_LIB.SET_MARK('open','C_EXISTS_3','OBLIGATION',
                       'CHECK AUTH NO: '||I_check_auth_no);
      open C_EXISTS_3;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS_3','OBLIGATION',
                       'CHECK AUTH NO: '||I_check_auth_no);
      fetch C_EXISTS_3 into L_exists;
      if C_EXISTS_3%FOUND then
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS_3','OBLIGATION',
                       'CHECK AUTH NO: '||I_check_auth_no);
      close C_EXISTS_3;
   end if;

   if O_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CHECK_AUTH_NO',NULL,NULL,NULL);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'OBLIGATION_SQL.AUTH_EXISTS',
                                            to_char(SQLCODE));
   return FALSE;
END AUTH_EXISTS;
--------------------------------------------------------------------------------
FUNCTION COMP_EXIST (O_error_message      IN OUT   VARCHAR2,
                     O_exists             IN OUT   BOOLEAN,
                     I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                     I_obligation_level   IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                     I_key_value_1        IN       OBLIGATION.KEY_VALUE_1%TYPE,
                     I_key_value_2        IN       OBLIGATION.KEY_VALUE_2%TYPE,
                     I_key_value_3        IN       OBLIGATION.KEY_VALUE_3%TYPE,
                     I_key_value_4        IN       OBLIGATION.KEY_VALUE_4%TYPE,
                     I_key_value_5        IN       OBLIGATION.KEY_VALUE_5%TYPE,
                     I_key_value_6        IN       OBLIGATION.KEY_VALUE_6%TYPE,
                     I_comp_id            IN       OBLIGATION_COMP.COMP_ID%TYPE)
   RETURN BOOLEAN IS

   L_exists   VARCHAR2(1) := NULL;

   cursor C_EXISTS_1 is
      select 'X'
        from obligation_comp
       where obligation_key = I_obligation_key
         and comp_id = NVL(I_comp_id, comp_id);




   cursor C_EXISTS_2 is
      select 'X'
        from obligation_comp oc, obligation o
       where oc.obligation_key = o.obligation_key
         and comp_id = NVL(I_comp_id, comp_id)
         and ((obligation_level in ('PO','CUST','POT')
               and key_value_1  = I_key_value_1
               and key_value_2 is NULL
               and key_value_3 is NULL
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level = 'POIT'
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3 is NULL
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level = 'TRVV'
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level in ('TRBL','TRCO','TRVVEP')
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4  = I_key_value_4
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level in ('TRVP','TRCPO','TRBLP')
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4  = I_key_value_4
               and key_value_5  = I_key_value_5
               and key_value_6 is NULL)
           or (obligation_level in ('TRCP','TRBP','CEDT')
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4  = I_key_value_4
               and key_value_5  = I_key_value_5
               and key_value_6  = I_key_value_6));


BEGIN
   O_exists := FALSE;

   if I_obligation_key is not NULL then
      SQL_LIB.SET_MARK('open','C_EXISTS_1','OBLIGATION_COMP',
                       'OBLIGATION KEY: '||to_char(I_obligation_key));
      open C_EXISTS_1;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS_1','OBLIGATION_COMP',
                       'OBLIGATION KEY: '||to_char(I_obligation_key));
      fetch C_EXISTS_1 into L_exists;
      if C_EXISTS_1%FOUND then
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS_1','OBLIGATION_COMP',
                       'OBLIGATION KEY: '||to_char(I_obligation_key));
      close C_EXISTS_1;
   elsif I_obligation_level is not NULL then
      SQL_LIB.SET_MARK('open','C_EXISTS_2','OBLIGATION_COMP',
                       'OBLIGATION KEY: '||to_char(I_obligation_key));
      open C_EXISTS_2;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS_2','OBLIGATION_COMP',
                       'OBLIGATION KEY: '||to_char(I_obligation_key));
      fetch C_EXISTS_2 into L_exists;
      if C_EXISTS_2%FOUND then
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS_2','OBLIGATION_COMP',
                       'OBLIGATION KEY: '||to_char(I_obligation_key));
      close C_EXISTS_2;
   end if;

   if O_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('NO_COMPONENTS',NULL,NULL,NULL);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'OBLIGATION_SQL.COMP_EXIST',
                                            to_char(SQLCODE));
   return FALSE;
END COMP_EXIST;
--------------------------------------------------------------------------------
FUNCTION LOC_EXIST (O_error_message      IN OUT   VARCHAR2,
                    O_exists             IN OUT   BOOLEAN,
                    I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                    I_obligation_level   IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                    I_key_value_1        IN       OBLIGATION.KEY_VALUE_1%TYPE,
                    I_key_value_2        IN       OBLIGATION.KEY_VALUE_2%TYPE,
                    I_key_value_3        IN       OBLIGATION.KEY_VALUE_3%TYPE,
                    I_key_value_4        IN       OBLIGATION.KEY_VALUE_4%TYPE,
                    I_key_value_5        IN       OBLIGATION.KEY_VALUE_5%TYPE,
                    I_key_value_6        IN       OBLIGATION.KEY_VALUE_6%TYPE,
                    I_comp_id            IN       OBLIGATION_COMP.COMP_ID%TYPE,
                    I_location           IN       OBLIGATION_COMP_LOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_exists   VARCHAR2(1) := NULL;

   cursor C_EXISTS_1 is
      select 'X'
        from obligation_comp_loc
       where obligation_key = I_obligation_key
         and comp_id        = I_comp_id
         and location       = NVL(I_location, location);




   cursor C_EXISTS_2 is
      select 'X'
        from obligation_comp_loc ocl, obligation o
       where ocl.obligation_key = o.obligation_key
         and ocl.comp_id        = I_comp_id
         and location           = NVL(I_location, location)
         and ((obligation_level in ('PO','CUST','POT')
               and key_value_1  = I_key_value_1
               and key_value_2 is NULL
               and key_value_3 is NULL
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level = 'POIT'
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3 is NULL
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level = 'TRVV'
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level in ('TRBL','TRCO','TRVVEP')
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4  = I_key_value_4
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level in ('TRVP','TRCPO','TRBLP')
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4  = I_key_value_4
               and key_value_5  = I_key_value_5
               and key_value_6 is NULL)
           or (obligation_level in ('TRCP','TRBP','CEDT')
               and key_value_1  = I_key_value_1
               and key_value_2  = I_key_value_2
               and key_value_3  = I_key_value_3
               and key_value_4  = I_key_value_4
               and key_value_5  = I_key_value_5
               and key_value_6  = I_key_value_6));

BEGIN
   O_exists := FALSE;
   if I_obligation_key is not NULL then
      SQL_LIB.SET_MARK('open','C_EXISTS_1','OBLIGATION_COMP_LOC',
                       'OBLIGATION KEY: '||to_char(I_obligation_key));
      open C_EXISTS_1;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS_1','OBLIGATION_COMP_LOC',
                       'OBLIGATION KEY: '||to_char(I_obligation_key));
      fetch C_EXISTS_1 into L_exists;
      if C_EXISTS_1%FOUND then
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS_1','OBLIGATION_COMP_LOC',
                       'OBLIGATION KEY: '||to_char(I_obligation_key));
      close C_EXISTS_1;
   elsif I_obligation_level is not NULL then
      SQL_LIB.SET_MARK('open','C_EXISTS_2','OBLIGATION_COMP_LOC',
                       'OBLIGATION KEY: '||to_char(I_obligation_key));
      open C_EXISTS_2;
      SQL_LIB.SET_MARK('FETCH','C_EXISTS_2','OBLIGATION_COMP_LOC',
                       'OBLIGATION KEY: '||to_char(I_obligation_key));
      fetch C_EXISTS_2 into L_exists;
      if C_EXISTS_2%FOUND then
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_EXISTS_2','OBLIGATION_COMP_LOC',
                       'OBLIGATION KEY: '||to_char(I_obligation_key));
      close C_EXISTS_2;
   end if;

   if O_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('NO_LOCS',NULL,NULL,NULL);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'OBLIGATION_SQL.LOC_EXIST',
                                            to_char(SQLCODE));
   return FALSE;
END LOC_EXIST;
--------------------------------------------------------------------------------
FUNCTION DELETE_COMPS (O_error_message    IN OUT   VARCHAR2,
                       I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN IS

BEGIN
   delete from obligation_comp
    where obligation_key = I_obligation_key;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'OBLIGATION_SQL.DELETE_COMPS',
                                             to_char(SQLCODE));
   return FALSE;
END DELETE_COMPS;
--------------------------------------------------------------------------------
FUNCTION DELETE_LOCS (O_error_message    IN OUT   VARCHAR2,
                      I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                      I_comp_id          IN       OBLIGATION_COMP.COMP_ID%TYPE)
   RETURN BOOLEAN IS
BEGIN
   delete from obligation_comp_loc
    where obligation_key = I_obligation_key
      and comp_id        = NVL(I_comp_id, comp_id);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'OBLIGATION_SQL.DELETE_LOCS',
                                             to_char(SQLCODE));
   return FALSE;
END DELETE_LOCS;
--------------------------------------------------------------------------------
FUNCTION LOCK_OBLIGATION (O_error_message    IN OUT   VARCHAR2,
                          I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                          I_obligation_level IN       OBLIGATION.OBLIGATION_LEVEL%TYPE)
   RETURN BOOLEAN  IS

   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_OBLIGATION is
      select 'x'
        from obligation
       where obligation_key = I_obligation_key
         for update nowait;

BEGIN

   L_table := 'OBLIGATION';

   SQL_LIB.SET_MARK('OPEN','C_LOCK_OBLIGATION','OBLIGATION','OBLIGATION KEY: '||to_char(I_obligation_key)||', '||
                                                            'OBLIGATION LEVEL: '||to_char(I_obligation_level));
   open C_LOCK_OBLIGATION;

   SQL_LIB.SET_MARK('CLOSE','C_LOCK_OBLIGATION','OBLIGATION','OBLIGATION KEY: '||to_char(I_obligation_key)||', '||
                                                             'OBLIGATION LEVEL: '||to_char(I_obligation_level));
   close C_LOCK_OBLIGATION;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', L_table,
                                             to_char(I_obligation_key),
                                             I_obligation_level);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'OBLIGATION_SQL.LOCK_OBLIGATION',
                                             to_char(SQLCODE));
   return FALSE;
END LOCK_OBLIGATION;
--------------------------------------------------------------------------------
FUNCTION LOCK_DETAILS (O_error_message    IN OUT   VARCHAR2,
                       I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                       I_lock_table       IN       VARCHAR2,
                       I_comp_id          IN       OBLIGATION_COMP.COMP_ID%TYPE)
   RETURN BOOLEAN IS

   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_COMPS is
      select 'x'
        from obligation_comp
       where obligation_key = I_obligation_key
         for update nowait;

   cursor C_LOCK_LOCS is
      select 'x'
        from obligation_comp_loc
       where obligation_key = I_obligation_key
         and comp_id        = NVL(I_comp_id, comp_id)
         for update nowait;
BEGIN

   if I_lock_table = 'OBLIGATION_COMP_LOC'
      or I_lock_table is NULL then
      L_table := 'OBLIGATION_COMP_LOC';
      open C_LOCK_LOCS;
      close C_LOCK_LOCS;
   end if;
   ---
   if I_lock_table = 'OBLIGATION_COMP'
      or I_lock_table is NULL then
      L_table := 'OBLIGATION_COMP';
      open C_LOCK_COMPS;
      close C_LOCK_COMPS;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', L_table,
                                             to_char(I_obligation_key),
                                             I_comp_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'OBLIGATION_SQL.LOCK_DETAILS',
                                             to_char(SQLCODE));
   return FALSE;
END LOCK_DETAILS;
--------------------------------------------------------------------------------
FUNCTION DELETE_ALC(O_error_message    IN OUT   VARCHAR2,
                    I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60):= 'OBLIGATION_SQL.DELETE_ALC';
   L_order_no        ORDHEAD.ORDER_NO%TYPE;
   ---
   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_GET_ORD is
      select distinct order_no
        from alc_head
       where obligation_key = I_obligation_key;

   cursor C_LOCK_ALC_COMP_LOC is
      select 'x'
        from alc_comp_loc
       where order_no = L_order_no
         and seq_no in (select seq_no
                          from alc_head
                         where obligation_key = I_obligation_key
                           and order_no       = L_order_no)
         for update nowait;

   cursor C_LOCK_ALC_HEAD is
      select 'x'
        from alc_head
       where obligation_key = I_obligation_key
         for update nowait;

BEGIN
   FOR C_rec in C_GET_ORD LOOP
      L_order_no := C_rec.order_no;
      ---
      L_table := 'ALC_COMP_LOC';
      SQL_LIB.SET_MARK('open','C_LOC_ALC_COMP_LOC','ALC_COMP_LOC','OBLIGATION KEY: '||to_char(I_obligation_key)||
                                                                  ', ORDER: '||to_char(L_order_no));
      open C_LOCK_ALC_COMP_LOC;
      SQL_LIB.SET_MARK('CLOSE','C_LOC_ALC_COMP_LOC','ALC_COMP_LOC','OBLIGATION KEY: '||to_char(I_obligation_key)||
                                                                   ', ORDER: '||to_char(L_order_no));
      close C_LOCK_ALC_COMP_LOC;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'ALC_COMP_LOC','OBLIGATION KEY: '||to_char(I_obligation_key)||
                                                    ', ORDER: '||to_char(L_order_no));
      delete from alc_comp_loc
            where order_no = L_order_no
              and seq_no in (select seq_no
                               from alc_head
                              where obligation_key = I_obligation_key
                                and order_no       = L_order_no);
      -- Expense comps will be reallocated at the end of the process
      if ALC_ALLOC_SQL.ADD_PO_TO_QUEUE(O_error_message,
                                       L_order_no) = FALSE then
         return FALSE;
      end if;
   END LOOP;
   ---
   L_order_no := NULL;
   L_table    := 'ALC_HEAD';
   ---
   SQL_LIB.SET_MARK('open','C_LOC_ALC_HEAD','ALC_HEAD','OBLIGATION KEY: '||to_char(I_obligation_key));
   open C_LOCK_ALC_HEAD;
   SQL_LIB.SET_MARK('CLOSE','C_LOC_ALC_HEAD','ALC_HEAD','OBLIGATION KEY: '||to_char(I_obligation_key));
   close C_LOCK_ALC_HEAD;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'ALC_HEAD','OBLIGATION KEY: '||to_char(I_obligation_key));
   delete from alc_head
         where obligation_key = I_obligation_key;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_obligation_key),
                                            to_char(L_order_no));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_ALC;
--------------------------------------------------------------------------------
FUNCTION CHECK_INV_LOCS(O_error_message      IN OUT   VARCHAR2,
                        O_inv_loc_exists     IN OUT   BOOLEAN,
                        I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                        I_obligation_level   IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                        I_key_value_1        IN       OBLIGATION.KEY_VALUE_1%TYPE,
                        I_key_value_2        IN       OBLIGATION.KEY_VALUE_2%TYPE,
                        I_comp_id            IN       OBLIGATION_COMP.COMP_ID%TYPE)
   RETURN BOOLEAN IS
   L_program   VARCHAR2(62)                      := 'OBLIGATION_SQL.CHECK_INV_LOCS';
   L_exists    VARCHAR2(1)                       := NULL;
   L_location  OBLIGATION_COMP_LOC.LOCATION%TYPE;

   cursor C_OBL_LOC is
      select distinct location
        from obligation_comp_loc
       where obligation_key   = I_obligation_key
         and comp_id          = I_comp_id;

   cursor C_PO_LOC is
      select 'x'
        from ordloc
       where order_no = I_key_value_1
         and location = L_location;

   cursor C_POIT_LOC is
      select 'x'
        from ordloc
       where order_no = I_key_value_1
         and item     = I_key_value_2
         and location = L_location;
BEGIN
   O_inv_loc_exists := FALSE;

/*For the passed-in obligation key check that each distinct location on obligation_comp_loc
  table actually exists on order.*/

   for C_rec in C_OBL_LOC loop
      L_location := C_rec.location;
      if I_obligation_level = 'PO' then
         SQL_LIB.SET_MARK('open','C_PO_LOC','ORDLOC',
                          'Order_no: '||I_key_value_1||', Location: '||to_char(L_location));
         open C_PO_LOC;
         SQL_LIB.SET_MARK('FETCH','C_PO_LOC','ORDLOC',
                          'Order_no: '||I_key_value_1||', Location: '||to_char(L_location));
         fetch C_PO_LOC into L_exists;
         if C_PO_LOC%NOTFOUND then
            O_inv_loc_exists := TRUE;
            SQL_LIB.SET_MARK('CLOSE','C_PO_LOC','ORDLOC',
                          'Order_no: '||I_key_value_1||', Location: '||to_char(L_location));
            close C_PO_LOC;
            return TRUE;
         end if;
         SQL_LIB.SET_MARK('CLOSE','C_PO_LOC','ORDLOC',
                          'Order_no: '||I_key_value_1||', Location: '||to_char(L_location));
         close C_PO_LOC;
      elsif I_obligation_level = 'POIT' then
         SQL_LIB.SET_MARK('open','C_POIT_LOC','ORDLOC',
                          'Order_no: '||I_key_value_1||', Item: '||I_key_value_2);
         open C_POIT_LOC;
         SQL_LIB.SET_MARK('FETCH','C_POIT_LOC','ORDLOC',
                          'Order_no: '||I_key_value_1||', Item: '||I_key_value_2);
         fetch C_POIT_LOC into L_exists;
         if C_POIT_LOC%NOTFOUND then
            O_inv_loc_exists := TRUE;
            SQL_LIB.SET_MARK('CLOSE','C_POIT_LOC','ORDLOC',
                             'Order_no: '||I_key_value_1||', Item: '||I_key_value_2);
            close C_POIT_LOC;
            return TRUE;
         end if;
         SQL_LIB.SET_MARK('CLOSE','C_POIT_LOC','ORDLOC',
                          'Order_no: '||I_key_value_1||', Item: '||I_key_value_2);
         close C_POIT_LOC;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END CHECK_INV_LOCS;
--------------------------------------------------------------------------------
FUNCTION OBL_EXISTS(O_error_message    IN OUT   VARCHAR2,
                    O_exists           IN OUT   BOOLEAN,
                    I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN IS
   L_exists  VARCHAR2(1);
   L_program VARCHAR2(50) := 'OBLIGATION_SQL.OBL_EXISTS';
   cursor C_CHECK_OBL is
      select 'Y'
        from obligation
       where obligation_key = I_obligation_key;

BEGIN
   SQL_LIB.SET_MARK('open','C_CHECK_OBL','OBLIGATION','Obligation key '|| I_obligation_key);
     open C_CHECK_OBL;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_OBL','OBLIGATION','Obligation key '|| I_obligation_key);
     fetch C_CHECK_OBL into L_exists;
   if C_CHECK_OBL%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_OBL','OBLIGATION','Obligation key '|| I_obligation_key);
      close C_CHECK_OBL;
      O_error_message := SQL_LIB.CREATE_MSG('ENTITY_DOES_NOT_EXIST',NULL,NULL,NULL);
      O_exists := FALSE;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_OBL','OBLIGATION','Obligation key '|| I_obligation_key);
   close C_CHECK_OBL;
   O_exists := TRUE;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END OBL_EXISTS;
--------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------
FUNCTION OBL_FILTER_LIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diff            IN OUT VARCHAR2,
                         I_obligation_key  IN     OBLIGATION_COMP_LOC.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN IS

   cursor C_COUNT_OBL_COMP_LOC is
      select count(obligation_key)
        from obligation_comp_loc
       where obligation_key = I_obligation_key;
   cursor C_COUNT_V_OBL_COMP_LOC is
      select count(obligation_key)
        from v_obligation_comp_loc
       where obligation_key = I_obligation_key;
   L_program      VARCHAR2(60)   := 'OBLIGATION_SQL.OBL_FILTER_LIST';
   L_obligation   NUMBER(6)      := -765547;
   L_vobligation  NUMBER(6)      := -987436;
BEGIN

   if I_obligation_key is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_obligation_key',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   -- get the number of records in OBLIGATION_COMP_LOC
   SQL_LIB.SET_MARK('open',
                    'C_COUNT_OBL_COMP_LOC',
                    'OBLIGATION_COMP_LOC',
                     NULL);
   open C_COUNT_OBL_COMP_LOC;
   SQL_LIB.SET_MARK('FETCH',
                    'C_COUNT_OBL_COMP_LOC',
                    'OBLIGATION_COMP_LOC',
                     NULL);
   fetch C_COUNT_OBL_COMP_LOC into L_obligation;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_COUNT_OBL_COMP_LOC',
                    'OBLIGATION_COMP_LOC',
                     NULL);
   close C_COUNT_OBL_COMP_LOC;
   -- get the number of records in V_OBLIGATION_COMP_LOC
   SQL_LIB.SET_MARK('open',
                    'C_COUNT_V_OBL_COMP_LOC',
                    'V_OBLIGATION_COMP_LOC',
                     NULL);
   open C_COUNT_V_OBL_COMP_LOC;
   SQL_LIB.SET_MARK('FETCH',
                    'C_COUNT_V_OBL_COMP_LOC',
                    'V_OBLIGATION_COMP_LOC',
                     NULL);
   fetch C_COUNT_V_OBL_COMP_LOC into L_vobligation;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_COUNT_V_OBL_COMP_LOC',
                    'V_OBLIGATION_COMP_LOC',
                     NULL);
   close C_COUNT_V_OBL_COMP_LOC;
   if NVL(L_obligation,0) != NVL(L_vobligation,0) then
     O_diff := 'Y';
   else
     O_diff := 'N';
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'L_program',
                                            to_char(SQLCODE));
      return FALSE;
END OBL_FILTER_LIST;
------------------------------------------------------------------------------------------------

FUNCTION GET_DEFAULT_QTY(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_qty                IN OUT   TRANSPORTATION.ITEM_QTY%TYPE,
                         O_item_qty_uom            IN OUT   TRANSPORTATION.ITEM_QTY_UOM%TYPE,
                         I_obligation_level        IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                         I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                         I_voyage_id               IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                         I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                         I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE,
                         I_asn                     IN       SHIPMENT.ASN%TYPE,
                         I_item                    IN       TRANSPORTATION.ITEM%TYPE,
                         I_container_id            IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                         I_bl_awb_id               IN       TRANSPORTATION.BL_AWB_ID%TYPE,
                         I_entry_no                IN       CE_HEAD.ENTRY_NO%TYPE)
   RETURN BOOLEAN IS

   L_item_qty_uom         TRANSPORTATION.ITEM_QTY_UOM%TYPE;
   L_item_qty             TRANSPORTATION.ITEM_QTY%TYPE;
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_uom_count            NUMBER(10);
   L_program              VARCHAR2(50) := 'OBLIGATION_SQL.GET_DEFAULT_QTY';
   I_calling_function     VARCHAR2(30) := 'OBLIGATION';

   ------------------------------------
   -- Define ref cur types
   ------------------------------------
   TYPE                   RefCurTyp IS REF CURSOR;  -- define weak REF CURSOR type
   -- Trans Qty Ref Cur
   TranQtyCur             RefCurTyp;
   TranSkuCur             RefCurTyp;
   -- Trans Uom Ref Cur
   TranUomCur             RefCurTyp;

   v_tran_sql                  VARCHAR2(5000) ;
   v_sku_sql                  VARCHAR2(5000) ;
   v_uom_sql              VARCHAR2(5000) := 'select NVL(count(distinct tra.item_qty_uom),0) from transportation tra where 1=1 ';

   L_format               VARCHAR2(8)   := 'DD/MM/RR';
   L_etd                  VARCHAR2(8)   := to_char(I_estimated_depart_date,'DD/MM/RR');

   L_v_sql_item           item_master.item%TYPE;
   L_v_sql_item_qty       transportation.item_qty%TYPE;
   L_v_sql_item_qty_uom   transportation.item_qty_uom%TYPE;
   L_v_sql_origin_country_id
                          TRANSPORTATION.ORIGIN_COUNTRY_ID%TYPE;
   L_v_sql_supplier       ORDHEAD.SUPPLIER%TYPE;
   v_found                BOOLEAN:= false;

   cursor C_ASN_QTY is
      select k.item,
             NVL(SUM(k.qty_received),NVL(SUM(k.qty_expected),0)) item_qty,
             max(im.standard_uom) item_qty_uom
        from shipment s,
             shipsku k,
             item_master im
       where s.shipment = k.shipment
         and s.asn      = I_asn
         and s.order_no = NVL(I_order_no,s.order_no)
         and NVL(k.carton,-1)   = NVL(I_container_id, NVL(k.carton,-1))
         and k.item     = im.item
         group by k.item;

   cursor C_ASN_UOM is
      select NVL(count(distinct im.standard_uom),0)
        from shipment s,
             shipsku k,
             item_master im
       where s.shipment = k.shipment
         and s.asn      = I_asn
         and s.order_no = NVL(I_order_no,s.order_no)
         and NVL(k.carton,-1)   = NVL(I_container_id, NVL(k.carton,-1))
         and k.item     = im.item;

   cursor C_ORDER_QTY is
      select k.item,NVL(SUM(k.qty_received),0) item_qty,
             max(im.standard_uom) item_qty_uom
        from shipment s,
             shipsku k,
             item_master im
       where s.shipment = k.shipment
         and s.order_no = I_order_no
         and k.item     = NVL(I_item, k.item)
         and k.item     = im.item
         group by k.item;

   cursor C_ORDER_UOM is
      select NVL(count(distinct im.standard_uom),0)
        from ordloc ol,
             item_master im
       where ol.order_no = I_order_no
         and ol.item     = NVL(I_item, ol.item)
         and ol.item     = im.item;

   cursor C_CUST_QTY is
      select coi.item,
             coi.manifest_item_qty item_qty,
             coi.manifest_item_qty_uom item_qty_uom
        from ce_ord_item coi,
             ce_head     ch
       where ch.entry_no = I_entry_no
         and ch.ce_id    = coi.ce_id;

   cursor C_CUST_UOM is
      select NVL(count(distinct coi.manifest_item_qty_uom),0)
        from ce_ord_item coi,
             ce_head     ch
       where ch.entry_no = I_entry_no
         and ch.ce_id    = coi.ce_id;

BEGIN
   if I_obligation_level = 'CUST' then
      if I_entry_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_entry_no',
                                                L_program,
                                                NULL);
         return FALSE;
      end if;
   end if;
   if I_obligation_level in ('PO','POIT','POT') then
      if I_order_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_order_no',
                                                L_program,
                                                NULL);
         return FALSE;
      end if;
   end if;
   O_item_qty := 0;
   L_item_qty := 0;
   if O_item_qty_uom is not NULL then
      L_item_qty_uom := O_item_qty_uom;
   else
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_obligation_level in ('TRBL','TRBLP','TRBP',
                             'TRCO','TRCP','TRCPO',
                             'TRVP','TRVV','TRVVEP','POT') then

      if L_item_qty_uom is NULL then

              SQL_LIB.SET_MARK('open',
                      'C_TRANS_UOM',
                          'transportation',
                          'voyage_flt_id='||I_voyage_id||' estimated_depart_date='||I_estimated_depart_date||
                          ' order_no='||I_order_no||' item='||I_item||' container_id='||I_container_id||
                          ' bl_awb_id='||I_bl_awb_id);
              open TranUomCur FOR v_uom_sql;
              SQL_LIB.SET_MARK('FETCH',
                          'C_TRANS_UOM',
                          'transportation',
                          'voyage_flt_id='||I_voyage_id||' estimated_depart_date='||I_estimated_depart_date||
                          ' order_no='||I_order_no||' item='||I_item||' container_id='||I_container_id||
                          ' bl_awb_id='||I_bl_awb_id);
               fetch TranUomCur into L_uom_count;
               SQL_LIB.SET_MARK('CLOSE',
                          'C_TRANS_UOM',
                          'transportation',
                          'voyage_flt_id='||I_voyage_id||' estimated_depart_date='||I_estimated_depart_date||
                          ' order_no='||I_order_no||' item='||I_item||' container_id='||I_container_id||
                          ' bl_awb_id='||I_bl_awb_id);
              close TranUomCur;
         ---

            if L_uom_count > 1 then
               L_item_qty_uom := L_system_options_row.default_standard_uom;

             end if;
        end if;
      ---
        if GET_TRAN_QTY (O_error_message ,
                       L_item_qty   , 
                       L_item_qty_uom ,
                       I_vessel_id ,
                       I_voyage_id,
                       I_estimated_depart_date,
                       I_order_no , 
                       I_item ,     
                       I_container_id,
                       I_bl_awb_id ) = FALSE then
                  return FALSE;         
        end if;  
        O_item_qty_uom := L_item_qty_uom;
        O_item_qty     :=  L_item_qty ;   

   elsif I_obligation_level in ('PO','POIT') then
      if L_item_qty_uom is NULL then

         SQL_LIB.SET_MARK('open',
                          'C_ORDER_UOM',
                          'ordloc, item_master',
                          'Order_no: '||I_order_no||','||'Item: '||I_item);
         open C_ORDER_UOM;
         SQL_LIB.SET_MARK('FETCH',
                          'C_ORDER_UOM',
                          'ordloc, item_master',
                          'Order_no: '||I_order_no||','||'Item: '||I_item);
         fetch C_ORDER_UOM into L_uom_count;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORDER_UOM',
                          'ordloc, item_master',
                          'Order_no: '||I_order_no||','||'Item: '||I_item);
         close C_ORDER_UOM;

         if L_uom_count > 1 then
            L_item_qty_uom := L_system_options_row.default_standard_uom;
         end if;

      end if;

      for C1 in C_ORDER_QTY LOOP
         --Convert item quantity
         if L_item_qty_uom is not NULL then
            if UOM_SQL.CONVERT(O_error_message,
                               L_item_qty,
                               L_item_qty_uom,
                               C1.item_qty,
                               C1.item_qty_uom,
                               NVL(I_item,C1.item),
                               NULL,
                               NULL,
                               I_calling_function) = FALSE then
               return FALSE;
            end if;
            O_item_qty_uom := L_item_qty_uom;
         else
            O_item_qty_uom := C1.item_qty_uom;
            L_item_qty := C1.item_qty;
         end if;
         ---
         O_item_qty := O_item_qty + L_item_qty;
         L_item_qty := 0;

      END LOOP;

   elsif I_obligation_level in ('ASN','ASNP','ASNC') then

      if L_item_qty_uom is NULL then

         SQL_LIB.SET_MARK('open',
                          'C_ASN_UOM',
                          'shipment, shipsku, item_master',
                          'ASN: '||I_asn||','||'Order_no: '||I_order_no);
         open C_ASN_UOM;
         SQL_LIB.SET_MARK('FETCH',
                          'C_ASN_UOM',
                          'shipment, shipsku, item_master',
                          'ASN: '||I_asn||','||'Order_no: '||I_order_no);
         fetch C_ASN_UOM into L_uom_count;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_ASN_UOM',
                          'shipment, shipsku, item_master',
                          'ASN: '||I_asn||','||'Order_no: '||I_order_no);
         close C_ASN_UOM;

         if L_uom_count > 1 then
            L_item_qty_uom := L_system_options_row.default_standard_uom;
         end if;

      end if;

      for C1 in C_ASN_QTY LOOP
         --Convert item quantity
         if L_item_qty_uom is not NULL then
            if UOM_SQL.CONVERT(O_error_message,
                               L_item_qty,
                               L_item_qty_uom,
                               C1.item_qty,
                               C1.item_qty_uom,
                               NVL(I_item,C1.item),
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
            O_item_qty_uom := L_item_qty_uom;
         else
            O_item_qty_uom := C1.item_qty_uom;
            L_item_qty := C1.item_qty;
         end if;
         ---
         O_item_qty := O_item_qty + L_item_qty;
         L_item_qty := 0;

      END LOOP;

   elsif I_obligation_level = 'CUST' then

      if L_item_qty_uom is NULL then

         SQL_LIB.SET_MARK('open',
                          'C_CUST_UOM',
                          'ce_ord_item, ce_head',
                          'Entry_no: '||I_entry_no);
         open C_CUST_UOM;
         SQL_LIB.SET_MARK('FETCH',
                          'C_CUST_UOM',
                          'ce_ord_item, ce_head',
                          'Entry_no: '||I_entry_no);
         fetch C_CUST_UOM into L_uom_count;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CUST_UOM',
                          'ce_ord_item, ce_head',
                          'Entry_no: '||I_entry_no);
         close C_CUST_UOM;
         if L_uom_count > 1 then
            L_item_qty_uom := L_system_options_row.default_standard_uom;
         end if;

      end if;

      for C1 in C_CUST_QTY LOOP
         if L_item_qty_uom is not NULL then
            -- Convert item quantity
            if UOM_SQL.CONVERT(O_error_message,
                               L_item_qty,
                               L_item_qty_uom,
                               C1.item_qty,
                               C1.item_qty_uom,
                               NVL(I_item,C1.item),
                               NULL,
                               NULL,
                               I_calling_function) = FALSE then
               return FALSE;
            end if;
            O_item_qty_uom := L_item_qty_uom;
         else
            O_item_qty_uom := C1.item_qty_uom;
            L_item_qty := C1.item_qty;
         end if;

         O_item_qty := O_item_qty + L_item_qty;
         L_item_qty := 0;

      END LOOP;

   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END GET_DEFAULT_QTY;
------------------------------------------------------------------------------------------------
FUNCTION GET_TRAN_QTY (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_item_qty                IN OUT   TRANSPORTATION.ITEM_QTY%TYPE, 
                       O_item_qty_uom            IN OUT   TRANSPORTATION.ITEM_QTY_UOM%TYPE,
                       I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                       I_voyage_id               IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                       I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                       I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE, 
                       I_item                    IN       TRANSPORTATION.ITEM%TYPE,     
                       I_container_id            IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                       I_bl_awb_id               IN       TRANSPORTATION.BL_AWB_ID%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'OBLIGATION_SQL.GET_TRAN_QTY';
   L_origin_country_id TRANSPORTATION.ORIGIN_COUNTRY_ID%TYPE;
   L_supplier       ORDHEAD.SUPPLIER%TYPE;
   L_item           TRANSPORTATION.ITEM%TYPE;
   L_item_qty       TRANSPORTATION.ITEM_QTY%TYPE := NULL;
   v_found          BOOLEAN := FALSE;
   L_etd           TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE := NULL;
   L_item_qty_uom   TRANSPORTATION.ITEM_QTY_UOM%TYPE := O_item_qty_uom;
   L_trnsprt_method RTM_UNIT_OPTIONS.RTM_TRNSPRT_OBL_ALLOC_METHOD%TYPE;
   --
   CURSOR C_TRNSPRT_METHOD IS
      select RTM_TRNSPRT_OBL_ALLOC_METHOD
        from RTM_UNIT_OPTIONS;
   --
   CURSOR C_TRAN_BUILD IS
   with tran_ord as
      ( select tra.order_no,
               tra.item,
               nvl(tra.item_qty,0) item_qty, 
               tra.item_qty_uom,
               tra.origin_country_id,
               ohe.supplier,
               tra.vessel_id,
               tra.voyage_flt_id,
               tra.estimated_depart_date,
               tra.transportation_id
          from ordhead        ohe,
               transportation tra
         where tra.order_no                = ohe.order_no(+)
           and  NVL(tra.vessel_id,-1)             = NVL(I_vessel_id,NVL(tra.vessel_id,-1))
           and NVL(tra.voyage_flt_id,-1)          = NVL(I_voyage_id,NVL(tra.voyage_flt_id,-1))
           and (tra.estimated_depart_date  = nvl(L_etd,tra.estimated_depart_date) or (tra.estimated_depart_date is null and L_etd is null))
           and NVL(tra.order_no,-1)              = NVL(I_order_no,NVL(tra.order_no,-1))
           and NVL(tra.bl_awb_id,-1)             = NVL(I_bl_awb_id,NVL(tra.bl_awb_id,-1))
           and NVL(tra.container_id,-1)          = NVL(I_container_id,NVL(tra.container_id,-1)))
   select tran_ord.item,
          nvl(tran_ord.item_qty,0) item_qty, 
          tran_ord.item_qty_uom, 
          tran_ord.origin_country_id,
          tran_ord.supplier 
     from tran_ord
    where NVL(tran_ord.item ,-1)  = NVL(I_item,NVL(tran_ord.item,-1))
      and L_trnsprt_method        = 'TRNSPRT' 
   union all   
   select trs.item,
          nvl(trs.quantity,0) item_qty, 
          trs.quantity_uom, 
          tran_ord.origin_country_id, 
          tran_ord.supplier 
     from tran_ord,
          trans_sku trs   
    where trs.transportation_id = tran_ord.transportation_id
      and trs.item = I_item
      and I_item is not null
      and L_trnsprt_method        = 'TRNSPRT' 
   union all   
   select sk.item,
          NVL(sk.qty_received,NVL(sk.qty_expected,0)) item_qty,
          im.standard_uom, 
          tran_ord.origin_country_id,
          tran_ord.supplier 
     from tran_ord,
          transportation_shipment ts,
          shipsku  sk,
          shipment s ,
          item_master im
    where ts.vessel_id (+)          = tran_ord.vessel_id
      and ts.voyage_flt_id (+)      = tran_ord.voyage_flt_id
      and ts.estimated_depart_date (+)= tran_ord.estimated_depart_date
      and ts.order_no (+)           = tran_ord.order_no
      and s.order_no = tran_ord.order_no
      and s.shipment = NVL(ts.shipment, s.shipment)
      and sk.shipment = s.shipment
      and NVL(tran_ord.item ,-1)                 = NVL(I_item,NVL(tran_ord.item,-1))
      and sk.item = tran_ord.item
      and sk.item = im.item
      and L_trnsprt_method        = 'ASN' ;
   --
   CURSOR c_tran_ordsku IS
   WITH ordcnt AS
       (SELECT order_no, 
               SUM(ol3.qty_ordered) tot_cnt 
          FROM ordloc ol3, 
               item_master im2 
          WHERE ol3.item = im2.item 
            AND ol3.order_no =  NVL(I_order_no,ol3.order_no)
          GROUP BY ol3.order_no) ,
        ordicnt AS
        (SELECT ol4.order_no, 
                SUM(ol4.qty_ordered) itot_cnt
           FROM ordloc ol4
          WHERE ol4.order_no = NVL(I_order_no,ol4.order_no)
           AND ol4.item =  NVL(I_item,ol4.item)
         GROUP BY ol4.order_no) 
    select im.item, 
           ordicnt.itot_cnt ,
           ordcnt.tot_cnt  ,
           round((ordicnt.itot_cnt/ordcnt.tot_cnt)*tra.item_qty) item_qty,
           tra.item_qty_uom,
           tra.origin_country_id, 
           oh.supplier 
     from item_master im, 
          transportation tra , 
          ordhead oh, 
          ordcnt,
          ordicnt
     where ordcnt.order_no   = tra.order_no 
       and ordicnt.order_no  = tra.order_no 
       and oh.order_no       = tra.order_no 
       and im.item_parent    = tra.item 
       and tra.vessel_id     = nvl(I_vessel_id,tra.vessel_id)
       and tra.voyage_flt_id = nvl(I_voyage_id,tra.voyage_flt_id)
       and (tra.estimated_depart_date = nvl(L_etd,tra.estimated_depart_date) or (tra.estimated_depart_date is null and L_etd is null))
       and im.item           =  NVL(I_item,im.item)
       and nvl(tra.bl_awb_id,-1)             = nvl(I_bl_awb_id,NVL(tra.bl_awb_id,-1))
       and nvl(tra.container_id,-1)          = nvl(I_container_id,NVL(tra.container_id,-1))
       and L_trnsprt_method  = 'TRNSPRT' ;
   --
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_TRNSPRT_METHOD','rtm_unit_options',NULL);
   open  C_TRNSPRT_METHOD;
   --
   SQL_LIB.SET_MARK('FETCH','C_TRNSPRT_METHOD','rtm_unit_options',NULL);
   fetch C_TRNSPRT_METHOD into L_trnsprt_method;
   --
   SQL_LIB.SET_MARK('CLOSE','C_TRNSPRT_METHOD','rtm_unit_options',NULL);
   close C_TRNSPRT_METHOD;
   --
   if I_estimated_depart_date is not null then
      L_etd := TO_DATE(to_char(I_estimated_depart_date,'DD-MON-RR'), 'DD-MON-RR');
   end if;
   --
   
   
   
   FOR c_tran_rec in C_TRAN_BUILD LOOP
      if (    c_tran_rec.item_qty     <> 0
          and c_tran_rec.item_qty_uom is not NULL
          and c_tran_rec.item         is not NULL) then
          --
          if L_item_qty_uom is not NULL then
             --Convert item quantity
             if UOM_SQL.CONVERT(O_error_message,
                                L_item_qty,
                                L_item_qty_uom,
                                c_tran_rec.item_qty,
                                c_tran_rec.item_qty_uom,
                                c_tran_rec.item,
                                c_tran_rec.supplier,
                                c_tran_rec.origin_country_id) = FALSE then
                  return FALSE;
               end if;
               O_item_qty_uom := L_item_qty_uom;
            else
               O_item_qty_uom := c_tran_rec.item_qty_uom;
               L_item_qty     := c_tran_rec.item_qty;			   
            end if;
            ---
            O_item_qty := O_item_qty + L_item_qty;			
            L_item_qty := 0;
         end if;
	 v_found := TRUE ;
   END LOOP; 

   -- if there is no records returned by the above cursor check the PO for qty
   if v_found = FALSE then
      FOR c_ordsku_rec in c_tran_ordsku LOOP
         if (    c_ordsku_rec.item_qty     <> 0
             and c_ordsku_rec.item_qty_uom is not NULL
             and c_ordsku_rec.item         is not NULL) then
             --
             if L_item_qty_uom is not NULL then
                --Convert item quantity
                if UOM_SQL.CONVERT(O_error_message,
                                   L_item_qty,
                                   L_item_qty_uom,
                                   c_ordsku_rec.item_qty,
                                   c_ordsku_rec.item_qty_uom,
                                   c_ordsku_rec.item,
                                   c_ordsku_rec.supplier,
                                   c_ordsku_rec.origin_country_id) = FALSE then
                     return FALSE;
                  end if;
                  O_item_qty_uom := L_item_qty_uom;
               else
                  O_item_qty_uom := c_ordsku_rec.item_qty_uom;
                  L_item_qty     := c_ordsku_rec.item_qty;			   
               end if;
               ---
               O_item_qty := O_item_qty + L_item_qty;			
               L_item_qty := 0;
            end if;
      END LOOP;       
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END GET_TRAN_QTY;   
---------------------------------------------------------------------------------------------------------------------------------------
END;
/
