
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE OGA_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
-- Function Name: GET_INFO
-- Purpose      : Validate and retrieve information from the OGA table.
---------------------------------------------------------------------------------------------
FUNCTION GET_INFO (O_error_message IN OUT VARCHAR2,
                   O_exists        IN OUT	BOOLEAN,
                   O_oga_desc	     IN OUT	OGA.OGA_DESC%TYPE,
                   O_req_form      IN OUT OGA.REQ_FORM%TYPE,
                   I_oga_code      IN     OGA.OGA_CODE%TYPE)
      RETURN BOOLEAN;

END OGA_SQL;
/


