CREATE OR REPLACE PACKAGE HTS_CLEAR_ZONE_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name: TAX_ZONE_EXISTS
-- Purpose      : Checks if a record exists on the HTS_TAX_ZONE table for a passed in 
--                hts/import country/effect from/effect to/tax type.
---------------------------------------------------------------------------------------------
FUNCTION TAX_ZONE_EXISTS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists             IN OUT   BOOLEAN,
                         I_hts                IN       HTS_TAX_ZONE.HTS%TYPE,
                         I_import_country_id  IN       HTS_TAX_ZONE.IMPORT_COUNTRY_ID%TYPE,
                         I_effect_from        IN       HTS_TAX_ZONE.EFFECT_FROM%TYPE,
                         I_effect_to          IN       HTS_TAX_ZONE.EFFECT_TO%TYPE,
                         I_tax_type           IN       HTS_TAX_ZONE.TAX_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: FEE_ZONE_EXISTS
-- Purpose      : Checks if a record exists on the HTS_FEE_ZONE for a passed in 
--                hts/import country/effect from/effect to/fee type.
---------------------------------------------------------------------------------------------
FUNCTION FEE_ZONE_EXISTS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists            IN OUT   BOOLEAN,
                         I_hts               IN       HTS_FEE_ZONE.HTS%TYPE,
                         I_import_country_id IN       HTS_FEE_ZONE.IMPORT_COUNTRY_ID%TYPE,
                         I_effect_from       IN       HTS_FEE_ZONE.EFFECT_FROM%TYPE,
                         I_effect_to         IN       HTS_FEE_ZONE.EFFECT_TO%TYPE,
                         I_fee_type          IN       HTS_FEE_ZONE.FEE_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: TARIFF_TREATMENT_ZONE_EXISTS
-- Purpose      : Checks if a record exists on the HTS_TARIFF_TREATMENT_ZONE for a passed in 
--                hts/import country/effect from/effect to/tariff treatment.
---------------------------------------------------------------------------------------------
FUNCTION TARIFF_TREATMENT_ZONE_EXISTS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists              IN OUT   BOOLEAN,
                                      I_hts                 IN       HTS_TARIFF_TREATMENT_ZONE.HTS%TYPE,
                                      I_import_country_id   IN       HTS_TARIFF_TREATMENT_ZONE.IMPORT_COUNTRY_ID%TYPE,
                                      I_effect_from         IN       HTS_TARIFF_TREATMENT_ZONE.EFFECT_FROM%TYPE,
                                      I_effect_to           IN       HTS_TARIFF_TREATMENT_ZONE.EFFECT_TO%TYPE,
                                      I_tariff_treatment    IN       HTS_TARIFF_TREATMENT_ZONE.TARIFF_TREATMENT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_TAX_ZONE_DUP
-- Purpose      : Verify if a record already exists in HTS_TAX_ZONE for the input clearing zone.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_TAX_ZONE_DUP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_dup_exists        IN OUT   BOOLEAN,
                            I_hts               IN       HTS_TAX_ZONE.HTS%TYPE,
                            I_import_country_id IN       HTS_TAX_ZONE.IMPORT_COUNTRY_ID%TYPE,
                            I_effect_from       IN       HTS_TAX_ZONE.EFFECT_FROM%TYPE,
                            I_effect_to         IN       HTS_TAX_ZONE.EFFECT_TO%TYPE,
                            I_tax_type          IN       HTS_TAX_ZONE.TAX_TYPE%TYPE,
                            I_clearing_zone_id  IN       HTS_TAX_ZONE.CLEARING_ZONE_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_FEE_ZONE_DUP
-- Purpose      : Verify if a record already exists in HTS_FEE_ZONE for the input clearing zone.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_FEE_ZONE_DUP(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_dup_exists        IN OUT   BOOLEAN,
                            I_hts               IN       HTS_FEE_ZONE.HTS%TYPE,
                            I_import_country_id IN       HTS_FEE_ZONE.IMPORT_COUNTRY_ID%TYPE,
                            I_effect_from       IN       HTS_FEE_ZONE.EFFECT_FROM%TYPE,
                            I_effect_to         IN       HTS_FEE_ZONE.EFFECT_TO%TYPE,
                            I_fee_type          IN       HTS_FEE_ZONE.FEE_TYPE%TYPE,
                            I_clearing_zone_id  IN       HTS_FEE_ZONE.CLEARING_ZONE_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_TARIFF_TREATMENT_DUP
-- Purpose      : Verify if a record already exists in HTS_TARIFF_TREATMENT_ZONE for 
--                the input clearing zone.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_TARIFF_TREATMENT_DUP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_dup_exists         IN OUT   BOOLEAN,
                                    I_hts                IN       HTS_TARIFF_TREATMENT_ZONE.HTS%TYPE,
                                    I_import_country_id  IN       HTS_TARIFF_TREATMENT_ZONE.IMPORT_COUNTRY_ID%TYPE,
                                    I_effect_from        IN       HTS_TARIFF_TREATMENT_ZONE.EFFECT_FROM%TYPE,
                                    I_effect_to          IN       HTS_TARIFF_TREATMENT_ZONE.EFFECT_TO%TYPE,
                                    I_tariff_treatment   IN       HTS_TARIFF_TREATMENT_ZONE.TARIFF_TREATMENT%TYPE,
                                    I_clearing_zone_id   IN       HTS_TARIFF_TREATMENT_ZONE.CLEARING_ZONE_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_TAX_ZONE
-- Purpose      : Deletes the records from HTS_TAX_ZONE for the input combination.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_TAX_ZONE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts               IN       HTS_TAX_ZONE.HTS%TYPE,
                         I_import_country_id IN       HTS_TAX_ZONE.IMPORT_COUNTRY_ID%TYPE,
                         I_effect_from       IN       HTS_TAX_ZONE.EFFECT_FROM%TYPE,
                         I_effect_to         IN       HTS_TAX_ZONE.EFFECT_TO%TYPE,
                         I_tax_type          IN       HTS_TAX_ZONE.TAX_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_FEE_ZONE
-- Purpose      : Deletes the records from HTS_FEE_ZONE for the input combination.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_FEE_ZONE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts               IN       HTS_FEE_ZONE.HTS%TYPE,
                         I_import_country_id IN       HTS_FEE_ZONE.IMPORT_COUNTRY_ID%TYPE,
                         I_effect_from       IN       HTS_FEE_ZONE.EFFECT_FROM%TYPE,
                         I_effect_to         IN       HTS_FEE_ZONE.EFFECT_TO%TYPE,
                         I_fee_type          IN       HTS_FEE_ZONE.FEE_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_TARIFF_TREATMENT_ZONE
-- Purpose      : Deletes the records from HTS_TARIFF_TREATMENT_ZONE for the input combination.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_TARIFF_TREATMENT_ZONE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_hts                 IN       HTS_TARIFF_TREATMENT_ZONE.HTS%TYPE,
                                      I_import_country_id   IN       HTS_TARIFF_TREATMENT_ZONE.IMPORT_COUNTRY_ID%TYPE,
                                      I_effect_from         IN       HTS_TARIFF_TREATMENT_ZONE.EFFECT_FROM%TYPE,
                                      I_effect_to           IN       HTS_TARIFF_TREATMENT_ZONE.EFFECT_TO%TYPE,
                                      I_tariff_treatment    IN       HTS_TARIFF_TREATMENT_ZONE.TARIFF_TREATMENT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: LOCK_HTS_TAX
-- Purpose      : Locks the records in HTS_TAX for the input combination.
---------------------------------------------------------------------------------------------
FUNCTION LOCK_HTS_TAX(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_hts               IN       HTS_TAX.HTS%TYPE,
                      I_import_country_id IN       HTS_TAX.IMPORT_COUNTRY_ID%TYPE,
                      I_effect_from       IN       HTS_TAX.EFFECT_FROM%TYPE,
                      I_effect_to         IN       HTS_TAX.EFFECT_TO%TYPE,
                      I_tax_type          IN       HTS_TAX.TAX_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: LOCK_HTS_FEE
-- Purpose      : Locks the records in HTS_FEE for the input combination.
---------------------------------------------------------------------------------------------
FUNCTION LOCK_HTS_FEE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_hts               IN       HTS_FEE.HTS%TYPE,
                      I_import_country_id IN       HTS_FEE.IMPORT_COUNTRY_ID%TYPE,
                      I_effect_from       IN       HTS_FEE.EFFECT_FROM%TYPE,
                      I_effect_to         IN       HTS_FEE.EFFECT_TO%TYPE,
                      I_fee_type          IN       HTS_FEE.FEE_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: LOCK_HTS_TARIFF_TREATMENT
-- Purpose      : Locks the records in HTS_TARIFF_TREATMENT for the input combination.
---------------------------------------------------------------------------------------------
FUNCTION LOCK_HTS_TARIFF_TREATMENT(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_hts                 IN       HTS_TARIFF_TREATMENT.HTS%TYPE,
                                   I_import_country_id   IN       HTS_TARIFF_TREATMENT.IMPORT_COUNTRY_ID%TYPE,
                                   I_effect_from         IN       HTS_TARIFF_TREATMENT.EFFECT_FROM%TYPE,
                                   I_effect_to           IN       HTS_TARIFF_TREATMENT.EFFECT_TO%TYPE,
                                   I_tariff_treatment    IN       HTS_TARIFF_TREATMENT.TARIFF_TREATMENT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END HTS_CLEAR_ZONE_SQL;
/