



CREATE OR REPLACE PACKAGE CE_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------------
--Function Name:   GET_NEXT_CE_ID
--Purpose      :   Retrieves the next value for the CE ID for the 
--                 CE HEAD table.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_CE_ID(O_error_message   IN OUT VARCHAR2,
                        O_ce_id           IN OUT CE_HEAD.CE_ID%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------------------
--Function Name:   GET_DEFAULTS
--Purpose      :   Retrieves the Base Country ID for a new CE_ID on the 
--                 CE_HEAD table.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULTS(O_error_message       IN OUT VARCHAR2,
                      O_import_country_id   IN OUT CE_HEAD.IMPORT_COUNTRY_ID%TYPE,
                      O_currency_code       IN OUT CE_HEAD.CURRENCY_CODE%TYPE,
                      O_exchange_rate       IN OUT CE_HEAD.EXCHANGE_RATE%TYPE,
                      I_ce_id               IN     CE_HEAD.CE_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------------------
--Function Name:   GET_ENTRY_STATUS_DESC
--Purpose      :   Retrieves the Entry Status Description from the Entry Status
--                 table.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_ENTRY_STATUS_DESC(O_error_message      IN OUT VARCHAR2,
                               O_exists             IN OUT BOOLEAN,
                               O_description        IN OUT ENTRY_STATUS.ENTRY_STATUS_DESC%TYPE,
                               I_entry_status       IN     ENTRY_STATUS.ENTRY_STATUS%TYPE,
                               I_import_country_id  IN     ENTRY_STATUS.IMPORT_COUNTRY_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------------------
--Function Name:   GET_ENTRY_TYPE_DESC
--Purpose      :   Retrieves the Entry Tyoe Description from the Entry Type
--                 table.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_ENTRY_TYPE_DESC(O_error_message      IN OUT VARCHAR2,
                             O_exists             IN OUT BOOLEAN,
                             O_description        IN OUT ENTRY_TYPE.ENTRY_TYPE_DESC%TYPE,
                             I_entry_type         IN     ENTRY_TYPE.ENTRY_TYPE%TYPE,
                             I_import_country_id  IN     ENTRY_TYPE.IMPORT_COUNTRY_ID%TYPE)
   return BOOLEAN;

-------------------------------------------------------------------------------------------------------
--Function Name:  LOCK_HEAD_CHILDREN
--Purpose      :  Locks the CE_ID child records when deleteing from CE Head level.
--------------------------------------------------------------------------------------------------------
FUNCTION LOCK_HEAD_CHILDREN(O_error_message   IN OUT VARCHAR2,
                            I_ce_id           IN     CE_HEAD.CE_ID%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------------------------------
--Function Name:  DELETE_HEAD_CHILDREN
--Purpose      :  Deletes the CE_ID child records when deleteing from CE Head level.
--------------------------------------------------------------------------------------------------------
FUNCTION DELETE_HEAD_CHILDREN(O_error_message   IN OUT VARCHAR2,
                              I_ce_id           IN     CE_HEAD.CE_ID%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------------------------------
--Function Name:  LOCK_SHIPMENT_CHILDREN
--Purpose      :  Locks the child records when deleteing from CE Shipment level.
--------------------------------------------------------------------------------------------------------
FUNCTION LOCK_SHIPMENT_CHILDREN(O_error_message         IN OUT VARCHAR2,
                                I_ce_id                 IN     CE_HEAD.CE_ID%TYPE,
                                I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                                I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------------------------------
--Function Name:  DELETE_SHIPMENT_CHILDREN
--Purpose      :  Deletes child records when deleteing from CE Shipment level.
--------------------------------------------------------------------------------------------------------
FUNCTION DELETE_SHIPMENT_CHILDREN(O_error_message         IN OUT VARCHAR2,
                                  I_ce_id                 IN     CE_HEAD.CE_ID%TYPE,
                                  I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                                  I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                  I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------------------------------
--Function Name:  LOCK_ORD_ITEM_CHILDREN
--Purpose      :  Locks the child records when deleteing from CE Order Item level.
--------------------------------------------------------------------------------------------------------
FUNCTION LOCK_ORD_ITEM_CHILDREN(O_error_message         IN OUT VARCHAR2,
                                I_ce_id                 IN     CE_HEAD.CE_ID%TYPE,
                                I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                                I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                                I_item                  IN     ITEM_MASTER.ITEM%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------------------------------
--Function Name:  DELETE_ORD_ITEM_CHILDREN
--Purpose      :  Deletes child records when deleteing from CE Order Item level.
--------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORD_ITEM_CHILDREN(O_error_message         IN OUT VARCHAR2,
                                  I_ce_id                 IN     CE_HEAD.CE_ID%TYPE,
                                  I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                                  I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                  I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                  I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                                  I_item                  IN     ITEM_MASTER.ITEM%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   SHIPMENT_DETAILS_EXIST
--Purpose      :   This function verifies that child records exist on CE shipment and CE Order_item
--       tables for a given CE Ref. ID.
-----------------------------------------------------------------------------------------------------
FUNCTION CE_SHIPMENT_EXIST(O_error_message       IN OUT VARCHAR2,
                           O_shipment_exist      IN OUT BOOLEAN,
                           I_ce_id               IN     CE_SHIPMENT.CE_ID%TYPE)
  return  BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   SHIPMENT_ORDER_ITEM_EXISTS
--Purpose      :   This function will verify that a record exists on the CE Order Item table for a 
--      specific Vessel/Voyage/ETD combination.
-----------------------------------------------------------------------------------------------------
FUNCTION SHIPMENT_ORDER_ITEM_EXISTS(O_error_message   IN OUT   VARCHAR2,
                                    O_exists          IN OUT   BOOLEAN,
                                    I_ce_id           IN       CE_SHIPMENT.CE_ID%TYPE,
                                    I_vessel_id       IN       TRANSPORTATION.VESSEL_ID%TYPE,
                                    I_voyage_flt_id   IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                    I_estimated_depart_date IN TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE)

  return  BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   SHIPMENT_EXISTS
--Purpose      :   This function will verify if the passed in Vessel/Voyage/ETD and CE Ref.ID combindation
--       exists on the CE Shipment table.
-----------------------------------------------------------------------------------------------------
FUNCTION SHIPMENT_EXISTS(O_error_message          IN OUT   VARCHAR2,
                         O_exists                 IN OUT   BOOLEAN,
                         I_ce_id                  IN       CE_SHIPMENT.CE_ID%TYPE,
                         I_vessel_id              IN       TRANSPORTATION.VESSEL_ID%TYPE,
                         I_voyage_flt_id          IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                         I_estimated_depart_date  IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE)

  return  BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name:   CHECK_CANDIDATE_IND
--Purpose      :   This function will verify that a record exists on the transportation table with the 
--            passed-in Vessel/Voyage/ETD combination and the candidate_ind set to 'Y'.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CANDIDATE_IND(O_error_message         IN OUT   VARCHAR2,
                             O_exists                IN OUT   BOOLEAN,
                             I_vessel_id             IN       TRANSPORTATION.VESSEL_ID%TYPE,
                             I_voyage_flt_id         IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                             I_estimated_depart_date IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE)

  return  BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name:   DEFAULT_SHIPMENT_INFO
--Purpose      :   This function will retrieve information from the transportation table based on the passed in 
--   Vessel/Voyage/ETD combination. If multiple records are returned, defaulted information should 
--   be that of the first record retrieved.
--------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_SHIPMENT_INFO(O_error_message          IN OUT   VARCHAR2,
                               O_exists                 IN OUT   BOOLEAN,
                               O_vessel_scac_code       IN OUT   TRANSPORTATION.VESSEL_SCAC_CODE%TYPE,
                               O_lading_port            IN OUT   TRANSPORTATION.LADING_PORT%TYPE,
                               O_discharge_port         IN OUT   TRANSPORTATION.DISCHARGE_PORT%TYPE,
                               O_tran_mode_id           IN OUT   TRANSPORTATION.TRAN_MODE_ID%TYPE,
                               O_export_country_id      IN OUT   TRANSPORTATION.EXPORT_COUNTRY_ID%TYPE,
                               O_actual_arrival_date    IN OUT   TRANSPORTATION.ACTUAL_ARRIVAL_DATE%TYPE,
                               I_vessel_id              IN       TRANSPORTATION.VESSEL_ID%TYPE,
                               I_voyage_flt_id          IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                               I_estimated_depart_date  IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE)

  return  BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name: FORM_EXISTS
--Purpose      : This function will determine whether or not the Form Type/OGA Code comibnation already exists
--    for the passed in CE_ID.
--------------------------------------------------------------------------------------------------------

FUNCTION FORM_EXISTS(O_error_message   IN OUT  VARCHAR2,
                     O_exists          IN OUT  BOOLEAN,
                     I_ce_id           IN      CE_FORMS.CE_ID%TYPE,
                     I_form_type       IN      CE_FORMS.FORM_TYPE%TYPE,
                     I_oga_code        IN      CE_FORMS.OGA_CODE%TYPE)

  return BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name: PROTEST_NO_EXISTS
--Purpose      : This function will make sure there are no duplicate Protest Numbers for the CE_ID.
--------------------------------------------------------------------------------------------------------
FUNCTION PROTEST_NO_EXISTS(O_error_message   IN OUT VARCHAR2,
                           O_exists          IN OUT BOOLEAN,
                           I_ce_id           IN     CE_PROTEST.CE_ID%TYPE,
                           I_protest_no      IN     CE_PROTEST.PROTEST_NO%TYPE)

  return BOOLEAN;
---------------------------------------------------------------------------------------------------------
--Function Name:   GET_CE_ENTRY_NO
--Purpose      :   Retrieves the Entry No. from the Customs Entry Header
--                 table for a given CE Reference ID.
--------------------------------------------------------------------------------------------------------
FUNCTION GET_CE_ENTRY_NO(O_error_message  IN OUT VARCHAR2,
                         O_exists         IN OUT BOOLEAN,
                         O_entry_no       IN OUT CE_HEAD.ENTRY_NO%TYPE,
                         I_ce_id          IN     CE_HEAD.CE_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name:   GET_INFO
--Purpose      :   This function will provide header level information if a valid CE_ID or Entry_No is passed
--                 into the function.
--------------------------------------------------------------------------------------------------------
FUNCTION GET_INFO (O_error_message          IN OUT VARCHAR2,
                   O_exists                 IN OUT BOOLEAN,
                   O_ce_id                  IN OUT CE_HEAD.CE_ID%TYPE, 
                   O_entry_no               IN OUT CE_HEAD.ENTRY_NO%TYPE,
                   O_entry_date             IN OUT CE_HEAD.ENTRY_DATE%TYPE,
                   O_release_date           IN OUT CE_HEAD.RELEASE_DATE%TYPE,
                   O_summary_date           IN OUT CE_HEAD.SUMMARY_DATE%TYPE,
                   O_import_country_id      IN OUT CE_HEAD.IMPORT_COUNTRY_ID%TYPE,
                   O_entry_port             IN OUT CE_HEAD.ENTRY_PORT%TYPE,
                   O_entry_status           IN OUT CE_HEAD.ENTRY_STATUS%TYPE,
                   O_entry_type             IN OUT CE_HEAD.ENTRY_TYPE%TYPE,
                   O_importer_id            IN OUT CE_HEAD.IMPORTER_ID%TYPE,
                   O_bond_no                IN OUT CE_HEAD.BOND_NO%TYPE,
                   O_bond_type              IN OUT CE_HEAD.BOND_TYPE%TYPE,
                   O_broker_id              IN OUT CE_HEAD.BROKER_ID%TYPE,
                   O_broker_ref_id          IN OUT CE_HEAD.BROKER_REF_ID%TYPE,
                   I_ce_id                  IN     CE_HEAD.CE_ID%TYPE,
                   I_entry_no               IN     CE_HEAD.ENTRY_NO%TYPE)
             
   return BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name:   VALID_TRANS_ORDER_ITEM
--Purpose      :   This function will validate the passed in Vessel/Voyage/ETD, Order No. and Item combination
--                 on the transportation table where the candidate indicator is set to 'Y'.  Vessel/Voyage/ETD
--                 must be passed into the function.  Either Order No. or Item can be NULL but both
--                 cannot be NULL.
--------------------------------------------------------------------------------------------------------
FUNCTION VALID_TRANS_ORDER_ITEM (O_error_message         IN OUT VARCHAR2, 
                                 O_exists                IN OUT BOOLEAN,
                                 I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                                 I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                 I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                 I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                                 I_item                  IN     TRANSPORTATION.ITEM%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name: GET_TRANS_ORIGIN_COUNTRY
--Purpose      : This function will retreive the origin country from the transportation table 
--               based on the Vessel/Voyage/ETD, order_no and item combination.
--------------------------------------------------------------------------------------------------------
FUNCTION GET_TRANS_ORIGIN_COUNTRY(O_error_message         IN OUT  VARCHAR2,
                                  O_exists                IN OUT  BOOLEAN,
                                  O_origin_country_id     IN OUT  COUNTRY.COUNTRY_ID%TYPE,
                                  I_vessel_id             IN      TRANSPORTATION.VESSEL_ID%TYPE,
                                  I_voyage_flt_id         IN      TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                  I_estimated_depart_date IN      TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                  I_order_no              IN      ORDHEAD.ORDER_NO%TYPE,
                                  I_item                  IN      ITEM_MASTER.ITEM%TYPE)

  return BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name: VALID_TRANS_INVOICE
--Purpose      : This function will validate the passed-in invoice number against the transportation 
--               table based on the passed-in Vessel/Voyage/ETD.
--------------------------------------------------------------------------------------------------------
FUNCTION VALID_TRANS_INVOICE(O_error_message         IN OUT  VARCHAR2,
                             O_exists                IN OUT  BOOLEAN,
                             I_vessel_id             IN      TRANSPORTATION.VESSEL_ID%TYPE,
                             I_voyage_flt_id         IN      TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                             I_estimated_depart_date IN      TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                             I_invoice_id            IN      TRANSPORTATION.INVOICE_ID%TYPE)

   return BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name: VALID_BL_AWB
--Purpose      : This function will validate the passed-in BL_AWB_ID against the transportation 
--               table based on the passed-in Vessel/Voyage/ETD.
--------------------------------------------------------------------------------------------------------
FUNCTION VALID_BL_AWB(O_error_message         IN OUT  VARCHAR2,
                      O_exists                IN OUT  BOOLEAN,
                      I_vessel_id             IN      TRANSPORTATION.VESSEL_ID%TYPE,
                      I_voyage_flt_id         IN      TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                      I_estimated_depart_date IN      TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                      I_bl_awb_id             IN      TRANSPORTATION.BL_AWB_ID%TYPE,
                      I_order_no              IN      TRANSPORTATION.ORDER_NO%TYPE,
                      I_item                  IN      TRANSPORTATION.ITEM%TYPE,
                      I_invoice_id            IN      TRANSPORTATION.INVOICE_ID%TYPE)

   return BOOLEAN;
---------------------------------------------------------------------------------------------------------
--Function Name:   VALIDATE_ENTRY_NO
--Purpose      :   Validates that the Entry_No is unique.
--------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ENTRY_NO(O_error_message  IN OUT VARCHAR2,
                           O_exists         IN OUT BOOLEAN,
                           I_entry_no       IN     CE_HEAD.ENTRY_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name: VALID_TARIFF_TREATMENT
--Purpose      : This function will determine whether or not a given Tariff Treatment is valid for a 
--               given Origin Country/Item combination in the Customs Entry module.  A valid Tariff Treatment 
--               will have to be on the COUNTRY_TARIFF_TREATMENT for the passed in Origin Country or 
--               on the COND_TARIFF_TREATMENT for the passed in Item.
--------------------------------------------------------------------------------------------------------
FUNCTION VALID_TARIFF_TREATMENT(O_error_message     IN OUT VARCHAR2,
                                O_exists            IN OUT BOOLEAN,
                                I_tariff_treatment  IN     CE_ORD_ITEM.TARIFF_TREATMENT%TYPE,
                                I_item              IN     CE_ORD_ITEM.ITEM%TYPE,
                                I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE)

   return BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name:   CE_OBL_EXISTS
--Purpose      :   This function will check for the existence of a record on the
--                 Custom Entry Header obligation table.
--------------------------------------------------------------------------------------------------------
FUNCTION CE_OBL_EXISTS(O_error_message         IN OUT VARCHAR2,
                       O_exists                IN OUT BOOLEAN,
                       I_entry_no              IN     CE_HEAD.ENTRY_NO%TYPE)

   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name:   GET_CE_STATUS
--Purpose      :   This function will retrieve the Status off the CUstom Entry Header (ce_head) table 
--                 for the passed in ce_id.
--------------------------------------------------------------------------------------------------------
FUNCTION GET_CE_STATUS(O_error_message         IN OUT VARCHAR2,
                       O_exists                IN OUT BOOLEAN,
                       O_status                IN OUT CE_HEAD.STATUS%TYPE,
                       I_ce_id                 IN     CE_HEAD.CE_ID%TYPE)

   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name:   PROCESSED_ALC_EXISTS
--Purpose      :   This function will check for the existence of an ALC Status of 'Processed'('R') on the 
--                 Custom Entry Order/Item table (ce_ord_item).  
--                 This function can be called from three different levels: Customs Entry Header,
--                 Custom Entry Shipment and Customs Entry Oder Item.  If level being checked is Custom Entry Header 
--                 the valid passed in parameter is Entry Number only.  If the level is Custom Entry Shipment the valid
--                 passed in parameters should be Entry No., Vessel, Voyage, and Estimated Departure Date. If the 
--                 level is Custom Entry Order Item the valid passed in parameters should be Entry No., Vessel, 
--                 Voyage, Estimated Departure Date, Order Number and Item.
--------------------------------------------------------------------------------------------------------
FUNCTION PROCESSED_ALC_EXISTS(O_error_message          IN OUT  VARCHAR2,
                              O_exists                 IN OUT  BOOLEAN,
                              I_ce_id                  IN      CE_HEAD.CE_ID%TYPE,
                              I_vessel_id              IN      TRANSPORTATION.VESSEL_ID%TYPE,
                              I_voyage_flt_id          IN      TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                              I_estimated_depart_date  IN      TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                              I_order_no               IN      ORDHEAD.ORDER_NO%TYPE,
                              I_item                   IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name: LICENSE_VISA_EXISTS
-- Purpose:       This function will be used to determine if a CE License/Visa
--                record exists on the CE License Visa table (ce_lic_visa). 
--------------------------------------------------------------------------------------------------------
FUNCTION LICENSE_VISA_EXISTS(O_error_message         IN OUT VARCHAR2,
                             O_exists                IN OUT BOOLEAN,
                             I_ce_id                 IN     CE_HEAD.CE_ID%TYPE,
                             I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                             I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                             I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                             I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                             I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                             I_license_visa_type     IN     CE_LIC_VISA.LICENSE_VISA_TYPE%TYPE,
                             I_license_visa_id       IN     CE_LIC_VISA.LICENSE_VISA_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name:   CE_VVE_PO_ITEM_EXISTS
--Purpose      :   This function determines if any combination of the
--                 passed-in CE Ref.ID/Vessel/Voyage/ETD/Order/Item exists on the 
--                 CE Order Item table. 
-----------------------------------------------------------------------------------------------------
FUNCTION CE_VVE_PO_ITEM_EXISTS(O_error_message         IN OUT VARCHAR2, 
                               O_exists                IN OUT BOOLEAN,
                               I_ce_id                 IN     CE_ORD_ITEM.CE_ID%TYPE,
                               I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                               I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                               I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                               I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                               I_item                  IN     TRANSPORTATION.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
--Function Name:   GET_CE_ID
--Purpose      :   Retrieves the CE Ref. ID from the Customs Entry Header
--                 table for a given Entry Number.  
--------------------------------------------------------------------------------------------------------
FUNCTION GET_CE_ID(O_error_message  IN OUT VARCHAR2,
                   O_exists         IN OUT BOOLEAN,
                   O_ce_id          IN OUT CE_HEAD.CE_ID%TYPE,
                   I_entry_no       IN     CE_HEAD.ENTRY_NO%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------------
--Function Name:   GET_CURRENCY_RATE
--Purpose      :   Retrieves the currency code and exchange rate from the CE_HEAD 
--                 table for the given CE ID.
--------------------------------------------------------------------------------------
FUNCTION GET_CURRENCY_RATE(O_error_message   IN OUT VARCHAR2,
                           O_currency_code   IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
                           O_exchange_rate   IN OUT CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                           I_ce_id           IN     CE_HEAD.CE_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name: DELETE_ALC
--Purpose      : This function will delete associated ALC records for a given CE_ID.
--------------------------------------------------------------------------------------
FUNCTION DELETE_ALC(O_error_message  IN OUT  VARCHAR2,
                    I_ce_id          IN      CE_HEAD.CE_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name: CHARGES_EXIST
--Purpose      : Checks to see if a given customs entry charges record exists
--               for the Order/Item combination ('VIEW' mode check).
--------------------------------------------------------------------------------------
FUNCTION CHARGES_EXIST(O_error_message          IN OUT  VARCHAR2,
                       O_exists                 IN OUT  BOOLEAN,
                       I_ce_id                  IN      CE_CHARGES.CE_ID%TYPE,
                       I_vessel_id              IN      TRANSPORTATION.VESSEL_ID%TYPE,
                       I_voyage_flt_id          IN      TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                       I_estimated_depart_date  IN      TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                       I_order_no               IN      ORDHEAD.ORDER_NO%TYPE,
                       I_item                   IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name: UPDATE_ORD_ITEM_ALC_STATUS
--Purpose      : This function will update ALC_STATUS on CE_ORD_ITEM based on the passed-in
--               Customs Entry ID, Vessel ID, Voyage/Flight ID, Estimated Depart date 
--               Order No and Item combination with the passed in I_alc_status Value
--------------------------------------------------------------------------------------
FUNCTION UPDATE_ORD_ITEM_ALC_STATUS(O_error_message          IN OUT  VARCHAR2,
                                    I_alc_status             IN      CE_ORD_ITEM.ALC_STATUS%TYPE,
                                    I_ce_id                  IN      CE_CHARGES.CE_ID%TYPE,
                                    I_vessel_id              IN      TRANSPORTATION.VESSEL_ID%TYPE,
                                    I_voyage_flt_id          IN      TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                    I_estimated_depart_date  IN      TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                    I_order_no               IN      ORDHEAD.ORDER_NO%TYPE,
                                    I_item                   IN      ITEM_MASTER.ITEM%TYPE)       
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name: UPDATE_CE_ALC_STATUS
--Purpose      : This function will update ALC_STATUS on CE_HEAD with the value
--               in the passed in l_alc_status parameter.
--------------------------------------------------------------------------------------
FUNCTION GET_CE_ALC_STATUS(O_error_message   IN OUT  VARCHAR2,
                           O_alc_status      IN OUT  CE_ORD_ITEM.ALC_STATUS%TYPE,
                           I_ce_id           IN      CE_HEAD.CE_ID%TYPE,
                           I_order_no        IN      ORDHEAD.ORDER_NO%TYPE)       
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name: GET_ORD_ITEM_TRAN_INFO
--Purpose      : This function will retrieve invoice and qty info for the passed in
--               Vessel/Voyage/ETD/order/item/invoice/origin country combination.
--------------------------------------------------------------------------------------------------------
FUNCTION GET_ORD_ITEM_TRAN_INFO(O_error_message         IN OUT  VARCHAR2,
                                O_exists                IN OUT  BOOLEAN,
                                O_invoice_date          OUT     TRANSPORTATION.INVOICE_DATE%TYPE,
                                O_invoice_amt           OUT     TRANSPORTATION.INVOICE_AMT%TYPE,
                                O_currency_code         OUT     TRANSPORTATION.CURRENCY_CODE%TYPE,
                                O_exchange_rate         OUT     TRANSPORTATION.EXCHANGE_RATE%TYPE,
                                O_item_qty              OUT     TRANSPORTATION.ITEM_QTY%TYPE,
                                O_item_qty_uom          OUT     TRANSPORTATION.ITEM_QTY_UOM%TYPE,
                                I_vessel_id             IN      TRANSPORTATION.VESSEL_ID%TYPE,
                                I_voyage_flt_id         IN      TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                I_estimated_depart_date IN      TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                I_order_no              IN      TRANSPORTATION.ORDER_NO%TYPE,
                                I_item                  IN      TRANSPORTATION.ITEM%TYPE,
                                I_invoice_id            IN      TRANSPORTATION.INVOICE_ID%TYPE,
                                I_origin_country_id     IN      COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name:   INVOICE_EXIST
--Purpose      :   This function determines if a Customs Entry already exists for this
--                 Vessel/Voyage/ETD/Order/Item/Invoice combination on the CE Order Item table.
--                 It also checks whether this Customs Entry contains multiple invoices
--                 for the Vessel/Voyage/ETD/Order/Item combination.
-----------------------------------------------------------------------------------------------------
FUNCTION INVOICE_EXIST(O_error_message         IN OUT VARCHAR2,
                       O_exists                IN OUT BOOLEAN,
                       I_vessel_id             IN     CE_ORD_ITEM.VESSEL_ID%TYPE,
                       I_voyage_id             IN     CE_ORD_ITEM.VOYAGE_FLT_ID%TYPE, 
                       I_estimated_depart_date IN     CE_ORD_ITEM.ESTIMATED_DEPART_DATE%TYPE,
                       I_order_no              IN     CE_ORD_ITEM.ORDER_NO%TYPE,
                       I_item                  IN     CE_ORD_ITEM.ITEM%TYPE,
                       I_invoice_id            IN     CE_ORD_ITEM.INVOICE_ID%TYPE,
                       I_ce_id                 IN     CE_ORD_ITEM.CE_ID%TYPE)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- GET_INFO
-- Overloaded function which returns a ce_head row.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_exists         IN OUT  BOOLEAN,
                  O_ce_head_row    IN OUT  CE_HEAD%ROWTYPE,
                  I_ce_id          IN      CE_HEAD.CE_ID%TYPE,
                  I_entry_no       IN      CE_HEAD.ENTRY_NO%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------------------
--Function Name:   VALID_INVOICE
--Purpose      :   This function will check to see that Invoice No. is valid for a particular 
--                 Vessel/Voyage/ETD/PO/Item/BL/AWB combination.  
--------------------------------------------------------------------------------------------------------
FUNCTION VALID_INVOICE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists                IN OUT BOOLEAN,
                       I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                       I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                       I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                       I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                       I_item                  IN     TRANSPORTATION.ITEM%TYPE,
                       I_bl_awb_id             IN     TRANSPORTATION.BL_AWB_ID%TYPE,
                       I_invoice_id            IN     TRANSPORTATION.INVOICE_ID%TYPE)
  return BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name:   CHECK_CANDIDATE_IND_NO
--Purpose      :   This function will check to see if there are any records on the transportation table for
--                the passed-in Vessel/Voyage/ETD combination where the candidate_ind is set to 'N'.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CANDIDATE_IND_NO(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists                  IN OUT   BOOLEAN,
                                I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                                I_voyage_flt_id           IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE)

  return  BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function Name:   CHECK_CE_SUP_IMP_ATTR_EXISTS
--Purpose      :   This function will check to see if there are any suppliers for the Orders related to  
--                 Entry id passed-in with records missing from sup_import_attr table .
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CE_SUP_IMP_ATTR_EXISTS(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists         IN OUT   BOOLEAN,
                                      I_ce_id          IN       CE_HEAD.CE_ID%TYPE)
   return  BOOLEAN;
---------------------------------------------------------------------------------------------------------

END CE_SQL;
/
