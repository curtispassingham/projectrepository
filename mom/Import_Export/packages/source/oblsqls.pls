CREATE OR REPLACE PACKAGE OBLIGATION_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--Function Name:   GET_NEXT_OBLIGATION
--Purpose      :   Retrieves the next value for the OBLIGATION_KEY.
--------------------------------------------------------------------------------
FUNCTION GET_NEXT_OBLIGATION (O_error_message    IN OUT   VARCHAR2,
                              O_obligation_key   IN OUT   OBLIGATION.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   GET_INFO
--Purpose      :   Retrieves information off of OBLIGATION table based on
--                 passed-in obligation key.
--------------------------------------------------------------------------------
FUNCTION GET_INFO (O_error_message      IN OUT   VARCHAR2,
                   O_exists             IN OUT   BOOLEAN,
                   O_obligation_level   IN OUT   OBLIGATION.OBLIGATION_LEVEL%TYPE,
                   O_key_value_1        IN OUT   OBLIGATION.KEY_VALUE_1%TYPE,
                   O_key_value_2        IN OUT   OBLIGATION.KEY_VALUE_2%TYPE,
                   O_key_value_3        IN OUT   OBLIGATION.KEY_VALUE_3%TYPE,
                   O_key_value_4        IN OUT   OBLIGATION.KEY_VALUE_4%TYPE,
                   O_key_value_5        IN OUT   OBLIGATION.KEY_VALUE_5%TYPE,
                   O_key_value_6        IN OUT   OBLIGATION.KEY_VALUE_6%TYPE,
                   O_partner_type       IN OUT   PARTNER.PARTNER_TYPE%TYPE,
                   O_partner_id         IN OUT   PARTNER.PARTNER_ID%TYPE,
                   O_supplier           IN OUT   OBLIGATION.SUPPLIER%TYPE,
                   O_payment_method     IN OUT   OBLIGATION.PAYMENT_METHOD%TYPE,
                   O_ext_invc_no        IN OUT   OBLIGATION.EXT_INVC_NO%TYPE,
                   O_ext_invc_date      IN OUT   OBLIGATION.EXT_INVC_DATE%TYPE,
                   O_paid_date          IN OUT   OBLIGATION.PAID_DATE%TYPE,
                   O_paid_amt           IN OUT   OBLIGATION.PAID_AMT%TYPE,
                   O_currency_code      IN OUT   CURRENCIES.CURRENCY_CODE%TYPE,
                   O_exchange_rate      IN OUT   CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                   O_check_auth_no      IN OUT   OBLIGATION.CHECK_AUTH_NO%TYPE,
                   O_status             IN OUT   OBLIGATION.STATUS%TYPE,
                   I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name:   EXT_INVC_VALIDATE
--Purpose      :   This function determines whether or not an External Invoice No already exists on
--                 the OBLIGATION table for the partner/supplier based on the passed-in External
--                 Invoice No, partner/supplier combination.
--------------------------------------------------------------------------------
FUNCTION EXT_INVC_VALIDATE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   BOOLEAN,
                            I_partner_id      IN       OBLIGATION.PARTNER_ID%TYPE,
                            I_supplier        IN       OBLIGATION.SUPPLIER%TYPE,
                            I_ext_invc_no     IN       OBLIGATION.EXT_INVC_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name:   EXT_INVC_EXISTS
--Purpose      :   This fuction determines whether or not an Ext. Invoice No exists on
--                 the OBLIGATION table based on the passed-in Ext. Invoice No when both Obligation Key
--                 and Obligation Level parameters are NULL. If the obligation_key is not NULL then the
--                 search will be based on the passed-in Ext. Invoice No and Obligation Key. If the
--                 Obligation Level is not NULL then the search will be based on passed-in
--                 Ext. Invoice No and obligation_level/key_value(1,2,3,4,5,6) combination.
--------------------------------------------------------------------------------
FUNCTION EXT_INVC_EXISTS (O_error_message      IN OUT   VARCHAR2,
                          O_exists             IN OUT   BOOLEAN,
                          I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                          I_obligation_level   IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                          I_key_value_1        IN       OBLIGATION.KEY_VALUE_1%TYPE,
                          I_key_value_2        IN       OBLIGATION.KEY_VALUE_2%TYPE,
                          I_key_value_3        IN       OBLIGATION.KEY_VALUE_3%TYPE,
                          I_key_value_4        IN       OBLIGATION.KEY_VALUE_4%TYPE,
                          I_key_value_5        IN       OBLIGATION.KEY_VALUE_5%TYPE,
                          I_key_value_6        IN       OBLIGATION.KEY_VALUE_6%TYPE,
                          I_ext_invc_no        IN       OBLIGATION.EXT_INVC_NO%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   AUTH_EXISTS
--Purpose      :   This fuction determines whether or not a Check Authorization Number exists on
--                 the OBLIGATION table based on the passed-in check_auth_no when both obligation_key
--                 and obligation_level parameters are NULL. If the obligation_key is not NULL then
--                 the search will be based on the passed in check_auth_no and obligation_key.
--                 If the obligation_level is not NULL then the search will be based on passed-in
--                 check_auth_no and obligation_level/key_value(1,2,3,4,5,6) combination.
--------------------------------------------------------------------------------
FUNCTION AUTH_EXISTS (O_error_message       IN OUT   VARCHAR2,
                      O_exists              IN OUT   BOOLEAN,
                      I_obligation_key      IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                      I_obligation_level    IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                      I_key_value_1         IN       OBLIGATION.KEY_VALUE_1%TYPE,
                      I_key_value_2         IN       OBLIGATION.KEY_VALUE_2%TYPE,
                      I_key_value_3         IN       OBLIGATION.KEY_VALUE_3%TYPE,
                      I_key_value_4         IN       OBLIGATION.KEY_VALUE_4%TYPE,
                      I_key_value_5         IN       OBLIGATION.KEY_VALUE_5%TYPE,
                      I_key_value_6         IN       OBLIGATION.KEY_VALUE_6%TYPE,
                      I_check_auth_no       IN       OBLIGATION.CHECK_AUTH_NO%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   COMP_EXIST
--Purpose      :   This fuction determines if any Components exist on the OBLIGATION_COMP table based
--                 on the passed-in obligation_key or obligation_level/key_value(1,2,3,4,5,6) combination.
--                 A Component may be passed in to verify existence of a specific comp_id.
--------------------------------------------------------------------------------
FUNCTION COMP_EXIST (O_error_message      IN OUT   VARCHAR2,
                     O_exists             IN OUT   BOOLEAN,
                     I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                     I_obligation_level   IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                     I_key_value_1        IN       OBLIGATION.KEY_VALUE_1%TYPE,
                     I_key_value_2        IN       OBLIGATION.KEY_VALUE_2%TYPE,
                     I_key_value_3        IN       OBLIGATION.KEY_VALUE_3%TYPE,
                     I_key_value_4        IN       OBLIGATION.KEY_VALUE_4%TYPE,
                     I_key_value_5        IN       OBLIGATION.KEY_VALUE_5%TYPE,
                     I_key_value_6        IN       OBLIGATION.KEY_VALUE_6%TYPE,
                     I_comp_id            IN       OBLIGATION_COMP.COMP_ID%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   LOC_EXIST
--Purpose      :   This fuction determines if any Locations exist on the OBLIGATION_COMP_LOC table based
--                 on the passed-in obligation_key/comp_id or obligation_level/key_value(1,2,3,4,5,6)/comp_id
--                 combination. A Location may be passed in to verify existence of a specific location.
--------------------------------------------------------------------------------
FUNCTION LOC_EXIST (O_error_message      IN OUT   VARCHAR2,
                    O_exists             IN OUT   BOOLEAN,
                    I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                    I_obligation_level   IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                    I_key_value_1        IN       OBLIGATION.KEY_VALUE_1%TYPE,
                    I_key_value_2        IN       OBLIGATION.KEY_VALUE_2%TYPE,
                    I_key_value_3        IN       OBLIGATION.KEY_VALUE_3%TYPE,
                    I_key_value_4        IN       OBLIGATION.KEY_VALUE_4%TYPE,
                    I_key_value_5        IN       OBLIGATION.KEY_VALUE_5%TYPE,
                    I_key_value_6        IN       OBLIGATION.KEY_VALUE_6%TYPE,
                    I_comp_id            IN       OBLIGATION_COMP.COMP_ID%TYPE,
                    I_location           IN       OBLIGATION_COMP_LOC.LOCATION%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name:   DELETE_COMPS
--Purpose      :   This fuction deletes Obligation Component records for a given
--                 Obligation Header record.
--------------------------------------------------------------------------------
FUNCTION DELETE_COMPS (O_error_message    IN OUT   VARCHAR2,
                       I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   DELETE_LOCS
--Purpose      :   This fuction deletes Obligation Component Location records for
--                 a given Obligation Component record.
--------------------------------------------------------------------------------
FUNCTION DELETE_LOCS (O_error_message    IN OUT   VARCHAR2,
                      I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                      I_comp_id          IN       OBLIGATION_COMP.COMP_ID%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   LOCK_OBLIGATION
--Purpose      :   This function locks an obligation record based on the passed
--                 obligation_key and obligation_level.
--------------------------------------------------------------------------------
FUNCTION LOCK_OBLIGATION (O_error_message    IN OUT   VARCHAR2,
                          I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                          I_obligation_level IN       OBLIGATION.OBLIGATION_LEVEL%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   LOCK_DETAILS
--Purpose      :   When NULL is passed in for I_lock_table and I_comp_id, this function
--                 locks detail records from obligation_comp and obligaiton_comp_loc tables
--                 based on the passed-in obligation key. When table 'OBLIGATION_COMP_LOC'
--                 and a Component ID is passed in, this function will only lock records of
--                 the Obligation Component Location table based on the passed-in
--                 Obligation Key and Component ID.  When table 'OBLIGATION_COMP' is passed
--                 in and Component ID is NULL,this funciton will only lock records of the
--                 Obligation Components table based on the obligation_key.
--------------------------------------------------------------------------------
FUNCTION LOCK_DETAILS (O_error_message    IN OUT   VARCHAR2,
                       I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                       I_lock_table       IN       VARCHAR2,
                       I_comp_id          IN       OBLIGATION_COMP.COMP_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name:   DELETE_ALC
--Purpose:         This fuction deletes all ALC records associated with the given
--                 Obligation.
--------------------------------------------------------------------------------
FUNCTION DELETE_ALC(O_error_message    IN OUT   VARCHAR2,
                    I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name:   CHECK_INV_LOCS
--Purpose:         This function determines whether the locations being saved to the
--                 OBLIGATION_COMP_LOC table actually exist on the order the obligation
--                 is logged against. Valid locations are determined by searching the order
--                 location (ORDLOC) table based on the passed-in obligation level, component
--                 and associated key values.  This function will only be called for obligation
--                 levels 'Purchase Order' and 'Purchase Order Item' since no obligation
--                 component location records will be stored for all other levels.
--                 If an obligation location is found that does
--                 not exists on ORDLOC table then O_inv_loc_exists will be set BOOLEAN TRUE.
--------------------------------------------------------------------------------
FUNCTION CHECK_INV_LOCS(O_error_message      IN OUT   VARCHAR2,
                        O_inv_loc_exists     IN OUT   BOOLEAN,
                        I_obligation_key     IN       OBLIGATION.OBLIGATION_KEY%TYPE,
                        I_obligation_level   IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                        I_key_value_1        IN       OBLIGATION.KEY_VALUE_1%TYPE,
                        I_key_value_2        IN       OBLIGATION.KEY_VALUE_2%TYPE,
                        I_comp_id            IN       OBLIGATION_COMP.COMP_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name:  OBL_EXISTS
--Purpose      :  This function checks to see if an obligation exists on the
--                obligation table.
--------------------------------------------------------------------------------
FUNCTION OBL_EXISTS(O_error_message    IN OUT   VARCHAR2,
                    O_exists           IN OUT   BOOLEAN,
                    I_obligation_key   IN       OBLIGATION.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- Function: OBL_FILTER_LIST
-- Purpose : This function will check the number of records in the tables
--           OBLIGATION_COMP_LOC and V_OBLIGATION_COMP_LOC for a given obligation
--           key.  If the number of records in these 2 tables are the same, the
--           parameter O_diff will return 'N', otherwise, it will return 'Y'.
----------------------------------------------------------------------------------
FUNCTION OBL_FILTER_LIST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diff             IN OUT   VARCHAR2,
                         I_obligation_key   IN       OBLIGATION_COMP_LOC.OBLIGATION_KEY%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------

FUNCTION GET_DEFAULT_QTY(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_qty                IN OUT   TRANSPORTATION.ITEM_QTY%TYPE,
                         O_item_qty_uom            IN OUT   TRANSPORTATION.ITEM_QTY_UOM%TYPE,
                         I_obligation_level        IN       OBLIGATION.OBLIGATION_LEVEL%TYPE,
                         I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                         I_voyage_id               IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                         I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                         I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE,
                         I_asn                     IN       SHIPMENT.ASN%TYPE,
                         I_item                    IN       TRANSPORTATION.ITEM%TYPE,
                         I_container_id            IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                         I_bl_awb_id               IN       TRANSPORTATION.BL_AWB_ID%TYPE,
                         I_entry_no                IN       CE_HEAD.ENTRY_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function: GET_TRAN_QTY
--PURPOSE:  Function will return the transportation item qty and uom for the vessel/voyage/etd/po/item
--          /container for different RTM transport option.
--          
----------------------------------------------------------------------------------------------------------
FUNCTION GET_TRAN_QTY (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_item_qty                IN OUT   TRANSPORTATION.ITEM_QTY%TYPE, 
                       O_item_qty_uom            IN OUT   TRANSPORTATION.ITEM_QTY_UOM%TYPE,
                       I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                       I_voyage_id               IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                       I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                       I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE, 
                       I_item                    IN       TRANSPORTATION.ITEM%TYPE,     
                       I_container_id            IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                       I_bl_awb_id               IN       TRANSPORTATION.BL_AWB_ID%TYPE)
   RETURN BOOLEAN;
END;
/
