CREATE OR REPLACE PACKAGE CORESVC_HTS_DEFINITION AUTHID CURRENT_USER AS
   template_key           CONSTANT VARCHAR2(255)  := 'HTS_DEFINITION_DATA';
   action_new                      VARCHAR2(25)   := 'NEW';
   action_mod                      VARCHAR2(25)   := 'MOD';
   action_del                      VARCHAR2(25)   := 'DEL';

   HTS_sheet                       VARCHAR2(255)  := 'HTS';
   HTS$Action                      NUMBER         := 1;
   HTS$HTS                         NUMBER         := 2;
   HTS$IMPORT_COUNTRY_ID           NUMBER         := 3;
   HTS$EFFECT_FROM                 NUMBER         := 4;
   HTS$EFFECT_TO                   NUMBER         := 5;
   HTS$HTS_DESC                    NUMBER         := 6;
   HTS$UNITS                       NUMBER         := 8;
   HTS$UNITS_1                     NUMBER         := 9;
   HTS$UNITS_2                     NUMBER         := 10;
   HTS$UNITS_3                     NUMBER         := 11;
   HTS$DUTY_COMP_CODE              NUMBER         := 12;
   HTS$MORE_HTS_IND                NUMBER         := 13;
   HTS$QUOTA_CAT                   NUMBER         := 14;
   HTS$QUOTA_IND                   NUMBER         := 15;
   HTS$AD_IND                      NUMBER         := 16;
   HTS$CVD_IND                     NUMBER         := 17;

   HTS_TL_sheet                    VARCHAR2(255)  := 'HTS_TL';
   HTS_TL$Action                   NUMBER         := 1;
   HTS_TL$LANG                     NUMBER         := 2;
   HTS_TL$HTS                      NUMBER         := 3;
   HTS_TL$IMPORT_COUNTRY_ID        NUMBER         := 4;
   HTS_TL$EFFECT_FROM              NUMBER         := 5;
   HTS_TL$EFFECT_TO                NUMBER         := 6;
   HTS_TL$HTS_DESC                 NUMBER         := 7;

   HTT_sheet                       VARCHAR2(255)  := 'HTS_TARIFF_TREATMENT';
   HTT$Action                      NUMBER         := 1;
   HTT$HTS                         NUMBER         := 2;
   HTT$IMPORT_COUNTRY_ID           NUMBER         := 3;
   HTT$EFFECT_FROM                 NUMBER         := 4;
   HTT$EFFECT_TO                   NUMBER         := 5;
   HTT$TARIFF_TREATMENT            NUMBER         := 6;
   HTT$SPECIFIC_RATE               NUMBER         := 7;
   HTT$AV_RATE                     NUMBER         := 8;
   HTT$OTHER_RATE                  NUMBER         := 9;

   HTS_TT_EXC_sheet                VARCHAR2(255)  := 'HTS_TT_EXCLUSIONS';
   HTS_TT_EXC$Action               NUMBER         := 1;
   HTS_TT_EXC$HTS                  NUMBER         := 2;
   HTS_TT_EXC$IMPORT_COUNTRY_ID    NUMBER         := 3;
   HTS_TT_EXC$EFFECT_FROM          NUMBER         := 4;
   HTS_TT_EXC$EFFECT_TO            NUMBER         := 5;
   HTS_TT_EXC$TARIFF_TREATMENT     NUMBER         := 6;
   HTS_TT_EXC$ORIGIN_COUNTRY_ID    NUMBER         := 7;

   HTS_TT_ZONE_sheet               VARCHAR2(255)  := 'HTS_TARIFF_TREATMENT_ZONE';
   HTS_TT_ZONE$Action              NUMBER         := 1;
   HTS_TT_ZONE$HTS                 NUMBER         := 2;
   HTS_TT_ZONE$IMPORT_COUNTRY_ID   NUMBER         := 3;
   HTS_TT_ZONE$EFFECT_FROM         NUMBER         := 4;
   HTS_TT_ZONE$EFFECT_TO           NUMBER         := 5;
   HTS_TT_ZONE$TARIFF_TREATMENT    NUMBER         := 6;
   HTS_TT_ZONE$CLEARING_ZONE_ID    NUMBER         := 7;
   HTS_TT_ZONE$SPECIFIC_RATE       NUMBER         := 8;
   HTS_TT_ZONE$AV_RATE             NUMBER         := 9;
   HTS_TT_ZONE$OTHER_RATE          NUMBER         := 10;

   HTS_FEE_sheet                   VARCHAR2(255)  := 'HTS_FEE';
   HTS_FEE$Action                  NUMBER         := 1;
   HTS_FEE$HTS                     NUMBER         := 2;
   HTS_FEE$IMPORT_COUNTRY_ID       NUMBER         := 3;
   HTS_FEE$EFFECT_FROM             NUMBER         := 4;
   HTS_FEE$EFFECT_TO               NUMBER         := 5;
   HTS_FEE$FEE_TYPE                NUMBER         := 6;
   HTS_FEE$FEE_COMP_CODE           NUMBER         := 7;
   HTS_FEE$FEE_SPECIFIC_RATE       NUMBER         := 8;
   HTS_FEE$FEE_AV_RATE             NUMBER         := 9;

   HTS_FEE_ZONE_sheet              VARCHAR2(255)  := 'HTS_FEE_ZONE';
   HTS_FEE_ZONE$Action             NUMBER         := 1;
   HTS_FEE_ZONE$HTS                NUMBER         := 2;
   HTS_FEE_ZONE$IMPORT_COUNTRY_ID  NUMBER         := 3;
   HTS_FEE_ZONE$EFFECT_FROM        NUMBER         := 4;
   HTS_FEE_ZONE$EFFECT_TO          NUMBER         := 5;
   HTS_FEE_ZONE$FEE_TYPE           NUMBER         := 6;
   HTS_FEE_ZONE$CLEARING_ZONE_ID   NUMBER         := 7;
   HTS_FEE_ZONE$FEE_SPECIFIC_RATE  NUMBER         := 8;
   HTS_FEE_ZONE$FEE_AV_RATE        NUMBER         := 9;

   HTS_TAX_sheet                   VARCHAR2(25)   := 'HTS_TAX';
   HTS_TAX$Action                  NUMBER         := 1;
   HTS_TAX$HTS                     NUMBER         := 2;
   HTS_TAX$IMPORT_COUNTRY_ID       NUMBER         := 3;
   HTS_TAX$EFFECT_FROM             NUMBER         := 4;
   HTS_TAX$EFFECT_TO               NUMBER         := 5;
   HTS_TAX$TAX_TYPE                NUMBER         := 6;
   HTS_TAX$TAX_COMP_CODE           NUMBER         := 7;
   HTS_TAX$TAX_SPECIFIC_RATE       NUMBER         := 8;
   HTS_TAX$TAX_AV_RATE             NUMBER         := 9;

   HTS_TAX_ZONE_sheet              VARCHAR2(255)  := 'HTS_TAX_ZONE';
   HTS_TAX_ZONE$Action             NUMBER         := 1;
   HTS_TAX_ZONE$HTS                NUMBER         := 2;
   HTS_TAX_ZONE$IMPORT_COUNTRY_ID  NUMBER         := 3;
   HTS_TAX_ZONE$EFFECT_FROM        NUMBER         := 4;
   HTS_TAX_ZONE$EFFECT_TO          NUMBER         := 5;
   HTS_TAX_ZONE$TAX_TYPE           NUMBER         := 6;
   HTS_TAX_ZONE$CLEARING_ZONE_ID   NUMBER         := 7;
   HTS_TAX_ZONE$TAX_SPECIFIC_RATE  NUMBER         := 8;
   HTS_TAX_ZONE$TAX_AV_RATE        NUMBER         := 9;

   HTS_AD_sheet                    VARCHAR2(25)   := 'HTS_AD';
   HTS_AD$Action                   NUMBER         := 1;
   HTS_AD$HTS                      NUMBER         := 2;
   HTS_AD$IMPORT_COUNTRY_ID        NUMBER         := 3;
   HTS_AD$EFFECT_FROM              NUMBER         := 4;
   HTS_AD$EFFECT_TO                NUMBER         := 5;
   HTS_AD$MFG_ID                   NUMBER         := 6;
   HTS_AD$ORIGIN_COUNTRY_ID        NUMBER         := 7;
   HTS_AD$CASE_NO                  NUMBER         := 8;
   HTS_AD$SHIPPER_ID               NUMBER         := 9;
   HTS_AD$SUPPLIER                 NUMBER         := 10;
   HTS_AD$RATE                     NUMBER         := 11;
   HTS_AD$RELATED_CASE_NO          NUMBER         := 12;
   HTS_AD$EFFECTIVE_ENTRY_DATE     NUMBER         := 13;
   HTS_AD$EFFECTIVE_EXPORT_DATE    NUMBER         := 14;

   HTS_CVD_sheet                   VARCHAR2(25)   := 'HTS_CVD';
   HTS_CVD$Action                  NUMBER         := 1;
   HTS_CVD$HTS                     NUMBER         := 2;
   HTS_CVD$IMPORT_COUNTRY_ID       NUMBER         := 3;
   HTS_CVD$EFFECT_FROM             NUMBER         := 4;
   HTS_CVD$EFFECT_TO               NUMBER         := 5;
   HTS_CVD$ORIGIN_COUNTRY_ID       NUMBER         := 6;
   HTS_CVD$CASE_NO                 NUMBER         := 7;
   HTS_CVD$MFG_ID                  NUMBER         := 8;
   HTS_CVD$SHIPPER_ID              NUMBER         := 9;
   HTS_CVD$SUPPLIER                NUMBER         := 10;
   HTS_CVD$RATE                    NUMBER         := 11;
   HTS_CVD$RELATED_CASE_NO         NUMBER         := 12;
   HTS_CVD$EFFECTIVE_ENTRY_DATE    NUMBER         := 13;
   HTS_CVD$EFFECTIVE_EXPORT_DATE   NUMBER         := 14;

   HTS_REF_sheet                   VARCHAR2(255)  := 'HTS_REFERENCE';
   HTS_REF$Action                  NUMBER         := 1;
   HTS_REF$HTS                     NUMBER         := 2;
   HTS_REF$IMPORT_COUNTRY_ID       NUMBER         := 3;
   HTS_REF$EFFECT_FROM             NUMBER         := 4;
   HTS_REF$EFFECT_TO               NUMBER         := 5;
   HTS_REF$REFERENCE_ID            NUMBER         := 6;
   HTS_REF$REFERENCE_DESC          NUMBER         := 7;

   HTS_OGA_sheet                   VARCHAR2(255)  := 'HTS_OGA';
   HTS_OGA$Action                  NUMBER         := 1;
   HTS_OGA$HTS                     NUMBER         := 2;
   HTS_OGA$IMPORT_COUNTRY_ID       NUMBER         := 3;
   HTS_OGA$EFFECT_FROM             NUMBER         := 4;
   HTS_OGA$EFFECT_TO               NUMBER         := 5;
   HTS_OGA$OGA_CODE                NUMBER         := 6;
   HTS_OGA$REFERENCE_ID            NUMBER         := 7;
   HTS_OGA$COMMENTS                NUMBER         := 8;

   sheet_name_trans                S9T_PKG.TRANS_MAP_TYP;
   action_column                   VARCHAR2(255)  := 'ACTION';
   template_category               CODE_DETAIL.CODE%TYPE := 'RMSIMP';
   TYPE HTS_rec_tab is TABLE OF HTS%ROWTYPE;
   TYPE HTS_AD_rec_tab is TABLE OF HTS_AD%ROWTYPE;
   TYPE HTS_CVD_rec_tab is TABLE OF HTS_CVD%ROWTYPE;
   TYPE HTS_FEE_rec_tab is TABLE OF HTS_FEE%ROWTYPE;
   TYPE HTS_FEE_ZONE_rec_tab is TABLE OF HTS_FEE_ZONE%ROWTYPE;
   TYPE HTS_OGA_rec_tab is TABLE OF HTS_OGA%ROWTYPE;
   TYPE HTS_REF_rec_tab is TABLE OF HTS_REFERENCE%ROWTYPE;
   TYPE HTT_rec_tab is TABLE OF HTS_TARIFF_TREATMENT%ROWTYPE;
   TYPE HTS_TT_EXC_rec_tab is TABLE OF HTS_TT_EXCLUSIONS%ROWTYPE;
   TYPE HTS_TT_ZONE_rec_tab IS TABLE OF HTS_TARIFF_TREATMENT_ZONE%ROWTYPE;
   TYPE HTS_TAX_rec_tab is TABLE OF HTS_TAX%ROWTYPE;
   TYPE HTS_TAX_ZONE_rec_tab is TABLE OF HTS_TAX_ZONE%ROWTYPE;
--------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_count     IN OUT   NUMBER, 
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------
END CORESVC_HTS_DEFINITION;
/
