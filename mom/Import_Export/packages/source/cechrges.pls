CREATE OR REPLACE PACKAGE CE_CHARGES_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------
--Function Name: GET_NEXT_SEQ
--Purpose      : Retrieves the next sequence number for the given 
--               CE Ref ID/Vessel/Voyage/ETD/Order/Item combination
--               from the ce_charges table.
--------------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ(O_error_message          IN OUT  VARCHAR2,
                      O_seq_no                 IN OUT  CE_CHARGES.SEQ_NO%TYPE,
                      I_ce_id                  IN      CE_CHARGES.CE_ID%TYPE,
                      I_vessel_id              IN      CE_CHARGES.VESSEL_ID%TYPE,
                      I_voyage_id              IN      CE_CHARGES.VOYAGE_FLT_ID%TYPE,
                      I_estimated_depart_date  IN      CE_CHARGES.ESTIMATED_DEPART_DATE%TYPE,
                      I_order_no               IN      ORDHEAD.ORDER_NO%TYPE,
                      I_item                   IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
--Function Name: CHARGES_EXIST
--Purpose      : Checks to see if a given customs entry charges record exists
--               on the ce_charges table.
--------------------------------------------------------------------------------------
FUNCTION CHARGES_EXIST(O_error_message          IN OUT  VARCHAR2,
                       O_exists                 IN OUT  BOOLEAN,
                       I_ce_id                  IN      CE_CHARGES.CE_ID%TYPE,
                       I_vessel_id              IN      CE_CHARGES.VESSEL_ID%TYPE,
                       I_voyage_id              IN      CE_CHARGES.VOYAGE_FLT_ID%TYPE,
                       I_estimated_depart_date  IN      CE_CHARGES.ESTIMATED_DEPART_DATE%TYPE,
                       I_order_no               IN      ORDHEAD.ORDER_NO%TYPE,
                       I_item                   IN      ITEM_MASTER.ITEM%TYPE,
                       I_pack_item              IN      ITEM_MASTER.ITEM%TYPE,
                       I_hts                    IN      HTS.HTS%TYPE,
                       I_effect_from            IN      HTS.EFFECT_FROM%TYPE,
                       I_effect_to              IN      HTS.EFFECT_TO%TYPE,
                       I_comp_id                IN      ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name: DELETE_HTS
--Purpose      : Deletes all records on the ce_charges table for a given
--               CE Ref ID/Vessel/Voyage/ETD/Order/Item/Hts combination.
--------------------------------------------------------------------------------------
FUNCTION DELETE_HTS(O_error_message          IN OUT  VARCHAR2,
                    I_ce_id                  IN      CE_CHARGES.CE_ID%TYPE,
                    I_vessel_id              IN      CE_CHARGES.VESSEL_ID%TYPE,
                    I_voyage_id              IN      CE_CHARGES.VOYAGE_FLT_ID%TYPE,
                    I_estimated_depart_date  IN      CE_CHARGES.ESTIMATED_DEPART_DATE%TYPE,
                    I_order_no               IN      ORDHEAD.ORDER_NO%TYPE,
                    I_item                   IN      ITEM_MASTER.ITEM%TYPE,
                    I_pack_item              IN      ITEM_MASTER.ITEM%TYPE,
                    I_hts                    IN      HTS.HTS%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
--Function Name: INSERT_COMPS
--Purpose      : Defaults assessments from the ordsku_hts_assess table that
--               correspond to the passed in hts.  Also, defaults any expense
--               components that exist in the CVB 'Value For Duty'.
--------------------------------------------------------------------------------------
FUNCTION INSERT_COMPS(O_error_message         IN OUT VARCHAR2,
                      I_ce_id                 IN     CE_HEAD.CE_ID%TYPE,
                      I_vessel_id             IN     CE_SHIPMENT.VESSEL_ID%TYPE,
                      I_voyage_flt_id         IN     CE_SHIPMENT.VOYAGE_FLT_ID%TYPE,
                      I_estimated_depart_date IN     CE_SHIPMENT.ESTIMATED_DEPART_DATE%TYPE,
                      I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                      I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                      I_pack_item             IN     ITEM_MASTER.ITEM%TYPE,
                      I_hts                   IN     HTS.HTS%TYPE,
                      I_import_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                      I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                      I_effect_to             IN     HTS.EFFECT_TO%TYPE,
                      I_cvb_code              IN     CVB_HEAD.CVB_CODE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name: INSERT_COMPS
--Purpose      : This is an overloaded function which does not take the cvb_code
--               Defaults assessments from the ordsku_hts_assess table that
--               correspond to the passed in hts.  Also, defaults any expense
--               components that exist in the CVB 'Value For Duty'.
--------------------------------------------------------------------------------------
FUNCTION INSERT_COMPS(O_error_message         IN OUT VARCHAR2,
                      I_ce_id                 IN     CE_HEAD.CE_ID%TYPE,
                      I_vessel_id             IN     CE_SHIPMENT.VESSEL_ID%TYPE,
                      I_voyage_flt_id         IN     CE_SHIPMENT.VOYAGE_FLT_ID%TYPE,
                      I_estimated_depart_date IN     CE_SHIPMENT.ESTIMATED_DEPART_DATE%TYPE,
                      I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                      I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                      I_pack_item             IN     ITEM_MASTER.ITEM%TYPE,
                      I_hts                   IN     HTS.HTS%TYPE,
                      I_import_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                      I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                      I_effect_to             IN     HTS.EFFECT_TO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:   CALC_TOTALS
--Purpose      :   Calculates the total duty, total taxes, total VFD, total elc
--                 and any other component total 
--------------------------------------------------------------------------------------
FUNCTION CALC_TOTALS(O_error_message     IN OUT VARCHAR2,
                     O_total_duty        IN OUT CE_CHARGES.COMP_VALUE%TYPE,
                     O_total_vfd         IN OUT CE_CHARGES.COMP_VALUE%TYPE,
                     O_total_taxes       IN OUT CE_CHARGES.COMP_VALUE%TYPE,
                     O_total_other       IN OUT CE_CHARGES.COMP_VALUE%TYPE,
                     O_total_est_assess  IN OUT CE_CHARGES.COMP_VALUE%TYPE,
                     I_ce_id             IN     CE_HEAD.CE_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:   DEFAULT_CHARGES
--Purpose      :   Defaults assessments and expenses into ce_charges for 
--                 the given CE Ref ID/Vessel/Voyage/ETD/Order/Item combination
--------------------------------------------------------------------------------------
FUNCTION DEFAULT_CHARGES(O_error_message   IN OUT VARCHAR2,
                         I_ce_id           IN     CE_HEAD.CE_ID%TYPE,
                         I_vessel_id       IN     CE_SHIPMENT.VESSEL_ID%TYPE,
                         I_voyage_flt_id   IN     CE_SHIPMENT.VOYAGE_FLT_ID%TYPE,
                         I_est_depart_date IN     CE_SHIPMENT.ESTIMATED_DEPART_DATE%TYPE,
                         I_order_no        IN     CE_ORD_ITEM.ORDER_NO%TYPE,
                         I_item            IN     CE_ORD_ITEM.ITEM%TYPE,
                         I_import_country  IN     COUNTRY.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:   CVB_RATE_TOTAL
--Purpose      :   This function will sum the CVB comp_id rates for all CE_charges
--                 for given ce_id, VVE, order_no, item and pack_item.
--------------------------------------------------------------------------------------
FUNCTION CVB_RATE_TOTAL(O_error_message     IN OUT VARCHAR2,
                        O_rate_total        IN OUT ELC_COMP.COMP_RATE%TYPE,
                        I_ce_id             IN     CE_HEAD.CE_ID%TYPE,
                        I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                        I_vessel_id         IN     CE_SHIPMENT.VESSEL_ID%TYPE,
                        I_voyage_flt_id     IN     CE_SHIPMENT.VOYAGE_FLT_ID%TYPE,
                        I_est_depart_date   IN     CE_SHIPMENT.ESTIMATED_DEPART_DATE%TYPE,
                        I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                        I_item              IN     ITEM_MASTER.ITEM%TYPE,
                        I_pack_item         IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:   ORD_HTS_ASSESS_EXISTS
--Purpose      :   This function will check for the existence of ORDSKU_HTS_ASSESS
--                 for the given comp_id.
--------------------------------------------------------------------------------------
FUNCTION ORD_HTS_ASSESS_EXISTS(O_error_message  IN OUT VARCHAR2,
                               O_exists         IN OUT  BOOLEAN,
                               I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                               I_item           IN     ITEM_MASTER.ITEM%TYPE,
                               I_pack_item      IN     ITEM_MASTER.ITEM%TYPE,
                               I_hts            IN     HTS.HTS%TYPE,
                               I_effect_from    IN     HTS.EFFECT_FROM%TYPE,
                               I_effect_to      IN     HTS.EFFECT_TO%TYPE,
                               I_comp_id        IN     ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:   VALIDATE_CHARGES
--Purpose      :   This function will Validate the passed-in comp_id for the CE_charges
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_CHARGES(O_error_message     IN OUT VARCHAR2,
                          O_valid             IN OUT BOOLEAN,
                          I_ce_id             IN     CE_HEAD.CE_ID%TYPE,
                          I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                          I_comp_id           IN     ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN;     
--------------------------------------------------------------------------------------
--Function Name:   UPDATE_CVB_CODE
--Purpose      :   This function will update all cvb_codes for the passed-in ce_id, VVE
--                 order_no, HTS, effect_from, effect_to, item and pack_item (if applies)
--                 on the CE_CHARGES table with the passed in cvb_code.
--------------------------------------------------------------------------------------
FUNCTION UPDATE_CVB_CODE(O_error_message         IN OUT VARCHAR2,
                         I_ce_id                 IN     CE_HEAD.CE_ID%TYPE,
                         I_vessel_id             IN     CE_SHIPMENT.VESSEL_ID%TYPE,
                         I_voyage_flt_id         IN     CE_SHIPMENT.VOYAGE_FLT_ID%TYPE,
                         I_estimated_depart_date IN     CE_SHIPMENT.ESTIMATED_DEPART_DATE%TYPE,
                         I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item             IN     ITEM_MASTER.ITEM%TYPE,
                         I_hts                   IN     HTS.HTS%TYPE,
                         I_import_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                         I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                         I_effect_to             IN     HTS.EFFECT_TO%TYPE,
                         I_cvb_code              IN     CVB_HEAD.CVB_CODE%TYPE)
RETURN BOOLEAN;     
--------------------------------------------------------------------------------------
--Function Name:   CALC_PACK_QTY
--Purpose      :   This new function will be called to convert a pack quantity from 
--                 the passed in quantity and UOM to Eaches ('EA') UOM.  The function will 
--                 assume that a valid pack_no is passed in and will return a value of 
--                 zero for the qty if conversion failed due to missing or insufficient
--                 conversion information.
--------------------------------------------------------------------------------------
FUNCTION CALC_PACK_QTY(O_error_message  IN OUT VARCHAR2,
                       O_pack_qty       IN OUT CE_ORD_ITEM.MANIFEST_ITEM_QTY%TYPE,
                       I_pack_no        IN     ITEM_MASTER.ITEM%TYPE,
                       I_qty            IN     CE_ORD_ITEM.MANIFEST_ITEM_QTY%TYPE,
                       I_uom            IN     UOM_CLASS.UOM%TYPE,
                       I_supplier       IN     SUPS.SUPPLIER%TYPE,
                       I_origin_country IN     COUNTRY.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:   CALC_CE_ORD_ITEM_CHRG
--Purpose      :   This function will be called to calculate the CE_COMP charge for a given 
--                 order/item and comp_id combination associated with an HTS code that 
--                 does not exist on the ORDSKU_HTS table.
--------------------------------------------------------------------------------------
FUNCTION CALC_CE_ORD_ITEM_CHRG(O_error_message         IN OUT VARCHAR2,
                               O_ce_comp_rate          IN OUT ELC_COMP.COMP_RATE%TYPE,
                               O_ce_comp_value         IN OUT ORDLOC_EXP.EST_EXP_VALUE%TYPE,
                               I_ce_id                 IN     CE_HEAD.CE_ID%TYPE,
                               I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                               I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                               I_pack_item             IN     ITEM_MASTER.ITEM%TYPE,
                               I_hts                   IN     HTS.HTS%TYPE,
                               I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                               I_effect_to             IN     HTS.EFFECT_TO%TYPE,
                               I_cvb_code              IN     CVB_HEAD.CVB_CODE%TYPE,
                               I_ce_currency_code      IN     CURRENCIES.CURRENCY_CODE%TYPE,
                               I_ce_exchange_rate      IN     ORDLOC_EXP.EXCHANGE_RATE%TYPE,
                               I_comp_id               IN     ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:   CALC_UPDATE_ORD_ITEM_CE_COMP
--Purpose      :   This function will call CALC_CE_ORD_ITEM_CHRG followed by 
--                 UPDATE_ORD_ITEM_CE_COMP for every comp id in ce_comp_min_max
--------------------------------------------------------------------------------------
FUNCTION CALC_UPDATE_ORD_ITEM_CE_COMP(O_error_message         IN OUT VARCHAR2,
                                      I_ce_id                 IN     CE_HEAD.CE_ID%TYPE,
                                      I_vessel_id             IN     CE_SHIPMENT.VESSEL_ID%TYPE,
                                      I_voyage_flt_id         IN     CE_SHIPMENT.VOYAGE_FLT_ID%TYPE,
                                      I_estimated_depart_date IN     CE_SHIPMENT.ESTIMATED_DEPART_DATE%TYPE,
                                      I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                                      I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                                      I_pack_item             IN     ITEM_MASTER.ITEM%TYPE,
                                      I_hts                   IN     HTS.HTS%TYPE,
                                      I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                                      I_effect_to             IN     HTS.EFFECT_TO%TYPE,
                                      I_cvb_code              IN     CVB_HEAD.CVB_CODE%TYPE,
                                      I_ce_currency_code      IN     CURRENCIES.CURRENCY_CODE%TYPE,
                                      I_ce_exchange_rate      IN     ORDLOC_EXP.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:   UPDATE_ORD_ITEM_CE_COMP
--Purpose      :   This function will update the CE_COMP component rate and value with the 
--                 passed-in ce_comp_rate and ce_comp_value on the CE-CHARGES table for the passed-in 
--                 ce_id, vessel_id, voyage_flt_id, estimated_depart_date, order, item, 
--                 pack_item (if applicable), hts, effect_from, effect_to and where the comp_id 
--                 is passed as input
--------------------------------------------------------------------------------------
FUNCTION UPDATE_ORD_ITEM_CE_COMP(O_error_message         IN OUT VARCHAR2,
                                 I_ce_id                 IN     CE_HEAD.CE_ID%TYPE,
                                 I_vessel_id             IN     CE_SHIPMENT.VESSEL_ID%TYPE,
                                 I_voyage_flt_id         IN     CE_SHIPMENT.VOYAGE_FLT_ID%TYPE,
                                 I_estimated_depart_date IN     CE_SHIPMENT.ESTIMATED_DEPART_DATE%TYPE,
                                 I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                                 I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                                 I_pack_item             IN     ITEM_MASTER.ITEM%TYPE,
                                 I_hts                   IN     HTS.HTS%TYPE,
                                 I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                                 I_effect_to             IN     HTS.EFFECT_TO%TYPE,
                                 I_ce_comp_rate          IN     ELC_COMP.COMP_RATE%TYPE,
                                 I_ce_comp_value         IN     ORDSKU_HTS_ASSESS.EST_ASSESS_VALUE%TYPE,
                                 I_comp_id               IN     ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:   GET_CALC_CE_COMP
--Purpose      :   This function will sum the CE_COMP charges on the Customs Entry, 
--                 returned by CALC_CE_COMP for each Comp_id in CE_COMP_MIN)MAX table 
--------------------------------------------------------------------------------------
FUNCTION GET_CALC_CE_COMP(O_error_message   IN OUT VARCHAR2,
                          O_total_ce_comp   IN OUT CE_CHARGES.COMP_VALUE%TYPE,
                          I_ce_id           IN     CE_HEAD.CE_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:   CALC_CE_COMP
--Purpose      :   This function will sum the CE_COMP charges on the Customs Entry, 
--                 truncate them to fall within the min/max range (if necessary) and 
--                 distribute the charges back to each item.
--------------------------------------------------------------------------------------
FUNCTION CALC_CE_COMP(O_error_message   IN OUT VARCHAR2,
                      O_total_ce_comp   IN OUT CE_CHARGES.COMP_VALUE%TYPE,
                      I_ce_id           IN     CE_HEAD.CE_ID%TYPE,
                      I_comp_id         IN     CE_CHARGES.COMP_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:   DISTRIBUTE_CE_COMP
--Purpose      :   This new function will take the passed in total CE_COMP value and 
--                 distribute the charge appropriately to the CE_CHARGES table 
--                 for the passed in ce_id.
--------------------------------------------------------------------------------------
FUNCTION DISTRIBUTE_CE_COMP(O_error_message    IN OUT VARCHAR2,
                            I_ce_id            IN     CE_HEAD.CE_ID%TYPE,
                            I_total_ce_comp    IN     CE_CHARGES.COMP_VALUE%TYPE,
                            I_ce_currency      IN     CURRENCIES.CURRENCY_CODE%TYPE,
                            I_ce_exchange_rate IN     CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                            I_comp_id          IN     CE_CHARGES.COMP_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name: HTS_EXISTS
--Purpose      : Checks to see if a given HTS code exists
--               on the ce_charges table for the passed in 
--               CE/VVE/Order/Item/Pack Item combination.
--------------------------------------------------------------------------------------
FUNCTION HTS_EXISTS(O_error_message          IN OUT  VARCHAR2,
                    O_exists                 IN OUT  BOOLEAN,
                    I_ce_id                  IN      CE_CHARGES.CE_ID%TYPE,
                    I_vessel_id              IN      CE_CHARGES.VESSEL_ID%TYPE,
                    I_voyage_id              IN      CE_CHARGES.VOYAGE_FLT_ID%TYPE,
                    I_estimated_depart_date  IN      CE_CHARGES.ESTIMATED_DEPART_DATE%TYPE,
                    I_order_no               IN      ORDHEAD.ORDER_NO%TYPE,
                    I_item                   IN      ITEM_MASTER.ITEM%TYPE,
                    I_pack_item              IN      ITEM_MASTER.ITEM%TYPE,
                    I_hts                    IN      HTS.HTS%TYPE,
                    I_effect_from            IN      HTS.EFFECT_FROM%TYPE,
                    I_effect_to              IN      HTS.EFFECT_TO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: INSERT_ALWAYS_COMPS
-- Purpose:       This function will insert any 'Always Default' assessment 
--                components for a newly added HTS code to a CE/VVE/Order/Item/Pack
--                combination.
--------------------------------------------------------------------------------------
FUNCTION INSERT_ALWAYS_COMPS(O_error_message         IN OUT VARCHAR2,
                             I_ce_id                 IN     CE_HEAD.CE_ID%TYPE,
                             I_vessel_id             IN     CE_SHIPMENT.VESSEL_ID%TYPE,
                             I_voyage_flt_id         IN     CE_SHIPMENT.VOYAGE_FLT_ID%TYPE,
                             I_estimated_depart_date IN     CE_SHIPMENT.ESTIMATED_DEPART_DATE%TYPE,
                             I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                             I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                             I_pack_item             IN     ITEM_MASTER.ITEM%TYPE,
                             I_hts                   IN     HTS.HTS%TYPE,
                             I_import_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                             I_effect_to             IN     HTS.EFFECT_TO%TYPE,
                             I_comp_id               IN     ELC_COMP.COMP_ID%TYPE,
                             I_cvb_code              IN     CVB_HEAD.CVB_CODE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: CHECK_COMP_ID_EXISTS
-- Purpose:       This function will check if the input Comp_id and Country exists in 
--                CE_COMP_MIN_MAX table
--------------------------------------------------------------------------------------
FUNCTION CHECK_COMP_ID_EXISTS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists                IN OUT BOOLEAN,
                              I_comp_id               IN     CE_COMP_MIN_MAX.COMP_ID%TYPE,
                              I_import_country_id     IN     COUNTRY.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
END CE_CHARGES_SQL;
/
