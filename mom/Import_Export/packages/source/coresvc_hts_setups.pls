CREATE OR REPLACE PACKAGE CORESVC_HTS_SETUP AUTHID CURRENT_USER AS
   template_key        CONSTANT VARCHAR2(255)       :='HTS_SETUP_DATA';
   template_category   CODE_DETAIL.CODE%TYPE        :='RMSIMP';

   action_new                       VARCHAR2(25)    :='NEW';
   action_mod                       VARCHAR2(25)    :='MOD';
   action_del                       VARCHAR2(25)    :='DEL';

   HTS_IMP_CNT_SETUP_sheet          VARCHAR2(255)   :='HTS_IMPORT_COUNTRY_SETUP';
   HTS_IMP_CNT_SETUP$Action         NUMBER          :=1;
   HTS_IMP_CNT_SETUP$IMP_CNT_ID     NUMBER          :=2;
   HTS_IMP_CNT_SETUP$HTS_FMT_MASK   NUMBER          :=3;
   HTS_IMP_CNT_SETUP$HTS_HED_LNTH   NUMBER          :=4;

   TARIFF_TREATMENT_sheet           VARCHAR2(255)   :='TARIFF_TREATMENT';
   TARIFF_TREATMENT$Action          NUMBER          :=1;
   TARIFF_TRTMNT$TARIFF_TRTMNT      NUMBER          :=2;
   TARIFF_TRTMNT$TARIF_TRTMNT_DEC   NUMBER          :=3;
   TARIFF_TRTMNT$CONDITIONAL_IND    NUMBER          :=4;

   TARIFF_TREATMENT_TL_sheet        VARCHAR2(255)   :='TARIFF_TREATMENT_TL';
   TARIFF_TREATMENT_TL$Action       NUMBER          :=1;
   TARIFF_TREATMENT_TL$LANG         NUMBER          :=2;
   TARIFF_TRTMNT_TL$TARIFF_TRTMNT   NUMBER          :=3;
   TARIFF_TRMT_TL$TARIFF_TRMT_DEC   NUMBER          :=4;

   HTS_CHAPTER_sheet                VARCHAR2(255)   := 'HTS_CHAPTER';
   HTS_CHAPTER$Action               NUMBER          :=1;
   HTS_CHAPTER$IMPORT_COUNTRY_ID    NUMBER          :=4;
   HTS_CHAPTER$CHAPTER_DESC         NUMBER          :=3;
   HTS_CHAPTER$CHAPTER              NUMBER          :=2;

   HTS_CHAPTER_TL_sheet             VARCHAR2(255)   := 'HTS_CHAPTER_TL';
   HTS_CHAPTER_TL$Action            NUMBER          :=1;
   HTS_CHAPTER_TL$LANG              NUMBER          :=2;
   HTS_CHAPTER_TL$IMPT_CTRY_ID      NUMBER          :=3;
   HTS_CHAPTER_TL$CHAPTER           NUMBER          :=4;
   HTS_CHAPTER_TL$CHAPTER_DESC      NUMBER          :=5;

   HTS_CR_sheet                     VARCHAR2(255)   := 'HTS_CHAPTER_RESTRAINTS';
   HTS_CR$ACTION                    NUMBER          := 1;
   HTS_CR$CHAPTER                   NUMBER          := 2;
   HTS_CR$IMPORT_COUNTRY_ID         NUMBER          := 3;
   HTS_CR$ORIGIN_COUNTRY_ID         NUMBER          := 4;
   HTS_CR$RESTRAINT_TYPE            NUMBER          := 5;
   HTS_CR$RESTRAINT_DESC            NUMBER          := 6;
   HTS_CR$RESTRAINT_QTY             NUMBER          := 7;
   HTS_CR$UOM                       NUMBER          := 8;
   HTS_CR$CLOSING_DATE              NUMBER          := 9;
   HTS_CR$QUOTA_CAT                 NUMBER          := 10;
   HTS_CR$RESTRAINT_SUFFIX          NUMBER          := 11;
   HTS_CR$RESTRAINT_TYPE_UPD        NUMBER          := 12;

   HTS_CR_TL_sheet                  VARCHAR2(255)   := 'HTS_CHAPTER_RESTRAINTS_TL';
   HTS_CR_TL$ACTION                 NUMBER          := 1;
   HTS_CR_TL$LANG                   NUMBER          := 2;
   HTS_CR_TL$CHAPTER                NUMBER          := 3;
   HTS_CR_TL$IMPORT_COUNTRY_ID      NUMBER          := 4;
   HTS_CR_TL$ORIGIN_COUNTRY_ID      NUMBER          := 5;
   HTS_CR_TL$RESTRAINT_TYPE         NUMBER          := 6;
   HTS_CR_TL$RESTRAINT_DESC         NUMBER          := 7; 
   
   QUOTA_CAT_sheet                  VARCHAR2(255)   := 'QUOTA_CATEGORY';
   QUOTA_CAT$Action                 NUMBER          := 1;
   QUOTA_CAT$QUOTA_CAT              NUMBER          := 2;
   QUOTA_CAT$IMPORT_COUNTRY_ID      NUMBER          := 3;
   QUOTA_CAT$CATEGORY_DESC          NUMBER          := 4;
   
   QUOTA_CAT_TL_sheet               VARCHAR2(255)   := 'QUOTA_CATEGORY_TL';
   QUOTA_CAT_TL$Action              NUMBER          := 1;
   QUOTA_CAT_TL$LANG                NUMBER          := 2;
   QUOTA_CAT_TL$QUOTA_CAT           NUMBER          := 3;
   QUOTA_CAT_TL$IMPORT_COUNTRY_ID   NUMBER          := 4;
   QUOTA_CAT_TL$CATEGORY_DESC       NUMBER          := 5;
   
   COUNTRY_TARIFF_TREATMENT_sheet   VARCHAR2(255)   := 'COUNTRY_TARIFF_TREATMENT';
   COUNTRY_TRF_TT$Action            NUMBER          :=1;
   COUNTRY_TRF_TT$EFFECTIVE_TO      NUMBER          :=5;
   COUNTRY_TRF_TT$EFFECTIVE_FROM    NUMBER          :=4;
   COUNTRY_TRF_TT$TARIFF_TT         NUMBER          :=3;
   COUNTRY_TRF_TT$COUNTRY_ID        NUMBER          :=2;
   
   OGA_sheet                        VARCHAR2(255)   :='OGA';
   OGA$Action                       NUMBER          :=1;
   OGA$OGA_CODE                     NUMBER          :=2;
   OGA$OGA_DESC                     NUMBER          :=3;
   OGA$REQ_FORM                     NUMBER          :=4;

   OGA_TL_sheet                     VARCHAR2(255)   :='OGA_TL';
   OGA_TL$Action                    NUMBER          :=1;
   OGA_TL$LANG                      NUMBER          :=2;
   OGA_TL$OGA_CODE                  NUMBER          :=3;
   OGA_TL$OGA_DESC                  NUMBER          :=4;

   TYPE HTS_IMP_CNT_SETUP_REC_TAB IS TABLE OF HTS_IMPORT_COUNTRY_SETUP%ROWTYPE;
   TYPE TARIFF_TREATMENT_REC_TAB IS TABLE OF TARIFF_TREATMENT%ROWTYPE;
   TYPE TARIFF_TREATMENT_TL_REC_TAB IS TABLE OF TARIFF_TREATMENT_TL%ROWTYPE;
   TYPE OGA_REC_TAB IS TABLE OF OGA%ROWTYPE;

   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255) := 'ACTION';
---------------------------------------------------------------------------------------------   
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------  
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_count     OUT      NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------  
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     OUT      NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------  
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
---------------------------------------------------------------------------------------------
END CORESVC_HTS_SETUP;
/
