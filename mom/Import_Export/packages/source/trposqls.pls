CREATE OR REPLACE PACKAGE TRANSPORTATION_PO_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------------
--Function Name:  GET_ITEM_QTY_AND_INVOICE_AMT
--Purpose      :  Retrieves Item Qty and Invoice Amt.
----------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_QTY_AND_INVOICE_AMT(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_item_qty          IN OUT TRANSPORTATION.ITEM_QTY%TYPE,
                                      O_invoice_amt       IN OUT TRANSPORTATION.INVOICE_AMT%TYPE,
                                      I_transportation_id IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE,
                                      I_order_no          IN     TRANSPORTATION.ORDER_NO%TYPE,
                                      I_item              IN     TRANSPORTATION.ITEM%TYPE,
                                      I_mode              IN     NUMBER,
                                      I_item_qty          IN     TRANSPORTATION.ITEM_QTY%TYPE DEFAULT 0)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   CREATE_TRANSPORTATION
--Purpose      :   Used to populate the TRANSPORTATION table with all records of the associated PO for
--             :   corresponding PO/VVE information entered into the PO-Level transportation from.
-----------------------------------------------------------------------------------------------------
FUNCTION CREATE_TRANSPORTATION(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no               IN     TRANSPORTATION.ORDER_NO%TYPE,
                               I_vessel_id              IN     TRANSPORTATION.VESSEL_ID%TYPE,
                               I_voyage_flt_id          IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                               I_estimated_depart_date  IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                               I_item_default_level     IN     CODE_DETAIL.CODE%TYPE,
                               I_create_child_diffs     IN     VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function Name:  TRANPO_LOCK
--Purpose      :  Used by tranpo.fmb to lock selected records.
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_LOCK(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_transportation_id IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function Name:  TRANPO_INSERT
--Purpose      :  Used by tranpo.fmb insert new records.
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_INSERT(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_tra            IN     TRANSPORTATION%ROWTYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function Name:  TRANPO_ITEM_UPDATE
--Purpose      :  Used by tranpo.fmb to perform the "apply" action on the Item Update block.
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_ITEM_UPDATE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_tra            IN     TRANSPORTATION%ROWTYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function Name:  TRANPO_MASS_UPDATE
--Purpose      :  Used by tranpo.fmb to perform the "apply" action on the Mass Update block.
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_MASS_UPDATE(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cb_x_status                 IN     VARCHAR2,
                            I_cb_x_container_id           IN     VARCHAR2,
                            I_cb_x_bl_awb_id              IN     VARCHAR2,
                            I_cb_x_invoice_id             IN     VARCHAR2,
                            I_cb_x_invoice_date           IN     VARCHAR2,
                            I_cb_x_actual_depart_date     IN     VARCHAR2,
                            I_cb_x_estimated_arrival_date IN     VARCHAR2,
                            I_cb_x_actual_arrival_date    IN     VARCHAR2,
                            I_cb_x_delivery_date          IN     VARCHAR2,
                            I_cb_x_vessel_scac_code       IN     VARCHAR2,
                            I_cb_x_tran_mode_id           IN     VARCHAR2,
                            I_cb_x_freight_type           IN     VARCHAR2,
                            I_cb_x_trans_partner_type     IN     VARCHAR2,
                            I_cb_x_trans_partner_id       IN     VARCHAR2,
                            I_cb_x_consolidation_cntry_id IN     VARCHAR2,
                            I_cb_x_origin_country_id      IN     VARCHAR2,
                            I_cb_x_export_country_id      IN     VARCHAR2,
                            I_cb_x_lading_port            IN     VARCHAR2,
                            I_cb_x_discharge_port         IN     VARCHAR2,
                            I_cb_x_fcr_id                 IN     VARCHAR2,
                            I_cb_x_fcr_date               IN     VARCHAR2,
                            I_cb_x_shipment_no            IN     VARCHAR2,
                            I_cb_x_receipt_id             IN     VARCHAR2,
                            I_cb_x_seal_id                IN     VARCHAR2,
                            I_cb_x_container_scac_code    IN     VARCHAR2,
                            I_cb_x_freight_size           IN     VARCHAR2,
                            I_cb_x_packing_method         IN     VARCHAR2,
                            I_cb_x_service_contract_no    IN     VARCHAR2,
                            I_cb_x_in_transit_no          IN     VARCHAR2,
                            I_cb_x_in_transit_date        IN     VARCHAR2,
                            I_cb_x_lot_no                 IN     VARCHAR2,
                            I_cb_x_rush_ind               IN     VARCHAR2,
                            I_cb_x_candidate_ind          IN     VARCHAR2,
                            I_cb_x_comments               IN     VARCHAR2,
                            I_tra                         IN     TRANSPORTATION%ROWTYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function Name:  TRANPO_SELECTION
--Purpose      :  used by tranpo.fmb to update the "selected records" indicator.
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_SELECTION(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_transportation_id     IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE,
                          I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                          I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                          I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                          I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                          I_selected_ind          IN     TRANSPORTATION.SELECTED_IND%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function Name:  TRANPO_DELETE
--Purpose      :  Used by tranpo.fmb to delete selected records.
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_DELETE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_transportation_id IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function Name:  TRANS_SKU_EXISTS
--Purpose      :  Used by tranpo.fmb to determine whether or not child records exist.
----------------------------------------------------------------------------------------------------
FUNCTION TRANS_SKU_EXISTS(O_error_message     IN OUT VARCHAR2,
                          O_exists            IN OUT BOOLEAN,
                          I_transportation_id IN     TRANS_SKU.TRANSPORTATION_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function Name:  VALIDATE_TRANPO_ITEM
--Purpose      :  Used by tranpo.fmb to validate new items being added to the Transportation.
----------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TRANPO_ITEM(O_error_message         IN OUT VARCHAR2,
                              O_valid                 IN OUT BOOLEAN,
                              O_v_item_master         IN OUT V_ITEM_MASTER%ROWTYPE,
                              I_item                  IN     TRANSPORTATION.ITEM%TYPE,
                              I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                              I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                              I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                              I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                              I_item_default_level    IN     VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
--Function Name:TRANPO_VALIDATE
--Purpose      :Validates if a transportation record is associated with an obligation or customs entry.
------------------------------------------------------------------------------------------------------
FUNCTION TRANPO_VALIDATE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN     TRANSPORTATION.ORDER_NO%TYPE)

RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function Name:  VALIDATE_APPLY_REC
--Purpose      :  Used by tranpo.fmb to validate existing items being added to the Transportation.
----------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_APPLY_REC(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid                   IN OUT   BOOLEAN,
                            I_item                    IN       TRANSPORTATION.ITEM%TYPE,
                            I_container_id            IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                            I_bl_awb_id               IN       TRANSPORTATION.BL_AWB_ID%TYPE,
                            I_invoice_id              IN       TRANSPORTATION.INVOICE_ID%TYPE,
                            I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE,
                            I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                            I_voyage_flt_id           IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                            I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                            I_item_default_level      IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function Name:  VALIDATE_VVE_SHIP
--Purpose      :  Used by tranpo.fmb to validate a shipment being added or removed.
----------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_VVE_SHIP(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_status                  IN OUT   VARCHAR2,
                           I_shipment                IN       SHIPMENT.SHIPMENT%TYPE,
                           I_asn                     IN       SHIPMENT.ASN%TYPE,
                           I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE,
                           I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                           I_voyage_flt_id           IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                           I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                           I_add_remove_ind          IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function Name:  TRANPO_MASS_UPDATE
--Purpose      :  This function will be called from ADF screen Transportation 
--                to perform the "apply" action on the Mass Update.
----------------------------------------------------------------------------------------------------
FUNCTION TRANPO_MASS_UPDATE(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cb_x_status                   IN       VARCHAR2,
                            I_cb_x_actual_depart_date       IN       VARCHAR2,
                            I_cb_x_estimated_arrival_date   IN       VARCHAR2,
                            I_cb_x_actual_arrival_date      IN       VARCHAR2,
                            I_cb_x_delivery_date            IN       VARCHAR2,
                            I_cb_x_shipment_no              IN       VARCHAR2,
                            I_cb_x_receipt_id               IN       VARCHAR2,
                            I_cb_x_fcr_id                   IN       VARCHAR2,
                            I_cb_x_fcr_date                 IN       VARCHAR2,
                            I_cb_x_in_transit_no            IN       VARCHAR2,
                            I_cb_x_in_transit_date          IN       VARCHAR2,
                            I_cb_x_service_contract_no      IN       VARCHAR2,
                            I_cb_x_lot_no                   IN       VARCHAR2,
                            I_cb_x_seal_id                  IN       VARCHAR2,
                            I_cb_x_packing_method           IN       VARCHAR2,
                            I_cb_x_trans_partner_id         IN       VARCHAR2,
                            I_cb_x_lading_port              IN       VARCHAR2,
                            I_cb_x_discharge_port           IN       VARCHAR2,
                            I_cb_x_container_scac_code      IN       VARCHAR2,
                            I_cb_x_vessel_scac_code         IN       VARCHAR2,
                            I_cb_x_freight_type             IN       VARCHAR2,
                            I_cb_x_freight_size             IN       VARCHAR2,
                            I_cb_x_consolidation_cntry_id   IN       VARCHAR2,
                            I_cb_x_origin_country_id        IN       VARCHAR2,
                            I_cb_x_export_country_id        IN       VARCHAR2,
                            I_cb_x_tran_mode_id             IN       VARCHAR2,
                            I_cb_x_rush_ind                 IN       VARCHAR2,
                            I_cb_x_candidate_ind            IN       VARCHAR2,
                            I_cb_x_comments                 IN       VARCHAR2,
                            I_initial_container_id          IN       VARCHAR2,
                            I_initial_bl_awb_id             IN       VARCHAR2,
                            I_initial_invoice_id            IN       VARCHAR2,
                            I_tra                           IN       WRP_TRANSPORTATION_REC)
RETURN INTEGER;
--------------------------------------------------------------------------------------------------------------
END TRANSPORTATION_PO_SQL;
/
