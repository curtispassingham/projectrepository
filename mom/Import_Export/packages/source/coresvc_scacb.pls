CREATE OR REPLACE PACKAGE BODY CORESVC_SCAC AS
   cursor C_SVC_SCAC(I_process_id NUMBER,
                     I_chunk_id NUMBER) is
      select pk_scac.rowid        as pk_scac_rid,
             st.rowid             as st_rid,
             st.scac_code_desc,
             UPPER(st.scac_code) as scac_code,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)     as action,
             st.process$status
        from svc_scac st,
             scac pk_scac
       where st.process_id   = I_process_id
         and st.chunk_id     = I_chunk_id
         and UPPER(st.scac_code)    = pk_scac.scac_code (+);
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
   
   Type scac_tl_TAB IS TABLE OF scac_tl%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   
   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.EXISTS(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(CASE
                                                                        WHEN I_sqlcode IS NULL THEN
                                                                             I_sqlerrm
                                                                        ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                        END);
   
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LasT_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LasT_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets     s9t_pkg.names_map_typ;
   scac_cols    s9t_pkg.names_map_typ;
   scac_tl_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets               :=s9t_pkg.get_sheet_names(I_file_id);
   scac_cols              :=s9t_pkg.get_col_names(I_file_id,SCAC_SHEET);
   SCAC$ACTION            := scac_cols('ACTION');
   SCAC$SCAC_CODE         := scac_cols('SCAC_CODE');
   SCAC$SCAC_CODE_DESC    := scac_cols('SCAC_CODE_DESC');
   
   scac_tl_cols              :=s9t_pkg.get_col_names(I_file_id,scac_tl_SHEET);
   scac_tl$ACTION            := scac_tl_cols('ACTION');
   scac_tl$LANG              := scac_tl_cols('LANG');
   scac_tl$SCAC_CODE         := scac_tl_cols('SCAC_CODE');
   scac_tl$SCAC_CODE_DESC    := scac_tl_cols('SCAC_CODE_DESC');

END POPULATE_NAMES;
-------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SCAC( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = SCAC_SHEET )
   select s9t_row(s9t_cells(CORESVC_SCAC.action_mod ,
                            scac_code,
                            scac_code_desc))
     from scac ;
END POPULATE_SCAC;
-------------------------------------------------------------------------------------------
PROCEDURE POPULATE_scac_tl( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = scac_tl_SHEET )
   select s9t_row(s9t_cells(CORESVC_SCAC.action_mod ,
                            lang,
                            scac_code,
                            scac_code_desc))
     from scac_tl ;
END POPULATE_scac_tl;
--------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file      s9t_file;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;

   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(SCAC_SHEET);
   L_file.sheets(l_file.get_sheet_index(SCAC_SHEET)).column_headers := s9t_cells( 'ACTION',
                                                                                  'SCAC_CODE',
                                                                                  'SCAC_CODE_DESC');
   s9t_pkg.SAVE_OBJ(L_file);

   L_file.add_sheet(scac_tl_SHEET);
   L_file.sheets(l_file.get_sheet_index(scac_tl_SHEET)).column_headers := s9t_cells( 'ACTION',
                                                                                     'LANG',
                                                                                     'SCAC_CODE',
                                                                                     'SCAC_CODE_DESC');
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
-----------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_SCAC.CREATE_S9T';
   L_file    s9t_file;
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key)=FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_SCAC(O_file_id);
      POPULATE_SCAC_TL(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SCAC( I_file_id     IN   S9T_FOLDER.FILE_ID%TYPE,
                            I_process_id  IN   SVC_SCAC.PROCESS_ID%TYPE) IS
   TYPE svc_scac_col_typ IS TABLE OF SVC_SCAC%ROWTYPE;
   L_temp_rec    SVC_SCAC%ROWTYPE;
   svc_scac_col  svc_scac_col_typ         := NEW svc_scac_col_typ();
   L_process_id  SVC_SCAC.PROCESS_ID%TYPE;
   L_error       BOOLEAN                  := FALSE;
   L_default_rec SVC_SCAC%ROWTYPE;
   cursor C_MANDATORY_IND is
      select scac_code_desc_mi,
             scac_code_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'SCAC') 
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('SCAC_CODE_DESC' as scac_code_desc,
                                                                'SCAC_CODE'      as scac_code,
                                                                 NULL            as dummy));
   L_mi_rec   C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_SCAC';
   L_pk_columns    VARCHAR2(255)  := 'SCAC Code';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN 
  -- Get default values.
   FOR rec IN (select scac_code_desc_dv,
                      scac_code_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                   = template_key
                          and wksht_key                      = 'SCAC')
                       PIVOT (MAX(default_value) as dv
                              FOR(column_key) IN ('SCAC_CODE_DESC' as scac_code_desc,
                                                  'SCAC_CODE'      as scac_code,
                                                   NULL            as dummy)))
   LOOP
      BEGIN
         L_default_rec.scac_code_desc := rec.scac_code_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'SCAC ' ,
                             NULL,
                            'SCAC_CODE_DESC ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.scac_code := rec.scac_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'SCAC ' ,
                             NULL,
                            'SCAC_CODE ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
   (select r.get_cell(SCAC$ACTION)                 as action,
           r.get_cell(SCAC$SCAC_CODE_DESC)         as scac_code_desc,
           UPPER(r.get_cell(SCAC$SCAC_CODE))       as scac_code,
           r.get_row_seq()                         as row_seq
      from s9t_folder sf,
           TABLE(sf.s9t_file_obj.sheets) ss,
           TABLE(ss.s9t_rows) r
      where sf.file_id  = I_file_id
        and ss.sheet_name = sheet_name_trans(SCAC_SHEET))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SCAC_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.scac_code_desc := rec.scac_code_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SCAC_SHEET,
                            rec.row_seq,
                            'SCAC_CODE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.scac_code := rec.scac_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SCAC_SHEET,
                            rec.row_seq,
                            'SCAC_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_SCAC.action_new then
         L_temp_rec.scac_code_desc := NVL( L_temp_rec.scac_code_desc,L_default_rec.scac_code_desc);
         L_temp_rec.scac_code      := NVL( L_temp_rec.scac_code,L_default_rec.scac_code);
      end if;
      if NOT(L_temp_rec.scac_code is NOT NULL 
             and 1 = 1)then
         WRITE_S9T_ERROR( I_file_id,
                          SCAC_SHEET,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_scac_col.extend();
         svc_scac_col(svc_scac_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_scac_col.COUNT SAVE EXCEPTIONS
      merge into SVC_SCAC st
      using(select(case
                   when L_mi_rec.scac_code_desc_mi    = 'N'
                    and svc_scac_col(i).action = CORESVC_SCAC.action_mod
                    and s1.scac_code_desc is NULL
                   then mt.scac_code_desc
                   else s1.scac_code_desc
                   end) as scac_code_desc,
                  (case
                   when L_mi_rec.scac_code_mi    = 'N'
                    and svc_scac_col(i).action = CORESVC_SCAC.action_mod
                    and s1.scac_code is NULL
                   then mt.scac_code
                   else s1.scac_code
                   end) as scac_code,
                  null as dummy
              from (select svc_scac_col(i).scac_code_desc as scac_code_desc,
                           svc_scac_col(i).scac_code      as scac_code,
                           null                           as dummy
                      from dual ) s1,
                             SCAC mt
             where mt.scac_code (+)     = s1.scac_code   
                   and 1 = 1 )sq
                on (st.scac_code      = sq.scac_code 
                    and svc_scac_col(i).ACTION IN (CORESVC_SCAC.action_mod,CORESVC_SCAC.action_del))
      when matched then
      update
         set process_id        = svc_scac_col(i).process_id ,
             chunk_id          = svc_scac_col(i).chunk_id ,
             row_seq           = svc_scac_col(i).row_seq ,
             action            = svc_scac_col(i).action ,
             process$status    = svc_scac_col(i).process$status ,
             scac_code_desc    = sq.scac_code_desc ,
             create_id         = svc_scac_col(i).create_id ,
             create_datetime   = svc_scac_col(i).create_datetime ,
             last_upd_id       = svc_scac_col(i).last_upd_id ,
             last_upd_datetime = svc_scac_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             scac_code_desc ,
             scac_code ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_scac_col(i).process_id ,
             svc_scac_col(i).chunk_id ,
             svc_scac_col(i).row_seq ,
             svc_scac_col(i).action ,
             svc_scac_col(i).process$status ,
             sq.scac_code_desc ,
             sq.scac_code ,
             svc_scac_col(i).create_id ,
             svc_scac_col(i).create_datetime ,
             svc_scac_col(i).last_upd_id ,
             svc_scac_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
               if L_error_code=1 then
                  L_error_code:=NULL;
                  L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
               end if;
            WRITE_S9T_ERROR( I_file_id,
                             SCAC_SHEET,
                             svc_scac_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_SCAC;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SCAC_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                              I_process_id   IN   svc_scac_tl.PROCESS_ID%TYPE) IS
                             
   TYPE svc_scac_tl_col_TYP IS TABLE OF svc_scac_tl%ROWTYPE;
   L_temp_rec         svc_scac_tl%ROWTYPE;
   svc_scac_tl_col    svc_scac_tl_col_typ := NEW svc_scac_tl_col_typ();
   L_process_id       svc_scac_tl.PROCESS_ID%TYPE;
   L_error            BOOLEAN := FALSE;
   L_default_rec      svc_scac_tl%ROWTYPE;
   
   cursor C_MANDATORY_IND is
      select scac_code_desc_mi,
             scac_code_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'scac_tl') 
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('SCAC_CODE_DESC' as scac_code_desc,
                                       'SCAC_CODE'      as scac_code,
                                       'LANG'           as lang));
                                       
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_SCAC_TL';
   L_pk_columns    VARCHAR2(255)  := 'SCAC Code, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
      
BEGIN
  -- Get default values.
   FOR rec IN (select scac_code_desc_dv,
                      scac_code_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'scac_tl')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('SCAC_CODE_DESC' as scac_code_desc,
                                                'SCAC_CODE'      as scac_code,
                                                'LANG'           as lang)))
   LOOP
      BEGIN
         L_default_rec.scac_code_desc := rec.scac_code_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            scac_tl_SHEET ,
                            NULL,
                           'SCAC_CODE_DESC' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.scac_code := rec.scac_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           scac_tl_SHEET ,
                            NULL,
                           'SCAC_CODE' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            scac_tl_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(scac_tl$action))         as action,
                      r.get_cell(scac_tl$scac_code_desc)        as scac_code_desc,
                      UPPER(r.get_cell(scac_tl$scac_code))      as scac_code,
                      r.get_cell(scac_tl$lang)                  as lang,
                      r.get_row_seq()                           as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(scac_tl_SHEET))
   LOOP
      L_temp_rec := NULL;     
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            scac_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.scac_code_desc := rec.scac_code_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            scac_tl_SHEET,
                            rec.row_seq,
                            'SCAC_CODE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.scac_code := rec.scac_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            scac_tl_SHEET,
                            rec.row_seq,
                            'SCAC_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            scac_tl_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_scac.action_new then
         L_temp_rec.scac_code_desc := NVL( L_temp_rec.scac_code_desc,L_default_rec.scac_code_desc);
         L_temp_rec.scac_code      := NVL( L_temp_rec.scac_code,L_default_rec.scac_code);
         L_temp_rec.lang           := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.scac_code is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         scac_tl_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_scac_tl_col.extend();
         svc_scac_tl_col(svc_scac_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_scac_tl_col.COUNT SAVE EXCEPTIONS
      merge into svc_scac_tl st
      using(select
                  (case
                   when l_mi_rec.scac_code_desc_mi = 'N'
                    and svc_scac_tl_col(i).action = CORESVC_scac.action_mod
                    and s1.scac_code_desc IS NULL then
                        mt.scac_code_desc
                   else s1.scac_code_desc
                   end) as scac_code_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_scac_tl_col(i).action = CORESVC_scac.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.scac_code_mi = 'N'
                    and svc_scac_tl_col(i).action = CORESVC_scac.action_mod
                    and s1.scac_code IS NULL then
                        mt.scac_code
                   else s1.scac_code
                   end) as scac_code
              from (select svc_scac_tl_col(i).scac_code_desc as scac_code_desc,
                           svc_scac_tl_col(i).scac_code        as scac_code,
                           svc_scac_tl_col(i).lang              as lang
                      from dual) s1,
                   scac_tl mt
             where mt.scac_code (+) = s1.scac_code 
               and mt.lang (+)       = s1.lang) sq
                on (st.scac_code = sq.scac_code and
                    st.lang = sq.lang and
                    svc_scac_tl_col(i).ACTION IN (CORESVC_scac.action_mod,CORESVC_scac.action_del))
      when matched then
      update
         set process_id        = svc_scac_tl_col(i).process_id ,
             chunk_id          = svc_scac_tl_col(i).chunk_id ,
             row_seq           = svc_scac_tl_col(i).row_seq ,
             action            = svc_scac_tl_col(i).action ,
             process$status    = svc_scac_tl_col(i).process$status ,
             scac_code_desc = sq.scac_code_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             scac_code_desc ,
             scac_code ,
             lang)
      values(svc_scac_tl_col(i).process_id ,
             svc_scac_tl_col(i).chunk_id ,
             svc_scac_tl_col(i).row_seq ,
             svc_scac_tl_col(i).action ,
             svc_scac_tl_col(i).process$status ,
             sq.scac_code_desc ,
             sq.scac_code ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            
            WRITE_S9T_ERROR( I_file_id,
                            scac_tl_SHEET,
                            svc_scac_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_SCAC_TL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count     IN OUT   NUMBER,
                      I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)  := 'CORESVC_SCAC.PROCESS_S9T';
   L_file           s9t_file;
   L_sheets         S9T_PKG.NAMES_MAP_TYP;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	    EXCEPTION;
   PRAGMA	    EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE)=FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_SCAC(I_file_id,I_process_id);
      PROCESS_S9T_SCAC_TL(I_file_id,I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_SCAC_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_error           IN OUT   BOOLEAN,
                          I_rec             IN       C_SVC_SCAC%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_SCAC.PROCESS_SCAC_VAL';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SCAC';
   L_exists  BOOLEAN;
BEGIN
   if I_rec.action = action_del then
      if TRANSPORTATION_SQL.CHECK_DELETE_SCAC_CODE(O_error_message,
                                                   L_exists,
                                                   I_rec.scac_code) = FALSE or
            L_exists = TRUE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'SCAC_CODE',
                        'CANNOT_DELETE_SCAC');
            O_error := TRUE; 
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SCAC_VAL;
---------------------------------------------------------------------------------------------------------
FUNCTION EXEC_SCAC_INS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_scac_temp_rec     IN       SCAC%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_SCAC.EXEC_SCAC_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SCAC';
BEGIN
   insert into scac
        values I_scac_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SCAC_INS;
-----------------------------------------------------------------------------------------
FUNCTION EXEC_SCAC_UPD( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_scac_temp_rec     IN       SCAC%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_SCAC.EXEC_SCAC_UPD';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SCAC';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);
   cursor C_SCAC_LOCK is
      select 'X'
        from scac
       where scac_code = I_scac_temp_rec.scac_code
       for update nowait;
BEGIN
   open C_SCAC_LOCK;
   close C_SCAC_LOCK;   

   update scac
      set row = I_scac_temp_rec
    where 1 = 1
      and scac_code = I_scac_temp_rec.scac_code;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_scac_temp_rec.scac_code,  
                                             NULL);
     
      return FALSE;
   when OTHERS then
      if C_SCAC_LOCK%ISOPEN then
         close C_SCAC_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SCAC_UPD;
-----------------------------------------------------------------------------------------------------
FUNCTION EXEC_SCAC_DEL( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_scac_temp_rec     IN       SCAC%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_SCAC.EXEC_SCAC_DEL';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SCAC';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   cursor C_scac_tl_LOCK is
      select 'X'
        from scac_tl
       where scac_code = I_scac_temp_rec.scac_code
       for update nowait;

   cursor C_SCAC_LOCK is
      select 'X'
        from scac
       where scac_code = I_scac_temp_rec.scac_code
       for update nowait;
BEGIN
   open C_scac_tl_LOCK;
   close C_scac_tl_LOCK;
   
   delete
     from scac_tl
    where scac_code = I_scac_temp_rec.scac_code;  
   
   open C_SCAC_LOCK;
   close C_SCAC_LOCK;

   delete
     from scac
    where scac_code = I_scac_temp_rec.scac_code;    

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_scac_temp_rec.scac_code,  
                                             NULL);
     
      return FALSE;
   when OTHERS then
      if C_SCAC_LOCK%ISOPEN then
         close C_SCAC_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SCAC_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_SCAC_TL_INS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_scac_tl_ins_tab      IN       scac_tl_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_SCAC.EXEC_SCAC_TL_INS';

BEGIN
   if I_scac_tl_ins_tab is NOT NULL and I_scac_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_scac_tl_ins_tab.COUNT()
         insert into scac_tl
              values I_scac_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_SCAC_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_SCAC_TL_UPD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_scac_tl_upd_tab     IN       SCAC_TL_TAB,
                          I_scac_tl_upd_rst     IN       ROW_SEQ_TAB,
                          I_process_id          IN       SVC_SCAC_TL.PROCESS_ID%TYPE,
                          I_chunk_id            IN       SVC_SCAC_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SCAC.EXEC_SCAC_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'scac_tl';   
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_SCAC_TL_UPD(I_scac   scac_tl.SCAC_CODE%TYPE,
                             I_lang   scac_tl.LANG%TYPE) is
      select 'x'
        from scac_tl
       where scac_code = I_scac
         and lang = I_lang
         for update nowait;

BEGIN
   if I_scac_tl_upd_tab is NOT NULL and I_scac_tl_upd_tab.count > 0 then
      for i in I_scac_tl_upd_tab.FIRST..I_scac_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_scac_tl_upd_tab(i).lang);
            L_key_val2 := 'SCAC Code: '||to_char(I_scac_tl_upd_tab(i).scac_code);
            open C_LOCK_SCAC_TL_UPD(I_scac_tl_upd_tab(i).scac_code,
                                    I_scac_tl_upd_tab(i).lang);
            close C_LOCK_SCAC_TL_UPD;
           
            update scac_tl
               set scac_code_desc = I_scac_tl_upd_tab(i).scac_code_desc,
                   last_update_id = I_scac_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_scac_tl_upd_tab(i).last_update_datetime
             where lang = I_scac_tl_upd_tab(i).lang
               and scac_code = I_scac_tl_upd_tab(i).scac_code;

         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_SCAC_TL',
                           I_scac_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_SCAC_TL_UPD%ISOPEN then
         close C_LOCK_SCAC_TL_UPD;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program, 
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_SCAC_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_SCAC_TL_DEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_scac_tl_del_tab   IN       SCAC_TL_TAB,
                          I_scac_tl_del_rst   IN       ROW_SEQ_TAB,
                          I_process_id        IN       SVC_SCAC_TL.PROCESS_ID%TYPE,
                          I_chunk_id          IN       SVC_SCAC_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SCAC.EXEC_SCAC_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'scac_tl';   
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_SCAC_TL_DEL(I_scac   scac_tl.SCAC_CODE%TYPE,
                             I_lang   scac_tl.LANG%TYPE) is
      select 'x'
        from scac_tl
       where scac_code = I_scac
         and lang = I_lang
         for update nowait;

BEGIN
   if I_scac_tl_del_tab is NOT NULL and I_scac_tl_del_tab.count > 0 then
      for i in I_scac_tl_del_tab.FIRST..I_scac_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_scac_tl_del_tab(i).lang);
            L_key_val2 := 'SCAC Code: '||to_char(I_scac_tl_del_tab(i).scac_code);
            open C_LOCK_SCAC_TL_DEL(I_scac_tl_del_tab(i).scac_code,
                                    I_scac_tl_del_tab(i).lang);
            close C_LOCK_SCAC_TL_DEL;
          
            delete from scac_tl
             where lang = I_scac_tl_del_tab(i).lang
               and scac_code = I_scac_tl_del_tab(i).scac_code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_SCAC_TL',
                           I_scac_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_SCAC_TL_DEL%ISOPEN then
         close C_LOCK_SCAC_TL_DEL;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program, 
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_SCAC_TL_DEL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_SCAC( O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_process_id        IN     SVC_SCAC.PROCESS_ID%TYPE,
                       I_chunk_id          IN     SVC_SCAC.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      :='CORESVC_SCAC.PROCESS_SCAC';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_SCAC';
   L_process_error BOOLEAN                           := FALSE;   
   L_error         BOOLEAN;
   L_scac_temp_rec SCAC%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_SCAC(I_process_id,
                         I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      if rec.action = action_new
         and rec.PK_SCAC_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SCAC_CODE',
                     'DUP_SCAC');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_SCAC_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SCAC_CODE',
                     'INV_SCAC_CODE');
         L_error :=TRUE;
      end if;
      if rec.scac_code_desc is NULL 
        and rec.action IN (action_new,action_mod)then
        WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SCAC_CODE_DESC',
                     'ENTER_SCAC_CODE_DESC');
         L_error :=TRUE;
      end if;
      if PROCESS_SCAC_VAL(O_error_message,
                          L_error,
                          rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;
      if NOT L_error then
         L_scac_temp_rec.scac_code_desc         := rec.scac_code_desc;
         L_scac_temp_rec.scac_code              := rec.scac_code;
         if rec.action = action_new then
            if EXEC_SCAC_INS( O_error_message,
                              L_scac_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_SCAC_UPD( O_error_message,
                              L_scac_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_SCAC_DEL( O_error_message,
                              L_scac_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SCAC;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SCAC_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       svc_scac_tl.PROCESS_ID%TYPE,
                         I_chunk_id        IN       svc_scac_tl.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_SCAC.PROCESS_SCAC_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SCAC_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SCAC';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SCAC_TL';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_scac_tl_temp_rec     SCAC_TL%ROWTYPE;
   L_scac_tl_upd_rst      ROW_SEQ_TAB;
   L_scac_tl_del_rst      ROW_SEQ_TAB;
   
   cursor C_SVC_SCAC_TL(I_process_id NUMBER,
                        I_chunk_id NUMBER) is
      select pk_scac_tl.rowid  as pk_scac_tl_rid,
             fk_scac.rowid     as fk_scac_rid,
             fk_lang.rowid     as fk_lang_rid,
             st.lang,
             UPPER(st.scac_code)     as scac_code,
             st.scac_code_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_scac_tl  st,
             scac         fk_scac,
             scac_tl      pk_scac_tl,
             lang         fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.scac_code   =  fk_scac.scac_code (+)
         and st.lang        =  pk_scac_tl.lang (+)
         and upper(st.scac_code) =  upper(pk_scac_tl.scac_code (+))
         and st.lang        =  fk_lang.lang (+);
         
   TYPE svc_scac_tl is TABLE OF C_SVC_SCAC_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_scac_tl_tab         svc_scac_tl;
   
   L_scac_tl_ins_tab         scac_tl_tab         := NEW scac_tl_tab();
   L_scac_tl_upd_tab         scac_tl_tab         := NEW scac_tl_tab();
   L_scac_tl_del_tab         scac_tl_tab         := NEW scac_tl_tab();
   
BEGIN
   if C_SVC_SCAC_TL%ISOPEN then
      close C_SVC_SCAC_TL;
   end if;
   
   open C_SVC_SCAC_TL(I_process_id,
                       I_chunk_id);
   LOOP 
      fetch C_SVC_SCAC_TL bulk collect into L_svc_scac_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_scac_tl_tab.COUNT > 0 then
         FOR i in L_svc_scac_tl_tab.FIRST..L_svc_scac_tl_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_scac_tl_tab(i).action is NULL
               or L_svc_scac_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_scac_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_scac_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_scac_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_scac_tl_tab(i).action = action_new 
               and L_svc_scac_tl_tab(i).pk_scac_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_scac_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_scac_tl_tab(i).action IN (action_mod, action_del) 
               and L_svc_scac_tl_tab(i).lang is NOT NULL
               and L_svc_scac_tl_tab(i).scac_code is NOT NULL 
               and L_svc_scac_tl_tab(i).pk_scac_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_scac_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_scac_tl_tab(i).action = action_new 
               and L_svc_scac_tl_tab(i).scac_code is NOT NULL
               and L_svc_scac_tl_tab(i).fk_scac_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_scac_tl_tab(i).row_seq,
                            'SCAC_CODE',
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_scac_tl_tab(i).action = action_new 
               and L_svc_scac_tl_tab(i).lang is NOT NULL
               and L_svc_scac_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_scac_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_scac_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_scac_tl_tab(i).scac_code_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_scac_tl_tab(i).row_seq,
                              'SCAC_CODE_DESC',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_scac_tl_tab(i).scac_code is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_scac_tl_tab(i).row_seq,
                           'SCAC_CODE',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_scac_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_scac_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_scac_tl_temp_rec.lang := L_svc_scac_tl_tab(i).lang;
               L_scac_tl_temp_rec.scac_code := L_svc_scac_tl_tab(i).scac_code;
               L_scac_tl_temp_rec.scac_code_desc := L_svc_scac_tl_tab(i).scac_code_desc;
               L_scac_tl_temp_rec.create_datetime := SYSDATE;
               L_scac_tl_temp_rec.create_id := GET_USER;
               L_scac_tl_temp_rec.last_update_datetime := SYSDATE;
               L_scac_tl_temp_rec.last_update_id := GET_USER;
            
               if L_svc_scac_tl_tab(i).action = action_new then
                  L_scac_tl_ins_tab.extend;
                  L_scac_tl_ins_tab(L_scac_tl_ins_tab.count()) := L_scac_tl_temp_rec;
               end if; 

               if L_svc_scac_tl_tab(i).action = action_mod then
                  L_scac_tl_upd_tab.extend;
                  L_scac_tl_upd_tab(L_scac_tl_upd_tab.count()) := L_scac_tl_temp_rec;
                  L_scac_tl_upd_rst(L_scac_tl_upd_tab.count()) := L_svc_scac_tl_tab(i).row_seq;
               end if; 

               if L_svc_scac_tl_tab(i).action = action_del then
                  L_scac_tl_del_tab.extend;
                  L_scac_tl_del_tab(L_scac_tl_del_tab.count()) := L_scac_tl_temp_rec;
                  L_scac_tl_del_rst(L_scac_tl_del_tab.count()) := L_svc_scac_tl_tab(i).row_seq;
               end if; 
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_SCAC_TL%NOTFOUND;
   END LOOP;
   close C_SVC_SCAC_TL;

   if EXEC_SCAC_TL_INS(O_error_message,
                        L_scac_tl_ins_tab) = FALSE then
      return FALSE;
   end if;
   
   if EXEC_SCAC_TL_UPD(O_error_message,
                       L_scac_tl_upd_tab,
                       L_scac_tl_upd_rst,
                       I_process_id,
                       I_chunk_id) = FALSE then                        
      return FALSE;
   end if;
   
   if EXEC_SCAC_TL_DEL(O_error_message,
                       L_scac_tl_del_tab,
                       L_scac_tl_del_rst,
                       I_process_id,
                       I_chunk_id) = FALSE then  
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SCAC_TL;
--------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete 
     from svc_scac_tl
    where process_id = I_process_id;

   delete 
     from svc_scac
    where process_id = I_process_id;
END;
--------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    :='CORESVC_SCAC.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x' 
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';
 
   cursor C_GET_WARN_COUNT is
      select 'x' 
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_SCAC(O_error_message,
                   I_process_id,
                   I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   if PROCESS_SCAC_TL(O_error_message,
                      I_process_id,
                      I_chunk_id)=FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER 
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then 
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_SCAC;
/