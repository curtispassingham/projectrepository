CREATE OR REPLACE PACKAGE BODY TRANSPORTATION_SQL AS
---------------------------------------------------------------------------------------------------------
--Function Name:   GET_NEXT_ID
--Purpose      :   Retrieves the next value for the Transportation ID for the TRANSPORTATION
--                 table.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_ID(O_error_message          IN OUT VARCHAR2,
                     O_transportation_id      IN OUT TRANSPORTATION.TRANSPORTATION_ID%TYPE)
   return BOOLEAN is

   L_first_time               VARCHAR2(3) := 'YES';
   L_wrap_seq_no              TRANSPORTATION.TRANSPORTATION_ID%TYPE;
   L_exists                   VARCHAR2(1) := NULL;

   cursor C_TRANS_EXISTS is
      select 'x'
        from transportation
       where transportation_id  = O_transportation_id;

   cursor C_SELECT_NEXTVAL is
      select transportation_sequence.NEXTVAL
        from dual;

BEGIN
   LOOP
      SQL_LIB.SET_MARK('OPEN','C_SELECT_NEXTVAL','DUAL',NULL);
      open C_SELECT_NEXTVAL;
      SQL_LIB.SET_MARK('FETCH','C_SELECT_NEXTVAL','DUAL',NULL);
      fetch C_SELECT_NEXTVAL into O_transportation_id;
      if L_first_time = 'YES' then
         L_wrap_seq_no   := O_transportation_id;
         L_first_time    := 'NO';
      elsif O_transportation_id = L_wrap_seq_no then
         O_error_message := SQL_LIB.CREATE_MSG('NO_TRANSPORT_NUM', NULL, NULL, NULL);
         SQL_LIB.SET_MARK('CLOSE','C_SELECT_NEXTVAL','DUAL',NULL);
         close C_SELECT_NEXTVAL;
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_SELECT_NEXTVAL','DUAL',NULL);
      close C_SELECT_NEXTVAL;

      SQL_LIB.SET_MARK('OPEN','C_TRANS_EXISTS','TRANSPORTATION',
                      'TRANSPORTATION ID: '||to_char(O_transportation_id));
      open C_TRANS_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_TRANS_EXISTS','TRANSPORTATION',
                       'TRANSPORTATION ID: '||to_char(O_transportation_id));
      fetch C_TRANS_EXISTS into L_exists;
      if C_TRANS_EXISTS%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_TRANS_EXISTS','TRANSPORTATION',
                          'TRANSPORTATION ID: '||to_char(O_transportation_id));
         close C_TRANS_EXISTS;
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_TRANS_EXISTS','TRANSPORTATION',
                       'TRANSPORTATION ID: '||to_char(O_transportation_id));
      close C_TRANS_EXISTS;
   END LOOP;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSPORTATION_SQL.GET_NEXT_ID',
                                            to_char(SQLCODE));
   return FALSE;
END GET_NEXT_ID;
-----------------------------------------------------------------------------------------------------------------
FUNCTION TRAN_EXISTS(O_error_message           IN OUT VARCHAR2,
                      O_exists                  IN OUT BOOLEAN,
                      I_transportation_id       IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE)
   return BOOLEAN is

   L_exists             VARCHAR2(1)       := NULL;

   cursor C_TRAN_EXISTS is
      select 'Y'
        from transportation
       where transportation_id = I_transportation_id;
BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_TRAN_EXISTS','TRANSPORTAION','TRANSPOTATION ID: '||to_char(I_transportation_id));
   open C_TRAN_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_TRAN_EXISTS','TRANSPORTAION','TRANSPOTATION ID: '||to_char(I_transportation_id));
   fetch C_TRAN_EXISTS into L_exists;
   if C_TRAN_EXISTS%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_TRAN_EXISTS','TRANSPORTAION','TRANSPOTATION ID: '||to_char(I_transportation_id));
   close C_TRAN_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'TRANSPORTATION_SQL.TRAN_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END TRAN_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_TOTALS(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_carton_qty              IN OUT   TRANSPORTATION.CARTON_QTY%TYPE,
                    IO_carton_qty_uom         IN OUT   TRANSPORTATION.CARTON_UOM%TYPE,
                    O_item_qty                IN OUT   TRANSPORTATION.ITEM_QTY%TYPE,
                    IO_item_qty_uom           IN OUT   TRANSPORTATION.ITEM_QTY_UOM%TYPE,
                    O_gross_wt                IN OUT   TRANSPORTATION.GROSS_WT%TYPE,
                    IO_gross_wt_uom           IN OUT   TRANSPORTATION.GROSS_WT_UOM%TYPE,
                    O_net_wt                  IN OUT   TRANSPORTATION.NET_WT%TYPE,
                    IO_net_wt_uom             IN OUT   TRANSPORTATION.NET_WT_UOM%TYPE,
                    O_cubic                   IN OUT   TRANSPORTATION.CUBIC%TYPE,
                    IO_cubic_uom              IN OUT   TRANSPORTATION.CUBIC_UOM%TYPE,
                    O_invoice_amt             IN OUT   TRANSPORTATION.INVOICE_AMT%TYPE,
                    I_total_level             IN       CODE_DETAIL.CODE%TYPE,
                    I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                    I_voyage_id               IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                    I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                    I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE,
                    I_item                    IN       TRANSPORTATION.ITEM%TYPE,
                    I_container_id            IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                    I_bl_awb_id               IN       TRANSPORTATION.BL_AWB_ID%TYPE,
                    I_invoice_id              IN       TRANSPORTATION.INVOICE_ID%TYPE,
                    I_currency_code           IN       TRANSPORTATION.CURRENCY_CODE%TYPE,
                    I_exchange_rate           IN       TRANSPORTATION.EXCHANGE_RATE%TYPE,
                    I_total_field             IN       VARCHAR2 DEFAULT NULL)
   return BOOLEAN is
   L_program           VARCHAR2(64) := 'TRANSPORTATION_SQL.GET_TOTALS';
   L_carton_qty        TRANSPORTATION.CARTON_QTY%TYPE  := 0;
   L_carton_qty_old    TRANSPORTATION.CARTON_QTY%TYPE  := O_carton_qty;
   L_carton_uom        UOM_CLASS.UOM%TYPE;
   L_item_qty          TRANSPORTATION.ITEM_QTY%TYPE    := 0;
   L_item_qty_old      TRANSPORTATION.ITEM_QTY%TYPE    := O_item_qty;
   L_item_qty_uom      UOM_CLASS.UOM%TYPE;
   L_gross_wt          TRANSPORTATION.GROSS_WT%TYPE    := 0;
   L_gross_wt_old      TRANSPORTATION.GROSS_WT%TYPE    := O_gross_wt;
   L_gross_wt_uom      UOM_CLASS.UOM%TYPE;
   L_net_wt            TRANSPORTATION.NET_WT%TYPE      := 0;
   L_net_wt_old        TRANSPORTATION.NET_WT%TYPE      := O_net_wt;
   L_net_wt_uom        UOM_CLASS.UOM%TYPE;
   L_cubic             TRANSPORTATION.CUBIC%TYPE       := 0;
   L_cubic_old         TRANSPORTATION.CUBIC%TYPE       := O_cubic;
   L_cubic_uom         UOM_CLASS.UOM%TYPE;
   L_invoice_amt       TRANSPORTATION.INVOICE_AMT%TYPE := 0;
   L_out_currency_code TRANSPORTATION.CURRENCY_CODE%TYPE;


   cursor C_VVE is
      select nvl(carton_qty,0) carton_qty,
             carton_uom,
             nvl(item_qty,0) item_qty,
             item_qty_uom,
             nvl(gross_wt,0) gross_wt,
             gross_wt_uom,
             nvl(net_wt,0) net_wt,
             net_wt_uom,
             nvl(cubic,0) cubic,
             cubic_uom,
             nvl(invoice_amt,0) invoice_amt,
             currency_code,
             exchange_rate,
             item
        from transportation
       where vessel_id = I_vessel_id
         and voyage_flt_id = I_voyage_id
         and estimated_depart_date = I_estimated_depart_date;

   cursor C_ORDER_ITEM is
      select nvl(carton_qty,0) carton_qty,
             carton_uom,
             nvl(item_qty,0) item_qty,
             item_qty_uom,
             nvl(gross_wt,0) gross_wt,
             gross_wt_uom,
             nvl(net_wt,0) net_wt,
             net_wt_uom,
             nvl(cubic,0) cubic,
             cubic_uom,
             nvl(invoice_amt,0) invoice_amt,
             currency_code,
             exchange_rate
        from transportation
       where order_no = I_order_no
         and item = I_item;

   cursor C_CONTAINER is
      select nvl(carton_qty,0) carton_qty,
             carton_uom,
             nvl(item_qty,0) item_qty,
             item_qty_uom,
             nvl(gross_wt,0) gross_wt,
             gross_wt_uom,
             nvl(net_wt,0) net_wt,
             net_wt_uom,
             nvl(cubic,0) cubic,
             cubic_uom,
             nvl(invoice_amt,0) invoice_amt,
             currency_code,
             exchange_rate,
             item
        from transportation
       where container_id = I_container_id;

   cursor C_BL_AWB is
      select nvl(carton_qty,0) carton_qty,
             carton_uom,
             nvl(item_qty,0) item_qty,
             item_qty_uom,
             nvl(gross_wt,0) gross_wt,
             gross_wt_uom,
             nvl(net_wt,0) net_wt,
             net_wt_uom,
             nvl(cubic,0) cubic,
             cubic_uom,
             nvl(invoice_amt,0) invoice_amt,
             currency_code,
             exchange_rate,
             item
        from transportation
       where bl_awb_id = I_bl_awb_id;

   cursor C_INVOICE_NO is
      select nvl(carton_qty,0) carton_qty,
             carton_uom,
             nvl(item_qty,0) item_qty,
             item_qty_uom,
             nvl(gross_wt,0) gross_wt,
             gross_wt_uom,
             nvl(net_wt,0) net_wt,
             net_wt_uom,
             nvl(cubic,0) cubic,
             cubic_uom,
             nvl(invoice_amt,0) invoice_amt,
             currency_code,
             exchange_rate,
             item
        from transportation
       where invoice_id = I_invoice_id;

BEGIN

   O_carton_qty  := 0;
   O_item_qty    := 0;
   O_gross_wt    := 0;
   O_net_wt      := 0;
   O_cubic       := 0;
   O_invoice_amt := 0;
   ---
   ---Default UOM if NULL
   if IO_carton_qty_uom is NULL then
      L_carton_uom := 'CT';
   else
      L_carton_uom := IO_carton_qty_uom;
   end if;
   ---
   if IO_item_qty_uom is NULL then
      L_item_qty_uom := 'EA';
   else
      L_item_qty_uom := IO_item_qty_uom;
   end if;
   ---
   if IO_gross_wt_uom is NULL then
      L_gross_wt_uom := 'LBS';
   else
      L_gross_wt_uom := IO_gross_wt_uom;
   end if;
   ---
   if IO_net_wt_uom is NULL then
      L_net_wt_uom := 'LBS';
   else
      L_net_wt_uom := IO_net_wt_uom;
   end if;
   ---
   if IO_cubic_uom is NULL then
      L_cubic_uom := 'CFT';
   else
      L_cubic_uom := IO_cubic_uom;
   end if;
   ---
---Open appropriate cursor based on Totals Level--
   if I_total_level = 'VVE' then
      FOR C1 in C_VVE LOOP
         --Convert carton quantity--
         if C1.carton_qty != 0 and (I_total_field is NULL or I_total_field = 'CQTY') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_carton_qty,
                               L_carton_uom,
                               C1.carton_qty,
                               C1.carton_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert item quantity--
         if C1.item_qty != 0 and (I_total_field is NULL or I_total_field = 'IQTY') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_item_qty,
                               L_item_qty_uom,
                               C1.item_qty,
                               C1.item_qty_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert gross weight--
         if C1.gross_wt != 0 and (I_total_field is NULL or I_total_field = 'GWT') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_gross_wt,
                               L_gross_wt_uom,
                               C1.gross_wt,
                               C1.gross_wt_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert net weight--
         if C1.net_wt != 0 and (I_total_field is NULL or I_total_field = 'NWT') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_net_wt,
                               L_net_wt_uom,
                               C1.net_wt,
                               C1.net_wt_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert cubic--
         if C1.cubic != 0 and (I_total_field is NULL or I_total_field = 'CUBIC') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_cubic,
                               L_cubic_uom,
                               C1.cubic,
                               C1.cubic_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         ---Convert Currency--
         if C1.invoice_amt <> 0 then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    C1.invoice_amt,
                                    C1.currency_code,
                                    I_currency_code,
                                    L_invoice_amt,
                                    'N',
                                    NULL,
                                    NULL,
                                    C1.exchange_rate,
                                    I_exchange_rate) = FALSE then
               return FALSE;
            end if;
         else
            L_invoice_amt := 0;
         end if;
         ---
         ---Sum values---
         O_carton_qty  := O_carton_qty  + L_carton_qty;
         O_item_qty    := O_item_qty    + L_item_qty;
         O_gross_wt    := O_gross_wt    + L_gross_wt;
         O_net_wt      := O_net_wt      + L_net_wt;
         O_cubic       := O_cubic       + L_cubic;
         O_invoice_amt := O_invoice_amt + L_invoice_amt;
         --Reset variables--
         L_carton_qty  := 0;
         L_item_qty    := 0;
         L_gross_wt    := 0;
         L_net_wt      := 0;
         L_cubic       := 0;
         L_invoice_amt := 0;
         ---
      END LOOP;
   elsif I_total_level = 'POIT' then
      FOR C1 in C_ORDER_ITEM LOOP
         --Convert carton quantity--
         if C1.carton_qty != 0 and (I_total_field is NULL or I_total_field = 'CQTY') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_carton_qty,
                               L_carton_uom,
                               C1.carton_qty,
                               C1.carton_uom,
                               I_item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert item quantity--
         if C1.item_qty != 0 and (I_total_field is NULL or I_total_field = 'IQTY') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_item_qty,
                               L_item_qty_uom,
                               C1.item_qty,
                               C1.item_qty_uom,
                               I_item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert gross weight--
         if C1.gross_wt != 0 and (I_total_field is NULL or I_total_field = 'GWT') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_gross_wt,
                               L_gross_wt_uom,
                               C1.gross_wt,
                               C1.gross_wt_uom,
                               I_item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert net weight--
         if C1.net_wt != 0 and (I_total_field is NULL or I_total_field = 'NWT') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_net_wt,
                               L_net_wt_uom,
                               C1.net_wt,
                               C1.net_wt_uom,
                               I_item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert cubic--
         if C1.cubic != 0 and (I_total_field is NULL or I_total_field = 'CUBIC') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_cubic,
                               L_cubic_uom,
                               C1.cubic,
                               C1.cubic_uom,
                               I_item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         ---Convert Currency--
         if C1.invoice_amt <> 0 then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    C1.invoice_amt,
                                    C1.currency_code,
                                    I_currency_code,
                                    L_invoice_amt,
                                    'N',
                                    NULL,
                                    NULL,
                                    C1.exchange_rate,
                                    I_exchange_rate) = FALSE then
               return FALSE;
            end if;
         else
            L_invoice_amt := 0;
         end if;
         ---
         ---Sum values---
         O_carton_qty  := O_carton_qty  + L_carton_qty;
         O_item_qty    := O_item_qty    + L_item_qty;
         O_gross_wt    := O_gross_wt    + L_gross_wt;
         O_net_wt      := O_net_wt      + L_net_wt;
         O_cubic       := O_cubic       + L_cubic;
         O_invoice_amt := O_invoice_amt + L_invoice_amt;
         --Reset variables--
         L_carton_qty  := 0;
         L_item_qty    := 0;
         L_gross_wt    := 0;
         L_net_wt      := 0;
         L_cubic       := 0;
         L_invoice_amt := 0;
         ---
      END LOOP;
   elsif I_total_level = 'CO' then
      FOR C1 in C_CONTAINER LOOP
         --Convert carton quantity--
         if C1.carton_qty != 0 and (I_total_field is NULL or I_total_field = 'CQTY') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_carton_qty,
                               L_carton_uom,
                               C1.carton_qty,
                               C1.carton_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert item quantity--
         if C1.item_qty != 0 and (I_total_field is NULL or I_total_field = 'IQTY') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_item_qty,
                               L_item_qty_uom,
                               C1.item_qty,
                               C1.item_qty_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert gross weight--
         if C1.gross_wt != 0 and (I_total_field is NULL or I_total_field = 'GWT') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_gross_wt,
                               L_gross_wt_uom,
                               C1.gross_wt,
                               C1.gross_wt_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert net weight--
         if C1.net_wt != 0 and (I_total_field is NULL or I_total_field = 'NWT') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_net_wt,
                               L_net_wt_uom,
                               C1.net_wt,
                               C1.net_wt_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert cubic--
         if C1.cubic != 0 and (I_total_field is NULL or I_total_field = 'CUBIC') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_cubic,
                               L_cubic_uom,
                               C1.cubic,
                               C1.cubic_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         ---Convert Currency--
         if C1.invoice_amt <> 0 then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    C1.invoice_amt,
                                    C1.currency_code,
                                    I_currency_code,
                                    L_invoice_amt,
                                    'N',
                                    NULL,
                                    NULL,
                                    C1.exchange_rate,
                                    I_exchange_rate) = FALSE then
               return FALSE;
            end if;
         else
            L_invoice_amt := 0;
         end if;
         ---
         ---Sum values---
         O_carton_qty  := O_carton_qty  + L_carton_qty;
         O_item_qty    := O_item_qty    + L_item_qty;
         O_gross_wt    := O_gross_wt    + L_gross_wt;
         O_net_wt      := O_net_wt      + L_net_wt;
         O_cubic       := O_cubic       + L_cubic;
         O_invoice_amt := O_invoice_amt + L_invoice_amt;
         --Reset variables--
         L_carton_qty  := 0;
         L_item_qty    := 0;
         L_gross_wt    := 0;
         L_net_wt      := 0;
         L_cubic       := 0;
         L_invoice_amt := 0;
         ---
      END LOOP;
   elsif I_total_level = 'BL' then
      FOR C1 in C_BL_AWB LOOP
         --Convert carton quantity--
         if C1.carton_qty != 0 and (I_total_field is NULL or I_total_field = 'CQTY') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_carton_qty,
                               L_carton_uom,
                               C1.carton_qty,
                               C1.carton_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert item quantity--
         if C1.item_qty != 0 and (I_total_field is NULL or I_total_field = 'IQTY') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_item_qty,
                               L_item_qty_uom,
                               C1.item_qty,
                               C1.item_qty_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert gross weight--
         if C1.gross_wt != 0 and (I_total_field is NULL or I_total_field = 'GWT') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_gross_wt,
                               L_gross_wt_uom,
                               C1.gross_wt,
                               C1.gross_wt_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert net weight--
         if C1.net_wt != 0 and (I_total_field is NULL or I_total_field = 'NWT') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_net_wt,
                               L_net_wt_uom,
                               C1.net_wt,
                               C1.net_wt_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert cubic--
         if C1.cubic != 0 and (I_total_field is NULL or I_total_field = 'CUBIC') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_cubic,
                               L_cubic_uom,
                               C1.cubic,
                               C1.cubic_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         ---Convert Currency--
         if C1.invoice_amt <> 0 then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    C1.invoice_amt,
                                    C1.currency_code,
                                    I_currency_code,
                                    L_invoice_amt,
                                    'N',
                                    NULL,
                                    NULL,
                                    C1.exchange_rate,
                                    I_exchange_rate) = FALSE then
               return FALSE;
            end if;
         else
            L_invoice_amt := 0;
         end if;
         ---
         ---Sum values---
         O_carton_qty  := O_carton_qty  + L_carton_qty;
         O_item_qty    := O_item_qty    + L_item_qty;
         O_gross_wt    := O_gross_wt    + L_gross_wt;
         O_net_wt      := O_net_wt      + L_net_wt;
         O_cubic       := O_cubic       + L_cubic;
         O_invoice_amt := O_invoice_amt + L_invoice_amt;
         --Reset variables--
         L_carton_qty  := 0;
         L_item_qty    := 0;
         L_gross_wt    := 0;
         L_net_wt      := 0;
         L_cubic       := 0;
         L_invoice_amt := 0;
         ---
      END LOOP;
   elsif I_total_level = 'CI' then
      FOR C1 in C_INVOICE_NO LOOP
         --Convert carton quantity--
         if C1.carton_qty != 0 and (I_total_field is NULL or I_total_field = 'CQTY') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_carton_qty,
                               L_carton_uom,
                               C1.carton_qty,
                               C1.carton_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert item quantity--
         if C1.item_qty != 0 and (I_total_field is NULL or I_total_field = 'IQTY') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_item_qty,
                               L_item_qty_uom,
                               C1.item_qty,
                               C1.item_qty_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert gross weight--
         if C1.gross_wt != 0 and (I_total_field is NULL or I_total_field = 'GWT') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_gross_wt,
                               L_gross_wt_uom,
                               C1.gross_wt,
                               C1.gross_wt_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert net weight--
         if C1.net_wt != 0 and (I_total_field is NULL or I_total_field = 'NWT') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_net_wt,
                               L_net_wt_uom,
                               C1.net_wt,
                               C1.net_wt_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         --Convert cubic--
         if C1.cubic != 0 and (I_total_field is NULL or I_total_field = 'CUBIC') then
            if UOM_SQL.CONVERT(O_error_message,
                               L_cubic,
                               L_cubic_uom,
                               C1.cubic,
                               C1.cubic_uom,
                               C1.item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         ---Convert Currency--
         if C1.invoice_amt <> 0 then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    C1.invoice_amt,
                                    C1.currency_code,
                                    I_currency_code,
                                    L_invoice_amt,
                                    'N',
                                    NULL,
                                    NULL,
                                    C1.exchange_rate,
                                    I_exchange_rate) = FALSE then
               return FALSE;
            end if;
         else
            L_invoice_amt := 0;
         end if;
         ---
         ---Sum values---
         O_carton_qty  := O_carton_qty  + L_carton_qty;
         O_item_qty    := O_item_qty    + L_item_qty;
         O_gross_wt    := O_gross_wt    + L_gross_wt;
         O_net_wt      := O_net_wt      + L_net_wt;
         O_cubic       := O_cubic       + L_cubic;
         O_invoice_amt := O_invoice_amt + L_invoice_amt;
         --Reset variables--
         L_carton_qty  := 0;
         L_item_qty    := 0;
         L_gross_wt    := 0;
         L_net_wt      := 0;
         L_cubic       := 0;
         L_invoice_amt := 0;
         ---
      END LOOP;
   end if;
   ---Assign UOM's---
   IO_carton_qty_uom := L_carton_uom;
   IO_item_qty_uom   := L_item_qty_uom;
   IO_gross_wt_uom   := L_gross_wt_uom;
   IO_net_wt_uom     := L_net_wt_uom;
   IO_cubic_uom      := L_cubic_uom;
    ---
   if I_total_field is NOT NULL then
      if I_total_field != 'CQTY' then
         O_carton_qty := L_carton_qty_old;
      end if;
      ---
      if I_total_field != 'IQTY' then
         O_item_qty := L_item_qty_old;
      end if;
      ---
      if I_total_field != 'GWT' then
         O_gross_wt := L_gross_wt_old;
      end if;
      ---
      if I_total_field != 'NWT' then
         O_net_wt := L_net_wt_old;
      end if;
      ---
      if I_total_field != 'CUBIC' then
         O_cubic := L_cubic_old;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_TOTALS;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_FREIGHT_TYPE_DESC(O_error_message       IN OUT VARCHAR2,
                               O_exists              IN OUT BOOLEAN,
                               O_freight_type_desc   IN OUT FREIGHT_TYPE_TL.FREIGHT_TYPE_DESC%TYPE,
                               I_freight_type        IN     FREIGHT_TYPE.FREIGHT_TYPE%TYPE)
   return  BOOLEAN is

   cursor  C_GET_FREIGHT_DESC is
      select freight_type_desc
        from v_freight_type_tl
       where freight_type = I_freight_type;
BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_GET_FREIGHT_DESC','FREIGHT_TYPE',
                    'freight_type: '||I_freight_type);
   open C_GET_FREIGHT_DESC;
   SQL_LIB.SET_MARK('FETCH','C_GET_FREIGHT_DESC','FREIGHT_TYPE',
                    'freight_type: '||I_freight_type);
   fetch C_GET_FREIGHT_DESC into O_freight_type_desc;

   if C_GET_FREIGHT_DESC%NOTFOUND then
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FREIGHT_TYPE',I_freight_type,NULL,NULL);
   else
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_GET_FREIGHT_DESC','FREIGHT_TYPE',
                    'freight_type: '||I_freight_type);
   close C_GET_FREIGHT_DESC;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE',
                                            'SQLERRM',
                                            'TRANSPORTATION_SQL.GET_FREIGHT_TYPE_DESC',
                                            to_char(SQLCODE));
   return FALSE;
END GET_FREIGHT_TYPE_DESC;
---------------------------------------------------------------------------------------------------------
FUNCTION MERGE_FREIGHT_TYPE_TL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_freight_type        IN       FREIGHT_TYPE_TL.FREIGHT_TYPE%TYPE,
                               I_freight_type_desc   IN       FREIGHT_TYPE_TL.FREIGHT_TYPE_DESC%TYPE,
                               I_lang                IN       FREIGHT_TYPE_TL.LANG%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(61) := 'TRANSPORTATION_SQL.MERGE_FREIGHT_TYPE_TL';
   L_table         VARCHAR2(30) := 'FREIGHT_TYPE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   L_freight_exists    VARCHAR2(1)  := NULL;
   L_orig_lang_ind   VARCHAR2(1)  := 'N';
   L_reviewed_ind    VARCHAR2(1)  := 'N';

   cursor C_FREIGHT_TYPE_EXIST is
      select 'x'
        from freight_type_tl
       where freight_type = I_freight_type
         and rownum = 1;

   cursor C_LOCK_FREIGHT_TYPE_TL is
      select 'x'
        from freight_type_tl
       where freight_type = I_freight_type
         and lang   = I_lang
         for update nowait;

BEGIN

   -- Check first if the freight type (regardless of language) already exists in the table.
   -- If it already exists, set orig_lang_ind = 'N'. Otherwise, set orig_lang_ind = 'Y'.
   -- Reviewed_ind is only set to 'N' for entries in the original language to indicate
   -- if the original description has changed and translation should be reviewed for accuracy.
   -- Both flags are only used for inserts, not updates.    

   open C_FREIGHT_TYPE_EXIST;
   fetch C_FREIGHT_TYPE_EXIST into L_freight_exists;
   close C_FREIGHT_TYPE_EXIST;

   if L_freight_exists is NULL then
      L_orig_lang_ind := 'Y';
      L_reviewed_ind := 'N';
   else
      L_orig_lang_ind := 'N';
      L_reviewed_ind := 'Y';
   end if;

   open C_LOCK_FREIGHT_TYPE_TL;
   close C_LOCK_FREIGHT_TYPE_TL;

   merge into freight_type_tl fstl
      using (select I_freight_type freight_type,
                    I_lang lang,
                    I_freight_type_desc freight_type_desc,
                    L_orig_lang_ind orig_lang_ind, --used for inserts only
                    L_reviewed_ind reviewed_ind,   --used for inserts only
                    user create_id,
                    sysdate create_datetime,
                    user last_update_id,
                    sysdate last_update_datetime
               from dual) use_this
         on (fstl.freight_type = use_this.freight_type and
             fstl.lang   = use_this.lang)
   when matched then
      update
         set fstl.freight_type_desc = use_this.freight_type_desc,
             fstl.reviewed_ind = decode(fstl.orig_lang_ind, 'Y', 'N', reviewed_ind), --when description is changed for the original language, set the entry for translation review
             fstl.last_update_id = use_this.last_update_id,
             fstl.last_update_datetime = use_this.last_update_datetime
   when NOT matched then
      insert (freight_type,
              lang,
              freight_type_desc,
              orig_lang_ind,
              reviewed_ind,
              create_id,
              create_datetime,
              last_update_id,
              last_update_datetime)
      values (use_this.freight_type,
              use_this.lang,
              use_this.freight_type_desc,
              use_this.orig_lang_ind,
              use_this.reviewed_ind,
              use_this.create_id,
              use_this.create_datetime,
              use_this.last_update_id,
              use_this.last_update_datetime);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            to_char(SQLCODE));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_FREIGHT_TYPE_TL;
---------------------------------------------------------------------------------------------------------
FUNCTION DEL_FREIGHT_TYPE_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_freight_type    IN       FREIGHT_TYPE_TL.FREIGHT_TYPE%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(61) := 'TRANSPORTATION_SQL.DEL_FREIGHT_TYPE_TL';
   L_table         VARCHAR2(30) := 'FREIGHT_TYPE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_FREIGHT_TYPE_TL is
      select 'x'
        from freight_type_tl
       where freight_type = I_freight_type
         for update nowait;

BEGIN

   open C_FREIGHT_TYPE_TL;
   close C_FREIGHT_TYPE_TL;

   delete from freight_type_tl
    where freight_type = I_freight_type;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            to_char(SQLCODE));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEL_FREIGHT_TYPE_TL;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_FREIGHT_TYPE (O_error_message     IN OUT VARCHAR2,
                                    O_exists            IN OUT BOOLEAN,
                                    I_freight_type      IN     FREIGHT_TYPE.FREIGHT_TYPE%TYPE)
   return BOOLEAN is
      L_exists  VARCHAR(1);
      L_program VARCHAR(50) := 'TRANSPORTATION_SQL.CHECK_DELETE_FREIGHT_TYPE';

   cursor C_CHECK_FREIGHT_TYPE is
      select 'X'
        from transportation
       where freight_type = I_freight_type;

BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_CHECK_FREIGHT_TYPE', 'TRANSPORTATION','freight_type: ' || I_freight_type);
   open C_CHECK_FREIGHT_TYPE;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_FREIGHT_TYPE','TRANSPORTATION','freight_type: ' || I_freight_type);
   fetch C_CHECK_FREIGHT_TYPE into L_exists;

   if C_CHECK_FREIGHT_TYPE%FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DELETE_FREIGHT_TYP',NULL,NULL,NULL);
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_FREIGHT_TYPE','TRANSPORTATION','freight_type: ' || I_freight_type);
   close C_CHECK_FREIGHT_TYPE;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_DELETE_FREIGHT_TYPE;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_MAIN_INFO(O_error_message          IN OUT VARCHAR2,
                       O_return_code            IN OUT BOOLEAN,
                       O_vessel_id              IN OUT TRANSPORTATION.VESSEL_ID%TYPE,
                       O_voyage_flt_id          IN OUT TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                       O_estimated_depart_date  IN OUT TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                       O_order_no               IN OUT TRANSPORTATION.ORDER_NO%TYPE,
                       O_item                   IN OUT TRANSPORTATION.ITEM%TYPE,
                       O_container_id           IN OUT TRANSPORTATION.CONTAINER_ID%TYPE,
                       O_bl_awb_id              IN OUT TRANSPORTATION.BL_AWB_ID%TYPE,
                       O_invoice_id             IN OUT TRANSPORTATION.INVOICE_ID%TYPE,
                       O_status                 IN OUT TRANSPORTATION.STATUS%TYPE,
                       I_transportation_id      IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)    :=    'TRANSPORTATION_SQL.GET_MAIN_INFO';
   ---
   cursor C_GET_TRANS_INFO is
      select vessel_id,
             voyage_flt_id,
             estimated_depart_date,
             order_no,
             item,
             container_id,
             bl_awb_id,
             invoice_id,
             status
        from transportation
       where transportation_id = I_transportation_id;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_TRANS_INFO','TRANSPORTATION',
                    'transportation_id: '||to_char(I_transportation_id));
   open  C_GET_TRANS_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_TRANS_INFO','TRANSPORTATION',
                    'transportation_id: '||to_char(I_transportation_id));
   fetch C_GET_TRANS_INFO into O_vessel_id,
                               O_voyage_flt_id,
                               O_estimated_depart_date,
                               O_order_no,
                               O_item,
                               O_container_id,
                               O_bl_awb_id,
                               O_invoice_id,
                               O_status;
   if C_GET_TRANS_INFO%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_TRANS_INFO', to_char(I_transportation_id));
      O_return_code   := FALSE;
      SQL_LIB.SET_MARK('CLOSE','C_GET_TRANS_INFO','TRANSPORTATION',
                       'transportation_id: '||to_char(I_transportation_id));
      close C_GET_TRANS_INFO;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_TRANS_INFO','TRANSPORTATION',
                    'transportation_id: '||to_char(I_transportation_id));
   close C_GET_TRANS_INFO;
   ---
   O_return_code := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END GET_MAIN_INFO;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_ESTIMATED_DEPART_DATE(O_error_message          IN OUT VARCHAR2,
                                   O_estimated_depart_date  IN OUT TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                   I_order_no               IN     TRANSPORTATION.ORDER_NO%TYPE,
                                   I_vessel_id              IN     TRANSPORTATION.VESSEL_ID%TYPE,
                                   I_voyage_flt_id          IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(80)    :=    'TRANSPORTATION_SQL.GET_ESTIMATED_DEPART_DATE';
   ---
   cursor C_GET_ESTIMATED_DEPART_DATE is
      select estimated_depart_date
        from transportation
       where voyage_flt_id     = I_voyage_flt_id
         and vessel_id         = I_vessel_id
         and order_no          = I_order_no
         and transportation_id > 0;

BEGIN
   O_estimated_depart_date := NULL;

   SQL_LIB.SET_MARK('OPEN','C_GET_ESTIMATED_DEPART_DATE','TRANSPORTATION',
                    'order_no: '||to_char(I_order_no)||' '||
                    'vessel_id: '||to_char(I_vessel_id)||' '||
                    'voyage_flt_id: '||to_char(I_voyage_flt_id));
   open  C_GET_ESTIMATED_DEPART_DATE;

   SQL_LIB.SET_MARK('FETCH','C_GET_ESTIMATED_DEPART_DATE','TRANSPORTATION',
                    'order_no: '||to_char(I_order_no)||' '||
                    'vessel_id: '||to_char(I_vessel_id)||' '||
                    'voyage_flt_id: '||to_char(I_voyage_flt_id));
   fetch C_GET_ESTIMATED_DEPART_DATE into O_estimated_depart_date;

   SQL_LIB.SET_MARK('CLOSE','C_GET_ESTIMATED_DEPART_DATE','TRANSPORTATION',
                    'order_no: '||to_char(I_order_no)||' '||
                    'vessel_id: '||to_char(I_vessel_id)||' '||
                    'voyage_flt_id: '||to_char(I_voyage_flt_id));
   close C_GET_ESTIMATED_DEPART_DATE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END GET_ESTIMATED_DEPART_DATE;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_SCAC_CODE_DESC(O_error_message          IN OUT  VARCHAR2,
                            O_exists                 IN OUT  BOOLEAN,
                            O_scac_code_desc         IN OUT  SCAC.SCAC_CODE_DESC%TYPE,
                            I_scac_code              IN      SCAC.SCAC_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)  :=  'TRANSPORTATION_SQL.GET_SCAC_CODE_DESC';
   L_error_message  VARCHAR2(255);
   ---
   cursor C_GET_SCAC_CODE is
      select scac_code_desc
        from v_scac_tl
       where scac_code = I_scac_code;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_SCAC_CODE','TRANSPORTATION',
                    'scac_code: '||I_scac_code);
   open  C_GET_SCAC_CODE;
   SQL_LIB.SET_MARK('FETCH','C_GET_SCAC_CODE','TRANSPORTATION',
                    'scac_code: '||I_scac_code);
   fetch C_GET_SCAC_CODE into O_scac_code_desc;
   if C_GET_SCAC_CODE%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_SCAC_CODE', I_scac_code);
      O_exists   := FALSE;
      SQL_LIB.SET_MARK('CLOSE','C_SCAC_CODE','TRANSPORTATION',
                       'scac_code: '||I_scac_code);
      close C_GET_SCAC_CODE;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_SCAC_CODE','TRANSPORTATION',
                    'scac_code: '||I_scac_code);
   close C_GET_SCAC_CODE;
   ---
   O_exists := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END GET_SCAC_CODE_DESC;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_FREIGHT_SIZE_DESC(O_error_message         IN OUT  VARCHAR2,
                               O_exists                IN OUT  BOOLEAN,
                               O_freight_size_desc     IN OUT  FREIGHT_SIZE_TL.FREIGHT_SIZE_DESC%TYPE,
                               I_freight_size          IN      FREIGHT_SIZE.FREIGHT_SIZE%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)  :=  'TRANSPORTATION_SQL.GET_FREIGHT_SIZE_DESC';
   L_error_message  VARCHAR2(255);
   ---
   cursor C_GET_FREIGHT_SIZE is
      select freight_size_desc
        from v_freight_size_tl
       where freight_size = I_freight_size;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_FREIGHT_SIZE','TRANSPORTATION',
                    'freight_size: '||I_freight_size);
   open  C_GET_FREIGHT_SIZE;
   SQL_LIB.SET_MARK('FETCH','C_GET_FREIGHT_SIZE','TRANSPORTATION',
                    'freight_size: '||I_freight_size);
   fetch C_GET_FREIGHT_SIZE into O_freight_size_desc;
   if C_GET_FREIGHT_SIZE%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_FREIGHT_SIZE', I_freight_size);
      O_exists   := FALSE;
      SQL_LIB.SET_MARK('CLOSE','C_GET_FREIGHT_SIZE','TRANSPORTATION',
                       'freight_size: '||I_freight_size);
      close C_GET_FREIGHT_SIZE;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_FREIGHT_SIZE','TRANSPORTATION',
                    'freight_size: '||I_freight_size);
   close C_GET_FREIGHT_SIZE;
   ---
   O_exists := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END GET_FREIGHT_SIZE_DESC;
---------------------------------------------------------------------------------------------------------
FUNCTION MERGE_FREIGHT_SIZE_TL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_freight_size        IN       FREIGHT_SIZE_TL.FREIGHT_SIZE%TYPE,
                               I_freight_size_desc   IN       FREIGHT_SIZE_TL.FREIGHT_SIZE_DESC%TYPE,
                               I_lang                IN       FREIGHT_SIZE_TL.LANG%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(61) := 'TRANSPORTATION_SQL.MERGE_FREIGHT_SIZE_TL';
   L_table         VARCHAR2(30) := 'FREIGHT_SIZE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   L_freight_exists    VARCHAR2(1)  := NULL;
   L_orig_lang_ind   VARCHAR2(1)  := 'N';
   L_reviewed_ind    VARCHAR2(1)  := 'N';

   cursor C_FREIGHT_SIZE_EXIST is
      select 'x'
        from freight_size_tl
       where freight_size = I_freight_size
         and rownum = 1;

   cursor C_LOCK_FREIGHT_SIZE_TL is
      select 'x'
        from freight_size_tl
       where freight_size = I_freight_size
         and lang   = I_lang
         for update nowait;

BEGIN

   -- Check first if the freight size (regardless of language) already exists in the table.
   -- If it already exists, set orig_lang_ind = 'N'. Otherwise, set orig_lang_ind = 'Y'.
   -- Reviewed_ind is only set to 'N' for entries in the original language to indicate
   -- if the original description has changed and translation should be reviewed for accuracy.
   -- Both flags are only used for inserts, not updates.    

   open C_FREIGHT_SIZE_EXIST;
   fetch C_FREIGHT_SIZE_EXIST into L_freight_exists;
   close C_FREIGHT_SIZE_EXIST;

   if L_freight_exists is NULL then
      L_orig_lang_ind := 'Y';
      L_reviewed_ind := 'N';
   else
      L_orig_lang_ind := 'N';
      L_reviewed_ind := 'Y';
   end if;

   open C_LOCK_FREIGHT_SIZE_TL;
   close C_LOCK_FREIGHT_SIZE_TL;

   merge into freight_size_tl fstl
      using (select I_freight_size freight_size,
                    I_lang lang,
                    I_freight_size_desc freight_size_desc,
                    L_orig_lang_ind orig_lang_ind, --used for inserts only
                    L_reviewed_ind reviewed_ind,   --used for inserts only
                    user create_id,
                    sysdate create_datetime,
                    user last_update_id,
                    sysdate last_update_datetime
               from dual) use_this
         on (fstl.freight_size = use_this.freight_size and
             fstl.lang   = use_this.lang)
   when matched then
      update
         set fstl.freight_size_desc = use_this.freight_size_desc,
             fstl.reviewed_ind = decode(fstl.orig_lang_ind, 'Y', 'N', reviewed_ind), --when description is changed for the original language, set the entry for translation review
             fstl.last_update_id = use_this.last_update_id,
             fstl.last_update_datetime = use_this.last_update_datetime
   when NOT matched then
      insert (freight_size,
              lang,
              freight_size_desc,
              orig_lang_ind,
              reviewed_ind,
              create_id,
              create_datetime,
              last_update_id,
              last_update_datetime)
      values (use_this.freight_size,
              use_this.lang,
              use_this.freight_size_desc,
              use_this.orig_lang_ind,
              use_this.reviewed_ind,
              use_this.create_id,
              use_this.create_datetime,
              use_this.last_update_id,
              use_this.last_update_datetime);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            to_char(SQLCODE));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_FREIGHT_SIZE_TL;
---------------------------------------------------------------------------------------------------------
FUNCTION DEL_FREIGHT_SIZE_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_freight_size    IN       FREIGHT_SIZE_TL.FREIGHT_SIZE%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(61) := 'TRANSPORTATION_SQL.DEL_FREIGHT_SIZE_TL';
   L_table         VARCHAR2(30) := 'FREIGHT_SIZE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_FREIGHT_SIZE_TL is
      select 'x'
        from freight_size_tl
       where freight_size = I_freight_size
         for update nowait;

BEGIN

   open C_FREIGHT_SIZE_TL;
   close C_FREIGHT_SIZE_TL;

   delete from freight_size_tl
    where freight_size = I_freight_size;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            to_char(SQLCODE));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEL_FREIGHT_SIZE_TL;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_FREIGHT_SIZE(O_error_message   IN OUT VARCHAR2,
                                   O_exists          IN OUT BOOLEAN,
                                   I_freight_size    IN     FREIGHT_SIZE.FREIGHT_SIZE%TYPE)
   RETURN BOOLEAN IS
      L_exists   VARCHAR(1);
      L_program  VARCHAR(50) := 'TRANSPORTATION_SQL.CHECK_DELETE_FREIGHT_SIZE';
   cursor C_CHECK_FREIGHT_SIZE is
      select 'X'
        from transportation
       where freight_size = I_freight_size;
BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_CHECK_FREIGHT_SIZE','TRANSPORTATION','freight_size :' || I_freight_size);
   open C_CHECK_FREIGHT_SIZE;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_FREIGHT_SIZE','TRANSPORTATION','freight_size :' || I_freight_size);
   fetch C_CHECK_FREIGHT_SIZE into L_exists;

   if C_CHECK_FREIGHT_SIZE%FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DELETE_FREIGHT_SIZ',NULL,NULL,NULL);
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_FREIGHT_SIZE','TRANSPORTATION','freight_type :' || I_freight_size);
   close C_CHECK_FREIGHT_SIZE;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_DELETE_FREIGHT_SIZE;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_SCAC_CODE(O_error_message   IN OUT VARCHAR2,
                                O_exists          IN OUT BOOLEAN,
                                I_scac_code       IN     SCAC.SCAC_CODE%TYPE)
   RETURN BOOLEAN IS
      L_exists   VARCHAR(1);
      L_program  VARCHAR(50) := 'TRANSPORTATION_SQL.CHECK_DELETE_SCAC_CODE';
   cursor C_CHECK_SCAC is
      select 'X'
         from transportation
        where vessel_scac_code = I_scac_code
           or container_scac_code = I_scac_code;
BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_CHECK_SCAC','TRANSPORTATION','scac_code :' || I_scac_code);
   open C_CHECK_SCAC;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_SCAC','TRANSPORTATION','scac_code :' || I_scac_code);
   fetch C_CHECK_SCAC into L_exists;

   if C_CHECK_SCAC%FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DELETE_SCAC',NULL,NULL,NULL);
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_SCAC','TRANSPORTATION','   scac_code :' || I_scac_code);
   close C_CHECK_SCAC;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_DELETE_SCAC_CODE;
---------------------------------------------------------------------------------------------------------
FUNCTION LIC_VISA_EXISTS(O_error_message             IN OUT   VARCHAR2,
                         O_exists              IN OUT   BOOLEAN,
                         I_transportation_id   IN       TRANSPORTATION.TRANSPORTATION_ID%TYPE,
                         I_import_country_id   IN       TRANS_LIC_VISA.IMPORT_COUNTRY_ID%TYPE,
                         I_license_visa_id     IN       TRANS_LIC_VISA.LICENSE_VISA_ID%TYPE,
                         I_license_visa_type   IN       TRANS_LIC_VISA.LICENSE_VISA_TYPE%TYPE)
   return BOOLEAN IS
      L_program        VARCHAR2(50)  :=  'TRANSPORTATION_SQL.LIC_VISA_EXISTS';
      L_error_message  VARCHAR2(255);
      L_exists VARCHAR2(1);

      cursor C_EXISTS is
         select 'x'
           from trans_lic_visa
          where transportation_id = I_transportation_id
            and import_country_id = nvl(I_import_country_id, import_country_id)
            and license_visa_id = nvl(I_license_visa_id, license_visa_id)
            and license_visa_type = nvl(I_license_visa_type, license_visa_type);
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_EXISTS','TRANSPORTATION',
                    'transportation_id: '||I_transportation_id);
   open  C_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_EXISTS','TRANSPORTATION',
                    'transportation_id: '||I_transportation_id);
   fetch C_EXISTS into L_exists;
   if C_EXISTS%FOUND then
      if I_license_visa_type = 'L' then
         O_error_message := SQL_LIB.CREATE_MSG('LIC_EXISTS',I_license_visa_id);
      else
         O_error_message := SQL_LIB.CREATE_MSG('VISA_EXISTS',I_license_visa_id);
      end if;
      O_exists   := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','TRANSPORTATION',
                    'transportation_id: '||I_transportation_id);
   close C_EXISTS;

   return(TRUE);


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END LIC_VISA_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION CONTAINER_EXISTS (O_error_message   IN OUT VARCHAR2,
                           O_exists          IN OUT BOOLEAN,
                           I_container_id    IN     TRANSPORTATION.CONTAINER_ID%TYPE)
   return BOOLEAN is

      L_exists   VARCHAR2(1) := NULL;

      cursor C_CONT_EXISTS is
         select 'X'
           from transportation
          where container_id = I_container_id;
BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_CONT_EXISTS','TRANSPORTATION','CONTAINER ID:' || I_container_id);
   open C_CONT_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_CONT_EXISTS','TRANSPORTATION','CONTAINER ID:' || I_container_id);
   fetch C_CONT_EXISTS into L_exists;
   if C_CONT_EXISTS%NOTFOUND then
      O_exists        := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_CONTAINER',NULL,NULL,NULL);
   else
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_CONT_EXISTS','TRANSPORTATION','CONTAINER ID:' || I_container_id);
   close C_CONT_EXISTS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSPORTATION_SQL.CONTAINER_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;

END CONTAINER_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION BL_AWB_EXISTS(O_error_message   IN OUT VARCHAR2,
                       O_exists          IN OUT BOOLEAN,
                       I_bl_awb_id       IN     TRANSPORTATION.BL_AWB_ID%TYPE)
   return BOOLEAN is

      L_exists   VARCHAR2(1) := NULL;

      cursor C_BL_AWB_EXISTS is
         select 'X'
           from transportation
          where bl_awb_id = I_bl_awb_id;
BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_BL_AWB_EXISTS','TRANSPORTATION','BL AWB ID:' || I_bl_awb_id);
   open C_BL_AWB_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_BL_AWB_EXISTS','TRANSPORTATION','BL AWB ID:' || I_bl_awb_id);
   fetch C_BL_AWB_EXISTS into L_exists;
   if C_BL_AWB_EXISTS%NOTFOUND then
      O_exists        := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_BL_AWB',NULL,NULL,NULL);
   else
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_BL_AWB_EXISTS','TRANSPORTATION','BL AWB ID:' || I_bl_awb_id);
   close C_BL_AWB_EXISTS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSPORTATION_SQL.BL_AWB_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;

END BL_AWB_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION INV_EXISTS(O_error_message   IN OUT VARCHAR2,
                    O_exists          IN OUT BOOLEAN,
                    I_invoice_id      IN     TRANSPORTATION.INVOICE_ID%TYPE)
   return BOOLEAN is
      L_exists   VARCHAR2(1) := NULL;

      cursor C_INV_EXISTS is
         select 'X'
           from transportation
          where invoice_id = I_invoice_id;
BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_INV_EXISTS','TRANSPORTATION','INVOICE ID:' || I_invoice_id);
   open C_INV_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_INV_EXISTS','TRANSPORTATION','INVOICE ID:' || I_invoice_id);
   fetch C_INV_EXISTS into L_exists;
   if C_INV_EXISTS%NOTFOUND then
      O_exists        := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_COM_INVOICE',NULL,NULL,NULL);
   else
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_INV_EXISTS','TRANSPORTATION','INVOICE ID:' || I_invoice_id);
   close C_INV_EXISTS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSPORTATION_SQL.INV_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;

END INV_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION VVE_EXISTS(O_error_message          IN OUT VARCHAR2,
                    O_exists                 IN OUT BOOLEAN,
                    I_vessel_id              IN     TRANSPORTATION.VESSEL_ID%TYPE,
                    I_voyage_flt_id          IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                    I_estimated_depart_date  IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE)
   return BOOLEAN is
      L_exists   VARCHAR2(1) := NULL;

      cursor C_VVE_EXISTS is
         select 'X'
           from transportation
          where vessel_id             = NVL(I_vessel_id, vessel_id)
            and voyage_flt_id         = NVL(I_voyage_flt_id, voyage_flt_id)
            and estimated_depart_date = NVL(I_estimated_depart_date, estimated_depart_date);

BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_VVE_EXISTS','TRANSPORTATION',NULL);
   open C_VVE_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_VVE_EXISTS','TRANSPORTATION',NULL);   fetch C_VVE_EXISTS into L_exists;
   if C_VVE_EXISTS%NOTFOUND then
      O_exists        := FALSE;

      if I_vessel_id is NULL and I_voyage_flt_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_EST_DEPART_DATE',NULL,NULL,NULL);
      end if;
      if I_vessel_id is NULL and I_estimated_depart_date is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_VOY_FLT',NULL,NULL,NULL);
      end if;
      if I_voyage_flt_id is NULL and I_estimated_depart_date is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_VESSEL',NULL,NULL,NULL);
      end if;
      if I_vessel_id is not NULL and I_voyage_flt_id is not NULL and I_estimated_depart_date is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_VVE_COMBO',NULL,NULL,NULL);
      end if;
      if I_vessel_id is not NULL and I_voyage_flt_id is not NULL and I_estimated_depart_date is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_VV_COMBO',NULL,NULL,NULL);
      end if;
      if I_voyage_flt_id is not NULL and I_estimated_depart_date is not NULL and I_vessel_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_VOY_EDD_COMBO',NULL,NULL,NULL);
      end if;
      if I_vessel_id is not NULL and I_estimated_depart_date is not NULL and I_voyage_flt_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_VE_COMBO',NULL,NULL,NULL);
      end if;
   else
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_VVE_EXISTS','TRANSPORTATION',NULL);
   close C_VVE_EXISTS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSPORTATION_SQL.VVE_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;

END VVE_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION PACKING_EXISTS(O_error_message     IN OUT   VARCHAR2,
                        O_exists              IN OUT   BOOLEAN,
                        I_transportation_id   IN       TRANSPORTATION.TRANSPORTATION_ID%TYPE,
                        I_from_carton         IN       TRANS_PACKING.FROM_CARTON%TYPE,
                                I_to_carton           IN        TRANS_PACKING.TO_CARTON%TYPE)
   return BOOLEAN IS
      L_program        VARCHAR2(50)  :=  'TRANSPORTATION_SQL.PACKING_EXISTS';
      L_error_message  VARCHAR2(255);
      L_exists         VARCHAR2(1);


 cursor C_RECORD_EXISTS is
    select 'X'
      from trans_packing
     where transportation_id = I_transportation_id
       and from_carton       = NVL(I_from_carton,from_carton)
       and to_carton         = NVL(I_to_carton,to_carton);

BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_RECORD_EXISTS','TRANS_PACKING',
                    'transportation_id:'||to_char(I_transportation_id));

   open C_RECORD_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_RECORD_EXISTS','TRANS_PACKING',
                    'transportation_id:'||to_char(I_transportation_id));
   fetch C_RECORD_EXISTS into L_exists;

   if C_RECORD_EXISTS%FOUND then
      O_exists := TRUE;
      O_error_message:=SQL_LIB.CREATE_MSG('INV_CARTON_RANGE', I_from_carton, I_to_carton, NULL);
      close C_RECORD_EXISTS;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_RECORD_EXISTS','TRANS_PACKING',
      'transportation_id:'||to_char(I_transportation_id));

   close C_RECORD_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                                          to_char(SQLCODE));
      return FALSE;

END PACKING_EXISTS;
---------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_PACKING_REC(O_error_message       IN OUT VARCHAR2,
                             O_return_code         IN OUT BOOLEAN,
                             I_transportation_id   IN TRANSPORTATION.TRANSPORTATION_ID%TYPE)
       return BOOLEAN is

      L_program        VARCHAR2(50)  :=  'TRANSPORTATION_SQL.DEFAULT_PACKING_REC';

BEGIN
   update trans_packing
      set carton_receive_qty = nvl(carton_receive_qty, carton_qty),
          carton_rec_qty_uom = nvl(carton_rec_qty_uom, carton_qty_uom),
          carton_pack_rec_qty = nvl(carton_pack_rec_qty, carton_pack_qty),
          carton_pack_rec_qty_uom = nvl(carton_pack_rec_qty_uom, carton_pack_qty_uom),
          item_receive_qty = nvl(item_receive_qty, item_qty),
          item_rec_qty_uom = nvl(item_rec_qty_uom, item_qty_uom)
    where transportation_id  = I_transportation_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                                    L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DEFAULT_PACKING_REC;
----------------------------------------------------------------------------------------------------
FUNCTION DELIVERY_EXISTS(O_error_message             IN OUT   VARCHAR2,
                         O_exists              IN OUT   BOOLEAN,
                         I_transportation_id   IN       TRANSPORTATION.TRANSPORTATION_ID%TYPE,
                         I_delivery_location   IN       TRANS_DELIVERY.DELIVERY_LOCATION%TYPE,
                         I_delivery_loc_type   IN       TRANS_DELIVERY.DELIVERY_LOC_TYPE%TYPE)

   return BOOLEAN IS
   L_program        VARCHAR2(50)  :=  'TRANSPORTATION_SQL.DELIVERY_EXISTS';
   L_error_message  VARCHAR2(255);
   L_exists         VARCHAR2(1);

   cursor C_TRANSDELIVERY_EXISTS is
      select 'X'
        from trans_delivery
       where transportation_id  = I_transportation_id;

   cursor C_DELIVERY_EXISTS is
      select 'X'
        from trans_delivery
       where transportation_id  = I_transportation_id
         and delivery_location  = I_delivery_location
         and delivery_loc_type  = I_delivery_loc_type;

BEGIN
   O_exists := FALSE;
   If I_delivery_location is not NULL and I_delivery_loc_type is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_DELIVERY_EXISTS','TRANS_DELIVERY',
                       'transportation_id:'||to_char(I_transportation_id));

      open C_DELIVERY_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_DELIVERY_EXISTS','TRANS_DELIVERY',
                         'transportation_id:'||to_char(I_transportation_id));

      fetch C_DELIVERY_EXISTS into L_exists;
      if C_DELIVERY_EXISTS%FOUND then
         O_exists := TRUE;
         O_error_message:=SQL_LIB.CREATE_MSG('INV_DELIVERY_LOC',NULL,NULL,NULL);
         SQL_LIB.SET_MARK('CLOSE','C_DELIVERY_EXISTS','TRANS_DELIVERY',
                          'transportation_id:'||to_char(I_transportation_id));
         close C_DELIVERY_EXISTS;
         return TRUE;
      else
         SQL_LIB.SET_MARK('CLOSE','C_DELIVERY_EXISTS','TRANS_DELIVERY',
                          'transportation_id:'||to_char(I_transportation_id));
         close C_DELIVERY_EXISTS;
      end if;
   else
      SQL_LIB.SET_MARK('OPEN','C_TRANSDELIVERY_EXISTS','TRANS_DELIVERY',
                       'transportation_id:'||to_char(I_transportation_id));
      open C_TRANSDELIVERY_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_TRANSDELIVERY_EXISTS','TRANS_DELIVERY',
                         'transportation_id:'||to_char(I_transportation_id));

      fetch C_TRANSDELIVERY_EXISTS into L_exists;
      if C_TRANSDELIVERY_EXISTS%NOTFOUND then
         O_exists := FALSE;
         O_error_message:=SQL_LIB.CREATE_MSG('INV_DELV_INFO',NULL,NULL,NULL);
         SQL_LIB.SET_MARK('CLOSE','C_TRANSDELIVERY_EXISTS','TRANS_DELIVERY',
                          'transportation_id:'||to_char(I_transportation_id));
         close C_TRANSDELIVERY_EXISTS;
         return TRUE;
      else
         O_exists := TRUE;
         SQL_LIB.SET_MARK('CLOSE','C_TRANSDELIVERY_EXISTS','TRANS_DELIVERY',
                          'transportation_id:'||to_char(I_transportation_id));
         close C_TRANSDELIVERY_EXISTS;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                                          to_char(SQLCODE));
      return FALSE;

END DELIVERY_EXISTS;
------------------------------------------------------------------------------------------------------------
FUNCTION CLAIM_EXISTS(O_error_message        IN OUT   VARCHAR2,
                      O_exists                 IN OUT   BOOLEAN,
                      I_transportation_id      IN       TRANSPORTATION.TRANSPORTATION_ID%TYPE,
                      I_claim_id               IN       TRANS_CLAIMS.CLAIM_ID%TYPE)

   return BOOLEAN IS
   L_program        VARCHAR2(50)  :=  'TRANSPORTATION_SQL.CLAIM_EXISTS';
   L_error_message  VARCHAR2(255);
   L_exists         VARCHAR2(1);

   cursor C_TRANCLAIM_EXISTS is
      select 'X'
        from trans_claims
       where transportation_id  = I_transportation_id;

   cursor C_CLAIM_EXISTS is
      select 'X'
        from trans_claims
       where transportation_id  = I_transportation_id
         and claim_id           = I_claim_id;

BEGIN
   O_exists := FALSE;
   if I_claim_id is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_CLAIM_EXISTS','TRANS_CLAIMS',
                       'transportation_id:'||to_char(I_transportation_id));

      open C_CLAIM_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_CLAIM_EXISTS','TRANS_CLAIMS',
                         'transportation_id:'||to_char(I_transportation_id));

      fetch C_CLAIM_EXISTS into L_exists;
      if C_CLAIM_EXISTS%FOUND then
         O_exists := TRUE;
         O_error_message:=SQL_LIB.CREATE_MSG('INV_CLAIM',I_claim_id,NULL,NULL);
         close C_CLAIM_EXISTS;
         return TRUE;
      else
         SQL_LIB.SET_MARK('CLOSE','C_CLAIM_EXISTS','TRANS_CLAIMS',
                          'transportation_id:'||to_char(I_transportation_id));
         close C_CLAIM_EXISTS;
      end if;
   else
      SQL_LIB.SET_MARK('OPEN','C_TRANCLAIM_EXISTS','TRANS_CLAIMS',
                       'transportation_id:'||to_char(I_transportation_id));

      open C_TRANCLAIM_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_TRANCLAIM_EXISTS','TRANS_CLAIMS',
                         'transportation_id:'||to_char(I_transportation_id));

      fetch C_TRANCLAIM_EXISTS into L_exists;
      if C_TRANCLAIM_EXISTS%NOTFOUND then
         O_exists := FALSE;
         O_error_message:=SQL_LIB.CREATE_MSG('INV_CLAIM_INFO',NULL,NULL,NULL);
         SQL_LIB.SET_MARK('CLOSE','C_TRANCLAIM_EXISTS','TRANS_CLAIMS',
                          'transportation_id:'||to_char(I_transportation_id));
         close C_TRANCLAIM_EXISTS;
         return TRUE;
      else
         O_exists := TRUE;
         SQL_LIB.SET_MARK('CLOSE','C_TRANCLAIM_EXISTS','TRANS_CLAIMS',
                          'transportation_id:'||to_char(I_transportation_id));
         close C_TRANCLAIM_EXISTS;
      end if;
    end if;

    return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                                          to_char(SQLCODE));
      return FALSE;

END CLAIM_EXISTS;
-----------------------------------------------------------------------------------------------
FUNCTION DUP_TRANS_ITEM(O_error_message         IN OUT VARCHAR2,
                       O_exists                 IN OUT BOOLEAN,
                       I_transportation_id      IN     TRANS_SKU.TRANSPORTATION_ID%TYPE,
                       I_item                   IN     TRANS_SKU.ITEM%TYPE)
    return BOOLEAN IS

      L_program        VARCHAR2(50)  :=  'TRANSPORTATION_SQL.DUP_TRANS_ITEM';
      L_error_message  VARCHAR2(255);
      L_exists         VARCHAR2(1);

   cursor C_DUP_TRANS_ITEM is
      select 'X'
        from trans_sku
       where transportation_id = I_transportation_id
         and item               = I_item;

BEGIN
   O_exists := FALSE;
   open C_DUP_TRANS_ITEM;
   fetch C_DUP_TRANS_ITEM into L_exists;
   if C_DUP_TRANS_ITEM%FOUND then
      O_exists := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_TRANS_ID',NULL,NULL,NULL);
      close C_DUP_TRANS_ITEM;
      return TRUE;
   else
      O_exists:= FALSE;
      close C_DUP_TRANS_ITEM;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DUP_TRANS_ITEM;
----------------------------------------------------------------------------------------------
FUNCTION TRANS_SKU_SEQ(O_error_message          IN OUT VARCHAR2,
                       O_seq_no                 IN OUT TRANS_SKU.SEQ_NO%TYPE,
                       I_transportation_id      IN     TRANS_SKU.TRANSPORTATION_ID%TYPE)
   return BOOLEAN IS

   L_program        VARCHAR2(50)  :=  'TRANSPORTATION_SQL.TRANS_SKU_SEQ';

   cursor C_TRANS_SKU_SEQ is
     select nvl(max(seq_no),0) + 1
       from trans_sku
      where transportation_id = I_transportation_id;

BEGIN
   open C_TRANS_SKU_SEQ;
   fetch C_TRANS_SKU_SEQ into O_seq_no;
   close C_TRANS_SKU_SEQ;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END TRANS_SKU_SEQ;
---------------------------------------------------------------------------------------
FUNCTION CONT_VVE_PO_ITEM_EXISTS(O_error_message         IN OUT VARCHAR2,
                                  O_exists                IN OUT BOOLEAN,
                                  I_container_id          IN     TRANSPORTATION.CONTAINER_ID%TYPE,
                                  I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                                  I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                  I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                  I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                                  I_item                  IN     TRANSPORTATION.ITEM%TYPE)
   return BOOLEAN is
  L_exists                VARCHAR2(1)                           := NULL;
  L_item_desc              ITEM_MASTER.ITEM_DESC%TYPE;
  L_item_level             ITEM_MASTER.ITEM_LEVEL%TYPE;
  L_tran_level             ITEM_MASTER.TRAN_LEVEL%TYPE;
  L_status                 ITEM_MASTER.STATUS%TYPE;
  L_pack_ind               ITEM_MASTER.PACK_IND%TYPE;
  L_dept                   ITEM_MASTER.DEPT%TYPE;
  L_dept_name              DEPS.DEPT_NAME%TYPE;
  L_class                  ITEM_MASTER.CLASS%TYPE;
  L_class_name             CLASS.CLASS_NAME%TYPE;
  L_subclass               ITEM_MASTER.SUBCLASS%TYPE;
  L_subclass_name          SUBCLASS.SUB_NAME%TYPE; 
  L_sellable_ind           ITEM_MASTER.SELLABLE_IND%TYPE;
  L_orderable_ind          ITEM_MASTER.ORDERABLE_IND%TYPE;
  L_pack_type              ITEM_MASTER.PACK_TYPE%TYPE;
  L_simple_pack_ind        ITEM_MASTER.SIMPLE_PACK_IND%TYPE;
  L_waste_type             ITEM_MASTER.WASTE_TYPE%TYPE;
  L_item_parent            ITEM_MASTER.ITEM_PARENT%TYPE         :=NULL;
  L_item_grandparent       ITEM_MASTER.ITEM_GRANDPARENT%TYPE    :=NULL;
  L_short_desc             ITEM_MASTER.SHORT_DESC%TYPE;
  L_waste_pct              ITEM_MASTER.WASTE_PCT%TYPE;
  L_default_waste_pct      ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;

  cursor C_CONT is
      select 'X'
        from transportation
       where container_id          = NVL(I_container_id, container_id)
         and vessel_id             = NVL(I_vessel_id, vessel_id)
         and voyage_flt_id         = NVL(I_voyage_flt_id, voyage_flt_id)
         and estimated_depart_date = NVL(I_estimated_depart_date, estimated_depart_date)
         and order_no              = NVL(I_order_no, order_no)
         and( item                 = NVL(I_item, item)
          or  item                 = L_item_parent
          or  item                 = L_item_grandparent)
         and container_id          is not NULL
         and vessel_id             is not NULL
         and voyage_flt_id         is not NULL
         and estimated_depart_date is not NULL
         and order_no              is not NULL
         and item                  is not NULL;


BEGIN
   O_exists := TRUE;
   if I_item is NULL then
      SQL_LIB.SET_MARK('OPEN','C_CONT','TRANSPORTATION',NULL);
      open C_CONT;
      SQL_LIB.SET_MARK('FETCH','C_CONT','TRANSPORTATION',NULL);
      fetch C_CONT into L_exists;
      if C_CONT%NOTFOUND then
         O_exists := FALSE;
         O_error_message := SQL_LIB.CREATE_MSG('INV_CONT_VVE_PO',NULL,NULL,NULL);
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_CONT','TRANSPORTATION',NULL);
      close C_CONT;
   else
      if ITEM_ATTRIB_SQL.GET_INFO(O_error_message,
                                  L_item_desc,
                                  L_item_level,
                                  L_tran_level,
                                  L_status,
                                  L_pack_ind,
                                  L_dept,
                                  L_dept_name,
                                  L_class,
                                  L_class_name,
                                  L_subclass,
                                  L_subclass_name,                                  
                                  L_sellable_ind,
                                  L_orderable_ind,
                                  L_pack_type,
                                  L_simple_pack_ind,
                                  L_waste_type,
                                  L_item_parent,
                                  L_item_grandparent,
                                  L_short_desc,
                                  L_waste_pct,
                                  L_default_waste_pct,
                                  I_item) = FALSE then
         return FALSE;
      end if;
      if L_tran_level = L_item_level then
         SQL_LIB.SET_MARK('OPEN','C_CONT','TRANSPORTATION',NULL);
         open C_CONT;
         SQL_LIB.SET_MARK('FETCH','C_CONT','TRANSPORTATION',NULL);
         fetch C_CONT into L_exists;
         if C_CONT%NOTFOUND then
            O_exists     := FALSE;
            O_error_message := SQL_LIB.CREATE_MSG('INV_CONT_VVE_PO_ITEM',NULL,NULL,NULL);
            close c_cont;
         else
            O_exists := TRUE;
            SQL_LIB.SET_MARK('CLOSE','C_CONT','TRANSPORTATION',NULL);
            close c_cont;
         end if;
                --- item not at tran level
      else
         O_exists     := FALSE;
         O_error_message := SQL_LIB.CREATE_MSG('INV_CONT_VVE_PO_ITEM',NULL,NULL,NULL);
      end if;
   end if;
   return TRUE;

   EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSPORTATION_SQL.CONT_VVE_PO_ITEM_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;

END CONT_VVE_PO_ITEM_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION BL_AWB_VVE_PO_ITEM_EXISTS(O_error_message         IN OUT VARCHAR2,
                                   O_exists                IN OUT BOOLEAN,
                                   I_bl_awb_id             IN     TRANSPORTATION.BL_AWB_ID%TYPE,
                                   I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                                   I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                   I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                   I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                                   I_item                  IN     TRANSPORTATION.ITEM%TYPE)
   return BOOLEAN is
  L_exists                 VARCHAR2(1)                           := NULL;
  L_item_desc              ITEM_MASTER.ITEM_DESC%TYPE;
  L_item_level             ITEM_MASTER.ITEM_LEVEL%TYPE;
  L_tran_level             ITEM_MASTER.TRAN_LEVEL%TYPE;
  L_status                 ITEM_MASTER.STATUS%TYPE;
  L_pack_ind               ITEM_MASTER.PACK_IND%TYPE;
  L_dept                   ITEM_MASTER.DEPT%TYPE;
  L_dept_name              DEPS.DEPT_NAME%TYPE;
  L_class                  ITEM_MASTER.CLASS%TYPE;
  L_class_name             CLASS.CLASS_NAME%TYPE;
  L_subclass               ITEM_MASTER.SUBCLASS%TYPE;
  L_subclass_name          SUBCLASS.SUB_NAME%TYPE;  
  L_sellable_ind           ITEM_MASTER.SELLABLE_IND%TYPE;
  L_orderable_ind          ITEM_MASTER.ORDERABLE_IND%TYPE;
  L_pack_type              ITEM_MASTER.PACK_TYPE%TYPE;
  L_simple_pack_ind        ITEM_MASTER.SIMPLE_PACK_IND%TYPE;
  L_waste_type             ITEM_MASTER.WASTE_TYPE%TYPE;
  L_item_parent            ITEM_MASTER.ITEM_PARENT%TYPE         := NULL;
  L_item_grandparent       ITEM_MASTER.ITEM_GRANDPARENT%TYPE    := NULL;
  L_short_desc             ITEM_MASTER.SHORT_DESC%TYPE;
  L_waste_pct              ITEM_MASTER.WASTE_PCT%TYPE;
  L_default_waste_pct      ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;


 cursor C_BL is
      select 'X'
        from transportation
       where bl_awb_id             = NVL(I_bl_awb_id, bl_awb_id)
         and vessel_id             = NVL(I_vessel_id, vessel_id)
         and voyage_flt_id         = NVL(I_voyage_flt_id, voyage_flt_id)
         and estimated_depart_date = NVL(I_estimated_depart_date, estimated_depart_date)
         and order_no              = NVL(I_order_no, order_no)
         and( item                 = NVL(I_item, item)
          or item                  = L_item_parent
          or item                  = L_item_grandparent)
         and bl_awb_id             is not NULL
         and vessel_id             is not NULL
         and voyage_flt_id         is not NULL
         and estimated_depart_date is not NULL
         and order_no              is not NULL
         and item                  is not NULL;


BEGIN
   O_exists := TRUE;
   if I_ITEM is NULL then
      SQL_LIB.SET_MARK('OPEN','C_BL','TRANSPORTATION',NULL);
      open C_BL;
      SQL_LIB.SET_MARK('FETCH','C_BL','TRANSPORTATION',NULL);
      fetch C_BL into L_exists;
      if C_BL%NOTFOUND then
         O_exists := FALSE;
         O_error_message := SQL_LIB.CREATE_MSG('INV_BL_AWB_VVE_PO',NULL,NULL,NULL);
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_BL','TRANSPORTATION',NULL);
      close C_BL;
   else
      if ITEM_ATTRIB_SQL.GET_INFO(O_error_message,
                                  L_item_desc,
                                  L_item_level,
                                  L_tran_level,
                                  L_status,
                                  L_pack_ind,
                                  L_dept,
                                  L_dept_name,
                                  L_class,
                                  L_class_name,
                                  L_subclass,
                                  L_subclass_name,                                  
                                  L_sellable_ind,
                                  L_orderable_ind,
                                  L_pack_type,
                                  L_simple_pack_ind,
                                  L_waste_type,
                                  L_item_parent,
                                  L_item_grandparent,
                                  L_short_desc,
                                  L_waste_pct,
                                  L_default_waste_pct,
                                  I_item) = FALSE then
         return FALSE;
      end if;
      if L_tran_level = L_item_level then
         SQL_LIB.SET_MARK('OPEN','C_BL','TRANSPORTATION',NULL);
         open C_BL;
         SQL_LIB.SET_MARK('FETCH','C_BL','TRANSPORTATION',NULL);
         fetch C_BL into L_exists;
         if C_BL%NOTFOUND then
            O_exists := FALSE;
            O_error_message := SQL_LIB.CREATE_MSG('INV_BL_AWB_VVE_PO_ITEM',NULL,NULL,NULL);
            close C_BL;
         else
            SQL_LIB.SET_MARK('CLOSE','C_BL','TRANSPORTATION',NULL);
            close C_BL;
         end if;
             ---item not at tran_level
      else
         O_exists := FALSE;
         O_error_message := SQL_LIB.CREATE_MSG('INV_BL_AWB_VVE_PO_ITEM',NULL,NULL,NULL);
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSPORTATION_SQL.BL_AWB_VVE_PO_ITEM_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;
END BL_AWB_VVE_PO_ITEM_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION VVE_PO_ITEM_EXISTS(O_error_message         IN OUT VARCHAR2,
                            O_exists                IN OUT BOOLEAN,
                            I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                            I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                            I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                            I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                            I_item                  IN     TRANSPORTATION.ITEM%TYPE)
   return BOOLEAN is
  L_exists                VARCHAR2(1)                            := NULL;
  L_item_desc              ITEM_MASTER.ITEM_DESC%TYPE;
  L_item_level             ITEM_MASTER.ITEM_LEVEL%TYPE;
  L_tran_level             ITEM_MASTER.TRAN_LEVEL%TYPE;
  L_status                 ITEM_MASTER.STATUS%TYPE;
  L_pack_ind               ITEM_MASTER.PACK_IND%TYPE;
  L_dept                   ITEM_MASTER.DEPT%TYPE;
  L_dept_name              DEPS.DEPT_NAME%TYPE;
  L_class                  ITEM_MASTER.CLASS%TYPE;
  L_class_name             CLASS.CLASS_NAME%TYPE;
  L_subclass               ITEM_MASTER.SUBCLASS%TYPE;
  L_subclass_name          SUBCLASS.SUB_NAME%TYPE;  
  L_sellable_ind           ITEM_MASTER.SELLABLE_IND%TYPE;
  L_orderable_ind          ITEM_MASTER.ORDERABLE_IND%TYPE;
  L_pack_type              ITEM_MASTER.PACK_TYPE%TYPE;
  L_simple_pack_ind        ITEM_MASTER.SIMPLE_PACK_IND%TYPE;
  L_waste_type             ITEM_MASTER.WASTE_TYPE%TYPE;
  L_item_parent            ITEM_MASTER.ITEM_PARENT%TYPE         := NULL;
  L_item_grandparent       ITEM_MASTER.ITEM_GRANDPARENT%TYPE    := NULL;
  L_short_desc             ITEM_MASTER.SHORT_DESC%TYPE;
  L_waste_pct              ITEM_MASTER.WASTE_PCT%TYPE;
  L_default_waste_pct      ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;


   cursor C_VVE_PO_ITEM is
      select 'X'
        from transportation
       where vessel_id             = NVL(I_vessel_id, vessel_id)
         and voyage_flt_id         = NVL(I_voyage_flt_id, voyage_flt_id)
         and estimated_depart_date = NVL(I_estimated_depart_date, estimated_depart_date)
         and order_no              = NVL(I_order_no, order_no)
         and(item                  = NVL(I_item, item)
          or item                  = L_item_parent
          or item                  = L_item_grandparent)
         and vessel_id             is not NULL
         and voyage_flt_id         is not NULL
         and estimated_depart_date is not NULL
         and order_no              is not NULL
         and item                  is not NULL;

BEGIN
   O_exists := TRUE;
   if I_item is NULL then
      SQL_LIB.SET_MARK('OPEN','C_VVE_PO_ITEM','TRANSPORTATION',NULL);
      open C_VVE_PO_ITEM;
      SQL_LIB.SET_MARK('FETCH','C_VVE_PO_ITEM','TRANSPORTATION',NULL);
      fetch C_VVE_PO_ITEM into L_exists;
      if C_VVE_PO_ITEM%NOTFOUND then
          O_exists := FALSE;
          O_error_message := SQL_LIB.CREATE_MSG('INV_VVE_PO',NULL,NULL,NULL);
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_VVE_PO_ITEM','TRANSPORTATION',NULL);
      close C_VVE_PO_ITEM;
   else
      if ITEM_ATTRIB_SQL.GET_INFO(O_error_message,
                                  L_item_desc,
                                  L_item_level,
                                  L_tran_level,
                                  L_status,
                                  L_pack_ind,
                                  L_dept,
                                  L_dept_name,
                                  L_class,
                                  L_class_name,
                                  L_subclass,
                                  L_subclass_name,                                  
                                  L_sellable_ind,
                                  L_orderable_ind,
                                  L_pack_type,
                                  L_simple_pack_ind,
                                  L_waste_type,
                                  L_item_parent,
                                  L_item_grandparent,
                                  L_short_desc,
                                  L_waste_pct,
                                  L_default_waste_pct,
                                  I_item) = FALSE then
          return FALSE;
       end if;
       if L_tran_level = L_item_level then
          SQL_LIB.SET_MARK('OPEN','C_VVE_PO_ITEM','TRANSPORTATION',NULL);
          open C_VVE_PO_ITEM;
          SQL_LIB.SET_MARK('FETCH','C_VVE_PO_ITEM','TRANSPORTATION',NULL);
          fetch C_VVE_PO_ITEM into L_exists;
          if C_VVE_PO_ITEM%NOTFOUND then
             O_exists := FALSE;
             O_error_message := SQL_LIB.CREATE_MSG('INV_VVE_PO_ITEM',NULL,NULL,NULL);
             close C_VVE_PO_ITEM;
          else
             SQL_LIB.SET_MARK('CLOSE','C_VVE_PO_ITEM','TRANSPORTATION',NULL);
             close C_VVE_PO_ITEM;
          end if;
             ---item not at tran level
       else
          O_exists := FALSE;
          O_error_message := SQL_LIB.CREATE_MSG('INV_VVE_PO_ITEM',NULL,NULL,NULL);
       end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'TRANSPORTATION_SQL.VVE_PO_ITEM_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;
END VVE_PO_ITEM_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION RECORD_EXISTS(O_error_message          IN OUT VARCHAR2,
                       O_exists                 IN OUT BOOLEAN,
                       I_vessel_id              IN     TRANSPORTATION.VESSEL_ID%TYPE,
                       I_voyage_flt_id          IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                       I_estimated_depart_date  IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                       I_order_no               IN     TRANSPORTATION.ORDER_NO%TYPE,
                       I_item                   IN     TRANSPORTATION.ITEM%TYPE,
                       I_container_id           IN     TRANSPORTATION.CONTAINER_ID%TYPE,
                       I_bl_awb_id              IN     TRANSPORTATION.BL_AWB_ID%TYPE,
                       I_invoice_id             IN     TRANSPORTATION.INVOICE_ID%TYPE,
                       I_row_id                 IN     ROWID)

RETURN BOOLEAN IS

   L_function   VARCHAR2(50)   := 'TRANSPORTATION_SQL.RECORD_EXISTS';
   L_exists     VARCHAR2(1);

   cursor C_EXISTS is
      select 'x'
        from transportation
       where ((vessel_id = I_vessel_id and I_vessel_id is not NULL)
              or (vessel_id is NULL and I_vessel_id is NULL))
         and ((voyage_flt_id = I_voyage_flt_id and I_voyage_flt_id is not NULL)
              or (voyage_flt_id is NULL and I_voyage_flt_id is NULL))
         and ((estimated_depart_date = I_estimated_depart_date and I_estimated_depart_date is not NULL)
              or (estimated_depart_date is NULL and I_estimated_depart_date is NULL))
         and ((order_no = I_order_no and I_order_no is not NULL)
              or (order_no is NULL and I_order_no is NULL))
         and ((item = I_item and I_item is not NULL)
              or (item is NULL and I_item is NULL))
         and ((container_id = I_container_id  and I_container_id is not NULL)
              or (container_id is NULL and I_container_id  is NULL))
         and ((bl_awb_id = I_bl_awb_id and I_bl_awb_id is not NULL)
              or (bl_awb_id is NULL and I_bl_awb_id is NULL))
         and ((invoice_id = I_invoice_id and I_invoice_id is not NULL)
              or (invoice_id is NULL and I_invoice_id is NULL))
         and (vessel_id                is not NULL
              or voyage_flt_id         is not NULL
              or estimated_depart_date is not NULL
              or order_no              is not NULL
              or item                  is not NULL
              or container_id          is not NULL
              or invoice_id            is not NULL)
         and ((rowid != I_row_id and I_row_id is not NULL) or I_row_id is NULL);
BEGIN
   O_exists := TRUE;

   SQL_LIB.SET_MARK('OPEN','C_EXISTS','TRANSPORTATION',NULL);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_EXISTS','TRANSPORTATION',NULL);
   fetch C_EXISTS into L_exists;
   if C_EXISTS%NOTFOUND then
      O_exists := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','TRANSPORTATION',NULL);
   close C_EXISTS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END RECORD_EXISTS;
--------------------------------------------------------------------------------
FUNCTION TRANSKU_EXISTS(O_error_message     IN OUT VARCHAR2,
                        O_exists            IN OUT BOOLEAN,
                        I_transportation_id IN TRANSPORTATION.TRANSPORTATION_ID%TYPE)
return BOOLEAN IS
   L_program        VARCHAR2(50)  :=  'TRANSPORTATION_SQL.TRANSKU_EXISTS';
   L_exists         VARCHAR2(1);

   cursor C_TRANSKU_EXISTS is
      select 'X'
        from trans_sku
       where transportation_id  = I_transportation_id;

BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_TRANSKU_EXISTS','TRANS_SKU',
                    'transportation_id:'||to_char(I_transportation_id));
   open C_TRANSKU_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_TRANSKU_EXISTS','TRANS_SKU',
                    'transportation_id:'||to_char(I_transportation_id));
   fetch C_TRANSKU_EXISTS into L_exists;
   if C_TRANSKU_EXISTS%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_TRANSKU_EXISTS','TRANS_SKU',
                    'transportation_id:'||to_char(I_transportation_id));
   close C_TRANSKU_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                                          to_char(SQLCODE));
      return FALSE;
END TRANSKU_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_QTYS(O_error_message           IN OUT   VARCHAR2,
                  O_carton_qty              IN OUT   TRANSPORTATION.CARTON_QTY%TYPE,
                  IO_carton_qty_uom         IN OUT   TRANSPORTATION.CARTON_UOM%TYPE,
                  O_item_qty                IN OUT   TRANSPORTATION.ITEM_QTY%TYPE,
                  IO_item_qty_uom           IN OUT   TRANSPORTATION.ITEM_QTY_UOM%TYPE,
                  O_gross_wt                IN OUT   TRANSPORTATION.GROSS_WT%TYPE,
                  IO_gross_wt_uom           IN OUT   TRANSPORTATION.GROSS_WT_UOM%TYPE,
                  O_net_wt                  IN OUT   TRANSPORTATION.NET_WT%TYPE,
                  IO_net_wt_uom             IN OUT   TRANSPORTATION.NET_WT_UOM%TYPE,
                  O_cubic                   IN OUT   TRANSPORTATION.CUBIC%TYPE,
                  IO_cubic_uom              IN OUT   TRANSPORTATION.CUBIC_UOM%TYPE,
                  O_invoice_amt             IN OUT   TRANSPORTATION.INVOICE_AMT%TYPE,
                  I_supplier                IN       SUPS.SUPPLIER%TYPE,
                  I_origin_country_id       IN       COUNTRY.COUNTRY_ID%TYPE,
                  I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                  I_voyage_id               IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                  I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                  I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE,
                  I_item                    IN       TRANSPORTATION.ITEM%TYPE,
                  I_container_id            IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                  I_bl_awb_id               IN       TRANSPORTATION.BL_AWB_ID%TYPE,
                  I_invoice_id              IN       TRANSPORTATION.INVOICE_ID%TYPE,
                  I_currency_code           IN       TRANSPORTATION.CURRENCY_CODE%TYPE,
                  I_exchange_rate           IN       TRANSPORTATION.EXCHANGE_RATE%TYPE,
                  I_calling_function        IN       VARCHAR2 DEFAULT NULL)

   RETURN BOOLEAN IS
   L_program           VARCHAR2(64)                      := 'TRANSPORTATION_SQL.GET_QTYS';
   L_carton_qty        TRANSPORTATION.CARTON_QTY%TYPE    := 0;
   L_carton_uom        UOM_CLASS.UOM%TYPE;
   L_item_qty          TRANSPORTATION.ITEM_QTY%TYPE      := 0;
   L_item_qty_uom      UOM_CLASS.UOM%TYPE;
   L_gross_wt          TRANSPORTATION.GROSS_WT%TYPE      := 0;
   L_gross_wt_uom      UOM_CLASS.UOM%TYPE;
   L_net_wt            TRANSPORTATION.NET_WT%TYPE        := 0;
   L_net_wt_uom        UOM_CLASS.UOM%TYPE;
   L_cubic             TRANSPORTATION.CUBIC%TYPE         := 0;
   L_cubic_uom         UOM_CLASS.UOM%TYPE;
   L_invoice_amt       TRANSPORTATION.INVOICE_AMT%TYPE   := 0;
   L_out_currency_code TRANSPORTATION.CURRENCY_CODE%TYPE;

   cursor C_ALL is
      select nvl(carton_qty,0) carton_qty,
             carton_uom,
             nvl(item_qty,0) item_qty,
             item_qty_uom,
             nvl(gross_wt,0) gross_wt,
             gross_wt_uom,
             nvl(net_wt,0) net_wt,
             net_wt_uom,
             nvl(cubic,0) cubic,
             cubic_uom,
             nvl(invoice_amt,0) invoice_amt,
             currency_code,
             exchange_rate
        from transportation
       where vessel_id             = I_vessel_id
         and voyage_flt_id         = I_voyage_id
         and estimated_depart_date = I_estimated_depart_date
         and order_no              = I_order_no
         and item                  = I_item
         and ((container_id          = I_container_id)
              or (container_id is NULL and I_container_id is NULL))
         and ((bl_awb_id             = I_bl_awb_id)
              or (bl_awb_id is NULL and I_bl_awb_id is NULL))
         and ((invoice_id            = I_invoice_id)
              or (invoice_id is NULL and I_invoice_id is NULL));

BEGIN
   O_carton_qty  := 0;
   O_item_qty    := 0;
   O_gross_wt    := 0;
   O_net_wt      := 0;
   O_cubic       := 0;
   O_invoice_amt := 0;
   ---
   ---Default UOM if NULL
   if IO_carton_qty_uom is NULL then
      L_carton_uom := 'CT';
   else
      L_carton_uom := IO_carton_qty_uom;
   end if;
   ---
   if IO_item_qty_uom is NULL then
      L_item_qty_uom := 'EA';
   else
      L_item_qty_uom := IO_item_qty_uom;
   end if;
   ---
   if IO_gross_wt_uom is NULL then
      L_gross_wt_uom := 'LBS';
   else
      L_gross_wt_uom := IO_gross_wt_uom;
   end if;
   ---
   if IO_net_wt_uom is NULL then
      L_net_wt_uom := 'LBS';
   else
      L_net_wt_uom := IO_net_wt_uom;
   end if;
   ---
   if IO_cubic_uom is NULL then
      L_cubic_uom := 'CFT';
   else
      L_cubic_uom := IO_cubic_uom;
   end if;
   ---
   FOR C1 in C_ALL LOOP
      --Convert carton quantity--
      if C1.carton_qty <> 0 then
         if UOM_SQL.CONVERT(O_error_message,
                            L_carton_qty,
                            L_carton_uom,
                            C1.carton_qty,
                            C1.carton_uom,
                            I_item,
                            I_supplier,
                            I_origin_country_id) = FALSE then
            return FALSE;
         end if;
      end if;
      --Convert item quantity--
      if C1.item_qty <> 0 then
         if UOM_SQL.CONVERT(O_error_message,
                            L_item_qty,
                            L_item_qty_uom,
                            C1.item_qty,
                            C1.item_qty_uom,
                            I_item,
                            I_supplier,
                            I_origin_country_id,
                            I_calling_function) = FALSE then
            return FALSE;
         end if;
      end if;
      --Convert gross weight--
      if C1.gross_wt <> 0 then

         if UOM_SQL.CONVERT(O_error_message,
                            L_gross_wt,
                            L_gross_wt_uom,
                            C1.gross_wt,
                            C1.gross_wt_uom,
                            I_item,
                            I_supplier,
                            I_origin_country_id) = FALSE then
            return FALSE;
         end if;
      end if;
      --Convert net weight--
      if C1.net_wt <> 0 then
         if UOM_SQL.CONVERT(O_error_message,
                            L_net_wt,
                            L_net_wt_uom,
                            C1.net_wt,
                            C1.net_wt_uom,
                            I_item,
                            I_supplier,
                            I_origin_country_id) = FALSE then
            return FALSE;
         end if;
      end if;
      --Convert cubic--
      if C1.cubic <> 0 then
         if UOM_SQL.CONVERT(O_error_message,
                            L_cubic,
                            L_cubic_uom,
                            C1.cubic,
                            C1.cubic_uom,
                            I_item,
                            I_supplier,
                            I_origin_country_id) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      ---Convert Currency--
      if C1.invoice_amt <> 0 then
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 C1.invoice_amt,
                                 C1.currency_code,
                                 I_currency_code,
                                 L_invoice_amt,
                                 'N',
                                 NULL,
                                 NULL,
                                 C1.exchange_rate,
                                 I_exchange_rate) = FALSE then
            return FALSE;
         end if;
      else
         L_invoice_amt := 0;
      end if;
      ---
      ---Sum values---
      O_carton_qty  := O_carton_qty  + L_carton_qty;
      O_item_qty    := O_item_qty    + L_item_qty;
      O_gross_wt    := O_gross_wt    + L_gross_wt;
      O_net_wt      := O_net_wt      + L_net_wt;
      O_cubic       := O_cubic       + L_cubic;
      O_invoice_amt := O_invoice_amt + L_invoice_amt;

      --Reset variables--
      L_carton_qty  := 0;
      L_item_qty    := 0;
      L_gross_wt    := 0;
      L_net_wt      := 0;
      L_cubic       := 0;
      L_invoice_amt := 0;
      ---
   END LOOP;
   ---
   ---Assign UOM's---
   IO_carton_qty_uom := L_carton_uom;
   IO_item_qty_uom   := L_item_qty_uom;
   IO_gross_wt_uom   := L_gross_wt_uom;
   IO_net_wt_uom     := L_net_wt_uom;
   IO_cubic_uom      := L_cubic_uom;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_QTYS;
--------------------------------------------------------------------------------
FUNCTION GET_SHIP_QTY(O_error_message         IN OUT  VARCHAR2,
                      O_ship_qty              IN OUT  TRANSPORTATION.ITEM_QTY%TYPE,
                      I_supplier              IN      SUPS.SUPPLIER%TYPE,
                      I_origin_country_id     IN      COUNTRY.COUNTRY_ID%TYPE,
                      I_uom                   IN      UOM_CLASS.UOM%TYPE,
                      I_vessel_id             IN      TRANSPORTATION.VESSEL_ID%TYPE,
                      I_voyage_flt_id         IN      TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                      I_estimated_depart_date IN      TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                      I_order_no              IN      TRANSPORTATION.ORDER_NO%TYPE,
                      I_item                  IN      TRANSPORTATION.ITEM%TYPE,
                      I_pack_item             IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_get_class_ind          VARCHAR2(4);
   L_program                VARCHAR2(50)                            := 'TRANSPORTATION_SQL.GET_SHIP_QTY';
   L_tran_exists            VARCHAR2(1)                             := 'N';
   L_carton_qty             TRANSPORTATION.CARTON_QTY%TYPE          := 0;
   L_temp_qty               TRANSPORTATION.ITEM_QTY%TYPE            := 0;
   L_item_qty               TRANSPORTATION.ITEM_QTY%TYPE            := 0;
   L_gross_wt               TRANSPORTATION.GROSS_WT%TYPE            := 0;
   L_net_wt                 TRANSPORTATION.NET_WT%TYPE              := 0;
   L_cubic                  TRANSPORTATION.CUBIC%TYPE               := 0;
   L_invoice_amt            TRANSPORTATION.INVOICE_AMT%TYPE         := 0;
   L_uom                    UOM_CLASS.UOM%TYPE                      := I_uom;
   L_parent_uom             UOM_CLASS.UOM%TYPE;
   L_standard_class         UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor            UOM_CONVERSION.FACTOR%TYPE;
   L_packsku_qty            TRANSPORTATION.ITEM_QTY%TYPE;
   L_transku_qty            TRANS_SKU.QUANTITY%TYPE;
   L_transku_uom            TRANS_SKU.QUANTITY_UOM%TYPE;
   L_parent_order_qty       ORDLOC.QTY_ORDERED%TYPE;
   L_parent_qty             ORDLOC.QTY_ORDERED%TYPE;
   L_item_order_qty         ORDLOC.QTY_ORDERED%TYPE;
   L_item_desc              ITEM_MASTER.ITEM_DESC%TYPE;
   L_item_level             ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level             ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_status                 ITEM_MASTER.STATUS%TYPE;
   L_pack_ind               ITEM_MASTER.PACK_IND%TYPE;
   L_dept                   ITEM_MASTER.DEPT%TYPE;
   L_dept_name              DEPS.DEPT_NAME%TYPE;
   L_class                  ITEM_MASTER.CLASS%TYPE;
   L_class_name             CLASS.CLASS_NAME%TYPE;
   L_subclass               ITEM_MASTER.SUBCLASS%TYPE;
   L_subclass_name          SUBCLASS.SUB_NAME%TYPE;  
   L_sellable_ind           ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind          ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type              ITEM_MASTER.PACK_TYPE%TYPE;
   L_simple_pack_ind        ITEM_MASTER.SIMPLE_PACK_IND%TYPE;
   L_waste_type             ITEM_MASTER.WASTE_TYPE%TYPE;
   L_item_parent            ITEM_MASTER.ITEM_PARENT%TYPE            := NULL;
   L_item_grandparent       ITEM_MASTER.ITEM_GRANDPARENT%TYPE       := NULL;
   L_short_desc             ITEM_MASTER.SHORT_DESC%TYPE;
   L_waste_pct              ITEM_MASTER.WASTE_PCT%TYPE;
   L_default_waste_pct      ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;
   L_item                   ITEM_MASTER.ITEM%TYPE                   := NULL ;

   ---
   cursor C_GET_CO_BL_INVC(L_item ITEM_MASTER.ITEM%TYPE) is
      select distinct container_id,
             bl_awb_id,
             invoice_id
        from transportation
       where vessel_id             = I_vessel_id
         and voyage_flt_id         = I_voyage_flt_id
         and estimated_depart_date = I_estimated_depart_date
         and order_no              = I_order_no
         and (item                 = L_item
          or item                  = L_item_parent
          or item                  = L_item_grandparent);

   cursor C_GET_PACKSKU_QTY is
        select qty
          from v_packsku_qty
         where pack_no  = I_pack_item
           and item     = I_item;

   cursor C_CHECK_TRAN is
      select 'Y'
        from transportation
       where vessel_id             = I_vessel_id
         and voyage_flt_id         = I_voyage_flt_id
         and estimated_depart_date = I_estimated_depart_date
         and order_no              = I_order_no
         and item                  = I_item;

   cursor C_GET_TRANSKU_QTY is
      select SUM(quantity),
             quantity_uom
        from trans_sku
       where item = L_item
         and transportation_id in (select transportation_id
                                     from transportation
                                    where vessel_id             = I_vessel_id
                                      and voyage_flt_id         = I_voyage_flt_id
                                      and estimated_depart_date = I_estimated_depart_date
                                      and order_no              = I_order_no
                                      and item                  = NVL(L_item_grandparent,L_item_parent))

         group by quantity_uom;

   cursor C_PARENT_QTY is
      select NVL(SUM(ol.qty_ordered),0)
        from ordloc ol,
             ordsku os
       where ol.order_no = I_order_no
         and os.order_no = ol.order_no
         and ol.item     = os.item
         and os.item in (select item
                          from item_master
                         where item_parent = NVL(L_item_grandparent,L_item_parent));

   cursor C_ITEM_ORDER_QTY is
      select NVL(ol.qty_ordered,0)
        from ordloc ol,
             ordsku os
       where ol.order_no = I_order_no
         and os.order_no = ol.order_no
         and ol.item     = os.item
         and os.item     = I_item;

BEGIN
   O_ship_qty := 0;
   ---
   if I_pack_item is not NULL then
      L_item := I_pack_item;
      L_uom  := 'EA';
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_PACKSKU_QTY','V_PACKSKU_QTY','pack: '||I_pack_item||', item: '||I_item);
      open C_GET_PACKSKU_QTY;
      SQL_LIB.SET_MARK('FETCH','C_GET_PACKSKU_QTY','V_PACKSKU_QTY','pack: '||I_pack_item||', item: '||I_item);
      fetch C_GET_PACKSKU_QTY into L_packsku_qty;
      SQL_LIB.SET_MARK('CLOSE','C_GET_PACKSKU_QTY','V_PACKSKU_QTY','pack: '||I_pack_item||', item: '||I_item);
      close C_GET_PACKSKU_QTY;
   else
      L_item := I_item;
     ---
      if ITEM_ATTRIB_SQL.GET_INFO(O_error_message,
                                        L_item_desc,
                                        L_item_level,
                                        L_tran_level,
                                        L_status,
                                        L_pack_ind,
                                        L_dept,
                                        L_dept_name,
                                        L_class,
                                        L_class_name,
                                        L_subclass,
                                        L_subclass_name,                                        
                                        L_sellable_ind,
                                        L_orderable_ind,
                                        L_pack_type,
                                        L_simple_pack_ind,
                                        L_waste_type,
                                        L_item_parent,
                                        L_item_grandparent,
                                        L_short_desc,
                                        L_waste_pct,
                                        L_default_waste_pct,
                                        L_item) = FALSE then
         return FALSE;
      end if;
      ---
      if L_tran_level = L_item_level then
         SQL_LIB.SET_MARK('OPEN','C_CHECK_TRAN','TRANSPORTATION','Item: '||I_item);
         open C_CHECK_TRAN;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_TRAN','TRANSPORTATION','Item: '||I_item);
         fetch C_CHECK_TRAN into L_tran_exists;
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_TRAN','TRANSPORTATION','Item: '||I_item);
         close C_CHECK_TRAN;
         ---
         if L_tran_exists = 'N' and (L_item_parent is not NULL or L_item_grandparent is not NULL) then
            ---
            SQL_LIB.SET_MARK('OPEN','C_GET_TRANSKU_QTY','TRANS_SKU','item: '||I_item);
            open C_GET_TRANSKU_QTY;
            SQL_LIB.SET_MARK('FETCH','C_GET_TRANSKU_QTY','TRANS_SKU','item: '||I_item);
            fetch C_GET_TRANSKU_QTY into L_transku_qty,L_transku_uom;
            SQL_LIB.SET_MARK('CLOSE','C_GET_TRANSKU_QTY','TRANS_SKU','item: '||I_item);
            close C_GET_TRANSKU_QTY;
            ---
            if L_transku_qty != 0 then
               if UOM_SQL.CONVERT(O_error_message,
                                  L_transku_qty,
                                  I_uom,
                                  L_transku_qty,
                                  L_transku_uom,
                                  I_item,
                                  I_supplier,
                                  I_origin_country_id) = FALSE then
                  return FALSE;
               end if;
               ---
               O_ship_qty := L_transku_qty;
               return TRUE;
            else
               ---L_transku_qty is Zero
               FOR C_rec in C_GET_CO_BL_INVC(NVL(l_Item_grandparent, L_item_parent))LOOP
                  if TRANSPORTATION_SQL.GET_QTYS(O_error_message,
                                                 L_carton_qty,
                                                 L_uom,
                                                 L_item_qty,
                                                 L_uom,
                                                 L_gross_wt,
                                                 L_uom,
                                                 L_net_wt,
                                                 L_uom,
                                                 L_cubic,
                                                 L_uom,
                                                 L_invoice_amt,
                                                 I_supplier,
                                                 I_origin_country_id,
                                                 I_vessel_id,
                                                 I_voyage_flt_id,
                                                 I_estimated_depart_date,
                                                 I_order_no,
                                                 nvl(L_item_grandparent,L_item_parent),
                                                 C_rec.container_id,
                                                 C_rec.bl_awb_id,
                                                 C_rec.invoice_id,
                                                 NULL,
                                                 NULL) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  if L_item_qty      > 0 then
                     L_temp_qty := L_item_qty;
                  elsif L_carton_qty > 0 then
                     L_temp_qty := L_carton_qty;
                  elsif L_gross_wt   > 0 then
                     L_temp_qty := L_gross_wt;
                  elsif L_cubic      > 0 then
                     L_temp_qty := L_cubic;
                  elsif L_net_wt     > 0 then
                     L_temp_qty := L_net_wt;
                  else
                     L_temp_qty := 0;
                  end if;

                  O_ship_qty := O_ship_qty + L_temp_qty;
               end loop;

               --- total quantity for all tansaction level items on the order
               SQL_LIB.SET_MARK('OPEN','C_PARENT_QTY','ORDLOC, ORDSKU',NULL);
               open C_PARENT_QTY;
               SQL_LIB.SET_MARK('FETCH','C_PARENT_QTY','ORDLOC, ORDSKU',NULL);
               fetch C_PARENT_QTY into L_parent_qty;
               SQL_LIB.SET_MARK('OPEN','C_PARENT_QTY','ORDLOC, ORDSKU',NULL);
               close C_PARENT_QTY;

               if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                                   L_parent_uom,
                                                   L_standard_class,
                                                   L_conv_factor,
                                                   NVL(L_item_grandparent,L_item_parent),
                                                   L_get_class_ind) = FALSE then
                  return FALSE;
               end if;
                  if UOM_SQL.CONVERT(O_error_message,
                                  L_parent_qty,
                                  I_uom,
                                  L_parent_qty,
                                  L_parent_uom,
                                  L_item,
                                  I_supplier,
                                  I_origin_country_id) = FALSE then

                      return FALSE;
                  end if;

               SQL_LIB.SET_MARK('OPEN','C_ITEM_ORDER_QTY','ORDLOC, ORDSKU',NULL);
               open C_ITEM_ORDER_QTY;
               SQL_LIB.SET_MARK('FETCH','C_ITEM_ORDER_QTY','ORDLOC, ORDSKU',NULL);
               fetch C_ITEM_ORDER_QTY into L_item_order_qty;
               SQL_LIB.SET_MARK('OPEN','C_ITEM_ORDER_QTY','ORDLOC, ORDSKU',NULL);
               close C_ITEM_ORDER_QTY;
               ---
               if L_parent_qty <> 0 then
                  O_ship_qty := O_ship_qty * (L_item_order_qty / L_parent_qty);
               else
                  O_ship_qty := O_ship_qty * L_item_order_qty;
               end if;
               return TRUE;
            end if; --- L_transku_qty
         end if; --- item_parent info does exists
      end if; --- item is at transaction level
  end if;
    ---
   FOR C_rec in C_GET_CO_BL_INVC(L_item) LOOP
      if TRANSPORTATION_SQL.GET_QTYS(O_error_message,
                                     L_carton_qty,
                                     L_uom,
                                     L_item_qty,
                                     L_uom,
                                     L_gross_wt,
                                     L_uom,
                                     L_net_wt,
                                     L_uom,
                                     L_cubic,
                                     L_uom,
                                     L_invoice_amt,
                                     I_supplier,
                                     I_origin_country_id,
                                     I_vessel_id,
                                     I_voyage_flt_id,
                                     I_estimated_depart_date,
                                     I_order_no,
                                     L_item,
                                     C_rec.container_id,
                                     C_rec.bl_awb_id,
                                     C_rec.invoice_id,
                                     NULL,
                                     NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if L_item_qty      > 0 then
         L_temp_qty := L_item_qty;
      elsif L_carton_qty > 0 then
         L_temp_qty := L_carton_qty;
      elsif L_gross_wt   > 0 then
         L_temp_qty := L_gross_wt;
      elsif L_cubic      > 0 then
         L_temp_qty := L_cubic;
      elsif L_net_wt     > 0 then
         L_temp_qty := L_net_wt;
      else
         L_temp_qty := 0;
      end if;
      ---
      O_ship_qty := O_ship_qty + L_temp_qty;
   END LOOP;
   ---
   if I_pack_item is not NULL then
      O_ship_qty := O_ship_qty * L_packsku_qty;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_SHIP_QTY;
--------------------------------------------------------------------------------
FUNCTION TRAN_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_partial_view  IN OUT VARCHAR2,
                          I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN is

      L_program        VARCHAR2(50)  :=  'TRANSPORTATION_SQL.TRAN_FILTER_LIST';

      cursor C_TRA is
      select count(item) table_count
        from transportation tra
       where tra.order_no = I_order_no;

      cursor C_VTRA is
      select count(item) view_count
        from v_transportation vtra
       where vtra.order_no = I_order_no;

      L_tra_ct        NUMBER(10);
      L_vtra_ct       NUMBER(10);
      /*START_RMS11_INS_SIR23922*/
      c_tra_row       C_TRA%ROWTYPE;
      c_vtra_row      C_VTRA%ROWTYPE;
      /*END_RMS11_INS_SIR23922*/

BEGIN

   if I_order_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','order no',L_program,NULL);
      return FALSE;
   end if;

   open C_TRA;
   fetch C_TRA into c_tra_row;
   close C_TRA;

   open C_VTRA;
   fetch C_VTRA into c_vtra_row;
   close C_VTRA;

   if (c_tra_row.table_count = c_vtra_row.view_count) then
      O_partial_view := 'N';
   else
      O_partial_view := 'Y';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END TRAN_FILTER_LIST;
-----------------------------------------------------------------------------------------------------
FUNCTION APPROVED_HTS_EXISTS(O_error_message       IN OUT RTK_ERRORS.RTK_KEY%TYPE,
                             O_hts_details_missing IN OUT BOOLEAN,
                             I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                             I_item                IN     ITEM_MASTER.ITEM%TYPE,
                             I_transpo_id          IN     TRANS_SKU.TRANSPORTATION_ID%TYPE)
   return BOOLEAN is

   cursor C_HTS is
      select items.item
        from (select iem1.item
                from item_master iem1
               where iem1.item = I_item
           union all
              select iem2.item
                from item_master iem2
               where iem2.item_parent = I_item
           union all
              select iem3.item
                from item_master iem3
               where iem3.item_grandparent = I_item
           union all
              select pks.item
                from packitem_breakout pks
               where pks.pack_no = I_item
           intersect
            ( select os.item
                from ordsku os, item_master iem4
               where os.order_no = I_order_no
                 and iem4.item = I_item
                 and os.item = iem4.item
                 and NVL(iem4.pack_type,'V') = 'V'
               UNION
              select pb.item
                from ordsku os, item_master iem4, packitem_breakout pb
               where os.order_no = I_order_no
                 and iem4.item = I_item
                 and os.item = iem4.item
                 and NVL(iem4.pack_type,'V') = 'B'
                 and pb.pack_no = iem4.item )
             ) items
       where not exists (select 1
                           from ordsku_hts orh
                          where orh.order_no = I_order_no
                            and orh.item = items.item
                            and orh.status = 'A'
                            and rownum = 1)
         and rownum = 1;

   cursor C_HTS_TRANS_SKU is
      select items.item
        from (select iem1.item
                from item_master iem1
               where iem1.item = I_item
           union all
              select iem2.item
                from item_master iem2
               where iem2.item_parent = I_item
           union all
              select iem3.item
                from item_master iem3
               where iem3.item_grandparent = I_item
           union all
              select pks.item
                from packitem_breakout pks
               where pks.pack_no = I_item
           intersect
              select ts.item
                from trans_sku ts
               where ts.transportation_id = I_transpo_id) items
       where not exists (select 1
                           from ordsku_hts orh
                          where orh.order_no = I_order_no
                            and orh.item = items.item
                            and orh.status = 'A'
                            and rownum = 1)
         and rownum = 1;

   cursor C_TRANS_SKU_EXIST is
      select 'x'
        from trans_sku ts
       where ts.transportation_id = I_transpo_id
         and rownum = 1;

   L_program VARCHAR2(50) := 'TRANSPORTATION_SQL.APPROVED_HTS_EXISTS';
   L_item    ITEM_MASTER.ITEM%TYPE;
   L_dummy   VARCHAR2(1);

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_TRANS_SKU_EXIST;',
                    'trans_sku',
                    'transportation_id: '|| I_transpo_id);
   open C_TRANS_SKU_EXIST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_TRANS_SKU_EXIST;',
                    'trans_sku',
                    'transportation_id: '|| I_transpo_id);
   fetch C_TRANS_SKU_EXIST into L_dummy;

   -- if there are no trans_sku records, check all items associated with PO
   -- else check only those that are in the trans_sku table
   if C_TRANS_SKU_EXIST%NOTFOUND then
      SQL_LIB.SET_MARK('OPEN',
                       'C_HTS;',
                       'item_master, packitem_breakout, ordsku, ordsku_hts',
                       'item: '|| I_item || ', order_no: ' || I_order_no);
      open C_HTS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_HTS;',
                       'item_master, packitem_breakout, ordsku, ordsku_hts',
                       'item: '|| I_item || ', order_no: ' || I_order_no);
      fetch C_HTS into L_item;

      /* if the cursor finds a row, details are missing */
      O_hts_details_missing := C_HTS%FOUND;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_HTS;',
                       'item_master, packitem_breakout, ordsku, ordsku_hts',
                       'item: '|| I_item || ', order_no: ' || I_order_no);
      close C_HTS;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_HTS_TRANS_SKU;',
                       'item_master, packitem_breakout, trans_sku, ordsku_hts',
                       'item: '|| I_item || ', transportation_id: ' || I_transpo_id);
      open C_HTS_TRANS_SKU;

      SQL_LIB.SET_MARK('FETCH',
                       'C_HTS_TRANS_SKU;',
                       'item_master, packitem_breakout, trans_sku, ordsku_hts',
                       'item: '|| I_item || ', transportation_id: ' || I_transpo_id);
      fetch C_HTS_TRANS_SKU into L_item;

      /* if the cursor finds a row, details are missing */
      O_hts_details_missing := C_HTS_TRANS_SKU%FOUND;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_HTS_TRANS_SKU;',
                       'item_master, packitem_breakout, trans_sku, ordsku_hts',
                       'item: '|| I_item || ', transportation_id: ' || I_transpo_id);
      close C_HTS_TRANS_SKU;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_TRANS_SKU_EXIST;',
                    'trans_sku',
                    'transportation_id: '||I_transpo_id);
   close C_TRANS_SKU_EXIST;

   return TRUE;

   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END APPROVED_HTS_EXISTS;
-----------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_PO (O_error_message       IN OUT RTK_ERRORS.RTK_KEY%TYPE,
                        I_item                IN     ITEM_MASTER.ITEM%TYPE,
                        I_parent_item         IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                        I_order               IN     ORDLOC.ORDER_NO%TYPE,
                        I_transportation_id   IN     TRANS_SKU.TRANSPORTATION_ID%TYPE,
                        I_diff_1              IN     ITEM_MASTER.DIFF_1%TYPE,
                        I_diff_2              IN     ITEM_MASTER.DIFF_2%TYPE,
                        I_diff_3              IN     ITEM_MASTER.DIFF_3%TYPE,
                        I_diff_4              IN     ITEM_MASTER.DIFF_4%TYPE,
                        O_exists              OUT    BOOLEAN)

return BOOLEAN is

cursor C_CHECK_ITEM is
select distinct  im.item
  from item_master im,ordloc ol
 where (im.item_parent = I_parent_item or im.item_grandparent = I_parent_item)
   and im.item = I_item
   and im.item = ol.item
   and ol.order_no = I_order
   and im.item not in (select item
                       from trans_sku
                        where transportation_id = I_transportation_id)
   and ((im.diff_1 = NVL(I_Diff_1, im.diff_1) or im.diff_1 is null)
   and  (im.diff_2 = NVL(I_Diff_2, im.diff_2) or im.diff_2 is null)
   and  (im.diff_3 = NVL(I_Diff_3, im.diff_3) or im.diff_3 is null)
   and  (im.diff_4 = NVL(I_Diff_4, im.diff_4) or im.diff_4 is null));

   L_program VARCHAR2(50) := 'TRANSPORTATION_SQL.CHECK_ITEM_PO';
   L_item    ITEM_MASTER.ITEM%TYPE;

BEGIN

   O_exists := FALSE;

   if I_item is null then
      O_error_message:= sql_lib.create_msg('REQUIRED_INPUT_IS_NULL', 'I_item',L_program, NULL);
      return FALSE;
   end if;

   if I_order  is null then
      O_error_message:= sql_lib.create_msg('REQUIRED_INPUT_IS_NULL', 'I_order',L_program, NULL);
      return FALSE;
   end if;

   if I_parent_item is null then
      O_error_message:= sql_lib.create_msg('REQUIRED_INPUT_IS_NULL', 'I_parent_item',L_program, NULL);
      return FALSE;
   end if;

   if I_transportation_id is null then
      O_error_message:= sql_lib.create_msg('REQUIRED_INPUT_IS_NULL', 'I_transportation_id',L_program, NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_ITEM','item_master, ord_loc','NULL');
   open C_CHECK_ITEM;

   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ITEM','item_master, ord_loc','NULL');
   fetch C_CHECK_ITEM into L_item;
   if C_CHECK_ITEM%FOUND then

      O_exists := TRUE;

          SQL_LIB.SET_MARK('CLOSE','C_CHECK_ITEM', 'item_master, ord_loc','NULL');
          close C_CHECK_ITEM;
          return TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CHECK_ITEM', 'item_master, ord_loc','NULL');
   close C_CHECK_ITEM;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END CHECK_ITEM_PO;
-----------------------------------------------------------------------------------------------------
FUNCTION CHECK_RECEIPT_EXISTS(O_error_message           IN OUT   VARCHAR2,
                              O_exists                  IN OUT   BOOLEAN,
                              I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE,
                              I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                              I_voyage_flt_id           IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                              I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                              I_container_id            IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                              I_bl_awb_id               IN       TRANSPORTATION.BL_AWB_ID%TYPE,
                              I_item                    IN       TRANSPORTATION.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64)              := 'TRANSPORTATION_SQL.CHECK_RECEIPT_EXISTS';
   L_receipt_exists    VARCHAR2(1);

   cursor C_CHECK_RECEIPT is
      select 'Y'
        from shipment s,
             shipsku k,
             item_master i,
             transportation t
       where s.order_no              = I_order_no
         and t.order_no              = s.order_no
         and k.qty_received          > 0
         and k.shipment              = s.shipment
         and k.item                  = i.item
         and (i.item                 = t.item
              or i.item_parent       = t.item
              or i.item_grandparent  = t.item)
         and t.vessel_id             = I_vessel_id
         and t.voyage_flt_id         = I_voyage_flt_id
         and t.estimated_depart_date = I_estimated_depart_date
         and (I_container_id is null or t.container_id = I_container_id)
         and (I_bl_awb_id is null or t.bl_awb_id = I_bl_awb_id)
         and (I_item is null or t.item = I_item);
BEGIN

   O_exists := FALSE;

   if I_order_no is null then
      O_error_message:= sql_lib.create_msg('REQUIRED_INPUT_IS_NULL', 'I_order_no',L_program, NULL);
      return FALSE;
   end if;

   if I_vessel_id is null then
      O_error_message:= sql_lib.create_msg('REQUIRED_INPUT_IS_NULL', 'I_vessel_id',L_program, NULL);
      return FALSE;
   end if;

   if I_voyage_flt_id is null then
      O_error_message:= sql_lib.create_msg('REQUIRED_INPUT_IS_NULL', 'I_voyage_flt_id',L_program, NULL);
      return FALSE;
   end if;

   if I_estimated_depart_date is null then
      O_error_message:= sql_lib.create_msg('REQUIRED_INPUT_IS_NULL', 'I_estimated_depart_date',L_program, NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_RECEIPT','TRANSPORTATION,SHIPMENT','NULL');
   open C_CHECK_RECEIPT;

   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_RECEIPT','TRANSPORTATION,SHIPMENT','NULL');
   fetch C_CHECK_RECEIPT into L_receipt_exists;

   O_exists := C_CHECK_RECEIPT%FOUND;

   SQL_LIB.SET_MARK('CLOSE','C_CHECK_RECEIPT', 'TRANSPORTATION,SHIPMENT','NULL');
   close C_CHECK_RECEIPT;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END CHECK_RECEIPT_EXISTS;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_RECEIVED_QTYS(O_error_message   IN OUT   VARCHAR2,
                           O_item_qty        IN OUT   TRANSPORTATION.ITEM_QTY%TYPE,
                           I_order_no        IN       TRANSPORTATION.ORDER_NO%TYPE,
                           I_item            IN       TRANSPORTATION.ITEM%TYPE,
                           I_container_id    IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                           I_bl_awb_id       IN       TRANSPORTATION.BL_AWB_ID%TYPE,
                           I_uom             IN       UOM_CLASS.UOM%TYPE DEFAULT NULL,
                           I_calling_function IN      VARCHAR2 DEFAULT NULL)
return BOOLEAN is

   L_program     VARCHAR2(64)              := 'TRANSPORTATION_SQL.GET_RECEIVED_QTYS';
   L_item_qty    SHIPSKU.QTY_RECEIVED%TYPE := 0;
   L_total_qty   SHIPSKU.QTY_RECEIVED%TYPE := 0;

   cursor C_RECEIVED_QTY is
      select shs.item, NVL(SUM(shs.qty_received),0) qty_received
        from item_master m,shipment shp,shipsku shs
       where (m.item = I_item
              or m.item_parent = I_item
              or m.item_grandparent = I_item)
         and m.item_level = m.tran_level
         and shp.shipment = shs.shipment
         and shp.order_no = I_order_no
         and shs.item     = m.item
       group by shs.item;

BEGIN

   if I_item IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','item no',L_program,I_item);
      return FALSE;
   end if;

   if I_order_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','order no',L_program,I_order_no);
      return FALSE;
   end if;

   for rec in C_RECEIVED_QTY loop
      L_item_qty := rec.qty_received;
      if I_uom is not null then
         if UOM_SQL.CONVERT(O_error_message,
                            L_item_qty,
                            I_uom,
                            L_item_qty,
                            NULL, -- standard UOM,
                            rec.item,
                            NULL,
                            NULL,
                            I_calling_function) = FALSE then
            return FALSE;
         end if;
      end if;
      L_total_qty := L_total_qty + L_item_qty;
   end loop;

   O_item_qty := L_total_qty;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END GET_RECEIVED_QTYS;
-----------------------------------------------------------------------------------------------------
FUNCTION DELETE_DETAILS(O_error_message       IN OUT   RTK_ERRORS.RTK_KEY%TYPE,
                        I_transportation_id   IN       TRANSPORTATION.TRANSPORTATION_ID%TYPE)
return BOOLEAN is

   L_program                 VARCHAR2(64)       := 'TRANSPORTATION_SQL.DELETE_DETAILS';
   L_table                   VARCHAR2(30);
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(Record_Locked, -54);
   L_return_code             BOOLEAN;
   L_vessel_id               TRANSPORTATION.VESSEL_ID%TYPE;
   L_voyage_flt_id           TRANSPORTATION.VOYAGE_FLT_ID%TYPE;
   L_estimated_depart_date   TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE;
   L_order_no                TRANSPORTATION.ORDER_NO%TYPE;
   L_item                    TRANSPORTATION.ITEM%TYPE;
   L_container_id            TRANSPORTATION.CONTAINER_ID%TYPE;
   L_bl_awb_id               TRANSPORTATION.BL_AWB_ID%TYPE;
   L_invoice_id              TRANSPORTATION.INVOICE_ID%TYPE;
   L_status                  TRANSPORTATION.STATUS%TYPE;

   cursor C_LOCK_TRANS_DELIVERY is
      select 'x'
        from trans_delivery
       where transportation_id = I_transportation_id
         for update nowait;

   cursor C_LOCK_TRANS_PACKING is
      select 'x'
        from trans_packing
       where transportation_id = I_transportation_id
         for update nowait;

   cursor C_LOCK_TRANS_LIC_VISA is
      select 'x'
        from trans_lic_visa
       where transportation_id = I_transportation_id
         for update nowait;

   cursor C_LOCK_TRANS_CLAIMS is
      select 'x'
        from trans_claims
       where transportation_id = I_transportation_id
         for update nowait;

   cursor C_LOCK_TRANS_SKU is
      select 'x'
        from trans_sku
       where transportation_id = I_transportation_id
         for update nowait;

   cursor C_LOCK_MISSING_DOC is
      select 'x'
        from missing_doc
       where transportation_id = I_transportation_id
         for update nowait;

   cursor C_LOCK_TIMELINE1 is
      select 'Y'
        from timeline
       where timeline_type in ('TRPI','TRPOBL')
         and key_value_1 = to_char(L_order_no)
         and key_value_2 = decode(timeline_type,'TRPI',L_item,'TRPOBL',L_bl_awb_id)
         for update nowait;

   cursor C_LOCK_TIMELINE2 is
      select 'Y'
        from timeline
       where timeline_type in ('TR','TRBL','TRCO','TRCI')
         and key_value_1 = decode(timeline_type,'TR',to_char(I_transportation_id),
                                                'TRBL',L_bl_awb_id,
                                                'TRCO',L_container_id,
                                                'TRCI',L_invoice_id)
         and key_value_2 is NULL
         for update nowait;

   cursor C_LOCK_TRAN_SHIPMENT is
      select 'Y'
        from transportation_shipment ts
       where exists(select 'Y'
                      from transportation t
                     where t.vessel_id = ts.vessel_id
                       and t.voyage_flt_id   = ts.voyage_flt_id
                       and t.estimated_depart_date = ts.estimated_depart_date
                       and t.order_no = ts.order_no
                       and t.transportation_id = I_transportation_id)
         and not exists(select 'Y'
                          from transportation t
                         where t.vessel_id = ts.vessel_id
                           and t.voyage_flt_id   = ts.voyage_flt_id
                           and t.estimated_depart_date = ts.estimated_depart_date
                           and t.order_no = ts.order_no
                           and t.transportation_id != I_transportation_id)
         for update nowait;

BEGIN

   if TRANSPORTATION_SQL.GET_MAIN_INFO(O_error_message,
                                       L_return_code,
                                       L_vessel_id,
                                       L_voyage_flt_id,
                                       L_estimated_depart_date,
                                       L_order_no,
                                       L_item,
                                       L_container_id,
                                       L_bl_awb_id,
                                       L_invoice_id,
                                       L_status,
                                       I_transportation_id) = FALSE then
      return FALSE;
   end if;

   L_table := 'TRANS_DELIVERY';

   SQL_LIB.SET_MARK('OPEN','C_LOCK_TRANS_DELIVERY','TRANS_DELIVERY','Transportation ID'||I_transportation_id);
   open C_LOCK_TRANS_DELIVERY;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_TRANS_DELIVERY','TRANS_DELIVERY','Transportation ID'||I_transportation_id);
   close C_LOCK_TRANS_DELIVERY;

   SQL_LIB.SET_MARK('DELETE',NULL,'TRANS_DELIVERY',NULL);
   delete from trans_delivery
    where transportation_id = I_transportation_id;

   ---
   L_table := 'TRANS_PACKING';

   SQL_LIB.SET_MARK('OPEN','C_LOCK_TRANS_PACKING','TRANS_PACKING','Transportation ID'||I_transportation_id);
   open C_LOCK_TRANS_PACKING;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_TRANS_PACKING','TRANS_PACKING','Transportation ID'||I_transportation_id);
   close C_LOCK_TRANS_PACKING;

   SQL_LIB.SET_MARK('DELETE',NULL,'TRANS_PACKING',NULL);
   delete from trans_packing
    where transportation_id = I_transportation_id;

   ---
   L_table := 'TRANS_LIC_VISA';

   SQL_LIB.SET_MARK('OPEN','C_LOCK_TRANS_LIC_VISA','TRANS_LIC_VISA','Transportation ID'||I_transportation_id);
   open C_LOCK_TRANS_LIC_VISA;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_TRANS_LIC_VISA','TRANS_LIC_VISA','Transportation ID'||I_transportation_id);
   close C_LOCK_TRANS_LIC_VISA;

   SQL_LIB.SET_MARK('DELETE',NULL,'TRANS_LIC_VISA',NULL);
   delete from trans_lic_visa
    where transportation_id = I_transportation_id;

   ---
   L_table := 'TRANS_CLAIMS';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_TRANS_CLAIMS','TRANS_CLAIMS','Transportation ID'||I_transportation_id);
   open C_LOCK_TRANS_CLAIMS;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_TRANS_CLAIMS','TRANS_CLAIMS','Transportation ID'||I_transportation_id);
   close C_LOCK_TRANS_CLAIMS;

   SQL_LIB.SET_MARK('DELETE',NULL,'TRANS_CLAIMS',NULL);
   delete from trans_claims
    where transportation_id = I_transportation_id;

   ---
   L_table := 'TRANS_SKU';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_TRANS_SKU','TRANS_SKU','Transportation ID'||I_transportation_id);
   open C_LOCK_TRANS_SKU;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_TRANS_SKU','TRANS_SKU','Transportation ID'||I_transportation_id);
   close C_LOCK_TRANS_SKU;

   SQL_LIB.SET_MARK('DELETE',NULL,'TRANS_SKU',NULL);
   delete from trans_sku
    where transportation_id = I_transportation_id;

   ---
   L_table := 'MISSING_DOC';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_MISSING_DOC','MISSING_DOC','Transportation ID'||I_transportation_id);
   open C_LOCK_MISSING_DOC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_MISSING_DOC','MISSING_DOC','Transportation ID'||I_transportation_id);
   close C_LOCK_MISSING_DOC;

   SQL_LIB.SET_MARK('DELETE',NULL,'MISSING_DOC',NULL);
   delete from missing_doc
    where transportation_id = I_transportation_id;

   ---
   L_table := 'TIMELINE';

   open C_LOCK_TIMELINE1;
   close C_LOCK_TIMELINE1;

   delete from timeline tl
    where tl.timeline_type in ('TRPI','TRPOBL')
      and tl.key_value_1 = to_char(L_order_no)
      and tl.key_value_2 = decode(tl.timeline_type,'TRPI',L_item,L_bl_awb_id)
      and not exists(select 'Y'
                       from transportation t
                      where t.order_no = L_order_no
                        and decode(tl.timeline_type,'TRPI',t.item,t.bl_awb_id) = decode(tl.timeline_type,'TRPI',L_item,L_bl_awb_id)
                        and t.transportation_id != I_transportation_id);

   open C_LOCK_TIMELINE2;
   close C_LOCK_TIMELINE2;

   delete from timeline t2
    where t2.timeline_type in ('TR','TRBL','TRCO','TRCI')
      and t2.key_value_1 = decode(t2.timeline_type,'TR',to_char(I_transportation_id),    
                                                   'TRBL',L_bl_awb_id,
                                                   'TRCO',L_container_id,
                                                    L_invoice_id)
      and t2.key_value_2 is NULL
      and not exists(select 'Y' 
                       from transportation t
                      where decode(t2.timeline_type,'TR',to_char(t.transportation_id),
                                                    'TRBL',t.bl_awb_id,
                                                    'TRCO',t.container_id,
                                                    t.invoice_id) = decode(t2.timeline_type,'TR',to_char(I_transportation_id),
                                                                                            'TRBL',L_bl_awb_id,
                                                                                            'TRCO',L_container_id,
                                                                                            L_invoice_id)
                        and t.transportation_id != I_transportation_id);

   ---
   L_table := 'TRANSPORTATION_SHIPMENT';
   open C_LOCK_TRAN_SHIPMENT;
   close C_LOCK_TRAN_SHIPMENT;

   delete from transportation_shipment ts
    where exists(select 'Y'
                   from transportation t
                  where t.vessel_id = ts.vessel_id
                    and t.voyage_flt_id   = ts.voyage_flt_id
                    and t.estimated_depart_date = ts.estimated_depart_date
                    and t.order_no = ts.order_no
                    and t.transportation_id = I_transportation_id)
      and not exists(select 'Y'
                      from transportation t
                     where t.vessel_id = ts.vessel_id
                       and t.voyage_flt_id   = ts.voyage_flt_id
                       and t.estimated_depart_date = ts.estimated_depart_date
                       and t.order_no = ts.order_no
                       and t.transportation_id != I_transportation_id);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            TO_CHAR(I_transportation_id),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END DELETE_DETAILS;
-----------------------------------------------------------------------------------------------------
FUNCTION ADD_ALL_CHILD_ITEMS(O_error_message       IN OUT RTK_ERRORS.RTK_KEY%TYPE,
                             I_transportation_id   IN     TRANS_SKU.TRANSPORTATION_ID%TYPE,
                             I_quantity            IN     TRANS_SKU.QUANTITY%TYPE,
                             I_quantity_uom        IN     TRANS_SKU.QUANTITY_UOM%TYPE)
return BOOLEAN is

cursor C_GET_CHILD_ITEMS(L_max_seq_no TRANS_SKU.SEQ_NO%TYPE) is
select t.transportation_id transportation_id,
       (L_max_seq_no + rownum) seq_no,
       im.item item,
       I_quantity quantity,
       I_quantity_uom quantity_uom
  from item_master im,ordloc ol, transportation t
 where t.transportation_id = I_transportation_id
   and (im.item_parent = t.item or im.item_grandparent = t.item)
   and im.item = ol.item
   and ol.order_no = t.order_no
   and not exists (select item
                     from trans_sku
                    where transportation_id = t.transportation_id
                      and item = im.item
                      and rownum = 1);
   
   L_program VARCHAR2(50) := 'TRANSPORTATION_SQL.ADD_ALL_CHILD_ITEMS';
   L_max_seq_no  TRANS_SKU.SEQ_NO%TYPE := 0;

   TYPE t_get_child_items IS TABLE OF C_GET_CHILD_ITEMS%ROWTYPE INDEX BY PLS_INTEGER;
   
   r_get_child_items t_get_child_items;
   
   
BEGIN

   if I_transportation_id is null then
      O_error_message:= sql_lib.create_msg('REQUIRED_INPUT_IS_NULL', 'I_transportation_id',L_program, NULL);
      return FALSE;
   end if;
    
   select nvl(max(seq_no),0) into L_max_seq_no from trans_sku where transportation_id = I_transportation_id;
   
   
   open C_GET_CHILD_ITEMS(L_max_seq_no);
   fetch C_GET_CHILD_ITEMS bulk collect into r_get_child_items;
   
   if r_get_child_items is not null then
      if r_get_child_items.count > 0 then
         FORALL i in 1..r_get_child_items.count
            insert into trans_sku values r_get_child_items(i);
      end if;
   end if;
   
   close C_GET_CHILD_ITEMS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END ADD_ALL_CHILD_ITEMS;
-----------------------------------------------------------------------------------------------------
FUNCTION CE_OBL_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists            IN OUT BOOLEAN,
                       I_transportation_id IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE)

RETURN BOOLEAN IS
   L_program_name            VARCHAR2(100) := 'TRANSPORTATION_SQL.CE_OBL_EXISTS';
   L_obl_exists              VARCHAR2(1)   := NULL;
   L_ce_exists               VARCHAR2(1)   := NULL;
   L_return_code             BOOLEAN;
   L_vessel_id               TRANSPORTATION.VESSEL_ID%TYPE;
   L_voyage_flt_id           TRANSPORTATION.VOYAGE_FLT_ID%TYPE;
   L_estimated_depart_date   TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE;
   L_order_no                TRANSPORTATION.ORDER_NO%TYPE;
   L_item                    TRANSPORTATION.ITEM%TYPE;
   L_container_id            TRANSPORTATION.CONTAINER_ID%TYPE;
   L_bl_awb_id               TRANSPORTATION.BL_AWB_ID%TYPE;
   L_invoice_id              TRANSPORTATION.INVOICE_ID%TYPE;
   L_status                  TRANSPORTATION.STATUS%TYPE; 

   cursor C_CE_EXISTS is
      select 'x'
        from ce_ord_item
       where vessel_id  = L_vessel_id
         and voyage_flt_id  = L_voyage_flt_id
         and estimated_depart_date  = L_estimated_depart_date
         and order_no = L_order_no
         and item = L_item;

   cursor C_OBLIGATION_EXISTS is
      select 'X'
        from obligation
       where ((obligation_level = 'TRVV'
               and key_value_1  = L_vessel_id
               and key_value_2  = L_voyage_flt_id
               and key_value_3  = L_estimated_depart_date
               and key_value_4 is NULL
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level in ('TRVVEP','TRVP')
               and key_value_1  = L_vessel_id
               and key_value_2  = L_voyage_flt_id
               and key_value_3  = L_estimated_depart_date
               and key_value_4  = L_order_no
               and ((obligation_level = 'TRVVEP' and key_value_5 is NULL) or (obligation_level = 'TRVP' and key_value_5 = L_item))
               and key_value_6 is NULL)
           or (obligation_level in ('TRBL','TRCO')
               and key_value_1  = DECODE(obligation_level,'TRCO',L_container_id,L_bl_awb_id)
               and key_value_2  = L_vessel_id
               and key_value_3  = L_voyage_flt_id
               and key_value_4  = L_estimated_depart_date
               and key_value_5 is NULL
               and key_value_6 is NULL)
           or (obligation_level in ('TRCPO','TRBLP')
               and key_value_1  = DECODE(obligation_level,'TRCPO',L_container_id,L_bl_awb_id)
               and key_value_2  = L_vessel_id
               and key_value_3  = L_voyage_flt_id
               and key_value_4  = L_estimated_depart_date
               and key_value_5  = L_order_no
               and key_value_6 is NULL)
           or (obligation_level in ('TRCP','TRBP')
               and key_value_1  = DECODE(obligation_level,'TRCP',L_container_id,L_bl_awb_id)
               and key_value_2  = L_vessel_id
               and key_value_3  = L_voyage_flt_id
               and key_value_4  = L_estimated_depart_date
               and key_value_5  = L_order_no
               and key_value_6  = L_item));

BEGIN

   O_exists := FALSE;

   if TRANSPORTATION_SQL.GET_MAIN_INFO(O_error_message,
                                       L_return_code,
                                       L_vessel_id,
                                       L_voyage_flt_id,
                                       L_estimated_depart_date,
                                       L_order_no,
                                       L_item,
                                       L_container_id,
                                       L_bl_awb_id,
                                       L_invoice_id,
                                       L_status,
                                       I_transportation_id) = FALSE then
      return FALSE;
   end if;

   open C_OBLIGATION_EXISTS;
   fetch C_OBLIGATION_EXISTS into L_obl_exists;
   close C_OBLIGATION_EXISTS;

   if L_obl_exists is NOT NULL then
      O_exists        := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('OBL_TRANPO_EXISTS',
                                            NULL,
                                            NULL,
                                            NULL);
      return TRUE;
   end if;

   open C_CE_EXISTS;
   fetch C_CE_EXISTS into L_ce_exists;
   close C_CE_EXISTS;

   if L_ce_exists is NOT NULL then
      O_exists        := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('CE_TRANPO_EXISTS',
                                            NULL,
                                            NULL,
                                            NULL);
      return TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END CE_OBL_EXISTS;
-----------------------------------------------------------------------------------------------------
END TRANSPORTATION_SQL;
/
