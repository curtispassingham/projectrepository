CREATE OR REPLACE PACKAGE BODY CORESVC_HTS_SETUP AS
--------------------------------------------------------------------------------------
   cursor C_SVC_HTS_IMPORT_COUNTRY_SETUP(I_process_id   NUMBER,
                                         I_chunk_id     NUMBER) is
      select pk_hts_import_country_setup.rowid  as pk_hts_import_country_setu_rid,
             st.rowid as st_rid,
             hics_cnt_fk.rowid    as hics_cnt_fk_rid,
             st.import_country_id,
             st.hts_format_mask,
             st.hts_heading_length,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) as action,
             st.process$status
        from svc_hts_import_country_setup st,
             hts_import_country_setup pk_hts_import_country_setup,
             country hics_cnt_fk
       where st.process_id         = I_process_id
         and st.chunk_id           = I_chunk_id
         and st.import_country_id  = pk_hts_import_country_setup.import_country_id (+)
         and st.import_country_id  = hics_cnt_fk.country_id (+);

   cursor C_SVC_TARIFF_TREATMENT(I_process_id   NUMBER,
                                 I_chunk_id     NUMBER) is
      select pk_tariff_treatment.rowid  as pk_tariff_treatment_rid,
             st.rowid as st_rid,
             st.tariff_treatment,
             st.tariff_treatment_desc,
             st.conditional_ind,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) as action,
             st.process$status
        from svc_tariff_treatment st,
             tariff_treatment pk_tariff_treatment
       where st.process_id         = I_process_id
         and st.chunk_id           = I_chunk_id
         and st.tariff_treatment   = pk_tariff_treatment.tariff_treatment (+);

   cursor C_SVC_HTS_CHAPTER(I_process_id   NUMBER,
                            I_chunk_id     NUMBER) is
      select pk_hts_chapter.rowid        as pk_hts_chapter_rid,
             htc_cnt_fk.rowid            as htc_cnt_fk_rid,
             st.rowid                    as st_rid,
             UPPER(st.import_country_id) as import_country_id,
             UPPER(st.chapter_desc)      as chapter_desc,
             st.chapter,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)            as action,
             st.process$status
        from svc_hts_chapter st,
             hts_chapter pk_hts_chapter,
             country htc_cnt_fk
       where st.process_id                       = I_process_id
         and st.chunk_id                         = I_chunk_id
         and UPPER(st.import_country_id)         = pk_hts_chapter.import_country_id (+)
         and LTRIM(st.chapter,'0')               = LTRIM(pk_hts_chapter.chapter(+),'0')
         and UPPER(st.import_country_id)         = htc_cnt_fk.country_id (+);

   cursor C_SVC_HTS_CR(I_process_id   NUMBER,
                       I_chunk_id     NUMBER) is
      select pk_hts_chapter_restraints.rowid  as pk_hts_chapter_restraints_rid,
             st.rowid                         as st_rid,
             hcr_cnt_fk2.rowid                as hcr_cnt_fk2_rid,
             hcr_htc_fk.rowid                 as hcr_htc_fk_rid,
             hcr_qca_fk.rowid                 as hcr_qca_fk_rid,
             st.restraint_suffix,
             st.quota_cat,
             st.closing_date,
             UPPER(st.uom)                    as uom,
             st.restraint_qty,
             st.restraint_desc,
             st.restraint_type,
             st.restraint_type_upd,
             UPPER(st.origin_country_id)      as origin_country_id,
             UPPER(st.import_country_id)      as import_country_id,
             st.chapter,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)                 as action,
             st.process$status
        from svc_hts_chapter_restraints st,
             hts_chapter_restraints pk_hts_chapter_restraints,
             country hcr_cnt_fk2,
             hts_chapter hcr_htc_fk,
             quota_category hcr_qca_fk,
             dual
       where st.process_id               = I_process_id
         and st.chunk_id                 = I_chunk_id
         and st.restraint_type           = pk_hts_chapter_restraints.restraint_type (+)
         and UPPER(st.origin_country_id) = pk_hts_chapter_restraints.origin_country_id (+)
         and UPPER(st.import_country_id) = pk_hts_chapter_restraints.import_country_id (+)
         and st.chapter                  = pk_hts_chapter_restraints.chapter (+)
         and st.quota_cat                = hcr_qca_fk.quota_cat (+)
         and UPPER(st.import_country_id) = hcr_qca_fk.import_country_id (+)
         and UPPER(st.import_country_id) = hcr_htc_fk.import_country_id (+)
         and st.chapter                  = hcr_htc_fk.chapter (+)
         and UPPER(st.origin_country_id) = hcr_cnt_fk2.country_id (+)
         and (st.process$status = 'N' or st.process$status  is NULL);

   cursor C_SVC_QUOTA_CAT(I_process_id   NUMBER,
                          I_chunk_id     NUMBER) is
      select pk_quota_category.rowid  as pk_quota_category_rid,
             qca_cnt_fk.rowid         as qca_cnt_fk_rid,
             st.quota_cat,
             UPPER(st.import_country_id) as import_country_id,
             st.category_desc,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)         as action,
             st.process$status
        from svc_quota_category st,
             quota_category     pk_quota_category,
             country            qca_cnt_fk
       where st.process_id        = I_process_id
         and st.chunk_id          = I_chunk_id
         and UPPER(st.import_country_id) = pk_quota_category.import_country_id (+)
         and st.quota_cat         = pk_quota_category.quota_cat (+)
         and UPPER(st.import_country_id) = qca_cnt_fk.country_id (+);

   cursor C_SVC_COUNTRY_TARIFF_TREATMENT(I_process_id NUMBER,I_chunk_id NUMBER) is
      select pk_country_tariff_treatment.rowid  as pk_country_tariff_treatmen_rid,
             st.rowid                           as st_rid,
             ctr_ttt_fk.rowid                   as ctr_ttt_fk_rid,
             st.effective_to,
             st.effective_from,
             UPPER(st.tariff_treatment) as tariff_treatment,
             UPPER(st.country_id)       as country_id,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action) as action,
             st.process$status
        from svc_country_tariff_treatment st,
             country_tariff_treatment pk_country_tariff_treatment,
             tariff_treatment ctr_ttt_fk,
             dual
       where st.process_id              = I_process_id
         and st.chunk_id                = I_chunk_id
         and st.effective_from          = pk_country_tariff_treatment.effective_from (+)
         and UPPER(st.tariff_treatment) = pk_country_tariff_treatment.tariff_treatment (+)
         and UPPER(st.country_id)       = pk_country_tariff_treatment.country_id (+)
         and UPPER(st.tariff_treatment) = ctr_ttt_fk.tariff_treatment (+)
         and (st.process$status = 'N' or st.process$status  is NULL);

   cursor C_SVC_OGA(I_process_id   NUMBER,
                    I_chunk_id     NUMBER) is
      select pk_oga.rowid  as pk_oga_rid,
             st.rowid as st_rid,
             st.oga_code,
             st.oga_desc,
             st.req_form,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) as action,
             st.process$status
        from svc_oga st,
             oga pk_oga
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.oga_code   = pk_oga.oga_code (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
   
   Type TARIFF_TREATMENT_TL_TAB IS TABLE OF TARIFF_TREATMENT_TL%ROWTYPE;
   Type HTS_CHAPTER_TAB IS TABLE OF HTS_CHAPTER_TL%ROWTYPE;
   Type HTS_CHAPTER_RESTRAINTS_TAB IS TABLE OF HTS_CHAPTER_RESTRAINTS_TL%ROWTYPE;
   Type QUOTA_CATEGORY_TAB IS TABLE OF QUOTA_CATEGORY_TL%ROWTYPE;
   Type OGA_TL_TAB IS TABLE OF OGA_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   LP_primary_lang       LANG.LANG%TYPE;

--------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
   L_program VARCHAR2(64):='CORESVC_HTS_SETUP.GET_SHEET_NAME_TRANS';
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID        := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO   := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY   := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY      := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY     := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ        := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY      :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE,
                       I_error_type   IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E')IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
--------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets                        S9T_PKG.NAMES_MAP_TYP;

   HTS_IMPORT_COUNTRY_SETUP_cols   S9T_PKG.NAMES_MAP_TYP;

   TARIFF_TREATMENT_cols           S9T_PKG.NAMES_MAP_TYP;
   TARIFF_TREATMENT_TL_cols        S9T_PKG.NAMES_MAP_TYP;

   HTS_CHAPTER_cols                S9T_PKG.NAMES_MAP_TYP;
   HTS_CHAPTER_TL_cols             S9T_PKG.NAMES_MAP_TYP;

   HTS_CR_cols                     s9t_pkg.NAMES_MAP_TYP;
   HTS_CR_tl_cols                  s9t_pkg.NAMES_MAP_TYP;

   QUOTA_CAT_cols                  s9t_pkg.NAMES_MAP_TYP;
   QUOTA_CAT_TL_cols               s9t_pkg.NAMES_MAP_TYP;

   COUNTRY_TARIFF_TREATMENT_cols   s9t_pkg.NAMES_MAP_TYP;

   OGA_cols                        S9T_PKG.NAMES_MAP_TYP;
   OGA_TL_cols                     S9T_PKG.NAMES_MAP_TYP;
BEGIN
   L_sheets                         := s9t_pkg.get_sheet_names(I_file_id);

   --Hts_import_country_setup
   HTS_IMPORT_COUNTRY_SETUP_cols    := s9t_pkg.get_col_names(I_file_id,HTS_IMP_CNT_SETUP_sheet);
   HTS_IMP_CNT_SETUP$Action         := HTS_IMPORT_COUNTRY_SETUP_cols('ACTION');
   HTS_IMP_CNT_SETUP$IMP_CNT_ID     := HTS_IMPORT_COUNTRY_SETUP_cols('IMPORT_COUNTRY_ID');
   HTS_IMP_CNT_SETUP$HTS_FMT_MASK   := HTS_IMPORT_COUNTRY_SETUP_cols('HTS_FORMAT_MASK');
   HTS_IMP_CNT_SETUP$HTS_HED_LNTH   := HTS_IMPORT_COUNTRY_SETUP_cols('HTS_HEADING_LENGTH');

   -- Tariff_treatment
   TARIFF_TREATMENT_cols            := s9t_pkg.get_col_names(I_file_id,TARIFF_TREATMENT_sheet);
   TARIFF_TREATMENT$Action          := TARIFF_TREATMENT_cols('ACTION');
   TARIFF_TRTMNT$TARIFF_TRTMNT      := TARIFF_TREATMENT_cols('TARIFF_TREATMENT');
   TARIFF_TRTMNT$TARIF_TRTMNT_DEC   := TARIFF_TREATMENT_cols('TARIFF_TREATMENT_DESC');
   TARIFF_TRTMNT$CONDITIONAL_IND    := TARIFF_TREATMENT_cols('CONDITIONAL_IND');

   -- Tariff_treatment_tl
   TARIFF_TREATMENT_TL_cols         := s9t_pkg.get_col_names(I_file_id,TARIFF_TREATMENT_TL_sheet);
   TARIFF_TREATMENT_TL$Action       := TARIFF_TREATMENT_TL_cols('ACTION');
   TARIFF_TREATMENT_TL$LANG         := TARIFF_TREATMENT_TL_cols('LANG');
   TARIFF_TRTMNT_TL$TARIFF_TRTMNT   := TARIFF_TREATMENT_TL_cols('TARIFF_TREATMENT');
   TARIFF_TRMT_TL$TARIFF_TRMT_DEC   := TARIFF_TREATMENT_TL_cols('TARIFF_TREATMENT_DESC');

   -- HTS Chapter
   HTS_CHAPTER_cols                 := s9t_pkg.get_col_names(I_file_id,HTS_CHAPTER_sheet);
   HTS_CHAPTER$Action               := HTS_CHAPTER_cols('ACTION');
   HTS_CHAPTER$IMPORT_COUNTRY_ID    := HTS_CHAPTER_cols('IMPORT_COUNTRY_ID');
   HTS_CHAPTER$CHAPTER              := HTS_CHAPTER_cols('CHAPTER');
   HTS_CHAPTER$CHAPTER_DESC         := HTS_CHAPTER_cols('CHAPTER_DESC');

   -- HTS Chapter_tl
   HTS_CHAPTER_TL_cols              := s9t_pkg.get_col_names(I_file_id,HTS_CHAPTER_TL_SHEET);
   HTS_CHAPTER_TL$Action            := HTS_CHAPTER_TL_cols('ACTION');
   HTS_CHAPTER_TL$LANG              := HTS_CHAPTER_TL_cols('LANG');
   HTS_CHAPTER_TL$IMPT_CTRY_ID      := HTS_CHAPTER_TL_cols('IMPORT_COUNTRY_ID');
   HTS_CHAPTER_TL$CHAPTER           := HTS_CHAPTER_TL_cols('CHAPTER');
   HTS_CHAPTER_TL$CHAPTER_DESC      := HTS_CHAPTER_TL_cols('CHAPTER_DESC');

   --HTS_chapter_restraints
   HTS_CR_cols               := S9T_PKG.get_col_names(I_file_id,HTS_CR_sheet);
   HTS_CR$ACTION             := HTS_CR_cols('ACTION');
   HTS_CR$RESTRAINT_SUFFIX   := HTS_CR_cols('RESTRAINT_SUFFIX');
   HTS_CR$QUOTA_CAT          := HTS_CR_cols('QUOTA_CAT');
   HTS_CR$CLOSING_DATE       := HTS_CR_cols('CLOSING_DATE');
   HTS_CR$UOM                := HTS_CR_cols('UOM');
   HTS_CR$RESTRAINT_QTY      := HTS_CR_cols('RESTRAINT_QTY');
   HTS_CR$RESTRAINT_DESC     := HTS_CR_cols('RESTRAINT_DESC');
   HTS_CR$RESTRAINT_TYPE     := HTS_CR_cols('RESTRAINT_TYPE');
   HTS_CR$RESTRAINT_TYPE_UPD := HTS_CR_cols('RESTRAINT_TYPE_UPD');
   HTS_CR$ORIGIN_COUNTRY_ID  := HTS_CR_cols('ORIGIN_COUNTRY_ID');
   HTS_CR$IMPORT_COUNTRY_ID  := HTS_CR_cols('IMPORT_COUNTRY_ID');
   HTS_CR$CHAPTER            := HTS_CR_cols('CHAPTER');
   
   --HTS_chapter_restraints_tl
   HTS_CR_TL_cols               := S9T_PKG.get_col_names(I_file_id,HTS_CR_TL_sheet);
   HTS_CR_TL$ACTION             := HTS_CR_TL_cols('ACTION');
   HTS_CR_TL$LANG               := HTS_CR_TL_cols('LANG');
   HTS_CR_TL$CHAPTER            := HTS_CR_TL_cols('CHAPTER');
   HTS_CR_TL$IMPORT_COUNTRY_ID  := HTS_CR_TL_cols('IMPORT_COUNTRY_ID');
   HTS_CR_TL$ORIGIN_COUNTRY_ID  := HTS_CR_TL_cols('ORIGIN_COUNTRY_ID');
   HTS_CR_TL$RESTRAINT_TYPE     := HTS_CR_TL_cols('RESTRAINT_TYPE');
   HTS_CR_TL$RESTRAINT_DESC     := HTS_CR_TL_cols('RESTRAINT_DESC');

   --Quota Category
   QUOTA_CAT_cols              :=s9t_pkg.get_col_names(I_file_id,QUOTA_CAT_sheet);
   QUOTA_CAT$ACTION            := QUOTA_CAT_cols('ACTION');
   QUOTA_CAT$QUOTA_CAT         := QUOTA_CAT_cols('QUOTA_CAT');
   QUOTA_CAT$IMPORT_COUNTRY_ID := QUOTA_CAT_cols('IMPORT_COUNTRY_ID');
   QUOTA_CAT$CATEGORY_DESC     := QUOTA_CAT_cols('CATEGORY_DESC');

   --Quota Category tl
   QUOTA_CAT_TL_cols              :=s9t_pkg.get_col_names(I_file_id,QUOTA_CAT_TL_sheet);
   QUOTA_CAT_TL$ACTION            := QUOTA_CAT_TL_cols('ACTION');
   QUOTA_CAT_TL$LANG              := QUOTA_CAT_TL_cols('LANG');
   QUOTA_CAT_TL$QUOTA_CAT         := QUOTA_CAT_TL_cols('QUOTA_CAT');
   QUOTA_CAT_TL$IMPORT_COUNTRY_ID := QUOTA_CAT_TL_cols('IMPORT_COUNTRY_ID');
   QUOTA_CAT_TL$CATEGORY_DESC     := QUOTA_CAT_TL_cols('CATEGORY_DESC');

   -- Country Tariff Treatment
   COUNTRY_TARIFF_TREATMENT_cols    :=s9t_pkg.get_col_names(I_file_id,COUNTRY_TARIFF_TREATMENT_sheet);
   COUNTRY_TRF_TT$Action            := COUNTRY_TARIFF_TREATMENT_cols(action_column);
   COUNTRY_TRF_TT$EFFECTIVE_TO      := COUNTRY_TARIFF_TREATMENT_cols('EFFECTIVE_TO');
   COUNTRY_TRF_TT$EFFECTIVE_FROM    := COUNTRY_TARIFF_TREATMENT_cols('EFFECTIVE_FROM');
   COUNTRY_TRF_TT$TARIFF_TT         := COUNTRY_TARIFF_TREATMENT_cols('TARIFF_TREATMENT');
   COUNTRY_TRF_TT$COUNTRY_ID        := COUNTRY_TARIFF_TREATMENT_cols('COUNTRY_ID');

   -- OGA
   OGA_cols                         := s9t_pkg.get_col_names(I_file_id,OGA_sheet);
   OGA$Action                       := OGA_cols('ACTION');
   OGA$OGA_CODE                     := OGA_cols('OGA_CODE');
   OGA$OGA_DESC                     := OGA_cols('OGA_DESC');
   OGA$REQ_FORM                     := OGA_cols('REQ_FORM');

   --OGA TL
   OGA_TL_cols                      := s9t_pkg.get_col_names(I_file_id,OGA_TL_sheet);
   OGA_TL$Action                    := OGA_TL_cols('ACTION');
   OGA_TL$LANG                      := OGA_TL_cols('LANG');
   OGA_TL$OGA_CODE                  := OGA_TL_cols('OGA_CODE');
   OGA_TL$OGA_DESC                  := OGA_TL_cols('OGA_DESC');

END POPULATE_NAMES;
--------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_IMP_CNT_SETUP( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id    = I_file_id
                         and ss.sheet_name = HTS_IMP_CNT_SETUP_sheet )
               select s9t_row(s9t_cells(CORESVC_HTS_SETUP.action_mod,
                                        import_country_id,
                                        hts_format_mask,
                                        hts_heading_length))
                 from hts_import_country_setup;
END POPULATE_HTS_IMP_CNT_SETUP;
--------------------------------------------------------------------------------------
PROCEDURE POPULATE_TARIFF_TREATMENT( I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id    = I_file_id
                         and ss.sheet_name = TARIFF_TREATMENT_sheet )
               select s9t_row(s9t_cells(CORESVC_HTS_SETUP.action_mod,
                                        tariff_treatment,
                                        tariff_treatment_desc,
                                        conditional_ind))
                 from tariff_treatment;
END POPULATE_TARIFF_TREATMENT;
--------------------------------------------------------------------------------------
PROCEDURE POPULATE_TARIFF_TREATMENT_TL( I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id    = I_file_id
                         and ss.sheet_name = TARIFF_TREATMENT_TL_sheet )
               select s9t_row(s9t_cells(CORESVC_HTS_SETUP.action_mod ,
                                        lang,
                                        tariff_treatment,
                                        tariff_treatment_desc))
                 from tariff_treatment_tl;
END POPULATE_TARIFF_TREATMENT_TL;
--------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_CHAPTER( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = HTS_CHAPTER_sheet )
   select s9t_row(s9t_cells( CORESVC_HTS_SETUP.action_mod,
                             import_country_id,
                             chapter,
                             chapter_desc))
     from hts_chapter ;
END POPULATE_HTS_CHAPTER;
-------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_CHAPTER_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = HTS_CHAPTER_TL_SHEET )
   select s9t_row(s9t_cells( CORESVC_HTS_SETUP.action_mod,
                             lang,
                             import_country_id,
                             chapter,
                             chapter_desc))
     from hts_chapter_tl ;
END POPULATE_HTS_CHAPTER_TL;
-------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_CR(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = HTS_CR_sheet)
              select s9t_row(S9T_CELLS(CORESVC_HTS_SETUP.action_mod,
                                       chapter,
                                       import_country_id,
                                       origin_country_id,
                                       restraint_type,
                                       restraint_type,
                                       restraint_desc,
                                       restraint_qty,
                                       uom,
                                       closing_date,
                                       quota_cat,
                                       restraint_suffix))
                from hts_chapter_restraints;
END POPULATE_HTS_CR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_CR_TL(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = HTS_CR_TL_SHEET)
              select s9t_row(S9T_CELLS(CORESVC_HTS_SETUP.action_mod,
                                       lang,
                                       chapter,
                                       import_country_id,
                                       origin_country_id,
                                       restraint_type,
                                       restraint_desc))
                from hts_chapter_restraints_tl;
END POPULATE_HTS_CR_TL;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_QUOTA_CAT(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE( select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = QUOTA_CAT_sheet )
               select s9t_row(s9t_cells( CORESVC_HTS_SETUP.action_mod,
                                         quota_cat,
                                         import_country_id,
                                         category_desc))
                 from quota_category ;
END POPULATE_QUOTA_CAT;
------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_QUOTA_CAT_TL(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE( select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = QUOTA_CAT_TL_sheet )
               select s9t_row(s9t_cells( CORESVC_HTS_SETUP.action_mod,
                                         lang,
                                         quota_cat,
                                         import_country_id,
                                         category_desc))
                 from quota_category_tl ;
END POPULATE_QUOTA_CAT_TL;
--------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_COUNTRY_TRF_TT(I_file_id IN NUMBER)IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id    = I_file_id
                         and ss.sheet_name = COUNTRY_TARIFF_TREATMENT_sheet)

   select s9t_row(s9t_cells(CORESVC_HTS_SETUP.action_mod,
                            COUNTRY_ID,
                            TARIFF_TREATMENT,
                            EFFECTIVE_FROM,
                            EFFECTIVE_TO                            
                            ))
     from country_tariff_treatment ;
END POPULATE_COUNTRY_TRF_TT;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_OGA( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE( select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id    = I_file_id
                         and ss.sheet_name = OGA_sheet )
               select s9t_row(s9t_cells(CORESVC_HTS_SETUP.action_mod ,
                                        oga_code,
                                        oga_desc,
                                        req_form))
                 from oga;
END POPULATE_OGA;
-------------------------------------------------------------------------------------
PROCEDURE POPULATE_OGA_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = OGA_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_HTS_SETUP.action_mod,
                            lang,
                            oga_code,
                            oga_desc))
     from oga_tl;
END POPULATE_OGA_TL;
-------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS

   L_file        S9T_FILE;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(HTS_IMP_CNT_SETUP_sheet);
   L_file.sheets(l_file.get_sheet_index(HTS_IMP_CNT_SETUP_sheet)).column_headers := s9t_cells('ACTION',
                                                                                              'IMPORT_COUNTRY_ID',
                                                                                              'HTS_FORMAT_MASK',
                                                                                              'HTS_HEADING_LENGTH');
   L_file.add_sheet(TARIFF_TREATMENT_sheet);
   L_file.sheets(l_file.get_sheet_index(TARIFF_TREATMENT_sheet)).column_headers := s9t_cells('ACTION',
                                                                                             'TARIFF_TREATMENT',
                                                                                             'TARIFF_TREATMENT_DESC',
                                                                                             'CONDITIONAL_IND');

   L_file.add_sheet(TARIFF_TREATMENT_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(TARIFF_TREATMENT_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                                'LANG',
                                                                                                'TARIFF_TREATMENT',
                                                                                                'TARIFF_TREATMENT_DESC');
   L_file.add_sheet(HTS_CHAPTER_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_CHAPTER_sheet)).column_headers := s9t_cells('ACTION',
                                                                                        'IMPORT_COUNTRY_ID',
                                                                                        'CHAPTER',
                                                                                        'CHAPTER_DESC');
   L_file.add_sheet(HTS_CHAPTER_TL_SHEET);
   L_file.sheets(L_file.get_sheet_index(HTS_CHAPTER_TL_SHEET)).column_headers := s9t_cells('ACTION',
                                                                                           'LANG',
                                                                                           'IMPORT_COUNTRY_ID',
                                                                                           'CHAPTER',
                                                                                           'CHAPTER_DESC');
   L_file.add_sheet(HTS_CR_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_CR_sheet)).column_headers := S9T_CELLS('ACTION',
                                                                                   'CHAPTER',
                                                                                   'IMPORT_COUNTRY_ID',
                                                                                   'ORIGIN_COUNTRY_ID',
                                                                                   'RESTRAINT_TYPE',
                                                                                   'RESTRAINT_TYPE_UPD',
                                                                                   'RESTRAINT_DESC',
                                                                                   'RESTRAINT_QTY',
                                                                                   'UOM',
                                                                                   'CLOSING_DATE',
                                                                                   'QUOTA_CAT',
                                                                                   'RESTRAINT_SUFFIX');
   L_file.add_sheet(HTS_CR_TL_SHEET);
   L_file.sheets(L_file.get_sheet_index(HTS_CR_TL_SHEET)).column_headers := S9T_CELLS('ACTION',
                                                                                      'LANG',
                                                                                      'CHAPTER',
                                                                                      'IMPORT_COUNTRY_ID',
                                                                                      'ORIGIN_COUNTRY_ID',
                                                                                      'RESTRAINT_TYPE',
                                                                                      'RESTRAINT_DESC');
   L_file.add_sheet(QUOTA_CAT_sheet);
   L_file.sheets(l_file.get_sheet_index(QUOTA_CAT_sheet)).column_headers := s9t_cells('ACTION',
                                                                                      'QUOTA_CAT',
                                                                                      'IMPORT_COUNTRY_ID',
                                                                                      'CATEGORY_DESC');
   L_file.add_sheet(QUOTA_CAT_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(QUOTA_CAT_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                         'LANG',
                                                                                         'QUOTA_CAT',
                                                                                         'IMPORT_COUNTRY_ID',
                                                                                         'CATEGORY_DESC');
   L_file.add_sheet(COUNTRY_TARIFF_TREATMENT_sheet);
   L_file.sheets(L_file.GET_SHEET_INDEX(COUNTRY_TARIFF_TREATMENT_sheet)).column_headers := s9t_cells(action_column,
                                                                                                     'COUNTRY_ID',
                                                                                                     'TARIFF_TREATMENT',
                                                                                                     'EFFECTIVE_FROM',
                                                                                                     'EFFECTIVE_TO');
   L_file.add_sheet(OGA_sheet);
   L_file.sheets(l_file.get_sheet_index(OGA_sheet)).column_headers := s9t_cells('ACTION',
                                                                                'OGA_CODE',
                                                                                'OGA_DESC',
                                                                                'REQ_FORM');
   L_file.add_sheet(OGA_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(OGA_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                   'LANG',
                                                                                   'OGA_CODE',
                                                                                   'OGA_DESC');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(64):='CORESVC_HTS_SETUP.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   
   ---
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   ---
   
   if I_template_only_ind = 'N' then
      POPULATE_HTS_IMP_CNT_SETUP(O_file_id);
      POPULATE_TARIFF_TREATMENT(O_file_id);
      POPULATE_TARIFF_TREATMENT_TL(O_file_id);
      POPULATE_HTS_CHAPTER(O_file_id);
      POPULATE_HTS_CHAPTER_TL(O_file_id);
      POPULATE_HTS_CR(O_file_id);
      POPULATE_HTS_CR_TL(O_file_id);
      POPULATE_QUOTA_CAT(O_file_id);
      POPULATE_QUOTA_CAT_TL(O_file_id);
      POPULATE_COUNTRY_TRF_TT(O_file_id);
      POPULATE_OGA(O_file_id);
      POPULATE_OGA_TL(O_file_id);
      COMMIT;
   end if;
   
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   
   ---
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   ---
   
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_IMP_CNT_SETUP(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                        I_process_id   IN   SVC_HTS_IMPORT_COUNTRY_SETUP.process_id%TYPE) IS

   L_process_id    SVC_HTS_IMPORT_COUNTRY_SETUP.PROCESS_ID%TYPE;
   L_temp_rec      SVC_HTS_IMPORT_COUNTRY_SETUP%ROWTYPE;
   L_default_rec   SVC_HTS_IMPORT_COUNTRY_SETUP%ROWTYPE;
   L_error         BOOLEAN := FALSE;
   TYPE SVC_HTS_IMP_CNT_SETUP_COL_TYP IS TABLE OF SVC_HTS_IMPORT_COUNTRY_SETUP%ROWTYPE;
   SVC_HTS_IMP_CNT_SETUP_COL SVC_HTS_IMP_CNT_SETUP_COL_TYP :=NEW SVC_HTS_IMP_CNT_SETUP_COL_TYP();
   
   cursor C_MANDATORY_IND is
      select IMPORT_COUNTRY_ID_mi,
             HTS_FORMAT_MASK_mi,
             HTS_HEADING_LENGTH_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = CORESVC_HTS_SETUP.template_key
                 and wksht_key    = 'HTS_IMPORT_COUNTRY_SETUP') 
               PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'IMPORT_COUNTRY_ID' AS IMPORT_COUNTRY_ID,
                                                                 'HTS_FORMAT_MASK' AS HTS_FORMAT_MASK,
                                                                 'HTS_HEADING_LENGTH' AS HTS_HEADING_LENGTH));
   L_mi_rec c_mandatory_ind%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_HTS_IMPORT_COUNTRY_SETUP';
   L_pk_columns    VARCHAR2(255)  := 'Import Country ID';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select IMPORT_COUNTRY_ID_dv,
                      HTS_FORMAT_MASK_dv,
                      HTS_HEADING_LENGTH_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key   = CORESVC_HTS_SETUP.template_key
                          and wksht_key      = 'HTS_IMPORT_COUNTRY_SETUP') 
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                                              'HTS_FORMAT_MASK' as HTS_FORMAT_MASK,
                                                                              'HTS_HEADING_LENGTH' as HTS_HEADING_LENGTH)))
   LOOP
      BEGIN
         L_default_rec.IMPORT_COUNTRY_ID := rec.IMPORT_COUNTRY_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_IMPORT_COUNTRY_SETUP ' ,
                            NULL,
                           'IMPORT_COUNTRY_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.HTS_FORMAT_MASK := rec.HTS_FORMAT_MASK_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_IMPORT_COUNTRY_SETUP ' ,
                            NULL,
                           'HTS_FORMAT_MASK ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.HTS_HEADING_LENGTH := rec.HTS_HEADING_LENGTH_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_IMPORT_COUNTRY_SETUP ' ,
                            NULL,
                           'HTS_HEADING_LENGTH ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
   
   ---
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   ---
   
   FOR rec IN (select r.get_cell(HTS_IMP_CNT_SETUP$Action)        as Action,
                      r.get_cell(HTS_IMP_CNT_SETUP$IMP_CNT_ID)    as IMPORT_COUNTRY_ID,
                      r.get_cell(HTS_IMP_CNT_SETUP$HTS_FMT_MASK)  as HTS_FORMAT_MASK,
                      r.get_cell(HTS_IMP_CNT_SETUP$HTS_HED_LNTH)  as HTS_HEADING_LENGTH,
                      r.get_row_seq()                             as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(HTS_IMP_CNT_SETUP_sheet))
   LOOP
      L_temp_rec.action            := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_IMP_CNT_SETUP_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.IMPORT_COUNTRY_ID := rec.IMPORT_COUNTRY_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_IMP_CNT_SETUP_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.HTS_FORMAT_MASK := rec.HTS_FORMAT_MASK;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_IMP_CNT_SETUP_sheet,
                            rec.row_seq,
                            'HTS_FORMAT_MASK',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.HTS_HEADING_LENGTH := rec.HTS_HEADING_LENGTH;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_IMP_CNT_SETUP_sheet,
                            rec.row_seq,
                            'HTS_HEADING_LENGTH',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      ---

      if rec.action = CORESVC_HTS_SETUP.action_new then
         L_temp_rec.IMPORT_COUNTRY_ID  := NVL(L_temp_rec.IMPORT_COUNTRY_ID,
                                              L_default_rec.IMPORT_COUNTRY_ID);
         L_temp_rec.HTS_FORMAT_MASK    := NVL(L_temp_rec.HTS_FORMAT_MASK,
                                              L_default_rec.HTS_FORMAT_MASK);
         L_temp_rec.HTS_HEADING_LENGTH := NVL(L_temp_rec.HTS_HEADING_LENGTH,
                                              L_default_rec.HTS_HEADING_LENGTH);
      end if;
---

      if not ( L_temp_rec.IMPORT_COUNTRY_ID is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_IMP_CNT_SETUP_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         SVC_HTS_IMP_CNT_SETUP_COL.extend();
         SVC_HTS_IMP_CNT_SETUP_COL(SVC_HTS_IMP_CNT_SETUP_COL.COUNT()) := L_temp_rec;
      end if;

   END LOOP;
   BEGIN
      forall i IN 1..SVC_HTS_IMP_CNT_SETUP_COL.COUNT SAVE EXCEPTIONS
      merge into SVC_HTS_IMPORT_COUNTRY_SETUP st
      using(select(case
                   when l_mi_rec.IMPORT_COUNTRY_ID_mi    = 'N'
                    and SVC_HTS_IMP_CNT_SETUP_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.IMPORT_COUNTRY_ID IS NULL
                   then mt.IMPORT_COUNTRY_ID
                   else s1.IMPORT_COUNTRY_ID
                   end) AS IMPORT_COUNTRY_ID,
                  (case
                   when l_mi_rec.HTS_FORMAT_MASK_mi    = 'N'
                    and SVC_HTS_IMP_CNT_SETUP_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.HTS_FORMAT_MASK IS NULL
                   then mt.HTS_FORMAT_MASK
                   else s1.HTS_FORMAT_MASK
                   end) AS HTS_FORMAT_MASK,
                  (case
                   when l_mi_rec.HTS_HEADING_LENGTH_mi    = 'N'
                    and SVC_HTS_IMP_CNT_SETUP_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.HTS_HEADING_LENGTH IS NULL
                   then mt.HTS_HEADING_LENGTH
                   else s1.HTS_HEADING_LENGTH
                   end) AS HTS_HEADING_LENGTH,
                  null as dummy
              from (select SVC_HTS_IMP_CNT_SETUP_COL(i).IMPORT_COUNTRY_ID AS IMPORT_COUNTRY_ID,
                           SVC_HTS_IMP_CNT_SETUP_COL(i).HTS_FORMAT_MASK AS HTS_FORMAT_MASK,
                           SVC_HTS_IMP_CNT_SETUP_COL(i).HTS_HEADING_LENGTH AS HTS_HEADING_LENGTH,
                           null as dummy
                      from dual ) s1,
                   HTS_IMPORT_COUNTRY_SETUP mt
             where mt.IMPORT_COUNTRY_ID (+)    = s1.IMPORT_COUNTRY_ID)sq
                on (st.IMPORT_COUNTRY_ID      = sq.IMPORT_COUNTRY_ID and
                    SVC_HTS_IMP_CNT_SETUP_COL(i).ACTION IN (CORESVC_HTS_SETUP.action_mod,CORESVC_HTS_SETUP.action_del))
      when matched then
      update
         set process_id           = SVC_HTS_IMP_CNT_SETUP_COL(i).process_id ,
             chunk_id             = SVC_HTS_IMP_CNT_SETUP_COL(i).chunk_id ,
             row_seq              = SVC_HTS_IMP_CNT_SETUP_COL(i).row_seq ,
             action               = SVC_HTS_IMP_CNT_SETUP_COL(i).action ,
             process$status       = SVC_HTS_IMP_CNT_SETUP_COL(i).process$status ,
             hts_heading_length   = sq.hts_heading_length ,
             hts_format_mask      = sq.hts_format_mask ,
             create_id            = SVC_HTS_IMP_CNT_SETUP_COL(i).create_id ,
             create_datetime      = SVC_HTS_IMP_CNT_SETUP_COL(i).create_datetime ,
             last_upd_id          = SVC_HTS_IMP_CNT_SETUP_COL(i).last_upd_id ,
             last_upd_datetime    = SVC_HTS_IMP_CNT_SETUP_COL(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             import_country_id ,
             hts_format_mask ,
             hts_heading_length ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(SVC_HTS_IMP_CNT_SETUP_COL(i).process_id ,
             SVC_HTS_IMP_CNT_SETUP_COL(i).chunk_id ,
             SVC_HTS_IMP_CNT_SETUP_COL(i).row_seq ,
             SVC_HTS_IMP_CNT_SETUP_COL(i).action ,
             SVC_HTS_IMP_CNT_SETUP_COL(i).process$status ,
             sq.import_country_id ,
             sq.hts_format_mask ,
             sq.hts_heading_length ,
             SVC_HTS_IMP_CNT_SETUP_COL(i).create_id ,
             SVC_HTS_IMP_CNT_SETUP_COL(i).create_datetime ,
             SVC_HTS_IMP_CNT_SETUP_COL(i).last_upd_id ,
             SVC_HTS_IMP_CNT_SETUP_COL(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP

            ---
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            ---
            WRITE_S9T_ERROR(I_file_id,
                            HTS_IMP_CNT_SETUP_sheet,
                            SVC_HTS_IMP_CNT_SETUP_COL(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_IMP_CNT_SETUP;
--------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TARIFF_TREATMENT( I_file_id      IN   s9t_folder.file_id%TYPE,
                                        I_process_id   IN   SVC_TARIFF_TREATMENT.process_id%TYPE) IS

   TYPE svc_TARIFF_TREATMENT_col_typ IS TABLE OF SVC_TARIFF_TREATMENT%ROWTYPE;
   L_temp_rec SVC_TARIFF_TREATMENT%ROWTYPE;
   svc_TARIFF_TREATMENT_col svc_TARIFF_TREATMENT_col_typ :=NEW svc_TARIFF_TREATMENT_col_typ();
   L_process_id SVC_TARIFF_TREATMENT.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_TARIFF_TREATMENT%ROWTYPE;
   
   cursor C_MANDATORY_IND is
      select TARIFF_TREATMENT_mi,
             TARIFF_TREATMENT_DESC_mi,
             CONDITIONAL_IND_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key   = CORESVC_HTS_SETUP.template_key
                 and wksht_key      = 'TARIFF_TREATMENT') 
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('TARIFF_TREATMENT'      as TARIFF_TREATMENT,
                                                                'TARIFF_TREATMENT_DESC' as TARIFF_TREATMENT_DESC,
                                                                'CONDITIONAL_IND'       as CONDITIONAL_IND));
   L_mi_rec c_mandatory_ind%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_TARIFF_TREATMENT';
   L_pk_columns    VARCHAR2(255)  := 'Tariff Treatment';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   
BEGIN
  -- Get default values.
   FOR rec IN (select TARIFF_TREATMENT_dv,
                      TARIFF_TREATMENT_DESC_dv,
                      CONDITIONAL_IND_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_HTS_SETUP.template_key
                          and wksht_key     = 'TARIFF_TREATMENT') 
                        PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('TARIFF_TREATMENT' AS TARIFF_TREATMENT,
                                                                             'TARIFF_TREATMENT_DESC' AS TARIFF_TREATMENT_DESC,
                                                                             'CONDITIONAL_IND' AS CONDITIONAL_IND)))
   LOOP
      BEGIN
         L_default_rec.TARIFF_TREATMENT := rec.TARIFF_TREATMENT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TARIFF_TREATMENT' ,
                            NULL,
                            'TARIFF_TREATMENT',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TARIFF_TREATMENT_DESC := rec.TARIFF_TREATMENT_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TARIFF_TREATMENT ' ,
                            NULL,
                            'TARIFF_TREATMENT_DESC ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CONDITIONAL_IND := rec.CONDITIONAL_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TARIFF_TREATMENT ' ,
                            NULL,
                            'CONDITIONAL_IND ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(TARIFF_TREATMENT$Action)                  as Action,
                      r.get_cell(TARIFF_TRTMNT$TARIFF_TRTMNT)        as TARIFF_TREATMENT,
                      r.get_cell(TARIFF_TRTMNT$TARIF_TRTMNT_DEC)   as TARIFF_TREATMENT_DESC,
                      r.get_cell(TARIFF_TRTMNT$CONDITIONAL_IND)         as CONDITIONAL_IND,
                      r.get_row_seq()                                      as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(TARIFF_TREATMENT_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TARIFF_TREATMENT := rec.TARIFF_TREATMENT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_sheet,
                            rec.row_seq,
                            'TARIFF_TREATMENT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TARIFF_TREATMENT_DESC := rec.TARIFF_TREATMENT_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_sheet,
                            rec.row_seq,
                            'TARIFF_TREATMENT_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CONDITIONAL_IND := rec.CONDITIONAL_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_sheet,
                            rec.row_seq,
                            'CONDITIONAL_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      ---
      if rec.action = CORESVC_HTS_SETUP.action_new then
         L_temp_rec.TARIFF_TREATMENT      := NVL(L_temp_rec.TARIFF_TREATMENT,
                                                 L_default_rec.TARIFF_TREATMENT);
         L_temp_rec.TARIFF_TREATMENT_DESC := NVL(L_temp_rec.TARIFF_TREATMENT_DESC,
                                                 L_default_rec.TARIFF_TREATMENT_DESC);
         L_temp_rec.CONDITIONAL_IND       := NVL(L_temp_rec.CONDITIONAL_IND,
                                                 L_default_rec.CONDITIONAL_IND);
      end if;
      ---

      if not ( L_temp_rec.TARIFF_TREATMENT is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         TARIFF_TREATMENT_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                        SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_TARIFF_TREATMENT_col.extend();
         svc_TARIFF_TREATMENT_col(svc_TARIFF_TREATMENT_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   
   BEGIN
      forall i IN 1..svc_TARIFF_TREATMENT_col.COUNT SAVE EXCEPTIONS
      merge into SVC_TARIFF_TREATMENT st
      using(select(case
                   when l_mi_rec.TARIFF_TREATMENT_mi    = 'N'
                    and svc_TARIFF_TREATMENT_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.TARIFF_TREATMENT IS NULL
                   then mt.TARIFF_TREATMENT
                   else s1.TARIFF_TREATMENT
                   end) AS TARIFF_TREATMENT,
                  (case
                   when l_mi_rec.TARIFF_TREATMENT_DESC_mi    = 'N'
                    and svc_TARIFF_TREATMENT_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.TARIFF_TREATMENT_DESC IS NULL
                   then mt.TARIFF_TREATMENT_DESC
                   else s1.TARIFF_TREATMENT_DESC
                   end) AS TARIFF_TREATMENT_DESC,
                  (case
                   when l_mi_rec.CONDITIONAL_IND_mi    = 'N'
                    and svc_TARIFF_TREATMENT_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.CONDITIONAL_IND IS NULL
                   then mt.CONDITIONAL_IND
                   else s1.CONDITIONAL_IND
                   end) AS CONDITIONAL_IND,
                   null as dummy
              from (select svc_TARIFF_TREATMENT_col(i).TARIFF_TREATMENT AS TARIFF_TREATMENT,
                           svc_TARIFF_TREATMENT_col(i).TARIFF_TREATMENT_DESC AS TARIFF_TREATMENT_DESC,
                           svc_TARIFF_TREATMENT_col(i).CONDITIONAL_IND AS CONDITIONAL_IND,
                           null as dummy
                      from dual) s1,
                   TARIFF_TREATMENT mt
             where mt.TARIFF_TREATMENT (+)    = s1.TARIFF_TREATMENT)sq
                on ( st.TARIFF_TREATMENT      = sq.TARIFF_TREATMENT and
                     svc_TARIFF_TREATMENT_col(i).ACTION IN (CORESVC_HTS_SETUP.action_mod,CORESVC_HTS_SETUP.action_del))
      when matched then
      update
         set process_id              = svc_TARIFF_TREATMENT_col(i).process_id ,
             chunk_id                = svc_TARIFF_TREATMENT_col(i).chunk_id ,
             row_seq                 = svc_TARIFF_TREATMENT_col(i).row_seq ,
             action                  = svc_TARIFF_TREATMENT_col(i).action ,
             process$status          = svc_TARIFF_TREATMENT_col(i).process$status ,
             tariff_treatment_desc   = sq.tariff_treatment_desc,
             conditional_ind         = sq.conditional_ind,
             create_id               = svc_TARIFF_TREATMENT_col(i).create_id,
             create_datetime         = svc_TARIFF_TREATMENT_col(i).create_datetime,
             last_upd_id             = svc_TARIFF_TREATMENT_col(i).last_upd_id,
             last_upd_datetime       = svc_TARIFF_TREATMENT_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             tariff_treatment ,
             tariff_treatment_desc ,
             conditional_ind ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_TARIFF_TREATMENT_col(i).process_id ,
             svc_TARIFF_TREATMENT_col(i).chunk_id ,
             svc_TARIFF_TREATMENT_col(i).row_seq ,
             svc_TARIFF_TREATMENT_col(i).action ,
             svc_TARIFF_TREATMENT_col(i).process$status ,
             sq.tariff_treatment ,
             sq.tariff_treatment_desc ,
             sq.conditional_ind ,
             svc_TARIFF_TREATMENT_col(i).create_id ,
             svc_TARIFF_TREATMENT_col(i).create_datetime ,
             svc_TARIFF_TREATMENT_col(i).last_upd_id ,
             svc_TARIFF_TREATMENT_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;

            ---
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            ---

            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_sheet,
                            svc_TARIFF_TREATMENT_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TARIFF_TREATMENT;
--------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TARIFF_TRTMNT_TL( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                        I_process_id   IN   SVC_TARIFF_TREATMENT_TL.PROCESS_ID%TYPE) IS

   L_temp_rec                     SVC_TARIFF_TREATMENT_TL%ROWTYPE;
   L_process_id                   SVC_TARIFF_TREATMENT_TL.PROCESS_ID%TYPE;
   L_error                        BOOLEAN :=FALSE;
   L_default_rec                  SVC_TARIFF_TREATMENT_TL%ROWTYPE;

   TYPE SVC_TARIFF_TRTMNT_TL_COL_TYP IS TABLE OF SVC_TARIFF_TREATMENT_TL%ROWTYPE;
   SVC_TARIFF_TREATMENT_TL_COL    SVC_TARIFF_TRTMNT_TL_COL_TYP := NEW SVC_TARIFF_TRTMNT_TL_COL_TYP();

   cursor C_MANDATORY_IND is
      select lang_mi,
             tariff_treatment_mi,
             tariff_treatment_desc_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key   = CORESVC_HTS_SETUP.TEMPLATE_KEY
                 and wksht_key      = 'TARIFF_TREATMENT_TL') 
              PIVOT (MAX(mandatory) AS mi
                FOR (column_key) IN ('LANG' AS LANG,
                                     'TARIFF_TREATMENT' AS TARIFF_TREATMENT,
                                     'TARIFF_TREATMENT_DESC' AS TARIFF_TREATMENT_DESC));
   L_mi_rec c_mandatory_ind%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_TARIFF_TREATMENT_TL';
   L_pk_columns    VARCHAR2(255)  := 'Tariff treatment, Language';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   
BEGIN
  -- Get default values.
   FOR rec IN (select LANG_dv,
                      TARIFF_TREATMENT_dv,
                      TARIFF_TREATMENT_DESC_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key   = CORESVC_HTS_SETUP.TEMPLATE_KEY
                          and wksht_key      = 'TARIFF_TREATMENT_TL'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ('LANG' AS LANG,
                                                     'TARIFF_TREATMENT' AS TARIFF_TREATMENT,
                                                     'TARIFF_TREATMENT_DESC' AS TARIFF_TREATMENT_DESC)))
   ---
   LOOP
    
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_TL_sheet,
                            NULL,
                           'LANG ',
                            NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.TARIFF_TREATMENT := rec.TARIFF_TREATMENT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_TL_sheet,
                            NULL,
                            'TARIFF_TREATMENT',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.TARIFF_TREATMENT_DESC := rec.TARIFF_TREATMENT_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_TL_sheet,
                            NULL,
                            'TARIFF_TREATMENT_DESC ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   ---
   
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   
   FOR rec IN (select r.get_cell(TARIFF_TREATMENT_TL$Action)                as Action,
                      r.get_cell(TARIFF_TREATMENT_TL$LANG)                  as LANG,
                      r.get_cell(TARIFF_TRTMNT_TL$TARIFF_TRTMNT)      as TARIFF_TREATMENT,
                      r.get_cell(TARIFF_TRMT_TL$TARIFF_TRMT_DEC) as TARIFF_TREATMENT_DESC,
                      r.get_row_seq()                                       as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(TARIFF_TREATMENT_TL_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;

      ---
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      ---

      ---
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      ---

      ---
      BEGIN
         L_temp_rec.TARIFF_TREATMENT := rec.TARIFF_TREATMENT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_TL_sheet,
                            rec.row_seq,
                            'TARIFF_TREATMENT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      ---

      ---
      BEGIN
         L_temp_rec.TARIFF_TREATMENT_DESC := rec.TARIFF_TREATMENT_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_TL_sheet,
                            rec.row_seq,
                            'TARIFF_TREATMENT_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
     ---

      if rec.action = CORESVC_HTS_SETUP.action_new then
         L_temp_rec.LANG                    := NVL( L_temp_rec.LANG,
                                                    L_default_rec.LANG);
         L_temp_rec.TARIFF_TREATMENT        := NVL( L_temp_rec.TARIFF_TREATMENT,
                                                    L_default_rec.TARIFF_TREATMENT);
         L_temp_rec.TARIFF_TREATMENT_DESC   := NVL( L_temp_rec.TARIFF_TREATMENT_DESC,
                                                    L_default_rec.TARIFF_TREATMENT_DESC);
      end if;

      if not (L_temp_rec.TARIFF_TREATMENT is NOT NULL and L_temp_rec.LANG is NOT NULL )then
         WRITE_S9T_ERROR(I_file_id,
                         TARIFF_TREATMENT_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      
      if NOT L_error then
         svc_TARIFF_TREATMENT_TL_col.extend();
         svc_TARIFF_TREATMENT_TL_col(svc_TARIFF_TREATMENT_TL_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;
   
   BEGIN
      forall i IN 1..svc_tariff_treatment_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_TARIFF_TREATMENT_TL st
      using (select(case
                   when L_mi_rec.LANG_mi    = 'N'
                    and svc_tariff_treatment_tl_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.LANG IS NULL
                   then mt.LANG
                   else s1.LANG
                   end) AS LANG,
                  (case
                   when L_mi_rec.TARIFF_TREATMENT_mi    = 'N'
                    and svc_tariff_treatment_tl_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.TARIFF_TREATMENT IS NULL
                   then mt.TARIFF_TREATMENT
                   else s1.TARIFF_TREATMENT
                   end) AS TARIFF_TREATMENT,
                  (case
                   when L_mi_rec.TARIFF_TREATMENT_DESC_mi    = 'N'
                    and svc_tariff_treatment_tl_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.TARIFF_TREATMENT_DESC IS NULL
                   then mt.TARIFF_TREATMENT_DESC
                   else s1.TARIFF_TREATMENT_DESC
                   end) AS TARIFF_TREATMENT_DESC,
                  null as dummy
              from (select svc_tariff_treatment_tl_col(i).LANG                  as LANG,
                           svc_tariff_treatment_tl_col(i).TARIFF_TREATMENT      as TARIFF_TREATMENT,
                           svc_tariff_treatment_tl_col(i).TARIFF_TREATMENT_DESC as TARIFF_TREATMENT_DESC
                      from dual ) s1,
                           TARIFF_TREATMENT_TL mt
                     where mt.TARIFF_TREATMENT (+)  = s1.TARIFF_TREATMENT   
                       and mt.LANG (+)          = s1.LANG)sq
         on ( st.TARIFF_TREATMENT  = sq.TARIFF_TREATMENT and
             st.LANG              = sq.LANG and
             svc_tariff_treatment_tl_col(i).ACTION IN (CORESVC_HTS_SETUP.action_mod,
                                                       CORESVC_HTS_SETUP.action_del))
         when matched then
            update
               set process_id              = svc_TARIFF_TREATMENT_TL_col(i).process_id ,
                   chunk_id                = svc_TARIFF_TREATMENT_TL_col(i).chunk_id ,
                   row_seq                 = svc_TARIFF_TREATMENT_TL_col(i).row_seq ,
                   action                  = svc_TARIFF_TREATMENT_TL_col(i).action ,
                   process$status          = svc_TARIFF_TREATMENT_TL_col(i).process$status ,
                   tariff_treatment_desc   = sq.tariff_treatment_desc
         when NOT matched then
            insert(process_id,
                   chunk_id ,
                   row_seq ,
                   action ,
                   process$status ,
                   lang ,
                   tariff_treatment ,
                   tariff_treatment_desc)
            values(svc_TARIFF_TREATMENT_TL_col(i).process_id ,
                   svc_TARIFF_TREATMENT_TL_col(i).chunk_id ,
                   svc_TARIFF_TREATMENT_TL_col(i).row_seq ,
                   svc_TARIFF_TREATMENT_TL_col(i).action ,
                   svc_TARIFF_TREATMENT_TL_col(i).process$status ,
                   sq.lang ,
                   sq.tariff_treatment ,
                   sq.tariff_treatment_desc);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T', L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            TARIFF_TREATMENT_TL_sheet,
                            svc_TARIFF_TREATMENT_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TARIFF_TRTMNT_TL;
--------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_CHAPTER( I_FILE_ID    IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id IN   SVC_HTS_CHAPTER.PROCESS_ID%TYPE) IS
   TYPE svc_HTS_CHAPTER_col_typ IS TABLE OF SVC_HTS_CHAPTER%ROWTYPE;
   L_temp_rec                               SVC_HTS_CHAPTER%ROWTYPE;
   svc_HTS_CHAPTER_col                      svc_HTS_CHAPTER_col_typ           :=NEW svc_HTS_CHAPTER_col_typ();
   L_process_id                             SVC_HTS_CHAPTER.PROCESS_ID%TYPE;
   L_error                                  BOOLEAN                           :=FALSE;
   L_default_rec                            SVC_HTS_CHAPTER%ROWTYPE;

   cursor C_MANDATORY_IND is
      select
             IMPORT_COUNTRY_ID_mi,
             CHAPTER_DESC_mi,
             CHAPTER_mi,
             1 as dummy
        from (SELECT column_key,
                     mandatory
                FROM s9t_tmpl_cols_def
               WHERE template_key                                = CORESVC_HTS_SETUP.template_key
                 AND wksht_key                                   = 'HTS_CHAPTER'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                            'CHAPTER_DESC'      as CHAPTER_DESC,
                                            'CHAPTER'           as CHAPTER,
                                             null               as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_HTS_CHAPTER';
   L_pk_columns    VARCHAR2(255)  := 'HTS Heading,Import Country';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select IMPORT_COUNTRY_ID_dv,
                      CHAPTER_DESC_dv,
                      CHAPTER_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key   = CORESVC_HTS_SETUP.template_key
                          and wksht_key      = 'HTS_CHAPTER'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                      'CHAPTER_DESC'      as CHAPTER_DESC,
                                                      'CHAPTER'           as CHAPTER,
                                                       NULL               as dummy))) LOOP
   BEGIN
      L_default_rec.IMPORT_COUNTRY_ID := rec.IMPORT_COUNTRY_ID_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'HTS_CHAPTER',
                         NULL,
                         'IMPORT_COUNTRY_ID',
                         'INV_DEFAULT',
                         SQLERRM);
   END;
   BEGIN
      L_default_rec.CHAPTER_DESC := rec.CHAPTER_DESC_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'HTS_CHAPTER',
                         NULL,
                         'CHAPTER_DESC',
                         'INV_DEFAULT',
                         SQLERRM);
   END;
   BEGIN
      L_default_rec.CHAPTER := rec.CHAPTER_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'HTS_CHAPTER',
                         NULL,
                         'CHAPTER',
                         'INV_DEFAULT',
                         SQLERRM);
   END;
END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(HTS_CHAPTER$Action))            as ACTION,
                      UPPER(r.get_cell(HTS_CHAPTER$IMPORT_COUNTRY_ID)) as IMPORT_COUNTRY_ID,
                      UPPER(r.get_cell(HTS_CHAPTER$CHAPTER_DESC))      as CHAPTER_DESC,
                      r.get_cell(HTS_CHAPTER$CHAPTER)                  as CHAPTER,
                      r.get_row_seq()                                  as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(HTS_CHAPTER_sheet)) LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CHAPTER_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.IMPORT_COUNTRY_ID := rec.IMPORT_COUNTRY_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CHAPTER_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CHAPTER_DESC := rec.CHAPTER_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CHAPTER_sheet,
                            rec.row_seq,
                            'CHAPTER_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CHAPTER := rec.CHAPTER;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CHAPTER_sheet,
                            rec.row_seq,
                            'CHAPTER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_HTS_SETUP.action_new then
         L_temp_rec.IMPORT_COUNTRY_ID := NVL( L_temp_rec.IMPORT_COUNTRY_ID,
                                              L_default_rec.IMPORT_COUNTRY_ID);
         L_temp_rec.CHAPTER_DESC      := NVL( L_temp_rec.CHAPTER_DESC,
                                              L_default_rec.CHAPTER_DESC);
         L_temp_rec.CHAPTER           := NVL( L_temp_rec.CHAPTER,
                                              L_default_rec.CHAPTER);
      end if;
      if NOT (l_temp_rec.import_country_id is NOT NULL and
              L_temp_rec.chapter is NOT NULL) then
         WRITE_S9T_ERROR( I_file_id,
                          HTS_CHAPTER_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_HTS_CHAPTER_col.extend();
         svc_HTS_CHAPTER_col(svc_HTS_CHAPTER_col.COUNT()):=l_temp_rec;
      end if;
  END LOOP;
  BEGIN
  forall i IN 1..svc_HTS_CHAPTER_col.COUNT SAVE EXCEPTIONS
  merge into SVC_HTS_CHAPTER st
  using (select (case
                    when L_mi_rec.IMPORT_COUNTRY_ID_mi    = 'N'
                     and svc_HTS_CHAPTER_col(i).action = CORESVC_HTS_SETUP.action_mod
                     and s1.IMPORT_COUNTRY_ID       is NULL
                    then mt.IMPORT_COUNTRY_ID
                    else s1.IMPORT_COUNTRY_ID
                    end) AS IMPORT_COUNTRY_ID,
                (case
                    when L_mi_rec.CHAPTER_DESC_mi    = 'N'
                     and svc_HTS_CHAPTER_col(i).action = CORESVC_HTS_SETUP.action_mod
                     and s1.CHAPTER_DESC            is NULL
                    then mt.CHAPTER_DESC
                    else s1.CHAPTER_DESC
                    end) AS CHAPTER_DESC,
                (case
                    when L_mi_rec.CHAPTER_mi    = 'N'
                     and svc_HTS_CHAPTER_col(i).action = CORESVC_HTS_SETUP.action_mod
                     and s1.CHAPTER                 is NULL
                    then mt.CHAPTER
                    else s1.CHAPTER
                    end) AS CHAPTER,
                    null as dummy
            from (select svc_HTS_CHAPTER_col(i).IMPORT_COUNTRY_ID as IMPORT_COUNTRY_ID,
                         svc_HTS_CHAPTER_col(i).CHAPTER_DESC      as CHAPTER_DESC,
                         svc_HTS_CHAPTER_col(i).CHAPTER           as CHAPTER,
                         null as dummy
                    from dual) s1,
                         HTS_CHAPTER mt
                   where mt.IMPORT_COUNTRY_ID (+)     = s1.IMPORT_COUNTRY_ID   and
                         mt.CHAPTER (+)               = s1.CHAPTER and
                         1 = 1) sq
                     on (st.IMPORT_COUNTRY_ID      = sq.IMPORT_COUNTRY_ID and
                         st.CHAPTER                = sq.CHAPTER and
                         svc_HTS_CHAPTER_col(i).action IN (CORESVC_HTS_SETUP.action_mod,CORESVC_HTS_SETUP.action_del))
          when matched then
             update
                set PROCESS_ID        = svc_HTS_CHAPTER_col(i).PROCESS_ID ,
                    CHUNK_ID          = svc_HTS_CHAPTER_col(i).CHUNK_ID ,
                    ROW_SEQ           = svc_HTS_CHAPTER_col(i).ROW_SEQ ,
                    ACTION            = svc_HTS_CHAPTER_col(i).ACTION ,
                    PROCESS$STATUS    = svc_HTS_CHAPTER_col(i).PROCESS$STATUS ,
                    CHAPTER_DESC      = sq.CHAPTER_DESC ,
                    CREATE_ID         = svc_HTS_CHAPTER_col(i).CREATE_ID ,
                    CREATE_DATETIME   = svc_HTS_CHAPTER_col(i).CREATE_DATETIME ,
                    LAST_UPD_ID       = svc_HTS_CHAPTER_col(i).LAST_UPD_ID ,
                    LAST_UPD_DATETIME = svc_HTS_CHAPTER_col(i).LAST_UPD_DATETIME
          when NOT matched then
             insert (PROCESS_ID ,
                     CHUNK_ID ,
                     ROW_SEQ ,
                     ACTION ,
                     PROCESS$STATUS ,
                     IMPORT_COUNTRY_ID ,
                     CHAPTER_DESC ,
                     CHAPTER ,
                     CREATE_ID ,
                     CREATE_DATETIME ,
                     LAST_UPD_ID ,
                     LAST_UPD_DATETIME)
             values (svc_HTS_CHAPTER_col(i).PROCESS_ID ,
                     svc_HTS_CHAPTER_col(i).CHUNK_ID ,
                     svc_HTS_CHAPTER_col(i).ROW_SEQ ,
                     svc_HTS_CHAPTER_col(i).ACTION ,
                     svc_HTS_CHAPTER_col(i).PROCESS$STATUS ,
                     sq.IMPORT_COUNTRY_ID ,
                     sq.CHAPTER_DESC ,
                     sq.CHAPTER ,
                     svc_HTS_CHAPTER_col(i).CREATE_ID ,
                     svc_HTS_CHAPTER_col(i).CREATE_DATETIME ,
                     svc_HTS_CHAPTER_col(i).LAST_UPD_ID ,
                     svc_HTS_CHAPTER_col(i).LAST_UPD_DATETIME);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          HTS_CHAPTER_sheet,
                          svc_HTS_CHAPTER_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_HTS_CHAPTER;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_CHAPTER_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                     I_process_id   IN   SVC_HTS_CHAPTER_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_CHAPTER_TL_col_TYP IS TABLE OF SVC_HTS_CHAPTER_TL%ROWTYPE;
   L_temp_rec               SVC_HTS_CHAPTER_TL%ROWTYPE;
   SVC_HTS_CHAPTER_TL_col   SVC_HTS_CHAPTER_TL_col_TYP := NEW SVC_HTS_CHAPTER_TL_col_TYP();
   L_process_id             SVC_HTS_CHAPTER_TL.PROCESS_ID%TYPE;
   L_error                  BOOLEAN := FALSE;
   L_default_rec            SVC_HTS_CHAPTER_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select chapter_desc_mi,
             chapter_mi,
             import_country_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'HTS_CHAPTER_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('CHAPTER_DESC'        as chapter_desc,
                                       'CHAPTER'             as chapter,
                                       'IMPORT_COUNTRY_ID'   as import_country_id,
                                       'LANG'                as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_HTS_CHAPTER_TL';
   L_pk_columns    VARCHAR2(255)  := 'Import Country ID, Chapter, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select chapter_desc_dv,
                      chapter_dv,
                      import_country_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'HTS_CHAPTER_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('CHAPTER_DESC'       as chapter_desc,
                                                'CHAPTER'            as chapter,
                                                'IMPORT_COUNTRY_ID'  as import_country_id,
                                                'LANG'               as lang)))
   LOOP
      BEGIN
         L_default_rec.chapter_desc := rec.chapter_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CHAPTER_TL_SHEET ,
                            NULL,
                           'CHAPTER_DESC' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.chapter := rec.chapter_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CHAPTER_TL_SHEET ,
                            NULL,
                           'CHAPTER' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           HTS_CHAPTER_TL_SHEET ,
                            NULL,
                           'IMPORT_COUNTRY_ID' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CHAPTER_TL_SHEET ,
                            NULL,
                           'LANG' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(hts_chapter_TL$action))              as action,
                      UPPER(r.get_cell(hts_chapter_TL$chapter_desc))        as chapter_desc,
                      r.get_cell(hts_chapter_TL$chapter)                    as chapter,
                      UPPER(r.get_cell(hts_chapter_TL$impt_ctry_id))        as import_country_id,
                      r.get_cell(hts_chapter_TL$lang)                       as lang,
                      r.get_row_seq()                                       as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(HTS_CHAPTER_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            hts_chapter_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CHAPTER_TL_SHEET,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.chapter := rec.chapter;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CHAPTER_TL_SHEET,
                            rec.row_seq,
                            'CHAPTER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.chapter_desc := rec.chapter_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CHAPTER_TL_SHEET,
                            rec.row_seq,
                            'CHAPTER_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CHAPTER_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_HTS_SETUP.action_new then
         L_temp_rec.chapter_desc      := NVL( L_temp_rec.chapter_desc,L_default_rec.chapter_desc);
         L_temp_rec.chapter           := NVL( L_temp_rec.chapter,L_default_rec.chapter);
         L_temp_rec.import_country_id := NVL( L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.lang              := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.import_country_id is NOT NULL and
              L_temp_rec.chapter is NOT NULL and
              L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_CHAPTER_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_HTS_CHAPTER_TL_col.extend();
         SVC_HTS_CHAPTER_TL_col(SVC_HTS_CHAPTER_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..SVC_HTS_CHAPTER_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_HTS_CHAPTER_TL st
      using(select
                  (case
                   when l_mi_rec.chapter_desc_mi = 'N'
                    and SVC_HTS_CHAPTER_TL_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.chapter_desc IS NULL then
                        mt.chapter_desc
                   else s1.chapter_desc
                   end) as chapter_desc,
                  (case
                   when l_mi_rec.chapter_mi = 'N'
                    and SVC_HTS_CHAPTER_TL_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.chapter IS NULL then
                        mt.chapter
                   else s1.chapter
                   end) as chapter,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and SVC_HTS_CHAPTER_TL_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.import_country_id_mi = 'N'
                    and SVC_HTS_CHAPTER_TL_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.import_country_id IS NULL then
                        mt.import_country_id
                   else s1.import_country_id
                   end) as import_country_id
              from (select SVC_HTS_CHAPTER_TL_col(i).chapter_desc      as chapter_desc,
                           SVC_HTS_CHAPTER_TL_col(i).chapter           as chapter,
                           SVC_HTS_CHAPTER_TL_col(i).import_country_id as import_country_id,
                           SVC_HTS_CHAPTER_TL_col(i).lang              as lang
                      from dual) s1,
                   hts_chapter_tl mt
             where mt.import_country_id (+) = s1.import_country_id
               and mt.chapter (+)    = s1.chapter
               and mt.lang (+)       = s1.lang) sq
                on (st.import_country_id = sq.import_country_id and
                    st.chapter = sq.chapter and
                    st.lang = sq.lang and
                    SVC_HTS_CHAPTER_TL_col(i).ACTION IN (CORESVC_HTS_SETUP.action_mod,CORESVC_HTS_SETUP.action_del))
      when matched then
      update
         set process_id        = SVC_HTS_CHAPTER_TL_col(i).process_id ,
             chunk_id          = SVC_HTS_CHAPTER_TL_col(i).chunk_id ,
             row_seq           = SVC_HTS_CHAPTER_TL_col(i).row_seq ,
             action            = SVC_HTS_CHAPTER_TL_col(i).action ,
             process$status    = SVC_HTS_CHAPTER_TL_col(i).process$status ,
             chapter_desc      = sq.chapter_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             chapter_desc ,
             chapter ,
             import_country_id ,
             lang)
      values(SVC_HTS_CHAPTER_TL_col(i).process_id ,
             SVC_HTS_CHAPTER_TL_col(i).chunk_id ,
             SVC_HTS_CHAPTER_TL_col(i).row_seq ,
             SVC_HTS_CHAPTER_TL_col(i).action ,
             SVC_HTS_CHAPTER_TL_col(i).process$status ,
             sq.chapter_desc ,
             sq.chapter ,
             sq.import_country_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            HTS_CHAPTER_TL_SHEET,
                            SVC_HTS_CHAPTER_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_CHAPTER_TL;
----------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_CR(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                             I_process_id   IN   SVC_HTS_CHAPTER_RESTRAINTS.PROCESS_ID%TYPE) IS

   TYPE svc_HTS_CR_col_typ IS TABLE OF SVC_HTS_CHAPTER_RESTRAINTS%ROWTYPE;
   L_temp_rec       SVC_HTS_CHAPTER_RESTRAINTS%ROWTYPE;
   svc_HTS_CR_col   svc_HTS_CR_col_typ := NEW svc_HTS_CR_col_typ();
   L_process_id     SVC_HTS_CHAPTER_RESTRAINTS.PROCESS_ID%TYPE;
   L_error          BOOLEAN := FALSE;
   L_default_rec    SVC_HTS_CHAPTER_RESTRAINTS%ROWTYPE;

   cursor C_MANDATORY_IND is
      select RESTRAINT_SUFFIX_mi,
             QUOTA_CAT_mi,
             CLOSING_DATE_mi,
             UOM_mi,
             RESTRAINT_QTY_mi,
             RESTRAINT_DESC_mi,
             RESTRAINT_TYPE_mi,
             ORIGIN_COUNTRY_ID_mi,
             IMPORT_COUNTRY_ID_mi,
             CHAPTER_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key = template_key
                 and wksht_key    = 'HTS_CHAPTER_RESTRAINTS')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('RESTRAINT_SUFFIX'  as RESTRAINT_SUFFIX,
                                       'QUOTA_CAT'         as QUOTA_CAT,
                                       'CLOSING_DATE'      as CLOSING_DATE,
                                       'UOM'               as UOM,
                                       'RESTRAINT_QTY'     as RESTRAINT_QTY,
                                       'RESTRAINT_DESC'    as RESTRAINT_DESC,
                                       'RESTRAINT_TYPE'    as RESTRAINT_TYPE,
                                       'ORIGIN_COUNTRY_ID' as ORIGIN_COUNTRY_ID,
                                       'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                       'CHAPTER'           as CHAPTER,
                                       null                as dummy));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_HTS_CHAPTER_RESTRAINTS';
   L_pk_columns    VARCHAR2(255)  := 'Chapter,Importing Country,Country Of Sourcing,Type';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
   -- Get default values.
   FOR rec IN (select RESTRAINT_SUFFIX_dv,
                      QUOTA_CAT_dv,
                      CLOSING_DATE_dv,
                      UOM_dv,
                      RESTRAINT_QTY_dv,
                      RESTRAINT_DESC_dv,
                      RESTRAINT_TYPE_dv,
                      ORIGIN_COUNTRY_ID_dv,
                      IMPORT_COUNTRY_ID_dv,
                      CHAPTER_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key     = template_key
                          and wksht_key        = 'HTS_CHAPTER_RESTRAINTS')
                        PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ('RESTRAINT_SUFFIX'  as RESTRAINT_SUFFIX,
                                                     'QUOTA_CAT'         as QUOTA_CAT,
                                                     'CLOSING_DATE'      as CLOSING_DATE,
                                                     'UOM'               as UOM,
                                                     'RESTRAINT_QTY'     as RESTRAINT_QTY,
                                                     'RESTRAINT_DESC'    as RESTRAINT_DESC,
                                                     'RESTRAINT_TYPE'    as RESTRAINT_TYPE,
                                                     'ORIGIN_COUNTRY_ID' as ORIGIN_COUNTRY_ID,
                                                     'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                     'CHAPTER'           as CHAPTER,
                                                     NULL                as dummy)))
   LOOP
      BEGIN
         L_default_rec.RESTRAINT_SUFFIX := rec.RESTRAINT_SUFFIX_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_CHAPTER_RESTRAINTS',
                            NULL,
                           'RESTRAINT_SUFFIX',
                           'INV_DEFAULT',
                            SQLERRM);
      END;

      BEGIN
         L_default_rec.QUOTA_CAT := rec.QUOTA_CAT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_CHAPTER_RESTRAINTS',
                            NULL,
                           'QUOTA_CAT',
                           'INV_DEFAULT',
                            SQLERRM);
      END;

      BEGIN
         L_default_rec.CLOSING_DATE := rec.CLOSING_DATE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_CHAPTER_RESTRAINTS',
                            NULL,
                           'CLOSING_DATE',
                           'INV_DEFAULT',
                            SQLERRM);
      END;

      BEGIN
         L_default_rec.UOM := rec.UOM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_CHAPTER_RESTRAINTS',
                            NULL,
                           'UOM ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;

      BEGIN
         L_default_rec.RESTRAINT_QTY := rec.RESTRAINT_QTY_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_CHAPTER_RESTRAINTS',
                            NULL,
                           'RESTRAINT_QTY',
                           'INV_DEFAULT',
                            SQLERRM);
      END;

      BEGIN
         L_default_rec.RESTRAINT_DESC := rec.RESTRAINT_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_CHAPTER_RESTRAINTS',
                            NULL,
                           'RESTRAINT_DESC',
                           'INV_DEFAULT',
                            SQLERRM);
      END;

      BEGIN
         L_default_rec.RESTRAINT_TYPE := rec.RESTRAINT_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_CHAPTER_RESTRAINTS',
                            NULL,
                           'RESTRAINT_TYPE',
                           'INV_DEFAULT',
                            SQLERRM);
      END;

      BEGIN
         L_default_rec.ORIGIN_COUNTRY_ID := rec.ORIGIN_COUNTRY_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_CHAPTER_RESTRAINTS',
                            NULL,
                           'ORIGIN_COUNTRY_ID',
                           'INV_DEFAULT',
                            SQLERRM);
      END;

      BEGIN
         L_default_rec.IMPORT_COUNTRY_ID := rec.IMPORT_COUNTRY_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_CHAPTER_RESTRAINTS',
                            NULL,
                           'IMPORT_COUNTRY_ID',
                           'INV_DEFAULT',
                            SQLERRM);
      END;

      BEGIN
         L_default_rec.CHAPTER := rec.CHAPTER_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HTS_CHAPTER_RESTRAINTS',
                            NULL,
                           'CHAPTER' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(HTS_CR$ACTION))             as ACTION,
                      r.get_cell(HTS_CR$RESTRAINT_SUFFIX)          as RESTRAINT_SUFFIX,
                      r.get_cell(HTS_CR$QUOTA_CAT)                 as QUOTA_CAT,
                      r.get_cell(HTS_CR$CLOSING_DATE)              as CLOSING_DATE,
                      UPPER(r.get_cell(HTS_CR$UOM))                as UOM,
                      r.get_cell(HTS_CR$RESTRAINT_QTY)             as RESTRAINT_QTY,
                      r.get_cell(HTS_CR$RESTRAINT_DESC)            as RESTRAINT_DESC,
                      r.get_cell(HTS_CR$RESTRAINT_TYPE)            as RESTRAINT_TYPE,
                      r.get_cell(HTS_CR$RESTRAINT_TYPE_UPD)        as RESTRAINT_TYPE_UPD,
                      UPPER(r.get_cell(HTS_CR$ORIGIN_COUNTRY_ID))  as ORIGIN_COUNTRY_ID,
                      UPPER(r.get_cell(HTS_CR$IMPORT_COUNTRY_ID))  as IMPORT_COUNTRY_ID,
                      r.get_cell(HTS_CR$CHAPTER)                   as CHAPTER,
                      r.get_row_seq()                              as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(HTS_CR_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;

      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.RESTRAINT_SUFFIX := rec.RESTRAINT_SUFFIX;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_sheet,
                            rec.row_seq,
                            'RESTRAINT_SUFFIX',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.QUOTA_CAT := rec.QUOTA_CAT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_sheet,
                            rec.row_seq,
                            'QUOTA_CAT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.CLOSING_DATE := rec.CLOSING_DATE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_sheet,
                            rec.row_seq,
                            'CLOSING_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.UOM := UPPER(rec.UOM);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_sheet,
                            rec.row_seq,
                            'UOM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.RESTRAINT_QTY := rec.RESTRAINT_QTY;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_sheet,
                            rec.row_seq,
                            'RESTRAINT_QTY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.RESTRAINT_DESC := rec.RESTRAINT_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_sheet,
                            rec.row_seq,
                            'RESTRAINT_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.RESTRAINT_TYPE := rec.RESTRAINT_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_sheet,
                            rec.row_seq,
                            'RESTRAINT_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.RESTRAINT_TYPE_UPD := rec.RESTRAINT_TYPE_UPD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_sheet,
                            rec.row_seq,
                            'RESTRAINT_TYPE_UPD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.ORIGIN_COUNTRY_ID := UPPER(rec.ORIGIN_COUNTRY_ID);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_sheet,
                            rec.row_seq,
                            'ORIGIN_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.IMPORT_COUNTRY_ID := UPPER(rec.IMPORT_COUNTRY_ID);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.CHAPTER := rec.CHAPTER;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_sheet,
                            rec.row_seq,
                            'CHAPTER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_HTS_SETUP.action_new then
         L_temp_rec.RESTRAINT_SUFFIX  := NVL(L_temp_rec.RESTRAINT_SUFFIX,L_default_rec.RESTRAINT_SUFFIX);
         L_temp_rec.QUOTA_CAT         := NVL(L_temp_rec.QUOTA_CAT,L_default_rec.QUOTA_CAT);
         L_temp_rec.CLOSING_DATE      := NVL(L_temp_rec.CLOSING_DATE,L_default_rec.CLOSING_DATE);
         L_temp_rec.UOM               := NVL(L_temp_rec.UOM,L_default_rec.UOM);
         L_temp_rec.RESTRAINT_QTY     := NVL(L_temp_rec.RESTRAINT_QTY,L_default_rec.RESTRAINT_QTY);
         L_temp_rec.RESTRAINT_DESC    := NVL(L_temp_rec.RESTRAINT_DESC,L_default_rec.RESTRAINT_DESC);
         L_temp_rec.RESTRAINT_TYPE    := NVL(L_temp_rec.RESTRAINT_TYPE,L_default_rec.RESTRAINT_TYPE);
         L_temp_rec.ORIGIN_COUNTRY_ID := NVL(L_temp_rec.ORIGIN_COUNTRY_ID,L_default_rec.ORIGIN_COUNTRY_ID);
         L_temp_rec.IMPORT_COUNTRY_ID := NVL(L_temp_rec.IMPORT_COUNTRY_ID,L_default_rec.IMPORT_COUNTRY_ID);
         L_temp_rec.CHAPTER           := NVL(L_temp_rec.CHAPTER,L_default_rec.CHAPTER);
      end if;

      if NOT (L_temp_rec.RESTRAINT_TYPE is NOT NULL and
              L_temp_rec.ORIGIN_COUNTRY_ID is NOT NULL and
              L_temp_rec.IMPORT_COUNTRY_ID is NOT NULL and
              L_temp_rec.CHAPTER is NOT NULL and
              1 = 1)then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_CR_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_HTS_CR_col.extend();
         svc_HTS_CR_col(svc_HTS_CR_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_HTS_CR_col.COUNT SAVE EXCEPTIONS
      merge into SVC_HTS_CHAPTER_RESTRAINTS st
      using(select
                  (case
                   when L_mi_rec.RESTRAINT_SUFFIX_mi = 'N'
                    and svc_HTS_CR_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.RESTRAINT_SUFFIX is NULL then
                           mt.RESTRAINT_SUFFIX
                   else s1.RESTRAINT_SUFFIX
                   end) as RESTRAINT_SUFFIX,
                  (case
                   when L_mi_rec.QUOTA_CAT_mi = 'N'
                    and svc_HTS_CR_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.QUOTA_CAT is NULL then
                           mt.QUOTA_CAT
                   else s1.QUOTA_CAT
                   end) as QUOTA_CAT,
                  (case
                   when L_mi_rec.CLOSING_DATE_mi = 'N'
                    and svc_HTS_CR_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.CLOSING_DATE is NULL then
                           mt.CLOSING_DATE
                   else s1.CLOSING_DATE
                   end) as CLOSING_DATE,
                  (case
                   when L_mi_rec.UOM_mi = 'N'
                    and svc_HTS_CR_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.UOM is NULL then
                           mt.UOM
                   else s1.UOM
                   end) as UOM,
                  (case
                   when L_mi_rec.RESTRAINT_QTY_mi = 'N'
                    and svc_HTS_CR_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.RESTRAINT_QTY is NULL then
                           mt.RESTRAINT_QTY
                   else s1.RESTRAINT_QTY
                   end) as RESTRAINT_QTY,
                  (case
                   when L_mi_rec.RESTRAINT_DESC_mi = 'N'
                    and svc_HTS_CR_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.RESTRAINT_DESC is NULL then
                           mt.RESTRAINT_DESC
                   else s1.RESTRAINT_DESC
                   end) as RESTRAINT_DESC,
                  (case
                   when L_mi_rec.RESTRAINT_TYPE_mi = 'N'
                    and svc_HTS_CR_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.RESTRAINT_TYPE is NULL then
                           mt.RESTRAINT_TYPE
                   else s1.RESTRAINT_TYPE
                   end) as RESTRAINT_TYPE,
                  (case
                   when L_mi_rec.ORIGIN_COUNTRY_ID_mi = 'N'
                    and svc_HTS_CR_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.ORIGIN_COUNTRY_ID is NULL then
                           mt.ORIGIN_COUNTRY_ID
                   else s1.ORIGIN_COUNTRY_ID
                   end) as ORIGIN_COUNTRY_ID,
                  (case
                   when L_mi_rec.IMPORT_COUNTRY_ID_mi = 'N'
                    and svc_HTS_CR_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.IMPORT_COUNTRY_ID is NULL then
                           mt.IMPORT_COUNTRY_ID
                   else s1.IMPORT_COUNTRY_ID
                   end) as IMPORT_COUNTRY_ID,
                  (case
                   when L_mi_rec.CHAPTER_mi = 'N'
                    and svc_HTS_CR_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.CHAPTER is NULL then
                           mt.CHAPTER
                   else s1.CHAPTER
                   end) as CHAPTER,
                  null as dummy
              from (select svc_HTS_CR_col(i).RESTRAINT_SUFFIX  as RESTRAINT_SUFFIX,
                           svc_HTS_CR_col(i).QUOTA_CAT         as QUOTA_CAT,
                           svc_HTS_CR_col(i).CLOSING_DATE      as CLOSING_DATE,
                           svc_HTS_CR_col(i).UOM               as UOM,
                           svc_HTS_CR_col(i).RESTRAINT_QTY     as RESTRAINT_QTY,
                           svc_HTS_CR_col(i).RESTRAINT_DESC    as RESTRAINT_DESC,
                           svc_HTS_CR_col(i).RESTRAINT_TYPE    as RESTRAINT_TYPE,
                           svc_HTS_CR_col(i).ORIGIN_COUNTRY_ID as ORIGIN_COUNTRY_ID,
                           svc_HTS_CR_col(i).IMPORT_COUNTRY_ID as IMPORT_COUNTRY_ID,
                           svc_HTS_CR_col(i).CHAPTER           as CHAPTER,
                           null as dummy
                      from dual ) s1,
                   HTS_CHAPTER_RESTRAINTS mt
             where mt.RESTRAINT_TYPE (+)    = s1.RESTRAINT_TYPE
               and mt.ORIGIN_COUNTRY_ID (+) = s1.ORIGIN_COUNTRY_ID
               and mt.IMPORT_COUNTRY_ID (+) = s1.IMPORT_COUNTRY_ID
               and mt.CHAPTER (+)           = s1.CHAPTER
               and 1 = 1) sq
                on (st.RESTRAINT_TYPE     = sq.RESTRAINT_TYPE and
                    st.ORIGIN_COUNTRY_ID  = sq.ORIGIN_COUNTRY_ID and
                    st.IMPORT_COUNTRY_ID  = sq.IMPORT_COUNTRY_ID and
                    st.CHAPTER            = sq.CHAPTER and
                    svc_HTS_CR_col(i).ACTION IN (CORESVC_HTS_SETUP.action_mod,CORESVC_HTS_SETUP.action_del))
      when matched then
      update
         set process_id         = svc_HTS_CR_col(i).process_id,
             chunk_id           = svc_HTS_CR_col(i).chunk_id,
             row_seq            = svc_HTS_CR_col(i).row_seq,
             action             = svc_HTS_CR_col(i).action,
             process$status     = svc_HTS_CR_col(i).process$status,
             restraint_suffix   = sq.restraint_suffix,
             closing_date       = sq.closing_date,
             quota_cat          = sq.quota_cat,
             uom                = sq.uom,
             restraint_desc     = sq.restraint_desc,
             restraint_qty      = sq.restraint_qty,
             restraint_type_upd = svc_HTS_CR_col(i).restraint_type_upd,
             create_id          = svc_HTS_CR_col(i).create_id,
             create_datetime    = svc_HTS_CR_col(i).create_datetime,
             last_upd_id        = svc_HTS_CR_col(i).last_upd_id,
             last_upd_datetime  = svc_HTS_CR_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id,
             row_seq,
             action,
             process$status,
             restraint_suffix,
             quota_cat,
             closing_date,
             uom,
             restraint_qty,
             restraint_desc,
             restraint_type,
             restraint_type_upd,
             origin_country_id,
             import_country_id,
             chapter,
             create_id,
             create_datetime,
             last_upd_id,
             last_upd_datetime)
      values(svc_HTS_CR_col(i).process_id,
             svc_HTS_CR_col(i).chunk_id,
             svc_HTS_CR_col(i).row_seq,
             svc_HTS_CR_col(i).action,
             svc_HTS_CR_col(i).process$status,
             sq.restraint_suffix,
             sq.quota_cat,
             sq.closing_date,
             sq.uom,
             sq.restraint_qty,
             sq.restraint_desc,
             sq.restraint_type,
             svc_HTS_CR_col(i).restraint_type_upd,
             sq.origin_country_id,
             sq.import_country_id,
             sq.chapter,
             svc_HTS_CR_col(i).create_id,
             svc_HTS_CR_col(i).create_datetime,
             svc_HTS_CR_col(i).last_upd_id,
             svc_HTS_CR_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             HTS_CR_sheet,
                             svc_HTS_CR_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_CR;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_CR_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                I_process_id   IN   SVC_HTS_CHAPTER_RESTRAINTS_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_CR_TL_COL_TYP IS TABLE OF SVC_HTS_CHAPTER_RESTRAINTS_TL%ROWTYPE;
   L_temp_rec            SVC_HTS_CHAPTER_RESTRAINTS_TL%ROWTYPE;
   SVC_HTS_CR_TL_COL     SVC_HTS_CR_TL_COL_TYP := NEW SVC_HTS_CR_TL_COL_TYP();
   L_process_id          SVC_HTS_CHAPTER_RESTRAINTS_TL.PROCESS_ID%TYPE;
   L_error               BOOLEAN := FALSE;
   L_default_rec         SVC_HTS_CHAPTER_RESTRAINTS_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select restraint_desc_mi,
             chapter_mi,
             import_country_id_mi,
             origin_country_id_mi,
             restraint_type_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'HTS_CHAPTER_RESTRAINTS_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('RESTRAINT_DESC'     as restraint_desc,
                                       'CHAPTER'            as chapter,
                                       'IMPORT_COUNTRY_ID'  as import_country_id,
                                       'ORIGIN_COUNTRY_ID'  as origin_country_id,
                                       'RESTRAINT_TYPE'     as restraint_type,
                                       'LANG'               as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_HTS_CHAPTER_RESTRAINTS';
   L_pk_columns    VARCHAR2(255)  := 'Chapter, Import Country, Origin Country, Restraint Type, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select restraint_desc_dv,
                      chapter_dv,
                      import_country_id_dv,
                      origin_country_id_dv,
                      restraint_type_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'HTS_CHAPTER_RESTRAINTS_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('RESTRAINT_DESC'     as restraint_desc,
                                                'CHAPTER'            as chapter,
                                                'IMPORT_COUNTRY_ID'  as import_country_id,
                                                'ORIGIN_COUNTRY_ID'  as origin_country_id,
                                                'RESTRAINT_TYPE'     as restraint_type,
                                                'LANG'               as lang)))
   LOOP
      BEGIN
         L_default_rec.restraint_desc := rec.restraint_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET ,
                            NULL,
                            'RESTRAINT_DESC' ,
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.chapter := rec.chapter_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET ,
                            NULL,
                            'CHAPTER' ,
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET ,
                            NULL,
                            'IMPORT_COUNTRY_ID' ,
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.origin_country_id := rec.origin_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET ,
                            NULL,
                            'ORIGIN_COUNTRY_ID' ,
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.restraint_type := rec.restraint_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET ,
                            NULL,
                            'RESTRAINT_TYPE' ,
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET ,
                            NULL,
                           'LANG' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(HTS_CR_TL$ACTION))             as action,
                      r.get_cell(HTS_CR_TL$LANG)                      as lang,
                      r.get_cell(HTS_CR_TL$CHAPTER)                   as chapter,
                      UPPER(r.get_cell(HTS_CR_TL$IMPORT_COUNTRY_ID))  as import_country_id,
                      UPPER(r.get_cell(HTS_CR_TL$ORIGIN_COUNTRY_ID))  as origin_country_id,
                      r.get_cell(HTS_CR_TL$RESTRAINT_TYPE)            as restraint_type,
                      r.get_cell(HTS_CR_TL$RESTRAINT_DESC)            as restraint_desc,
                      r.get_row_seq()                                 as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(HTS_CR_TL_SHEET))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.chapter := rec.chapter;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET,
                            rec.row_seq,
                            'CHAPTER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := UPPER(rec.import_country_id);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.origin_country_id := UPPER(rec.origin_country_id);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET,
                            rec.row_seq,
                            'ORIGIN_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.restraint_type := rec.restraint_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET,
                            rec.row_seq,
                            'RESTRAINT_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.restraint_desc := rec.restraint_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CR_TL_SHEET,
                            rec.row_seq,
                            'RESTRAINT_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_HTS_SETUP.action_new then
         L_temp_rec.lang               := NVL( L_temp_rec.lang,L_default_rec.lang);
         L_temp_rec.chapter            := NVL( L_temp_rec.chapter,L_default_rec.chapter);
         L_temp_rec.import_country_id  := NVL( L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.origin_country_id  := NVL( L_temp_rec.origin_country_id,L_default_rec.origin_country_id);
         L_temp_rec.restraint_type     := NVL( L_temp_rec.restraint_type,L_default_rec.restraint_type);
         L_temp_rec.restraint_desc     := NVL( L_temp_rec.lang,L_default_rec.restraint_desc);
      end if;
      if NOT (L_temp_rec.chapter is NOT NULL and
              L_temp_rec.import_country_id is NOT NULL and
              L_temp_rec.origin_country_id is NOT NULL and
              L_temp_rec.restraint_type is NOT NULL and
              L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_CR_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_HTS_CR_TL_COL.extend();
         SVC_HTS_CR_TL_COL(SVC_HTS_CR_TL_COL.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..SVC_HTS_CR_TL_COL.COUNT SAVE EXCEPTIONS
      merge into svc_hts_chapter_restraints_tl st
      using(select
                  (case
                   when l_mi_rec.restraint_desc_mi = 'N'
                    and SVC_HTS_CR_TL_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.restraint_desc IS NULL then
                        mt.restraint_desc
                   else s1.restraint_desc
                   end) as restraint_desc,
                  (case
                   when l_mi_rec.restraint_type_mi = 'N'
                    and SVC_HTS_CR_TL_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.restraint_type IS NULL then
                        mt.restraint_type
                   else s1.restraint_type
                   end) as restraint_type,
                  (case
                   when l_mi_rec.import_country_id_mi = 'N'
                    and SVC_HTS_CR_TL_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.import_country_id IS NULL then
                        mt.import_country_id
                   else s1.import_country_id
                   end) as import_country_id,
                  (case
                   when l_mi_rec.chapter_mi = 'N'
                    and SVC_HTS_CR_TL_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.chapter IS NULL then
                        mt.chapter
                   else s1.chapter
                   end) as chapter,
                  (case
                   when l_mi_rec.origin_country_id_mi = 'N'
                    and SVC_HTS_CR_TL_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.origin_country_id IS NULL then
                        mt.origin_country_id
                   else s1.origin_country_id
                   end) as origin_country_id,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and SVC_HTS_CR_TL_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang
              from (select SVC_HTS_CR_TL_COL(i).restraint_desc    as restraint_desc,
                           SVC_HTS_CR_TL_COL(i).restraint_type    as restraint_type,
                           SVC_HTS_CR_TL_COL(i).import_country_id as import_country_id,
                           SVC_HTS_CR_TL_COL(i).origin_country_id as origin_country_id,
                           SVC_HTS_CR_TL_COL(i).chapter           as chapter,
                           SVC_HTS_CR_TL_COL(i).lang              as lang
                      from dual) s1,
                   hts_chapter_restraints_tl mt
             where mt.lang (+)                 = s1.lang
               and mt.chapter (+)              = s1.chapter
               and mt.import_country_id (+)    = s1.import_country_id
               and mt.origin_country_id (+)    = s1.origin_country_id
               and mt.restraint_type (+)       = s1.restraint_type) sq
                on (st.lang = sq.lang and
                    st.chapter = sq.chapter and
                    st.import_country_id = sq.import_country_id and
                    st.origin_country_id = sq.origin_country_id and
                    st.restraint_type = sq.restraint_type and
                    SVC_HTS_CR_TL_COL(i).ACTION IN (CORESVC_HTS_SETUP.action_mod,CORESVC_HTS_SETUP.action_del))
      when matched then
      update
         set process_id        = SVC_HTS_CR_TL_COL(i).process_id ,
             chunk_id          = SVC_HTS_CR_TL_COL(i).chunk_id ,
             row_seq           = SVC_HTS_CR_TL_COL(i).row_seq ,
             action            = SVC_HTS_CR_TL_COL(i).action ,
             process$status    = SVC_HTS_CR_TL_COL(i).process$status ,
             restraint_desc    = sq.restraint_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             lang ,
             chapter ,
             import_country_id ,
             origin_country_id ,
             restraint_type ,
             restraint_desc)
      values(SVC_HTS_CR_TL_COL(i).process_id ,
             SVC_HTS_CR_TL_COL(i).chunk_id ,
             SVC_HTS_CR_TL_COL(i).row_seq ,
             SVC_HTS_CR_TL_COL(i).action ,
             SVC_HTS_CR_TL_COL(i).process$status ,
             sq.lang ,
             sq.chapter ,
             sq.import_country_id,
             sq.origin_country_id,
             sq.restraint_type,
             sq.restraint_desc);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            HTS_CR_TL_SHEET,
                            SVC_HTS_CR_TL_COL(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_CR_TL;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_QUOTA_CAT( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id   IN   SVC_QUOTA_CATEGORY.process_id%TYPE) is

   TYPE svc_QUOTA_CAT_col_typ IS TABLE OF SVC_QUOTA_CATEGORY%ROWTYPE;
   L_temp_rec          SVC_QUOTA_CATEGORY%ROWTYPE;
   svc_QUOTA_CAT_col   svc_QUOTA_CAT_col_typ :=NEW svc_QUOTA_CAT_col_typ();
   L_process_id        SVC_QUOTA_CATEGORY.process_id%TYPE;
   L_error             BOOLEAN:=FALSE;
   L_default_rec       SVC_QUOTA_CATEGORY%ROWTYPE;
   cursor C_MANDATORY_IND is
      select QUOTA_CAT_mi,
             IMPORT_COUNTRY_ID_mi,
             CATEGORY_DESC_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'QUOTA_CATEGORY')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('QUOTA_CAT' as QUOTA_CAT,
                                       'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                       'CATEGORY_DESC' as CATEGORY_DESC,
                                        null as dummy));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_QUOTA_CATEGORY';
   L_pk_columns    VARCHAR2(255)  := 'Quota Category,Importing Country';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select  QUOTA_CAT_dv,
                       IMPORT_COUNTRY_ID_dv,
                       CATEGORY_DESC_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'QUOTA_CATEGORY')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ( 'QUOTA_CAT' as QUOTA_CAT,
                                                 'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                 'CATEGORY_DESC' as CATEGORY_DESC,
                                                 NULL as dummy)))
   LOOP
      BEGIN
         L_default_rec.QUOTA_CAT := rec.QUOTA_CAT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'QUOTA_CATEGORY',
                            NULL,
                            'QUOTA_CAT',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.IMPORT_COUNTRY_ID := rec.IMPORT_COUNTRY_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'QUOTA_CATEGORY',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CATEGORY_DESC := rec.CATEGORY_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'QUOTA_CATEGORY',
                            NULL,
                            'CATEGORY_DESC',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(QUOTA_CAT$ACTION)            as ACTION,
                      r.get_cell(QUOTA_CAT$QUOTA_CAT)         as QUOTA_CAT,
                      UPPER(r.get_cell(QUOTA_CAT$IMPORT_COUNTRY_ID)) as IMPORT_COUNTRY_ID,
                      r.get_cell(QUOTA_CAT$CATEGORY_DESC)     as CATEGORY_DESC,
                      r.get_row_seq()                         as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id    = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(QUOTA_CAT_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := false;

      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.QUOTA_CAT := rec.QUOTA_CAT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_sheet,
                            rec.row_seq,
                            'QUOTA_CAT',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.IMPORT_COUNTRY_ID := rec.IMPORT_COUNTRY_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.CATEGORY_DESC := rec.CATEGORY_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_sheet,
                            rec.row_seq,
                            'CATEGORY_DESC',
                            SQLCODE,SQLERRM);
            L_error := true;
      END;
      if rec.action = CORESVC_HTS_SETUP.action_new then
         L_temp_rec.QUOTA_CAT         := NVL( L_temp_rec.QUOTA_CAT,l_default_rec.QUOTA_CAT);
         L_temp_rec.IMPORT_COUNTRY_ID := NVL( L_temp_rec.IMPORT_COUNTRY_ID,l_default_rec.IMPORT_COUNTRY_ID);
         L_temp_rec.CATEGORY_DESC     := NVL( L_temp_rec.CATEGORY_DESC,l_default_rec.CATEGORY_DESC);
      end if;
      if not (L_temp_rec.IMPORT_COUNTRY_ID is not null and
              L_temp_rec.QUOTA_CAT is not null and
              1 = 1) then
         WRITE_S9T_ERROR( I_file_id,
                       QUOTA_CAT_sheet,
                       rec.row_seq,
                       NULL,
                       NULL,
                       SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         l_error := true;
      end if;
      if NOT l_error then
         svc_QUOTA_CAT_col.extend();
         svc_QUOTA_CAT_col(svc_QUOTA_CAT_col.COUNT()):=L_temp_rec;
      end if;

      L_temp_rec := NULL;

   END LOOP;
   BEGIN
   forall i IN 1..svc_QUOTA_CAT_col.COUNT SAVE EXCEPTIONS
   merge into SVC_QUOTA_CATEGORY st
   using(select(case
                when l_mi_rec.QUOTA_CAT_mi = 'N'
                 and svc_QUOTA_CAT_col(i).action = CORESVC_HTS_SETUP.action_mod
                 and s1.QUOTA_CAT IS NULL then
                     mt.QUOTA_CAT
                else s1.QUOTA_CAT
                end) as QUOTA_CAT,
               (case
                when l_mi_rec.IMPORT_COUNTRY_ID_mi = 'N'
                 and svc_QUOTA_CAT_col(i).action = CORESVC_HTS_SETUP.action_mod
                 and s1.IMPORT_COUNTRY_ID IS NULL then
                     mt.IMPORT_COUNTRY_ID
                else s1.IMPORT_COUNTRY_ID
                end) as IMPORT_COUNTRY_ID,
               (case
                when l_mi_rec.CATEGORY_DESC_mi = 'N'
                 and svc_QUOTA_CAT_col(i).action = CORESVC_HTS_SETUP.action_mod
                 and s1.CATEGORY_DESC IS NULL then
                     mt.CATEGORY_DESC
                else s1.CATEGORY_DESC
                end) as CATEGORY_DESC,
               null as dummy
          from (select svc_QUOTA_CAT_col(i).QUOTA_CAT as QUOTA_CAT,
                       svc_QUOTA_CAT_col(i).IMPORT_COUNTRY_ID as IMPORT_COUNTRY_ID,
                       svc_QUOTA_CAT_col(i).CATEGORY_DESC as CATEGORY_DESC,
                       null as dummy
                  from dual) s1,
               QUOTA_CATEGORY mt
         where mt.IMPORT_COUNTRY_ID (+) = s1.IMPORT_COUNTRY_ID
           and mt.QUOTA_CAT (+)         = s1.QUOTA_CAT
           and 1 = 1) sq
           ON (st.IMPORT_COUNTRY_ID = sq.IMPORT_COUNTRY_ID
           and st.QUOTA_CAT     = sq.QUOTA_CAT
           and svc_QUOTA_CAT_col(i).ACTION IN(CORESVC_HTS_SETUP.action_mod,
                                              CORESVC_HTS_SETUP.action_del))
      when matched then
      update
         set PROCESS_ID        = svc_QUOTA_CAT_col(i).PROCESS_ID ,
             CHUNK_ID          = svc_QUOTA_CAT_col(i).CHUNK_ID ,
             ROW_SEQ           = svc_QUOTA_CAT_col(i).ROW_SEQ ,
             ACTION            = svc_QUOTA_CAT_col(i).ACTION,
             PROCESS$STATUS    = svc_QUOTA_CAT_col(i).PROCESS$STATUS ,
             CATEGORY_DESC     = sq.CATEGORY_DESC ,
             CREATE_ID         = svc_QUOTA_CAT_col(i).CREATE_ID ,
             CREATE_DATETIME   = svc_QUOTA_CAT_col(i).CREATE_DATETIME ,
             LAST_UPD_ID       = svc_QUOTA_CAT_col(i).LAST_UPD_ID ,
             LAST_UPD_DATETIME = svc_QUOTA_CAT_col(i).LAST_UPD_DATETIME
      when NOT matched then
      insert( PROCESS_ID ,
              CHUNK_ID ,
              ROW_SEQ ,
              ACTION ,
              PROCESS$STATUS ,
              QUOTA_CAT ,
              IMPORT_COUNTRY_ID ,
              CATEGORY_DESC ,
              CREATE_ID ,
              CREATE_DATETIME ,
              LAST_UPD_ID ,
              LAST_UPD_DATETIME)
        VALUES( svc_QUOTA_CAT_col(i).PROCESS_ID ,
                svc_QUOTA_CAT_col(i).CHUNK_ID ,
                svc_QUOTA_CAT_col(i).ROW_SEQ ,
                svc_QUOTA_CAT_col(i).ACTION ,
                svc_QUOTA_CAT_col(i).PROCESS$STATUS ,
                sq.QUOTA_CAT ,
                sq.IMPORT_COUNTRY_ID ,
                sq.CATEGORY_DESC ,
                svc_QUOTA_CAT_col(i).CREATE_ID ,
                svc_QUOTA_CAT_col(i).CREATE_DATETIME ,
                svc_QUOTA_CAT_col(i).LAST_UPD_ID ,
                svc_QUOTA_CAT_col(i).LAST_UPD_DATETIME);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_sheet,
                            svc_QUOTA_CAT_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_QUOTA_CAT;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_QUOTA_CAT_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id   IN   SVC_QUOTA_CATEGORY_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_QUOTA_CATEGORY_TL_COL_TYP IS TABLE OF SVC_QUOTA_CATEGORY_TL%ROWTYPE;
   L_temp_rec         SVC_QUOTA_CATEGORY_TL%ROWTYPE;
   SVC_QUOTA_CATEGORY_TL_COL   SVC_QUOTA_CATEGORY_TL_COL_TYP := NEW SVC_QUOTA_CATEGORY_TL_COL_TYP();
   L_process_id       SVC_QUOTA_CATEGORY_TL.PROCESS_ID%TYPE;
   L_error            BOOLEAN := FALSE;
   L_default_rec      SVC_QUOTA_CATEGORY_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select category_desc_mi,
             import_country_id_mi,
             quota_cat_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'QUOTA_CATEGORY_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('CATEGORY_DESC'      as category_desc,
                                       'IMPORT_COUNTRY_ID'  as import_country_id,
                                       'QUOTA_CAT'          as quota_cat,
                                       'LANG'               as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_QUOTA_CATEGORY_TL';
   L_pk_columns    VARCHAR2(255)  := 'Quota Category, Import Country ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select category_desc_dv,
                      import_country_id_dv,
                      quota_cat_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'QUOTA_CATEGORY_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('CATEGORY_DESC'      as category_desc,
                                                'IMPORT_COUNTRY_ID'  as import_country_id,
                                                'QUOTA_CAT'          as quota_cat,
                                                'LANG'               as lang)))
   LOOP
      BEGIN
         L_default_rec.category_desc := rec.category_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_TL_SHEET ,
                            NULL,
                           'CATEGORY_DESC' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_TL_SHEET ,
                            NULL,
                           'IMPORT_COUNTRY_ID' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.quota_cat := rec.quota_cat_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           QUOTA_CAT_TL_SHEET ,
                            NULL,
                           'QUOTA_CAT' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_TL_SHEET ,
                            NULL,
                           'LANG' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(quota_cat_tl$action))       as action,
                      r.get_cell(quota_cat_tl$category_desc)       as category_desc,
                      r.get_cell(quota_cat_tl$import_country_id)   as import_country_id,
                      UPPER(r.get_cell(quota_cat_tl$quota_cat))    as quota_cat,
                      r.get_cell(quota_cat_tl$lang)                as lang,
                      r.get_row_seq()                              as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(QUOTA_CAT_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.category_desc := rec.category_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_TL_SHEET,
                            rec.row_seq,
                            'CATEGORY_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_TL_SHEET,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.quota_cat := rec.quota_cat;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_TL_SHEET,
                            rec.row_seq,
                            'QUOTA_CAT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_HTS_SETUP.action_new then
         L_temp_rec.category_desc     := NVL( L_temp_rec.category_desc,L_default_rec.category_desc);
         L_temp_rec.import_country_id := NVL( L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.quota_cat         := NVL( L_temp_rec.quota_cat,L_default_rec.quota_cat);
         L_temp_rec.lang              := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.quota_cat is NOT NULL and L_temp_rec.import_country_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         QUOTA_CAT_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_QUOTA_CATEGORY_TL_COL.extend();
         SVC_QUOTA_CATEGORY_TL_COL(SVC_QUOTA_CATEGORY_TL_COL.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..SVC_QUOTA_CATEGORY_TL_COL.COUNT SAVE EXCEPTIONS
      merge into SVC_QUOTA_CATEGORY_TL st
      using(select
                  (case
                   when l_mi_rec.category_desc_mi = 'N'
                    and SVC_QUOTA_CATEGORY_TL_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.category_desc IS NULL then
                        mt.category_desc
                   else s1.category_desc
                   end) as category_desc,
                  (case
                   when l_mi_rec.import_country_id_mi = 'N'
                    and SVC_QUOTA_CATEGORY_TL_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.import_country_id IS NULL then
                        mt.import_country_id
                   else s1.import_country_id
                   end) as import_country_id,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and SVC_QUOTA_CATEGORY_TL_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.quota_cat_mi = 'N'
                    and SVC_QUOTA_CATEGORY_TL_COL(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.quota_cat IS NULL then
                        mt.quota_cat
                   else s1.quota_cat
                   end) as quota_cat
              from (select SVC_QUOTA_CATEGORY_TL_COL(i).category_desc     as category_desc,
                           SVC_QUOTA_CATEGORY_TL_COL(i).import_country_id as import_country_id,
                           SVC_QUOTA_CATEGORY_TL_COL(i).quota_cat         as quota_cat,
                           SVC_QUOTA_CATEGORY_TL_COL(i).lang              as lang
                      from dual) s1,
                   quota_category_TL mt
             where mt.quota_cat (+) = s1.quota_cat
               and mt.import_country_id (+) = s1.import_country_id
               and mt.lang (+) = s1.lang) sq
                on (st.quota_cat = sq.quota_cat and
                    st.import_country_id = sq.import_country_id and
                    st.lang = sq.lang and
                    SVC_QUOTA_CATEGORY_TL_COL(i).ACTION IN (CORESVC_HTS_SETUP.action_mod,CORESVC_HTS_SETUP.action_del))
      when matched then
      update
         set process_id        = SVC_QUOTA_CATEGORY_TL_COL(i).process_id ,
             chunk_id          = SVC_QUOTA_CATEGORY_TL_COL(i).chunk_id ,
             row_seq           = SVC_QUOTA_CATEGORY_TL_COL(i).row_seq ,
             action            = SVC_QUOTA_CATEGORY_TL_COL(i).action ,
             process$status    = SVC_QUOTA_CATEGORY_TL_COL(i).process$status ,
             category_desc     = sq.category_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             category_desc ,
             import_country_id ,
             quota_cat ,
             lang)
      values(SVC_QUOTA_CATEGORY_TL_COL(i).process_id ,
             SVC_QUOTA_CATEGORY_TL_COL(i).chunk_id ,
             SVC_QUOTA_CATEGORY_TL_COL(i).row_seq ,
             SVC_QUOTA_CATEGORY_TL_COL(i).action ,
             SVC_QUOTA_CATEGORY_TL_COL(i).process$status ,
             sq.category_desc ,
             sq.import_country_id ,
             sq.quota_cat ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            QUOTA_CAT_TL_SHEET,
                            SVC_QUOTA_CATEGORY_TL_COL(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_QUOTA_CAT_TL;
--------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CNTR_TARIFF_TR(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                     I_process_id   IN   SVC_COUNTRY_TARIFF_TREATMENT.process_id%TYPE)IS

   TYPE svc_CNTR_TARIFF_TR_col_typ is TABLE OF SVC_COUNTRY_TARIFF_TREATMENT%ROWTYPE;
   L_temp_rec SVC_COUNTRY_TARIFF_TREATMENT%ROWTYPE;
   svc_CNTR_TARIFF_TR_col svc_CNTR_TARIFF_TR_col_typ :=NEW svc_CNTR_TARIFF_TR_col_typ();
   L_process_id SVC_COUNTRY_TARIFF_TREATMENT.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_COUNTRY_TARIFF_TREATMENT%ROWTYPE;
   cursor C_MANDATORY_IND is select EFFECTIVE_TO_mi,
                                    EFFECTIVE_FROM_mi,
                                    TARIFF_TREATMENT_mi,
                                    COUNTRY_ID_mi
                               from (select column_key,
                                            mandatory
                                       from s9t_tmpl_cols_def
                                      where template_key = CORESVC_HTS_SETUP.template_key
                                        and wksht_key    = COUNTRY_TARIFF_TREATMENT_sheet) 
                              pivot (MAX(mandatory) as mi FOR (column_key) IN ('EFFECTIVE_TO' as EFFECTIVE_TO,
                                                                               'EFFECTIVE_FROM' as EFFECTIVE_FROM,
                                                                               'TARIFF_TREATMENT' as TARIFF_TREATMENT,
                                                                               'COUNTRY_ID' as COUNTRY_ID));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dmL_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COUNTRY_TARIFF_TREATMENT';
   L_pk_columns    VARCHAR2(255)  := 'Effective From Date,Tariff Treatment,Country';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   FOR rec IN (select EFFECTIVE_TO_dv,
                      EFFECTIVE_FROM_dv,
                      TARIFF_TREATMENT_dv,
                      COUNTRY_ID_dv
                from (select column_key,
                             default_value
                        from s9t_tmpl_cols_def
                       where template_key = CORESVC_HTS_SETUP.template_key
                         and wksht_key    = COUNTRY_TARIFF_TREATMENT_sheet)
                pivot (MAX(default_value) as dv FOR (column_key) IN ('EFFECTIVE_TO' as EFFECTIVE_TO,
                                                                     'EFFECTIVE_FROM' as EFFECTIVE_FROM,
                                                                     'TARIFF_TREATMENT' as TARIFF_TREATMENT,
                                                                     'COUNTRY_ID' as COUNTRY_ID))
               )
   LOOP  
   BEGIN  
      L_default_rec.EFFECTIVE_TO := rec.EFFECTIVE_TO_dv;  
      EXCEPTION  
         when OTHERS then  
            WRITE_S9T_ERROR(I_file_id,
                            'COUNTRY_TARIFF_TREATMENT',
                            NULL,
                            'EFFECTIVE_TO',
                            'INV_DEFAULT',
                            SQLERRM);  
   END;
   BEGIN
      L_default_rec.EFFECTIVE_FROM := rec.EFFECTIVE_FROM_dv;  
      EXCEPTION  
         when OTHERS then  
            WRITE_S9T_ERROR(I_file_id,
                            'COUNTRY_TARIFF_TREATMENT',
                            NULL,
                            'EFFECTIVE_FROM',
                            'INV_DEFAULT',
                            SQLERRM);  
   END;
   BEGIN  
      L_default_rec.TARIFF_TREATMENT := rec.TARIFF_TREATMENT_dv;  
      EXCEPTION  
         when OTHERS then  
            WRITE_S9T_ERROR(I_file_id,
                            'COUNTRY_TARIFF_TREATMENT',
                            NULL,
                            'TARIFF_TREATMENT',
                            'INV_DEFAULT',
                            SQLERRM);  
   END;
   BEGIN  
      L_default_rec.COUNTRY_ID := rec.COUNTRY_ID_dv;  
      EXCEPTION  
         when OTHERS then   
            WRITE_S9T_ERROR(I_file_id,
                            'COUNTRY_TARIFF_TREATMENT',
                            NULL,
                            'COUNTRY_ID',
                            'INV_DEFAULT',
                            SQLERRM);  
   END;
END LOOP;

   OPEN C_MANDATORY_IND;
   FETCH C_MANDATORY_IND
   INTO L_mi_rec;
   CLOSE C_MANDATORY_IND;
   FOR rec IN (select r.GET_CELL(COUNTRY_TRF_TT$Action)                as Action,
                      r.GET_CELL(COUNTRY_TRF_TT$EFFECTIVE_TO)          as EFFECTIVE_TO,
                      r.GET_CELL(COUNTRY_TRF_TT$EFFECTIVE_FROM)        as EFFECTIVE_FROM,
                      UPPER(r.GET_CELL(COUNTRY_TRF_TT$TARIFF_TT))      as TARIFF_TREATMENT,
                      UPPER(r.GET_CELL(COUNTRY_TRF_TT$COUNTRY_ID))     as COUNTRY_ID,
                      r.get_row_seq()                                  as row_seq
                 from s9t_folder sf,
                      TABLE(sf.S9T_FILE_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(COUNTRY_TARIFF_TREATMENT_sheet)
              )
   LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_TARIFF_TREATMENT_sheet,
                               rec.row_seq,
                               action_column,
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.EFFECTIVE_TO := rec.EFFECTIVE_TO;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_TARIFF_TREATMENT_sheet,
                               rec.row_seq,
                               'EFFECTIVE_TO',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.EFFECTIVE_FROM := rec.EFFECTIVE_FROM;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_TARIFF_TREATMENT_sheet,
                               rec.row_seq,
                               'EFFECTIVE_FROM',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TARIFF_TREATMENT := rec.TARIFF_TREATMENT;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_TARIFF_TREATMENT_sheet,
                               rec.row_seq,
                               'TARIFF_TREATMENT',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COUNTRY_ID := rec.COUNTRY_ID;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_TARIFF_TREATMENT_sheet,
                               rec.row_seq,
                               'COUNTRY_ID',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      if rec.action = CORESVC_HTS_SETUP.action_new then
         L_temp_rec.EFFECTIVE_TO     := NVL( L_temp_rec.EFFECTIVE_TO,L_default_rec.EFFECTIVE_TO);
         L_temp_rec.EFFECTIVE_FROM   := NVL( L_temp_rec.EFFECTIVE_FROM,L_default_rec.EFFECTIVE_FROM);
         L_temp_rec.TARIFF_TREATMENT := NVL( L_temp_rec.TARIFF_TREATMENT,L_default_rec.TARIFF_TREATMENT);
         L_temp_rec.COUNTRY_ID       := NVL( L_temp_rec.COUNTRY_ID,L_default_rec.COUNTRY_ID);
      end if;
      if NOT (L_temp_rec.EFFECTIVE_FROM   is NOT NULL and
              L_temp_rec.TARIFF_TREATMENT is NOT NULL and
              L_temp_rec.COUNTRY_ID       is NOT NULL) then
         WRITE_S9T_ERROR( I_file_id,
                          COUNTRY_TARIFF_TREATMENT_sheet,
                          rec.row_seq,

                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
        L_error:=TRUE;
      end if;
      if NOT L_error then
         svc_CNTR_TARIFF_TR_col.EXTEND();
         svc_CNTR_TARIFF_TR_col(svc_CNTR_TARIFF_TR_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;
   
   BEGIN
   
   FORALL i IN 1..svc_CNTR_TARIFF_TR_col.COUNT 
   SAVE EXCEPTIONS Merge INTO SVC_COUNTRY_TARIFF_TREATMENT st USING
      (select (case
                  when L_mi_rec.EFFECTIVE_TO_mi    = 'N'
                   and svc_CNTR_TARIFF_TR_col(i).action = CORESVC_HTS_SETUP.action_mod
                   and s1.EFFECTIVE_TO             is NULL
                  then mt.EFFECTIVE_TO
                  else s1.EFFECTIVE_TO
               end) as EFFECTIVE_TO,
              (case
                  when L_mi_rec.EFFECTIVE_FROM_mi    = 'N'
                   and svc_CNTR_TARIFF_TR_col(i).action = CORESVC_HTS_SETUP.action_mod
                   and s1.EFFECTIVE_FROM           is NULL
                  then mt.EFFECTIVE_FROM
                  else s1.EFFECTIVE_FROM
               end) as EFFECTIVE_FROM,
              (case
                  when L_mi_rec.TARIFF_TREATMENT_mi    = 'N'
                   and svc_CNTR_TARIFF_TR_col(i).action = CORESVC_HTS_SETUP.action_mod
                   and s1.TARIFF_TREATMENT         is NULL
                  then mt.TARIFF_TREATMENT
                  else s1.TARIFF_TREATMENT
               end) as TARIFF_TREATMENT,
              (case
                  when L_mi_rec.COUNTRY_ID_mi    = 'N'
                   and svc_CNTR_TARIFF_TR_col(i).action = CORESVC_HTS_SETUP.action_mod
                   and s1.COUNTRY_ID               is NULL
                  then mt.COUNTRY_ID
                  else s1.COUNTRY_ID
               end) as COUNTRY_ID
         from (select svc_CNTR_TARIFF_TR_col(i).EFFECTIVE_TO as EFFECTIVE_TO,
                      svc_CNTR_TARIFF_TR_col(i).EFFECTIVE_FROM as EFFECTIVE_FROM,
                      svc_CNTR_TARIFF_TR_col(i).TARIFF_TREATMENT as TARIFF_TREATMENT,
                      svc_CNTR_TARIFF_TR_col(i).COUNTRY_ID as COUNTRY_ID
               from dual
               )s1,
                COUNTRY_TARIFF_TREATMENT mt
         where mt.EFFECTIVE_FROM (+)    = s1.EFFECTIVE_FROM   and
               mt.TARIFF_TREATMENT (+)  = s1.TARIFF_TREATMENT   and
               mt.COUNTRY_ID (+)        = s1.COUNTRY_ID) sq 
           ON (st.EFFECTIVE_FROM    = sq.EFFECTIVE_FROM and
               st.TARIFF_TREATMENT  = sq.TARIFF_TREATMENT and
               st.COUNTRY_ID        = sq.COUNTRY_ID and
               svc_CNTR_TARIFF_TR_col(i).ACTION in (CORESVC_HTS_SETUP.action_mod,CORESVC_HTS_SETUP.action_del)
              )
      when matched then
         update SET PROCESS_ID        = svc_CNTR_TARIFF_TR_col(i).PROCESS_ID ,
                    CHUNK_ID          = svc_CNTR_TARIFF_TR_col(i).CHUNK_ID ,
                    ROW_SEQ           = svc_CNTR_TARIFF_TR_col(i).ROW_SEQ ,
                    action            = svc_CNTR_TARIFF_TR_col(i).ACTION,
                    PROCESS$STATUS    = svc_CNTR_TARIFF_TR_col(i).PROCESS$STATUS ,
                    EFFECTIVE_TO      = sq.EFFECTIVE_TO ,
                    CREATE_ID         = svc_CNTR_TARIFF_TR_col(i).CREATE_ID ,
                    CREATE_DATETIME   = svc_CNTR_TARIFF_TR_col(i).CREATE_DATETIME ,
                    LAST_UPD_ID       = svc_CNTR_TARIFF_TR_col(i).LAST_UPD_ID ,
                    LAST_UPD_DATETIME = svc_CNTR_TARIFF_TR_col(i).LAST_UPD_DATETIME 
      when NOT matched then
         insert (PROCESS_ID ,
                 CHUNK_ID ,
                 ROW_SEQ ,
                 ACTION ,
                 PROCESS$STATUS ,
                 EFFECTIVE_TO ,
                 EFFECTIVE_FROM ,
                 TARIFF_TREATMENT ,
                 COUNTRY_ID ,
                 CREATE_ID ,
                 CREATE_DATETIME ,
                 LAST_UPD_ID ,
                 LAST_UPD_DATETIME)
         values (svc_CNTR_TARIFF_TR_col(i).PROCESS_ID ,
                 svc_CNTR_TARIFF_TR_col(i).CHUNK_ID ,
                 svc_CNTR_TARIFF_TR_col(i).ROW_SEQ ,
                 svc_CNTR_TARIFF_TR_col(i).ACTION ,
                 svc_CNTR_TARIFF_TR_col(i).PROCESS$STATUS ,
                 sq.EFFECTIVE_TO ,
                 sq.EFFECTIVE_FROM ,
                 sq.TARIFF_TREATMENT ,
                 sq.COUNTRY_ID ,
                 svc_CNTR_TARIFF_TR_col(i).CREATE_ID ,
                 svc_CNTR_TARIFF_TR_col(i).CREATE_DATETIME ,
                 svc_CNTR_TARIFF_TR_col(i).LAST_UPD_ID ,
                 svc_CNTR_TARIFF_TR_col(i).LAST_UPD_DATETIME);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            
            WRITE_S9T_ERROR( I_file_id,
                            COUNTRY_TARIFF_TREATMENT_sheet,
                            svc_CNTR_TARIFF_TR_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
      END LOOP;
   when OTHERS then
      if C_MANDATORY_IND%ISOPEN then
         close C_MANDATORY_IND;
      end if;
   rollback;
END;

END PROCESS_S9T_CNTR_TARIFF_TR;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_OGA( I_file_id      IN   s9t_folder.file_id%TYPE,
                           I_process_id   IN   SVC_OGA.process_id%TYPE) IS

   TYPE svc_OGA_col_typ IS TABLE OF SVC_OGA%ROWTYPE;
   L_temp_rec SVC_OGA%ROWTYPE;
   svc_OGA_col svc_OGA_col_typ :=NEW svc_OGA_col_typ();
   L_process_id SVC_OGA.process_id%TYPE;
   L_error BOOLEAN := FALSE;
   L_default_rec SVC_OGA%ROWTYPE;
   
   cursor C_MANDATORY_IND is
      select OGA_CODE_mi,
             OGA_DESC_mi,
             REQ_FORM_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = CORESVC_HTS_SETUP.template_key
                 and wksht_key    = 'OGA') 
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('OGA_CODE' as OGA_CODE,
                                                                'OGA_DESC' as OGA_DESC,
                                                                'REQ_FORM' as REQ_FORM));

   L_mi_rec        c_mandatory_ind%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_OGA';
   L_pk_columns    VARCHAR2(255)  := 'OGA Code';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  -- Get default values.
   FOR rec IN (select OGA_CODE_dv,
                      OGA_DESC_dv,
                      REQ_FORM_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_HTS_SETUP.template_key
                          and wksht_key     = 'OGA') 
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('OGA_CODE' as OGA_CODE,
                                                                             'OGA_DESC' as OGA_DESC,
                                                                             'REQ_FORM' as REQ_FORM)))
   LOOP
      BEGIN
         L_default_rec.OGA_CODE := rec.OGA_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'OGA' ,
                            NULL,
                            'OGA_CODE' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.OGA_DESC := rec.OGA_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'OGA' ,
                            NULL,
                            'OGA_DESC' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.REQ_FORM := rec.REQ_FORM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'OGA ' ,
                            NULL,
                            'REQ_FORM ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   
   ---
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   ---
   
   FOR rec IN (select r.get_cell(OGA$Action)      as Action,
                      r.get_cell(OGA$OGA_CODE)    as OGA_CODE,
                      r.get_cell(OGA$OGA_DESC)    as OGA_DESC,
                      r.get_cell(OGA$REQ_FORM)    as REQ_FORM,
                      r.get_row_seq()             as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id     = I_file_id
                  and ss.sheet_name  = GET_SHEET_NAME_TRANS(OGA_sheet))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OGA_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.OGA_CODE := rec.OGA_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OGA_sheet,
                            rec.row_seq,
                            'OGA_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.OGA_DESC := rec.OGA_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OGA_sheet,
                            rec.row_seq,
                            'OGA_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.REQ_FORM := rec.REQ_FORM;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OGA_sheet,
                            rec.row_seq,
                            'REQ_FORM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      ---
      if rec.action = CORESVC_HTS_SETUP.action_new then
         L_temp_rec.OGA_CODE := NVL( L_temp_rec.OGA_CODE,
                                     L_default_rec.OGA_CODE);
         L_temp_rec.OGA_DESC := NVL( L_temp_rec.OGA_DESC,
                                     L_default_rec.OGA_DESC);
         L_temp_rec.REQ_FORM := NVL( L_temp_rec.REQ_FORM,
                                     L_default_rec.REQ_FORM);
      end if;
      ---

      ---
      if not (L_temp_rec.OGA_CODE is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         OGA_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      ---

      if NOT L_error then
         svc_OGA_col.extend();
         svc_OGA_col(svc_OGA_col.COUNT()) := L_temp_rec;
      end if;

   END LOOP;
   BEGIN
      forall i IN 1..svc_OGA_col.COUNT SAVE EXCEPTIONS
      merge into SVC_OGA st
      using(select(case
                   when l_mi_rec.OGA_CODE_mi    = 'N'
                    and svc_OGA_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.OGA_CODE IS NULL
                   then mt.OGA_CODE
                   else s1.OGA_CODE
                   end) AS OGA_CODE,
                  (case
                   when l_mi_rec.OGA_DESC_mi    = 'N'
                    and svc_OGA_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.OGA_DESC IS NULL
                   then mt.OGA_DESC
                   else s1.OGA_DESC
                   end) AS OGA_DESC,
                  (case
                   when l_mi_rec.REQ_FORM_mi    = 'N'
                    and svc_OGA_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.REQ_FORM IS NULL
                   then mt.REQ_FORM
                   else s1.REQ_FORM
                   end) AS REQ_FORM,
                  null as dummy
              from (select svc_OGA_col(i).OGA_CODE AS OGA_CODE,
                           svc_OGA_col(i).OGA_DESC AS OGA_DESC,
                           svc_OGA_col(i).REQ_FORM AS REQ_FORM,
                           null as dummy
                      from dual ) s1,
                   OGA mt
             where mt.OGA_CODE (+)     = s1.OGA_CODE)sq
                on (st.OGA_CODE      = sq.OGA_CODE and
                    svc_OGA_col(i).ACTION IN (CORESVC_HTS_SETUP.action_mod,CORESVC_HTS_SETUP.action_del))
      when matched then
      update
         set process_id         = svc_OGA_col(i).process_id ,
             chunk_id           = svc_OGA_col(i).chunk_id ,
             row_seq            = svc_OGA_col(i).row_seq ,
             action             = svc_OGA_col(i).action ,
             process$status     = svc_OGA_col(i).process$status ,
             oga_desc           = sq.oga_desc ,
             req_form           = sq.req_form ,
             create_id          = svc_OGA_col(i).create_id ,
             create_datetime    = svc_OGA_col(i).create_datetime ,
             last_upd_id        = svc_OGA_col(i).last_upd_id ,
             last_upd_datetime  = svc_OGA_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             oga_code ,
             oga_desc ,
             req_form ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_OGA_col(i).process_id ,
             svc_OGA_col(i).chunk_id ,
             svc_OGA_col(i).row_seq ,
             svc_OGA_col(i).action ,
             svc_OGA_col(i).process$status ,
             sq.oga_code ,
             sq.oga_desc ,
             sq.req_form ,
             svc_OGA_col(i).create_id ,
             svc_OGA_col(i).create_datetime ,
             svc_OGA_col(i).last_upd_id ,
             svc_OGA_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP

            ---
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            ---

            WRITE_S9T_ERROR(I_file_id,
                            OGA_sheet,
                            svc_OGA_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_OGA;
--------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_OGA_TL( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                              I_process_id IN   SVC_OGA_TL.PROCESS_ID%TYPE) IS
   
   L_temp_rec      SVC_OGA_TL%ROWTYPE;
   L_process_id    SVC_OGA_TL.process_id%TYPE;
   L_error         BOOLEAN:=FALSE;
   L_default_rec   SVC_OGA_TL%ROWTYPE;
   
   TYPE svc_OGA_TL_col_typ IS TABLE OF SVC_OGA_TL%ROWTYPE;
   svc_OGA_TL_col svc_OGA_TL_col_typ :=NEW svc_OGA_TL_col_typ();
   
   cursor C_MANDATORY_IND is
      select LANG_mi,
             OGA_CODE_mi,
             OGA_DESC_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key   = template_key
                 and wksht_key      = 'OGA_TL'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('LANG' as LANG,
                                            'OGA_CODE' as OGA_CODE,
                                            'OGA_DESC' as OGA_DESC ));
   L_mi_rec c_mandatory_ind%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select LANG_dv,
                      OGA_CODE_dv,
                      OGA_DESC_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = template_key
                          and wksht_key     = 'OGA_TL') 
                       PIVOT (MAX(default_value) as dv FOR (column_key) IN ('LANG'     as LANG,
                                                                            'OGA_CODE' as OGA_CODE,
                                                                            'OGA_DESC' as OGA_DESC)))
   LOOP
      BEGIN
         L_default_rec.OGA_DESC := rec.OGA_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'OGA_TL' ,
                            NULL,
                            'OGA_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.OGA_CODE := rec.OGA_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'OGA_TL' ,
                            NULL,
                            'OGA_CODE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'OGA_TL',
                            NULL,
                           'LANG',
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(OGA_TL$Action)    as Action,
                      r.get_cell(OGA_TL$LANG)      as LANG,
                      r.get_cell(OGA_TL$OGA_CODE)  as OGA_CODE,
                      r.get_cell(OGA_TL$OGA_DESC)  as OGA_DESC,
                      r.get_row_seq()              as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(OGA_TL_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OGA_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.OGA_DESC := rec.OGA_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OGA_TL_sheet,
                            rec.row_seq,
                            'OGA_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.OGA_CODE := rec.OGA_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OGA_TL_sheet,
                            rec.row_seq,
                            'OGA_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OGA_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_HTS_SETUP.action_new then
         L_temp_rec.LANG     := NVL( L_temp_rec.LANG,L_default_rec.LANG);
         L_temp_rec.OGA_CODE := NVL( L_temp_rec.OGA_CODE,L_default_rec.OGA_CODE);
         L_temp_rec.OGA_DESC := NVL( L_temp_rec.OGA_DESC,L_default_rec.OGA_DESC);
      end if;
      if not (L_temp_rec.LANG is NOT NULL and
              L_temp_rec.OGA_CODE is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         OGA_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                        'S9T_OGA_TL_PK');
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_OGA_TL_col.extend();
         svc_OGA_TL_col(svc_OGA_TL_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_OGA_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_OGA_TL st
      using(select(case
                   when l_mi_rec.OGA_DESC_mi    = 'N'
                    and svc_OGA_TL_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.OGA_DESC IS NULL
                   then mt.OGA_DESC
                   else s1.OGA_DESC
                   end) AS OGA_DESC,
                  (case
                   when l_mi_rec.OGA_CODE_mi    = 'N'
                    and svc_OGA_TL_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.OGA_CODE IS NULL
                   then mt.OGA_CODE
                   else s1.OGA_CODE
                   end) AS OGA_CODE,
                  (case
                   when l_mi_rec.LANG_mi    = 'N'
                    and svc_OGA_TL_col(i).action = CORESVC_HTS_SETUP.action_mod
                    and s1.LANG IS NULL
                   then mt.LANG
                   else s1.LANG
                   end) AS LANG,
                  null as dummy
              from (select svc_OGA_TL_col(i).OGA_DESC AS OGA_DESC,
                           svc_OGA_TL_col(i).OGA_CODE AS OGA_CODE,
                           svc_OGA_TL_col(i).LANG AS LANG,
                           null as dummy
                      from dual ) s1,
            OGA_TL mt
             where mt.LANG (+)     = s1.LANG   and
                   mt.OGA_CODE (+) = s1.OGA_CODE)sq
                on (st.LANG        = sq.LANG and
                    st.OGA_CODE    = sq.OGA_CODE and
                    svc_OGA_TL_col(i).ACTION IN (CORESVC_HTS_SETUP.action_mod,CORESVC_HTS_SETUP.action_del))
      when matched then
      update
         set process_id        = svc_OGA_TL_col(i).process_id ,
             chunk_id          = svc_OGA_TL_col(i).chunk_id ,
             row_seq           = svc_OGA_TL_col(i).row_seq ,
             action            = svc_OGA_TL_col(i).action ,
             process$status    = svc_OGA_TL_col(i).process$status ,
             oga_desc          = sq.oga_desc ,
             create_id         = svc_OGA_TL_col(i).create_id ,
             create_datetime   = svc_OGA_TL_col(i).create_datetime ,
             last_upd_id       = svc_OGA_TL_col(i).last_upd_id ,
             last_upd_datetime = svc_OGA_TL_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             oga_desc ,
             oga_code ,
             lang ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_OGA_TL_col(i).process_id ,
             svc_OGA_TL_col(i).chunk_id ,
             svc_OGA_TL_col(i).row_seq ,
             svc_OGA_TL_col(i).action ,
             svc_OGA_TL_col(i).process$status ,
             sq.oga_desc ,
             sq.oga_code ,
             sq.lang ,
             svc_OGA_TL_col(i).create_id ,
             svc_OGA_TL_col(i).create_datetime ,
             svc_OGA_TL_col(i).last_upd_id ,
             svc_OGA_TL_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            WRITE_S9T_ERROR(I_file_id,
                            OGA_TL_sheet,
                            svc_OGA_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            sql%bulk_exceptions(i).error_code,
                            'S9T_OGA_TL_DUP');
         END LOOP;
   END;
END PROCESS_S9T_OGA_TL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_error_count     OUT      NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64):='CORESVC_HTS_SETUP.PROCESS_S9T';
   L_file             s9t_file;
   L_sheets           s9t_pkg.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   commit;
   s9t_pkg.ods2obj(I_file_id);
   commit;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   
   if s9t_pkg.code2desc(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   
   s9t_pkg.save_obj(L_file);
   
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);

      PROCESS_S9T_HTS_IMP_CNT_SETUP(I_file_id,I_process_id);
      PROCESS_S9T_TARIFF_TREATMENT(I_file_id,I_process_id);
      PROCESS_S9T_TARIFF_TRTMNT_TL(I_file_id,I_process_id);
      PROCESS_S9T_HTS_CHAPTER(I_file_id,I_process_id);
      PROCESS_S9T_HTS_CHAPTER_TL(I_file_id,I_process_id);
      PROCESS_S9T_HTS_CR(I_file_id,I_process_id);
      PROCESS_S9T_HTS_CR_TL(I_file_id,I_process_id);
      PROCESS_S9T_QUOTA_CAT(I_file_id,I_process_id);
      PROCESS_S9T_QUOTA_CAT_TL(I_file_id,I_process_id);
      PROCESS_S9T_CNTR_TARIFF_TR(I_file_id,I_process_id);
      PROCESS_S9T_OGA(I_file_id,I_process_id);
      PROCESS_S9T_OGA_TL(I_file_id,I_process_id); 

   end if;
   
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count

         insert into s9t_errors
              values lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', 
                                             NULL, 
                                             NULL, 
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
      insert into s9t_errors
           values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_IMP_CNT_SETUP_INS(O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,                                    L_hts_imp_cnt_setup_temp_rec   IN       HTS_IMPORT_COUNTRY_SETUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_HTS_IMP_CNT_SETUP_INS';
   L_table   VARCHAR2(64):= 'SVC_HTS_IMPORT_COUNTRY_SETUP';
BEGIN
   insert into hts_import_country_setup
        values L_hts_imp_cnt_setup_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_IMP_CNT_SETUP_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_IMP_CNT_SETUP_UPD( O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     L_hts_imp_cnt_setup_temp_rec   IN       HTS_IMPORT_COUNTRY_SETUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_HTS_IMP_CNT_SETUP_UPD';
   L_table   VARCHAR2(64):= 'SVC_HTS_IMPORT_COUNTRY_SETUP';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_HTSCNTRY_DEL is
      select 'x'
        from hts_import_country_setup
       where import_country_id = L_hts_imp_cnt_setup_temp_rec.import_country_id
         for update nowait;
BEGIN

   ---
   open  C_LOCK_HTSCNTRY_DEL;
   close C_LOCK_HTSCNTRY_DEL;
   ---  
   
   update hts_import_country_setup
      set row = L_hts_imp_cnt_setup_temp_rec
    where import_country_id = L_hts_imp_cnt_setup_temp_rec.import_country_id;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_hts_imp_cnt_setup_temp_rec.import_country_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_HTSCNTRY_DEL%ISOPEN then
         close C_LOCK_HTSCNTRY_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_IMP_CNT_SETUP_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_IMP_CNT_SETUP_DEL( O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     L_hts_imp_cnt_setup_temp_rec   IN       HTS_IMPORT_COUNTRY_SETUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_HTS_IMP_CNT_SETUP_DEL';
   L_table   VARCHAR2(255):= 'SVC_HTS_IMPORT_COUNTRY_SETUP';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   
    --Cursor to lock the record
   cursor C_LOCK_HTSCNTRY_DEL is
      select 'x'
        from hts_import_country_setup
       where import_country_id = L_hts_imp_cnt_setup_temp_rec.import_country_id
         for update nowait;
BEGIN
   open  C_LOCK_HTSCNTRY_DEL;
   close C_LOCK_HTSCNTRY_DEL;
   
   delete
     from hts_import_country_setup
    where import_country_id = L_hts_imp_cnt_setup_temp_rec.import_country_id;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_hts_imp_cnt_setup_temp_rec.import_country_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_HTSCNTRY_DEL%ISOPEN then
         close C_LOCK_HTSCNTRY_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_IMP_CNT_SETUP_DEL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_IMP_CNT_SETUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN       SVC_HTS_IMPORT_COUNTRY_SETUP.PROCESS_ID%TYPE,
                                   I_chunk_id        IN       SVC_HTS_IMPORT_COUNTRY_SETUP.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_process_error   BOOLEAN        := FALSE;
   L_program         VARCHAR2(64)   :='CORESVC_HTS_SETUP.PROCESS_HTS_IMP_CNT_SETUP';
   L_table           VARCHAR2(64)   :='SVC_HTS_IMPORT_COUNTRY_SETUP';
   L_error_message   VARCHAR2(600);
   L_error           BOOLEAN;

   L_hts_imp_cnt_setup_temp_rec HTS_IMPORT_COUNTRY_SETUP%ROWTYPE;

BEGIN
   FOR rec IN c_svc_HTS_IMPORT_COUNTRY_SETUP(I_process_id,
                                             I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

      ---
      if rec.action is NULL
         or rec.action NOT IN (action_new,
                               action_mod,
                               action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      ---

      if rec.action = action_new
         and rec.pk_hts_import_country_setu_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     'HTS_IMPCTRY_EXISTS');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.PK_HTS_IMPORT_COUNTRY_SETU_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_new,action_mod)
         and rec.hics_cnt_fk_rid is NULL
         and rec.IMPORT_COUNTRY_ID is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     'INV_VALUE');
         L_error :=TRUE;
      end if;

    if NOT(rec.IMPORT_COUNTRY_ID  IS NOT NULL) 
       and rec.action in (action_mod, action_del, action_new) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     'FIELD_NOT_NULL');
         L_error :=TRUE;
    end if;

    if NOT(rec.HTS_FORMAT_MASK  IS NOT NULL) 
       and rec.action in (action_mod, action_new)then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS_FORMAT_MASK',
                     'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;
    if NOT(rec.HTS_HEADING_LENGTH  IS NOT NULL) 
       and rec.action in (action_mod, action_new)then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS_HEADING_LENGTH',
                     'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;
  
      if NOT L_error then
         L_hts_imp_cnt_setup_temp_rec.hts_heading_length   := rec.hts_heading_length;
         L_hts_imp_cnt_setup_temp_rec.hts_format_mask      := rec.hts_format_mask;
         L_hts_imp_cnt_setup_temp_rec.import_country_id    := rec.import_country_id;
         if rec.action = action_new then
            if EXEC_HTS_IMP_CNT_SETUP_INS(L_error_message,
                                          L_hts_imp_cnt_setup_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_HTS_IMP_CNT_SETUP_UPD(L_error_message,
                                          L_hts_imp_cnt_setup_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_HTS_IMP_CNT_SETUP_DEL(L_error_message,
                                          L_hts_imp_cnt_setup_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_hts_import_country_setup st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_hts_import_country_setup st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_hts_import_country_setup st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_IMP_CNT_SETUP;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TARIFF_TREATMENT_INS(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   L_tariff_treatment_temp_rec   IN       TARIFF_TREATMENT%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_TARIFF_TREATMENT_INS';
   L_table   VARCHAR2(64):= 'SVC_TARIFF_TREATMENT';
BEGIN
   insert into tariff_treatment
        values L_tariff_treatment_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TARIFF_TREATMENT_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TARIFF_TREATMENT_UPD(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   L_tariff_treatment_temp_rec   IN       TARIFF_TREATMENT%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_HTS_SETUP.EXEC_TARIFF_TREATMENT_UPD';
   L_table   VARCHAR2(64) := 'SVC_TARIFF_TREATMENT';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_TARIFF_UPD is
      select 'x'
        from tariff_treatment
       where tariff_treatment = L_tariff_treatment_temp_rec.tariff_treatment
         for update nowait;
BEGIN

   ---
   open  C_LOCK_TARIFF_UPD;
   close C_LOCK_TARIFF_UPD;
   ---

   update tariff_treatment
      set row = L_tariff_treatment_temp_rec
    where tariff_treatment = L_tariff_treatment_temp_rec.tariff_treatment;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_tariff_treatment_temp_rec.tariff_treatment,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_TARIFF_UPD%ISOPEN then
         close C_LOCK_TARIFF_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TARIFF_TREATMENT_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TARIFF_TREATMENT_DEL(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   L_tariff_treatment_temp_rec   IN       TARIFF_TREATMENT%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_TARIFF_TREATMENT_DEL';
   L_table   VARCHAR2(64):= 'SVC_TARIFF_TREATMENT';
   L_exists                  VARCHAR2(1) := 'N';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_TARIFF_DEL is
      select 'x'
        from tariff_treatment
       where tariff_treatment = L_tariff_treatment_temp_rec.tariff_treatment
         for update nowait;

   cursor C_LOCK_TARIFF_TL_DEL is
      select 'x'
        from tariff_treatment_tl
       where tariff_treatment = L_tariff_treatment_temp_rec.tariff_treatment
         for update nowait;

   cursor C_CHK_CHILD_EXISTS is
   select 'Y'
     from (select tariff_treatment from COUNTRY_TARIFF_TREATMENT
           union
           select tariff_treatment from HTS_TARIFF_TREATMENT
           union
           select tariff_treatment from COND_TARIFF_TREATMENT
           union
           select tariff_treatment from HTS_TARIFF_TREATMENT_ZONE
           union
           select tariff_treatment from HTS_TT_EXCLUSIONS) DEP_TBL
     where DEP_TBL.tariff_treatment = L_tariff_treatment_temp_rec.tariff_treatment;

   cursor C_LOCK_CNTRY_TARIFF_DEL is
      select 'x'
        from country_tariff_treatment ctt
       where exists (select 1 
                       from svc_country_tariff_treatment sctt
                      where sctt.tariff_treatment  = ctt.tariff_treatment
                        and sctt.tariff_treatment  = L_tariff_treatment_temp_rec.tariff_treatment
                        and upper(action)          =  upper(action_del))
         for update nowait;

BEGIN
   open  C_LOCK_CNTRY_TARIFF_DEL;
   close C_LOCK_CNTRY_TARIFF_DEL;   
   
   delete
     from country_tariff_treatment ctt
    where exists (select 1 
                    from svc_country_tariff_treatment sctt
                   where sctt.tariff_treatment  = ctt.tariff_treatment
                     and sctt.tariff_treatment  = L_tariff_treatment_temp_rec.tariff_treatment
                     and upper(action)          =  upper(action_del));

   if(sql%rowcount > 0) then
      update svc_country_tariff_treatment
         set process$status    ='P'
       where tariff_treatment  = L_tariff_treatment_temp_rec.tariff_treatment
         and upper(action)     = upper(action_del);
   end if;
   
   open C_CHK_CHILD_EXISTS;
   fetch C_CHK_CHILD_EXISTS into L_exists;
   close C_CHK_CHILD_EXISTS;
   
   if L_exists = 'Y' then
      --This tariff treatment is currently in use. Cannot delete.
      O_error_message := 'NO_DELT_CNTRY_TARIFF';
      return FALSE;   
   end if;
   
   ---
   open  C_LOCK_TARIFF_TL_DEL;
   close C_LOCK_TARIFF_TL_DEL;
   ---

   ---
   open  C_LOCK_TARIFF_DEL;
   close C_LOCK_TARIFF_DEL;
   ---

   delete
     from tariff_treatment_tl
    where tariff_treatment = L_tariff_treatment_temp_rec.tariff_treatment;

   delete
     from tariff_treatment
    where tariff_treatment = L_tariff_treatment_temp_rec.tariff_treatment;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_tariff_treatment_temp_rec.tariff_treatment,
                                             NULL);
      return FALSE;
   when OTHERS then

      if C_LOCK_TARIFF_TL_DEL%ISOPEN then
         close C_LOCK_TARIFF_TL_DEL;
      end if;

      if C_LOCK_TARIFF_DEL%ISOPEN then
         close C_LOCK_TARIFF_DEL;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TARIFF_TREATMENT_DEL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_TARIFF_TREATMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id      IN       SVC_TARIFF_TREATMENT.PROCESS_ID%TYPE,
                                  I_chunk_id        IN       SVC_TARIFF_TREATMENT.CHUNK_ID%TYPE )
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)   :='CORESVC_HTS_SETUP.PROCESS_TARIFF_TREATMENT';
   L_table           VARCHAR2(64)   :='SVC_TARIFF_TREATMENT';
   L_process_error   BOOLEAN        := FALSE;
   L_error           BOOLEAN;
   L_error_message   VARCHAR2(600);
   L_tariff_treatment_temp_rec TARIFF_TREATMENT%ROWTYPE;
BEGIN
   FOR rec IN c_svc_TARIFF_TREATMENT(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL or rec.action NOT IN (action_new,
                                                  action_mod,
                                                  action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      if rec.action = action_new
         and rec.PK_TARIFF_TREATMENT_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TARIFF_TREATMENT',
                     'DUPLICATE_TARIFF');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_TARIFF_TREATMENT_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TARIFF_TREATMENT',
                     'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;
      if NOT(rec.TARIFF_TREATMENT  IS NOT NULL) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TARIFF_TREATMENT',
                     'ENTER_TARIFF');
         L_error :=TRUE;
      end if;
      if NOT(rec.TARIFF_TREATMENT_DESC  IS NOT NULL) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TARIFF_TREATMENT_DESC',
                     'ENTER_TARIFF_DESC');
         L_error :=TRUE;
      end if;
      if NOT(rec.CONDITIONAL_IND IS NOT NULL) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CONDITIONAL_IND',
                     'ENTER_TARIFF_IND');
         L_error :=TRUE;
      end if;
      if NOT(rec.TARIFF_TREATMENT  =  UPPER (rec.TARIFF_TREATMENT)) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TARIFF_TREATMENT',
                     'INV_TARIFF_UP');
         L_error :=TRUE;
      end if;
      if NOT(rec.CONDITIONAL_IND IN('Y','N')) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CONDITIONAL_IND',
                     'INV_Y_N_IND');
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_tariff_treatment_temp_rec.conditional_ind               := rec.conditional_ind;
         L_tariff_treatment_temp_rec.tariff_treatment_desc         := rec.tariff_treatment_desc;
         L_tariff_treatment_temp_rec.tariff_treatment              := rec.tariff_treatment;
         if rec.action = action_new then
            if EXEC_TARIFF_TREATMENT_INS(L_error_message,
                                         L_tariff_treatment_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_TARIFF_TREATMENT_UPD(L_error_message,
                                         L_tariff_treatment_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_TARIFF_TREATMENT_DEL(L_error_message,
                                         L_tariff_treatment_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_tariff_treatment st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;

      ---
      if NOT L_process_error then
         update svc_tariff_treatment st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_tariff_treatment st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      ---

   END LOOP;
 
   return TRUE;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TARIFF_TREATMENT;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_TARIFF_TREATMENT_TL_INS(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_tariff_treatment_tl_ins_tab   IN       TARIFF_TREATMENT_TL_TAB)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_TARIFF_TREATMENT_TL_INS';
   L_table   VARCHAR2(64):= 'SVC_TARIFF_TREATMENT_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;
BEGIN
   if I_tariff_treatment_tl_ins_tab is NOT NULL and I_tariff_treatment_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_tariff_treatment_tl_ins_tab.COUNT()
         insert into tariff_treatment_tl
              values I_tariff_treatment_tl_ins_tab(i);
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TARIFF_TREATMENT_TL_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TARIFF_TREATMENT_TL_UPD(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_tariff_treatment_tl_upd_tab   IN       TARIFF_TREATMENT_TL_TAB,
                                      I_tariff_treatment_tl_upd_rst   IN       ROW_SEQ_TAB,
                                      I_process_id                    IN       SVC_TARIFF_TREATMENT_TL.PROCESS_ID%TYPE,
                                      I_chunk_id                      IN       SVC_TARIFF_TREATMENT_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_TARIFF_TREATMENT_TL_UPD';
   L_table   VARCHAR2(64):= 'SVC_TARIFF_TREATMENT_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_TARIFF_TL_UPD(I_tariff_treatment  tariff_treatment_tl.tariff_treatment%TYPE,
                               I_lang              tariff_treatment_tl.LANG%TYPE) is
      select 'x'
        from tariff_treatment_tl
       where tariff_treatment = I_tariff_treatment
         and lang = I_lang
         for update nowait;
BEGIN
   if I_tariff_treatment_tl_upd_tab is NOT NULL and I_tariff_treatment_tl_upd_tab.count > 0 then
      for i in I_tariff_treatment_tl_upd_tab.FIRST..I_tariff_treatment_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_tariff_treatment_tl_upd_tab(i).lang);
            L_key_val2 := 'Tariff Treatment: '||to_char(I_tariff_treatment_tl_upd_tab(i).tariff_treatment);
            open C_LOCK_TARIFF_TL_UPD(I_tariff_treatment_tl_upd_tab(i).tariff_treatment,
                                      I_tariff_treatment_tl_upd_tab(i).lang);
            close C_LOCK_TARIFF_TL_UPD;
            
            update tariff_treatment_tl
               set tariff_treatment_desc = I_tariff_treatment_tl_upd_tab(i).tariff_treatment_desc,
                   last_update_id        = I_tariff_treatment_tl_upd_tab(i).last_update_id,
                   last_update_datetime  = I_tariff_treatment_tl_upd_tab(i).last_update_datetime
             where tariff_treatment      = I_tariff_treatment_tl_upd_tab(i).tariff_treatment
               and lang                  = I_tariff_treatment_tl_upd_tab(i).lang; 
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_TARIFF_TREATMENT_TL',
                           I_tariff_treatment_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TARIFF_TREATMENT_TL_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_TARIFF_TREATMENT_TL_DEL(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_tariff_treatment_tl_del_tab   IN       TARIFF_TREATMENT_TL_TAB,
                                      I_tariff_treatment_tl_del_rst   IN       ROW_SEQ_TAB,
                                      I_process_id                    IN       SVC_TARIFF_TREATMENT_TL.PROCESS_ID%TYPE,
                                      I_chunk_id                      IN       SVC_TARIFF_TREATMENT_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_TARIFF_TREATMENT_TL_DEL';
   L_table   VARCHAR2(64):= 'SVC_TARIFF_TREATMENT_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_TARIFF_TL_DEL(I_tariff_treatment  tariff_treatment_tl.tariff_treatment%TYPE,
                               I_lang              tariff_treatment_tl.LANG%TYPE) is
      select 'x'
        from tariff_treatment_tl
       where tariff_treatment = I_tariff_treatment
         and lang = I_lang
         for update nowait;
BEGIN
   if I_tariff_treatment_tl_del_tab is NOT NULL and I_tariff_treatment_tl_del_tab.count > 0 then
      for i in I_tariff_treatment_tl_del_tab.FIRST..I_tariff_treatment_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_tariff_treatment_tl_del_tab(i).lang);
            L_key_val2 := 'Tariff Treatment: '||to_char(I_tariff_treatment_tl_del_tab(i).tariff_treatment);
            open C_LOCK_TARIFF_TL_DEL(I_tariff_treatment_tl_del_tab(i).tariff_treatment,
                                      I_tariff_treatment_tl_del_tab(i).lang);
            close C_LOCK_TARIFF_TL_DEL;
            
            delete from tariff_treatment_tl
             where tariff_treatment = I_tariff_treatment_tl_del_tab(i).tariff_treatment
               and lang             = I_tariff_treatment_tl_del_tab(i).lang;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_TARIFF_TREATMENT_TL',
                           I_tariff_treatment_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TARIFF_TREATMENT_TL_DEL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_TARIFF_TREATMENT_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_process_id      IN       SVC_TARIFF_TREATMENT_TL.PROCESS_ID%TYPE,
                                     I_chunk_id        IN       SVC_TARIFF_TREATMENT_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)   := 'CORESVC_HTS_SETUP.PROCESS_TARIFF_TREATMENT_TL';
   L_error_message      VARCHAR2(600);
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   := 'SVC_TARIFF_TREATMENT_TL';
   L_base_trans_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   := 'TARIFF_TREATMENT_TL';
   L_error              BOOLEAN         := FALSE;
   L_process_error      BOOLEAN         := FALSE;
   L_TARIFF_TREATMENT_TL_temp_rec TARIFF_TREATMENT_TL%ROWTYPE;
   L_tariff_treatment_tl_upd_rst  ROW_SEQ_TAB;
   L_tariff_treatment_tl_del_rst  ROW_SEQ_TAB;
   
   cursor C_SVC_TARIFF_TREATMENT_TL(I_process_id NUMBER,
                                    I_chunk_id NUMBER) is
      select pk_tariff_treatment_tl.rowid  as pk_tariff_treatment_tl_rid,
             st.rowid                      as st_rid,
             ttt_tt_fk.rowid               as ttt_tt_fk_rid,
             ttt_lang_fk.rowid             as ttt_lang_fk_rid,
             st.lang,
             st.tariff_treatment,
             st.tariff_treatment_desc,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) as action,
             st.process$status
        from svc_tariff_treatment_tl st,
             tariff_treatment_tl pk_tariff_treatment_tl,
             tariff_treatment ttt_tt_fk,
             lang ttt_lang_fk
       where st.process_id        = I_process_id
         and st.chunk_id          = I_chunk_id
         and st.tariff_treatment  = pk_tariff_treatment_tl.tariff_treatment (+)
         and st.lang              = pk_tariff_treatment_tl.lang (+)
         and st.lang              = ttt_lang_fk.lang (+)
         and st.tariff_treatment  = ttt_tt_fk.tariff_treatment (+);

   TYPE SVC_TARIFF_TREATMENT_TL is TABLE OF C_SVC_TARIFF_TREATMENT_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   
   L_svc_tariff_treatment_tl_tab   SVC_TARIFF_TREATMENT_TL;
   L_tariff_treatment_tl_ins_tab   TARIFF_TREATMENT_TL_tab   := NEW TARIFF_TREATMENT_TL_tab();
   L_tariff_treatment_tl_upd_tab   TARIFF_TREATMENT_TL_tab   := NEW TARIFF_TREATMENT_TL_tab();
   L_tariff_treatment_tl_del_tab   TARIFF_TREATMENT_TL_tab   := NEW TARIFF_TREATMENT_TL_tab();   
BEGIN

   if C_SVC_TARIFF_TREATMENT_TL%ISOPEN then
      close C_SVC_TARIFF_TREATMENT_TL;
   end if;
   
   open C_SVC_TARIFF_TREATMENT_TL(I_process_id,
                                  I_chunk_id);
   LOOP
      fetch C_SVC_TARIFF_TREATMENT_TL bulk collect into L_svc_tariff_treatment_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_tariff_treatment_tl_tab.COUNT > 0 then
         FOR i in L_svc_tariff_treatment_tl_tab.FIRST..L_svc_tariff_treatment_tl_tab.LAST LOOP
            L_error               := FALSE;

            -- check if action is valid
            if L_svc_tariff_treatment_tl_tab(i).action is NULL
               or L_svc_tariff_treatment_tl_tab(i).action NOT IN (action_new,action_mod,action_del) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tariff_treatment_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=true;
            end if;

            --check for primary_lang
            if L_svc_tariff_treatment_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tariff_treatment_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_tariff_treatment_tl_tab(i).action = action_new
               and L_svc_tariff_treatment_tl_tab(i).PK_TARIFF_TREATMENT_TL_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tariff_treatment_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_tariff_treatment_tl_tab(i).action IN (action_mod,action_del)
               and L_svc_tariff_treatment_tl_tab(i).lang is NOT NULL
               and L_svc_tariff_treatment_tl_tab(i).TARIFF_TREATMENT_DESC is NOT NULL
               and L_svc_tariff_treatment_tl_tab(i).PK_TARIFF_TREATMENT_TL_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tariff_treatment_tl_tab(i).row_seq,
                           NULL,
                          'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            --Check for FK
            if L_svc_tariff_treatment_tl_tab(i).action = action_new
               and L_svc_tariff_treatment_tl_tab(i).TARIFF_TREATMENT is NOT NULL
               and L_svc_tariff_treatment_tl_tab(i).ttt_tt_fk_rid is NULL then
                   WRITE_ERROR(I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               L_table,
                               L_svc_tariff_treatment_tl_tab(i).row_seq,
                               'TARIFF_TREATMENT',
                               'INV_TARIFF_TREATMENT');
                   L_error :=TRUE;
            end if;

            if L_svc_tariff_treatment_tl_tab(i).action = action_new
               and L_svc_tariff_treatment_tl_tab(i).lang is NOT NULL
               and L_svc_tariff_treatment_tl_tab(i).ttt_lang_fk_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tariff_treatment_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_tariff_treatment_tl_tab(i).LANG is null then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tariff_treatment_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if L_svc_tariff_treatment_tl_tab(i).TARIFF_TREATMENT is null then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tariff_treatment_tl_tab(i).row_seq,
                           'TARIFF_TREATMENT',
                           'ENTER_TARIFF');
               L_error :=TRUE;
            end if;

            if L_svc_tariff_treatment_tl_tab(i).action in (action_new, action_mod)
               and L_svc_tariff_treatment_tl_tab(i).TARIFF_TREATMENT_DESC is NULL then
                 WRITE_ERROR(I_process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                             I_chunk_id,
                             L_table,
                             L_svc_tariff_treatment_tl_tab(i).row_seq,
                             'TARIFF_TREATMENT_DESC',
                             'ENTER_TARIFF_DESC');
                 L_error :=TRUE;
            end if;

            if NOT L_error then
               L_tariff_treatment_tl_temp_rec.lang                    := L_svc_tariff_treatment_tl_tab(i).lang;
               L_tariff_treatment_tl_temp_rec.tariff_treatment        := L_svc_tariff_treatment_tl_tab(i).tariff_treatment;
               L_tariff_treatment_tl_temp_rec.tariff_treatment_desc   := L_svc_tariff_treatment_tl_tab(i).tariff_treatment_desc;
               L_tariff_treatment_tl_temp_rec.create_datetime         := SYSDATE;
               L_tariff_treatment_tl_temp_rec.create_id               := GET_USER;
               L_tariff_treatment_tl_temp_rec.last_update_datetime    := SYSDATE;
               L_tariff_treatment_tl_temp_rec.last_update_id          := GET_USER;

               if L_svc_tariff_treatment_tl_tab(i).action = action_new then
                  L_tariff_treatment_tl_ins_tab.extend;
                  L_tariff_treatment_tl_ins_tab(L_tariff_treatment_tl_ins_tab.count()) := L_tariff_treatment_tl_temp_rec;
               end if;
               
               if L_svc_tariff_treatment_tl_tab(i).action = action_mod then
                  L_tariff_treatment_tl_upd_tab.extend;
                  L_tariff_treatment_tl_upd_tab(L_tariff_treatment_tl_upd_tab.count()) := L_tariff_treatment_tl_temp_rec;
                  L_tariff_treatment_tl_upd_rst(L_tariff_treatment_tl_upd_tab.count()) := L_svc_tariff_treatment_tl_tab(i).row_seq;
               end if;
               
               if L_svc_tariff_treatment_tl_tab(i).action = action_del then
                  L_tariff_treatment_tl_del_tab.extend;
                  L_tariff_treatment_tl_del_tab(L_tariff_treatment_tl_del_tab.count()) := L_tariff_treatment_tl_temp_rec;
                  L_tariff_treatment_tl_del_rst(L_tariff_treatment_tl_del_tab.count()) := L_svc_tariff_treatment_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_TARIFF_TREATMENT_TL%NOTFOUND;
   END LOOP;
   close C_SVC_TARIFF_TREATMENT_TL;
   
   if EXEC_TARIFF_TREATMENT_TL_INS(O_error_message,   
                                   L_tariff_treatment_tl_ins_tab)=FALSE then
      return FALSE;
   end if;
   
   if EXEC_TARIFF_TREATMENT_TL_UPD(O_error_message,
                                   L_tariff_treatment_tl_upd_tab,
                                   L_tariff_treatment_tl_upd_rst,
                                   I_process_id,
                                   I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_TARIFF_TREATMENT_TL_DEL(O_error_message,
                                   L_tariff_treatment_tl_del_tab,
                                   L_tariff_treatment_tl_del_rst,
                                   I_process_id,
                                   I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TARIFF_TREATMENT_TL;
--------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CHAPTER_INS( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_hts_chapter_temp_rec  IN       HTS_CHAPTER%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                        := 'CORESVC_HTS_SETUP.EXEC_HTS_CHAPTER_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   := 'SVC_HTS_CHAPTER';
BEGIN
   insert into hts_chapter
      values I_hts_chapter_temp_rec;

   return TRUE;
EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('HTS_CHAPTER_EXISTS',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_CHAPTER_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CHAPTER_UPD( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_hts_chapter_temp_rec  IN       HTS_CHAPTER%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                       := 'CORESVC_HTS_SETUP.EXEC_HTS_CHAPTER_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_HTS_CHAPTER';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_HTS_CHAPTER_UPD is
      select 'x'
        from hts_chapter
       where import_country_id = I_hts_chapter_temp_rec.import_country_id
         and chapter           = I_hts_chapter_temp_rec.chapter
         for update nowait;
BEGIN
   open  C_HTS_CHAPTER_UPD;
   close C_HTS_CHAPTER_UPD;

   update hts_chapter
      set row = I_hts_chapter_temp_rec
    where 1 = 1
      and import_country_id = I_hts_chapter_temp_rec.import_country_id
      and chapter           = I_hts_chapter_temp_rec.chapter;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                                L_table,
                                                                I_hts_chapter_temp_rec.import_country_id,
                                                                I_hts_chapter_temp_rec.chapter);

      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_HTS_CHAPTER_UPD%ISOPEN   then
         close C_HTS_CHAPTER_UPD;
      end if;
      return FALSE;
END EXEC_HTS_CHAPTER_UPD;
-------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CHAPTER_DEL( O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_hts_chapter_temp_rec  IN      HTS_CHAPTER%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                           := 'CORESVC_HTS_SETUP.EXEC_HTS_CHAPTER_DEL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE      := 'SVC_HTS_CHAPTER';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_HTS_CHAPTER_DEL is
      select 'x'
        from HTS_CHAPTER
       where import_country_id = I_hts_chapter_temp_rec.import_country_id
         and chapter = I_hts_chapter_temp_rec.chapter
         for update nowait;

   cursor C_HTS_CHAPTER_TL_DEL is
      select 'x'
        from hts_chapter_tl
       where import_country_id = I_hts_chapter_temp_rec.import_country_id
         and chapter = I_hts_chapter_temp_rec.chapter
         for update nowait;
BEGIN
   open  C_HTS_CHAPTER_TL_DEL;
   close C_HTS_CHAPTER_TL_DEL;

   delete
     from hts_chapter_tl
    where import_country_id = I_hts_chapter_temp_rec.import_country_id
      and chapter = I_hts_chapter_temp_rec.chapter;

   open  C_HTS_CHAPTER_DEL;
   close C_HTS_CHAPTER_DEL;

   if HTS_SQL.DELETE_HTS_CHAPTER(O_error_message,
                                 I_hts_chapter_temp_rec.import_country_id,
                                 I_hts_chapter_temp_rec.chapter) = FALSE then
      return FALSE;
   end if;

   delete
     from hts_chapter
    where 1 = 1
      and import_country_id = I_hts_chapter_temp_rec.import_country_id
      and chapter = I_hts_chapter_temp_rec.chapter;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                                L_table,
                                                                I_hts_chapter_temp_rec.import_country_id,
                                                                I_hts_chapter_temp_rec.chapter);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_HTS_CHAPTER_DEL%ISOPEN   then
         close C_HTS_CHAPTER_DEL;
      end if;
      if C_HTS_CHAPTER_TL_DEL%ISOPEN   then
         close C_HTS_CHAPTER_TL_DEL;
      end if;
      return FALSE;
END EXEC_HTS_CHAPTER_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CHAPTER_TL_INS(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_hts_chapter_tl_ins_tab    IN       HTS_CHAPTER_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_SETUP.EXEC_HTS_CHAPTER_TL_INS';

BEGIN
   if I_hts_chapter_tl_ins_tab is NOT NULL and I_hts_chapter_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_hts_chapter_tl_ins_tab.COUNT()
         insert into hts_chapter_TL
              values I_hts_chapter_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_HTS_CHAPTER_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CHAPTER_TL_UPD(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_hts_chapter_tl_upd_tab   IN       HTS_CHAPTER_TAB,
                                 I_hts_chapter_tl_upd_rst   IN       ROW_SEQ_TAB,
                                 I_process_id               IN       SVC_HTS_CHAPTER_TL.PROCESS_ID%TYPE,
                                 I_chunk_id                 IN       SVC_HTS_CHAPTER_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_HTS_SETUP.EXEC_HTS_CHAPTER_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CHAPTER_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_HTS_CHAPTER_TL_UPD(I_chapter      HTS_CHAPTER_TL.CHAPTER%TYPE,
                                    I_country      HTS_CHAPTER_TL.IMPORT_COUNTRY_ID%TYPE,
                                    I_lang         HTS_CHAPTER_TL.LANG%TYPE) IS
      select 'x'
        from hts_chapter_tl
       where import_country_id = I_country
         and chapter = I_chapter
         and lang = I_lang
         for update nowait;

BEGIN
   if I_hts_chapter_tl_upd_tab is NOT NULL and I_hts_chapter_tl_upd_tab.count > 0 then
      for i in I_hts_chapter_tl_upd_tab.FIRST..I_hts_chapter_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_hts_chapter_tl_upd_tab(i).lang);
            L_key_val2 := 'Chapter: '||to_char(I_hts_chapter_tl_upd_tab(i).chapter);
            open C_LOCK_HTS_CHAPTER_TL_UPD(I_hts_chapter_tl_upd_tab(i).chapter,
                                           I_hts_chapter_tl_upd_tab(i).import_country_id,
                                           I_hts_chapter_tl_upd_tab(i).lang);
            close C_LOCK_HTS_CHAPTER_TL_UPD;
            
            update hts_chapter_tl
               set chapter_desc = I_hts_chapter_tl_upd_tab(i).chapter_desc,
                   last_update_id = I_hts_chapter_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_hts_chapter_tl_upd_tab(i).last_update_datetime
             where lang = I_hts_chapter_tl_upd_tab(i).lang
               and chapter = I_hts_chapter_tl_upd_tab(i).chapter
               and import_country_id = I_hts_chapter_tl_upd_tab(i).import_country_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_HTS_CHAPTER_TL',
                           I_hts_chapter_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_HTS_CHAPTER_TL_UPD%ISOPEN then
         close C_LOCK_HTS_CHAPTER_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_HTS_CHAPTER_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CHAPTER_TL_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_hts_chapter_tl_del_tab   IN       HTS_CHAPTER_TAB,
                                 I_hts_chapter_tl_del_rst   IN       ROW_SEQ_TAB,
                                 I_process_id               IN       SVC_HTS_CHAPTER_TL.PROCESS_ID%TYPE,
                                 I_chunk_id                 IN       SVC_HTS_CHAPTER_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_HTS_SETUP.EXEC_HTS_CHAPTER_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CHAPTER_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_HTS_CHAPTER_TL_DEL(I_chapter            HTS_CHAPTER.CHAPTER%TYPE,
                                    I_import_country_id  HTS_CHAPTER_TL.IMPORT_COUNTRY_ID%TYPE,
                                    I_lang               HTS_CHAPTER_TL.LANG%TYPE) is
      select 'x'
        from hts_chapter_tl
       where import_country_id = I_import_country_id
         and chapter = I_chapter
         and lang = I_lang
         for update nowait;

BEGIN
   if I_hts_chapter_tl_del_tab is NOT NULL and I_hts_chapter_tl_del_tab.count > 0 then
      for i in I_hts_chapter_tl_del_tab.FIRST..I_hts_chapter_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_hts_chapter_tl_del_tab(i).lang);
            L_key_val2 := 'Chapter: '||to_char(I_hts_chapter_tl_del_tab(i).chapter);
            open C_LOCK_HTS_CHAPTER_TL_DEL(I_hts_chapter_tl_del_tab(i).import_country_id,
                                           I_hts_chapter_tl_del_tab(i).chapter,
                                           I_hts_chapter_tl_del_tab(i).lang);
            close C_LOCK_HTS_CHAPTER_TL_DEL;
            
            delete hts_chapter_tl
             where lang = I_hts_chapter_tl_del_tab(i).lang
               and import_country_id = I_hts_chapter_tl_del_tab(i).import_country_id
               and chapter = I_hts_chapter_tl_del_tab(i).chapter;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_HTS_CHAPTER_TL',
                           I_hts_chapter_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_HTS_CHAPTER_TL_DEL%ISOPEN then
         close C_LOCK_HTS_CHAPTER_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_HTS_CHAPTER_TL_DEL;
-------------------------------------------------------------------------------------
FUNCTION CHECK_DEL_HTS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists              IN OUT BOOLEAN,
                       I_import_country_id   IN     HTS_CHAPTER.IMPORT_COUNTRY_ID%TYPE,
                       I_hts_chapter         IN     HTS_CHAPTER.CHAPTER%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_HTS_SETUP.CHECK_DEL_HTS';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_CHAPTER';
   L_exists       VARCHAR2(1)                       :=  NULL;
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_locked, -54);

   cursor C_HTS is
      select 'Y'
        from hts
       where chapter = I_hts_chapter
         and import_country_id = I_import_country_id;
BEGIN
   O_exists := FALSE;
   --hts
   open C_HTS;
   fetch C_HTS into L_exists;
   if C_HTS%FOUND then
      close C_HTS;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_CHAP_HTS',
                                             NULL,
                                             NULL,
                                             NULL);
      O_exists         := TRUE;
      return TRUE;
   end if;
   close C_HTS;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   if C_HTS%ISOPEN then
      close C_HTS;
   end if;
   ROLLBACK;
   return FALSE;
END CHECK_DEL_HTS;
------------------------------------------------------------------------------------
FUNCTION CHECK_LOCK_HTS_CHAPTER(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_import_country_id IN     HTS_CHAPTER.IMPORT_COUNTRY_ID%TYPE,
                                I_hts_chapter       IN     HTS_CHAPTER.CHAPTER%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_HTS_SETUP.CHECK_LOCK_HTS_CHAPTER';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CHAPTER_RESTRAINTS';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_locked, -54);

   cursor C_LOCK_HTS_CHAPTER_RESTRAINTS is
      select 'Y'
        from hts_chapter_restraints
       where chapter           = I_hts_chapter
         and import_country_id = I_import_country_id
         for update nowait;

   cursor C_LOCK_REQ_DOC is
      select 'Y'
        from req_doc
        where module       = 'HTSC'
          and key_value_1  = I_hts_chapter
          and key_value_2  = I_import_country_id
          for update nowait;
BEGIN
   --HTS_CHAPTER_RESTRAINTS--
   open C_LOCK_HTS_CHAPTER_RESTRAINTS;
   close C_LOCK_HTS_CHAPTER_RESTRAINTS;

   --REQ_DOC--
   L_table := 'REQ_DOC';
   open C_LOCK_REQ_DOC;
   close C_LOCK_REQ_DOC;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_hts_chapter,
                                             I_import_country_id);
      if C_LOCK_REQ_DOC%ISOPEN then
         close C_LOCK_REQ_DOC;
      end if;
      if C_LOCK_HTS_CHAPTER_RESTRAINTS%ISOPEN then
         close C_LOCK_HTS_CHAPTER_RESTRAINTS;
      end if;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_REQ_DOC%ISOPEN then
         close C_LOCK_REQ_DOC;
      end if;

      if C_LOCK_HTS_CHAPTER_RESTRAINTS%ISOPEN then
         close C_LOCK_HTS_CHAPTER_RESTRAINTS;
      end if;

      rollback;
      return FALSE;
END CHECK_LOCK_HTS_CHAPTER;
------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_HTS_CHAPTER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error          IN OUT BOOLEAN,
                                 I_rec            IN     C_SVC_HTS_CHAPTER%ROWTYPE)
RETURN BOOLEAN IS
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_CHAPTER';
   L_hts_format_mask HTS_IMPORT_COUNTRY_SETUP.HTS_FORMAT_MASK%TYPE;
   L_heading_length  HTS_IMPORT_COUNTRY_SETUP.HTS_HEADING_LENGTH%TYPE;
   L_exists          BOOLEAN;
BEGIN
   /* this validation checks that HTS heading length is maintained for
   the entered import_country_id in the HTS_IMPORT_COUNTRY_SETUP*/
   if I_rec.action = action_new
      and I_rec.import_country_id is NOT NULL
      and I_rec.htc_cnt_fk_rid is NOT NULL then
      if HTS_SQL.GET_FORMAT_MASK_HEADING_LENGTH(O_error_message,
                                                L_hts_format_mask,
                                                L_heading_length,
                                                I_rec.import_country_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;
      if L_heading_length = 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     'NO_HTS_HEADING_LEN_SETUP');
         O_error := TRUE;
      end if;
   end if;
   /* this validation checks that the chapter length is equal to heading length*/
   if I_rec.action = action_new
      and I_rec.htc_cnt_fk_rid is NOT NULL
      and I_rec.chapter is NOT NULL
      and L_heading_length <> 0 then
      if NVL(LENGTH(I_rec.chapter),0) <> L_heading_length then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     'INV_HTS_CHAP_LENGTH');
         O_error := TRUE;
      end if;
   end if;
   --this validation check that the chapter is numeric
   if I_rec.action = action_new then
      if TRANSLATE(I_rec.chapter,'A1234567890','A') is NOT NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CHAPTER',
                     'NON_NUMERIC');
         O_error := TRUE;
      end if;
   end if;

   --this validation checks if chapter has any child record attached
   if I_rec.action = action_del
      and I_rec.pk_hts_chapter_rid is NOT NULL   then
      if CHECK_DEL_HTS(O_error_message,
                       L_exists,
                       I_rec.import_country_id,
                       I_rec.chapter) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;
      if L_exists = TRUE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CHAPTER',
                     O_error_message);
         O_error := TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              'CORESVC_HTS_SETUP.PROCESS_VAL_HTS_CHAPTER',
                                              TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_HTS_CHAPTER;
-------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_CHAPTER( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id    IN     SVC_HTS_CHAPTER.PROCESS_ID%TYPE,
                              I_chunk_id      IN     SVC_HTS_CHAPTER.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program              VARCHAR2(64)                      :='CORESVC_HTS_SETUP.PROCESS_HTS_CHAPTER';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_HTS_CHAPTER';
   L_process_error        BOOLEAN                           := FALSE;
   L_hts_chapter_temp_rec HTS_CHAPTER%ROWTYPE;
   L_error                BOOLEAN;
   L_proceed_delete       BOOLEAN;
BEGIN
   FOR rec IN C_SVC_HTS_CHAPTER(I_process_id,
                                I_chunk_id)
   LOOP
      SAVEPOINT successful_hts_chapter;
      L_error          := FALSE;
      L_process_error  := FALSE;
      L_proceed_delete := TRUE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      if rec.action = action_new
         and rec.chapter            is NOT NULL
         and rec.import_country_id  is NOT NULL
         and rec.pk_hts_chapter_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS Heading,Import Country',
                     'HTS_EXISTS');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.htc_cnt_fk_rid     is NOT NULL
         and rec.chapter            is NOT NULL
         and rec.import_country_id  is NOT NULL
         and rec.pk_hts_chapter_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS Heading,Import Country',
                     'INV_CHAPTER');
         L_error :=TRUE;
      end if;
      if rec.htc_cnt_fk_rid is NULL
         and rec.import_country_id is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     'INV_COUNTRY');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_new, action_mod)
         and NOT(  rec.chapter_desc  is NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CHAPTER_DESC',
                     'HTS_CHAPTER_DESC_REQ');
          L_error :=TRUE;
      end if;

      --Other validations present in the forms
      if PROCESS_VAL_HTS_CHAPTER(O_error_message,
                                 L_error,
                                 rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_hts_chapter_temp_rec.chapter            := rec.chapter;
         L_hts_chapter_temp_rec.chapter_desc       := rec.chapter_desc;
         L_hts_chapter_temp_rec.import_country_id  := rec.import_country_id;
         if rec.action = action_del then
            if CHECK_LOCK_HTS_CHAPTER(O_error_message,
                                      rec.import_country_id,
                                      rec.chapter) = FALSE then
               WRITE_ERROR(rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           rec.chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_proceed_delete := FALSE;
            end if;
         end if;
         if rec.action = action_new then
            if EXEC_HTS_CHAPTER_INS( O_error_message,
                                     L_hts_chapter_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_HTS_CHAPTER_UPD( O_error_message,
                                     L_hts_chapter_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del
            and L_proceed_delete then
            if EXEC_HTS_CHAPTER_DEL( O_error_message,
                                     L_hts_chapter_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               ROLLBACK to successful_hts_chapter;
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_CHAPTER;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_CHAPTER_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN       SVC_HTS_CHAPTER_TL.PROCESS_ID%TYPE,
                                I_chunk_id        IN       SVC_HTS_CHAPTER_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                    VARCHAR2(64) := 'CORESVC_HTS_SETUP.PROCESS_HTS_CHAPTER_TL';
   L_table                      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_CHAPTER_TL';
   L_error_message              RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CHAPTER_TL';
   L_base_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CHAPTER';
   L_error                      BOOLEAN := FALSE;
   L_process_error              BOOLEAN := FALSE;
   L_hts_chapter_tl_temp_rec    HTS_CHAPTER_TL%ROWTYPE;
   L_hts_chapter_tl_upd_rst     ROW_SEQ_TAB;
   L_hts_chapter_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_HTS_CHAPTER_TL(I_process_id NUMBER,
                               I_chunk_id NUMBER) is
      select pk_hts_chapter_TL.rowid  as pk_hts_chapter_TL_rid,
             fk_hts_chapter.rowid     as fk_hts_chapter_rid,
             fk_lang.rowid            as fk_lang_rid,
             st.lang,
             st.import_country_id,
             st.chapter_desc,
             st.chapter,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_hts_chapter_tl  st,
             hts_chapter         fk_hts_chapter,
             hts_chapter_tl      pk_hts_chapter_TL,
             lang                fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.import_country_id  =  fk_hts_chapter.import_country_id (+)
         and st.chapter            =  fk_hts_chapter.chapter (+)
         and st.lang               =  pk_hts_chapter_TL.lang (+)
         and st.import_country_id  =  pk_hts_chapter_TL.import_country_id (+)
         and st.chapter            =  pk_hts_chapter_TL.chapter (+)
         and st.lang               =  fk_lang.lang (+);

   TYPE SVC_HTS_CHAPTER_TL is TABLE OF C_SVC_HTS_CHAPTER_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_hts_chapter_tab        SVC_HTS_CHAPTER_TL;

   L_hts_chapter_TL_ins_tab         hts_chapter_tab         := NEW hts_chapter_tab();
   L_hts_chapter_TL_upd_tab         hts_chapter_tab         := NEW hts_chapter_tab();
   L_hts_chapter_TL_del_tab         hts_chapter_tab         := NEW hts_chapter_tab();

BEGIN
   if C_SVC_HTS_CHAPTER_TL%ISOPEN then
      close C_SVC_HTS_CHAPTER_TL;
   end if;

   open C_SVC_HTS_CHAPTER_TL(I_process_id,
                             I_chunk_id);
   LOOP
      fetch C_SVC_HTS_CHAPTER_TL bulk collect into L_svc_hts_chapter_tab limit LP_bulk_fetch_limit;
      if L_svc_hts_chapter_tab.COUNT > 0 then
         FOR i in L_svc_hts_chapter_tab.FIRST..L_svc_hts_chapter_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_hts_chapter_tab(i).action is NULL
               or L_svc_hts_chapter_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_chapter_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

             --check for primary_lang
            if L_svc_hts_chapter_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_chapter_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_hts_chapter_tab(i).action = action_new
               and L_svc_hts_chapter_tab(i).pk_hts_chapter_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_chapter_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_hts_chapter_tab(i).action IN (action_mod, action_del)
               and L_svc_hts_chapter_tab(i).lang is NOT NULL
               and L_svc_hts_chapter_tab(i).import_country_id is NOT NULL
               and L_svc_hts_chapter_tab(i).chapter is NOT NULL
               and L_svc_hts_chapter_tab(i).pk_hts_chapter_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_hts_chapter_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_hts_chapter_tab(i).action = action_new
               and L_svc_hts_chapter_tab(i).import_country_id is NOT NULL
               and L_svc_hts_chapter_tab(i).chapter is NOT NULL
               and L_svc_hts_chapter_tab(i).fk_hts_chapter_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_hts_chapter_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_hts_chapter_tab(i).action = action_new
               and L_svc_hts_chapter_tab(i).lang is NOT NULL
               and L_svc_hts_chapter_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_chapter_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_hts_chapter_tab(i).action in (action_new, action_mod) then
               if L_svc_hts_chapter_tab(i).chapter_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_hts_chapter_tab(i).row_seq,
                              'CHAPTER_DESC',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_hts_chapter_tab(i).chapter is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_chapter_tab(i).row_seq,
                           'CHAPTER',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_hts_chapter_tab(i).import_country_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_chapter_tab(i).row_seq,
                           'IMPORT_COUNTRY_ID',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_hts_chapter_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_chapter_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_hts_chapter_tl_temp_rec.lang := L_svc_hts_chapter_tab(i).lang;
               L_hts_chapter_tl_temp_rec.import_country_id := L_svc_hts_chapter_tab(i).import_country_id;
               L_hts_chapter_tl_temp_rec.chapter := L_svc_hts_chapter_tab(i).chapter;
               L_hts_chapter_tl_temp_rec.chapter_desc := L_svc_hts_chapter_tab(i).chapter_desc;
               L_hts_chapter_tl_temp_rec.create_datetime := SYSDATE;
               L_hts_chapter_tl_temp_rec.create_id := GET_USER;
               L_hts_chapter_tl_temp_rec.last_update_datetime := SYSDATE;
               L_hts_chapter_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_hts_chapter_tab(i).action = action_new then
                  L_hts_chapter_TL_ins_tab.extend;
                  L_hts_chapter_TL_ins_tab(L_hts_chapter_TL_ins_tab.count()) := L_hts_chapter_tl_temp_rec;
               end if;

               if L_svc_hts_chapter_tab(i).action = action_mod then
                  L_hts_chapter_TL_upd_tab.extend;
                  L_hts_chapter_TL_upd_tab(L_hts_chapter_TL_upd_tab.count()) := L_hts_chapter_tl_temp_rec;
                  L_hts_chapter_tl_upd_rst(L_hts_chapter_TL_upd_tab.count()) := L_svc_hts_chapter_tab(i).row_seq;
               end if;

               if L_svc_hts_chapter_tab(i).action = action_del then
                  L_hts_chapter_TL_del_tab.extend;
                  L_hts_chapter_TL_del_tab(L_hts_chapter_TL_del_tab.count()) := L_hts_chapter_tl_temp_rec;
                  L_hts_chapter_tl_del_rst(L_hts_chapter_TL_del_tab.count()) := L_svc_hts_chapter_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_HTS_CHAPTER_TL%NOTFOUND;
   END LOOP;
   close C_SVC_HTS_CHAPTER_TL;

   if EXEC_HTS_CHAPTER_TL_INS(O_error_message,
                              L_hts_chapter_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_HTS_CHAPTER_TL_UPD(O_error_message,
                              L_hts_chapter_tl_upd_tab,
                              L_hts_chapter_tl_upd_rst,
                              I_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_HTS_CHAPTER_TL_DEL(O_error_message,
                              L_hts_chapter_tl_del_tab,
                              L_hts_chapter_tl_del_rst,
                              I_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_CHAPTER_TL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_CR_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error           IN OUT   BOOLEAN,
                            I_rec             IN       C_SVC_HTS_CR%ROWTYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64):= 'CORESVC_HTS_SETUP.PROCESS_HTS_CR_VAL';
   L_exists            VARCHAR2(1)  := 'N';
   L_restraint         VARCHAR2(1)  := 'N';
   L_restraint_upd     VARCHAR2(1)  := 'N';
   L_restraint_exists  VARCHAR2(1)  := 'N';
   L_desc              UOM_CLASS_TL.UOM_DESC_TRANS%TYPE;

   cursor C_CHECK_CODE(I_code        CODE_DETAIL.CODE%TYPE,
                       I_code_type   CODE_DETAIL.CODE_TYPE%TYPE) is
      select 'Y'
        from code_detail
       where code_type = I_code_type
         and code = I_code;

   --Cursor to check if restraint_type is being modified to existing type
   cursor C_RESTRAINT_EXISTS is
      select 'Y'
        from hts_chapter_restraints
       where chapter = I_rec.chapter
         and import_country_id = I_rec.import_country_id
         and origin_country_id = I_rec.origin_country_id
         and restraint_type = I_rec.restraint_type_upd;

BEGIN

   --Validate restraint_type when it is being modified
   if I_rec.action = action_mod then
      --Check if restraint_type is being modified to NULL
      if I_rec.restraint_type_upd is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     'SVC_HTS_CHAPTER_RESTRAINTS',
                     I_rec.row_seq,
                     'RESTRAINT_TYPE_UPD',
                     'SVC_COL_REQD');
         O_error := TRUE;
      --Check if restraint_type is being modified to an existing restraint_type for the current origin_country_id
      elsif I_rec.restraint_type <> I_rec.restraint_type_upd then
         open  C_RESTRAINT_EXISTS;
         fetch C_RESTRAINT_EXISTS into L_restraint_exists;
         close C_RESTRAINT_EXISTS;
         if L_restraint_exists = 'Y' then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        'SVC_HTS_CHAPTER_RESTRAINTS',
                        I_rec.row_seq,
                        'RESTRAINT_TYPE_UPD',
                        'HTS_CHAP_RESTRAINT_EXIST');
            O_error := TRUE;
         else
            --restraint_type_upd must be present in the code_detail table with code_type 'RTYP'
            open C_CHECK_CODE(I_rec.restraint_type_upd,
                             'RTYP');
            fetch C_CHECK_CODE into L_restraint_upd;
            close C_CHECK_CODE;
            if L_restraint_upd = 'N' then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           'SVC_HTS_CHAPTER_RESTRAINTS',
                           I_rec.row_seq,
                           'RESTRAINT_TYPE_UPD',
                           'INV_RESTRAINT_TYPE');
               O_error := TRUE;
            end if;
         end if;
      end if;
   end if;

   --restraint_type must be present in the code_detail table with code_type 'RTYP'
   if I_rec.action = action_new then
      open C_CHECK_CODE(I_rec.restraint_type,
                        'RTYP');
      fetch C_CHECK_CODE into L_restraint;
      close C_CHECK_CODE;
      if L_restraint = 'N' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     'SVC_HTS_CHAPTER_RESTRAINTS',
                     I_rec.row_seq,
                     'RESTRAINT_TYPE',
                     'INV_RESTRAINT_TYPE');
         O_error := TRUE;
      end if;
   end if;

   --Check if restraint_qty is negative
   if I_rec.restraint_qty < 0 then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  'SVC_HTS_CHAPTER_RESTRAINTS',
                  I_rec.row_seq,
                  'RESTRAINT_QTY',
                  'GREATER_0');
      O_error := TRUE;
   end if;

   --Call function to check whether UOM is in the V_UOM_CLASS_TL table
   if I_rec.uom is not NULL then
      if UOM_SQL.GET_DESC(O_error_message,
                          L_desc,
                          I_rec.uom) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     'SVC_HTS_CHAPTER_RESTRAINTS',
                     I_rec.row_seq,
                     'UOM',
                     'UOM_NOT_EXIST');
         O_error := TRUE;
      end if;
   end if;

   -- Validate suffix
   if I_rec.restraint_suffix is NOT NULL then
      open C_CHECK_CODE(I_rec.restraint_suffix,
                        'RSUF');
      fetch C_CHECK_CODE into L_exists;
      close C_CHECK_CODE;
      if L_exists = 'N' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     'SVC_HTS_CHAPTER_RESTRAINTS',
                     I_rec.row_seq,
                     'RESTRAINT_SUFFIX',
                     'INV_SUFFIX');
         O_error := TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      --Close open cursors
      if C_CHECK_CODE%ISOPEN then
        close C_CHECK_CODE;
      end if;
      if C_RESTRAINT_EXISTS%ISOPEN then
        close C_RESTRAINT_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_CR_VAL;
-------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CR_INS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts_cr_temp_rec   IN       HTS_CHAPTER_RESTRAINTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_SETUP.EXEC_HTS_CR_INS';
BEGIN

   insert into hts_chapter_restraints
        values I_hts_cr_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_CR_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CR_UPD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts_cr_temp_rec    IN       HTS_CHAPTER_RESTRAINTS%ROWTYPE,
                         I_base_restraint_typ IN       HTS_CHAPTER_RESTRAINTS.RESTRAINT_TYPE%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_SETUP.EXEC_HTS_CR_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CHAPTER_RESTRAINTS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_HTS_CR_UPD is
      select 'x'
        from hts_chapter_restraints
       where restraint_type    = I_base_restraint_typ
         and origin_country_id = I_hts_cr_temp_rec.origin_country_id
         and import_country_id = I_hts_cr_temp_rec.import_country_id
         and chapter           = I_hts_cr_temp_rec.chapter
         for update nowait;

BEGIN
   open  C_LOCK_HTS_CR_UPD;
   close C_LOCK_HTS_CR_UPD;

   update hts_chapter_restraints
      set row = I_hts_cr_temp_rec
    where 1 = 1
      and restraint_type    = I_base_restraint_typ
      and origin_country_id = I_hts_cr_temp_rec.origin_country_id
      and import_country_id = I_hts_cr_temp_rec.import_country_id
      and chapter           = I_hts_cr_temp_rec.chapter;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      close C_LOCK_HTS_CR_UPD;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_hts_cr_temp_rec.chapter,
                                             I_hts_cr_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      if C_LOCK_HTS_CR_UPD%ISOPEN then
         close C_LOCK_HTS_CR_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_CR_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CR_DEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts_cr_temp_rec   IN       HTS_CHAPTER_RESTRAINTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_SETUP.EXEC_HTS_CR_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CHAPTER_RESTRAINTS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_HTS_CR_DEL is
      select 'x'
        from hts_chapter_restraints
       where restraint_type    = I_hts_cr_temp_rec.restraint_type
         and origin_country_id = I_hts_cr_temp_rec.origin_country_id
         and import_country_id = I_hts_cr_temp_rec.import_country_id
         and chapter           = I_hts_cr_temp_rec.chapter
         for update nowait;

   cursor C_LOCK_HTS_CR_TL_DEL is
      select 'x'
        from hts_chapter_restraints_tl
       where restraint_type    = I_hts_cr_temp_rec.restraint_type
         and origin_country_id = I_hts_cr_temp_rec.origin_country_id
         and import_country_id = I_hts_cr_temp_rec.import_country_id
         and chapter           = I_hts_cr_temp_rec.chapter
         for update nowait;

BEGIN
   open  C_LOCK_HTS_CR_TL_DEL;
   close C_LOCK_HTS_CR_TL_DEL;

   delete from hts_chapter_restraints_tl
    where 1 = 1
      and restraint_type    = I_hts_cr_temp_rec.restraint_type
      and origin_country_id = I_hts_cr_temp_rec.origin_country_id
      and import_country_id = I_hts_cr_temp_rec.import_country_id
      and chapter           = I_hts_cr_temp_rec.chapter;

   open  C_LOCK_HTS_CR_DEL;
   close C_LOCK_HTS_CR_DEL;

   delete from hts_chapter_restraints
    where restraint_type    = I_hts_cr_temp_rec.restraint_type
      and origin_country_id = I_hts_cr_temp_rec.origin_country_id
      and import_country_id = I_hts_cr_temp_rec.import_country_id
      and chapter           = I_hts_cr_temp_rec.chapter;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      close C_LOCK_HTS_CR_DEL;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_hts_cr_temp_rec.chapter,
                                             I_hts_cr_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      if C_LOCK_HTS_CR_DEL%ISOPEN   then
         close C_LOCK_HTS_CR_DEL;
      end if;
      if C_LOCK_HTS_CR_TL_DEL%ISOPEN   then
         close C_LOCK_HTS_CR_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_CR_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CR_TL_INS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_hts_cr_tl_ins_tab    IN       HTS_CHAPTER_RESTRAINTS_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_SETUP.EXEC_HTS_CR_TL_INS';

BEGIN
   if I_hts_cr_tl_ins_tab is NOT NULL and I_hts_cr_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_hts_cr_tl_ins_tab.COUNT()
         insert into hts_chapter_restraints_tl
              values I_hts_cr_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_HTS_CR_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CR_TL_UPD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_hts_cr_tl_upd_tab   IN       HTS_CHAPTER_RESTRAINTS_TAB,
                            I_hts_cr_tl_upd_rst   IN       ROW_SEQ_TAB,
                            I_process_id          IN       SVC_HTS_CHAPTER_RESTRAINTS_TL.PROCESS_ID%TYPE,
                            I_chunk_id            IN       SVC_HTS_CHAPTER_RESTRAINTS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_HTS_SETUP.EXEC_HTS_CR_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CHAPTER_RESTRAINTS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_HTS_CR_TL_UPD(I_chapter             HTS_CHAPTER_RESTRAINTS_TL.CHAPTER%TYPE,
                               I_import_country_id   HTS_CHAPTER_RESTRAINTS_TL.IMPORT_COUNTRY_ID%TYPE,
                               I_origin_country_id   HTS_CHAPTER_RESTRAINTS_TL.ORIGIN_COUNTRY_ID%TYPE,
                               I_restraint_type      HTS_CHAPTER_RESTRAINTS_TL.RESTRAINT_TYPE%TYPE,
                               I_lang                HTS_CHAPTER_RESTRAINTS_TL.LANG%TYPE) is
      select 'x'
        from hts_chapter_restraints_tl
       where chapter = I_chapter
         and import_country_id = I_import_country_id
         and origin_country_id = I_origin_country_id
         and restraint_type = I_restraint_type
         and lang = I_lang
         for update nowait;

BEGIN
   if I_hts_cr_tl_upd_tab is NOT NULL and I_hts_cr_tl_upd_tab.count > 0 then
      for i in I_hts_cr_tl_upd_tab.FIRST..I_hts_cr_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_hts_cr_tl_upd_tab(i).lang);
            L_key_val2 := 'Chapter: '||to_char(I_hts_cr_tl_upd_tab(i).chapter);
            open C_LOCK_HTS_CR_TL_UPD(I_hts_cr_tl_upd_tab(i).chapter,
                                      I_hts_cr_tl_upd_tab(i).import_country_id,
                                      I_hts_cr_tl_upd_tab(i).origin_country_id,
                                      I_hts_cr_tl_upd_tab(i).restraint_type,
                                      I_hts_cr_tl_upd_tab(i).lang);
            close C_LOCK_HTS_CR_TL_UPD;
            
            update hts_chapter_restraints_tl
               set restraint_desc = I_hts_cr_tl_upd_tab(i).restraint_desc,
                   last_update_id = I_hts_cr_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_hts_cr_tl_upd_tab(i).last_update_datetime
             where lang = I_hts_cr_tl_upd_tab(i).lang
               and chapter = I_hts_cr_tl_upd_tab(i).chapter
               and import_country_id = I_hts_cr_tl_upd_tab(i).import_country_id
               and origin_country_id = I_hts_cr_tl_upd_tab(i).origin_country_id
               and restraint_type    = I_hts_cr_tl_upd_tab(i).restraint_type;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_HTS_CHAPTER_RESTRAINTS_TL',
                           I_hts_cr_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_HTS_CR_TL_UPD%ISOPEN then
         close C_LOCK_HTS_CR_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_HTS_CR_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CR_TL_DEL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_hts_cr_tl_del_tab   IN       HTS_CHAPTER_RESTRAINTS_TAB,
                            I_hts_cr_tl_del_rst   IN       ROW_SEQ_TAB,
                            I_process_id          IN       SVC_HTS_CHAPTER_RESTRAINTS_TL.PROCESS_ID%TYPE,
                            I_chunk_id            IN       SVC_HTS_CHAPTER_RESTRAINTS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_HTS_SETUP.EXEC_HTS_CR_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CHAPTER_RESTRAINTS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_HTS_CR_TL_DEL(I_chapter             HTS_CHAPTER_RESTRAINTS_TL.CHAPTER%TYPE,
                               I_import_country_id   HTS_CHAPTER_RESTRAINTS_TL.IMPORT_COUNTRY_ID%TYPE,
                               I_origin_country_id   HTS_CHAPTER_RESTRAINTS_TL.ORIGIN_COUNTRY_ID%TYPE,
                               I_restraint_type      HTS_CHAPTER_RESTRAINTS_TL.RESTRAINT_TYPE%TYPE,
                               I_lang                HTS_CHAPTER_RESTRAINTS_TL.LANG%TYPE) is
      select 'x'
        from hts_chapter_restraints_tl
       where chapter = I_chapter
         and import_country_id = I_import_country_id
         and origin_country_id = I_origin_country_id
         and restraint_type = I_restraint_type
         and lang = I_lang
         for update nowait;

BEGIN
   if I_hts_cr_tl_del_tab is NOT NULL and I_hts_cr_tl_del_tab.count > 0 then
      for i in I_hts_cr_tl_del_tab.FIRST..I_hts_cr_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_hts_cr_tl_del_tab(i).lang);
            L_key_val2 := 'Chapter: '||to_char(I_hts_cr_tl_del_tab(i).chapter);
            open C_LOCK_HTS_CR_TL_DEL(I_hts_cr_tl_del_tab(i).chapter,
                                      I_hts_cr_tl_del_tab(i).import_country_id,
                                      I_hts_cr_tl_del_tab(i).origin_country_id,
                                      I_hts_cr_tl_del_tab(i).restraint_type,
                                      I_hts_cr_tl_del_tab(i).lang);
            close C_LOCK_HTS_CR_TL_DEL;
            
            delete hts_chapter_restraints_tl
             where lang              = I_hts_cr_tl_del_tab(i).lang
               and chapter           = I_hts_cr_tl_del_tab(i).chapter
               and import_country_id = I_hts_cr_tl_del_tab(i).import_country_id
               and origin_country_id = I_hts_cr_tl_del_tab(i).origin_country_id
               and restraint_type    = I_hts_cr_tl_del_tab(i).restraint_type;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_HTS_CHAPTER_RESTRAINTS_TL',
                           I_hts_cr_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_HTS_CR_TL_DEL%ISOPEN then
         close C_LOCK_HTS_CR_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_HTS_CR_TL_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_CR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_HTS_CHAPTER_RESTRAINTS.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_HTS_CHAPTER_RESTRAINTS.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64)                      :='CORESVC_HTS_SETUP.PROCESS_HTS_CR';
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_HTS_CHAPTER_RESTRAINTS';
   L_process_error     BOOLEAN                           := FALSE;
   L_error             BOOLEAN;
   L_hts_cr_temp_rec   HTS_CHAPTER_RESTRAINTS%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_HTS_CR(I_process_id,
                           I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;

      --Check if action is invalid
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      --Check if HTS_CHAPTER_RESTRAINT already exists
      if rec.action = action_new
         and rec.PK_HTS_CHAPTER_RESTRAINTS_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Chapter,Importing Country,Country Of Sourcing,Type',
                     'HTS_CHAP_RESTRAINT_EXIST');
         L_error := TRUE;
      end if;

      --Check if trying to modify, delete a non existent HTS_CHAPTER_RESTRAINT
      if rec.action IN (action_mod,action_del)
         and rec.PK_HTS_CHAPTER_RESTRAINTS_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Chapter,Importing Country,Country Of Sourcing,Type',
                     'INV_HTS_CHAP_RESTRAINT');
         L_error := TRUE;
      end if;

      --Check if HTS_CHAPTER is missing
      if rec.hcr_htc_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CHAPTER',
                     'INV_CHAPTER');
         L_error := TRUE;
      end if;

      --Check if QUOTA_CATEGORY is missing
      if rec.action IN (action_new,action_mod) then
         if rec.hcr_qca_fk_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'QUOTA_CAT',
                        'INV_QUOTA_CATEGORY');
            L_error := TRUE;
         end if;
      end if;

      --Check if given origin_country is not present
      if rec.hcr_cnt_fk2_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ORIGIN_COUNTRY_ID',
                     'INV_COUNTRY');
         L_error := TRUE;
      end if;

      --Check if quota category is NULL
      if rec.action IN (action_new,action_mod) then
         if NOT(rec.QUOTA_CAT is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'QUOTA_CAT',
                        'ENTER_CATEGORY');
            L_error := TRUE;
         end if;
      end if;

      --Check if closing date is NULL
      if rec.action IN (action_new,action_mod) then
         if NOT(rec.CLOSING_DATE is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CLOSING_DATE',
                        'CLOSE_DATE_EXIST');
            L_error := TRUE;
         end if;
      end if;

      --Check if UOM is NULL
      if rec.action IN (action_new,action_mod) then
         if NOT(rec.UOM is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'UOM',
                        'ENTER_UOM');
            L_error := TRUE;
         end if;
      end if;

      --Check if restraint quantity is NULL
      if rec.action IN (action_new,action_mod) then
         if NOT(rec.RESTRAINT_QTY is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'RESTRAINT_QTY',
                        'ENTER_QUANTITY');
            L_error := TRUE;
         end if;
      end if;

      --Check if restraint description is NULL
      if rec.action IN (action_new,action_mod) then
         if NOT(rec.RESTRAINT_DESC is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'RESTRAINT_DESC',
                        'SVC_COL_REQD');
            L_error := TRUE;
         end if;
      end if;

      --perform validation
      if PROCESS_HTS_CR_VAL(O_error_message,
                            L_error,
                            rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_hts_cr_temp_rec.chapter           := rec.chapter;
         L_hts_cr_temp_rec.import_country_id := rec.import_country_id;
         L_hts_cr_temp_rec.origin_country_id := rec.origin_country_id;
         L_hts_cr_temp_rec.restraint_desc    := rec.restraint_desc;
         L_hts_cr_temp_rec.restraint_qty     := rec.restraint_qty;
         L_hts_cr_temp_rec.uom               := rec.uom;
         L_hts_cr_temp_rec.closing_date      := rec.closing_date;
         L_hts_cr_temp_rec.quota_cat         := rec.quota_cat;
         L_hts_cr_temp_rec.restraint_suffix  := rec.restraint_suffix;

         if rec.action = action_mod then
            L_hts_cr_temp_rec.restraint_type := rec.restraint_type_upd;
         else
            L_hts_cr_temp_rec.restraint_type := rec.restraint_type;
         end if;

         --When action is NEW
         if rec.action = action_new then
            if EXEC_HTS_CR_INS(O_error_message,
                               L_hts_cr_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         --When action is MOD
         if rec.action = action_mod then
            if EXEC_HTS_CR_UPD(O_error_message,
                               L_hts_cr_temp_rec,
                               rec.restraint_type) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         --When action is DEL
         if rec.action = action_del then
            if EXEC_HTS_CR_DEL(O_error_message,
                               L_hts_cr_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_CR;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_CR_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       SVC_HTS_CHAPTER_RESTRAINTS_TL.PROCESS_ID%TYPE,
                           I_chunk_id        IN       SVC_HTS_CHAPTER_RESTRAINTS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_HTS_SETUP.PROCESS_HTS_CR_TL';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_CHAPTER_RESTRAINTS_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CHAPTER_RESTRAINTS_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CHAPTER_RESTRAINTS';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_hts_cr_tl_temp_rec   HTS_CHAPTER_RESTRAINTS_TL%ROWTYPE;
   L_hts_cr_tl_upd_rst    ROW_SEQ_TAB;
   L_hts_cr_tl_del_rst    ROW_SEQ_TAB;

   cursor C_SVC_HTS_CR_TL(I_process_id NUMBER,
                          I_chunk_id NUMBER) is
      select pk_hts_cr_tl.rowid               as pk_hts_cr_tl_rid,
             fk_hts_chapter_restraints.rowid  as fk_hts_cr_rid,
             fk_lang.rowid                    as fk_lang_rid,
             st.lang,
             st.chapter,
             st.import_country_id,
             st.origin_country_id,
             st.restraint_type,
             st.restraint_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)                 as action,
             st.process$status
        from svc_hts_chapter_restraints_tl   st,
             hts_chapter_restraints          fk_hts_chapter_restraints,
             hts_chapter_restraints_tl       pk_hts_cr_tl,
             lang                            fk_lang
       where st.process_id        =  I_process_id
         and st.chunk_id          =  I_chunk_id
         and st.chapter           =  fk_hts_chapter_restraints.chapter (+)
         and st.origin_country_id =  fk_hts_chapter_restraints.origin_country_id (+)
         and st.import_country_id =  fk_hts_chapter_restraints.import_country_id (+)
         and st.restraint_type    =  fk_hts_chapter_restraints.restraint_type (+)
         and st.lang              =  pk_hts_cr_tl.lang (+)
         and st.chapter           =  pk_hts_cr_tl.chapter (+)
         and st.origin_country_id =  pk_hts_cr_tl.origin_country_id (+)
         and st.import_country_id =  pk_hts_cr_tl.import_country_id (+)
         and st.restraint_type    =  pk_hts_cr_tl.restraint_type (+)
         and st.lang              =  fk_lang.lang (+)
         and (st.process$status = 'N' or st.process$status  is NULL);

   TYPE SVC_HTS_CR_TL is TABLE OF C_SVC_HTS_CR_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_hts_cr_tab        SVC_HTS_CR_TL;

   L_hts_cr_tl_ins_tab         hts_chapter_restraints_tab         := NEW hts_chapter_restraints_tab();
   L_hts_cr_tl_upd_tab         hts_chapter_restraints_tab         := NEW hts_chapter_restraints_tab();
   L_hts_cr_tl_del_tab         hts_chapter_restraints_tab         := NEW hts_chapter_restraints_tab();

BEGIN
   if C_SVC_HTS_CR_TL%ISOPEN then
      close C_SVC_HTS_CR_TL;
   end if;

   open C_SVC_HTS_CR_TL(I_process_id,
                        I_chunk_id);
   LOOP
      fetch C_SVC_HTS_CR_TL bulk collect into L_svc_hts_cr_tab limit LP_bulk_fetch_limit;
      if L_svc_hts_cr_tab.COUNT > 0 then
         FOR i in L_svc_hts_cr_tab.FIRST..L_svc_hts_cr_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_hts_cr_tab(i).action is NULL
               or L_svc_hts_cr_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_cr_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_hts_cr_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_cr_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_hts_cr_tab(i).action = action_new
               and L_svc_hts_cr_tab(i).pk_hts_cr_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_cr_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_hts_cr_tab(i).action IN (action_mod, action_del)
               and L_svc_hts_cr_tab(i).lang is NOT NULL
               and L_svc_hts_cr_tab(i).chapter is NOT NULL
               and L_svc_hts_cr_tab(i).import_country_id is NOT NULL
               and L_svc_hts_cr_tab(i).origin_country_id is NOT NULL
               and L_svc_hts_cr_tab(i).restraint_type is NOT NULL
               and L_svc_hts_cr_tab(i).pk_hts_cr_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_hts_cr_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_hts_cr_tab(i).action = action_new
               and L_svc_hts_cr_tab(i).chapter is NOT NULL
               and L_svc_hts_cr_tab(i).import_country_id is NOT NULL
               and L_svc_hts_cr_tab(i).origin_country_id is NOT NULL
               and L_svc_hts_cr_tab(i).restraint_type is NOT NULL
               and L_svc_hts_cr_tab(i).fk_hts_cr_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_hts_cr_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_hts_cr_tab(i).action = action_new
               and L_svc_hts_cr_tab(i).lang is NOT NULL
               and L_svc_hts_cr_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_cr_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_hts_cr_tab(i).action in (action_new, action_mod) then
               if L_svc_hts_cr_tab(i).restraint_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_hts_cr_tab(i).row_seq,
                              'RESTRAINT_DESC',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_hts_cr_tab(i).chapter is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_cr_tab(i).row_seq,
                           'CHAPTER',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_hts_cr_tab(i).import_country_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_cr_tab(i).row_seq,
                           'IMPORT_COUNTRY_ID',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_hts_cr_tab(i).origin_country_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_cr_tab(i).row_seq,
                           'ORIGIN_COUNTRY_ID',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_hts_cr_tab(i).restraint_type is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_cr_tab(i).row_seq,
                           'RESTRAINT_TYPE',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_hts_cr_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_cr_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_hts_cr_tl_temp_rec.lang := L_svc_hts_cr_tab(i).lang;
               L_hts_cr_tl_temp_rec.chapter := L_svc_hts_cr_tab(i).chapter;
               L_hts_cr_tl_temp_rec.import_country_id := L_svc_hts_cr_tab(i).import_country_id;
               L_hts_cr_tl_temp_rec.origin_country_id := L_svc_hts_cr_tab(i).origin_country_id;
               L_hts_cr_tl_temp_rec.restraint_type := L_svc_hts_cr_tab(i).restraint_type;
               L_hts_cr_tl_temp_rec.restraint_desc := L_svc_hts_cr_tab(i).restraint_desc;
               L_hts_cr_tl_temp_rec.create_datetime := SYSDATE;
               L_hts_cr_tl_temp_rec.create_id := GET_USER;
               L_hts_cr_tl_temp_rec.last_update_datetime := SYSDATE;
               L_hts_cr_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_hts_cr_tab(i).action = action_new then
                  L_hts_cr_tl_ins_tab.extend;
                  L_hts_cr_tl_ins_tab(L_hts_cr_tl_ins_tab.count()) := L_hts_cr_tl_temp_rec;
               end if;

               if L_svc_hts_cr_tab(i).action = action_mod then
                  L_hts_cr_tl_upd_tab.extend;
                  L_hts_cr_tl_upd_tab(L_hts_cr_tl_upd_tab.count()) := L_hts_cr_tl_temp_rec;
                  L_hts_cr_tl_upd_rst(L_hts_cr_tl_upd_tab.count()) := L_svc_hts_cr_tab(i).row_seq;
               end if;

               if L_svc_hts_cr_tab(i).action = action_del then
                  L_hts_cr_tl_del_tab.extend;
                  L_hts_cr_tl_del_tab(L_hts_cr_tl_del_tab.count()) := L_hts_cr_tl_temp_rec;
                  L_hts_cr_tl_del_rst(L_hts_cr_tl_del_tab.count()) := L_svc_hts_cr_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_HTS_CR_TL%NOTFOUND;
   END LOOP;
   close C_SVC_HTS_CR_TL;

   if EXEC_HTS_CR_TL_INS(O_error_message,
                         L_hts_cr_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_HTS_CR_TL_UPD(O_error_message,
                         L_hts_cr_tl_upd_tab,
                         L_hts_cr_tl_upd_rst,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_HTS_CR_TL_DEL(O_error_message,
                         L_hts_cr_tl_del_tab,
                         L_hts_cr_tl_del_rst,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_CR_TL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_QUOTA_CAT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error           IN OUT   BOOLEAN ,
                               I_rec             IN       C_SVC_QUOTA_CAT%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_HTS_SETUP.PROCESS_VAL_QUOTA_CAT';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_QUOTA_CATEGORY';
   L_exists    BOOLEAN                           := FALSE;

BEGIN
---for delete, if quota category is referred in dependent tables
   if I_rec.action = action_del
      and I_rec.quota_cat is NOT NULL
      and I_rec.import_country_id is NOT NULL then
      if HTS_SQL.QUOTA_CAT_EXISTS(O_error_message,
                                  L_exists,
                                  I_rec.quota_cat,
                                  I_rec.import_country_id) = FALSE then
         return FALSE;
      end if;
      if L_exists = TRUE then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'QUOTA_CAT',
                     'NO_DELT_QUOTA_CAT');
        O_error :=true;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END PROCESS_VAL_QUOTA_CAT;
--------------------------------------------------------------------------------------
FUNCTION EXEC_QUOTA_CAT_INS( O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_quota_cat_temp_rec   IN       QUOTA_CATEGORY%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_SETUP.EXEC_QUOTA_CAT_INS';
BEGIN
   insert into quota_category
        values I_quota_cat_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_QUOTA_CAT_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_QUOTA_CAT_UPD( O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_quota_cat_temp_rec   IN       QUOTA_CATEGORY%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_SETUP.EXEC_QUOTA_CAT_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'QUOTA_CATEGORY';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_QUOTA_CAT_UPD is
      select 'x'
        from quota_category
       where import_country_id = I_quota_cat_temp_rec.import_country_id
         and quota_cat         = I_quota_cat_temp_rec.quota_cat
         for update nowait;

BEGIN
   update quota_category
      set row = I_quota_cat_temp_rec
    where 1 = 1
      and import_country_id = I_quota_cat_temp_rec.import_country_id
      and quota_cat         = I_quota_cat_temp_rec.quota_cat;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_quota_cat_temp_rec.import_country_id,
                                             I_quota_cat_temp_rec.quota_cat);
      return FALSE;
   when OTHERS then
      if C_LOCK_QUOTA_CAT_UPD%ISOPEN then
         close C_LOCK_QUOTA_CAT_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_QUOTA_CAT_UPD;
---------------------------------------------------------------------------------------------------
FUNCTION EXEC_QUOTA_CAT_DEL( O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_quota_cat_temp_rec   IN       QUOTA_CATEGORY%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_SETUP.EXEC_QUOTA_CAT_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'QUOTA_CATEGORY';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_exists        BOOLEAN;

   cursor C_LOCK_QUOTA_CAT_TL_DEL is
      select 'x'
        from quota_category_tl
       where import_country_id = I_quota_cat_temp_rec.import_country_id
         and quota_cat         = I_quota_cat_temp_rec.quota_cat
         for update nowait;

   cursor C_LOCK_QUOTA_CAT_DEL is
      select 'x'
        from quota_category
       where import_country_id = I_quota_cat_temp_rec.import_country_id
         and quota_cat         = I_quota_cat_temp_rec.quota_cat
         for update nowait;

   cursor C_LOCK_HTS_CR_DEL is
      select 'x'
        from hts_chapter_restraints ch
       where exists (select 1 
                     from svc_hts_chapter_restraints scr
                    where scr.import_country_id = ch.import_country_id
                      and scr.quota_cat         = ch.quota_cat
                      and quota_cat             = I_quota_cat_temp_rec.quota_cat
                      and import_country_id     = I_quota_cat_temp_rec.import_country_id
                      and upper(action)         =  upper(action_del))
         for update nowait;
   
  cursor C_LOCK_HTS_CR_TL_DEL is
     select 'x'
       from hts_chapter_restraints_tl chrtl
      where exists(select 1 
                     from hts_chapter_restraints  cr
                    where cr.restraint_type        = chrtl.restraint_type
                      and cr.origin_country_id     = chrtl.origin_country_id
                      and cr.import_country_id     = chrtl.import_country_id
                      and cr.chapter               = chrtl.chapter
                      and cr.import_country_id     = I_quota_cat_temp_rec.import_country_id
                      and cr.quota_cat             = I_quota_cat_temp_rec.quota_cat)
        and exists(select 1
                    from svc_hts_chapter_restraints_tl scrtl
                   where scrtl.restraint_type     = chrtl.restraint_type
                     and scrtl.origin_country_id  = chrtl.origin_country_id
                     and scrtl.import_country_id  = chrtl.import_country_id
                     and scrtl.chapter            = chrtl.chapter
                     and upper(action)            =  upper(action_del))
        for update nowait;
BEGIN
   open  C_LOCK_HTS_CR_TL_DEL;
   close C_LOCK_HTS_CR_TL_DEL;
   
   delete
      from hts_chapter_restraints_tl chrtl
     where exists(select 1 
                    from hts_chapter_restraints  cr
                   where cr.restraint_type        = chrtl.restraint_type
                     and cr.origin_country_id     = chrtl.origin_country_id
                     and cr.import_country_id     = chrtl.import_country_id
                     and cr.chapter               = chrtl.chapter
                     and cr.import_country_id     = I_quota_cat_temp_rec.import_country_id
                     and cr.quota_cat             = I_quota_cat_temp_rec.quota_cat)
       and exists(select 1
                    from svc_hts_chapter_restraints_tl scrtl
                   where scrtl.restraint_type     = chrtl.restraint_type
                     and scrtl.origin_country_id  = chrtl.origin_country_id
                     and scrtl.import_country_id  = chrtl.import_country_id
                     and scrtl.chapter            = chrtl.chapter
                     and upper(action)            =  upper(action_del));

   if(sql%rowcount > 0) then   
      update svc_hts_chapter_restraints_tl chrtl
         set process$status ='P'
        where exists(select 1 
                       from hts_chapter_restraints  chr
                      where chr.restraint_type        = chrtl.restraint_type
                        and chr.origin_country_id     = chrtl.origin_country_id
                        and chr.import_country_id     = chrtl.import_country_id
                        and chr.chapter               = chrtl.chapter
                        and chr.import_country_id     = I_quota_cat_temp_rec.import_country_id
                        and chr.quota_cat             = I_quota_cat_temp_rec.quota_cat)
          and exists(select 1
                       from svc_hts_chapter_restraints
                      where quota_cat            = I_quota_cat_temp_rec.quota_cat
                        and import_country_id    = I_quota_cat_temp_rec.import_country_id
                        and upper(action)        =  upper(action_del));
   end if;

   open  C_LOCK_HTS_CR_DEL;
   close C_LOCK_HTS_CR_DEL;   
   
   delete
      from hts_chapter_restraints ch
     where exists (select 1 
                     from svc_hts_chapter_restraints scr
                    where scr.import_country_id = ch.import_country_id
                      and scr.quota_cat         = ch.quota_cat
                      and quota_cat             = I_quota_cat_temp_rec.quota_cat
                      and import_country_id     = I_quota_cat_temp_rec.import_country_id
                      and upper(action)         =  upper(action_del));

   if(sql%rowcount > 0) then
      update svc_hts_chapter_restraints
         set process$status ='P'
       where quota_cat          = I_quota_cat_temp_rec.quota_cat
         and import_country_id  = I_quota_cat_temp_rec.import_country_id
         and upper(action)      =  upper(action_del);
   end if;
   
   if HTS_SQL.QUOTA_CAT_EXISTS(O_error_message,
                               L_exists,
                               I_quota_cat_temp_rec.quota_cat,
                               I_quota_cat_temp_rec.import_country_id) = FALSE then
      return FALSE;
   end if;

   if L_exists then
      --This quota category is currently in use. Cannot delete.
      O_error_message := 'NO_DELT_QUOTA_CAT';
      return FALSE;
   end if;
    
   open C_LOCK_QUOTA_CAT_TL_DEL;
   close C_LOCK_QUOTA_CAT_TL_DEL;

   delete from quota_category_tl
    where import_country_id = I_quota_cat_temp_rec.import_country_id
      and quota_cat         = I_quota_cat_temp_rec.quota_cat;

   open C_LOCK_QUOTA_CAT_DEL;
   close C_LOCK_QUOTA_CAT_DEL;

   delete from quota_category
    where import_country_id = I_quota_cat_temp_rec.import_country_id
      and quota_cat         = I_quota_cat_temp_rec.quota_cat;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_quota_cat_temp_rec.import_country_id,
                                             I_quota_cat_temp_rec.quota_cat);
      return FALSE;
   when OTHERS then
      if C_LOCK_QUOTA_CAT_TL_DEL%ISOPEN then
         close C_LOCK_QUOTA_CAT_TL_DEL;
      end if;
      if C_LOCK_QUOTA_CAT_DEL%ISOPEN then
         close C_LOCK_QUOTA_CAT_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_QUOTA_CAT_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_QUOTA_CATEGORY_TL_INS(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_quota_category_tl_ins_tab    IN       QUOTA_CATEGORY_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_SETUP.EXEC_QUOTA_CATEGORY_TL_INS';

BEGIN
   if I_quota_category_tl_ins_tab is NOT NULL and I_quota_category_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_quota_category_tl_ins_tab.COUNT()
         insert into quota_category_TL
              values I_quota_category_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_QUOTA_CATEGORY_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_QUOTA_CATEGORY_TL_DEL(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_quota_category_tl_del_tab   IN       QUOTA_CATEGORY_TAB,
                                    I_quota_category_tl_del_rst   IN       ROW_SEQ_TAB,
                                    I_process_id                  IN       SVC_QUOTA_CATEGORY_TL.PROCESS_ID%TYPE,
                                    I_chunk_id                    IN       SVC_QUOTA_CATEGORY_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_HTS_SETUP.EXEC_QUOTA_CATEGORY_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'QUOTA_CATEGORY_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_QUOTA_CATEGORY_TL_DEL(I_quota_category     QUOTA_CATEGORY_TL.QUOTA_CAT%TYPE,
                                       I_import_country_id 	QUOTA_CATEGORY_TL.IMPORT_COUNTRY_ID%TYPE,
                                       I_lang               QUOTA_CATEGORY_TL.LANG%TYPE) is
      select 'x'
        from quota_category_tl
       where quota_cat = I_quota_category
         and import_country_id = I_import_country_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_quota_category_tl_del_tab is NOT NULL and I_quota_category_tl_del_tab.count > 0 then
      for i in I_quota_category_tl_del_tab.FIRST..I_quota_category_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_quota_category_tl_del_tab(i).lang);
            L_key_val2 := 'Quota Category: '||to_char(I_quota_category_tl_del_tab(i).quota_cat);
            open C_LOCK_QUOTA_CATEGORY_TL_DEL(I_quota_category_tl_del_tab(i).quota_cat,
                                              I_quota_category_tl_del_tab(i).import_country_id,
                                              I_quota_category_tl_del_tab(i).lang);
            close C_LOCK_QUOTA_CATEGORY_TL_DEL;
           
            delete quota_category_tl
             where lang = I_quota_category_tl_del_tab(i).lang
               and import_country_id = I_quota_category_tl_del_tab(i).import_country_id
               and quota_cat = I_quota_category_tl_del_tab(i).quota_cat;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_QUOTA_CATEGORY_TL',
                           I_quota_category_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_QUOTA_CATEGORY_TL_DEL%ISOPEN then
         close C_LOCK_QUOTA_CATEGORY_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_QUOTA_CATEGORY_TL_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_QUOTA_CATEGORY_TL_UPD(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_quota_category_tl_upd_tab   IN       QUOTA_CATEGORY_TAB,
                                    I_quota_category_tl_upd_rst   IN       ROW_SEQ_TAB,
                                    I_process_id                  IN       SVC_QUOTA_CATEGORY_TL.PROCESS_ID%TYPE,
                                    I_chunk_id                    IN       SVC_QUOTA_CATEGORY_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_HTS_SETUP.EXEC_QUOTA_CATEGORY_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'QUOTA_CATEGORY_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_QUOTA_CATEGORY_TL_UPD(I_quota_category     QUOTA_CATEGORY_TL.QUOTA_CAT%TYPE,
                                       I_import_country_id 	QUOTA_CATEGORY_TL.IMPORT_COUNTRY_ID%TYPE,
                                       I_lang               QUOTA_CATEGORY_TL.LANG%TYPE) is
      select 'x'
        from quota_category_tl
       where quota_cat = I_quota_category
         and import_country_id = I_import_country_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_quota_category_tl_upd_tab is NOT NULL and I_quota_category_tl_upd_tab.count > 0 then
      for i in I_quota_category_tl_upd_tab.FIRST..I_quota_category_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_quota_category_tl_upd_tab(i).lang);
            L_key_val2 := 'Quota Category: '||to_char(I_quota_category_tl_upd_tab(i).quota_cat);
            open C_LOCK_QUOTA_CATEGORY_TL_UPD(I_quota_category_tl_upd_tab(i).quota_cat,
                                              I_quota_category_tl_upd_tab(i).import_country_id,
                                              I_quota_category_tl_upd_tab(i).lang);
            close C_LOCK_QUOTA_CATEGORY_TL_UPD;
            
            update quota_category_tl
               set category_desc = I_quota_category_tl_upd_tab(i).category_desc,
                   last_update_id = I_quota_category_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_quota_category_tl_upd_tab(i).last_update_datetime
             where lang = I_quota_category_tl_upd_tab(i).lang
               and import_country_id = I_quota_category_tl_upd_tab(i).import_country_id
               and quota_cat = I_quota_category_tl_upd_tab(i).quota_cat;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_QUOTA_CATEGORY_TL',
                           I_quota_category_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_QUOTA_CATEGORY_TL_UPD%ISOPEN then
         close C_LOCK_QUOTA_CATEGORY_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_QUOTA_CATEGORY_TL_UPD;
---------------------------------------------------------------------------------------------------
FUNCTION PROCESS_QUOTA_CAT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       SVC_QUOTA_CATEGORY.PROCESS_ID%TYPE,
                           I_chunk_id        IN       SVC_QUOTA_CATEGORY.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_HTS_SETUP.PROCESS_QUOTA_CAT';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_QUOTA_CATEGORY';
   L_error                BOOLEAN;
   L_process_error        BOOLEAN := FALSE;
   L_quota_cat_temp_rec   QUOTA_CATEGORY%ROWTYPE;

BEGIN
   FOR rec IN C_SVC_QUOTA_CAT(I_process_id,
                              I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;

      --for invalid action
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

      --for duplicate record
      if rec.action = action_new
         and rec.PK_QUOTA_CATEGORY_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Quota Category,Importing Country',
                     'QUOTA_CAT_EXISTS');
         L_error :=TRUE;
      end if;

      --for insert/update if category desc is NULL
      if rec.action IN (action_new, action_mod)
         and NOT(  rec.CATEGORY_DESC  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CATEGORY_DESC',
                     'ENTER_DESC');
         L_error :=TRUE;
      end if;

      --for update/delete if record not exists
      if rec.action IN (action_mod,action_del)
         and rec.quota_cat is NOT NULL
         and rec.import_country_id is NOT NUll
         and rec.PK_QUOTA_CATEGORY_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Quota Category,Importing Country',
                     'INV_QUOTA_CATEGORY');
         L_error :=TRUE;
      end if;

      --for insert if country_id not exists in COUNTRY table(foreign key)
      if rec.action = action_new
         and rec.import_country_id is NOT NULL
         and rec.qca_cnt_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     'INV_COUNTRY');
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_quota_cat_temp_rec.quota_cat         := rec.quota_cat;
         L_quota_cat_temp_rec.import_country_id := rec.import_country_id;
         L_quota_cat_temp_rec.category_desc     := rec.category_desc;
         if rec.action = action_new then
            if EXEC_QUOTA_CAT_INS( O_error_message,
                                   L_quota_cat_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_QUOTA_CAT_UPD( O_error_message,
                                   L_quota_cat_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_QUOTA_CAT_DEL( O_error_message,
                                   L_quota_cat_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_QUOTA_CAT;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_QUOTA_CAT_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_QUOTA_CATEGORY_TL.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_QUOTA_CATEGORY_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                       VARCHAR2(64) := 'CORESVC_HTS_SETUP.PROCESS_QUOTA_CAT_TL';
   L_error_message                 RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'QUOTA_CATEGORY_TL';
   L_base_table                    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'QUOTA_CATEGORY';
   L_table                         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_QUOTA_CATEGORY_TL';
   L_error                         BOOLEAN := FALSE;
   L_process_error                 BOOLEAN := FALSE;
   L_quota_category_tl_temp_rec    QUOTA_CATEGORY_TL%ROWTYPE;
   L_quota_category_tl_upd_rst     ROW_SEQ_TAB;
   L_quota_category_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_QUOTA_CATEGORY_TL(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select pk_quota_category_TL.rowid  as pk_quota_category_tl_rid,
             fk_quota_category.rowid     as fk_quota_category_rid,
             fk_lang.rowid               as fk_lang_rid,
             st.lang,
             st.quota_cat,
             UPPER(st.import_country_id) as import_country_id,
             st.category_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_quota_category_tl  st,
             quota_category         fk_quota_category,
             quota_category_tl      pk_quota_category_tl,
             lang                   fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.quota_cat          =  fk_quota_category.quota_cat (+)
         and st.import_country_id  = fk_quota_category.import_country_id (+)
         and st.lang               = pk_quota_category_tl.lang (+)
         and st.quota_cat          = pk_quota_category_TL.quota_cat (+)
         and st.import_country_id  = pk_quota_category_TL.import_country_id (+)
         and st.lang               = fk_lang.lang (+);

   TYPE SVC_QUOTA_CATEGORY_TL is TABLE OF C_SVC_QUOTA_CATEGORY_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_quota_category_tab        SVC_QUOTA_CATEGORY_TL;

   L_quota_category_TL_ins_tab         quota_category_tab         := NEW quota_category_tab();
   L_quota_category_TL_upd_tab         quota_category_tab         := NEW quota_category_tab();
   L_quota_category_TL_del_tab         quota_category_tab         := NEW quota_category_tab();

BEGIN
   if C_SVC_QUOTA_CATEGORY_TL%ISOPEN then
      close C_SVC_QUOTA_CATEGORY_TL;
   end if;

   open C_SVC_QUOTA_CATEGORY_TL(I_process_id,
                                I_chunk_id);
   LOOP
      fetch C_SVC_QUOTA_CATEGORY_TL bulk collect into L_svc_quota_category_tab limit LP_bulk_fetch_limit;
      if L_svc_quota_category_tab.COUNT > 0 then
         FOR i in L_svc_quota_category_tab.FIRST..L_svc_quota_category_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_quota_category_tab(i).action is NULL
               or L_svc_quota_category_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_quota_category_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_quota_category_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_quota_category_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_quota_category_tab(i).action = action_new
               and L_svc_quota_category_tab(i).pk_quota_category_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_quota_category_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_quota_category_tab(i).action IN (action_mod, action_del)
               and L_svc_quota_category_tab(i).lang is NOT NULL
               and L_svc_quota_category_tab(i).quota_cat is NOT NULL
               and L_svc_quota_category_tab(i).import_country_id is NOT NULL
               and L_svc_quota_category_tab(i).pk_quota_category_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_quota_category_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_quota_category_tab(i).action = action_new
               and L_svc_quota_category_tab(i).fk_quota_category_rid is NULL
               and L_svc_quota_category_tab(i).quota_cat is NOT NULL
               and L_svc_quota_category_tab(i).import_country_id is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_quota_category_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_quota_category_tab(i).action = action_new
               and L_svc_quota_category_tab(i).lang is NOT NULL
               and L_svc_quota_category_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_quota_category_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_quota_category_tab(i).action in (action_new, action_mod) then
               if L_svc_quota_category_tab(i).category_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_quota_category_tab(i).row_seq,
                              'CATEGORY_DESC',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_quota_category_tab(i).import_country_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_quota_category_tab(i).row_seq,
                           'IMPORT_COUNTRY_ID',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_quota_category_tab(i).quota_cat is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_quota_category_tab(i).row_seq,
                           'QUOTA_CAT',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_quota_category_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_quota_category_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_quota_category_tl_temp_rec.lang := L_svc_quota_category_tab(i).lang;
               L_quota_category_tl_temp_rec.quota_cat := L_svc_quota_category_tab(i).quota_cat;
               L_quota_category_tl_temp_rec.import_country_id := L_svc_quota_category_tab(i).import_country_id;
               L_quota_category_tl_temp_rec.category_desc := L_svc_quota_category_tab(i).category_desc;
               L_quota_category_tl_temp_rec.create_datetime := SYSDATE;
               L_quota_category_tl_temp_rec.create_id := GET_USER;
               L_quota_category_tl_temp_rec.last_update_datetime := SYSDATE;
               L_quota_category_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_quota_category_tab(i).action = action_new then
                  L_quota_category_TL_ins_tab.extend;
                  L_quota_category_TL_ins_tab(L_quota_category_TL_ins_tab.count()) := L_quota_category_tl_temp_rec;
               end if;

               if L_svc_quota_category_tab(i).action = action_mod then
                  L_quota_category_TL_upd_tab.extend;
                  L_quota_category_TL_upd_tab(L_quota_category_TL_upd_tab.count()) := L_quota_category_tl_temp_rec;
                  L_quota_category_tl_upd_rst(L_quota_category_TL_upd_tab.count()) := L_svc_quota_category_tab(i).row_seq;
               end if;

               if L_svc_quota_category_tab(i).action = action_del then
                  L_quota_category_TL_del_tab.extend;
                  L_quota_category_TL_del_tab(L_quota_category_TL_del_tab.count()) := L_quota_category_tl_temp_rec;
                  L_quota_category_tl_del_rst(L_quota_category_TL_del_tab.count()) := L_svc_quota_category_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_QUOTA_CATEGORY_TL%NOTFOUND;
   END LOOP;
   close C_SVC_QUOTA_CATEGORY_TL;

   if EXEC_QUOTA_CATEGORY_TL_INS(O_error_message,
                                 L_quota_category_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_QUOTA_CATEGORY_TL_UPD(O_error_message,
                                 L_quota_category_TL_upd_tab,
                                 L_quota_category_tl_upd_rst,
                                 I_process_id,
                                 I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_QUOTA_CATEGORY_TL_DEL(O_error_message,
                                 L_quota_category_TL_del_tab,
                                 L_quota_category_tl_del_rst,
                                 I_process_id,
                                 I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_QUOTA_CAT_TL;
-------------------------------------------------------------------------------------------
FUNCTION EXEC_COUNTRY_TRF_TT_INS (O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_country_trf_tt_temp_rec IN     COUNTRY_TARIFF_TREATMENT%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)  := 'CORESVC_HTS_SETUP.EXEC_COUNTRY_TRF_TT_INS';
BEGIN
  insert into COUNTRY_TARIFF_TREATMENT 
       values I_country_trf_tt_temp_rec;
  return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END EXEC_COUNTRY_TRF_TT_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_COUNTRY_TRF_TT_UPD(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_country_trf_tt_temp_rec IN     COUNTRY_TARIFF_TREATMENT%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_SETUP.EXEC_COUNTRY_TRF_TT_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COUNTRY_TARIFF_TREATMENT';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_COUNTRY_TRF_TT_UPD is
      select 'x'
        from country_tariff_treatment
       where effective_from   = I_country_trf_tt_temp_rec.EFFECTIVE_FROM
         and tariff_treatment = I_country_trf_tt_temp_rec.TARIFF_TREATMENT
         and country_id       = I_country_trf_tt_temp_rec.COUNTRY_ID
         for update nowait;

BEGIN
   open  C_LOCK_COUNTRY_TRF_TT_UPD;
   close C_LOCK_COUNTRY_TRF_TT_UPD;

   update country_tariff_treatment 
      set row = I_country_trf_tt_temp_rec
    where effective_from   = I_country_trf_tt_temp_rec.EFFECTIVE_FROM
      and tariff_treatment = I_country_trf_tt_temp_rec.TARIFF_TREATMENT
      and country_id       = I_country_trf_tt_temp_rec.COUNTRY_ID;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_country_trf_tt_temp_rec.effective_from||','||I_country_trf_tt_temp_rec.tariff_treatment,
                                             I_country_trf_tt_temp_rec.country_id);
      return FALSE;
   when OTHERS then
      if C_LOCK_COUNTRY_TRF_TT_UPD%ISOPEN then
         close C_LOCK_COUNTRY_TRF_TT_UPD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END EXEC_COUNTRY_TRF_TT_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_COUNTRY_TRF_TT_DEL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_country_trf_tt_temp_rec IN     COUNTRY_TARIFF_TREATMENT%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_SETUP.EXEC_COUNTRY_TRF_TT_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COUNTRY_TARIFF_TREATMENT';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_COUNTRY_TRF_TT_DEL is
      select 'x'
        from country_tariff_treatment
       where effective_from   = I_country_trf_tt_temp_rec.effective_from
         and tariff_treatment = I_country_trf_tt_temp_rec.tariff_treatment
         and country_id       = I_country_trf_tt_temp_rec.country_id
         for update nowait;

BEGIN
   open  C_LOCK_COUNTRY_TRF_TT_DEL;
   close C_LOCK_COUNTRY_TRF_TT_DEL;

   delete from COUNTRY_TARIFF_TREATMENT 
    where effective_from   = I_country_trf_tt_temp_rec.effective_from
      and tariff_treatment = I_country_trf_tt_temp_rec.tariff_treatment
      and country_id       = I_country_trf_tt_temp_rec.country_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_country_trf_tt_temp_rec.effective_from||','||I_country_trf_tt_temp_rec.tariff_treatment,
                                             I_country_trf_tt_temp_rec.country_id);
      return FALSE;
   when OTHERS then
      if C_LOCK_COUNTRY_TRF_TT_DEL%ISOPEN then
         close C_LOCK_COUNTRY_TRF_TT_DEL;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END EXEC_COUNTRY_TRF_TT_DEL;
--------------------------------------------------------------------------------
FUNCTION P_CHECK_EFFECTIVE_FROM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error          IN OUT BOOLEAN,
                                I_rec            IN     C_SVC_COUNTRY_TARIFF_TREATMENT%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_HTS_SETUP.P_CHECK_EFFECTIVE_FROM';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COUNTRY_TARIFF_TREATMENT';

BEGIN
   if I_rec.action = action_new then
      if I_rec.effective_from is NOT NULL then
         if I_rec.effective_to is not NULL and
            I_rec.effective_from > I_rec.effective_to then
               WRITE_ERROR(I_rec.process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_rec.chunk_id,
                          L_table,
                          I_rec.row_seq,
                          'EFFECTIVE_FROM',
                          'FROM_DATE_BEFORE_TO_DATE');
               O_error :=TRUE;
         end if;
      end if;   
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program, 
                                             TO_CHAR(SQLCODE));

END P_CHECK_EFFECTIVE_FROM;
--------------------------------------------------------------------------------
FUNCTION P_CHECK_EFFECTIVE_TO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error         IN OUT BOOLEAN,
                              I_rec           IN     C_SVC_COUNTRY_TARIFF_TREATMENT%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_HTS_SETUP.P_CHECK_EFFECTIVE_TO';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COUNTRY_TARIFF_TREATMENT';

BEGIN

   if I_rec.action = action_mod then
      if I_rec.effective_to is NOT NULL then
         if I_rec.effective_from is NOT NULL and
            I_rec.effective_to < I_rec.effective_from then
               WRITE_ERROR(I_rec.process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_rec.chunk_id,
                          L_table,
                          I_rec.row_seq,
                          'EFFECTIVE_TO',
                          'TO_DATE_AFTER_FROM_DATE');
               O_error :=TRUE;
         end if;
      end if;   
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program, 
                                             TO_CHAR(SQLCODE));

END P_CHECK_EFFECTIVE_TO;
--------------------------------------------------------------------------------
FUNCTION P_CHECK_OVERLAP(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error          IN OUT BOOLEAN,
                         I_rec            IN     C_SVC_COUNTRY_TARIFF_TREATMENT%ROWTYPE )
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_HTS_SETUP.P_CHECK_OVERLAP';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COUNTRY_TARIFF_TREATMENT';
   L_overlap  VARCHAR2(1)  := 'N';
   
   cursor C_OVERLAP is
      select 'Y'
        from country_tariff_treatment
       where country_id       = I_rec.country_id
         and tariff_treatment = I_rec.tariff_treatment
         and not (NVL(I_rec.effective_to,effective_from) < effective_from
                  or I_rec.effective_from > NVL(effective_to,I_rec.effective_from))
         and(I_rec.PK_COUNTRY_TARIFF_TREATMEN_rid != COUNTRY_TARIFF_TREATMENT.rowid
               or I_rec.PK_COUNTRY_TARIFF_TREATMEN_rid is NULL);

BEGIN
   open C_OVERLAP;
   fetch C_OVERLAP into L_overlap;
   close C_OVERLAP;
   if L_overlap = 'Y' then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'TARIFF_TREATMENT',
                  'OVERLAP_TARIFF_TREAT');
      O_error :=TRUE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_OVERLAP%ISOPEN then
         close C_OVERLAP;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program, 
                                             TO_CHAR(SQLCODE));
END P_CHECK_OVERLAP;
--------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_COUNTRY_TAR_TT(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error          IN OUT BOOLEAN,
                                    I_rec            IN     C_SVC_COUNTRY_TARIFF_TREATMENT%ROWTYPE) 
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_COUNTRY_ATTR.PROCESS_VAL_COUNTRY_TAR_TT';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COUNTRY_TARIFF_TREATMENT';
   L_exists         BOOLEAN;
   L_country_desc   COUNTRY.COUNTRY_DESC%TYPE;

BEGIN
   if I_rec.action IN (action_new) then
      if COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                       I_rec.country_id,   
                                       L_country_desc) = FALSE then 
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COUNTRY_ID',
                     O_error_message);
         O_error := TRUE;
      end if;
   end if;

   if P_CHECK_EFFECTIVE_FROM(O_error_message,
                             O_error,
                             I_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'EFFECTIVE_FROM',
                  O_error_message);
      O_error := TRUE;
   end if;
                          
   if P_CHECK_EFFECTIVE_TO(O_error_message,
                           O_error,
                           I_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'EFFECTIVE_TO',
                  O_error_message);
      O_error := TRUE;
   end if;

   if P_CHECK_OVERLAP(O_error_message,
                      O_error,
                      I_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      O_error := TRUE;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_VAL_COUNTRY_TAR_TT;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_COUNTRY_TRF_TT(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id     IN     SVC_COUNTRY_TARIFF_TREATMENT.PROCESS_ID%TYPE,
                                I_chunk_id       IN     SVC_COUNTRY_TARIFF_TREATMENT.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                       := 'CORESVC_HTS_SETUP.PROCESS_COUNTRY_TRF_TT';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_COUNTRY_TARIFF_TREATMENT';
   L_error   BOOLEAN;
   L_process_error BOOLEAN;
   L_country_trf_tt_temp_rec COUNTRY_TARIFF_TREATMENT%ROWTYPE;
   
BEGIN
   FOR rec IN C_SVC_COUNTRY_TARIFF_TREATMENT(I_process_id,I_chunk_id)
   LOOP
      L_error       := FALSE;
      
      if rec.action is NULL or 
         rec.action NOT IN (action_new,action_mod,action_del)then
         
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new and 
         rec.pk_country_tariff_treatmen_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Country,Effective From Date,Tariff Treatment',
                     'TARIFF_EXISTS');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del) and 
         rec.pk_country_tariff_treatmen_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Country,Effective From Date,Tariff Treatment',
                     'COUNTRY_TT_MISSING');
         L_error :=TRUE;
      end if;

      if rec.ctr_ttt_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TARIFF_TREATMENT',
                     'TARIFF_TREATMENT_MISSING');
         L_error :=TRUE;
      end if;

      if PROCESS_VAL_COUNTRY_TAR_TT(O_error_message,
                                    L_error,
                                    rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_country_trf_tt_temp_rec.EFFECTIVE_TO       := rec.EFFECTIVE_TO;
         L_country_trf_tt_temp_rec.EFFECTIVE_FROM     := rec.EFFECTIVE_FROM;
         L_country_trf_tt_temp_rec.TARIFF_TREATMENT   := rec.TARIFF_TREATMENT;
         L_country_trf_tt_temp_rec.COUNTRY_ID         := rec.COUNTRY_ID;

         L_process_error := FALSE;

         if rec.action = action_new then
            if EXEC_COUNTRY_TRF_TT_INS(O_error_message,
                                       L_country_trf_tt_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_COUNTRY_TRF_TT_UPD(O_error_message,
                                       L_country_trf_tt_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_COUNTRY_TRF_TT_DEL(O_error_message,
                                       L_country_trf_tt_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_SVC_COUNTRY_TARIFF_TREATMENT%ISOPEN then
         close C_SVC_COUNTRY_TARIFF_TREATMENT;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;   

END PROCESS_COUNTRY_TRF_TT;
-----------------------------------------------------------------------------
FUNCTION EXEC_OGA_INS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE, 
                      L_oga_temp_rec    IN       OGA%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_OGA_INS';
   L_table   VARCHAR2(64):= 'SVC_OGA';
BEGIN
   insert into oga
        values L_oga_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_OGA_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_OGA_UPD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      L_oga_temp_rec    IN       OGA%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_OGA_UPD';
   L_table   VARCHAR2(64):= 'SVC_OGA';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   
    --Cursor to lock the record
   cursor C_LOCK_OGA_UPD is
      select 'x'
        from oga
       where oga_code = L_oga_temp_rec.oga_code;
BEGIN
   ---
   open  C_LOCK_OGA_UPD;
   close C_LOCK_OGA_UPD;
   ---
   update oga
      set row = L_oga_temp_rec
    where oga_code = L_oga_temp_rec.oga_code;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_oga_temp_rec.oga_code,
                                             NULL);
      return FALSE;
   when OTHERS then
       if C_LOCK_OGA_UPD%ISOPEN then
         close C_LOCK_OGA_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_OGA_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_OGA_DEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_oga_temp_rec    IN       OGA%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_OGA_DEL';
   L_table   VARCHAR2(64):= 'SVC_OGA';
   L_exists  VARCHAR2(1) := 'N';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_OGA_TL_DEL is
      select 'X'
        from oga_tl
       where oga_code = I_oga_temp_rec.oga_code
       for update nowait;

   cursor C_LOCK_OGA_DEL is
      select 'x'
        from oga
       where oga_code = I_oga_temp_rec.oga_code
       for update nowait;

   cursor C_CHK_CHILD_EXISTS is
      select 'Y'
        from(select oga_code from HTS_OGA
             union
             select oga_code from CE_FORMS) DEP_TBL
       where DEP_TBL.oga_code = I_oga_temp_rec.oga_code;
BEGIN

   open C_CHK_CHILD_EXISTS;
   fetch C_CHK_CHILD_EXISTS into L_exists;
   close C_CHK_CHILD_EXISTS;
   
   if L_exists = 'Y' then
      -- An Other Government Agency (OGA) Code is currently in use. Cannot delete.
      O_error_message := 'NO_DELT_OGA_CODE';
      return FALSE;   
   end if;

   open  C_LOCK_OGA_TL_DEL;
   close C_LOCK_OGA_TL_DEL;
   
   delete
     from oga_tl
    where oga_code = I_oga_temp_rec.oga_code;

   open  C_LOCK_OGA_DEL;
   close C_LOCK_OGA_DEL;

   delete
     from oga
    where oga_code = I_oga_temp_rec.oga_code;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_oga_temp_rec.oga_code,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_OGA_DEL%ISOPEN then
         close C_LOCK_OGA_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_OGA_DEL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_OGA( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_process_id      IN       SVC_OGA.PROCESS_ID%TYPE,
                      I_chunk_id        IN       SVC_OGA.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) :='CORESVC_HTS_SETUP.PROCESS_OGA';
   L_table     VARCHAR2(64) :='SVC_OGA';
   L_exists    VARCHAR2(1)  := 'N';
   L_process_error BOOLEAN  := FALSE;
   L_error BOOLEAN;
   L_error_message VARCHAR2(600);
   L_OGA_temp_rec OGA%ROWTYPE;

   cursor C_CHK_CHILD_EXISTS(I_OGA VARCHAR2) is
   select 'Y'
     from (select oga_code from HTS_OGA
           union
           select oga_code from CE_FORMS) DEP_TBL
     where DEP_TBL.oga_code = I_OGA; 
   
BEGIN
   FOR rec IN c_svc_OGA(I_process_id,
                        I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      if rec.action = action_new
         and rec.PK_OGA_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'OGA_CODE',
                     'OGA_CODE_EXISTS');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_OGA_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'OGA_CODE',
                     'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;

      if rec.action = action_del then
         open C_CHK_CHILD_EXISTS(rec.OGA_CODE);
         fetch C_CHK_CHILD_EXISTS  into L_exists;
         if C_CHK_CHILD_EXISTS%FOUND then
            WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           'CHILD_EXISTS');
             L_error := TRUE;
         end if;
         close C_CHK_CHILD_EXISTS;
      end if;

    if NOT(rec.OGA_CODE  IS NOT NULL) 
       and rec.action in (action_mod, action_del, action_new) then
       WRITE_ERROR(I_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_chunk_id,
                   L_table,
                   rec.row_seq,
                   'OGA_CODE',
                   'ENTER_OGA_CODE');
         L_error :=TRUE;
    end if;

    if NOT(rec.OGA_DESC  IS NOT NULL) 
       and rec.action in (action_mod, action_new)then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'OGA_DESC',
                     'ENTER_OGA_DESC');
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_oga_temp_rec.req_form              := rec.req_form;
         L_oga_temp_rec.oga_desc              := rec.oga_desc;
         L_oga_temp_rec.oga_code              := rec.oga_code;
         if rec.action = action_new then
            if EXEC_OGA_INS(L_error_message,
                            L_oga_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_OGA_UPD(L_error_message,
                            L_oga_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_OGA_DEL(L_error_message,
                            L_oga_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_oga st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_oga st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_oga st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_OGA;
--------------------------------------------------------------------------------------
FUNCTION EXEC_OGA_TL_INS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE, 
                         I_oga_tl_ins_tab    IN       OGA_TL_TAB)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_OGA_TL_INS';
BEGIN
   if I_oga_tl_ins_tab is NOT NULL and I_oga_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_oga_tl_ins_tab.COUNT()
         insert into oga_tl
              values I_oga_tl_ins_tab(i);
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_OGA_TL_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_OGA_TL_UPD(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_oga_tl_upd_tab   IN       OGA_TL_TAB,
                         I_oga_tl_upd_rst   IN       ROW_SEQ_TAB,
                         I_process_id       IN       SVC_OGA_TL.PROCESS_ID%TYPE,
                         I_chunk_id         IN       SVC_OGA_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_OGA_TL_UPD';
   L_table   VARCHAR2(64):= 'OGA_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_OGA_TL_UPD(I_oga_code    OGA_TL.OGA_CODE%TYPE,
                            I_lang        OGA_TL.LANG%TYPE) is
      select 'x'
        from oga_tl
       where oga_code = I_oga_code
         and lang = I_lang
         for update nowait;
BEGIN
   if I_oga_tl_upd_tab is NOT NULL and I_oga_tl_upd_tab.count > 0 then
      for i in I_oga_tl_upd_tab.FIRST..I_oga_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_oga_tl_upd_tab(i).lang);
            L_key_val2 := 'OGA Code: '||to_char(I_oga_tl_upd_tab(i).oga_code);
            open C_LOCK_OGA_TL_UPD(I_oga_tl_upd_tab(i).oga_code,
                                   I_oga_tl_upd_tab(i).lang);
            close C_LOCK_OGA_TL_UPD;
            
            update oga_tl
               set oga_desc = I_oga_tl_upd_tab(i).oga_desc,
                   last_update_id = I_oga_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_oga_tl_upd_tab(i).last_update_datetime
             where lang = I_oga_tl_upd_tab(i).lang
               and oga_code = I_oga_tl_upd_tab(i).oga_code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_OGA_TL',
                           I_oga_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_LOCK_OGA_TL_UPD%ISOPEN then
         close C_LOCK_OGA_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_OGA_TL_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_OGA_TL_DEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,  
                         I_oga_tl_del_tab    IN       OGA_TL_TAB,
                         I_oga_tl_del_rst    IN       ROW_SEQ_TAB,
                         I_process_id        IN       SVC_OGA_TL.PROCESS_ID%TYPE,
                         I_chunk_id          IN       SVC_OGA_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_HTS_SETUP.EXEC_OGA_TL_DEL';
   L_table   VARCHAR2(64):= 'OGA_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_OGA_TL_DEL(I_oga_code    OGA_TL.OGA_CODE%TYPE,
                            I_lang        OGA_TL.LANG%TYPE) is
      select 'x'
        from oga_tl
       where oga_code = I_oga_code
         and lang = I_lang
         for update nowait;
BEGIN
   if I_oga_tl_del_tab is NOT NULL and I_oga_tl_del_tab.count > 0 then
      for i in I_oga_tl_del_tab.FIRST..I_oga_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_oga_tl_del_tab(i).lang);
            L_key_val2 := 'OGA Code: '||to_char(I_oga_tl_del_tab(i).oga_code);
            open C_LOCK_OGA_TL_DEL(I_oga_tl_del_tab(i).oga_code,
                                   I_oga_tl_del_tab(i).lang);
            close C_LOCK_OGA_TL_DEL;
           
            delete oga_tl
             where lang = I_oga_tl_del_tab(i).lang
               and oga_code = I_oga_tl_del_tab(i).oga_code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_OGA_TL',
                           I_oga_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      END LOOP;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_LOCK_OGA_TL_DEL%ISOPEN then
         close C_LOCK_OGA_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_OGA_TL_DEL;
------------------------------------------------------------------
FUNCTION PROCESS_OGA_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_OGA_TL.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_OGA_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64)                     :='CORESVC_HTS_SETUP.PROCESS_OGA_TL';
   L_table              VARCHAR2(64)                     :='SVC_OGA_TL';
   L_base_trans_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='OGA_TL';
   
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   L_error              BOOLEAN;
   L_process_error      BOOLEAN := FALSE;
   L_OGA_TL_temp_rec    OGA_TL%ROWTYPE;
   L_oga_tl_upd_rst     ROW_SEQ_TAB;
   L_oga_tl_del_rst     ROW_SEQ_TAB;
   
   cursor C_SVC_OGA_TL(I_process_id NUMBER,
                       I_chunk_id NUMBER) is
      select pk_oga_tl.rowid  AS pk_oga_tl_rid,
             st.rowid AS st_rid,
             ogat_oga_fk.rowid    AS ogat_oga_fk_rid,
             ogat_lang_fk.rowid    AS ogat_lang_fk_rid,
             st.oga_desc,
             st.oga_code,
             st.lang,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_oga_tl st,
             oga_tl pk_oga_tl,
             oga ogat_oga_fk,
             lang ogat_lang_fk
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.lang       = pk_oga_tl.lang (+)
         and st.oga_code   = pk_oga_tl.oga_code (+)
         and st.oga_code   = ogat_oga_fk.oga_code (+)
         and st.lang       = ogat_lang_fk.lang (+);

   TYPE SVC_OGA_TL is TABLE OF C_SVC_OGA_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_oga_tl_tab   SVC_OGA_TL;

   L_oga_tl_ins_tab         OGA_TL_tab         := NEW OGA_TL_tab();
   L_oga_tl_upd_tab         OGA_TL_tab         := NEW OGA_TL_tab();
   L_oga_tl_del_tab         OGA_TL_tab         := NEW OGA_TL_tab();
   
BEGIN
   if C_SVC_OGA_TL%ISOPEN then
      close C_SVC_OGA_TL;
   end if;   
   open c_svc_oga_tl(I_process_id,
                     I_chunk_id);
   LOOP
      fetch C_SVC_OGA_TL bulk collect into L_svc_oga_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_oga_tl_tab.COUNT > 0 then
         FOR i in L_svc_oga_tl_tab.FIRST..L_svc_oga_tl_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_oga_tl_tab(i).action is NULL
               or L_svc_oga_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_oga_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_oga_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_oga_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_oga_tl_tab(i).action = action_new
               and L_svc_oga_tl_tab(i).pk_oga_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_oga_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_oga_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_oga_tl_tab(i).lang is NOT NULL
               and L_svc_oga_tl_tab(i).oga_code is NOT NULL
               and L_svc_oga_tl_tab(i).pk_oga_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_oga_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_oga_tl_tab(i).action = action_new
               and L_svc_oga_tl_tab(i).oga_code is NOT NULL
               and L_svc_oga_tl_tab(i).ogat_oga_fk_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_oga_tl_tab(i).row_seq,
                            'OGA_CODE',
                            'OGA_CODE_NOTFOUND');
               L_error :=TRUE;
            end if;

            if L_svc_oga_tl_tab(i).action = action_new
               and L_svc_oga_tl_tab(i).lang is NOT NULL
               and L_svc_oga_tl_tab(i).ogat_lang_fk_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_oga_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_oga_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_oga_tl_tab(i).oga_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_oga_tl_tab(i).row_seq,
                              'OGA_DESC',
                              'ENTER_OGA_DESC');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_oga_tl_tab(i).oga_code is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_oga_tl_tab(i).row_seq,
                           'OGA_CODE',
                           'ENTER_OGA_CODE');
               L_error :=TRUE;
            end if;

            if L_svc_oga_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_oga_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_oga_tl_temp_rec.lang := L_svc_oga_tl_tab(i).lang;
               L_oga_tl_temp_rec.oga_code := L_svc_oga_tl_tab(i).oga_code;
               L_oga_tl_temp_rec.oga_desc := L_svc_oga_tl_tab(i).oga_desc;
               L_oga_tl_temp_rec.create_datetime := SYSDATE;
               L_oga_tl_temp_rec.create_id := GET_USER;
               L_oga_tl_temp_rec.last_update_datetime := SYSDATE;
               L_oga_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_oga_tl_tab(i).action = action_new then
                  L_oga_tl_ins_tab.extend;
                  L_oga_tl_ins_tab(L_oga_tl_ins_tab.count()) := L_oga_tl_temp_rec;
               end if;

               if L_svc_oga_tl_tab(i).action = action_mod then
                  L_oga_tl_upd_tab.extend;
                  L_oga_tl_upd_tab(L_oga_tl_upd_tab.count()) := L_oga_tl_temp_rec;
                  L_oga_tl_upd_rst(L_oga_tl_upd_tab.count()) := L_svc_oga_tl_tab(i).row_seq;
               end if;

               if L_svc_oga_tl_tab(i).action = action_del then
                  L_oga_tl_del_tab.extend;
                  L_oga_tl_del_tab(L_oga_tl_del_tab.count()) := L_oga_tl_temp_rec;
                  L_oga_tl_del_rst(L_oga_tl_del_tab.count()) := L_svc_oga_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_OGA_TL%NOTFOUND;
   END LOOP;
   close C_SVC_OGA_TL;

   if EXEC_OGA_TL_INS(O_error_message,
                      L_oga_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_OGA_TL_UPD(O_error_message,
                      L_oga_tl_upd_tab,
                      L_oga_tl_upd_rst,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_OGA_TL_DEL(O_error_message,
                      L_oga_tl_del_tab,
                      L_oga_tl_del_rst,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_OGA_TL;
--------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   SVC_CHANNELS.PROCESS_ID%TYPE) IS

BEGIN

   delete
     from svc_hts_import_country_setup
    where process_id= I_process_id;

   delete
     from svc_tariff_treatment_tl
    where process_id= I_process_id;

   delete
     from svc_tariff_treatment
    where process_id= I_process_id;

   delete
     from svc_hts_chapter_tl
    where process_id = I_process_id;

   delete
     from svc_hts_chapter
    where process_id = I_process_id;

   delete
     from svc_hts_chapter_restraints_tl
    where process_id = i_process_id;

   delete
     from svc_hts_chapter_restraints
    where process_id = I_process_id;

   delete 
     from svc_quota_category_tl
    where process_id = I_process_id;

   delete 
     from svc_quota_category
    where process_id = I_process_id;

   delete 
     from svc_country_tariff_treatment 
    where process_id= I_process_id;

   delete
     from SVC_OGA_TL
    where process_id= I_process_id;

   delete
     from SVC_OGA
    where process_id= I_process_id;

END CLEAR_STAGING_DATA;
--------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     OUT      NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_HTS_SETUP.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_HTS_IMP_CNT_SETUP(O_error_message,
                                I_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_TARIFF_TREATMENT(O_error_message,
                               I_process_id,
                               I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_TARIFF_TREATMENT_TL(O_error_message,
                                  I_process_id,
                                  I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_QUOTA_CAT(O_error_message,
                        I_process_id,
                        I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_QUOTA_CAT_TL(O_error_message,
                           I_process_id,
                           I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_HTS_CHAPTER(O_error_message,
                          I_process_id,
                          I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   if PROCESS_HTS_CHAPTER_TL(O_error_message,
                             I_process_id,
                             I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_HTS_CR(O_error_message,
                     I_process_id,
                     I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_HTS_CR_TL(O_error_message,
                        I_process_id,
                        I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_COUNTRY_TRF_TT(O_error_message,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_OGA(O_error_message,
                  I_process_id,
                  I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   if PROCESS_OGA_TL(O_error_message,
                     I_process_id,
                     I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT; 
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------------
END CORESVC_HTS_SETUP;
/
