



CREATE OR REPLACE PACKAGE HTS_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name: HTS_FEE_EXISTS
-- Purpose      : Checks for the existence of HTS number on HTS_FEE table.
--                Sets O_exists to TRUE if found.
---------------------------------------------------------------------------------------------
FUNCTION HTS_FEE_EXISTS  (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists              IN OUT BOOLEAN,
                          I_hts                 IN     hts.hts%TYPE,
                          I_import_country_id   IN     hts.import_country_id%TYPE,
                          I_effect_from         IN     hts.effect_from%TYPE,
                          I_effect_to           IN     hts.effect_to%TYPE)

   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: HTS_TAX_EXISTS
-- Purpose      : Checks for the existence of HTS number on HTS_TAX table.
--                Sets O_exists to TRUE if found.
---------------------------------------------------------------------------------------------
FUNCTION HTS_TAX_EXISTS  (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists              IN OUT BOOLEAN,
                          I_hts                 IN     hts.hts%TYPE,
                          I_import_country_id   IN     hts.import_country_id%TYPE,
                          I_effect_from         IN     hts.effect_from%TYPE,
                          I_effect_to           IN     hts.effect_to%TYPE)

   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: HTS_OGA_EXISTS
-- Purpose      : Checks for the existence of HTS number on HTS_OGA table.
--                Sets O_exists to TRUE if found.
---------------------------------------------------------------------------------------------
FUNCTION HTS_OGA_EXISTS  (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists              IN OUT BOOLEAN,
                          I_hts                 IN     hts.hts%TYPE,
                          I_import_country_id   IN     hts.import_country_id%TYPE,
                          I_effect_from         IN     hts.effect_from%TYPE,
                          I_effect_to           IN     hts.effect_to%TYPE)

   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: HTS_REFERENCE_EXISTS
-- Purpose      : Checks for the existence of HTS number on HTS_REFERENCE table.
--                Sets O_exists to TRUE if found.
---------------------------------------------------------------------------------------------
FUNCTION HTS_REFERENCE_EXISTS  (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists              IN OUT BOOLEAN,
                                I_hts                 IN     hts.hts%TYPE,
                                I_import_country_id   IN     hts.import_country_id%TYPE,
                                I_effect_from         IN     hts.effect_from%TYPE,
                                I_effect_to           IN     hts.effect_to%TYPE)

   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: HTS_CVD_EXISTS
-- Purpose      : Checks for the existence of HTS number on HTS_CVD table.
--                Sets O_exists to TRUE if found.
---------------------------------------------------------------------------------------------
FUNCTION HTS_CVD_EXISTS  (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists              IN OUT BOOLEAN,
                          I_hts                 IN     hts.hts%TYPE,
                          I_import_country_id   IN     hts.import_country_id%TYPE,
                          I_effect_from         IN     hts.effect_from%TYPE,
                          I_effect_to           IN     hts.effect_to%TYPE)

   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: HTS_AD_EXISTS
-- Purpose      : Checks for the existence of HTS number on HTS_AD table.
--                Sets O_exists to TRUE if found.
---------------------------------------------------------------------------------------------
FUNCTION HTS_AD_EXISTS (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists              IN OUT BOOLEAN,
                        I_hts                 IN     hts.hts%TYPE,
                        I_import_country_id   IN     hts.import_country_id%TYPE,
                        I_effect_from         IN     hts.effect_from%TYPE,
                        I_effect_to           IN     hts.effect_to%TYPE)

   RETURN BOOLEAN;


---------------------------------------------------------------------------------------------
-- Function Name: HTS_TT_EXCLUSIONS_EXISTS
-- Purpose      : Checks for the existence of HTS number on HTS_TT_EXCLUSIONS table.
--                Sets O_exists to TRUE if found.
---------------------------------------------------------------------------------------------
FUNCTION HTS_TT_EXCLUSIONS_EXISTS  (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists             IN OUT   BOOLEAN,
                                    I_hts                IN       hts.hts%TYPE,
                                    I_import_country_id  IN       hts.import_country_id%TYPE,
                                    I_effect_from        IN       hts.effect_from%TYPE,
                                    I_effect_to          IN       hts.effect_to%TYPE,
                                    I_tariff_treatment   IN       hts_tt_exclusions.tariff_treatment%TYPE,
                                    I_origin_country_id  IN       hts_tt_exclusions.origin_country_id%TYPE)

   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: HTS_TARIFF_TREATMENT_EXISTS
-- Purpose      : Checks for the existence of HTS number on HTS_TARIFF_TREATMENT table.
--                Sets O_exists to TRUE if found.
---------------------------------------------------------------------------------------------
FUNCTION HTS_TARIFF_TREATMENT_EXISTS  (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists             IN OUT   BOOLEAN,
                                       I_hts                IN       hts.hts%TYPE,
                                       I_import_country_id  IN       hts.import_country_id%TYPE,
                                       I_effect_from        IN       hts.effect_from%TYPE,
                                       I_effect_to          IN       hts.effect_to%TYPE,
                                       I_tariff_treatment   IN       hts_tariff_treatment.tariff_treatment%TYPE)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_HTS_DESC
-- Purpose      : Retrieves HTS description.
---------------------------------------------------------------------------------------------
FUNCTION GET_HTS_DESC(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_hts_desc           IN OUT  hts.hts_desc%TYPE,
                      I_hts                IN      hts.hts%TYPE,
                      I_import_country_id  IN      hts.import_country_id%TYPE,
                      I_effect_from        IN      hts.effect_from%TYPE,
                      I_effect_to          IN      hts.effect_to%TYPE)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_HTS_CHAPTER_DESC
-- Purpose      : Retrieves HTS Chapter description.
---------------------------------------------------------------------------------------------
FUNCTION GET_HTS_CHAPTER_DESC (O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_chapter_desc       IN OUT  HTS_CHAPTER.CHAPTER_DESC%TYPE,
                               I_import_country_id  IN      HTS_CHAPTER.IMPORT_COUNTRY_ID%TYPE,
                               I_chapter            IN      HTS_CHAPTER.CHAPTER%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_LOCK_HTS
-- Purpose      : This function should only be passed 'NEW' or 'EDIT' mode. When passing in 'EDIT' mode,
--                checks if HTS exists on ITEM_HTS and ORDSKU_HTS tables. If no records found,
--                sets O_exists to FALSE and locks child table records for deletion. If records exist,
--                sets O_exists to TRUE and exits the function. If mode is 'NEW', locks child table
--                records for deletion.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_LOCK_HTS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists               IN OUT BOOLEAN,
                        I_mode                 IN     VARCHAR2,
                        I_hts                  IN     HTS.HTS%TYPE,
                        I_import_country_id    IN     HTS.IMPORT_COUNTRY_ID%TYPE,
                        I_effect_from          IN     HTS.EFFECT_FROM%TYPE,
                        I_effect_to            IN     HTS.EFFECT_TO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_HTS
-- Purpose      : This function deletes child records of specified hts and should
--                be called after function HTS_SQL.CHECK_LOCK_HTS.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_HTS      (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts                 IN     hts.hts%TYPE,
                          I_import_country_id   IN     hts.import_country_id%TYPE,
                          I_effect_from         IN     hts.effect_from%TYPE,
                          I_effect_to           IN     hts.effect_to%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_LOCK_HTS_CHAPTER
-- Purpose      : Checks if HTS Chapter exists on HTS table. If no record exists, sets BOOLEAN
--                FALSE for O_exists then locks child records. If HTS record does exist,
--                returns TRUE for O_exists and exits the function.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_LOCK_HTS_CHAPTER(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists             IN OUT BOOLEAN,
                                I_import_country_id  IN     hts.import_country_id%TYPE,
                                I_hts_chapter        IN     hts_chapter.chapter%TYPE)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_HTS_CHAPTER
-- Purpose      : Deletes child records of passed_in hts_chapter. Should be called after
--                function HTS_SQL.CHECK_LOCK_HTS_CHAPTER.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_HTS_CHAPTER (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_import_country_id   IN     hts.import_country_id%TYPE,
                             I_hts_chapter         IN     hts_chapter.chapter%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_HTS_INFO
-- Purpose      : Obtains HTS information for indicated import country and effect dates.
---------------------------------------------------------------------------------------------
FUNCTION GET_HTS_INFO  (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_hts_desc           IN OUT hts.hts_desc%TYPE,
                        O_chapter            IN OUT hts.chapter%TYPE,
                        O_quota_cat          IN OUT hts.quota_cat%TYPE,
                        O_more_hts_ind       IN OUT hts.more_hts_ind%TYPE,
                        O_duty_comp_code     IN OUT hts.duty_comp_code%TYPE,
                        O_units              IN OUT hts.units%TYPE,
                        O_units_1            IN OUT hts.units_1%TYPE,
                        O_units_2            IN OUT hts.units_2%TYPE,
                        O_units_3            IN OUT hts.units_3%TYPE,
                        O_cvd_ind            IN OUT hts.cvd_ind%TYPE,
                        O_ad_ind             IN OUT hts.ad_ind%TYPE,
                        O_quota_ind          IN OUT hts.quota_ind%TYPE,
                        I_hts                IN     hts.hts%TYPE,
                        I_import_country_id  IN     hts.import_country_id%TYPE,
                        I_effect_from        IN     hts.effect_from%TYPE,
                        I_effect_to          IN     hts.effect_to%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: HTS_COUNT
-- Purpose      : Obtains HTS count for indicated import country.
---------------------------------------------------------------------------------------------
FUNCTION HTS_COUNT (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_count               IN OUT NUMBER,
                    I_hts                 IN     HTS.HTS%TYPE,
                    I_import_country_id   IN     HTS.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_EFFECTIVE_DATES
-- Purpose      : Obtains HTS effect_from and effect_to dates for an HTS and importing country.
---------------------------------------------------------------------------------------------
FUNCTION GET_EFFECTIVE_DATES   (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_effect_from        IN OUT HTS.EFFECT_FROM%TYPE,
                                O_effect_to          IN OUT HTS.EFFECT_TO%TYPE,
                                I_hts                IN     HTS.HTS%TYPE,
                                I_import_country_id  IN     HTS.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_VALID_EFFECTIVE_DATES
-- Purpose      : Obtains HTS effect_from and effect_to dates for an HTS and importing country.
--                limits by vdate
---------------------------------------------------------------------------------------------
FUNCTION GET_VALID_EFFECTIVE_DATES   (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_effect_from        IN OUT HTS.EFFECT_FROM%TYPE,
                                      O_effect_to          IN OUT HTS.EFFECT_TO%TYPE,
                                      I_hts                IN     HTS.HTS%TYPE,
                                      I_import_country_id  IN     HTS.IMPORT_COUNTRY_ID%TYPE,
                                      I_date               IN     HTS.EFFECT_FROM%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_QUOTA_CATEGORY_DESC
-- Purpose      : Obtains translated quota category description for given quota category number
--                and importing country id.
---------------------------------------------------------------------------------------------
FUNCTION GET_QUOTA_CATEGORY_DESC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_quota_cat_desc     IN OUT QUOTA_CATEGORY.CATEGORY_DESC%TYPE,
                                 I_quota_cat          IN     QUOTA_CATEGORY.QUOTA_CAT%TYPE,
                                 I_import_country_id  IN     QUOTA_CATEGORY.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: HTS_VDATE
-- Purpose      : Returns BOOLEAN TRUE for O_return_value if an active HTS exists by verifying
--                if VDATE falls between effect_from and effect_to dates.
---------------------------------------------------------------------------------------------
FUNCTION HTS_VDATE (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_return_value        IN OUT BOOLEAN,
                    I_hts                 IN     HTS.HTS%TYPE,
                    I_import_country_id   IN     HTS.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: IMPORT_COUNTRY_EXISTS
-- Purpose      : This function will check for the existence of a given Import Country on the HTS table.
--                If no records are found return 'N' in the output parameter O_exists.
--                Otherwise pass out 'Y' in O_exists.
---------------------------------------------------------------------------------------------
FUNCTION IMPORT_COUNTRY_EXISTS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists            IN OUT   BOOLEAN,
                               I_import_country_id IN       COUNTRY.COUNTRY_ID%TYPE)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_HTS_DATE_RANGE
-- Purpose      : Checks for the existence of an HTS number on HTS table with overlapping effective dates.
--                Sets O_exists to TRUE if found.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_HTS_DATE_RANGE  (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists              IN OUT BOOLEAN,
                                   I_hts                 IN     hts.hts%TYPE,
                                   I_import_country_id   IN     hts.import_country_id%TYPE,
                                   I_effect_from         IN     hts.effect_from%TYPE,
                                   I_effect_to           IN     hts.effect_to%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_HTS_CVD
-- Purpose      : Checks for the existence of a duplicate HTS_CVD record.
--                Sets O_exists to TRUE if found.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_HTS_CVD  (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists            IN OUT BOOLEAN,
                            I_hts               IN     hts.hts%TYPE,
                            I_import_country_id IN     hts.import_country_id%TYPE,
                            I_effect_from       IN     hts.effect_from%TYPE,
                            I_effect_to         IN     hts.effect_to%TYPE,
                            I_origin_country_id IN     hts_cvd.origin_country_id%TYPE)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_HTS_AD
-- Purpose      : Checks for the existence of a duplicate HTS_AD record.
--                Sets O_exists to TRUE if found.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_HTS_AD  (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists            IN OUT BOOLEAN,
                           I_hts               IN     hts.hts%TYPE,
                           I_import_country_id IN     hts.import_country_id%TYPE,
                           I_effect_from       IN     hts.effect_from%TYPE,
                           I_effect_to         IN     hts.effect_to%TYPE,
                           I_origin_country_id IN     hts_ad.origin_country_id%TYPE,
                           I_mfg_id            IN     hts_ad.mfg_id%TYPE)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: QUOTA_CAT_EXISTS
-- Purpose      : Checks for the existence of a quota category on the
--                HTS_CHAPTER_RESTRAINTS table.  Sets O_exists to TRUE if found.
---------------------------------------------------------------------------------------------
FUNCTION QUOTA_CAT_EXISTS (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists             IN OUT BOOLEAN,
                           I_quota_cat          IN     QUOTA_CATEGORY.QUOTA_CAT%TYPE,
                           I_import_country_id  IN     QUOTA_CATEGORY.IMPORT_COUNTRY_ID%TYPE)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_QUOTA_CAT
-- Purpose      : Retrieves the quota category from the item_hts table for I_hts,
--                I_effect_from, I_effect_to, and I_import_country.
---------------------------------------------------------------------------------------------
FUNCTION GET_QUOTA_CAT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_quota_cat       IN OUT  HTS.QUOTA_CAT%TYPE,
                       I_hts             IN      HTS.HTS%TYPE,
                       I_effect_from     IN      HTS.EFFECT_FROM%TYPE,
                       I_effect_to       IN      HTS.EFFECT_TO%TYPE,
                       I_import_country  IN      HTS.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_FORMAT_MASK_HEADING_LENGTH
-- Purpose      : Retrieves the format mask and length for the input import country
--                from HTS_IMPORT_COUNTRY_SETUP.
---------------------------------------------------------------------------------------------
FUNCTION GET_FORMAT_MASK_HEADING_LENGTH (O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_hts_format_mask     IN OUT  HTS_IMPORT_COUNTRY_SETUP.HTS_FORMAT_MASK%TYPE,
                                         O_hts_heading_length  IN OUT  HTS_IMPORT_COUNTRY_SETUP.HTS_HEADING_LENGTH%TYPE,
                                         I_import_country      IN      HTS.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: HTS_CHAPTER_EXISTS
-- Purpose      : Checks if a HTS Chapter exists for the input country.
---------------------------------------------------------------------------------------------
FUNCTION HTS_CHAPTER_EXISTS(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists               IN OUT  BOOLEAN,
                            I_import_country_id    IN      HTS_CHAPTER.IMPORT_COUNTRY_ID%TYPE,
                            I_hts_chapter          IN      HTS_CHAPTER.CHAPTER%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: HTS_CHAPTER_RESTRAINTS_EXISTS
-- Purpose      : Checks if a Restraint exist for the input HTS Chapter and the input country.
---------------------------------------------------------------------------------------------
FUNCTION HTS_CHAPTER_RESTRAINTS_EXISTS(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists               IN OUT  BOOLEAN,
                                       I_import_country_id    IN      HTS_CHAPTER.IMPORT_COUNTRY_ID%TYPE,
                                       I_hts_chapter          IN      HTS_CHAPTER.CHAPTER%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_QUOT_CTRY_EXISTS
-- Purpose      : Checks if a QUOTA_CATEGORY exist for the input Quota Category and the input country.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_QUOT_CTRY_EXISTS(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists               IN OUT  BOOLEAN,
                                I_import_country_id    IN      QUOTA_CATEGORY.IMPORT_COUNTRY_ID%TYPE,
                                I_quota_cat            IN      QUOTA_CATEGORY.QUOTA_CAT%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END;
/


