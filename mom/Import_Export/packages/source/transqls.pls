CREATE OR REPLACE PACKAGE TRANSPORTATION_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------------------
--Function Name:   GET_NEXT_ID
--Purpose      :   Retrieves the next value for the Transportation ID for the
--                 TRANSPORTATION table.
-----------------------------------------------------------------------------------------------------

FUNCTION GET_NEXT_ID(O_error_message      IN OUT VARCHAR2,
                     O_transportation_id  IN OUT TRANSPORTATION.TRANSPORTATION_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   TRAN_EXISTS
--Purpose      :   Checks if passed-in Transportation ID exists on the TRANSPORTATION table.
--                 O_exists will return FALSE if no records are retrieved, otherwise will return TRUE.
-----------------------------------------------------------------------------------------------------
FUNCTION TRAN_EXISTS(O_error_message           IN OUT VARCHAR2,
                     O_exists                  IN OUT BOOLEAN,
                     I_transportation_id       IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   GET_TOTALS
--Purpose      :   Sums weights, volumes, unit and value totals based on the level
--                 passed into the function.  Any UOMs not passed into the function
--                 will be defaulted.
-----------------------------------------------------------------------------------------------------
FUNCTION GET_TOTALS(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_carton_qty              IN OUT   TRANSPORTATION.CARTON_QTY%TYPE,
                    IO_carton_qty_uom         IN OUT   TRANSPORTATION.CARTON_UOM%TYPE,
                    O_item_qty                IN OUT   TRANSPORTATION.ITEM_QTY%TYPE,
                    IO_item_qty_uom           IN OUT   TRANSPORTATION.ITEM_QTY_UOM%TYPE,
                    O_gross_wt                IN OUT   TRANSPORTATION.GROSS_WT%TYPE,
                    IO_gross_wt_uom           IN OUT   TRANSPORTATION.GROSS_WT_UOM%TYPE,
                    O_net_wt                  IN OUT   TRANSPORTATION.NET_WT%TYPE,
                    IO_net_wt_uom             IN OUT   TRANSPORTATION.NET_WT_UOM%TYPE,
                    O_cubic                   IN OUT   TRANSPORTATION.CUBIC%TYPE,
                    IO_cubic_uom              IN OUT   TRANSPORTATION.CUBIC_UOM%TYPE,
                    O_invoice_amt             IN OUT   TRANSPORTATION.INVOICE_AMT%TYPE,
                    I_total_level             IN       CODE_DETAIL.CODE%TYPE,
                    I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                    I_voyage_id               IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                    I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                    I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE,
                    I_item                    IN       TRANSPORTATION.ITEM%TYPE,
                    I_container_id            IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                    I_bl_awb_id               IN       TRANSPORTATION.BL_AWB_ID%TYPE,
                    I_invoice_id              IN       TRANSPORTATION.INVOICE_ID%TYPE,
                    I_currency_code           IN       TRANSPORTATION.CURRENCY_CODE%TYPE,
                    I_exchange_rate           IN       TRANSPORTATION.EXCHANGE_RATE%TYPE,
                    I_total_field             IN       VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- Function Name: GET_MAIN_INFO
-- Purpose:       This function retrieves main information from the
--                transportation table, given a valid transportation_id
--                and returns it to the calling form.
-----------------------------------------------------------------------------------------------------
FUNCTION GET_MAIN_INFO(O_error_message          IN OUT VARCHAR2,
                       O_return_code            IN OUT BOOLEAN,
                       O_vessel_id              IN OUT TRANSPORTATION.VESSEL_ID%TYPE,
                       O_voyage_flt_id          IN OUT TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                       O_estimated_depart_date  IN OUT TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                       O_order_no               IN OUT TRANSPORTATION.ORDER_NO%TYPE,
                       O_item                   IN OUT TRANSPORTATION.ITEM%TYPE,
                       O_container_id           IN OUT TRANSPORTATION.CONTAINER_ID%TYPE,
                       O_bl_awb_id              IN OUT TRANSPORTATION.BL_AWB_ID%TYPE,
                       O_invoice_id             IN OUT TRANSPORTATION.INVOICE_ID%TYPE,
                       O_status                 IN OUT TRANSPORTATION.STATUS%TYPE,
                       I_transportation_id      IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- Function Name: GET_ESTIMATED_DEPART_DATE
-- Purpose:       This function retrieves the estimated depart date of a transportation record using
--                the order_no, vessel_id, and voyage_flt_id as input parameters.
-----------------------------------------------------------------------------------------------------
FUNCTION GET_ESTIMATED_DEPART_DATE(O_error_message          IN OUT VARCHAR2,
                                   O_estimated_depart_date  IN OUT TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                   I_order_no               IN     TRANSPORTATION.ORDER_NO%TYPE,
                                   I_vessel_id              IN     TRANSPORTATION.VESSEL_ID%TYPE,
                                   I_voyage_flt_id          IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- Function Name: GET_SCAC_CODE_DESC
-- Purpose:       This function retrieves the translated description of the
--                Standard Carrier Alpha Code description from the SCAC table.
--                This function may also be used to validate a SCAC code.
-----------------------------------------------------------------------------------------------------
FUNCTION GET_SCAC_CODE_DESC(O_error_message          IN OUT VARCHAR2,
                            O_exists                 IN OUT BOOLEAN,
                            O_scac_code_desc         IN OUT SCAC.SCAC_CODE_DESC%TYPE,
                            I_scac_code              IN     SCAC.SCAC_CODE%TYPE)
   RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------------
-- Function Name: GET_FREIGHT_SIZE_DESC
-- Purpose:       This function retrieves the translated description of the
--                Freight Size from the FREIGHT_SIZE table.  This function
--                may also be used to validate a Freight Size.
-----------------------------------------------------------------------------------------------------
FUNCTION GET_FREIGHT_SIZE_DESC(O_error_message          IN OUT VARCHAR2,
                               O_exists                 IN OUT BOOLEAN,
                               O_freight_size_desc      IN OUT FREIGHT_SIZE_TL.FREIGHT_SIZE_DESC%TYPE,
                               I_freight_size           IN     FREIGHT_SIZE.FREIGHT_SIZE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- FUNCTION NAME: MERGE_FREIGHT_SIZE_TL
-- PURPOSE:   This function will modify or insert a record in the FREIGHT_SIZE_TL table
--            given the I_freight_size, I_freight_size_desc and the input Language
-----------------------------------------------------------------------------------------------------
FUNCTION MERGE_FREIGHT_SIZE_TL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_freight_size        IN       FREIGHT_SIZE_TL.FREIGHT_SIZE%TYPE,
                               I_freight_size_desc   IN       FREIGHT_SIZE_TL.FREIGHT_SIZE_DESC%TYPE,
                               I_lang                IN       FREIGHT_SIZE_TL.LANG%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- FUNCTION NAME: DEL_FREIGHT_SIZE_TL
-- PURPOSE:   This function will delete a record in the FREIGHT_SIZE_TL table
-------------------------------------------------------------------------------------------------
FUNCTION DEL_FREIGHT_SIZE_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_freight_size    IN       FREIGHT_SIZE_TL.FREIGHT_SIZE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:  GET_FREIGHT_TYPE_DESC
--Purpose      :  This function will return the translated description of the Freight Type from the
--                FREIGHT_TYPE table. This function will also be used to validate a Freight Type.
-----------------------------------------------------------------------------------------------------
FUNCTION GET_FREIGHT_TYPE_DESC(O_error_message       IN OUT VARCHAR2,
                               O_exists              IN OUT BOOLEAN,
                               O_freight_type_desc   IN OUT FREIGHT_TYPE_TL.FREIGHT_TYPE_DESC%TYPE,
                               I_freight_type        IN     FREIGHT_TYPE.FREIGHT_TYPE%TYPE)

   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- FUNCTION NAME: MERGE_FREIGHT_TYPE_TL
-- PURPOSE:   This function will modify or insert a record in the FREIGHT_TYPE_TL table
--            given the I_freight_type, I_freight_type_desc and the input Language
-----------------------------------------------------------------------------------------------------
FUNCTION MERGE_FREIGHT_TYPE_TL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_freight_type        IN       FREIGHT_TYPE_TL.FREIGHT_TYPE%TYPE,
                               I_freight_type_desc   IN       FREIGHT_TYPE_TL.FREIGHT_TYPE_DESC%TYPE,
                               I_lang                IN       FREIGHT_TYPE_TL.LANG%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- FUNCTION NAME: DEL_FREIGHT_TYPE_TL
-- PURPOSE:   This function will delete a record in the FREIGHT_TYPE_TL table
-------------------------------------------------------------------------------------------------
FUNCTION DEL_FREIGHT_TYPE_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_freight_type    IN       FREIGHT_TYPE_TL.FREIGHT_TYPE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:  CHECK_DELETE_FREIGHT_TYPE
--Purpose      :  This function will verify that the Freight Type being deleted is not referenced on
--                the TRANSPORTATION table. Sets O_exists to TRUE if found.
-----------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_FREIGHT_TYPE (O_error_message     IN OUT VARCHAR2,
                                    O_exists            IN OUT BOOLEAN,
                                    I_freight_type      IN     FREIGHT_TYPE.FREIGHT_TYPE%TYPE)

   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:  CHECK_DELETE_FREIGHT_SIZE
--Purpose      :  This function will verify that the Freight size being deleted is not referenced on
--                the TRANSPORTATION table. Sets O_exists to TRUE if found.
-----------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_FREIGHT_SIZE(O_error_message   IN OUT VARCHAR2,
                                   O_exists          IN OUT BOOLEAN,
                                   I_freight_size    IN     FREIGHT_SIZE.FREIGHT_SIZE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--FUNCTION NAME:  CHECK_DELETE_SCAC_CODE
--PURPOSE      :  This function will verify that the SCAC Code being deleted is not referenced on the
--                TRANSPORTATION table. Sets O_exists to TRUE if found.
-----------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_SCAC_CODE(O_error_message     IN OUT VARCHAR2,
                                O_exists            IN OUT BOOLEAN,
                                I_scac_code         IN     SCAC.SCAC_CODE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- Function Name:  LIC_VISA_EXISTS
-- Purpose      :  This function will verify whether or not a given license/visa already exists.
--                 Sets O_exists to TRUE if found.
-----------------------------------------------------------------------------------------------------
FUNCTION LIC_VISA_EXISTS(O_error_message             IN OUT   VARCHAR2,
                         O_exists              IN OUT   BOOLEAN,
                         I_transportation_id   IN       TRANSPORTATION.TRANSPORTATION_ID%TYPE,
                         I_import_country_id   IN       TRANS_LIC_VISA.IMPORT_COUNTRY_ID%TYPE,
                         I_license_visa_id     IN       TRANS_LIC_VISA.LICENSE_VISA_ID%TYPE,
                         I_license_visa_type   IN       TRANS_LIC_VISA.LICENSE_VISA_TYPE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name:  CONTAINER_EXISTS
-- Purpose      :  This function will determine whether or not the passed in Container ID exists on
--                 the TRANSPORTATION table. Sets O_exists to TRUE if cursor returns a record.
-----------------------------------------------------------------------------------------------------
FUNCTION CONTAINER_EXISTS (O_error_message   IN OUT VARCHAR2,
                           O_exists          IN OUT BOOLEAN,
                           I_container_id    IN     TRANSPORTATION.CONTAINER_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- Function Name:  BL_AWB_EXISTS
-- Purpose      :  This function will determine whether or not the passed in Bill of Lading/Airway Bill
--                 exists on the TRANSPORTATION table. Sets O_exists to TRUE if cursor returns a record.
-----------------------------------------------------------------------------------------------------
FUNCTION BL_AWB_EXISTS(O_error_message   IN OUT VARCHAR2,
                       O_exists          IN OUT BOOLEAN,
                       I_bl_awb_id       IN     TRANSPORTATION.BL_AWB_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- Function Name:  INV_EXISTS
-- Purpose      :  This function will determine whether or not the passed in Commercial Invoice exists
--                 on the TRANSPORTATION table. Sets O_exists to TRUE if cursor returns a record.
-----------------------------------------------------------------------------------------------------
FUNCTION INV_EXISTS(O_error_message   IN OUT VARCHAR2,
                    O_exists          IN OUT BOOLEAN,
                    I_invoice_id      IN     TRANSPORTATION.INVOICE_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- Function Name:  VVE_EXISTS
-- Purpose      :  This function will determine whether or not the passed in Vessel, Voyage or Estimated
--                 Departure Date exists on the TRANSPORTATION table. Sets O_exists to TRUE if returns a record.
-----------------------------------------------------------------------------------------------------
FUNCTION VVE_EXISTS(O_error_message          IN OUT VARCHAR2,
                    O_exists                 IN OUT BOOLEAN,
                    I_vessel_id              IN     TRANSPORTATION.VESSEL_ID%TYPE,
                    I_voyage_flt_id          IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                    I_estimated_depart_date  IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: PACKING_EXISTS
-- Purpose      : This function will determine whether or not the passed in delivery information already
--                exists on the TRANS_PACKING table. This function will also be used to validate whether
--                records exist on the TRANS_PACKING table for the given transportation_id.
-----------------------------------------------------------------------------------------------------
FUNCTION PACKING_EXISTS (O_error_message       IN OUT VARCHAR2,
                         O_exists              IN OUT BOOLEAN,
                         I_transportation_id   IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE,
                         I_from_carton         IN     TRANS_PACKING.FROM_CARTON%TYPE,
                         I_to_carton           IN     TRANS_PACKING.TO_CARTON%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
-- Function Name: DEFAULT_PACKING_REC
-- Purpose      : This function will default the carton_qty, item_qty and carton_pack_qty fields into the
--                carton_rec_qty, item_rec_qty and carton_pack_rec_qty fields on the TRANS_PACKING table.
----------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_PACKING_REC(O_error_message       IN OUT VARCHAR2,
                             O_return_code         IN OUT BOOLEAN,
                             I_transportation_id   IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- Function Name: DELIVERY_EXISTS
-- Purpose      : This function will determine whether or not the passed in delivery information already
--                exists on the TRANS_DELIVERY table. This function will also be used to validate whether
--                      records exist on the TRANS_DELIVERY table for the given transportation_id.
---------------------------------------------------------------------------------------------------------
FUNCTION DELIVERY_EXISTS(O_error_message             IN OUT   VARCHAR2,
                         O_exists              IN OUT   BOOLEAN,
                         I_transportation_id   IN       TRANSPORTATION.TRANSPORTATION_ID%TYPE,
                         I_delivery_location   IN       TRANS_DELIVERY.DELIVERY_LOCATION%TYPE,
                         I_delivery_loc_type   IN       TRANS_DELIVERY.DELIVERY_LOC_TYPE%TYPE)

   return BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: CLAIM_EXISTS
-- Purpose      : This function will determine whether or not the passed in delivery information
--                already exists on the TRANS_CLAIMS table. This function will also be used to
--                validate whether records exist on the TRANS_CLAIMS table for the given transportation_id.
--------------------------------------------------------------------------------------------
FUNCTION CLAIM_EXISTS(O_error_message        IN OUT   VARCHAR2,
                      O_exists                 IN OUT   BOOLEAN,
                      I_transportation_id      IN       TRANSPORTATION.TRANSPORTATION_ID%TYPE,
                      I_claim_id               IN       TRANS_CLAIMS.CLAIM_ID%TYPE)

   return BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: DUP_TRANS_ITEM
-- Purpose      : This function will determine whether or not the TRANS_SKU record has duplicate
--                ITEM's.
--------------------------------------------------------------------------------------------
FUNCTION DUP_TRANS_ITEM(O_error_message         IN OUT VARCHAR2,
                       O_exists                 IN OUT BOOLEAN,
                       I_transportation_id      IN     TRANS_SKU.TRANSPORTATION_ID%TYPE,
                       I_item                   IN     TRANS_SKU.ITEM%TYPE)
    return BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: TRANS_SKU_SEQ
-- Purpose      : This function will retrieve the next value for the Sequence Number for the
--                Transportation ID.
--------------------------------------------------------------------------------------------
FUNCTION TRANS_SKU_SEQ(O_error_message          IN OUT VARCHAR2,
                       O_seq_no                 IN OUT TRANS_SKU.SEQ_NO%TYPE,
                       I_transportation_id      IN     TRANS_SKU.TRANSPORTATION_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------------------
--Function Name:   CONT_VVE_PO_ITEM_EXISTS
--Purpose      :   This function determines if any combination of the
--                 passed-in Container/Vessel/Voyage/ETD/Order/Item exists on the
--                 Transportation table.
-----------------------------------------------------------------------------------------------------
FUNCTION CONT_VVE_PO_ITEM_EXISTS(O_error_message         IN OUT VARCHAR2,
                                 O_exists                IN OUT BOOLEAN,
                                 I_container_id          IN     TRANSPORTATION.CONTAINER_ID%TYPE,
                                 I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                                 I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                 I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                 I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                                 I_item                  IN     TRANSPORTATION.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   BL_AWB_VVE_PO_ITEM_EXISTS
--Purpose      :   This function determines if any combination of the
--                 passed-in Bill of Lading/Vessel/Voyage/ETD/Order/Item exists on the
--                 Transportation table.
-----------------------------------------------------------------------------------------------------
FUNCTION BL_AWB_VVE_PO_ITEM_EXISTS(O_error_message         IN OUT VARCHAR2,
                                   O_exists                IN OUT BOOLEAN,
                                   I_bl_awb_id             IN     TRANSPORTATION.BL_AWB_ID%TYPE,
                                   I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                                   I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                                   I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                                   I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                                   I_item                  IN     TRANSPORTATION.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   VVE_PO_ITEM_EXISTS
--Purpose      :   This function determines if any combination of the
--                 passed-in Vessel/Voyage/ETD/Order/Item exists on the
--                 Transportation table.
-----------------------------------------------------------------------------------------------------
FUNCTION VVE_PO_ITEM_EXISTS(O_error_message         IN OUT VARCHAR2,
                            O_exists                IN OUT BOOLEAN,
                            I_vessel_id             IN     TRANSPORTATION.VESSEL_ID%TYPE,
                            I_voyage_flt_id         IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                            I_estimated_depart_date IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                            I_order_no              IN     TRANSPORTATION.ORDER_NO%TYPE,
                            I_item                  IN     TRANSPORTATION.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--Function Name:   RECORD_EXISTS
--Purpose      :   This function determines if the passed in values
--                 constitute a unique record on the TRANSPORTATION table.
--------------------------------------------------------------------------------
FUNCTION RECORD_EXISTS(O_error_message          IN OUT VARCHAR2,
                       O_exists                 IN OUT BOOLEAN,
                       I_vessel_id              IN     TRANSPORTATION.VESSEL_ID%TYPE,
                       I_voyage_flt_id          IN     TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                       I_estimated_depart_date  IN     TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                       I_order_no               IN     TRANSPORTATION.ORDER_NO%TYPE,
                       I_item                   IN     TRANSPORTATION.ITEM%TYPE,
                       I_container_id           IN     TRANSPORTATION.CONTAINER_ID%TYPE,
                       I_bl_awb_id              IN     TRANSPORTATION.BL_AWB_ID%TYPE,
                       I_invoice_id             IN     TRANSPORTATION.INVOICE_ID%TYPE,
                       I_row_id                 IN     ROWID)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: TRANSKU_EXISTS
-- Purpose      : This function will determine whether or not any records
--                exist on the trans_sku table for the given transportation_id.
-----------------------------------------------------------------------------------------------------
FUNCTION TRANSKU_EXISTS(O_error_message     IN OUT VARCHAR2,
                        O_exists            IN OUT BOOLEAN,
                        I_transportation_id IN TRANSPORTATION.TRANSPORTATION_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   GET_QTYS
--Purpose      :   Sums weights, volumes, unit and value totals for a given unique
--                 Transportation record (i.e. Vessel/Voyage/ETD/PO/Item/Container/
--                 BL/AWB/Invoice combination).  Any UOMs not passed into the function
--                 will be defaulted.
-----------------------------------------------------------------------------------------------------
FUNCTION GET_QTYS(O_error_message         IN OUT  VARCHAR2,
                  O_carton_qty            IN OUT  TRANSPORTATION.CARTON_QTY%TYPE,
                  IO_carton_qty_uom       IN OUT  TRANSPORTATION.CARTON_UOM%TYPE,
                  O_item_qty              IN OUT  TRANSPORTATION.ITEM_QTY%TYPE,
                  IO_item_qty_uom         IN OUT  TRANSPORTATION.ITEM_QTY_UOM%TYPE,
                  O_gross_wt              IN OUT  TRANSPORTATION.GROSS_WT%TYPE,
                  IO_gross_wt_uom         IN OUT  TRANSPORTATION.GROSS_WT_UOM%TYPE,
                  O_net_wt                IN OUT  TRANSPORTATION.NET_WT%TYPE,
                  IO_net_wt_uom           IN OUT  TRANSPORTATION.NET_WT_UOM%TYPE,
                  O_cubic                 IN OUT  TRANSPORTATION.CUBIC%TYPE,
                  IO_cubic_uom            IN OUT  TRANSPORTATION.CUBIC_UOM%TYPE,
                  O_invoice_amt           IN OUT  TRANSPORTATION.INVOICE_AMT%TYPE,
                  I_supplier              IN      SUPS.SUPPLIER%TYPE,
                  I_origin_country_id     IN      COUNTRY.COUNTRY_ID%TYPE,
                  I_vessel_id             IN      TRANSPORTATION.VESSEL_ID%TYPE,
                  I_voyage_id             IN      TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                  I_estimated_depart_date IN      TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                  I_order_no              IN      TRANSPORTATION.ORDER_NO%TYPE,
                  I_item                  IN      TRANSPORTATION.ITEM%TYPE,
                  I_container_id          IN      TRANSPORTATION.CONTAINER_ID%TYPE,
                  I_bl_awb_id             IN      TRANSPORTATION.BL_AWB_ID%TYPE,
                  I_invoice_id            IN      TRANSPORTATION.INVOICE_ID%TYPE,
                  I_currency_code         IN      TRANSPORTATION.CURRENCY_CODE%TYPE,
                  I_exchange_rate         IN      TRANSPORTATION.EXCHANGE_RATE%TYPE,
                  I_calling_function      IN      VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   GET_SHIP_QTYS
--Purpose      :   Sums quantity totals by calling TRANSPORTATION_SQL.GET_QTYS for all
--                 distinct Container/BL/AWB/Invoice records on the transportation table for a
--                 given Vesesl/Voyage/ETD/Order/Item combination. The O_ship_qty will be based
--                 on the first unit that is greater than zero selected from the
--                 following order: item_qty, carton_qty, gross_wt, cubic, net_wt.
--
--                 If you want the ship qty for a particular component sku of a buyer pack, pass the
--                 pack number in the parameter I_pack_item, and the component sku in the parameter
--                 I_item.  If you want the ship qty for the entire pack, pass the pack number in
--                 the parameter I_item and pass NULL in I_pack_item.  For any other items pass the
--                 item number in I_item and pass NULL in I_pack_item.
--------------------------------------------------------------------------------------------------
FUNCTION GET_SHIP_QTY(O_error_message         IN OUT  VARCHAR2,
                      O_ship_qty              IN OUT  TRANSPORTATION.ITEM_QTY%TYPE,
                      I_supplier              IN      SUPS.SUPPLIER%TYPE,
                      I_origin_country_id     IN      COUNTRY.COUNTRY_ID%TYPE,
                      I_uom                   IN      UOM_CLASS.UOM%TYPE,
                      I_vessel_id             IN      TRANSPORTATION.VESSEL_ID%TYPE,
                      I_voyage_flt_id         IN      TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                      I_estimated_depart_date IN      TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                      I_order_no              IN      TRANSPORTATION.ORDER_NO%TYPE,
                      I_item                  IN      TRANSPORTATION.ITEM%TYPE,
                      I_pack_item             IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
/*START_RMS11_INS_TCD17*/
--Function Name:   TRAN_FILTER_LIST
--Purpose      :   Accepts an item as input and returns an indicator as to whether or not the user
--                 has a partial view of the TRANSPORTATION table.
-----------------------------------------------------------------------------------------------------
FUNCTION TRAN_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_partial_view  IN OUT VARCHAR2,
                          I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   APPROVED_HTS_EXISTS
--Purpose      :   Checks for the existance of an approved HTS code for the PO-item combination
--             :   passed in. Sets O_hts_details_missing to TRUE if details are missing.
-----------------------------------------------------------------------------------------------------
FUNCTION APPROVED_HTS_EXISTS(O_error_message       IN OUT RTK_ERRORS.RTK_KEY%TYPE,
                             O_hts_details_missing IN OUT BOOLEAN,
                             I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                             I_item                IN     ITEM_MASTER.ITEM%TYPE,
                             I_transpo_id          IN     TRANS_SKU.TRANSPORTATION_ID%TYPE)
   RETURN BOOLEAN;
/*END_RMS11_INS_TCD17*/
-----------------------------------------------------------------------------------------------------

--Function Name:   CHECK_ITEM_PO
--Purpose      :   Checks that an item exist on a PO.
--             :
-----------------------------------------------------------------------------------------------------

FUNCTION CHECK_ITEM_PO (O_error_message       IN OUT RTK_ERRORS.RTK_KEY%TYPE,
                        I_item                IN     ITEM_MASTER.ITEM%TYPE,
                        I_parent_item         IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                        I_order               IN     ORDLOC.ORDER_NO%TYPE,
                        I_transportation_id   IN     TRANS_SKU.TRANSPORTATION_ID%TYPE,
                        I_diff_1              IN     ITEM_MASTER.DIFF_1%TYPE,
                        I_diff_2              IN     ITEM_MASTER.DIFF_2%TYPE,
                        I_diff_3              IN     ITEM_MASTER.DIFF_3%TYPE,
                        I_diff_4              IN     ITEM_MASTER.DIFF_4%TYPE,
                        O_exists              OUT    BOOLEAN)

RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   CHECK_RECEIPT_EXISTS
--Purpose      :   Checks whether a shipment has been received for the Transportation
-----------------------------------------------------------------------------------------------------
FUNCTION CHECK_RECEIPT_EXISTS(O_error_message           IN OUT   VARCHAR2,
                              O_exists                  IN OUT   BOOLEAN,
                              I_order_no                IN       TRANSPORTATION.ORDER_NO%TYPE,
                              I_vessel_id               IN       TRANSPORTATION.VESSEL_ID%TYPE,
                              I_voyage_flt_id           IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                              I_estimated_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                              I_container_id            IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                              I_bl_awb_id               IN       TRANSPORTATION.BL_AWB_ID%TYPE,
                              I_item                    IN       TRANSPORTATION.ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function Name:   GET_RECEIVED_QTY
--Purpose      :   Sums received quantity a given unique item/order
-----------------------------------------------------------------------------------------------------
FUNCTION GET_RECEIVED_QTYS(O_error_message      IN OUT   VARCHAR2,
                           O_item_qty           IN OUT   TRANSPORTATION.ITEM_QTY%TYPE,
                           I_order_no           IN       TRANSPORTATION.ORDER_NO%TYPE,
                           I_item               IN       TRANSPORTATION.ITEM%TYPE,
                           I_container_id       IN       TRANSPORTATION.CONTAINER_ID%TYPE,
                           I_bl_awb_id          IN       TRANSPORTATION.BL_AWB_ID%TYPE,
                           I_uom                IN       UOM_CLASS.UOM%TYPE DEFAULT NULL,
                           I_calling_function   IN       VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   DELETE_DETAILS
--Purpose      :   Deletes all the child records for the given transportation id.
-----------------------------------------------------------------------------------------------------
FUNCTION DELETE_DETAILS(O_error_message       IN OUT   RTK_ERRORS.RTK_KEY%TYPE,
                        I_transportation_id   IN       TRANSPORTATION.TRANSPORTATION_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   ADD_ALL_CHILD_ITEMS
--Purpose      :   Inserts all the child records on the Order for the given transportation id.
-----------------------------------------------------------------------------------------------------
FUNCTION ADD_ALL_CHILD_ITEMS(O_error_message       IN OUT RTK_ERRORS.RTK_KEY%TYPE,
                             I_transportation_id   IN     TRANS_SKU.TRANSPORTATION_ID%TYPE,
                             I_Quantity            IN     TRANS_SKU.QUANTITY%TYPE,
                             I_Quantity_Uom        IN     TRANS_SKU.QUANTITY_UOM%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   CE_OBL_EXISTS
--Purpose      :   This function will determine whether or not any custom entry or obligation record
--                 exist for the given transportation_id.
-----------------------------------------------------------------------------------------------------
FUNCTION CE_OBL_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists            IN OUT BOOLEAN,
                       I_transportation_id IN     TRANSPORTATION.TRANSPORTATION_ID%TYPE)

RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
END TRANSPORTATION_SQL;
/
