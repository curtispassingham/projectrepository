CREATE OR REPLACE PACKAGE IMPORT_SQL AUTHID CURRENT_USER IS
  ---------------------------------------------------------------------------------------------
  -- Module                   : htsfind (Form Module) 
  -- Source Object            : FUNCTION   -> P_CHECK_EXISTS
  ---------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTS(O_error_message   IN OUT   VARCHAR2,
					  I_country         IN OUT   hts.import_country_id%TYPE,
					  I_hts             IN OUT   hts.hts%TYPE,
					  I_effect_from     IN OUT   hts.effect_from%TYPE,
					  I_effect_to       IN OUT   hts.effect_to%TYPE)
   return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : tranpo (Form Module) 
  -- Source Object            : PROCEDURE  -> P_POST_QUERY
  ---------------------------------------------------------------------------------------------
FUNCTION POST_QUERY(O_error_message    IN OUT   VARCHAR2,
					I_SHIPMENT         IN       NUMBER,
					I_TI_TO_LOC_DESC   IN OUT   VARCHAR2,
					I_TO_LOC           IN       NUMBER,
					I_TO_LOC_TYPE      IN       VARCHAR2)
   return BOOLEAN;
END IMPORT_SQL;
/