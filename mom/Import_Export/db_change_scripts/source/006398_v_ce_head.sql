--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	VIEW UPDATED:				V_CE_HEAD
----------------------------------------------------------------------------

WHENEVER SQLERROR EXIT

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_CE_HEAD'

CREATE OR REPLACE FORCE VIEW V_CE_HEAD
                   AS SELECT CEH.CE_ID,
                             CEH.STATUS,
                             CEH.ENTRY_NO,
                             CEH.ENTRY_DATE,
                             CEH.ENTRY_STATUS,
                             CEH.ENTRY_TYPE,
                             CEH.ENTRY_PORT,
                             CEH.SUMMARY_DATE,
                             CEH.RELEASE_DATE,
                             CEH.BROKER_ID,
                             CEH.BROKER_REF_ID,
                             CEH.FILE_NO,
                             CEH.IMPORTER_ID,
                             CEH.IMPORT_COUNTRY_ID,
                             CEH.CURRENCY_CODE,
                             CEH.EXCHANGE_RATE,
                             CEH.BOND_NO,
                             CEH.BOND_TYPE,
                             CEH.SURETY_CODE,
                             CEH.CONSIGNEE_ID,
                             CEH.LIVE_IND,
                             CEH.BATCH_NO,
                             CEH.ENTRY_TEAM,
                             CEH.LIQUIDATION_AMT,
                             CEH.LIQUIDATION_DATE,
                             CEH.RELIQUIDATION_AMT,
                             CEH.RELIQUIDATION_DATE,
                             CEH.MERCHANDISE_LOC,
                             CEH.LOCATION_CODE,
                             CEH.PAYEE_TYPE,
                             CEH.PAYEE,
                             CEH.COMMENTS,
                             CEH.DOWNLOADED_IND
                        FROM CE_HEAD CEH
                       WHERE NOT EXISTS (SELECT 'X'
                                           FROM CE_ORD_ITEM COI
                                          WHERE CEH.CE_ID = COI.CE_ID)
                    OR EXISTS (SELECT 'X'
                                 FROM CE_ORD_ITEM COI,
                                      V_ITEM_MASTER VIR
                                WHERE VIR.ITEM = COI.ITEM
                                  AND CEH.CE_ID  = COI.CE_ID);

COMMENT ON TABLE V_CE_HEAD IS 'This view will be used to display Customs Entry records for search screens or LOVs using a security policy to filter User access.';