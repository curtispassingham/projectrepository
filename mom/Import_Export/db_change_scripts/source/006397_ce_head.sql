--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'CE_HEAD'
ALTER TABLE CE_HEAD ADD DOWNLOADED_IND VARCHAR2 (1 BYTE) DEFAULT 'N' NOT NULL
/

COMMENT ON COLUMN CE_HEAD.DOWNLOADED_IND is 'Indicates whether this Customs Entry record was previously downloaded. This will be set to ''Y''es when the record''s status is set from ''D''ownloaded to ''W''orksheet.'
/


PROMPT ADDING CONSTRAINT 'CHK_CE_HEAD_DOWNLOADED_IND'
ALTER TABLE CE_HEAD ADD CONSTRAINT
 CHK_CE_HEAD_DOWNLOADED_IND CHECK (DOWNLOADED_IND IN ('Y','N'))
/
