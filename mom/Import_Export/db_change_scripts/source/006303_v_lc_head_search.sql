--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      VIEW CREATION:                           V_LC_HEAD_SEARCH
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       CREATIING VIEW
--------------------------------------
PROMPT Creating View 'V_LC_HEAD_SEARCH'
CREATE OR REPLACE FORCE VIEW V_LC_HEAD_SEARCH AS 
WITH partner_bk_tl  as (select * from v_partner_tl where partner_type = 'BK'),
     partner_bk_sec as (select * from v_partner where partner_type = 'BK')
select lch.LC_REF_ID
      ,lch.BANK_LC_ID
      ,lch.STATUS
      ,lch.FORM_TYPE
      ,lch.LC_TYPE
      ,lch.CURRENCY_CODE
      ,lch.ORIGIN_COUNTRY_ID
      ,lch.APPLICANT
      ,ap.partner_desc Applicant_Name
      ,lch.BENEFICIARY
      ,vs.sup_name Beneficiary_Name
      ,lch.APPLICATION_DATE
      ,lch.CONFIRMED_DATE
      ,lch.EXPIRATION_DATE
      ,lch.EARLIEST_SHIP_DATE
      ,lch.LATEST_SHIP_DATE
      ,lch.CREDIT_AVAIL_WITH
      ,caw.partner_desc Credit_Avail_With_Name
      ,lch.ISSUING_BANK
      ,isb.partner_desc Issuing_Bank_Name
      ,lch.ADVISING_BANK
      ,adb.partner_desc Advising_Bank_Name
      ,lch.CONFIRMING_BANK
      ,cfb.partner_desc Confirming_Bank_Name
      ,lch.TRANSFERRING_BANK
      ,tsf.partner_desc Transferring_Bank_Name
      ,lch.NEGOTIATING_BANK
      ,ngb.partner_desc Negotiating_Bank_Name
      ,lch.PAYING_BANK
      ,pay.partner_desc Paying_Bank_Name
      ,lch.VARIANCE_PCT
      ,lch.SPECIFICATION
      ,lch.AMOUNT_TYPE
      ,lch.PRESENTATION_TERMS
      ,lch.PURCHASE_TYPE
      ,lch.PLACE_OF_EXPIRY
      ,lch.ADVICE_METHOD
      ,lch.ISSUANCE
      ,lch.DRAFTS_AT
      ,lch.FOB_TITLE_PASS
      ,lch.FOB_TITLE_PASS_DESC
      ,lch.WITH_RECOURSE_IND
      ,lch.TRANSFERABLE_IND
      ,lch.PARTIAL_SHIPMENT_IND
      ,lch.LC_NEG_DAYS
      ,lch.COMMENTS
  from lc_head      lch,
       v_sups       vs, 
       v_partner    ap,
       partner_bk_sec isb,
       partner_bk_tl caw,
       partner_bk_tl adb,
       partner_bk_tl cfb,
       partner_bk_tl tsf,
       partner_bk_tl ngb,
       partner_bk_tl pay
 where lch.BENEFICIARY       = vs.supplier
   and lch.APPLICANT         = ap.partner_id
   and ap.partner_type       = 'AP'
   and lch.Issuing_Bank      = isb.partner_id
   and NVL(lch.Credit_Avail_With, '-999') = caw.partner_id(+)
   and NVL(lch.Advising_Bank, '-999')     = adb.partner_id(+)
   and NVL(lch.Confirming_Bank, '-999')   = cfb.partner_id(+)
   and NVL(lch.Transferring_Bank, '-999') = tsf.partner_id(+)
   and NVL(lch.Negotiating_Bank, '-999')  = ngb.partner_id(+)
   and NVL(lch.PAYING_BANK, '-999')       = pay.partner_id(+)
   and (   lch.Credit_Avail_With is null  
        or exists (select 'x'
                     from partner_bk_sec caws
                    where caws.partner_id   = lch.Credit_Avail_With
                      and rownum = 1))
   and (   lch.Advising_Bank is null  
        or exists (select 'x'
                     from partner_bk_sec adbs
                    where adbs.partner_id   = lch.Advising_Bank
                      and rownum = 1))
   and (   lch.Confirming_Bank is null  
        or exists (select 'x'
                     from partner_bk_sec cfbs
                    where cfbs.partner_id   = lch.Confirming_Bank
                      and rownum = 1))
   and (   lch.Transferring_Bank is null  
        or exists (select 'x'
                     from partner_bk_sec tsfs
                    where tsfs.partner_id   = lch.Transferring_Bank
                      and rownum = 1))
   and (   lch.Negotiating_Bank is null  
        or exists (select 'x'
                     from partner_bk_sec ngbs
                    where ngbs.partner_id   = lch.Negotiating_Bank
                      and rownum = 1))
   and (   lch.PAYING_BANK is null  
        or exists (select 'x'
                     from partner_bk_sec pays
                    where pays.partner_id   = lch.PAYING_BANK
                      and rownum = 1));
                      
COMMENT ON TABLE V_LC_HEAD_SEARCH IS 'This view is used for letter of credit search screen in RMS Alloy.'
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.LC_REF_ID IS 'The internal RMS Letter of Credit number.' 
/ 

COMMENT ON COLUMN V_LC_HEAD_SEARCH.BANK_LC_ID IS 'The letter of credit number given by the issuing bank and should be used for any reference made to the Letter of Credit.'
/ 

COMMENT ON COLUMN V_LC_HEAD_SEARCH.STATUS IS 'This will determine what status the Letter of Credit is in.  Valid values are Worksheet, Submitted, Approved, Extracted, Confirmed and Closed.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.FORM_TYPE IS 'This determines the level of detail the Letter of Credit will send to the issuing bank.  L = Long form or S = Short form.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.LC_TYPE IS 'This is the type of Letter of Credit that is being applied for.  M = Master (One Letter of Credit to many PO); N = Normal (One Letter of Credit to one PO); or R = Revolving (Continually adding POs to an Letter of Credit. Letter of Credit never closes).' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.CURRENCY_CODE IS 'This is the currency code of the orders on the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.ORIGIN_COUNTRY_ID IS 'This is the ID for the country of origin of the orders on the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.APPLICANT IS 'This is the ID of the party applying for the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.Applicant_Name IS 'Name of the party applying for the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.BENEFICIARY IS 'This is the ID for the party that the Letter of Credit is issued to.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.Beneficiary_Name IS 'Name of the party that the Letter of Credit is issued to.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.APPLICATION_DATE IS 'This is the date the Letter of Credit is considered an application for Letter of Credit and is sent to the issuing bank.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.CONFIRMED_DATE IS 'This is the date that the Letter of Credit is confirmed by the issuing bank and the Bank LC ID is populated.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.EXPIRATION_DATE IS 'This is the earliest date for all POs included in the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.EARLIEST_SHIP_DATE IS 'This is the earliest date for all POs included in the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.LATEST_SHIP_DATE IS 'This is the latest ship date for all POs included in the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.CREDIT_AVAIL_WITH IS 'Code for bank with which credit is available.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.Credit_Avail_With_Name IS 'Name for bank with which credit is available.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.ISSUING_BANK IS 'This is the ID for the bank issuing the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.Issuing_Bank_Name IS 'Name of the bank issuing the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.ADVISING_BANK IS 'This is the ID for the bank advising the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.Advising_Bank_Name IS 'Name of the bank advising the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.CONFIRMING_BANK IS 'This is the ID for the bank which guarantees the Letter of Credit funds by confirming the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.Confirming_Bank_Name IS 'Name of the bank which guarantees the Letter of Credit funds by confirming the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.TRANSFERRING_BANK IS 'This is the ID for the bank transferring funds for the Letter of Credit.  If funds need to be transferred between banks a transferring bank will handle this activity.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.Transferring_Bank_Name IS 'Name of the bank transferring funds for the Letter of Credit.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.NEGOTIATING_BANK IS 'This is the ID for bank responsible for negotiating a Letter of Credit between the seller and advising bank.  A default will be set up in the Bank dialogue. LOV will be used to display valid banks.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.Negotiating_Bank_Name IS 'Name of the bank responsible for negotiating a Letter of Credit between the seller and advising bank.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.PAYING_BANK IS 'This is the ID for bank responsible for paying the Letter of Credit to the exporter.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.Paying_Bank_Name IS 'Name of the bank responsible for paying the Letter of Credit to the exporter.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.VARIANCE_PCT IS 'Allowed currency variance percentage for the Letter of Credit.  e.g. If  the variance pct is 5, this means that Letter of Credit can be under or over paid by 5 percent. The standard for percent fields is (12,4).  However, due to the SWIFT file download restriction of a 2 digit integer, the field will be restricted on-line to 0 - 99.  Therefore, if data is inserted into this table through a conversion process, the inserted data should also be limited to 0 - 99.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.SPECIFICATION IS 'Defines any condition for the credit, e.g. maximum, or insurance to be paid additionally.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.AMOUNT_TYPE IS 'Indicates the amount type. Must be either Exact or Approximately.  If the amount type specifies Exact then the amount of the Letter of Credit must be exactly what it indicates.  If the amount type is Approximately, then the amount of the Letter of Credit may be within the variance percentage of the amount.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.PRESENTATION_TERMS IS 'Terms of presentation. e.g. to the order of any bank, or to XYZ Bank. P= by Payment A=by acceptance N=by negotiation' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.PURCHASE_TYPE IS 'Purchase terms from the PO. e.g. FOB, CIF.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.PLACE_OF_EXPIRY IS 'This is the ID for the place where the Letter of Credit will expire.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.ADVICE_METHOD IS 'Advice method used for the Letter of Credit, e.g. Full Wire, Mail, Overnight.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.ISSUANCE IS 'Issuance for the Letter of Credit, e.g. Cable, Telex.'
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.DRAFTS_AT IS 'This field specifies the terms of draft (or when payment is to be made) for the Letter of Credit.  e.g. Sight, 30 days from receipt of B/L.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.FOB_TITLE_PASS IS 'Indicator used to determine where the title for goods is passed from the vendor to the purchaser.  Examples include city, factory.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.FOB_TITLE_PASS_DESC IS 'User entered field describing the code where the title of the merchandise is to be passed.  Could be a city name, factory name, or place of origin.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.WITH_RECOURSE_IND IS 'Indicates conditional payment on the part of the bank as instructed by the buyer.  Valid values are Y and N.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.TRANSFERABLE_IND IS 'Indicates if the Letter of Credit is transferable. Valid values are Y and N.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.PARTIAL_SHIPMENT_IND IS 'Indicates if goods covered by the Letter of Credit can be partially shipped. Valid values are Y and N.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.LC_NEG_DAYS IS 'The number of days to negotiate documents.' 
/

COMMENT ON COLUMN V_LC_HEAD_SEARCH.COMMENTS IS 'The field will hold any comments the user wants to add to the Letter of Credit.' 
/
