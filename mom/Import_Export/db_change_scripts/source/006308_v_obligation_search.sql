--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_OBLIGATION_SEARCH
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_OBLIGATION_SEARCH'
CREATE OR REPLACE FORCE VIEW "V_OBLIGATION_SEARCH"
("OBLIGATION_KEY",
      "OBLIGATION_LEVEL",
      "OBLIGATION_LEVEL_DESC",
       "CONTAINER",
       "VESSEL",
       "VOYAGE_FLIGHT",
       "EST_DEPART_DATE",
       "ORDER_NO",
       "ITEM",
       "BOL_AWD",
       "ENTRY_NO",
       "ASN",
       "CARTON",
       "PARTNER_TYPE",
       "PARTNER_ID",
       "PARTNER_DESC",
       "SUPPLIER",
       "SUP_NAME",
       "EXT_INVC_NO",
       "EXT_INVC_DATE",
       "PAID_DATE",
       "PAID_AMT",
       "PAYMENT_METHOD",
       "CHECK_AUTH_NO",
       "CURRENCY_CODE",
       "EXCHANGE_RATE",
       "STATUS",
       "STATUS_DESC",
       "COMMENT_DESC",
       "ITEM_DESC")
AS (
  /*'TRCO', 'TRCPO', 'TRCP'*/
   select ob.obligation_key,
          ob.obligation_level,
          cd2.code_desc,
          ob.key_value_1 container,
          ob.key_value_2 vessel,
          ob.key_value_3 voyage_flight,
          ob.key_value_4 est_depart_date,
          ob.key_value_5 order_no,
          ob.key_value_6 item,
          null bol_awd,
          null entry_no,
          null asn,
          null carton,
          cd3.code_desc,
          ob.partner_id,
          p.partner_desc,
          ob.supplier,
          s.sup_name,
          ob.ext_invc_no,
          ob.ext_invc_date,
          ob.paid_date,
          ob.paid_amt,
          ob.payment_method,
          ob.check_auth_no,
          ob.currency_code,
          ob.exchange_rate,
          ob.status,
          cd.code_desc,
          ob.comment_desc,
          im.item_desc
   from  obligation ob,
         v_item_master im,
         v_sups s,
         v_partner p,
         v_code_detail cd,
         v_code_detail cd2,
         v_code_detail cd3
   where ob.obligation_level in ('TRCO', 'TRCPO', 'TRCP')
     and ob.key_value_6 = im.item (+)
     and ob.partner_id = p.partner_id(+)
     and ob.supplier = s.supplier (+)
     and cd.code_type = 'OBST'
     and ob.status = cd.code
     and cd2.code_type = 'OBLG'
     and ob.partner_type = cd3.code(+)
     and cd3.code_type(+) = 'PART'
     and ob.obligation_level = cd2.code
   union all
   /*'TRBL', 'TRBLP', 'TRBP'*/
   select ob.obligation_key,
          ob.obligation_level,
          cd2.code_desc,
          null container,
          ob.key_value_2 vessel,
          ob.key_value_3 voyage_flight,
          ob.key_value_4 est_depart_date,
          ob.key_value_5 order_no,
          ob.key_value_6 item,
          ob.key_value_1 bol_awd,
          null entry_no,
          null asn,
          null carton,
          cd3.code_desc,
          ob.partner_id,
          p.partner_desc,
          ob.supplier,
          s.sup_name,
          ob.ext_invc_no,
          ob.ext_invc_date,
          ob.paid_date,
          ob.paid_amt,
          ob.payment_method,
          ob.check_auth_no,
          ob.currency_code,
          ob.exchange_rate,
          ob.status,
          cd.code_desc,
          ob.comment_desc,
          im.item_desc
   from  obligation ob,
         v_item_master im,
         v_sups s,
         v_partner p,
         v_code_detail cd,
         v_code_detail cd2,
         v_code_detail cd3
   where ob.obligation_level in ('TRBL', 'TRBLP', 'TRBP')
     and ob.key_value_6 = im.item (+)
     and ob.partner_id = p.partner_id(+)
     and ob.supplier = s.supplier (+)
     and cd.code_type = 'OBST'
     and ob.status = cd.code
     and cd2.code_type = 'OBLG'
     and ob.partner_type = cd3.code(+)
     and cd3.code_type(+) = 'PART'
     and ob.obligation_level = cd2.code
   union all
   /*'TRVV', 'TRVVEP', 'TRVP'*/
   select ob.obligation_key,
          ob.obligation_level,
          cd2.code_desc,
          null container,
          ob.key_value_1 vessel,
          ob.key_value_2 voyage_flight,
          ob.key_value_3 est_depart_date,
          ob.key_value_4 order_no,
          ob.key_value_5 item,
          null bol_awd,
          null entry_no,
          null asn,
          null carton,
          cd3.code_desc,
          ob.partner_id,
          p.partner_desc,
          ob.supplier,
          s.sup_name,
          ob.ext_invc_no,
          ob.ext_invc_date,
          ob.paid_date,
          ob.paid_amt,
          ob.payment_method,
          ob.check_auth_no,
          ob.currency_code,
          ob.exchange_rate,
          ob.status,
          cd.code_desc,
          ob.comment_desc,
          im.item_desc
   from  obligation ob,
         v_item_master im,
         v_sups s,
         v_partner p,
         v_code_detail cd,
         v_code_detail cd2,
         v_code_detail cd3
   where ob.obligation_level in ('TRVV', 'TRVVEP', 'TRVP')
     and ob.key_value_5 = im.item (+)
     and ob.partner_id = p.partner_id(+)
     and ob.supplier = s.supplier (+)
     and cd.code_type = 'OBST'
     and ob.status = cd.code
     and cd2.code_type = 'OBLG'
     and ob.partner_type = cd3.code(+)
     and cd3.code_type(+) = 'PART'
     and ob.obligation_level = cd2.code
   union all
   /*'PO', 'POIT'*/
   select ob.obligation_key,
          ob.obligation_level,
          cd2.code_desc,
          null container,
          null vessel,
          null voyage_flight,
          null est_depart_date,
          ob.key_value_1 order_no,
          ob.key_value_2 item,
          null bol_awd,
          null entry_no,
          null asn,
          null carton,
          cd3.code_desc,
          ob.partner_id,
          p.partner_desc,
          ob.supplier,
          s.sup_name,
          ob.ext_invc_no,
          ob.ext_invc_date,
          ob.paid_date,
          ob.paid_amt,
          ob.payment_method,
          ob.check_auth_no,
          ob.currency_code,
          ob.exchange_rate,
          ob.status,
          cd.code_desc,
          ob.comment_desc,
          im.item_desc
   from  obligation ob,
         v_item_master im,
         v_sups s,
         v_partner p,
         v_code_detail cd,
         v_code_detail cd2,
         v_code_detail cd3
   where ob.obligation_level in ('PO', 'POIT')
     and ob.key_value_2 = im.item (+)
     and ob.partner_id = p.partner_id(+)
     and ob.supplier = s.supplier (+)
     and cd.code_type = 'OBST'
     and ob.status = cd.code
     and cd2.code_type = 'OBLG'
     and ob.partner_type = cd3.code(+)
     and cd3.code_type(+) = 'PART'
     and ob.obligation_level = cd2.code
   union all
   /*'CUST'*/
   select ob.obligation_key,
          ob.obligation_level,
          cd2.code_desc,
          null container,
          null vessel,
          null voyage_flight,
          null est_depart_date,
          null order_no,
          null item,
          null bol_awd,
          ob.key_value_1 entry_no,
          null asn,
          null carton,
          cd3.code_desc,
          ob.partner_id,
          p.partner_desc,
          ob.supplier,
          s.sup_name,
          ob.ext_invc_no,
          ob.ext_invc_date,
          ob.paid_date,
          ob.paid_amt,
          ob.payment_method,
          ob.check_auth_no,
          ob.currency_code,
          ob.exchange_rate,
          ob.status,
          cd.code_desc,
          ob.comment_desc,
          null item_desc
   from  obligation ob,
         v_sups s,
         v_partner p,
         v_code_detail cd,
         v_code_detail cd2,
         v_code_detail cd3
   where ob.obligation_level in ('CUST')
     and ob.partner_id = p.partner_id(+)
     and ob.supplier = s.supplier (+)
     and cd.code_type = 'OBST'
     and ob.status = cd.code
     and cd2.code_type = 'OBLG'
     and ob.partner_type = cd3.code(+)
     and cd3.code_type(+) = 'PART'
     and ob.obligation_level = cd2.code
   union all
   /*'POT'*/
   select ob.obligation_key,
          ob.obligation_level,
          cd2.code_desc,
          null container,
          null vessel,
          null voyage_flight,
          null est_depart_date,
          ob.key_value_1 order_no,
          null item,
          null bol_awd,
          null entry_no,
          null asn,
          null carton,
          cd3.code_desc,
          ob.partner_id,
          p.partner_desc,
          ob.supplier,
          s.sup_name,
          ob.ext_invc_no,
          ob.ext_invc_date,
          ob.paid_date,
          ob.paid_amt,
          ob.payment_method,
          ob.check_auth_no,
          ob.currency_code,
          ob.exchange_rate,
          ob.status,
          cd.code_desc,
          ob.comment_desc,
          null item_desc
   from  obligation ob,
         v_sups s,
         v_partner p,
         v_code_detail cd,
         v_code_detail cd2,
         v_code_detail cd3
   where ob.obligation_level in ('POT')
     and ob.partner_id = p.partner_id(+)
     and ob.supplier = s.supplier (+)
     and cd.code_type = 'OBST'
     and ob.status = cd.code
     and cd2.code_type = 'OBLG'
     and ob.partner_type = cd3.code(+)
     and cd3.code_type(+) = 'PART'
     and ob.obligation_level = cd2.code
   union all
   /*'ASN', 'ASNP'*/
   select ob.obligation_key,
          ob.obligation_level,
          cd2.code_desc,
          null container,
          null vessel,
          null voyage_flight,
          null est_depart_date,
          ob.key_value_2 order_no,
          null item,
          null bol_awd,
          null entry_no,
          ob.key_value_1 asn,
          null carton,
          cd3.code_desc,
          ob.partner_id,
          p.partner_desc,
          ob.supplier,
          s.sup_name,
          ob.ext_invc_no,
          ob.ext_invc_date,
          ob.paid_date,
          ob.paid_amt,
          ob.payment_method,
          ob.check_auth_no,
          ob.currency_code,
          ob.exchange_rate,
          ob.status,
          cd.code_desc,
          ob.comment_desc,
          null item_desc
   from  obligation ob,
         v_sups s,
         v_partner p,
         v_code_detail cd,
         v_code_detail cd2,
         v_code_detail cd3
   where ob.obligation_level in ('ASN', 'ASNP')
     and ob.partner_id = p.partner_id(+)
     and ob.supplier = s.supplier (+)
     and cd.code_type = 'OBST'
     and ob.status = cd.code
     and cd2.code_type = 'OBLG'
     and ob.partner_type = cd3.code(+)
     and cd3.code_type(+) = 'PART'
     and ob.obligation_level = cd2.code
   union all
   /*'ASNC'*/
   select ob.obligation_key,
          ob.obligation_level,
          cd2.code_desc,
          null container,
          null vessel,
          null voyage_flight,
          null est_depart_date,
          null order_no,
          null item,
          null bol_awd,
          null entry_no,
          ob.key_value_1 asn,
          ob.key_value_2 carton,
          cd3.code_desc,
          ob.partner_id,
          p.partner_desc,
          ob.supplier,
          s.sup_name,
          ob.ext_invc_no,
          ob.ext_invc_date,
          ob.paid_date,
          ob.paid_amt,
          ob.payment_method,
          ob.check_auth_no,
          ob.currency_code,
          ob.exchange_rate,
          ob.status,
          cd.code_desc,
          ob.comment_desc,
          null item_desc
   from  obligation ob,
         v_sups s,
         v_partner p,
         v_code_detail cd,
         v_code_detail cd2,
         v_code_detail cd3
   where ob.obligation_level in ('ASNC')
     and ob.partner_id = p.partner_id(+)
     and ob.supplier = s.supplier (+)
     and cd.code_type = 'OBST'
     and ob.status = cd.code
     and cd2.code_type = 'OBLG'
     and ob.partner_type = cd3.code(+)
     and cd3.code_type(+) = 'PART'
     and ob.obligation_level = cd2.code)
/

COMMENT ON TABLE V_OBLIGATION_SEARCH IS 'This view is used for the Obligation Search screen in RMS Alloy.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."OBLIGATION_KEY" IS 'Contains the unique sequence number that is used to distinguish between the different obligations.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."OBLIGATION_LEVEL" IS 'Contains the level that the obligation comes in at. The possible levels are CUST (Customs Entry Header), PO (Purchase Order Header), POIT (Purchase Order/Item), TRCO (Transportation Container), TRCP (Transportation Container PO/Item), TRBL (Transportation Bill of Lading/Airway Bill), TRBP (Transportation BL/AWB PO/Item), TRVV (Transportation Vessel/Voyage/ETD), and TRVP (Transportation Vessel/Voyage/ETD PO/Item).'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."OBLIGATION_LEVEL_DESC" IS 'Translated description of the obligation level.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."CONTAINER" IS 'Contains the id number of the container in which the item was shipped.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."VESSEL" IS 'Contains vessel or other vehicle identification.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."VOYAGE_FLIGHT" IS 'Contains voyage or flight number.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."EST_DEPART_DATE" IS 'Estimated date of departure for the goods leaving the port of lading.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."ORDER_NO" IS 'Contains the number that uniquely identifies an order within the system.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."ITEM" IS 'Unique alphanumeric value that identifies the item.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."BOL_AWD" IS 'Contains the Bill of Lading or Airway Bill number. This id will be generated by the agent that is shipping the goods.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."ENTRY_NO" IS 'This is the Customs assigned number for the entry of goods.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."ASN" IS 'Contains the unique number identifying a specific shipment of goods within the system.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."CARTON" IS 'Identifies the UCC-128 carton number for shipments originating from the Advance Shipment Notification process as carton shipments.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."PARTNER_TYPE" IS 'Contains the type of partner that sent the obligation.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."PARTNER_ID" IS 'Contains the identification number of the partner that sent the obligation.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."PARTNER_DESC" IS 'Contains the translated partner description or name.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."SUPPLIER" IS 'Contains the identification number of the supplier that sent the obligation.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."SUP_NAME" IS 'Contains the translated supplier trading name.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."EXT_INVC_NO" IS 'Contains the identification code of the obligation.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."EXT_INVC_DATE" IS 'Contains the date on the obligation.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."PAID_DATE" IS 'Contains the date that the obligation was paid.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."PAID_AMT" IS 'Contains amount of the obligation that was paid. This is stored in the currency of the obligation.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."PAYMENT_METHOD" IS 'Contains the method used to pay the obligation.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."CHECK_AUTH_NO" IS 'Contains the number on the check that is used to pay the obligation.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."CURRENCY_CODE" IS 'Contains the currency that the obligation amount is in.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."EXCHANGE_RATE" IS 'Contains rate of exchange used on the obligation.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."STATUS" IS 'Contains the status of the obligation. Possible choices are P (Pending), or A (Approved). This status is used to determine when the obligation information can be used by accounts payable.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."STATUS_DESC" IS 'Translated description of the Status.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."COMMENT_DESC" IS 'Contains user comments.'
/

COMMENT ON COLUMN V_OBLIGATION_SEARCH."ITEM_DESC" IS 'Long translated description of the item.'
/




