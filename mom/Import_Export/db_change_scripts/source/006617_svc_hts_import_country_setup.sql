--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_HTS_IMPORT_COUNTRY_SETUP
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_HTS_IMPORT_COUNTRY_SETUP'
CREATE TABLE SVC_HTS_IMPORT_COUNTRY_SETUP
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  IMPORT_COUNTRY_ID VARCHAR2(3 ),
  HTS_FORMAT_MASK VARCHAR2(50 ),
  HTS_HEADING_LENGTH NUMBER(2,0),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_HTS_IMPORT_COUNTRY_SETUP is 'This is a staging table used for Admin API spreadsheet upload process. It is used to temporarily hold data before it is uploaded/updated in HTS_IMPORT_COUNTRY_SETUP.'
/

COMMENT ON COLUMN SVC_HTS_IMPORT_COUNTRY_SETUP.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_HTS_IMPORT_COUNTRY_SETUP.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_HTS_IMPORT_COUNTRY_SETUP.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_HTS_IMPORT_COUNTRY_SETUP.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_HTS_IMPORT_COUNTRY_SETUP.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_HTS_IMPORT_COUNTRY_SETUP.IMPORT_COUNTRY_ID is 'Holds the import country.'
/

COMMENT ON COLUMN SVC_HTS_IMPORT_COUNTRY_SETUP.HTS_FORMAT_MASK is 'Holds the format mask of HTS for the import country. The value should start with FM and the separator should be in double quotes, Example: FMXXXX"."XX"."XXXX'
/

COMMENT ON COLUMN SVC_HTS_IMPORT_COUNTRY_SETUP.HTS_HEADING_LENGTH is 'Holds the number of characters that forms the chapter in the HTS.'
/

COMMENT ON COLUMN SVC_HTS_IMPORT_COUNTRY_SETUP.CREATE_ID is 'User who created the record.'
/

COMMENT ON COLUMN SVC_HTS_IMPORT_COUNTRY_SETUP.CREATE_DATETIME is 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_HTS_IMPORT_COUNTRY_SETUP.LAST_UPD_ID is 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_HTS_IMPORT_COUNTRY_SETUP.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_HTS_IMPORT_COUNTRY_SETUP'
ALTER TABLE SVC_HTS_IMPORT_COUNTRY_SETUP
 ADD CONSTRAINT SVC_HTS_IMP_CNT_SETUP_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_HTS_IMPORT_COUNTRY_SETUP'
ALTER TABLE SVC_HTS_IMPORT_COUNTRY_SETUP
 ADD CONSTRAINT SVC_HTS_IMP_CNT_SETUP_UK UNIQUE
  (IMPORT_COUNTRY_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

