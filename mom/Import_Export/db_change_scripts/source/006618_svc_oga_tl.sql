--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_OGA_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_OGA_TL'
CREATE TABLE SVC_OGA_TL
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  LANG NUMBER(6,0),
  OGA_CODE VARCHAR2(3 ),
  OGA_DESC VARCHAR2(250 ),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_OGA_TL is 'This is a staging table used for Admin API spreadsheet upload process. It is used to temporarily hold data before it is uploaded/updated in OGA_TL.'
/

COMMENT ON COLUMN SVC_OGA_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_OGA_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_OGA_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_OGA_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_OGA_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_OGA_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_OGA_TL.OGA_CODE is 'Contains the customs approved code to uniquely identify a special tariff program.'
/

COMMENT ON COLUMN SVC_OGA_TL.OGA_DESC is 'Contains the description for the tariff treatment ID.  Example: NAFTA is North American Free Trade Agreement.'
/

COMMENT ON COLUMN SVC_OGA_TL.CREATE_ID is 'User who created the record.'
/

COMMENT ON COLUMN SVC_OGA_TL.CREATE_DATETIME is 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_OGA_TL.LAST_UPD_ID is 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_OGA_TL.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_OGA_TL'
ALTER TABLE SVC_OGA_TL
 ADD CONSTRAINT SVC_OGA_TL_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_OGA_TL'
ALTER TABLE SVC_OGA_TL
 ADD CONSTRAINT SVC_OGA_TL_UK UNIQUE
  (OGA_CODE,
   LANG
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

