--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	View Modified:						 v_alc_ship
---------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       MODIFYING VIEW
--------------------------------------
PROMPT CREATING VIEW 'V_ALC_SHIP';

CREATE OR REPLACE FORCE VIEW V_ALC_SHIP
("ORDER_NO",
 "ITEM", 
 "PACK_ITEM",
 "VESSEL_ID",
 "VOYAGE_FLT_ID",
 "ESTIMATED_DEPART_DATE")
AS 
    SELECT distinct order_no,
           item,
           pack_item,
           vessel_id,
           voyage_flt_id,
           estimated_depart_date
      from alc_head
     where vessel_id is not NULL;

COMMENT ON TABLE V_ALC_SHIP IS 'This view is used in the Transportation Shipment section of the ALC screen to show all transportation shipments for the given order/item/pack item.'
/

COMMENT ON COLUMN V_ALC_SHIP.ORDER_NO IS 'Contains the order number.'
/

COMMENT ON COLUMN V_ALC_SHIP.ITEM IS 'Contains the item number.'
/

COMMENT ON COLUMN V_ALC_SHIP.PACK_ITEM IS 'Contains the pack number the item is part of.'
/

COMMENT ON COLUMN V_ALC_SHIP.VESSEL_ID IS 'Contains the transportation vessel id.'
/

COMMENT ON COLUMN V_ALC_SHIP.VOYAGE_FLT_ID IS 'Contains the voyage flight id.'
/

COMMENT ON COLUMN V_ALC_SHIP.ESTIMATED_DEPART_DATE IS 'Contains the estimated departure date of the shipment.'
/

