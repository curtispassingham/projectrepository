--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ALC_HEAD'

PROMPT Dropping CHECK CONSTRAINT 'CHK_ALC_HEAD_STATUS'
DECLARE
  L_constraint_exists number := 0;
BEGIN
  SELECT count(*) INTO L_constraint_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_ALC_HEAD_STATUS'
     AND constraint_TYPE = 'C';

  if (L_constraint_exists != 0) then
      execute immediate 'ALTER TABLE ALC_HEAD DROP CONSTRAINT  CHK_ALC_HEAD_STATUS';
  end if;
end;
/

UPDATE ALC_HEAD SET STATUS = 'E' WHERE OBLIGATION_KEY IS NULL AND CE_ID IS NULL AND STATUS = 'N'
/

PROMPT Adding CHECK CONSTRAINT 'CHK_ALC_HEAD_STATUS'
ALTER TABLE ALC_HEAD ADD CONSTRAINT CHK_ALC_HEAD_STATUS CHECK (((OBLIGATION_KEY IS NULL AND CE_ID IS NULL AND STATUS IN ('E')) OR (OBLIGATION_KEY IS NOT NULL AND STATUS IN ('P','PW','N')) OR (CE_ID IS NOT NULL AND STATUS IN ('P','PW','N'))))
/
