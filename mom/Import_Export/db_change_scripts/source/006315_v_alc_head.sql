--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_BR_ITEM_LOC_FISCAL_ATTRIB
----------------------------------------------------------------------------

--whenever sqlerror exit

--------------------------------------
--    MODIFYING VIEW       V_ALC_HEAD
--------------------------------------
PROMPT CREATING VIEW 'V_ALC_HEAD';

CREATE OR REPLACE FORCE VIEW V_ALC_HEAD
("ORDER_NO", 
"SHIPMENT", 
"ITEM", 
"PACK_ITEM", 
"SEQ_NO", 
"OBLIGATION_KEY", 
"CE_ID", 
"VESSEL_ID", 
"VOYAGE_FLT_ID", 
"ESTIMATED_DEPART_DATE", 
"ALC_QTY", 
"STATUS", 
"PO_ALC_STATUS", 
"ERROR_IND") AS 
 SELECT ahd.order_no order_no,
    ahd.shipment,
    ahd.item item,
    ahd.pack_item pack_item,
    ahd.seq_no seq_no,
    ahd.obligation_key obligation_key,
    ahd.ce_id ce_id,
    ahd.vessel_id vessel_id,
    ahd.voyage_flt_id voyage_flt_id,
    ahd.estimated_depart_date estimated_depart_date,
    ahd.alc_qty alc_qty,
    ahd.status status,
    CAST(alc_sql.determine_status( MAX(DECODE(ahd.status, 'E', 'Y')) over (partition BY order_no, shipment), MAX(DECODE(ahd.status, 'N', 'Y')) over (partition BY order_no, shipment), MAX(DECODE(ahd.status, 'P', 'Y')) over (partition BY order_no, shipment), MAX(DECODE(ahd.status, 'F', 'Y', 'PW', 'Y')) over (partition BY order_no, shipment)) AS VARCHAR2(2)) po_alc_status,
    ahd.error_ind error_ind
  FROM alc_head ahd,
    v_deps vdp,
    v_item_master vir
  WHERE vir.item = ahd.item
  AND vir.dept   = vdp.dept
  AND(EXISTS(SELECT 'x'
               FROM obligation ob
              WHERE (ob.obligation_key = ahd.obligation_key
                AND ahd.obligation_key   IS NOT NULL)
                 OR ( ahd.obligation_key  IS NULL
                AND ahd.ce_id            IS NOT NULL))
	           OR EXISTS (SELECT 'x' 
			                FROM alc_comp_loc acl, ordloc_exp exp
                           WHERE acl.order_no = exp.order_no
                             AND ahd.order_no=acl.order_no
                             AND acl.comp_id = exp.comp_id
                             AND (exp.nom_flag_5 = '+' or exp.nom_flag_5 = '-'))
               OR EXISTS (SELECT 'x'
                            FROM alc_comp_loc acl, ordsku_hts_assess a, ordsku_hts h
                           WHERE acl.order_no = a.order_no
                             AND acl.order_no=ahd.order_no
                             AND h.order_no = a.order_no
                             AND h.seq_no = a.seq_no
                             AND acl.comp_id = a.comp_id
                             AND (a.nom_flag_5 = '+' or a.nom_flag_5 = '-')))
/
							 