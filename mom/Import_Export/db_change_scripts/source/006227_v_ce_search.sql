--------------------------------------------------------
-- Copyright (c) 2014, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------

----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_CE_SEARCH
----------------------------------------------------------------------------

--whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_CE_SEARCH'

CREATE OR REPLACE FORCE VIEW "V_CE_SEARCH" 
  ("CE_ID", 
  "STATUS", 
  "STATUS_DESC", 
  "ENTRY_NO", 
  "ENTRY_DATE", 
  "ENTRY_STATUS", 
  "ENTRY_STATUS_DESC", 
  "ENTRY_TYPE", 
  "ENTRY_TYPE_DESC", 
  "ENTRY_PORT", 
  "ENTRY_PORT_DESC", 
  "SUMMARY_DATE", 
  "RELEASE_DATE", 
  "BROKER_ID", 
  "BROKER_NAME", 
  "BROKER_REF_ID", 
  "FILE_NO", 
  "IMPORTER_ID", 
  "IMPORTER_NAME", 
  "IMPORT_COUNTRY_ID", 
  "COUNTRY_DESC", 
  "CURRENCY_CODE", 
  "EXCHANGE_RATE", 
  "BOND_NO", 
  "BOND_TYPE", 
  "BOND_TYPE_DESC", 
  "SURETY_CODE", 
  "SURETY_DESC", 
  "CONSIGNEE_ID", 
  "CONSIGNEE_NAME", 
  "LIQUIDATION_AMT", 
  "LIQUIDATION_DATE", 
  "RELIQUIDATION_AMT", 
  "RELIQUIDATION_DATE", 
  "MERCHANDISE_LOC", 
  "LOCATION_CODE", 
  "COMMENTS", 
  "VESSEL_ID", 
  "VOYAGE_FLIGHT_NO", 
  "EST_DEPART_DATE", 
  "ORDER_NO", 
  "INVOICE", 
  "BOL_AWB", 
  "DIVISION", 
  "DIV_NAME", 
  "GROUP_NO", 
  "GROUP_NAME", 
  "DEPT", 
  "DEPT_NAME", 
  "CLASS", 
  "CLASS_NAME", 
  "SUBCLASS", 
  "SUB_NAME", 
  "ITEM", 
  "ITEM_DESC") AS 
  with ce as 
   (select ce_id,
           status,
           (select code_desc from v_code_detail where code_type = 'CEST' and code = status) status_desc,
           entry_no,
           entry_date,
           entry_status,
           (select entry_status_desc from v_entry_status_tl vtl where vtl.entry_status = ch.entry_status) entry_status_desc,
           entry_type,
           (select entry_type_desc from v_entry_type_tl vtl where vtl.entry_type = ch.entry_type) entry_type_desc,
           entry_port,
           (select outloc_desc from v_outloc_tl vtl where vtl.outloc_type = 'EP' and vtl.outloc_id = ch.entry_port) entry_port_desc,
           summary_date,
           release_date,
           broker_id,
           (select partner_desc from v_partner_tl vtl where vtl.partner_type = 'BR' and vtl.partner_id = ch.broker_id) broker_name,
           broker_ref_id,
           file_no,
           importer_id,
           (select partner_desc from v_partner_tl vtl where vtl.partner_type = 'IM' and vtl.partner_id = ch.importer_id) importer_name,
           import_country_id,
           (select country_desc from v_country_tl vtl where vtl.country_id = ch.import_country_id) country_desc,
           currency_code,
           exchange_rate,
           bond_no,
           bond_type,
           (select code_desc from v_code_detail where code_type = 'BOTP' and code = ch.bond_type) bond_type_desc,
           surety_code,
           (select code_desc from v_code_detail where code_type = 'SUCO' and code = ch.surety_code) surety_desc, 
           consignee_id,
           (select partner_desc from v_partner_tl vtl where vtl.partner_type = 'CN' and vtl.partner_id = ch.consignee_id) consignee_name,
           liquidation_amt,
           liquidation_date,
           reliquidation_amt,
           reliquidation_date,
           merchandise_loc,
           location_code,
           comments
      from ce_head ch)
-- Records with entry only in CE_HEAD and CE_SHIPMENT
select ce.*,
       cs.vessel_id,
       cs.voyage_flt_id voyage_flight_no,   
       cs.estimated_depart_date est_depart_date,
       null order_no,
       null invoice,
       null bol_awb,
       null division,
       null div_name,
       null group_no, 
       null group_name,
       null dept,
       null dept_name, 
       null class,	   
       null class_name,	
       null subclass,	   
       null sub_name,
       null item,   
       null item_desc 
  from ce,
       ce_shipment cs
 where ce.ce_id = cs.ce_id(+)
   and not exists (select 1 
                     from ce_ord_item co
                    where co.ce_id = cs.ce_id
                      and co.vessel_id = cs.vessel_id
                      and co.voyage_flt_id = cs.voyage_flt_id
                      and co.estimated_depart_date = cs.estimated_depart_date
                      and rownum = 1)
UNION ALL
--Records with entry in all CE_HEAD, CE_SHIPMENT and CE_ORD_ITEM.
select ce.*,
       cs.vessel_id,
       cs.voyage_flt_id voyage_flight_no,   
       cs.estimated_depart_date est_depart_date,
       co.order_no,
       co.invoice_id invoice,
       co.bl_awb_id bol_awb,
       im.division,
       vmh.div_name,   
       im.group_no,   
       vmh.group_name,
       im.dept,
       vmh.dept_name, 
       im.class,
       vmh.class_name, 
       im.subclass,
       vmh.sub_name,	
       co.item,
       (select item_desc from v_item_master_tl vtl where vtl.item = co.item) item_desc    
  from ce,
       ce_shipment cs,
       ce_ord_item co,
       v_item_master im,
       v_merch_hierarchy vmh  
 where ce.ce_id = cs.ce_id
   and co.ce_id = cs.ce_id
   and co.vessel_id = cs.vessel_id
   and co.voyage_flt_id = cs.voyage_flt_id
   and co.estimated_depart_date = cs.estimated_depart_date
   and co.item = im.item
   and im.dept = vmh.dept
   and im.class = vmh.class
   and im.subclass = vmh.subclass;
/

COMMENT ON TABLE V_CE_SEARCH IS 'This view is used for the customs entry search screen in RMS Alloy. It includes all fields that can be used as the search criteria along with their descriptions to be displayed in the search result.'
/

COMMENT ON COLUMN V_CE_SEARCH."CE_ID" IS 'This is the unique identifier for the Customs Entry module.'
/

COMMENT ON COLUMN V_CE_SEARCH."STATUS" IS 'This is the internal status of the CE ID.  Valid values will be: Worksheet, Downloaded, and Uploaded.'
/

COMMENT ON COLUMN V_CE_SEARCH."STATUS_DESC" IS 'Contains the translated name of the status.'
/

COMMENT ON COLUMN V_CE_SEARCH."ENTRY_NO" IS 'This is the Customs assigned number for the entry of goods.'
/

COMMENT ON COLUMN V_CE_SEARCH."ENTRY_DATE" IS 'This is the date that the entry number was assigned.'
/

COMMENT ON COLUMN V_CE_SEARCH."ENTRY_STATUS" IS 'This is the status which the entry is currently in.  This status is defined by the customs agency.'
/

COMMENT ON COLUMN V_CE_SEARCH."ENTRY_STATUS_DESC" IS 'Contains the translated name of the entry_status.'
/

COMMENT ON COLUMN V_CE_SEARCH."ENTRY_TYPE" IS 'This is a Customs defined entry type.'
/

COMMENT ON COLUMN V_CE_SEARCH."ENTRY_TYPE_DESC" IS 'Contains the translated name of the entry_type.'
/

COMMENT ON COLUMN V_CE_SEARCH."ENTRY_PORT" IS 'This contains the outside location where the entry is taking place.'
/

COMMENT ON COLUMN V_CE_SEARCH."ENTRY_PORT_DESC" IS 'Contains the translated name of the entry_type.'
/

COMMENT ON COLUMN V_CE_SEARCH."SUMMARY_DATE" IS 'Date the Entry Summary document is submitted to Customs.'
/

COMMENT ON COLUMN V_CE_SEARCH."RELEASE_DATE" IS 'Contains the date when the goods are released from the customs agency.'
/

COMMENT ON COLUMN V_CE_SEARCH."BROKER_ID" IS 'This identifies which broker partner is handling the entry.'
/

COMMENT ON COLUMN V_CE_SEARCH."BROKER_NAME" IS 'Contains the translated name of the broker_id.'
/

COMMENT ON COLUMN V_CE_SEARCH."BROKER_REF_ID" IS 'Contains a customs defined reference number for the importing broker.'
/

COMMENT ON COLUMN V_CE_SEARCH."FILE_NO" IS 'This column matches up with the U.S. Customs 7501 File Number field.'
/

COMMENT ON COLUMN V_CE_SEARCH."IMPORTER_ID" IS 'This identifies who is importing the goods.'
/

COMMENT ON COLUMN V_CE_SEARCH."IMPORTER_NAME" IS 'Contains the translated name of the IMPORTER_ID.'
/

COMMENT ON COLUMN V_CE_SEARCH."IMPORT_COUNTRY_ID" IS 'This is the country in which the entry is occurring.'
/

COMMENT ON COLUMN V_CE_SEARCH."COUNTRY_DESC" IS 'Contains the translated name of the import_country_id.'
/

COMMENT ON COLUMN V_CE_SEARCH."CURRENCY_CODE" IS 'This is the currency in which all fees will be paid.'
/

COMMENT ON COLUMN V_CE_SEARCH."EXCHANGE_RATE" IS 'This is the exchange rate which would convert the Entry Currency into the primary currency.'
/

COMMENT ON COLUMN V_CE_SEARCH."BOND_NO" IS 'This is the number of the bond required for entry.'
/

COMMENT ON COLUMN V_CE_SEARCH."BOND_TYPE" IS 'A code representing the valid bond types.  U.S. Customs valid bond type codes are:	 0 = No bond required, 8 = Continuous bond, 9 = Single transaction bond'
/

COMMENT ON COLUMN V_CE_SEARCH."BOND_TYPE_DESC" IS 'Contains the translated name of the bond_type.'
/

COMMENT ON COLUMN V_CE_SEARCH."SURETY_CODE" IS 'A code identifying the surety company providing bond coverage for the importation.'
/

COMMENT ON COLUMN V_CE_SEARCH."SURETY_DESC" IS 'Contains the translated name of the surety_code.'
/

COMMENT ON COLUMN V_CE_SEARCH."CONSIGNEE_ID" IS 'This identifies which Oracle Retail partner will eventually receive the goods.'
/

COMMENT ON COLUMN V_CE_SEARCH."CONSIGNEE_NAME" IS 'Contains the translated name of the consignee_id.'
/

COMMENT ON COLUMN V_CE_SEARCH."LIQUIDATION_AMT" IS 'This value is the liquidation amount of the entry.'
/

COMMENT ON COLUMN V_CE_SEARCH."LIQUIDATION_DATE" IS 'This is the date the liquidation amount is reported.'
/

COMMENT ON COLUMN V_CE_SEARCH."RELIQUIDATION_AMT" IS 'This value is the reliquidation amount of the entry.'
/

COMMENT ON COLUMN V_CE_SEARCH."RELIQUIDATION_DATE" IS 'This is the date the reliquidation amount is reported.'
/

COMMENT ON COLUMN V_CE_SEARCH."MERCHANDISE_LOC" IS 'This column matches with the U.S. 7501 Location of Goods field. (Note: this is not a reference to a Oracle Retail Store, Warehouse or Outside Location.)'
/

COMMENT ON COLUMN V_CE_SEARCH."LOCATION_CODE" IS 'This column matches with the U.S. 7501 G.O. No. field. (Note: this is not a reference to a Oracle Retail Store, Warehouse or Outside Location.)'
/

COMMENT ON COLUMN V_CE_SEARCH."COMMENTS" IS 'This contains the user comments.'
/

COMMENT ON TABLE V_CE_SEARCH IS 'This view is used for the customs entry search screen in RMS Alloy. It includes all fields that can be used as the search criteria along with their descriptions to be displayed in the search result.'
/

COMMENT ON COLUMN V_CE_SEARCH."VESSEL_ID" IS 'Contains vessel or other vehicle identification.'
/

COMMENT ON COLUMN V_CE_SEARCH."VOYAGE_FLIGHT_NO" IS 'voyage_flight_no. Contains voyage or flight number.'
/

COMMENT ON COLUMN V_CE_SEARCH."EST_DEPART_DATE" IS 'Estimated date of departure for the goods leaving the port of lading.'
/

COMMENT ON COLUMN V_CE_SEARCH."ORDER_NO" IS 'Contains the purchase order number from which the item was originated.'
/

COMMENT ON COLUMN V_CE_SEARCH."INVOICE" IS 'Contains the invoice id associated with the purchase order or item.'
/

COMMENT ON COLUMN V_CE_SEARCH."BOL_AWB" IS 'Contains the Bill of Lading or Airway Bill number.  This id will be generated by the agent that is shipping the goods.'
/

COMMENT ON COLUMN V_CE_SEARCH."DIVISION" IS 'Contains the number which uniquely identifies the division.'
/

COMMENT ON COLUMN V_CE_SEARCH."DIV_NAME" IS 'Contains the translated name of the division.'
/

COMMENT ON COLUMN V_CE_SEARCH."GROUP_NO" IS 'Contains the number which uniquely identifies the group.'
/

COMMENT ON COLUMN V_CE_SEARCH."GROUP_NAME" IS 'Contains the translated name of the group.'
/

COMMENT ON COLUMN V_CE_SEARCH."DEPT" IS 'Contains the number which uniquely identifies the department.'
/

COMMENT ON COLUMN V_CE_SEARCH."DEPT_NAME" IS 'Contains the translated name of the department.'
/

COMMENT ON COLUMN V_CE_SEARCH."CLASS" IS 'Contains the number which uniquely identifies the class within a department.'
/

COMMENT ON COLUMN V_CE_SEARCH."CLASS_NAME" IS 'Contains the translated name of the class.'
/

COMMENT ON COLUMN V_CE_SEARCH."SUBCLASS" IS 'Contains the number which uniquely identifies the subclass within a department and class.'
/

COMMENT ON COLUMN V_CE_SEARCH."SUB_NAME" IS 'Contains the translated name of the subclass.'
/

COMMENT ON COLUMN V_CE_SEARCH."ITEM" IS 'Unique alphanumeric value that identifies the item.'
/

COMMENT ON COLUMN V_CE_SEARCH."ITEM_DESC" IS 'Long description of the item.'
/
