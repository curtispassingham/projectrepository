--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_OGA
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_OGA'
CREATE TABLE SVC_OGA
 (PROCESS_ID NUMBER(10,0) NOT NULL,
  CHUNK_ID NUMBER(10,0) DEFAULT 1 NOT NULL,
  ROW_SEQ NUMBER(20,0) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ) DEFAULT 'N',
  OGA_CODE VARCHAR2(3 ),
  OGA_DESC VARCHAR2(250 ),
  REQ_FORM VARCHAR2(30 ),
  CREATE_ID VARCHAR2(30 ) DEFAULT USER,
  CREATE_DATETIME DATE DEFAULT SYSDATE,
  LAST_UPD_ID VARCHAR2(30 ) DEFAULT USER,
  LAST_UPD_DATETIME DATE DEFAULT SYSDATE
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_OGA is 'This is a staging table used for Admin API spreadsheet upload process. It is used to temporarily hold data before it is uploaded/updated in OGA.'
/

COMMENT ON COLUMN SVC_OGA.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_OGA.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_OGA.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_OGA.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_OGA.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_OGA.OGA_CODE is 'Contains a unique code which will identify the government agency.  The codes are provided by customs.'
/

COMMENT ON COLUMN SVC_OGA.OGA_DESC is 'Contains a description of the government agency.'
/

COMMENT ON COLUMN SVC_OGA.REQ_FORM is 'Contains a description of the government agency.'
/

COMMENT ON COLUMN SVC_OGA.CREATE_ID is 'User who created the record.'
/

COMMENT ON COLUMN SVC_OGA.CREATE_DATETIME is 'Date time when record was inserted.'
/

COMMENT ON COLUMN SVC_OGA.LAST_UPD_ID is 'User who last updated the record.'
/

COMMENT ON COLUMN SVC_OGA.LAST_UPD_DATETIME is 'Date time when record was last updated.'
/


PROMPT Creating Primary Key on 'SVC_OGA'
ALTER TABLE SVC_OGA
 ADD CONSTRAINT SVC_OGA_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_OGA'
ALTER TABLE SVC_OGA
 ADD CONSTRAINT SVC_OGA_UK UNIQUE
  (OGA_CODE
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

