CREATE OR REPLACE PACKAGE MBL_RPO_SVC AUTHID CURRENT_USER AS

LP_ordhead_row      ORDHEAD%ROWTYPE := NULL;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--Function Name : PO_SEARCH
--Purpose       : This function will return recent purchase order details ,
--                according to input passed in search criteria and 
--                pagination details.
--------------------------------------------------------------------------------
FUNCTION PO_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result          IN OUT MBL_RPO_SEARCH_RES_TBL,
                   IO_pagination     IN OUT MBL_PAGINATION_REC,
                   I_search_record   IN     MBL_RPO_USER_CRITERIA_REC)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--Function Name : GET_ORIG_CODE_LIST
--Purpose       : This function will return code, code_description (translated) ,
--                code_seq from code detail table of code type 'OROR'.           
--------------------------------------------------------------------------------
FUNCTION GET_ORIG_CODE_LIST(O_error_message   IN  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_result          OUT MBL_CODE_DETAIL_TBL)
                            
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--Function Name : GET_PO_STATUS_LIST
--Purpose       : This function will return code, code_description (translated) ,
--                code_seq from code detail table of code type 'ORST'.           
--------------------------------------------------------------------------------
FUNCTION GET_PO_STATUS_LIST(O_error_message   IN  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_result         OUT MBL_CODE_DETAIL_TBL)
                            
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--Function Name : GET_PO_SUMMARY
--Purpose       : This function will return order summary details like status,
--                total cost,total retail,otb details,tran item,parent items,
--                parent items with diff details for a passed in order number.
--------------------------------------------------------------------------------
FUNCTION GET_PO_SUMMARY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result          IN OUT MBL_RPO_ORDER_SUM_REC,
                        I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--Function Name : GET_PO_ITEM_LOC
--Purpose       : This function will return location level qty recieved,
--                total cost, for a passed in order number and item,
--               Item can be tran level or parent or parent aggregated at diff.
---------------------------------------------------------------------------------
FUNCTION GET_PO_ITEM_LOC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result          IN OUT MBL_RPO_ORDER_ITEM_LOC_TBL,
                         IO_pagination     IN OUT MBL_PAGINATION_REC,
                         I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                         I_level           IN     VARCHAR2,
                         I_item            IN     ITEM_MASTER.ITEM%TYPE,
                         I_diff_1          IN     ITEM_MASTER.DIFF_1%TYPE,
                         I_diff_2          IN     ITEM_MASTER.DIFF_2%TYPE,
                         I_diff_3          IN     ITEM_MASTER.DIFF_3%TYPE,
                         I_diff_4          IN     ITEM_MASTER.DIFF_4%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--Function Name : GET_PO_ITEMS
--Purpose       : This function will return items information in particular order
--                based on the input display level,either at transcation level or
--                parent item level or parent item at diff aggregated level.
---------------------------------------------------------------------------------
FUNCTION GET_PO_ITEMS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_result              IN OUT MBL_RPO_ORDER_SUM_ITEM_TBL,
                      IO_pagination         IN OUT MBL_PAGINATION_REC,
                      I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                      I_items_disply_level  IN     VARCHAR2)

RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION APPROVE_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result           OUT MBL_RPO_STATUS_REC,
                    I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REJECT_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result           OUT MBL_RPO_STATUS_REC,
                   I_comments      IN     ORDHEAD.COMMENT_DESC%TYPE,
                   I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION CANCEL_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result           OUT MBL_RPO_STATUS_REC,
                   I_cancel_code   IN     ORDLOC.CANCEL_CODE%TYPE,
                   I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--Function Name : GET_PO_USERS
--Purpose       : This function will return list of users who have created orders 
--                based on the input search string, page number and page size.
-------------------------------------------------------------------------------------
FUNCTION GET_PO_USERS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_result          IN OUT   OBJ_VARCHAR_ID_TABLE,
                      IO_pagination     IN OUT   MBL_PAGINATION_REC,
                      I_search_string   IN       VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--Function Name : UPDATE_PO_DATES
--Purpose       : This function will take a list of order numbers and update
--                the orders' dates.
------------------------------------------------------------------------------------
FUNCTION UPDATE_PO_DATES(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result             OUT MBL_RPO_STATUS_REC,
                         I_not_before_date IN     ORDHEAD.NOT_BEFORE_DATE%TYPE,
                         I_not_after_date  IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                         I_otb_eow_date    IN     ORDHEAD.OTB_EOW_DATE%TYPE,
                         I_orders          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--Function Name : GET_CANCEL_CODE_LIST
--Purpose       : This function will return code, code_description (translated) ,
--                code_seq from code detail table of code type 'ORCA'.           
-------------------------------------------------------------------------------------
FUNCTION GET_CANCEL_CODE_LIST(O_error_message   IN  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_result          OUT MBL_CODE_DETAIL_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------------
END MBL_RPO_SVC;
/
