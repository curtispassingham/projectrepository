CREATE OR REPLACE PACKAGE MBL_PO_SVC AUTHID CURRENT_USER AS
--------------------------------------------------------------------
FUNCTION GET_NEXT_ORDER_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_order_no         OUT ORDHEAD.ORDER_NO%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------
FUNCTION GET_SUP_TERMS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_terms            OUT MBL_PO_SUP_TERMS_TBL)
RETURN NUMBER;
----------------------------------------------------------------------
FUNCTION ITEM_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_result          IN OUT MBL_PO_ITEM_SEARCH_RESULT_TBL,
                     IO_pagination     IN OUT MBL_PAGINATION_REC,
                     I_search_type     IN     VARCHAR2,
                     I_search_string   IN     VARCHAR2,
                     I_dept            IN     ITEM_MASTER.DEPT%TYPE DEFAULT NULL,   --optional
                     I_supplier        IN     SUPS.SUPPLIER%TYPE DEFAULT NULL,      --optional
                     I_locations       IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL)    --optional
RETURN NUMBER;
----------------------------------------------------------------------
FUNCTION ITEM_LOAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result          IN OUT MBL_PO_ITEM_SEARCH_RESULT_TBL,
                   I_items           IN     OBJ_VARCHAR_ID_TABLE,
                   I_supplier        IN     SUPS.SUPPLIER%TYPE DEFAULT NULL,      --optional
                   I_locations       IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL)    --optional
RETURN NUMBER;
----------------------------------------------------------------------
FUNCTION SUPPLIER_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result          IN OUT MBL_PO_SUP_SEARCH_RESULT_TBL,
                         IO_pagination     IN OUT MBL_PAGINATION_REC,
                         I_search_string   IN     VARCHAR2,
                         I_locations       IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL,  --optional
                         I_items           IN     OBJ_VARCHAR_ID_TABLE DEFAULT NULL)  --optional
RETURN NUMBER;
----------------------------------------------------------------------
FUNCTION SUPPLIER_LOAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_result          IN OUT MBL_PO_SUP_SEARCH_RESULT_TBL,
                       I_suppliers       IN     OBJ_NUMERIC_ID_TABLE,
                       I_locations       IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL,  --optional
                       I_items           IN     OBJ_VARCHAR_ID_TABLE DEFAULT NULL)  --optional
RETURN NUMBER;
----------------------------------------------------------------------
FUNCTION LOC_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result          IN OUT MBL_PO_LOC_SEARCH_RESULT_TBL,
                    IO_pagination     IN OUT MBL_PAGINATION_REC,
                    I_loc_type        IN     VARCHAR2,
                    I_search_string   IN     VARCHAR2,
                    I_org_unit_id     IN     ORG_UNIT.ORG_UNIT_ID%TYPE DEFAULT NULL, --optional
                    I_supplier        IN     SUPS.SUPPLIER%TYPE DEFAULT NULL)        --optional
RETURN NUMBER;
----------------------------------------------------------------------
FUNCTION LOC_LOAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_result          IN OUT MBL_PO_LOC_SEARCH_RESULT_TBL,
                  I_locations       IN     OBJ_NUMERIC_ID_TABLE,
                  I_supplier        IN     SUPS.SUPPLIER%TYPE DEFAULT NULL)     --optional
RETURN NUMBER;
----------------------------------------------------------------------
FUNCTION CREATE_PO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_order_object    IN     MBL_PO_XORDERDESC_REC)
RETURN NUMBER;
--------------------------------------------------------------------------
END MBL_PO_SVC;
/
