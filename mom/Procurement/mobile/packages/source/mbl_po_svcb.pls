CREATE OR REPLACE PACKAGE BODY MBL_PO_SVC AS
--------------------------------------------------------------------
--------------------------------------------------------------------
FUNCTION ITEM_QUERY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result          IN OUT MBL_PO_ITEM_SEARCH_RESULT_TBL,
                    IO_pagination     IN OUT MBL_PAGINATION_REC,
                    I_search_type     IN     VARCHAR2,
                    I_search_string   IN     VARCHAR2,
                    I_items           IN     OBJ_VARCHAR_ID_TABLE,
                    I_dept            IN     ITEM_MASTER.DEPT%TYPE DEFAULT NULL,   --optional
                    I_supplier        IN     SUPS.SUPPLIER%TYPE DEFAULT NULL,      --optional
                    I_locations       IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL)    --optional
RETURN NUMBER;
-----------------------------------------------------------------------
FUNCTION SUPPLIER_QUERY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result          IN OUT MBL_PO_SUP_SEARCH_RESULT_TBL,
                        IO_pagination     IN OUT MBL_PAGINATION_REC,
                        I_search_string   IN     VARCHAR2,
                        I_suppliers       IN     OBJ_NUMERIC_ID_TABLE,
                        I_locations       IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL,  --optional
                        I_items           IN     OBJ_VARCHAR_ID_TABLE DEFAULT NULL)  --optional
RETURN NUMBER;
----------------------------------------------------------------------
FUNCTION LOC_QUERY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result              IN OUT MBL_PO_LOC_SEARCH_RESULT_TBL,
                   IO_pagination         IN OUT MBL_PAGINATION_REC,
                   I_include_store_ind   IN     VARCHAR2,
                   I_include_wh_ind      IN     VARCHAR2,
                   I_search_string       IN     VARCHAR2,
                   I_locations           IN     OBJ_NUMERIC_ID_TABLE,
                   I_org_unit_id         IN     ORG_UNIT.ORG_UNIT_ID%TYPE DEFAULT NULL,  --optional
                   I_supplier            IN     SUPS.SUPPLIER%TYPE DEFAULT NULL)         --optional
RETURN NUMBER;
--------------------------------------------------------------------
--------------------------------------------------------------------
FUNCTION GET_NEXT_ORDER_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_order_no         OUT ORDHEAD.ORDER_NO%TYPE)
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_PO_SVC.GET_NEXT_ORDER_NO';
   L_return_code        VARCHAR2(10) := 'FALSE';

BEGIN

   NEXT_ORDER_NUMBER(O_order_no,
                     L_return_code,
                     O_error_message);
   if L_return_code = 'FALSE' then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);   
      RETURN MBL_CONSTANTS.FAILURE;
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END GET_NEXT_ORDER_NO;
--------------------------------------------------------------------------
FUNCTION GET_SUP_TERMS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_terms            OUT MBL_PO_SUP_TERMS_TBL)
RETURN NUMBER IS
   L_program            VARCHAR2(61)   := 'MBL_PO_SVC.GET_SUP_TERMS';
   L_vdate              DATE           := get_vdate;
   L_user_language      LANG.LANG%TYPE := LANGUAGE_SQL.GET_USER_LANGUAGE;
   L_primary_language   LANG.LANG%TYPE := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   L_counter_tabel      OBJ_NUMERIC_ID_TABLE;

   cursor c_terms is
      select MBL_PO_SUP_TERMS_REC(
               t.terms,
               t.terms_code,
               t.terms_desc)
        from ( select td.terms,
                      th.terms_code,
                      th.terms_desc,
                      rank() over(partition by td.terms order by l.priority) rank
                 from terms_detail td,
                      terms_head_tl th,
                      (select lang,
                              1 priority,
                              iso_code
                         from lang
                        where lang = L_user_language
                       union
                       select lang,
                              2 priority,
                              iso_code
                         from lang
                        where lang = L_primary_language) l
                where td.terms        = th.terms
                  and th.lang         = l.lang
                  and td.enabled_flag = 'Y'
                  and (    NVL(td.start_date_active, L_vdate) <= L_vdate
                       and NVL(td.end_date_active,   L_vdate) >= L_vdate)) t
       where t.rank = 1
       order by t.terms_desc;

BEGIN

   open c_terms;
   fetch c_terms bulk collect into O_terms;
   close c_terms;
   ---
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_terms%ISOPEN then
         close c_terms;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END GET_SUP_TERMS;
--------------------------------------------------------------------------
FUNCTION ITEM_LOAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result          IN OUT MBL_PO_ITEM_SEARCH_RESULT_TBL,
                   I_items           IN     OBJ_VARCHAR_ID_TABLE,
                   I_supplier        IN     SUPS.SUPPLIER%TYPE DEFAULT NULL,    --optional
                   I_locations       IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL)  --optional
RETURN NUMBER IS
   L_program       VARCHAR2(61) := 'MBL_PO_SVC.ITEM_LOAD';
   L_pagination    MBL_PAGINATION_REC;
BEGIN

   if ITEM_QUERY(O_error_message,
                 O_result,
                 L_pagination,  
                 NULL,  --I_search_type,
                 NULL,  --I_search_string,
                 I_items,
                 NULL,  --I_dept
                 I_supplier,
                 I_locations) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if; 

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END ITEM_LOAD;
--------------------------------------------------------------------------
FUNCTION ITEM_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_result          IN OUT MBL_PO_ITEM_SEARCH_RESULT_TBL,
                     IO_pagination     IN OUT MBL_PAGINATION_REC,
                     I_search_type     IN     VARCHAR2,
                     I_search_string   IN     VARCHAR2,
                     I_dept            IN     ITEM_MASTER.DEPT%TYPE DEFAULT NULL,   --optional
                     I_supplier        IN     SUPS.SUPPLIER%TYPE DEFAULT NULL,      --optional
                     I_locations       IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL)    --optional
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_PO_SVC.ITEM_SEARCH';
BEGIN

   if ITEM_QUERY(O_error_message,
                 O_result,
                 IO_pagination,
                 I_search_type,
                 I_search_string,
                 NULL,  --I_items,
                 I_dept,
                 I_supplier,
                 I_locations) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END ITEM_SEARCH;
--------------------------------------------------------------------------
FUNCTION ITEM_QUERY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result          IN OUT MBL_PO_ITEM_SEARCH_RESULT_TBL,
                    IO_pagination     IN OUT MBL_PAGINATION_REC,
                    I_search_type     IN     VARCHAR2,
                    I_search_string   IN     VARCHAR2,
                    I_items           IN     OBJ_VARCHAR_ID_TABLE,
                    I_dept            IN     ITEM_MASTER.DEPT%TYPE DEFAULT NULL,   --optional
                    I_supplier        IN     SUPS.SUPPLIER%TYPE DEFAULT NULL,      --optional
                    I_locations       IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL)    --optional
RETURN NUMBER IS
   L_program            VARCHAR2(61)   := 'MBL_PO_SVC.ITEM_QUERY';
   L_row_start          NUMBER(10)     := 0;
   L_row_end            NUMBER(10)     := 0;
   L_num_rec_found      NUMBER(10)     := 0; 
   L_vdate              DATE           := get_vdate;
   
   L_counter_tabel      OBJ_NUMERIC_ID_TABLE;
   L_system_options_rec SYSTEM_OPTIONS%ROWTYPE;

   cursor c_item is
      select inner2.result, inner2.total_rows from (
      -----
      select MBL_PO_ITEM_SEARCH_RESULT_REC(
                inner.item,
                inner.item_desc,
                inner.dept,
                inner.supplier,
                inner.origin_country_id,
                inner.supp_pack_size,
                inner.unit_cost,
                inner.currency_code,
                DECODE(L_system_options_rec.rpm_ind,'Y',izp.standard_retail,inner.curr_selling_unit_retail) * mc.exchange_rate,  --base_unit_retail
                inner.currency_code,                     --base_retail_currency
                DECODE(L_system_options_rec.rpm_ind,'Y',izp.standard_uom,inner.curr_selling_uom),                        --base_retail_uom
                inner.image,
                CAST(MULTISET(select MBL_PO_ITEM_SEARCH_RST_LOC_REC(
                                     il.loc,
                                     il.loc_type,
                                     il.status,
                                     il.unit_retail * mc.exchange_rate,
                                     inner.currency_code,
                                     inner.standard_uom)
                                from item_loc il,
                                     (select from_currency,
                                             to_currency,
                                             effective_date,
                                             exchange_rate,
                                             rank() over
                                                 (PARTITION BY from_currency, to_currency, exchange_type
                                                      ORDER BY effective_date DESC) date_rank
                                        from mv_currency_conversion_rates
                                       where exchange_type   = 'C'
                                         and effective_date <= (L_vdate + 1)) mc,
                                     (select s.store loc, s.currency_code 
                                        from store s, 
                                             table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input
                                       where s.store = value(input)
                                      union all
                                      select w.wh loc, w.currency_code 
                                        from wh w, 
                                             table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input
                                       where w.wh = value(input)
                                     ) loc
                               where il.item = inner.item
                                 and il.loc  = loc.loc
                                 --
                                 and mc.from_currency = loc.currency_code
                                 and mc.to_currency   = inner.currency_code
                                 and mc.date_rank     = 1) as MBL_PO_ITEM_SEARCH_RST_LOC_TBL)) result,
             row_number() over (order by inner.item_desc, inner.item, inner.supplier, inner.origin_country_id) counter,
             count(*) over () total_rows
        from (select im.item,
                     im.item_desc,
                     im.dept,
                     im.standard_uom,
                     im.curr_selling_unit_retail,
                     im.curr_selling_uom,
                     isc.supplier,
                     isc.origin_country_id,
                     isc.supp_pack_size,
                     isc.unit_cost,
                     s.currency_code,
                     rz.zone_id,
                     iim.image_addr||iim.image_name image,
                     row_number() over (partition by im.item, isc.supplier, isc.origin_country_id order by im.item, iim.create_datetime) num
                from v_item_master im,
                     item_supplier isup,
                     item_supp_country isc, 
                     v_sups s,
                     rpm_merch_retail_def_expl def,
                     rpm_zone rz,
                     deps d,
                     item_image iim
               where (((I_search_type = MBL_CONSTANTS.ITEM_SEARCH_TYPE_ITEM and
                        (lower(im.item)      like(lower('%'||nvl(I_search_string,'-90909')||'%')) or
                         lower(im.item_desc) like(lower('%'||nvl(I_search_string,'-90909')||'%'))))
                       or
                       (I_search_type = MBL_CONSTANTS.ITEM_SEARCH_TYPE_VPN and
                        (lower(isup.vpn) like(lower('%'||nvl(I_search_string,'-90909')||'%')))))
                      or
                      (I_search_type IS NULL and
                       im.item in (select value(input) from table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) input)))
                 and im.item_level           = im.tran_level
                 and im.status               = 'A'
                 and im.orderable_ind        = 'Y'
                 and (   NVL(im.pack_type,'-999')        != 'B'
                      or (    im.pack_type                = 'B'
                          and NVL(im.order_as_type, 'P') != 'E'))
                 and im.item                 = isup.item
                 and isup.supplier           = nvl(I_supplier, isup.supplier)
                 and isup.supplier           = isc.supplier
                 and isup.item               = isc.item
                 and ((    I_supplier              is NULL
                       and isc.primary_supp_ind    = 'Y'
                       and isc.primary_country_ind = 'Y')
                      or
                      (    I_supplier              is not NULL
                       and isc.primary_country_ind = 'Y'))
                 and isc.supplier            = s.supplier
                 --
                 and im.dept                 = def.dept
                 and im.class                = def.class
                 and im.subclass             = def.subclass
                 and def.regular_zone_group  = rz.zone_group_id
                 and rz.base_ind             = 1
                 --
                 and im.dept                 = d.dept
                 and d.purchase_type         = 0 --Normal Merchandise
                 --
                 and im.item                 = iim.item(+)
                 --
                 and im.dept                 = CASE 
                                                  WHEN I_dept is NULL     THEN im.dept
                                                  WHEN I_dept is NOT NULL THEN
                                                     CASE 
                                                        WHEN L_system_options_rec.dept_level_orders = 'N' THEN im.dept
                                                        WHEN L_system_options_rec.dept_level_orders = 'Y' 
                                                         AND im.pack_ind                            = 'N' THEN I_dept
                                                        WHEN L_system_options_rec.dept_level_orders = 'Y' 
                                                         AND im.pack_ind                            = 'Y' THEN 
                                                           CASE
                                                              WHEN NOT EXISTS (select 'X'
                                                                                 from v_packsku_qty vpq,
                                                                                      item_master comp_im
                                                                                where vpq.pack_no   = im.item
                                                                                  and vpq.item      = comp_im.item
                                                                                  and comp_im.dept != I_dept) THEN I_dept
                                                           ELSE MBL_CONSTANTS.DUMMY_NUMBER END --end case pack comp
                                                        ELSE MBL_CONSTANTS.DUMMY_NUMBER    END --end case dept_level_orders
                                                  ELSE MBL_CONSTANTS.DUMMY_NUMBER          END --end case I_dept
                 --
                 and not exists (select 'X'
                                   from daily_purge dp
                                  where dp.key_value = im.item
                                    and dp.table_name = 'ITEM_MASTER')) inner,
             (select from_currency,
                     to_currency,
                     effective_date,
                     exchange_rate,
                     rank() over
                         (PARTITION BY from_currency, to_currency, exchange_type
                              ORDER BY effective_date DESC) date_rank
                from mv_currency_conversion_rates
               where exchange_type   = 'C'
                 and effective_date <= (L_vdate + 1)) mc,
             rpm_item_zone_price izp
      where inner.num     = 1
        and inner.item    = izp.item(+)
        and inner.zone_id = izp.zone_id(+)
        --
        and mc.from_currency          = nvl(izp.standard_retail_currency, inner.currency_code)
        and mc.to_currency            = inner.currency_code
        and mc.date_rank              = 1

      ) inner2
      -----
      where inner2.counter    >= L_row_start
        and inner2.counter     < L_row_end
      order by result.item_desc;

BEGIN

   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;
   --
   open c_item;
   fetch c_item bulk collect into O_result, L_counter_tabel;
   close c_item;
   --
   if L_counter_tabel       is NULL or 
      L_counter_tabel.count <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_tabel(L_counter_tabel.first);
   end if;
   --
   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   --

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      if c_item%ISOPEN then
         close c_item;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
   when OTHERS then
      if c_item%ISOPEN then
         close c_item;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END ITEM_QUERY;
--------------------------------------------------------------------------
FUNCTION SUPPLIER_LOAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_result          IN OUT MBL_PO_SUP_SEARCH_RESULT_TBL,
                       I_suppliers       IN     OBJ_NUMERIC_ID_TABLE,
                       I_locations       IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL,  --optional
                       I_items           IN     OBJ_VARCHAR_ID_TABLE DEFAULT NULL)  --optional
RETURN NUMBER IS
   L_program    VARCHAR2(61) := 'MBL_PO_SVC.SUPPLIER_LOAD';
   L_pagination MBL_PAGINATION_REC;
   
BEGIN

   if SUPPLIER_QUERY(O_error_message,
                     O_result,
                     L_pagination,
                     NULL,  --I_search_string,
                     I_suppliers,
                     I_locations,
                     I_items) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END SUPPLIER_LOAD;
--------------------------------------------------------------------------
FUNCTION SUPPLIER_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result          IN OUT MBL_PO_SUP_SEARCH_RESULT_TBL,
                         IO_pagination     IN OUT MBL_PAGINATION_REC,
                         I_search_string   IN     VARCHAR2,
                         I_locations       IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL,  --optional
                         I_items           IN     OBJ_VARCHAR_ID_TABLE DEFAULT NULL)  --optional
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_PO_SVC.SUPPLIER_SEARCH';
BEGIN

   if SUPPLIER_QUERY(O_error_message,
                     O_result,
                     IO_pagination,
                     I_search_string,
                     NULL,  --I_suppliers,
                     I_locations,
                     I_items) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END SUPPLIER_SEARCH;
--------------------------------------------------------------------------
FUNCTION SUPPLIER_QUERY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result          IN OUT MBL_PO_SUP_SEARCH_RESULT_TBL,
                        IO_pagination     IN OUT MBL_PAGINATION_REC,
                        I_search_string   IN     VARCHAR2,
                        I_suppliers       IN     OBJ_NUMERIC_ID_TABLE,
                        I_locations       IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL,  --optional
                        I_items           IN     OBJ_VARCHAR_ID_TABLE DEFAULT NULL)  --optional
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_PO_SVC.SUPPLIER_QUERY';
   L_item_count         number(20)   := 0;
   L_row_start          NUMBER(10)   := 0;
   L_row_end            NUMBER(10)   := 0;
   L_num_rec_found      NUMBER(10)   := 0; 
   L_org_unit_id        ORG_UNIT.ORG_UNIT_ID%TYPE := NULL;
   
   L_system_options_rec SYSTEM_OPTIONS%ROWTYPE;
   L_counter_tabel      OBJ_NUMERIC_ID_TABLE;
   
   cursor C_GET_LOC_ORG_UNIT is
      select s.org_unit_id
        from store s,
             table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input_loc
       where s.store = value(input_loc)
         and rownum = 1
      union
      select wh.org_unit_id
        from wh,
             table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input_loc
       where wh.wh = value(input_loc)
         and rownum = 1;


   cursor c_supplier is
      select MBL_PO_SUP_SEARCH_RESULT_REC(
                inner.supplier,
                inner.sup_name,
                inner.currency_code,
                inner.terms,
                inner.default_item_lead_time,
                CAST(MULTISET(select MBL_PO_SUP_ITEM_RESULT_REC(
                                     isc.item,
                                     isc.origin_country_id,
                                     isc.lead_time)
                                from item_supp_country isc,
                                     table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) input
                               where isc.item     = value(input)
                                 and isc.supplier = inner.supplier) as MBL_PO_SUP_ITEM_RESULT_TBL),
                CAST(MULTISET(select MBL_PO_SUP_ITEM_LOC_RESULT_REC(
                                     iscl.item,
                                     iscl.loc,
                                     iscl.pickup_lead_time)
                                from item_supp_country_loc iscl,
                                     table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) input_item,
                                     table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input_loc
                               where iscl.item     = value(input_item)
                                 and iscl.loc      = value(input_loc)
                                 and iscl.supplier = inner.supplier) as MBL_PO_SUP_ITEM_LOC_RESULT_TBL)),
             inner.total_rows
        from (
      ----
      select s.supplier,
             s.sup_name,
             s.currency_code,
             s.terms,
             s.default_item_lead_time,
             row_number() over (order by s.sup_name, s.supplier) counter,
             count(*) over () total_rows
        from v_sups s,
             (select isup.supplier,
                     rank() OVER (PARTITION BY isup.supplier
                                      ORDER BY isup.item) val_supp
                       from item_supplier isup,
                            table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) input
                      where isup.item     = value(input)
                        and L_item_count  > 0
                     union all 
                     select supplier, 
                            L_item_count val_supp 
                       from sups 
                      where L_item_count = 0) item_supp
       where ((lower(s.supplier) like(lower('%'||nvl(I_search_string,'-90909')||'%')) or
               lower(s.sup_name) like(lower('%'||nvl(I_search_string,'-90909')||'%')))
              or
              (s.supplier in (select value(input) from table(cast(I_suppliers as OBJ_NUMERIC_ID_TABLE)) input)))
         and (   (    L_system_options_rec.supplier_sites_ind = 'Y'
                  and s.supplier_parent is NOT NULL) --bring only supplier sites
              or L_system_options_rec.supplier_sites_ind = 'N')
         and s.sup_status       = 'A'
         --
         and s.supplier         = item_supp.supplier 
         and item_supp.val_supp = L_item_count
         --
         and (   L_system_options_rec.org_unit_ind = 'N'
              or (    L_system_options_rec.org_unit_ind = 'Y'
                  and exists (select 'x'
                                from partner_org_unit p
                               where p.partner_type in ('S','U')
                                 and p.partner      = s.supplier
                                 and p.org_unit_id  = NVL(L_org_unit_id,p.org_unit_id))))) inner
      ----
      where inner.counter    >= L_row_start
        and inner.counter     < L_row_end
      order by inner.sup_name;

BEGIN

   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   --
   if I_items is not NULL then
      if I_items.count > 0 then
        L_item_count := I_items.count;
      end if;
   end if;
   --
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;
   --
   if L_system_options_rec.org_unit_ind = 'Y' and
      (I_locations is NOT NULL and I_locations.count > 0) then
      open C_GET_LOC_ORG_UNIT;
      fetch C_GET_LOC_ORG_UNIT into L_org_unit_id;
      close C_GET_LOC_ORG_UNIT;
   end if;
   --
   open c_supplier;
   fetch c_supplier bulk collect into O_result, L_counter_tabel;
   close c_supplier;
   --
   if L_counter_tabel       is NULL or 
      L_counter_tabel.count <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_tabel(L_counter_tabel.first);
   end if;
   --
   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   --

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      if C_GET_LOC_ORG_UNIT%ISOPEN then
         close C_GET_LOC_ORG_UNIT;
      end if;
      if c_supplier%ISOPEN then
         close c_supplier;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);   
      RETURN MBL_CONSTANTS.FAILURE;
   when OTHERS then
      if C_GET_LOC_ORG_UNIT%ISOPEN then
         close C_GET_LOC_ORG_UNIT;
      end if;
      if c_supplier%ISOPEN then
         close c_supplier;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END SUPPLIER_QUERY;
--------------------------------------------------------------------------
FUNCTION LOC_LOAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_result          IN OUT MBL_PO_LOC_SEARCH_RESULT_TBL,
                  I_locations       IN     OBJ_NUMERIC_ID_TABLE,
                  I_supplier        IN     SUPS.SUPPLIER%TYPE DEFAULT NULL)     --optional
RETURN NUMBER IS
   L_program    VARCHAR2(61) := 'MBL_PO_SVC.LOC_LOAD';
   L_pagination MBL_PAGINATION_REC;
   
BEGIN

   if LOC_QUERY(O_error_message,
                O_result,
                L_pagination,
                'Y',   --I_include_store_ind,
                'Y',   --I_include_wh_ind,
                NULL,  --I_search_string
                I_locations,
                NULL,  --I_org_unit_id
                I_supplier) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END LOC_LOAD;
--------------------------------------------------------------------------
FUNCTION LOC_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result          IN OUT MBL_PO_LOC_SEARCH_RESULT_TBL,
                    IO_pagination     IN OUT MBL_PAGINATION_REC,
                    I_loc_type        IN     VARCHAR2,
                    I_search_string   IN     VARCHAR2,
                    I_org_unit_id     IN     ORG_UNIT.ORG_UNIT_ID%TYPE DEFAULT NULL, --optional
                    I_supplier        IN     SUPS.SUPPLIER%TYPE DEFAULT NULL)        --optional
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_PO_SVC.LOC_SEARCH';
   L_include_store_ind  varchar2(2)  := 'N';
   L_include_wh_ind     varchar2(2)  := 'N';
BEGIN

   if I_loc_type = 'S' then
      L_include_store_ind  := 'Y';
   end if;
   if I_loc_type = 'W' then
      L_include_wh_ind     := 'Y';
   end if;
   if I_loc_type = 'A' then
      L_include_store_ind  := 'Y';
      L_include_wh_ind     := 'Y';
   end if;

   if LOC_QUERY(O_error_message,
                O_result,
                IO_pagination,
                L_include_store_ind,
                L_include_wh_ind,
                I_search_string,
                NULL,  --I_locations,
                I_org_unit_id,
                I_supplier) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END LOC_SEARCH;
--------------------------------------------------------------------------
FUNCTION LOC_QUERY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result              IN OUT MBL_PO_LOC_SEARCH_RESULT_TBL,
                   IO_pagination         IN OUT MBL_PAGINATION_REC,
                   I_include_store_ind   IN     VARCHAR2,
                   I_include_wh_ind      IN     VARCHAR2,
                   I_search_string       IN     VARCHAR2,
                   I_locations           IN     OBJ_NUMERIC_ID_TABLE,
                   I_org_unit_id         IN     ORG_UNIT.ORG_UNIT_ID%TYPE DEFAULT NULL, --optional
                   I_supplier            IN     SUPS.SUPPLIER%TYPE DEFAULT NULL)        --optional

RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_PO_SVC.LOC_QUERY';
   L_row_start          NUMBER(10)   := 0;
   L_row_end            NUMBER(10)   := 0;
   L_num_rec_found      NUMBER(10)   := 0; 
   L_vdate              DATE         := get_vdate;
   L_org_unit_id        ORG_UNIT.ORG_UNIT_ID%TYPE := I_org_unit_id;
   
   L_system_options_rec SYSTEM_OPTIONS%ROWTYPE;
   L_counter_tabel      OBJ_NUMERIC_ID_TABLE;

   cursor C_GET_SUPP_ORG_UNIT is
      select p.org_unit_id
        from partner_org_unit p
       where p.partner      = I_supplier
         and p.partner_type in ('S','U');

   cursor c_loc is
      select MBL_PO_LOC_SEARCH_RESULT_REC(inner.location,
                                       inner.loc_type,
                                       inner.loc_name,
                                       inner.location_currency,
                                       inner.org_unit_id),
             inner.total_rows
        from (
      ----
      select i.location,
             i.loc_type,
             i.loc_name,
             i.location_currency,
             i.org_unit_id,
             row_number() over (order by i.loc_name, i.location) counter,
             count(*) over () total_rows
        from (select distinct s.store location,
                     'S' loc_type,
                     s.store_name loc_name, --v_store translate the name
                     s.currency_code location_currency,
                     s.org_unit_id
                from v_store s
               where ((s.store             like('%'||nvl(I_search_string,'-90909')||'%') or
                       lower(s.store_name) like('%'||lower(nvl(I_search_string,'-90909'))||'%'))
                      or
                      (s.store in (select value(input) from table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input)))
                 and I_include_store_ind = 'Y'
                 and s.stockholding_ind  = 'Y'
                 and s.store_type        = 'C'
                 and NVL(s.store_close_date, to_date('29991231','YYYYMMDD')) - NVL(s.stop_order_days,0) > L_vdate
              union all
              select distinct w.wh location,
                     'W' loc_type,
                     w.wh_name loc_name,
                     w.currency_code location_currency,
                     w.org_unit_id
                from v_wh w,
                     wh
               where ((w.wh             like('%'||nvl(I_search_string,'-90909')||'%') or
                       lower(w.wh_name) like('%'||lower(nvl(I_search_string,'-90909'))||'%'))
                      or
                      (w.wh in (select value(input) from table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input))
                     )
                 and I_include_wh_ind   = 'Y'
                 and w.stockholding_ind = 'Y'
                 and w.wh               = wh.wh
                 and wh.finisher_ind    = 'N') i
       where NVL(i.org_unit_id,MBL_CONSTANTS.DUMMY_NUMBER) = DECODE(L_system_options_rec.org_unit_ind, 'Y',
                                                                                NVL(L_org_unit_id,NVL(i.org_unit_id,MBL_CONSTANTS.DUMMY_NUMBER)), 
                                                                                NVL(i.org_unit_id,MBL_CONSTANTS.DUMMY_NUMBER))) inner
      ----
       where inner.counter    >= L_row_start
         and inner.counter     < L_row_end
       order by inner.loc_name;

BEGIN

   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;
   --
   if L_system_options_rec.org_unit_ind = 'Y' and
      I_supplier is NOT NULL then
      open C_GET_SUPP_ORG_UNIT;
      fetch C_GET_SUPP_ORG_UNIT into L_org_unit_id;
      close C_GET_SUPP_ORG_UNIT;
   end if;
   --
   open c_loc;
   fetch c_loc bulk collect into O_result, L_counter_tabel;
   close c_loc;
   --
   if L_counter_tabel       is NULL or 
      L_counter_tabel.count <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_tabel(L_counter_tabel.first);
   end if;
   --
   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   --
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      if C_GET_SUPP_ORG_UNIT%ISOPEN then
         close C_GET_SUPP_ORG_UNIT;
      end if;
      if c_loc%ISOPEN then
         close c_loc;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);   
      RETURN MBL_CONSTANTS.FAILURE;
   when OTHERS then
      if C_GET_SUPP_ORG_UNIT%ISOPEN then
         close C_GET_SUPP_ORG_UNIT;
      end if;
      if c_loc%ISOPEN then
         close c_loc;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END LOC_QUERY;
--------------------------------------------------------------------------
FUNCTION CREATE_PO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_order_object    IN     MBL_PO_XORDERDESC_REC)
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_PO_SVC.CREATE_PO';
   L_status_code        VARCHAR2(10) := NULL;
   L_order_object       "RIB_XOrderDesc_REC";
   
   cursor C_CONV_TO_RIB is
      select "RIB_XOrderDesc_REC"( 0  --rib_oid number
                                 , I_order_object.order_no
                                 , I_order_object.supplier 
                                 , I_order_object.currency_code 
                                 , I_order_object.terms 
                                 , I_order_object.not_before_date 
                                 , I_order_object.not_after_date 
                                 , I_order_object.otb_eow_date 
                                 , I_order_object.dept 
                                 , I_order_object.status 
                                 , I_order_object.exchange_rate 
                                 , I_order_object.include_on_ord_ind 
                                 , I_order_object.written_date
                                 , CAST(MULTISET(select "RIB_XOrderDtl_REC"( 0 --rib_oid number
                                                                           , item
                                                                           , location
                                                                           , unit_cost
                                                                           , ref_item
                                                                           , origin_country_id
                                                                           , supp_pack_size
                                                                           , qty_ordered
                                                                           , location_type
                                                                           , cancel_ind
                                                                           , reinstate_ind)
                                                   from table(cast(I_order_object.XOrderDtl_tbl as MBL_PO_XORDERDTL_TBL)) input) as "RIB_XOrderDtl_TBL") --XOrderDtl_TBL
                                 , I_order_object.orig_ind 
                                 , I_order_object.edi_po_ind 
                                 , I_order_object.pre_mark_ind 
                                 , I_order_object.user_id 
                                 , I_order_object.comment_desc)
        from dual;
   
BEGIN

   open C_CONV_TO_RIB;
   fetch C_CONV_TO_RIB into L_order_object;
   close C_CONV_TO_RIB;
   ---
   RMSSUB_XORDER.CONSUME(L_status_code,
                         O_error_message,
                         L_order_object,
                         RMSSUB_XORDER.LP_cre_type);
   --                   
   if L_status_code != API_CODES.SUCCESS then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   ---
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_CONV_TO_RIB%ISOPEN then
         close C_CONV_TO_RIB;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END CREATE_PO;
--------------------------------------------------------------------------
--------------------------------------------------------------------------
END MBL_PO_SVC;
/
