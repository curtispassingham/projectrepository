CREATE OR REPLACE PACKAGE BODY MBL_RPO_SVC AS
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
FUNCTION GET_OTB_TBL(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_result          IN OUT MBL_RPO_ORDER_SUM_OTB_TBL,
                     I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_DISPLAY_LEVEL(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_items_disply_level   OUT    VARCHAR2,
                                   I_order_no             IN     ORDHEAD.ORDER_NO%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
FUNCTION COPY_PO_STATUS_REC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_result           OUT MBL_RPO_STATUS_REC,
                            I_result        IN     RMS_OI_PO_STATUS_REC,
                            I_return        IN     NUMBER,
                            I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
FUNCTION PO_SEARCH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result          IN OUT MBL_RPO_SEARCH_RES_TBL,
                   IO_pagination     IN OUT MBL_PAGINATION_REC,
                   I_search_record   IN     MBL_RPO_USER_CRITERIA_REC)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'MBL_RPO_SVC.PO_SEARCH';

   L_row_start          NUMBER(10)     := 0;
   L_row_end            NUMBER(10)     := 0;
   L_num_rec_found      NUMBER(10)     := 0;
   L_counter_table      OBJ_NUMERIC_ID_TABLE; 

   L_all_suppliers      VARCHAR2(1) := null;  
   L_all_departments    VARCHAR2(1) := null;
   L_all_create_id      VARCHAR2(1) := null;
   
   L_statuses           OBJ_VARCHAR_ID_TABLE;
   L_origin_codes       OBJ_NUMERIC_ID_TABLE; 

   cursor c_order_search is
      select /*+ parallel(i2,2) */ MBL_RPO_SEARCH_RES_REC(i2.order_no,
                                    i2.status,
                                    i2.supplier,
                                    i2.sup_name,
                                    i2.not_before_date,
                                    i2.not_after_date,
                                    (select sum(nvl(ol.unit_cost,0) * nvl(ol.qty_ordered,0)) from ordloc ol where ol.order_no = i2.order_no),
                                    i2.currency_code,
                                    i2.previously_approved_ind,
                                    i2.editable_ind),
             i2.total_rows                      
                      
                 from (select oh.order_no,
                              oh.status,
                              oh.supplier,
                              su.sup_name,
                              oh.not_before_date,
                              oh.not_after_date,                     
                              oh.currency_code,
                              oh.written_date, 
                              CASE WHEN oh.orig_approval_date is not NULL and 
                                        oh.status != 'A' THEN 'Y'
                              ELSE                            'N' 
                              END previously_approved_ind,
                              CASE WHEN oh.wf_order_no is not NULL THEN 'N'
                                   WHEN oh.order_type = 'CO' and
                                        pco.oms_ind   = 'Y'        THEN 'N'
                              ELSE                                      'Y' 
                              END editable_ind,
                              row_number() over (order by oh.written_date desc) counter,
                              count(*) over () total_rows                             
                        from table(cast(L_statuses as OBJ_VARCHAR_ID_TABLE)) input_statuses,
                             table(cast(L_origin_codes as OBJ_NUMERIC_ID_TABLE)) input_origin_codes,                              
                             v_ordhead oh,
                             v_sups  su,
                             product_config_options pco
                       where oh.status             = value(input_statuses)                         
                         and oh.orig_ind           = value(input_origin_codes)
                         and oh.supplier           = su.supplier
                         and (I_search_record.order_no is NULL or oh.order_no like('%'||I_search_record.order_no||'%') )                         

                         --                         
                         and oh.written_date >= NVL(I_search_record.start_create_date, oh.written_date)
                         and oh.written_date <= NVL(I_search_record.end_create_date, oh.written_date)
                         and (L_all_create_id = MBL_CONSTANTS.YES_IND or oh.create_id in (select value(input_create_id)
                                                                                            from table(cast(I_search_record.create_id as OBJ_VARCHAR_ID_TABLE)) input_create_id))                                         
                         -- 
                         and (L_all_suppliers = MBL_CONSTANTS.YES_IND or exists (select 1 
                                                                                   from table(cast(I_search_record.suppliers as OBJ_NUMERIC_ID_TABLE)) input_sups
                                                                                  where oh.supplier = value(input_sups)))                                       
                         --
                         and (L_all_departments = MBL_CONSTANTS.YES_IND or exists ( select 1 
                                                                                      from v_ordsku os,
                                                                                           item_master im 
                                                                                     where os.order_no = oh.order_no 
                                                                                       and os.item=im.item  
                                                                                       and im.dept in ( select value(input_departments) 
                                                                                                          from table(cast(I_search_record.departments as OBJ_NUMERIC_ID_TABLE)) input_departments)))
                         order by oh.written_date desc,oh.order_no                                                                                 
                         --
 
                      ) i2 
      where i2.counter    >= L_row_start
        and i2.counter    < L_row_end;

BEGIN

   if I_search_record.suppliers is not NULL and I_search_record.suppliers.count > 0 then
      L_all_suppliers := MBL_CONSTANTS.NO_IND;
   else
      L_all_suppliers := MBL_CONSTANTS.YES_IND;
   end if;
   ---
   if I_search_record.departments is not NULL and I_search_record.departments.count > 0 then
      L_all_departments := MBL_CONSTANTS.NO_IND;
   else
      L_all_departments := MBL_CONSTANTS.YES_IND;
   end if;
   ---
   if I_search_record.create_id is not NULL and I_search_record.create_id.count > 0 then
      L_all_create_id := MBL_CONSTANTS.NO_IND;
   else
      L_all_create_id := MBL_CONSTANTS.YES_IND;
   end if;
   ---
   if I_search_record.statuses is not NULL and I_search_record.statuses.count > 0 then
      L_statuses := I_search_record.statuses;
   else
      L_statuses := OBJ_VARCHAR_ID_TABLE('W', 'S', 'A');
   end if;

   if I_search_record.origin_codes is not NULL and I_search_record.origin_codes.count > 0 then
      L_origin_codes := I_search_record.origin_codes;
   else
      select to_number(code) bulk collect into L_origin_codes from code_detail where code_type = 'OROR';
   end if;
   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   open c_order_search;
   fetch c_order_search bulk collect into O_result, L_counter_table;
   close c_order_search;

   if L_counter_table is NULL or L_counter_table.count <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_table(L_counter_table.first);
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION    
   when OTHERS then
      if c_order_search%ISOPEN then
         close c_order_search;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program); 
      RETURN MBL_CONSTANTS.FAILURE;
END PO_SEARCH;
--------------------------------------------------------------------------------
FUNCTION GET_ORIG_CODE_LIST(O_error_message   IN  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_result          OUT MBL_CODE_DETAIL_TBL)

RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_RPO_SVC.GET_ORIG_CODE_LIST';

BEGIN   

   if MBL_COMMON_SVC.GET_CODE_DETAIL(O_error_message,
                                     'OROR',
                                     O_result) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;   
   
   RETURN MBL_CONSTANTS.SUCCESS;
EXCEPTION      
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE; 
END GET_ORIG_CODE_LIST;
--------------------------------------------------------------------------------
FUNCTION GET_PO_STATUS_LIST(O_error_message   IN  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_result          OUT MBL_CODE_DETAIL_TBL)

RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_RPO_SVC.GET_PO_STATUS_LIST';

BEGIN   

   if MBL_COMMON_SVC.GET_CODE_DETAIL(O_error_message,
                                     'ORST',
                                     O_result) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if; 
   
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION   
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE; 
END GET_PO_STATUS_LIST;
--------------------------------------------------------------------------------
FUNCTION GET_PO_SUMMARY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_result          IN OUT MBL_RPO_ORDER_SUM_REC,                        
                        I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
RETURN NUMBER IS

   L_program              VARCHAR2(61) := 'MBL_RPO_SVC.GET_PO_SUMMARY';

   L_vdate                DATE           := GET_VDATE; 
   L_otb_tbl              MBL_RPO_ORDER_SUM_OTB_TBL := MBL_RPO_ORDER_SUM_OTB_TBL();
   L_items_disply_level   VARCHAR2(20) ;
   L_otb_system_ind       SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE;       
   
   cursor c_order is
      select MBL_RPO_ORDER_SUM_REC(i.order_no,
                                   i.status,
                                   i.supplier,
                                   i.sup_name,
                                   i.not_before_date,
                                   i.not_after_date,
                                   i.otb_eow_date,
                                   i.terms,                               
                                   i.terms_code,
                                   i.terms_desc,                                
                                   i.total_cost,
                                   i.total_retail,
                                   i.currency_code,
                                   i.create_id,
                                   i.written_date, 
                                   NULL, --default_display_level
                                   i.previously_approved_ind,
                                   i.editable_ind,
                                   NULL  --otb_tbl
                                  ) 
        from (select distinct 
                     oh.order_no,
                     oh.status,
                     oh.supplier,
                     oh.sup_name,
                     oh.not_before_date,
                     oh.not_after_date,
                     oh.otb_eow_date,
                     oh.terms,
                     t.terms_code,
                     t.terms_desc,
                     --
                     oc.total_cost,
                     sum(oc.total_retail*mc.exchange_rate) over (partition by oh.order_no) total_retail,
                     oh.currency_code,
                     oh.create_id,
                     oh.written_date,
                     oh.previously_approved_ind,
                     oh.editable_ind
                from (select oh1.order_no,
                             oh1.status,
                             oh1.supplier supplier,
                             su.sup_name sup_name,
                             oh1.not_before_date,
                             oh1.not_after_date,
                             oh1.otb_eow_date,
                             oh1.terms,
                             oh1.create_id,
                             oh1.written_date,
                             oh1.currency_code,
                             CASE WHEN oh1.orig_approval_date is not NULL and 
                                       oh1.status != 'A' THEN 'Y'
                              ELSE                            'N' 
                              END previously_approved_ind,
                              CASE WHEN oh1.wf_order_no is not NULL THEN 'N'
                                   WHEN oh1.order_type = 'CO' and
                                        pco.oms_ind    = 'Y'        THEN 'N'
                              ELSE                                       'Y' 
                              END editable_ind
                       from  v_ordhead oh1,
                             v_sups su,
                             product_config_options pco
                       where oh1.order_no = I_order_no
                         and oh1.supplier = su.supplier
                      )oh,                     
                     V_TERMS_HEAD_TL t,
                     (select distinct
                             oloc.order_no,
                             sum(oloc.unit_cost * oloc.qty_ordered) 
                                over (partition by oloc.order_no) total_cost,
                             sum(oloc.unit_retail * oloc.qty_ordered) 
                                over (partition by oloc.order_no, l.currency_code) total_retail,
                             l.currency_code
                        from ordloc oloc,
                             (select store loc, currency_code from store
                              union all
                              select wh loc, currency_code from wh) l
                       where oloc.order_no = I_order_no
                         and oloc.location = l.loc) oc,
                     (select from_currency,
                              to_currency,
                              effective_date,
                              exchange_rate,
                              rank() over
                                  (PARTITION BY from_currency, to_currency, exchange_type
                                       ORDER BY effective_date DESC) date_rank
                         from mv_currency_conversion_rates
                        where exchange_type   = 'C'
                          and effective_date <= (L_vdate + 1)) mc
               where oh.terms         = t.terms
                 and oh.order_no      = oc.order_no(+)
                 --
                 and mc.from_currency = NVL(oc.currency_code, oh.currency_code)
                 and mc.to_currency   = oh.currency_code
                 and mc.date_rank     = 1) i;


BEGIN

   open c_order;
   fetch c_order into O_result;
   if c_order%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_ORDER_NO',NULL,NULL,NULL);
      close c_order;
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;
   close c_order;
   -- 
   if GET_DEFAULT_DISPLAY_LEVEL(O_error_message,
                                L_items_disply_level,
                                I_order_no) = MBL_CONSTANTS.FAILURE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if; 
   O_result.default_display_level := L_items_disply_level;   
   --
   if SYSTEM_OPTIONS_SQL.GET_OTB_SYSTEM_IND(O_error_message,
                                            L_otb_system_ind) = FALSE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if; 
   --
   if L_otb_system_ind = 'Y' then
      if GET_OTB_TBL(O_error_message,
                     L_otb_tbl,
                     I_order_no) = MBL_CONSTANTS.FAILURE then
         RAISE MBL_CONSTANTS.PROGRAM_ERROR;
      end if;
   end if;   
   -- 
   O_result.otb_table := L_otb_tbl;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      if c_order%ISOPEN then
         close c_order;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
   
   when OTHERS then
      if c_order%ISOPEN then
         close c_order;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_PO_SUMMARY;
---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_OTB_TBL(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_result          IN OUT MBL_RPO_ORDER_SUM_OTB_TBL,
                     I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
RETURN NUMBER IS

   L_program              VARCHAR2(61) := 'MBL_RPO_SVC.GET_OTB_TBL';

   L_vdate                DATE := get_vdate;
   L_prim_currency        SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;
   L_ord_currency         ORDHEAD.CURRENCY_CODE%TYPE;
   L_elc_ind              SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_default_tax_type     SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;

   L_otb_eow_date         DATE;
   L_eow_date             DATE;
   L_period_ahead         NUMBER(10);   

   L_retail_tbl           MBL_RPO_ORDER_SUM_OTB_TBL;

   L_order_total          NUMBER(20,4) := 0;
   L_budget_amount        NUMBER(20,4);
   L_approved_amount      NUMBER(20,4);
   L_received_amount      NUMBER(20,4);
   L_outstanding_amount   NUMBER(20,4);

   L_otb_rec              MBL_RPO_ORDER_SUM_OTB_REC;
   L_nonpack_cost_tbl     MBL_RPO_ORDER_SUM_OTB_TBL := MBL_RPO_ORDER_SUM_OTB_TBL();
   L_pack_cost_tbl        MBL_RPO_ORDER_SUM_OTB_TBL := MBL_RPO_ORDER_SUM_OTB_TBL();

   L_dept                 subclass.dept%TYPE;
   L_class                subclass.class%TYPE;
   L_subclass             subclass.subclass%TYPE;
   L_subclass_name        VARCHAR2(120);
   L_pack_no              item_master.item%TYPE;
   L_pack_cost            item_supp_country.unit_cost%TYPE;

   L_total_elc            NUMBER;
   L_total_exp            NUMBER;
   L_exp_currency         CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate_exp    CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_total_dty            NUMBER;
   L_dty_currency         CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate        ORDHEAD.EXCHANGE_RATE%TYPE;
   

   cursor c_retail is
     with
     loc as
     (select s.store loc, 
             s.currency_code 
        from store s
      union all
      select w.wh loc, 
             w.currency_code 
        from wh w),
     mc as
     (select from_currency,
             to_currency,
             effective_date,
             exchange_rate,
             rank() over
             (PARTITION BY from_currency, to_currency, exchange_type
                  ORDER BY effective_date DESC) date_rank
        from mv_currency_conversion_rates
       where exchange_type   = 'C'
         and effective_date <= (L_vdate + 1)
     )      
     select MBL_RPO_ORDER_SUM_OTB_REC(
             i.dept,
             i.class,
             i.subclass,
             s.sub_name,
             i.subclass_retail,
             decode(i.order_type, 
                    MBL_CONSTANTS.ORDER_TYPE_ARB, a_budget_amt ,
                    MBL_CONSTANTS.ORDER_TYPE_BRB, b_budget_amt ,
                    MBL_CONSTANTS.ORDER_TYPE_NB, n_budget_amt  ,
                    0),                   --budget_amount
             decode(i.order_type, 
                    MBL_CONSTANTS.ORDER_TYPE_ARB, a_receipts_amt ,
                    MBL_CONSTANTS.ORDER_TYPE_BRB, b_receipts_amt ,
                    MBL_CONSTANTS.ORDER_TYPE_NB, n_receipts_amt  ,
                    0),                   --receipts_amount          
             decode(i.order_type, 
                    MBL_CONSTANTS.ORDER_TYPE_ARB, a_approved_amt ,
                    MBL_CONSTANTS.ORDER_TYPE_BRB, b_approved_amt ,
                    MBL_CONSTANTS.ORDER_TYPE_NB, n_approved_amt ,
                    0),                    --approved_amount
             decode(i.order_type, 
                    MBL_CONSTANTS.ORDER_TYPE_ARB, a_approved_amt - a_receipts_amt ,
                    MBL_CONSTANTS.ORDER_TYPE_BRB, b_approved_amt - b_receipts_amt,
                    MBL_CONSTANTS.ORDER_TYPE_NB, n_approved_amt - n_receipts_amt,
                    0)                     --outstanding_amount
             )
        from (select inner.order_type,
                     inner.otb_eow_date,
                     inner.dept,
                     inner.class,
                     inner.subclass,
                     sum(inner.subclass_retail) subclass_retail
                from (--Non Pack Retail
                      select oh.order_type,
                             oh.otb_eow_date,
                             im.dept,
                             im.class,
                             im.subclass,
                             sum(ol.unit_retail * (ol.qty_ordered - NVL(ol.qty_received,0)) * mc.exchange_rate) subclass_retail
                        from ordhead oh,
                             ordloc ol,
                             item_master im,
                             deps d,                             
                             loc,                             
                             mc
                       where oh.order_no     = I_order_no
                         and oh.order_no     = ol.order_no
                         and ol.item         = im.item
                         and im.pack_ind     = MBL_CONSTANTS.NO_IND
                         and im.dept         = d.dept
                         and d.otb_calc_type = 'R'
                         --
                         and ol.location     = loc.loc
                         --
                         and mc.from_currency = loc.currency_code
                         and mc.to_currency   = L_ord_currency
                         and mc.date_rank     = 1
                       group by oh.order_type,
                                oh.otb_eow_date,
                                im.dept,
                                im.class,
                                im.subclass
                      UNION ALL
                      --Pack Retail
                      select oh.order_type,
                             oh.otb_eow_date,
                             im.dept,
                             im.class,
                             im.subclass,
                             sum(il.unit_retail * (vpq.qty * (ol.qty_ordered - NVL(ol.qty_received,0))) * mc.exchange_rate) subclass_retail
                        from ordhead oh,
                             ordloc ol,
                             v_packsku_qty vpq,
                             item_master im,
                             item_loc il,
                             deps d,                             
                             loc,                             
                             mc
                       where oh.order_no     = I_order_no
                         and oh.order_no     = ol.order_no
                         and ol.item         = vpq.pack_no
                         and vpq.item        = im.item
                         and im.item         = il.item
                         and ol.location     = il.loc
                         and im.dept         = d.dept
                         and d.otb_calc_type = 'R'
                         --
                         and ol.location     = loc.loc
                         --
                         and mc.from_currency = loc.currency_code
                         and mc.to_currency   = L_ord_currency
                         and mc.date_rank     = 1
                       group by oh.order_type,
                                oh.otb_eow_date,
                                im.dept,
                                im.class,
                                im.subclass) inner
               group by inner.order_type,
                        inner.otb_eow_date,
                        inner.dept,
                        inner.class,
                        inner.subclass) i,
             otb o,
             v_subclass_tl s            
       where i.dept          = s.dept
         and i.class         = s.class
         and i.subclass      = s.subclass
         and i.otb_eow_date  = o.eow_date(+)
         and i.dept          = o.dept(+)
         and i.class         = o.class(+)
         and i.subclass      = o.subclass(+);

   cursor c_nonpack_cost is
      with 
      loc as
      (select s.store loc, 
              s.currency_code 
         from store s
       union all
       select w.wh loc, 
              w.currency_code 
         from wh w
      ),
      exchg as
      (select from_currency,
              to_currency,
              effective_date,
              exchange_rate,
              rank() over
              (PARTITION BY from_currency, to_currency, exchange_type
                   ORDER BY effective_date DESC) date_rank
         from mv_currency_conversion_rates
        where exchange_type   = 'C'
          and effective_date <= (L_vdate + 1)
      )      
      select oh.order_no,
             oh.supplier,
             oh.order_type,
             oh.otb_eow_date,
             oh.import_country_id,
             os.origin_country_id,
             ol.item,
             NULL comp_item,
             ol.location,
             im.dept,
             im.class,
             im.subclass,
             s.sub_name,
             ol.unit_cost * (ol.qty_ordered - NVL(ol.qty_received,0)) unit_cost,
             ol.unit_cost * mc.exchange_rate * (ol.qty_ordered - NVL(ol.qty_received,0)) unit_cost_prim,
             decode(oh.order_type,
                    MBL_CONSTANTS.ORDER_TYPE_ARB, a_budget_amt,
                    MBL_CONSTANTS.ORDER_TYPE_BRB, b_budget_amt,
                    MBL_CONSTANTS.ORDER_TYPE_NB, n_budget_amt,
                    0) budget_amount,
             decode(oh.order_type, 
                    MBL_CONSTANTS.ORDER_TYPE_ARB, a_receipts_amt,
                    MBL_CONSTANTS.ORDER_TYPE_BRB, b_receipts_amt,
                    MBL_CONSTANTS.ORDER_TYPE_NB, n_receipts_amt,
                    0) received_amount,                                         
             decode(oh.order_type,
                    MBL_CONSTANTS.ORDER_TYPE_ARB, a_approved_amt,
                    MBL_CONSTANTS.ORDER_TYPE_BRB, b_approved_amt,
                    MBL_CONSTANTS.ORDER_TYPE_NB, n_approved_amt,
                    0) approved_amount
        from ordhead oh,
             ordsku os,
             ordloc ol,
             item_master im,
             deps d,
             otb o,
             loc,             
             exchg mc,            
             v_subclass_tl s     
       where oh.order_no      = I_order_no
         and oh.order_no      = os.order_no
         and os.order_no      = ol.order_no
         and os.item          = ol.item
         and ol.item          = im.item
         and im.pack_ind      = MBL_CONSTANTS.NO_IND
         and im.dept          = d.dept
         and d.otb_calc_type  = 'C'
         and im.dept          = s.dept
         and im.class         = s.class
         and im.subclass      = s.subclass
         and oh.otb_eow_date  = o.eow_date(+)
         and im.dept          = o.dept(+)
         and im.class         = o.class(+)
         and im.subclass      = o.subclass(+)
         --
         and ol.location      = loc.loc
         and mc.from_currency = loc.currency_code
         and mc.to_currency   = L_ord_currency
         and mc.date_rank     = 1
         --         
       order by im.dept,
                im.class,
                im.subclass;

   cursor c_pack_cost is
      with
      loc as
      (select s.store loc, 
              s.currency_code 
         from store s
       union all
      select w.wh loc, 
             w.currency_code 
        from wh w
      ),
      exchg as
      (select from_currency,
              to_currency,
              effective_date,
              exchange_rate,
              rank() over
              (PARTITION BY from_currency, to_currency, exchange_type
                   ORDER BY effective_date DESC) date_rank
         from mv_currency_conversion_rates
        where exchange_type   = 'C'
          and effective_date <= (L_vdate + 1)
      )      
      select i.order_no,
             i.supplier,
             i.order_type,
             i.otb_eow_date,
             i.import_country_id,
             i.origin_country_id,
             i.pack_no,
             i.pack_type,
             i.comp_item,
             i.location,
             i.dept,
             i.class,
             i.subclass,
             s.sub_name,
             i.unit_cost,
             i.unit_cost_prim,
             i.comp_cost,
             i.pack_cost,
             i.comp_cost_prim,
             i.pack_cost_prim,
             decode(i.order_type,
                    MBL_CONSTANTS.ORDER_TYPE_ARB, a_budget_amt,
                    MBL_CONSTANTS.ORDER_TYPE_BRB, b_budget_amt,
                    MBL_CONSTANTS.ORDER_TYPE_NB, n_budget_amt ,
                    0) budget_amount,
             decode(i.order_type, 
                    MBL_CONSTANTS.ORDER_TYPE_ARB, a_receipts_amt,
                    MBL_CONSTANTS.ORDER_TYPE_BRB, b_receipts_amt,
                    MBL_CONSTANTS.ORDER_TYPE_NB, n_receipts_amt,
                    0) received_amount,                                       
             decode(i.order_type,
                    MBL_CONSTANTS.ORDER_TYPE_ARB, a_approved_amt,
                    MBL_CONSTANTS.ORDER_TYPE_BRB, b_approved_amt,
                    MBL_CONSTANTS.ORDER_TYPE_NB, n_approved_amt,
                    0) approved_amount
        from otb o,
             (--
              --Pack Cost SALES tax
              --
              select oh.order_no,
                     oh.supplier,
                     oh.order_type,
                     oh.otb_eow_date,
                     oh.import_country_id,
                     os.origin_country_id,
                     ol.item pack_no,
                     impack.pack_type,
                     im.item comp_item,
                     ol.location,
                     im.dept,
                     im.class,
                     im.subclass,
                     ol.unit_cost,
                     ol.unit_cost * mc.exchange_rate unit_cost_prim,
                     isc.unit_cost * vpq.qty comp_cost,
                     sum(isc.unit_cost * vpq.qty) over (partition by oh.order_no, os.item) pack_cost,
                     isc.unit_cost * vpq.qty *  mc2.exchange_rate comp_cost_prim,
                     sum(isc.unit_cost * vpq.qty* mc2.exchange_rate) over (partition by oh.order_no, os.item) pack_cost_prim
                from ordhead oh,
                     ordsku os,
                     ordloc ol,
                     item_master impack,
                     item_master im,
                     v_packsku_qty vpq,
                     deps d,
                     item_supp_country isc,
                     sups s,
                     loc,                    
                     exchg mc,                     
                     exchg mc2
               where oh.order_no           = I_order_no
                 and oh.order_no           = os.order_no
                 and os.order_no           = ol.order_no
                 and os.item               = ol.item
                 and ol.item               = impack.item
                 and ol.item               = vpq.pack_no
                 and vpq.item              = im.item
                 and im.dept               = d.dept
                 and d.otb_calc_type       = 'C'
                 --
                 and L_default_tax_type    = 'SALES'
                 --
                 and isc.item              = vpq.item
                 and isc.supplier          = oh.supplier
                 and isc.origin_country_id = os.origin_country_id
                 and isc.supplier          = s.supplier
                 --
                 and ol.location      = loc.loc
                 and mc.from_currency = loc.currency_code
                 and mc.to_currency   = L_ord_currency
                 and mc.date_rank     = 1
                 --
                 and mc2.from_currency = s.currency_code
                 and mc2.to_currency   = L_ord_currency
                 and mc2.date_rank     = 1
              union all
              --
              --Pack Cost GTAX or SVAT tax
              --
              select oh.order_no,
                     oh.supplier,
                     oh.order_type,
                     oh.otb_eow_date,
                     oh.import_country_id,
                     os.origin_country_id,
                     ol.item pack_no,
                     impack.pack_type,
                     im.item comp_item,
                     ol.location,
                     im.dept,
                     im.class,
                     im.subclass,
                     ol.unit_cost,
                     ol.unit_cost * mc.exchange_rate unit_cost_prim,
                     --
                     decode(ca.default_po_cost,
                            'BC', ich.base_cost * vpq.qty, 
                                  ich.negotiated_item_cost * vpq.qty) comp_cost,
                     --
                     sum(decode(ca.default_po_cost,
                                'BC', ich.base_cost * vpq.qty, 
                                      ich.negotiated_item_cost * vpq.qty))
                        over (partition by oh.order_no, os.item) pack_cost,
                     --
                     decode(ca.default_po_cost,
                            'BC', ich.base_cost * vpq.qty * mc2.exchange_rate,
                                  ich.negotiated_item_cost * vpq.qty * mc2.exchange_rate) comp_cost_prim,
                     --
                     sum(decode(ca.default_po_cost,
                                'BC', ich.base_cost * vpq.qty * mc2.exchange_rate,
                                      ich.negotiated_item_cost * vpq.qty * mc2.exchange_rate))
                        over (partition by oh.order_no, os.item) pack_cost_prim
                from ordhead oh,
                     ordsku os,
                     ordloc ol,
                     item_master impack,
                     item_master im,
                     v_packsku_qty vpq,
                     deps d,
                     item_cost_head  ich,
                     sups s,
                     country_attrib ca,
                     loc,                     
                     exchg mc,                     
                     exchg mc2
               where oh.order_no            = I_order_no
                 and oh.order_no            = os.order_no
                 and os.order_no            = ol.order_no
                 and os.item                = ol.item
                 and ol.item                = impack.item
                 and ol.item                = vpq.pack_no
                 and vpq.item               = im.item
                 and im.dept                = d.dept
                 and d.otb_calc_type        = 'C'
                 --
                 and L_default_tax_type    != 'SALES'
                 --
                 and ich.item               = vpq.item
                 and ich.supplier           = oh.supplier
                 and ich.origin_country_id  = os.origin_country_id
                 and ich.origin_country_id  = ca.country_id
                 and ich.prim_dlvy_ctry_ind = 'Y'
                 and ich.supplier           = s.supplier
                 --
                 and ol.location      = loc.loc
                 and mc.from_currency = loc.currency_code
                 and mc.to_currency   = L_ord_currency
                 and mc.date_rank     = 1
                 --
                 and mc2.from_currency = s.currency_code
                 and mc2.to_currency   = L_ord_currency
                 and mc2.date_rank     = 1
             ) i,
             v_subclass_tl s            
       where i.dept          = s.dept
         and i.class         = s.class
         and i.subclass      = s.subclass
         and i.otb_eow_date  = o.eow_date(+)
         and i.dept          = o.dept(+)
         and i.class         = o.class(+)
         and i.subclass      = o.subclass(+);


BEGIN

   O_result := MBL_RPO_ORDER_SUM_OTB_TBL();

   if SYSTEM_OPTIONS_SQL.GET_ELC_IND(O_error_message,
                                     L_elc_ind) = FALSE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;
   if SYSTEM_OPTIONS_SQL.GET_DEFAULT_TAX_TYPE(O_error_message,
                                              L_default_tax_type) = FALSE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                       L_prim_currency) = FALSE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;

   select otb_eow_date,currency_code,exchange_rate
     into L_otb_eow_date,L_ord_currency,L_exchange_rate
     from ordhead oh
    where oh.order_no = I_order_no;

   if DATES_SQL.GET_EOW_DATE(O_error_message,
                             L_eow_date) = FALSE then
      RAISE MBL_CONSTANTS.PROGRAM_ERROR;
   end if;

   L_period_ahead := (L_otb_eow_date - L_eow_date) / 7;

   -------------------------
   -----RETAIL
   -------------------------

   open c_retail;
   fetch c_retail bulk collect into L_retail_tbl;
   close c_retail;

   if L_retail_tbl is not NULL and L_retail_tbl.count > 0 then
      for i IN L_retail_tbl.FIRST..L_retail_tbl.LAST LOOP
         L_dept     := L_retail_tbl(i).dept;
         L_class    := L_retail_tbl(i).class;
         L_subclass := L_retail_tbl(i).subclass;         
      end loop;
   end if;

   -------------------------
   -----NON PACK COST
   -------------------------

   --reset
   L_dept               := NULL;
   L_class              := NULL;
   L_subclass           := NULL;
   L_subclass_name      := NULL;
   L_budget_amount      := NULL;
   L_approved_amount    := NULL;
   L_received_amount    := NULL;
   L_outstanding_amount := NULL;
   L_order_total        := 0;

   for rec in c_nonpack_cost loop

      if L_dept is NULL then
         L_dept               := rec.dept;
         L_class              := rec.class;
         L_subclass           := rec.subclass;
         L_subclass_name      := rec.sub_name;
         L_budget_amount      := rec.budget_amount;
         L_received_amount    := rec.received_amount;
         L_approved_amount    := rec.approved_amount;
         L_outstanding_amount := rec.approved_amount - rec.received_amount;
      end if;

      if L_dept     != rec.dept or
         L_class    != rec.class or
         L_subclass != rec.subclass then    
    

         L_otb_rec := MBL_RPO_ORDER_SUM_OTB_REC(L_dept,
                                                L_class,
                                                L_subclass,
                                                L_subclass_name,
                                                L_order_total,
                                                L_budget_amount,
                                                L_received_amount,
                                                L_approved_amount,
                                                L_outstanding_amount);
         L_nonpack_cost_tbl.extend;
         L_nonpack_cost_tbl(L_nonpack_cost_tbl.count) := L_otb_rec;

         L_dept               := rec.dept;
         L_class              := rec.class;
         L_subclass           := rec.subclass;
         L_subclass_name      := rec.sub_name;
         L_budget_amount      := rec.budget_amount;
         L_received_amount    := rec.received_amount;
         L_approved_amount    := rec.approved_amount;
         L_outstanding_amount := rec.approved_amount - rec.received_amount;

         L_order_total     := 0;

      end if;
  
      if L_elc_ind = MBL_CONSTANTS.YES_IND then
         -- retrieve the landed cost for the item in primary currency
         if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                     L_total_elc,
                                     L_total_exp,
                                     L_exp_currency,
                                     L_exchange_rate_exp,
                                     L_total_dty,
                                     L_dty_currency,
                                     I_order_no,
                                     rec.item,
                                     NULL,  --comp_item
                                     NULL,  --zone_id
                                     rec.location,
                                     rec.supplier,
                                     rec.origin_country_id,
                                     rec.import_country_id,
                                     rec.unit_cost) = FALSE then
            RAISE MBL_CONSTANTS.PROGRAM_ERROR;
         end if;
         --
         if L_total_elc != 0 then
            if CURRENCY_SQL.CONVERT (O_error_message,
                                     L_total_elc,
                                     L_prim_currency,
                                     L_ord_currency,
                                     L_total_elc,
                                     'N',
                                     NULL,
                                     NULL,
                                     NULL,
                                     L_exchange_rate) = FALSE then
               RAISE MBL_CONSTANTS.PROGRAM_ERROR;
             end if;
          end if;
          --

      else
         L_total_elc := rec.unit_cost_prim;
      end if;

      L_order_total := L_order_total + L_total_elc;

   end loop;

   if L_dept is not NULL then 

      L_otb_rec := MBL_RPO_ORDER_SUM_OTB_REC(L_dept,
                                             L_class,
                                             L_subclass,
                                             L_subclass_name,
                                             L_order_total,
                                             L_budget_amount,
                                             L_received_amount,
                                             L_approved_amount,
                                             L_outstanding_amount);
      L_nonpack_cost_tbl.extend;
      L_nonpack_cost_tbl(L_nonpack_cost_tbl.count) := L_otb_rec;
   end if;

   -------------------------
   -----PACK COST
   -------------------------

   --reset
   L_dept               := NULL;
   L_class              := NULL;
   L_subclass           := NULL;
   L_subclass_name      := NULL;
   L_budget_amount      := NULL;
   L_approved_amount    := NULL;
   L_received_amount    := NULL;
   L_outstanding_amount := NULL;
   L_order_total        := 0;

   for rec in c_pack_cost loop

      if L_dept is NULL then
         L_dept               := rec.dept;
         L_class              := rec.class;
         L_subclass           := rec.subclass;
         L_subclass_name      := rec.sub_name;
         L_budget_amount      := rec.budget_amount;
         L_received_amount    := rec.received_amount;
         L_approved_amount    := rec.approved_amount;
         L_outstanding_amount := rec.approved_amount - rec.received_amount;
      end if;


      if L_dept     != rec.dept or
         L_class    != rec.class or
         L_subclass != rec.subclass then 

         L_otb_rec := MBL_RPO_ORDER_SUM_OTB_REC(L_dept,
                                                L_class,
                                                L_subclass,
                                                L_subclass_name,
                                                L_order_total,
                                                L_budget_amount,
                                                L_received_amount,
                                                L_approved_amount,
                                                L_outstanding_amount);
         L_pack_cost_tbl.extend;
         L_pack_cost_tbl(L_pack_cost_tbl.count) := L_otb_rec;

         L_dept               := rec.dept;
         L_class              := rec.class;
         L_subclass           := rec.subclass;
         L_subclass_name      := rec.sub_name;
         L_budget_amount      := rec.budget_amount;
         L_received_amount    := rec.received_amount;
         L_approved_amount    := rec.approved_amount;
         L_outstanding_amount := rec.approved_amount - rec.received_amount;

         L_order_total     := 0;

      end if;

      if L_elc_ind = MBL_CONSTANTS.YES_IND then

         if rec.pack_type = 'V' then

            -- retrieve the landed cost for the item in primary currency
            if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                        L_total_elc,
                                        L_total_exp,
                                        L_exp_currency,
                                        L_exchange_rate_exp,
                                        L_total_dty,
                                        L_dty_currency,
                                        I_order_no,
                                        rec.pack_no,
                                        NULL,  --comp_item
                                        NULL,  --zone_id
                                        rec.location,
                                        rec.supplier,
                                        rec.origin_country_id,
                                        rec.import_country_id,
                                        rec.unit_cost) = FALSE then
               RAISE MBL_CONSTANTS.PROGRAM_ERROR;
            end if;

         else --rec.pack_type = 'B'

            -- retrieve the landed cost for the item in primary currency
            if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                        L_total_elc,
                                        L_total_exp,
                                        L_exp_currency,
                                        L_exchange_rate_exp,
                                        L_total_dty,
                                        L_dty_currency,
                                        I_order_no,
                                        rec.pack_no,
                                        rec.comp_item,
                                        NULL,  --zone_id
                                        rec.location,
                                        rec.supplier,
                                        rec.origin_country_id,
                                        rec.import_country_id,
                                        rec.unit_cost) = FALSE then
               RAISE MBL_CONSTANTS.PROGRAM_ERROR;
            end if;

         end if;
         --
         if L_total_elc != 0 then
            if CURRENCY_SQL.CONVERT (O_error_message,
                                     L_total_elc,
                                     L_prim_currency,
                                     L_ord_currency,
                                     L_total_elc,
                                     'N',
                                     NULL,
                                     NULL,
                                     NULL,
                                     L_exchange_rate) = FALSE then
               RAISE MBL_CONSTANTS.PROGRAM_ERROR;
             end if;
          end if;
          --
         L_total_elc := L_total_elc * (rec.comp_cost_prim / rec.pack_cost_prim);

      else
         L_total_elc := rec.unit_cost_prim * (rec.comp_cost_prim / rec.pack_cost_prim);
      end if;


      L_order_total := L_order_total + L_total_elc;

   end loop;

   --put last record in table
   if L_dept is not NULL then 
      L_otb_rec := MBL_RPO_ORDER_SUM_OTB_REC(L_dept,
                                             L_class,
                                             L_subclass,
                                             L_subclass_name,
                                             L_order_total,
                                             L_budget_amount,
                                             L_received_amount,
                                             L_approved_amount,
                                             L_outstanding_amount);
      L_pack_cost_tbl.extend;
      L_pack_cost_tbl(L_pack_cost_tbl.count) := L_otb_rec;
   end if;

   -------------------------
   -----Built final answer
   -------------------------
   O_result := L_retail_tbl MULTISET UNION ALL L_nonpack_cost_tbl MULTISET UNION ALL L_pack_cost_tbl;
   --
   if O_result is not null and O_result.count > 0 then
   
      for i in 1..O_result.count loop
         --
         if O_result(i).budget_amount is not null and O_result(i).budget_amount <>  0  then
             if CURRENCY_SQL.CONVERT (O_error_message,
                                      O_result(i).budget_amount,
                                      L_prim_currency,
                                      L_ord_currency,
                                      O_result(i).budget_amount,
                                      'N',
                                      NULL,
                                      NULL,
                                      NULL,
                                      L_exchange_rate) = FALSE then
                RAISE MBL_CONSTANTS.PROGRAM_ERROR;
             end if;   
         end if;
         -- 
         if O_result(i).received_amount is not null and O_result(i).received_amount <>  0  then
             if CURRENCY_SQL.CONVERT (O_error_message,
                                      O_result(i).received_amount,
                                      L_prim_currency,
                                      L_ord_currency,
                                      O_result(i).received_amount,
                                      'N',
                                      NULL,
                                      NULL,
                                      NULL,
                                      L_exchange_rate) = FALSE then
                RAISE MBL_CONSTANTS.PROGRAM_ERROR;
             end if;   
         end if;
         --
         if O_result(i).approved_amount is not null and O_result(i).approved_amount <>  0  then
            if CURRENCY_SQL.CONVERT (O_error_message,
                                     O_result(i).approved_amount,
                                     L_prim_currency,
                                     L_ord_currency,
                                     O_result(i).approved_amount,
                                     'N',
                                     NULL,
                                     NULL,
                                     NULL,
                                     L_exchange_rate) = FALSE then
               RAISE MBL_CONSTANTS.PROGRAM_ERROR;
             end if;   
          end if;
         --
         if O_result(i).outstanding_amount is not null and O_result(i).outstanding_amount <>  0  then
            if CURRENCY_SQL.CONVERT (O_error_message,
                                     O_result(i).outstanding_amount,
                                     L_prim_currency,
                                     L_ord_currency,
                                     O_result(i).outstanding_amount,
                                     'N',
                                     NULL,
                                     NULL,
                                     NULL,
                                     L_exchange_rate) = FALSE then
               RAISE MBL_CONSTANTS.PROGRAM_ERROR;
             end if;   
          end if;
         --
      end loop;
   
   end if;
   --
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
     
      if c_retail%ISOPEN then
         close c_retail;
      end if;
      --
      if c_nonpack_cost%ISOPEN then
         close c_nonpack_cost;
      end if;
      --
      if c_pack_cost%ISOPEN then
         close c_pack_cost;
      end if;       
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;   
   when OTHERS then
      
      if c_retail%ISOPEN then
         close c_retail;
      end if;
      --
      if c_nonpack_cost%ISOPEN then
         close c_nonpack_cost;
      end if;
      --
      if c_pack_cost%ISOPEN then
         close c_pack_cost;
      end if;
      --
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_OTB_TBL;
--------------------------------------------------------------------------------
FUNCTION GET_PO_ITEM_LOC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result          IN OUT MBL_RPO_ORDER_ITEM_LOC_TBL,
                         IO_pagination     IN OUT MBL_PAGINATION_REC,
                         I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                         I_level           IN     VARCHAR2,
                         I_item            IN     ITEM_MASTER.ITEM%TYPE,
                         I_diff_1          IN     ITEM_MASTER.DIFF_1%TYPE,
                         I_diff_2          IN     ITEM_MASTER.DIFF_2%TYPE,
                         I_diff_3          IN     ITEM_MASTER.DIFF_3%TYPE,
                         I_diff_4          IN     ITEM_MASTER.DIFF_4%TYPE)
RETURN NUMBER IS

   L_program            VARCHAR2(61) := 'MBL_RPO_SVC.GET_PO_ITEM_LOC';

   L_currency_code      ORDHEAD.CURRENCY_CODE%TYPE := NULL;

   L_row_start          NUMBER(10)     := 0;
   L_row_end            NUMBER(10)     := 0;
   L_num_rec_found      NUMBER(10)     := 0;
   L_counter_table      OBJ_NUMERIC_ID_TABLE;

   cursor c_order_item_loc is
      select MBL_RPO_ORDER_ITEM_LOC_REC(i2.location,
                                        i2.loc_name,                                        
                                        i2.qty_ordered,
                                        i2.total_cost,
                                        L_currency_code),
                                        i2.total_rows
        from (select i.location,
                     i.loc_name,                     
                     i.qty_ordered,
                     i.total_cost,
                     row_number() over (order by i.location desc) counter,
                     count(*) over () total_rows
                from (select distinct 
                             ol.location,
                             l.loc_name,
                             --                             
                             sum(NVL(ol.qty_ordered,0)) over (partition by ol.location) qty_ordered,
                             sum(ol.unit_cost * ol.qty_ordered) over (partition by ol.location) total_cost
                        from ordloc ol,
                             (select im.item 
                                from item_master im 
                               where I_level        = MBL_CONSTANTS.PARENT_LVL
                                 and im.item_parent = I_item
                              UNION ALL
                              select im.item 
                                from item_master im
                               where I_level        = MBL_CONSTANTS.PARENT_DIFF_LVL
                                 and im.item_parent = I_item
                                 and NVL(im.diff_1,MBL_CONSTANTS.DUMMY_STRING) = NVL(NVL(I_diff_1,im.diff_1),MBL_CONSTANTS.DUMMY_STRING)
                                 and NVL(im.diff_2,MBL_CONSTANTS.DUMMY_STRING) = NVL(NVL(I_diff_2,im.diff_2),MBL_CONSTANTS.DUMMY_STRING)
                                 and NVL(im.diff_3,MBL_CONSTANTS.DUMMY_STRING) = NVL(NVL(I_diff_3,im.diff_3),MBL_CONSTANTS.DUMMY_STRING)
                                 and NVL(im.diff_4,MBL_CONSTANTS.DUMMY_STRING) = NVL(NVL(I_diff_4,im.diff_4),MBL_CONSTANTS.DUMMY_STRING)
                              UNION ALL
                              select I_item from dual 
                               where I_level = MBL_CONSTANTS.TRAN_LVL) it,
                             (select store loc, store_name loc_name from v_store_tl
                              UNION ALL
                              select wh loc, wh_name loc_name from v_wh_tl) l
                       where ol.order_no        = I_order_no
                         and ol.item            = it.item
                         and ol.location        = l.loc) i) i2
               where i2.counter    >= L_row_start
                 and i2.counter    <  L_row_end
               order by i2.location;

BEGIN

   select oh.currency_code
     into L_currency_code
     from ordhead oh
    where oh.order_no = I_order_no;

   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   open c_order_item_loc;
   fetch c_order_item_loc bulk collect into O_result, L_counter_table;
   close c_order_item_loc;

   if L_counter_table is NULL or L_counter_table.count <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_table(L_counter_table.first);
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when NO_DATA_FOUND then
      if c_order_item_loc%ISOPEN then
         close c_order_item_loc;
      end if;    
      O_error_message := sql_lib.create_msg('INV_ORDER_NO');
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
   
   when MBL_CONSTANTS.PROGRAM_ERROR then
      if c_order_item_loc%ISOPEN then
         close c_order_item_loc;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
      
   when OTHERS then
      if c_order_item_loc%ISOPEN then
         close c_order_item_loc;
      end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_PO_ITEM_LOC;
--------------------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_DISPLAY_LEVEL(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_items_disply_level   OUT    VARCHAR2,
                                   I_order_no             IN     ORDHEAD.ORDER_NO%TYPE)
RETURN NUMBER IS
   L_program            VARCHAR2(80) := 'MBL_RPO_SVC.GET_DEFAULT_DISPLAY_LEVEL';
   L_ind                VARCHAR2(1)  := NULL;   
   --
   cursor c_item_level is 
      select 'Y'
        from dual 
       where not exists ( 
                          select 1 
                            from ordsku os,
                                 item_master im                                                           
                           where os.order_no    =  I_order_no
                             and os.item        = im.item
                             and im.item_parent is  NULL
                          union all 
                          select 1 
                            from ordsku os,
                                 item_master im,
                                 item_master imparent
                           where os.order_no                 =  I_order_no
                             and os.item                     = im.item
                             and im.item_parent              = imparent.item
                             and im.item_grandparent         is NULL
                             and imparent.item_aggregate_ind = MBL_CONSTANTS.NO_IND
                          union all 
                          select 1 
                            from ordsku os,
                                 item_master im,
                                 item_master grandparent
                           where os.order_no                    =  I_order_no
                             and os.item                        = im.item
                             and im.item_grandparent            = grandparent.item
                             and grandparent.item_aggregate_ind = MBL_CONSTANTS.NO_IND
                        );
   cursor c_ordloc_wksht is
      select 'Y' 
        from ordloc_wksht ow
       where exists (select 1
                       from (
                              select oh.order_no order_no
                                from ordhead oh
                               where oh.order_no = I_order_no
                               minus
                               select distinct os.order_no order_no
                                 from ordsku os
                                where os.order_no = I_order_no
                              ) od
                       where od.order_no = ow.order_no
                    );
         
         
BEGIN
   if I_order_no is NULL then     
      O_error_message := sql_lib.create_msg('NULL_ORDER_NO',NULL,NULL,NULL);
      raise MBL_CONSTANTS.PROGRAM_ERROR;
   end if;
   --
   open  c_ordloc_wksht;
   fetch c_ordloc_wksht into L_ind;
   if c_ordloc_wksht%found and L_ind = MBL_CONSTANTS.YES_IND then
      O_items_disply_level := MBL_CONSTANTS.TRAN_LVL;
      close c_ordloc_wksht;
      RETURN MBL_CONSTANTS.SUCCESS;
   end if;
   close c_ordloc_wksht;
   --
   open  c_item_level;
   fetch c_item_level into L_ind;
   if c_item_level%notfound then
      L_ind := MBL_CONSTANTS.NO_IND;
   end if;
   close c_item_level;
   --
   if L_ind = MBL_CONSTANTS.YES_IND then
      O_items_disply_level := MBL_CONSTANTS.PARENT_LVL;
   else
      O_items_disply_level := MBL_CONSTANTS.TRAN_LVL;
   end if;
   --
   RETURN MBL_CONSTANTS.SUCCESS;
EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      if c_item_level%ISOPEN then
         close c_item_level;
      end if;
      if c_ordloc_wksht%ISOPEN then
         close c_item_level;
      end if; 
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE; 
      
   when OTHERS then
      if c_item_level%ISOPEN then
         close c_item_level;
      end if;
      if c_ordloc_wksht%ISOPEN then
         close c_item_level;
      end if; 
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;    
END GET_DEFAULT_DISPLAY_LEVEL;
-------------------------------------------------------------------------------------------------
FUNCTION GET_PO_ITEMS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_result              IN OUT MBL_RPO_ORDER_SUM_ITEM_TBL,
                      IO_pagination         IN OUT MBL_PAGINATION_REC,
                      I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                      I_items_disply_level  IN     VARCHAR2)

RETURN NUMBER IS
   L_program              VARCHAR2(80) := 'MBL_RPO_SVC.GET_PO_ITEMS';
   L_items_disply_level   VARCHAR2(20) ;
   L_default_image        VARCHAR2(400) ;
   L_currency_code        ORDHEAD.CURRENCY_CODE%TYPE := NULL;
   
   L_row_start          NUMBER(10)     := 0;
   L_row_end            NUMBER(10)     := 0;
   L_num_rec_found      NUMBER(10)     := 0;
   L_counter_table      OBJ_NUMERIC_ID_TABLE;
   
   cursor c_order_item is
      select MBL_RPO_ORDER_SUM_ITEM_REC(i.item,
                                        i.item_desc,
                                        NULL, --diff_1
                                        NULL, --diff_1_desc
                                        NULL, --diff_2
                                        NULL, --diff_2_desc
                                        NULL, --diff_3
                                        NULL, --diff_3_desc
                                        NULL, --diff_4
                                        NULL, --diff_4_desc
                                        i.qty_ordered,
                                        i.total_cost,
                                        L_currency_code,
                                        NVL(i.item_image_url,L_default_image)),
             i.total_rows
        from (select img.item item,
                     img.item_desc,
                     img.qty_ordered,
                     img.total_cost,
                     img.item_image_url,
                     --
                     row_number() over (order by img.item) counter,
                     count(*) over () total_rows
                from (select distinct 
                             os.item,
                             im.item_desc item_desc,
                             sum(ol.qty_ordered) over (partition by os.item) qty_ordered,
                             sum(ol.unit_cost * ol.qty_ordered) over (partition by os.item) total_cost,
                             i.image_addr||i.image_name item_image_url,
                             rank() over (partition by os.item
                                              order by i.create_datetime) image_rank
                        from ordsku os,
                             ordloc ol,
                             v_item_master im,
                             item_image i
                       where os.order_no = I_order_no
                         and os.order_no = ol.order_no
                         and os.item     = ol.item
                         and os.item     = im.item
                         and os.item     = i.item(+)) img
               where img.image_rank = 1) i                
       where i.counter >= L_row_start
         and i.counter <  L_row_end
       order by i.item;

   cursor c_order_parent is
      select MBL_RPO_ORDER_SUM_ITEM_REC(i.item,
                                        i.item_desc,
                                        NULL, --diff_1
                                        NULL, --diff_1_desc
                                        NULL, --diff_2
                                        NULL, --diff_2_desc
                                        NULL, --diff_3
                                        NULL, --diff_3_desc                                       
                                        NULL, --diff_4
                                        NULL, --diff_4_desc
                                        i.qty_ordered,
                                        i.total_cost,
                                        L_currency_code,
                                        NVL(i.item_image_url,L_default_image)),
             i.total_rows                                   
        from (select i2.item,
                     i2.item_desc,
                     i2.qty_ordered,
                     i2.total_cost,
                     i2.item_image_url,
                     row_number() over (order by i2.item) counter,
                     count(*) over () total_rows
               from (select distinct
                            imparent.item item,
                            imparent.item_desc item_desc,
                            sum(ol.qty_ordered) over (partition by imparent.item) qty_ordered,
                            sum(ol.unit_cost * ol.qty_ordered) over (partition by imparent.item) total_cost,
                            i.image_addr||i.image_name item_image_url,
                            rank() over (partition by imparent.item 
                                              order by i.create_datetime) image_rank
                       from ordsku os,
                            ordloc ol,
                            item_master im,
                            v_item_master imparent,
                            item_image i                            
                      where os.order_no    = I_order_no                        
                        and os.order_no    = ol.order_no
                        and os.item        = ol.item
                        and os.item        = im.item
                        and im.item_parent = imparent.item
                        and imparent.item  = i.item(+)
                     ) i2
               where i2.image_rank = 1       
             ) i                         
        where i.counter >= L_row_start
          and i.counter <  L_row_end       
       order by i.item;

   cursor c_order_parent_diff is
      select MBL_RPO_ORDER_SUM_ITEM_REC(i.item,
                                        i.item_desc,
                                        i.diff_1,
                                        df_1.description,
                                        i.diff_2,
                                        df_2.description,
                                        i.diff_3,
                                        df_3.description,
                                        i.diff_4,
                                        df_4.description,
                                        i.qty_ordered,
                                        i.total_cost,
                                        L_currency_code,
                                        NVL(i.item_image_url,L_default_image)),
              i.total_rows                           
        from (   select i2.item,                      
                        i2.item_desc,                      
                        i2.qty_ordered,                      
                        i2.total_cost,                      
                        coalesce(i2.item_image_url,
                             (select i.image_addr || i.image_name item_image_url
			        from item_master im,
			             item_image i
			       where im.item_parent = i2.item
			         and (im.diff_1 = i2.diff_1 or i2.diff_1 is null)
			         and (im.diff_2 = i2.diff_2 or i2.diff_2 is null)
			         and (im.diff_3 = i2.diff_3 or i2.diff_3 is null)
			         and (im.diff_4 = i2.diff_4 or i2.diff_4 is null)
			         and i.item = im.item
			         and i.primary_ind = MBL_CONSTANTS.YES_IND
			         and rownum = 1
                             ),
                             (select i.image_addr || i.image_name item_image_url
                                 from item_image i
                                where i.item = i2.item
                                  and i.primary_ind = MBL_CONSTANTS.YES_IND                       
                             )                        
                            )item_image_url,
                        i2.diff_1,                      
                        i2.diff_2,                      
                        i2.diff_3,                      
                        i2.diff_4,                      
                        row_number() over(order by i2.item) counter,                      
                        count(*) over() total_rows               
                   from (                       
                             select imparent.item item,                               
                                    imparent.item_desc item_desc,                               
                                    sum(ol.qty_ordered) over (partition by imparent.item,                               
                                                                           decode(imparent.diff_1_aggregate_ind, MBL_CONSTANTS.YES_IND, im.diff_1, NULL),                               
                                                                           decode(imparent.diff_2_aggregate_ind, MBL_CONSTANTS.YES_IND, im.diff_2, NULL),                               
                                                                           decode(imparent.diff_3_aggregate_ind, MBL_CONSTANTS.YES_IND, im.diff_3, NULL),                               
                                                                           decode(imparent.diff_4_aggregate_ind, MBL_CONSTANTS.YES_IND, im.diff_4, NULL)                               
                                                             ) qty_ordered,                               
                                    sum(ol.unit_cost * ol.qty_ordered) over (partition by imparent.item,                               
                                                                                          decode(imparent.diff_1_aggregate_ind, MBL_CONSTANTS.YES_IND, im.diff_1, NULL),                               
                                                                                          decode(imparent.diff_2_aggregate_ind, MBL_CONSTANTS.YES_IND, im.diff_2, NULL),                               
                                                                                          decode(imparent.diff_3_aggregate_ind, MBL_CONSTANTS.YES_IND, im.diff_3, NULL),                               
                                                                                          decode(imparent.diff_4_aggregate_ind, MBL_CONSTANTS.YES_IND, im.diff_4, NULL)                               
                                                                            ) total_cost,                               
                                    decode(imparent.diff_1_aggregate_ind,MBL_CONSTANTS.YES_IND,im.diff_1,NULL) diff_1,                               
                                    decode(imparent.diff_2_aggregate_ind,MBL_CONSTANTS.YES_IND,im.diff_2,NULL) diff_2,                               
                                    decode(imparent.diff_3_aggregate_ind,MBL_CONSTANTS.YES_IND,im.diff_3,NULL) diff_3,                               
                                    decode(imparent.diff_4_aggregate_ind,MBL_CONSTANTS.YES_IND,im.diff_4,NULL) diff_4,                               
                                    i.image_addr || i.image_name item_image_url,                               
                                    row_number() over(partition by imparent.item,                               
                                                                   decode(imparent.diff_1_aggregate_ind, MBL_CONSTANTS.YES_IND, im.diff_1, NULL),                               
                                                                   decode(imparent.diff_2_aggregate_ind, MBL_CONSTANTS.YES_IND, im.diff_2, NULL),                               
                                                                   decode(imparent.diff_3_aggregate_ind, MBL_CONSTANTS.YES_IND, im.diff_3, NULL),                               
                                                                   decode(imparent.diff_4_aggregate_ind, MBL_CONSTANTS.YES_IND, im.diff_4, NULL)                               
                                    order by im.item nulls last) image_rank                       
                               from ordsku os,                               
                                    ordloc ol,                               
                                    item_master im,                               
                                    v_item_master imparent,                               
                                    item_image i                       
                              where os.order_no = I_order_no                             
                                and os.order_no = ol.order_no                             
                                and os.item = ol.item                             
                                and os.item = im.item                             
                                and im.item_parent = imparent.item                             
                                and imparent.item_aggregate_ind = MBL_CONSTANTS.YES_IND                             
                                and i.primary_ind(+) = MBL_CONSTANTS.YES_IND                             
                                and im.item = i.item(+)
                        ) i2               
                where i2.image_rank = 1 
             ) i ,
             v_diff_id_group_type df_1,
             v_diff_id_group_type df_2,
             v_diff_id_group_type df_3,
             v_diff_id_group_type df_4
        where i.counter >= L_row_start
          and i.counter <  L_row_end 
          and df_1.id_group(+) = i.diff_1
          and df_2.id_group(+) = i.diff_2
          and df_3.id_group(+) = i.diff_3
          and df_4.id_group(+) = i.diff_4
       order by i.item; 
   

BEGIN
   
   if I_order_no is NULL then     
      O_error_message := sql_lib.create_msg('NULL_ORDER_NO',NULL,NULL,NULL);
      raise MBL_CONSTANTS.PROGRAM_ERROR;
   end if;
  
   select oh.currency_code
        into L_currency_code
        from ordhead oh
    where oh.order_no = I_order_no;

   if I_items_disply_level is NULL then
      --
      if GET_DEFAULT_DISPLAY_LEVEL(O_error_message,
                                   L_items_disply_level,
                                   I_order_no) = MBL_CONSTANTS.FAILURE then
          raise MBL_CONSTANTS.PROGRAM_ERROR;
      end if;   
      --
   elsif I_items_disply_level is not NULL then
      L_items_disply_level := I_items_disply_level;
      --
      if L_items_disply_level not in (MBL_CONSTANTS.PARENT_LVL,MBL_CONSTANTS.TRAN_LVL,MBL_CONSTANTS.PARENT_DIFF_LVL) then      
         O_error_message := sql_lib.create_msg('INV_HIER_LEVEL',NULL,NULL,NULL);
         raise MBL_CONSTANTS.PROGRAM_ERROR;
      end if; 
      --
   end if;
   --
   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
         RETURN MBL_CONSTANTS.FAILURE;
   end if;
   --
   if MBL_COMMON_SVC.GET_DEFAULT_ITEM_IMAGE(O_error_message,
                                            L_default_image) = MBL_CONSTANTS.FAILURE then
         RETURN MBL_CONSTANTS.FAILURE;
   end if;
   --
   if L_items_disply_level = MBL_CONSTANTS.TRAN_LVL then
      open c_order_item;
      fetch c_order_item bulk collect into O_result, L_counter_table;
      close c_order_item;
   end if; 
   --
   if L_items_disply_level = MBL_CONSTANTS.PARENT_LVL then
      open c_order_parent;
      fetch c_order_parent bulk collect into O_result, L_counter_table;
      close c_order_parent; 
   end if;
   --
   if L_items_disply_level = MBL_CONSTANTS.PARENT_DIFF_LVL then
      open c_order_parent_diff;
      fetch c_order_parent_diff bulk collect into O_result, L_counter_table;
      close c_order_parent_diff;  
   end if;
   -- 
   if L_counter_table is NULL or L_counter_table.count <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_table(L_counter_table.first);
   end if;
   --   
   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;
   --
   
   RETURN MBL_CONSTANTS.SUCCESS;
EXCEPTION
   when NO_DATA_FOUND then
      if c_order_item%ISOPEN then
         close c_order_item;
      end if;
      if c_order_parent%ISOPEN then
         close c_order_parent;
      end if;
      if c_order_parent_diff%ISOPEN then
         close c_order_parent_diff;
      end if;      
      O_error_message := sql_lib.create_msg('INV_ORDER_NO');
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
   
   when MBL_CONSTANTS.PROGRAM_ERROR then
      if c_order_item%ISOPEN then
         close c_order_item;
      end if;
      if c_order_parent%ISOPEN then
         close c_order_parent;
      end if;
      if c_order_parent_diff%ISOPEN then
         close c_order_parent_diff;
      end if;      
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
      
   when OTHERS then
      if c_order_item%ISOPEN then
         close c_order_item;
      end if;
      if c_order_parent%ISOPEN then
         close c_order_parent;
      end if;
      if c_order_parent_diff%ISOPEN then
         close c_order_parent_diff;
      end if;      
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;    
END GET_PO_ITEMS;
------------------------------------------------------------------------------------
FUNCTION APPROVE_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_result           OUT MBL_RPO_STATUS_REC,
                    I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program   VARCHAR2(61) := 'MBL_RPO_SVC.APPROVE_PO';
   L_result    RMS_OI_PO_STATUS_REC;
   L_return    NUMBER(1) := MBL_CONSTANTS.SUCCESS;

BEGIN

   L_return := RMS_OI_ACTION_PO.APPROVE_PO(O_error_message,
                                           L_result,
                                           0,
                                           I_orders);
   --
   if COPY_PO_STATUS_REC(O_error_message,
                         O_result,
                         L_result,
                         L_return,
                         I_orders) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;
   ---
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END APPROVE_PO;
--------------------------------------------------------------------------------
FUNCTION REJECT_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result           OUT MBL_RPO_STATUS_REC,
                   I_comments      IN     ORDHEAD.COMMENT_DESC%TYPE,
                   I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program   VARCHAR2(61) := 'MBL_RPO_SVC.REJECT_PO';
   L_result    RMS_OI_PO_STATUS_REC;
   L_return    NUMBER(1) := MBL_CONSTANTS.SUCCESS;

BEGIN

   L_return := RMS_OI_ACTION_PO.REJECT_PO(O_error_message,
                                          L_result,
                                          0,
                                          I_comments,
                                          I_orders);
   --
   if COPY_PO_STATUS_REC(O_error_message,
                         O_result,
                         L_result,
                         L_return,
                         I_orders) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;
   ---
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END REJECT_PO;
--------------------------------------------------------------------------------
FUNCTION CANCEL_PO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_result           OUT MBL_RPO_STATUS_REC,
                   I_cancel_code   IN     ORDLOC.CANCEL_CODE%TYPE,
                   I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program   VARCHAR2(61) := 'MBL_RPO_SVC.CANCEL_PO';
   L_result    RMS_OI_PO_STATUS_REC;
   L_return    NUMBER(1) := MBL_CONSTANTS.SUCCESS;

BEGIN

   L_return := RMS_OI_ACTION_PO.CANCEL_PO(O_error_message,
                                          L_result,
                                          0,
                                          I_cancel_code,
                                          I_orders);
   --
   if COPY_PO_STATUS_REC(O_error_message,
                         O_result,
                         L_result,
                         L_return,
                         I_orders) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;
   ---
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END CANCEL_PO;
--------------------------------------------------------------------------------------------------
FUNCTION COPY_PO_STATUS_REC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_result           OUT MBL_RPO_STATUS_REC,
                            I_result        IN     RMS_OI_PO_STATUS_REC,
                            I_return        IN     NUMBER,
                            I_orders        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(61) := 'MBL_RPO_SVC.COPY_PO_STATUS_REC';
   
   cursor C_ALL_PO_ERRORS is
     select MBL_RPO_FAIL_REC(value(input),
                             O_error_message)
       from table(cast(I_orders as OBJ_NUMERIC_ID_TABLE)) input;
   
   cursor C_FAIL_PO_ERRORS is
     select MBL_RPO_FAIL_REC(input.order_no,
                             input.error_message)
       from table(cast(I_result.fail_orders_tbl as RMS_OI_PO_FAIL_TBL)) input;
       
BEGIN

   if I_return = MBL_CONSTANTS.FAILURE then
      if O_error_message is not NULL and
         (I_result.success_orders_count = 0 or 
          I_result.success_orders_count is NULL) then
         O_result := new MBL_RPO_STATUS_REC(0,
                                            NULL,
                                            I_orders.count,
                                            NULL);
        --
         open C_ALL_PO_ERRORS;
         fetch C_ALL_PO_ERRORS bulk collect into O_result.fail_orders_tbl;
         close C_ALL_PO_ERRORS;
      else
         RETURN MBL_CONSTANTS.FAILURE;
      end if;
   else
      O_result := new MBL_RPO_STATUS_REC(I_result.success_orders_count,
                                         I_result.success_orders_tbl,
                                         I_result.fail_orders_count,
                                         NULL);
      if I_result.fail_orders_count > 0 then
         open C_FAIL_PO_ERRORS;
         fetch C_FAIL_PO_ERRORS bulk collect into O_result.fail_orders_tbl;
         close C_FAIL_PO_ERRORS;
      else
         O_result.fail_orders_tbl := NULL;
      end if;
   end if;

   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_ALL_PO_ERRORS%ISOPEN then close C_ALL_PO_ERRORS; end if;
      if C_FAIL_PO_ERRORS%ISOPEN then close C_FAIL_PO_ERRORS; end if;
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END COPY_PO_STATUS_REC;
--------------------------------------------------------------------------------------------------------
FUNCTION GET_PO_USERS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_result          IN OUT   OBJ_VARCHAR_ID_TABLE,
                      IO_pagination     IN OUT   MBL_PAGINATION_REC,
                      I_search_string   IN       VARCHAR2)
RETURN NUMBER IS

   L_program   VARCHAR2(61) := 'MBL_RPO_SVC.GET_PO_USERS';
   L_row_start       NUMBER(10)   := 0;
   L_row_end         NUMBER(10)   := 0;
   L_num_rec_found   NUMBER(10)   := 0;
   L_counter_table   OBJ_NUMERIC_ID_TABLE;
   
    cursor C_GET_PO_USERS is
       select i2.create_id,
              i2.total_rows
         from (select i.create_id,
                      ROW_NUMBER() OVER (ORDER BY i.create_id) counter,
                      COUNT(*) OVER () total_rows
                 from (select distinct oh.create_id
                         from v_ordhead oh
                         where upper(oh.create_id) like upper(('%'||nvl(I_search_string,oh.create_id)||'%'))
                       ) i
              )i2
        where i2.counter    >= L_row_start
          and i2.counter    <  L_row_end
        order by i2.create_id;

BEGIN

   if MBL_COMMON_SVC.GET_PAGE_BOUNDRIES(O_error_message,
                                        L_row_start,
                                        L_row_end,
                                        IO_pagination.I_page_size,
                                        IO_pagination.I_page_number) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   open C_GET_PO_USERS;
   fetch C_GET_PO_USERS BULK COLLECT into O_result,
                                          L_counter_table;
   close C_GET_PO_USERS;

   if L_counter_table is NULL or L_counter_table.COUNT <= 0 then
      L_num_rec_found := 0;
   else
      L_num_rec_found := L_counter_table(L_counter_table.FIRST);
   end if;

   if MBL_COMMON_SVC.GET_PAGE_NUMBERS(O_error_message,
                                      IO_pagination,
                                      L_num_rec_found) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;

   return MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when MBL_CONSTANTS.PROGRAM_ERROR then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
      
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
END GET_PO_USERS;
------------------------------------------------------------------------------------
FUNCTION UPDATE_PO_DATES(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_result             OUT MBL_RPO_STATUS_REC,
                         I_not_before_date IN     ORDHEAD.NOT_BEFORE_DATE%TYPE,
                         I_not_after_date  IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                         I_otb_eow_date    IN     ORDHEAD.OTB_EOW_DATE%TYPE,
                         I_orders          IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program   VARCHAR2(61) := 'MBL_RPO_SVC.UPDATE_PO_DATES';
   L_result    RMS_OI_PO_STATUS_REC;
   L_return    NUMBER(1) := MBL_CONSTANTS.SUCCESS;

BEGIN

   L_return := RMS_OI_ACTION_PO.UPDATE_PO_DATES(O_error_message,
                                                L_result,
                                                0,
                                                I_not_before_date,
                                                I_not_after_date,
                                                I_otb_eow_date,
                                                I_orders);
   --
   if COPY_PO_STATUS_REC(O_error_message,
                         O_result,
                         L_result,
                         L_return,
                         I_orders) = MBL_CONSTANTS.FAILURE then
      return MBL_CONSTANTS.FAILURE;
   end if;        
   ---
   return MBL_CONSTANTS.SUCCESS;

EXCEPTION     
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      return MBL_CONSTANTS.FAILURE;
END UPDATE_PO_DATES;
--------------------------------------------------------------------------------
FUNCTION GET_CANCEL_CODE_LIST(O_error_message   IN  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_result          OUT MBL_CODE_DETAIL_TBL)

RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_RPO_SVC.GET_CANCEL_CODE_LIST';

BEGIN   

   if MBL_COMMON_SVC.GET_CODE_DETAIL(O_error_message,
                                     'ORCA',
                                     O_result) = MBL_CONSTANTS.FAILURE then
      RETURN MBL_CONSTANTS.FAILURE;
   end if;   
   
   RETURN MBL_CONSTANTS.SUCCESS;
EXCEPTION      
   when OTHERS then
      MBL_COMMON_SVC.HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE; 
END GET_CANCEL_CODE_LIST;
--------------------------------------------------------------------------------------------------------
END MBL_RPO_SVC;
/
