----------------------------------------------------------------------------------------------------------------------------------------------------------
DROP TYPE MBL_PO_SUP_TERMS_TBL FORCE;
DROP TYPE MBL_PO_SUP_TERMS_REC FORCE;

CREATE OR REPLACE TYPE MBL_PO_SUP_TERMS_REC AS OBJECT
(
   TERMS            VARCHAR2(15),
   TERMS_CODE       VARCHAR2(50),
   TERMS_DESC       VARCHAR2(240)
)
/
CREATE OR REPLACE TYPE MBL_PO_SUP_TERMS_TBL AS TABLE OF MBL_PO_SUP_TERMS_REC
/

-----------------------------------------------------------------------------
DROP TYPE MBL_PO_ITEM_SEARCH_RST_LOC_TBL FORCE;
DROP TYPE MBL_PO_ITEM_SEARCH_RST_LOC_REC FORCE;

CREATE OR REPLACE TYPE MBL_PO_ITEM_SEARCH_RST_LOC_REC AS OBJECT
(
   LOCATION              NUMBER(10),
   LOC_TYPE              VARCHAR2(1),
   ITEM_LOC_STATUS       VARCHAR2(1),
   UNIT_RETAIL           NUMBER(20,4),
   UNIT_RETAIL_CURRENCY  VARCHAR2(3),
   UNIT_RETAIL_UOM       VARCHAR2(3)
)
/
CREATE OR REPLACE TYPE MBL_PO_ITEM_SEARCH_RST_LOC_TBL AS TABLE OF MBL_PO_ITEM_SEARCH_RST_LOC_REC
/

-----------------------------------------------------------------------------
DROP TYPE MBL_PO_ITEM_SEARCH_RESULT_TBL FORCE;
DROP TYPE MBL_PO_ITEM_SEARCH_RESULT_REC FORCE;

CREATE OR REPLACE TYPE MBL_PO_ITEM_SEARCH_RESULT_REC AS OBJECT
(
   ITEM                 VARCHAR2(25),
   ITEM_DESC            VARCHAR2(255),
   DEPT                 NUMBER(4), 
   SUPPLIER             NUMBER(10),
   ORIGIN_COUNTRY_ID    VARCHAR2(3),
   SUPP_PACK_SIZE       NUMBER(12,4),
   UNIT_COST            NUMBER(20,4),
   SUPPLIER_CURRENCY    VARCHAR2(3),
   BASE_UNIT_RETAIL     NUMBER(20,4),
   BASE_RETAIL_CURRENCY VARCHAR2(3),
   BASE_RETAIL_UOM      VARCHAR2(3),
   ITEM_IMAGE_URL       VARCHAR2(375),
   LOCATION_TBL         MBL_PO_ITEM_SEARCH_RST_LOC_TBL
)
/
CREATE OR REPLACE TYPE MBL_PO_ITEM_SEARCH_RESULT_TBL AS TABLE OF MBL_PO_ITEM_SEARCH_RESULT_REC
/

-----------------------------------------------------------------------------
DROP TYPE MBL_PO_SUP_ITEM_RESULT_REC FORCE;
DROP TYPE MBL_PO_SUP_ITEM_RESULT_TBL FORCE;

CREATE OR REPLACE TYPE MBL_PO_SUP_ITEM_RESULT_REC AS OBJECT
(
   ITEM                VARCHAR2(25),
   ORIGIN_COUNTRY_ID   VARCHAR2(3),
   LEAD_TIME           NUMBER(4)
)
/
CREATE OR REPLACE TYPE MBL_PO_SUP_ITEM_RESULT_TBL AS TABLE OF MBL_PO_SUP_ITEM_RESULT_REC
/

-----------------------------------------------------------------------------
DROP TYPE MBL_PO_SUP_ITEM_LOC_RESULT_REC FORCE;
DROP TYPE MBL_PO_SUP_ITEM_LOC_RESULT_TBL FORCE;

CREATE OR REPLACE TYPE MBL_PO_SUP_ITEM_LOC_RESULT_REC AS OBJECT
(
   ITEM                VARCHAR2(25),
   LOC                 NUMBER(10),
   PICKUP_LEAD_TIME    NUMBER(4)
)
/
CREATE OR REPLACE TYPE MBL_PO_SUP_ITEM_LOC_RESULT_TBL AS TABLE OF MBL_PO_SUP_ITEM_LOC_RESULT_REC
/

-----------------------------------------------------------------------------
DROP TYPE MBL_PO_SUP_SEARCH_RESULT_TBL FORCE;
DROP TYPE MBL_PO_SUP_SEARCH_RESULT_REC FORCE;

CREATE OR REPLACE TYPE MBL_PO_SUP_SEARCH_RESULT_REC AS OBJECT
(
   SUPPLIER                NUMBER(10),
   SUP_NAME                VARCHAR2(240),
   SUPPLIER_CURRENCY       VARCHAR2(3),
   TERMS                   VARCHAR2(15),
   DEFAULT_ITEM_LEAD_TIME  NUMBER(4),
   SUP_ITEM_TBL            MBL_PO_SUP_ITEM_RESULT_TBL,
   SUP_ITEM_LOC_TBL        MBL_PO_SUP_ITEM_LOC_RESULT_TBL
)
/
CREATE OR REPLACE TYPE MBL_PO_SUP_SEARCH_RESULT_TBL AS TABLE OF MBL_PO_SUP_SEARCH_RESULT_REC
/

-----------------------------------------------------------------------------
DROP TYPE MBL_PO_LOC_SEARCH_RESULT_TBL FORCE;
DROP TYPE MBL_PO_LOC_SEARCH_RESULT_REC FORCE;

CREATE OR REPLACE TYPE MBL_PO_LOC_SEARCH_RESULT_REC AS OBJECT
(
   LOCATION            NUMBER(10),
   LOC_TYPE            VARCHAR2(1),
   LOC_NAME            VARCHAR2(150),
   LOCATION_CURRENCY   VARCHAR2(3),
   ORG_UNIT_ID         NUMBER(15)
)
/
CREATE OR REPLACE TYPE MBL_PO_LOC_SEARCH_RESULT_TBL AS TABLE OF MBL_PO_LOC_SEARCH_RESULT_REC
/

-----------------------------------------------------------------------------
/* This is temporary work around until a fix is available for the platform rib object bug*/
DROP TYPE MBL_PO_XORDERDESC_REC FORCE;
DROP TYPE MBL_PO_XORDERDTL_TBL FORCE;
DROP TYPE MBL_PO_XORDERDTL_REC FORCE;

CREATE OR REPLACE TYPE MBL_PO_XORDERDTL_REC AS OBJECT
(
  ITEM              VARCHAR2(25),
  LOCATION          NUMBER(10),
  UNIT_COST         NUMBER(20,4),
  REF_ITEM          VARCHAR2(25),
  ORIGIN_COUNTRY_ID VARCHAR2(3),
  SUPP_PACK_SIZE    NUMBER(12,4),
  QTY_ORDERED       NUMBER(12,4),
  LOCATION_TYPE     VARCHAR2(1),
  CANCEL_IND        VARCHAR2(1),
  REINSTATE_IND     VARCHAR2(1)
)
/
CREATE OR REPLACE TYPE MBL_PO_XORDERDTL_TBL AS TABLE OF MBL_PO_XORDERDTL_REC
/
----
CREATE OR REPLACE TYPE MBL_PO_XORDERDESC_REC AS OBJECT
(
  ORDER_NO           VARCHAR2(12),
  SUPPLIER           VARCHAR2(10),
  CURRENCY_CODE      VARCHAR2(3),
  TERMS              VARCHAR2(15),
  NOT_BEFORE_DATE    DATE,
  NOT_AFTER_DATE     DATE,
  OTB_EOW_DATE       DATE,
  DEPT               NUMBER(4),
  STATUS             VARCHAR2(1),
  EXCHANGE_RATE      NUMBER(20,10),
  INCLUDE_ON_ORD_IND VARCHAR2(1),
  WRITTEN_DATE       DATE,
  XORDERDTL_TBL      MBL_PO_XORDERDTL_TBL,
  ORIG_IND           VARCHAR2(1),
  EDI_PO_IND         VARCHAR2(1),
  PRE_MARK_IND       VARCHAR2(1),
  USER_ID            VARCHAR2(30),
  COMMENT_DESC       VARCHAR2(2000)
)
/
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
