--------------------------------------------------------------------------
-----------------------------------------------------------------------------

DROP TYPE MBL_RPO_USER_CRITERIA_REC FORCE;
/
DROP TYPE MBL_RPO_USER_CRITERIA_TBL FORCE;
/

CREATE OR REPLACE TYPE MBL_RPO_USER_CRITERIA_REC AS OBJECT
(
   STATUSES             OBJ_VARCHAR_ID_TABLE,
   CREATE_ID            OBJ_VARCHAR_ID_TABLE,
   START_CREATE_DATE    DATE,
   END_CREATE_DATE      DATE,
   ORDER_NO             NUMBER(12),
   SUPPLIERS            OBJ_NUMERIC_ID_TABLE,
   ORIGIN_CODES         OBJ_NUMERIC_ID_TABLE,
   DEPARTMENTS          OBJ_NUMERIC_ID_TABLE
);
/

CREATE OR REPLACE TYPE MBL_RPO_USER_CRITERIA_TBL AS TABLE OF MBL_RPO_USER_CRITERIA_REC;
/

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

DROP TYPE MBL_RPO_SEARCH_RES_TBL FORCE;
/
DROP TYPE MBL_RPO_SEARCH_RES_REC FORCE;
/

CREATE OR REPLACE TYPE MBL_RPO_SEARCH_RES_REC AS OBJECT
(
   ORDER_NO                 NUMBER(12),
   STATUS                   VARCHAR2(1),
   SUPPLIER                 NUMBER(10),
   SUP_NAME                 VARCHAR2(240), 
   NOT_BEFORE_DATE          DATE,
   NOT_AFTER_DATE           DATE,
   TOTAL_COST               NUMBER(20,4),
   CURRENCY_CODE            VARCHAR2(3),
   PREVIOUSLY_APPROVED_IND  VARCHAR2(1),
   EDITABLE_IND             VARCHAR2(1)
);
/
CREATE OR REPLACE TYPE MBL_RPO_SEARCH_RES_TBL AS TABLE OF MBL_RPO_SEARCH_RES_REC;
/

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

DROP TYPE MBL_RPO_ORDER_ITEM_LOC_TBL FORCE;
/
DROP TYPE MBL_RPO_ORDER_ITEM_LOC_REC FORCE;
/

CREATE OR REPLACE TYPE MBL_RPO_ORDER_ITEM_LOC_REC AS OBJECT
(
   LOC                NUMBER(10),
   LOC_NAME           VARCHAR2(150),   
   QTY_ORDERED        NUMBER(12,4),
   TOTAL_COST         NUMBER(20,4),
   CURRENCY_CODE      VARCHAR2(3)
);
/
CREATE OR REPLACE TYPE MBL_RPO_ORDER_ITEM_LOC_TBL AS TABLE OF MBL_RPO_ORDER_ITEM_LOC_REC;
/

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

DROP TYPE MBL_RPO_ORDER_SUM_ITEM_TBL FORCE;
/
DROP TYPE MBL_RPO_ORDER_SUM_ITEM_REC FORCE;
/

CREATE OR REPLACE TYPE MBL_RPO_ORDER_SUM_ITEM_REC AS OBJECT
(
   ITEM               VARCHAR2(25),
   ITEM_DESC          VARCHAR2(250),
   DIFF_1             VARCHAR2(10),
   DIFF_1_DESC        VARCHAR2(250),
   DIFF_2             VARCHAR2(10),
   DIFF_2_DESC        VARCHAR2(250),   
   DIFF_3             VARCHAR2(10),
   DIFF_3_DESC        VARCHAR2(250),
   DIFF_4             VARCHAR2(10),
   DIFF_4_DESC        VARCHAR2(250),
   QTY_ORDERED        NUMBER(12,4),
   TOTAL_COST         NUMBER(20,4),
   CURRENCY_CODE      VARCHAR2(3),
   ITEM_IMAGE_URL     VARCHAR2(400)
);
/
CREATE OR REPLACE TYPE MBL_RPO_ORDER_SUM_ITEM_TBL AS TABLE OF MBL_RPO_ORDER_SUM_ITEM_REC;
/

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

DROP TYPE MBL_RPO_ORDER_SUM_OTB_TBL FORCE;
/
DROP TYPE MBL_RPO_ORDER_SUM_OTB_REC FORCE;
/

CREATE OR REPLACE TYPE MBL_RPO_ORDER_SUM_OTB_REC AS OBJECT
(
   DEPT                 NUMBER(4),
   CLASS                NUMBER(4),
   SUBCLASS             NUMBER(4),
   SUBCLASS_NAME        VARCHAR2(120),
   ORDER_AMOUNT         NUMBER(20,4),
   BUDGET_AMOUNT        NUMBER(20,4),
   RECEIVED_AMOUNT      NUMBER(20,4),   
   APPROVED_AMOUNT      NUMBER(20,4),   
   OUTSTANDING_AMOUNT   NUMBER(20,4)
);
/
CREATE OR REPLACE TYPE MBL_RPO_ORDER_SUM_OTB_TBL AS TABLE OF MBL_RPO_ORDER_SUM_OTB_REC;
/

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------


DROP TYPE MBL_RPO_ORDER_SUM_REC FORCE;
/

CREATE OR REPLACE TYPE MBL_RPO_ORDER_SUM_REC AS OBJECT
(
   ORDER_NO                NUMBER(12),
   STATUS                  VARCHAR2(1),
   SUPPLIER                NUMBER(10),
   SUP_NAME                VARCHAR2(240),
   NOT_BEFORE_DATE         DATE,
   NOT_AFTER_DATE          DATE,
   OTB_EOW_DATE            DATE,
   TERMS                   VARCHAR2(15),
   TERMS_CODE              VARCHAR2(50),
   TERMS_DESC              VARCHAR2(240),
   TOTAL_COST              NUMBER(20,4),
   TOTAL_RETAIL            NUMBER(20,4),
   CURRENCY_CODE           VARCHAR2(3),
   CREATE_ID               VARCHAR2(30),
   WRITTEN_DATE            DATE,   
   DEFAULT_DISPLAY_LEVEL   VARCHAR2(20),
   PREVIOUSLY_APPROVED_IND VARCHAR2(1),
   EDITABLE_IND            VARCHAR2(1),
   OTB_TABLE               MBL_RPO_ORDER_SUM_OTB_TBL
);
/
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

DROP TYPE MBL_RPO_FAIL_TBL FORCE;
/
DROP TYPE MBL_RPO_FAIL_REC FORCE;
/

CREATE OR REPLACE TYPE  MBL_RPO_FAIL_REC AS OBJECT
(
   ORDER_NO           NUMBER(12),
   ERROR_MESSAGE      VARCHAR2(255)
);
/
CREATE OR REPLACE TYPE MBL_RPO_FAIL_TBL AS TABLE OF MBL_RPO_FAIL_REC;
/

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
DROP TYPE MBL_RPO_STATUS_REC FORCE;
/

CREATE OR REPLACE TYPE MBL_RPO_STATUS_REC AS OBJECT
(
   SUCCESS_ORDERS_COUNT      NUMBER(10),
   SUCCESS_ORDERS_TBL        OBJ_NUMERIC_ID_TABLE,
   FAIL_ORDERS_COUNT         NUMBER(10),
   FAIL_ORDERS_TBL           MBL_RPO_FAIL_TBL
);
/