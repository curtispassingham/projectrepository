
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK


CREATE OR REPLACE FUNCTION SPLIT_PO_SQL (O_ERROR_MESSAGE                IN OUT VARCHAR2,
                                         I_ORDER_NO                     IN     VARCHAR2)
RETURN BINARY_INTEGER AUTHID CURRENT_USER AS
   L_program                VARCHAR2(50) := 'SPLIT_PO_SQL';
   L_return                 BINARY_INTEGER;

BEGIN
   L_return := SPLIT_WRAPPER_SQL.SPLIT_WRAPPER(I_order_no,
                                               O_error_message);
   return L_return;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return -1;
END;
/


CREATE OR REPLACE FUNCTION CREATE_PO_SQL (O_ERROR_MESSAGE                IN OUT VARCHAR2,
                                          I_SESSION_ID                   IN     VARCHAR2,
                                          I_APPROVAL_IND                 IN     VARCHAR2)
RETURN BINARY_INTEGER AUTHID CURRENT_USER AS
   L_program                VARCHAR2(50) := 'CREATE_PO_SQL';
   L_return                 BINARY_INTEGER;

BEGIN
   L_return := BUYER_BUILD_SQL.BUILD_PO_WRAPPER(I_session_id,
                                                I_approval_ind,
                                                O_error_message);
   return L_return;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return -1;
END;
/
