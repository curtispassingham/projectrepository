
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE FUNCTION SCALE_CONSTRAINTS_SQL(O_error_message                IN OUT VARCHAR2,
                                                 I_order_no                     IN     VARCHAR2)
RETURN BINARY_INTEGER AUTHID CURRENT_USER AS 

   L_program                VARCHAR2(50) := 'SCALE_CONSTRAINTS_SQL';
   L_return                 BINARY_INTEGER;
   
BEGIN
   L_return := SUP_CONSTRAINTS_SQL.SCALING_CNSTR(O_error_message,
                                                 I_order_no);
   /* Error Msg needs to be parsed before it is written on ord_inv_mgmt table */
   if O_error_message is NOT NULL and L_return = 1 then 
      O_error_message := SQL_LIB.PARSE_MSG(O_error_message);
   end if;
   return L_return;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return -1;
END;
/
