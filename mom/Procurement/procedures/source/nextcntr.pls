
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-----------------------------------------------------------------------------
-- Name:    NEXT_CONTRACT_NO
-- Purpose: This contract number sequence generator will return to the calling
--          module.  Upon success (TRUE) a contract number.  Upon failure 
--          (FALSE) an appropriate error message to be displayed
--	      by the calling module.
-----------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_CONTRACT_NO 
(contract_number	IN OUT		NUMBER,
return_code		IN OUT		VARCHAR2,
error_message		IN OUT		VARCHAR2) AUTHID CURRENT_USER IS

	L_contract_sequence		NUMBER(6);
	wrap_sequence_number		NUMBER(6);
	check_digit			NUMBER(1);
	first_time			VARCHAR2(1) := 'Y';
	dummy				VARCHAR2(1);

	CURSOR c_contract_exists (contract_number_param NUMBER) IS
	SELECT 'x'
	  FROM	contract_header
	 WHERE	contract_header.contract_no = contract_number_param;

	CURSOR c_sequence IS
	SELECT contract_sequence.NEXTVAL
	  FROM sys.dual; 

BEGIN
   LOOP
      OPEN c_sequence; 
      FETCH c_sequence into L_contract_sequence;
      CLOSE c_sequence; 

      IF (first_time = 'Y') THEN
         wrap_sequence_number := L_contract_sequence;
         first_time := 'N';
      ELSIF (L_contract_sequence = wrap_sequence_number) THEN
         error_message := 'Fatal error: no available contract numbers';
         return_code := 'FALSE';
         EXIT;
      END IF;	
			
      contract_number := L_contract_sequence;
      OPEN c_contract_exists(contract_number);
      FETCH c_contract_exists into dummy;
      IF (c_contract_exists%NOTFOUND) THEN
         return_code := 'TRUE';
         CLOSE c_contract_exists;
         EXIT;
      END IF;
      CLOSE c_contract_exists;
      END LOOP;
EXCEPTION
   WHEN OTHERS THEN
      error_message := SQLERRM || ' from NEXT_CONTRACT_NO proc.';
      return_code := 'FALSE';
END NEXT_CONTRACT_NO;
/ 


