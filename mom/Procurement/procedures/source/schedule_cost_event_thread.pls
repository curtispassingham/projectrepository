CREATE OR REPLACE PROCEDURE SCHEDULE_COST_EVENT_THREAD (I_cost_event_process_id  IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                                        I_thread_no              IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                                                        O_return_code               OUT NUMBER,
                                                        O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE) AUTHID CURRENT_USER AS

    job_name VARCHAR2(255);

BEGIN
   -- assign cost event id to job name
   job_name := 'cost_event_' || I_cost_event_process_id || '_thread_' || I_thread_no;

   -- create job
   DBMS_SCHEDULER.CREATE_JOB(
      job_name          =>  job_name,
      program_name      =>  'FUTURE_COST_THREAD',
      start_date        =>  SYSDATE,
      enabled           =>  FALSE,
      auto_drop         =>  TRUE);

   -- pass cost event id to job
   DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE (
      job_name                => job_name,
      argument_position       => 1,
      argument_value          => I_cost_event_process_id);

   -- pass thread number to job
   DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE (
      job_name                => job_name,
      argument_position       => 2,
      argument_value          => I_thread_no);

   -- run the job
   DBMS_SCHEDULER.ENABLE (job_name);
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQLERRM || ' from SCHEDULE_COST_EVENT_THREAD';
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
END;
/
