SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE FUNCTION APPLY_DEAL_PERFORM(O_error_message                 IN OUT VARCHAR2,
                                              I_deal_id                       IN     VARCHAR2,
                                              I_deal_detail_id                IN     VARCHAR2,
                                              I_amt_per_unit                  IN OUT VARCHAR2,
                                              I_old_period_turnover           IN     VARCHAR2,
                                              I_new_period_turnover           IN     VARCHAR2,
                                              I_convert_amt_per_unit          IN     VARCHAR2,
                                              I_update_forecast_unit_amt      IN     VARCHAR2,
                                              I_update_actual_fixed_totals    IN     VARCHAR2,
                                              I_upd_deal_detail_actual_total  IN     VARCHAR2,
                                              I_update_budget_fixed_totals    IN     VARCHAR2,
                                              I_upd_deal_detail_budget_total  IN     VARCHAR2,
                                              I_update_total_baseline         IN     VARCHAR2,
                                              I_update_turnover_trend         IN     VARCHAR2,
                                              I_forecast_income_calc          IN     VARCHAR2,
                                              I_deal_to_date_calcs            IN     VARCHAR2)
RETURN BINARY_INTEGER IS
   L_program              VARCHAR2(100) := 'APPLY_DEAL_PERFORM';

BEGIN
   if DEAL_INCOME_SQL.EXTERNAL_APPLY_DEAL_PERF(O_error_message,
                                               I_deal_id,
                                               I_deal_detail_id,
                                               I_amt_per_unit,
                                               I_old_period_turnover,
                                               I_new_period_turnover,
                                               I_convert_amt_per_unit,
                                               I_update_forecast_unit_amt,
                                               I_update_actual_fixed_totals,
                                               I_upd_deal_detail_actual_total,
                                               I_update_budget_fixed_totals,
                                               I_upd_deal_detail_budget_total,
                                               I_update_total_baseline,
                                               I_update_turnover_trend,
                                               I_forecast_income_calc,
                                               I_deal_to_date_calcs) = FALSE then
       return 1;
   end if;

   return 0;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
   return 1;
END APPLY_DEAL_PERFORM;
/