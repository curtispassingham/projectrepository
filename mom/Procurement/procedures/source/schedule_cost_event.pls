CREATE OR REPLACE PROCEDURE SCHEDULE_COST_EVENT (I_cost_event_process_id  IN      COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                                 O_return_code               OUT  NUMBER,
                                                 O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE) AUTHID CURRENT_USER AS

    job_name VARCHAR2(255);

BEGIN
   -- assign cost event id to job name
   job_name := 'cost_event_' || I_cost_event_process_id;

   -- create job
   DBMS_SCHEDULER.CREATE_JOB(
      job_name          =>  job_name,
      program_name      =>  'FUTURE_COST_PROG',
      start_date        =>  SYSDATE,
      enabled           =>  FALSE,
      auto_drop         =>  TRUE);

   -- pass cost event id to job
   DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE (
      job_name                => job_name,
      argument_position       => 1,
      argument_value          => I_cost_event_process_id);

   -- run the job
   DBMS_SCHEDULER.ENABLE (job_name);
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQLERRM || ' from SCHEDULE_COST_EVENT';
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
END;
/
