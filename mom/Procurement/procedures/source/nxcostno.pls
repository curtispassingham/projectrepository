
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
---------------------------------------------------------------------------------
-- Name:    NEXT_COSTCHG_NUMBER
-- Purpose: This Price Change number sequence generator will return to the calling 
--          program/procedure.  Upon success (TRUE) a Price Change number with a 
--          verified check digit attached.  Upon failure (FALSE) an appropriate 
--          error message for display purposes by the calling program/procedure.
----------------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE NEXT_COSTCHG_NUMBER(O_cost_change_number     IN OUT  NUMBER,
                                                O_return_code            IN OUT  VARCHAR2,
                                                O_error_message          IN OUT  VARCHAR2) AUTHID CURRENT_USER IS

   L_wrap_sequence_number   NUMBER;
   L_first_time             VARCHAR2(3) := 'Y';
   L_dummy                  VARCHAR2(1);
   L_program                VARCHAR2(50) := 'NEXT_COSTCHG_NUMBER';
   L_cc_number              NUMBER;
   
   cursor C_CC_EXISTS is
      SELECT DISTINCT 'x'
        FROM cost_susp_sup_head 
       WHERE cost_change =  L_cc_number;       
       
BEGIN
   LOOP
      SELECT cc_sequence.NEXTVAL
        INTO L_cc_number 
        FROM sys.dual;
      ---
      if (L_first_time = 'Y') then
         L_wrap_sequence_number := L_cc_number;
         L_first_time := 'N';
      elsif (L_cc_number = L_wrap_sequence_number) then
         O_error_message := 'INV_COST_CHANGE';
         O_return_code := 'FALSE';
         EXIT;
      end if;
      ---
      SQL_LIB.SET_MARK('open',
                       'C_CC_EXISTS',
                       'cost_susp_sup_head',
                       NULL);                                            

      open  C_CC_EXISTS;
      ---
      L_dummy := NULL;
      ---
      SQL_LIB.SET_MARK('fetch',
                       'C_CC_EXISTS',
                       'cost_susp_sup_head',
                       NULL);                                            
      fetch C_CC_EXISTS into L_dummy;
      ---
      if (L_dummy is NULL) then
         O_cost_change_number := L_cc_number;
         O_return_code := 'TRUE';
         close C_CC_EXISTS;
         EXIT;
      end if;
      ---
      SQL_LIB.SET_MARK('close',
                       'C_CC_EXISTS',
                       'cost_susp_sup_head',
                       NULL);                                            
      close C_CC_EXISTS;
        
   END LOOP;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      O_return_code := 'FALSE';
END NEXT_COSTCHG_NUMBER;
/


