
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PROCEDURE CHK_REV_DEPENDENCY_EXISTS (I_order_no        IN      ORDHEAD.ORDER_NO%TYPE,
                                                       I_rev_days        IN      SYSTEM_OPTIONS.EDI_REV_DAYS%TYPE,
                                                       I_vdate           IN      PERIOD.VDATE%TYPE,
                                                       O_exists          IN OUT  VARCHAR2,
                                                       O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE) AUTHID CURRENT_USER AS

   L_obligation         OBLIGATION.OBLIGATION_KEY%TYPE;
   L_custom_id          CE_ORD_ITEM.CE_ID%TYPE;
   L_exists             VARCHAR2(1) :='N';
   L_program            VARCHAR2(50) := 'CHK_REV_DEPENDENCY_EXISTS';
   
   cursor C_OBLIGATION_KEY IS
         SELECT distinct obligation_key
           FROM alc_head
          WHERE order_no = TO_NUMBER(I_order_no)
            AND obligation_key IS NOT NULL;
   
   cursor C_CUSTOM_ID IS
         SELECT distinct ce_id
           FROM alc_head
          WHERE order_no = TO_NUMBER(I_order_no)
            AND ce_id IS NOT NULL;
   
   cursor C_ORD_OBL_REV_DPND IS
         SELECT 'Y'
           FROM alc_head
          WHERE order_no <> TO_NUMBER(I_order_no)
            AND obligation_key = L_obligation
            AND order_no NOT IN 
                (SELECT oh.order_no          
                   FROM ordhead oh,
                        ordlc lc
                  WHERE oh.status = 'C'
                    AND lc.order_no(+) = oh.order_no
                    AND (TO_DATE(I_vdate,'YYYYMMDD') - I_rev_days) >
                        NVL(oh.close_date, TO_DATE(I_vdate,'YYYYMMDD')));
                  
   cursor C_ORD_CUS_REV_DPND is
         SELECT 'Y'
           FROM alc_head
          WHERE order_no <> TO_NUMBER(I_order_no)
            AND ce_id = L_custom_id
            AND order_no NOT IN 
                (SELECT oh.order_no          
                   FROM ordhead oh,
                        ordlc lc
                  WHERE oh.status = 'C'
                    AND lc.order_no(+) = oh.order_no
                    AND (TO_DATE(I_vdate,'YYYYMMDD') - I_rev_days) >
                        NVL(oh.close_date, TO_DATE(I_vdate,'YYYYMMDD')));
                                  
BEGIN

   OPEN  C_OBLIGATION_KEY;
   
   LOOP
   FETCH C_OBLIGATION_KEY into L_obligation;
      
      if L_obligation IS NULL then
         OPEN C_CUSTOM_ID;
         
         LOOP 
         FETCH C_CUSTOM_ID into L_custom_id;
         EXIT WHEN C_CUSTOM_ID%NOTFOUND;
         
            OPEN C_ORD_CUS_REV_DPND;
            
            FETCH C_ORD_CUS_REV_DPND into L_exists;
            
            CLOSE C_ORD_CUS_REV_DPND;
         
            EXIT WHEN L_exists = 'Y';
         END LOOP;
         
         CLOSE C_CUSTOM_ID;
      
      else
      
         OPEN  C_ORD_OBL_REV_DPND;
         
         FETCH C_ORD_OBL_REV_DPND into L_exists;
         
         CLOSE C_ORD_OBL_REV_DPND;
         
         EXIT WHEN L_exists = 'Y';
      
      end if;
      
   EXIT WHEN C_OBLIGATION_KEY%NOTFOUND;
   
   END LOOP;
   
   CLOSE C_OBLIGATION_KEY;
      
   if L_exists IS NULL then
      O_exists := 'N';
   else 
      O_exists := L_exists;
   end if;
   
EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
END CHK_REV_DEPENDENCY_EXISTS;
/
