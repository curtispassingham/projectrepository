
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------------------------------
-- Name:    NEXT_ORDER_NUMBER
-- Purpose: This order number sequence generator will return to the calling 
--          program/procedure.  Upon success (TRUE) an order number with a 
--          verified check digit attached.  Upon failure (FALSE) an appropriate 
--          error message for display purposes by the calling program/procedure.
--------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_ORDER_NUMBER (O_order_number  IN OUT ORDHEAD.ORDER_NO%TYPE,
                                               O_return_code   IN OUT VARCHAR2,
                                               O_error_message IN OUT VARCHAR2) AUTHID CURRENT_USER IS

   L_order_sequence         ORDHEAD.ORDER_NO%TYPE;
   L_wrap_sequence_number   ORDHEAD.ORDER_NO%TYPE;
   L_first_time             VARCHAR2(3) := 'Yes';
   L_dummy                  VARCHAR2(1);

   CURSOR c_order_exists(order_number_param ORDHEAD.ORDER_NO%TYPE) IS
       SELECT DISTINCT'x'
         FROM ordhead
        WHERE ordhead.order_no = order_number_param;

BEGIN
    LOOP
        SELECT order_sequence.NEXTVAL
          INTO L_order_sequence
          FROM sys.dual;

        IF (L_first_time = 'Yes') THEN
            L_wrap_sequence_number := L_order_sequence;
            L_first_time := 'No';
        ELSIF (L_order_sequence = L_wrap_sequence_number) THEN
            O_error_message := 'Fatal error - no available order numbers';
            O_return_code := 'FALSE';
            EXIT;
        END IF;

        O_order_number := L_order_sequence;

        OPEN  c_order_exists(O_order_number);
        FETCH c_order_exists into L_dummy;
        IF (c_order_exists%notfound) THEN
            O_return_code := 'TRUE';
            CLOSE c_order_exists;
            EXIT;
        END IF;
        CLOSE c_order_exists;
    END LOOP;
EXCEPTION
    WHEN OTHERS THEN
        O_error_message := SQLERRM || ' from NEXT_ORDER_NO proc.';
        O_return_code := 'FALSE';
END NEXT_ORDER_NUMBER;
/
