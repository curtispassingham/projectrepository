CREATE OR REPLACE FUNCTION APPLY_DEALS_TO_ORDER(O_error_message         IN OUT VARCHAR2,
                                                I_order_no              IN     VARCHAR2,
                                                I_order_appr_ind        IN     VARCHAR2,
                                                I_recalc_all_ind        IN     VARCHAR2,
                                                I_override_manual_ind   IN     VARCHAR2)
RETURN BINARY_INTEGER AUTHID CURRENT_USER IS

   L_program   VARCHAR2(30)   := 'APPLY_DEALS_TO_ORDER';

BEGIN
   if DEAL_ORD_LIB_SQL.EXTERNAL_SHARE_APPLY_DEALS(O_error_message,
                                                  I_order_no,
                                                  I_order_appr_ind,
                                                  I_recalc_all_ind,
                                                  I_override_manual_ind) = FALSE then
      return -1;
   end if;

   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program, 
                                             TO_CHAR(SQLCODE));
      return -1;
END APPLY_DEALS_TO_ORDER;
/