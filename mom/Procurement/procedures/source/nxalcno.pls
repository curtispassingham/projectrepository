
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------------------------------
-- Name:    NEXT_ALLOC_NO
-- Purpose: This allocation number sequence generator will return to the calling 
--          program/procedure.  Upon success (TRUE) an alloc number with a 
--          verified check digit attached.  Upon failure (FALSE) an appropriate 
--          error message for display purposes by the calling program/procedure.
--------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NEXT_ALLOC_NO 
                       (O_alloc_no      IN OUT alloc_header.alloc_no%TYPE,
                        O_return_code   IN OUT VARCHAR2,
                        O_error_message IN OUT VARCHAR2) AUTHID CURRENT_USER IS

   L_alloc_sequence       alloc_header.alloc_no%TYPE;
   L_wrap_sequence_number alloc_header.alloc_no%TYPE;
   L_check_digit          NUMBER(1);
   L_first_time           VARCHAR2(3) := 'Yes';
   L_dummy                VARCHAR2(1);

   cursor C_ALLOC_EXISTS(alloc_number_param NUMBER) is
      select 'x'
         from alloc_header 
         where alloc_header.alloc_no = alloc_number_param;

   cursor C_ALLOC_ORDER_SEQUENCE is
     select alloc_order_sequence.NEXTVAL
       from sys.dual;

BEGIN
   LOOP
      open C_ALLOC_ORDER_SEQUENCE;
      fetch C_ALLOC_ORDER_SEQUENCE into L_alloc_sequence;
      close C_ALLOC_ORDER_SEQUENCE;

      if (L_first_time = 'Yes') then
         L_wrap_sequence_number := L_alloc_sequence;
         L_first_time := 'No';
      elsif (L_alloc_sequence = L_wrap_sequence_number) then
         O_error_message := 'Fatal error - no available alloc numbers';
         O_return_code := 'FALSE';
         exit;
      end if;

      O_alloc_no := L_alloc_sequence;

      open c_alloc_exists(O_alloc_no);
      fetch c_alloc_exists into L_dummy;
      if (c_alloc_exists%notfound) then
         O_return_code := 'TRUE';
         close c_alloc_exists;
         exit;
      end if;
      close c_alloc_exists;
   end LOOP;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'NEXT_ALLOC_NO',
                                            to_char(SQLCODE));
      O_return_code := 'FALSE';
END NEXT_ALLOC_NO;
/
