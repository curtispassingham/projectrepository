CREATE OR REPLACE PACKAGE COMP_RT_UPD_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
--Function Name: IS_UNIQUE
--Purpose      : This Function will check if the input ia unique
---------------------------------------------------------------------------------------------
FUNCTION IS_UNIQUE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists            IN OUT BOOLEAN ,
                   I_defaulting_level  IN     COST_COMP_UPD_STG.DEFAULTING_LEVEL%TYPE,
                   I_effective_date    IN     COST_COMP_UPD_STG.EFFECTIVE_DATE%TYPE,
                   I_comp_id           IN     COST_COMP_UPD_STG.COMP_ID%TYPE,
                   I_dept              IN     COST_COMP_UPD_STG.DEPT%TYPE,
                   I_from_loc          IN     COST_COMP_UPD_STG.FROM_LOC%TYPE,
                   I_to_loc            IN     COST_COMP_UPD_STG.TO_LOC%TYPE,
                   I_exp_prof_key      IN     COST_COMP_UPD_STG.EXP_PROF_KEY%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name: GET_NEXT_SEQUENCE
--Purpose      : This Function will return the next available sequence number
---------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQUENCE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_seq_no            IN OUT COST_COMP_UPD_STG.SEQ_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name: GET_DEFAULT_VALUES 
--Purpose      : Returns the values of the record with the previous effective_date
---------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_VALUES(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists            IN OUT BOOLEAN, 
                            O_comp_rate         IN OUT COST_COMP_UPD_STG.NEW_COMP_RATE%TYPE,
                            O_comp_currency     IN OUT ELC_COMP.COMP_CURRENCY%TYPE,
                            O_per_count         IN OUT ELC_COMP.PER_COUNT%TYPE,
                            O_per_count_uom     IN OUT ELC_COMP.PER_COUNT_UOM%TYPE,
                            I_defaulting_level  IN     COST_COMP_UPD_STG.DEFAULTING_LEVEL%TYPE,
                            I_comp_id           IN     COST_COMP_UPD_STG.COMP_ID%TYPE,
                            I_dept              IN     COST_COMP_UPD_STG.DEPT%TYPE,
                            I_from_loc          IN     COST_COMP_UPD_STG.FROM_LOC%TYPE,
                            I_to_loc            IN     COST_COMP_UPD_STG.TO_LOC%TYPE,
                            I_exp_prof_key      IN     COST_COMP_UPD_STG.EXP_PROF_KEY%TYPE,
                            I_effective_date    IN     COST_COMP_UPD_STG.EFFECTIVE_DATE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name: UPDATE_OLD_VALUES
--Purpose      : Returns the values of the record with the previous effective_date
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_OLD_VALUES(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_comp_rate         IN     COST_COMP_UPD_STG.NEW_COMP_RATE%TYPE,
                           I_comp_currency     IN     ELC_COMP.COMP_CURRENCY%TYPE,
                           I_per_count         IN     ELC_COMP.PER_COUNT%TYPE,
                           I_per_count_uom     IN     ELC_COMP.PER_COUNT_UOM%TYPE,
                           I_defaulting_level  IN     COST_COMP_UPD_STG.DEFAULTING_LEVEL%TYPE,
                           I_comp_id           IN     COST_COMP_UPD_STG.COMP_ID%TYPE,
                           I_dept              IN     COST_COMP_UPD_STG.DEPT%TYPE,
                           I_from_loc          IN     COST_COMP_UPD_STG.FROM_LOC%TYPE,
                           I_to_loc            IN     COST_COMP_UPD_STG.TO_LOC%TYPE,
                           I_exp_prof_key      IN     COST_COMP_UPD_STG.EXP_PROF_KEY%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END COMP_RT_UPD_SQL;
/
