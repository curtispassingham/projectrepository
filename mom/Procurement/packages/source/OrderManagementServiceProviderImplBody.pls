/******************************************************************************
* Service Name     : PurchaseOrderManagementService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/PurchaseOrderManagementService/v1
* Description      : Order Management.
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE BODY PurchaseOrderManagementService AS


/******************************************************************************
 *
 * Operation       : preIssueOrderNumber
 * Description     : Generate pre-issued order numbers for the calling application.
 *
 * Input           : "RIB_OrdNumCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/OrdNumCriVo/v1
 * Description     : Order number request object contains the details of the pre-issued order numbers to generate in RMS.
 *
 * Output          : "RIB_OrdNumColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/OrdNumColDesc/v1
 * Description     : OrdNumColDesc object is a collection of Pre-Issued Order Numbers generated in RMS.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 *
 ******************************************************************************/
PROCEDURE preIssueOrderNumber(I_serviceOperationContext   IN OUT   "RIB_ServiceOpContext_REC",
                              I_businessObject            IN       "RIB_OrdNumCriVo_REC",
                              O_serviceOperationStatus       OUT   "RIB_ServiceOpStatus_REC",
                              O_businessObject               OUT   "RIB_OrdNumColDesc_REC")
IS

   L_program         VARCHAR2(60)             := 'PurchaseOrderManagementService.preIssueOrderNumber';
   L_success_rec     "RIB_SuccessStatus_REC"  := NULL;
   L_success_tbl     "RIB_SuccessStatus_TBL"  := "RIB_SuccessStatus_TBL"();
   L_fail_rec        "RIB_FailStatus_REC"     := NULL;
   L_fail_tbl        "RIB_FailStatus_TBL"     := "RIB_FailStatus_TBL"();
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN

   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;

   if CORESVC_ORD_PREISSUE_SQL.GENERATE_ORDER_NOS(L_error_message,
                                                  O_businessObject,
                                                  I_serviceOperationContext,
                                                  I_businessObject) = FALSE then
      L_fail_rec := "RIB_FailStatus_REC"(0,
                                         L_error_message,
                                         'error',
                                         'error',
                                         NULL);
      L_fail_tbl.EXTEND;
      L_fail_tbl(L_fail_tbl.COUNT) := L_fail_rec;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, 
                                                            NULL,
                                                            L_fail_tbl);
      return;
   end if;

   L_success_rec := "RIB_SuccessStatus_REC"(0,
                                            'preIssueOrderNumber service call was successful.');
   L_success_tbl.EXTEND;
   L_success_tbl(L_success_tbl.COUNT) := L_success_rec;
   O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0,
                                                         L_success_tbl);

EXCEPTION
  when OTHERS then
     L_fail_rec := "RIB_FailStatus_REC"(0,
                                        SQLERRM,
                                        'error',
                                        'error',
                                        NULL);
     L_fail_tbl.EXTEND;
     L_fail_tbl(L_fail_tbl.COUNT) := L_fail_rec;
     O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0,
                                                           NULL,
                                                           L_fail_tbl);

END preIssueOrderNumber;
/******************************************************************************/

END PurchaseOrderManagementService;
/
