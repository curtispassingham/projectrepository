CREATE OR REPLACE PACKAGE BODY ORDER_VALIDATE_SQL AS
---------------------------------------------------------------------
FUNCTION ORDLOC_WKSHT_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_diff          IN OUT VARCHAR2,
                                  I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_count_order    NUMBER(6);
   L_count_vorder   NUMBER(6);
   L_program        VARCHAR2(50) := 'ORDER_VALIDATE_SQL.ORDLOC_WKSHT_FILTER_LIST';

   cursor C_COUNT_ORDER IS
      select COUNT(order_no)
        from ordloc_wksht
       where order_no = I_order_no;

   cursor C_COUNT_VORDER IS
      select count(1)
        from ordloc_wksht ow, 
             v_ordhead vo, 
             v_item_master vi
       where ow.order_no = I_order_no
         and ow.order_no = vo.order_no
         and nvl(ow.item, ow.item_parent) = vi.item
         and (ow.location is null
              or exists (select /*+rule*/ 'x'
                           from v_location vl 
                          where vl.location_id = ow.location));

BEGIN
   ---
   open C_COUNT_ORDER;
   fetch C_COUNT_ORDER into L_count_order;
   close C_COUNT_ORDER;
   ---
   open C_COUNT_VORDER;
   fetch C_COUNT_VORDER into L_count_vorder;
   close C_COUNT_VORDER;
   ---
   if NVL(L_count_vorder,0) != NVL(L_count_order,0) then
      O_diff := 'Y';
   else
      O_diff := 'N';
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ORDLOC_WKSHT_FILTER_LIST;
---------------------------------------------------------------------
FUNCTION ORDER_EXISTS(O_error_message  IN OUT VARCHAR2,
                      O_exists         IN OUT BOOLEAN,
                      I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_exists   VARCHAR2(1) := 'N';

   cursor C_ORDER is
      select 'Y'
        from ordhead
       where order_no = I_order_no
         and rownum = 1;

BEGIN

   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ORDER','ORDHEAD','ORDER_NO: '||to_char(I_order_no));
   open C_ORDER;
   SQL_LIB.SET_MARK('FETCH','C_ORDER','ORDHEAD','ORDER_NO: '||to_char(I_order_no));
   fetch C_ORDER into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_ORDER','ORDHEAD','ORDER_NO: '||to_char(I_order_no));
   close C_ORDER;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_VALIDATE_SQL.ORDER_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END ORDER_EXISTS;
---------------------------------------------------------------------
FUNCTION CHECK_DETAIL_RECS(O_error_message    IN OUT  VARCHAR2,
                           O_cust_details_ind IN OUT  VARCHAR2,
                           O_items_ind        IN OUT  VARCHAR2,
                           I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                           I_customer_ind     IN      VARCHAR2)
RETURN BOOLEAN IS

   L_exists  VARCHAR2(1) := 'N';

   cursor C_DETAIL is
      select 'Y'
        from ordsku
       where order_no = I_order_no
         and rownum = 1;

BEGIN
   if I_customer_ind = 'Y' then
      if CHECK_CUST_RECS (O_error_message,
                          O_cust_details_ind,
                          I_order_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   open  C_DETAIL;
   fetch C_DETAIL into L_exists;
   close C_DETAIL;
   ---
   if L_exists = 'N' then
      O_items_ind := 'N';
   else
      O_items_ind := 'Y';
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        'ORDER_VALIDATE_SQL.CHECK_DETAIL_RECS',
                        to_char(SQLCODE));
      RETURN FALSE;
END CHECK_DETAIL_RECS;
----------------------------------------------------------------------
FUNCTION CHECK_CUST_RECS (O_error_message    IN OUT VARCHAR2,
                          O_cust_details_ind IN OUT VARCHAR2,
                          I_order_no         IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_record   VARCHAR2(1) := 'N';

   cursor C_ORDCUST is
      select 'Y'
        from ordcust
       where order_no = I_order_no;

BEGIN
   open  C_ORDCUST;
   fetch C_ORDCUST into L_record;
   close C_ORDCUST;
   ---
   if L_record = 'N' then
      O_cust_details_ind := 'N';
   else
      O_cust_details_ind := 'Y';
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        'ORDER_VALIDATE_SQL.CHECK_CUST_RECS',
                        to_char(SQLCODE));
      return FALSE;
END CHECK_CUST_RECS;
-----------------------------------------------------------------------
FUNCTION CHECK_ALLOC_QTY (O_error_message      IN OUT   VARCHAR2,
                          O_alloc_excess_ind   IN OUT   VARCHAR2,
                          I_order_no           IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_location       ORDLOC.LOCATION%TYPE;
   L_qty_ordered    ORDLOC.QTY_ORDERED%TYPE;
   L_item           ITEM_MASTER.ITEM%TYPE;
   L_qty_alloc      ALLOC_DETAIL.QTY_ALLOCATED%TYPE;
   L_dummy          VARCHAR2(1);

   cursor C_ALLOC_EXISTS is
      select 'x'
        from alloc_header
       where order_no = I_order_no
         and status <> 'C';

   cursor C_QTY_ORDERED is
      select location,
             qty_ordered,
             item
        from ordloc
       where loc_type = 'W'
         and order_no = I_order_no;

   cursor C_QTY_ALLOCATED is
      select NVL(SUM(d.qty_allocated),0)
        from alloc_detail d,
             alloc_header h
       where h.item     = L_item
         and h.wh       = L_location
         and h.order_no = I_order_no
         and h.alloc_no = d.alloc_no;

BEGIN
   O_alloc_excess_ind := 'N';
   ---
   open C_ALLOC_EXISTS;
   fetch C_ALLOC_EXISTS into L_dummy;
   if C_ALLOC_EXISTS%NOTFOUND then
      close C_ALLOC_EXISTS;
      return TRUE;
   else
      close C_ALLOC_EXISTS;
   end if;
   ---
   FOR C_QTY_ORDERED_REC in C_QTY_ORDERED LOOP
      L_qty_ordered   := C_QTY_ORDERED_REC.qty_ordered;
      L_location      := C_QTY_ORDERED_REC.location;
      L_item          := C_QTY_ORDERED_REC.item;
      ---
      open  C_QTY_ALLOCATED;
      fetch C_QTY_ALLOCATED into L_qty_alloc;
      close C_QTY_ALLOCATED;
      ---
      if L_qty_ordered < L_qty_alloc then
         O_alloc_excess_ind := 'Y';
         EXIT;
      elsif L_qty_ordered > L_qty_alloc then
         O_alloc_excess_ind := 'L';
         EXIT;
     end if;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        'ORDER_VALIDATE_SQL.CHECK_ALLOC_QTY',
                        to_char(SQLCODE));
      return FALSE;
END CHECK_ALLOC_QTY;
-----------------------------------------------------------------------
FUNCTION COUNTRY_EXISTS(O_error_message    IN OUT VARCHAR2,
                        O_exists           IN OUT BOOLEAN,
                        I_country_id       IN     COUNTRY.COUNTRY_ID%TYPE,
                        I_table_name       IN     VARCHAR2,
                        I_order_no         IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_dummy  VARCHAR2(1);

   cursor C_ORDER_WORKSHEET is
      select 'x'
        from ordloc_wksht
       where order_no          = I_order_no
         and origin_country_id = I_country_id;

BEGIN
   O_exists := TRUE;
   ---
   ---if the table name passed in is ORDLOC_WKSHT then check that the country passed in
   ---exists on that table
   if I_table_name = 'ORDLOC_WKSHT' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_ORDER_WORKSHEET','ORDLOC_WKSHT',
                       'order no: '||to_char(I_order_no)||
                       ', origin country: '||I_country_id);
      open C_ORDER_WORKSHEET;
      ---
      SQL_LIB.SET_MARK('FETCH','C_ORDER_WORKSHEET','ORDLOC_WKSHT',
                       'order no: '||to_char(I_order_no)||
                       ', origin country: '||I_country_id);

      fetch C_ORDER_WORKSHEET into L_dummy;
      if C_ORDER_WORKSHEET%NOTFOUND then
         O_exists := FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_ORDER_WORKSHEET','ORDLOC_WKSHT',
                       'order no: '||to_char(I_order_no)||
                       ', origin country: '||I_country_id);

      close C_ORDER_WORKSHEET;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        'ORDER_VALIDATE_SQL.COUNTRY_EXISTS',
                        to_char(SQLCODE));
      return FALSE;
END COUNTRY_EXISTS;
-----------------------------------------------------------------------
FUNCTION VALID_TO_TICKET_AT_LOC(O_error_message    IN OUT VARCHAR2,
                                O_valid            IN OUT BOOLEAN,
                                I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                                I_location         IN     ORDLOC.LOCATION%TYPE,
                                I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_exists          VARCHAR2(1) := NULL;

   cursor C_CHECK_STATUS is
      select 'x'
        from ordhead
       where order_no = I_order_no
         and status   = 'A';

   cursor C_CHECK_DIRECT_ORDER is
      select 'x'
        from ordhead oh,
             ordloc ol
       where oh.order_no     = I_order_no
         and ol.order_no     = oh.order_no
         and ol.location     = I_location
         and ol.loc_type     = I_loc_type
         and ol.qty_ordered != 0;

   cursor C_CHECK_ALLOCATION is
      select 'x'
        from ordhead oh,
             alloc_header ah,
             alloc_detail ad
        where oh.order_no       = I_order_no
          and ah.order_no       = oh.order_no
          and ah.alloc_no       = ad.alloc_no
          and ad.to_loc         = I_location
          and ad.to_loc_type    = I_loc_type
          and ad.qty_allocated != 0;
BEGIN
   if I_order_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_location IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_loc_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'loc_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                      'C_CHECK_STATUS',
                      'ordhead',
                      'Order_no: '||to_char(I_order_no));
   open C_CHECK_STATUS;
   SQL_LIB.SET_MARK('FETCH',
                      'C_CHECK_STATUS',
                      'ordhead',
                      'Order_no: '||to_char(I_order_no));
   fetch C_CHECK_STATUS into L_exists;
   SQL_LIB.SET_MARK('CLOSE',
                      'C_CHECK_STATUS',
                      'ordhead',
                      'Order_no: '||to_char(I_order_no));
   close C_CHECK_STATUS;
   ---
   if L_exists is NULL then
      O_valid := FALSE;
      return TRUE;
   end if;
   ---
   L_exists := NULL;
   ---
   -- check if stock ordered directly to a location
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_DIRECT_ORDER',
                    'ordhead, ordloc',
                    'Order no: '||to_char(I_order_no));
   open C_CHECK_DIRECT_ORDER;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_DIRECT_ORDER',
                    'ordhead, ordloc',
                    'Order no: '||to_char(I_order_no));
   fetch C_CHECK_DIRECT_ORDER into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_DIRECT_ORDER',
                    'ordhead, ordloc',
                    'Order no: '||to_char(I_order_no));
   close C_CHECK_DIRECT_ORDER;

   if L_exists is NULL then -- check if stock allocated to a location
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_ALLOCATION',
                       'ordhead, alloc_header, alloc_detail',
                       'Order no: '||to_char(I_order_no)||' Location: '||to_char(I_location));
      open C_CHECK_ALLOCATION;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_ALLOCATION',
                       'ordhead, alloc_header, alloc_detail',
                       'Order no: '||to_char(I_order_no)||' Location: '||to_char(I_location));
      fetch C_CHECK_ALLOCATION into L_exists;


      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_ALLOCATION',
                       'ordhead, alloc_header, alloc_detail',
                       'Order no: '||to_char(I_order_no)||' Location: '||to_char(I_location));
      close C_CHECK_ALLOCATION;

      if L_exists is NULL then
         O_valid := FALSE;
         return TRUE;
      end if;
   end if;

   O_valid := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        'ORDER_VALIDATE_SQL.VALID_TO_TICKET_AT_LOC',
                        to_char(SQLCODE));
      RETURN FALSE;
END VALID_TO_TICKET_AT_LOC;
-----------------------------------------------------------------------
FUNCTION SPLIT_REF_ORDNO_EXISTS(O_error_message     IN OUT VARCHAR2,
                                O_exists            IN OUT BOOLEAN,
                                I_split_ref_ordno   IN     ORDHEAD.SPLIT_REF_ORDNO%TYPE)
   return BOOLEAN is

   L_dummy   VARCHAR2(1) := 'N';

   cursor C_EXISTS is
      select 'x'
        from ordhead
       where split_ref_ordno = I_split_ref_ordno;

BEGIN
   if I_split_ref_ordno is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_split_ref_ordno',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   O_exists := TRUE;

   SQL_LIB.SET_MARK('OPEN','C_EXISTS','ORDHEAD','SPLIT REF ORDER NO: '||to_char(I_split_ref_ordno));
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_EXISTS','ORDHEAD','SPLIT REF ORDER NO: '||to_char(I_split_ref_ordno));
   fetch C_EXISTS into L_dummy;
   ---
   if C_EXISTS%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','ORDHEAD','SPLIT REF ORDER NO: '||to_char(I_split_ref_ordno));
   close C_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_VALIDATE_SQL.SPLIT_REF_ORDNO_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END SPLIT_REF_ORDNO_EXISTS;
---------------------------------------------------------------------
FUNCTION VALID_TO_SPLIT_ORDER(O_error_message   IN OUT   VARCHAR2,
                              O_valid           IN OUT   BOOLEAN,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN is

   L_status                ORDHEAD.STATUS%TYPE;
   L_orig_ind              ORDHEAD.ORIG_IND%TYPE;
   L_contract_no           ORDHEAD.CONTRACT_NO%TYPE;
   L_alloc_exists          BOOLEAN;
   L_asn_exists            BOOLEAN;
   L_buyer_pack_exists     BOOLEAN;
   L_ordloc_exists         BOOLEAN;
   L_location              ORDLOC.LOCATION%TYPE;
   L_loc_type              ORDLOC.LOC_TYPE%TYPE;
   L_multiple_locs_exist   BOOLEAN;
   L_virtual_wh            WH.WH%TYPE;
   L_truck_split_ind       ORD_INV_MGMT.TRUCK_SPLIT_IND%TYPE;
   L_orig_approval_date    ORDHEAD.ORIG_APPROVAL_DATE%TYPE;
   L_order_type            ORDHEAD.ORDER_TYPE%TYPE;


   cursor C_ORDHEAD_INFO is
      select status,
             orig_ind,
             contract_no,
             orig_approval_date,
             order_type
        from ordhead
       where order_no = I_order_no;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   O_valid := TRUE;

   open C_ORDHEAD_INFO;
   fetch C_ORDHEAD_INFO into L_status,
                             L_orig_ind,
                             L_contract_no,
                             L_orig_approval_date,
                             L_order_type;
   close C_ORDHEAD_INFO;

   if L_status != 'W' then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_SPLIT_NON_WS_PO',
                                            NULL,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;

   if L_orig_ind = 5 then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_SPLIT_VNDR_GEN_PO',
                                            NULL,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;

   if L_contract_no is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_SPLIT_CONTRACT_PO',
                                            NULL,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;

   if L_orig_approval_date is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_SPLIT_APPROV_ORD',
                                            NULL,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;

   if L_order_type = 'CO' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_SPLIT_CUST_ORD',
                                            NULL,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;

   if ALLOC_EXISTS(O_error_message,
                   L_alloc_exists,
                   L_asn_exists,
                   I_order_no) = FALSE then
      return FALSE;
   end if;

   if L_alloc_exists = TRUE then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_SPLIT_ALLOC_PO',
                                            NULL,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;

   if ORDER_ATTRIB_SQL.CHECK_BUYER_PACKS(O_error_message,
                                         L_buyer_pack_exists,
                                         I_order_no) = FALSE then
      return FALSE;
   end if;

   if L_buyer_pack_exists = TRUE then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_SPLIT_BUYER_PK_PO',
                                            NULL,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;

   if ORDER_ITEM_ATTRIB_SQL.ORDLOC_EXISTS(O_error_message,
                                          L_ordloc_exists,
                                          I_order_no) = FALSE then
      return FALSE;
   end if;

   if L_ordloc_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_SPLIT_NO_LOCS',
                                            NULL,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;


   if ORDER_ATTRIB_SQL.MULTIPLE_LOCS_EXIST(O_error_message,
                                           L_location,
                                           L_loc_type,
                                           L_multiple_locs_exist,
                                           L_virtual_wh,
                                           I_order_no) = FALSE then
      return FALSE;
   end if;

   if L_multiple_locs_exist = TRUE then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_SPLIT_MULT_LOCS_PO',
                                            NULL,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;

   if ORD_INV_MGMT_SQL.GET_TRUCK_SPLIT_IND(O_error_message,
                                           L_truck_split_ind,
                                           I_order_no) = FALSE then
      return FALSE;
   end if;

   if L_truck_split_ind != 'Y' or L_truck_split_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_SPLIT_NO_CNSTR',
                                            NULL,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_VALIDATE_SQL.VALID_TO_SPLIT_ORDER',
                                             to_char(SQLCODE));
      return FALSE;
END VALID_TO_SPLIT_ORDER;
---------------------------------------------------------------------
FUNCTION ALLOC_EXISTS(O_error_message   IN OUT   VARCHAR2,
                      O_exists          IN OUT   BOOLEAN,
                      O_ASN_exists      IN OUT   BOOLEAN,
                      I_order_no        IN       ALLOC_HEADER.ORDER_NO%TYPE)
   return BOOLEAN is

   L_dummy        VARCHAR2(1);
   L_asn_exists   VARCHAR2(1);
   
   cursor C_ALLOC_EXISTS is
      select 'x'
        from alloc_header
       where order_no = I_order_no
         and rownum = 1;
         
   cursor C_CHECK_ASN_ALLOC is
      select 'x'
        from alloc_header
       where order_no = I_order_no
         and doc_type = 'ASN'
         and rownum = 1;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_ALLOC_EXISTS',
                    'ALLOC_HEADER',
                    'order_no = '||to_char(I_order_no));
   open C_ALLOC_EXISTS;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_ALLOC_EXISTS',
                    'ALLOC_HEADER',
                    'order_no = '||to_char(I_order_no));
   fetch C_ALLOC_EXISTS into L_dummy;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ALLOC_EXISTS',
                    'ALLOC_HEADER',
                    'order_no = '||to_char(I_order_no));
   close C_ALLOC_EXISTS;

   if L_dummy is NOT NULL then
      O_exists := TRUE;
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_ASN_ALLOC',
                       'ALLOC_HEADER',
                       'order_no = '||to_char(I_order_no));
      open C_CHECK_ASN_ALLOC;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_ASN_ALLOC',
                       'ALLOC_HEADER',
                       'order_no = '||to_char(I_order_no));
      fetch C_CHECK_ASN_ALLOC into L_asn_exists;
      
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_ASN_ALLOC',
                       'ALLOC_HEADER',
                       'order_no = '||to_char(I_order_no));
      close C_CHECK_ASN_ALLOC;
      
      if L_asn_exists is NOT NULL then
         O_ASN_exists := TRUE;
      else
         O_ASN_exists := FALSE;
      end if;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_VALIDATE_SQL.ALLOC_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END ALLOC_EXISTS;
---------------------------------------------------------------------
FUNCTION VALIDATE_SHIP_QTY(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_cancellable_qty   IN OUT   ORDLOC.QTY_ORDERED%TYPE,
                           I_order_no          IN       ORDLOC.ORDER_NO%TYPE,
                           I_item              IN       ORDLOC.ITEM%TYPE,
                           I_loc               IN       ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_ship_sum     ORDLOC.QTY_ORDERED%TYPE := NULL;
   L_appt_sum     ORDLOC.QTY_ORDERED%TYPE := NULL;
   L_qty_ordered  ORDLOC.QTY_ORDERED%TYPE := NULL;

   cursor C_SHIPMENT_SUM is
        select qty_expected from 
            (select NVL(SUM(NVL(shs.qty_expected, 0)), 0) qty_expected
              from shipsku shs,
                     shipment shp
             where shs.shipment = shp.shipment
                and shp.order_no = I_order_no
                and ((shp.to_loc_type = 'W' and shp.to_loc = I_loc)
                 or (shp.to_loc_type = 'S' and shp.to_loc = I_loc))
                and shp.status_code <> 'C'
                and shs.item = I_item
                union
            select NVL(SUM(NVL(shsl.qty_expected, 0)), 0) qty_expected
              from shipsku shs,
                     shipment shp,
                     shipsku_loc shsl
             where shs.shipment = shp.shipment
                and shs.shipment = shsl.shipment
                and shsl.seq_no = shs.seq_no
                and shsl.item = shs.item
                and shp.order_no = I_order_no
          and shsl.to_loc=I_loc
                and (shp.to_loc_type = 'W' and shp.to_loc in (select physical_wh from wh where wh = I_loc and physical_wh <> wh))        
                and shp.status_code <> 'C'
                and shs.item = I_item)
            where qty_expected>0;
            
            
   cursor C_APPT_SUM is
       select NVL(SUM(NVL(DECODE(ah.status, 'AC', ad.qty_received, ad.qty_appointed), 0)), 0)
        from appt_detail ad,
             appt_head ah
       where ad.appt = ah.appt
         and ad.doc_type = 'P'
         and ad.doc = I_order_no
         and (ad.loc_type = 'W' and ad.loc in (select physical_wh from wh where wh = I_loc)
          or (ad.loc_type = 'S' and ad.loc = I_loc))
         and ad.item = I_item;

   cursor C_QTY_ORDERED is
      select NVL(qty_ordered, 0)
        from ordloc
       where order_no = I_order_no
         and item = I_item
         and location = I_loc;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_loc',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_SHIPMENT_SUM','SHIPMENT,SHIPSKU','Order No: '||to_char(I_order_no));
   open C_SHIPMENT_SUM;
   SQL_LIB.SET_MARK('FETCH','C_SHIPMENT_SUM','SHIPMENT,SHIPSKU','Order No: '||to_char(I_order_no));
   fetch C_SHIPMENT_SUM into L_ship_sum;
   SQL_LIB.SET_MARK('CLOSE','C_SHIPMENT_SUM','SHIPMENT,SHIPSKU','Order No: '||to_char(I_order_no));
   close C_SHIPMENT_SUM;
   ---
   SQL_LIB.SET_MARK('OPEN','C_APPT_SUM','APPT_DETAIL','Order No: '||to_char(I_order_no));
   open C_APPT_SUM;
   SQL_LIB.SET_MARK('FETCH','C_APPT_SUM','APPT_DETAIL','Order No: '||to_char(I_order_no));
   fetch C_APPT_SUM into L_appt_sum;
   SQL_LIB.SET_MARK('CLOSE','C_APPT_SUM','APPT_DETAIL','Order No: '||to_char(I_order_no));
   close C_APPT_SUM;
   
   if NVL(L_ship_sum,0) > L_appt_sum then
      O_cancellable_qty := NVL(L_ship_sum,0);
   else
      O_cancellable_qty := L_appt_sum;
   end if;

   ---
   SQL_LIB.SET_MARK('OPEN','C_QTY_ORDERED','ORDLOC','Order No: '||to_char(I_order_no));
   open C_QTY_ORDERED;
   SQL_LIB.SET_MARK('FETCH','C_QTY_ORDERED','ORDLOC','Order No: '||to_char(I_order_no));
   fetch C_QTY_ORDERED into L_qty_ordered;
   SQL_LIB.SET_MARK('CLOSE','C_QTY_ORDERED','ORDLOC','Order No: '||to_char(I_order_no));
   close C_QTY_ORDERED;

   if O_cancellable_qty > L_qty_ordered then
      O_cancellable_qty := L_qty_ordered;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_VALIDATE_SQL.VALIDATE_SHIP_QTY',
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_SHIP_QTY;
---------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_ORDLOC
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION ORDLOC_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_diff          IN OUT VARCHAR2,
                            I_order_no      IN ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   CURSOR C_COUNT_ORDER IS
      select count(order_no)
        from ordloc
       where order_no = I_order_no;

   CURSOR C_COUNT_VORDER IS
      select count(order_no)
        from v_ordloc
       where order_no = I_order_no;

   L_order  NUMBER(6) := -765547;
   L_vorder NUMBER(6) := -987436;
BEGIN
   OPEN c_count_order;
   FETCH c_count_order INTO L_order;
   CLOSE c_count_order;

   OPEN c_count_vorder;
   FETCH c_count_vorder INTO L_vorder;
   CLOSE c_count_vorder;

   if nvl(L_vorder,0) != nvl(L_order,0) then
     O_diff := 'Y';
   else
     O_diff := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                               SQLERRM,
                                              'ORDER_VALIDATE_SQL.ORDLOC_FILTER_LIST',
                                               to_char(SQLCODE));
    return FALSE;
END ORDLOC_FILTER_LIST;
---------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_ORDSKU
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION ORDSKU_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_diff          IN OUT VARCHAR2,
                            I_order_no      IN ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   CURSOR C_COUNT_ORDER IS
      select count(order_no)
        from ordsku
       where order_no = I_order_no;

   CURSOR C_COUNT_VORDER IS
      select count(order_no)
        from v_ordsku
       where order_no = I_order_no;

   L_order  NUMBER(6) := -765547;
   L_vorder NUMBER(6) := -987436;
BEGIN
   OPEN c_count_order;
   FETCH c_count_order INTO L_order;
   CLOSE c_count_order;

   OPEN c_count_vorder;
   FETCH c_count_vorder INTO L_vorder;
   CLOSE c_count_vorder;

   if L_vorder != L_order then
     O_diff := 'Y';
   else
     O_diff := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                               SQLERRM,
                                              'ORDER_VALIDATE_SQL.ORDSKU_FILTER_LIST',
                                               to_char(SQLCODE));
    return FALSE;
END ORDSKU_FILTER_LIST;
---------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_ALLOC_DETAIL
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION ALLOCDTL_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_diff         IN OUT VARCHAR2,
                              I_order_no        IN ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   CURSOR C_COUNT_ALLOC IS
      select count(ad.alloc_no)
        from alloc_header ah,
             alloc_detail ad
       where ah.alloc_no = ad.alloc_no
         and ah.order_no = I_order_no;

   CURSOR C_COUNT_VALLOC IS
      select count(ad.alloc_no)
        from alloc_header ah,
             v_alloc_detail ad
       where ah.alloc_no = ad.alloc_no
         and ah.order_no = I_order_no;

   L_alloc  NUMBER(6) := -765547;
   L_valloc NUMBER(6) := -987436;
BEGIN
   OPEN c_count_alloc;
   FETCH c_count_alloc INTO L_alloc;
   CLOSE c_count_alloc;

   OPEN c_count_valloc;
   FETCH c_count_valloc INTO L_valloc;
   CLOSE c_count_valloc;

   if nvl(L_valloc,0) != nvl(L_alloc,0) then
     O_diff := 'Y';
   else
     O_diff := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                          SQLERRM,
                                          'ORDER_VALIDATE_SQL.ALLOCDTL_FILTER_LIST',
                                          to_char(SQLCODE));
    return FALSE;
END ALLOCDTL_FILTER_LIST;
--------------------------------------------------------------------------------------------

FUNCTION ITEM_ON_RECV_ORD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_order_flag      IN OUT   VARCHAR2,
                           I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(100)   := 'ORDER_VALIDATE_SQL.ITEM_ON_RECV_ORD';
   L_dummy     VARCHAR2(1);
   cursor C_ORD_ITEM is
      select 'Y'
        from ordhead oh,
             ordloc ol
       where oh.order_no = ol.order_no
         and oh.status   = 'A'
         and ol.item     = I_item
         and NVL(ol.qty_received, 0) > 0
         and NVL(ol.qty_received, 0) < ol.qty_ordered
         and rownum = 1;
BEGIN
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   --
   SQL_LIB.SET_MARK('OPEN',
                    'C_ORD_ITEM',
                    'ORDHEAD, ORDSKU, ORDLOC',
                    'item: ' ||TO_CHAR(I_item));
   open C_ORD_ITEM;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ORD_ITEM',
                    'ORDHEAD, ORDSKU, ORDLOC',
                    'item: ' ||TO_CHAR(I_item));
   fetch C_ORD_ITEM into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORD_ITEM',
                    'ORDHEAD, ORDSKU, ORDLOC',
                    'item: ' ||TO_CHAR(I_item));
   close C_ORD_ITEM;
   --
   if L_dummy is NULL then
      O_order_flag := 'N';
   else
      O_order_flag := 'Y';
   end if;
   --
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END ITEM_ON_RECV_ORD;
--------------------------------------------------------------------------------------------
-- Function Name: PARENT_DIFF_EXISTS
-- Purpose      : Verifies if the item parent has differentiators
--------------------------------------------------------------------------------------------
FUNCTION PARENT_DIFF_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist           IN OUT   BOOLEAN,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                            I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS
   
   L_exist_flag varchar2(1);
   L_program   VARCHAR2(100) := 'ORDER_VALIDATE_SQL.PARENT_DIFF_EXISTS';
   
   cursor C_VERIFY_PARENT_DIFF is
         select 'Y'
             from ordsku os,
                  item_master im,
                  item_master im1
            where os.order_no =I_order_no
              and os.item =I_item
              and im1.item = os.item
              and im.item=im1.item_parent
              and(im.diff_1 is NOT NULL
               or im.diff_2 is NOT NULL 
               or im.diff_3 is NOT NULL 
               or im.diff_4 is NOT NULL)
         UNION  
           select 'Y'
             from ordsku os,
                  item_master im,
                  packitem p,
                  item_master im1
            where os.order_no =I_order_no
              and os.item =I_item
              and p.pack_no=os.item 
              and im1.item=p.item            
              and im.item=im1.item_parent
              and(im.diff_1 is NOT NULL
               or im.diff_2 is NOT NULL 
               or im.diff_3 is NOT NULL 
               or im.diff_4 is NOT NULL);       

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_VERIFY_PARENT_DIFF',
                    'ORDHEAD, ORDSKU, ITEM_MASTER',
                    'item: ' ||TO_CHAR(I_item));
   open C_VERIFY_PARENT_DIFF;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_VERIFY_PARENT_DIFF',
                    'ORDHEAD, ORDSKU, ITEM_MASTER',
                    'item: ' ||TO_CHAR(I_item));
   fetch C_VERIFY_PARENT_DIFF
     into L_exist_flag;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_VERIFY_PARENT_DIFF',
                    'ORDHEAD, ORDSKU, ITEM_MASTER',
                    'item: ' ||TO_CHAR(I_item));
   close C_VERIFY_PARENT_DIFF;
   
   if L_exist_flag is null then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PARENT_DIFF_EXISTS;
--------------------------------------------------------------------------------------------
FUNCTION CHECK_ORDHEAD_DATES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_vdate             PERIOD.VDATE%TYPE   := GET_VDATE;
   L_not_before_date   ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_not_after_date    ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_program           VARCHAR2(64) := 'ORDER_VALIDATE_SQL.CHECK_ORDHEAD_DATES';

   cursor C_GET_ORDHEAD_DATES is
      select not_before_date,
             not_after_date
        from ordhead
       where order_no = I_order_no;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            I_order_no,
                                            'NULL',
                                            'NULL');
      return FALSE;
   end if;

   open  C_GET_ORDHEAD_DATES;
   fetch C_GET_ORDHEAD_DATES into L_not_before_date,
                                  L_not_after_date;
   close C_GET_ORDHEAD_DATES;

   if L_not_before_date < L_vdate or
      L_not_after_date  < L_vdate or
      L_not_before_date > L_not_after_date then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_DATES',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_VALIDATE_SQL.CHECK_ORDHEAD_DATES',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END;
--------------------------------------------------------------------------------------------
FUNCTION RECEIVED_QTY_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_qty_received    IN OUT   VARCHAR2,
                             I_order_no        IN       ORDLOC.ORDER_NO%TYPE)
   return BOOLEAN is
   
 
     
   cursor C_CHECK_RECEIVED_QTY is
      select 'x'
        from ordloc
       where order_no = I_order_no
         and qty_received > 0
         and rownum = 1;
         
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   O_qty_received := 'N' ;
   SQL_LIB.SET_MARK('OPEN','C_CHECK_RECEIVED_QTY','ORDLOC','Order No: '||to_char(I_order_no));
   open C_CHECK_RECEIVED_QTY;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_RECEIVED_QTY','ORDLOC','Order No: '||to_char(I_order_no));
   fetch C_CHECK_RECEIVED_QTY into O_qty_received;
   if C_CHECK_RECEIVED_QTY%FOUND then
      O_qty_received := 'Y' ;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_RECEIVED_QTY','ORDLOC','Order No: '||to_char(I_order_no));
   close C_CHECK_RECEIVED_QTY;
   ---
  
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_VALIDATE_SQL.RECEIVED_QTY_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END RECEIVED_QTY_EXISTS;
--------------------------------------------------------------------------------------------
-- Function Name: ORDER_QTY_EXISTS
-- Purpose      : Verifies if the order has zero quantities.
--------------------------------------------------------------------------------------------
FUNCTION ORDER_QTY_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exist           IN OUT   VARCHAR2,
                          I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   
   L_zero_qty  NUMBER(12,4);
   L_program   VARCHAR2(100) := 'ORDER_VALIDATE_SQL.ORDER_QTY_EXISTS';
   
   cursor C_GET_ZERO_QTY is
      select nvl(sum(qty_ordered),0)
        from ordloc
       where order_no = I_order_no;
         
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   open C_GET_ZERO_QTY;
   fetch C_GET_ZERO_QTY into L_zero_qty;
   if L_zero_qty <= 0 then
      O_exist := 'Y' ;
   end if;
   close C_GET_ZERO_QTY;
   ---
  
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_VALIDATE_SQL.ORDER_QTY_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END ORDER_QTY_EXISTS;
------------------------------------------------------------------------------------------------------
END ORDER_VALIDATE_SQL;
/
