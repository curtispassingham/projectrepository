



CREATE OR REPLACE PACKAGE ORDER_RCV_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-- Usage:
--  Due to performance concerns, bulk DML statements are used in
--   this package.  To facalitate that some additional work is necessary
--   by callers of this package.
--
--  Before calls to PO_LINE_ITEM or RCV_LINE_ITEM a call must be made to
--   INIT_PO_ASN_LOC_GROUP.  This ensures that there is no leftover data in
--   the strucures used to the bulk DML.
--
--  After call to PO_LINE_ITEM or RCV_LINE_ITEM a call must be made to
--   FINISH_PO_ASN_LOC_GROUP.  The uses the data built up in the TSF_LINE_ITEM
--   and ALLOC_LINE_ITEM to perform the bulk DML.
--
--  The idea is to put as many calls to PO_LINE_ITEM and/or RCV_LINE_ITEM
--   between calls to INIT_PO_ASN_LOC_GROUP and FINISH_PO_ASN_LOC_GROUP.  This will
--   minimize the number of times certain insert/update statement are performed
--   and speed up processing.
--
--  Even if only one call is made to PO_LINE_ITEM or RCV_LINE_ITEM,
--   INIT_PO_ASN_LOC_GROUP and FINISH_PO_ASN_LOC_GROUP must still be called.

--  See rmssub_receivingb.pls for an example of a call.

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--                                 GLOBALS                                   --
-------------------------------------------------------------------------------

LP_vdate                DATE          := get_vdate;
LP_user                 VARCHAR2(50)  := get_user;

--- System option variables that only need to be fetched once per session
LP_so_not_done           BOOLEAN       := TRUE;
LP_so_std_av_ind         system_options.std_av_ind%TYPE;
LP_so_elc_ind            system_options.elc_ind%TYPE;
LP_so_import_ind         system_options.import_ind%TYPE;
LP_so_currency_code      system_options.currency_code%TYPE;
LP_so_default_tax_type   system_options.default_tax_type%TYPE;
LP_so_org_unit_ind       system_options.org_unit_ind%TYPE;
LP_so_sim_ind            system_options.sim_ind%TYPE;
--- Set codes for deposit item checking, code_detail type IMIT
LP_contents_item_code    VARCHAR2(1) := 'E';
LP_container_item_code   VARCHAR2(1) := 'A';
LP_so_rtm_simplified_ind system_options.rtm_simplified_ind%TYPE;
--- Keep track of the location so we don't need to fetch the currency
--- for each line item received at the same location in this session.
LP_last_loc             ordloc.location%TYPE;
LP_loc_type             item_loc.loc_type%TYPE;
LP_loc_currency         ordhead.currency_code%TYPE;

--- Keep track of the weight for shipsku update
LP_shipment             shipsku.shipment%TYPE;
LP_tran_item            shipsku.item%TYPE;
LP_inv_status           shipsku.inv_status%TYPE;
LP_total_weight         shipsku.weight_expected%TYPE := NULL;
LP_total_weight_uom     shipsku.weight_expected_uom%TYPE := NULL;

--- Table of accumlated OTB call info.  The info is grouped together
--- and used to call OTB_SQL.ORD_RECEIVE in a seperate process.  This
--- process is broken out to avoid locking issues.
LP_rib_otb_tbl  RIB_OTB_TBL := null;

LP_curr_shipment            SHIPMENT.SHIPMENT%TYPE := NULL;
LP_prv_shipment             SHIPMENT.SHIPMENT%TYPE := NULL;
LP_online_rcv               VARCHAR2(1)            := 'N';

--- ordhead info fetched once per po/asn/phyloc group
TYPE header_lookup_record IS RECORD
(
   ord_check_order_no             ordhead.order_no%TYPE,

   ord_check_order_status         ordhead.status%TYPE,
   ord_check_order_orig_ind       ordhead.orig_ind%TYPE,
   ord_check_supplier             ordhead.supplier%TYPE,
   ord_check_ord_currency         ordhead.currency_code%TYPE,
   ord_check_ord_exchange_rate    ordhead.exchange_rate%TYPE,
   ord_check_earliest_ship_date   DATE,
   ord_check_latest_ship_date     DATE,
   ord_check_import_country_id    ordhead.import_country_id%TYPE,
   ord_check_cont_no              ordhead.contract_no%TYPE,
   ord_check_edi_asn              sups.edi_asn%TYPE,
   ord_check_supp_currency        ordhead.currency_code%TYPE,
   ord_check_settlement_code      sups.settlement_code%TYPE,


   ship_check_order_no             ordhead.order_no%TYPE,
   ship_check_phy_loc              item_loc.loc%TYPE,
   ship_check_asn                  shipment.asn%TYPE,

   ship_check_ship_no              shipment.shipment%TYPE,
   ship_check_ship_origin          shipment.ship_origin%TYPE,
   ref_doc_no                      shipment.ref_doc_no%TYPE,
   parent_ship_no                  shipment.shipment%TYPE
);
LP_header_lookup_record  header_lookup_record;

--- Global variables used so that invoice logic is only called once when an item
--- is distributed to (possibly) multiple virtual whs.
LP_deals_exist          BOOLEAN                    := FALSE;
LP_deals_shipment       ordloc_invc_cost.shipment%TYPE;

-------------------------------------------------------------------------------
--                                DATA TYPES                                 --
-------------------------------------------------------------------------------

TYPE item_rcv_record IS RECORD
(
   input_item           item_master.item%TYPE,                 --parameter: may be ref or tran level
   carton               shipsku.carton%TYPE,                   --parameter
   item                 item_master.item%TYPE,                 --lookup: item_master -- tran_level item
   item_parent          item_master.item_parent%TYPE,          --lookup: item_master
   item_grandparent     item_master.item_grandparent%TYPE,     --lookup: item_master
   item_level           item_master.item_level%TYPE,           --lookup: item_master
   tran_level           item_master.tran_level%TYPE,           --lookup: item_master
   diff_1               item_master.diff_1%TYPE,               --lookup: item_master
   diff_2               item_master.diff_2%TYPE,               --lookup: item_master
   diff_3               item_master.diff_3%TYPE,               --lookup: item_master
   diff_4               item_master.diff_4%TYPE,               --lookup: item_master
   dept                 item_master.dept%TYPE,                 --lookup: item_master
   class                item_master.class%TYPE,                --lookup: item_master
   subclass             item_master.subclass%TYPE,             --lookup: item_master
   pack_ind             item_master.pack_ind%TYPE,             --lookup: item_master
   pack_type            item_master.pack_type%TYPE,            --lookup: item_master
   sellable_ind         item_master.sellable_ind%TYPE,         --lookup: item_master
   orderable_ind        item_master.orderable_ind%TYPE,        --lookup: item_master
   inventory_ind        item_master.inventory_ind%TYPE,        --lookup: item_master
   suom                 item_master.standard_uom%TYPE,         --lookup: item_master
   simple_pack_ind      item_master.simple_pack_ind%TYPE,      --lookup: item_master
   catch_weight_ind     item_master.catch_weight_ind%TYPE,     --lookup: item_master
   deposit_item_type    item_master.deposit_item_type%TYPE,    --lookup: item_master
   container_item       item_master.container_item%TYPE,       --lookup: item_master
   tran_date            DATE,                                  --parameter
   loc                  item_loc.loc%TYPE,                     --lookup/parameter
   loc_type             item_loc.loc_type%TYPE,                --lookup: LOCATION_ATTRIB_SQL.GET_TYPE
   phy_loc              item_loc.loc%TYPE,                     --parameter
   distro_type          VARCHAR2(1),                           --parameter
   distro_number        alloc_header.alloc_no%TYPE,            --parameter
   destination          alloc_detail.to_loc%TYPE,              --parameter: location allocated to
   disp                 inv_status_codes.inv_status_code%TYPE, --parameter: inventory bucket received into
   inv_status           shipsku.inv_status%TYPE,               --lookup: INVADJ_SQL.GET_INV_STATUS
   order_no             ordhead.order_no%TYPE,                 --parameter
   order_status         ordhead.status%TYPE,                   --lookup: ordhead
   order_orig_ind       ordhead.orig_ind%TYPE,                 --lookup: ordhead
   earliest_ship_date   DATE,                                  --lookup: ordhead
   latest_ship_date     DATE,                                  --lookup: ordhead
   import_country_id    ordhead.import_country_id%TYPE,        --lookup: ordhead
   origin_country_id    ordsku.origin_country_id%TYPE,         --lookup: ordhead , primary if new ordsku
   asn                  shipment.asn%TYPE,                     --parameter
   appt                 appt_head.appt%TYPE,                   --parameter
   receipt_no           SHIPMENT.EXT_REF_NO_IN%TYPE,           --parameter
   ship_no              shipment.shipment%TYPE,                --lookup: shipment based on ord/asn/item or generated
   ship_origin          shipment.ship_origin%TYPE,             --lookup: shipment based on ord/asn/item or generated
   new_shipsku_ind      VARCHAR2(1),                           --calculated: 'Y',if shipsku record created
                                                               --            'N',if shipsku record existed
   new_ordloc_ind       VARCHAR2(1),                           --calculated: 'Y',if ordloc record created
                                                               --            'N',if ordloc record existed
   supplier             sups.supplier%TYPE,                    --lookup: ordhead
   edi_asn              sups.edi_asn%TYPE,                     --lookup: sups
   settlement_code      sups.settlement_code%TYPE,             --lookup: sups
   supp_pack_size       ordsku.supp_pack_size%TYPE,            --lookup: ordloc or iscl
   tran_type            VARCHAR2(1),                           --parameter: R = Receipt
                                                               --           A = Adjustment
   online_ind           VARCHAR2(1),                           --parameter: Y = Yes, online
                                                               --           N = No, not online
                                                               --           A = Auto PO
   ref_doc_no           shipment.ref_doc_no%TYPE,
   alc_finalize_ind     VARCHAR2(1),                           --ALC Finalize indicator
   ship_seq_no          shipsku.seq_no%TYPE                    --Shipment seq no
); -- end item_rcv_record


TYPE comp_item IS RECORD
(
   item                 item_master.item%TYPE,                           --lookup: v_packsku_qty
   qty                  item_loc_soh.stock_on_hand%TYPE,                 --lookup: v_packsku_qty
   unit_cost_loc        item_loc_soh.unit_cost%TYPE,                     --lookup: ITEMLOC_ATTRIB_SQL
   unit_retail_loc      item_loc.unit_retail%TYPE,                       --lookup: ITEMLOC_ATTRIB_SQL
   unit_retail_prim     item_loc.unit_retail%TYPE,                       --calculate: convert comp.unit_retail_loc
   pack_ind             item_master.pack_ind%TYPE,                       --lookup: item_master
   dept                 item_master.dept%TYPE,                           --lookup: item_master
   class                item_master.class%TYPE,                          --lookup: item_master
   subclass             item_master.subclass%TYPE,                       --lookup: item_master
   inventory_ind        item_master.inventory_ind%TYPE,                  --lookup: item_master
   unit_ebc_loc         item_supp_country_loc.extended_base_cost%TYPE,   --lookup: item_supp_country_loc
   cost_ratio           NUMBER
); -- end comp_item


TYPE comp_item_array IS TABLE of comp_item
   INDEX BY BINARY_INTEGER;


TYPE cost_retail_qty_record IS RECORD
(
   --- currencies
   -----------------------------------------------------
   ord_currency         ordhead.currency_code%TYPE,            --lookup: ordhead
   loc_currency         ordhead.currency_code%TYPE,            --lookup: store/wh
   supp_currency        ordhead.currency_code%TYPE,            --lookup: sups
   prim_currency        ordhead.currency_code%TYPE,            --lookup: system_options
   ord_exchange_rate    ordhead.exchange_rate%TYPE,            --lookup: ordhead


   --- qtys
   -----------------------------------------------------
   input_qty            ordloc.qty_ordered%TYPE,               --parameter
   weight               item_loc_soh.average_weight%TYPE,      --parameter
   weight_uom           uom_class.uom%TYPE,                    --parameter
   ol_qty_ordered       ordloc.qty_ordered%TYPE    := 0,       --lookup: ordloc
   ol_qty_received      ordloc.qty_received%TYPE   := 0,       --lookup: ordloc
   overage_qty          ordloc.qty_ordered%TYPE    := 0,       --calculate
      -- any new qty received that is above the ordered qty,
      -- calculated based on order qtys and passed in qty
   shipped_qty          shipsku.qty_expected%TYPE  := 0,


   --- costs
   -----------------------------------------------------
   unit_cost_input   ordloc_exp.est_exp_value%TYPE   := NULL, --parameter
      -- unit_cost in order curency, only sent from online
      -- when adding a new item/loc to the order

   unit_cost_order   ordloc_exp.est_exp_value%TYPE, --lookup: ordloc
      -- unit cost in order currency, selected from ordloc;
      -- if no ordloc records exist then use input_cost;
      -- if no ordloc records and no input_cost then
      -- use supp_cost

   unit_cost_loc     ordloc_exp.est_exp_value%TYPE, --calculate
      -- unit cost in local currency,
      -- converted from order cost

   unit_cost_supp    ordloc_exp.est_exp_value%TYPE, --lookup: iscl
      -- unit cost in supplier currency,
      -- selected from item_supp_country_loc

   unit_cost_prim    ordloc_exp.est_exp_value%TYPE, --calculate
      -- unit cost in primary currency,
      -- converted from order cost

   otb_cost_prim     ordloc_exp.est_exp_value%TYPE := 0, --calculate
      -- prim currency, used in call to UPD_OTB

   pack_cost_loc     ordloc_exp.est_exp_value%TYPE := 0,  --calculate
      -- pack cost sum of comp costs, in local currency

   pack_ebc_loc      ordloc.unit_cost%TYPE := 0, --calculate
      -- pack ebc sum of comp ebc, in local currency

   total_lc_loc      ordloc_exp.est_exp_value%TYPE := 0, --lookup:ELC_CALC_SQL
      -- total landed cost in local currency,
      -- used in OTB logic when ELC is on

   av_cost_loc       ordloc_exp.est_exp_value%TYPE := 0, --calculate
      -- average unit cost in local currency

   gross_cost        ordloc.unit_cost%TYPE,

   --- retails
   -----------------------------------------------------
   unit_retail_loc       ordloc.unit_retail%TYPE,              --lookup: ordloc or package call
      -- unit_retail selected from ordloc in local currency,
      -- or built with package call if not on order;
      -- overwritten with shipsku cost if adjustment

   unit_retail_prim     ordloc.unit_retail%TYPE,               --calculate
      -- unit_retail (loc_retail) converted to
      -- primary currency


   --- shapshot info
   -----------------------------------------------------
   stock_count_processed   BOOLEAN := FALSE,
   snapshot_cost           ordloc.unit_cost%TYPE,              --calculate
   snapshot_retail         ordloc.unit_retail%TYPE,            --calculate


   --- contract info
   -----------------------------------------------------
   cont_no                 contract_header.contract_no%TYPE   := NULL,
   cont_type               contract_header.contract_type%TYPE := NULL,
   cont_cost               contract_cost.unit_cost%TYPE       := 0,

   --- misc. info
   -----------------------------------------------------
   neg_soh_ac              ordloc.unit_cost%TYPE := 0,         --calculate
      -- set by update_item_stock if stock on hand is
      -- negative, used for tran_data adjustment

   negative_soh            item_loc_soh.stock_on_hand%TYPE := 0,
   -- Negative stock on hand qty returned by update_item_stock
   -- used for tran_data adjustment

   receive_as_type         item_loc.receive_as_type%TYPE := 'E', --lookup: item_loc
      -- used to determine whether certain processing is done
      -- at the pack or comp level

   upd_unit_cost           VARCHAR2(1)                         --calculate
      -- unit cost on item_loc_soh is updated if transaction
      -- is not an adjustment and standard costing is used

); -- end cost_retail_qty_record
TYPE ord_shipment IS RECORD
(
   order_no             ordhead.order_no%TYPE,
   shipment_no          shipment.shipment%TYPE
); -- end ord_shipment
TYPE ord_shipment_tbl IS TABLE of ord_shipment INDEX BY BINARY_INTEGER;
LP_ord_shipment_tbl         ord_shipment_tbl;

TYPE po_rcv_rec_type IS RECORD
(
   alloc_no        NUMBER,
   from_loc        NUMBER,
   to_loc          NUMBER,
   qty_allocated   NUMBER,
   po_rcv_qty      NUMBER,
   dist_qty        NUMBER
); --end po_rcv_rec_type
TYPE po_rcv_table_type IS TABLE OF po_rcv_rec_type INDEX BY BINARY_INTEGER;
-------------------------------------------------------------------------------
--                             PUBLIC FUNCTIONS                              --
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--    Name: INIT_PO_ASN_LOC_GROUP
-- Purpose: Called befoe processing a ReceiptDesc message. ensures OTB info
--          is cleared out for new call
-------------------------------------------------------------------------------
FUNCTION INIT_PO_ASN_LOC_GROUP(O_error_message   IN OUT  rtk_errors.rtk_text%TYPE)
return BOOLEAN;

-------------------------------------------------------------------------------
--    Name: FINISH_PO_ASN_LOC_GROUP
-- Purpose: Called after processing a ReceiptDesc message. Returns OTB info
--          generated by processing the message.
-------------------------------------------------------------------------------
FUNCTION FINISH_PO_ASN_LOC_GROUP(O_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
                                 O_rib_otb_tbl     IN OUT  RIB_OTB_TBL)
return BOOLEAN;

-------------------------------------------------------------------------------
--    Name: PO_LINE_ITEM
-- Purpose: Called once for each line item received.  Validates input and calls
--          RCV_LINE_ITEM for each item/location.  If the item is received to a
--          physical wh in a multichannel environment then this function will
--          call distribution logic to determine each item/vwh/qty and then
--          call RCV_LINE_ITEM for each of these combinations.
-------------------------------------------------------------------------------
FUNCTION PO_LINE_ITEM(O_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
                      I_loc             IN      item_loc.loc%TYPE,
                      I_order_no        IN      ordhead.order_no%TYPE,
                      I_item            IN      item_master.item%TYPE,
                      I_qty             IN      tran_data.units%TYPE,
                      I_tran_type       IN      VARCHAR2,
                      I_tran_date       IN      DATE,
                      I_receipt_number  IN      SHIPMENT.EXT_REF_NO_IN%TYPE,
                      I_asn             IN      shipment.asn%TYPE,
                      I_appt            IN      appt_head.appt%TYPE,
                      I_carton          IN      shipsku.carton%TYPE,
                      I_distro_type     IN      VARCHAR2,
                      I_distro_number   IN      alloc_header.alloc_no%TYPE,
                      I_destination     IN      alloc_detail.to_loc%TYPE,
                      I_disp            IN      inv_status_codes.inv_status_code%TYPE,
                      I_unit_cost       IN      ordloc.unit_cost%TYPE,
                      I_shipped_qty     IN      shipsku.qty_expected%TYPE,
                      I_weight          IN      item_loc_soh.average_weight%TYPE,
                      I_weight_uom      IN      UOM_CLASS.UOM%TYPE,
                      I_online_ind      IN      VARCHAR2,
                      I_gross_cost      IN      ORDLOC.UNIT_COST%TYPE    DEFAULT NULL,
                      I_ref_doc_no      IN      TRAN_DATA.REF_NO_1%TYPE  DEFAULT NULL)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
--    Name: RCV_LINE_ITEM
-- Purpose: Called for each distinct item/location combination.  Validates
--          input and calls private functions to complete receiving logic for each
--          item (or component item if pack is received).
-------------------------------------------------------------------------------
FUNCTION RCV_LINE_ITEM(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_phy_loc         IN      ITEM_LOC.LOC%TYPE,
                       I_loc             IN      ITEM_LOC.LOC%TYPE,
                       I_loc_type        IN      ITEM_LOC.LOC_TYPE%TYPE,
                       I_order_no        IN      ORDHEAD.ORDER_NO%TYPE,
                       I_item            IN      ITEM_MASTER.ITEM%TYPE,
                       I_qty             IN      TRAN_DATA.UNITS%TYPE,
                       I_tran_type       IN      VARCHAR2,
                       I_tran_date       IN      DATE,
                       I_receipt_number  IN      SHIPMENT.EXT_REF_NO_IN%TYPE,
                       I_asn             IN      SHIPMENT.ASN%TYPE,
                       I_appt            IN      APPT_HEAD.APPT%TYPE,
                       I_carton          IN      SHIPSKU.CARTON%TYPE,
                       I_distro_type     IN      VARCHAR2,
                       I_distro_number   IN      TSFHEAD.TSF_NO%TYPE,
                       I_destination     IN      ALLOC_DETAIL.TO_LOC%TYPE,
                       I_disp            IN      INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                       I_unit_cost       IN      ORDLOC.UNIT_COST%TYPE,
                       I_shipped_qty     IN      SHIPSKU.QTY_EXPECTED%TYPE,
                       I_weight          IN      ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                       I_weight_uom      IN      UOM_CLASS.UOM%TYPE,
                       I_online_ind      IN      VARCHAR2,
                       I_gross_cost      IN      ORDLOC.UNIT_COST%TYPE              DEFAULT NULL,
                       I_ref_doc_no      IN      TRAN_DATA.REF_NO_1%TYPE            DEFAULT NULL,
                       I_upd_chrgs       IN      ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE DEFAULT 0)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
--    Name: RECEIVE_AUTO_PO
-- Purpose: Called from DSD receipts (vendor supplied chips, soda, etc.) as well
--          for Customer Orders.  Selects each item/carton/qty
--          combo from shipsku and calls PO_LINE_ITEM to receive.
-------------------------------------------------------------------------------
FUNCTION RECEIVE_AUTO_PO(O_error_message  IN OUT  VARCHAR2,
                         I_order_no       IN      shipment.order_no%TYPE,
                         I_shipment       IN      shipment.shipment%TYPE,
                         I_location       IN      item_loc.loc%TYPE,
                         I_tran_date      IN      DATE,
                         I_invoice_ind    IN      VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
--    Name: PO_LINE_ITEM_ONLINE
-- Purpose: Called from any forms that need to receive line items.
--          This function is a wrapper around PO_LINE_ITEM that calls
--          the INIT and FINISH functions mentioned above.
--          It also takes the outputted OTB object from the FINISH function
--          and sends it to RMSSUB_OTBMOD.CONSUME, so that OTB is updated.
-------------------------------------------------------------------------------
FUNCTION PO_LINE_ITEM_ONLINE(O_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
                             I_loc             IN      item_loc.loc%TYPE,
                             I_order_no        IN      ordhead.order_no%TYPE,
                             I_item            IN      item_master.item%TYPE,
                             I_qty             IN      tran_data.units%TYPE,
                             I_tran_type       IN      VARCHAR2,
                             I_tran_date       IN      DATE,
                             I_receipt_number  IN      appt_detail.receipt_no%TYPE,
                             I_asn             IN      shipment.asn%TYPE,
                             I_appt            IN      appt_head.appt%TYPE,
                             I_carton          IN      shipsku.carton%TYPE,
                             I_distro_type     IN      VARCHAR2,
                             I_distro_number   IN      alloc_header.alloc_no%TYPE,
                             I_destination     IN      alloc_detail.to_loc%TYPE,
                             I_disp            IN      inv_status_codes.inv_status_code%TYPE,
                             I_unit_cost       IN      ordloc.unit_cost%TYPE,
                             I_online_ind      IN      VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
--    Name: PO_LINE_ITEM_ONLINE
-- Purpose: Overloaded function if shipment to process is known
-------------------------------------------------------------------------------
FUNCTION PO_LINE_ITEM_ONLINE(O_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
                             I_loc             IN      item_loc.loc%TYPE,
                             I_order_no        IN      ordhead.order_no%TYPE,
                             I_item            IN      item_master.item%TYPE,
                             I_qty             IN      tran_data.units%TYPE,
                             I_tran_type       IN      VARCHAR2,
                             I_tran_date       IN      DATE,
                             I_receipt_number  IN      appt_detail.receipt_no%TYPE,
                             I_asn             IN      shipment.asn%TYPE,
                             I_appt            IN      appt_head.appt%TYPE,
                             I_carton          IN      shipsku.carton%TYPE,
                             I_distro_type     IN      VARCHAR2,
                             I_distro_number   IN      alloc_header.alloc_no%TYPE,
                             I_destination     IN      alloc_detail.to_loc%TYPE,
                             I_disp            IN      inv_status_codes.inv_status_code%TYPE,
                             I_unit_cost       IN      ordloc.unit_cost%TYPE,
                             I_online_ind      IN      VARCHAR2,
                             --
                             I_shipment        IN      SHIPMENT.SHIPMENT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION DSD_ORDER_RCV(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no        IN       ORDAUTO_TEMP.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PO_LINE_ITEM_ONLINE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_loc              IN       ITEM_LOC.LOC%TYPE,
                             I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                             I_item             IN       ITEM_MASTER.ITEM%TYPE,
                             I_qty              IN       TRAN_DATA.UNITS%TYPE,
                             I_tran_type        IN       VARCHAR2,
                             I_tran_date        IN       DATE,
                             I_receipt_number   IN       APPT_DETAIL.RECEIPT_NO%TYPE,
                             I_asn              IN       SHIPMENT.ASN%TYPE,
                             I_appt             IN       APPT_HEAD.APPT%TYPE,
                             I_carton           IN       SHIPSKU.CARTON%TYPE,
                             I_distro_type      IN       VARCHAR2,
                             I_distro_number    IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                             I_destination      IN       ALLOC_DETAIL.TO_LOC%TYPE,
                             I_disp             IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                             I_unit_cost        IN       ORDLOC.UNIT_COST%TYPE,
                             I_online_ind       IN       VARCHAR2,
                             --
                             I_shipment         IN       SHIPMENT.SHIPMENT%TYPE,
                             --
                             I_weight           IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                             I_weight_uom       IN       UOM_CLASS.UOM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PO_IMPORT_ITEM_ONLINE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_loc                IN       ITEM_LOC.LOC%TYPE,
                               I_order_no           IN       ORDHEAD.ORDER_NO%TYPE,
                               I_tran_type          IN       VARCHAR2,
                               I_tran_date          IN       DATE,
                               I_receipt_number     IN       APPT_DETAIL.RECEIPT_NO%TYPE,
                               I_asn                IN       SHIPMENT.ASN%TYPE,
                               I_appt               IN       APPT_HEAD.APPT%TYPE,
                               I_distro_type        IN       VARCHAR2,
                               I_distro_number      IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                               I_destination        IN       ALLOC_DETAIL.TO_LOC%TYPE,
                               I_disp               IN       INV_STATUS_CODES.INV_STATUS_CODE%TYPE,
                               I_unit_cost          IN       ORDLOC.UNIT_COST%TYPE,
                               I_online_ind         IN       VARCHAR2,
                               I_shipment           IN       SHIPMENT.SHIPMENT%TYPE,
                               I_item_receiving_tbl IN       RECEIVE_SQL.ITEM_RECV_TABLE )
RETURN BOOLEAN;
-------------------------------------------------------------------------------

-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
-- compile private functions as public for unit testing
$if $$UTPLSQL=TRUE $then

FUNCTION VALIDATE_INPUT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item_rec        IN OUT   ITEM_RCV_RECORD,
                        I_comp_items      IN OUT   COMP_ITEM_ARRAY,
                        I_values          IN OUT   COST_RETAIL_QTY_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION ITEM_CHECK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item_rec        IN OUT   ITEM_RCV_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_SYSTEM_OPTIONS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION ORD_CHECK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_item_rec        IN OUT   ITEM_RCV_RECORD,
                   I_values          IN OUT   COST_RETAIL_QTY_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_ORD_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION APPT_CHECK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item_rec        IN       ITEM_RCV_RECORD,
                    I_values          IN       COST_RETAIL_QTY_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SHIP_CHECK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item_rec        IN OUT   ITEM_RCV_RECORD,
                    I_values          IN       COST_RETAIL_QTY_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_SHIP_INFO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item_rec      IN OUT ITEM_RCV_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION ON_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_item_rec        IN OUT   ITEM_RCV_RECORD,
                  I_values          IN OUT   COST_RETAIL_QTY_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION Insert_Update_SHIPSKU(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item_rec          IN OUT   ITEM_RCV_RECORD,
                               I_values            IN OUT   COST_RETAIL_QTY_RECORD,
                               I_update_qty_only   IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION UPDATE_INVC_MATCH_WKSHT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item_rec         IN OUT  ITEM_RCV_RECORD,
                                 I_values           IN OUT  COST_RETAIL_QTY_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION LOAD_COMPS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item_rec        IN OUT   ITEM_RCV_RECORD,
                    I_comp_items      IN OUT   COMP_ITEM_ARRAY,
                    I_values          IN OUT   COST_RETAIL_QTY_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION UPDATE_ALLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_alloc_no        IN       ALLOC_DETAIL.ALLOC_NO%TYPE,
                      I_destination     IN       ALLOC_DETAIL.TO_LOC%TYPE,
                      I_input_qty       IN       ALLOC_DETAIL.PO_RCVD_QTY%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION ALC_PROCESSING(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item_rec        IN OUT   ITEM_RCV_RECORD,
                        I_comp_items      IN       COMP_ITEM_ARRAY,
                        I_values          IN OUT   COST_RETAIL_QTY_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION BACK_OUT_ALC(O_error_message     IN OUT   VARCHAR2,
                      I_item_rec          IN       ITEM_RCV_RECORD,
                      I_values            IN       COST_RETAIL_QTY_RECORD,
                      I_comp_item         IN       ITEM_MASTER.ITEM%TYPE,
                      I_unit_cost_prim    IN       ORDLOC.UNIT_COST%TYPE,
                      I_ol_qty_received   IN       ORDLOC.QTY_RECEIVED%TYPE,
                      I_pack_cost_loc     IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                      I_comp_cost_loc     IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                      I_comp_qty          IN       ORDLOC.QTY_RECEIVED%TYPE,
                      I_dept              IN       DEPS.DEPT%TYPE,
                      I_class             IN       CLASS.CLASS%TYPE,
                      I_subclass          IN       SUBCLASS.SUBCLASS%TYPE,
                      I_alc_status        IN       ALC_HEAD.STATUS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_ELC_TOTAL_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_elc_loc         IN OUT   ORDLOC_EXP.EST_EXP_VALUE%TYPE,
                           I_item_rec        IN       ITEM_RCV_RECORD,
                           I_values          IN       COST_RETAIL_QTY_RECORD,
                           I_item            IN       ITEM_MASTER.ITEM%TYPE,
                           I_comp_item       IN       ITEM_MASTER.ITEM%TYPE,
                           I_unit_cost_ord   IN       ITEM_LOC_SOH.UNIT_COST%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PACK_LEVEL_UPDATES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_values          IN OUT   COST_RETAIL_QTY_RECORD,
                            I_item_rec        IN       ITEM_RCV_RECORD,
                            I_weight          IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                            I_weight_uom      IN       UOM_CLASS.UOM%TYPE,
                            I_receipt_date    IN       DATE DEFAULT NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SET_AV_COST_OTB_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_values              IN OUT   COST_RETAIL_QTY_RECORD,
                              I_pack_no              IN       ITEM_MASTER.ITEM%TYPE,
                              I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                              I_comp_item            IN       ITEM_MASTER.ITEM%TYPE,
                              I_comp_unit_cost_loc   IN       ITEM_LOC_SOH.UNIT_COST%TYPE,
                              I_item_rec             IN       ITEM_RCV_RECORD,
                              I_comp_cost_ratio      IN       NUMBER DEFAULT 1)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_STOCK(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_neg_soh_ac              IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                           O_neg_soh                 IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_item                    IN       ITEM_MASTER.ITEM%TYPE,
                           I_comp_ind                IN       VARCHAR,
                           I_loc                     IN       ITEM_LOC.LOC%TYPE,
                           I_qty                     IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                           I_weight                  IN       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                           I_weight_uom              IN       UOM_CLASS.UOM%TYPE,
                           I_av_cost_loc             IN       ITEM_LOC_SOH.UNIT_COST%TYPE,
                           I_total_cost_loc          IN       ITEM_LOC_SOH.UNIT_COST%TYPE,
                           I_tran_date               IN       DATE,
                           I_tran_type               IN       VARCHAR2,
                           I_upd_unit_cost           IN       VARCHAR2,
                           I_stock_count_processed   IN       BOOLEAN,
                           I_new_ordloc_ind          IN       VARCHAR2,
                           I_receive_as_type         IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                           I_inv_status              IN       INV_STATUS_QTY.INV_STATUS%TYPE,
                           I_loc_type                IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                           I_order_no                IN       ORDHEAD.ORDER_NO%TYPE,
                           I_unit_retail_loc         IN       ORDLOC.UNIT_RETAIL%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION STOCKLEDGER_INFO(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_neg_soh_ac              IN OUT   TRAN_DATA.TOTAL_COST%TYPE,
                          I_neg_soh                 IN       TRAN_DATA.UNITS%TYPE,
                          I_total_cost_loc          IN OUT   TRAN_DATA.TOTAL_COST%TYPE,
                          I_item                    IN       TRAN_DATA.ITEM%TYPE,
                          I_pack_ind                IN       ITEM_MASTER.PACK_IND%TYPE,
                          I_receive_as_type         IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                          I_dept                    IN       TRAN_DATA.DEPT%TYPE,
                          I_class                   IN       TRAN_DATA.CLASS%TYPE,
                          I_subclass                IN       TRAN_DATA.SUBCLASS%TYPE,
                          I_tran_date               IN       DATE,
                          I_tran_type               IN       VARCHAR2,
                          I_receipt_qty             IN       TRAN_DATA.UNITS%TYPE,
                          I_total_retail_loc        IN       TRAN_DATA.TOTAL_RETAIL%TYPE,
                          I_shipment                IN       SHIPMENT.SHIPMENT%TYPE,
                          I_order_no                IN       ORDHEAD.ORDER_NO%TYPE,
                          I_stock_count_processed   IN       BOOLEAN,
                          I_snapshot_cost           IN       ORDLOC.UNIT_COST%TYPE,
                          I_snapshot_retail         IN       ORDLOC.UNIT_RETAIL%TYPE,
                          I_loc                     IN       ITEM_LOC.LOC%TYPE,
                          I_loc_type                IN       ITEM_LOC.LOC_TYPE%TYPE,
                          I_ref_pack_no             IN       TRAN_DATA.REF_PACK_NO%TYPE,
                          I_total_cost_excl_elc     IN       TRAN_DATA.TOTAL_COST_EXCL_ELC%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SUP_DATA_INSERTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_supplier        IN       SUPS.SUPPLIER%TYPE,
                          I_dept            IN       DEPS.DEPT%TYPE,
                          I_tran_date       IN       DATE,
                          I_cost_prim       IN       ITEM_LOC_SOH.UNIT_COST%TYPE,
                          I_retail_prim     IN       ITEM_LOC.UNIT_RETAIL%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION FLUSH_SUP_DATA_CACHE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION UPD_INV_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item            IN       ITEM_MASTER.ITEM%TYPE,
                        I_inv_status      IN       SHIPSKU.INV_STATUS%TYPE,
                        I_qty             IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                        I_total_cost      IN       ORDLOC.UNIT_COST%TYPE     DEFAULT NULL,
                        I_total_retail    IN       ORDLOC.UNIT_RETAIL%TYPE   DEFAULT NULL,
                        I_loc             IN       ITEM_LOC.LOC%TYPE,
                        I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                        I_tran_date       IN       PERIOD.VDATE%TYPE,
                        I_order_no        IN       ORDHEAD.ORDER_NO%TYPE     DEFAULT NULL)

RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION CHECK_AGAINST_CONTRACT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_values          IN OUT   COST_RETAIL_QTY_RECORD,
                                I_item_rec        IN       ITEM_RCV_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION INVC_PROCESSING(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_values          IN       COST_RETAIL_QTY_RECORD,
                         I_item_rec        IN       ITEM_RCV_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION IB_LINK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_item_rec        IN       ITEM_RCV_RECORD)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION OPEN_ORDER(O_error_message   IN OUT   VARCHAR2,
                    I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION CONTAINER_ITEM_CHECK(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_container_item_rec IN OUT   ITEM_RCV_RECORD,
                              I_container_values   IN OUT   COST_RETAIL_QTY_RECORD,
                              I_container_item     IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION UPDATE_SHIPSKU_WEIGHT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_shipment           IN       SHIPSKU.SHIPMENT%TYPE,
                               I_item               IN       SHIPSKU.ITEM%TYPE,
                               I_carton             IN       SHIPSKU.CARTON%TYPE,
                               I_inv_status         IN       SHIPSKU.INV_STATUS%TYPE,
                               I_weight             IN       SHIPSKU.WEIGHT_EXPECTED%TYPE,
                               I_weight_uom         IN       SHIPSKU.WEIGHT_EXPECTED_UOM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_SHIPSKU_LOC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item_rec          IN OUT   ITEM_RCV_RECORD,
                                   I_values            IN OUT   COST_RETAIL_QTY_RECORD)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
FUNCTION ADD_ORD_SHIPMENT_TO_QUEUE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                                   I_shipment        IN       SHIPSKU.SHIPMENT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PROCESS_REALLOC_ALC_FOR_QUEUE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN;

$end

END ORDER_RCV_SQL;
/