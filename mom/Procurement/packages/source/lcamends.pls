
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE LC_AMEND_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------------
-- Function Name: NET_EFFECT
-- Purpose:       Calculates the net effect of an amendment made to the passed in
--                LC Ref ID.
--------------------------------------------------------------------------------------
FUNCTION NET_EFFECT(O_error_message  IN OUT  VARCHAR2,
                    O_net_effect     IN OUT  NUMBER,
                    I_amend_no       IN      LC_AMENDMENTS.AMEND_NO%TYPE,
                    I_lc_ref_id      IN      LC_AMENDMENTS.LC_REF_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: GENERATE
-- Purpose:       Generates an amendment number for amendments being generated and
--                updates the lc_amendments table with the amendment number.  If
--                the amendment affects the letter of credit amount, writes a record
--                to the lc_activity table and updates the issuing bank's partner
--                record.
--------------------------------------------------------------------------------------
FUNCTION GENERATE(O_error_message  IN OUT  VARCHAR2,
                  I_lc_ref_id      IN      LC_AMENDMENTS.LC_REF_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: GET_FIXED_FORMAT_VALUES
-- Purpose:       Retrieves the values of the letter of credit.  First, retrieves the
--                value from the lc_amendments table.  If no amendment found,
--                retrieves the value from the lc_head table.  These are values that
--                are found on the fixed format window of amendments.
--------------------------------------------------------------------------------------
FUNCTION GET_FIXED_FORMAT_VALUES(O_error_message IN OUT VARCHAR2,
                                 O_early_ship    IN OUT LC_HEAD.EARLIEST_SHIP_DATE%TYPE,
                                 O_late_ship     IN OUT LC_HEAD.LATEST_SHIP_DATE%TYPE,
                                 O_exp_date      IN OUT LC_HEAD.EXPIRATION_DATE%TYPE,
                                 O_place_of_exp  IN OUT LC_HEAD.PLACE_OF_EXPIRY%TYPE,
                                 O_net_amount    IN OUT LC_HEAD.AMOUNT%TYPE,
                                 O_neg_days      IN OUT LC_HEAD.LC_NEG_DAYS%TYPE,
                                 O_present_term  IN OUT LC_HEAD.PRESENTATION_TERMS%TYPE,
                                 O_transship     IN OUT LC_HEAD.TRANSSHIPMENT_IND%TYPE,
                                 O_transfer      IN OUT LC_HEAD.TRANSFERABLE_IND%TYPE,
                                 O_part_ship     IN OUT LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE,
                                 I_lc_ref_id     IN     LC_AMENDMENTS.LC_REF_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: GET_ORIGINAL_VALUES
-- Purpose:       Retrieves the values of the letter of credit.  First, retrieves the
--                value from the lc_amendments table.  If no amendment found,
--                retrieves the value from the lc_head table.  These are values that
--                are affected by order changes.
--------------------------------------------------------------------------------------
FUNCTION GET_ORIGINAL_VALUES(O_error_message  IN OUT VARCHAR2,
                             O_early_ship     IN OUT LC_HEAD.EARLIEST_SHIP_DATE%TYPE,
                             O_late_ship      IN OUT LC_HEAD.LATEST_SHIP_DATE%TYPE,
                             O_exp_date       IN OUT LC_HEAD.EXPIRATION_DATE%TYPE,
                             O_country        IN OUT LC_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                             O_transship      IN OUT LC_HEAD.TRANSSHIPMENT_IND%TYPE,
                             O_part_ship      IN OUT LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE,
                             I_lc_ref_id      IN     LC_AMENDMENTS.LC_REF_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Purpose:       Converts the amendment into text format.
--------------------------------------------------------------------------------------
FUNCTION AMEND_TEXT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_amend_text      IN OUT   LC_AMENDMENTS.NEW_VALUE%TYPE,
                    I_order_no        IN       LC_AMENDMENTS.ORDER_NO%TYPE,
                    I_item            IN       LC_AMENDMENTS.ITEM%TYPE,
                    I_coded_field     IN       CODE_DETAIL.CODE%TYPE,
                    I_field           IN       CODE_DETAIL.CODE_DESC%TYPE,
                    I_original        IN       LC_AMENDMENTS.ORIGINAL_VALUE%TYPE,
                    I_new             IN       LC_AMENDMENTS.NEW_VALUE%TYPE,
                    I_effect          IN       LC_AMENDMENTS.EFFECT%TYPE,
                    I_currency        IN       CURRENCIES.CURRENCY_CODE%TYPE,
                    I_parent_ind      IN       VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Purpose:       Writes a record to the lc_amendments table when a letter of credit
--                field changes.
--------------------------------------------------------------------------------------
FUNCTION WRITE_FIXED_AMEND(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_old_early_ship     IN       LC_HEAD.EARLIEST_SHIP_DATE%TYPE,
                           I_new_early_ship     IN       LC_HEAD.EARLIEST_SHIP_DATE%TYPE,
                           I_old_late_ship      IN       LC_HEAD.LATEST_SHIP_DATE%TYPE,
                           I_new_late_ship      IN       LC_HEAD.LATEST_SHIP_DATE%TYPE,
                           I_old_exp_date       IN       LC_HEAD.EXPIRATION_DATE%TYPE,
                           I_new_exp_date       IN       LC_HEAD.EXPIRATION_DATE%TYPE,
                           I_old_place_of_exp   IN       LC_HEAD.PLACE_OF_EXPIRY%TYPE,
                           I_new_place_of_exp   IN       LC_HEAD.PLACE_OF_EXPIRY%TYPE,
                           I_old_net_amount     IN       LC_HEAD.AMOUNT%TYPE,
                           I_new_net_amount     IN       LC_HEAD.AMOUNT%TYPE,
                           I_old_neg_days       IN       LC_HEAD.LC_NEG_DAYS%TYPE,
                           I_new_neg_days       IN       LC_HEAD.LC_NEG_DAYS%TYPE,
                           I_old_present_term   IN       LC_HEAD.PRESENTATION_TERMS%TYPE,
                           I_new_present_term   IN       LC_HEAD.PRESENTATION_TERMS%TYPE,
                           I_old_transship      IN       LC_HEAD.TRANSSHIPMENT_IND%TYPE,
                           I_new_transship      IN       LC_HEAD.TRANSSHIPMENT_IND%TYPE,
                           I_old_transferable   IN       LC_HEAD.TRANSFERABLE_IND%TYPE,
                           I_new_transferable   IN       LC_HEAD.TRANSFERABLE_IND%TYPE,
                           I_old_partial_ship   IN       LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE,
                           I_new_partial_ship   IN       LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE,
                           I_add_req_doc        IN       REQ_DOC.DOC_ID%TYPE,
                           I_remove_req_doc     IN       REQ_DOC.DOC_ID%TYPE,
                           I_lc_exp_days        IN       SYSTEM_OPTIONS.LC_EXP_DAYS%TYPE,


                           I_lc_ref_id          IN       LC_HEAD.LC_REF_ID%TYPE,
                           I_new_comment        IN       LC_AMENDMENTS.NEW_VALUE%TYPE)

RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Purpose:       Writes an amendment to the lc_amendments table when an order is
--                removed from a letter of credit.
--------------------------------------------------------------------------------------
FUNCTION WRITE_ORDER_AMEND(O_error_message  IN OUT  VARCHAR2,
                           I_order_no       IN      LC_AMENDMENTS.ORDER_NO%TYPE,
                           I_lc_ref_id      IN      LC_AMENDMENTS.LC_REF_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: UPDATE_BANK
-- Purpose:       Updates the issuing bank fields for any drawdowns or amendments
--                that occur to the letter of credit.
--------------------------------------------------------------------------------------
FUNCTION UPDATE_BANK(O_error_message  IN OUT VARCHAR2,
                     I_update_type    IN     VARCHAR2,
                     I_amount         IN     PARTNER.LINE_OF_CREDIT%TYPE,
                     I_issuing_bank   IN     PARTNER.PARTNER_ID%TYPE)
         RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name : STYLE_ROLLUP
-- Purpose       : Rolls item level information up  and
--                 returns the new_value, original_value, and effect.
FUNCTION PARENT_ROLLUP (O_error_message  IN OUT VARCHAR2,
                       O_new_value      IN OUT LC_AMENDMENTS.NEW_VALUE%TYPE,
                       O_original_value IN OUT LC_AMENDMENTS.ORIGINAL_VALUE%TYPE,
                       O_effect         IN OUT LC_AMENDMENTS.EFFECT%TYPE,
                       I_lc_ref_id      IN     LC_AMENDMENTS.LC_REF_ID%TYPE,
                       I_order_no       IN     LC_AMENDMENTS.ORDER_NO%TYPE,
                       I_item_parent    IN     LC_AMENDMENTS.ITEM%TYPE,
                       I_amended_field  IN     LC_AMENDMENTS.AMENDED_FIELD%TYPE,
                       I_amend_no       IN     LC_AMENDMENTS.AMEND_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
END LC_AMEND_SQL;
/
