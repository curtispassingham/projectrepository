
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SUP_BRACKET_COST_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------
FUNCTION BRACKETS_EXIST(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exist         IN OUT  BOOLEAN,
                        I_supplier      IN      SUP_BRACKET_COST.SUPPLIER%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION UPDATE_COST_CHANGE_FOR_DEFAULT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_status        IN OUT COST_SUSP_SUP_HEAD.STATUS%TYPE,
                                        I_cost_change   IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                                        I_supplier      IN     COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                                        I_bracket_value IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION GET_DEFAULT_BRACKET(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_bracket_value IN OUT  SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                             I_seq_no        IN      SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION INV_MGMT_LVL_BRACKETS_EXIST(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exist         IN OUT  BOOLEAN,
                                     I_seq_no        IN      SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION COST_CHANGE_EXISTS(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist         IN OUT  BOOLEAN,
                            I_cost_change   IN      COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION DELETE_DFLT_NEXT_BRACKET_LVL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_supplier      IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                                      I_dept          IN     SUP_BRACKET_COST.DEPT%TYPE,
                                      I_location      IN     SUP_BRACKET_COST.LOCATION%TYPE,
                                      I_seq_no        IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION DELETE_BRACKET(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_bracket_value IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                        I_supplier      IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                        I_dept          IN     SUP_BRACKET_COST.DEPT%TYPE,
                        I_location      IN     SUP_BRACKET_COST.LOCATION%TYPE,
                        I_seq_no        IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION INSERT_BRACKET(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_bracket_value1 IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                        I_bracket_uom1   IN     SUP_INV_MGMT.BRACKET_UOM1%TYPE,
                        I_bracket_value2 IN     SUP_BRACKET_COST.BRACKET_VALUE2%TYPE,
                        I_default_ind    IN     SUP_BRACKET_COST.DEFAULT_BRACKET_IND%TYPE,
                        I_supplier       IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                        I_dept           IN     SUP_BRACKET_COST.DEPT%TYPE,
                        I_location       IN     SUP_BRACKET_COST.LOCATION%TYPE,
                        I_cost_change    IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                        I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE,
                        I_bracket_lvl    IN     SUPS.INV_MGMT_LVL%TYPE,
                        I_new_struct     IN     VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION CHANGE_DEFAULT_BRACKET(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_cost_change    IN OUT COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                                I_bracket_value1 IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                                I_bracket_uom1   IN     SUP_INV_MGMT.BRACKET_UOM1%TYPE,
                                I_bracket_value2 IN     SUP_BRACKET_COST.BRACKET_VALUE2%TYPE,
                                I_supplier       IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                                I_dept           IN     SUP_BRACKET_COST.DEPT%TYPE,
                                I_location       IN     SUP_BRACKET_COST.LOCATION%TYPE,
                                I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE,
                                I_bracket_lvl    IN     SUPS.INV_MGMT_LVL%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION DUP_BRACKET_CHECK(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists         IN OUT BOOLEAN,
                           I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE,
                           I_bracket_value1 IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION ITEM_COST_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_items_exist    IN OUT BOOLEAN,
                          O_items_approved IN OUT BOOLEAN,
                          I_supplier       IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                          I_dept           IN     SUP_BRACKET_COST.DEPT%TYPE,
                          I_location       IN     SUP_BRACKET_COST.LOCATION%TYPE,
                          I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE,
                          I_bracket_lvl    IN     SUPS.INV_MGMT_LVL%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION COST_CHG_DUPS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists         IN OUT BOOLEAN,
                       O_cost_change    IN OUT COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                       O_reason         IN OUT COST_SUSP_SUP_HEAD.REASON%TYPE,
                       I_bracket_value1 IN     COST_SUSP_SUP_DETAIL_LOC.BRACKET_VALUE1%TYPE,
                       I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION MAX_COST_CHG(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_cost_change    IN OUT COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                      I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE,
                      I_bracket_lvl    IN     SUPS.INV_MGMT_LVL%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION DEFAULT_NEXT_BRACKET_LVL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists         IN OUT BOOLEAN,
                                  I_supplier       IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                                  I_dept           IN     SUP_BRACKET_COST.DEPT%TYPE,
                                  I_location       IN     SUP_BRACKET_COST.LOCATION%TYPE,
                                  I_items_approved IN     BOOLEAN,
                                  I_bracket_lvl    IN     SUPS.INV_MGMT_LVL%TYPE,
                                  I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION DFLT_BKT_DUPS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists         IN OUT BOOLEAN,
                       O_cost_change    IN OUT COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                       I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
END SUP_BRACKET_COST_SQL;
/


