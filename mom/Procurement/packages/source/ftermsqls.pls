
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE FTERM_SQL AUTHID CURRENT_USER AS

-- Define a record type to store freight terms records
TYPE fterm_record IS RECORD(terms                freight_terms.freight_terms%TYPE,
                            description          freight_terms_tl.term_desc%TYPE,
                            start_date_active    freight_terms.start_date_active%TYPE,
                            end_date_active      freight_terms.end_date_active%TYPE,
                            enabled_flag         freight_terms.enabled_flag%TYPE); 

---------------------------------------------------------------------
-- FUNCTION PROCESS_TERMS will take the input terms record
-- and place the information into a local terms record which will
-- be used in the package to manipulate the data. It will then
-- call a series of support functions to perform all business logic
-- on the record.
---------------------------------------------------------------------
FUNCTION PROCESS_TERMS (O_text             IN OUT VARCHAR2,
                        I_fterm_record       IN     FTERM_RECORD)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION INSERT_TERMS will insert any valid terms on the terms
-- table.  It is called from PUT_TERMS
---------------------------------------------------------------------
FUNCTION INSERT_TERMS(O_text         IN OUT VARCHAR2,
                      I_fterm_record IN     FTERM_RECORD)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION UPDATE_TERMS will update any valid terms on the terms
-- table.  It is called from PUT_TERMS
---------------------------------------------------------------------
FUNCTION UPDATE_TERMS(O_text             IN OUT VARCHAR2,
                      I_fterm_record     IN     FTERM_RECORD)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION VALIDATE_TERMS is a wrapper function which is 
-- used to call CHECK_NULLS, CHECK_ENABLED for 
-- any terms record input into the package.
---------------------------------------------------------------------
FUNCTION VALIDATE_TERMS(O_text          IN OUT VARCHAR2,
                        I_fterm_record  IN     FTERM_RECORD)
return BOOLEAN;
---------------------------------------------------------------------
-- FUNCTION CHECK_NULLS will check an input value if it's null.  If
--  so, an error message will be created and the int_errors error
--  handling will be called.
---------------------------------------------------------------------
FUNCTION CHECK_NULLS (O_text            IN OUT VARCHAR2,
                      I_record_variable IN     VARCHAR2)
return BOOLEAN;
---------------------------------------------------------------------
END FTERM_SQL;
/
