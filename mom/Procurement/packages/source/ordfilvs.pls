
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORDER_FILTER_VALIDATE_SQL AUTHID CURRENT_USER AS

-----------------------------------------------------------------------------------
--- Function:   FILTER_PARENT
--- Purpose:	 Validates the :B_filter_head.TI_parent_item field
--- Called By:	ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_ITEM_PARENT(O_error_message	IN OUT	VARCHAR2,
			    I_order_no		IN	ORDLOC_WKSHT.ORDER_NO%TYPE,
			    I_item		IN      ORDLOC_WKSHT.ITEM%TYPE,
			    I_item_parent	IN	ORDLOC_WKSHT.ITEM_PARENT%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
--- Function:  FILTER_ITEM
--- Purpose:	Validates the :B_filter_head.TI_item field
--- Called By:	ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_ITEM(O_error_message	IN OUT	VARCHAR2,
                     I_order_no		IN	ORDLOC_WKSHT.ORDER_NO%TYPE,
                     I_item		IN	ORDLOC_WKSHT.ITEM%TYPE,
                     I_item_parent	IN	ORDLOC_WKSHT.ITEM_PARENT%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
--- Function:  FILTER_DIFF1
--- Purpose:	Validates the :B_filter_head.TI_diff1 field
--- Called By:	ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_DIFF1 (O_error_message	IN OUT	VARCHAR2,
                       I_order_no	IN	ORDLOC_WKSHT.ORDER_NO%TYPE,
                       I_item		IN	ORDLOC_WKSHT.ITEM%TYPE,
                       I_item_parent	IN	ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                       I_diff_1		IN	ORDLOC_WKSHT.DIFF_1%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
--- Function:  FILTER_STORE_GRADE
--- Purpose:	Validates the :B_filter_head.TI_store_grade field
--- Called By:	ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_STORE_GRADE (O_error_message	IN OUT	VARCHAR2,
                             I_order_no		IN	ORDLOC_WKSHT.ORDER_NO%TYPE,
                             I_item		IN	ORDLOC_WKSHT.ITEM%TYPE,
                             I_item_parent	IN	ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                             I_store_grade	IN	ORDLOC_WKSHT.STORE_GRADE%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
--- Function:  FILTER_GROUP_NO
--- Purpose:	Validates the :B_filter_head.TI_store_grade_group_no field
--- Called By:	ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_GROUP_NO (O_error_message		IN OUT	VARCHAR2,
                          O_store_grade_group_desc	IN OUT	STORE_GRADE_GROUP.STORE_GRADE_GROUP_DESC%TYPE,
                          I_order_no			IN	ORDLOC_WKSHT.ORDER_NO%TYPE,
                          I_item			IN	ORDLOC_WKSHT.ITEM%TYPE,
                          I_item_parent			IN	ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                          I_store_grade_group_id	IN	ORDLOC_WKSHT.STORE_GRADE_GROUP_ID%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
--- Function:  FILTER_DIFF2
--- Purpose:	Validates the :B_filter_head.TI_diff2 field
--- Called By:	ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_DIFF2 (O_error_message	IN OUT	VARCHAR2,
                       I_order_no	IN	ORDLOC_WKSHT.ORDER_NO%TYPE,
                       I_item		IN	ORDLOC_WKSHT.ITEM%TYPE,
                       I_item_parent	IN	ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                       I_diff_2		IN	ORDLOC_WKSHT.DIFF_2%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
--- Function:  FILTER_LOCATION
--- Purpose:	Validates the :B_filter_head.TI_location field
--- Called By:	ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_LOCATION (O_error_message	IN OUT	VARCHAR2,
                          I_order_no		IN	ORDLOC_WKSHT.ORDER_NO%TYPE,
                          I_item		IN	ORDLOC_WKSHT.ITEM%TYPE,
                          I_item_parent		IN	ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                          I_location		IN	ORDLOC_WKSHT.LOCATION%TYPE,
                          I_loc_type		IN	ORDLOC_WKSHT.LOC_TYPE%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
--- Function:  FILTER_STANDARD_UOM
--- Purpose:	Validates the :B_filter_head.TI_standard_uom field
--- Called By:	ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_STANDARD_UOM (O_error_message	IN OUT	VARCHAR2,
                              I_order_no	IN	ORDLOC_WKSHT.ORDER_NO%TYPE,
                              I_item		IN	ORDLOC_WKSHT.ITEM%TYPE,
                              I_item_parent	IN	ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                              I_standard_uom	IN	ORDLOC_WKSHT.STANDARD_UOM%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
--- Function:  FILTER_UOP
--- Purpose:	Validates the :B_filter_head.TI_uop field
--- Called By:	ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_UOP (O_error_message	IN OUT	VARCHAR2,
                     I_order_no		IN	ORDLOC_WKSHT.ORDER_NO%TYPE,
                     I_item		IN	ORDLOC_WKSHT.ITEM%TYPE,
                     I_item_parent	IN	ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                     I_uop		IN	CODE_DETAIL.CODE_DESC%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
--- Function:  FILTER_DIFF3
--- Purpose:	Validates the :B_filter_head.TI_diff3 field
--- Called By:	ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_DIFF3 (O_error_message	IN OUT	VARCHAR2,
                       I_order_no	IN	ORDLOC_WKSHT.ORDER_NO%TYPE,
                       I_item		IN	ORDLOC_WKSHT.ITEM%TYPE,
                       I_item_parent	IN	ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                       I_diff_3		IN	ORDLOC_WKSHT.DIFF_3%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------
--- Function:  FILTER_DIFF4
--- Purpose:	Validates the :B_filter_head.TI_diff4 field
--- Called By:	ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_DIFF4 (O_error_message	IN OUT	VARCHAR2,
                       I_order_no	IN	ORDLOC_WKSHT.ORDER_NO%TYPE,
                       I_item		IN	ORDLOC_WKSHT.ITEM%TYPE,
                       I_item_parent	IN	ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                       I_diff_4		IN	ORDLOC_WKSHT.DIFF_4%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------------

END ORDER_FILTER_VALIDATE_SQL;
/