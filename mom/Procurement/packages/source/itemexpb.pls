CREATE OR REPLACE PACKAGE BODY ITEM_EXPENSE_SQL  AS
---------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ(O_error_message IN OUT  VARCHAR2,
                      O_seq_no        IN OUT  ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                      I_item          IN      ITEM_MASTER.ITEM%TYPE,
                      I_supplier      IN      SUPS.SUPPLIER%TYPE,
                      I_item_exp_type IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE)
   return BOOLEAN is

      cursor C_MAX_SEQ is
         select nvl(max(item_exp_seq),0) + 1
           from item_exp_head ieh, item_master im , 
          (select nvl(nvl(item_grandparent,item_parent),item) parent_item 
           from item_master 
           where item=I_item) pim
          where ieh.item          = im.item
            and ( im.item = pim.parent_item 
                  or im.item_parent=pim.parent_item 
                  or im.item_grandparent=parent_item)


            and ieh.supplier      = I_supplier
            and ieh.item_exp_type = I_item_exp_type;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_MAX_SEQ', 'ITEM_EXP_HEAD', NULL);
   open C_MAX_SEQ;
   SQL_LIB.SET_MARK('FETCH', 'C_MAX_SEQ', 'ITEM_EXP_HEAD', NULL);
   fetch C_MAX_SEQ into O_seq_no;
   SQL_LIB.SET_MARK('CLOSE', 'C_MAX_SEQ', 'ITEM_EXP_HEAD', NULL);
   close C_MAX_SEQ;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_EXPENSE_SQL.GET_NEXT_SEQ',
                                             to_char(SQLCODE));
   return FALSE;
END GET_NEXT_SEQ;
---------------------------------------------------------------------------------------------
FUNCTION BASE_EXP_CHANGED (O_error_message       IN OUT  VARCHAR2,
                           I_item                IN      ITEM_MASTER.ITEM%TYPE,
                           I_supplier            IN      SUPS.SUPPLIER%TYPE,
                           I_item_exp_type       IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                           I_item_exp_seq        IN      ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                           I_base_exp_ind        IN      ITEM_EXP_HEAD.BASE_EXP_IND%TYPE,
                           I_origin_country_id   IN      ITEM_EXP_HEAD.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN is
      L_table        VARCHAR2(30);
      RECORD_LOCKED  EXCEPTION;
      PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

      cursor C_LOCK_TYPE is
         select 'X'
           from item_exp_head
          where item              = I_item
            and supplier          = I_supplier
            and item_exp_type     = I_item_exp_type
            and (I_origin_country_id is NULL or
                 origin_country_id = I_origin_country_id)
            for update nowait;

BEGIN
   L_table            := 'ITEM_EXP_HEAD';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_TYPE', 'ITEM_EXP_HEAD', NULL);
   open C_LOCK_TYPE;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_TYPE', 'ITEM_EXP_HEAD', NULL);
   close C_LOCK_TYPE;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'ITEM_EXP_HEAD', NULL);
   update item_exp_head
      set base_exp_ind         = 'N',
          last_update_datetime = sysdate,
          last_update_id       = SYS_CONTEXT('USERENV', 'SESSION_USER')
    where item                 = I_item
      and supplier             = I_supplier
      and item_exp_type        = I_item_exp_type
      and (I_origin_country_id is NULL or
           origin_country_id = I_origin_country_id);

   if I_base_exp_ind    = 'Y' then
   SQL_LIB.SET_MARK('UPDATE', NULL, 'ITEM_EXP_HEAD', NULL);
      update item_exp_head
         set base_exp_ind         = 'Y',
             last_update_datetime = sysdate,
             last_update_id       = SYS_CONTEXT('USERENV', 'SESSION_USER')
       where item                 = I_item
         and supplier             = I_supplier
         and item_exp_type        = I_item_exp_type
         and item_exp_seq         = I_item_exp_seq
         and (I_origin_country_id is NULL or
              origin_country_id = I_origin_country_id);
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_item,
                                             to_char(I_supplier));

      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_EXPENSE_SQL.BASE_EXP_CHANGED',
                                             to_char(SQLCODE));
   return FALSE;

END BASE_EXP_CHANGED;
---------------------------------------------------------------------------------------------
FUNCTION EXP_HEAD_EXIST(O_error_message     IN OUT  VARCHAR2,
                        O_exists            IN OUT  BOOLEAN,
                        I_item_exp_type     IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                        I_item              IN      ITEM_MASTER.ITEM%TYPE,
                        I_supplier          IN      SUPS.SUPPLIER%TYPE,
                        I_zone_id           IN      COST_ZONE.ZONE_ID%TYPE,
                        I_zone_group_id     IN      COST_ZONE.ZONE_GROUP_ID%TYPE,
                        I_origin_country_id IN      COUNTRY.COUNTRY_ID%TYPE,
                        I_lading_port       IN      OUTLOC.OUTLOC_ID%TYPE,
                        I_discharge_port    IN      OUTLOC.OUTLOC_ID%TYPE)
   return BOOLEAN is
      L_exists      VARCHAR2(1);

      cursor C_COUNTRY_EXISTS is
         select 'Y'
           from item_exp_head
          where item              = I_item
            and supplier          = I_supplier
            and item_exp_type     = 'C'
            and origin_country_id = I_origin_country_id
            and lading_port       = I_lading_port
            and discharge_port    = I_discharge_port;

      cursor C_ZONE_EXISTS is
         select 'Y'
           from item_exp_head
          where item              = I_item
            and supplier          = I_supplier
            and item_exp_type     = 'Z'
            and zone_group_id     = I_zone_group_id
            and zone_id           = I_zone_id
            and discharge_port    = I_discharge_port;


BEGIN
   O_exists := FALSE;
   if I_item_exp_type = 'C' then
      SQL_LIB.SET_MARK('OPEN', 'C_COUNTRY_EXISTS', 'ITEM_EXP_HEAD', NULL);
      open C_COUNTRY_EXISTS;
      SQL_LIB.SET_MARK('FETCH', 'C_COUNTRY_EXISTS', 'ITEM_EXP_HEAD', NULL);
      fetch C_COUNTRY_EXISTS into L_exists;

      if C_COUNTRY_EXISTS%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('DUP_ITEM_EXP_HEAD',NULL,NULL,NULL);
         O_exists        := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNTRY_EXISTS', 'ITEM_EXP_HEAD', NULL);
      close C_COUNTRY_EXISTS;

   elsif I_item_exp_type = 'Z' then
      SQL_LIB.SET_MARK('OPEN', 'C_ZONE_EXISTS', 'ITEM_EXP_HEAD', NULL);
      open C_ZONE_EXISTS;
      SQL_LIB.SET_MARK('FETCH', 'C_ZONE_EXISTS', 'ITEM_EXP_HEAD', NULL);
      fetch C_ZONE_EXISTS into L_exists;

      if C_ZONE_EXISTS%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('DUP_ITEM_EXP_ZONE',NULL,NULL,NULL);
         O_exists        := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_ZONE_EXISTS', 'ITEM_EXP_HEAD', NULL);
      close C_ZONE_EXISTS;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_EXPENSE_SQL.EXP_HEAD_EXIST',
                                             to_char(SQLCODE));
   return FALSE;

END EXP_HEAD_EXIST;
---------------------------------------------------------------------------------------------
FUNCTION EXP_DETAILS_EXIST(O_error_message     IN OUT  VARCHAR2,
                           O_exists            IN OUT  BOOLEAN,
                           I_item              IN      ITEM_MASTER.ITEM%TYPE,
                           I_supplier          IN      SUPS.SUPPLIER%TYPE,
                           I_item_exp_type     IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                           I_item_exp_seq      IN      ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                           I_comp_id           IN      ELC_COMP.COMP_ID%TYPE)
   return BOOLEAN is
      L_exists    VARCHAR2(1);

      cursor C_DETAIL_EXIST is
         select 'Y'
           from item_exp_detail
          where item            = I_item
            and supplier        = I_supplier
            and item_exp_type   = I_item_exp_type
            and item_exp_seq    = I_item_exp_seq
            and comp_id         = I_comp_id;
BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN', 'C_DETAIL_EXIST', 'ITEM_EXP_HEAD', NULL);
   open C_DETAIL_EXIST;
   SQL_LIB.SET_MARK('FETCH', 'C_DETAIL_EXIST', 'ITEM_EXP_HEAD', NULL);
   fetch C_DETAIL_EXIST into L_exists;
   if C_DETAIL_EXIST%FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_ITEM_EXP_DETAIL',NULL,NULL,NULL);
      O_exists        := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_DETAIL_EXIST', 'ITEM_EXP_HEAD', NULL);
   close C_DETAIL_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_EXPENSE_SQL.EXP_DETAILS_EXIST',
                                             to_char(SQLCODE));
   return FALSE;

END EXP_DETAILS_EXIST;
---------------------------------------------------------------------------------------------
FUNCTION LOCK_EXP_DETAILS(O_error_message    IN OUT  VARCHAR2,
                          I_item              IN      ITEM_MASTER.ITEM%TYPE,
                          I_supplier          IN      SUPS.SUPPLIER%TYPE,
                          I_item_exp_type     IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                          I_item_exp_seq      IN      ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE)
   return BOOLEAN is

      L_table        VARCHAR2(30);
      RECORD_LOCKED  EXCEPTION;
      PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

      cursor C_LOCK_ITEM_DETAIL is
         select 'X'
           from item_exp_detail
          where item           = I_item
            and supplier       = I_supplier
            and item_exp_type  = I_item_exp_type
            and item_exp_seq   = I_item_exp_seq
            for update nowait;

BEGIN
   L_table := 'ITEM_EXP_DETAIL';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_DETAIL', 'ITEM_EXP_DETAIL', NULL);
   open C_LOCK_ITEM_DETAIL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_DETAIL', 'ITEM_EXP_DETAIL', NULL);
   close C_LOCK_ITEM_DETAIL;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_item,
                                             to_char(I_supplier));

      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_EXPENSE_SQL.LOCK_EXP_DETAILS',
                                             to_char(SQLCODE));
      return FALSE;
END LOCK_EXP_DETAILS;

---------------------------------------------------------------------------------------------
FUNCTION DEL_EXP_DETAILS(O_error_message     IN OUT  VARCHAR2,
                         I_item              IN      ITEM_MASTER.ITEM%TYPE,
                         I_supplier          IN      SUPS.SUPPLIER%TYPE,
                         I_item_exp_type     IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                         I_item_exp_seq      IN      ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE)
   return BOOLEAN is

BEGIN
   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_EXP_DETAIL', NULL);
   delete from item_exp_detail
          where item           = I_item
            and supplier       = I_supplier
            and item_exp_type  = I_item_exp_type
            and item_exp_seq   = I_item_exp_seq;

   SQL_LIB.SET_MARK('DELETE', NULL, 'COST_COMP_UPD_STG', NULL);
   delete from cost_comp_upd_stg
          where item           = I_item
            and supplier       = I_supplier
            and item_exp_type  = I_item_exp_type
            and item_exp_seq   = I_item_exp_seq;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_EXPENSE_SQL.DEL_EXP_DETAILS',
                                             to_char(SQLCODE));
   return FALSE;
END DEL_EXP_DETAILS;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_HEADER_NO_DETAILS(O_error_message     IN OUT  VARCHAR2,
                                 O_exists            IN OUT  BOOLEAN,
                                 I_item              IN      ITEM_MASTER.ITEM%TYPE,
                                 I_supplier          IN      SUPS.SUPPLIER%TYPE,
                                 I_item_exp_type     IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE)
   return BOOLEAN is

      L_exists      VARCHAR2(1);

      cursor C_CHECK_FOR_DETAILS is
         select 'X'
           from item_exp_head ih
          where ih.item            = I_item
            and ih.supplier        = I_supplier
            and ih.item_exp_type   = I_item_exp_type
            and ih.item_exp_seq not in
                  (select id.item_exp_seq
                     from item_exp_detail id
                    where id.item            = I_item
                      and id.supplier        = I_supplier
                      and id.item_exp_type   = I_item_exp_type);

BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK FOR_DETAILS', 'ITEM_EXP_HEAD, ITEM_EXP_DETAIL', NULL);
   open C_CHECK_FOR_DETAILS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK FOR_DETAILS', 'ITEM_EXP_HEAD, ITEM_EXP_DETAIL', NULL);
   fetch C_CHECK_FOR_DETAILS into L_exists;

   if C_CHECK_FOR_DETAILS%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK FOR_DETAILS', 'ITEM_EXP_HEAD', NULL);
   close C_CHECK_FOR_DETAILS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_EXPENSE_SQL.CHECK_HEADER_NO_DETAILS',
                                             to_char(SQLCODE));
   return FALSE;

END CHECK_HEADER_NO_DETAILS;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_HEADER(O_error_message     IN OUT  VARCHAR2,
                       I_item              IN      ITEM_MASTER.ITEM%TYPE,
                       I_supplier          IN      SUPS.SUPPLIER%TYPE,
                       I_item_exp_type     IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE)
   return BOOLEAN is

      L_table        VARCHAR2(30);
      RECORD_LOCKED  EXCEPTION;
      PRAGMA         EXCEPTION_INIT(Record_Locked, -54);


      cursor C_LOCK_ITEM_HEAD is
         select 'X'
           from item_exp_head ih
          where ih.item            = I_item
            and ih.supplier        = I_supplier
            and ih.item_exp_type   = I_item_exp_type
            and ih.item_exp_seq not in
                  (select id.item_exp_seq
                     from item_exp_detail id
                    where id.item            = I_item
                      and id.supplier        = I_supplier
                      and id.item_exp_type   = I_item_exp_type)
            for update nowait;

BEGIN
   L_table := 'ITEM_EXP_HEAD';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_HEAD', 'ITEM_EXP_HEAD, ITEM_EXP_DETAIL', NULL);
   open C_LOCK_ITEM_HEAD;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_HEAD', 'ITEM_EXP_HEAD, ITEM_EXP_DETAIL', NULL);
   close C_LOCK_ITEM_HEAD;

   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_EXP_HEAD, ITEM_EXP_DETAIL', NULL);
   delete from item_exp_head ih
          where ih.item            = I_item
            and ih.supplier        = I_supplier
            and ih.item_exp_type   = I_item_exp_type
            and ih.item_exp_seq not in
                  (select id.item_exp_seq
                     from item_exp_detail id
                    where id.item            = I_item
                      and id.supplier        = I_supplier
                      and id.item_exp_type   = I_item_exp_type);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_item,
                                             to_char(I_supplier));

      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_EXPENSE_SQL.DELETE_HEADER',
                                             to_char(SQLCODE));
   return FALSE;

END DELETE_HEADER;
------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_EXPENSES(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_supplier          IN     SUPS.SUPPLIER%TYPE,
                          I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(40)   := 'ITEM_EXPENSE_SQL.DEFAULT_EXPENSES';
   L_zone_group_id      COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE := NULL;

BEGIN
   if DEFAULT_EXPENSES(O_error_message,
                       I_item,
                       I_supplier,
                       I_origin_country_id,
                       L_zone_group_id)= FALSE  then
      return FALSE;
   end if;

return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DEFAULT_EXPENSES;
------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_EXPENSES(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_supplier          IN     SUPS.SUPPLIER%TYPE,
                          I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                          I_cost_zone_group   IN     ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(40)   := 'ITEM_EXPENSE_SQL.DEFAULT_EXPENSES';
   L_records_inserted   VARCHAR2(1)    := 'N';
   L_zone_group_id      COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   L_exp_type           ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE;
   L_module             EXP_PROF_HEAD.MODULE%TYPE := 'SUPP';
   L_seq_no             ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE;

   cursor C_SEQ_NO is
      select nvl(max(item_exp_seq), 0) + 1
        from item_exp_head ieh,  item_master im , 
       (select nvl(nvl(item_grandparent,item_parent),item) parent_item 
        from item_master 
        where item=I_item) pim
        where ieh.item          = im.item
         and ( im.item = pim.parent_item 
               or im.item_parent=pim.parent_item 
               or im.item_grandparent=parent_item)

         and supplier      = I_supplier
         and item_exp_type = L_exp_type;

   cursor C_ZONES is
      select zone_id
        from cost_zone
       where zone_group_id = L_zone_group_id;

   cursor C_PTNR is
      select partner_type_1 partner_type,
             partner_1 partner
        from sup_import_attr
       where supplier = I_supplier
         and partner_type_1 is not null
         and partner_1 is not null
       UNION
      select partner_type_2 partner_type,
             partner_2 partner
        from sup_import_attr
       where supplier = I_supplier
         and partner_type_2 is not null
         and partner_2 is not null
       UNION
      select partner_type_3 partner_type,
             partner_3 partner
        from sup_import_attr
       where supplier = I_supplier
         and partner_type_3 is not null
         and partner_3 is not null
       UNION
      select 'AG' partner_type,
             agent partner
        from sup_import_attr
       where supplier = I_supplier
         and agent is not null
       UNION
      select 'BK' partner_type,
             advising_bank partner
        from sup_import_attr
       where supplier = I_supplier
         and advising_bank is not null
       UNION
      select 'BK' partner_type,
             issuing_bank partner
        from sup_import_attr
       where supplier = I_supplier
         and issuing_bank is not null
       UNION
      select 'FA' partner_type,
             factory partner
        from sup_import_attr
       where supplier = I_supplier
         and factory is not null;

BEGIN
   if I_origin_country_id is NULL then
      L_exp_type := 'Z';
   else
      L_exp_type := 'C';
   end if;
   ---
   if L_exp_type = 'Z' then
      if I_cost_zone_group is null then
         if ITEM_ATTRIB_SQL.GET_COST_ZONE_GROUP(O_error_message,
                                                L_zone_group_id,
                                                I_item) = FALSE then
            return FALSE;
         end if;
      else
         L_zone_group_id  :=I_cost_zone_group;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_SEQ_NO','ITEM_EXP_HEAD',NULL);
   open C_SEQ_NO;
   SQL_LIB.SET_MARK('FETCH','C_SEQ_NO','ITEM_EXP_HEAD',NULL);
   fetch C_SEQ_NO into L_seq_no;
   SQL_LIB.SET_MARK('CLOSE','C_SEQ_NO','ITEM_EXP_HEAD',NULL);
   close C_SEQ_NO;
   ---
   if INSERT_EXPENSES(O_error_message,
                      L_records_inserted,
                      I_item,
                      I_supplier,
                      I_origin_country_id,
                      L_zone_group_id,
                      L_module,
                      L_exp_type,
                      L_seq_no,
                      NULL,
                      NULL) = FALSE then
      return FALSE;
   end if;
   ---
   if L_records_inserted = 'N' then   -- Get Base Profiles
      if L_exp_type = 'Z' then
         FOR C_rec in C_ZONES LOOP
            insert into item_exp_head(item,
                                      supplier,
                                      item_exp_type,
                                      item_exp_seq,
                                      origin_country_id,
                                      zone_id,
                                      lading_port,
                                      discharge_port,
                                      zone_group_id,
                                      base_exp_ind,
                                      create_datetime,
                                      last_update_datetime,
                                      last_update_id,
                                      create_id)
                              (select I_item,
                                      I_supplier,
                                      'Z',
                                      L_seq_no,
                                      NULL,
                                      C_rec.zone_id,
                                      NULL,
                                      eph.discharge_port,
                                      L_zone_group_id,
                                      'N',
                                      sysdate,
                                      sysdate,
                                      get_user,
                                      get_user
                                 from exp_prof_head eph
                                where eph.module        = 'SUPP'
                                  and eph.key_value_1   = to_char(I_supplier)
                                  and eph.exp_prof_type = L_exp_type
                                  and eph.base_prof_ind = 'Y'
                                  and not exists(select 'x'
                                                   from item_exp_head ieh
                                                  where ieh.item           = I_item
                                                    and ieh.supplier       = I_supplier
                                                    and ieh.item_exp_type  = L_exp_type
                                                    and ieh.zone_id        = C_rec.zone_id
                                                    and ieh.zone_group_id  = L_zone_group_id
                                                    and ieh.discharge_port = eph.discharge_port)
                                  and exists( select 'x'
                                                from item_supp_country isc
                                               where isc.item              = I_item
                                                 and isc.supplier          = I_supplier)
                               UNION
                               select I_item,
                                      I_supplier,
                                      'Z',
                                      L_seq_no,
                                      NULL,
                                      C_rec.zone_id,
                                      NULL,
                                      eph.discharge_port,
                                      L_zone_group_id,
                                      'N',
                                      sysdate,
                                      sysdate,
                                      get_user,
                                      get_user
                                 from exp_prof_head eph
                                where eph.module        = 'PTNR'
                                  and (eph.key_value_1,eph.key_value_2) in
                                        (
                                            select partner_type_1 partner_type,
                                                   partner_1 partner
                                              from sup_import_attr
                                             where supplier = to_char(I_supplier)
                                               and partner_type_1 is not null
                                               and partner_1 is not null
                                             UNION
                                            select partner_type_2 partner_type,
                                                   partner_2 partner
                                              from sup_import_attr
                                             where supplier = to_char(I_supplier)
                                               and partner_type_2 is not null
                                              and partner_2 is not null
                                             UNION
                                            select partner_type_3 partner_type,
                                                   partner_3 partner
                                              from sup_import_attr
                                             where supplier = to_char(I_supplier)
                                              and partner_type_3 is not null
                                              and partner_3 is not null
                                            UNION
                                            select 'AG',
                                                   agent
                                              from sup_import_attr
                                             where supplier = I_supplier
                                               and agent is not NULL
                                            UNION
                                            select 'BK',
                                                   advising_bank
                                              from sup_import_attr
                                             where supplier = I_supplier
                                               and advising_bank is not NULL
                                            UNION
                                            select 'BK',
                                                   issuing_bank
                                              from sup_import_attr
                                             where supplier = I_supplier
                                               and issuing_bank is not NULL
                                        )
                                  and eph.exp_prof_type = L_exp_type
                                  and eph.base_prof_ind = 'Y'
                                  and not exists(select 'x'
                                                   from item_exp_head ieh
                                                  where ieh.item           = I_item
                                                    and ieh.supplier       = I_supplier
                                                    and ieh.item_exp_type  = L_exp_type
                                                    and ieh.zone_id        = C_rec.zone_id
                                                    and ieh.zone_group_id  = L_zone_group_id
                                                    and ieh.discharge_port = eph.discharge_port)
                                  and exists( select 'x'
                                                from item_supp_country isc
                                               where isc.item              = I_item
                                                 and isc.supplier          = I_supplier));
         ---
         insert into item_exp_detail(item,
                                     supplier,
                                     item_exp_type,
                                     item_exp_seq,
                                     comp_id,
                                     cvb_code,
                                     comp_rate,
                                     comp_currency,
                                     per_count,
                                     per_count_uom,
                                     est_exp_value,
                                     nom_flag_1,
                                     nom_flag_2,
                                     nom_flag_3,
                                     nom_flag_4,
                                     nom_flag_5,
                                     display_order,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id,
                                     defaulted_from,
                                     key_value_1,
                                     create_id)
                              select I_item,
                                     I_supplier,
                                     'Z',
                                     ieh.item_exp_seq,
                                     epd.comp_id,
                                     epd.cvb_code,
                                     epd.comp_rate,
                                     epd.comp_currency,
                                     epd.per_count,
                                     epd.per_count_uom,
                                     0,
                                     epd.nom_flag_1,
                                     epd.nom_flag_2,
                                     epd.nom_flag_3,
                                     epd.nom_flag_4,
                                     epd.nom_flag_5,
                                     elc.display_order,
                                     sysdate,
                                     sysdate,
                                     get_user,
                                     'S',
                                     I_supplier,
                                     get_user
                                from exp_prof_detail epd,
                                     exp_prof_head eph,
                                     item_exp_head ieh,
                                     elc_comp elc
                               where eph.module         = 'SUPP'
                                 and eph.key_value_1    = to_char(I_supplier)
                                 and eph.exp_prof_type  = L_exp_type
                                 and eph.base_prof_ind  = 'Y'
                                 and eph.exp_prof_key   = epd.exp_prof_key
                                 and epd.comp_id        = elc.comp_id
                                 and ieh.item           = I_item
                                 and ieh.supplier       = I_supplier
                                 and ieh.zone_group_id  = L_zone_group_id
                                 and ieh.item_exp_type  = eph.exp_prof_type
                                 and ieh.discharge_port = eph.discharge_port
                                 and not exists(select 'x'
                                                  from item_exp_detail ied
                                                 where ied.item           = I_item
                                                   and ied.supplier       = I_supplier
                                                   and ied.item_exp_type  = L_exp_type
                                                   and ied.item_exp_seq   = ieh.item_exp_seq
                                                   and ied.comp_id        = epd.comp_id);
            L_seq_no := L_seq_no + 1;
         END LOOP;

         --
         for C_rec_sia in C_PTNR loop
            insert into item_exp_detail(item,
                                        supplier,
                                        item_exp_type,
                                        item_exp_seq,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        comp_currency,
                                        per_count,
                                        per_count_uom,
                                        est_exp_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id,
                                        defaulted_from,
                                        key_value_1,
                                        key_value_2,
                                        create_id)
                                 select I_item,
                                        I_supplier,
                                        'Z',
                                        ieh.item_exp_seq,
                                        epd.comp_id,
                                        epd.cvb_code,
                                        epd.comp_rate,
                                        epd.comp_currency,
                                        epd.per_count,
                                        epd.per_count_uom,
                                        0,
                                        epd.nom_flag_1,
                                        epd.nom_flag_2,
                                        epd.nom_flag_3,
                                        epd.nom_flag_4,
                                        epd.nom_flag_5,
                                        elc.display_order,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        'P',
                                        C_rec_sia.partner_type,
                                        C_rec_sia.partner,
                                        get_user
                                   from exp_prof_detail epd,
                                        exp_prof_head eph,
                                        item_exp_head ieh,
                                        elc_comp elc
                                  where eph.module         = 'PTNR'
                                    and eph.key_value_1    = C_rec_sia.partner_type
                                    and eph.key_value_2    = C_rec_sia.partner
                                    and eph.exp_prof_type  = L_exp_type
                                    and eph.base_prof_ind  = 'Y'
                                    and eph.exp_prof_key   = epd.exp_prof_key
                                    and epd.comp_id        = elc.comp_id
                                    and ieh.item           = I_item
                                    and ieh.supplier       = I_supplier
                                    and ieh.zone_group_id  = L_zone_group_id
                                    and ieh.item_exp_type  = eph.exp_prof_type
                                    and ieh.discharge_port = eph.discharge_port
                                    and not exists(select 'x'
                                                     from item_exp_detail ied
                                                    where ied.item           = I_item
                                                      and ied.supplier       = I_supplier
                                                      and ied.item_exp_type  = L_exp_type
                                                      and ied.item_exp_seq   = ieh.item_exp_seq
                                                     and ied.comp_id        = epd.comp_id);
         end loop;
         ---

      else -- Get Supplier 'Country' level base profiles
         insert into item_exp_head(item,
                                   supplier,
                                   item_exp_type,
                                   item_exp_seq,
                                   origin_country_id,
                                   zone_id,
                                   lading_port,
                                   discharge_port,
                                   zone_group_id,
                                   base_exp_ind,
                                   create_datetime,
                                   last_update_datetime,
                                   last_update_id,
                                   create_id)
                            select I_item,
                                   I_supplier,
                                   'C',
                                   L_seq_no,
                                   I_origin_country_id,
                                   NULL,
                                   eph.lading_port,
                                   eph.discharge_port,
                                   NULL,
                                   'N',
                                   sysdate,
                                   sysdate,
                                   get_user,
                                   get_user
                              from exp_prof_head eph
                             where eph.module        = 'SUPP'
                               and eph.key_value_1   = to_char(I_supplier)
                               and eph.exp_prof_type = L_exp_type
                               and eph.base_prof_ind = 'Y'
                               and not exists(select 'x'
                                                from item_exp_head ieh
                                               where ieh.item              = I_item
                                                 and ieh.supplier          = I_supplier
                                                 and ieh.item_exp_type     = L_exp_type
                                                 and ieh.origin_country_id = I_origin_country_id
                                                 and ieh.lading_port       = eph.lading_port
                                                 and ieh.discharge_port    = eph.discharge_port)
                               and exists( select 'x'
                                             from item_supp_country isc
                                             where isc.item              = I_item
                                               and isc.supplier          = I_supplier
                                               and isc.origin_country_id = I_origin_country_id)
                            UNION
                            select I_item,
                                   I_supplier,
                                   'C',
                                   L_seq_no,
                                   I_origin_country_id,
                                   NULL,
                                   eph.lading_port,
                                   eph.discharge_port,
                                   NULL,
                                   'N',
                                   sysdate,
                                   sysdate,
                                   get_user,
                                   get_user
                              from exp_prof_head eph
                             where eph.module        = 'PTNR'
                               and (eph.key_value_1,eph.key_value_2) in
                                        (
                                            select partner_type_1 partner_type,
                                                   partner_1 partner
                                              from sup_import_attr
                                             where supplier = to_char(I_supplier)
                                               and partner_type_1 is not null
                                               and partner_1 is not null
                                             UNION
                                            select partner_type_2 partner_type,
                                                   partner_2 partner
                                              from sup_import_attr
                                             where supplier = to_char(I_supplier)
                                               and partner_type_2 is not null
                                              and partner_2 is not null
                                             UNION
                                            select partner_type_3 partner_type,
                                                   partner_3 partner
                                              from sup_import_attr
                                             where supplier = to_char(I_supplier)
                                              and partner_type_3 is not null
                                              and partner_3 is not null
                                            UNION
                                            select 'AG',
                                                   agent
                                              from sup_import_attr
                                             where supplier = I_supplier
                                               and agent is not NULL
                                            UNION
                                            select 'BK',
                                                   advising_bank
                                              from sup_import_attr
                                             where supplier = I_supplier
                                               and advising_bank is not NULL
                                            UNION
                                            select 'BK',
                                                   issuing_bank
                                              from sup_import_attr
                                             where supplier = I_supplier
                                               and issuing_bank is not NULL
                                        )
                               and eph.exp_prof_type = L_exp_type
                               and eph.base_prof_ind = 'Y'
                               and not exists(select 'x'
                                                from item_exp_head ieh
                                               where ieh.item              = I_item
                                                 and ieh.supplier          = I_supplier
                                                 and ieh.item_exp_type     = L_exp_type
                                                 and ieh.origin_country_id = I_origin_country_id
                                                 and ieh.lading_port       = eph.lading_port
                                                 and ieh.discharge_port    = eph.discharge_port)
                               and exists( select 'x'
                                             from item_supp_country isc
                                             where isc.item              = I_item
                                               and isc.supplier          = I_supplier
                                               and isc.origin_country_id = I_origin_country_id);
         ---
         insert into item_exp_detail(item,
                                     supplier,
                                     item_exp_type,
                                     item_exp_seq,
                                     comp_id,
                                     cvb_code,
                                     comp_rate,
                                     comp_currency,
                                     per_count,
                                     per_count_uom,
                                     est_exp_value,
                                     nom_flag_1,
                                     nom_flag_2,
                                     nom_flag_3,
                                     nom_flag_4,
                                     nom_flag_5,
                                     display_order,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id,
                                     defaulted_from,
                                     key_value_1,
                                     create_id)
                              select I_item,
                                     I_supplier,
                                     'C',
                                     ieh.item_exp_seq,
                                     epd.comp_id,
                                     epd.cvb_code,
                                     epd.comp_rate,
                                     epd.comp_currency,
                                     epd.per_count,
                                     epd.per_count_uom,
                                     0,
                                     epd.nom_flag_1,
                                     epd.nom_flag_2,
                                     epd.nom_flag_3,
                                     epd.nom_flag_4,
                                     epd.nom_flag_5,
                                     elc.display_order,
                                     sysdate,
                                     sysdate,
                                     get_user,
                                     'S',
                                     I_supplier,
                                     get_user
                                from exp_prof_detail epd,
                                     exp_prof_head eph,
                                     item_exp_head ieh,
                                     elc_comp elc
                               where eph.module            = 'SUPP'
                                 and eph.key_value_1       = to_char(I_supplier)
                                 and eph.exp_prof_type     = L_exp_type
                                 and eph.base_prof_ind     = 'Y'
                                 and eph.exp_prof_key      = epd.exp_prof_key
                                 and epd.comp_id           = elc.comp_id
                                 and ieh.item              = I_item
                                 and ieh.supplier          = I_supplier
                                 and ieh.item_exp_type     = eph.exp_prof_type
                                 and ieh.discharge_port    = eph.discharge_port
                                 and ieh.origin_country_id = I_origin_country_id
                                 and ieh.lading_port       = eph.lading_port
                                 and not exists(select 'x'
                                                  from item_exp_detail ied
                                                 where ied.item           = I_item
                                                   and ied.supplier       = I_supplier
                                                   and ied.item_exp_type  = L_exp_type
                                                   and ied.item_exp_seq   = ieh.item_exp_seq
                                                   and ied.comp_id        = epd.comp_id);

         for C_rec_sia in C_ptnr loop
            insert into item_exp_detail(item,
                                        supplier,
                                        item_exp_type,
                                        item_exp_seq,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        comp_currency,
                                        per_count,
                                        per_count_uom,
                                        est_exp_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id,
                                        defaulted_from,
                                        key_value_1,
                                        key_value_2,
                                        create_id)
                                 select I_item,
                                        I_supplier,
                                        'C',
                                        ieh.item_exp_seq,
                                        epd.comp_id,
                                        epd.cvb_code,
                                        epd.comp_rate,
                                        epd.comp_currency,
                                        epd.per_count,
                                        epd.per_count_uom,
                                        0,
                                        epd.nom_flag_1,
                                        epd.nom_flag_2,
                                        epd.nom_flag_3,
                                        epd.nom_flag_4,
                                        epd.nom_flag_5,
                                        elc.display_order,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        'P',
                                        C_rec_sia.partner_type,
                                        C_rec_sia.partner,
                                        get_user
                                   from exp_prof_detail epd,
                                        exp_prof_head eph,
                                        item_exp_head ieh,
                                        elc_comp elc
                                  where eph.module            = 'PTNR'
                                    and eph.key_value_1       = C_rec_sia.partner_type
                                    and eph.key_value_2       = C_rec_sia.partner
                                    and eph.exp_prof_type     = L_exp_type
                                    and eph.base_prof_ind     = 'Y'
                                    and eph.exp_prof_key      = epd.exp_prof_key
                                    and epd.comp_id           = elc.comp_id
                                    and ieh.item              = I_item
                                    and ieh.supplier          = I_supplier
                                    and ieh.item_exp_type     = eph.exp_prof_type
                                    and ieh.discharge_port    = eph.discharge_port
                                    and ieh.origin_country_id = I_origin_country_id
                                    and ieh.lading_port       = eph.lading_port
                                    and not exists(select 'x'
                                                     from item_exp_detail ied
                                                    where ied.item           = I_item
                                                      and ied.supplier       = I_supplier
                                                      and ied.item_exp_type  = L_exp_type
                                                      and ied.item_exp_seq   = ieh.item_exp_seq
                                                      and ied.comp_id        = epd.comp_id);
         end loop;
         ---
      end if;
   end if;

   if L_module = 'SUPP' then -- Default all the partner's expenses.
      for rec in C_ptnr loop
         ---
         SQL_LIB.SET_MARK('OPEN','C_SEQ_NO','ITEM_EXP_HEAD',NULL);
         open C_seq_no;
         SQL_LIB.SET_MARK('FETCH','C_SEQ_NO','ITEM_EXP_HEAD',NULL);
         fetch C_seq_no into L_seq_no;
         SQL_LIB.SET_MARK('CLOSE','C_SEQ_NO','ITEM_EXP_HEAD',NULL);
         close C_seq_no;
         ---

         if INSERT_EXPENSES(O_error_message,
                            L_records_inserted,
                            I_item,
                            I_supplier,
                            I_origin_country_id,
                            L_zone_group_id,
                            'PTNR',
                            L_exp_type,
                            L_seq_no,
                            rec.partner_type,
                            rec.partner) = FALSE then
            return FALSE;
         end if;

         if L_records_inserted = 'N' then   -- Get Base Profiles
            if L_exp_type = 'Z' then
               for C_rec in C_zones loop
                  insert into item_exp_head(item,
                                            supplier,
                                            item_exp_type,
                                            item_exp_seq,
                                            origin_country_id,
                                            zone_id,
                                            lading_port,
                                            discharge_port,
                                            zone_group_id,
                                            base_exp_ind,
                                            create_datetime,
                                            last_update_datetime,
                                            last_update_id,
                                            create_id)
                                     select I_item,
                                            I_supplier,
                                            'Z',
                                            L_seq_no,
                                            NULL,
                                            C_rec.zone_id,
                                            NULL,
                                            eph.discharge_port,
                                            L_zone_group_id,
                                            'N',
                                            sysdate,
                                            sysdate,
                                            get_user,
                                            get_user
                                       from exp_prof_head eph
                                      where eph.module        = 'PTNR'
                                        and eph.key_value_1   = rec.partner_type
                                        and eph.key_value_2   = rec.partner
                                        and eph.exp_prof_type = L_exp_type
                                        and eph.base_prof_ind = 'Y'
                                        and not exists(select 'x'
                                                         from item_exp_head ieh
                                                        where ieh.item           = I_item
                                                          and ieh.supplier       = I_supplier
                                                          and ieh.item_exp_type  = L_exp_type
                                                          and ieh.zone_id        = C_rec.zone_id
                                                          and ieh.zone_group_id  = L_zone_group_id
                                                          and ieh.discharge_port = eph.discharge_port)
                                        and exists( select 'x'
                                                      from item_supp_country isc
                                                     where isc.item              = I_item
                                                       and isc.supplier          = I_supplier);
                  ---
                  insert into item_exp_detail(item,
                                              supplier,
                                              item_exp_type,
                                              item_exp_seq,
                                              comp_id,
                                              cvb_code,
                                              comp_rate,
                                              comp_currency,
                                              per_count,
                                              per_count_uom,
                                              est_exp_value,
                                              nom_flag_1,
                                              nom_flag_2,
                                              nom_flag_3,
                                              nom_flag_4,
                                              nom_flag_5,
                                              display_order,
                                              create_datetime,
                                              last_update_datetime,
                                              last_update_id,
                                              defaulted_from,
                                              key_value_1,
                                              key_value_2,
                                              create_id)
                                       select I_item,
                                              I_supplier,
                                              'Z',
                                              ieh.item_exp_seq,
                                              epd.comp_id,
                                              epd.cvb_code,
                                              epd.comp_rate,
                                              epd.comp_currency,
                                              epd.per_count,
                                              epd.per_count_uom,
                                              0,
                                              epd.nom_flag_1,
                                              epd.nom_flag_2,
                                              epd.nom_flag_3,
                                              epd.nom_flag_4,
                                              epd.nom_flag_5,
                                              elc.display_order,
                                              sysdate,
                                              sysdate,
                                              get_user,
                                              'P',
                                              rec.partner_type,
                                              rec.partner,
                                              get_user
                                         from exp_prof_detail epd,
                                              exp_prof_head eph,
                                              item_exp_head ieh,
                                              elc_comp elc
                                        where eph.module         = 'PTNR'
                                          and eph.key_value_1    = rec.partner_type
                                          and eph.key_value_2    = rec.partner
                                          and eph.exp_prof_type  = L_exp_type
                                          and eph.base_prof_ind  = 'Y'
                                          and eph.exp_prof_key   = epd.exp_prof_key
                                          and epd.comp_id        = elc.comp_id
                                          and ieh.item           = I_item
                                          and ieh.supplier       = I_supplier
                                          and ieh.zone_group_id  = L_zone_group_id
                                          and ieh.item_exp_type  = eph.exp_prof_type
                                          and ieh.discharge_port = eph.discharge_port
                                          and not exists(select 'x'
                                                           from item_exp_detail ied
                                                          where ied.item           = I_item
                                                            and ied.supplier       = I_supplier
                                                            and ied.item_exp_type  = L_exp_type
                                                            and ied.item_exp_seq   = ieh.item_exp_seq
                                                            and ied.comp_id        = epd.comp_id);
                     L_seq_no := L_seq_no + 1;
               end loop;
            else -- Get Supplier 'Country' level base profiles
               insert into item_exp_head(item,
                                         supplier,
                                         item_exp_type,
                                         item_exp_seq,
                                         origin_country_id,
                                         zone_id,
                                         lading_port,
                                         discharge_port,
                                         zone_group_id,
                                         base_exp_ind,
                                         create_datetime,
                                         last_update_datetime,
                                         last_update_id,
                                         create_id)
                                  select I_item,
                                         I_supplier,
                                         'C',
                                         L_seq_no,
                                         I_origin_country_id,
                                         NULL,
                                         eph.lading_port,
                                         eph.discharge_port,
                                         NULL,
                                         'N',
                                         sysdate,
                                         sysdate,
                                         get_user,
                                         get_user
                                    from exp_prof_head eph
                                   where eph.module        = 'PTNR'
                                     and eph.key_value_1   = rec.partner_type
                                     and eph.key_value_2   = rec.partner
                                     and eph.exp_prof_type = L_exp_type
                                     and eph.base_prof_ind = 'Y'
                                     and not exists(select 'x'
                                                      from item_exp_head ieh
                                                     where ieh.item              = I_item
                                                       and ieh.supplier          = I_supplier
                                                       and ieh.item_exp_type     = L_exp_type
                                                       and ieh.origin_country_id = I_origin_country_id
                                                       and ieh.lading_port       = eph.lading_port
                                                       and ieh.discharge_port    = eph.discharge_port)
                                      and exists( select 'x'
                                                    from item_supp_country isc
                                                   where isc.item              = I_item
                                                     and isc.supplier          = I_supplier
                                                     and isc.origin_country_id = I_origin_country_id);
                ---
               insert into item_exp_detail(item,
                                           supplier,
                                           item_exp_type,
                                           item_exp_seq,
                                           comp_id,
                                           cvb_code,
                                           comp_rate,
                                           comp_currency,
                                           per_count,
                                           per_count_uom,
                                           est_exp_value,
                                           nom_flag_1,
                                           nom_flag_2,
                                           nom_flag_3,
                                           nom_flag_4,
                                           nom_flag_5,
                                           display_order,
                                           create_datetime,
                                           last_update_datetime,
                                           last_update_id,
                                           defaulted_from,
                                           key_value_1,
                                           key_value_2,
                                           create_id)
                                    select I_item,
                                           I_supplier,
                                           'C',
                                           ieh.item_exp_seq,
                                           epd.comp_id,
                                           epd.cvb_code,
                                           epd.comp_rate,
                                           epd.comp_currency,
                                           epd.per_count,
                                           epd.per_count_uom,
                                           0,
                                           epd.nom_flag_1,
                                           epd.nom_flag_2,
                                           epd.nom_flag_3,
                                           epd.nom_flag_4,
                                           epd.nom_flag_5,
                                           elc.display_order,
                                           sysdate,
                                           sysdate,
                                           get_user,
                                           'P',
                                           rec.partner_type,
                                           rec.partner,
                                           get_user
                                      from exp_prof_detail epd,
                                           exp_prof_head eph,
                                           item_exp_head ieh,
                                           elc_comp elc
                                     where eph.module            = 'PTNR'
                                       and eph.key_value_1       = rec.partner_type
                                       and eph.key_value_2       = rec.partner
                                       and eph.exp_prof_type     = L_exp_type
                                       and eph.base_prof_ind     = 'Y'
                                       and eph.exp_prof_key      = epd.exp_prof_key
                                       and epd.comp_id           = elc.comp_id
                                       and ieh.item              = I_item
                                       and ieh.supplier          = I_supplier
                                       and ieh.item_exp_type     = eph.exp_prof_type
                                       and ieh.discharge_port    = eph.discharge_port
                                       and ieh.origin_country_id = I_origin_country_id
                                       and ieh.lading_port       = eph.lading_port
                                       and not exists(select 'x'
                                                        from item_exp_detail ied
                                                       where ied.item           = I_item
                                                         and ied.supplier       = I_supplier
                                                         and ied.item_exp_type  = L_exp_type
                                                         and ied.item_exp_seq   = ieh.item_exp_seq
                                                         and ied.comp_id        = epd.comp_id);
            end if;
         end if;
         L_seq_no := L_seq_no + 1;
      end loop;
   end if;   --end PTNR
   ---
   -- Insert country level expense profiles that are attached to the origin country
   ---
   if I_origin_country_id is not NULL then
      L_module   := 'CTRY';
      ---
      SQL_LIB.SET_MARK('OPEN','C_SEQ_NO','ITEM_EXP_HEAD',NULL);
      open C_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH','C_SEQ_NO','ITEM_EXP_HEAD',NULL);
      fetch C_SEQ_NO into L_seq_no;
      SQL_LIB.SET_MARK('CLOSE','C_SEQ_NO','ITEM_EXP_HEAD',NULL);
      close C_SEQ_NO;
      ---
      if INSERT_EXPENSES(O_error_message,
                         L_records_inserted,
                         I_item,
                         I_supplier,
                         I_origin_country_id,
                         L_zone_group_id,
                         L_module,
                         L_exp_type,
                         L_seq_no,
                         NULL,
                         NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   -- Insert Always Default Expenses from the ELC_COMP table
   ---
   insert into item_exp_detail(item,
                               supplier,
                               item_exp_type,
                               item_exp_seq,
                               comp_id,
                               cvb_code,
                               comp_rate,
                               comp_currency,
                               per_count,
                               per_count_uom,
                               est_exp_value,
                               nom_flag_1,
                               nom_flag_2,
                               nom_flag_3,
                               nom_flag_4,
                               nom_flag_5,
                               display_order,
                               create_datetime,
                               last_update_datetime,
                               last_update_id,
                               defaulted_from,
                               key_value_1,
                               key_value_2,
                               create_id)
                        select I_item,
                               I_supplier,
                               L_exp_type,
                               ieh.item_exp_seq,
                               elc.comp_id,
                               elc.cvb_code,
                               elc.comp_rate,
                               elc.comp_currency,
                               elc.per_count,
                               elc.per_count_uom,
                               0,
                               elc.nom_flag_1,
                               elc.nom_flag_2,
                               elc.nom_flag_3,
                               elc.nom_flag_4,
                               elc.nom_flag_5,
                               elc.display_order,
                               sysdate,
                               sysdate,
                               get_user,
                               'E',
                               NULL,
                               NULL,
                               get_user
                          from item_exp_head ieh,
                               elc_comp elc
                         where ieh.item               = I_item
                           and ieh.supplier           = I_supplier
                           and ieh.item_exp_type      = L_exp_type
                           and elc.always_default_ind = 'Y'
                           and elc.comp_type          = 'E'
                           and elc.expense_type       = ieh.item_exp_type
                           and not exists(select 'x'
                                            from item_exp_detail ied
                                           where ied.item           = I_item
                                             and ied.supplier       = I_supplier
                                             and ied.item_exp_type  = L_exp_type
                                             and ied.item_exp_seq   = ieh.item_exp_seq
                                             and ied.comp_id        = elc.comp_id);
   ---
   -- Calculate the Expenses that were inserted.
   ---
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'IE',
                             I_item,
                             I_supplier,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             I_origin_country_id,
                             NULL,
                             NULL) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;

END DEFAULT_EXPENSES;
------------------------------------------------------------------------------------------------
FUNCTION INSERT_EXPENSES(O_error_message     IN OUT VARCHAR2,
                         O_records_inserted  IN OUT VARCHAR2,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_supplier          IN     SUPS.SUPPLIER%TYPE,
                         I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                         I_zone_group_id     IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                         I_module            IN     EXP_PROF_HEAD.MODULE%TYPE,
                         I_exp_type          IN     EXP_PROF_HEAD.EXP_PROF_TYPE%TYPE,
                         I_seq_no            IN     ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                         I_partner_type      IN     ITEM_EXP_DETAIL.KEY_VALUE_1%TYPE,
                         I_partner           IN     ITEM_EXP_DETAIL.KEY_VALUE_2%TYPE)
   RETURN BOOLEAN IS
   L_program     VARCHAR2(40)  := 'ITEM_EXPENSE_SQL.INSERT_EXPENSES';
   L_exists      VARCHAR2(1);
   L_exp_exists  BOOLEAN;
   ---
   L_defaulted_from   ITEM_EXP_DETAIL.DEFAULTED_FROM%TYPE;
   L_key_value_1      ITEM_EXP_DETAIL.KEY_VALUE_1%TYPE;
   L_key_value_2      ITEM_EXP_DETAIL.KEY_VALUE_2%TYPE := NULL;
   ---
   cursor C_HEADER_EXISTS is
      select 'Y'
        from item_exp_head
       where item          = I_item
         and supplier      = I_supplier
         and item_exp_type = I_exp_type;

   cursor C_PTNR is
   select partner_type_1 partner_type,
          partner_1 partner
     from sup_import_attr
    where supplier = I_supplier
      and partner_type_1 is not null
      and partner_1 is not null
    UNION
   select partner_type_2 partner_type,
          partner_2 partner
     from sup_import_attr
    where supplier = I_supplier
      and partner_type_2 is not null
      and partner_2 is not null
    UNION
   select partner_type_3 partner_type,
          partner_3 partner
     from sup_import_attr
    where supplier = I_supplier
           and partner_type_3 is not null
      and partner_3 is not null
    UNION
   select 'AG' partner_type,
          agent partner
     from sup_import_attr
    where supplier = I_supplier
      and agent is not null
    UNION
   select 'BK' partner_type,
          advising_bank partner
     from sup_import_attr
    where supplier = I_supplier
      and advising_bank is not null
    UNION
   select 'BK' partner_type,
          issuing_bank partner
     from sup_import_attr
    where supplier = I_supplier
      and issuing_bank is not null
    UNION
   select 'FA' partner_type,
          factory partner
     from sup_import_attr
    where supplier = I_supplier
      and factory is not null;

BEGIN
   O_records_inserted := 'Y';
   ---
   if I_module = 'SUPP' then
      L_defaulted_from := 'S';
      L_key_value_1    := I_supplier;
   elsif I_module = 'CTRY' then
      L_defaulted_from := 'C';
      L_key_value_1    := I_origin_country_id;
   elsif I_module = 'PTNR' then
      L_defaulted_from := 'P';
      L_key_value_1    := I_partner_type;
      L_key_value_2    := I_partner;
   end if;
   ---
   if EXP_PROF_SQL.PROF_HEAD_EXIST(O_error_message,
                                   L_exp_exists,
                                   I_exp_type,
                                   I_module,
                                   L_key_value_1,
                                   L_key_value_2,
                                   I_zone_group_id,
                                   NULL,                --I_zone_id
                                   I_origin_country_id,
                                   NULL,                --I_lading_port
                                   NULL) = FALSE then   --I_discharge_port
      return FALSE;
   end if;
   --
   if L_exp_exists = FALSE then
      O_records_inserted := 'N';
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'ITEM_EXP_HEAD', NULL);
   insert into item_exp_head(item,
                             supplier,
                             item_exp_type,
                             item_exp_seq,
                             origin_country_id,
                             zone_id,
                             lading_port,
                             discharge_port,
                             zone_group_id,
                             base_exp_ind,
                             create_datetime,
                             last_update_datetime,
                             last_update_id,
                             create_id)
                      select I_item,
                             I_supplier,
                             I_exp_type,
                             I_seq_no + rownum,
                             I_origin_country_id,
                             eph.zone_id,
                             eph.lading_port,
                             eph.discharge_port,
                             I_zone_group_id,
                             'N',
                             sysdate,
                             sysdate,
                             get_user,
                             get_user
                        from exp_prof_head eph
                       where eph.module        = I_module
                         and eph.key_value_1   = decode(I_module,'SUPP',to_char(I_supplier),'CTRY',I_origin_country_id,'PTNR',I_partner_type)
                         and (eph.key_value_2   = I_partner
                              or (eph.key_value_2 is null
                              and I_partner is null))
                         and eph.exp_prof_type = I_exp_type
                         and ((I_exp_type                = 'Z'
                               and eph.zone_group_id     = I_zone_group_id)
                          or (I_exp_type                 = 'C'
                               and eph.origin_country_id = I_origin_country_id))
                         and not exists(select 'x'
                                          from item_exp_head ieh
                                         where ieh.item           = I_item
                                           and ieh.supplier       = I_supplier
                                           and ieh.item_exp_type  = I_exp_type
                                           and ((I_exp_type               = 'Z'
                                                 and ieh.zone_id          = eph.zone_id
                                                 and ieh.zone_group_id    = I_zone_group_id)
                                            or (I_exp_type                = 'C'
                                                and ieh.origin_country_id = I_origin_country_id
                                                and ieh.lading_port       = eph.lading_port))
                                           and ieh.discharge_port  = eph.discharge_port)
                         and exists( select 'x'
                                       from item_supp_country isc
                                      where isc.item              = I_item
                                        and isc.supplier          = I_supplier
                                        and isc.origin_country_id = NVL(I_origin_country_id, isc.origin_country_id));
   if SQL%FOUND then
      insert into item_exp_detail(item,
                                  supplier,
                                  item_exp_type,
                                  item_exp_seq,
                                  comp_id,
                                  cvb_code,
                                  comp_rate,
                                  comp_currency,
                                  per_count,
                                  per_count_uom,
                                  est_exp_value,
                                  nom_flag_1,
                                  nom_flag_2,
                                  nom_flag_3,
                                  nom_flag_4,
                                  nom_flag_5,
                                  display_order,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id,
                                  defaulted_from,
                                  key_value_1,
                                  key_value_2,
                                  create_id)
                           select I_item,
                                  I_supplier,
                                  I_exp_type,
                                  ieh.item_exp_seq,
                                  epd.comp_id,
                                  epd.cvb_code,
                                  epd.comp_rate,
                                  epd.comp_currency,
                                  epd.per_count,
                                  epd.per_count_uom,
                                  0,
                                  epd.nom_flag_1,
                                  epd.nom_flag_2,
                                  epd.nom_flag_3,
                                  epd.nom_flag_4,
                                  epd.nom_flag_5,
                                  elc.display_order,
                                  sysdate,
                                  sysdate,
                                  get_user,
                                  L_defaulted_from,
                                  L_key_value_1,
                                  L_key_value_2,
                                  get_user
                             from exp_prof_detail epd,
                                  exp_prof_head eph,
                                  item_exp_head ieh,
                                  elc_comp elc
                            where eph.module         = I_module
                              and eph.key_value_1    = decode(I_module,'SUPP',to_char(I_supplier),'CTRY',I_origin_country_id,'PTNR',I_partner_type)
                              and (eph.key_value_2   = I_partner
                                   or (eph.key_value_2 is null
                                   and I_partner is null))
                              and eph.exp_prof_type  = I_exp_type
                              and eph.discharge_port = ieh.discharge_port
                              and eph.exp_prof_key   = epd.exp_prof_key
                              and epd.comp_id        = elc.comp_id
                              and ieh.item           = I_item
                              and ieh.supplier       = I_supplier
                              and ieh.item_exp_type  = eph.exp_prof_type
                              and ((I_exp_type            = 'Z'
                                    and eph.zone_group_id = I_zone_group_id
                                    and eph.zone_group_id = ieh.zone_group_id
                                    and eph.zone_id       = ieh.zone_id)
                               or (I_exp_type                = 'C'
                                   and eph.origin_country_id = I_origin_country_id
                                   and eph.origin_country_id = ieh.origin_country_id
                                   and eph.lading_port       = ieh.lading_port))
                              and not exists(select 'x'
                                               from item_exp_detail ied
                                              where ied.item           = I_item
                                                and ied.supplier       = I_supplier
                                                and ied.item_exp_type  = I_exp_type
                                                and ied.item_exp_seq   = ieh.item_exp_seq
                                                and ied.comp_id        = epd.comp_id);
      ---
      if I_module = 'SUPP' then
         for C_rec_sia in C_PTNR loop
            insert into item_exp_detail(item,
                                        supplier,
                                        item_exp_type,
                                        item_exp_seq,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        comp_currency,
                                        per_count,
                                        per_count_uom,
                                        est_exp_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id,
                                        defaulted_from,
                                        key_value_1,
                                        key_value_2,
                                        create_id)
                                 select I_item,
                                        I_supplier,
                                        I_exp_type,
                                        ieh.item_exp_seq,
                                        epd.comp_id,
                                        epd.cvb_code,
                                        epd.comp_rate,
                                        epd.comp_currency,
                                        epd.per_count,
                                        epd.per_count_uom,
                                        0,
                                        epd.nom_flag_1,
                                        epd.nom_flag_2,
                                        epd.nom_flag_3,
                                        epd.nom_flag_4,
                                        epd.nom_flag_5,
                                        elc.display_order,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        'P',
                                        C_rec_sia.partner_type,
                                        C_rec_sia.partner,
                                        get_user
                                   from exp_prof_detail epd,
                                        exp_prof_head eph,
                                        item_exp_head ieh,
                                        elc_comp elc
                                  where eph.module         = 'PTNR'
                                    and eph.key_value_1    = C_rec_sia.partner_type
                                    and eph.key_value_2    = C_rec_sia.partner
                                    and eph.exp_prof_type  = I_exp_type
                                    and eph.discharge_port = ieh.discharge_port
                                    and eph.exp_prof_key   = epd.exp_prof_key
                                    and epd.comp_id        = elc.comp_id
                                    and ieh.item           = I_item
                                    and ieh.supplier       = I_supplier
                                    and ieh.item_exp_type  = eph.exp_prof_type
                                    and ((I_exp_type            = 'Z'
                                          and eph.zone_group_id = I_zone_group_id
                                          and eph.zone_group_id = ieh.zone_group_id
                                          and eph.zone_id       = ieh.zone_id)
                                     or (I_exp_type                = 'C'
                                         and eph.origin_country_id = I_origin_country_id
                                         and eph.origin_country_id = ieh.origin_country_id
                                         and eph.lading_port       = ieh.lading_port))
                                    and not exists(select 'x'
                                                     from item_exp_detail ied
                                                    where ied.item           = I_item
                                                      and ied.supplier       = I_supplier
                                                      and ied.item_exp_type  = I_exp_type
                                                      and ied.item_exp_seq   = ieh.item_exp_seq
                                                      and ied.comp_id        = epd.comp_id);
         end loop;
      end if;
      ---

   else
      SQL_LIB.SET_MARK('OPEN', NULL, 'C_HEADER_EXISTS', NULL);
      open C_HEADER_EXISTS;
      SQL_LIB.SET_MARK('FETCH', NULL, 'C_HEADER_EXISTS', NULL);
      fetch C_HEADER_EXISTS into L_exists;
      SQL_LIB.SET_MARK('CLOSE', NULL, 'C_HEADER_EXISTS', NULL);
      close C_HEADER_EXISTS;
      ---
      if L_exists = 'Y' then
          SQL_LIB.SET_MARK('INSERT', NULL, 'ITEM_EXP_DETAIL', NULL);
          insert into item_exp_detail(item,
                                      supplier,
                                      item_exp_type,
                                      item_exp_seq,
                                      comp_id,
                                      cvb_code,
                                      comp_rate,
                                      comp_currency,
                                      per_count,
                                      per_count_uom,
                                      est_exp_value,
                                      nom_flag_1,
                                      nom_flag_2,
                                      nom_flag_3,
                                      nom_flag_4,
                                      nom_flag_5,
                                      display_order,
                                      create_datetime,
                                      last_update_datetime,
                                      last_update_id,
                                      defaulted_from,
                                      key_value_1,
                                      key_value_2,
                                      create_id)
                               select I_item,
                                      I_supplier,
                                      I_exp_type,
                                      ieh.item_exp_seq,
                                      epd.comp_id,
                                      epd.cvb_code,
                                      epd.comp_rate,
                                      epd.comp_currency,
                                      epd.per_count,
                                      epd.per_count_uom,
                                      0,
                                      epd.nom_flag_1,
                                      epd.nom_flag_2,
                                      epd.nom_flag_3,
                                      epd.nom_flag_4,
                                      epd.nom_flag_5,
                                      elc.display_order,
                                      sysdate,
                                      sysdate,
                                      get_user,
                                      L_defaulted_from,
                                      L_key_value_1,
                                      L_key_value_2,
                                      get_user
                                 from exp_prof_detail epd,
                                      exp_prof_head eph,
                                      item_exp_head ieh,
                                      elc_comp elc
                                where eph.module         = I_module
                                  and eph.key_value_1    = decode(I_module,'SUPP',to_char(I_supplier),'CTRY',I_origin_country_id,'PTNR',I_partner_type)
                                  and (eph.key_value_2   = I_partner
                                       or (eph.key_value_2 is null
                                       and I_partner is null))
                                  and eph.exp_prof_type  = I_exp_type
                                  and eph.discharge_port = ieh.discharge_port
                                  and eph.exp_prof_key   = epd.exp_prof_key
                                  and epd.comp_id        = elc.comp_id
                                  and ieh.item           = I_item
                                  and ieh.supplier       = I_supplier
                                  and ieh.item_exp_type  = eph.exp_prof_type
                                  and ((I_exp_type            = 'Z'
                                        and eph.zone_group_id = I_zone_group_id
                                        and eph.zone_group_id = ieh.zone_group_id
                                        and eph.zone_id       = ieh.zone_id)
                                   or (I_exp_type                = 'C'
                                       and eph.origin_country_id = I_origin_country_id
                                       and eph.origin_country_id = ieh.origin_country_id
                                       and eph.lading_port       = ieh.lading_port))
                                  and not exists(select 'x'
                                                   from item_exp_detail ied
                                                  where ied.item           = I_item
                                                    and ied.supplier       = I_supplier
                                                    and ied.item_exp_type  = I_exp_type
                                                    and ied.item_exp_seq   = ieh.item_exp_seq
                                                    and ied.comp_id        = epd.comp_id);

          if I_module = 'SUPP' then
             for C_REC_SIA in C_PTNR loop
                insert into item_exp_detail(item,
                                            supplier,
                                            item_exp_type,
                                            item_exp_seq,
                                            comp_id,
                                            cvb_code,
                                            comp_rate,
                                            comp_currency,
                                            per_count,
                                            per_count_uom,
                                            est_exp_value,
                                            nom_flag_1,
                                            nom_flag_2,
                                            nom_flag_3,
                                            nom_flag_4,
                                            nom_flag_5,
                                            display_order,
                                            create_datetime,
                                            last_update_datetime,
                                            last_update_id,
                                            defaulted_from,
                                            key_value_1,
                                            key_value_2,
                                            create_id)
                                     select I_item,
                                            I_supplier,
                                            I_exp_type,
                                            ieh.item_exp_seq,
                                            epd.comp_id,
                                            epd.cvb_code,
                                            epd.comp_rate,
                                            epd.comp_currency,
                                            epd.per_count,
                                            epd.per_count_uom,
                                            0,
                                            epd.nom_flag_1,
                                            epd.nom_flag_2,
                                            epd.nom_flag_3,
                                            epd.nom_flag_4,
                                            epd.nom_flag_5,
                                            elc.display_order,
                                            sysdate,
                                            sysdate,
                                            get_user,
                                            'P',
                                            C_rec_sia.partner_type,
                                            C_rec_sia.partner,
                                            get_user
                                       from exp_prof_detail epd,
                                            exp_prof_head eph,
                                            item_exp_head ieh,
                                            elc_comp elc
                                      where eph.module         = 'PTNR'
                                        and eph.key_value_1    = C_rec_sia.partner_type
                                        and eph.key_value_2    = C_rec_sia.partner
                                        and eph.exp_prof_type  = I_exp_type
                                        and eph.discharge_port = ieh.discharge_port
                                        and eph.exp_prof_key   = epd.exp_prof_key
                                        and epd.comp_id        = elc.comp_id
                                        and ieh.item           = I_item
                                        and ieh.supplier       = I_supplier
                                        and ieh.item_exp_type  = eph.exp_prof_type
                                        and ((I_exp_type            = 'Z'
                                        and eph.zone_group_id = I_zone_group_id
                                        and eph.zone_group_id = ieh.zone_group_id
                                        and eph.zone_id       = ieh.zone_id)
                                         or (I_exp_type                = 'C'
                                        and eph.origin_country_id = I_origin_country_id
                                        and eph.origin_country_id = ieh.origin_country_id
                                        and eph.lading_port       = ieh.lading_port))
                                        and not exists(select 'x'
                                                         from item_exp_detail ied
                                                        where ied.item           = I_item
                                                          and ied.supplier       = I_supplier
                                                          and ied.item_exp_type  = I_exp_type
                                                          and ied.item_exp_seq   = ieh.item_exp_seq
                                                          and ied.comp_id        = epd.comp_id);
             end loop;
           --
          end if; -- if I module = 'SUPP'
          ---
          if SQL%NOTFOUND then
             O_records_inserted := 'N';
          end if;
      else
         O_records_inserted := 'N';
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;

END INSERT_EXPENSES;
------------------------------------------------------------------------------------------------
FUNCTION INSERT_ALWAYS_EXPENSES(O_error_message     IN OUT VARCHAR2,
                                I_item              IN     ITEM_MASTER.ITEM%TYPE,
                                I_supplier          IN     SUPS.SUPPLIER%TYPE,
                                I_item_exp_type     IN     ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                                I_item_exp_seq      IN     ITEM_EXP_DETAIL.ITEM_EXP_SEQ%TYPE,
                                I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   L_program     VARCHAR2(40)  := 'ITEM_EXPENSE_SQL.INSERT_ALWAYS_EXPENSES';

BEGIN
   insert into item_exp_detail(item,
                               supplier,
                               item_exp_type,
                               item_exp_seq,
                               comp_id,
                               cvb_code,
                               comp_rate,
                               comp_currency,
                               per_count,
                               per_count_uom,
                               est_exp_value,
                               nom_flag_1,
                               nom_flag_2,
                               nom_flag_3,
                               nom_flag_4,
                               nom_flag_5,
                               display_order,
                               create_datetime,
                               last_update_datetime,
                               last_update_id,
                               defaulted_from,
                               key_value_1,
                               key_value_2,
                               create_id)
                        select I_item,
                               I_supplier,
                               I_item_exp_type,
                               ieh.item_exp_seq,
                               elc.comp_id,
                               elc.cvb_code,
                               elc.comp_rate,
                               elc.comp_currency,
                               elc.per_count,
                               elc.per_count_uom,
                               0,
                               elc.nom_flag_1,
                               elc.nom_flag_2,
                               elc.nom_flag_3,
                               elc.nom_flag_4,
                               elc.nom_flag_5,
                               elc.display_order,
                               sysdate,
                               sysdate,
                               get_user,
                               'E',
                               NULL,
                               NULL,
                               get_user
                          from item_exp_head ieh,
                               elc_comp elc
                         where ieh.item               = I_item
                           and ieh.supplier           = I_supplier
                           and ieh.item_exp_type      = I_item_exp_type
                           and ieh.item_exp_seq       = I_item_exp_seq
                           and elc.always_default_ind = 'Y'
                           and elc.comp_type          = 'E'
                           and elc.expense_type       = ieh.item_exp_type
                           and not exists(select 'x'
                                            from item_exp_detail ied
                                           where ied.item           = I_item
                                             and ied.supplier       = I_supplier
                                             and ied.item_exp_type  = I_item_exp_type
                                             and ied.item_exp_seq   = ieh.item_exp_seq
                                             and ied.comp_id        = elc.comp_id);
   ---
   -- Calculate the Expenses that were inserted.
   ---
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'IE',
                             I_item,
                             I_supplier,
                             I_item_exp_type,
                             I_item_exp_seq,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             I_origin_country_id,
                             NULL,
                             NULL) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;

END INSERT_ALWAYS_EXPENSES;
------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_GROUP_EXP(O_error_message        IN OUT VARCHAR2,
                           I_item                 IN     ITEM_MASTER.ITEM%TYPE,
                           I_supplier             IN     SUPS.SUPPLIER%TYPE,
                           I_origin_country_id    IN     COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(40)   := 'ITEM_EXPENSE_SQL.DEFAULT_GROUP_EXP';

   cursor C_GET_KIDS is
      select item
        from item_master
       where (item_parent = I_item
          or item_grandparent = I_item)
         and item_level <= tran_level;

BEGIN

   FOR C_rec in C_GET_KIDS LOOP
      if DEFAULT_EXPENSES(O_error_message,
                          C_rec.item,
                          I_supplier,
                          I_origin_country_id) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END DEFAULT_GROUP_EXP;

-----------------------------------------------------------------------------------------------
FUNCTION COPY_DOWN_PARENT_EXP(O_error_message        IN OUT VARCHAR2,
                              I_parent               IN     ITEM_MASTER.ITEM%TYPE,
                              I_supplier             IN     SUPS.SUPPLIER%TYPE,
                              I_origin_country_id    IN     COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(40)   := 'ITEM_EXPENSE_SQL.COPY_DOWN_PARENT_EXP';
   L_exp_type           ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE;
   L_seq_no             ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE;

   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   L_elc_tbl            OBJ_ELC_COST_EVENT_TBL   :=   OBJ_ELC_COST_EVENT_TBL();
   L_cost_event_id      COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_system_options     SYSTEM_OPTIONS%ROWTYPE;

   cursor C_GET_ELC_CHANGE is
      select OBJ_ELC_COST_EVENT_REC(c.item,
                                    c.supplier,
                                    c.origin_country_id,
                                    c.zone_group_id,
                                    c.zone_id)
                               from (select distinct ieh.item,
                                                     ieh.supplier,
                                                     CASE 
                                                     WHEN ieh.item_exp_type = 'C' then ieh.origin_country_id
                                                     ELSE NULL
                                                     END origin_country_id,
                                                     ieh.zone_group_id,
                                                     ieh.zone_id
                                                from item_exp_head ieh,
                                                     item_master im
                                               where ieh.item  = im.item
                                                 and (im.item_parent = I_parent or
                                                      im.item_grandparent = I_parent)
                                                 and ieh.supplier = I_supplier) c;
---

BEGIN
   if I_origin_country_id is NULL then
      L_exp_type := 'Z';
   else
      L_exp_type := 'C';
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            L_system_options) = FALSE then
      return FALSE;
   end if;
   ---
   delete from item_exp_detail ied
      where exists (select 'x'
                      from item_master im, item_exp_head ieh
                     where im.item = ied.item
                       and (im.item_parent = I_parent
                        or im.item_grandparent = I_parent)
                       and (I_origin_country_id is NULL or
                            ieh.origin_country_id = I_origin_country_id)
                       and ieh.item_exp_type = L_exp_type
                       and ieh.supplier = I_supplier
                       and ieh.item = ied.item
                       and ied.item_exp_seq = ieh.item_exp_seq
                       and ied.supplier = ieh.supplier
                       and ied.item_exp_type = ieh.item_exp_type);

   delete from item_exp_head ieh
      where exists (select 'x'
                      from item_master im
                     where (im.item_parent = I_parent
                        or im.item_grandparent = I_parent)
                       and ieh.item = im.item)
           and (I_origin_country_id is NULL or
                ieh.origin_country_id = I_origin_country_id)
           and ieh.item_exp_type = L_exp_type
           and ieh.supplier = I_supplier;

   insert into item_exp_head(item,
                             supplier,
                             item_exp_type,
                             item_exp_seq,
                             origin_country_id,
                             zone_id,
                             lading_port,
                             discharge_port,
                             zone_group_id,
                             base_exp_ind,
                             create_datetime,
                             last_update_datetime,
                             last_update_id,
                             create_id)
                      select im.item,
                             ieh.supplier,
                             ieh.item_exp_type,
                             ieh.item_exp_seq,
                             ieh.origin_country_id,
                             ieh.zone_id,
                             ieh.lading_port,
                             ieh.discharge_port,
                             ieh.zone_group_id,
                             ieh.base_exp_ind,
                             sysdate,
                             sysdate,
                             get_user,
                             get_user
                        from item_exp_head ieh,
                             item_master im
                       where ieh.item               = I_parent
                         and (im.item_parent        = ieh.item or im.item_grandparent = ieh.item)
                         and (ieh.origin_country_id = I_origin_country_id or I_origin_country_id is NULL)
                         and ieh.supplier           = I_supplier
                         and ieh.item_exp_type      = L_exp_type
                         and exists( select 'x'
                                       from item_supp_country isc
                                      where isc.item              = im.item
                                        and isc.supplier          = ieh.supplier
                                        and isc.origin_country_id = NVL(ieh.origin_country_id, isc.origin_country_id));
      ---
      insert into item_exp_detail(item,
                                  supplier,
                                  item_exp_type,
                                  item_exp_seq,
                                  comp_id,
                                  cvb_code,
                                  comp_rate,
                                  comp_currency,
                                  per_count,
                                  per_count_uom,
                                  est_exp_value,
                                  nom_flag_1,
                                  nom_flag_2,
                                  nom_flag_3,
                                  nom_flag_4,
                                  nom_flag_5,
                                  display_order,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id,
                                  defaulted_from,
                                  key_value_1,
                                  key_value_2,
                                  create_id)
                           select im.item,
                                  ied.supplier,
                                  ied.item_exp_type,
                                  ied.item_exp_seq,
                                  ied.comp_id,
                                  ied.cvb_code,
                                  ied.comp_rate,
                                  ied.comp_currency,
                                  ied.per_count,
                                  ied.per_count_uom,
                                  ied.est_exp_value,
                                  ied.nom_flag_1,
                                  ied.nom_flag_2,
                                  ied.nom_flag_3,
                                  ied.nom_flag_4,
                                  ied.nom_flag_5,
                                  ied.display_order,
                                  sysdate,
                                  sysdate,
                                  get_user,
                                  ied.defaulted_from,
                                  ied.key_value_1,
                                  ied.key_value_2,
                                  get_user
                             from item_master im,
                                  item_exp_detail ied,
                                  item_exp_head ieh
                            where ied.item               = I_parent
                              and (im.item_parent        = ied.item or im.item_grandparent = ied.item)
                              and ied.item               = ieh.item
                              and ied.item_exp_type      = L_exp_type
                              and ied.item_exp_seq       = ieh.item_exp_seq
                              and ied.item_exp_type      = ieh.item_exp_type
                              and (ieh.origin_country_id = I_origin_country_id or I_origin_country_id is NULL)
                              and ied.supplier = I_supplier
                              and ied.supplier = ieh.supplier
                              and exists( select 'x'
                                            from item_supp_country isc
                                           where isc.item              = im.item
                                             and isc.supplier          = ieh.supplier
                                             and isc.origin_country_id = NVL(ieh.origin_country_id, isc.origin_country_id));
   ---
   if L_system_options.elc_ind = 'Y' and
      (L_system_options.elc_inclusive_ind_comp_store = 'Y' or L_system_options.elc_inclusive_ind_wf_store = 'Y') then
      ---
      open C_GET_ELC_CHANGE;
      fetch C_GET_ELC_CHANGE bulk collect into L_elc_tbl;
      close C_GET_ELC_CHANGE;
      ---
      if L_elc_tbl.COUNT > 0 then
         if FUTURE_COST_EVENT_SQL.ADD_ELC_EVENTS(L_error_message,
                                                 L_cost_event_id,
                                                 L_elc_tbl,
                                                 USER) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
return TRUE;
EXCEPTION
   when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                to_char(SQLCODE));
      return FALSE;
END COPY_DOWN_PARENT_EXP;
-----------------------------------------------------------------------------------------------
FUNCTION GET_EXP_ORG_CTRY(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exp_ctry_exists IN OUT  BOOLEAN,
                          I_exp_prof_type   IN      EXP_PROF_HEAD.EXP_PROF_TYPE%TYPE,
                          I_supplier        IN      SUPS.SUPPLIER%TYPE,
                          I_org_ctry_id     IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN is

   L_exp_ctry   ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_program    VARCHAR2(40)   := 'ITEM_EXPENSE_SQL.GET_EXP_ORG_CTRY';

      cursor C_EXP_CTRY_ID is
      select origin_country_id
        from exp_prof_head
       where exp_prof_type     = I_exp_prof_type
         and key_value_1       = to_char(I_supplier)
         and origin_country_id = I_org_ctry_id;

BEGIN

   O_exp_ctry_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN', 'C_EXP_CTRY_ID', 'EXP_PROF_HEAD', NULL);
   open C_EXP_CTRY_ID;

   SQL_LIB.SET_MARK('FETCH', NULL, 'C_EXP_CTRY_ID', NULL);
   fetch C_EXP_CTRY_ID into L_exp_ctry;

   If C_EXP_CTRY_ID%FOUND then
      O_exp_ctry_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_EXP_CTRY_ID', 'EXP_PROF_HEAD', NULL);
   close C_EXP_CTRY_ID;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END GET_EXP_ORG_CTRY;
-----------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_EXP (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item             IN       ITEM_MASTER.ITEM%TYPE,
                       I_supplier         IN       SUPS.SUPPLIER%TYPE,
                       I_origin_country   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       O_count            IN OUT   NUMBER)
   RETURN BOOLEAN IS

   cursor C_GET_ITEM_EXP is
      select 'x'
        from item_exp_head
       where item = I_item
         and supplier = I_supplier
         and ((item_exp_type = 'C'
               and origin_country_id = I_origin_country
               and base_exp_ind = 'Y')
              or item_exp_type = 'Z')
         and rownum = 1;
      
   L_exists    VARCHAR2(1);
   L_program   VARCHAR2(40) := 'ITEM_EXPENSE_SQL.GET_ITEM_EXP';

BEGIN

   open C_GET_ITEM_EXP;

   fetch C_GET_ITEM_EXP into L_exists;

   if C_GET_ITEM_EXP%NOTFOUND then
      O_count := 0;
   else
      O_count := 1;
   end if;

   close C_GET_ITEM_EXP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ITEM_EXP;
-----------------------------------------------------------------------------------------------
END;
/