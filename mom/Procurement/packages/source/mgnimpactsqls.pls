CREATE OR REPLACE PACKAGE MGNIMPACT_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Purpose: The Package contains functions used by the margin impact form mgnimpact.fmb
---------------------------------------------------------------------------------------------

-- Defining package variables.
TYPE margin_impact_rec is RECORD 
   (loc_type            CODE_DETAIL.CODE_DESC%TYPE,
    location            FUTURE_COST.LOCATION%TYPE,
    location_desc       V_LOCATION.LOCATION_NAME%type,
    effective_date      FUTURE_COST.ACTIVE_DATE%TYPE,
    currency_code       FUTURE_COST.CURRENCY_CODE%TYPE,
    pricing_cost        FUTURE_COST.PRICING_COST%TYPE,
    unit_retail         RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
    margin_percent      NUMBER,
    error_message       RTK_ERRORS.RTK_TEXT%TYPE,
    return_code         VARCHAR2(5));

                         
TYPE margin_impact_tbl is TABLE of margin_impact_rec INDEX BY BINARY_INTEGER;

---------------------------------------------------------------------------------------------
-- Procedure Name: GET_MRGN_IMPACT_INFO
-- Purpose: The function is called by the form mgnimpact.fmb. The functions retrieves the 
--          margin impact information and returns the records into the table margin_impact_tbl.
---------------------------------------------------------------------------------------------
PROCEDURE GET_MRGN_IMPACT_INFO(IO_margin_impact_tbl    IN OUT  MARGIN_IMPACT_TBL,
                               I_item                  IN      ITEM_MASTER.ITEM%TYPE,
                               I_supplier              IN      SUPS.SUPPLIER%TYPE,
                               I_origin_country        IN      COUNTRY.COUNTRY_ID%TYPE,
                               I_cost_change_level     IN      VARCHAR2,
                               I_cost_change           IN      COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                               I_status                IN      COST_SUSP_SUP_HEAD.STATUS%TYPE,
                               I_active_date           IN      COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                               I_cost_event_process_id IN      COST_EVENT.COST_EVENT_PROCESS_ID%TYPE);

---------------------------------------------------------------------------------------------
-- Procedure Name: DELETE_FC_WORKSPACE
-- Purpose: The function is called by the form mgnimpact.fmb. The function deletes the records
--          from FUTURE_COST_EVENT_WORKSPACE table for a cost change passed to the function.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_FC_WORKSPACE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_change      IN     COST_EVENT_COST_CHG.COST_CHANGE%TYPE)
   RETURN BOOLEAN;

END MGNIMPACT_SQL;
/
