
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ORDER_TYPE_SQL AS
-------------------------------------------------------------------------------
FUNCTION GET_ORDER_TYPE_DESC(O_error_message    IN OUT VARCHAR2,
                             O_order_type_desc  IN OUT ORDER_TYPES_TL.ORDER_TYPE_DESC%TYPE,
                             I_order_type       IN     ORDER_TYPES.ORDER_TYPE%TYPE)
                             RETURN BOOLEAN IS

   cursor C_GET_DESC is
   select order_type_desc
     from v_order_types_tl
    where order_type = I_order_type;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_DESC','v_order_types_tl', 'order_type: '||I_order_type);
   open C_GET_DESC;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_DESC','v_order_types_tl', 'order_type: '||I_order_type);
   fetch C_GET_DESC into O_order_type_desc;
   ---
   if C_GET_DESC%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('ORDER_TYPE_NOT_FOUND', I_order_type, NULL, NULL);
      SQL_LIB.SET_MARK('CLOSE','C_GET_DESC','v_order_types_tl', 'order_type: '||I_order_type);
      close C_GET_DESC;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_DESC','v_order_types_tl', 'order_type: '||I_order_type);
   close C_GET_DESC;
   ---
   return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_TYPE_SQL.GET_ORDER_TYPE_DESC',
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORDER_TYPE_DESC;
-------------------------------------------------------------------------------
END ORDER_TYPE_SQL;
/
