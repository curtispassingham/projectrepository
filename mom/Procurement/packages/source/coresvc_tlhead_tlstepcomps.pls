CREATE OR REPLACE PACKAGE CORESVC_TLHEAD_TLSTEPCOMP AUTHID CURRENT_USER AS
   template_key             CONSTANT VARCHAR2(25)  := 'TLHEAD_TLSTEPCOMP_DATA';
   action_new                        VARCHAR2(25)  := 'NEW';
   action_mod                        VARCHAR2(25)  := 'MOD';
   action_del                        VARCHAR2(25)  := 'DEL';
   TIMELINE_STEPS_sheet              VARCHAR2(25)  := 'TIMELINE_STEPS';
   TIMELINE_STEPS$Action             NUMBER        := 1;
   TIMELINE_STEPS$TIMELINE_NO        NUMBER        := 2;
   TIMELINE_STEPS$STEP_NO            NUMBER        := 3;
   TIMELINE_STEPS$TIMELINE_TYPE      NUMBER        := 4;
   TIMELINE_STEPS$DAYS_COMPLETED     NUMBER        := 5;
   TIMELINE_STEPS$DISPLAY_SEQ        NUMBER        := 6;
   sheet_name_trans                  S9T_PKG.trans_map_typ;
   action_column                     VARCHAR2(25)  := 'ACTION';
   template_category                 CODE_DETAIL.CODE%TYPE := 'RMSIMP';
   TYPE TIMELINE_STEPS_rec_tab is TABLE OF TIMELINE_STEPS%ROWTYPE;
   TIMELINE_HEAD_sheet               VARCHAR2(25)  := 'TIMELINE_HEAD';
   TIMELINE_HEAD$Action              NUMBER        := 1;
   TIMELINE_HEAD$TIMELINE_NO         NUMBER        := 2;
   TIMELINE_HEAD$TIMELINE_DESC       NUMBER        := 3;
   TIMELINE_HEAD$TIMELINE_TYPE       NUMBER        := 4;
   TIMELINE_HEAD$TIMELINE_BASE       NUMBER        := 5;
   TYPE TIMELINE_HEAD_rec_tab is TABLE OF TIMELINE_HEAD%ROWTYPE;

   TIMELINE_HEAD_TL_sheet            VARCHAR2(25)  := 'TIMELINE_HEAD_TL';
   TIMELINE_HEAD_TL$Action           NUMBER        := 1;
   TIMELINE_HEAD_TL$LANG             NUMBER        := 2;
   TIMELINE_HEAD_TL$TIMELINE_NO      NUMBER        := 3;
   TIMELINE_HEAD_TL$TIMELINE_DESC    NUMBER        := 4;

   TIMELINE_STEP_COMP_sheet          VARCHAR2(25)  := 'TIMELINE_STEP_COMP';
   TIMELINE_STEP_COMP$Action         NUMBER        := 1;
   TIMELINE_STEP_COMP$STEP_DESC      NUMBER        := 4;
   TIMELINE_STEP_COMP$STEP_NO        NUMBER        := 3;
   TSTEP_COMP$TIMELINE_TYPE          NUMBER        := 2;

   TSTEP_COMP_TL_sheet               VARCHAR2(25)  := 'TIMELINE_STEP_COMP_TL';
   TSTEP_COMP_TL$Action              NUMBER        := 1;
   TSTEP_COMP_TL$LANG                NUMBER        := 2;
   TSTEP_COMP_TL$TIMELINE_TYPE       NUMBER        := 3;
   TSTEP_COMP_TL$STEP_NO             NUMBER        := 4;
   TSTEP_COMP_TL$STEP_DESC           NUMBER        := 5;

   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN     CHAR DEFAULT 'N')
   RETURN BOOLEAN;

   FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count   IN OUT NUMBER,
                        I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id    IN     NUMBER)
   RETURN BOOLEAN;

   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;

   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;

END CORESVC_TLHEAD_TLSTEPCOMP;
/