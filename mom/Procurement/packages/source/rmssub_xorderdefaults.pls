
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XORDER_DEFAULT AUTHID CURRENT_USER AS

DEFAULT_ORIG_IND     ORDHEAD.ORIG_IND%TYPE      := '2';
DEFAULT_EDI_PO_IND   ORDHEAD.EDI_PO_IND%TYPE    := 'N';
DEFAULT_PREMARK_IND  ORDHEAD.PRE_MARK_IND%TYPE  := 'N';

FUNCTION DEFAULT_ORIGIN_COUNTRY_ID(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_origin_country_id  IN OUT ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                   IO_exists             IN OUT BOOLEAN,
                                   I_item                IN     ITEM_SUPPLIER.ITEM%TYPE,
                                   I_supplier            IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN;

----------------------------------------------------------------------------
END RMSSUB_XORDER_DEFAULT;
/
