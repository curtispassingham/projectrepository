
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SUP_BRACKET_COST_SQL AS
------------------------------------------------------------------------------
FUNCTION BRACKETS_EXIST(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exist         IN OUT  BOOLEAN,
                        I_supplier      IN      SUP_BRACKET_COST.SUPPLIER%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(60) := 'SUP_BRACKET_COST_SQL.BRACKETS_EXIST';
   L_dummy       VARCHAR2(1)  := NULL;

   cursor C_EXIST is
      select 'x'
        from sup_bracket_cost
       where supplier = I_supplier;

BEGIN
   if I_supplier is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_supplier',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_EXIST','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
   open C_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_EXIST','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
   fetch C_EXIST into L_dummy;
   if C_EXIST%NOTFOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_EXIST','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
   close C_EXIST;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END BRACKETS_EXIST;
----------------------------------------------------------------------------------
FUNCTION UPDATE_COST_CHANGE_FOR_DEFAULT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_status        IN OUT COST_SUSP_SUP_HEAD.STATUS%TYPE,
                                        I_cost_change   IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                                        I_supplier      IN     COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                                        I_bracket_value IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(60) := 'SUP_BRACKET_COST_SQL.UPDATE_COST_CHANGE_FOR_DEFAULT';
   L_cssd_dummy    VARCHAR2(1)  := NULL;
   L_cssdl_dummy   VARCHAR2(1)  := NULL;

   cursor C_LOCK_CSSD is
      select 'x'
        from cost_susp_sup_detail csd
       where csd.supplier = I_supplier
         and exists (select 'x'
                       from cost_susp_sup_head csh
                      where csh.cost_change = csd.cost_change
                        and csh.reason in (1,2,3))
         for update nowait;

   cursor C_LOCK_CSSDL is
      select 'x'
        from cost_susp_sup_detail_loc csl
       where csl.supplier = I_supplier
         and exists (select 'x'
                       from cost_susp_sup_head csh
                      where csh.cost_change = csl.cost_change
                        and csh.reason in (1,2,3))
         for update nowait;

   cursor C_GET_STATUS is
      select status
        from cost_susp_sup_head
       where cost_change = I_cost_change;

BEGIN
   open C_GET_STATUS;
   fetch C_GET_STATUS into O_status;
   close C_GET_STATUS;

   if O_status = 'W' then
      open C_LOCK_CSSD;
      close C_LOCK_CSSD;
      ---
      update cost_susp_sup_detail csd
         set csd.default_bracket_ind = DECODE(csd.bracket_value1, I_bracket_value, 'Y', 'N')
       where csd.supplier = I_supplier
         and exists (select 'x'
                       from cost_susp_sup_head csh
                      where csh.cost_change = csd.cost_change
                        and csh.reason in (1,2,3));
      ---
      open C_LOCK_CSSDL;
      close C_LOCK_CSSDL;
      ---
      update cost_susp_sup_detail_loc csl
         set csl.default_bracket_ind = DECODE(csl.bracket_value1, I_bracket_value, 'Y', 'N')
       where csl.supplier = I_supplier
         and exists (select 'x'
                       from cost_susp_sup_head csh
                      where csh.cost_change = csl.cost_change
                        and csh.reason in (1,2,3));

   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END UPDATE_COST_CHANGE_FOR_DEFAULT;
----------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_BRACKET(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_bracket_value IN OUT  SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                             I_seq_no        IN      SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(60) := 'SUP_BRACKET_COST_SQL.GET_DEFAULT_BRACKET';

   cursor C_GET_DEFAULT_BRACKET is
      select bracket_value1
        from sup_bracket_cost
       where sup_dept_seq_no = I_seq_no
         and default_bracket_ind = 'Y';

BEGIN
   if I_seq_no is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_seq_no',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_DEFAULT_BRACKET','SUP_BRACKET_COST', 'Sup Dept Seq No: '|| to_char(I_seq_no));
   open C_GET_DEFAULT_BRACKET;
   SQL_LIB.SET_MARK('FETCH','C_GET_DEFAULT_BRACKET','SUP_BRACKET_COST', 'Sup Dept Seq No: '|| to_char(I_seq_no));
   fetch C_GET_DEFAULT_BRACKET into O_bracket_value;
   SQL_LIB.SET_MARK('CLOSE','C_GET_DEFAULT_BRACKET','SUP_BRACKET_COST', 'Sup Dept Seq No: '|| to_char(I_seq_no));
   close C_GET_DEFAULT_BRACKET;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END GET_DEFAULT_BRACKET;
---------------------------------------------------------------------------------
FUNCTION INV_MGMT_LVL_BRACKETS_EXIST(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exist         IN OUT  BOOLEAN,
                                     I_seq_no        IN      SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(60) := 'SUP_BRACKET_COST_SQL.INV_MGMT_LVL_BRACKETS_EXIST';
   L_dummy       VARCHAR2(1)  := NULL;

   cursor C_EXIST is
      select 'x'
        from sup_bracket_cost
       where sup_dept_seq_no = I_seq_no;

BEGIN
   if I_seq_no is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_seq_no',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_EXIST','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_seq_no));
   open C_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_EXIST','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_seq_no));
   fetch C_EXIST into L_dummy;
   if C_EXIST%NOTFOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_EXIST','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_seq_no));
   close C_EXIST;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END INV_MGMT_LVL_BRACKETS_EXIST;
---------------------------------------------------------------------------------
FUNCTION COST_CHANGE_EXISTS(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist         IN OUT  BOOLEAN,
                            I_cost_change   IN      COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(60) := 'SUP_BRACKET_COST_SQL.COST_CHANGE_EXISTS';
   L_dummy       VARCHAR2(1)  := NULL;

   cursor C_EXIST is
      select 'x'
        from cost_susp_sup_head
       where cost_change = I_cost_change;

BEGIN
   if I_cost_change is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_cost_change',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_EXIST','COST_SUSP_SUP_HEAD', 'Cost Change: '|| to_char(I_cost_change));
   open C_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_EXIST','COST_SUSP_SUP_HEAD', 'Cost Change: '|| to_char(I_cost_change));
   fetch C_EXIST into L_dummy;
   if C_EXIST%NOTFOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_EXIST','COST_SUSP_SUP_HEAD', 'Cost Change: '|| to_char(I_cost_change));
   close C_EXIST;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END COST_CHANGE_EXISTS;
---------------------------------------------------------------------------------
FUNCTION DELETE_DFLT_NEXT_BRACKET_LVL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_supplier      IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                                      I_dept          IN     SUP_BRACKET_COST.DEPT%TYPE,
                                      I_location      IN     SUP_BRACKET_COST.LOCATION%TYPE,
                                      I_seq_no        IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(60)  := 'SUP_BRACKET_COST_SQL.DELETE_DFLT_NEXT_BRACKET_LVL';
   L_table          VARCHAR2(30)  := 'ITEM_SUPP_COUNTRY_BRACKET_COST';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   L_bracket_lvl    SUPS.INV_MGMT_LVL%TYPE;

   cursor C_DELETE_SUPP_DEPT_LOC is
      select 'x'
        from item_supp_country_bracket_cost iscbc,
             wh
       where iscbc.supplier  = I_supplier
         and wh.physical_wh  = I_location
         and iscbc.location  = wh.wh
         and exists (select 'x'
                       from item_master im
                      where im.dept = I_dept
                        and im.item = iscbc.item)
         for update nowait;

   cursor C_DELETE_SUPP_DEPT is
       select 'x'
        from item_supp_country_bracket_cost iscbc
       where iscbc.supplier        = I_supplier
         and exists (select 'x'
                       from item_master im
                      where im.dept = I_dept
                        and im.item = iscbc.item)
         and exists (select 'x'
                       from v_sim_seq_explode v,
                            item_master im
                      where v.sup_dept_seq_no = I_seq_no
                        and v.dept            = im.dept
                        and v.wh              = iscbc.location
                        and im.item           = iscbc.item)
         for update nowait;

   cursor C_DELETE_SUPP_LOC is
       select 'x'
        from item_supp_country_bracket_cost iscbc,
             wh
       where iscbc.supplier = I_supplier
         and wh.physical_wh = I_location
         and iscbc.location = wh.wh
         for update nowait;


   cursor C_DELETE_SUPP is
       select 'x'
         from item_supp_country_bracket_cost iscbc
        where iscbc.supplier        = I_supplier
          and exists (select 'x'
                        from v_sim_seq_explode v,
                             item_master im
                       where v.sup_dept_seq_no = I_seq_no
                         and v.dept            = im.dept
                         and im.item           = iscbc.item
                         and v.wh              = iscbc.location)
          for update nowait;

BEGIN
   if I_supplier is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_supplier',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   if not SUP_INV_MGMT_SQL.GET_INV_MGMT_LEVEL(O_error_message,
                                              L_bracket_lvl,
                                              I_supplier) then
      return FALSE;
   end if;

   if I_dept is not NULL and I_location is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_SUPP_DEPT_LOC','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept) || 'Location: '|| to_char(I_location));
      open C_DELETE_SUPP_DEPT_LOC;
      SQL_LIB.SET_MARK('CLOSE','C_SUPP_DEPT_LOC','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept) || 'Location: '|| to_char(I_location));
      close C_DELETE_SUPP_DEPT_LOC;

      SQL_LIB.SET_MARK('DELETE','C_SUPP_DEPT_LOC','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept) || 'Location: '|| to_char(I_location));

      delete from item_supp_country_bracket_cost iscbc
       where iscbc.supplier = I_supplier
         and iscbc.location in (select wh.wh
                                  from wh
                                 where wh.physical_wh = I_location)
         and exists (select 'x'
                       from item_master im
                      where im.dept = I_dept
                        and im.item = iscbc.item);

   elsif I_dept is not NULL and I_location is NULL then
      SQL_LIB.SET_MARK('OPEN','C_SUPP_DEPT','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept));
      open C_DELETE_SUPP_DEPT;
      SQL_LIB.SET_MARK('CLOSE','C_SUPP_DEPT','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept));
      close C_DELETE_SUPP_DEPT;

      SQL_LIB.SET_MARK('DELETE','C_SUPP_DEPT','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept));

      if L_bracket_lvl in ('S', 'D') then
         delete from item_supp_country_bracket_cost iscbc
          where iscbc.supplier = I_supplier
            and iscbc.location is NULL
            and exists (select 'x'
                       from v_sim_seq_explode v,
                            item_master im
                      where v.sup_dept_seq_no = I_seq_no
                        and v.dept            = im.dept
                        and im.item           = iscbc.item);
      end if;

      delete from item_supp_country_bracket_cost iscbc
       where iscbc.supplier        = I_supplier
         and exists (select 'x'
                       from item_master im
                      where im.dept = I_dept
                        and im.item = iscbc.item)
         and exists (select 'x'
                       from v_sim_seq_explode v,
                            item_master im
                      where v.sup_dept_seq_no = I_seq_no
                        and v.dept            = im.dept
                        and v.wh              = iscbc.location
                        and im.item           = iscbc.item);

   elsif I_dept is NULL and I_location is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_SUPP_LOC','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Location: '|| to_char(I_location));
      open C_DELETE_SUPP_LOC;
      SQL_LIB.SET_MARK('CLOSE','C_SUPP_LOC','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Location: '|| to_char(I_location));
      close C_DELETE_SUPP_LOC;

      SQL_LIB.SET_MARK('DELETE','C_SUPP_LOC','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Location: '|| to_char(I_location));

      delete from item_supp_country_bracket_cost
       where supplier  = I_supplier
         and location in (select wh.wh
                            from wh
                           where wh.physical_wh = I_location);

   else
      SQL_LIB.SET_MARK('OPEN','C_SUPP','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier));
      open C_DELETE_SUPP;
      SQL_LIB.SET_MARK('CLOSE','C_SUPP','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier));
      close C_DELETE_SUPP;

      SQL_LIB.SET_MARK('DELETE','C_SUPP','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier));
      if L_bracket_lvl in ('S', 'D') then
         delete from item_supp_country_bracket_cost iscbc
          where iscbc.supplier = I_supplier
            and iscbc.location is NULL
            and exists (select 'x'
                       from v_sim_seq_explode v,
                            item_master im
                      where v.sup_dept_seq_no = I_seq_no
                        and v.dept            = im.dept
                        and im.item           = iscbc.item);
      end if;

      delete from item_supp_country_bracket_cost iscbc
       where iscbc.supplier        = I_supplier
         and exists (select 'x'
                       from v_sim_seq_explode v,
                            item_master im
                      where v.sup_dept_seq_no = I_seq_no
                        and v.dept            = im.dept
                        and im.item           = iscbc.item
                        and v.wh              = iscbc.location);
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Supplier: ' || to_char(I_supplier) ||', Department: '|| to_char(I_dept)
                                             ||', Location: '|| to_char(I_location),
                                             NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END DELETE_DFLT_NEXT_BRACKET_LVL;
---------------------------------------------------------------------------------
FUNCTION DELETE_BRACKET(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_bracket_value IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                        I_supplier      IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                        I_dept          IN     SUP_BRACKET_COST.DEPT%TYPE,
                        I_location      IN     SUP_BRACKET_COST.LOCATION%TYPE,
                        I_seq_no        IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(60)  := 'SUP_BRACKET_COST_SQL.DELETE_BRACKET';
   L_table          VARCHAR2(30)  := 'ITEM_SUPP_COUNTRY_BRACKET_COST';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   L_bracket_lvl    SUPS.INV_MGMT_LVL%TYPE;

   cursor C_DELETE_SUPP_DEPT_LOC is
      select 'x'
        from item_supp_country_bracket_cost
       where sup_dept_seq_no = I_seq_no
         and (bracket_value1 = I_bracket_value
          or I_bracket_value is NULL)
         for update nowait;

   cursor C_DELETE_SUPP_DEPT is
       select 'x'
        from item_supp_country_bracket_cost iscbc
       where iscbc.sup_dept_seq_no = I_seq_no
         and (iscbc.bracket_value1 = I_bracket_value
          or I_bracket_value is NULL)
         and exists (select 'x'
                       from item_master im
                      where im.dept = I_dept
                        and im.item = iscbc.item)
         for update nowait;

   cursor C_DELETE_SUPP_LOC is
       select 'x'
        from item_supp_country_bracket_cost iscbc,
             wh
       where iscbc.supplier = I_supplier
         and wh.physical_wh = I_location
         and iscbc.location = wh.wh
         and (iscbc.bracket_value1 = I_bracket_value
          or I_bracket_value is NULL)
         for update nowait;


   cursor C_DELETE_SUPP is
       select 'x'
         from item_supp_country_bracket_cost
        where sup_dept_seq_no = I_seq_no
          and (bracket_value1 = I_bracket_value
           or I_bracket_value is NULL)
          for update nowait;

BEGIN
   if I_seq_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_seq_no',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   if not SUP_INV_MGMT_SQL.GET_INV_MGMT_LEVEL(O_error_message,
                                              L_bracket_lvl,
                                              I_supplier) then
      return FALSE;
   end if;

   if I_dept is not NULL and I_location is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_SUPP_DEPT_LOC','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept) || 'Location: '|| to_char(I_location)
                       || 'Primary Bracket Value: '|| to_char(I_bracket_value));
      open C_DELETE_SUPP_DEPT_LOC;
      SQL_LIB.SET_MARK('CLOSE','C_SUPP_DEPT_LOC','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept) || 'Location: '|| to_char(I_location)
                       || 'Primary Bracket Value: '|| to_char(I_bracket_value));
      close C_DELETE_SUPP_DEPT_LOC;

      SQL_LIB.SET_MARK('DELETE','C_SUPP_DEPT_LOC','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept) || 'Location: '|| to_char(I_location)
                       || 'Primary Bracket Value: '|| to_char(I_bracket_value));

      delete from item_supp_country_bracket_cost
       where sup_dept_seq_no = I_seq_no
         and (bracket_value1 = I_bracket_value
          or I_bracket_value is NULL);

   elsif I_dept is not NULL and I_location is NULL then
      SQL_LIB.SET_MARK('OPEN','C_SUPP_DEPT','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept) || 'Primary Bracket Value: '|| to_char(I_bracket_value));
      open C_DELETE_SUPP_DEPT;
      SQL_LIB.SET_MARK('CLOSE','C_SUPP_DEPT','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept) || 'Primary Bracket Value: '|| to_char(I_bracket_value));
      close C_DELETE_SUPP_DEPT;

      SQL_LIB.SET_MARK('DELETE','C_SUPP_DEPT','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept) || 'Primary Bracket Value: '|| to_char(I_bracket_value));

      delete from item_supp_country_bracket_cost iscbc
       where iscbc.sup_dept_seq_no = I_seq_no
         and (iscbc.bracket_value1 = I_bracket_value
          or I_bracket_value is NULL)
         and exists (select 'x'
                       from item_master im
                      where im.dept = I_dept
                        and im.item = iscbc.item);

   elsif I_dept is NULL and I_location is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_SUPP_LOC','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Location: '|| to_char(I_location) || 'Primary Bracket Value: '|| to_char(I_bracket_value));
      open C_DELETE_SUPP_LOC;
      SQL_LIB.SET_MARK('CLOSE','C_SUPP_LOC','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Location: '|| to_char(I_location) || 'Primary Bracket Value: '|| to_char(I_bracket_value));
      close C_DELETE_SUPP_LOC;

      SQL_LIB.SET_MARK('DELETE','C_SUPP_LOC','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Location: '|| to_char(I_location) || 'Primary Bracket Value: '|| to_char(I_bracket_value));

      delete from item_supp_country_bracket_cost
       where supplier  = I_supplier
         and location in (select wh.wh
                            from wh
                           where wh.physical_wh = I_location)
         and (bracket_value1 = I_bracket_value
          or I_bracket_value is NULL);

   else
      SQL_LIB.SET_MARK('OPEN','C_SUPP','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Primary Bracket Value: '|| to_char(I_bracket_value));
      open C_DELETE_SUPP;
      SQL_LIB.SET_MARK('CLOSE','C_SUPP','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Primary Bracket Value: '|| to_char(I_bracket_value));
      close C_DELETE_SUPP;

      SQL_LIB.SET_MARK('DELETE','C_SUPP','ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Primary Bracket Value: '|| to_char(I_bracket_value));

      delete from item_supp_country_bracket_cost
       where sup_dept_seq_no       = I_seq_no
         and (bracket_value1 = I_bracket_value
          or I_bracket_value is NULL);
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Supplier: ' || to_char(I_supplier) ||', Department: '|| to_char(I_dept)
                                             ||', Location: '|| to_char(I_location),
                                             NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END DELETE_BRACKET;
---------------------------------------------------------------------------------
FUNCTION INSERT_BRACKET(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_bracket_value1   IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                        I_bracket_uom1     IN     SUP_INV_MGMT.BRACKET_UOM1%TYPE,
                        I_bracket_value2   IN     SUP_BRACKET_COST.BRACKET_VALUE2%TYPE,
                        I_default_ind      IN     SUP_BRACKET_COST.DEFAULT_BRACKET_IND%TYPE,
                        I_supplier         IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                        I_dept             IN     SUP_BRACKET_COST.DEPT%TYPE,
                        I_location         IN     SUP_BRACKET_COST.LOCATION%TYPE,
                        I_cost_change      IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                        I_seq_no           IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE,
                        I_bracket_lvl      IN     SUPS.INV_MGMT_LVL%TYPE,
                        I_new_struct       IN     VARCHAR2)
RETURN BOOLEAN IS
   L_program          VARCHAR2(60)  := 'SUP_BRACKET_COST_SQL.INSERT_BRACKET';
   L_item_exist       BOOLEAN;
   L_cc_exist         BOOLEAN;
   L_items_approved   BOOLEAN;
   L_return_code      VARCHAR2(5);
   L_new_structure    BOOLEAN;
   L_temp_cc_desc     COST_SUSP_SUP_HEAD.COST_CHANGE_DESC%TYPE;
   L_create_date      DATE;

   cursor C_GET_DATE is
      select vdate + cost_prior_create_days
        from period,
             foundation_unit_options;

BEGIN
   if I_supplier is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_supplier',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   if not SUP_BRACKET_COST_SQL.ITEM_COST_EXISTS(O_error_message,
                                                L_item_exist,
                                                L_items_approved,
                                                I_supplier,
                                                I_dept,
                                                I_location,
                                                I_seq_no,
                                                I_bracket_lvl) then
      return FALSE;
   end if;

   if L_item_exist = FALSE then
      return TRUE;
   end if;

--- If get passed above check, then the supplier has been attached to items.

   if L_items_approved = FALSE then

      if I_bracket_lvl in ('S', 'D') then
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept) || 'Primary Bracket Value: '|| to_char(I_bracket_value1)
                       || 'Secondary Bracket Value: '|| to_char(I_bracket_value2));

         insert into item_supp_country_bracket_cost(item,
                                                    sup_dept_seq_no,
                                                    supplier,
                                                    origin_country_id,
                                                    location,
                                                    loc_type,
                                                    default_bracket_ind,
                                                    bracket_value1,
                                                    bracket_value2,
                                                    unit_cost)
                                             select isc.item,
                                                    I_seq_no,
                                                    I_supplier,
                                                    isc.origin_country_id,
                                                    NULL,
                                                    NULL,
                                                    I_default_ind,
                                                    I_bracket_value1,
                                                    I_bracket_value2,
                                                    NULL
                                               from item_supp_country isc,
                                                    item_master im
                                              where isc.supplier  = I_supplier
                                                and (im.pack_type != 'B'
                                                 or  im.pack_type is NULL)
                                                and isc.item = im.item
                                                and (exists (select 'x'
                                                                from item_master im
                                                               where im.dept = I_dept
                                                                 and im.item = isc.item)
                                                     or (I_dept is NULL
                                                    and exists (select 'x'
                                                                  from v_sim_seq_explode v,
                                                                       item_master im
                                                                 where v.sup_dept_seq_no = I_seq_no
                                                                   and v.dept            = im.dept
                                                                   and im.item           = isc.item)));
      end if;

      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_BRACKET_COST','Supplier: '|| to_char(I_supplier)
                       || 'Department: '|| to_char(I_dept) || 'Location: '|| to_char(I_location)
                       || 'Primary Bracket Value: '|| to_char(I_bracket_value1));

      insert into item_supp_country_bracket_cost(item,
                                                 sup_dept_seq_no,
                                                 supplier,
                                                 origin_country_id,
                                                 location,
                                                 loc_type,
                                                 default_bracket_ind,
                                                 bracket_value1,
                                                 bracket_value2,
                                                 unit_cost)
                                          select distinct iscl.item,
                                                 I_seq_no,
                                                 I_supplier,
                                                 iscl.origin_country_id,
                                                 iscl.loc,
                                                 'W',
                                                 I_default_ind,
                                                 I_bracket_value1,
                                                 I_bracket_value2,
                                                 NULL
                                            from item_supp_country_loc iscl,
                                                 item_master im,
                                                 wh
                                           where iscl.supplier   = I_supplier
                                             and (im.pack_type   != 'B'
                                              or  im.pack_type is NULL)
                                             and iscl.item = im.item
                                             and (wh.physical_wh = I_location
                                             and iscl.loc        = wh.wh
                                                 or (I_location is NULL
                                                and exists (select 'x'
                                                              from v_sim_seq_explode
                                                             where sup_dept_seq_no = I_seq_no
                                                               and wh = iscl.loc)))
                                             and (exists (select 'x'
                                                             from item_master im
                                                            where im.dept = I_dept
                                                              and im.item = iscl.item)
                                                 or (I_dept is NULL
                                                and exists (select 'x'
                                                              from v_sim_seq_explode v,
                                                                   item_master im
                                                             where v.sup_dept_seq_no = I_seq_no
                                                               and v.dept = im.dept
                                                               and im.item = iscl.item
                                                               and v.wh    = iscl.loc)));
   else
   --- create a cost change

      SQL_LIB.SET_MARK('OPEN','C_GET_DATE','FOUNDATION_UNIT_OPTIONS, PERIOD',NULL);
      open C_GET_DATE;
      SQL_LIB.SET_MARK('FETCH','C_GET_DATE','FOUNDATION_UNIT_OPTIONS, PERIOD',NULL);
      fetch C_GET_DATE into L_create_date;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DATE','FOUNDATION_UNIT_OPTIONS, PERIOD',NULL);
      close C_GET_DATE;
      ---
      L_temp_cc_desc := 'Ins bkt: Supp '||to_char(I_supplier);

      if I_dept is not NULL and I_location is NULL then
         L_temp_cc_desc := (L_temp_cc_desc||'/'||to_char(I_dept));
      elsif I_dept is NULL and I_location is not NULL then
         L_temp_cc_desc := L_temp_cc_desc||'/'||to_char(I_location);
      elsif I_dept is not NULL and I_location is not NULL then
         L_temp_cc_desc := L_temp_cc_desc||'/'||to_char(I_dept)||'/'||to_char(I_location);
      end if;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'COST_SUSP_SUP_HEAD','Cost Change: '|| to_char(I_cost_change));

      if I_new_struct = 'N' then

         if not SUP_BRACKET_COST_SQL.COST_CHANGE_EXISTS(O_error_message,
                                                        L_cc_exist,
                                                        I_cost_change) then
            return FALSE;
         end if;

         if L_cc_exist = FALSE then

            insert into cost_susp_sup_head(cost_change,
                                           cost_change_desc,
                                           reason,
                                           active_date,
                                           status,
                                           cost_change_origin,
                                           create_date,
                                           create_id)
                                   values (I_cost_change,
                                           L_temp_cc_desc,
                                           1,
                                           L_create_date, --- this field will be editable in dialogue
                                           'W',
                                           'SUP',
                                           sysdate,
                                           user);
         end if;
      else
         if not SUP_BRACKET_COST_SQL.COST_CHANGE_EXISTS(O_error_message,
                                                        L_cc_exist,
                                                        I_cost_change) then
            return FALSE;
         end if;

         if L_cc_exist = FALSE then
            insert into cost_susp_sup_head(cost_change,
                                           cost_change_desc,
                                           reason,
                                           active_date,
                                           status,
                                           cost_change_origin,
                                           create_date,
                                           create_id)
                                   values (I_cost_change,
                                           L_temp_cc_desc,
                                           2,
                                           L_create_date, --- this field will be editable in dialogue
                                           'W',
                                           'SUP',
                                           sysdate,
                                           user);
         end if;
      end if;


      if I_bracket_lvl = 'A' then

         SQL_LIB.SET_MARK('INSERT',NULL,'COST_SUSP_SUP_DETAIL_LOC','Cost Change: '|| to_char(I_cost_change)
                          || 'Supplier: '|| to_char(I_supplier) || 'Department: '|| to_char(I_dept)
                          || 'Location: '|| to_char(I_location));

         if I_dept is not NULL and I_location is not NULL then
            insert into cost_susp_sup_detail_loc(cost_change,
                                                 sup_dept_seq_no,
                                                 supplier,
                                                 origin_country_id,
                                                 item,
                                                 dept,
                                                 loc_type,
                                                 loc,
                                                 bracket_value1,
                                                 bracket_value2,
                                                 bracket_uom1,
                                                 default_bracket_ind,
                                                 unit_cost,
                                                 recalc_ord_ind)
                                          select I_cost_change,
                                                 I_seq_no,
                                                 I_supplier,
                                                 iscl.origin_country_id,
                                                 iscl.item,
                                                 I_dept,
                                                 'W',
                                                 iscl.loc,
                                                 I_bracket_value1,
                                                 I_bracket_value2,
                                                 I_bracket_uom1,
                                                 I_default_ind,
                                                 0,
                                                 'N'
                                            from item_supp_country_loc iscl,
                                                 item_master im,
                                                 wh
                                           where iscl.supplier       = I_supplier
                                             and wh.physical_wh      = I_location
                                             and iscl.loc            = wh.wh
                                             and iscl.loc_type       = 'W'
                                             and iscl.item           = im.item
                                             and im.status           = 'A'
                                             and im.dept             = I_dept
                                             and (im.pack_type       != 'B'
                                              or  im.pack_type is NULL);

         elsif I_dept is not NULL and I_location is NULL then
            insert into cost_susp_sup_detail_loc(cost_change,
                                                 sup_dept_seq_no,
                                                 supplier,
                                                 origin_country_id,
                                                 item,
                                                 dept,
                                                 loc_type,
                                                 loc,
                                                 bracket_value1,
                                                 bracket_value2,
                                                 bracket_uom1,
                                                 default_bracket_ind,
                                                 unit_cost,
                                                 recalc_ord_ind)
                                          select I_cost_change,
                                                 I_seq_no,
                                                 I_supplier,
                                                 iscl.origin_country_id,
                                                 iscl.item,
                                                 I_dept,
                                                 'W',
                                                 iscl.loc,
                                                 I_bracket_value1,
                                                 I_bracket_value2,
                                                 I_bracket_uom1,
                                                 I_default_ind,
                                                 0,
                                                 'N'
                                            from item_supp_country_loc iscl,
                                                 item_master im,
                                                 wh
                                           where iscl.supplier       = I_supplier
                                             and iscl.loc            = wh.wh
                                             and iscl.loc_type       = 'W'
                                             and iscl.item           = im.item
                                             and im.status           = 'A'
                                             and im.dept             = I_dept
                                             and (im.pack_type       != 'B'
                                              or  im.pack_type is NULL)
                                             and exists (select 'x'
                                                           from v_sim_seq_explode v,
                                                                item_master im
                                                          where v.sup_dept_seq_no = I_seq_no
                                                            and im.item           = iscl.item
                                                            and v.wh              = iscl.loc);

         elsif I_dept is NULL and I_location is NULL then
            insert into cost_susp_sup_detail_loc(cost_change,
                                                 sup_dept_seq_no,
                                                 supplier,
                                                 origin_country_id,
                                                 item,
                                                 dept,
                                                 loc_type,
                                                 loc,
                                                 bracket_value1,
                                                 bracket_value2,
                                                 bracket_uom1,
                                                 default_bracket_ind,
                                                 unit_cost,
                                                 recalc_ord_ind)
                                          select I_cost_change,
                                                 I_seq_no,
                                                 I_supplier,
                                                 iscl.origin_country_id,
                                                 iscl.item,
                                                 I_dept,
                                                 'W',
                                                 iscl.loc,
                                                 I_bracket_value1,
                                                 I_bracket_value2,
                                                 I_bracket_uom1,
                                                 I_default_ind,
                                                 0,
                                                 'N'
                                            from item_supp_country_loc iscl,
                                                 item_master im,
                                                 wh
                                           where iscl.supplier       = I_supplier
                                             and iscl.loc            = wh.wh
                                             and iscl.loc_type       = 'W'
                                             and iscl.item           = im.item
                                             and im.status           = 'A'
                                             and (im.pack_type       != 'B'
                                              or  im.pack_type is NULL)
                                             and exists (select 'x'
                                                           from v_sim_seq_explode v,
                                                                item_master im
                                                          where v.sup_dept_seq_no = I_seq_no
                                                            and v.dept            = im.dept
                                                            and im.item           = iscl.item
                                                            and v.wh              = iscl.loc);
         end if;

      elsif I_bracket_lvl = 'D' then
         SQL_LIB.SET_MARK('INSERT',NULL,'COST_SUSP_SUP_DETAIL','Cost Change: '|| to_char(I_cost_change)
                          || 'Supplier: '|| to_char(I_supplier) || 'Department: '|| to_char(I_dept));

         if I_dept is not NULL then
            insert into cost_susp_sup_detail(cost_change,
                                             sup_dept_seq_no,
                                             supplier,
                                             origin_country_id,
                                             item,
                                             dept,
                                             bracket_value1,
                                             bracket_value2,
                                             bracket_uom1,
                                             default_bracket_ind,
                                             unit_cost,
                                             recalc_ord_ind)
                                      select I_cost_change,
                                             I_seq_no,
                                             I_supplier,
                                             isc.origin_country_id,
                                             isc.item,
                                             I_dept,
                                             I_bracket_value1,
                                             I_bracket_value2,
                                             I_bracket_uom1,
                                             I_default_ind,
                                             0,
                                             'N'
                                        from item_supp_country isc,
                                             item_master im
                                       where isc.supplier        = I_supplier
                                         and isc.item            = im.item
                                         and im.status           = 'A'
                                         and im.dept             = I_dept
                                         and (im.pack_type       != 'B'
                                          or  im.pack_type is NULL);

         else
            insert into cost_susp_sup_detail(cost_change,
                                             sup_dept_seq_no,
                                             supplier,
                                             origin_country_id,
                                             item,
                                             dept,
                                             bracket_value1,
                                             bracket_value2,
                                             bracket_uom1,
                                             default_bracket_ind,
                                             unit_cost,
                                             recalc_ord_ind)
                                      select I_cost_change,
                                             I_seq_no,
                                             I_supplier,
                                             isc.origin_country_id,
                                             isc.item,
                                             I_dept,
                                             I_bracket_value1,
                                             I_bracket_value2,
                                             I_bracket_uom1,
                                             I_default_ind,
                                             0,
                                             'N'
                                        from item_supp_country isc,
                                             item_master im
                                       where isc.supplier        = I_supplier
                                         and isc.item            = im.item
                                         and im.status           = 'A'
                                         and (im.pack_type       != 'B'
                                          or  im.pack_type is NULL)
                                         and exists (select 'x'
                                                       from v_sim_seq_explode
                                                      where sup_dept_seq_no = I_seq_no
                                                        and dept            = im.dept);

         end if;
      elsif I_bracket_lvl = 'L' then
         SQL_LIB.SET_MARK('INSERT',NULL,'COST_SUSP_SUP_DETAIL','Cost Change: '|| to_char(I_cost_change)
                          || 'Supplier: '|| to_char(I_supplier) || 'Location: '|| to_char(I_location));

         if I_location is not NULL then
            insert into cost_susp_sup_detail_loc(cost_change,
                                                 sup_dept_seq_no,
                                                 supplier,
                                                 origin_country_id,
                                                 item,
                                                 dept,
                                                 loc_type,
                                                 loc,
                                                 bracket_value1,
                                                 bracket_value2,
                                                 bracket_uom1,
                                                 default_bracket_ind,
                                                 unit_cost,
                                                 recalc_ord_ind)
                                          select I_cost_change,
                                                 I_seq_no,
                                                 I_supplier,
                                                 iscl.origin_country_id,
                                                 iscl.item,
                                                 I_dept,
                                                 'W',
                                                 iscl.loc,
                                                 I_bracket_value1,
                                                 I_bracket_value2,
                                                 I_bracket_uom1,
                                                 I_default_ind,
                                                 0,
                                                 'N'
                                            from item_supp_country_loc iscl,
                                                 item_master im,
                                                 wh
                                           where iscl.supplier       = I_supplier
                                             and iscl.loc_type       = 'W'
                                             and wh.physical_wh      = I_location
                                             and iscl.loc            = wh.wh
                                             and iscl.item           = im.item
                                             and im.status           = 'A'
                                             and (im.pack_type       != 'B'
                                              or  im.pack_type is NULL);
         else
            insert into cost_susp_sup_detail_loc(cost_change,
                                                 sup_dept_seq_no,
                                                 supplier,
                                                 origin_country_id,
                                                 item,
                                                 dept,
                                                 loc_type,
                                                 loc,
                                                 bracket_value1,
                                                 bracket_value2,
                                                 bracket_uom1,
                                                 default_bracket_ind,
                                                 unit_cost,
                                                 recalc_ord_ind)
                                          select I_cost_change,
                                                 I_seq_no,
                                                 I_supplier,
                                                 iscl.origin_country_id,
                                                 iscl.item,
                                                 I_dept,
                                                 'W',
                                                 iscl.loc,
                                                 I_bracket_value1,
                                                 I_bracket_value2,
                                                 I_bracket_uom1,
                                                 I_default_ind,
                                                 0,
                                                 'N'
                                            from item_supp_country_loc iscl,
                                                 item_master im,
                                                 wh
                                           where iscl.supplier       = I_supplier
                                             and iscl.loc            = wh.wh
                                             and iscl.loc_type       = 'W'
                                             and iscl.item           = im.item
                                             and im.status           = 'A'
                                             and (im.pack_type       != 'B'
                                              or  im.pack_type is NULL)
                                             and exists (select 'x'
                                                           from v_sim_seq_explode v,
                                                                item_master im
                                                          where v.sup_dept_seq_no = I_seq_no
                                                            and v.dept            = im.dept
                                                            and im.item           = iscl.item
                                                            and v.wh              = iscl.loc);
         end if;

      elsif I_bracket_lvl = 'S' then
         SQL_LIB.SET_MARK('INSERT',NULL,'COST_SUSP_SUP_DETAIL','Cost Change: '|| to_char(I_cost_change)
                          || 'Supplier: '|| to_char(I_supplier));

         insert into cost_susp_sup_detail(cost_change,
                                          sup_dept_seq_no,
                                          supplier,
                                          origin_country_id,
                                          item,
                                          dept,
                                          bracket_value1,
                                          bracket_value2,
                                          bracket_uom1,
                                          default_bracket_ind,
                                          unit_cost,
                                          recalc_ord_ind)
                                   select I_cost_change,
                                          I_seq_no,
                                          I_supplier,
                                          isc.origin_country_id,
                                          isc.item,
                                          I_dept,
                                          I_bracket_value1,
                                          I_bracket_value2,
                                          I_bracket_uom1,
                                          I_default_ind,
                                          0,
                                          'N'
                                     from item_supp_country isc,
                                          item_master im
                                    where isc.supplier        = I_supplier
                                      and isc.item            = im.item
                                      and im.status           = 'A'
                                      and (im.pack_type       != 'B'
                                       or  im.pack_type is NULL)
                                      and exists (select 'x'
                                                    from v_sim_seq_explode
                                                   where sup_dept_seq_no = I_seq_no
                                                     and dept            = im.dept);
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END INSERT_BRACKET;
---------------------------------------------------------------------------------
FUNCTION CHANGE_DEFAULT_BRACKET(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_cost_change    IN OUT COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                                I_bracket_value1 IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                                I_bracket_uom1   IN     SUP_INV_MGMT.BRACKET_UOM1%TYPE,
                                I_bracket_value2 IN     SUP_BRACKET_COST.BRACKET_VALUE2%TYPE,
                                I_supplier       IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                                I_dept           IN     SUP_BRACKET_COST.DEPT%TYPE,
                                I_location       IN     SUP_BRACKET_COST.LOCATION%TYPE,
                                I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE,
                                I_bracket_lvl    IN     SUPS.INV_MGMT_LVL%TYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(60)  := 'SUP_BRACKET_COST_SQL.CHANGE_DEFAULT_BRACKET';
   L_table            VARCHAR2(30)  := 'ITEM_SUPP_COUNTRY_BRACKET_COST';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);
   L_item_exist       BOOLEAN;
   L_items_approved   BOOLEAN;
   L_return_code      VARCHAR2(5);
   L_temp_cc_desc     COST_SUSP_SUP_HEAD.COST_CHANGE_DESC%TYPE;
   L_cc_exist         BOOLEAN;
   L_reason           COST_SUSP_SUP_HEAD.REASON%TYPE;
   L_create_date      DATE;
   L_status           COST_SUSP_SUP_HEAD.STATUS%TYPE;

   cursor C_GET_DATE is
      select vdate + cost_prior_create_days
        from period,
             foundation_unit_options;

   cursor C_LOCK_ISCBC_N is
      select 'x'
        from item_supp_country_bracket_cost
       where sup_dept_seq_no = I_seq_no
         and bracket_value1 != I_bracket_value1
         for update nowait;

   cursor C_LOCK_ISCBC_Y is
      select 'x'
        from item_supp_country_bracket_cost
       where sup_dept_seq_no = I_seq_no
         and bracket_value1  = I_bracket_value1
         for update nowait;

   cursor C_GET_ITEMS is
      select distinct item,
                      origin_country_id,
                      location
        from item_supp_country_bracket_cost
       where sup_dept_seq_no = I_seq_no
         and location is not NULL;

BEGIN
   if I_seq_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_seq_no',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   if not SUP_BRACKET_COST_SQL.ITEM_COST_EXISTS(O_error_message,
                                                L_item_exist,
                                                L_items_approved,
                                                I_supplier,
                                                I_dept,
                                                I_location,
                                                I_seq_no,
                                                I_bracket_lvl) then
      return FALSE;
   end if;

   if L_item_exist = FALSE then
      return TRUE;
   end if;

--- If get passed above check, then the supplier has been attached to items.

   if L_items_approved = FALSE then
      open C_LOCK_ISCBC_N;
      close C_LOCK_ISCBC_N;
      update item_supp_country_bracket_cost
         set default_bracket_ind = 'N'
       where sup_dept_seq_no     = I_seq_no
         and bracket_value1     != I_bracket_value1;

      open C_LOCK_ISCBC_Y;
      close C_LOCK_ISCBC_Y;
      update item_supp_country_bracket_cost
         set default_bracket_ind = 'Y'
       where sup_dept_seq_no     = I_seq_no
         and bracket_value1      = I_bracket_value1;

      for rec in C_GET_ITEMS LOOP
         if not ITEM_BRACKET_COST_SQL.UPDATE_LOCATION_COST(O_error_message,
                                                           rec.item,
                                                           I_supplier,
                                                           rec.origin_country_id,
                                                           rec.location,
                                                           'Y') then
            return FALSE;
         end if;
      END LOOP;
   else
   --- create a cost change if the default bracket indicator is 'Y'
      NEXT_COSTCHG_NUMBER(O_cost_change,
                          L_return_code,
                          O_error_message);
      if L_return_code = 'FALSE' then
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_DATE','FOUNDATION_UNIT_OPTIONS, PERIOD',NULL);
      open C_GET_DATE;
      SQL_LIB.SET_MARK('FETCH','C_GET_DATE','FOUNDATION_UNIT_OPTIONS, PERIOD',NULL);
      fetch C_GET_DATE into L_create_date;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DATE','FOUNDATION_UNIT_OPTIONS, PERIOD',NULL);
      close C_GET_DATE;
      ---
      L_temp_cc_desc := 'Dft bkt: Supp '||to_char(I_supplier);

      if I_dept is not NULL and I_location is NULL then
         L_temp_cc_desc := (L_temp_cc_desc||'/'||to_char(I_dept));
      elsif I_dept is NULL and I_location is not NULL then
         L_temp_cc_desc := L_temp_cc_desc||'/'||to_char(I_location);
      elsif I_dept is not NULL and I_location is not NULL then
         L_temp_cc_desc := L_temp_cc_desc||'/'||to_char(I_dept)||'/'||to_char(I_location);
      end if;

      if not SUP_BRACKET_COST_SQL.COST_CHANGE_EXISTS(O_error_message,
                                                     L_cc_exist,
                                                     O_cost_change) then
         return FALSE;
      end if;

      if L_cc_exist = FALSE then
         insert into cost_susp_sup_head(cost_change,
                                        cost_change_desc,
                                        reason,
                                        active_date,
                                        status,
                                        cost_change_origin,
                                        create_date,
                                        create_id)
                                 values(O_cost_change,
                                        L_temp_cc_desc,
                                        3,
                                        L_create_date, --- this field will be editable in dialogue
                                        'W',
                                        'SUP',
                                        sysdate,
                                        user);
      end if;

      if I_bracket_lvl in ('S','D') then
          insert into cost_susp_sup_detail(cost_change,
                                           sup_dept_seq_no,
                                           supplier,
                                           origin_country_id,
                                           item,
                                           bracket_value1,
                                           bracket_value2,
                                           bracket_uom1,
                                           default_bracket_ind,
                                           unit_cost,
                                           recalc_ord_ind,
                                           dept)
                                    select distinct O_cost_change,
                                           I_seq_no,
                                           I_supplier,
                                           iscbc.origin_country_id,
                                           iscbc.item,
                                           I_bracket_value1,
                                           I_bracket_value2,
                                           I_bracket_uom1,
                                           'Y',
                                           iscbc.unit_cost,
                                           'N',
                                           I_dept
                                      from item_supp_country_bracket_cost iscbc,
                                           item_master im
                                     where iscbc.sup_dept_seq_no = I_seq_no
                                       and iscbc.bracket_value1  = I_bracket_value1
                                       and iscbc.item            = im.item
                                       and iscbc.location is NULL
                                       and im.status             = 'A';

      elsif I_bracket_lvl in ('A','L') then
         insert into cost_susp_sup_detail_loc(cost_change,
                                              sup_dept_seq_no,
                                              supplier,
                                              origin_country_id,
                                              item,
                                              loc_type,
                                              loc,
                                              bracket_value1,
                                              bracket_value2,
                                              bracket_uom1,
                                              default_bracket_ind,
                                              unit_cost,
                                              recalc_ord_ind,
                                              dept)
                                       select O_cost_change,
                                              I_seq_no,
                                              I_supplier,
                                              iscbc.origin_country_id,
                                              iscbc.item,
                                              'W',
                                              iscbc.location,
                                              I_bracket_value1,
                                              I_bracket_value2,
                                              I_bracket_uom1,
                                              'Y',
                                              iscbc.unit_cost,
                                              'N',
                                              I_dept
                                         from item_supp_country_bracket_cost iscbc,
                                              item_master im
                                        where iscbc.sup_dept_seq_no = I_seq_no
                                          and iscbc.bracket_value1  = I_bracket_value1
                                          and iscbc.item            = im.item
                                          and im.status             = 'A';
      end if;
   end if;


   if not UPDATE_COST_CHANGE_FOR_DEFAULT(O_error_message,
                                         L_status,
                                         O_cost_change,
                                         I_supplier,
                                         I_bracket_value1) then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Supplier: ' || to_char(I_supplier) ||', Department: '|| to_char(I_dept)
                                             ||', Location: '|| to_char(I_location),
                                             NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END CHANGE_DEFAULT_BRACKET;
---------------------------------------------------------------------------------
FUNCTION DUP_BRACKET_CHECK(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists         IN OUT BOOLEAN,
                           I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE,
                           I_bracket_value1 IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(60) := 'SUP_BRACKET_COST_SQL.DUP_BRACKET_CHECK';
   L_dummy     VARCHAR2(1);

   cursor C_DUP_CHECK is
      select 'x'
        from sup_bracket_cost
       where sup_dept_seq_no = I_seq_no
         and bracket_value1  = I_bracket_value1;

BEGIN
   O_exists := FALSE;

   if I_seq_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_seq_no',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_DUP_CHECK','SUP_BRACKET_COST','Sup Dept Seq No: '|| to_char(I_seq_no)
                    || 'Primary Bracket Value: '|| to_char(I_bracket_value1));
   open C_DUP_CHECK;
   SQL_LIB.SET_MARK('FETCH','C_DUP_CHECK','SUP_BRACKET_COST','Sup Dept Seq No: '|| to_char(I_seq_no)
                    || 'Primary Bracket Value: '|| to_char(I_bracket_value1));
   fetch C_DUP_CHECK into L_dummy;
   if C_DUP_CHECK%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_DUP_CHECK','SUP_BRACKET_COST','Sup Dept Seq No: '|| to_char(I_seq_no)
                    || 'Primary Bracket Value: '|| to_char(I_bracket_value1));
   close C_DUP_CHECK;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END DUP_BRACKET_CHECK;
---------------------------------------------------------------------------------
FUNCTION ITEM_COST_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_items_exist    IN OUT BOOLEAN,
                          O_items_approved IN OUT BOOLEAN,
                          I_supplier       IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                          I_dept           IN     SUP_BRACKET_COST.DEPT%TYPE,
                          I_location       IN     SUP_BRACKET_COST.LOCATION%TYPE,
                          I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE,
                          I_bracket_lvl    IN     SUPS.INV_MGMT_LVL%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(60)     := 'SUP_BRACKET_COST_SQL.ITEM_COST_EXISTS';
   L_items_status    VARCHAR2(1);
   L_items_approved  VARCHAR2(1);

   cursor C_ITEM_COST_EXIST_SD is
      select im.status
        from item_supp_country isc,
             item_master im
       where isc.supplier  = I_supplier
         and (im.pack_type != 'B'
          or  im.pack_type is NULL)
         and isc.item = im.item
         and (exists (select 'x'
                       from item_master im
                      where im.dept = I_dept
                        and im.item = isc.item)
             or (I_dept is NULL
            and exists (select 'x'
                          from v_sim_seq_explode v,
                               item_master im
                         where v.sup_dept_seq_no = I_seq_no
                           and v.dept            = im.dept
                           and im.item           = isc.item)));

   cursor C_ITEM_COST_EXIST_LA is
      select im.status
        from item_supp_country_loc iscl,
             item_master im,
             wh
       where iscl.supplier = I_supplier
         and (im.pack_type != 'B'
          or  im.pack_type is NULL)
         and iscl.item = im.item
         and (exists (select 'x'
                       from item_master im
                      where im.dept = I_dept
                        and im.item = iscl.item)
             or (I_dept is NULL
            and exists (select 'x'
                          from v_sim_seq_explode v,
                               item_master im
                         where v.sup_dept_seq_no = I_seq_no
                           and v.dept            = im.dept
                           and im.item           = iscl.item
                           and v.wh              = iscl.loc)))
         and (iscl.loc = wh.wh
              and wh.physical_wh = I_location
               or (I_location is NULL
              and exists (select 'x'
                            from v_sim_seq_explode
                           where sup_dept_seq_no = I_seq_no
                             and wh              = iscl.loc)));

BEGIN

   O_items_exist := FALSE;
   O_items_approved := FALSE;

   if I_supplier is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_supplier',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   if I_bracket_lvl in ('S', 'D') then
      SQL_LIB.SET_MARK('OPEN','C_ITEM_COST_EXIST_SD','ITEM_SUPP_COUNTRY','Supplier: '|| to_char(I_supplier));
      open C_ITEM_COST_EXIST_SD;
      SQL_LIB.SET_MARK('FETCH','C_ITEM_COST_EXIST_SD','ITEM_SUPP_COUNTRY','Supplier: '|| to_char(I_supplier));
      loop
         fetch C_ITEM_COST_EXIST_SD into L_items_status;
         EXIT when C_ITEM_COST_EXIST_SD%NOTFOUND;
         EXIT when L_items_status = 'A';
      end loop;
      if L_items_status is not null then
         if L_items_status ='A' then
            O_items_approved := TRUE;
         else
            O_items_approved := FALSE;
         end if;
         O_items_exist := TRUE;
      else
         O_items_exist := FALSE;
         O_items_approved := FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_COST_EXIST_SD','ITEM_SUPP_COUNTRY','Supplier: '|| to_char(I_supplier));
      close C_ITEM_COST_EXIST_SD;
   else -- I_bracket_lvl is 'A' or 'L'
      SQL_LIB.SET_MARK('OPEN','C_ITEM_COST_EXIST_LA','ITEM_SUPP_COUNTRY_LOC','Supplier: '|| to_char(I_supplier));
      open C_ITEM_COST_EXIST_LA;
      SQL_LIB.SET_MARK('FETCH','C_ITEM_COST_EXIST_LA','ITEM_SUPP_COUNTRY_LOC','Supplier: '|| to_char(I_supplier));
      loop
         fetch C_ITEM_COST_EXIST_LA into L_items_status;
         EXIT when C_ITEM_COST_EXIST_LA%NOTFOUND;
         EXIT when L_items_status = 'A';
      end loop;
      if L_items_status is not null then
         if L_items_status ='A' then
            O_items_approved := TRUE;
         else
            O_items_approved := FALSE;
         end if;
         O_items_exist := TRUE;
      else
         O_items_exist := FALSE;
         O_items_approved := FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_COST_EXIST_LA','ITEM_SUPP_COUNTRY_LOC','Supplier: '|| to_char(I_supplier));
      close C_ITEM_COST_EXIST_LA;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END ITEM_COST_EXISTS;
---------------------------------------------------------------------------------
FUNCTION COST_CHG_DUPS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists         IN OUT BOOLEAN,
                       O_cost_change    IN OUT COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                       O_reason         IN OUT COST_SUSP_SUP_HEAD.REASON%TYPE,
                       I_bracket_value1 IN     COST_SUSP_SUP_DETAIL_LOC.BRACKET_VALUE1%TYPE,
                       I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(60) := 'SUP_BRACKET_COST_SQL.COST_CHG_DUPS';
   L_status         COST_SUSP_SUP_HEAD.STATUS%TYPE;
   L_cost_change    COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE;

   cursor C_DUP is
      select distinct cost_change
        from cost_susp_sup_detail
       where sup_dept_seq_no = I_seq_no
         and (bracket_value1  = I_bracket_value1
          or  I_bracket_value1 is NULL)
      union all
      select distinct cost_change
        from cost_susp_sup_detail_loc
       where sup_dept_seq_no = I_seq_no
         and (bracket_value1  = I_bracket_value1
          or  I_bracket_value1 is NULL);

   cursor C_CC_HEAD is
      select reason, status
        from cost_susp_sup_head
       where cost_change = L_cost_change;

BEGIN
   O_exists := FALSE;

   if I_seq_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_seq_no',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   FOR rec in C_DUP LOOP
      L_cost_change := rec.cost_change;
      SQL_LIB.SET_MARK('OPEN','C_CC_HEAD','COST_SUSP_SUP_HEAD','Cost Chg: '|| to_char(O_cost_change));
      open C_CC_HEAD;
      SQL_LIB.SET_MARK('FETCH','C_CC_HEAD','COST_SUSP_SUP_HEAD','Cost Chg: '|| to_char(O_cost_change));
      fetch C_CC_HEAD into O_reason, L_status;
      if L_status in ('W', 'S', 'A', 'R') then
         O_cost_change := L_cost_change;
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_CC_HEAD','COST_SUSP_SUP_HEAD','Cost Chg: '|| to_char(O_cost_change));
      close C_CC_HEAD;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END COST_CHG_DUPS;
---------------------------------------------------------------------------------
FUNCTION MAX_COST_CHG(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_cost_change    IN OUT COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                      I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE,
                      I_bracket_lvl    IN     SUPS.INV_MGMT_LVL%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(60) := 'SUP_BRACKET_COST_SQL.MAX_COST_CHG';

   cursor C_MAX_CC_SD is
      select max(cost_change)
        from cost_susp_sup_detail
       where sup_dept_seq_no = I_seq_no;

   cursor C_MAX_CC_LA is
      select max(cost_change)
        from cost_susp_sup_detail_loc
       where sup_dept_seq_no = I_seq_no;

BEGIN
   if I_seq_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_seq_no',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   if I_bracket_lvl in ('S', 'D') then
      SQL_LIB.SET_MARK('OPEN','C_MAX_CC_SD','COST_SUSP_SUP_DETAIL_LOC','Seq No: '|| to_char(I_seq_no));
      open C_MAX_CC_SD;
      SQL_LIB.SET_MARK('FETCH','C_MAX_CC_SD','COST_SUSP_SUP_DETAIL_LOC','Seq No: '|| to_char(I_seq_no));
      fetch C_MAX_CC_SD into O_cost_change;
      SQL_LIB.SET_MARK('CLOSE','C_MAX_CC_SD','COST_SUSP_SUP_DETAIL_LOC','Seq No: '|| to_char(I_seq_no));
      close C_MAX_CC_SD;
   else
      SQL_LIB.SET_MARK('OPEN','C_MAX_CC_LA','COST_SUSP_SUP_DETAIL_LOC','Seq No: '|| to_char(I_seq_no));
      open C_MAX_CC_LA;
      SQL_LIB.SET_MARK('FETCH','C_MAX_CC_LA','COST_SUSP_SUP_DETAIL_LOC','Seq No: '|| to_char(I_seq_no));
      fetch C_MAX_CC_LA into O_cost_change;
      SQL_LIB.SET_MARK('CLOSE','C_MAX_CC_LA','COST_SUSP_SUP_DETAIL_LOC','Seq No: '|| to_char(I_seq_no));
      close C_MAX_CC_LA;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END MAX_COST_CHG;
---------------------------------------------------------------------------------
FUNCTION DEFAULT_NEXT_BRACKET_LVL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists         IN OUT BOOLEAN,
                                  I_supplier       IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                                  I_dept           IN     SUP_BRACKET_COST.DEPT%TYPE,
                                  I_location       IN     SUP_BRACKET_COST.LOCATION%TYPE,
                                  I_items_approved IN     BOOLEAN,
                                  I_bracket_lvl    IN     SUPS.INV_MGMT_LVL%TYPE,
                                  I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(60) := 'SUP_BRACKET_COST_SQL.DEFAULT_NEXT_BRACKET_LVL';
   L_sbc_lvl     SUPS.INV_MGMT_LVL%TYPE;
   L_return_code VARCHAR2(5);
   L_cost_change COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE;

   cursor C_SUPP_DEPT is
      select 'D'
        from sup_bracket_cost
       where supplier = I_supplier
         and dept = I_dept
         and location is NULL
         and sup_dept_seq_no != I_seq_no;

   cursor C_SUPP is
      select 'S'
        from sup_bracket_cost
       where supplier = I_supplier
         and dept is NULL
         and location is NULL
         and sup_dept_seq_no != I_seq_no;

   cursor C_UPPER_BRACKET1 is
      select sbc.bracket_value1,
             sim.bracket_uom1,
             sbc.bracket_value2,
             sbc.default_bracket_ind,
             sbc.sup_dept_seq_no
        from sup_bracket_cost sbc,
             sup_inv_mgmt sim
       where sbc.supplier = I_supplier
         and (sbc.dept    = I_dept
          or I_dept is NULL)
         and sbc.location is NULL
         and sbc.sup_dept_seq_no = sim.sup_dept_seq_no;

   cursor C_UPPER_BRACKET2 is
      select sbc.bracket_value1,
             sim.bracket_uom1,
             sbc.bracket_value2,
             sbc.default_bracket_ind,
             sbc.sup_dept_seq_no
        from sup_bracket_cost sbc,
             sup_inv_mgmt sim
       where sbc.supplier = I_supplier
         and sbc.dept is NULL
         and sbc.location is NULL
         and sbc.sup_dept_seq_no = sim.sup_dept_seq_no;

BEGIN
   if I_supplier is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_supplier',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   if I_dept is not NULL and I_location is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_SUPP_DEPT','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
      open C_SUPP_DEPT;
      SQL_LIB.SET_MARK('FETCH','C_SUPP_DEPT','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
      fetch C_SUPP_DEPT into L_sbc_lvl;
      if C_SUPP_DEPT%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE', 'C_SUPP_DEPT', 'SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
         close C_SUPP_DEPT;

         SQL_LIB.SET_MARK('OPEN','C_SUPP','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
         open C_SUPP;
         SQL_LIB.SET_MARK('FETCH','C_SUPP','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
         fetch C_SUPP into L_sbc_lvl;
         if C_SUPP%NOTFOUND then
            SQL_LIB.SET_MARK('CLOSE','C_SUPP','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
            close C_SUPP;
            O_exists := FALSE;
            return TRUE;
         else
            SQL_LIB.SET_MARK('CLOSE','C_SUPP','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
            close C_SUPP;
         end if;
      else
         SQL_LIB.SET_MARK('CLOSE','C_SUPP_DEPT','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
         close C_SUPP_DEPT;
      end if;


      if I_items_approved = FALSE then
         if not SUP_BRACKET_COST_SQL.DELETE_DFLT_NEXT_BRACKET_LVL(O_error_message,
                                                                  I_supplier,
                                                                  I_dept,
                                                                  I_location,
                                                                  I_seq_no) then
            return FALSE;
         end if;

         if L_sbc_lvl = 'D' then

            FOR rec in C_UPPER_BRACKET1 LOOP
               if not SUP_BRACKET_COST_SQL.INSERT_BRACKET(O_error_message,
                                                          rec.bracket_value1,
                                                          rec.bracket_uom1,
                                                          rec.bracket_value2,
                                                          rec.default_bracket_ind,
                                                          I_supplier,
                                                          I_dept,
                                                          I_location,
                                                          NULL,
                                                          I_seq_no,
                                                          I_bracket_lvl,
                                                          NULL) then
                  return FALSE;
               end if;

               update item_supp_country_bracket_cost
                  set sup_dept_seq_no = rec.sup_dept_seq_no
                where sup_dept_seq_no = I_seq_no;

            END LOOP;

         elsif L_sbc_lvl = 'S' then
            FOR rec in C_UPPER_BRACKET2 LOOP
               if not SUP_BRACKET_COST_SQL.INSERT_BRACKET(O_error_message,
                                                          rec.bracket_value1,
                                                          rec.bracket_uom1,
                                                          rec.bracket_value2,
                                                          rec.default_bracket_ind,
                                                          I_supplier,
                                                          I_dept,
                                                          I_location,
                                                          NULL,
                                                          I_seq_no,
                                                          I_bracket_lvl,
                                                          NULL) then
                  return FALSE;
               end if;

               update item_supp_country_bracket_cost
                  set sup_dept_seq_no = rec.sup_dept_seq_no
                where sup_dept_seq_no = I_seq_no;

            END LOOP;
         end if;
      else
      --- create a cost change
         NEXT_COSTCHG_NUMBER(L_cost_change,
                              L_return_code,
                              O_error_message);

         if L_return_code = 'FALSE' then
            return FALSE;
         end if;

         if not SUP_BRACKET_COST_SQL.DELETE_DFLT_NEXT_BRACKET_LVL(O_error_message,
                                                                  I_supplier,
                                                                  I_dept,
                                                                  I_location,
                                                                  I_seq_no) then
            return FALSE;
         end if;

         if L_sbc_lvl = 'D' then
            FOR rec in C_UPPER_BRACKET1 LOOP
               if not SUP_BRACKET_COST_SQL.INSERT_BRACKET(O_error_message,
                                                          rec.bracket_value1,
                                                          rec.bracket_uom1,
                                                          rec.bracket_value2,
                                                          rec.default_bracket_ind,
                                                          I_supplier,
                                                          I_dept,
                                                          I_location,
                                                          L_cost_change,
                                                          I_seq_no,
                                                          I_bracket_lvl,
                                                          'Y') then
                  return FALSE;
               end if;

               update cost_susp_sup_detail_loc
                  set sup_dept_seq_no = rec.sup_dept_seq_no
                where cost_change     = L_cost_change;

            END LOOP;
         elsif L_sbc_lvl = 'S' then
            FOR rec in C_UPPER_BRACKET2 LOOP
               if not SUP_BRACKET_COST_SQL.INSERT_BRACKET(O_error_message,
                                                          rec.bracket_value1,
                                                          rec.bracket_uom1,
                                                          rec.bracket_value2,
                                                          rec.default_bracket_ind,
                                                          I_supplier,
                                                          I_dept,
                                                          I_location,
                                                          L_cost_change,
                                                          I_seq_no,
                                                          I_bracket_lvl,
                                                          'Y') then
                  return FALSE;
               end if;

               update cost_susp_sup_detail_loc
                  set sup_dept_seq_no = rec.sup_dept_seq_no
                where cost_change     = L_cost_change;

            END LOOP;
         end if;
      end if;
   elsif I_dept is NULL or I_location is NULL then
         SQL_LIB.SET_MARK('OPEN','C_SUPP','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
         open C_SUPP;
         SQL_LIB.SET_MARK('FETCH','C_SUPP','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
         fetch C_SUPP into L_sbc_lvl;
         if C_SUPP%NOTFOUND then
            SQL_LIB.SET_MARK('CLOSE','C_SUPP','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
            close C_SUPP;
            O_exists := FALSE;
            return TRUE;
         else
            SQL_LIB.SET_MARK('CLOSE','C_SUPP','SUP_BRACKET_COST', 'Supplier: '|| to_char(I_supplier));
            close C_SUPP;

            if I_items_approved = FALSE then
               if not SUP_BRACKET_COST_SQL.DELETE_DFLT_NEXT_BRACKET_LVL(O_error_message,
                                                                        I_supplier,
                                                                        I_dept,
                                                                        I_location,
                                                                        I_seq_no) then
                  return FALSE;
               end if;

               FOR rec in C_UPPER_BRACKET2 LOOP
                  if not SUP_BRACKET_COST_SQL.INSERT_BRACKET(O_error_message,
                                                             rec.bracket_value1,
                                                             rec.bracket_uom1,
                                                             rec.bracket_value2,
                                                             rec.default_bracket_ind,
                                                             I_supplier,
                                                             I_dept,
                                                             I_location,
                                                             NULL,
                                                             I_seq_no,
                                                             I_bracket_lvl,
                                                             NULL) then
                     return FALSE;
                  end if;

                  update item_supp_country_bracket_cost
                     set sup_dept_seq_no = rec.sup_dept_seq_no
                   where sup_dept_seq_no = I_seq_no;

               END LOOP;
            else
            --- create a cost change

               NEXT_COSTCHG_NUMBER(L_cost_change,
                                    L_return_code,
                                    O_error_message);

               if L_return_code = 'FALSE' then
                  return FALSE;
               end if;

               if not SUP_BRACKET_COST_SQL.DELETE_DFLT_NEXT_BRACKET_LVL(O_error_message,
                                                                        I_supplier,
                                                                        I_dept,
                                                                        I_location,
                                                                        I_seq_no) then
                  return FALSE;
               end if;

               FOR rec in C_UPPER_BRACKET2 LOOP
                  if not SUP_BRACKET_COST_SQL.INSERT_BRACKET(O_error_message,
                                                             rec.bracket_value1,
                                                             rec.bracket_uom1,
                                                             rec.bracket_value2,
                                                             rec.default_bracket_ind,
                                                             I_supplier,
                                                             I_dept,
                                                             I_location,
                                                             L_cost_change,
                                                             I_seq_no,
                                                             I_bracket_lvl,
                                                             'Y') then
                     return FALSE;
                  end if;

                  if I_dept is NULL and I_location is not NULL then
                     update cost_susp_sup_detail_loc
                        set sup_dept_seq_no = rec.sup_dept_seq_no
                      where cost_change     = L_cost_change;
                  elsif I_dept is not NULL and I_location is NULL then
                     update cost_susp_sup_detail
                        set sup_dept_seq_no = rec.sup_dept_seq_no
                      where cost_change = L_cost_change;
                  end if;

               END LOOP;
            end if;
         end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END DEFAULT_NEXT_BRACKET_LVL;
---------------------------------------------------------------------------------
FUNCTION DFLT_BKT_DUPS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists         IN OUT BOOLEAN,
                       O_cost_change    IN OUT COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                       I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(60) := 'SUP_BRACKET_COST_SQL.DFLT_BKT_DUPS';
   L_status         COST_SUSP_SUP_HEAD.STATUS%TYPE;
   L_cost_change    COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE;

   cursor C_DUP is
      select distinct cost_change
        from cost_susp_sup_detail
       where sup_dept_seq_no = I_seq_no
      union all
      select distinct cost_change
        from cost_susp_sup_detail_loc
       where sup_dept_seq_no = I_seq_no;

   cursor C_CC_HEAD is
      select status
        from cost_susp_sup_head
       where cost_change = L_cost_change
         and reason = 3;

BEGIN
   O_exists := FALSE;

   if I_seq_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_seq_no',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   FOR rec in C_DUP LOOP
      L_cost_change := rec.cost_change;
      SQL_LIB.SET_MARK('OPEN','C_CC_HEAD','COST_SUSP_SUP_HEAD','Cost Chg: '|| to_char(O_cost_change));
      open C_CC_HEAD;
      SQL_LIB.SET_MARK('FETCH','C_CC_HEAD','COST_SUSP_SUP_HEAD','Cost Chg: '|| to_char(O_cost_change));
      fetch C_CC_HEAD into L_status;
      if C_CC_HEAD%FOUND then
         if L_status in ('W', 'S', 'A', 'R') then
            O_cost_change := L_cost_change;
            O_exists := TRUE;
            close C_CC_HEAD;
            return TRUE;
         else
            O_exists := FALSE;
         end if;
      else
         O_exists := FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_CC_HEAD','COST_SUSP_SUP_HEAD','Cost Chg: '|| to_char(O_cost_change));
      close C_CC_HEAD;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

   return FALSE;
END DFLT_BKT_DUPS;
---------------------------------------------------------------------------------
END SUP_BRACKET_COST_SQL;
/
