
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY LC_ACTIVE_SQL AS
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_ACTIVITY(O_error_message  IN OUT VARCHAR2,
                         O_net_amount     IN OUT lc_activity.amount%TYPE,
                         O_open_amount    IN OUT lc_activity.amount%TYPE,
                         O_amendments     IN OUT lc_activity.amount%TYPE,
                         O_drawdowns      IN OUT lc_activity.amount%TYPE,
                         O_charges        IN OUT lc_activity.amount%TYPE,
                         I_amount         IN     lc_activity.amount%TYPE,
                         I_lc_ref_id      IN     lc_activity.lc_ref_id%TYPE)	
RETURN BOOLEAN IS

   L_program         VARCHAR2(40)                       := 'LC_ACTIVE_SQL.UPDATE_ACTIVITY';
   L_amount_act      LC_ACTIVITY.AMOUNT%TYPE            := 0;
   L_amount_lc       LC_ACTIVITY.AMOUNT%TYPE            := 0;
   L_total_amt       LC_ACTIVITY.AMOUNT%TYPE            := 0;
   L_currency_act    CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_act    CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_currency_lc     CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_lc     CURRENCY_RATES.EXCHANGE_RATE%TYPE;

   cursor C_GET_LC_CURR is 
      select currency_code,
             exchange_rate
        from lc_head
       where lc_ref_id = I_lc_ref_id;

   cursor C_LC_ACTIVE_AMEND is
      select NVL(amount, 0) amount,
             currency_code,
             exchange_rate
        from lc_activity
       where lc_ref_id = I_lc_ref_id
         and trans_code = 'A';

   cursor C_LC_ACTIVE_DRAW is
      select NVL(amount, 0) amount,
             currency_code,
             exchange_rate
        from lc_activity
       where lc_ref_id = I_lc_ref_id
         and trans_code = 'D';

   cursor C_LC_ACTIVE_CHARGES is
      select NVL(amount, 0) amount,
             currency_code,
             exchange_rate
        from lc_activity
       where lc_ref_id = I_lc_ref_id
         and trans_code = 'B';

BEGIN
   SQL_LIB.SET_MARK ('OPEN','C_GET_LC_CURR','LC_HEAD','lc ref id: '||to_char(I_lc_ref_id));
   open C_GET_LC_CURR;
   SQL_LIB.SET_MARK ('FETCH','C_GET_LC_CURR','LC_HEAD','lc ref id: '||to_char(I_lc_ref_id));
   fetch C_GET_LC_CURR into L_currency_lc,
                            L_exchange_lc;
   SQL_LIB.SET_MARK ('CLOSE','C_GET_LC_CURR','LC_HEAD','lc ref id: '||to_char(I_lc_ref_id));
   close C_GET_LC_CURR;
   ---
   for C_rec in C_LC_ACTIVE_AMEND loop
      L_amount_act   := C_rec.amount;
      L_currency_act := C_rec.currency_code;
      L_exchange_act := C_rec.exchange_rate;
      ---
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_amount_act,
                              L_currency_act,
                              L_currency_lc,
                              L_amount_lc,
                              'C',
                              NULL,
                              NULL,
                              L_exchange_act,
                              L_exchange_lc) = FALSE then
         return FALSE;
      end if;
      ---
      L_total_amt := L_total_amt + L_amount_lc;
   end loop;
   ---
   O_amendments := L_total_amt;
   ---
   L_total_amt  := 0;
   L_amount_act := 0;
   L_amount_lc  := 0;
   ---
   for C_rec in C_LC_ACTIVE_DRAW loop
      L_amount_act   := C_rec.amount;
      L_currency_act := C_rec.currency_code;
      L_exchange_act := C_rec.exchange_rate;
      ---
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_amount_act,
                              L_currency_act,
                              L_currency_lc,
                              L_amount_lc,
                              'C',
                              NULL,
                              NULL,
                              L_exchange_act,
                              L_exchange_lc) = FALSE then
         return FALSE;
      end if;
      ---
      L_total_amt := L_total_amt + L_amount_lc;
   end loop;
   ---
   O_drawdowns := L_total_amt;
   ---
   L_total_amt  := 0;
   L_amount_act := 0;
   L_amount_lc  := 0;
   ---
   for C_rec in C_LC_ACTIVE_CHARGES loop
      L_amount_act   := C_rec.amount;
      L_currency_act := C_rec.currency_code;
      L_exchange_act := C_rec.exchange_rate;
      ---
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_amount_act,
                              L_currency_act,
                              L_currency_lc,
                              L_amount_lc,
                              'C',
                              NULL,
                              NULL,
                              L_exchange_act,
                              L_exchange_lc) = FALSE then
         return FALSE;
      end if;
      ---
      L_total_amt := L_total_amt + L_amount_lc;
   end loop;
   ---
   O_charges     := L_total_amt;
   O_net_amount  := I_amount     + O_amendments;
   O_open_amount := O_net_amount - O_drawdowns;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END UPDATE_ACTIVITY;
-----------------------------------------------------------------------------------------------
FUNCTION ORDER_EXIST(O_error_message  IN OUT VARCHAR2,
                     O_exist          IN OUT BOOLEAN,
                     I_lc_ref_id      IN     lc_detail.lc_ref_id%TYPE,
                     I_order          IN     lc_detail.order_no%TYPE)	
RETURN BOOLEAN IS
   
   L_program      VARCHAR2(64) := 'LC_ACTIVE_SQL.ORDER_EXIST';
   L_exists       VARCHAR2(1);

   cursor C_LC_DETAIL is
      select 'x'
        from lc_detail
       where lc_ref_id = I_lc_ref_id
         and order_no  = I_order;

BEGIN

   SQL_LIB.SET_MARK ('OPEN', 'C_LC_DETAIL', 'lc_detail', 
                     'LC Ref ID: ' || to_char(I_lc_ref_id) ||
                     ' Order: ' || to_char(I_order));
   open C_LC_DETAIL;
   ---
   SQL_LIB.SET_MARK ('FETCH', 'C_LC_DETAIL', 'lc_detail',
                     'LC Ref ID: ' || to_char(I_lc_ref_id) ||
                     ' Order: ' || to_char(I_order));
   fetch C_LC_DETAIL into L_exists;
   if C_LC_DETAIL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG ('NO_LC_DETLS_EXIST',
                                              NULL,
                                              NULL,
                                              NULL);
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   SQL_LIB.SET_MARK ('CLOSE', 'C_LC_DETAIL', 'lc_detail',
                     'LC Ref ID: ' || to_char(I_lc_ref_id) ||
                     ' Order: ' || to_char(I_order));
   close C_LC_DETAIL;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END ORDER_EXIST;
--------------------------------------------------------------------------------------
FUNCTION ACTIVITY(O_error_message     IN OUT VARCHAR2,
                  I_lc_ref_id         IN     LC_HEAD.LC_REF_ID%TYPE,
                  I_amend_amount      IN     LC_ACTIVITY.AMOUNT%TYPE,
                  I_amend_no          IN     LC_ACTIVITY.TRANS_NO%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'LC_ACTIVE_SQL.ACTIVITY';
   L_vdate       DATE         := GET_VDATE;
   L_exists      VARCHAR2(1);
   L_order_no    LC_DETAIL.ORDER_NO%TYPE;
   L_amount      LC_DETAIL.COST%TYPE;
   L_qty         LC_DETAIL.QTY%TYPE;
   L_form_type   LC_HEAD.FORM_TYPE%TYPE;       
   L_long_amount LC_DETAIL.COST%TYPE;
   L_currency_code LC_HEAD.CURRENCY_CODE%TYPE;
   L_exchange_rate LC_HEAD.EXCHANGE_RATE%TYPE;

   cursor C_LC_ORDERS is
     select distinct ld.order_no,
            SUM(ld.cost*nvl(ld.qty, 1)) amount
       from lc_detail ld,
            lc_head lh
      where ld.lc_ref_id = I_lc_ref_id
        and lh.lc_ref_id = I_lc_ref_id
       group by order_no;

   cursor C_LC_HEAD is
     select currency_code,
            exchange_rate
       from lc_head
      where lc_ref_id = I_lc_ref_id;

BEGIN
   if I_lc_ref_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_LC',NULL,NULL,NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('OPEN','C_LC_HEAD','LC_HEAD','lc_ref_id: ' ||
                     to_char(I_lc_ref_id));
      open C_LC_HEAD;
      SQL_LIB.SET_MARK('FETCH','C_LC_HEAD','LC_HEAD','lc_ref_id: ' ||
                     to_char(I_lc_ref_id));
      fetch C_LC_HEAD into L_currency_code, L_exchange_rate;
      if C_LC_HEAD%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_LC',NULL,NULL,NULL);
         SQL_LIB.SET_MARK('CLOSE','C_LC_HEAD','LC_HEAD','lc_ref_id: ' ||
                     to_char(I_lc_ref_id));
         close C_LC_HEAD;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_LC_HEAD','LC_HEAD','lc_ref_id: ' ||
                     to_char(I_lc_ref_id));
      close C_LC_HEAD;
   end if;

   if I_amend_no is NULL then  -- Confirmation of LC--

      SQL_LIB.SET_MARK ('OPEN', 'C_LC_ORDERS', 'LC_DETAIL',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      for C_LC_ORDERS_REC in C_LC_ORDERS loop

         L_order_no    := C_LC_ORDERS_REC.order_no;
         L_amount      := C_LC_ORDERS_REC.amount;

         SQL_LIB.SET_MARK('INSERT',NULL,'LC_ACTIVITY',NULL);
         insert into lc_activity (lc_ref_id,
                                  order_no,
                                  invoice_no,
                                  trans_no,
                                  trans_code,
                                  amount,
                                  activity_date,
                                  currency_code,
                                  exchange_rate,
                                  comments)
                          values (I_lc_ref_id,
                                  L_order_no,
                                  NULL,
                                  NULL,
                                  'L',
                                  L_amount,
                                  L_vdate,
                                  L_currency_code,
                                  L_exchange_rate,
                                  NULL);
      end loop;

      --- if an open letter of credit is confirmed (no details)
      --- then write an lc_activity record with NULL for order_no
      if L_order_no is NULL then
         SQL_LIB.SET_MARK('INSERT',NULL,'LC_ACTIVITY',NULL);
         insert into lc_activity (lc_ref_id,
                                  trans_code,
                                  activity_date)
                          values (I_lc_ref_id,
                                  'L',
                                  L_vdate);
      end if;

   else

      SQL_LIB.SET_MARK('INSERT',NULL,'LC_ACTIVITY',NULL);
      insert into lc_activity (lc_ref_id,
                               trans_no,
                               trans_code,
                               amount,
                               activity_date,
                               currency_code,
                               exchange_rate)
                       values (I_lc_ref_id,
                               I_amend_no,
                               'A',
                               I_amend_amount,
                               L_vdate,
                               L_currency_code,
                               L_exchange_rate);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END ACTIVITY;
--------------------------------------------------------------------------------------
END LC_ACTIVE_SQL;
/


