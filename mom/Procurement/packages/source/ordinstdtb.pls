
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ORDER_INSTOCK_DATE_SQL AS
--------------------------------------------------------
FUNCTION GET_MAX_INSTOCK_DT(O_error_message             IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                            O_max_est_instock_date      IN OUT    ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE,
                            I_order_no                  IN        ORDLOC.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_max_instock_date       ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE;
   L_program                VARCHAR2(60) := 'ORDER_INSTOCK_DATE_SQL.GET_MAX_INSTOCK_DT';
   L_exist                  BOOLEAN;

   cursor C_PARTIAL_MAX_INSTOCK_DATE is
      select MAX(estimated_instock_date)
        from ordloc
       where order_no = I_order_no
         and qty_ordered - nvl(qty_received, 0) > 0;

  cursor C_FULL_RCVD_MAX_INSTOCK_DATE is
      select MAX(estimated_instock_date)
        from ordloc
       where order_no = I_order_no;
BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no: '||to_char(I_order_no),
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if ORDER_ATTRIB_SQL.PARTIAL_NONE_RCVD_ORD(O_error_message,
                                             L_exist,
                                             I_order_no) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   end if;       
	if L_exist then
      SQL_LIB.SET_MARK('OPEN', 'C_PARTIAL_MAX_INSTOCK_DATE', 'ORDLOC', 'ORDER_NO:'||to_char(I_order_no));
      open C_PARTIAL_MAX_INSTOCK_DATE;
      SQL_LIB.SET_MARK('FETCH', 'C_PARTIAL_MAX_INSTOCK_DATE', 'ORDLOC', 'ORDER_NO:'||to_char(I_order_no));
      fetch C_PARTIAL_MAX_INSTOCK_DATE into O_max_est_instock_date;
      SQL_LIB.SET_MARK('CLOSE', 'C_PARTIAL_MAX_INSTOCK_DATE', 'ORDLOC', 'ORDER_NO:'||to_char(I_order_no));
      close C_PARTIAL_MAX_INSTOCK_DATE;
   else
      SQL_LIB.SET_MARK('OPEN', 'C_FULL_RCVD_MAX_INSTOCK_DATE', 'ORDLOC', 'ORDER_NO:'||to_char(I_order_no));
      open C_FULL_RCVD_MAX_INSTOCK_DATE;
      SQL_LIB.SET_MARK('FETCH', 'C_FULL_RCVD_MAX_INSTOCK_DATE', 'ORDLOC', 'ORDER_NO:'||to_char(I_order_no));
      fetch C_FULL_RCVD_MAX_INSTOCK_DATE into O_max_est_instock_date;
      SQL_LIB.SET_MARK('CLOSE', 'C_FULL_RCVD_MAX_INSTOCK_DATE', 'ORDLOC', 'ORDER_NO:'||to_char(I_order_no));
      close C_FULL_RCVD_MAX_INSTOCK_DATE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END GET_MAX_INSTOCK_DT;
--------------------------------------------------------------------------------
FUNCTION UPD_INSTOCK_DT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no          IN       ORDLOC.ORDER_NO%TYPE,
                        I_item              IN       ITEM_MASTER.ITEM%TYPE,
                        I_location          IN       ORDLOC.LOCATION%TYPE,
                        I_supplier          IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE)

   RETURN BOOLEAN IS

   L_estimated_instock_date    ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE;
   RECORD_LOCKED               EXCEPTION;
   PRAGMA                      EXCEPTION_INIT(Record_Locked, -54);
   L_program                   VARCHAR2(60) := 'ORDER_INSTOCK_DATE_SQL.UPD_INSTOCK_DT';

   cursor C_LOCK_ORDLOC_REC is
      select 'X'
        from ordloc
       where order_no = I_order_no
         and item     = I_item
         and location = I_location
         for update nowait;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_order_no: '||to_char(I_order_no),
                                              L_program,
                                              NULL);
     return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item: '||to_char(I_item),
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location: '||to_char(I_location),
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ORDLOC_REC', 'ORDLOC', 'ORDER_NO:'||to_char(I_order_no)||'ITEM:'||I_item||'LOCATION:'||to_char(I_location)||'SUPPLIER:'||to_char(I_supplier));
   open C_LOCK_ORDLOC_REC;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ORDLOC_REC', 'ORDLOC', 'ORDER_NO:'||to_char(I_order_no)||'ITEM:'||I_item||'LOCATION:'||to_char(I_location)||'SUPPLIER:'||to_char(I_supplier));
   close C_LOCK_ORDLOC_REC;

   if ORDER_INSTOCK_DATE_SQL.GET_INSTOCK_DT(O_error_message,
                                            L_estimated_instock_date,
                                            I_order_no,
                                            I_item,
                                            I_location,
                                            I_supplier) = FALSE then
      return FALSE;
   end if;

   update ordloc
      set estimated_instock_date = L_estimated_instock_date
    where order_no               = I_order_no
      and item                   = I_item
      and location               = I_location;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ORDLOC',
                                            'I_order_no: '||to_char(I_order_no)||'I_item: '||to_char(I_item)||'I_location: '||to_char(I_location)||'I_supplier: '||to_char(I_supplier),
                                             I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END UPD_INSTOCK_DT;
--------------------------------------------------------
FUNCTION GET_INSTOCK_DT(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_est_instock_date     IN OUT   ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE,
                        I_order_no             IN       ORDLOC.ORDER_NO%TYPE,
                        I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                        I_location             IN       ORDLOC.LOCATION%TYPE,
                        I_supplier             IN       ORDHEAD.SUPPLIER%TYPE)

   RETURN BOOLEAN AS

   cursor C_ORD_DETAILS is
    select d.inbound_handling_days,
           b.written_date,
           nvl(b.orig_approval_date, b.written_date) mydate,
           c.origin_country_id,
           I_supplier inbound_transit_times,
           c.item
      from ordhead b,
           ordsku c,
           item_loc d
     where b.order_no = I_order_no
       and b.order_no = c.order_no
       and c.item = d.item
       and c.item = I_item
       and d.loc  = I_location;

   L_ord_details          C_ORD_DETAILS%ROWTYPE;
   L_loc_type             ITEM_LOC.LOC_TYPE%TYPE;
   L_transit_times        TRANSIT_TIMES%ROWTYPE;
   L_item_supp_country    ITEM_SUPP_COUNTRY%ROWTYPE;
   L_program              VARCHAR2(60) := 'ORDER_INSTOCK_DATE.GET_INSTOCK_DT';
   L_exists               BOOLEAN;

BEGIN

   if I_item is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_item: '||to_char(I_item),
                                              L_program,
                                              NULL);
      return FALSE;
   end if;

   if I_order_no is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_order_no: '||to_char(I_order_no),
                                              L_program,
                                              NULL);
      return FALSE;
   end if;

   if I_location is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_location: '||to_char(I_location),
                                              L_program,
                                              NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier: '||to_char(I_supplier),
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_loc_type,
                                   I_location) = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_ORD_DETAILS','ORDLOC '||'ORDHEAD', NULL);
   open C_ORD_DETAILS;
   SQL_LIB.SET_MARK('FETCH','C_ORD_DETAILS','ORDLOC '||'ORDHEAD', NULL);
   fetch C_ORD_DETAILS into L_ord_details;
   SQL_LIB.SET_MARK('CLOSE','C_ORD_DETAILS','ORDLOC '||'ORDHEAD', NULL);
   close C_ORD_DETAILS;

   if TRANSIT_TIMES_SQL.GET_ROW(O_error_message,
                                L_exists,
                                L_transit_times,
                                I_supplier,
                                I_location,
                                L_ord_details.item) = FALSE then
      return FALSE;
   end if;

   if ITEM_SUPP_COUNTRY_SQL.GET_ROW(O_error_message,
                                   L_exists,
                                   L_item_supp_country,
                                   I_item,
                                   I_supplier,
                                   L_ord_details.origin_country_id) = FALSE then
     return FALSE;
   end if;

   if L_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('NO_ITEM_RECORD_FOUND',
                                            'L_item_supp_country.item: '||to_char(L_item_supp_country.item),
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   O_est_instock_date := L_ord_details.mydate +
                         nvl(L_item_supp_country.lead_time,0) +
                         nvl(L_transit_times.transit_time,0) +
                         nvl(L_ord_details.inbound_handling_days, 0);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END GET_INSTOCK_DT;
--------------------------------------------------------
FUNCTION MASS_UPD_INSTOCK_DT(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no       IN       ORDLOC.ORDER_NO%TYPE,
                             I_supplier       IN       ORDHEAD.SUPPLIER%TYPE,
                             I_date           IN       DATE)
   RETURN BOOLEAN AS

   cursor C_LOCK_MASS_ORDLOC_REC is
   select item, location
     from ordloc
    where order_no = I_order_no
      and qty_ordered - nvl(qty_received, 0) > 0
      for update nowait;

   L_estimated_instock_date      ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE;
   L_program                     VARCHAR2(60) := 'ORDER_INSTOCK_DATE.MSS_UPD_INSTOCK_DT';
   RECORD_LOCKED                 EXCEPTION;
   PRAGMA                        EXCEPTION_INIT(Record_Locked, -54);

BEGIN

  if I_order_no is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_order_no: '||to_char(I_order_no),
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

     if I_supplier is NULL then
        O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                              'I_supplier: '||to_char(I_supplier),
                                               L_program,
                                               NULL);
      return FALSE;
   end if;

   for C_REC in C_LOCK_MASS_ORDLOC_REC LOOP
      if I_date is NULL then
         if ORDER_INSTOCK_DATE_SQL.GET_INSTOCK_DT(O_error_message,
                                                  L_estimated_instock_date,
                                                  I_order_no,
                                                  C_REC.item,
                                                  C_REC.location,
                                                  I_supplier) = FALSE then
            return FALSE;
         end if;
      end if;

   update ordloc
      set estimated_instock_date = nvl(I_date, L_estimated_instock_date)
      where current of C_LOCK_MASS_ORDLOC_REC;

      exit when C_REC.item is NULL;
   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ORDLOC',
                                            'I_order_no: '||to_char(I_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      o_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END MASS_UPD_INSTOCK_DT;

END ORDER_INSTOCK_DATE_SQL;
/

