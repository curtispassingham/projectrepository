CREATE OR REPLACE PACKAGE CORESVC_FULFILORD_PO AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------
--Name     : CREATE_PO
--Function : This function will kickoff the core logic for validating and processing the
--           Customer Order Create message for Drop Ship and Multi-site POs.  This will
--           create a purchase order related to a customer order fulfillment request.
--           This will also return a collection of ORDCUST_IDs created in RMS in status
--           'C' (order fully created), 'P' (order partially created) or 'X' (order
--           fully rejected).
----------------------------------------------------------------------------------------
FUNCTION CREATE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_ordcust_ids     IN OUT   ID_TBL,
                   I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                   I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Name     : CANCEL_PO
-- Function : This function contains the core logic to cancel a customer order PO (Drop
--            Ship and Multi-site) related to a customer order fulfillment cancellation
--            request.
----------------------------------------------------------------------------------------
FUNCTION CANCEL_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                   I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
END CORESVC_FULFILORD_PO;
/