CREATE OR REPLACE PACKAGE STORE_GRADE_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name: FINISH_ORDER
-- Purpose:       This function will distribute an item by store grade on the basis of quantity, 
--                percentage or ratio.It will update the orderloc_wksht table based on  distribution 
--                information present in the Store_grade_dist_temp table.
---------------------------------------------------------------------------------------------

FUNCTION FINISH_ORDER( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                       I_dist_uom_type   IN       ORDLOC_WKSHT.UOP%TYPE,
                       I_dist_uom        IN       ORDLOC_WKSHT.UOP%TYPE,
                       I_purch_uom       IN       ORDLOC_WKSHT.UOP%TYPE,
                       I_distribute_by   IN       VARCHAR2)
RETURN BOOLEAN;
   
END STORE_GRADE_SQL; 
/