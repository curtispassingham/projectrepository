CREATE OR REPLACE PACKAGE PC_ATTRIB_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------------
-- Function Name: GET_DESC
-- Purpose  : This function retrieves the price change description
--                for a given price change/cost change number and type.
--                The valid types are :
--                   'I'   - price change
--                   'G'   - price change by merch/org
--                   'SUP' - cost/price change by supplier
--                   'ITEM' - cost/price change by Item
-- Calls    : <none>
-- Created  : 05-SEP-96 By Julian Aggerbeck
-------------------------------------------------------------------------------------------------
FUNCTION GET_DESC(I_cost_change     IN       NUMBER,
                  I_type            IN       VARCHAR2,
                  O_description     IN OUT   COST_SUSP_SUP_HEAD.COST_CHANGE_DESC%TYPE,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: GET_HEADER_INFO
-- Purpose  : This function retrieves the information from the header table
--                for a given price change/cost change number and type.
--                The valid types are :
--                   'I'   - price change
--                   'G'   - price change by merch/org
--                   'SUP' - cost/price change by supplier
--                   'ITEM' - cost/price change by Item
-- Calls    : <none>
-- Created  : 11-SEP-96 By Julian Aggerbeck
-------------------------------------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(I_cost_change     IN       NUMBER,
                         I_type            IN       VARCHAR2,
                         O_description     IN OUT   COST_SUSP_SUP_HEAD.COST_CHANGE_DESC%TYPE,
                         O_reason          IN OUT   COST_SUSP_SUP_HEAD.REASON%TYPE,
                         O_status          IN OUT   COST_SUSP_SUP_HEAD.STATUS%TYPE,
                         O_active_date     IN OUT   COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE, 
                         O_approval_date   IN OUT   COST_SUSP_SUP_HEAD.APPROVAL_DATE%TYPE,
                         O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: GET_REASON_DESC
-- Purpose  : This function retrieves reason description
--                for a given reason number and type.
--                The valid types are :
--                   'I'   - price change
--                   'G'   - price change by merch/org
--                   'SUP' - cost/price change by supplier
--                   'ITEM' - cost/price change by Item
-- Calls    : <none>
-- Created  : 05-SEP-96 By Julian Aggerbeck
-------------------------------------------------------------------------------------------------
FUNCTION GET_REASON_DESC(I_reason          IN       COST_CHG_REASON.REASON%TYPE,
                         I_type            IN       VARCHAR2,
                         O_description     IN OUT   COST_CHG_REASON_TL.REASON_DESC%TYPE,
                         O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE) 
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- FUNCTION NAME:  GET_COST_CHG_ORG
-- PURPOSE:        Take the cost_change and return cost_change_origin
--                 from the cost_susp_sup_head table.
-- CALLS:          None
-- INPUT VALUES:   I_price_change
-- OUTPUT VALUES:  O_cost_chg_org, O_error_message
-- AUTHORED BY:    Chad Whipple
-------------------------------------------------------------------------------------------------
   FUNCTION GET_COST_CHG_ORG(I_cost_change  IN  NUMBER,
                             O_cost_chg_org  IN OUT  VARCHAR2,
                             O_error_message  IN OUT  VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- FUNCTION NAME: MERGE_COST_CHG_REASON_TL
-- PURPOSE:   This function will modify or insert a record in the COST_CHG_REASON_TL table
--            given the Cost Change Reason and the Input Language
-------------------------------------------------------------------------------------------------
FUNCTION MERGE_COST_CHG_REASON_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_reason          IN       COST_CHG_REASON_TL.REASON%TYPE,
                                  I_reason_desc     IN       COST_CHG_REASON_TL.REASON_DESC%TYPE,
                                  I_lang            IN       COST_CHG_REASON_TL.LANG%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- FUNCTION NAME: DEL_COST_CHG_REASON_TL
-- PURPOSE:   This function will delete a record in the COST_CHG_REASON_TL table
-------------------------------------------------------------------------------------------------
FUNCTION DEL_COST_CHG_REASON_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_reason          IN       COST_CHG_REASON_TL.REASON%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
END;
/

