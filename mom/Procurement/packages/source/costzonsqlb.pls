CREATE OR REPLACE PACKAGE BODY COST_ZONE_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION INSERT_COST_ZONE_FOR_VWH(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_zone_group               IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                                  I_zone                     IN       COST_ZONE.ZONE_ID%TYPE,
                                  I_pwh                      IN       WH.PHYSICAL_WH%TYPE,
                                  I_primary_discharge_port   IN       COST_ZONE_GROUP_LOC.PRIMARY_DISCHARGE_PORT%TYPE DEFAULT NULL)
   return BOOLEAN IS

   L_program_name   VARCHAR2(50) := 'COST_ZONE_SQL.INSERT_COST_ZONE_FOR_VWH';
   L_cost_level     COST_ZONE_GROUP.COST_LEVEL%TYPE;

BEGIN

   if COST_ZONE_ATTRIB_SQL.GET_COST_LEVEL(I_zone_group,
                                          L_cost_level,
                                          O_error_message)= FALSE then
      return FALSE;
   end if;
   ---
   if L_cost_level = 'L' then
      insert into cost_zone_group_loc(zone_group_id,
                                      zone_id,
                                      loc_type,
                                      location,
                                      primary_discharge_port)
                               select I_zone_group,
                                      I_pwh,
                                      'W',
                                      wh.wh,
                                      I_primary_discharge_port
                                 from wh
                                where physical_wh      = I_pwh
                                  and stockholding_ind = 'Y'
                                  and not exists(select 'x'
                                                   from cost_zone_group_loc
                                                  where location      = wh.wh
                                                    and zone_id       = nvl(I_zone,I_pwh)
                                                    and zone_group_id = I_zone_group);

   elsif L_cost_level = 'Z' then
      insert into cost_zone_group_loc(zone_group_id,
                                      zone_id,
                                      loc_type,
                                      location,
                                      primary_discharge_port)
                               select I_zone_group,
                                      I_zone,
                                      'W',
                                      wh.wh,
                                      I_primary_discharge_port
                                 from wh
                                where physical_wh      = I_pwh
                                  and stockholding_ind = 'Y'
                                  and not exists(select 'x'
                                                   from cost_zone_group_loc
                                                  where location      = wh.wh
                                                    and zone_id       = I_zone
                                                    and zone_group_id = I_zone_group);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_COST_ZONE_FOR_VWH;
--------------------------------------------------------------------------------
FUNCTION DELETE_COST_ZONE_FOR_VWH(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_records_deleted   IN OUT   BOOLEAN,
                                  I_zone_group        IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                                  I_zone              IN       COST_ZONE.ZONE_ID%TYPE,
                                  I_pwh               IN       WH.PHYSICAL_WH%TYPE)
   return BOOLEAN IS

   L_program_name   VARCHAR2(50) := 'COST_ZONE_SQL.DELETE_COST_ZONE_FOR_VWH';
   L_cost_level     COST_ZONE_GROUP.COST_LEVEL%TYPE;

   cursor C_LOCK_ZONE_ID is
      select 'x'
        from cost_zone_group_loc cz
       where loc_type = 'W'
         and(zone_id    = I_pwh
             or zone_id = I_zone)
         and zone_group_id = I_zone_group
         and exists (select wh
                       from wh
                      where wh.wh            = cz.location
                        and wh.physical_wh   = nvl(I_pwh, physical_wh)
                        and stockholding_ind = 'Y')
         for UPDATE NOWAIT;

BEGIN
   O_records_deleted := FALSE;
   ---
   if COST_ZONE_ATTRIB_SQL.GET_COST_LEVEL(I_zone_group,
                                          L_cost_level,
                                          O_error_message)= FALSE then
      return FALSE;
   end if;
   ---
   if L_cost_level = 'L' then
      open C_LOCK_ZONE_ID;
      close C_LOCK_ZONE_ID;
      ---
      delete
        from cost_zone_group_loc cz
       where loc_type = 'W'
         and zone_id  = I_pwh
         and zone_group_id = I_zone_group
         and exists (select wh
                       from wh
                      where wh.wh            = cz.location
                        and wh.physical_wh   = I_pwh
                        and stockholding_ind = 'Y');
      if SQL%FOUND then
         O_records_deleted := TRUE;
      end if;
   ---
   elsif L_cost_level = 'Z' then
      open C_LOCK_ZONE_ID;
      close C_LOCK_ZONE_ID;
      ---
      delete
        from cost_zone_group_loc cz
       where loc_type = 'W'
         and zone_id  = I_zone
         and zone_group_id = I_zone_group
         and exists (select wh
                       from wh
                      where wh.wh            = cz.location
                        and wh.physical_wh   = nvl(I_pwh, physical_wh)
                        and stockholding_ind = 'Y');
      if SQL%FOUND then
         O_records_deleted := TRUE;
      end if;
   ---
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_COST_ZONE_FOR_VWH;
---------------------------------------------------------------------------------------------
FUNCTION COST_ZONE_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_zone_group      IN       COST_ZONE_GROUP_LOC.ZONE_GROUP_ID%TYPE,
                         I_zone_id         IN       COST_ZONE_GROUP_LOC.ZONE_ID%TYPE DEFAULT NULL,
                         I_location        IN       COST_ZONE_GROUP_LOC.LOCATION%TYPE)
   return BOOLEAN IS

   L_program_name   VARCHAR2(50) := 'COST_ZONE_SQL.COST_ZONE_EXIST';
   L_dummy          VARCHAR2(1)  := 'N';
   ---

   cursor C_LOC_EXIST is
      select 'Y'
        from cost_zone_group_loc
       where zone_group_id = I_zone_group
         and zone_id = NVL(I_zone_id, zone_id)
         and location = I_location;

BEGIN

   open  C_LOC_EXIST;
   fetch C_LOC_EXIST into L_dummy;
   close C_LOC_EXIST;

   ---
   O_exists := FALSE;
   ---

   if L_dummy = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END COST_ZONE_EXIST;
---------------------------------------------------------------------------------------------
FUNCTION GET_COST_ZONE_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_zone_desc       IN OUT   COST_ZONE.DESCRIPTION%TYPE,
                          O_exists          IN OUT   BOOLEAN,
                          I_zone_id         IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE)
   return BOOLEAN IS

   L_program_name   VARCHAR2(50) := 'COST_ZONE_SQL.GET_COST_ZONE_ID';

   cursor C_GET_COST_ZONE is
      select distinct cz.description
        from cost_zone cz
       where cz.zone_id = NVL(I_zone_id,cz.zone_id)
         and NOT EXISTS (select 'X'
                           from daily_purge dp
                          where dp.table_name = 'COST_ZONE_GROUP'
                            and dp.key_value = cz.zone_group_id
                            and dp.delete_type = 'D');

BEGIN

   open C_GET_COST_ZONE;
   fetch C_GET_COST_ZONE INTO O_zone_desc;

   if C_GET_COST_ZONE%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_GET_COST_ZONE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END GET_COST_ZONE_ID;
---------------------------------------------------------------------------------------------
FUNCTION INSERT_COST_ZONE_LOC(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_zone_group               IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                              I_zone                     IN       COST_ZONE.ZONE_ID%TYPE,
                              I_location                 IN       COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                              I_loc_type                 IN       COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                              I_primary_discharge_port   IN       COST_ZONE_GROUP_LOC.PRIMARY_DISCHARGE_PORT%TYPE DEFAULT NULL)
   return BOOLEAN IS

   L_program_name   VARCHAR2(50) := 'COST_ZONE_SQL.INSERT_COST_ZONE_LOC';

BEGIN

   ---
   if I_zone_group is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_zone_group',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_zone is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_zone',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   ---

   insert into cost_zone_group_loc(zone_group_id,
                                   location,
                                   loc_type,
                                   zone_id,
                                   primary_discharge_port)
                           values (I_zone_group,
                                   I_location,
                                   I_loc_type,
                                   I_zone,
                                   I_primary_discharge_port);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_COST_ZONE_LOC;
--------------------------------------------------------------------------------
FUNCTION DELETE_COST_ZONE_LOC(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_zone_group               IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                              I_zone                     IN       COST_ZONE.ZONE_ID%TYPE,
                              I_location                 IN       COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                              I_loc_type                 IN       COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE)
   return BOOLEAN IS

   L_program_name   VARCHAR2(50) := 'COST_ZONE_SQL.DELETE_COST_ZONE_LOC';

   cursor C_LOCK_ZONE_ID is
      select 'x'
        from cost_zone_group_loc 
       where zone_group_id = I_zone_group
         and zone_id = I_zone
         and loc_type = I_loc_type
         and location = I_location
         for update NOWAIT;

BEGIN

   ---
   if I_zone_group is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_zone_group',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_zone is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_zone',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;
   ---

   open C_LOCK_ZONE_ID;
   close C_LOCK_ZONE_ID;

   delete from cost_zone_group_loc
       where zone_group_id = I_zone_group
         and zone_id = I_zone
         and loc_type = I_loc_type
         and location = I_location;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_COST_ZONE_LOC;

---------------------------------------------------------------------------------------------

FUNCTION DISCHARGE_PORT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   BOOLEAN,
                               O_outloc_desc     IN OUT   OUTLOC.OUTLOC_DESC%TYPE,
                               I_outloc_id       IN       OUTLOC.OUTLOC_ID%TYPE)
   return BOOLEAN IS

   L_program_name   VARCHAR2(250) := 'COST_ZONE_SQL.DISCHARGE_PORT_EXISTS';
   L_exists         VARCHAR2(1)   := 'N';
   ---

   cursor C_PORT_ID_EXISTS is
      select 'Y',
             outloc_desc
        from outloc
       where outloc_type = 'DP'
         and outloc_id = I_outloc_id;

BEGIN

   ---
   if I_outloc_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_outloc_id',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;

   open  C_PORT_ID_EXISTS;
   fetch C_PORT_ID_EXISTS into L_exists,
                               O_outloc_desc;
   close C_PORT_ID_EXISTS;

   ---
   O_exists := FALSE;
   ---

   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program_name,
                                           to_char(SQLCODE));
      return FALSE;
END DISCHARGE_PORT_EXISTS;
---------------------------------------------------------------------------------------

FUNCTION GET_PRIM_DISCHRG_PORT(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_primary_discharge_port   IN OUT   COST_ZONE_GROUP_LOC.PRIMARY_DISCHARGE_PORT%TYPE,
                               I_zone_group_id            IN OUT   COST_ZONE_GROUP_LOC.ZONE_GROUP_ID%TYPE,
                               I_zone_id                  IN       COST_ZONE_GROUP_LOC.ZONE_ID%TYPE)
   return BOOLEAN IS

   L_program_name   VARCHAR2(250) := 'COST_ZONE_SQL.GET_PRIM_DISCHRG_PORT';

   cursor C_GET_DISCHRG_PORT is
      select primary_discharge_port
        from cost_zone_group_loc
       where zone_group_id = I_zone_group_id
         and zone_id = I_zone_id;

BEGIN

   -- No Validation is needed. Primary Discharge Port is Not mandatory.

   open C_GET_DISCHRG_PORT;
   fetch C_GET_DISCHRG_PORT into O_primary_discharge_port;
   close C_GET_DISCHRG_PORT;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program_name,
                                           to_char(SQLCODE));
      return FALSE;
END;

----------------------------------------------------------------------------------------
FUNCTION GET_FIRST_COST_ZONE_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_zone_id         IN OUT   COST_ZONE.ZONE_ID%TYPE,
                                O_zone_desc       IN OUT   COST_ZONE.DESCRIPTION%TYPE,
                                I_zone_group      IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                                I_location        IN       COST_ZONE_GROUP_LOC.LOCATION%TYPE)
   return BOOLEAN IS

   L_program_name   VARCHAR2(250) := 'COST_ZONE_SQL.GET_FIRST_COST_ZONE_ID';

   cursor C_GET_ZONE_ID_GROUP_LOC is
      select cz.zone_id,
             cz.description 
        from cost_zone cz,
             cost_zone_group_loc czgl
       where cz.zone_id         = czgl.zone_id
         and cz.zone_group_id   = czgl.zone_group_id
         and czgl.zone_group_id = I_zone_group
         and czgl.location      = I_location;

   cursor C_GET_ZONE_ID_GROUP is
      select cz.zone_id,
             cz.description
        from cost_zone cz
       where cz.zone_group_id   = I_zone_group
         and cz.base_cost_ind   = 'Y';

   cursor C_GET_ZONE_ID_LOC is
      select cz.zone_id,
             cz.description
        from cost_zone cz,
             cost_zone_group_loc czgl
       where cz.zone_id         = czgl.zone_id
         and cz.zone_group_id   = czgl.zone_group_id
         and czgl.location      = I_location
         and rownum             = 1;

   cursor C_GET_ANY_ZONE_ID is
      select zone_id,
             description
        from cost_zone
       where rownum = 1;

BEGIN

   if I_location is NOT NULL and I_zone_group is NOT NULL then
      open C_GET_ZONE_ID_GROUP_LOC;
      fetch C_GET_ZONE_ID_GROUP_LOC into O_zone_id,
                                         O_zone_desc;
      close C_GET_ZONE_ID_GROUP_LOC;

   elsif I_location is NULL and I_zone_group is NOT NULL then 
      open C_GET_ZONE_ID_GROUP;
      fetch C_GET_ZONE_ID_GROUP into O_zone_id,
                                     O_zone_desc;
      close C_GET_ZONE_ID_GROUP;

   elsif I_zone_group is NULL and I_location is NOT NULL then
      open C_GET_ZONE_ID_LOC;
      fetch C_GET_ZONE_ID_LOC into O_zone_id,
                                   O_zone_desc;
      close C_GET_ZONE_ID_LOC;
   end if;

   if O_zone_id is NULL then 
      open C_GET_ANY_ZONE_ID;
      fetch C_GET_ANY_ZONE_ID into O_zone_id,
                                   O_zone_desc;
      close C_GET_ANY_ZONE_ID;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END;

----------------------------------------------------------------------------------------
FUNCTION DELETE_COST_ZONE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_zone_group_id   IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                          I_zone_id         IN       COST_ZONE_GROUP_LOC.ZONE_ID%TYPE)
   return BOOLEAN IS

   L_program_name   VARCHAR2(250) := 'COST_ZONE_SQL.DELETE_COST_ZONE';
   L_item          item_exp_head.item%TYPE;
   L_supplier      item_exp_head.supplier%TYPE;
   L_item_exp_type item_exp_head.item_exp_type%TYPE;
   L_item_exp_seq  item_exp_head.item_exp_seq%TYPE;
   L_exists        VARCHAR2(1);
   L_table         VARCHAR2(20);
   Record_Locked        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   cursor C_ORDLOC_EXP_EXISTS is
     select 1
       from ordloc_exp oe,
            cost_zone_group_loc czgl,
            item_master im
      where oe.location           = czgl.location
        and oe.loc_type           = czgl.loc_type
        and oe.item               = im.item
        and im.cost_zone_group_id = czgl.zone_group_id
        and czgl.zone_group_id    = I_zone_group_id
        and czgl.zone_id          = I_zone_id
        and rownum =1;
   
   cursor C_ITEM_EXP_DETAIL is
      select 'Y'
        from item_exp_detail ted
       where exists( select 1
                       from item_exp_head teh
                      where teh.item = ted.item
                        and teh.supplier = ted.supplier
                        and teh.item_exp_type = ted.item_exp_type
                        and teh.item_exp_seq = ted.item_exp_seq
                        and teh.zone_group_id = I_zone_group_id
                        and teh.zone_id = I_zone_id
                        and rownum=1)
         for update of item nowait;

   cursor C_ITEM_EXP_HEAD is
      select 'Y'
        from item_exp_head
       where zone_group_id = I_zone_group_id
         and zone_id = I_zone_id
         for update of zone_group_id nowait;

   cursor C_LOCS_LOCK is
      select 'Y'
        from cost_zone_group_loc
       where zone_group_id = I_zone_group_id
         and zone_id = I_zone_id
         for update of zone_group_id nowait;

BEGIN

   open C_ORDLOC_EXP_EXISTS;
   fetch C_ORDLOC_EXP_EXISTS into L_exists;
   if C_ORDLOC_EXP_EXISTS%FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('ORDLOC_EXP_EXISTS',
                                            I_zone_id,
                                            I_zone_group_id,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'ITEM_EXP_DETAIL';
   open C_ITEM_EXP_DETAIL;
   close C_ITEM_EXP_DETAIL;
   ---
   delete from item_exp_detail ted
      where  exists( select 1
                       from item_exp_head teh
                      where teh.item = ted.item
                        and teh.supplier = ted.supplier
                        and teh.item_exp_type = ted.item_exp_type
                        and teh.item_exp_seq = ted.item_exp_seq
                        and teh.zone_group_id = I_zone_group_id
                        and teh.zone_id = I_zone_id);

   ---
   L_table := 'ITEM_EXP_HEAD';
   open C_ITEM_EXP_HEAD;
   close C_ITEM_EXP_HEAD;
   ---
   delete from item_exp_head
      where zone_group_id = I_zone_group_id
        and zone_id = I_zone_id;
   ---
   L_table := 'COST_ZONE_GROUP_LOC';
   open C_LOCS_LOCK;
   close C_LOCS_LOCK;
   ---
   delete from cost_zone_group_loc
      where zone_group_id = I_zone_group_id
        and zone_id       = I_zone_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END;
----------------------------------------------------------------------------------------
FUNCTION INSERT_COSTZONE_LOC(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_zone_group_id           IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                             I_zone_group_desc         IN       COST_ZONE_GROUP.DESCRIPTION%TYPE,
                             I_currency_code           IN       COST_ZONE.CURRENCY_CODE%TYPE,
                             I_like_zone_group_id      IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE)
   return BOOLEAN IS

   L_program_name   VARCHAR2(250) := 'COST_ZONE_SQL.INSERT_COSTZONE_LOC';
   L_cost_level     COST_ZONE_GROUP.COST_LEVEL%TYPE;
   L_store_default  STORE.STORE%TYPE;
   L_wh_default     WH.WH%TYPE;
   L_exists         VARCHAR2(1) := 'N';
   L_error_message  RTK_ERRORS.RTK_TEXT%TYPE; 

   cursor C_COST_LEVEL is
      select cost_level
        from cost_zone_group
       where zone_group_id = I_zone_group_id;

   cursor C_GET_ST_DEFAULT is
      select store 
        from store 
       where rownum = 1; 

   cursor C_GET_WH_DEFAULT is
      select wh
        from wh 
       where wh = physical_wh
         and rownum = 1; 

   cursor C_COST_ZONE is
      select 'Y' 
        from cost_zone
       where zone_group_id = I_zone_group_id
         and rownum = 1;


BEGIN
   open C_COST_LEVEL;
   fetch C_COST_LEVEL into L_cost_level;
   close C_COST_LEVEL;
   ---
   if L_cost_level = 'C' then
      open C_COST_ZONE;
      fetch C_COST_ZONE into L_exists;
      close C_COST_ZONE;
---
      if L_exists = 'N' then

         insert into cost_zone(zone_group_id,
                               zone_id,
                               description,
                               currency_code,
                               base_cost_ind)
                       values (I_zone_group_id,
                               I_zone_group_id,
                               I_zone_group_desc,
                               I_currency_code,
                               'Y');
         ---
         insert into cost_zone_group_loc(zone_group_id,
                                         location,
                                         loc_type,
                                         zone_id)
                                  select I_zone_group_id,
                                         store,
                                         'S',
                                         I_zone_group_id
                                    from store;
         ---
         insert into cost_zone_group_loc(zone_group_id,
                                         location,
                                         loc_type,
                                         zone_id)
                                  select I_zone_group_id,
                                          wh,
                                          'W',
                                          I_zone_group_id
                                     from wh;
      end if;
   elsif L_cost_level = 'L' then

      open C_COST_ZONE;
      fetch C_COST_ZONE into L_exists;
      close C_COST_ZONE;

      if L_exists = 'N' then

         insert into cost_zone(zone_group_id,
                               zone_id,
                               description,
                               currency_code,
                               base_cost_ind)
                        select I_zone_group_id,
                               store,
                               store_name,
                               currency_code,
                               'N'
                          from store;
 
         insert into cost_zone(zone_group_id,
                               zone_id,
                               description,
                               currency_code,
                               base_cost_ind)
                        select I_zone_group_id,
                               wh,
                               wh_name,
                               currency_code,
                               'N'
                          from wh
                         where wh = physical_wh;

         insert into cost_zone_group_loc(zone_group_id,
                                         location,
                                         loc_type,
                                         zone_id)
                                  select I_zone_group_id,
                                         store,
                                         'S',
                                         store
                                    from store;

         insert into cost_zone_group_loc(zone_group_id,
                                        location,
                                        loc_type,
                                        zone_id)
                                 select I_zone_group_id,
                                        wh,
                                        'W',
                                        physical_wh
                                   from wh;

         open C_GET_ST_DEFAULT;
         fetch C_GET_ST_DEFAULT into L_store_default;

         if C_GET_ST_DEFAULT%NOTFOUND then
            open C_GET_WH_DEFAULT;
            fetch C_GET_WH_DEFAULT into L_wh_default;

            update cost_zone
               set base_cost_ind = 'Y'
             where zone_id = L_wh_default;

            close C_GET_WH_DEFAULT;
         else

            update cost_zone
               set base_cost_ind = 'Y'
             where zone_id = L_store_default;
         end if;
      
         close C_GET_ST_DEFAULT;
      end if;
      ---
   elsif L_cost_level = 'Z'  and I_like_zone_group_id is NOT NULL then

      insert into cost_zone(zone_group_id,
                            zone_id,
                            description,
                            currency_code,
                            base_cost_ind)
                     select I_zone_group_id,
                            zone_id,
                            description,
                            currency_code,
                            base_cost_ind
                       from cost_zone
                      where zone_group_id = I_like_zone_group_id;   

      insert into cost_zone_group_loc(zone_group_id,
                                      location,
                                      loc_type,
                                      zone_id)
                               select I_zone_group_id,
                                      location,
                                      loc_type,
                                      zone_id
                                 from cost_zone_group_loc
                                where zone_group_id = I_like_zone_group_id; 
  
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END;
----------------------------------------------------------------------------------------
END COST_ZONE_SQL;
/