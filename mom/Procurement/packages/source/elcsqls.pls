CREATE OR REPLACE PACKAGE ELC_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name:  CHECK_DELETE_COMP
--Purpose      :  Check if the given component is currently being referenced in a 
--                different module. 
-------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_COMP(O_error_message IN OUT VARCHAR2,
                           O_exists        IN OUT BOOLEAN,
                           I_comp_id       IN     ELC_COMP.COMP_ID%TYPE,
                           I_comp_type     IN     ELC_COMP.COMP_TYPE%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  CHECK_DELETE_CVB
--Purpose      :  Check if the given computation value base is currently being 
--                referenced in a different module.
-------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_CVB(O_error_message IN OUT VARCHAR2,
                          O_exists        IN OUT BOOLEAN,
                          I_cvb_code      IN     CVB_HEAD.CVB_CODE%TYPE)
         RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  GET_COMP_DESC
--Purpose      :  Retrieve the description for a given component. 
-------------------------------------------------------------------------------
FUNCTION GET_COMP_DESC(O_error_message  IN OUT VARCHAR2,
                       O_exists         IN OUT BOOLEAN,
                       O_comp_desc      IN OUT ELC_COMP.COMP_DESC%TYPE,
                       I_comp_id        IN     ELC_COMP.COMP_ID%TYPE)
         RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  GET_CVB_DESC
--Purpose      :  Retrieve the description for a given computation value 
--                base code.
-------------------------------------------------------------------------------
FUNCTION GET_CVB_DESC(O_error_message IN OUT VARCHAR2,
                      O_exists        IN OUT BOOLEAN,
                      O_cvb_desc      IN OUT CVB_HEAD.CVB_DESC%TYPE,
                      I_cvb_code      IN     CVB_HEAD.CVB_CODE%TYPE)
         RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  GET_COMP_DETAILS
--Purpose      :  Retrieve the details associated with a given component. 
-------------------------------------------------------------------------------
FUNCTION GET_COMP_DETAILS(O_error_message  IN OUT VARCHAR2,
                          O_exists         IN OUT BOOLEAN,
                          O_comp_desc      IN OUT ELC_COMP.COMP_DESC%TYPE,
                          O_comp_type      IN OUT ELC_COMP.COMP_TYPE%TYPE,
                          O_expense_type   IN OUT ELC_COMP.EXPENSE_TYPE%TYPE,
                          O_assess_type    IN OUT ELC_COMP.ASSESS_TYPE%TYPE,
                          O_import_country IN OUT ELC_COMP.IMPORT_COUNTRY_ID%TYPE,
                          O_cvb_code       IN OUT CVB_HEAD.CVB_CODE%TYPE,
                          O_comp_rate      IN OUT ELC_COMP.COMP_RATE%TYPE,
                          O_calc_basis     IN OUT ELC_COMP.CALC_BASIS%TYPE,
                          O_cost_basis     IN OUT ELC_COMP.COST_BASIS%TYPE,
                          O_display_order  IN OUT ELC_COMP.DISPLAY_ORDER%TYPE,
                          O_comp_currency  IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
                          O_per_count      IN OUT ELC_COMP.PER_COUNT%TYPE,
                          O_per_count_uom  IN OUT ELC_COMP.PER_COUNT_UOM%TYPE,
                          O_nom_flag_1     IN OUT ELC_COMP.NOM_FLAG_1%TYPE,
                          O_nom_flag_2     IN OUT ELC_COMP.NOM_FLAG_2%TYPE,
                          O_nom_flag_3     IN OUT ELC_COMP.NOM_FLAG_3%TYPE,
                          O_nom_flag_4     IN OUT ELC_COMP.NOM_FLAG_4%TYPE,
                          O_nom_flag_5     IN OUT ELC_COMP.NOM_FLAG_5%TYPE,
                          I_comp_id        IN     ELC_COMP.COMP_ID%TYPE)
         RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  GET_CALC_BASIS
--Purpose      :  Retrieve the calculation basis for a given component.
-------------------------------------------------------------------------------
FUNCTION GET_CALC_BASIS(O_error_message IN OUT VARCHAR2,
                        O_calc_basis    IN OUT ELC_COMP.CALC_BASIS%TYPE,
                        I_comp_id       IN     ELC_COMP.COMP_ID%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  GET_ASSESS_CURRENCY
--Purpose      :  Retrieve the import currency from the elc_comp table based on 
--                the passed in component and import country.  
-------------------------------------------------------------------------------
FUNCTION GET_ASSESS_CURRENCY(O_error_message     IN OUT VARCHAR2,
                             O_exists            IN OUT BOOLEAN,
                             O_comp_currency     IN OUT ELC_COMP.COMP_CURRENCY%TYPE,
                             I_import_country_id IN     ELC_COMP.IMPORT_COUNTRY_ID%TYPE,
                             I_comp_id           IN     ELC_COMP.COMP_ID%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  GET_ASSESS_DEFAULT CURRENCY
--Purpose      :  One currency code is allowed for an import country on the 
--                elc_comp table.  Therefore, retrieve the currency code of 
--                the passed in import country.
-------------------------------------------------------------------------------
FUNCTION GET_ASSESS_DEFAULT_CURRENCY(O_error_message     IN OUT VARCHAR2,
                                     O_exists            IN OUT BOOLEAN,
                                     O_comp_currency     IN OUT ELC_COMP.COMP_CURRENCY%TYPE,
                                     I_import_country_id IN     ELC_COMP.IMPORT_COUNTRY_ID%TYPE,
                                     I_comp_id           IN     ELC_COMP.COMP_ID%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  ASSESS_TYPE_EXISTS
--Purpose      :  An assessment type must exist on either the hts_fee table or
--                the hts_tax table.  Validate the given assessment type exists
--                on either one of those tables.
-------------------------------------------------------------------------------
FUNCTION ASSESS_TYPE_EXISTS(O_error_message IN OUT VARCHAR2,
                            O_exists        IN OUT BOOLEAN,
                            I_assess_type   IN     ELC_COMP.ASSESS_TYPE%TYPE)
         RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  COMP_EXISTS
--Purpose      :  Validate the given component exists on the elc_comp table.
-------------------------------------------------------------------------------
FUNCTION COMP_EXISTS(O_error_message  IN OUT VARCHAR2,
                     O_exists         IN OUT BOOLEAN,
                     I_comp_id        IN     ELC_COMP.COMP_ID%TYPE)
         RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  CVB_EXISTS 
--Purpose      :  Validate the given computation value base code exists on the
--                cvb_head table.
-------------------------------------------------------------------------------
FUNCTION CVB_EXISTS(O_error_message IN OUT VARCHAR2,
                    O_exists        IN OUT BOOLEAN,
                    I_cvb_code      IN     CVB_HEAD.CVB_CODE%TYPE)
         RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  CVB_COMP_EXISTS
--Purpose      :  Validate the given component is attached to the given 
--                computation value base code.
-------------------------------------------------------------------------------
FUNCTION CVB_COMP_EXISTS(O_error_message IN OUT VARCHAR2,
                         O_exists        IN OUT BOOLEAN,
                         I_cvb_code      IN     CVB_HEAD.CVB_CODE%TYPE,
                         I_comp_id       IN     ELC_COMP.COMP_ID%TYPE)
         RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  VALIDATE_ASSESS_COMP
--Purpose      :  An import country is required for an assessment.  Therefore,
--                if an import country is passed in, validate the component
--                is an assessment with that import country.  Else if no 
--                import country is passed in, going to validate the component
--                is an assessment.
-------------------------------------------------------------------------------
FUNCTION VALIDATE_ASSESS_COMP(O_error_message     IN OUT VARCHAR2,
                              O_exists            IN OUT BOOLEAN,
                              I_comp_id           IN     ELC_COMP.COMP_ID%TYPE,
                              I_import_country_id IN     ELC_COMP.IMPORT_COUNTRY_ID%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  VALIDATE_EXP_COMP
--Purpose      :  An expense type is required for an expense.  Therefore, if
--                an expense type ('Z' - Zone or 'C' - Country) is passed in, 
--                validate the component is an expense with that expense
--                type.  Else if no expense type is passed in, going to
--                validate the component is an expense.
-------------------------------------------------------------------------------
FUNCTION VALIDATE_EXP_COMP(O_error_message IN OUT VARCHAR2,
                           O_exists        IN OUT BOOLEAN,
                           I_comp_id       IN     ELC_COMP.COMP_ID%TYPE,
                           I_expense_type  IN     ELC_COMP.EXPENSE_TYPE%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  VALIDATE_ASSESS_CVB
--Purpose      :  If the cvb code passed in has assessments attached to it, then
--                validate the cvb code.  If the import country id is NULL,
--                then pass out the import country of an assessment component
--                attached to the passed in cvb code.  Else check if the import 
--                country matches the import country of assessment components
--                attached to the passed in cvb code. 
-------------------------------------------------------------------------------
FUNCTION VALIDATE_ASSESS_CVB(O_error_message      IN OUT VARCHAR2,
                             O_exists             IN OUT BOOLEAN,
                             IO_import_country_id IN OUT ELC_COMP.IMPORT_COUNTRY_ID%TYPE,
                             I_cvb_code           IN     CVB_HEAD.CVB_CODE%TYPE,
                             I_comp_id            IN     ELC_COMP.COMP_ID%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  VALIDATE_CVB_COMP
--Purpose      :  Validate the given component can be attached to the given 
--                cvb code.  If the given component is an assessment then
--                verify the import country matches the import country of
--                assessment components attached to the given cvb code. 
-------------------------------------------------------------------------------
FUNCTION VALIDATE_CVB_COMP(O_error_message IN OUT VARCHAR2,
                           O_exists        IN OUT BOOLEAN,
                           I_cvb_code      IN     CVB_HEAD.CVB_CODE%TYPE,
                           I_comp_id       IN     ELC_COMP.COMP_ID%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_COMP_TYPE
-- Purpose      : Returns the comp_type from the elc_comp table. A check to see 
--                whether the comp is an assessment, expense or up charge.
-------------------------------------------------------------------------------
FUNCTION GET_COMP_TYPE(O_error_message  IN OUT VARCHAR2,
                       O_exists         IN OUT BOOLEAN,
                       O_comp_type      IN OUT ELC_COMP.COMP_TYPE%TYPE,
                       I_comp_id        IN     ELC_COMP.COMP_ID%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_UP_CHRG_GRUP
-- Purpose      : Returns the Up Charge Group from the elc_comp table for the 
--                passed in component.
----------------------------------------------------------------------------------
FUNCTION GET_UP_CHRG_GROUP(O_error_message  IN OUT VARCHAR2,
                           O_exists         IN OUT BOOLEAN,
                           O_up_chrg_group  IN OUT ELC_COMP.UP_CHRG_GROUP%TYPE,
                           I_comp_id        IN     ELC_COMP.COMP_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------             
-- Function Name: LOCK_ELC_COMP
-- Purpose      : This function locks an elc_comp row for a single comp_id.
----------------------------------------------------------------------------------
FUNCTION LOCK_ELC_COMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_comp_id         IN       ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
FUNCTION CREATE_FC_FOR_COSTTMPL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_comp_id         IN       ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
END ELC_SQL;
/


