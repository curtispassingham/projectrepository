CREATE OR REPLACE PACKAGE PO_INDUCT_ERRVAL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------
FUNCTION CHECK_PO_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                         I_order_no        IN       V_POIND_ERRORS_LIST.ORDER_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_SUPPLIER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_sup_name        IN OUT   VARCHAR2,
                               O_exists          IN OUT   BOOLEAN,
                               I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                               I_supplier        IN       V_POIND_ERRORS_LIST.SUPPLIER%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_LOC_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_location_name   IN OUT   STORE.STORE_NAME%TYPE,
                          O_exists          IN OUT   BOOLEAN,
                          I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                          I_location        IN       V_POIND_ERRORS_LIST.LOCATION%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_item_desc       IN OUT   VARCHAR2,
                           O_exists          IN OUT   BOOLEAN,
                           I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                           I_item            IN       V_POIND_ERRORS_LIST.ITEM%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_TABLE_DESC_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists          IN OUT   BOOLEAN,
                                 I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                                 I_wksht           IN       V_POIND_ERRORS_LIST.TABLE_DESC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ROW_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT   BOOLEAN,
                          I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                          I_row             IN       V_POIND_ERRORS_LIST.ROW_SEQ%TYPE,
                          I_wksht           IN       V_POIND_ERRORS_LIST.TABLE_DESC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_COLUMN_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT   BOOLEAN,
                             I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                             I_column          IN       V_POIND_ERRORS_LIST.COLUMN_DESC%TYPE,
                             I_wksht           IN       V_POIND_ERRORS_LIST.TABLE_DESC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ERR_MSG_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                              I_error           IN       V_POIND_ERRORS_LIST.ERROR_MSG%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
END PO_INDUCT_ERRVAL;
/
