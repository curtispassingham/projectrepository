/******************************************************************************
* Service Name     : PurchaseOrderManagementService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/PurchaseOrderManagementService/v1
* Description      : Order Management.
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE PurchaseOrderManagementService AUTHID CURRENT_USER AS

/******************************************************************************
 *
 * Operation       : preIssueOrderNumber
 * Description     : Generate pre-issued order numbers for the calling application.
 *
 * Input           : "RIB_OrdNumCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/OrdNumCriVo/v1
 * Description     : Order number request object contains the details of the pre-issued order numbers to generate in RMS.
 *
 * Output          : "RIB_OrdNumColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/OrdNumColDesc/v1
 * Description     : OrdNumColDesc object is a collection of Pre-Issued Order Numbers generated in RMS.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 *
 ******************************************************************************/
PROCEDURE preIssueOrderNumber(I_serviceOperationContext   IN OUT   "RIB_ServiceOpContext_REC",
                              I_businessObject            IN       "RIB_OrdNumCriVo_REC",
                              O_serviceOperationStatus       OUT   "RIB_ServiceOpStatus_REC",
                              O_businessObject               OUT   "RIB_OrdNumColDesc_REC");
/******************************************************************************/

END PurchaseOrderManagementService;
/
