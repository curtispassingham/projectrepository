CREATE OR REPLACE PACKAGE BODY RMSAIASUB_PAYTERM AS
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPE
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT  VARCHAR2,
                        O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause          IN      VARCHAR2,
                        I_program        IN      VARCHAR2);

--------------------------------------------------------------------------------
-- PUBLIC
--------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code            OUT  VARCHAR2,
                  O_error_message          OUT  VARCHAR2,
                  O_rib_paytermref_rec     OUT  "RIB_PayTermRef_REC",
                  I_message             IN      "RIB_PayTermDesc_REC",
                  I_message_type        IN      VARCHAR2)
IS

   L_program                VARCHAR2(50) := 'RMSAIASUB_PAYTERM.CONSUME';
   L_message_type           VARCHAR2(15) := LOWER(I_message_type);
   L_payterm_rec            TERMS_SQL.PAYTERM_REC;
   L_system_options         SYSTEM_OPTIONS%ROWTYPE;
   PROGRAM_ERROR            EXCEPTION;

BEGIN

   O_STATUS_CODE := API_CODES.SUCCESS;

   if SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- Perform common api initialization tasks.
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE');
      raise PROGRAM_ERROR;
   end if;

   if L_message_type in (RMSAIASUB_PAYTERM.HDR_ADD, RMSAIASUB_PAYTERM.HDR_UPD,
                         RMSAIASUB_PAYTERM.DTL_ADD, RMSAIASUB_PAYTERM.DTL_UPD) then
      -- Validate message contents and populate records
      if RMSAIASUB_PAYTERM_VALIDATE.CHECK_MESSAGE(O_error_message,
                                                  O_rib_paytermref_rec,
                                                  L_payterm_rec,
                                                  I_message,
                                                  L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- INSERT/UPDATE tables
      if RMSAIASUB_PAYTERM_SQL.PERSIST(O_error_message,
                                       L_payterm_rec,
                                       L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE_TYPE');
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when PROGRAM_ERROR then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);
   when OTHERS then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);

END CONSUME;

--------------------------------------------------------------------------------
-- PRIVATE
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT   VARCHAR2,
                        O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause          IN       VARCHAR2,
                        I_program        IN       VARCHAR2) IS

   L_program  VARCHAR2(50) := 'RMSAIASUB_PAYTERM.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             O_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
END HANDLE_ERRORS;

--------------------------------------------------------------------------------
END RMSAIASUB_PAYTERM;

/
