CREATE OR REPLACE PACKAGE BODY RMSSUB_XORDER AS
-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE/FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION CAPITALIZE_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_message        IN OUT NOCOPY   "RIB_XOrderDesc_REC")
   RETURN BOOLEAN;

PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2);
-------------------------------------------------------------------------------------------------------
   -- PUBLIC PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2) IS

   PROGRAM_ERROR    EXCEPTION;

   L_program               VARCHAR2(50)   := 'RMSSUB_XORDER.CONSUME';
   
   L_ref_message           "RIB_XOrderRef_REC";
   L_message               "RIB_XOrderDesc_REC";
   L_message_type          VARCHAR2(50)   := LOWER(I_message_type);
   L_process_id            SVC_PROCESS_TRACKER.PROCESS_ID%TYPE;
   L_attempt_rms_load      VARCHAR2(5);
   L_svc_ordhead_rec       SVC_ORDHEAD%ROWTYPE;
   L_svc_orddetail_tbl     RMSSUB_XORDER_SQL.ORDDTL_TAB;

BEGIN
   O_STATUS_CODE := API_CODES.SUCCESS;

   -- perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   L_process_id := CORESVC_ITEM_PSEQ.NEXTVAL;

   if L_message_type in (LP_cre_type, LP_dtl_cre_type, LP_mod_type, LP_dtl_mod_type) then
      L_message := treat(I_MESSAGE AS "RIB_XOrderDesc_REC");

      if L_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;

      -- capitalize message fields
      if not CAPITALIZE_FIELDS(O_error_message,
                               L_message) then
         raise PROGRAM_ERROR;
      end if;

      if RMSSUB_XORDER_SQL.BUILD_RECORDS(O_error_message,
                                             L_svc_ordhead_rec,
                                             L_svc_orddetail_tbl,
                                             L_message,
                                             L_message_type) = FALSE then                                                                                              
         raise PROGRAM_ERROR;
      end if;

      -- INSERT/UPDATE table
      if RMSSUB_XORDER_SQL.PERSIST(O_error_message,
                                       L_svc_ordhead_rec,
                                       L_svc_orddetail_tbl,
                                       L_message_type,
                                       L_process_id) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      L_attempt_rms_load := L_message.attempt_rms_load; 
   elsif L_message_type in (LP_del_type, LP_dtl_del_type) then
      L_ref_message := treat(I_MESSAGE AS "RIB_XOrderRef_REC");

      if L_ref_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;

      if RMSSUB_XORDER_SQL.BUILD_RECORDS(O_error_message,
                                         L_svc_ordhead_rec,
                                         L_svc_orddetail_tbl,
                                         L_ref_message,
                                         L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if RMSSUB_XORDER_SQL.PERSIST(O_error_message,
                                   L_svc_ordhead_rec,
                                   L_svc_orddetail_tbl,
                                   L_message_type,
                                   L_process_id) = FALSE then

         raise PROGRAM_ERROR;
      end if;
      L_attempt_rms_load := L_ref_message.attempt_rms_load;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'));
      raise PROGRAM_ERROR;
   end if;

   if L_attempt_rms_load is NOT NULL and L_attempt_rms_load NOT IN ('RMS','STG') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DESTINATION', L_message.attempt_rms_load);
      raise PROGRAM_ERROR;
   end if;

   insert into svc_process_tracker(process_id,
                                   process_desc,
                                   template_key,
                                   action_type,
                                   process_source,
                                   process_destination,
                                   status,
                                   rms_async_id,
                                   action_date,
                                   user_id)
                            values(L_process_id,
                                   'XORDER',
                                   'RMSSUB_XORDER',
                                   'U',
                                   'EXT',
                                   NVL(L_attempt_rms_load, 'RMS'),
                                   DECODE(NVL(L_attempt_rms_load, 'RMS'),'RMS','N','PS'),
                                   NULL,
                                   SYSDATE,
                                   user);
         
   if NVL(L_attempt_rms_load, 'RMS') = 'RMS' then       
      if PO_INDUCT_SQL.EXEC_ASYNC(O_error_message,
                                  NULL,
                                  L_process_id) = FALSE then      
         raise PROGRAM_ERROR;
      end if;
   end if;

   return;

EXCEPTION    
   when PROGRAM_ERROR then    
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program); 
   when OTHERS then  
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program); 
END CONSUME;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE/FUNCTION BODY
-------------------------------------------------------------------------------------------------------
FUNCTION CAPITALIZE_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_message        IN OUT NOCOPY   "RIB_XOrderDesc_REC")
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'RMSSUB_XORDER.CAPITALIZE_FIELD';

BEGIN

   IO_message.currency_code := UPPER(IO_message.currency_code);
   IO_message.status := UPPER(IO_message.status);
   IO_message.include_on_ord_ind := UPPER(IO_message.include_on_ord_ind);
   IO_message.orig_ind := UPPER(IO_message.orig_ind);
   IO_message.edi_po_ind := UPPER(IO_message.edi_po_ind);
   IO_message.pre_mark_ind := UPPER(IO_message.pre_mark_ind);

   if IO_message.xorderdtl_tbl is NOT NULL AND IO_message.xorderdtl_tbl.COUNT > 0 then
      FOR i in IO_message.xorderdtl_tbl.first..IO_message.xorderdtl_tbl.last LOOP
         IO_message.xorderdtl_tbl(i).origin_country_id := UPPER(IO_message.xorderdtl_tbl(i).origin_country_id);
         IO_message.xorderdtl_tbl(i).location_type := UPPER(IO_message.xorderdtl_tbl(i).location_type);
         IO_message.xorderdtl_tbl(i).cancel_ind := UPPER(IO_message.xorderdtl_tbl(i).cancel_ind);
         IO_message.xorderdtl_tbl(i).reinstate_ind := UPPER(IO_message.xorderdtl_tbl(i).reinstate_ind);
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CAPITALIZE_FIELDS;
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2) IS

   L_program VARCHAR2(50) := 'RMSSUB_XORDER.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);
EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XORDER;
/