CREATE OR REPLACE PACKAGE FDL_TRANDATA_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------------------------------
TYPE fdl_trandata_rec IS RECORD(deal_no              FIXED_DEAL_GL_REF_DATA.DEAL_NO%TYPE,
                                fixed_deal_amt       FIXED_DEAL_GL_REF_DATA.FIXED_DEAL_AMT%TYPE,
                                collect_date         FIXED_DEAL_GL_REF_DATA.COLLECT_DATE%TYPE,
                                dept                 FIXED_DEAL_GL_REF_DATA.DEPT%TYPE,
                                class                FIXED_DEAL_GL_REF_DATA.CLASS%TYPE,
                                subclass             FIXED_DEAL_GL_REF_DATA.SUBCLASS%TYPE,
                                loc_type             FIXED_DEAL_GL_REF_DATA.LOC_TYPE%TYPE,
                                location             FIXED_DEAL_GL_REF_DATA.LOCATION%TYPE,
                                contrib_ratio        FIXED_DEAL_GL_REF_DATA.CONTRIB_RATIO%TYPE,
                                contrib_amount       FIXED_DEAL_GL_REF_DATA.CONTRIB_AMOUNT%TYPE,
                                set_of_books_id      FIXED_DEAL_GL_REF_DATA.SET_OF_BOOKS_ID%TYPE,
                                reference_trace_id   FIXED_DEAL_GL_REF_DATA.REFERENCE_TRACE_ID%TYPE,
                                error_message        RTK_ERRORS.RTK_TEXT%TYPE,
                                return_code          VARCHAR2(5));

TYPE fdl_trandata_tbl IS TABLE OF fdl_trandata_rec INDEX BY BINARY_INTEGER;
--------------------------------------------------------------------------------------------------------------------
--- Procedure Name: QUERY_PROCEDURE
--- Purpose:        This procedure will query FIXED_DEAL_GL_REF_DATA table and populate the above declared 
---                 table of records that will be used as the 'base table' in the Fixed Deal Transaction Data From.
--------------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (IO_fdl_trandata_tbl     IN OUT   FDL_TRANDATA_SQL.FDL_TRANDATA_TBL,
                           I_deal_no               IN       FIXED_DEAL_GL_REF_DATA.DEAL_NO%TYPE,
                           I_supplier              IN       DEAL_HEAD.SUPPLIER%TYPE,
                           I_dept                  IN       FIXED_DEAL_GL_REF_DATA.DEPT%TYPE,
                           I_class                 IN       FIXED_DEAL_GL_REF_DATA.CLASS%TYPE,
                           I_subclass              IN       FIXED_DEAL_GL_REF_DATA.SUBCLASS%TYPE,
                           I_loc_type              IN       FIXED_DEAL_GL_REF_DATA.LOC_TYPE%TYPE,
                           I_location              IN       FIXED_DEAL_GL_REF_DATA.LOCATION%TYPE,
                           I_collect_date_start    IN       FIXED_DEAL_GL_REF_DATA.COLLECT_DATE%TYPE,
                           I_collect_date_end      IN       FIXED_DEAL_GL_REF_DATA.COLLECT_DATE%TYPE,
                           I_reference_trace_id    IN       FIXED_DEAL_GL_REF_DATA.REFERENCE_TRACE_ID%TYPE);
--------------------------------------------------------------------------------------------------------------------
--- Procedure Name: GET_MIN_DATE
--- Purpose:        This procedure will query FIXED_DEAL_GL_REF_DATA table to get the minimum possible date for 
---                 collect_date
--------------------------------------------------------------------------------------------------------------------
FUNCTION GET_MIN_DATE (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_min_date              IN OUT   FIXED_DEAL_GL_REF_DATA.COLLECT_DATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------------
--- Procedure Name: GET_DEAL_SUPPLIER
--- Purpose:        This procedure will query DEAL_HEAD table to get the supplier info of the current deal
--------------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_SUPPLIER (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_supplier              IN OUT   DEAL_HEAD.SUPPLIER%TYPE,
                            I_deal_no               IN       DEAL_HEAD.DEAL_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------------
END FDL_TRANDATA_SQL;
/
