
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ORDER_BRACKET_CALC_SQL AS
------------------------------------------------------------------------------
FUNCTION APPLY_BRACKETS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                        I_override_manual_ind IN     DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(60) := 'ORDER_BRACKET_CALC_SQL.APPLY_BRACKETS';
   L_bracket_rec    SUP_BRACKET_COST%ROWTYPE;
   L_purchase_type  ORDHEAD.PURCHASE_TYPE%TYPE;
   L_bracket_level  SUPS.INV_MGMT_LVL%TYPE;

   cursor C_SUPP_PO_TYPE is
      select supplier,
             purchase_type
        from ordhead
       where order_no = I_order_no;

   cursor C_GET_DEPTS is
      select distinct(im.dept) dept
        from item_master im,
             ordloc ol
       where ol.order_no = I_order_no
         and ol.item     = im.item;

   cursor C_GET_LOC is
      select distinct(location) location
        from ordloc
       where order_no = I_order_no
         and loc_type = 'W';
BEGIN
   if I_order_no is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_order_no',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN','C_SUPP_PO_TYPE','ORDHEAD', 'Order No: '|| to_char(I_order_no));
   open C_SUPP_PO_TYPE;
   SQL_LIB.SET_MARK('FETCH','C_SUPP_PO_TYPE','ORDHEAD', 'Order No: '|| to_char(I_order_no));
   fetch C_SUPP_PO_TYPE into L_bracket_rec.supplier,
                             L_purchase_type;
   SQL_LIB.SET_MARK('CLOSE','C_SUPP_PO_TYPE','ORDHEAD', 'Order No: '|| to_char(I_order_no));
   close C_SUPP_PO_TYPE;
   if L_purchase_type in ('FOB', 'BACK') then
      if not UPDATE_ORDER_COST(O_error_message,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               L_bracket_rec.supplier,
                               I_order_no,
                               L_purchase_type,
                               I_override_manual_ind) then
         return FALSE;
      end if;
   elsif L_purchase_type = 'DLV' then
      if not SUP_INV_MGMT_SQL.GET_INV_MGMT_LEVEL(O_error_message,
                                                 L_bracket_level,
                                                 L_bracket_rec.supplier) then
         return FALSE;
      end if;
      if L_bracket_level = 'S' then
         if not GET_QUANTITY_ORDERED(O_error_message,
                                     L_bracket_rec.bracket_value1,
                                     L_bracket_rec.bracket_value2,
                                     L_bracket_rec.sup_dept_seq_no,
                                     NULL, -- dept
                                     NULL, -- location
                                     L_bracket_rec.supplier,
                                     I_order_no) then
            return FALSE;
         end if;
         if not UPDATE_ORDER_COST(O_error_message,
                                  L_bracket_rec.bracket_value1,
                                  L_bracket_rec.bracket_value2,
                                  NULL, -- dept
                                  NULL, -- location
                                  L_bracket_rec.supplier,
                                  I_order_no,
                                  L_purchase_type,
                                  I_override_manual_ind) then
            return FALSE;
         end if;
      elsif L_bracket_level = 'D' then
         FOR dept_rec in C_GET_DEPTS LOOP
            L_bracket_rec.dept     := dept_rec.dept;
            L_bracket_rec.location := NULL;
            if not GET_QUANTITY_ORDERED(O_error_message,
                                        L_bracket_rec.bracket_value1,
                                        L_bracket_rec.bracket_value2,
                                        L_bracket_rec.sup_dept_seq_no,
                                        L_bracket_rec.dept,
                                        NULL, -- location
                                        L_bracket_rec.supplier,
                                        I_order_no) then
               return FALSE;
            end if;
            if not UPDATE_ORDER_COST(O_error_message,
                                     L_bracket_rec.bracket_value1,
                                     L_bracket_rec.bracket_value2,
                                     L_bracket_rec.dept,
                                     NULL, -- location
                                     L_bracket_rec.supplier,
                                     I_order_no,
                                     L_purchase_type,
                                     I_override_manual_ind) then
               return FALSE;
            end if;
         END LOOP;
      elsif L_bracket_level = 'L' then
         FOR loc_rec in C_GET_LOC LOOP
            L_bracket_rec.location := loc_rec.location;
            L_bracket_rec.dept     := NULL;
            if not GET_QUANTITY_ORDERED(O_error_message,
                                        L_bracket_rec.bracket_value1,
                                        L_bracket_rec.bracket_value2,
                                        L_bracket_rec.sup_dept_seq_no,
                                        NULL, -- dept
                                        L_bracket_rec.location,
                                        L_bracket_rec.supplier,
                                        I_order_no) then
               return FALSE;
            end if;
            if not UPDATE_ORDER_COST(O_error_message,
                                     L_bracket_rec.bracket_value1,
                                     L_bracket_rec.bracket_value2,
                                     NULL, -- dept
                                     L_bracket_rec.location,
                                     L_bracket_rec.supplier,
                                     I_order_no,
                                     L_purchase_type,
                                     I_override_manual_ind) then
               return FALSE;
            end if;
         END LOOP;
      elsif L_bracket_level = 'A' then
         FOR dept_rec in C_GET_DEPTS LOOP
            L_bracket_rec.dept := dept_rec.dept;
            FOR loc_rec in C_GET_LOC LOOP
               L_bracket_rec.location := loc_rec.location;
               if not GET_QUANTITY_ORDERED(O_error_message,
                                           L_bracket_rec.bracket_value1,
                                           L_bracket_rec.bracket_value2,
                                           L_bracket_rec.sup_dept_seq_no,
                                           L_bracket_rec.dept,
                                           L_bracket_rec.location,
                                           L_bracket_rec.supplier,
                                           I_order_no) then
                  return FALSE;
               end if;
               if not UPDATE_ORDER_COST(O_error_message,
                                        L_bracket_rec.bracket_value1,
                                        L_bracket_rec.bracket_value2,
                                        L_bracket_rec.dept,
                                        L_bracket_rec.location,
                                        L_bracket_rec.supplier,
                                        I_order_no,
                                        L_purchase_type,
                                        I_override_manual_ind) then
                  return FALSE;
               end if;
            END LOOP;
         END LOOP;
      end if; -- bracket type
   end if; -- PO type
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END APPLY_BRACKETS;
---------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_COST(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_bracket_value1      IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                           I_bracket_value2      IN     SUP_BRACKET_COST.BRACKET_VALUE2%TYPE,
                           I_dept                IN     SUP_BRACKET_COST.DEPT%TYPE,
                           I_location            IN     SUP_BRACKET_COST.LOCATION%TYPE,
                           I_supplier            IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                           I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                           I_purchase_type       IN     ORDHEAD.PURCHASE_TYPE%TYPE,
                           I_override_manual_ind IN     DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(60)  := 'ORDER_BRACKET_CALC_SQL.UPDATE_ORDER_COST';
   L_table          VARCHAR2(30)  := 'ORDLOC';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   L_bracket_level  SUPS.INV_MGMT_LVL%TYPE;
   bracket1_rec     ITEM_SUPP_COUNTRY_BRACKET_COST%ROWTYPE;
   L_order_cost     ITEM_SUPP_COUNTRY_BRACKET_COST.UNIT_COST%TYPE;

   cursor C_LOCK_ORDLOC is
      select 'x'
        from ordloc ol,
             item_master im
       where order_no = I_order_no
         and (location = I_location or I_location is NULL)
         and ol.item = im.item
         and (im.dept = I_dept or I_dept is NULL)
         for update of ol.location nowait;

   cursor C_GET_FOB_COST is
      select iscbc.item item,
             iscbc.location location,
             iscbc.unit_cost unit_cost
        from item_supp_country_bracket_cost iscbc,
             ordloc ol,
             ordsku os
       where ol.order_no          = I_order_no
         and iscbc.supplier       = I_supplier
         and ol.location          = iscbc.location
         and ol.item              = iscbc.item
         and ol.order_no          = os.order_no
         and ol.item              = os.item
         and os.origin_country_id = iscbc.origin_country_id
         and bracket_value1 = -999;

   cursor C_GET_BRACKET1_COST is
      select iscbc.origin_country_id,
             iscbc.item item,
             iscbc.location location,
             iscbc.unit_cost unit_cost,
             iscbc.bracket_value1
        from item_supp_country_bracket_cost iscbc,
             item_master im,
             ordsku os
       where iscbc.supplier       = I_supplier
         and (iscbc.location      = I_location or I_location is NULL)
         and iscbc.item           = im.item
         and (im.dept             = I_dept or I_dept is NULL)
         and os.order_no          = I_order_no
         and os.item              = iscbc.item
         and os.origin_country_id = iscbc.origin_country_id
         and (bracket_value1 = (select max(iscbc.bracket_value1) bracket_value1
                                 from item_supp_country_bracket_cost iscbc,
                                      item_master im,
                                      ordsku os
                                where iscbc.supplier        = I_supplier
                                  and (iscbc.location       = I_location or I_location is NULL)
                                  and iscbc.item            = im.item
                                  and (im.dept              = I_dept or I_dept is NULL)
                                  and bracket_value1       <= I_bracket_value1
                                  and os.order_no           = I_order_no
                                  and os.item               = iscbc.item
                                  and os.origin_country_id  = iscbc.origin_country_id
                                  and iscbc.bracket_value1 != -999)
          or (I_bracket_value1 < (select min(iscbc.bracket_value1) bracket_value1
                                    from item_supp_country_bracket_cost iscbc,
                                         item_master im,
                                          ordsku os
                                   where iscbc.supplier        = I_supplier
                                     and (iscbc.location       = I_location  or I_location  is NULL)
                                     and iscbc.item            = im.item
                                     and (im.dept              = I_dept or I_dept is NULL)
                                     and os.order_no           = I_order_no
                                     and os.item               = iscbc.item
                                     and os.origin_country_id  = iscbc.origin_country_id
                                     and iscbc.bracket_value1 != -999)
           and default_bracket_ind = 'Y'));

   cursor C_GET_LOWEST_BRACKET1 is
      select unit_cost,
             bracket_value1
        from item_supp_country_bracket_cost iscbc1
       where iscbc1.supplier   = I_supplier
         and location          = bracket1_rec.location
         and item              = bracket1_rec.item
         and origin_country_id = bracket1_rec.origin_country_id
         and bracket_value1    = (select min(iscbc2.bracket_value1) bracket_value1
                                    from item_supp_country_bracket_cost iscbc2
                                   where iscbc2.supplier          = iscbc1.supplier
                                     and iscbc2.location          = iscbc1.location
                                     and iscbc2.item              = iscbc1.item
                                     and iscbc2.origin_country_id = iscbc1.origin_country_id
                                     and iscbc2.bracket_value1   != -999);
   lowest_bracket1_rec     C_GET_LOWEST_BRACKET1%ROWTYPE;

   cursor C_GET_BRACKET2_COST is
       select unit_cost,
              bracket_value2
        from item_supp_country_bracket_cost i1
       where supplier          = I_supplier
         and location          = bracket1_rec.location
         and item              = bracket1_rec.item
         and origin_country_id = bracket1_rec.origin_country_id
         and bracket_value2    = (select  max(bracket_value2)
                                    from item_supp_country_bracket_cost i2
                                   where i2.supplier          = i1.supplier
                                     and i2.location          = i1.location
                                     and i2.item              = i1.item
                                     and i2.bracket_value2   <= I_bracket_value2
                                     and i2.origin_country_id = i1.origin_country_id);
   bracket2_rec     C_GET_BRACKET2_COST%ROWTYPE;
BEGIN
   if I_order_no is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_order_no',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_supplier',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;
   open C_LOCK_ORDLOC;
   close C_LOCK_ORDLOC;
   if I_purchase_type in ('FOB', 'BACK') then
      FOR fob_cost_rec in C_GET_FOB_COST LOOP
         update ordloc
            set unit_cost      = fob_cost_rec.unit_cost,
                cost_source    = 'BRKT'
          where order_no = I_order_no
            and item     = fob_cost_rec.item
            and location = fob_cost_rec.location
            and (I_override_manual_ind = 'Y'
                or (I_override_manual_ind = 'N' and cost_source != 'MANL'));
      END LOOP;
   elsif I_purchase_type = 'DLV' then
      if I_bracket_value2 is NULL then
         FOR bracket1_loop_rec in C_GET_BRACKET1_COST LOOP
            bracket1_rec.origin_country_id := bracket1_loop_rec.origin_country_id;
            bracket1_rec.item              := bracket1_loop_rec.item;
            bracket1_rec.location          := bracket1_loop_rec.location;
            bracket1_rec.unit_cost         := bracket1_loop_rec.unit_cost;
            bracket1_rec.bracket_value1    := bracket1_loop_rec.bracket_value1;
            ---
            --- check if the bracket is the pickup bracket.  If it is not,
            --- then use it for updating costs.  If it is, then need to grab
            --- the lowest delivery bracket because order is delivered.
            ---
            if bracket1_rec.bracket_value1 != -999 then
               L_order_cost := bracket1_rec.unit_cost;
            else -- retrieve the lowest deliverable bracket and use that cost.
               open C_GET_LOWEST_BRACKET1;
               fetch C_GET_LOWEST_BRACKET1 into lowest_bracket1_rec;
               close C_GET_LOWEST_BRACKET1;
               L_order_cost := lowest_bracket1_rec.unit_cost;
            end if;
            update ordloc
               set unit_cost      = L_order_cost,
                   cost_source    = 'BRKT'
             where order_no = I_order_no
               and item     = bracket1_rec.item
               and location = bracket1_rec.location
               and (I_override_manual_ind = 'Y'
                   or (I_override_manual_ind = 'N' and cost_source != 'MANL'));
         END LOOP;
      else
         FOR bracket1_loop_rec in C_GET_BRACKET1_COST LOOP
            bracket1_rec.origin_country_id := bracket1_loop_rec.origin_country_id;
            bracket1_rec.item              := bracket1_loop_rec.item;
            bracket1_rec.location          := bracket1_loop_rec.location;
            bracket1_rec.unit_cost         := bracket1_loop_rec.unit_cost;
            bracket1_rec.bracket_value1    := bracket1_loop_rec.bracket_value1;
            ---
            --- Retreive the secondary bracket and cost to determine if the order
            --- achieved a lower cost in the secondary bracket.
            ---
            open C_GET_BRACKET2_COST;
            fetch C_GET_BRACKET2_COST into bracket2_rec;
            close C_GET_BRACKET2_COST;
            ---
            --- check if the primary bracket is the pickup bracket.  If it is not,
            --- then compare it with the costs of secondary bracket to determine
            --- which cost is lower.  If it is, then need to grab
            --- the lowest delivery bracket because order is delivered.
            ---
            if bracket1_rec.bracket_value1 != -999 then
               if (bracket1_rec.unit_cost <= bracket2_rec.unit_cost or bracket2_rec.unit_cost is NULL) then
                  L_order_cost := bracket1_rec.unit_cost;
               elsif bracket2_rec.unit_cost < bracket1_rec.unit_cost then
                  L_order_cost := bracket2_rec.unit_cost;
               end if;
            else -- retrieve the lowest deliverable bracket and use that cost.
               open C_GET_LOWEST_BRACKET1;
               fetch C_GET_LOWEST_BRACKET1 into lowest_bracket1_rec;
               close C_GET_LOWEST_BRACKET1;
               if (lowest_bracket1_rec.unit_cost <= bracket2_rec.unit_cost or bracket2_rec.unit_cost is NULL) then
                  L_order_cost := lowest_bracket1_rec.unit_cost;
               elsif bracket2_rec.unit_cost < lowest_bracket1_rec.unit_cost then
                  L_order_cost := bracket2_rec.unit_cost;
               end if;
            end if;
            update ordloc
               set unit_cost      = L_order_cost,
                   cost_source    = 'BRKT'
             where order_no = I_order_no
               and item     = bracket1_rec.item
               and location = bracket1_rec.location
               and (I_override_manual_ind = 'Y'
                   or (I_override_manual_ind = 'N' and cost_source != 'MANL'));
         END LOOP;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Supplier: '||to_char(I_supplier) ||', Department: '||to_char(I_dept)||',
                                             Location: '||to_char(I_location),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_ORDER_COST;
---------------------------------------------------------------------------------
FUNCTION APPLY_THRESHOLDS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_bracket_value1   IN OUT   SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                          O_bracket_type1    IN OUT   SUP_INV_MGMT.BRACKET_TYPE1%TYPE,
                          O_bracket_uom1     IN OUT   SUP_INV_MGMT.BRACKET_UOM1%TYPE,
                          O_bracket_value2   IN OUT   SUP_BRACKET_COST.BRACKET_VALUE2%TYPE,
                          O_bracket_type2    IN OUT   SUP_INV_MGMT.BRACKET_TYPE2%TYPE,
                          O_bracket_uom2     IN OUT   SUP_INV_MGMT.BRACKET_UOM2%TYPE,
                          O_next_bracket     IN OUT   BOOLEAN,
                          I_truck_total      IN       NUMBER,
                          I_total_ord_qty    IN       NUMBER,
                          I_max_bracket      IN       SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                          I_prim_or_sec      IN       VARCHAR2,
                          I_order_no         IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(60)  := 'ORDER_BRACKET_CALC_SQL.APPLY_THRESHOLDS';
   L_purchase_type  ORDHEAD.PURCHASE_TYPE%TYPE;
   L_order_rec      SUP_BRACKET_COST%ROWTYPE;
   L_loc_type       ORDHEAD.LOC_TYPE%TYPE;
   L_bracket_level  SUPS.INV_MGMT_LVL%TYPE;
   L_count          STORE.STORE%TYPE;
   L_truck_total    NUMBER(10);

   cursor C_SUPP_PO_TYPE is
      select supplier,
             purchase_type,
             dept,
             location,
             loc_type
        from ordhead
       where order_no = I_order_no;

   cursor C_GET_DEPT is
      select count(distinct(im.dept)),
             max(im.dept) dept
        from item_master im, ordsku os
       where os.order_no = I_order_no
         and os.item     = im.item
    group by os.order_no;

   cursor C_GET_MULTICHANNEL_LOC is
      select count(distinct(physical_wh)),
             max(distinct(physical_wh)) location
        from wh w,
             ordloc ol
       where ol.order_no = I_order_no
         and ol.location = w.wh;

   ---------------------------------------------------------------------------------
   --- INTERNAL FUNCTION CALCULATES THRESHOLD
   ---------------------------------------------------------------------------------
   FUNCTION CALC_THRESHOLD
   RETURN BOOLEAN IS
      L_program        VARCHAR2(60)  := 'ORDER_BRACKET_CALC_SQL.APPLY_THRESHOLDS.CALC_THRESHOLD';
      TYPE bracket_rec is RECORD(bracket_value1  SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                                 bracket_value2  SUP_BRACKET_COST.BRACKET_VALUE2%TYPE);
      L_previous      bracket_rec;
      L_next          bracket_rec;
      L_threshold     SUP_INV_MGMT.THRESHOLD_NEXT_BRACKET%TYPE;

      cursor C_GET_THRESHOLD is
         select threshold_next_bracket
           from sup_inv_mgmt
          where sup_dept_seq_no = L_order_rec.sup_dept_seq_no;

      cursor C_NEXT_BRACKET1 is
         select min(bracket_value1) bracket_value1
           from sup_bracket_cost
          where sup_dept_seq_no = L_order_rec.sup_dept_seq_no
            and bracket_value1 >= L_order_rec.bracket_value1
            and bracket_value1 != -999;

      cursor C_PREVIOUS_BRACKET1 is
         select nvl(max(bracket_value1), 0) bracket_value1
           from sup_bracket_cost
          where sup_dept_seq_no = L_order_rec.sup_dept_seq_no
            and bracket_value1 <= L_order_rec.bracket_value1
            and bracket_value1 != -999;

      cursor C_NEXT_BRACKET2 is
         select min(bracket_value2) bracket_value2
           from sup_bracket_cost
          where sup_dept_seq_no = L_order_rec.sup_dept_seq_no
            and bracket_value2 >= L_order_rec.bracket_value2;

      cursor C_PREVIOUS_BRACKET2 is
         select nvl(max(bracket_value2), 0) bracket_value2
           from sup_bracket_cost
          where sup_dept_seq_no = L_order_rec.sup_dept_seq_no
            and bracket_value2 <= L_order_rec.bracket_value2;
   BEGIN
      if not ITEM_BRACKET_COST_SQL.GET_BRACKET(O_error_message,
                                               O_bracket_type1,
                                               O_bracket_uom1,
                                               O_bracket_type2,
                                               O_bracket_uom2,
                                               L_order_rec.sup_dept_seq_no,
                                               L_order_rec.supplier,
                                               L_order_rec.dept,
                                               L_order_rec.location) then
         return FALSE;
      end if;
      --- if more than 1 truck is passed in, then use the passed in
      --- total order quantity to determine the partial quantity to
      --- apply thresholds to.
      if I_truck_total > 1 then
         L_truck_total := TRUNC(I_truck_total);
         if I_prim_or_sec = 'P' then
            L_order_rec.bracket_value1 := (I_total_ord_qty - I_max_bracket * L_truck_total);
         elsif I_prim_or_sec = 'S' then
            L_order_rec.bracket_value2 := (I_total_ord_qty - I_max_bracket * L_truck_total);
         end if;
      elsif I_truck_total = 1 then
         if I_prim_or_sec = 'P' then
            L_order_rec.bracket_value1 := I_total_ord_qty;
         elsif I_prim_or_sec = 'S' then
            L_order_rec.bracket_value2 := I_total_ord_qty;
         end if;
      end if;
      SQL_LIB.SET_MARK('OPEN','C_GET_THRESHOLD','SUP_INV_MGMT',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
      open C_GET_THRESHOLD;
      SQL_LIB.SET_MARK('FETCH','C_GET_THRESHOLD','SUP_INV_MGMT',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
      fetch C_GET_THRESHOLD into L_threshold;
      SQL_LIB.SET_MARK('CLOSE','C_GET_THRESHOLD','SUP_INV_MGMT',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
      close C_GET_THRESHOLD;
      if L_threshold is NULL then
         O_bracket_value1 := NULL;
         O_bracket_value2 := NULL;
         O_next_bracket := FALSE;
         return TRUE;
      end if;
      if I_prim_or_sec = 'P' then
         SQL_LIB.SET_MARK('OPEN','C_PREVIOUS_BRACKET1','SUP_BRACKET_COST',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
         open C_PREVIOUS_BRACKET1;
         SQL_LIB.SET_MARK('FETCH','C_PREVIOUS_BRACKET1','SUP_BRACKET_COST',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
         fetch C_PREVIOUS_BRACKET1 into L_previous.bracket_value1;
         SQL_LIB.SET_MARK('CLOSE','C_PREVIOUS_BRACKET1','SUP_BRACKET_COST',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
         close C_PREVIOUS_BRACKET1;
         ---
         SQL_LIB.SET_MARK('OPEN','C_NEXT_BRACKET1','SUP_BRACKET_COST',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
         open C_NEXT_BRACKET1;
         SQL_LIB.SET_MARK('FETCH','C_NEXT_BRACKET1','SUP_BRACKET_COST',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
         fetch C_NEXT_BRACKET1 into L_next.bracket_value1;
         SQL_LIB.SET_MARK('CLOSE','C_NEXT_BRACKET1','SUP_BRACKET_COST',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
         close C_NEXT_BRACKET1;
         --- The brackets above and below the order_rec.bracket_value are retrieved to
         --- determine if the next bracket can be achieved based on the threshold.
         if L_order_rec.bracket_value1 >=
          (L_next.bracket_value1 - (1 - L_threshold/100)*(L_next.bracket_value1 - L_previous.bracket_value1)) then
            O_bracket_value1 := L_next.bracket_value1;
            O_next_bracket := TRUE;
            return TRUE;
         else
            O_next_bracket := FALSE;
         end if;
      elsif I_prim_or_sec = 'S' then
         SQL_LIB.SET_MARK('OPEN','C_PREVIOUS_BRACKET2','SUP_BRACKET_COST',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
         open C_PREVIOUS_BRACKET2;
         SQL_LIB.SET_MARK('FETCH','C_PREVIOUS_BRACKET2','SUP_BRACKET_COST',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
         fetch C_PREVIOUS_BRACKET2 into L_previous.bracket_value2;
         SQL_LIB.SET_MARK('CLOSE','C_PREVIOUS_BRACKET2','SUP_BRACKET_COST',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
         close C_PREVIOUS_BRACKET2;
         ---
         SQL_LIB.SET_MARK('OPEN','C_NEXT_BRACKET2','SUP_BRACKET_COST',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
         open C_NEXT_BRACKET2;
         SQL_LIB.SET_MARK('FETCH','C_NEXT_BRACKET2','SUP_BRACKET_COST',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
         fetch C_NEXT_BRACKET2 into L_next.bracket_value2;
         SQL_LIB.SET_MARK('CLOSE','C_NEXT_BRACKET2','SUP_BRACKET_COST',
                       'Supplier: '|| to_char(L_order_rec.supplier)||
                       'Dept: '|| to_char(L_order_rec.dept)||
                       'Location: '|| to_char(L_order_rec.location));
         close C_NEXT_BRACKET2;
         --- The brackets above and below the order_rec.bracket_value are retrieved to
         --- determine if the next bracket can be achieved based on the threshold.
         if L_order_rec.bracket_value2 >=
          (L_next.bracket_value2 - (1 - L_threshold/100)*(L_next.bracket_value2 - L_previous.bracket_value2)) then
            O_bracket_value2 := L_next.bracket_value2;
            O_next_bracket := TRUE;
         else
            O_next_bracket := FALSE;
         end if;
      end if;
      return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                to_char(SQLCODE));
      return FALSE;
   END CALC_THRESHOLD;
---------------------------------------------------------------------------------
--- BEGIN APPLY THRESHOLD HERE
---------------------------------------------------------------------------------
BEGIN
   if I_order_no is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_order_no',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN','C_SUPP_PO_TYPE','ORDHEAD', 'Order No: '|| to_char(I_order_no));
   open C_SUPP_PO_TYPE;
   SQL_LIB.SET_MARK('FETCH','C_SUPP_PO_TYPE','ORDHEAD', 'Order No: '|| to_char(I_order_no));
   fetch C_SUPP_PO_TYPE into L_order_rec.supplier,
                             L_purchase_type,
                             L_order_rec.dept,
                             L_order_rec.location,
                             L_loc_type;
   SQL_LIB.SET_MARK('CLOSE','C_SUPP_PO_TYPE','ORDHEAD', 'Order No: '|| to_char(I_order_no));
   close C_SUPP_PO_TYPE;
   if L_purchase_type in('FOB', 'BACK') then
      O_error_message := SQL_LIB.CREATE_MSG('THRESHOLDS_NOT_APPLY', NULL, NULL, NULL);
      O_next_bracket := FALSE;
      return TRUE;
   elsif L_purchase_type = 'DLV' then
      if not SUP_INV_MGMT_SQL.GET_INV_MGMT_LEVEL(O_error_message,
                                                 L_bracket_level,
                                                 L_order_rec.supplier) then
         return FALSE;
      end if;
      if L_order_rec.dept is not NULL and L_order_rec.location is not NULL
         and L_loc_type = 'W' then
         if not CALC_THRESHOLD then
            return FALSE;
         end if;
      elsif L_bracket_level = 'S' then
         L_order_rec.dept := NULL;
         L_order_rec.location := NULL;
         if not CALC_THRESHOLD then
            return FALSE;
         end if;
      elsif L_bracket_level = 'D' then
         if L_order_rec.dept is NULL then
            SQL_LIB.SET_MARK('OPEN','C_GET_DEPT','ORDSKU', 'Order No: '|| to_char(I_order_no));
            open C_GET_DEPT;
            SQL_LIB.SET_MARK('FETCH','C_GET_DEPT','ORDSKU', 'Order No: '|| to_char(I_order_no));
            fetch C_GET_DEPT into L_count,
                                  L_order_rec.dept;
            SQL_LIB.SET_MARK('CLOSE','C_GET_DEPT','ORDSKU', 'Order No: '|| to_char(I_order_no));
            close C_GET_DEPT;
         else
            L_count := 1;
         end if;
         if L_count > 1 then
            O_next_bracket := FALSE;
            return TRUE;
         elsif L_count = 1 then
            L_order_rec.location := NULL;
            if not CALC_THRESHOLD then
               return FALSE;
            end if;
         end if;
      elsif L_bracket_level = 'L' then
         if L_order_rec.location is not NULL and L_loc_type = 'W' then
            L_order_rec.dept := NULL;
            if not CALC_THRESHOLD then
               return FALSE;
            end if;
         else
            SQL_LIB.SET_MARK('OPEN','C_GET_MULTICHANNEL_LOC','ORDLOC', 'Order No: '|| to_char(I_order_no));
            open C_GET_MULTICHANNEL_LOC;
            SQL_LIB.SET_MARK('FETCH','C_GET_MULTICHANNEL_LOC','ORDLOC', 'Order No: '|| to_char(I_order_no));
            fetch C_GET_MULTICHANNEL_LOC into L_count,
                                              L_order_rec.location;
            SQL_LIB.SET_MARK('CLOSE','C_GET_MULTICHANNEL_LOC','ORDLOC', 'Order No: '|| to_char(I_order_no));
            close C_GET_MULTICHANNEL_LOC;
            if L_count > 1 then
               O_next_bracket := FALSE;
               return TRUE;
            elsif L_count = 1 then
               L_order_rec.dept := NULL;
               if not CALC_THRESHOLD then
                  return FALSE;
               end if;
            end if;
         end if; -- order rec not null
      elsif L_bracket_level = 'A' then
         SQL_LIB.SET_MARK('OPEN','C_GET_DEPT','ORDSKU', 'Order No: '|| to_char(I_order_no));
         open C_GET_DEPT;
         SQL_LIB.SET_MARK('FETCH','C_GET_DEPT','ORDSKU', 'Order No: '|| to_char(I_order_no));
         fetch C_GET_DEPT into L_count,
                                L_order_rec.dept;
         SQL_LIB.SET_MARK('CLOSE','C_GET_DEPT','ORDSKU', 'Order No: '|| to_char(I_order_no));
         close C_GET_DEPT;
         if L_count > 1 then
            O_next_bracket := FALSE;
            return TRUE;
         elsif L_count = 1 then
            SQL_LIB.SET_MARK('OPEN','C_GET_MULTICHANNEL_LOC','ORDLOC', 'Order No: '|| to_char(I_order_no));
            open C_GET_MULTICHANNEL_LOC;
            SQL_LIB.SET_MARK('FETCH','C_GET_MULTICHANNEL_LOC','ORDLOC', 'Order No: '|| to_char(I_order_no));
            fetch C_GET_MULTICHANNEL_LOC into L_count,
                                              L_order_rec.location;
            SQL_LIB.SET_MARK('CLOSE','C_GET_MULTICHANNEL_LOC','ORDLOC', 'Order No: '|| to_char(I_order_no));
            close C_GET_MULTICHANNEL_LOC;
            if L_count > 1 then
               O_next_bracket := FALSE;
               return TRUE;
            elsif L_count = 1 then
               if not CALC_THRESHOLD then
                  return FALSE;
               end if;
            end if;
         end if; -- L_count
      end if; -- bracket type
   end if; -- PO type
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END APPLY_THRESHOLDS;
---------------------------------------------------------------------------------
FUNCTION GET_QUANTITY_ORDERED(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_qty_ord_uom1    IN OUT ORDLOC.QTY_ORDERED%TYPE,
                              O_qty_ord_uom2    IN OUT ORDLOC.QTY_ORDERED%TYPE,
                              O_sup_dept_seq_no IN OUT SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE,
                              I_dept            IN     SUP_BRACKET_COST.DEPT%TYPE,
                              I_location        IN     SUP_BRACKET_COST.LOCATION%TYPE,
                              I_supplier        IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                              I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(60)  := 'ORDER_BRACKET_CALC_SQL.GET_QUANTITY_ORDERED';
   L_prescaled_qty    ORDLOC.QTY_ORDERED%TYPE;
   L_bracket_type1    SUP_INV_MGMT.BRACKET_TYPE1%TYPE;
   L_bracket_uom1     SUP_INV_MGMT.BRACKET_UOM1%TYPE;
   L_bracket_type2    SUP_INV_MGMT.BRACKET_TYPE2%TYPE;
   L_bracket_uom2     SUP_INV_MGMT.BRACKET_UOM2%TYPE;
   L_conv_value       SUP_INV_MGMT.BRACKET_TYPE1%TYPE;
BEGIN
   if not ITEM_BRACKET_COST_SQL.GET_BRACKET(O_error_message,
                                            L_bracket_type1,
                                            L_bracket_uom1,
                                            L_bracket_type2,
                                            L_bracket_uom2,
                                            O_sup_dept_seq_no,
                                            I_supplier,
                                            I_dept,
                                            I_location) then
      return FALSE;
   end if;
   if L_bracket_type1 in ('M', 'V') then
      L_conv_value := L_bracket_uom1;
   elsif L_bracket_type1 = 'E' then
      L_conv_value := 'EA';
   end if;
   if L_bracket_type1 in ('M', 'V', 'E') then
      if not ORDER_ATTRIB_SQL.GET_TOTAL_UOM(I_order_no,
                                            NULL, -- item
                                            NULL, -- location
                                            L_conv_value,
                                            I_supplier,
                                            'O', -- order quantity type
                                            O_qty_ord_uom1,
                                            L_prescaled_qty,
                                            O_error_message) then
         return FALSE;
      end if;
   elsif L_bracket_type1 = 'C' then
      if not ORDER_ATTRIB_SQL.GET_TOTAL_CASES(O_error_message,
                                              O_qty_ord_uom1,
                                              L_prescaled_qty,
                                              I_order_no,
                                              NULL, -- item
                                              NULL /*location*/) then
         return FALSE;
      end if;
   elsif L_bracket_type1 = 'P' then
      if not ORDER_ATTRIB_SQL.GET_TOTAL_PALLETS(O_error_message,
                                                O_qty_ord_uom1,
                                                L_prescaled_qty,
                                                I_order_no,
                                                I_supplier,
                                                NULL, -- item
                                                NULL /*location*/) then
         return FALSE;
      end if;
   elsif L_bracket_type1 = 'S' then
      if not ORDER_ATTRIB_SQL.GET_TOTAL_STATCASE(O_error_message,
                                                 O_qty_ord_uom1,
                                                 L_prescaled_qty,
                                                 I_order_no,
                                                 I_supplier,
                                                 NULL, -- item
                                                 NULL /*location*/) then
         return FALSE;
      end if;
   end if;
   if L_bracket_type2 is not NULL then
      if L_bracket_type2 in ('M', 'V') then
         L_conv_value := L_bracket_uom2;
      elsif L_bracket_type2 = 'E' then
         L_conv_value := 'EA';
      end if;
      if L_bracket_type2 in ('M', 'V', 'E') then
         if not ORDER_ATTRIB_SQL.GET_TOTAL_UOM(I_order_no,
                                               NULL, -- item
                                               NULL, -- location
                                               L_conv_value,
                                               I_supplier,
                                               'O', -- order quantity type
                                               O_qty_ord_uom2,
                                               L_prescaled_qty,
                                               O_error_message) then
            return FALSE;
         end if;
      elsif L_bracket_type2 = 'C' then
         if not ORDER_ATTRIB_SQL.GET_TOTAL_CASES(O_error_message,
                                                 O_qty_ord_uom2,
                                                 L_prescaled_qty,
                                                 I_order_no,
                                                 NULL, -- item
                                                 NULL /*location*/) then
            return FALSE;
         end if;
      elsif L_bracket_type2 = 'P' then
         if not ORDER_ATTRIB_SQL.GET_TOTAL_PALLETS(O_error_message,
                                                   O_qty_ord_uom2,
                                                   L_prescaled_qty,
                                                   I_order_no,
                                                   I_supplier,
                                                   NULL, -- item
                                                   NULL /*location*/) then
            return FALSE;
         end if;
      elsif L_bracket_type2 = 'S' then
         if not ORDER_ATTRIB_SQL.GET_TOTAL_STATCASE(O_error_message,
                                                    O_qty_ord_uom2,
                                                    L_prescaled_qty,
                                                    I_order_no,
                                                    I_supplier,
                                                    NULL, -- item
                                                    NULL /*location*/) then
            return FALSE;
         end if;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_QUANTITY_ORDERED;
---------------------------------------------------------------------------------
END ORDER_BRACKET_CALC_SQL;
/
