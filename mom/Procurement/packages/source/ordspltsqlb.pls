CREATE OR REPLACE PACKAGE BODY SPLIT_WRAPPER_SQL AS

-------------------------------------------------------------------------------
FUNCTION SPLIT_WRAPPER(I_order_no        IN      ORDHEAD.ORDER_NO%TYPE,
                       O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BINARY_INTEGER IS
   
   L_function                   VARCHAR2(40) := 'SPLIT_WRAPPER_SQL.SPLIT_WRAPPER';
   L_order_header               ADD_LINE_ITEM_SQL.ORDHEAD_TBL;
   L_supplier_info              ADD_LINE_ITEM_SQL.SUPPLIER_INFO_REC;
   L_process_info               ADD_LINE_ITEM_SQL.DRIV_CUR_INFO_PROCESS_REC;
   
   L_pool_supp                  ORD_INV_MGMT.POOL_SUPPLIER%TYPE;
   L_file_id                    ORD_INV_MGMT.FILE_ID%TYPE;
   L_split_ind                  NUMBER;
   L_orders_processed           NUMBER := 0;
   L_return                     NUMBER := 0;
   
   
   Cursor C_DRIVER is 
      select 'O',  /* existing order source type */
             oh.supplier,
             NVL(oh.dept,-1),
             oh.status,
             ol.item,
             DECODE(im.pack_ind,'Y', 'P', 'M'),
             ol.location,
             NVL(wh.physical_wh, ol.location),
             NVL(wh.repl_wh_link, ol.location),
             ol.loc_type,
             'Y'  as due_ind,
             'N'  as xdock_ind,
             -1   as xdock_store,
             -1   as contract,
             '-1' as contract_type,
             ol.qty_ordered,
             NVL(ol.last_rounded_qty,0),
             NVL(ol.last_grp_rounded_qty,0),
             os.origin_country_id,
             ol.unit_cost,
             NVL(ol.unit_cost_init, ol.unit_cost),
             1    as supp_lead_time,
             1    as pickup_lead_time,
             ol.non_scale_ind,
             os.supp_pack_size,
             1    as eso,
             0    as aso,
             NVL(ol.tsf_po_link_no, -1),
             NULL as rr_rowid,
             NULL as ir_rowid,
             NULL as bw_rowid,
             NULL as pool_supp,
             NULL as file_id,
             NULL as contract_terms,
             NULL as contract_approval_ind,
             NULL as contract_header_rowid,
             NULL as split_ref_ord_no,
             0    as splitting_increment,
             0    as round_pct,
             0    as model_qty,
             0    as ib_days_to_event,
             oh.import_country_id as loc_country,
             NULL as costing_loc,
             NULL as store_type
        from ordhead oh,
             ordsku os,
             ordloc ol,
             item_master im,
             wh
       where oh.order_no = I_order_no
         and os.order_no = oh.order_no
         and ol.order_no = oh.order_no
         and os.item     = ol.item
         and im.item     = os.item
         and ol.location = wh.wh(+)
       order by ol.item,
                ol.location;
                
BEGIN
   if ADD_LINE_ITEM_SQL.SET_GLOBALS(O_error_message) = FALSE then
      return -1;
   end if;
   
   ADD_LINE_ITEM_SQL.LP_batch_ind := 'N';  
   /* set the inventory mgmt records for the passed in order */ 
   if ADD_LINE_ITEM_SQL.GET_INV_MGMT(L_supplier_info,
                                     I_order_no,
                                     L_pool_supp,
                                     L_file_id,
                                     O_error_message) = FALSE then
      return -1;
   end if;
   SQL_LIB.SET_MARK('OPEN',
                    'C_DRIVER',
                    'ordhead, ordsku, ordloc, item_master, wh',
                    NULL);
   open C_DRIVER;
   LOOP
      SQL_LIB.SET_MARK('FETCH',
                       'C_DRIVER',
                       'ordhead, ordsku, ordloc, item_master, wh',
                       NULL);
      fetch C_DRIVER into L_process_info.source_type,
                          L_process_info.supplier,
                          L_process_info.dept,
                          L_process_info.order_status,
                          L_process_info.item,
                          L_process_info.pack_ind,
                          L_process_info.loc,
                          L_process_info.phy_loc,
                          L_process_info.repl_wh_link,
                          L_process_info.loc_type,
                          L_process_info.due_ind,
                          L_process_info.xdock_ind,
                          L_process_info.xdock_store,
                          L_process_info.contract,
                          L_process_info.contract_type,
                          L_process_info.qty,
                          L_process_info.last_rounded_qty,
                          L_process_info.last_grp_rounded_qty,
                          L_process_info.ctry,
                          L_process_info.unit_cost,
                          L_process_info.unit_cost_init,
                          L_process_info.supp_lead_time,
                          L_process_info.pickup_lead_time,
                          L_process_info.non_scale_ind,
                          L_process_info.pack_size,
                          L_process_info.eso,
                          L_process_info.aso,
                          L_process_info.tsf_po_link,
                          L_process_info.rr_rowid,
                          L_process_info.ir_rowid,
                          L_process_info.bw_rowid,
                          L_process_info.pool_supp,
                          L_process_info.file_id,
                          L_process_info.contract_terms,
                          L_process_info.contract_approval_ind,
                          L_process_info.contract_header_rowid,
                          L_process_info.split_ref_ord_no,
                          L_process_info.splitting_increment,
                          L_process_info.round_pct,
                          L_process_info.model_qty,
                          L_process_info.ib_days_to_event,
                          L_process_info.loc_country,
                          L_process_info.costing_loc,
                          L_process_info.store_type;
      EXIT when C_DRIVER%NOTFOUND;
     
      /* asssign values for pool supplier and file_id */
      L_process_info.pool_supp   := L_pool_supp;
      L_process_info.file_id     := L_file_id;
      L_process_info.cnst_qty(1) := 0;
      L_process_info.cnst_qty(2) := 0;
      L_process_info.cnst_map(1) := 0;
      L_process_info.cnst_map(2) := 0;
      /* add the item/location to an existing order in the order linked lists or
         create a new order for the item/location in the linked list */
      if ADD_LINE_ITEM_SQL.ADD_TO_ORDER(L_order_header,
                                        L_process_info,
                                        L_supplier_info,
                                        I_order_no,
                                        O_error_message) = FALSE then
         return -1;
      end if;
   END LOOP;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_DRIVER',
                    'ordhead, ordsku, ordloc, item_master, wh',
                    NULL);
   close C_DRIVER;
   L_return := SPLIT_ORDER_SQL.SPLIT_PO(L_order_header,
                                        L_split_ind,
                                        O_error_message);
   
   if L_return <> 0 then
      return L_return;
   end if;

   if L_split_ind <> 0 then
      if CREATE_PO_LIB_SQL.DO_INSERTS(L_order_header,
                                      L_orders_processed,
                                      O_error_message)  = FALSE then
         return -1;
      end if;

      if CREATE_PO_LIB_SQL.UPDATE_GROUP_ROUNDED_QTY(L_order_header,
                                                    O_error_message) = FALSE then
         return -1;
      end if;

      if SPLIT_ORDER_SQL.DELETE_ORDER(I_order_no,
                                      O_error_message) = FALSE then
         return -1;
      end if;
   end if;

   return 0;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return -1;
END SPLIT_WRAPPER;
-------------------------------------------------------------------------------
END SPLIT_WRAPPER_SQL;
/