CREATE OR REPLACE PACKAGE ELC_CALC_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name: CALC_COMP
--Purpose      : To calculate all expenses and assessments.
-------------------------------------------------------------------------------
FUNCTION CALC_COMP(O_error_message     IN OUT VARCHAR2,
                   I_calc_type         IN     VARCHAR2,
                   I_item              IN     ITEM_MASTER.ITEM%TYPE,
                   I_supplier          IN     SUPS.SUPPLIER%TYPE,
                   I_item_exp_type     IN     ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                   I_item_exp_seq      IN     ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                   I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                   I_ord_seq_no        IN     ORDLOC_EXP.SEQ_NO%TYPE,
                   I_pack_item         IN     ORDLOC_EXP.PACK_ITEM%TYPE,
                   I_zone_id           IN     COST_ZONE.ZONE_ID%TYPE,
                   I_hts               IN     HTS.HTS%TYPE,
                   I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                   I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                   I_effect_from       IN     HTS.EFFECT_FROM%TYPE,
                   I_effect_to         IN     HTS.EFFECT_TO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
--Function Name: CALC_COMP
--Purpose      : To calculate expenses and assessments
--               package is Overloaded with this function to calculate the component 
--               expenses for a particular order location.Added a new parameter 
--               I_import_ord_ind to put a check to access the ordsku_hts and ordsku_hts_assess 
--               for Import Order only.
---------------------------------------------------------------------------------------- 
FUNCTION CALC_COMP(O_error_message     IN OUT VARCHAR2,
                   I_calc_type         IN     VARCHAR2,
                   I_item              IN     ITEM_MASTER.ITEM%TYPE,
                   I_supplier          IN     SUPS.SUPPLIER%TYPE,
                   I_item_exp_type     IN     ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                   I_item_exp_seq      IN     ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                   I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                   I_ord_seq_no        IN     ORDLOC_EXP.SEQ_NO%TYPE,
                   I_pack_item         IN     ORDLOC_EXP.PACK_ITEM%TYPE,
                   I_zone_id           IN     COST_ZONE.ZONE_ID%TYPE,
                   I_location          IN     ORDLOC.LOCATION%TYPE,
                   I_hts               IN     HTS.HTS%TYPE,
                   I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                   I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                   I_effect_from       IN     HTS.EFFECT_FROM%TYPE,
                   I_effect_to         IN     HTS.EFFECT_TO%TYPE,
                   I_import_ord_ind    IN     ORDHEAD.IMPORT_ORDER_IND%TYPE DEFAULT NULL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Function Name: UPDATE_TARIFF_RATES
--Purpose      : Internal function called by CALC_COMP used to calculate all
--               of the best tariff treatment rates to be used in the calculation
--               of the assessments.
----------------------------------------------------------------------------------------
FUNCTION UPDATE_TARIFF_RATES(O_error_message     IN OUT VARCHAR2,
                             I_calc_type         IN     VARCHAR2,
                             I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                             I_seq_no            IN     ORDSKU_HTS.SEQ_NO%TYPE,
                             I_item              IN     ITEM_MASTER.ITEM%TYPE,
                             I_pack_item         IN     ITEM_MASTER.ITEM%TYPE,
                             I_supplier          IN     SUPS.SUPPLIER%TYPE,
                             I_hts               IN     HTS.HTS%TYPE,
                             I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_effect_from       IN     HTS.EFFECT_FROM%TYPE,
                             I_effect_to         IN     HTS.EFFECT_TO%TYPE,
                             I_comp_id           IN     ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CALC_TOTALS
-- Purpose      : Calculates the Total Expense, Total Duty, and Total Estimated
--                Landed Cost values for a particular Item, Item/Supplier/Origin
--                Country combination, Order/Item/Location or Order/Item/Zone combination.
--                If getting the totals for an Item, must pass in the Item.
--                If supplier is NULL, the totals will be gotten for the Item's primary
--                supplier.
--                If getting the totals for an order, need to pass in
--                the Order No and the Zone (or the Location).
--                If the origin country is passed in as null the primary origin country
--                for the supplier will be used.
--                If import country is passed in as null, the base country on
--                system options will be used.
--                If getting the totals for a specific component item of
--                a buyer pack, pass the pack no in I_item and the component item in
--                I_comp_item.
--                The Total ELC is always passed out of the function in Primary
--                currency.
--                The total expense and total duty values are passed out in their
--                individual component currencies. Also the expense component's
--                exchange rate is passed out (these are passed out of the
--                function for easy conversion by the calling module if necessary).
--                If a value is passed in the I_cost parameter, it is expected to
--                be in Supplier currency if I_order_no is NULL, or Order currency
--                if I_order_no is not NULL.
--*************************************************************************************
--                The parameters that are NUMBER types, need to be that way for accuracy 
--                in rounding, DO NOT type-cast them.
--*************************************************************************************
----------------------------------------------------------------------------------------
FUNCTION CALC_TOTALS(O_error_message     IN OUT VARCHAR2,
                     O_total_elc         IN OUT NUMBER,
                     O_total_exp         IN OUT NUMBER,
                     O_exp_currency      IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
                     O_exchange_rate_exp IN OUT CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                     O_total_dty         IN OUT NUMBER,
                     O_dty_currency      IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
                     I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                     I_item              IN     ITEM_MASTER.ITEM%TYPE,
                     I_comp_item         IN     ITEM_MASTER.ITEM%TYPE,
                     I_zone_id           IN     COST_ZONE.ZONE_ID%TYPE,
                     I_location          IN     ORDLOC.LOCATION%TYPE,
                     I_supplier          IN     SUPS.SUPPLIER%TYPE,
                     I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                     I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                     I_cost              IN     ORDLOC.UNIT_COST%TYPE,
                     I_active_date       IN     FUTURE_COST.ACTIVE_DATE%TYPE DEFAULT NULL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CALC_BACKHAUL_TOTAL
-- Purpose      : Calculates the Total Backhaul Allowance for a particular Order.
----------------------------------------------------------------------------------------
FUNCTION CALC_BACKHAUL_TOTAL(O_error_message         IN OUT VARCHAR2,
                             O_total_allowance_prim  IN OUT ORDLOC_EXP.EST_EXP_VALUE%TYPE,
                             O_total_allowance_ord   IN OUT ORDLOC_EXP.EST_EXP_VALUE%TYPE,
                             I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                             I_currency_prim         IN     CURRENCIES.CURRENCY_CODE%TYPE,
                             I_currency_ord          IN     CURRENCIES.CURRENCY_CODE%TYPE,
                             I_exchange_rate_ord     IN     CURRENCY_RATES.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name : CALC_ORDER_TOTAL
-- Purpose : Calcates the ELC, EXPENSES and DUTY of an order, this function is called from ordcalc.
----------------------------------------------------------------------------------------------------
FUNCTION CALC_ORDER_TOTALS(O_error_message  IN OUT VARCHAR2,
                           O_total_elc         IN OUT NUMBER,
                           O_total_exp         IN OUT NUMBER,
                           O_exp_currency      IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
                           O_exchange_rate_exp IN OUT CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                           O_total_dty         IN OUT NUMBER,
                           O_dty_currency      IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
                           I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                           I_item              IN     ITEM_MASTER.ITEM%TYPE,
                           I_pack_ind          IN     ITEM_MASTER.PACK_IND%TYPE,
                           I_sellable_ind      IN     ITEM_MASTER.SELLABLE_IND%TYPE,
                           I_orderable_ind     IN     ITEM_MASTER.ORDERABLE_IND%TYPE,
                           I_pack_type         IN     ITEM_MASTER.PACK_TYPE%TYPE,
                           I_qty_ordered IN ORDLOC.QTY_ORDERED%TYPE,
                           I_comp_item         IN     ITEM_MASTER.ITEM%TYPE,
                           I_zone_id           IN     COST_ZONE.ZONE_ID%TYPE,
                           I_location          IN     ORDLOC.LOCATION%TYPE,
                           I_supplier          IN     SUPS.SUPPLIER%TYPE,
                           I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                           I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                           I_cost              IN     ORDLOC.UNIT_COST%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name : RECALC_EXP
-- Purpose : To Recalcates the ELC, EXPENSES and ASSESMENT for Country level when the cost of the Item is modified. 
----------------------------------------------------------------------------------------------------
FUNCTION RECALC_EXP(O_error_message       IN OUT   VARCHAR2,
                    I_item                IN       ITEM_MASTER.ITEM%TYPE,
                    I_supplier            IN       SUPS.SUPPLIER%TYPE,
                    I_origin_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
					I_cascade_children    IN       VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
END ELC_CALC_SQL;
/


