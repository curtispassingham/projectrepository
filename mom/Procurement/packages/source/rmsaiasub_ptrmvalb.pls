CREATE OR REPLACE PACKAGE BODY RMSAIASUB_PAYTERM_VALIDATE AS
--------------------------------------------------------------------------------

GP_system_options  SYSTEM_OPTIONS%ROWTYPE;
GP_message         "RIB_PayTermDesc_REC";
GP_message_type    VARCHAR2(15);


--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION CHECK_ENABLED(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION CHECK_TERMS_HEAD(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION CHECK_TERMS_DETAIL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message  IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                         O_paytermref_rec IN OUT         "RIB_PayTermRef_REC",
                         O_payterm_rec       OUT NOCOPY  TERMS_SQL.PAYTERM_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- PUBLIC
--------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_paytermref_rec IN OUT  "RIB_PayTermRef_REC",
                       O_payterm_rec    IN OUT  TERMS_SQL.PAYTERM_REC,
                       I_message        IN      "RIB_PayTermDesc_REC",
                       I_message_type   IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'RMSAIASUB_PAYTERM_VALIDATE.CHECK_MESSAGE';

BEGIN

   -- Populate pacakge variables
   GP_message      := I_message;
   GP_message_type := I_message_type;

   if SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            GP_system_options) = FALSE then
      return FALSE;
   end if;

   if CHECK_REQUIRED_FIELDS(O_error_message) = FALSE then
      return FALSE;
   end if;

   if CHECK_ENABLED(O_error_message) = FALSE then
      return FALSE;
   end if;

   if CHECK_TERMS_HEAD(O_error_message) = FALSE then
         return FALSE;
   end if;

   if CHECK_TERMS_DETAIL(O_error_message) = FALSE then
         return FALSE;
   end if;

   if POPULATE_RECORD(O_error_message,
                      O_paytermref_rec,
                      O_payterm_rec) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_MESSAGE;

--------------------------------------------------------------------------------
-- PRIVATE
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- CHECK_REQUIRED_FIELDS: Check all required fields for the create and modify
-- messages to ensure that they are not null.
--------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'RMSAIASUB_PAYTERM_VALIDATE.CHECK_REQUIRED_FIELDS';
   L_required        VARCHAR2(30) := NULL;

BEGIN

   if GP_message.terms is NULL then
      if GP_message_type != RMSAIASUB_PAYTERM.HDR_ADD then
         L_required := 'terms';
      end if;
   elsif GP_message.terms_code is NULL then
      L_required := 'terms_code';
   elsif GP_message.terms_desc is NULL then
      L_required := 'terms_desc';
   elsif GP_message.rank is NULL then
      L_required := 'rank';
   elsif GP_message_type = RMSAIASUB_PAYTERM.HDR_ADD
   and (GP_system_options.financial_ap = 'A'
      and GP_message.terms_xref_key is NULL) then
      L_required := 'term_xref_key';
   end if;
   if L_required is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', L_required);
      return FALSE;
   end if;

   if GP_message_type != RMSAIASUB_PAYTERM.HDR_UPD then

      if GP_message.paytermdtl_tbl is NULL
      or GP_message.paytermdtl_tbl.COUNT = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC');
         return FALSE;
      end if;

      FOR i in GP_message.paytermdtl_tbl.first..GP_message.paytermdtl_tbl.last LOOP

         if GP_message.paytermdtl_tbl(i).due_max_amount is NULL then
            L_required := 'Due Max Amount';
         elsif GP_message.paytermdtl_tbl(i).percent is NULL then
            L_required := 'Percent';
         elsif GP_message.paytermdtl_tbl(i).enabled_flag is NULL then
            L_required := 'Enabled Flag';
         end if;
         if L_required is NOT NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', L_required);
            return FALSE;
         end if;

         if GP_message.paytermdtl_tbl(i).due_days is NULL
         and GP_message.paytermdtl_tbl(i).due_dom is NOT NULL
         and GP_message.paytermdtl_tbl(i).due_mm_fwd is NOT NULL then
            NULL; -- VALID
         elsif GP_message.paytermdtl_tbl(i).due_days is NOT NULL
         and GP_message.paytermdtl_tbl(i).due_dom is NULL
         and GP_message.paytermdtl_tbl(i).due_mm_fwd is NULL then
            NULL; -- VALID
         elsif GP_message.paytermdtl_tbl(i).due_days is NOT NULL
         and GP_message.paytermdtl_tbl(i).due_dom is NOT NULL
         and GP_message.paytermdtl_tbl(i).due_mm_fwd is NOT NULL then
            NULL; -- VALID
         else
            -- NOT VALID
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_DUE_DAYS_DATA');
            return false;
         end if;

         if GP_message.paytermdtl_tbl(i).disc_dom is NOT NULL
         and GP_message.paytermdtl_tbl(i).disc_mm_fwd is NOT NULL
         and GP_message.paytermdtl_tbl(i).discdays is NULL then
            NULL; -- VALID
         elsif GP_message.paytermdtl_tbl(i).disc_dom is NULL
         and GP_message.paytermdtl_tbl(i).disc_mm_fwd is NULL
         and GP_message.paytermdtl_tbl(i).discdays is NOT NULL then
            NULL; -- VALID
         elsif GP_message.paytermdtl_tbl(i).disc_dom is NULL
         and GP_message.paytermdtl_tbl(i).disc_mm_fwd is NULL
         and GP_message.paytermdtl_tbl(i).discdays is NULL then
            NULL; -- VALID
         elsif GP_message.paytermdtl_tbl(i).disc_dom is NOT NULL
         and GP_message.paytermdtl_tbl(i).disc_mm_fwd is NOT NULL
         and GP_message.paytermdtl_tbl(i).discdays is NOT NULL then
            NULL; -- VALID
         else
            -- NOT VALID
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_DISC_DAYS_DATA');
            return FALSE;
         end if;

      end LOOP;

   end if;  -- GP_message_type <> RMSAIASUB_PAYTERM.HDR_UPD

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_REQUIRED_FIELDS;

--------------------------------------------------------------------------------
-- CHECK_ENABLED: Verify that the enabled_flag is set correctly for the
-- available activate range.
--------------------------------------------------------------------------------
FUNCTION CHECK_ENABLED(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50)      := 'RMSAIASUB_PAYTERM_VALIDATE.CHECK_ENABLED';
   L_vdate              PERIOD.VDATE%TYPE := GET_VDATE;
   L_start_date_active  TERMS_DETAIL.START_DATE_ACTIVE%TYPE;
   L_end_date_active    TERMS_DETAIL.START_DATE_ACTIVE%TYPE;
   PROGRAM_ERROR        EXCEPTION;

   -- Message date takes precedence over date from table.
   -- If both are NULL then use vdate.
   CURSOR c_get_daterange (indx  PLS_INTEGER) IS
      select NVL(NVL(GP_message.paytermdtl_tbl(indx).start_date_active, start_date_active), L_vdate),
             NVL(NVL(GP_message.paytermdtl_tbl(indx).end_date_active, end_date_active), L_vdate)
        from terms_detail
       where terms = GP_message.terms
         and terms_seq = GP_message.paytermdtl_tbl(indx).terms_seq;

BEGIN

   if GP_message_type != RMSAIASUB_PAYTERM.HDR_UPD then

   FOR i in GP_message.paytermdtl_tbl.first..GP_message.paytermdtl_tbl.last LOOP

      open c_get_daterange(i);
      fetch c_get_daterange into L_start_date_active,
                                 L_end_date_active;
      close c_get_daterange;

      if L_start_date_active > L_end_date_active then
         raise PROGRAM_ERROR;
      end if;

      -- If vdate is between start and end date then enabled flag must be Y
      if L_vdate between L_start_date_active and L_end_date_active
      and GP_message.paytermdtl_tbl(i).enabled_flag = 'N' then
         raise PROGRAM_ERROR;
      -- If vdate is greater than end date then enabled flag must be N
      elsif L_vdate > L_end_date_active
      and GP_message.paytermdtl_tbl(i).enabled_flag = 'Y' then
         raise PROGRAM_ERROR;
      -- If vdate is less than start date...
      elsif L_vdate < L_start_date_active then
         -- ...and AIA financials are integrated...
         if GP_system_options.financial_ap = 'A' then
            -- ...then enabled flag must be Y to allow for future payterms.
            if GP_message.paytermdtl_tbl(i).enabled_flag = 'N' then
               raise PROGRAM_ERROR;
            end if;
         -- ...and AIA financials are NOT integrated then enabled flag must be N.
         elsif GP_message.paytermdtl_tbl(i).enabled_flag = 'Y' then
            raise PROGRAM_ERROR;
         end if;
      end if;
   end loop;
   end if;  -- GP_message_type <> RMSAIASUB_PAYTERM.HDR_UPD

   return TRUE;

EXCEPTION
   when PROGRAM_ERROR then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ENABLED_FLAG');
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ENABLED;

--------------------------------------------------------------------------------
-- CHECK_TERMS_HEAD: Checks existence of terms_head records before creating
-- detail record.
--------------------------------------------------------------------------------
FUNCTION CHECK_TERMS_HEAD(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'RMSAIASUB_PAYTERM_VALIDATE.CHECK_TERMS_HEAD';

BEGIN

   if GP_message_type in (RMSAIASUB_PAYTERM.HDR_UPD, RMSAIASUB_PAYTERM.DTL_ADD) then
      if TERMS_SQL.HEADER_EXISTS(O_error_message,
                                 GP_message.terms) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
      return FALSE;
END CHECK_TERMS_HEAD;

--------------------------------------------------------------------------------
-- CHECK_TERMS_DETAIL: Checks existence of terms_head and terms_detail records
-- before updating detail record.
--------------------------------------------------------------------------------
FUNCTION CHECK_TERMS_DETAIL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'RMSAIASUB_PAYTERM_VALIDATE.CHECK_TERMS_DETAIL';

BEGIN

   if GP_message_type = RMSAIASUB_PAYTERM.DTL_UPD then
      FOR i in GP_message.paytermdtl_tbl.first..GP_message.paytermdtl_tbl.last LOOP
         if TERMS_SQL.DETAIL_EXISTS(O_error_message,
                                    GP_message.terms,
                                    GP_message.paytermdtl_tbl(i).terms_seq) = FALSE then
            return FALSE;
         end if;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
      return FALSE;
END CHECK_TERMS_DETAIL;

--------------------------------------------------------------------------------
-- POPULATE_RECORD: Populate PAYTERM_REC with the values from the create and
-- modify RIB messages.
--------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message  IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                         O_paytermref_rec IN OUT         "RIB_PayTermRef_REC",
                         O_payterm_rec       OUT NOCOPY  TERMS_SQL.PAYTERM_REC)
RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'RMSAIASUB_PAYTERM_VALIDATE.POPULATE_RECORD';
   L_terms    TERMS_HEAD.TERMS%TYPE;
   L_payterms_seq_TBL "RIB_TermsSeq_TBL";

BEGIN
   -- If this is a header add and AIA financials are integrated then
   -- get the next terms value from the sequence and use that.
   if GP_message_type = RMSAIASUB_PAYTERM.HDR_ADD
   and GP_system_options.financial_ap = 'A' then
      if TERMS_SQL.GET_NEXT_TERMS_VAL(O_error_message,
                                      L_terms) = FALSE then
         return FALSE;
      end if;
   -- Otherwise, use the terms value from the message.
   else
      L_terms := GP_message.terms;
   end if;
   O_payterm_rec.terms_head_row.terms          := L_terms;
   O_payterm_rec.terms_code                    := GP_message.terms_code;
   O_payterm_rec.terms_desc                    := GP_message.terms_desc;
   O_payterm_rec.terms_head_row.rank           := GP_message.rank;

   if GP_message_type != RMSAIASUB_PAYTERM.HDR_UPD then
      -- Initialize table
      O_payterm_rec.terms_details := TERMS_SQL.TERMS_DETAIL_TBL();
      L_payterms_seq_TBL := "RIB_TermsSeq_TBL"();
      FOR i in GP_message.paytermdtl_tbl.first..GP_message.paytermdtl_tbl.last LOOP
         O_payterm_rec.terms_details.EXTEND;
         O_payterm_rec.terms_details(i).terms             := L_terms;
         O_payterm_rec.terms_details(i).terms_seq         := GP_message.paytermdtl_tbl(i).terms_seq;
         O_payterm_rec.terms_details(i).duedays           := GP_message.paytermdtl_tbl(i).due_days;
         O_payterm_rec.terms_details(i).due_max_amount    := GP_message.paytermdtl_tbl(i).due_max_amount;
         O_payterm_rec.terms_details(i).due_dom           := GP_message.paytermdtl_tbl(i).due_dom;
         O_payterm_rec.terms_details(i).due_mm_fwd        := GP_message.paytermdtl_tbl(i).due_mm_fwd;
         O_payterm_rec.terms_details(i).discdays          := GP_message.paytermdtl_tbl(i).discdays;
         O_payterm_rec.terms_details(i).percent           := GP_message.paytermdtl_tbl(i).percent;
         O_payterm_rec.terms_details(i).disc_dom          := GP_message.paytermdtl_tbl(i).disc_dom;
         O_payterm_rec.terms_details(i).disc_mm_fwd       := GP_message.paytermdtl_tbl(i).disc_mm_fwd;
         O_payterm_rec.terms_details(i).cutoff_day        := GP_message.paytermdtl_tbl(i).cutoff_day;
         O_payterm_rec.terms_details(i).fixed_date        := GP_message.paytermdtl_tbl(i).fixed_date;
         O_payterm_rec.terms_details(i).enabled_flag      := GP_message.paytermdtl_tbl(i).enabled_flag;
         O_payterm_rec.terms_details(i).start_date_active := GP_message.paytermdtl_tbl(i).start_date_active;
         O_payterm_rec.terms_details(i).end_date_active   := GP_message.paytermdtl_tbl(i).end_date_active;
         
         L_payterms_seq_TBL.EXTEND;
         L_payterms_seq_TBL(i) := "RIB_TermsSeq_REC"(0,GP_message.paytermdtl_tbl(i).terms_seq);
      end LOOP;
   end if;
   -- If financials integrated with AIA then populate PayTermRef_REC.
   if GP_system_options.financial_ap = 'A' then
      O_paytermref_rec := "RIB_PayTermRef_REC"(0,
                                               GP_message.terms_xref_key,
                                               L_terms,
                                               L_payterms_seq_TBL);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_RECORD;

--------------------------------------------------------------------------------
END RMSAIASUB_PAYTERM_VALIDATE;

/
