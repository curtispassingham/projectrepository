CREATE OR REPLACE PACKAGE BODY RMSMFM_ORDER AS
--------------------------------------------------------
--------------------------------------------------------

-----------------------------------------------------------------------

-----------------------------------------------------------------------
--Globals
TYPE rowid_TBL  is table of ROWID INDEX BY BINARY_INTEGER;

TYPE odp_order_no_TBL  is table of order_details_published.order_no%TYPE INDEX BY BINARY_INTEGER;
TYPE odp_item_TBL  is table of order_details_published.item%TYPE INDEX BY BINARY_INTEGER;
TYPE odp_location_TBL  is table of order_details_published.location%TYPE INDEX BY BINARY_INTEGER;
TYPE odp_loc_type_TBL  is table of order_details_published.loc_type%TYPE INDEX BY BINARY_INTEGER;
TYPE odp_physical_location_TBL  is table of order_details_published.physical_location%TYPE INDEX BY BINARY_INTEGER;

LP_num_threads NUMBER(4):=NULL;
LP_max_count NUMBER(4):=NULL;

-- depending upon the message type, there will come a point in the publication process
-- when error retry will no longer be possible.  this variable will track whether or
-- not that point has been reached.
LP_error_status varchar2(1):=NULL;

-----------------------------------------------------------------------
--Local Specs

---
FUNCTION PROCESS_QUEUE_RECORD(O_error_message         OUT        VARCHAR2,
                              O_break_loop            OUT        BOOLEAN,
                              O_message           IN  OUT nocopy RIB_OBJECT,
                              O_routing_info      IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id        IN  OUT nocopy RIB_BUSOBJID_TBL,
                              O_message_type      IN  OUT        VARCHAR2,
                              O_delete_rowid_ind  IN  OUT        VARCHAR2,
                              I_order_no          IN             order_mfqueue.order_no%TYPE,
                              I_hdr_published     IN             order_pub_info.published%TYPE,
                              I_item              IN             order_mfqueue.item%TYPE,
                              I_location          IN             order_mfqueue.location%TYPE,
                              I_loc_type          IN             order_mfqueue.loc_type%TYPE,
                              I_physical_location IN             order_mfqueue.physical_location%TYPE,
                              I_pub_status        IN             order_mfqueue.pub_status%TYPE,
                              I_seq_no            IN             order_mfqueue.seq_no%TYPE,
                              I_rowid             IN             ROWID)
RETURN BOOLEAN;
---
FUNCTION MAKE_CREATE(O_error_message    IN OUT VARCHAR2,
                     O_message          IN OUT nocopy RIB_OBJECT,
                     O_routing_info     IN OUT nocopy RIB_ROUTINGINFO_TBL,
                     O_delete_rowid_ind IN OUT VARCHAR2,
                     I_order_no         IN     ordhead.order_no%TYPE,
                     I_seq_no           IN     order_mfqueue.seq_no%TYPE,
                     I_max_details      IN     rib_settings.max_details_to_publish%TYPE,
                     I_rowid            IN     ROWID)
RETURN BOOLEAN;
---
FUNCTION DELETE_QUEUE_REC(O_text      OUT VARCHAR2,
                          I_seq_no IN     order_mfqueue.seq_no%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_HEADER_OBJECT(O_error_message   IN OUT          VARCHAR2,
                             O_message         IN OUT NOCOPY   RIB_OBJECT,
                             O_supplier        IN OUT          ORDHEAD.SUPPLIER%TYPE,
                             I_order_no        IN              ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---

------ If to_loc is passed as NULL all detail records will be returned.  If
------ to_loc is passed only that detail will be returned.
FUNCTION BUILD_DETAIL_OBJECTS(O_error_message         IN OUT          VARCHAR2,
                              O_message               IN OUT NOCOPY   "RIB_PODtl_TBL",
                              O_order_mfqueue_rowid   IN OUT NOCOPY   ROWID_TBL,
                              O_order_mfqueue_size    IN OUT          BINARY_INTEGER,
                              O_routing_info          IN OUT NOCOPY   RIB_ROUTINGINFO_TBL,
                              O_delete_rowid_ind      IN OUT          VARCHAR2,
                              IO_message_type         IN OUT          ORDER_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_supplier              IN              ORDHEAD.SUPPLIER%TYPE,
                              I_order_no              IN              ORDHEAD.ORDER_NO%TYPE,
                              I_item                  IN              ORDER_MFQUEUE.ITEM%TYPE,
                              I_physical_location     IN              ORDER_MFQUEUE.PHYSICAL_LOCATION%TYPE,
                              I_max_details           IN              RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_SINGLE_DETAIL ( O_error_message                IN OUT          VARCHAR2,
                               O_message                      IN OUT NOCOPY   "RIB_PODtl_TBL",
                               IO_order_mfqueue_rowid         IN OUT NOCOPY   ROWID_TBL,
                               IO_order_mfqueue_size          IN OUT          BINARY_INTEGER,
                               IO_routing_info                IN OUT NOCOPY   RIB_ROUTINGINFO_TBL,
                               IO_rib_podtl_rec               IN OUT NOCOPY   "RIB_PODtl_REC",
                               IO_rib_povirtualdtl_rec        IN OUT NOCOPY   "RIB_POVirtualDtl_REC",
                               IO_rib_povirtualdtl_tbl        IN OUT NOCOPY   "RIB_POVirtualDtl_TBL",
                               IO_rib_routing_rec             IN OUT NOCOPY   RIB_ROUTINGINFO_REC,
                               IO_prev_item                   IN OUT          ORDLOC.ITEM%TYPE,
                               IO_prev_location               IN OUT          ORDLOC.LOCATION%TYPE,
                               IO_prev_physical_loc           IN OUT          ORDLOC.LOCATION%TYPE,
                               IO_physical_qty                IN OUT          ORDLOC.QTY_ORDERED%TYPE,
                               IO_odp_ins_order_no            IN OUT NOCOPY   ODP_ORDER_NO_TBL,
                               IO_odp_ins_item                IN OUT NOCOPY   ODP_ITEM_TBL,
                               IO_odp_ins_location            IN OUT NOCOPY   ODP_LOCATION_TBL,
                               IO_odp_ins_loc_type            IN OUT NOCOPY   ODP_LOC_TYPE_TBL,
                               IO_odp_ins_physical_location   IN OUT NOCOPY   ODP_PHYSICAL_LOCATION_TBL,
                               IO_odp_ins_size                IN OUT          BINARY_INTEGER,
                               IO_odp_upd_rowid               IN OUT NOCOPY   ROWID_TBL,
                               IO_odp_upd_size                IN OUT          BINARY_INTEGER,
                               I_detail_exists_ind            IN              VARCHAR2,
                               IO_extend                      IN OUT          BOOLEAN,
                               I_item                         IN              ORDLOC.ITEM%TYPE,
                               I_location                     IN              ORDLOC.LOCATION%TYPE,
                               I_loc_type                     IN              ORDLOC.LOC_TYPE%TYPE,
                               I_physical_location            IN              ORDLOC.LOCATION%TYPE,
                               I_physical_store_type          IN              STORE.STORE_TYPE%TYPE,
                               I_physical_stockholding_ind    IN              STORE.STOCKHOLDING_IND%TYPE,
                               I_unit_cost                    IN              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                               I_qty_ordered                  IN              ORDLOC.QTY_ORDERED%TYPE,
                               I_tsf_po_link_no               IN              ORDLOC.TSF_PO_LINK_NO%TYPE,
                               I_estimated_instock_date       IN              ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE,
                               I_ref_item                     IN              ORDSKU.REF_ITEM%TYPE,
                               I_supp_pack_size               IN              ORDSKU.SUPP_PACK_SIZE%TYPE,
                               I_origin_country_id            IN              ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                               I_earliest_ship_date           IN              ORDSKU.EARLIEST_SHIP_DATE%TYPE,
                               I_latest_ship_date             IN              ORDSKU.LATEST_SHIP_DATE%TYPE,
                               I_pickup_loc                   IN              ORDSKU.PICKUP_LOC%TYPE,
                               I_pickup_no                    IN              ORDSKU.PICKUP_NO%TYPE,
                               I_round_lvl                    IN              ITEM_SUPP_COUNTRY.ROUND_LVL%TYPE,
                               I_packing_method               IN              ITEM_SUPP_COUNTRY.PACKING_METHOD%TYPE,
                               I_oq_rowid                     IN              ROWID,
                               I_odp_rowid                    IN              ROWID,
                               I_order_no                     IN              ORDHEAD.ORDER_NO%TYPE,
                               O_delete_rowid_ind             IN OUT          VARCHAR2,
                               IO_details_processed           IN OUT          RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE,
                               I_max_details                  IN              RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE,
                               I_transaction_uom              IN              ORDCUST_DETAIL.TRANSACTION_UOM%TYPE DEFAULT NULL)
RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_CHANGE_OBJECTS(O_error_message     IN OUT VARCHAR2,
                                     O_message           IN OUT nocopy RIB_OBJECT,
                                     O_routing_info      IN OUT nocopy RIB_ROUTINGINFO_TBL,
                                     I_supplier          IN     ordhead.supplier%TYPE,
                                     IO_message_type     IN OUT order_mfqueue.message_type%TYPE,
                                     I_order_no          IN     ordhead.order_no%TYPE,
                                     I_item              IN     order_mfqueue.item%TYPE,
                                     I_physical_location IN     order_mfqueue.physical_location%TYPE,
                                     I_max_details       IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_DELETE(O_error_message     IN OUT        VARCHAR2,
                             O_message_type      IN OUT        VARCHAR2,
                             O_message           IN OUT nocopy RIB_OBJECT,
                             O_break_loop        IN OUT        BOOLEAN,
                             I_order_no          IN            ordhead.order_no%TYPE,
                             I_item              IN            ordloc.item%TYPE,
                             I_location          IN            ordloc.location%TYPE,
                             I_loc_type          IN            ordloc.loc_type%TYPE,
                             I_physical_location IN            ordloc.location%TYPE,
                             I_rowid             IN            ROWID)
RETURN BOOLEAN;
---
FUNCTION BUILD_DETAIL_DELETE_WH(O_error_message        IN OUT        VARCHAR2,
                                O_message_type         IN OUT        VARCHAR2,
                                O_rib_podtl_tbl        IN OUT nocopy "RIB_PODtl_TBL",
                                O_order_mfqueue_rowid  IN OUT nocopy rowid_TBL,
                                O_order_mfqueue_size   IN OUT        BINARY_INTEGER,
                                O_odp_rowid            IN OUT nocopy rowid_TBL,
                                O_odp_size             IN OUT        BINARY_INTEGER,
                                I_order_no             IN            ordhead.order_no%TYPE,
                                I_supplier             IN            ordhead.supplier%TYPE,
                                I_item                 IN            ordloc.item%TYPE,
                                I_physical_location    IN            ordloc.location%TYPE)
RETURN BOOLEAN;
---
FUNCTION ROUTING_INFO_ADD(O_error_message         OUT        VARCHAR2,
                          O_routing_info      IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                          I_physical_location IN             ordloc.location%TYPE,
                          I_loc_type          IN             ordloc.loc_type%TYPE)
RETURN BOOLEAN;
---
FUNCTION GET_ROUTING_TO_LOCS(O_error_message IN OUT        VARCHAR2,
                             I_order_no      IN            ordhead.order_no%TYPE,
                             O_routing_info  IN OUT nocopy RIB_ROUTINGINFO_TBL)
RETURN BOOLEAN;
---
FUNCTION GET_MSG_HEADER(O_error_message     OUT VARCHAR2,
                        O_ordhead_struct IN OUT ordhead_msg_rectype)
RETURN BOOLEAN;
---
FUNCTION LOCK_THE_BLOCK(O_error_msg        OUT VARCHAR2,
                        O_queue_locked     OUT BOOLEAN,
                        I_order_no      IN     order_mfqueue.order_no%TYPE)
RETURN BOOLEAN;
---
PROCEDURE HANDLE_ERRORS(O_status_code       IN OUT         VARCHAR2,
                        O_error_message     IN OUT         VARCHAR2,
                        O_message           IN OUT  nocopy RIB_OBJECT,
                        O_bus_obj_id        IN OUT  nocopy RIB_BUSOBJID_TBL,
                        O_routing_info      IN OUT  nocopy RIB_ROUTINGINFO_TBL,
                        I_seq_no            IN             order_mfqueue.seq_no%TYPE,
                        I_order_no          IN             order_mfqueue.order_no%TYPE,
                        I_item              IN             order_mfqueue.item%TYPE,
                        I_physical_location IN             order_mfqueue.physical_location%TYPE,
                        I_loc_type          IN             order_mfqueue.loc_type%TYPE);
---
-----------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_message         OUT VARCHAR2,
                I_message_type          IN  ORDER_MFQUEUE.MESSAGE_TYPE%TYPE,
                I_order_no              IN  ORDHEAD.ORDER_NO%TYPE,
                I_order_type            IN  ORDHEAD.ORDER_TYPE%TYPE,
                I_order_header_status   IN  ORDHEAD.STATUS%TYPE,
                I_supplier              IN  ORDHEAD.SUPPLIER%TYPE,
                I_item                  IN  ORDLOC.ITEM%TYPE,
                I_location              IN  ORDLOC.LOCATION%TYPE,
                I_loc_type              IN  ORDLOC.LOC_TYPE%TYPE,
                I_physical_location     IN  ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'RMSMFM_ORDER.ADDTOQ';
   L_status_code        VARCHAR2(1) := NULL;

   L_max_details        rib_settings.max_details_to_publish%TYPE:=NULL;
   L_thread_no          rib_settings.num_threads%TYPE:=NULL;
   L_num_threads        rib_settings.num_threads%TYPE:=NULL;
   L_minutes_time_lag   rib_settings.minutes_time_lag%TYPE:=NULL;
   L_order_type         ORDER_PUB_INFO.ORDER_TYPE%TYPE := NULL;

   L_exists             VARCHAR2(1):='N';
   L_initial_approval   VARCHAR2(1):='N';

   cursor C_HEAD is
      select opi.thread_no,
             opi.initial_approval_ind,
             opi.order_type
        from order_pub_info opi
       where opi.order_no = I_order_no;


BEGIN

   if I_message_type != HDR_ADD then
      open C_HEAD;
      fetch C_HEAD into L_thread_no,
                        L_initial_approval,
                        L_order_type;
      ---
      if C_HEAD%NOTFOUND then
         close C_HEAD;
         O_error_message := SQL_LIB.CREATE_MSG('PUB_INFO_NOT_FOUND',
                                               'order_pub_info',
                                               I_order_no,
                                               NULL);
         return FALSE;
      end if;
      ---
      close C_HEAD;

      if L_order_type = 'DSD' then
         return TRUE;
      end if;
   end if;

   ---
   -- If the order has not been approved,
   -- no messages will be added to the queue.
   ---
   if I_message_type in(DTL_ADD,DTL_UPD,DTL_DEL,HDR_DEL,HDR_UPD) then
      ---
      -- The indicator to keep track of whether the order has been inititally approved
      -- gets updated here.
      ---
      if I_message_type = HDR_UPD and
         I_order_header_status = 'A' and
         L_initial_approval = 'N' then

         update order_pub_info
            set initial_approval_ind = 'Y'
          where order_no = I_order_no;
         ---
         L_initial_approval := 'Y';
      end if;

      if L_initial_approval = 'N' then
         if I_message_type = HDR_DEL then
            delete from order_pub_info
             where order_no = I_order_no;
         end if;
         return TRUE;
      end if;
   end if;

   if I_message_type in(DTL_ADD,DTL_UPD,DTL_DEL) then
      if ITEM_ATTRIB_SQL.CONTENTS_ITEM_EXISTS(O_error_message,
                                              L_exists,
                                              I_item) = FALSE then
         return FALSE;
      end if;
   end if;

   ---
   -- If the message is a detail delete message, all previous records
   -- on the queue relating to the detail record can be deleted.
   ---

   if I_message_type = DTL_DEL and L_exists = 'N' then
      delete from order_mfqueue
       where order_no = I_order_no
         and item     = I_item
         and location = I_location;

   ---
   -- If the message is a detail update message, all previous DTL_UPD
   -- messages on the queue relating to the detail record can be deleted.
   ---
   elsif I_message_type = DTL_UPD and L_exists = 'N' then
      delete from order_mfqueue
       where order_no = I_order_no
         and item     = I_item
         and location = I_location
         and message_type = DTL_UPD;

   ---
   -- If the message is a header delete, all previous records on the queue
   -- relating to the order can be deleted.
   ---
   elsif I_message_type = HDR_DEL then
      delete from order_mfqueue
       where order_no = I_order_no;

   ---
   -- If the message is a header update, all previous HDR_UPD
   -- messages on the queue relating to the header record can be deleted.
   ---
   elsif I_message_type = HDR_UPD then
      delete from order_mfqueue
       where order_no = I_order_no
         and message_type = HDR_UPD;
   end if;
   ---
   -- HDR_ADD messages do not get added to the queue.  Instead, a record gets inserted
   -- into the order_pub_info table.
   ---
   -- The order_pub_info table keeps track of the order_no,
   -- thread_no for all messages associated with the order,
   -- an indicator to keep track of whether the order has been initially approved,
   -- and an indicator to keep track of whether or not the initial create message
   -- for the order has been published.
   ---
   if I_message_type = HDR_ADD then
      API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                   O_error_message,
                                   L_max_details,
                                   L_num_threads,
                                   L_minutes_time_lag,
                                   FAMILY);   --- I_family
      if L_status_code = API_CODES.UNHANDLED_ERROR then
         return FALSE;
      end if;
      ---
      insert into order_pub_info( order_no,
                                  thread_no,
                                  initial_approval_ind,
                                  published,
                                  order_type )
                         values ( I_order_no,
                                  MOD(I_order_no, L_num_threads)+1,
                                  DECODE(I_order_header_status, 'A', 'Y', 'N'),
                                  'N',
                                  I_order_type );
   elsif L_exists = 'N' then

      insert into order_mfqueue ( seq_no,
                                   order_no,
                                   item,
                                   location,
                                   loc_type,
                                   physical_location,
                                   message_type,
                                   thread_no,
                                   family,
                                   custom_message_type,
                                   pub_status,
                                   transaction_number,
                                   transaction_time_stamp )
                          values ( order_mfsequence.NEXTVAL,
                                   I_order_no,
                                   I_item,
                                   I_location,
                                   I_loc_type,
                                   I_physical_location,
                                   I_message_type,
                                   L_thread_no,
                                   RMSMFM_ORDER.FAMILY,
                                   'N',
                                   'U',
                                   I_order_no,
                                   SYSDATE);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;

END ADDTOQ;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1)
IS

   L_break_loop         BOOLEAN := TRUE;
   L_message_type       order_mfqueue.message_type%TYPE:=NULL;

   L_order_no           order_mfqueue.order_no%TYPE := NULL;
   L_item               order_mfqueue.item%TYPE := NULL;
   L_location           order_mfqueue.location%TYPE := NULL;
   L_loc_type           order_mfqueue.loc_type%TYPE := NULL;
   L_physical_location  order_mfqueue.physical_location%TYPE := NULL;

   L_seq_no             order_mfqueue.seq_no%TYPE:=NULL;

   L_delete_rowid_ind   VARCHAR2(1);
   L_pub_status         order_mfqueue.pub_status%TYPE:=NULL;
   L_rowid              ROWID  :=NULL;

   L_hdr_published      order_pub_info.published%TYPE:=NULL;
   L_hospital_found     VARCHAR2(1) := 'N';

   L_queue_locked       BOOLEAN := FALSE;
   L_seq_limit          order_mfqueue.seq_no%TYPE := 0;

   ---
   -- DRIVING CURSOR - selects ONE message from the queue
   ---
   cursor C_QUEUE is
      select q.order_no,
             q.item,
             q.location,
             q.loc_type,
             q.physical_location,
             q.message_type,
             q.pub_status,
             q.seq_no,
             q.rowid
        from order_mfqueue q
       where q.seq_no = (select min(q2.seq_no)
                           from order_mfqueue q2
                          where q2.thread_no = I_thread_val
                            and q2.pub_status = 'U'
                            and q2.seq_no > L_seq_limit)
         and q.thread_no = I_thread_val;

   cursor C_CHECK_HDR_PUBLISHED is
      select opi.published
        from order_pub_info opi
       where opi.order_no = L_order_no;

   cursor C_CHECK_FOR_HOSPITAL_MSGS is
      select 'Y'
        from order_mfqueue
       where order_no = L_order_no
         and pub_status = 'H';

BEGIN

   -- status of 'H'ospital
   LP_error_status := API_CODES.HOSPITAL;

   LOOP
      L_order_no := NULL;
      O_message  := NULL;
      ---
      open C_QUEUE;
      fetch C_QUEUE into L_order_no,
                         L_item,
                         L_location,
                         L_loc_type,
                         L_physical_location,
                         L_message_type,
                         L_pub_status,
                         L_seq_no,
                         L_rowid;
      close C_QUEUE;

      if L_order_no is NULL then
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;

      if LOCK_THE_BLOCK(O_error_msg,
                        L_queue_locked,
                        L_order_no) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_queue_locked then
         L_seq_limit := L_seq_no;
         O_error_msg := NULL;
      else

         open  C_CHECK_HDR_PUBLISHED;
         fetch C_CHECK_HDR_PUBLISHED into L_hdr_published;
         close C_CHECK_HDR_PUBLISHED;

         open  C_CHECK_FOR_HOSPITAL_MSGS;
         fetch C_CHECK_FOR_HOSPITAL_MSGS into L_hospital_found;
         close C_CHECK_FOR_HOSPITAL_MSGS;

         if L_hospital_found = 'Y' then
            O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                           NULL,
                                           NULL,
                                           NULL);
            raise PROGRAM_ERROR;
         end if;

         if PROCESS_QUEUE_RECORD(O_error_msg,
                                 L_break_loop,
                                 O_message,
                                 O_routing_info,
                                 O_bus_obj_id,
                                 L_message_type,
                                 L_delete_rowid_ind,
                                 L_order_no,
                                 L_hdr_published,
                                 L_item,
                                 L_location,
                                 L_loc_type,
                                 L_physical_location,
                                 L_pub_status,
                                 L_seq_no,
                                 L_rowid) = FALSE then
            raise PROGRAM_ERROR;
         end if;

         if L_break_loop = TRUE then
            O_message_type   := L_message_type;
            EXIT;
         end if;

      end if; -- if L_queue_locked

   END LOOP;

   if O_message IS NULL then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_order_no);
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_seq_no,
                    L_order_no,
                    L_item,
                    L_physical_location,
                    L_loc_type);
END GETNXT;
--------------------------------------------------------------------------------

PROCEDURE PUB_RETRY(O_status_code         OUT   VARCHAR2,
                    O_error_msg           OUT   VARCHAR2,
                    O_message_type    IN  OUT   VARCHAR2,
                    O_message             OUT   RIB_OBJECT,
                    O_bus_obj_id      IN  OUT   RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT   RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN        RIB_OBJECT)
IS

   L_seq_no             order_mfqueue.seq_no%TYPE:=NULL;

   L_break_loop         BOOLEAN := FALSE;

   L_order_no           order_mfqueue.order_no%TYPE:=NULL;
   L_hdr_published      order_pub_info.published%TYPE:=NULL;

   L_item               order_mfqueue.item%TYPE := NULL;
   L_location           order_mfqueue.location%TYPE := NULL;
   L_loc_type           order_mfqueue.loc_type%TYPE := NULL;
   L_physical_location  order_mfqueue.physical_location%TYPE := NULL;

   L_delete_rowid_ind   VARCHAR2(1);
   L_pub_status         order_mfqueue.pub_status%TYPE:=NULL;
   L_rowid              ROWID:=NULL;
   L_queue_locked       BOOLEAN := FALSE;

cursor C_RETRY_QUEUE is
   select q.order_no,
          oho.published,
          q.item,
          q.location,
          q.loc_type,
          q.physical_location,
          q.message_type,
          q.pub_status,
          q.rowid
     from order_mfqueue q,
          order_pub_info oho
    where q.seq_no = L_seq_no
      and oho.order_no = q.order_no;

BEGIN

   -- status of 'H'ospital
   LP_error_status := API_CODES.HOSPITAL;

   --get info from routing info
   ---assuming the only thing in the routing info is the seq_no
   L_seq_no := O_routing_info(1).value;

   --get info from queue table
   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_order_no,
                            L_hdr_published,
                            L_item,
                            L_location,
                            L_loc_type,
                            L_physical_location,
                            O_message_type,
                            L_pub_status,
                            L_rowid;
   close C_RETRY_QUEUE;

   if L_order_no is NULL then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   if LOCK_THE_BLOCK(O_error_msg,
                     L_queue_locked,
                     L_order_no) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked then
      O_status_code := API_CODES.HOSPITAL;
   else
      if PROCESS_QUEUE_RECORD(O_error_msg,
                              L_break_loop,
                              O_message,
                              O_routing_info,
                              O_bus_obj_id,
                              O_message_type,
                              L_delete_rowid_ind,
                              L_order_no,
                              L_hdr_published,
                              L_item,
                              L_location,
                              L_loc_type,
                              L_physical_location,
                              L_pub_status,
                              L_seq_no,
                              L_rowid) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      ---
      if O_message IS NULL then
         O_status_code := API_CODES.NO_MSG;
      else
         if L_delete_rowid_ind = 'N' then
            O_status_code := API_CODES.INCOMPLETE_MSG;
         else
            O_status_code := API_CODES.NEW_MSG;
         end if;
         ---
         O_bus_obj_id := RIB_BUSOBJID_TBL(L_order_no);
      end if;

   end if; -- if L_queue_locked

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_seq_no,
                    L_order_no,
                    L_item,
                    L_physical_location,
                    L_loc_type);

END PUB_RETRY;

--------------------------------------------------------------------------------

FUNCTION PROCESS_QUEUE_RECORD(O_error_message         OUT        VARCHAR2,
                              O_break_loop            OUT        BOOLEAN,
                              O_message           IN  OUT nocopy RIB_OBJECT,
                              O_routing_info      IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id        IN  OUT nocopy RIB_BUSOBJID_TBL,
                              O_message_type      IN  OUT        VARCHAR2,
                              O_delete_rowid_ind  IN  OUT        VARCHAR2,
                              I_order_no          IN             order_mfqueue.order_no%TYPE,
                              I_hdr_published     IN             order_pub_info.published%TYPE,
                              I_item              IN             order_mfqueue.item%TYPE,
                              I_location          IN             order_mfqueue.location%TYPE,
                              I_loc_type          IN             order_mfqueue.loc_type%TYPE,
                              I_physical_location IN             order_mfqueue.physical_location%TYPE,
                              I_pub_status        IN             order_mfqueue.pub_status%TYPE,
                              I_seq_no            IN             order_mfqueue.seq_no%TYPE,
                              I_rowid             IN             ROWID)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'RMSMFM_ORDER.PROCESS_QUEUE_RECORD';
   L_status_code        VARCHAR2(1) := NULL;

   L_supplier           ordhead.supplier%TYPE:=NULL;
   L_max_details        rib_settings.max_details_to_publish%TYPE:=NULL;
   L_num_threads        rib_settings.num_threads%TYPE:=NULL;
   L_minutes_time_lag   rib_settings.minutes_time_lag%TYPE:=NULL;

   L_rib_podesc_rec     "RIB_PODesc_REC"      := NULL;
   L_rib_poref_rec      "RIB_PORef_REC"       := NULL;

BEGIN

   O_break_loop := TRUE;

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_message,
                                L_max_details,
                                L_num_threads,
                                L_minutes_time_lag,
                                FAMILY);   --- I_family
   if L_status_code = API_CODES.UNHANDLED_ERROR then
      return FALSE;
   end if;

   if O_message_type = HDR_DEL and I_hdr_published = 'N' then
      O_break_loop := FALSE;
      ---
      LP_error_status := API_CODES.UNHANDLED_ERROR;
      ---
      delete from order_pub_info
       where order_no     = I_order_no;
      ---
      if DELETE_QUEUE_REC(O_error_message,
                          I_seq_no) = FALSE then
         return FALSE;
      end if;
      ---
   elsif O_message_type = HDR_DEL then
      O_message := "RIB_PORef_REC"(0,
                                 'P',
                                 I_order_no,
                                 NULL); -- POPHYDTLREF_TBL
      ---
      if GET_ROUTING_TO_LOCS(O_error_message,
                             I_order_no,
                             O_routing_info) = FALSE then
         return FALSE;
      end if;
      ---
      LP_error_status := API_CODES.UNHANDLED_ERROR;
      ---
      delete from order_pub_info
       where order_no     = I_order_no;
      ---
      delete from order_details_published
       where order_no     = I_order_no;
      ---
      if DELETE_QUEUE_REC(O_error_message,
                          I_seq_no) = FALSE then
         return FALSE;
      end if;
      ---
   elsif I_hdr_published in ('I', 'N') then

      if I_hdr_published = 'I' then
         O_message_type := DTL_ADD;
      else
         O_message_type := HDR_ADD;
      end if;

      -- publish the entire order             --PODesc (all details)
      if MAKE_CREATE(O_error_message,
                     O_message,
                     O_routing_info,
                     O_delete_rowid_ind,
                     I_order_no,
                     I_seq_no,
                     L_max_details,
                     I_rowid) = FALSE then
         return FALSE;
      end if;

   -- write the current record (anything but HDR_ADD)
   elsif O_message_type = HDR_UPD then          --PODesc (no details)

      if BUILD_HEADER_OBJECT( O_error_message,
                              O_message,
                              L_supplier,
                              I_order_no) = FALSE then
         return FALSE;
      end if;

      L_rib_podesc_rec := TREAT(O_message AS "RIB_PODesc_REC");

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      update order_pub_info
         set published = 'Y'
       where order_no = I_order_no;
      ---
      if GET_ROUTING_TO_LOCS(O_error_message,
                             I_order_no,
                             O_routing_info) = FALSE then
         return FALSE;
      end if;
      ---
      if DELETE_QUEUE_REC(O_error_message,
                          I_seq_no) = FALSE then
         return FALSE;
      end if;

   elsif O_message_type in (DTL_ADD, DTL_UPD) then       --PODesc (one or more details)

      if BUILD_DETAIL_CHANGE_OBJECTS(O_error_message,
                                     O_message,
                                     O_routing_info,
                                     L_supplier,
                                     O_message_type,
                                     I_order_no,
                                     I_item,
                                     I_physical_location,
                                     L_max_details) = FALSE then
         return FALSE;
      end if;

   elsif O_message_type = DTL_DEL then       --PODtlRef

      if BUILD_DETAIL_DELETE(O_error_message,
                             O_message_type,
                             O_message,
                             O_break_loop,
                             I_order_no,
                             I_item,
                             I_location,
                             I_loc_type,
                             I_physical_location,
                             I_rowid) = FALSE then
         return FALSE;
      end if;

      if O_break_loop = TRUE then
         ------ add physical loc to routing info
         if ROUTING_INFO_ADD(O_error_message,
                             O_routing_info,
                             I_physical_location,
                             I_loc_type) = FALSE then
            return FALSE;
         end if;

      end if; -- O_break_loop = TRUE

   end if; -- O_message_type = HDR_UPD

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;

END PROCESS_QUEUE_RECORD;

---------------------------------------------------------------------------------

FUNCTION MAKE_CREATE(O_error_message    IN OUT        VARCHAR2,
                     O_message          IN OUT nocopy RIB_OBJECT,
                     O_routing_info     IN OUT nocopy RIB_ROUTINGINFO_TBL,
                     O_delete_rowid_ind IN OUT        VARCHAR2,
                     I_order_no         IN            ordhead.order_no%TYPE,
                     I_seq_no           IN            order_mfqueue.seq_no%TYPE,
                     I_max_details      IN            rib_settings.max_details_to_publish%TYPE,
                     I_rowid            IN            ROWID)

RETURN BOOLEAN IS

   L_program               VARCHAR2(64) := 'RMSMFM_ORDER.MAKE_CREATE';

   L_supplier              ordhead.supplier%TYPE:=NULL;

   L_rib_podesc_rec        "RIB_PODesc_REC" := NULL;
   L_rib_podtl_tbl         "RIB_PODtl_TBL" := NULL;

   L_order_mfqueue_rowid   rowid_TBL;
   L_order_mfqueue_size    BINARY_INTEGER := 0;

   L_rib_routing_rec       RIB_ROUTINGINFO_REC := NULL;
   L_message_type          ORDER_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;

   PROGRAM_ERROR           EXCEPTION;

BEGIN

   O_delete_rowid_ind := 'Y';

   if BUILD_HEADER_OBJECT(O_error_message,
                          L_rib_podesc_rec,
                          L_supplier,
                          I_order_no) = FALSE then
      return FALSE;
   end if;

   if BUILD_DETAIL_OBJECTS(O_error_message,
                           L_rib_podtl_tbl,
                           L_order_mfqueue_rowid,
                           L_order_mfqueue_size,
                           O_routing_info,
                           O_delete_rowid_ind,
                           L_message_type,
                           L_supplier,
                           I_order_no,
                           null,        --- I_item
                           null,        --- I_physical_location
                           I_max_details) = FALSE then
      return FALSE;
   end if;

   --- if flag from BUILD_DETAIL_OBJECTS is TRUE, add rowid to L_order_mfqueue_rowid table;
   if O_delete_rowid_ind = 'Y' then
      L_order_mfqueue_size := L_order_mfqueue_size + 1;
      L_order_mfqueue_rowid(L_order_mfqueue_size) := I_rowid;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   update order_pub_info
      set published = DECODE(O_delete_rowid_ind, 'Y', 'Y', 'I')
    where order_no  = I_order_no;

   -- add the detail to the header
   L_rib_podesc_rec.podtl_tbl := L_rib_podtl_tbl;

   if L_order_mfqueue_size > 0 then
      FORALL i IN 1..L_order_mfqueue_size
         delete from order_mfqueue where rowid = L_order_mfqueue_rowid(i);
   end if;

   O_message := L_rib_podesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END MAKE_CREATE;

------------------------------------------------------------------------------------------

FUNCTION ROUTING_INFO_ADD(O_error_message         OUT        VARCHAR2,
                          O_routing_info      IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                          I_physical_location IN             ordloc.location%TYPE,
                          I_loc_type          IN             ordloc.loc_type%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'RMSMFM_ORDER.ROUTING_INFO_ADD';

   L_rib_routing_rec   RIB_ROUTINGINFO_REC := NULL;

BEGIN

   if O_routing_info is NULL then
      O_routing_info := RIB_ROUTINGINFO_TBL();
   end if;

   L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc', I_physical_location,
                                            'to_phys_loc_type',I_loc_type,
                                            null,null);

   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END ROUTING_INFO_ADD;

------------------------------------------------------------------------------------------

FUNCTION DELETE_QUEUE_REC(O_text      OUT VARCHAR2,
                          I_seq_no IN     order_mfqueue.seq_no%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'RMSMFM_ORDER.DELETE_QUEUE_REC';

BEGIN

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   delete from order_mfqueue
    where seq_no = I_seq_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_text := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                   SQLERRM,
                                   L_program,
                                   NULL);
      return FALSE;
END DELETE_QUEUE_REC;

------------------------------------------------------------------------------------------
FUNCTION BUILD_HEADER_OBJECT(O_error_message   IN OUT          VARCHAR2,
                             O_message         IN OUT NOCOPY   RIB_OBJECT,
                             O_supplier        IN OUT          ORDHEAD.SUPPLIER%TYPE,
                             I_order_no        IN              ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'RMSMFM_ORDER.BUILD_HEADER_OBJECT';

   L_header_struct       RMSMFM_ORDER.ORDHEAD_MSG_RECTYPE;
   L_customer_order_no   ORDCUST.CUSTOMER_ORDER_NO%TYPE := NULL;
   L_fulfill_order_no    ORDCUST.FULFILL_ORDER_NO%TYPE  := NULL;
   L_stockholding_ind    VARCHAR2(1);
   L_cust_ord_ind        VARCHAR2(1);

   L_rib_podesc_rec  "RIB_PODesc_REC" := NULL;

   PROGRAM_ERROR     EXCEPTION;

   cursor C_ORDER_HEAD is
      select oh.order_no,
             oh.order_type,
             oh.dept,
             oh.buyer,
             oh.supplier,
             oh.loc_type,
             oh.location,
             oh.promotion,
             oh.qc_ind,
             oh.not_before_date,
             oh.not_after_date,
             oh.otb_eow_date,
             oh.earliest_ship_date,
             oh.latest_ship_date,
             oh.close_date,
             oh.terms,
             oh.freight_terms,
             oh.payment_method,
             oh.backhaul_type,
             oh.backhaul_allowance,
             oh.ship_method,
             oh.purchase_type,
             oh.status,
             oh.ship_pay_method,
             oh.fob_trans_res,
             oh.fob_trans_res_desc,
             oh.fob_title_pass,
             oh.fob_title_pass_desc,
             oh.vendor_order_no,
             oh.exchange_rate,
             oh.factory,
             oh.agent,
             oh.discharge_port,
             oh.lading_port,
             oh.freight_contract_no,
             oh.po_type,
             oh.pre_mark_ind,
             oh.currency_code,
             oh.contract_no,
             oh.pickup_loc,
             oh.pickup_no,
             oh.pickup_date,
             oh.app_datetime,
             oh.comment_desc
        from ordhead oh
       where oh.order_no = I_order_no;

   cursor C_GET_STOCK_IND is
      select stockholding_ind
        from store
       where store = L_header_struct.ordhead_rec.location;

   cursor C_GET_ORDCUST is
      select customer_order_no,
             fulfill_order_no
        from ordcust
       where order_no = I_order_no;

BEGIN

   open C_ORDER_HEAD;
   fetch C_ORDER_HEAD into L_header_struct.ordhead_rec.order_no,
                           L_header_struct.ordhead_rec.order_type,
                           L_header_struct.ordhead_rec.dept,
                           L_header_struct.ordhead_rec.buyer,
                           L_header_struct.ordhead_rec.supplier,
                           L_header_struct.ordhead_rec.loc_type,
                           L_header_struct.ordhead_rec.location,
                           L_header_struct.ordhead_rec.promotion,
                           L_header_struct.ordhead_rec.qc_ind,
                           L_header_struct.ordhead_rec.not_before_date,
                           L_header_struct.ordhead_rec.not_after_date,
                           L_header_struct.ordhead_rec.otb_eow_date,
                           L_header_struct.ordhead_rec.earliest_ship_date,
                           L_header_struct.ordhead_rec.latest_ship_date,
                           L_header_struct.ordhead_rec.close_date,
                           L_header_struct.ordhead_rec.terms,
                           L_header_struct.ordhead_rec.freight_terms,
                           L_header_struct.ordhead_rec.payment_method,
                           L_header_struct.ordhead_rec.backhaul_type,
                           L_header_struct.ordhead_rec.backhaul_allowance,
                           L_header_struct.ordhead_rec.ship_method,
                           L_header_struct.ordhead_rec.purchase_type,
                           L_header_struct.ordhead_rec.status,
                           L_header_struct.ordhead_rec.ship_pay_method,
                           L_header_struct.ordhead_rec.fob_trans_res,
                           L_header_struct.ordhead_rec.fob_trans_res_desc,
                           L_header_struct.ordhead_rec.fob_title_pass,
                           L_header_struct.ordhead_rec.fob_title_pass_desc,
                           L_header_struct.ordhead_rec.vendor_order_no,
                           L_header_struct.ordhead_rec.exchange_rate,
                           L_header_struct.ordhead_rec.factory,
                           L_header_struct.ordhead_rec.agent,
                           L_header_struct.ordhead_rec.discharge_port,
                           L_header_struct.ordhead_rec.lading_port,
                           L_header_struct.ordhead_rec.freight_contract_no,
                           L_header_struct.ordhead_rec.po_type,
                           L_header_struct.ordhead_rec.pre_mark_ind,
                           L_header_struct.ordhead_rec.currency_code,
                           L_header_struct.ordhead_rec.contract_no,
                           L_header_struct.ordhead_rec.pickup_loc,
                           L_header_struct.ordhead_rec.pickup_no,
                           L_header_struct.ordhead_rec.pickup_date,
                           L_header_struct.ordhead_rec.app_datetime,
                           L_header_struct.ordhead_rec.comment_desc;
   if C_ORDER_HEAD%NOTFOUND then
      close C_ORDER_HEAD;
      O_error_message := SQL_LIB.CREATE_MSG('NO_ORDHEAD_PUB',
                                             I_order_no, NULL, NULL);
      return FALSE;
   end if;
   close C_ORDER_HEAD;

   if NOT GET_MSG_HEADER(O_error_message,
                         L_header_struct) then
      raise PROGRAM_ERROR;
   end if;

   if L_header_struct.ordhead_rec.order_type = 'CO' then
      if L_header_struct.ordhead_rec.loc_type = 'S' then

         open C_GET_STOCK_IND;
         fetch C_GET_STOCK_IND into L_stockholding_ind;
         close C_GET_STOCK_IND;

         if L_stockholding_ind = 'Y' then

            open C_GET_ORDCUST;
            fetch C_GET_ORDCUST into L_customer_order_no,
                                     L_fulfill_order_no;
            close C_GET_ORDCUST;

         end if;
      end if;
   end if;

   if L_customer_order_no is NOT NULL then
      L_cust_ord_ind := 'Y';
   else
      L_cust_ord_ind := 'N';
   end if;

   L_rib_podesc_rec := "RIB_PODesc_REC"(0,                                                 --rib_oid number
                                        'P',                                               --doc_type VARCHAR2(1),
                                        L_header_struct.ordhead_rec.order_no,              --NUMBER(12),
                                        L_header_struct.ordhead_rec.order_type,            --VARCHAR2(3),
                                        L_header_struct.order_type_desc,                   --VARCHAR2(40),
                                        L_header_struct.ordhead_rec.dept,                  --NUMBER(4,0),
                                        L_header_struct.dept_name,                         --VARCHAR2(20),
                                        L_header_struct.ordhead_rec.buyer,                 --NUMBER(4,0),
                                        L_header_struct.buyer_name,                        --VARCHAR2(32),
                                        L_header_struct.ordhead_rec.supplier,              --NUMBER(10,0),
                                        L_header_struct.ordhead_rec.promotion,             --NUMBER(10,0),
                                        L_header_struct.promotion_desc,                    --VARCHAR2(160),
                                        L_header_struct.ordhead_rec.qc_ind,                --VARCHAR2(1),
                                        L_header_struct.ordhead_rec.not_before_date,       --DATE,
                                        L_header_struct.ordhead_rec.not_after_date,        --DATE,
                                        L_header_struct.ordhead_rec.otb_eow_date,          --DATE,
                                        L_header_struct.ordhead_rec.earliest_ship_date,    --DATE,
                                        L_header_struct.ordhead_rec.latest_ship_date,      --DATE,
                                        L_header_struct.ordhead_rec.close_date,            --DATE,
                                        L_header_struct.ordhead_rec.terms,                 --VARCHAR2(15),
                                        L_header_struct.terms_code,                        --VARCHAR2(50),
                                        L_header_struct.ordhead_rec.freight_terms,         --VARCHAR2(30),
                                        L_cust_ord_ind,                                    --CUST_ORDER VARCHAR2(1)
                                        L_header_struct.ordhead_rec.payment_method,        --VARCHAR2(6),
                                        L_header_struct.payment_method_desc,               --VARCHAR2(40),
                                        L_header_struct.ordhead_rec.backhaul_type,         --VARCHAR2(6),
                                        L_header_struct.backhaul_type_desc,                --VARCHAR2(40),
                                        L_header_struct.ordhead_rec.backhaul_allowance,    --NUMBER(20,4),
                                        L_header_struct.ordhead_rec.ship_method,           --VARCHAR2(6),
                                        L_header_struct.ship_method_desc,                  --VARCHAR2(40),
                                        L_header_struct.ordhead_rec.purchase_type,         --VARCHAR2(6),
                                        L_header_struct.purchase_type_desc,                --VARCHAR2(40),
                                        L_header_struct.ordhead_rec.status,                --VARCHAR2(1),
                                        L_header_struct.ordhead_rec.ship_pay_method,       --VARCHAR2(2),
                                        L_header_struct.ship_pay_method_desc,              --VARCHAR2(40),
                                        L_header_struct.ordhead_rec.fob_trans_res,         --VARCHAR2(2),
                                        L_header_struct.fob_trans_res_code_desc,           --VARCHAR2(40),
                                        L_header_struct.ordhead_rec.fob_trans_res_desc,    --VARCHAR2(45),
                                        L_header_struct.ordhead_rec.fob_title_pass,        --VARCHAR2(2),
                                        L_header_struct.fob_title_pass_code_desc,          --VARCHAR2(40),
                                        L_header_struct.ordhead_rec.fob_title_pass_desc,   --VARCHAR2(45),
                                        L_header_struct.ordhead_rec.vendor_order_no,       --VARCHAR2(15),
                                        L_header_struct.ordhead_rec.exchange_rate,         --NUMBER(20,10),
                                        L_header_struct.ordhead_rec.factory,               --VARCHAR2(10),
                                        L_header_struct.factory_desc,                      --VARCHAR2(40),
                                        L_header_struct.ordhead_rec.agent,                 --VARCHAR2(10),
                                        L_header_struct.agent_desc,                        --VARCHAR2(40),
                                        L_header_struct.ordhead_rec.discharge_port,        --VARCHAR2(5),
                                        L_header_struct.discharge_port_desc,               --VARCHAR2(40),
                                        L_header_struct.ordhead_rec.lading_port,           --VARCHAR2(5),
                                        L_header_struct.lading_port_desc,                  --VARCHAR2(40),
                                        L_header_struct.ordhead_rec.freight_contract_no,   --VARCHAR2(10),
                                        L_header_struct.ordhead_rec.po_type,               --VARCHAR2(4),
                                        L_header_struct.po_type_desc,                      --VARCHAR2(60),
                                        L_header_struct.ordhead_rec.pre_mark_ind,          --VARCHAR2(1),
                                        L_header_struct.ordhead_rec.currency_code,         --VARCHAR2(3),
                                        L_header_struct.ordhead_rec.contract_no,           --NUMBER(6,0),
                                        L_header_struct.ordhead_rec.pickup_loc,            --VARCHAR2(45),
                                        L_header_struct.ordhead_rec.pickup_no,             --VARCHAR2(25),
                                        L_header_struct.ordhead_rec.pickup_date,           --DATE,
                                        L_header_struct.ordhead_rec.app_datetime,          --DATE,
                                        L_header_struct.ordhead_rec.comment_desc,          --VARCHAR2(2000),
                                        L_customer_order_no,                               --VARCHAR2(48),
                                        L_fulfill_order_no,                                --VARCHAR2(48),
                                        NULL);                                             --PODtl_TBL         "RIB_PODtl_TBL",

   O_supplier := L_header_struct.ordhead_rec.supplier;
   O_message  := L_rib_podesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END BUILD_HEADER_OBJECT;
------------------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_OBJECTS(O_error_message         IN OUT          VARCHAR2,
                              O_message               IN OUT NOCOPY   "RIB_PODtl_TBL",
                              O_order_mfqueue_rowid   IN OUT NOCOPY   ROWID_TBL,
                              O_order_mfqueue_size    IN OUT          BINARY_INTEGER,
                              O_routing_info          IN OUT NOCOPY   RIB_ROUTINGINFO_TBL,
                              O_delete_rowid_ind      IN OUT          VARCHAR2,
                              IO_message_type         IN OUT          ORDER_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_supplier              IN              ORDHEAD.SUPPLIER%TYPE,
                              I_order_no              IN              ORDHEAD.ORDER_NO%TYPE,
                              I_item                  IN              ORDER_MFQUEUE.ITEM%TYPE,
                              I_physical_location     IN              ORDER_MFQUEUE.PHYSICAL_LOCATION%TYPE,
                              I_max_details           IN              RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(64) := 'RMSMFM_ORDER.BUILD_DETAIL_OBJECTS';

   L_rib_podtl_rec              "RIB_PODtl_REC" := NULL;
   L_rib_podtl_tbl              "RIB_PODtl_TBL" := NULL;

   L_rib_povirtualdtl_rec       "RIB_POVirtualDtl_REC" := NULL;
   L_rib_povirtualdtl_tbl       "RIB_POVirtualDtl_TBL" := NULL;

   L_prev_physical_loc          ORDLOC.LOCATION%TYPE := -1;
   L_prev_location              ORDLOC.LOCATION%TYPE := -1;
   L_prev_item                  ORDLOC.ITEM%TYPE     := -1;

   L_physical_quantity          ORDLOC.QTY_ORDERED%TYPE := 0;

   L_odp_ins_order_no           odp_order_no_TBL;
   L_odp_ins_item               odp_item_TBL;
   L_odp_ins_location           odp_location_TBL;
   L_odp_ins_loc_type           odp_loc_type_TBL;
   L_odp_ins_physical_location  odp_physical_location_TBL;
   L_odp_ins_size               BINARY_INTEGER := 0;

   L_odp_upd_rowid              rowid_TBL;
   L_odp_upd_size               BINARY_INTEGER := 0;

   L_extend                     BOOLEAN := FALSE;
   L_records_found              BOOLEAN := FALSE;
   L_details_processed          RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 0;
   L_exists                     VARCHAR2(1) := NULL;
   L_transaction_uom            ORDCUST_DETAIL.TRANSACTION_UOM%TYPE := NULL;

   L_rib_routing_rec            RIB_ROUTINGINFO_REC :=NULL;

   L_stockholding_ind           STORE.STOCKHOLDING_IND%TYPE;

   cursor C_CHECK_FOR_INITAL_ADD_MSG is
      select 'x'
        from order_details_published odp
       where odp.order_no = I_order_no
         and odp.item     = I_item
         and odp.physical_location = I_physical_location
         and odp.detail_exists_ind = 'Y';

   cursor C_GET_TRANSACTION_UOM (I_item       ORDLOC.ITEM%TYPE,
                                 I_location   ORDLOC.LOCATION%TYPE) is
      select od.transaction_uom
        from ordcust_detail od,
             ordcust oc
       where oc.order_no = I_order_no
         and oc.fulfill_loc_id = I_location
         and oc.ordcust_no = od.ordcust_no
         and od.item = I_item;

   --- This cursor is used AFTER the initial HDR_CRE message has been sent.
   --- It grabs all messages corresponding to DTL_ADD.
   --- A DTL_ADD message is sent when there have been no DTL_ADD messages sent
   --- for the order_no/item/physical_location.
   --- We check to see if a DTL_ADD message has been sent by subquerying
   --- the order_details_published table.
   --- Note:  The message needs to have the current state of all order_no/item/virtual locs
   --- within the order_no/item/physical location.  That is why a join between
   --- order_mfqueue and ordloc is done by physical_location instead of virtual location.
   cursor C_GET_DETAIL_MSG_INFO_ADD is
      select ol.item,
             ol.location,
             ol.loc_type,
             oq.physical_location,
             ol.unit_cost,
             ol.qty_ordered,
             ol.tsf_po_link_no,
             ol.estimated_instock_date,
             os.ref_item,
             os.supp_pack_size,
             os.origin_country_id,
             os.earliest_ship_date,
             os.latest_ship_date,
             os.pickup_loc,
             os.pickup_no,
             isc.round_lvl,
             isc.packing_method,
             oq.rowid oq_rowid,
             odp.rowid odp_rowid,
             odp.detail_exists_ind,
             oh.order_type,
             st.store_type,
             st.stockholding_ind
        from ordloc ol,
             ordsku os,
             ordhead oh,
             item_supp_country isc,
             order_mfqueue oq,
             wh,
             store st,
             order_details_published odp
       where os.order_no          = I_order_no
         and ol.item              = os.item
         and ol.order_no          = os.order_no
         and ol.order_no          = oh.order_no
         and oq.order_no          = ol.order_no
         and os.item              = isc.item
         and isc.supplier         = I_supplier
         and os.origin_country_id = isc.origin_country_id
         and oq.item              = ol.item
         and oq.message_type      in (DTL_ADD, DTL_UPD)
         and oq.physical_location = NVL(wh.physical_wh, ol.location)
         and wh.wh (+)            = ol.location
         and st.store (+)         = ol.location
         and odp.order_no (+)     = ol.order_no
         and odp.item     (+)     = ol.item
         and odp.location (+)     = ol.location
         and not exists(select 'x'
                          from order_details_published odp
                         where odp.order_no = ol.order_no
                           and odp.item     = ol.item
                           and odp.physical_location = oq.physical_location
                           and odp.detail_exists_ind = 'Y')
         and ol.item not in (select item
                               from item_master
                              where deposit_item_type='A')

         order by 4, 1, 2;

   --- This cursor is used AFTER the initial HDR_CRE message has been sent.
   --- It grabs all messages corresponding to DTL_UPD.
   --- A DTL_UPD message is sent when the DTL_ADD message for the
   --- order_no/item/physical_location has already been sent.
   --- We check to see if a DTL_ADD message has been sent by subquerying
   --- the order_details_published table.
   --- Note:  The message needs to have the current state of all order_no/item/virtual locs
   --- within the order_no/item/physical location.  That is why a join between
   --- order_mfqueue and ordloc is done by physical_location instead of virtual location.
   cursor C_GET_DETAIL_MSG_INFO_UPD is
      select ol.item,
             ol.location,
             ol.loc_type,
             oq.physical_location,
             ol.unit_cost,
             ol.qty_ordered,
             ol.tsf_po_link_no,
             ol.estimated_instock_date,
             os.ref_item,
             os.supp_pack_size,
             os.origin_country_id,
             os.earliest_ship_date,
             os.latest_ship_date,
             os.pickup_loc,
             os.pickup_no,
             isc.round_lvl,
             isc.packing_method,
             oq.rowid oq_rowid,
             odp.rowid odp_rowid,
             odp.detail_exists_ind,
             oh.order_type,
             st.store_type,
             st.stockholding_ind
        from ordloc ol,
             ordsku os,
             ordhead oh,
             item_supp_country isc,
             order_mfqueue oq,
             wh,
             store st,
             order_details_published odp
       where os.order_no          = I_order_no
         and ol.item              = os.item
         and ol.order_no          = os.order_no
         and ol.order_no          = oh.order_no
         and oq.order_no          = ol.order_no
         and os.item              = isc.item
         and isc.supplier         = I_supplier
         and os.origin_country_id = isc.origin_country_id
         and oq.item              = ol.item
         and oq.message_type      in (DTL_ADD, DTL_UPD)
         and oq.physical_location = NVL(wh.physical_wh, ol.location)
         and wh.wh (+)            = ol.location
         and st.store(+)          = ol.location
         and odp.order_no (+)     = ol.order_no
         and odp.item     (+)     = ol.item
         and odp.location (+)     = ol.location
         and exists(select 'x'
                      from order_details_published odp
                     where odp.order_no = ol.order_no
                       and odp.item     = ol.item
                       and odp.physical_location = oq.physical_location
                       and odp.detail_exists_ind = 'Y')
         and ol.item not in (select item
                               from item_master
                              where deposit_item_type='A')

         order by 4, 1, 2;

   --- This cursor is used to generate the initial HDR_CRE message.
   --- It grabs the current state of the ordloc and ordsku tables,
   --- and grabs all of the rows on the mfqueue so that they can be deleted.
   --- The outer join to order_details_published is needed for when
   --- the HDR_CRE has been sent previously, but the max_details was exceeded.
   --- In that case, the next call to GETNXT needs to retrieve those details
   --- that have not already been published.
   cursor C_GET_DETAIL_MSG_INFO_MC is
      select ol.item,
             ol.location,
             ol.loc_type,
             NVL(wh.physical_wh, ol.location) physical_location,
             ol.unit_cost,
             ol.qty_ordered,
             ol.tsf_po_link_no,
             ol.estimated_instock_date,
             os.ref_item,
             os.supp_pack_size,
             os.origin_country_id,
             os.earliest_ship_date,
             os.latest_ship_date,
             os.pickup_loc,
             os.pickup_no,
             isc.round_lvl,
             isc.packing_method,
             odp.rowid odp_rowid,
             odp.detail_exists_ind,
             NULL      oq_rowid,
             oh.order_type,
             st.store_type,
             st.stockholding_ind
        from ordloc ol,
             ordsku os,
             ordhead oh,
             item_supp_country isc,
             wh,
             store st,
             order_details_published odp
       where os.order_no          = I_order_no
         and ol.item              = os.item
         and ol.order_no          = os.order_no
         and ol.order_no          = oh.order_no
         and os.item              = isc.item
         and isc.supplier         = I_supplier
         and os.origin_country_id = isc.origin_country_id
         and st.store (+)         = ol.location
         and wh.wh (+)            = ol.location
         and odp.order_no (+)     = ol.order_no
         and odp.item     (+)     = ol.item
         and odp.location (+)     = ol.location
         and ol.item not in (select item
                               from item_master
                              where deposit_item_type='A')
   UNION ALL
      select oq.item,
             oq.location,
             NULL,
             oq.physical_location,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             odp.rowid odp_rowid,
             odp.detail_exists_ind,
             oq.rowid oq_rowid,
             NULL,
             NULL,
             NULL
        from order_mfqueue oq,
             order_details_published odp
       where oq.order_no     = I_order_no
         and oq.message_type in (DTL_DEL, DTL_UPD, DTL_ADD)
         and odp.order_no (+)     = oq.order_no
         and odp.item     (+)     = oq.item
         and odp.location (+)     = oq.location
         and oq.item not in (select item
                               from item_master
                              where deposit_item_type='A')
         order by odp_rowid desc, 4, 1, 2;

BEGIN

   L_rib_podtl_rec := "RIB_PODtl_REC"(
           0, null,null,null,null,null,null,null,
              null,null,null,null,null,null,null,
              null,null,null,null,null,null,null,
              null,null,null,null);

   L_rib_povirtualdtl_rec := "RIB_POVirtualDtl_REC"(0, null, null, null);

   if O_message is NULL then
      L_rib_podtl_tbl := "RIB_PODtl_TBL"();
   else
      L_rib_podtl_tbl := O_message;
   end if;

   L_rib_routing_rec := RIB_ROUTINGINFO_REC('vir_to_loc',null,null,null,null,null);

   if IO_message_type is NULL then

      FOR rec IN C_GET_DETAIL_MSG_INFO_MC LOOP

         L_records_found := TRUE;

         if rec.order_type = 'CO' then
            if rec.loc_type = 'S' then
               if STORE_ATTRIB_SQL.GET_STOCKHOLDING_IND(O_error_message,
                                                        L_stockholding_ind,
                                                        rec.location) = FALSE then
                  return FALSE;
               end if;
            end if;

            if L_stockholding_ind = 'Y' then
               open C_GET_TRANSACTION_UOM(rec.item,
                                          rec.location);
               fetch C_GET_TRANSACTION_UOM into L_transaction_uom;
               close C_GET_TRANSACTION_UOM;
            end if;
         end if;

         if rec.odp_rowid is NULL then

            if BUILD_SINGLE_DETAIL ( O_error_message,
                                     L_rib_podtl_tbl,
                                     O_order_mfqueue_rowid,
                                     O_order_mfqueue_size,
                                     O_routing_info,
                                     L_rib_podtl_rec,
                                     L_rib_povirtualdtl_rec,
                                     L_rib_povirtualdtl_tbl,
                                     L_rib_routing_rec,
                                     L_prev_item,
                                     L_prev_location,
                                     L_prev_physical_loc,
                                     L_physical_quantity,
                                     L_odp_ins_order_no,
                                     L_odp_ins_item,
                                     L_odp_ins_location,
                                     L_odp_ins_loc_type,
                                     L_odp_ins_physical_location,
                                     L_odp_ins_size,
                                     L_odp_upd_rowid,
                                     L_odp_upd_size,
                                     rec.detail_exists_ind,
                                     L_extend,
                                     rec.item,
                                     rec.location,
                                     rec.loc_type,
                                     rec.physical_location,
                                     rec.store_type,
                                     rec.stockholding_ind,
                                     rec.unit_cost,
                                     rec.qty_ordered,
                                     rec.tsf_po_link_no,
                                     rec.estimated_instock_date,
                                     rec.ref_item,
                                     rec.supp_pack_size,
                                     rec.origin_country_id,
                                     rec.earliest_ship_date,
                                     rec.latest_ship_date,
                                     rec.pickup_loc,
                                     rec.pickup_no,
                                     rec.round_lvl,
                                     rec.packing_method,
                                     rec.oq_rowid,
                                     rec.odp_rowid,
                                     I_order_no,
                                     O_delete_rowid_ind,
                                     L_details_processed,
                                     I_max_details,
                                     L_transaction_uom) = FALSE then
               return FALSE;
            end if;

            if O_delete_rowid_ind = 'N' then
               EXIT;
            end if;

         end if;

      END LOOP;
   else

      --- if a DTL_ADD message for the current order_no/item/physical location
      --- has already been sent, we need to send a DTL_UPD message
      if IO_message_type = DTL_ADD then
         open C_CHECK_FOR_INITAL_ADD_MSG;
         fetch C_CHECK_FOR_INITAL_ADD_MSG into L_exists;
         ---
         if C_CHECK_FOR_INITAL_ADD_MSG%NOTFOUND then
            IO_message_type := DTL_ADD;
         else
            IO_message_type := DTL_UPD;
         end if;
         ---
         close C_CHECK_FOR_INITAL_ADD_MSG;
      end if;

      if IO_message_type = DTL_ADD then

         FOR rec IN C_GET_DETAIL_MSG_INFO_ADD LOOP

            L_records_found := TRUE;

            if rec.order_type = 'CO' then
               if rec.loc_type = 'S' then
                  if STORE_ATTRIB_SQL.GET_STOCKHOLDING_IND(O_error_message,
                                                           L_stockholding_ind,
                                                           rec.location) = FALSE then
                     return FALSE;
                  end if;
               end if;

               if L_stockholding_ind = 'Y' then
                  open C_GET_TRANSACTION_UOM(rec.item,
                                             rec.location);
                  fetch C_GET_TRANSACTION_UOM into L_transaction_uom;
                  close C_GET_TRANSACTION_UOM;
               end if;
            end if;

            if BUILD_SINGLE_DETAIL ( O_error_message,
                                     L_rib_podtl_tbl,
                                     O_order_mfqueue_rowid,
                                     O_order_mfqueue_size,
                                     O_routing_info,
                                     L_rib_podtl_rec,
                                     L_rib_povirtualdtl_rec,
                                     L_rib_povirtualdtl_tbl,
                                     L_rib_routing_rec,
                                     L_prev_item,
                                     L_prev_location,
                                     L_prev_physical_loc,
                                     L_physical_quantity,
                                     L_odp_ins_order_no,
                                     L_odp_ins_item,
                                     L_odp_ins_location,
                                     L_odp_ins_loc_type,
                                     L_odp_ins_physical_location,
                                     L_odp_ins_size,
                                     L_odp_upd_rowid,
                                     L_odp_upd_size,
                                     rec.detail_exists_ind,
                                     L_extend,
                                     rec.item,
                                     rec.location,
                                     rec.loc_type,
                                     rec.physical_location,
                                     rec.store_type,
                                     rec.stockholding_ind,
                                     rec.unit_cost,
                                     rec.qty_ordered,
                                     rec.tsf_po_link_no,
                                     rec.estimated_instock_date,
                                     rec.ref_item,
                                     rec.supp_pack_size,
                                     rec.origin_country_id,
                                     rec.earliest_ship_date,
                                     rec.latest_ship_date,
                                     rec.pickup_loc,
                                     rec.pickup_no,
                                     rec.round_lvl,
                                     rec.packing_method,
                                     rec.oq_rowid,
                                     rec.odp_rowid,
                                     I_order_no,
                                     O_delete_rowid_ind,
                                     L_details_processed,
                                     I_max_details,
                                     L_transaction_uom) = FALSE then
               return FALSE;
            end if;

            if O_delete_rowid_ind = 'N' then
               EXIT;
            end if;

         END LOOP;

      else
         FOR rec IN C_GET_DETAIL_MSG_INFO_UPD LOOP

            L_records_found := TRUE;

            if rec.order_type = 'CO' then
               if rec.loc_type = 'S' then
                  if STORE_ATTRIB_SQL.GET_STOCKHOLDING_IND(O_error_message,
                                                           L_stockholding_ind,
                                                           rec.location) = FALSE then
                     return FALSE;
                  end if;
               end if;

               if L_stockholding_ind = 'Y' then
                  open C_GET_TRANSACTION_UOM(rec.item,
                                             rec.location);
                  fetch C_GET_TRANSACTION_UOM into L_transaction_uom;
                  close C_GET_TRANSACTION_UOM;
               end if;
            end if;

            if BUILD_SINGLE_DETAIL (O_error_message,
                                    L_rib_podtl_tbl,
                                    O_order_mfqueue_rowid,
                                    O_order_mfqueue_size,
                                    O_routing_info,
                                    L_rib_podtl_rec,
                                    L_rib_povirtualdtl_rec,
                                    L_rib_povirtualdtl_tbl,
                                    L_rib_routing_rec,
                                    L_prev_item,
                                    L_prev_location,
                                    L_prev_physical_loc,
                                    L_physical_quantity,
                                    L_odp_ins_order_no,
                                    L_odp_ins_item,
                                    L_odp_ins_location,
                                    L_odp_ins_loc_type,
                                    L_odp_ins_physical_location,
                                    L_odp_ins_size,
                                    L_odp_upd_rowid,
                                    L_odp_upd_size,
                                    rec.detail_exists_ind,
                                    L_extend,
                                    rec.item,
                                    rec.location,
                                    rec.loc_type,
                                    rec.physical_location,
                                    rec.store_type,
                                    rec.stockholding_ind,
                                    rec.unit_cost,
                                    rec.qty_ordered,
                                    rec.tsf_po_link_no,
                                    rec.estimated_instock_date,
                                    rec.ref_item,
                                    rec.supp_pack_size,
                                    rec.origin_country_id,
                                    rec.earliest_ship_date,
                                    rec.latest_ship_date,
                                    rec.pickup_loc,
                                    rec.pickup_no,
                                    rec.round_lvl,
                                    rec.packing_method,
                                    rec.oq_rowid,
                                    rec.odp_rowid,
                                    I_order_no,
                                    O_delete_rowid_ind,
                                    L_details_processed,
                                    I_max_details,
                                    L_transaction_uom) = FALSE then
               return FALSE;
            end if;

            if O_delete_rowid_ind = 'N' then
               EXIT;
            end if;

         END LOOP;
      end if;

   end if;

   -- insert PODtl info
   if L_extend then
      L_rib_podtl_rec.physical_qty_ordered    := L_physical_quantity;
      L_rib_podtl_rec.povirtualdtl_tbl        := L_rib_povirtualdtl_tbl;
      L_rib_podtl_tbl.EXTEND;
      L_rib_podtl_tbl(L_rib_podtl_tbl.COUNT)  := L_rib_podtl_rec;
   end if;

   -- if no data found in cursor, raise error
   if not L_records_found then
      O_error_message := SQL_LIB.CREATE_MSG('NO_ORD_DETAIL_PUB',
                                             I_order_no, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   if L_odp_ins_size > 0 then

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      FORALL i IN 1..L_odp_ins_size
         insert into order_details_published (order_no,
                                              item,
                                              location,
                                              loc_type,
                                              physical_location,
                                              detail_exists_ind)
                                       values(L_odp_ins_order_no(i),
                                              L_odp_ins_item(i),
                                              L_odp_ins_location(i),
                                              L_odp_ins_loc_type(i),
                                              L_odp_ins_physical_location(i),
                                              'Y');

   end if;

   if L_odp_upd_size > 0 then

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      FORALL i IN 1..L_odp_upd_size
         update order_details_published
            set detail_exists_ind = 'Y'
          where rowid = L_odp_upd_rowid(i);

   end if;

   if L_rib_podtl_tbl.COUNT > 0 then
      O_message := L_rib_podtl_tbl;
   else
      O_message := NULL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END BUILD_DETAIL_OBJECTS;

------------------------------------------------------------------------------------------
-----
---- BUILD_SINGLE_DETAIL adds a single detail node to a message at the virtual location level.
---- If the physical location has changed from the previous call, a new PODTL node with one
---- POVIRTUALDTL node is created.  Otherwise, a POVIRTUALDTL node is added to the current
---- PODTL node.
-----
FUNCTION BUILD_SINGLE_DETAIL (O_error_message                IN OUT          VARCHAR2,
                              O_message                      IN OUT NOCOPY   "RIB_PODtl_TBL",
                              IO_order_mfqueue_rowid         IN OUT NOCOPY   ROWID_TBL,
                              IO_order_mfqueue_size          IN OUT          BINARY_INTEGER,
                              IO_routing_info                IN OUT NOCOPY   RIB_ROUTINGINFO_TBL,
                              IO_rib_podtl_rec               IN OUT NOCOPY   "RIB_PODtl_REC",
                              IO_rib_povirtualdtl_rec        IN OUT NOCOPY   "RIB_POVirtualDtl_REC",
                              IO_rib_povirtualdtl_tbl        IN OUT NOCOPY   "RIB_POVirtualDtl_TBL",
                              IO_rib_routing_rec             IN OUT NOCOPY   RIB_ROUTINGINFO_REC,
                              IO_prev_item                   IN OUT          ORDLOC.ITEM%TYPE,
                              IO_prev_location               IN OUT          ORDLOC.LOCATION%TYPE,
                              IO_prev_physical_loc           IN OUT          ORDLOC.LOCATION%TYPE,
                              IO_physical_qty                IN OUT          ORDLOC.QTY_ORDERED%TYPE,
                              IO_odp_ins_order_no            IN OUT NOCOPY   ODP_ORDER_NO_TBL,
                              IO_odp_ins_item                IN OUT NOCOPY   ODP_ITEM_TBL,
                              IO_odp_ins_location            IN OUT NOCOPY   ODP_LOCATION_TBL,
                              IO_odp_ins_loc_type            IN OUT NOCOPY   ODP_LOC_TYPE_TBL,
                              IO_odp_ins_physical_location   IN OUT NOCOPY   ODP_PHYSICAL_LOCATION_TBL,
                              IO_odp_ins_size                IN OUT          BINARY_INTEGER,
                              IO_odp_upd_rowid               IN OUT NOCOPY   ROWID_TBL,
                              IO_odp_upd_size                IN OUT          BINARY_INTEGER,
                              I_detail_exists_ind            IN              VARCHAR2,
                              IO_extend                      IN OUT          BOOLEAN,
                              I_item                         IN              ORDLOC.ITEM%TYPE,
                              I_location                     IN              ORDLOC.LOCATION%TYPE,
                              I_loc_type                     IN              ORDLOC.LOC_TYPE%TYPE,
                              I_physical_location            IN              ORDLOC.LOCATION%TYPE,
                              I_physical_store_type          IN              STORE.STORE_TYPE%TYPE,
                              I_physical_stockholding_ind    IN              STORE.STOCKHOLDING_IND%TYPE,
                              I_unit_cost                    IN              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                              I_qty_ordered                  IN              ORDLOC.QTY_ORDERED%TYPE,
                              I_tsf_po_link_no               IN              ORDLOC.TSF_PO_LINK_NO%TYPE,
                              I_estimated_instock_date       IN              ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE,
                              I_ref_item                     IN              ORDSKU.REF_ITEM%TYPE,
                              I_supp_pack_size               IN              ORDSKU.SUPP_PACK_SIZE%TYPE,
                              I_origin_country_id            IN              ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                              I_earliest_ship_date           IN              ORDSKU.EARLIEST_SHIP_DATE%TYPE,
                              I_latest_ship_date             IN              ORDSKU.LATEST_SHIP_DATE%TYPE,
                              I_pickup_loc                   IN              ORDSKU.PICKUP_LOC%TYPE,
                              I_pickup_no                    IN              ORDSKU.PICKUP_NO%TYPE,
                              I_round_lvl                    IN              ITEM_SUPP_COUNTRY.ROUND_LVL%TYPE,
                              I_packing_method               IN              ITEM_SUPP_COUNTRY.PACKING_METHOD%TYPE,
                              I_oq_rowid                     IN              ROWID,
                              I_odp_rowid                    IN              ROWID,
                              I_order_no                     IN              ORDHEAD.ORDER_NO%TYPE,
                              O_delete_rowid_ind             IN OUT          VARCHAR2,
                              IO_details_processed           IN OUT          RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE,
                              I_max_details                  IN              RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE,
                              I_transaction_uom              IN              ORDCUST_DETAIL.TRANSACTION_UOM%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'RMSMFM_ORDER.BUILD_SINGLE_DETAIL';

BEGIN

   ---
   -- If the item or location is different from the previous call to BUILD_SINGLE_DETAIL,
   -- a detail node is added to the message.
   -- I_loc_type will be NULL when the calling function is processing a record on the queue
   -- instead of a record on the ordloc table.
   -- In that case, we want to skip the processing that adds a detail node to the message
   -- and just delete the record from the queue.
   ---
   if (I_loc_type is NOT NULL) and
      ((I_physical_location != IO_prev_physical_loc) or
       (I_item              != IO_prev_item) or
       (I_location          != IO_prev_location  )) then

      if IO_details_processed >= I_max_details then
         O_delete_rowid_ind := 'N';
         return TRUE;
      end if;

      IO_prev_location := I_location;

      ---
      -- If the physical location or item has changed, a new PODTL node
      -- needs to be created.
      -- Otherwise, a PODTLVIRTUAL node will be added to the current PODTL node.
      ---
      if (I_physical_location != IO_prev_physical_loc) or
         (I_item              != IO_prev_item) then

         ---
         -- If the physical location has changed, add the physical location
         -- to the routing info.  The calling function should order by physical
         -- location first, so that no duplicate physical locations
         -- will be added to the routing info.
         ---
         if (I_physical_location != IO_prev_physical_loc) then

            ------ add physical loc to routing info
            if ROUTING_INFO_ADD(O_error_message,
                                IO_routing_info,
                                I_physical_location,
                                I_loc_type) = FALSE then
               return FALSE;
            end if;

         end if;

         IO_prev_physical_loc                := I_physical_location;
         IO_prev_item                        := I_item;

         if IO_extend then
            IO_rib_podtl_rec.physical_qty_ordered    := IO_physical_qty;
            IO_rib_podtl_rec.povirtualdtl_tbl        := IO_rib_povirtualdtl_tbl;
            O_message.EXTEND;
            O_message(O_message.COUNT) := IO_rib_podtl_rec;

         else
            IO_extend := TRUE;
         end if;

         IO_physical_qty                              := 0;

         IO_rib_podtl_rec.item                        := I_item;
         IO_rib_podtl_rec.ref_item                    := I_ref_item;
         IO_rib_podtl_rec.physical_location_type      := I_loc_type;
         IO_rib_podtl_rec.physical_location           := I_physical_location;
         IO_rib_podtl_rec.physical_store_type         := I_physical_store_type;
         IO_rib_podtl_rec.physical_stockholding_ind   := I_physical_stockholding_ind;
         IO_rib_podtl_rec.transaction_uom             := I_transaction_uom;
         IO_rib_podtl_rec.unit_cost                   := I_unit_cost;
         IO_rib_podtl_rec.origin_country_id           := I_origin_country_id;
         IO_rib_podtl_rec.supp_pack_size              := I_supp_pack_size;
         IO_rib_podtl_rec.earliest_ship_date          := I_earliest_ship_date;
         IO_rib_podtl_rec.latest_ship_date            := I_latest_ship_date;
         IO_rib_podtl_rec.pickup_loc                  := I_pickup_loc;
         IO_rib_podtl_rec.pickup_no                   := I_pickup_no;
         IO_rib_podtl_rec.packing_method              := I_packing_method;
         IO_rib_podtl_rec.round_lvl                   := I_round_lvl;
         IO_rib_podtl_rec.tsf_po_link_id              := I_tsf_po_link_no;
         IO_rib_podtl_rec.est_in_stock_date           := I_estimated_instock_date;

         IO_rib_podtl_rec.door_ind                    := NULL;
         IO_rib_podtl_rec.priority_level              := NULL;
         IO_rib_podtl_rec.new_item                    := NULL;
         IO_rib_podtl_rec.quarantine                  := NULL;
         IO_rib_podtl_rec.rcvd_unit_qty               := NULL;

         --- clear out the virtualdtl table
         IO_rib_povirtualdtl_tbl :=   "RIB_POVirtualDtl_TBL"();

      end if;   -- if physical_loc or item has changed

      ----- add virtual_loc, loc_type, qty_ordered to podtlvirtual node.
      IO_rib_povirtualdtl_rec.qty_ordered             := I_qty_ordered;
      IO_rib_povirtualdtl_rec.location_type           := I_loc_type;
      IO_rib_povirtualdtl_rec.location                := I_location;

      IO_physical_qty    := IO_physical_qty + I_qty_ordered;

      ----- add povirtual node to podtl node
      IO_rib_povirtualdtl_tbl.EXTEND;
      IO_rib_povirtualdtl_tbl(IO_rib_povirtualdtl_tbl.COUNT) := IO_rib_povirtualdtl_rec;

      ------ the order_detail_published record for the current order/item/loc combination
      ------ does not currently exist in the queue,
      ------ so it should be added to the queue
      if I_odp_rowid is NULL then

         IO_odp_ins_size := IO_odp_ins_size + 1;
         IO_odp_ins_order_no(IO_odp_ins_size)          := I_order_no;
         IO_odp_ins_item(IO_odp_ins_size)              := I_item;
         IO_odp_ins_location(IO_odp_ins_size)          := I_location;
         IO_odp_ins_loc_type(IO_odp_ins_size)          := I_loc_type;
         IO_odp_ins_physical_location(IO_odp_ins_size) := I_physical_location;

      elsif I_detail_exists_ind = 'N' then

         IO_odp_upd_size := IO_odp_upd_size + 1;
         IO_odp_upd_rowid(IO_odp_upd_size)             := I_odp_rowid;

      end if;

      IO_details_processed := IO_details_processed + 1;

   end if; -- if item or loc has changed

   if I_oq_rowid is not null then
      IO_order_mfqueue_size := IO_order_mfqueue_size + 1;
      IO_order_mfqueue_rowid(IO_order_mfqueue_size) := I_oq_rowid;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END BUILD_SINGLE_DETAIL;

------------------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_CHANGE_OBJECTS(O_error_message     IN OUT        VARCHAR2,
                                     O_message           IN OUT nocopy RIB_OBJECT,
                                     O_routing_info      IN OUT nocopy RIB_ROUTINGINFO_TBL,
                                     I_supplier          IN            ordhead.supplier%TYPE,
                                     IO_message_type     IN OUT        order_mfqueue.message_type%TYPE,
                                     I_order_no          IN            ordhead.order_no%TYPE,
                                     I_item              IN            order_mfqueue.item%TYPE,
                                     I_physical_location IN            order_mfqueue.physical_location%TYPE,
                                     I_max_details       IN            rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'RMSMFM_ORDER.BUILD_DETAIL_CHANGE_OBJECTS';

   L_supplier             ordhead.supplier%TYPE := I_supplier;

   L_rib_podesc_rec       "RIB_PODesc_REC" := NULL;

   L_rib_routing_rec      RIB_ROUTINGINFO_REC := NULL;

   L_order_mfqueue_rowid  rowid_TBL;
   L_order_mfqueue_size   BINARY_INTEGER := 0;

   L_delete_rowid_ind     VARCHAR2(1);

   PROGRAM_ERROR          EXCEPTION;

BEGIN

   if O_message is NULL then
      if BUILD_HEADER_OBJECT( O_error_message,
                              L_rib_podesc_rec,
                              L_supplier,
                              I_order_no) = FALSE then
      return FALSE;
   end if;

   else
      L_rib_podesc_rec := treat (O_message as "RIB_PODesc_REC");
   end if;

   if BUILD_DETAIL_OBJECTS( O_error_message,
                            L_rib_podesc_rec.podtl_tbl,
                            L_order_mfqueue_rowid,
                            L_order_mfqueue_size,
                            O_routing_info,
                            L_delete_rowid_ind,
                            IO_message_type,
                            L_supplier,
                            I_order_no,
                            I_item,
                            I_physical_location,
                            I_max_details) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_order_mfqueue_size > 0 then
      FORALL i IN 1..L_order_mfqueue_size
         delete from order_mfqueue where rowid = L_order_mfqueue_rowid(i);
   end if;

   O_message := L_rib_podesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END BUILD_DETAIL_CHANGE_OBJECTS;
------------------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_DELETE(O_error_message       IN OUT          VARCHAR2,
                             O_message_type        IN OUT          VARCHAR2,
                             O_message             IN OUT NOCOPY   RIB_OBJECT,
                             O_break_loop          IN OUT          BOOLEAN,
                             I_order_no            IN              ORDHEAD.ORDER_NO%TYPE,
                             I_item                IN              ORDLOC.ITEM%TYPE,
                             I_location            IN              ORDLOC.LOCATION%TYPE,
                             I_loc_type            IN              ORDLOC.LOC_TYPE%TYPE,
                             I_physical_location   IN              ORDLOC.LOCATION%TYPE,
                             I_rowid               IN              ROWID)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'RMSMFM_ORDER.BUILD_DETAIL_DELETE';

   L_odp_rowid            ROWID := NULL;
   L_supplier             ORDHEAD.SUPPLIER%TYPE := NULL;

   L_rib_podesc_rec       "RIB_PODesc_REC"      := NULL;
   L_rib_podtl_tbl        "RIB_PODtl_TBL"       := NULL;

   L_order_mfqueue_rowid  ROWID_TBL;
   L_order_mfqueue_size   BINARY_INTEGER := 0;
   L_odp_rowid_tbl        ROWID_TBL;
   L_odp_size             BINARY_INTEGER := 0;

   L_store_type           STORE.STORE_TYPE%TYPE       := NULL;
   L_stockholding_ind     STORE.STOCKHOLDING_IND%TYPE := NULL;

   cursor C_ORDER_DETAIL_PUBLISHED is
      select rowid
        from order_details_published odp
       where odp.order_no    = I_order_no
         and odp.item        = I_item
         and odp.location    = I_location;

   cursor C_GET_SUPPLIER is
      select supplier
        from ordhead
       where order_no = I_order_no;

   cursor C_GET_STORE_DTL is
      select store_type,
             stockholding_ind
        from store
       where store = I_physical_location;

BEGIN

   -- if the order_no/item/virtual location combination create message
   -- has not been published, the corresponding delete message
   -- for this order_no/item/virtual location should not be published

   open C_ORDER_DETAIL_PUBLISHED;
   fetch C_ORDER_DETAIL_PUBLISHED into L_odp_rowid;
   close C_ORDER_DETAIL_PUBLISHED;

   if L_odp_rowid is NULL then
      O_break_loop := FALSE;
      ---
      LP_error_status := API_CODES.UNHANDLED_ERROR;
      delete from order_mfqueue
       where rowid = I_rowid;
      ---
      return TRUE;
   end if;

   open C_GET_SUPPLIER;
   fetch C_GET_SUPPLIER into L_supplier;
   close C_GET_SUPPLIER;

   if I_loc_type = 'S' then
      open C_GET_STORE_DTL;
      fetch C_GET_STORE_DTL into L_store_type,
                                 L_stockholding_ind;
      close C_GET_STORE_DTL;
   end if;

   ---
   -- If the DTL_DEL message on the queue is for a Warehouse, the message
   -- that will be sent may be a DTL_UPD.  This would occur if one virtual wh was deleted
   -- from the order, but other virtual wh's within the same physical wh still exist
   -- on the order.  BUILD_DETAIL_DELETE_WH determines the message type, and if it is
   -- a DTL_UPD message, it builds the message.
   ---
   if I_loc_type = 'W' then

      L_rib_podtl_tbl := "RIB_PODtl_TBL"();
      ---
      if BUILD_DETAIL_DELETE_WH(O_error_message,
                                O_message_type,
                                L_rib_podtl_tbl,
                                L_order_mfqueue_rowid,
                                L_order_mfqueue_size,
                                L_odp_rowid_tbl,
                                L_odp_size,
                                I_order_no,
                                L_supplier,
                                I_item,
                                I_physical_location) = FALSE then
         return FALSE;
      end if;

   else
      O_message_type := DTL_DEL;
   end if;

   ---
   -- If it turns out that the message on the queue actually corresponds to a DTL_DEL
   -- message, the ref object for the message is created.
   ---
   if O_message_type = DTL_DEL then
      O_message := "RIB_PORef_REC"(
           0,                        -- rib_oid number
           'P',                      -- doc_type            VARCHAR2(1),
           I_order_no,               -- order_no            NUMBER(12),
           "RIB_PODtlRef_TBL"("RIB_PODtlRef_REC"(0,
                                             I_item,                -- item
                                             I_loc_type,            -- physical_location_type
                                             I_physical_location,   -- physical_location
                                             L_store_type,          -- physical_store_type
                                             L_stockholding_ind))); -- physical_stockholding_ind
   ---
   -- If it turns out that the message on the queue actually corresponds to a DTL_UPD
   -- message, the header object for the message is created, and the detail and header
   -- object are merged together.
   ---
   else
      if BUILD_HEADER_OBJECT( O_error_message,
                              L_rib_podesc_rec,
                              L_supplier,
                              I_order_no) = FALSE then
         return FALSE;
      end if;
      ---
      L_rib_podesc_rec.podtl_tbl := L_rib_podtl_tbl;
      O_message := L_rib_podesc_rec;
   end if;

   -- Include current rowid in list of rowid's on queue table
   -- to delete.
   if L_order_mfqueue_size = 0 then
      L_order_mfqueue_size := L_order_mfqueue_size + 1;
      L_order_mfqueue_rowid(L_order_mfqueue_size) := I_rowid;
   end if;

   if L_odp_size = 0 then
      L_odp_size := L_odp_size + 1;
      L_odp_rowid_tbl(L_odp_size) := L_odp_rowid;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   ---
   -- delete record(s) from order_details_published and order_mfqueue
   ---
   FORALL i IN 1..L_odp_size
      update order_details_published
         set detail_exists_ind = 'N'
       where rowid = L_odp_rowid_tbl(i);

   FORALL i IN 1..L_order_mfqueue_size
      delete from order_mfqueue
       where rowid = L_order_mfqueue_rowid(i);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;

END BUILD_DETAIL_DELETE;
------------------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_DELETE_WH(O_error_message        IN OUT        VARCHAR2,
                                O_message_type         IN OUT        VARCHAR2,
                                O_rib_podtl_tbl        IN OUT nocopy "RIB_PODtl_TBL",
                                O_order_mfqueue_rowid  IN OUT nocopy rowid_TBL,
                                O_order_mfqueue_size   IN OUT        BINARY_INTEGER,
                                O_odp_rowid            IN OUT nocopy rowid_TBL,
                                O_odp_size             IN OUT        BINARY_INTEGER,
                                I_order_no             IN            ordhead.order_no%TYPE,
                                I_supplier             IN            ordhead.supplier%TYPE,
                                I_item                 IN            ordloc.item%TYPE,
                                I_physical_location    IN            ordloc.location%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'RMSMFM_ORDER.BUILD_DETAIL_DELETE_WH';

   L_delete_rowid_ind        VARCHAR2(1) := 'Y';

   L_prev_physical_loc       ordloc.location%TYPE := -1;
   L_prev_location           ordloc.location%TYPE := -1;
   L_prev_item               ordloc.item%TYPE     := -1;
   L_physical_quantity       ordloc.qty_ordered%TYPE := 0;

   L_rib_podtl_rec           "RIB_PODtl_REC" := NULL;
   L_routing_info            RIB_ROUTINGINFO_TBL := NULL;
   L_rib_povirtualdtl_rec    "RIB_POVirtualDtl_REC" := NULL;
   L_rib_povirtualdtl_tbl    "RIB_POVirtualDtl_TBL" := NULL;

   L_odp_ins_order_no            odp_order_no_TBL;
   L_odp_ins_item                odp_item_TBL;
   L_odp_ins_location            odp_location_TBL;
   L_odp_ins_loc_type            odp_loc_type_TBL;
   L_odp_ins_physical_location   odp_physical_location_TBL;
   L_odp_ins_size                BINARY_INTEGER := 0;

   L_odp_upd_rowid               rowid_TBL;
   L_odp_upd_size                BINARY_INTEGER := 0;

   L_extend                      BOOLEAN := FALSE;
   L_records_found               BOOLEAN := FALSE;
   L_details_processed           rib_settings.max_details_to_publish%TYPE := 0;

   L_rib_routing_rec             RIB_ROUTINGINFO_REC :=NULL;

   L_detail_exists_ind           order_details_published.detail_exists_ind%TYPE := NULL;

   ---
   -- This cursor selects all ordloc records on the queue
   -- for the current order_no/item/physical location.
   -- It also unions to the order_mfqueue table
   -- to select all messages associated with the current
   -- order_no/item/physical_location.
   -- This is all done to get the current state of the current ord/item/phys loc.
   -- If there are records on the ordloc table, a DTL_UPD message will be sent.
   -- Otherwise, a DTL_DEL message will be sent.
   ---
   cursor C_GET_DETAIL_MSG_INFO is
      select ol.item,
             ol.location,
             ol.loc_type,
             ol.unit_cost,
             ol.qty_ordered,
             ol.tsf_po_link_no,
             ol.estimated_instock_date,
             os.ref_item,
             os.supp_pack_size,
             os.origin_country_id,
             os.earliest_ship_date,
             os.latest_ship_date,
             os.pickup_loc,
             os.pickup_no,
             isc.round_lvl,
             isc.packing_method,
             NULL odp_rowid,
             NULL oq_rowid
        from ordloc ol,
             ordsku os,
             item_supp_country isc,
             wh
       where os.order_no          = ol.order_no
         and ol.item              = os.item
         and ol.order_no          = os.order_no
         and os.item              = isc.item
         and isc.supplier         = I_supplier
         and os.origin_country_id = isc.origin_country_id
         and ol.order_no        = I_order_no
         and ol.item            = I_item
         and ol.location        = wh.wh
         and wh.physical_wh     = I_physical_location
         and ol.item not in (select item
                               from item_master
                              where deposit_item_type='A')
       UNION ALL
      select NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             odp.rowid odp_rowid,
             q.rowid oq_rowid
        from order_mfqueue q,
             order_details_published odp
       where q.order_no          = I_order_no
         and q.item              = I_item
         and q.physical_location = I_physical_location
         and q.message_type      in (DTL_ADD, DTL_UPD, DTL_DEL)
         and odp.order_no (+)    = q.order_no
         and odp.item     (+)    = q.item
         and odp.location (+)    = q.location
         and q.item not in (select item
                              from item_master
                             where deposit_item_type='A')

      order by 1, 2;

BEGIN

   L_rib_podtl_rec := "RIB_PODtl_REC"(
           0, null,null,null,null,null,null,null,
           null,null,null,null,null,null,null,
           null,null,null,null,null,null,null,
           null,null,null,null);

   L_rib_povirtualdtl_rec := "RIB_POVirtualDtl_REC"(0, null, null, null);

   for rec in C_GET_DETAIL_MSG_INFO LOOP

      ---
      -- If rec.item is not NULL, that means a record was found on ordloc.
      -- The message will be a DTL_UPD message.
      ---
      if rec.item is not NULL then
         L_records_found := TRUE;
      end if;

      L_details_processed := 0;

      ---
      -- If the current record retrived from the cursor has ordloc info,
      -- BUILD_SINGLE_DETAIL will add that info to the current PODTL message.
      ---
      if BUILD_SINGLE_DETAIL ( O_error_message,
                               O_rib_podtl_tbl,
                               O_order_mfqueue_rowid,
                               O_order_mfqueue_size,
                               L_routing_info,
                               L_rib_podtl_rec,
                               L_rib_povirtualdtl_rec,
                               L_rib_povirtualdtl_tbl,
                               L_rib_routing_rec,
                               L_prev_item,
                               L_prev_location,
                               L_prev_physical_loc,
                               L_physical_quantity,
                               L_odp_ins_order_no,
                               L_odp_ins_item,
                               L_odp_ins_location,
                               L_odp_ins_loc_type,
                               L_odp_ins_physical_location,
                               L_odp_ins_size,
                               L_odp_upd_rowid,
                               L_odp_upd_size,
                               L_detail_exists_ind,
                               L_extend,
                               rec.item,
                               rec.location,
                               rec.loc_type,
                               I_physical_location,
                               NULL,
                               NULL,
                               rec.unit_cost,
                               rec.qty_ordered,
                               rec.tsf_po_link_no,
                               rec.estimated_instock_date,
                               rec.ref_item,
                               rec.supp_pack_size,
                               rec.origin_country_id,
                               rec.earliest_ship_date,
                               rec.latest_ship_date,
                               rec.pickup_loc,
                               rec.pickup_no,
                               rec.round_lvl,
                               rec.packing_method,
                               rec.oq_rowid,
                               rec.odp_rowid,
                               I_order_no,
                               L_delete_rowid_ind,
                               L_details_processed,
                               1) = FALSE then          ---- 1 = max details to publish
         return FALSE;
      end if;

      ---
      -- Add the current rowid from order_details_published to an array of rowids.
      -- This array will be used to delete rows from order_details_published.
      ---
      if rec.odp_rowid is not NULL then
         O_odp_size := O_odp_size + 1;
         O_odp_rowid(O_odp_size) := rec.odp_rowid;
      end if;

   END LOOP;

   ---
   -- If one or more records were found on ordloc,
   -- the message is a DTL_UPD message.  Otherwise, it is a DTL_DEL message.
   ---
   if L_records_found then

      L_rib_podtl_rec.physical_qty_ordered    := L_physical_quantity;
      L_rib_podtl_rec.povirtualdtl_tbl        := L_rib_povirtualdtl_tbl;
      O_rib_podtl_tbl.EXTEND;
      O_rib_podtl_tbl(O_rib_podtl_tbl.COUNT)  := L_rib_podtl_rec;

      O_message_type := DTL_UPD;

   else

      O_message_type := DTL_DEL;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END BUILD_DETAIL_DELETE_WH;

------------------------------------------------------------------------------------------
FUNCTION GET_ROUTING_TO_LOCS(O_error_message IN OUT        VARCHAR2,
                             I_order_no      IN            ordhead.order_no%TYPE,
                             O_routing_info  IN OUT nocopy RIB_ROUTINGINFO_TBL)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'RMSMFM_ORDER.GET_ROUTING_TO_LOCS';

   cursor C_TO_LOCS is
      select distinct physical_location,
                      loc_type
        from order_details_published
       where order_no = I_order_no;

BEGIN

   FOR rec IN C_TO_LOCS LOOP

      if ROUTING_INFO_ADD(O_error_message,
                          O_routing_info,
                          rec.physical_location,
                          rec.loc_type) = FALSE then
         return FALSE;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_ROUTING_TO_LOCS;
--------------------------------------------------------------------------------
FUNCTION GET_MSG_HEADER(O_error_message     OUT  VARCHAR2,
                        O_ordhead_struct IN OUT  ORDHEAD_MSG_RECTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'RMSMFM_ORDER.GET_MSG_HEADER';

   L_terms_desc   TERMS.TERMS_DESC%TYPE;
   L_pm_promo_rec PM_PROMO_SQL.PROMO_REC := NULL;
BEGIN
   --- get po type desc
   if O_ordhead_struct.ordhead_rec.po_type is not NULL then
      if ORDER_ATTRIB_SQL.GET_PO_TYPE_DESC(O_error_message,
                                           O_ordhead_struct.ordhead_rec.po_type,
                                           O_ordhead_struct.po_type_desc) = FALSE then
         return FALSE;
      end if;
   end if;
   --- get buyer name
   if O_ordhead_struct.ordhead_rec.buyer is not NULL then
      if BUYER_ATTRIB_SQL.GET_NAME(O_ordhead_struct.ordhead_rec.buyer,
                                   O_ordhead_struct.buyer_name,
                                   O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   --- get dept name
   if O_ordhead_struct.ordhead_rec.dept is not NULL then
      if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                  O_ordhead_struct.ordhead_rec.dept,
                                  O_ordhead_struct.dept_name) = FALSE then
         return FALSE;
      end if;
   end if;
   --- get promotion desc
   if O_ordhead_struct.ordhead_rec.promotion is not NULL then
      if PM_PROMO_SQL.GET_PROMO(O_error_message,
                                O_ordhead_struct.ordhead_rec.promotion,
                                L_pm_promo_rec) = FALSE then
         return FALSE;
      end if;
      --
      O_ordhead_struct.promotion_desc := L_pm_promo_rec.promo_desc;
   end if;
   --- get payment terms desc
   if O_ordhead_struct.ordhead_rec.terms is not NULL then
      if ORDER_ATTRIB_SQL.PAY_TERMS_DESC(O_error_message,
                                         O_ordhead_struct.ordhead_rec.terms,
                                         O_ordhead_struct.terms_code,
                                         L_terms_desc) = FALSE then
         return FALSE;
      end if;
   end if;
   --- get order type description
   if O_ordhead_struct.ordhead_rec.order_type is not NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'ORDO',
                                    O_ordhead_struct.ordhead_rec.order_type,
                                    O_ordhead_struct.order_type_desc) = FALSE then
         return FALSE;
      end if;
   end if;
  --- get payment method description
   if O_ordhead_struct.ordhead_rec.payment_method is not NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'PYMT',
                                    O_ordhead_struct.ordhead_rec.payment_method,
                                    O_ordhead_struct.payment_method_desc) = FALSE then
         return FALSE;
      end if;
   end if;
   --- get backhaul type description
   if O_ordhead_struct.ordhead_rec.backhaul_type is not NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'BKHL',
                                    O_ordhead_struct.ordhead_rec.backhaul_type,
                                    O_ordhead_struct.backhaul_type_desc) = FALSE then
         return FALSE;
      end if;
   end if;
   --- get ship method description
   if O_ordhead_struct.ordhead_rec.ship_method is not NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'SHPM',
                                    O_ordhead_struct.ordhead_rec.ship_method,
                                    O_ordhead_struct.ship_method_desc) = FALSE then
         return FALSE;
      end if;
   end if;
   --- get purchase type description
   if O_ordhead_struct.ordhead_rec.purchase_type is not NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'PURT',
                                    O_ordhead_struct.ordhead_rec.purchase_type,
                                    O_ordhead_struct.purchase_type_desc) = FALSE then
         return FALSE;
      end if;
   end if;
   --- get ship pay method description
   if O_ordhead_struct.ordhead_rec.ship_pay_method is not NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'SHMT',
                                    O_ordhead_struct.ordhead_rec.ship_pay_method,
                                    O_ordhead_struct.ship_pay_method_desc) = FALSE then
         return FALSE;
      end if;
   end if;
   --- get trans res description
   if O_ordhead_struct.ordhead_rec.fob_trans_res is not NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'FOBT',
                                    O_ordhead_struct.ordhead_rec.fob_trans_res,
                                    O_ordhead_struct.fob_trans_res_code_desc) = FALSE then
         return FALSE;
      end if;
   end if;
   --- get title pass description
   if O_ordhead_struct.ordhead_rec.fob_title_pass is not NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'FOBT',
                                    O_ordhead_struct.ordhead_rec.fob_title_pass,
                                    O_ordhead_struct.fob_title_pass_code_desc) = FALSE then
         return FALSE;
      end if;
   end if;
   --- get factory description
   if O_ordhead_struct.ordhead_rec.factory is not NULL then
      if PARTNER_SQL.GET_DESC(O_error_message,
                              O_ordhead_struct.factory_desc,
                              O_ordhead_struct.ordhead_rec.factory,
                              'FA') = FALSE then
         return FALSE;
      end if;
   end if;
   --- get agent description
   if O_ordhead_struct.ordhead_rec.agent is not NULL then
      if PARTNER_SQL.GET_DESC(O_error_message,
                              O_ordhead_struct.agent_desc,
                              O_ordhead_struct.ordhead_rec.agent,
                              'AG') = FALSE then
         return FALSE;
      end if;
   end if;
   --- get discharge port description
   if O_ordhead_struct.ordhead_rec.discharge_port is not NULL then
      if OUTSIDE_LOCATION_SQL.GET_DESC(O_error_message,
                                       O_ordhead_struct.discharge_port_desc,
                                       O_ordhead_struct.ordhead_rec.discharge_port,
                                       'DP') = FALSE then
         return FALSE;
      end if;
   end if;
   --- get lading port description
   if O_ordhead_struct.ordhead_rec.lading_port is not NULL then
      if OUTSIDE_LOCATION_SQL.GET_DESC(O_error_message,
                                       O_ordhead_struct.lading_port_desc,
                                       O_ordhead_struct.ordhead_rec.lading_port,
                                       'LP') = FALSE then
         return FALSE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_MSG_HEADER;
------------------------------------------------------------------------------------------
FUNCTION LOCK_THE_BLOCK(O_error_msg        OUT VARCHAR2,
                        O_queue_locked     OUT BOOLEAN,
                        I_order_no      IN     order_mfqueue.order_no%TYPE)
RETURN BOOLEAN IS

   L_table             VARCHAR2(30)  := 'ORDER_MFQUEUE';
   L_key1              VARCHAR2(100) := I_order_no;
   L_key2              VARCHAR2(100) := NULL;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_QUEUE is
      select 'x'
        from order_mfqueue oq
       where oq.order_no = I_order_no
        for update nowait;

BEGIN

   O_queue_locked := FALSE;

   open C_LOCK_QUEUE;
   close C_LOCK_QUEUE;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_msg := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                        L_table,
                                        L_key1,
                                        L_key2);
      O_queue_locked := TRUE;
      return TRUE;
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_ORDER.LOCK_THE_BLOCK',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_THE_BLOCK;
------------------------------------------------------------------------------------------

PROCEDURE HANDLE_ERRORS(O_status_code       IN OUT         VARCHAR2,
                        O_error_message     IN OUT         VARCHAR2,
                        O_message           IN OUT  nocopy RIB_OBJECT,
                        O_bus_obj_id        IN OUT  nocopy RIB_BUSOBJID_TBL,
                        O_routing_info      IN OUT  nocopy RIB_ROUTINGINFO_TBL,
                        I_seq_no            IN             order_mfqueue.seq_no%TYPE,
                        I_order_no          IN             order_mfqueue.order_no%TYPE,
                        I_item              IN             order_mfqueue.item%TYPE,
                        I_physical_location IN             order_mfqueue.physical_location%TYPE,
                        I_loc_type          IN             order_mfqueue.loc_type%TYPE)
IS

   L_program          VARCHAR2(64) := 'RMSMFM_ORDER.HANDLE_ERRORS';
   -- for error handling
   L_rib_podtlref_rec "RIB_PODtlRef_REC";
   L_rib_podtlref_tbl "RIB_PODtlRef_TBL";
   L_rib_poref_rec    "RIB_PORef_REC";

   L_error_type       VARCHAR2(5) := NULL;
BEGIN

   O_status_code   := LP_error_status;

   if O_status_code = API_CODES.HOSPITAL then

      O_bus_obj_id    := RIB_BUSOBJID_TBL(I_order_no);
      O_routing_info  := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no', I_seq_no,null,null,null,null));

      L_rib_podtlref_rec := "RIB_PODtlRef_REC"(0,I_item,I_loc_type,I_physical_location,null,null);
      L_rib_podtlref_tbl := "RIB_PODtlRef_TBL"(L_rib_podtlref_rec);
      L_rib_poref_rec    := "RIB_PORef_REC"(0,
                                          'P',
                                          I_order_no,
                                          L_rib_podtlref_tbl);
      O_message := L_rib_poref_rec;

      update order_mfqueue
         set pub_status = LP_error_status
       where seq_no    = I_seq_no;

   end if;

   /* Pass out parsed error message */
   SQL_LIB.API_MSG(L_error_type,
                   O_error_message);

EXCEPTION
   when OTHERS then
      O_status_code := API_CODES.UNHANDLED_ERROR;
      ---
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));


END HANDLE_ERRORS;
------------------------------------------------------------------------------------------
END RMSMFM_ORDER;
/
