create or replace
PACKAGE BODY ORDER_SQL AS

----------------------------------------------------------------------------
-- Private Function
-- Function    : UPDATE_ORDHEAD_L10N_EXT
-- Purpose     : This private function will delete the old util code record
--                and insert a record for the new import country.
----------------------------------------------------------------------------
FUNCTION UPDATE_ORDHEAD_L10N_EXT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_ordhead_row     IN       ORDHEAD%ROWTYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------

   -- PUBLIC FUNCTIONS
----------------------------------------------------------------------------
FUNCTION INSERT_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_rec       IN       ORDER_SQL.ORDER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'ORDER_SQL.INSERT_ORDER';
   L_l10n_obj     L10N_OBJ      := L10N_OBJ();

BEGIN
   -- Insert order header record
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDHEAD', 'order_no: '||to_char(I_order_rec.ordhead_row.order_no));

   insert into ordhead (order_no,
                        order_type,
                        dept,
                        buyer,
                        supplier,
                        supp_add_seq_no,
                        loc_type,
                        location,
                        promotion,
                        qc_ind,
                        written_date,
                        not_before_date,
                        not_after_date,
                        otb_eow_date,
                        earliest_ship_date,
                        latest_ship_date,
                        close_date,
                        terms,
                        freight_terms,
                        orig_ind,
                        payment_method,
                        backhaul_type,
                        backhaul_allowance,
                        ship_method,
                        purchase_type,
                        status,
                        orig_approval_date,
                        orig_approval_id,
                        ship_pay_method,
                        fob_trans_res,
                        fob_trans_res_desc,
                        fob_title_pass,
                        fob_title_pass_desc,
                        edi_sent_ind,
                        edi_po_ind,
                        import_order_ind,
                        import_country_id,
                        po_ack_recvd_ind,
                        include_on_order_ind,
                        vendor_order_no,
                        exchange_rate,
                        factory,
                        agent,
                        discharge_port,
                        lading_port,
                        freight_contract_no,
                        po_type,
                        pre_mark_ind,
                        currency_code,
                        reject_code,
                        contract_no,
                        last_sent_rev_no,
                        split_ref_ordno,
                        pickup_loc,
                        pickup_no,
                        pickup_date,
                        app_datetime,
                        comment_desc,
                        partner_type_1,
                        partner1,
                        partner_type_2,
                        partner2,
                        partner_type_3,
                        partner3,
                        item,
                        import_id,
                        import_type,
                        routing_loc_id,
                        clearing_zone_id,
                        delivery_supplier,
                        triangulation_ind)
                values (I_order_rec.ordhead_row.order_no,
                        I_order_rec.ordhead_row.order_type,
                        I_order_rec.ordhead_row.dept,
                        I_order_rec.ordhead_row.buyer,
                        I_order_rec.ordhead_row.supplier,
                        I_order_rec.ordhead_row.supp_add_seq_no,
                        I_order_rec.ordhead_row.loc_type,
                        I_order_rec.ordhead_row.location,
                        I_order_rec.ordhead_row.promotion,
                        I_order_rec.ordhead_row.qc_ind,
                        I_order_rec.ordhead_row.written_date,
                        I_order_rec.ordhead_row.not_before_date,
                        I_order_rec.ordhead_row.not_after_date,
                        I_order_rec.ordhead_row.otb_eow_date,
                        I_order_rec.ordhead_row.earliest_ship_date,
                        I_order_rec.ordhead_row.latest_ship_date,
                        I_order_rec.ordhead_row.close_date,
                        I_order_rec.ordhead_row.terms,
                        I_order_rec.ordhead_row.freight_terms,
                        I_order_rec.ordhead_row.orig_ind,
                        I_order_rec.ordhead_row.payment_method,
                        I_order_rec.ordhead_row.backhaul_type,
                        I_order_rec.ordhead_row.backhaul_allowance,
                        I_order_rec.ordhead_row.ship_method,
                        I_order_rec.ordhead_row.purchase_type,
                        I_order_rec.ordhead_row.status,
                        I_order_rec.ordhead_row.orig_approval_date,
                        I_order_rec.ordhead_row.orig_approval_id,
                        I_order_rec.ordhead_row.ship_pay_method,
                        I_order_rec.ordhead_row.fob_trans_res,
                        I_order_rec.ordhead_row.fob_trans_res_desc,
                        I_order_rec.ordhead_row.fob_title_pass,
                        I_order_rec.ordhead_row.fob_title_pass_desc,
                        I_order_rec.ordhead_row.edi_sent_ind,
                        I_order_rec.ordhead_row.edi_po_ind,
                        I_order_rec.ordhead_row.import_order_ind,
                        I_order_rec.ordhead_row.import_country_id,
                        I_order_rec.ordhead_row.po_ack_recvd_ind,
                        I_order_rec.ordhead_row.include_on_order_ind,
                        I_order_rec.ordhead_row.vendor_order_no,
                        I_order_rec.ordhead_row.exchange_rate,
                        I_order_rec.ordhead_row.factory,
                        I_order_rec.ordhead_row.agent,
                        I_order_rec.ordhead_row.discharge_port,
                        I_order_rec.ordhead_row.lading_port,
                        I_order_rec.ordhead_row.freight_contract_no,
                        I_order_rec.ordhead_row.po_type,
                        I_order_rec.ordhead_row.pre_mark_ind,
                        I_order_rec.ordhead_row.currency_code,
                        I_order_rec.ordhead_row.reject_code,
                        I_order_rec.ordhead_row.contract_no,
                        I_order_rec.ordhead_row.last_sent_rev_no,
                        I_order_rec.ordhead_row.split_ref_ordno,
                        I_order_rec.ordhead_row.pickup_loc,
                        I_order_rec.ordhead_row.pickup_no,
                        I_order_rec.ordhead_row.pickup_date,
                        I_order_rec.ordhead_row.app_datetime,
                        I_order_rec.ordhead_row.comment_desc,
                        I_order_rec.ordhead_row.partner_type_1,
                        I_order_rec.ordhead_row.partner1,
                        I_order_rec.ordhead_row.partner_type_2,
                        I_order_rec.ordhead_row.partner2,
                        I_order_rec.ordhead_row.partner_type_3,
                        I_order_rec.ordhead_row.partner3,
                        I_order_rec.ordhead_row.item,
                        I_order_rec.ordhead_row.import_id,
                        I_order_rec.ordhead_row.import_type,
                        I_order_rec.ordhead_row.routing_loc_id,
                        I_order_rec.ordhead_row.clearing_zone_id,
                        I_order_rec.ordhead_row.delivery_supplier,
                        nvl(I_order_rec.ordhead_row.triangulation_ind, 'N'));
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_INSERT_REC');
      return FALSE;
   end if;

   --- Put the entries for the utilization code.
   L_l10n_obj.procedure_key := 'CREATE_TRAN_UTIL_CODE';
   L_l10n_obj.doc_type := 'PO';
   L_l10n_obj.doc_id := I_order_rec.ordhead_row.order_no;
   L_l10n_obj.country_id := I_order_rec.ordhead_row.import_country_id;

   --- Make a call to the l10n function to get default util code
   if L10N_SQL.EXEC_FUNCTION (O_error_message,
                              L_l10n_obj) = FALSE then
         return FALSE;
   end if;

   -- If ordhead record was successfully inserted, then bulk insert ordsku records
   if NOT INSERT_ORDSKUS(O_error_message,
                         I_order_rec.ordskus) then
      return FALSE;
   end if;

   -- If ordsku records were successfully inserted, then bulk insert ordloc records
   if NOT INSERT_ORDLOCS(O_error_message,
                         I_order_rec.ordlocs) then
      return FALSE;
   end if;

   -- Insert ord_tax_breakup
   if I_order_rec.ordhead_row.status = 'A' then
      L_l10n_obj.procedure_key := 'LOAD_ORDER_TAX_OBJECT';
      L_l10n_obj.doc_type := 'PO';
      L_l10n_obj.doc_id := I_order_rec.ordhead_row.order_no;

      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_l10n_obj) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_ORDER;
----------------------------------------------------------------------------
FUNCTION INSERT_ORDSKUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordskus         IN       ORDER_SQL.ORDSKU_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORDER_SQL.INSERT_ORDSKUS';

BEGIN
   -- bulk insert into ordsku
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU', 'order_no: '||to_char(I_ordskus.order_no));
   FORALL i IN 1 ..I_ordskus.items.COUNT
      insert into ordsku (order_no,
                          item,
                          ref_item,
                          origin_country_id,
                          earliest_ship_date,
                          latest_ship_date,
                          supp_pack_size,
                          non_scale_ind)
                  values (I_ordskus.order_no,
                          I_ordskus.items(i),
                          I_ordskus.ref_items(i),
                          I_ordskus.origin_country_ids(i),
                          I_ordskus.earliest_ship_dates(i),
                          I_ordskus.latest_ship_dates(i),
                          I_ordskus.supp_pack_sizes(i),
                          I_ordskus.non_scale_inds(i));

   if ORDER_SETUP_SQL.UPDATE_SHIP_DATES (O_error_message,
                                         I_ordskus.order_no) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_ORDSKUS;
----------------------------------------------------------------------------
FUNCTION INSERT_ORDLOCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordlocs         IN       ORDER_SQL.ORDLOC_REC)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(50) := 'ORDER_SQL.INSERT_ORDLOCS';
   L_supplier              ORDHEAD.SUPPLIER%TYPE;
   L_ordlocs               ORDER_SQL.ORDLOC_REC;

   cursor C_GET_SUPPLIER is
      select supplier
        from ordhead
       where order_no = I_ordlocs.order_no;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_SUPPLIER',
                    'ORDLOC',
                    'order_no: '||I_ordlocs.order_no);
   open C_GET_SUPPLIER;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_SUPPLIER',
                    'ORDLOC',
                    'order_no: '||I_ordlocs.order_no);
   fetch C_GET_SUPPLIER into L_supplier;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_SUPPLIER',
                    'ORDLOC',
                    'order_no: '||I_ordlocs.order_no);
   close C_GET_SUPPLIER;

   -- initialize collections
   L_ordlocs.items := ITEM_TBL();
   L_ordlocs.locations := LOC_TBL();
   L_ordlocs.loc_types := LOC_TYPE_TBL();
   L_ordlocs.unit_retails := UNIT_RETAIL_TBL();
   L_ordlocs.ordered_qtys := QTY_TBL();
   L_ordlocs.prescaled_qtys := QTY_TBL();
   L_ordlocs.cancelled_qtys := QTY_TBL();
   L_ordlocs.cancel_codes := INDICATOR_TBL();
   L_ordlocs.cancel_dates := DATE_TBL();
   L_ordlocs.cancel_ids := USERID_TBL();
   L_ordlocs.unit_costs := UNIT_COST_TBL();
   L_ordlocs.cost_sources := CODE_TBL();
   L_ordlocs.non_scale_inds := INDICATOR_TBL();
   L_ordlocs.estimated_instock_date := DATE_TBL();

   L_ordlocs.order_no := I_ordlocs.order_no;

      FOR i IN 1 ..I_ordlocs.items.COUNT LOOP

         L_ordlocs.items.EXTEND();
         L_ordlocs.locations.EXTEND();
         L_ordlocs.loc_types.EXTEND();
         L_ordlocs.unit_retails.EXTEND();
         L_ordlocs.ordered_qtys.EXTEND();
         L_ordlocs.prescaled_qtys.EXTEND();
         L_ordlocs.cancelled_qtys.EXTEND();
         L_ordlocs.cancel_codes.EXTEND();
         L_ordlocs.cancel_dates.EXTEND();
         L_ordlocs.cancel_ids.EXTEND();
         L_ordlocs.unit_costs.EXTEND();
         L_ordlocs.cost_sources.EXTEND();
         L_ordlocs.non_scale_inds.EXTEND();
         L_ordlocs.estimated_instock_date.EXTEND();

         L_ordlocs.items(i) := I_ordlocs.items(i);
         L_ordlocs.locations(i) := I_ordlocs.locations(i);
         L_ordlocs.loc_types(i):= I_ordlocs.loc_types(i);
         L_ordlocs.unit_retails(i):= I_ordlocs.unit_retails(i);
         L_ordlocs.ordered_qtys(i):= I_ordlocs.ordered_qtys(i);
         L_ordlocs.prescaled_qtys(i):= I_ordlocs.prescaled_qtys(i);
         L_ordlocs.unit_costs(i):= I_ordlocs.unit_costs(i);
         L_ordlocs.non_scale_inds(i):= I_ordlocs.non_scale_inds(i);
         L_ordlocs.cancelled_qtys(i):= I_ordlocs.cancelled_qtys(i);
         L_ordlocs.cancel_codes(i):= I_ordlocs.cancel_codes(i);
         L_ordlocs.cancel_dates(i):= I_ordlocs.cancel_dates(i);
         L_ordlocs.cancel_ids(i):= I_ordlocs.cancel_ids(i);
         L_ordlocs.cost_sources(i):= I_ordlocs.cost_sources(i);

         if ORDER_INSTOCK_DATE_SQL.GET_INSTOCK_DT(O_error_message,
                                                  L_ordlocs.estimated_instock_date(i),
                                                  L_ordlocs.order_no,
                                                  L_ordlocs.items(i),
                                                  L_ordlocs.locations(i),
                                                  L_supplier)= FALSE then
            return FALSE;
         end if;

      END LOOP;

   -- bulk insert into ordlocs
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'ORDLOC',
                       'order_no: '||to_char(L_ordlocs.order_no));

      FORALL i IN 1 ..L_ordlocs.items.COUNT

         insert into ordloc (order_no,
                             item,
                             location,
                             loc_type,
                             unit_retail,
                             qty_ordered,
                             qty_prescaled,
                             qty_cancelled,
                             cancel_code,
                             cancel_date,
                             cancel_id,
                             unit_cost,
                             unit_cost_init,
                             cost_source,
                             non_scale_ind,
                             estimated_instock_date)
                     values (L_ordlocs.order_no,
                             L_ordlocs.items(i),
                             L_ordlocs.locations(i),
                             L_ordlocs.loc_types(i),
                             L_ordlocs.unit_retails(i),
                             L_ordlocs.ordered_qtys(i),
                             L_ordlocs.prescaled_qtys(i),
                             L_ordlocs.cancelled_qtys(i),
                             L_ordlocs.cancel_codes(i),
                             L_ordlocs.cancel_dates(i),
                             L_ordlocs.cancel_ids(i),
                             L_ordlocs.unit_costs(i),
                             L_ordlocs.unit_costs(i),
                             L_ordlocs.cost_sources(i),
                             L_ordlocs.non_scale_inds(i),
                             L_ordlocs.estimated_instock_date(i));

   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_ORDLOCS;
----------------------------------------------------------------------------
FUNCTION UPDATE_ORDHEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordhead_row     IN       ORDHEAD%ROWTYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'ORDER_SQL.UPDATE_ORDHEAD';
   L_import_country_id    ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   L_l10n_obj             L10N_OBJ     := L10N_OBJ();

   cursor C_LOCK_ORDHEAD is
      select 'x'
        from ordhead
       where order_no = I_ordhead_row.order_no
         for update nowait;

   cursor C_GET_IMPORT_COUNTRY_ID is
      select import_country_id
        from ordhead
       where order_no = I_ordhead_row.order_no;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_IMPORT_COUNTRY_ID',
                    NULL,
                    'Order_no: '||I_ordhead_row.order_no);
   open C_GET_IMPORT_COUNTRY_ID;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_IMPORT_COUNTRY_ID',
                    NULL,
                    'Order_no: '||I_ordhead_row.order_no);
   fetch C_GET_IMPORT_COUNTRY_ID into L_import_country_id;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_IMPORT_COUNTRY_ID',
                    NULL,
                    'Order_no: '||I_ordhead_row.order_no);
   close C_GET_IMPORT_COUNTRY_ID;

   -- lock ordhead
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ORDHEAD', 'ORDHEAD', 'ORDER_NO: ' || I_ordhead_row.order_no);
   open C_LOCK_ORDHEAD;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ORDHEAD', 'ORDHEAD', 'ORDER_NO: ' || I_ordhead_row.order_no);
   close C_LOCK_ORDHEAD;

   -- perform update
   SQL_LIB.SET_MARK('UPDATE', NULL, 'ORDHEAD', 'order_no: '||to_char(I_ordhead_row.order_no));
   update ordhead
      set dept = NVL(I_ordhead_row.dept, dept),
          buyer = NVL(I_ordhead_row.buyer, buyer),
          supplier = I_ordhead_row.supplier,
          supp_add_seq_no = I_ordhead_row.supp_add_seq_no,
          loc_type = NVL(I_ordhead_row.loc_type,loc_type),                        
          location = NVL(I_ordhead_row.location,location),
          promotion = I_ordhead_row.promotion,
          qc_ind = I_ordhead_row.qc_ind,
          written_date = I_ordhead_row.written_date,
          not_before_date = I_ordhead_row.not_before_date,
          not_after_date = I_ordhead_row.not_after_date,
          otb_eow_date = I_ordhead_row.otb_eow_date,
          earliest_ship_date = I_ordhead_row.earliest_ship_date,
          latest_ship_date = I_ordhead_row.latest_ship_date,
          close_date = I_ordhead_row.close_date,
          terms = I_ordhead_row.terms,
          freight_terms = I_ordhead_row.freight_terms,
          payment_method = I_ordhead_row.payment_method,
          backhaul_type = I_ordhead_row.backhaul_type,
          backhaul_allowance = I_ordhead_row.backhaul_allowance,
          ship_method = I_ordhead_row.ship_method,
          purchase_type = I_ordhead_row.purchase_type,
          status = I_ordhead_row.status,
          orig_approval_date = I_ordhead_row.orig_approval_date,
          orig_approval_id = I_ordhead_row.orig_approval_id,
          ship_pay_method = I_ordhead_row.ship_pay_method,
          fob_trans_res = I_ordhead_row.fob_trans_res,
          fob_trans_res_desc = I_ordhead_row.fob_trans_res_desc,
          fob_title_pass = I_ordhead_row.fob_title_pass,
          fob_title_pass_desc = I_ordhead_row.fob_title_pass_desc,
          edi_sent_ind = I_ordhead_row.edi_sent_ind,
          edi_po_ind = I_ordhead_row.edi_po_ind,
          import_order_ind = I_ordhead_row.import_order_ind,
          import_country_id = I_ordhead_row.import_country_id,
          po_ack_recvd_ind = I_ordhead_row.po_ack_recvd_ind,
          include_on_order_ind = I_ordhead_row.include_on_order_ind,
          vendor_order_no = NVL(I_ordhead_row.vendor_order_no, vendor_order_no),
          exchange_rate = I_ordhead_row.exchange_rate,
          factory = I_ordhead_row.factory,
          agent = I_ordhead_row.agent,
          discharge_port = I_ordhead_row.discharge_port,
          lading_port = I_ordhead_row.lading_port,
          freight_contract_no = I_ordhead_row.freight_contract_no,
          pre_mark_ind = I_ordhead_row.pre_mark_ind,
          currency_code = I_ordhead_row.currency_code,
          reject_code = I_ordhead_row.reject_code,
          contract_no = I_ordhead_row.contract_no,
          last_sent_rev_no = I_ordhead_row.last_sent_rev_no,
          split_ref_ordno = I_ordhead_row.split_ref_ordno,
          pickup_loc = I_ordhead_row.pickup_loc,
          pickup_no = I_ordhead_row.pickup_no,
          pickup_date = I_ordhead_row.pickup_date,
          app_datetime = I_ordhead_row.app_datetime,
          comment_desc = NVL(I_ordhead_row.comment_desc, comment_desc),
          delivery_supplier = I_ordhead_row.delivery_supplier,
          triangulation_ind = I_ordhead_row.triangulation_ind,
          partner_type_1 = I_ordhead_row.partner_type_1,
          partner1 = I_ordhead_row.partner1,
          partner_type_2 = I_ordhead_row.partner_type_2,
          partner2 = I_ordhead_row.partner2,
          partner_type_3 = I_ordhead_row.partner_type_3,
          partner3 = I_ordhead_row.partner3,
          import_id = NVL(I_ordhead_row.import_id,import_id),
          import_type = NVL(I_ordhead_row.import_type,import_type),
          routing_loc_id = I_ordhead_row.routing_loc_id,
          clearing_zone_id = I_ordhead_row.clearing_zone_id,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where order_no = I_ordhead_row.order_no;

   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;

   if L_import_country_id != I_ordhead_row.import_country_id then
      --
      if NOT UPDATE_ORDHEAD_L10N_EXT(O_error_message,
                                     I_ordhead_row) then
      return FALSE;
      end if;
      --
   end if;

   -- Insert ord_tax_breakup
   if I_ordhead_row.status = 'A' then
      L_l10n_obj.procedure_key := 'LOAD_ORDER_TAX_OBJECT';
      L_l10n_obj.doc_type := 'PO';
      L_l10n_obj.doc_id := I_ordhead_row.order_no;

      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_l10n_obj) = FALSE then
         return FALSE;
      end if;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_ORDHEAD;
----------------------------------------------------------------------------
FUNCTION UPDATE_ORDSKUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordskus         IN       ORDER_SQL.ORDSKU_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORDER_SQL.UPDATE_ORDSKUS';

   cursor C_lock_ordskus is
      select 'x'
        from ordsku
       where order_no = I_ordskus.order_no
         and item in (select *
                        from TABLE (CAST(I_ordskus.items AS ITEM_TBL)))
         for update nowait;

BEGIN

   -- lock ordskus for update
   SQL_LIB.SET_MARK('OPEN', 'C_lock_ordskus', 'ORDSKU', 'ORDER_NO: ' || I_ordskus.order_no);
   open C_lock_ordskus;
   SQL_LIB.SET_MARK('CLOSE', 'C_lock_ordskus', 'ORDSKU', 'ORDER_NO: ' || I_ordskus.order_no);
   close C_lock_ordskus;

   -- bulk update ordsku
   SQL_LIB.SET_MARK('UPDATE', NULL, 'ORDSKU', 'order_no: '||to_char(I_ordskus.order_no));
   FORALL i IN 1 ..I_ordskus.items.COUNT
      update ordsku
         set ref_item = I_ordskus.ref_items(i),
             origin_country_id = NVL(I_ordskus.origin_country_ids(i), origin_country_id),
             earliest_ship_date = I_ordskus.earliest_ship_dates(i),
             latest_ship_date = I_ordskus.latest_ship_dates(i),
             supp_pack_size = I_ordskus.supp_pack_sizes(i),
             non_scale_ind = I_ordskus.non_scale_inds(i)
       where order_no = I_ordskus.order_no
         and item = I_ordskus.items(i);

   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;

   if ORDER_SETUP_SQL.UPDATE_SHIP_DATES (O_error_message,
                                         I_ordskus.order_no) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ORDSKUS;
-------------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordlocs         IN       ORDER_SQL.ORDLOC_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORDER_SQL.UPDATE_ORDLOCS';

   -- note: this cursor will lock more rows than needed, but should be ok
   cursor C_lock_ordlocs is
      select 'x'
        from ordloc
       where order_no = I_ordlocs.order_no
         and item in (select *
                        from TABLE (CAST(I_ordlocs.items AS ITEM_TBL)))
         and location in (select *
                            from TABLE (CAST(I_ordlocs.locations AS LOC_TBL)))
         for update nowait;

BEGIN

   -- lock ordlocs for update
   SQL_LIB.SET_MARK('OPEN', 'C_lock_ordlocs', 'ORDLOC', 'ORDER_NO: ' || I_ordlocs.order_no);
   open C_lock_ordlocs;
   SQL_LIB.SET_MARK('CLOSE', 'C_lock_ordlocs', 'ORDLOC', 'ORDER_NO: ' || I_ordlocs.order_no);
   close C_lock_ordlocs;

   -- bulk update ordlocs
   SQL_LIB.SET_MARK('UPDATE', NULL, 'ORDLOC', 'order_no: '||to_char(I_ordlocs.order_no));
   FORALL i IN 1 ..I_ordlocs.items.COUNT
   update ORDLOC
         set loc_type = I_ordlocs.loc_types(i),
             unit_retail = I_ordlocs.unit_retails(i),
             qty_ordered = I_ordlocs.ordered_qtys(i),
             qty_prescaled = I_ordlocs.prescaled_qtys(i),
             qty_cancelled = I_ordlocs.cancelled_qtys(i),
             cancel_code = I_ordlocs.cancel_codes(i),
             cancel_date = I_ordlocs.cancel_dates(i),
             cancel_id = I_ordlocs.cancel_ids(i),
             unit_cost = I_ordlocs.unit_costs(i),
             cost_source = I_ordlocs.cost_sources(i),
             non_scale_ind = I_ordlocs.non_scale_inds(i),
             estimated_instock_date = I_ordlocs.estimated_instock_date(i)
         where order_no = I_ordlocs.order_no
           and item = I_ordlocs.items(i)
           and location = I_ordlocs.locations(i)
           and loc_type = I_ordlocs.loc_types(i);

   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ORDLOCS;
----------------------------------------------------------------------------
FUNCTION DELETE_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'ORDER_SQL.DELETE_ORDER';

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(25);

   cursor C_lock_ordhead_cfa_ext is
      select 'x'
        from ordhead_cfa_ext
       where order_no = I_order_no
         for update nowait;

   cursor C_lock_ordhead is
      select 'x'
        from ordhead
       where order_no = I_order_no
         for update nowait;

   cursor C_lock_order_ext_lock is
      select 'x'
        from ordhead_l10n_ext
       where order_no = I_order_no
         for update nowait;


BEGIN

   if ORDER_SETUP_SQL.LOCK_DETAIL(O_error_message,
                                  I_order_no,
                                  NULL,
                                  NULL,
                                  'ordhead') = FALSE then
      return FALSE;
   end if;

   if ORDER_SETUP_SQL.DELETE_ORDER(O_error_message,
                                   I_order_no,
                                   NULL,
                                   NULL,
                                   NULL,
                                   'ordhead') = FALSE then
      return FALSE;
   end if;

   L_table := 'ordhead_l10n_ext';

   -- lock
   SQL_LIB.SET_MARK('OPEN',
                    'C_lock_order_ext_lock',
                    'ORDHEAD_L10N_EXT',
                    'ORDER_NO: '||to_char(I_order_no));
   open C_LOCK_ORDER_EXT_LOCK;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_lock_order_ext_lock',
                    'ORDHEAD_L10N_EXT',
                    'ORDER_NO: '||to_char(I_order_no));
   close C_lock_order_ext_lock;

   SQL_LIB.SET_MARK('DELETE',
                     NULL,
                     'ORDHEAD_L10N_EXT',
                     'ORDER_NO: ' || TO_CHAR(I_order_no));

   delete from ordhead_l10n_ext
      where order_no = I_order_no;

   L_table := 'ordhead_cfa_ext';

   -- lock ordhead_cfa_ext
   SQL_LIB.SET_MARK('OPEN', 'C_lock_ordhead_cfa_ext', 'ORDHEAD_CFA_EXT', 'ORDER_NO: ' || I_order_no);
   open C_lock_ordhead_cfa_ext;
   SQL_LIB.SET_MARK('CLOSE', 'C_lock_ordhead_cfa_ext', 'ORDHEAD_CFA_EXT', 'ORDER_NO: ' || I_order_no);
   close C_lock_ordhead_cfa_ext;

   -- delete from ordhead_cfa_ext
   SQL_LIB.SET_MARK('DELETE',
                     NULL,
                    'ORDHEAD_CFA_EXT',
                    'ORDER_NO: ' || TO_CHAR(I_order_no));

   delete from ordhead_cfa_ext
    where order_no = I_order_no;

   L_table := 'ordhead';
   -- lock ordhead
   SQL_LIB.SET_MARK('OPEN', 'C_lock_ordhead', 'ORDHEAD', 'ORDER_NO: ' || I_order_no);
   open C_lock_ordhead;
   SQL_LIB.SET_MARK('CLOSE', 'C_lock_ordhead', 'ORDHEAD', 'ORDER_NO: ' || I_order_no);
   close C_lock_ordhead;

   -- delete from ordhead
   SQL_LIB.SET_MARK('DELETE',
                     NULL,
                    'ORDHEAD',
                    'ORDER_NO: ' || TO_CHAR(I_order_no));

   delete from ordhead
    where order_no = I_order_no;

   return TRUE;

EXCEPTION
    when RECORD_LOCKED then
       O_error_message := sql_lib.create_msg('RECORD_LOCKED',
                                             L_table,
                                             I_order_no,
                                             NULL);
       return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ORDER;
----------------------------------------------------------------------------
FUNCTION DELETE_ORDSKUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordskus         IN       ORDER_SQL.ORDSKU_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'ORDER_SQL.DELETE_ORDSKUS';

   cursor C_lock_ordskus is
      select 'x'
        from ordsku
       where order_no = I_ordskus.order_no
         and item in (select *
                        from TABLE (CAST(I_ordskus.items AS ITEM_TBL)))
         for update nowait;

BEGIN

   -- lock ordskus for delete
   SQL_LIB.SET_MARK('OPEN', 'C_lock_ordskus', 'ORDSKU', 'ORDER_NO: ' || I_ordskus.order_no);
   open C_lock_ordskus;
   SQL_LIB.SET_MARK('CLOSE', 'C_lock_ordskus', 'ORDSKU', 'ORDER_NO: ' || I_ordskus.order_no);
   close C_lock_ordskus;

   -- bulk delete from ordsku
   FORALL i IN 1 ..I_ordskus.items.COUNT
      delete from ordsku
       where order_no = I_ordskus.order_no
         and item = I_ordskus.items(i);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ORDSKUS;
----------------------------------------------------------------------------
FUNCTION DELETE_ORDLOCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordlocs         IN       ORDER_SQL.ORDLOC_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'ORDER_SQL.DELETE_ORDLOCS';

   -- note: this cursor will lock more rows than needed, but should be ok
   cursor C_lock_ordlocs is
      select 'x'
        from ordloc
       where order_no = I_ordlocs.order_no
         and item in (select *
                        from TABLE (CAST(I_ordlocs.items AS ITEM_TBL)))
         and location in (select *
                            from TABLE (CAST(I_ordlocs.locations AS LOC_TBL)))
         for update nowait;

   cursor C_lock_ordskus is
      select 'x'
        from ordsku os
       where os.order_no = I_ordlocs.order_no
         and not exists (select 'x'
                           from ordloc ol
                          where ol.order_no = I_ordlocs.order_no
                            and os.item = ol.item
                            and rownum = 1)
         for update nowait;

   cursor C_lock_ord_tax_breakup is
      select 'x'
        from ord_tax_breakup os
       where os.order_no = I_ordlocs.order_no
         and not exists (select 'x'
                           from ordloc ol
                          where ol.order_no = I_ordlocs.order_no
                            and os.item = ol.item
                            and rownum = 1)
         for update nowait;

BEGIN

   -- lock ordlocs for delete
   SQL_LIB.SET_MARK('OPEN', 'C_lock_ordlocs', 'ORDLOC', 'ORDER_NO: ' || I_ordlocs.order_no);
   open C_lock_ordlocs;
   SQL_LIB.SET_MARK('CLOSE', 'C_lock_ordlocs', 'ORDLOC', 'ORDER_NO: ' || I_ordlocs.order_no);
   close C_lock_ordlocs;

   -- bulk delete from ordloc
   FORALL i IN 1 ..I_ordlocs.items.COUNT
      delete from ordloc
       where order_no = I_ordlocs.order_no
         and item = I_ordlocs.items(i)
         and location = I_ordlocs.locations(i);

   -- lock ordskus for delete
   SQL_LIB.SET_MARK('OPEN', 'C_lock_ordskus', 'ORDSKU', 'ORDER_NO: ' || I_ordlocs.order_no);
   open C_lock_ordskus;
   SQL_LIB.SET_MARK('CLOSE', 'C_lock_ordskus', 'ORDSKU', 'ORDER_NO: ' || I_ordlocs.order_no);
   close C_lock_ordskus;

   -- also delete from ordsku if all locations are deleted
   delete from ordsku os
    where os.order_no = I_ordlocs.order_no
      and not exists (select 'x'
                        from ordloc ol
                       where ol.order_no = I_ordlocs.order_no
                         and os.item = ol.item
                         and rownum = 1);

   -- lock ordlocs for delete
   SQL_LIB.SET_MARK('OPEN', 'C_lock_ord_tax_breakup', 'ORD_TAX_BREAKUP', 'ORDER_NO: ' || I_ordlocs.order_no);
   open C_lock_ord_tax_breakup;
   SQL_LIB.SET_MARK('CLOSE', 'C_lock_ord_tax_breakup', 'ORD_TAX_BREAKUP', 'ORDER_NO: ' || I_ordlocs.order_no);
   close C_lock_ord_tax_breakup;

   -- bulk delete from ordloc
   FORALL i IN 1 ..I_ordlocs.items.COUNT
      delete from ord_tax_breakup
       where order_no = I_ordlocs.order_no
         and item = I_ordlocs.items(i)
         and location = I_ordlocs.locations(i);


   return TRUE;



EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ORDLOCS;
-------------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_IMP_EXP (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_import_id       IN OUT   ORDHEAD.IMPORT_ID%TYPE,
                              O_import_type     IN OUT   ORDHEAD.IMPORT_TYPE%TYPE,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE) RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'ORDER_SQL.GET_DEFAULT_IMP_EXP';

   cursor C_IMPORT_ID is
   SELECT import_id,
          import_type
     FROM ordhead
    WHERE order_no = NVL(I_order_no,-999);

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_IMPORD_ID',
                    NULL,
                    'Order_no: '||I_order_no);
   open C_IMPORT_ID;
   ---
   SQL_LIB.SET_MARK('FETCH','C_IMPORD_ID',NULL,'Order_no: '||I_order_no);
   fetch C_IMPORT_ID into O_import_id,
                          O_import_type;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_IMPORD_ID',NULL,'Order_no: '||I_order_no);
   close C_IMPORT_ID;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEFAULT_IMP_EXP;
-------------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_ROUT_LOC (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_routing_loc_id   IN OUT   ORDHEAD.ROUTING_LOC_ID%TYPE,
                               I_order_no         IN       ORDHEAD.ORDER_NO%TYPE) RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'ORDER_SQL.GET_DEFAULT_ROUT_LOC';

   cursor C_ROUT_LOC is
   SELECT routing_loc_id
     FROM ordhead
    WHERE order_no = NVL(I_order_no,-999);

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_ROUT_LOC',
                    NULL,
                    'Order_no: '||I_order_no);
   open C_ROUT_LOC;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_ROUT_LOC',
                    NULL,
                    'Order_no: '||I_order_no);
   fetch C_ROUT_LOC into O_routing_loc_id;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ROUT_LOC',
                    NULL,
                    'Order_no: '||I_order_no);
   close C_ROUT_LOC;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEFAULT_ROUT_LOC;
-------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDHEAD_L10N_EXT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_ordhead_row     IN       ORDHEAD%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORDER_SQL.UPDATE_ORDHEAD_L10N_EXT';
   L_10n_obj      L10N_OBJ := L10N_OBJ();

   cursor c_lock_ordhead_l10n_ext is
      select 'x'
        from ordhead_l10n_ext
       where order_no = I_ordhead_row.order_no
         for update nowait;

BEGIN

   --- lock ordhead_l10n_ext
   SQL_LIB.SET_MARK('OPEN',
                    'c_lock_ordhead_l10n_ext',
                    'ORDHEAD_L10N_EXT',
                    'ORDER_NO: ' || I_ordhead_row.order_no);
   open c_lock_ordhead_l10n_ext;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'c_lock_ordhead_l10n_ext',
                    'ORDHEAD_L10N_EXT',
                    'ORDER_NO: ' || I_ordhead_row.order_no);
   close c_lock_ordhead_l10n_ext;

   -- delete ordhead_l10n_ext
   SQL_LIB.SET_MARK('DELETE',
                     NULL,
                     'ORDHEAD_L10N_EXT',
                     'ORDER_NO: '||to_char(I_ordhead_row.order_no));

   delete from ordhead_l10n_ext
      where order_no = I_ordhead_row.order_no;

   --- get new utilization code for the updated import_country_id.
   L_10n_obj.procedure_key := 'CREATE_TRAN_UTIL_CODE';
   L_10n_obj.doc_type := 'PO';
   L_10n_obj.doc_id := I_ordhead_row.order_no;
   L_10n_obj.country_id := I_ordhead_row.import_country_id;

   --- Make a call to the l10n function to get default util code.
   if L10N_SQL.EXEC_FUNCTION (O_error_message,
                              L_10n_obj) = FALSE then
         return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ORDHEAD_L10N_EXT;
-------------------------------------------------------------------------------------------------
FUNCTION DEL_ORDHEAD_ATTRIB(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no         IN       ORDHEAD_L10N_EXT.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'ORDER_SQL.DEL_ORDHEAD_ATTRIB';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_lock_ordhead_l10n_ext is
      select 'x'
        from ordhead_l10n_ext
       where order_no = I_order_no
         for update nowait;

BEGIN

   L_table := 'ORDHEAD_L10N_EXT';
   SQL_LIB.SET_MARK('OPEN',
                    'C_lock_ordhead_l10n_ext',
                    L_table,
                    'order_no: '||I_order_no);

   open  C_lock_ordhead_l10n_ext;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_lock_ordhead_l10n_ext',
                    L_table,
                    'order_no: '||I_order_no);

   close C_lock_ordhead_l10n_ext;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'order_no: '||I_order_no);

   delete from ordhead_l10n_ext
         where order_no = I_order_no;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'order_no: '||I_order_no,
                                             to_char(SQLCODE));
   return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;

END DEL_ORDHEAD_ATTRIB;
---------------------------------------------------------------------------------------

END ORDER_SQL;
/