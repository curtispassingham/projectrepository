CREATE OR REPLACE PACKAGE FUTURE_COST_EVENT_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------------------
-- Package Variables
-------------------------------------------------------------------------------------------------------
-- Actions
ADD_EVENT                      CONSTANT  VARCHAR2(3) := 'ADD';
REMOVE_EVENT                   CONSTANT  VARCHAR2(3) := 'REM';
MODIFY_EVENT                   CONSTANT  VARCHAR2(3) := 'MOD';
 
-- Cost Event Types
SUPP_COUNTRY_COST_EVENT_TYPE   CONSTANT  VARCHAR2(3) := 'SC';
NEW_ITEM_LOC_COST_EVENT_TYPE   CONSTANT  VARCHAR2(3) := 'NIL';
RETAIL_CHANGE_COST_EVENT_TYPE  CONSTANT  VARCHAR2(3) := 'RTC';
PRIMARY_PACK_COST_EVENT_TYPE   CONSTANT  VARCHAR2(3) := 'PP';
COST_CHANGE_COST_EVENT_TYPE    CONSTANT  VARCHAR2(3) := 'CC';
RECLASS_COST_EVENT_TYPE        CONSTANT  VARCHAR2(3) := 'R';
DEAL_COST_EVENT_TYPE           CONSTANT  VARCHAR2(3) := 'D';
MERCH_HIER_COST_EVENT_TYPE     CONSTANT  VARCHAR2(3) := 'MH';
ORG_HIER_COST_EVENT_TYPE       CONSTANT  VARCHAR2(3) := 'OH';
COST_ZONE_COST_EVENT_TYPE      CONSTANT  VARCHAR2(3) := 'CZ';
ELC_COST_EVENT_TYPE            CONSTANT  VARCHAR2(3) := 'ELC';
SUPP_HIER_COST_EVENT_TYPE      CONSTANT  VARCHAR2(3) := 'SH';
ITEM_COST_ZONE_GRP_EVENT_TYPE  CONSTANT  VARCHAR2(3) := 'ICZ';
TEMPLATE_COST_EVENT_TYPE       CONSTANT  VARCHAR2(3) := 'T';
TEMPLATE_RELN_COST_EVENT_TYPE  CONSTANT  VARCHAR2(3) := 'TR';
DEAL_PASSTHRU_COST_EVENT_TYPE  CONSTANT  VARCHAR2(3) := 'DP';
FRAN_COST_LOC_CHG_EVENT_TYPE   CONSTANT  VARCHAR2(3) := 'CL';

-- Run Types
ASYNC_COST_EVENT_RUN_TYPE      CONSTANT  VARCHAR2(5) := 'ASYNC';
BATCH_COST_EVENT_RUN_TYPE      CONSTANT  VARCHAR2(5) := 'BATCH';
SYNC_COST_EVENT_RUN_TYPE       CONSTANT  VARCHAR2(5) := 'SYNC';
LP_OVERRIDE_RUN_TYPE           VARCHAR2(5) := NULL;
 -- The following Run type will only be used if an event is comes with source
 -- temporary table indicator set to 'Y'es. OVRID run type will run like 'SYNC'
 -- regardless of the cost event run type. Right now this feature is only used
 -- by Cost Change event and New Item Loc for Franchise stores.
OVRID_COST_EVENT_RUN_TYPE      CONSTANT  VARCHAR2(5) := 'OVRID';

-- Cost Event Result Status Codes
NEW_COST_EVENT                 CONSTANT  VARCHAR2(1) := 'N';
ERROR_COST_EVENT               CONSTANT  VARCHAR2(1) := 'E';
REPROCESSING_COST_EVENT        CONSTANT  VARCHAR2(1) := 'R';
COMPLETE_COST_EVENT            CONSTANT  VARCHAR2(1) := 'C';

-- Return Codes
SUCCESS                        CONSTANT  NUMBER      := 0; -- Success
FAILURE                        CONSTANT  NUMBER      := 1; -- Failure

-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_NIL
-- Purpose      :  To add the cost event details for new item location.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_NIL(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_cost_event_process_id        OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                 I_item_locs                 IN     OBJ_ITEMLOC_TBL,
                 I_user                      IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user,
                 I_override_event_run_type   IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  CAPTURE_RETAIL_CHANGES
-- Purpose      :  This function will run in nightly batch after price change batch and before 
--                 date-roll. It will capture retail changes that have come for item-location combinations
--                 that are on percent off retail type of franchise cost template.
--------------------------------------------------------------------------------------------------------
FUNCTION CAPTURE_RETAIL_CHANGES(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_RETAIL_CHANGE
-- Purpose      :  To add the cost event details for retail changes.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_RETAIL_CHANGE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_item_locs              IN     OBJ_ITEM_LOC_RETAIL_TBL,
                           I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_COST_CHANGE_EVENT
-- Purpose      :  To add the cost event details for cost change.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_COST_CHANGE_EVENT(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_action                 IN     COST_EVENT.ACTION%TYPE,
                               I_cost_changes           IN     OBJ_CC_COST_EVENT_TBL,
                               I_persist_ind            IN     COST_EVENT.PERSIST_IND%TYPE,
                               I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user,
                               I_over_run_type          IN     COST_EVENT.OVERRIDE_RUN_TYPE%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_RECLASS_EVENT
-- Purpose      :  To add the cost event details for reclassification.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_RECLASS_EVENT(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_cost_event_process_id      OUT   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_action                  IN       COST_EVENT.ACTION%TYPE,
                           I_reclass_nos             IN       OBJ_RCLS_COST_EVENT_TBL,
                           I_user                    IN       COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_DEALS
-- Purpose      :  To add the cost event details for deals.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_DEALS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                   I_action                 IN     COST_EVENT.ACTION%TYPE,
                   I_deal_ids               IN     OBJ_NUMERIC_ID_TABLE,
                   I_persist_ind            IN     COST_EVENT.PERSIST_IND%TYPE,
                   I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_ORG_HIER_CHANGE
-- Purpose      :  To add the cost event details for organization hierarchy association change.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_ORG_HIER_CHANGE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_old_chain              IN     CHAIN.CHAIN%TYPE,
                             I_old_area               IN     AREA.AREA%TYPE,
                             I_old_region             IN     REGION.REGION%TYPE,
                             I_old_district           IN     DISTRICT.DISTRICT%TYPE,
                             I_new_chain              IN     CHAIN.CHAIN%TYPE,
                             I_new_area               IN     AREA.AREA%TYPE,
                             I_new_region             IN     REGION.REGION%TYPE,
                             I_new_district           IN     DISTRICT.DISTRICT%TYPE,
                             I_store                  IN     STORE.STORE%TYPE,
                             I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_ORG_HIER_CHANGE_WH
-- Purpose      :  To add the cost event details for organization hierarchy association change for a warehouse.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_ORG_HIER_CHANGE_WH(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                I_old_type               IN     WH.ORG_HIER_TYPE%TYPE,
                                I_old_value              IN     WH.ORG_HIER_VALUE%TYPE,
                                I_new_type               IN     WH.ORG_HIER_TYPE%TYPE,
                                I_new_value              IN     WH.ORG_HIER_VALUE%TYPE,
                                I_wh                     IN     WH.WH%TYPE,
                                I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_MERCH_HIER_CHANGE
-- Purpose      :  To add the cost event details for merchandise hierarchy association change.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_MERCH_HIER_CHANGE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_old_division           IN     DIVISION.DIVISION%TYPE,
                               I_old_group_no           IN     GROUPS.GROUP_NO%TYPE,
                               I_new_division           IN     DIVISION.DIVISION%TYPE,
                               I_new_group_no           IN     GROUPS.GROUP_NO%TYPE,
                               I_dept                   IN     DEPS.DEPT%TYPE,
                               I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_COST_ZONE_LOC_MOVE
-- Purpose      :  To add the cost event details for location cost zone change.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_COST_ZONE_LOC_MOVE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                I_location               IN     ITEM_LOC.LOC%TYPE,
                                I_zone_group_id          IN     COST_ZONE.ZONE_GROUP_ID%TYPE,
                                I_old_zone_id            IN     COST_ZONE.ZONE_ID%TYPE,
                                I_new_zone_id            IN     COST_ZONE.ZONE_ID%TYPE,
                                I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_ELC_EVENTS
-- Purpose      :  To add the cost event details for item expenses inserted/updated/deleted.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_ELC_EVENTS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_elc_changes            IN     OBJ_ELC_COST_EVENT_TBL,
                        I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user,
                        I_over_run_type          IN     COST_EVENT.OVERRIDE_RUN_TYPE%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_SUPP_HIER_CHANGE
-- Purpose      :  To add a cost event details for supplier hierarchy level change.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_SUPP_HIER_CHANGE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_supp_hier_chgs         IN     OBJ_ISCL_SUPP_HIER_CHG_TBL,
                              I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_SUPP_HIER_CHANGE_WRP
-- Purpose      :  To add a cost event details for supplier hierarchy level change.
--                 A wrapper for ADD_SUPP_HIER_CHANGE to mask boolean return type.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_SUPP_HIER_CHANGE_WRP(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                  I_supp_hier_chgs         IN     OBJ_ISCL_SUPP_HIER_CHG_TBL,
                                  I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN INTEGER;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_ITEM_COST_ZONE_GRP_CHG
-- Purpose      :  To add the cost event details for cost zone group change.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_ITEM_COST_ZONE_GRP_CHG(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                    I_items                  IN     OBJ_VARCHAR_ID_TABLE,
                                    I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_SUPP_COUNTRY
-- Purpose      :  To add the cost event details for supplier/country add/delete.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_SUPP_COUNTRY(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_action                 IN     COST_EVENT.ACTION%TYPE,
                          I_sc_detail_rec          IN     OBJ_SC_COST_EVENT_TBL,
                          I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_SUPP_COUNTRY_WRP
-- Purpose      :  To add the cost event details for supplier/country add/delete.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_SUPP_COUNTRY_WRP(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_action                 IN     COST_EVENT.ACTION%TYPE,
                              I_sc_detail_rec          IN     OBJ_SC_COST_EVENT_TBL,
                              I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN INTEGER;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_PRIMARY_PACK_COST_CHG
-- Purpose      :  To add the cost event details for item/loc primary cost pack update.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_PRIMARY_PACK_COST_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_cost_event_process_id    OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                   I_pp_detail_rec         IN     OBJ_PP_COST_EVENT_TBL,
                                   I_user                  IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_PRIMARY_PACK_COST_CHG
-- Purpose      :  Wrapper function for ADD_PRIMARY_PACK_COST_CHG to assist in call from ADF.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_PRIMARY_PACK_COST_CHG_WRP(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_cost_event_process_id    OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                       I_pp_detail_rec         IN     OBJ_PP_COST_EVENT_TBL,
                                       I_user                  IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN NUMBER;
--------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_COST_TMPL_CHG
-- Purpose      :  To add the cost event details for Cost Template updates.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_COST_TMPL_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_cost_event_process_id    OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_templ_ids             IN     OBJ_NUMERIC_ID_TABLE,
                           I_user                  IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_COST_TMPL_RELATIONSHIP_CHG
-- Purpose      :  To add the cost event details for Cost Template relationship updates.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_COST_TMPL_RELATIONSHIP_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_cost_event_process_id    OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                        I_templ_relns           IN     OBJ_TEMPL_RELN_EVENT_TBL,
                                        I_action                IN     COST_EVENT.ACTION%TYPE,
                                        I_user                  IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_DEAL_PASSTHRU_CHG
-- Purpose      :  To add the cost event details for deal pass-through updates.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_DEAL_PASSTHRU_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cost_event_process_id    OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_deal_pt_rec           IN     OBJ_DEAL_PASSTHRU_EVENT_TBL,
                               I_action                IN     COST_EVENT.ACTION%TYPE,
                               I_user                  IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  HANDLE_COST_EVENT
-- Purpose      :  This function is called by the add cost event functions.
--                 To process/schedule a cost event based on the cost_event_run_type_config run type.
--------------------------------------------------------------------------------------------------------
FUNCTION HANDLE_COST_EVENT(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_event_type              IN     COST_EVENT.EVENT_TYPE%TYPE,
                           I_override_event_run_type IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name:  REPROCESS_COST_EVENT
-- Purpose      :  This function is called to reprocess a failed cost event.
--                 Only asynchronous jobs can be reprocessed.
--------------------------------------------------------------------------------------------------------
FUNCTION REPROCESS_COST_EVENT(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id  IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id              IN     COST_EVENT_THREAD.THREAD_ID%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-- Function Name:  MOD_FRAN_COSTING_LOC
-- Purpose      :  To change the costing location of the franchise store
--------------------------------------------------------------------------------------------------------
FUNCTION MOD_FRAN_COSTING_LOC (O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cost_event_process_id        OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_item                      IN     ITEM_LOC.ITEM%TYPE,
                               I_fran_loc                  IN     ITEM_LOC.LOC%TYPE,
                               I_costing_loc               IN     ITEM_LOC.COSTING_LOC%TYPE,
                               I_cost_loc_change_date      IN     FUTURE_COST.ACTIVE_DATE%TYPE,
                               I_user                      IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  MOD_FRAN_COSTING_LOC_CH
-- Purpose      :  To change the costing location of the franchise store for child items
--------------------------------------------------------------------------------------------------------
FUNCTION MOD_FRAN_COSTING_LOC_CH(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_cost_event_process_id        OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                 I_item                      IN     ITEM_LOC.ITEM%TYPE,
                                 I_fran_loc                  IN     ITEM_LOC.LOC%TYPE,
                                 I_costing_loc               IN     ITEM_LOC.COSTING_LOC%TYPE,
                                 I_cost_loc_change_date      IN     FUTURE_COST.ACTIVE_DATE%TYPE,
                                 I_user                      IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_DEALS_WRP
-- Purpose      :  This function is a wrapper for ADD_DEALS to allow for wrapper generation through jPublisher.
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_DEALS_WRP(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_cost_event_process_id   IN OUT   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_action                  IN       COST_EVENT.ACTION%TYPE,
                       I_deal_ids                IN       OBJ_NUMERIC_ID_TABLE,
                       I_persist_ind             IN       COST_EVENT.PERSIST_IND%TYPE,
                       I_user                    IN       COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN INTEGER;
--------------------------------------------------------------------------------------------------------
-- Function Name:  ADD_COST_CHANGE_EVENT
-- Purpose      :  This function is an overloaded version of ADD_COST_CHANGE_EVENT, which would accept a
--                 single cost change instead of a table type;
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_COST_CHANGE_EVENT(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cost_event_process_id   IN OUT   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_action                  IN       COST_EVENT.ACTION%TYPE,
                               I_cost_change             IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                               I_src_tmp_ind             IN       VARCHAR2,
                               I_persist_ind             IN       COST_EVENT.PERSIST_IND%TYPE,
                               I_user                    IN       COST_EVENT.USER_ID%TYPE DEFAULT get_user,
                               I_over_run_type           IN       COST_EVENT.OVERRIDE_RUN_TYPE%TYPE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------                               
END FUTURE_COST_EVENT_SQL;
/
