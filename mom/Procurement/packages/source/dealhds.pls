
CREATE OR REPLACE PACKAGE DEAL_HEAD_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------------------------------
-- Function Name:  GET_ATTRIB
-- Purpose:        This function returns all the attributes of the deal_head table
----------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists          IN OUT   BOOLEAN,
                    O_deal_head_rec   IN OUT   DEAL_HEAD%ROWTYPE,
                    I_deal_id         IN       DEAL_HEAD.DEAL_ID%TYPE)
   RETURN BOOLEAN;

----------------------------------------------------------------------------------------
-- Function Name:  LOCK_DEAL_HEAD
-- Purpose:        This function locks the deal_head table
----------------------------------------------------------------------------------------
FUNCTION LOCK_DEAL_HEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_deal_id         IN       DEAL_HEAD.DEAL_ID%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
END DEAL_HEAD_SQL;
/