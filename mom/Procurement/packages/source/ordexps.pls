
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORDER_EXPENSE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name:  GET_NEXT_SEQ
--Purpose      :  Retrieves the next sequence number. 
-------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ(O_error_message IN OUT VARCHAR2,
                      O_seq_no        IN OUT ORDLOC_EXP.SEQ_NO%TYPE,
                      I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
         RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  EXP_EXIST
--Purpose      :  Checks if a passed in order item expense record
--                exists on the ORDLOC_exp table.
-------------------------------------------------------------------------------
FUNCTION EXP_EXIST(O_error_message IN OUT VARCHAR2,
                   O_exists        IN OUT BOOLEAN,
                   I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                   I_item          IN     ITEM_MASTER.ITEM%TYPE,
                   I_location      IN     ORDLOC.LOCATION%TYPE,
                   I_comp_id       IN     ELC_COMP.COMP_ID%TYPE,
                   I_pack_item     IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  DEL_EXP
--Purpose      :  Deletes the order item expense record passed in.
-------------------------------------------------------------------------------
FUNCTION DEL_EXP(O_error_message  IN OUT VARCHAR2,
                 I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                 I_seq_no         IN     ORDLOC_EXP.SEQ_NO%TYPE)
         RETURN BOOLEAN; 
--------------------------------------------------------------------------------------------- 
-- Function Name: INSERT_COST_COMP
-- Purpose      : This overloaded function allows the function to be called
--                without specifying the I_backhaul_only_ind.  If this function
--                is called it will call the other INSERT_COST_COMP function
--                passing 'N' in the I_backhaul_only_ind parameter.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_COST_COMP(O_error_message  IN OUT VARCHAR2,
                          I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                          I_item           IN     ITEM_MASTER.ITEM%TYPE,
                          I_component_item IN     ITEM_MASTER.ITEM%TYPE,
                          I_location       IN     ORDLOC.LOCATION%TYPE,
                          I_loc_type       IN     ORDLOC.LOC_TYPE%TYPE) 
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------- 
-- Function Name: INSERT_COST_COMP
-- Purpose      : This function calls the DEFAULT_EXPENSES (if I_backhaul_only_ind
--                is passed in as 'N') function to insert estimated
--                landed cost component records into the ORDLOC_exp table given 
--                the order number, item number and location number.  If location
--                is NULL, estimated landed cost component records will be created for 
--                all locations.  If both I_item and I_location are null then
--                expenses will default for all item/locs on the order.  If 
--                I_backhaul_only_ind is passed in as 'Y' then only the zone level
--                expenses with expense categories of 'B' will be defaulted.
--                Added I_import_ord_ind parameter to put a check to access ordsku_hts_assess
--                ordsku_hts table for Import Order only.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_COST_COMP(O_error_message     IN OUT VARCHAR2,
                          I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_component_item    IN     ITEM_MASTER.ITEM%TYPE,
                          I_location          IN     ORDLOC.LOCATION%TYPE,
                          I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                          I_backhaul_only_ind IN     VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION INSERT_COST_COMP(O_error_message     IN OUT VARCHAR2,
                          I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                          I_component_item    IN     ITEM_MASTER.ITEM%TYPE,
                          I_location          IN     ORDLOC.LOCATION%TYPE,
                          I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                          I_backhaul_only_ind IN     VARCHAR2,
                          I_import_ord_ind    IN     ORDHEAD.IMPORT_ORDER_IND%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DEFAULT_EXPENSES
-- Purpose      : Inserts expenses found on the Item Expenses tables into Order Item 
--                Expenses table for a given order number, item number, and location number.  
---------------------------------------------------------------------------------------------
FUNCTION DEFAULT_EXPENSES(O_error_message     IN OUT VARCHAR2,
                          I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                          I_supplier          IN     SUPS.SUPPLIER%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_component_item    IN     ITEM_MASTER.ITEM%TYPE,
                          I_location          IN     ORDLOC.LOCATION%TYPE,
                          I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                          I_exchange_type     IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                          I_zone_group_id     IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PACKITEM_EXPENSES
-- Purpose      : Internal function, only called by DEFAULT_EXPENSES.
--                Inserts expenses found on the Item Expenses tables into Order Item 
--                Expenses table for a given order number, packitem number, and location number.  
--                If both the component item and item fields are passed in, then only one record
--                will be inserted.  If the component item is NULL and records have not been 
--                inserted for the existing pack item, then all the items on the pack will be 
--                inserted.
---------------------------------------------------------------------------------------------
FUNCTION PACKITEM_EXPENSES(O_error_message     IN OUT VARCHAR2,
                           I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                           I_supplier          IN     SUPS.SUPPLIER%TYPE,
                           I_item              IN     ITEM_MASTER.ITEM%TYPE,
                           I_component_item    IN     ITEM_MASTER.ITEM%TYPE,
                           I_seq_no            IN     ORDLOC_EXP.SEQ_NO%TYPE,
                           I_location          IN     ORDLOC.LOCATION%TYPE,
                           I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                           I_exchange_type     IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DEFAULT_BACK_EXPENSES
-- Purpose      : Inserts Backhaul expenses found on the Item Expenses tables into Order Item 
--                Expenses table for a given order number, item number, and location number.  
---------------------------------------------------------------------------------------------
FUNCTION DEFAULT_BACK_EXPENSES(O_error_message     IN OUT VARCHAR2,
                               I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                               I_supplier          IN     SUPS.SUPPLIER%TYPE,
                               I_item              IN     ITEM_MASTER.ITEM%TYPE,
                               I_component_item    IN     ITEM_MASTER.ITEM%TYPE,
                               I_location          IN     ORDLOC.LOCATION%TYPE,
                               I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                               I_exchange_type     IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                               I_zone_group_id     IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PACKITEM_BACK_EXPENSES
-- Purpose      : Internal function, only called by DEFAULT_BACK_EXPENSES.
--                Inserts Backhaul expenses found on the Item Expenses tables into Order Item 
--                Expenses table for a given order number, packitem number, and location number.  
--                If both the component item and item fields are passed in, then only one record
--                will be inserted.  If the component item is NULL and records have not been 
--                inserted for the existing pack item, then all the items on the pack will be 
--                inserted.
---------------------------------------------------------------------------------------------
FUNCTION PACKITEM_BACK_EXPENSES(O_error_message     IN OUT VARCHAR2,
                                I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                I_supplier          IN     SUPS.SUPPLIER%TYPE,
                                I_item              IN     ITEM_MASTER.ITEM%TYPE,
                                I_component_item    IN     ITEM_MASTER.ITEM%TYPE,
                                I_seq_no            IN     ORDLOC_EXP.SEQ_NO%TYPE,
                                I_location          IN     ORDLOC.LOCATION%TYPE,
                                I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                                I_exchange_type     IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: INSERT_ZONE_COMPS
-- Purpose      : Internal function called by DEFAULT_EXPENSES/PACKITEM_EXPENSES.  This function
--                performs the actual inserts into the ORDLOC_EXP table that
--                have corresponding zone level expense records on the Item Expense 
--                Header table.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_ZONE_COMPS(O_error_message    IN OUT VARCHAR2,
                           O_records_inserted IN OUT VARCHAR2,
                           I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                           I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                           I_item             IN     ITEM_MASTER.ITEM%TYPE,
                           I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                           I_supplier         IN     SUPS.SUPPLIER%TYPE,
                           I_location         IN     ORDLOC.LOCATION%TYPE,
                           I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                           I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                           I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                           I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: INSERT_BASE_ZONE_COMPS
-- Purpose      : Internal function called by DEFAULT_EXPENSES/PACKITEM_EXPENSES.  
--                This function performs the actual inserts of 'Base' expenses 
--                (i.e. when a corresponding header record does not exist on 
--                the Item Expense Header table we insert the expenses for 
--                the Item/Supplier's Base) into the ORDLOC_EXP table.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_BASE_ZONE_COMPS(O_error_message    IN OUT VARCHAR2,
                                O_records_inserted IN OUT VARCHAR2,
                                I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                                I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                                I_item             IN     ITEM_MASTER.ITEM%TYPE,
                                I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                                I_supplier         IN     SUPS.SUPPLIER%TYPE,
                                I_location         IN     ORDLOC.LOCATION%TYPE,
                                I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                                I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                                I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                                I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: INSERT_CTRY_COMPS
-- Purpose      : Internal function called by DEFAULT_EXPENSES/PACKITEM_EXPENSES.  
--                This function performs the actual inserts into the ORDLOC_EXP table that
--                have corresponding country level expense records on the Item Expense 
--                Header table.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_CTRY_COMPS(O_error_message    IN OUT VARCHAR2,
                           O_records_inserted IN OUT VARCHAR2,
                           I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                           I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                           I_item             IN     ITEM_MASTER.ITEM%TYPE,
                           I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                           I_supplier         IN     SUPS.SUPPLIER%TYPE,
                           I_location         IN     ORDLOC.LOCATION%TYPE,
                           I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                           I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                           I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                           I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: INSERT_BASE_CTRY_COMPS
-- Purpose      : Internal function called by DEFAULT_EXPENSES/PACKITEM_EXPENSES.  
--                This functionperforms the actual inserts of 'Base' expenses (i.e. when a 
--                corresponding header record does not exist on the Item Expense 
--                Header table we insert the expenses for the Item/Supplier/Origin Country's Base) 
--                into the ORDLOC_EXP table.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_BASE_CTRY_COMPS(O_error_message    IN OUT VARCHAR2,
                                O_records_inserted IN OUT VARCHAR2,
                                I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                                I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                                I_item             IN     ITEM_MASTER.ITEM%TYPE,
                                I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                                I_supplier         IN     SUPS.SUPPLIER%TYPE,
                                I_location         IN     ORDLOC.LOCATION%TYPE,
                                I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                                I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                                I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                                I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: INSERT_PTNR_COMPS
-- Purpose      : Internal function called by DEFAULT_EXPENSES/PACKITEM_EXPENSES.
--                This function performs the actual inserts into the ORDLOC_EXP table that
--                have corresponding records on the Expense Profile Header table.
--                for the Order's Factory or Agent
---------------------------------------------------------------------------------------------
FUNCTION INSERT_PTNR_COMPS(O_error_message    IN OUT VARCHAR2,
                           O_records_inserted IN OUT VARCHAR2,
                           I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                           I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                           I_item             IN     ITEM_MASTER.ITEM%TYPE,
                           I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                           I_supplier         IN     SUPS.SUPPLIER%TYPE,
                           I_location         IN     ORDLOC.LOCATION%TYPE,
                           I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                           I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                           I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                           I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                           I_partner_type     IN     PARTNER.PARTNER_TYPE%TYPE,
                           I_partner          IN     VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: INSERT_BASE_PTNR_COMPS
-- Purpose      : Internal function called by DEFAULT_EXPENSES/PACKITEM_EXPENSES.  
--                This functionperforms the actual inserts of 'Base' expense profiles 
--                (i.e. when a corresponding header record does not exist on the 
--                Expense Profile Header table we insert the expenses for the 
--                Factory or Agent's Base) 
--                into the ORDLOC_EXP table.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_BASE_PTNR_COMPS(O_error_message    IN OUT VARCHAR2,
                                O_records_inserted IN OUT VARCHAR2,
                                I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                                I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                                I_item             IN     ITEM_MASTER.ITEM%TYPE,
                                I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                                I_supplier         IN     SUPS.SUPPLIER%TYPE,
                                I_location         IN     ORDLOC.LOCATION%TYPE,
                                I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                                I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                                I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                                I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                                I_partner_type     IN     PARTNER.PARTNER_TYPE%TYPE,
                                I_partner          IN     VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: INSERT_TEXP
-- Purpose      : Internal function called by DEFAULT_EXPENSES/PACKITEM_EXPENSES.  
--                This function performs the actual inserts of the Total Expense 
--                component (i.e. 'TEXP') into the ORDLOC_EXP table.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_TEXP(O_error_message    IN OUT VARCHAR2,
                     O_records_inserted IN OUT VARCHAR2,
                     I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                     I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                     I_item             IN     ITEM_MASTER.ITEM%TYPE,
                     I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                     I_supplier         IN     SUPS.SUPPLIER%TYPE,
                     I_location         IN     ORDLOC.LOCATION%TYPE,
                     I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                     I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                     I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                     I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  DEL_EXP
--Purpose      :  Deletes the Backhaul Expense records for the passed in order.
-------------------------------------------------------------------------------
FUNCTION DELETE_BACKHAUL_EXP(O_error_message  IN OUT VARCHAR2,
                             I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:  GET_UOM_RATE_VALUE
--Purpose      :  Retrieves the per count uom, component rate, and estimated
--                expense value from the ORDLOC_EXP table.  If a currency is
--                passed in then converts rate and estimated expense value to
--                the passed in currency.
--------------------------------------------------------------------------------------
FUNCTION GET_UOM_RATE_VALUE(O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            O_per_count_uom  IN OUT UOM_CLASS.UOM%TYPE,
                            O_comp_rate      IN OUT ELC_COMP.COMP_RATE%TYPE,        
                            O_est_exp_value  IN OUT ORDLOC_EXP.EST_EXP_VALUE%TYPE,
                            I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE,
                            I_pack_item      IN     ITEM_MASTER.ITEM%TYPE,
                            I_comp_id        IN     ELC_COMP.COMP_ID%TYPE,
                            I_currency_code  IN     CURRENCIES.CURRENCY_CODE%TYPE,
                            I_exchange_rate  IN     CURRENCY_RATES.EXCHANGE_RATE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  DELETE_EXP
--Purpose      :  Deletes expenses for the passed in order/item or
--                order/item/location combination.
-------------------------------------------------------------------------------
FUNCTION DELETE_EXP(O_error_message  IN OUT VARCHAR2,
                    I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                    I_item           IN     ITEM_MASTER.ITEM%TYPE,
                    I_location       IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN; 
---------------------------------------------------------------------------------------------
-- Function Name: DEFAULT_LOC_EXP
-- Purpose      : This function take the expenses associated with the passed in 
--                order/item/pack/zone group/zone/location combination and overwrite all
--                other order/item/location expenses where the location is in the same
--                order/item/zone.
---------------------------------------------------------------------------------------------
FUNCTION DEFAULT_LOC_EXP(O_error_message    IN OUT VARCHAR2,
                         I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item             IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                         I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                         I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                         I_location         IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
  
END ORDER_EXPENSE_SQL;
/
