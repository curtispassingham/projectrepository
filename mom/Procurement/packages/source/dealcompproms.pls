
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DEAL_COMP_PROM_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------
-- Filename: dealcompproms/b.pls
-- Purpose : This is used to check for record existence, retrieve data, and 
--         : delete data from the DEAL_COMP_PROM table
-- Author  : Marge Carino (Accenture - ERC)
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
-- Function Name: GET_ATTRIB
-- Purpose      : Retrieves a row of data in the DEAL_COMP_PROM table given a
--              : deal_id,deal_detail_id, promotion_id and promo_comp_id.
--              : This function is called by dealmain.fmb
----------------------------------------------------------------------------------
FUNCTION GET_ATTRIB (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_deal_comp_prom_rec   IN OUT   DEAL_COMP_PROM%ROWTYPE,
                     I_deal_id              IN       DEAL_HEAD.DEAL_ID%TYPE,
                     I_deal_detail_id       IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                     I_promotion_id         IN       DEAL_COMP_PROM.PROMOTION_ID%TYPE,
                     I_promo_comp_id        IN       DEAL_COMP_PROM.PROMO_COMP_ID%TYPE)
   RETURN BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: CHECK_EXIST
-- Purpose      : Checks for existence in the DEAL_COMP_PROM table given a
--              : deal_id,deal_detail_id, promotion_id and promo_comp_id.
--              : This function is called by dealmain.fmb
----------------------------------------------------------------------------------
FUNCTION CHECK_EXIST (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists           IN OUT   BOOLEAN,
                     I_deal_id          IN       DEAL_HEAD.DEAL_ID%TYPE,
                     I_deal_detail_id   IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                     I_promotion_id     IN       DEAL_COMP_PROM.PROMOTION_ID%TYPE,
                     I_promo_comp_id    IN       DEAL_COMP_PROM.PROMO_COMP_ID%TYPE)
   RETURN BOOLEAN;

----------------------------------------------------------------------------------
-- Function Name: DELETE_DEAL_PROM
-- Purpose      : Delete deals in the DEAL_COMP_PROM table given a
--              : deal_id and deal_detail_id. 
--              : This function is called by dealmain.fmb
----------------------------------------------------------------------------------
FUNCTION DELETE_DEAL_PROM (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_deal_id              IN       DEAL_HEAD.DEAL_ID%TYPE,
                           I_deal_detail_id       IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
END DEAL_COMP_PROM_SQL;
/
