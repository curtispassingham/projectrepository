
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY PO_DIFF_MATRIX_SQL AS

   cursor C_DIFF_ID_Z(L_id DIFF_IDS.DIFF_ID%TYPE) is
      select d.diff_id,
             z.diff_z
        from diff_ids d,
             diff_z_temp z
       where d.diff_id = L_id;

   cursor C_DIFF_GROUP(L_diff_group   DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE) is
      select diff_id
        from diff_group_detail
       where diff_group_id = L_diff_group
         and not exists (select 'x'
                           from diff_z_temp)
       order by display_seq;

   cursor C_DIFF_GROUP_Z(L_diff_group   DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE) is
      select d.diff_id,
             z.diff_z
        from diff_group_detail d,
             diff_z_temp z
       where d.diff_group_id = L_diff_group
       order by d.display_seq;

   cursor C_DIFF_RANGE(L_range_group_no   INTEGER,
                       L_diff_range       DIFF_RANGE_DETAIL.DIFF_RANGE%TYPE,
                       L_diff_group_id    DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE) is
       select distinct DECODE(L_range_group_no,1,r.diff_1,2,r.diff_2,r.diff_3) diff_id,
                       g.display_seq
                from diff_range_detail r,
                     diff_group_detail g
               where r.diff_range = L_diff_range
                 and DECODE(L_range_group_no,1,r.diff_1,2,r.diff_2,r.diff_3) = g.diff_id
                 and g.diff_group_id = L_diff_group_id
                 and not exists (select 'x'
                                   from diff_z_temp)
               order by g.display_seq;

   cursor C_DIFF_RANGE_Z(L_range_group_no   INTEGER,
                         L_diff_range       DIFF_RANGE_DETAIL.DIFF_RANGE%TYPE,
                         L_z_group_no       INTEGER,
                         L_diff_group_id    DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE) is
        select distinct DECODE(L_range_group_no,1,r.diff_1,2,r.diff_2,r.diff_3) diff_id,
                     z.diff_z,
                     g.display_seq
                from diff_range_detail r,
                     diff_group_detail g,
                     diff_z_temp z
               where r.diff_range = L_diff_range
                 and g.diff_group_id = L_diff_group_id
                 and DECODE(L_range_group_no, 1, r.diff_1, 2, r.diff_2, r.diff_3) = g.diff_id
                 and DECODE(L_z_group_no, 1, r.diff_1, 2, r.diff_2, 3, r.diff_3, z.diff_z) = z.diff_z
               order by g.display_seq;

-----------------------------------------------------------------------
FUNCTION EXPLODE_PROMPTS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_x_diff_ind            IN     VARCHAR2,
                         I_x_diff_group_id       IN     DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE,
                         I_x_diff_grouping_value IN     VARCHAR2,
                         I_x_range_group_no      IN     INTEGER,
                         I_y_diff_ind            IN     VARCHAR2,
                         I_y_diff_group_id       IN     DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE,
                         I_y_diff_grouping_value IN     VARCHAR2,
                         I_y_range_group_no      IN     INTEGER,
                         I_z_diff_ind            IN     VARCHAR2,
                         I_z_diff_group_id       IN     DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE,
                         I_z_diff_grouping_value IN     VARCHAR2,
                         I_z_range_group_no      IN     INTEGER)
   return BOOLEAN is

   cursor C_X_COUNT is
      select max(count(*))
        from diff_x_temp
       group by diff_z;

   L_inv_parm   VARCHAR2(30)      := NULL;
   L_program    VARCHAR2(100)     := 'PO_DIFF_MATRIX_SQL.EXPLODE_PROMPTS';
   i            BINARY_INTEGER;
   L_x_count    INTEGER;
   L_z_group_no INTEGER;

BEGIN


   -- validate mandatory input parameters. Since diff ranges can
   -- be associated with up to 3 diff groups, the specific group number
   -- (e.g. 1,2,3) must be sent in with the diff range.
   if I_x_diff_ind is NULL then
      L_inv_parm := 'I_x_diff_ind';
   elsif I_x_diff_grouping_value is NULL then
      L_inv_parm := 'I_x_diff_grouping_value';
   elsif I_y_diff_ind is NULL then
      L_inv_parm := 'I_y_diff_ind';
  elsif I_y_diff_grouping_value is NULL then
      L_inv_parm := 'I_y_grouping_value';
   elsif I_x_diff_ind = 'N' and I_x_range_group_no is NULL then
      L_inv_parm := 'I_x_range_group_no';
   elsif I_y_diff_ind = 'N' and I_y_range_group_no is NULL then
      L_inv_parm := 'I_y_range_group_no';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- initialize by deleting from the temp tables.  (tables could still have values if
   --  user has clicked the refresh button during the session.)
   delete from diff_x_temp;
   delete from po_diff_matrix_temp;
   delete from diff_z_temp;
   ---

   if POPULATE_DIFF_Z(O_error_message,
                      I_z_diff_ind,
                      I_z_diff_group_id,
                      I_z_diff_grouping_value,
                      I_z_range_group_no) = FALSE then
      return FALSE;
   end if;

   i := 1;

   if I_x_diff_ind = 'I' then --insert diff ID
      ---
      if I_z_diff_ind is NULL then
         -- no values will be on z axis so there is no need to create multiple
         insert into diff_x_temp (diff_x,
                                  seq_no)
                          select I_x_diff_grouping_value,
                                 nvl(max(seq_no + 1), 1)
                            from diff_x_temp;

      else
         for rec in C_DIFF_ID_Z(I_x_diff_grouping_value) LOOP
            insert into diff_x_temp (diff_x,
                                     diff_z,
                                     seq_no)
                             select I_x_diff_grouping_value,
                                    rec.diff_z,
                                    nvl(max(seq_no + 1), 1)
                               from diff_x_temp
                              where diff_z = rec.diff_z;
         end LOOP;
      end if;
      ---
   elsif I_x_diff_ind = 'G' then -- insert all diff IDs within passed group
      ---
      if I_z_diff_ind is NULL then

         for rec in C_DIFF_GROUP(I_x_diff_grouping_value) LOOP
            insert into diff_x_temp (diff_x,
                                     seq_no)
                             select rec.diff_id,
                                    nvl(max(seq_no + 1), 1)
                               from diff_x_temp;
         end LOOP;
      else
         for rec in C_DIFF_GROUP_Z(I_x_diff_grouping_value) LOOP
            insert into diff_x_temp(diff_x,
                                    diff_z,
                                    seq_no)
                             select rec.diff_id,
                                    rec.diff_z,
                                    nvl(max(seq_no + 1), 1)
                               from diff_x_temp
                              where diff_z = rec.diff_z;
         end LOOP;
      end if;
      ---
   elsif I_x_diff_ind = 'N' then -- insert all diff IDs within passed range
      ---
      if I_z_diff_ind is NULL then
         for rec in C_DIFF_RANGE(I_x_range_group_no,
                                 I_x_diff_grouping_value,
                                 I_x_diff_group_id) LOOP
            insert into diff_x_temp(diff_x,
                                    seq_no)
                             select rec.diff_id,
                                    nvl(max(seq_no + 1), 1)
                               from diff_x_temp;
         end LOOP;
      else
         if I_z_diff_ind = 'N' and I_z_diff_grouping_value = I_x_diff_grouping_value then
            -- Both the x and z axes are being limited by the same range
            L_z_group_no := I_z_range_group_no;
         end if;

         for rec in C_DIFF_RANGE_Z(I_x_range_group_no,
                                   I_x_diff_grouping_value,
                                   L_z_group_no,
                                   I_x_diff_group_id) LOOP
            insert into diff_x_temp(diff_x,
                                    diff_z,
                                    seq_no)
                             select rec.diff_id,
                                    rec.diff_z,
                                    nvl(max(seq_no + 1), 1)
                               from diff_x_temp
                              where diff_z = rec.diff_z;

         end LOOP;
      end if;
      ---
   elsif I_x_diff_ind = 'R' then
      null;
   end if;

   -- current system limitation: X axis cannot display more than 30 diff IDs.
   -- If the z axis is populated the number of 'x' values needs to be determined
   -- for each z value.

   open C_X_COUNT;
   fetch C_X_COUNT into L_x_count;
   close C_X_COUNT;
   if L_x_count > 30 then
      O_error_message := SQL_LIB.CREATE_MSG('MUST_LIMIT_X_AXIS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_y_diff_ind = 'I' then -- insert diff ID
      ---
      if I_z_diff_ind is NULL then
         insert into po_diff_matrix_temp(diff_y,
                                         seq_no)
                                 values (I_y_diff_grouping_value,
                                         1);
      else
         for rec in C_DIFF_ID_Z(I_y_diff_grouping_value) LOOP
            insert into po_diff_matrix_temp(diff_y,
                                            diff_z,
                                            seq_no)
                                    values (I_y_diff_grouping_value,
                                            rec.diff_z,
                                            1);
         end LOOP;
      end if;
      ---
   elsif I_y_diff_ind = 'G' then -- insert all diff IDs within the passed group
      ---
      if I_z_diff_ind is NULL then
         for rec in C_DIFF_GROUP(I_y_diff_grouping_value) LOOP
             insert into po_diff_matrix_temp(diff_y,
                                             seq_no)
                                      select rec.diff_id,
                                             nvl(max(seq_no + 1), 1)
                                        from po_diff_matrix_temp;
         end LOOP;
      else
         for rec in C_DIFF_GROUP_Z(I_y_diff_grouping_value) LOOP
             insert into po_diff_matrix_temp(diff_y,
                                             diff_z,
                                             seq_no)
                                      select rec.diff_id,
                                             rec.diff_z,
                                             nvl(max(seq_no + 1), 1)
                                        from po_diff_matrix_temp
                                       where diff_z = rec.diff_z;
         end LOOP;
      end if;
      ---
   elsif I_y_diff_ind = 'N' then -- insert all diff IDs within the passed range
      ---
      if I_z_diff_ind is NULL then
         for rec in C_DIFF_RANGE(I_y_range_group_no,
                                 I_y_diff_grouping_value,
                                 I_y_diff_group_id) LOOP
            insert into po_diff_matrix_temp(diff_y,
                                            seq_no)
                                     select rec.diff_id,
                                            nvl(max(seq_no + 1), 1)
                                       from po_diff_matrix_temp;
         end LOOP;
      else
         L_z_group_no := NULL;
         if I_z_diff_ind = 'N' and I_z_diff_grouping_value = I_y_diff_grouping_value then
            -- Both the y and z axes are being limited by the same range
            L_z_group_no := I_z_range_group_no;
         end if;

         for rec in C_DIFF_RANGE_Z(I_y_range_group_no,
                                   I_y_diff_grouping_value,
                                   L_z_group_no,
                                   I_y_diff_group_id) LOOP
            insert into po_diff_matrix_temp(diff_y,
                                            diff_z,
                                            seq_no)
                                     select rec.diff_id,
                                            rec.diff_z,
                                            nvl(max(seq_no + 1), 1)
                                       from po_diff_matrix_temp
                                      where diff_z = rec.diff_z;
         end LOOP;
     end if;
     ---
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_PROMPTS;
-----------------------------------------------------------------------
FUNCTION POPULATE_DIFF_Z(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_diff_ind              IN     VARCHAR2,
                         I_diff_z_group_id       IN     DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE,
                         I_diff_grouping_value   IN     VARCHAR2,
                         I_range_group_no        IN     VARCHAR2)
   return BOOLEAN is

   cursor C_RANGE_Z is
      select distinct DECODE(I_range_group_no, 1, r.diff_1, 2, r.diff_2, r.diff_3) diff_id,
             g.display_seq
        from diff_range_detail r,
             diff_group_detail g
       where r.diff_range = I_diff_grouping_value
         and g.diff_group_id = I_diff_z_group_id
         and DECODE(I_range_group_no, 1, r.diff_1, 2, r.diff_2, r.diff_3) = g.diff_id
       order by g.display_seq;

   cursor C_GROUP_Z is
      select diff_id
        from diff_group_detail
       where diff_group_id = I_diff_grouping_value
       order by display_seq;

   L_program     VARCHAR2(100)    := 'PO_DIFF_MATRIX_SQL.POPULATE_DIFF_Z';

BEGIN

   if I_diff_ind = 'I' then
      insert into diff_z_temp (diff_z, copy_ind)
                       values (I_diff_grouping_value, 'N');
   elsif I_diff_ind = 'G' then
      for rec in C_GROUP_Z LOOP
         insert into diff_z_temp (diff_z,
                                  seq_no,
                                  copy_ind)
                           select rec.diff_id,
                                  nvl(max(seq_no + 1), 1),
                                  'N'
                             from diff_z_temp;
      end LOOP;
   elsif I_diff_ind = 'N' then
      for rec in C_RANGE_Z LOOP
         insert into diff_z_temp (diff_z,
                                  seq_no,
                                  copy_ind)
                          select rec.diff_id,
                                 nvl(max(seq_no + 1), 1),
                                 'N'
                            from diff_z_temp;
      end LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_DIFF_Z;
-----------------------------------------------------------------------
FUNCTION DISTRIBUTE_TO_DIFF(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_matrix_total     IN     NUMBER,
                            I_matrix_qty_type  IN     VARCHAR2,
                            I_z_total          IN     NUMBER,
                            I_z_qty_type       IN     VARCHAR2,
                            I_z_axis_populated IN     BOOLEAN)
   return BOOLEAN is

   L_inv_parm               VARCHAR2(30)     := NULL;
   L_program                VARCHAR2(100)    := 'PO_DIFF_MATRIX_SQL.DISTRIBUTE_TO_DIFF';
   L_column_sum_statement   VARCHAR2(150);
   L_column_sum             NUMBER(12,4);
   L_matrix_sum             NUMBER(12,4);
   L_statement              VARCHAR2(2000)   := NULL;
   L_delete_me_tl_qty       number(4)        := 1000;
   L_qty_calc_statement     VARCHAR2(100);
   L_x_count                INTEGER;
   L_diff_z                 DIFF_Z_TEMP.DIFF_Z%TYPE;
   L_insert_qty             BOOLEAN := FALSE;

   cursor C_Z_INFO is
      select diff_z,
             nvl(value,0) value
        from diff_z_temp;

   cursor C_X_COUNT_FOR_Z is
      select count(*)
        from diff_x_temp
       where diff_z = L_diff_z;

   cursor C_X_COUNT is
      select count(*)
        from diff_x_temp;

BEGIN

   if I_matrix_total is NULL then
      L_inv_parm := 'I_matrix_total';
   elsif I_matrix_qty_type is NULL then
      L_inv_parm := 'I_qty_type';
   elsif I_z_axis_populated is NULL then
      L_inv_parm := 'I_z_axis_populated';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_z_axis_populated = TRUE then

      if I_z_qty_type = 'Q' or I_matrix_qty_type = 'Q' then
         L_insert_qty := TRUE;
      end if;

      for rec in C_Z_INFO LOOP

         L_diff_z := rec.diff_z;

         open C_X_COUNT_FOR_Z;
         fetch C_X_COUNT_FOR_Z into L_x_count;
         close C_X_COUNT_FOR_Z;

         for i in 1 .. L_x_count LOOP

            if I_z_qty_type = 'Q' then
               L_qty_calc_statement := rec.value;
            elsif I_z_qty_type = 'R' then
               L_qty_calc_statement := '('||rec.value||' / nvl('||I_z_total||', 1))';
               if I_matrix_qty_type != 'Q' then
                  L_qty_calc_statement := L_qty_calc_statement||' * 100 ';
               end if;
            elsif I_z_qty_type = 'P' then
               if I_matrix_qty_type = 'Q' then
                  L_qty_calc_statement := '('||rec.value||' / 100)';
               else
                  L_qty_calc_statement := rec.value;
               end if;
            end if;

            if I_matrix_qty_type = 'Q' then
               if I_z_qty_type != 'Q' then
                  L_qty_calc_statement := L_qty_calc_statement||' * p.value'||i;
               else
                  L_qty_calc_statement := ' p.value'||i;
               end if;
            elsif I_matrix_qty_type = 'R' then
               -- must retrieve matrix total for the current diff z to correctly calculate ratio
               L_matrix_sum := 0;
               for i in 1 .. L_x_count LOOP
                  L_column_sum_statement := 'select sum(value'||i||')'||
                                             ' from po_diff_matrix_temp '||
                                            ' where diff_z = :l_differ_z ';

                  EXECUTE IMMEDIATE L_column_sum_statement into L_column_sum USING L_diff_z;

                  if L_column_sum is not NULL then
                     L_matrix_sum := L_matrix_sum + L_column_sum;
                  end if;
               end LOOP;

               L_qty_calc_statement := L_qty_calc_statement||' * (p.value'||i||' / nvl('||L_matrix_sum||', 1))';

            elsif I_matrix_qty_type = 'P' then
               L_qty_calc_statement := L_qty_calc_statement||' * (p.value'||i||' / 100)';
            end if;
            L_statement := 'insert into diff_dist_matrix (diff_x, '||
                                                         'diff_y, '||
                                                         'diff_z, ';

            if L_insert_qty = TRUE then
               L_statement := L_statement||'qty) ';
            else
               L_statement := L_statement||'pct) ';
            end if;

            L_statement := L_statement||         'select x.diff_x, '||
                                                         'p.diff_y, '||
                                                         'p.diff_z, '||
                                                         L_qty_calc_statement||
                                                  ' from po_diff_matrix_temp p,'||
                                                  '      diff_x_temp x,'||
                                                  '      diff_z_temp z'||
                                                 ' where z.diff_z = :l_differ_z '||
                                                 '   and x.diff_z = z.diff_z'||
                                                 '   and p.diff_z = z.diff_z'||
                                                 '   and p.value'||i||' is not NULL'||
                                                 '   and x.seq_no = '||i;

            EXECUTE IMMEDIATE L_statement USING L_diff_z;
         end LOOP; -- end of 'x' diff loop for this diff z
      end LOOP; -- end of loop for this diff z
      ---
   else -- z axis is not populated

      open C_X_COUNT;
      fetch C_X_COUNT into L_x_count;
      close C_X_COUNT;

      if I_matrix_qty_type = 'Q' then
         L_insert_qty := TRUE;
      end if;

      for i in 1 .. L_x_count LOOP


         if I_matrix_qty_type in ('Q','P') then
            L_qty_calc_statement := 'p.value'||i;
         elsif I_matrix_qty_type = 'R' then
            L_qty_calc_statement := '(p.value'||i||' / '||I_matrix_total||') * 100';
         end if;
         L_statement := 'insert into diff_dist_matrix (diff_x, '||
                                                      'diff_y, ';

         if L_insert_qty = TRUE then
            L_statement := L_statement||'qty) ';
         else
            L_statement := L_statement||'pct) ';
         end if;

         L_statement := L_statement||         'select x.diff_x, '||
                                                      'p.diff_y, '||
                                                      L_qty_calc_statement||
                                               ' from po_diff_matrix_temp p,'||
                                               '      diff_x_temp x'||
                                              ' where p.value'||i||' is not NULL'||
                                                ' and x.seq_no = '||i;
         EXECUTE IMMEDIATE L_statement;
      end LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DISTRIBUTE_TO_DIFF;
------------------------------------------------------------------------
FUNCTION DISTRIBUTE_TO_ORD_ITEMS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_diff_x_no     IN     INTEGER,
                                 I_diff_y_no     IN     INTEGER,
                                 I_diff_z_no     IN     INTEGER,
                                 I_dist_type     IN     VARCHAR2,
                                 I_dist_uom_type IN     VARCHAR2,
                                 I_dist_uom      IN     UOM_CLASS.UOM%TYPE,
                                 I_uop           IN     ORDLOC_WKSHT.UOP%TYPE,
                                 I_where_clause  IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                                 I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN is

   L_inv_parm         VARCHAR2(30)   := NULL;
   L_program          VARCHAR2(100)  := 'PO_DIFF_MATRIX_SQL.DISTRIBUTE_TO_ORD_ITEMS';
   L_where_clause     VARCHAR2(4000);
   L_dist_qty_string  VARCHAR2(100);
   L_calc_qty_string  VARCHAR2(100);
   L_act_qty_string   VARCHAR2(100);
   L_wksht_qty_string VARCHAR2(100);
   L_var_qty_string   VARCHAR2(300);
   L_statement        LONG;
   L_diff_1_statement VARCHAR2(100);
   L_diff_2_statement VARCHAR2(100);
   L_diff_3_statement VARCHAR2(100);
   L_diff_4_statement VARCHAR2(100);

BEGIN

   ----------------------------------------------------------------------------------------
   -- order items can be diff-distributed by SUOM (standard uom) or UOP (unit of purchase).
   --
   -- Valid UOPs are either Case or the SUOM. (When Pallet is entered as a UOP,
   -- the quantity and UOP will be blown down to Case.)
   --
   -- Possible unit of distribution (UOD) and unit of purchase (UOP) combinations:
   -- 1. uop: SUOM -> uod: SUOM
   -- 2. uop: Case -> uod: SUOM
   -- 3. uop: Case -> uod: Case
   ----------------------------------------------------------------------------------------
   -- ordloc_wksht qty fields defined:
   --  calc_qty:  contains the raw (potentially decimal) distributed value in the SUOM.
   --             this value will be used by the round-to-pack logic to populate act_qty.
   --  act_qty:   contains the distribution quantity in the SUOM but has been rounded-to-pack.
   --  wksht_qty: contains the distribution quantity converted into the UOP and has been
   --             rounded to an integer.
   ----------------------------------------------------------------------------------------
   -- I_dist_uom_type will contain:  'C'ase, 'E'aches, or 'S'uom
   ----------------------------------------------------------------------------------------
   if I_diff_x_no is NULL then
      L_inv_parm := 'I_diff_x_no';
   elsif I_diff_y_no is NULL then
      L_inv_parm := 'I_diff_y_no';
   elsif I_dist_type is NULL then
      L_inv_parm := 'I_dist_type';
   elsif I_dist_uom_type is NULL then
      L_inv_parm := 'I_dist_uom_type';
   elsif I_dist_uom is NULL then
      L_inv_parm := 'I_dist_uom';
   elsif I_uop is NULL then
      L_inv_parm := 'I_uop';
   elsif I_order_no is NULL then
      L_inv_parm := 'I_order_no';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if PO_DIFF_MATRIX_SQL.UPDATE_ORDER_DIFF(O_error_message,
                                           I_order_no,
                                           I_where_clause,
                                           I_dist_type,
                                           I_dist_uom_type,
                                           I_dist_uom,
                                           I_uop) = FALSE then
      return FALSE;
   end if;
   ---
   if I_where_clause is NOT NULL then
      L_where_clause := ' and ' || I_where_clause;
   end if;
   ---
   if I_dist_type = 'Q' then
      -- distribution values are stored as quantities
      L_dist_qty_string := ' diff_dist_matrix.qty ';
   elsif I_dist_type = 'P' then
      -- distribution values are stored as percentage
      if I_dist_uom_type = 'C' then
         L_dist_qty_string := ' ordloc_wksht.wksht_qty * (diff_dist_matrix.pct / 100) ';
      else
         L_dist_qty_string := ' ordloc_wksht.calc_qty * (diff_dist_matrix.pct / 100) ';
      end if;
   end if;

   if I_dist_uom_type = 'C' then
      -- Case can't be a SUOM, so if the UOD is Case, the UOP must be Case
      L_wksht_qty_string := 'ROUND('||L_dist_qty_string||')';
      L_calc_qty_string := '('||L_dist_qty_string||' * ordloc_wksht.supp_pack_size) ';

      -- since dirtribution UOM is Case, the wksht qty that has been rounded to integer will result
      -- in a value that is already rounded to pack. This value can be multiplied by
      -- supplier pack size and placed in act_qty since no further rounding to pack is necessary.
      L_act_qty_string  := '('||L_wksht_qty_string||' * ordloc_wksht.supp_pack_size) ';
   else
      -- if UOP is case or eaches, populate wksht_qty that user will see with an integer value.
      -- if UOP is any other valid SUOM (e.g. pounds), leave value as is (may contain up to 4 decimal places.)

      if I_dist_uom = I_uop then
         -- UOD and UOP are both SUOM. No pack size conversion needed.
         -- if SUOM is eaches, round qty to integer
         if I_dist_uom_type = 'E' then
            L_wksht_qty_string := 'ROUND('||L_dist_qty_string||')';
         else
            L_wksht_qty_string := L_dist_qty_string;
         end if;
      elsif I_dist_uom != I_uop then
         -- divide distribution qty by pack size to convert into Case UOP. Use
         -- raw distro qty and oracle ROUND to ensure proper rounding to integer
         -- as supp pack size could be a decimal value.
         L_wksht_qty_string := 'ROUND('||L_dist_qty_string||'/ordloc_wksht.supp_pack_size)';
      end if;

      -- Insert raw qty into the calc_qty and act_qty columns because act_qty
      -- will eventually be rounded to pack size.
      L_calc_qty_string  := L_dist_qty_string;
      L_act_qty_string   := L_dist_qty_string;
   end if;

   L_var_qty_string := '((('||L_act_qty_string ||'-'||L_calc_qty_string ||') * 100) / '||L_calc_qty_string||')';

   if I_diff_x_no = 1 then
      L_diff_1_statement := ' diff_dist_matrix.diff_x ';
   elsif I_diff_x_no = 2 then
      L_diff_2_statement := ' diff_dist_matrix.diff_x ';
   elsif I_diff_x_no = 3 then
      L_diff_3_statement := ' diff_dist_matrix.diff_x ';
   elsif I_diff_x_no = 4 then
      L_diff_4_statement := ' diff_dist_matrix.diff_x ';
   end if;

   if I_diff_y_no = 1 then
      L_diff_1_statement := ' diff_dist_matrix.diff_y ';
   elsif I_diff_y_no = 2 then
      L_diff_2_statement := ' diff_dist_matrix.diff_y ';
   elsif I_diff_y_no = 3 then
      L_diff_3_statement := ' diff_dist_matrix.diff_y ';
   elsif I_diff_y_no = 4 then
      L_diff_4_statement := ' diff_dist_matrix.diff_y ';
   end if;

   if I_diff_z_no = 1 then
      L_diff_1_statement := ' diff_dist_matrix.diff_z ';
   elsif I_diff_z_no = 2 then
      L_diff_2_statement := ' diff_dist_matrix.diff_z ';
   elsif I_diff_z_no = 3 then
      L_diff_3_statement := ' diff_dist_matrix.diff_z ';
   elsif I_diff_z_no = 4 then
      L_diff_4_statement := ' diff_dist_matrix.diff_z ';
   end if;

   if L_diff_1_statement is NULL then
      L_diff_1_statement := ' ordloc_wksht.diff_1 ';
   end if;

   if L_diff_2_statement is NULL then
      L_diff_2_statement := ' ordloc_wksht.diff_2 ';
   end if;

   if L_diff_3_statement is NULL then
      L_diff_3_statement := ' ordloc_wksht.diff_3 ';
   end if;

   if L_diff_4_statement is NULL then
      L_diff_4_statement := ' ordloc_wksht.diff_4 ';
   end if;
   L_statement := 'insert into ordloc_wksht(order_no, '||
                                            'item_parent, '||
                                            'item, '||
                                            'ref_item, '||
                                            'diff_1, '||
                                            'diff_2, '||
                                            'diff_3, '||
                                            'diff_4, '||
                                            'store_grade, '||
                                            'store_grade_group_id, '||
                                            'loc_type, '||
                                            'location, '||
                                            'calc_qty, '||
                                            'act_qty, '||
                                            'standard_uom, '||
                                            'variance_qty, '||
                                            'origin_country_id, '||
                                            'wksht_qty, '||
                                            'uop, '||
                                            'supp_pack_size) '||
                                     'select distinct ordloc_wksht.order_no, '||
                                            'ordloc_wksht.item_parent, '||
                                            'ordloc_wksht.item, '||
                                            'ordloc_wksht.ref_item, '||
                                             L_diff_1_statement||', '||
                                             L_diff_2_statement||', '||
                                             L_diff_3_statement||', '||
                                             L_diff_4_statement||', '||
                                            'ordloc_wksht.store_grade, '||
                                            'ordloc_wksht.store_grade_group_id, '||
                                            'ordloc_wksht.loc_type, '||
                                            'ordloc_wksht.location, '||
                                            L_calc_qty_string ||', ' ||
                                            L_act_qty_string ||', ' ||
                                            'ordloc_wksht.standard_uom, '||
                                            L_var_qty_string ||', ' ||
                                            'ordloc_wksht.origin_country_id, '||
                                            L_wksht_qty_string||', ' ||
                                            'ordloc_wksht.uop, ' ||
                                            'ordloc_wksht.supp_pack_size '||
                       'from ordloc_wksht, diff_dist_matrix '||
                      'where ordloc_wksht.order_no = :l_order_no   '||
                       L_where_clause||
                       ' and (nvl(diff_dist_matrix.qty, 0) > 0 '||
                       '      or nvl(diff_dist_matrix.pct, 0) > 0) '||
                       ' and not exists (select ''x''  '||
                                         ' from ordloc_wksht o2 ' ||
                                         'where o2.order_no = ordloc_wksht.order_no ' ||
                                           'and o2.rowid != ordloc_wksht.rowid '||
                                           'and nvl(o2.store_grade_group_id,-1) = nvl(ordloc_wksht.store_grade_group_id,-1) ' ||
                                           'and nvl(o2.store_grade,-1) = nvl(ordloc_wksht.store_grade,-1) ' ||
                                           'and nvl(o2.loc_type,-1) = nvl(ordloc_wksht.loc_type,-1) ' ||
                                           'and nvl(o2.location,-1) = nvl(ordloc_wksht.location,-1) ' ||
                                           'and nvl(o2.item_parent,-1) = nvl(ordloc_wksht.item_parent,-1) ' ||
                                           'and diff_dist_matrix.diff_x in (o2.diff_1, o2.diff_2, o2.diff_3, o2.diff_4) '||
                                           'and diff_dist_matrix.diff_y in (o2.diff_1, o2.diff_2, o2.diff_3, o2.diff_4) '||
                                           'and (diff_dist_matrix.diff_z in (o2.diff_1, o2.diff_2, o2.diff_3, o2.diff_4) '||
                                           '     or diff_dist_matrix.diff_z is NULL) '||
                                           'and (nvl(o2.diff_1, -1) = nvl(ordloc_wksht.diff_1, -1) '||
                                                'or o2.diff_1 in (diff_dist_matrix.diff_x, '||
                                                                 'diff_dist_matrix.diff_y, '||
                                                                 'diff_dist_matrix.diff_z)) '||
                                           'and (nvl(o2.diff_2, -1) = nvl(ordloc_wksht.diff_2, -1) '||
                                                'or o2.diff_2 in (diff_dist_matrix.diff_x, '||
                                                                 'diff_dist_matrix.diff_y, '||
                                                                 'diff_dist_matrix.diff_z)) '||
                                           'and (nvl(o2.diff_3, -1) = nvl(ordloc_wksht.diff_3, -1) '||
                                                'or o2.diff_3 in (diff_dist_matrix.diff_x, '||
                                                                 'diff_dist_matrix.diff_y, '||
                                                                 'diff_dist_matrix.diff_z)) '||
                                           'and (nvl(o2.diff_4, -1) = nvl(ordloc_wksht.diff_4, -1) '||
                                                'or o2.diff_4 in (diff_dist_matrix.diff_x, '||
                                                                 'diff_dist_matrix.diff_y, '||
                                                                 'diff_dist_matrix.diff_z)))';

   EXECUTE IMMEDIATE L_statement USING I_order_no;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DISTRIBUTE_TO_ORD_ITEMS;
------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_DIFF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no      IN     ORDLOC_WKSHT.ORDER_NO%TYPE,
                           I_where_clause  IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                           I_dist_type     IN     VARCHAR2,
                           I_dist_uom_type IN     VARCHAR2,
                           I_dist_uom      IN     VARCHAR2,
                           I_uop           IN     ORDLOC_WKSHT.UOP%TYPE)
RETURN BOOLEAN IS

   L_inv_parm             VARCHAR2(30);
   L_program              VARCHAR2(100)     := 'PO_DIFF_MATRIX_SQL.UPDATE_ORDER_DIFF';
   L_where_clause         VARCHAR2(4000);
   L_calc_qty             ORDLOC_WKSHT.CALC_QTY%TYPE;
   L_wksht_qty            ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_var_qty              ORDLOC_WKSHT.VARIANCE_QTY%TYPE;
   L_act_qty              ORDLOC_WKSHT.ACT_QTY%TYPE;
   TYPE ORD_CURSOR is REF CURSOR;
   C_UPDATE               ORD_CURSOR;

   L_statement            VARCHAR2(2000);
   L_item_parent          ORDLOC_WKSHT.ITEM_PARENT%TYPE;
   L_item                 ORDLOC_WKSHT.ITEM%TYPE;
   L_diff_1               ORDLOC_WKSHT.DIFF_1%TYPE;
   L_diff_2               ORDLOC_WKSHT.DIFF_2%TYPE;
   L_diff_3               ORDLOC_WKSHT.DIFF_3%TYPE;
   L_diff_4               ORDLOC_WKSHT.DIFF_4%TYPE;
   L_store_grade_group_id ORDLOC_WKSHT.STORE_GRADE_GROUP_ID%TYPE;
   L_store_grade          ORDLOC_WKSHT.STORE_GRADE%TYPE;
   L_loc_type             ORDLOC_WKSHT.LOC_TYPE%TYPE;
   L_location             ORDLOC_WKSHT.LOCATION%TYPE;
   L_standard_uom         ORDLOC_WKSHT.STANDARD_UOM%TYPE;
   L_supp_pack_size       ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;
   L_multiple             ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;

BEGIN
   -- This function will update filtered-out records in ordmtxws that have diff values that match
   -- those in the distribution matrix.

   -- If I_dist_type = 'Q', distribution values on diff_dist_matrix are stored as quantities
   -- If I_dist_type = 'P', distribution values on diff_dist_matrix are stored as percentages.
   -- Percentage distribution values can result from distributing by percent or ratio.

   -- I_dist_uom_type will contain:  'C'ase, 'E'aches, or 'S'uom

   -- select statement below uses double-alias of ordloc_wksht table.
   -- The 'ordloc_wksht' join will be limited by the I_where clause. These records
   -- represent the records that are currently displayed on ordmtxws.

   -- The join to ordloc_wksht that is aliased with 'o2' represents any items
   -- that have been filtered out and are not currently displayed on ordmtxws. These
   -- are the records which may need to be updated

   if I_dist_type is NULL then
      L_inv_parm := 'I_dist_type';
   elsif I_dist_uom_type is NULL then
      L_inv_parm := 'I_dist_uom_type';
   elsif I_dist_uom is NULL then
      L_inv_parm := 'I_dist_uom';
   elsif I_uop is NULL then
      L_inv_parm := 'I_uop';
   elsif I_order_no is NULL then
      L_inv_parm := 'I_order_no';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   if I_where_clause is NOT NULL then
      L_where_clause := ' and ' || I_where_clause;
   end if;
   ---

   L_statement := 'select o2.item_parent, o2.item, o2.diff_1, o2.diff_2, o2.diff_3, o2.diff_4, o2.store_grade_group_id, '||
                          'o2.store_grade, o2.loc_type, o2.location, o2.standard_uom, o2.wksht_qty, o2.supp_pack_size, ';

   if I_dist_type = 'Q' then
      L_statement := L_statement
                     || 'd.qty * DECODE('''||I_dist_uom_type||''', ''C'', o2.supp_pack_size, 1) + nvl(o2.calc_qty, o2.act_qty) ';
   elsif I_dist_type = 'P' then
      L_statement := L_statement || 'ordloc_wksht.calc_qty * (d.pct / 100) + nvl(o2.calc_qty, o2.act_qty) ';
   end if;

   L_statement := L_statement||
                    'from ordloc_wksht, diff_dist_matrix d, ordloc_wksht o2 '||
                   'where ordloc_wksht.order_no = :l_order_no '||
                          L_where_clause||' '||
         'and o2.order_no = ordloc_wksht.order_no ' ||
         'and o2.item_parent = ordloc_wksht.item_parent ' ||
         'and nvl(o2.store_grade_group_id,-1) = nvl(ordloc_wksht.store_grade_group_id,-1) ' ||
         'and nvl(o2.store_grade,-1) = nvl(ordloc_wksht.store_grade,-1) ' ||
         'and nvl(o2.loc_type,-1) = nvl(ordloc_wksht.loc_type,-1) ' ||
         'and nvl(o2.location,-1) = nvl(ordloc_wksht.location,-1) ' ||
         'and (nvl(d.qty,0) > 0 '||
              'or nvl(d.pct,0) > 0) '||
         'and d.diff_x in (o2.diff_1, o2.diff_2, o2.diff_3, o2.diff_4) '||
         'and d.diff_y in (o2.diff_1, o2.diff_2, o2.diff_3, o2.diff_4) '||
         'and (d.diff_z in (o2.diff_1, o2.diff_2, o2.diff_3, o2.diff_4)'||
         '     or d.diff_z is NULL) '||
         'and (o2.diff_1 = ordloc_wksht.diff_1 or ordloc_wksht.diff_1 is NULL) '||
         'and (o2.diff_2 = ordloc_wksht.diff_2 or ordloc_wksht.diff_2 is NULL) '||
         'and (o2.diff_3 = ordloc_wksht.diff_3 or ordloc_wksht.diff_3 is NULL) '||
         'and (o2.diff_4 = ordloc_wksht.diff_4 or ordloc_wksht.diff_4 is NULL)';

   EXECUTE IMMEDIATE L_statement USING I_order_no;
   open C_UPDATE for L_statement USING I_order_no;

   LOOP

      fetch C_UPDATE into L_item_parent,
                          L_item,
                          L_diff_1,
                          L_diff_2,
                          L_diff_3,
                          L_diff_4,
                          L_store_grade_group_id,
                          L_store_grade,
                          L_loc_type,
                          L_location,
                          L_standard_uom,
                          L_wksht_qty,
                          L_supp_pack_size,
                          L_calc_qty;

      EXIT WHEN C_UPDATE%NOTFOUND;


      if I_uop = L_standard_uom and L_standard_uom != 'EA' then
         -- unit of distribution is SUOM also
         L_wksht_qty := ROUND(L_calc_qty, 4);
         L_act_qty   := L_wksht_qty;
         L_var_qty := ((L_act_qty - L_calc_qty)/L_calc_qty) * 100;
      else
         -- UOP is 'case' or the SUOM 'eaches'
         -- round these numbers to the nearest integer
         if I_uop = L_standard_uom then
            L_multiple := 1;
         else
            -- calc_qty is always stored in SUOM so the packsize is
            -- needed to calculate case qty to be inserted into wksht_qty
            L_multiple := L_supp_pack_size;
         end if;
         ---
         L_wksht_qty    := ROUND(L_calc_qty/L_multiple);
         L_act_qty      := (L_wksht_qty * L_multiple);
         L_var_qty := ((L_act_qty - L_calc_qty) / L_calc_qty) * 100;
      end if;
      update ordloc_wksht
         set act_qty      = L_act_qty,
             calc_qty     = L_calc_qty,
             variance_qty = L_var_qty,
             wksht_qty    = L_wksht_qty
       where order_no     = I_order_no
         and nvl(item_parent,-1)           = nvl(L_item_parent,-1)
         and nvl(store_grade, -1)          = nvl(L_store_grade, -1)
         and nvl(store_grade_group_id, -1) = nvl(L_store_grade_group_id, -1)
         and nvl(loc_type, -1)             = nvl(L_loc_type, -1)
         and nvl(location, -1)             = nvl(L_location, -1)
         and nvl(diff_1, -1)               = nvl(L_diff_1, -1)
         and nvl(diff_2, -1)               = nvl(L_diff_2, -1)
         and nvl(diff_3, -1)               = nvl(L_diff_3, -1)
         and nvl(diff_4, -1)               = nvl(L_diff_4, -1);
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ORDER_DIFF;
------------------------------------------------------------------------
FUNCTION REDIST_TO_ORD_ITEMS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_diff_x_no         IN     INTEGER,
                             I_diff_y_no         IN     INTEGER,
                             I_diff_z_no         IN     INTEGER,
                             I_dist_uom_type     IN     VARCHAR2,
                             I_dist_uom          IN     UOM_CLASS.UOM%TYPE,
                             I_standard_uom      IN     ORDLOC_WKSHT.STANDARD_UOM%TYPE,
                             I_origin_country_id IN     ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE,
                             I_uop               IN     ORDLOC_WKSHT.UOP%TYPE,
                             I_supp_pack_size    IN     ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE,
                             I_item_parent       IN     ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                             I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN is

   L_inv_parm              VARCHAR2(30) := NULL;
   L_program               VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.REDIST_TO_ORD_ITEMS';
   L_first_undist_diff_no  INTEGER;
   L_second_undist_diff_no INTEGER;
   L_first_undist_found    BOOLEAN;
   L_diff_1_statement      VARCHAR2(100);
   L_diff_2_statement      VARCHAR2(100);
   L_diff_3_statement      VARCHAR2(100);
   L_diff_4_statement      VARCHAR2(100);
   L_dist_qty_string       VARCHAR2(100);
   L_calc_qty_string       VARCHAR2(100);
   L_act_qty_string        VARCHAR2(100);
   L_wksht_qty_string      VARCHAR2(100);
   L_var_qty_string        VARCHAR2(300);
   L_statement             LONG;
   L_diff_1_undist_id_ind  VARCHAR2(1) := NULL;
   L_diff_2_undist_id_ind  VARCHAR2(1) := NULL;
   L_diff_3_undist_id_ind  VARCHAR2(1) := NULL;
   L_diff_4_undist_id_ind  VARCHAR2(1) := NULL;

BEGIN
   if I_diff_x_no is NULL then
      L_inv_parm := 'I_diff_x_no';
   elsif I_diff_y_no is NULL then
      L_inv_parm := 'I_diff_y_no';
   elsif I_dist_uom_type is NULL then
      L_inv_parm := 'I_dist_uom_type';
   elsif I_dist_uom is NULL then
      L_inv_parm := 'I_dist_uom';
   elsif I_standard_uom is NULL then
      L_inv_parm := 'I_standard_uom';
   elsif I_origin_country_id is NULL then
      L_inv_parm := 'I_origin_country_id';
   elsif I_uop is NULL then
      L_inv_parm := 'I_uop';
   elsif I_supp_pack_size is NULL then
      L_inv_parm := 'I_supp_pack_size';
   elsif I_item_parent is NULL then
      L_inv_parm := 'I_item_parent';
   elsif I_order_no is NULL then
      L_inv_parm := 'I_order_no';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if PO_DIFF_MATRIX_SQL.UPDATE_REDIST_ORDER_DIFF(O_error_message,
                                                  L_diff_1_statement,
                                                  L_diff_2_statement,
                                                  L_diff_3_statement,
                                                  L_diff_4_statement,
                                                  L_diff_1_undist_id_ind,
                                                  L_diff_2_undist_id_ind,
                                                  L_diff_3_undist_id_ind,
                                                  L_diff_4_undist_id_ind,
                                                  I_diff_x_no,
                                                  I_diff_y_no,
                                                  I_diff_z_no,
                                                  I_item_parent,
                                                  I_order_no,
                                                  I_dist_uom_type,
                                                  I_dist_uom,
                                                  I_uop) = FALSE then
      return FALSE;
   end if;

   -- redistribution will always be done with qty values
   L_dist_qty_string := ' diff_dist_matrix.qty ';

   if I_dist_uom_type = 'C' then
      -- Case can't be a SUOM, so if the UOD is Case, the UOP must be Case
      L_wksht_qty_string := 'ROUND('||L_dist_qty_string||') ';
      L_calc_qty_string := '('||L_dist_qty_string||' * '||I_supp_pack_size||') ';

      -- since dirtribution UOM is Case, the wksht qty that has been rounded to integer will result
      -- in a value that is already rounded to pack. This value can be multiplied by
      -- supplier pack size and placed in act_qty since no further rounding to pack is necessary.
      L_act_qty_string  := '('||L_wksht_qty_string||' * '||I_supp_pack_size||') ';
   else
      -- if UOP is case or eaches, populate wksht_qty that user will see with an integer value.
      -- if UOP is any other valid SUOM (e.g. pounds), leave value as is (may contain up to 4 decimal places.)

      if I_dist_uom = I_uop then
         -- UOD and UOP are both SUOM. No pack size conversion needed.
         -- if SUOM is eaches, round qty to integer
         if I_dist_uom_type = 'E' then
            L_wksht_qty_string := 'ROUND('||L_dist_qty_string||') ';
         else
            L_wksht_qty_string := L_dist_qty_string;
         end if;
      elsif I_dist_uom != I_uop then
         -- divide distribution qty by pack size to convert into Case UOP. Use
         -- raw distro qty and oracle ROUND to ensure proper rounding to integer
         -- as supp pack size could be a decimal value.
         L_wksht_qty_string := 'ROUND('||L_dist_qty_string||' / '||I_supp_pack_size||') ';
      end if;

      -- Insert raw qty into the calc_qty and act_qty columns because act_qty
      -- will eventually be rounded to pack size.
      L_calc_qty_string  := L_dist_qty_string;
      L_act_qty_string   := L_dist_qty_string;
   end if;

   L_var_qty_string := '((('||L_act_qty_string ||'-'||L_calc_qty_string ||') * 100) / '||L_calc_qty_string||')';
   L_statement := 'insert into ordloc_wksht(order_no, '||
                                            'item_parent, '||
                                            'diff_1, '||
                                            'diff_2, '||
                                            'diff_3, '||
                                            'diff_4, '||
                                            'calc_qty, '||
                                            'act_qty, '||
                                            'standard_uom, '||
                                            'variance_qty, '||
                                            'origin_country_id, '||
                                            'wksht_qty, '||
                                            'uop, '||
                                            'supp_pack_size) '||
                                    'select '||I_order_no||', '||
                                             I_item_parent||', '||
                                             L_diff_1_statement||', '||
                                             L_diff_2_statement||', '||
                                             L_diff_3_statement||', '||
                                             L_diff_4_statement||', '||
                                             L_calc_qty_string ||', ' ||
                                             L_act_qty_string ||', ' ||
                                             ''''||I_standard_uom||''', '||
                                             L_var_qty_string ||', ' ||
                                             ''''||I_origin_country_id||''', '||
                                             L_wksht_qty_string||', ' ||
                                             ''''||I_uop||''', ' ||
                                             I_supp_pack_size||
                      ' from diff_dist_matrix '||
                      'where (nvl(diff_dist_matrix.qty, 0) > 0 '||
                       '      or nvl(diff_dist_matrix.pct, 0) > 0) '||
                       ' and not exists (select ''x''  '||
                                         ' from ordloc_wksht ' ||
                                         'where order_no = :l_order_no '||
                                           'and item_parent = :l_item_parent  '||
                                           'and diff_'||I_diff_x_no||' = diff_x '||
                                           'and diff_'||I_diff_y_no||' = diff_y ';

   if I_diff_z_no is not NULL then
      L_statement := L_statement||' and diff_'||I_diff_z_no||' = diff_z ';
   end if;

   -- determine which of the parent items diffs are IDs or were not distributed.

   if L_diff_1_undist_id_ind is not NULL then
      if L_diff_1_undist_id_ind = 'U' then
         L_statement := L_statement||' and diff_1 is NULL ';
      else
         -- ind is 'I'd
         L_statement := L_statement||' and diff_1 = '||L_diff_1_statement||' ';
      end if;
   end if;

   if L_diff_2_undist_id_ind is not NULL then
      if L_diff_2_undist_id_ind = 'U' then
         L_statement := L_statement||' and diff_2 is NULL  ';
      else
         -- ind is 'I'd
         L_statement := L_statement||' and diff_2 = '||L_diff_2_statement||' ';
      end if;
   end if;

   if L_diff_3_undist_id_ind is not NULL then
      if L_diff_3_undist_id_ind = 'U' then
         L_statement := L_statement||' and diff_3 is NULL  ';
      else
         -- ind is 'I'd
         L_statement := L_statement||' and diff_3 = '||L_diff_3_statement||' ';
      end if;
   end if;

   if L_diff_4_undist_id_ind is not NULL then
      if L_diff_4_undist_id_ind = 'U' then
         L_statement := L_statement||' and diff_4 is NULL  ';
      else
         -- ind is 'I'd
         L_statement := L_statement||' and diff_4 = '||L_diff_4_statement||' ';
      end if;
   end if;

   L_statement := L_statement||')';

   EXECUTE IMMEDIATE L_statement USING I_order_no,I_item_parent;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REDIST_TO_ORD_ITEMS;
------------------------------------------------------------------------
FUNCTION UPDATE_REDIST_ORDER_DIFF(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_diff_1_statement      IN OUT VARCHAR2,
                                  O_diff_2_statement      IN OUT VARCHAR2,
                                  O_diff_3_statement      IN OUT VARCHAR2,
                                  O_diff_4_statement      IN OUT VARCHAR2,
                                  O_diff_1_undist_id_ind  IN OUT VARCHAR2,
                                  O_diff_2_undist_id_ind  IN OUT VARCHAR2,
                                  O_diff_3_undist_id_ind  IN OUT VARCHAR2,
                                  O_diff_4_undist_id_ind  IN OUT VARCHAR2,
                                  I_diff_x_no             IN     INTEGER,
                                  I_diff_y_no             IN     INTEGER,
                                  I_diff_z_no             IN     INTEGER,
                                  I_item_parent           IN     ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                                  I_order_no              IN     ORDLOC_WKSHT.ORDER_NO%TYPE,
                                  I_dist_uom_type         IN     VARCHAR2,
                                  I_dist_uom              IN     VARCHAR2,
                                  I_uop                   IN     ORDLOC_WKSHT.UOP%TYPE)

RETURN BOOLEAN IS

   L_inv_parm              VARCHAR2(30);
   L_program               VARCHAR2(100)     := 'PO_DIFF_MATRIX_SQL.UPDATE_REDIST_ORDER_DIFF';
   L_where_clause          VARCHAR2(4000);
   L_calc_qty              ORDLOC_WKSHT.CALC_QTY%TYPE;
   L_wksht_qty             ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_var_qty               ORDLOC_WKSHT.VARIANCE_QTY%TYPE;
   L_act_qty               ORDLOC_WKSHT.ACT_QTY%TYPE;
   TYPE ORD_CURSOR is REF  CURSOR;
   C_UPDATE                ORD_CURSOR;

   L_statement             VARCHAR2(2000);
   L_item_parent           ORDLOC_WKSHT.ITEM_PARENT%TYPE;
   L_diff_1                ORDLOC_WKSHT.DIFF_1%TYPE;
   L_diff_2                ORDLOC_WKSHT.DIFF_2%TYPE;
   L_diff_3                ORDLOC_WKSHT.DIFF_3%TYPE;
   L_diff_4                ORDLOC_WKSHT.DIFF_4%TYPE;
   L_standard_uom          ORDLOC_WKSHT.STANDARD_UOM%TYPE;
   L_supp_pack_size        ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;
   L_multiple              ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;
   L_diff_1_statement      VARCHAR2(100);
   L_diff_2_statement      VARCHAR2(100);
   L_diff_3_statement      VARCHAR2(100);
   L_diff_4_statement      VARCHAR2(100);
   L_diff_id_1             ITEM_MASTER.DIFF_1%TYPE := NULL;
   L_diff_id_2             ITEM_MASTER.DIFF_2%TYPE := NULL;
   L_diff_id_3             ITEM_MASTER.DIFF_3%TYPE := NULL;
   L_diff_id_4             ITEM_MASTER.DIFF_4%TYPE := NULL;

   cursor C_DIFF_IDS is
      select (select diff_id
                from diff_ids
               where diff_id = diff_1),
             (select diff_id
                from diff_ids
               where diff_id = diff_2),
             (select diff_id
                from diff_ids
               where diff_id = diff_3),
             (select diff_id
               from diff_ids
              where diff_id = diff_4)
               from item_master
       where item = I_item_parent;

BEGIN

   -- This function will update the quantities filtered-out records in ordmtxws that have diff values that
   -- match those in the distribution matrix. Note that redistribution is done for a single item_parent
   -- at a time.

   -- I_dist_uom_type will contain:  'C'ase, 'E'aches, or 'S'uom

   if I_diff_x_no is NULL then
      L_inv_parm := 'I_diff_x_no';
   elsif I_diff_y_no is NULL then
      L_inv_parm := 'I_diff_y_no';
   elsif I_item_parent is NULL then
      L_inv_parm := 'I_item_parent';
   elsif I_dist_uom_type is NULL then
      L_inv_parm := 'I_dist_uom_type';
   elsif I_dist_uom is NULL then
      L_inv_parm := 'I_dist_uom';
   elsif I_uop is NULL then
      L_inv_parm := 'I_uop';
   elsif I_order_no is NULL then
      L_inv_parm := 'I_order_no';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   -- do not remove any leading spaces from the strings below
   if I_diff_x_no = 1 then
      L_diff_1_statement := '  = diff_dist_matrix.diff_x ';
   elsif I_diff_x_no = 2 then
      L_diff_2_statement := '  = diff_dist_matrix.diff_x ';
   elsif I_diff_x_no = 3 then
      L_diff_3_statement := '  = diff_dist_matrix.diff_x ';
   elsif I_diff_x_no = 4 then
      L_diff_4_statement := '  = diff_dist_matrix.diff_x ';
   end if;

   if I_diff_y_no = 1 then
      L_diff_1_statement := '  = diff_dist_matrix.diff_y ';
   elsif I_diff_y_no = 2 then
      L_diff_2_statement := '  = diff_dist_matrix.diff_y ';
   elsif I_diff_y_no = 3 then
      L_diff_3_statement := '  = diff_dist_matrix.diff_y ';
   elsif I_diff_y_no = 4 then
      L_diff_4_statement := '  = diff_dist_matrix.diff_y ';
   end if;

   if I_diff_z_no = 1 then
      L_diff_1_statement := '  = diff_dist_matrix.diff_z ';
   elsif I_diff_z_no = 2 then
      L_diff_2_statement := '  = diff_dist_matrix.diff_z ';
   elsif I_diff_z_no = 3 then
      L_diff_3_statement := '  = diff_dist_matrix.diff_z ';
   elsif I_diff_z_no = 4 then
      L_diff_4_statement := '  = diff_dist_matrix.diff_z ';
   end if;

   -- determine if the item parent being redistributed in the matrix
   -- was associated with any diff ids. If so, the diff id should be
   -- inserted into the proper diff_n field on ordloc_wksht.
   open C_DIFF_IDS;
   fetch C_DIFF_IDS into L_diff_id_1,
                         L_diff_id_2,
                         L_diff_id_3,
                         L_diff_id_4;
   close C_DIFF_IDS;

   if L_diff_1_statement is NULL then
      if L_diff_id_1 is NULL then
         -- diff_1 was not distributed in the matrix, will be inserted into ordloc_wksht as null

         L_diff_1_statement := ' is NULL ';
         O_diff_1_undist_id_ind := 'U';
      else
         -- parent item's diff_1 is an ID, the ID will be inserted into ordloc_wksht
         L_diff_1_statement := '  = '''||L_diff_id_1||'''';
         O_diff_1_undist_id_ind := 'I';
      end if;
   end if;

   if L_diff_2_statement is NULL then
      if L_diff_id_2 is NULL then
         -- diff_2 was not distributed in the matrix, will be inserted into ordloc_wksht as null

         L_diff_2_statement := ' is NULL ';
         O_diff_2_undist_id_ind := 'U';
      else
         -- parent item's diff_2 is an ID, the ID will be inserted into ordloc_wksht
         L_diff_2_statement := '  = '''||L_diff_id_2||'''';
         O_diff_2_undist_id_ind := 'I';
      end if;
   end if;

   if L_diff_3_statement is NULL then
      if L_diff_id_3 is NULL then
         -- diff_3 was not distributed in the matrix, will be inserted into ordloc_wksht as null

         L_diff_3_statement := ' is NULL ';
         O_diff_3_undist_id_ind := 'U';
      else
         -- parent item's diff_3 is an ID, the ID will be inserted into ordloc_wksht
         L_diff_3_statement := '  = '''||L_diff_id_3||'''';
         O_diff_3_undist_id_ind := 'I';
      end if;
   end if;

   if L_diff_4_statement is NULL then
      if L_diff_id_4 is NULL then
         -- diff_4 was not distributed in the matrix, will be inserted into ordloc_wksht as null

         L_diff_4_statement := ' is NULL ';
         O_diff_4_undist_id_ind := 'U';
      else
         -- parent item's diff_4 is an ID, the ID will be inserted into ordloc_wksht
         L_diff_4_statement := '  = '''||L_diff_id_4||'''';
         O_diff_4_undist_id_ind := 'I';
      end if;
   end if;

   L_statement := 'select item_parent, diff_1, diff_2, diff_3, diff_4, '||
                        ' standard_uom, wksht_qty, supp_pack_size, ';

   L_statement := L_statement
      || ' nvl(calc_qty, act_qty) + diff_dist_matrix.qty * DECODE('''||I_dist_uom_type||''', ''C'',supp_pack_size,1) ';

   L_statement := L_statement||
                    'from ordloc_wksht, diff_dist_matrix '||
                   'where order_no = :l_order_no '||
         'and item_parent = :l_item_parent '||
         'and store_grade_group_id is NULL '||
         'and store_grade is NULL '||
         'and loc_type is NULL '||
         'and location is NULL '||
         'and nvl(diff_dist_matrix.qty , 0) > 0 '||   -- redistributions will always be done with qtys
         'and diff_1 '||L_diff_1_statement||' '||
         'and diff_2 '||L_diff_2_statement||' '||
         'and diff_3 '||L_diff_3_statement||' '||
         'and diff_4 '||L_diff_4_statement;

  EXECUTE IMMEDIATE L_statement  USING I_order_no,I_item_parent;
  open C_UPDATE for L_statement  USING I_order_no,I_item_parent;

   LOOP

     fetch C_UPDATE into L_item_parent,
                         L_diff_1,
                         L_diff_2,
                         L_diff_3,
                         L_diff_4,
                         L_standard_uom,
                         L_wksht_qty,
                         L_supp_pack_size,
                         L_calc_qty;
      EXIT WHEN C_UPDATE%NOTFOUND;

      if I_uop = L_standard_uom and L_standard_uom != 'EA' then
         -- unit of distribution is SUOM also
         L_wksht_qty := ROUND(L_calc_qty, 4);
         L_act_qty   := L_wksht_qty;
         L_var_qty := ((L_act_qty - L_calc_qty)/L_calc_qty) * 100;
      else
         -- UOP is 'case' or the SUOM 'eaches'
         -- round these numbers to the nearest integer
         if I_uop = L_standard_uom then
            L_multiple := 1;
         else
            -- calc_qty is always stored in SUOM so the packsize is
            -- needed to calculate case qty to be inserted into wksht_qty
            L_multiple := L_supp_pack_size;
         end if;
         ---
         L_wksht_qty    := ROUND(L_calc_qty/L_multiple);
         L_act_qty      := (L_wksht_qty * L_multiple);
         L_var_qty := ((L_act_qty - L_calc_qty) / L_calc_qty) * 100;
      end if;

      update ordloc_wksht
         set act_qty         = L_act_qty,
             calc_qty        = L_calc_qty,
             variance_qty    = L_var_qty,
             wksht_qty       = L_wksht_qty
       where order_no        = I_order_no
         and item_parent     = I_item_parent
         and nvl(diff_1, -1) = nvl(L_diff_1, -1)
         and nvl(diff_2, -1) = nvl(L_diff_2, -1)
         and nvl(diff_3, -1) = nvl(L_diff_3, -1)
         and nvl(diff_4, -1) = nvl(L_diff_4, -1);

   end LOOP;

   O_diff_1_statement := substr(L_diff_1_statement, 4);
   O_diff_2_statement := substr(L_diff_2_statement, 4);
   O_diff_3_statement := substr(L_diff_3_statement, 4);
   O_diff_4_statement := substr(L_diff_4_statement, 4);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_REDIST_ORDER_DIFF;
------------------------------------------------------------------------
FUNCTION VALIDATE_COPY_DIFF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid         IN OUT BOOLEAN,
                            O_diff_desc     IN OUT DIFF_IDS.DIFF_DESC%TYPE,
                            I_diff_id       IN     DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(100)     := 'PO_DIFF_MATRIX_SQL.VALIDATE_COPY_DIFF';
   L_valid            VARCHAR2(1)       := 'N';
   L_diff_type        V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_id_group_ind     DIFF_TYPE.DIFF_TYPE_DESC%TYPE;

   cursor C_VALIDATE is
      select 'Y'
        from diff_z_temp
       where diff_z = I_diff_id;

BEGIN
   if I_diff_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_diff_id',
                                            L_program,
                                            NULL);
         return FALSE;
   end if;
   ---
   open C_VALIDATE;
   fetch C_VALIDATE into L_valid;
   close C_VALIDATE;
   ---
   if L_valid = 'N' then
      O_valid := FALSE;
   else
      O_valid := TRUE;
      if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                O_diff_desc,
                                L_diff_type,
                                L_id_group_ind,
                                I_diff_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_COPY_DIFF;
-----------------------------------------------------------------------------
FUNCTION COPY_DIFF_Z_MATRIX(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_copy_from_diff    IN     DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(100)     := 'PO_DIFF_MATRIX_SQL.COPY_DIFF_Z_MATRIX';
   L_copy_from_x       DIFF_X_TEMP.DIFF_X%TYPE;
   L_copy_from_value   PO_DIFF_MATRIX_TEMP.VALUE1%TYPE;
   L_copy_from_seq     DIFF_X_TEMP.SEQ_NO%TYPE;
   L_copy_from_diff_y  PO_DIFF_MATRIX_TEMP.DIFF_Y%TYPE;
   L_copy_to_x         DIFF_X_TEMP.DIFF_X%TYPE;
   L_copy_to_seq       DIFF_X_TEMP.SEQ_NO%TYPE;
   L_copy_to_diff_z    DIFF_Z_TEMP.DIFF_Z%TYPE;
   ---
   L_fromval_statement VARCHAR2(200);
   L_update_statement  VARCHAR2(300);

   cursor C_COPY_FROM_XS is
      select x.diff_x copy_from_x,
             x.seq_no copy_from_seq,
             m.diff_y
        from diff_x_temp x,
             po_diff_matrix_temp m
       where m.diff_z = I_copy_from_diff
         and m.diff_z = x.diff_z
        order by copy_from_x, copy_from_seq;

   cursor C_COPY_TO_XS is
      select x.diff_x copy_to_x,
             x.seq_no copy_to_seq,
             z.diff_z copy_to_diff_z
        from diff_x_temp x,
             diff_z_temp z
       where z.diff_z = x.diff_z
         and x.diff_x = L_copy_from_x
         and z.copy_ind = 'Y';

   cursor C_GET_ZS is
      select diff_z copy_to_diff
        from diff_z_temp
       where copy_ind = 'Y';

BEGIN
   if I_copy_from_diff is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_copy_from_diff',
                                            L_program,
                                            NULL);
         return FALSE;
   end if;
   ---
   for rec in C_COPY_FROM_XS LOOP
      --- Fetch all copy from information.  cursor loops through each diff_x, seq_no, diff_y value
      L_copy_from_x   := rec.copy_from_x;
      L_copy_from_seq := rec.copy_from_seq;
      L_copy_from_diff_y := rec.diff_y;
      ---
      -- fetch the value in the correct po_diff_matrix_temp column
      L_fromval_statement := 'select value'||L_copy_from_seq
                           ||'  from po_diff_matrix_temp'
                           ||' where diff_z = :l_copy_from_diff '
                           ||'   and diff_y = :l_copy_frm_diff_y ';
      ---
      EXECUTE IMMEDIATE L_fromval_statement INTO L_copy_from_value USING I_copy_from_diff,L_copy_from_diff_y;

      for recs in C_COPY_TO_XS LOOP
         --- Fetch all copy to info.  cursor loops for all diffys for each 'copy to' diffz
         L_copy_to_x  := recs.copy_to_x;
         L_copy_to_seq := recs.copy_to_seq;
         L_copy_to_diff_z := recs.copy_to_diff_z;

         L_update_statement :=  'update po_diff_matrix_temp '
                              ||'   set value'||L_copy_to_seq||' = '||nvl(to_char(L_copy_from_value),'to_number(NULL)')
                              ||' where diff_y = :l_copy_frm_diff_y '
                              ||'   and diff_z = :l_copy_to_dif_z ';

         EXECUTE IMMEDIATE L_update_statement USING L_copy_from_diff_y,L_copy_to_diff_z;

      end LOOP;
   end LOOP;
   ---
   update diff_z_temp
      set copy_ind = 'N';

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END COPY_DIFF_Z_MATRIX;
------------------------------------------------------------------------
FUNCTION CONVERT_QTYS_BY_TYPE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_matrix_qty_type  IN     VARCHAR2,
                              I_z_total          IN     NUMBER,
                              I_z_qty_type       IN     VARCHAR2,
                              I_parent_total_qty IN     NUMBER,
                              I_qty_ind          IN     VARCHAR2)

  RETURN BOOLEAN IS

   L_inv_parm            VARCHAR2(30)           := NULL;
   L_program             VARCHAR2(100)          := 'PO_DIFF_MATRIX_SQL.CONVERT_QTYS_BY_TYPE';
   L_statement           VARCHAR2(2000)         := NULL;
   L_parent_total_qty    NUMBER(20)             := NULL;
   L_current_diff_z      DIFF_Z_TEMP.DIFF_Z%TYPE;
   L_x_count             INTEGER(2);
   L_qty_calc_statement  VARCHAR2(1000);
   L_diff_y              DIFF_IDS.DIFF_ID%TYPE  := NULL;
   L_matrix_total        NUMBER(20)             := NULL;

   TYPE value_record IS RECORD (value DIFF_IDS.DIFF_ID%TYPE);
   TYPE value_table IS TABLE OF value_record INDEX BY BINARY_INTEGER;

   L_val_table  value_table;

   cursor C_GET_DIFF_Z_VALUE is
      select diff_z,
             NVL(value, 0) value
        from diff_z_temp;

   cursor C_GET_X_COUNT is
      select count(*)
        from diff_x_temp
       where diff_z = L_current_diff_z;

   cursor C_GET_DIFF_Y(I_diff_z DIFF_IDS.DIFF_ID%TYPE) is
      select diff_y
        from po_diff_matrix_temp
       where diff_z = I_diff_z;

   cursor C_GET_DIFF_Y_VALUES(I_diff_z DIFF_IDS.DIFF_ID%TYPE) is
      select SUM(NVL(value1, 0) + NVL(value2, 0) + NVL(value3, 0) + NVL(value4, 0) + NVL(value5, 0) +
                 NVL(value6, 0) + NVL(value7, 0) + NVL(value8, 0) + NVL(value9, 0) + NVL(value10, 0) +
                 NVL(value11, 0) + NVL(value12, 0) + NVL(value13, 0) + NVL(value14, 0) + NVL(value15, 0) +
                 NVL(value16, 0) + NVL(value17, 0) + NVL(value18, 0) + NVL(value19, 0) + NVL(value20, 0) +
                 NVL(value21, 0) + NVL(value22, 0) + NVL(value23, 0) + NVL(value24, 0) + NVL(value25, 0) +
                 NVL(value26, 0) + NVL(value27, 0) + NVL(value28, 0) + NVL(value29, 0) + NVL(value30, 0))
        from po_diff_matrix_temp
       where diff_z = I_diff_z;

BEGIN

   if I_matrix_qty_type is NULL then
      L_inv_parm := 'I_matrix_qty_type';
   elsif I_z_qty_type is NULL then
      L_inv_parm := 'I_z_qty_type';
   elsif I_qty_ind is NULL then
      L_inv_parm := 'I_qty_ind';
   elsif I_parent_total_qty is NULL and I_qty_ind = 'Y' then
      L_inv_parm := 'I_parent_total_qty';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_qty_ind = 'Y' then
      L_parent_total_qty := I_parent_total_qty;
   else
      L_parent_total_qty := 1;
   end if;

   FOR diff_z_rec IN C_GET_DIFF_Z_VALUE LOOP

      L_current_diff_z := diff_z_rec.diff_z;

      open C_GET_X_COUNT;
      fetch C_GET_X_COUNT into L_x_count;
      close C_GET_X_COUNT;

      open C_GET_DIFF_Y_VALUES(diff_z_rec.diff_z);
      fetch C_GET_DIFF_Y_VALUES into L_matrix_total;
      close C_GET_DIFF_Y_VALUES;

      FOR diff_y_rec IN C_GET_DIFF_Y(diff_z_rec.diff_z) LOOP

         FOR counter IN 1 .. L_x_count LOOP
            L_qty_calc_statement := NULL;

            -- set qty calculation for x and y axis based
            if I_matrix_qty_type = 'R' then
               L_qty_calc_statement := L_parent_total_qty||' * '||'(p.value'||counter||' / '||L_matrix_total||')';
            elsif I_matrix_qty_type = 'P' then
               L_qty_calc_statement := L_parent_total_qty||' * '||'(p.value'||counter||' / 100)';
            end if;

            if I_z_qty_type = 'Q' then
               L_qty_calc_statement :=  L_qty_calc_statement||' * ('||diff_z_rec.value||') ';
            elsif I_z_qty_type = 'R' then
               L_qty_calc_statement :=  L_qty_calc_statement||' * ('||diff_z_rec.value||' / '||I_z_total||') ';
            elsif I_z_qty_type = 'P' then
               L_qty_calc_statement :=  L_qty_calc_statement||' * ('||diff_z_rec.value||' / 100) ';
            end if;

            L_statement := 'update po_diff_matrix_temp p'||
                             ' set p.value'||counter||
                                 ' = decode( ROUND('||L_qty_calc_statement||'),0,1,ROUND('||L_qty_calc_statement||'))'||
                           ' where diff_z = :l_diff_z '||
                             ' and diff_y = :l_diff_y ';

            EXECUTE IMMEDIATE L_statement USING diff_z_rec.diff_z,diff_y_rec.diff_y;

         end LOOP; -- end counter loop
      end LOOP;
   end LOOP; -- end z loop
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONVERT_QTYS_BY_TYPE;
------------------------------------------------------------------------
FUNCTION REPOP_MATRIX(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_diff_x_no             IN     NUMBER,
                      I_diff_y_no             IN     NUMBER,
                      I_diff_z_no             IN     NUMBER,
                      I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                      I_item_parent           IN     ITEM_MASTER.ITEM%TYPE,
                      I_where_clause          IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                      I_dist_uom_type         IN     VARCHAR2)
   return BOOLEAN is

   TYPE QTY_CURSOR is REF CURSOR;
   C_Z_QTY      QTY_CURSOR;
   C_XY_QTY     QTY_CURSOR;

   L_inv_parm       VARCHAR2(30) := NULL;
   L_program        VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.REPOP_MATRIX';
   L_z_qty          ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_xy_qty         ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_diff_z         DIFF_IDS.DIFF_ID%TYPE;
   L_diff_y         DIFF_IDS.DIFF_ID%TYPE;
   L_diff_x         DIFF_IDS.DIFF_ID%TYPE;
   L_seq_no         DIFF_X_TEMP.SEQ_NO%TYPE;
   L_statement      VARCHAR2(4000);
   L_statement2     VARCHAR2(4000);
   L_statement3     VARCHAR2(4000);
   L_where_clause   FILTER_TEMP.WHERE_CLAUSE%TYPE;
   L_qty_text       VARCHAR2(100);

   cursor C_SEQ_NO is
      select seq_no
        from diff_x_temp
       where diff_x = L_diff_x
         and NVL(diff_z, '-999') = NVL(L_diff_z, '-999');

BEGIN

   if I_diff_x_no is NULL then
      L_inv_parm := 'I_diff_x_no';
   elsif I_diff_y_no is NULL then
      L_inv_parm := 'I_diff_y_no';
   elsif I_order_no is NULL then
      L_inv_parm := 'I_order_no';
   elsif I_item_parent is NULL then
      L_inv_parm := 'I_item_parent';
   elsif I_dist_uom_type is NULL then
      L_inv_parm := 'I_dist_uom_type';
   end if;
   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_where_clause is NOT NULL then
      L_where_clause := ' and ' || I_where_clause;
   else
      L_where_clause := NULL;
   end if;

   if I_dist_uom_type = 'C' then
      L_qty_text := 'wksht_qty';
   else
      L_qty_text := 'act_qty';
   end if;

   if I_diff_z_no is NOT NULL then
      L_statement := 'select sum('||L_qty_text||'), diff_'||I_diff_z_no
                   ||' from ordloc_wksht'
                   ||' where item_parent = :I_item_parent'
                   ||' and order_no = :I_order_no' ||
                        L_where_clause
                   ||' group by diff_'||I_diff_z_no;

      EXECUTE IMMEDIATE L_statement USING I_item_parent, I_order_no;
      open C_Z_QTY for L_statement USING I_item_parent, I_order_no;

      LOOP
         fetch C_Z_QTY into L_z_qty, L_diff_z;

         EXIT WHEN C_Z_QTY%NOTFOUND;

         update diff_z_temp set value = L_z_qty
          where diff_z = L_diff_z;
      end LOOP;

      close C_Z_QTY;

      L_statement2 := 'select sum('||L_qty_text||'), diff_'||I_diff_z_no||', diff_'||I_diff_y_no||', diff_'||I_diff_x_no
                    ||' from ordloc_wksht'
                    ||' where item_parent = :I_item_parent'
                    ||' and order_no = :I_order_no' ||
                        L_where_clause
                    ||' group by diff_'||I_diff_z_no||', diff_'||I_diff_y_no||', diff_'||I_diff_x_no
                    ||' order by diff_'||I_diff_x_no||', diff_'||I_diff_z_no;

      EXECUTE IMMEDIATE L_statement2 USING I_item_parent, I_order_no;
      open C_XY_QTY for L_statement2 USING I_item_parent, I_order_no;

      LOOP
         fetch C_XY_QTY into L_xy_qty, L_diff_z, L_diff_y, L_diff_x;

         EXIT WHEN C_XY_QTY%NOTFOUND;

         open C_SEQ_NO;
         fetch C_SEQ_NO into L_seq_no;
         close C_SEQ_NO;

         L_statement3 := 'update po_diff_matrix_temp'
                       ||' set value'||L_seq_no||' = :L_xy_qty'
                       ||' where diff_y = :L_diff_y'
                       ||' and diff_z = :L_diff_z';

         EXECUTE IMMEDIATE L_statement3 USING L_xy_qty, L_diff_y, L_diff_z;
      end LOOP;

      close C_XY_QTY;
   else
      L_statement2 := 'select sum('||L_qty_text||'), diff_'||I_diff_y_no||', diff_'||I_diff_x_no
                    ||' from ordloc_wksht'
                    ||' where item_parent = :I_item_parent'
                    ||' and order_no = :I_order_no' ||
                        L_where_clause
                    ||' group by diff_'||I_diff_y_no||', diff_'||I_diff_x_no
                    ||' order by diff_'||I_diff_x_no;

      EXECUTE IMMEDIATE L_statement2 USING I_item_parent, I_order_no;
      open C_XY_QTY for L_statement2 USING I_item_parent, I_order_no;

      LOOP
         fetch C_XY_QTY into L_xy_qty, L_diff_y, L_diff_x;

         EXIT WHEN C_XY_QTY%NOTFOUND;

         open C_SEQ_NO;
         fetch C_SEQ_NO into L_seq_no;
         close C_SEQ_NO;

         L_statement3 := 'update po_diff_matrix_temp'
                       ||' set value'||L_seq_no||' = :L_xy_qty'
                       ||' where diff_y = :L_diff_y';

         EXECUTE IMMEDIATE L_statement3 USING L_xy_qty, L_diff_y;
      end LOOP;

      close C_XY_QTY;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REPOP_MATRIX;
------------------------------------------------------------------------
FUNCTION CLEAR_WKSHT_POST_REDIST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_standard_uom      IN OUT ORDLOC_WKSHT.STANDARD_UOM%TYPE,
                                 O_origin_country_id IN OUT ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE,
                                 O_supp_pack_size    IN OUT ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE,
                                 I_item_parent       IN     ITEM_MASTER.ITEM%TYPE,
                                 I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                 I_where_clause      IN     FILTER_TEMP.WHERE_CLAUSE%TYPE)
   return BOOLEAN is

   L_inv_parm            VARCHAR2(30) := NULL;
   L_program             VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.CLEAR_WKSHT_POST_REDIST';
   L_info_statement      VARCHAR2(4000);
   L_standard_uom        ORDLOC_WKSHT.STANDARD_UOM%TYPE;
   L_origin_country_id   ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE;
   L_uop                 ORDLOC_WKSHT.UOP%TYPE;
   L_supp_pack_size      ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;
   L_statement           VARCHAR2(4000);
   L_where_clause        FILTER_TEMP.WHERE_CLAUSE%TYPE;

BEGIN

   if I_item_parent is NULL then
      L_inv_parm := 'I_item_parent';
   elsif I_order_no is NULL then
      L_inv_parm := 'I_order_no';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- when redistributing an item, the po diff matrix will be back-populated using diffs
   -- and qtys from ordloc_wksht. All ordloc_wksht records (within the passed-in where clause) for
   -- the item parent being redistributed will then be deleted from ordloc_wksht.

   if I_where_clause is NOT NULL then
      L_where_clause := ' and ' || I_where_clause;
   end if;

   -- get info that will be inserted back into ordloc_wksht after previously distributed records are deleted.
   L_info_statement := 'select distinct standard_uom, origin_country_id, supp_pack_size '||
                       '  from ordloc_wksht '||
                       ' where order_no = :I_order_no '||
                       '   and item_parent = :I_item_parent'||
                       L_where_clause;

   EXECUTE IMMEDIATE L_info_statement INTO O_standard_uom,
                                           O_origin_country_id,
                                           O_supp_pack_size
                                     USING I_order_no,
                                           I_item_parent;

   -- delete previously distributed items
   L_statement := 'delete from ordloc_wksht'
                ||' where order_no = :I_order_no'
                ||'   and item_parent = :I_item_parent'
                || L_where_clause;

   EXECUTE IMMEDIATE L_statement USING I_order_no, I_item_parent;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'PO_DIFF_MATRIX_SQL.CLEAR_WKSHT_POST_REDIST',
                                            to_char(SQLCODE));
      return FALSE;
END CLEAR_WKSHT_POST_REDIST;
------------------------------------------------------------------------
FUNCTION APPLY_RATIO(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_diff_x_ratio       IN OUT DIFF_X_RATIO_TAB,
                     I_diff_ratio_id      IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE,
                     I_store              IN     DIFF_RATIO_DETAIL.STORE%TYPE,
                     I_diff_x_no          IN     NUMBER,
                     I_diff_y_no          IN     NUMBER,
                     I_diff_z_no          IN     NUMBER,
                     I_diff_z_id          IN     DIFF_IDS.DIFF_ID%TYPE,
                     I_range_x_no         IN     NUMBER,
                     I_range_y_no         IN     NUMBER,
                     I_range_z_no         IN     NUMBER,
                     I_range_id           IN     DIFF_RANGE_DETAIL.DIFF_RANGE%TYPE)

   return BOOLEAN is

   TYPE QTY_CURSOR is REF CURSOR;
   C_Z_QTY      QTY_CURSOR;
   C_XY_QTY     QTY_CURSOR;

   L_program          VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.APPLY_RATIO';
   L_ratio_group_1    DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE;
   L_ratio_group_2    DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE;
   L_ratio_group_3    DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE;
   L_z_qty            DIFF_RATIO_DETAIL.PCT%TYPE;
   L_xy_qty           DIFF_RATIO_DETAIL.PCT%TYPE;
   L_diff_z           DIFF_IDS.DIFF_ID%TYPE;
   L_diff_y           DIFF_IDS.DIFF_ID%TYPE;
   L_diff_x           DIFF_IDS.DIFF_ID%TYPE;
   L_statement        VARCHAR2(4000);
   L_statement2       VARCHAR2(4000);
   L_statement3       VARCHAR2(4000);
   L_seq_no           DIFF_X_TEMP.SEQ_NO%TYPE;
   L_loop_counter     NUMBER  := 1;

   cursor C_SEQ_NO is
      select seq_no, diff_z
        from diff_x_temp
       where diff_x = L_diff_x
         and (diff_z = L_diff_z or L_diff_z is NULL);

   cursor C_SEQ_NO2 is
      select seq_no, diff_z
        from diff_x_temp
       where diff_x = L_diff_x;

   cursor C_X is
      select dx.diff_x, dx.seq_no, drd.pct
        from diff_x_temp dx, diff_ratio_detail drd
       where NVL(dx.diff_z, '-999') = NVL(I_diff_z_id, '-999')
         and NVL(drd.store, -999) = NVL(I_store, -999)
         and dx.diff_x = drd.diff_1
         and drd.diff_ratio_id = I_diff_ratio_id
       order by dx.seq_no;

BEGIN

   if I_diff_ratio_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_diff_ratio_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_diff_z_no is NOT NULL then
      --- Applying a ratio will overwrite all values, therefore, we'll set them to NULL
      update diff_z_temp
         set value = NULL;

      L_statement := 'select sum(pct), diff_'||I_diff_z_no
                   ||' from diff_ratio_detail'
                   ||' where diff_ratio_id = :I_diff_ratio_id'
                   ||' and NVL(store, -999) = NVL(:I_store, -999)'
                   ||' group by diff_'||I_diff_z_no;

      EXECUTE IMMEDIATE L_statement USING I_diff_ratio_id, I_store;
      open C_Z_QTY for L_statement USING I_diff_ratio_id, I_store;

      LOOP
         fetch C_Z_QTY into L_z_qty, L_diff_z;

         EXIT when C_Z_QTY%NOTFOUND;

         update diff_z_temp
            set value = L_z_qty
          where diff_z = L_diff_z;
      end LOOP;
      close C_Z_QTY;

      if I_diff_x_no is NOT NULL then
         --- if I_diff_x_no is not null then the ratio affects all three diffs
         --- because a ratio can't affect just z and y or z and x

         --- Because it's affecting the matrix we'll overwrite all values currently there with NULL;
         update po_diff_matrix_temp
            set value1 = NULL,
                value2 = NULL,
                value3 = NULL,
                value4 = NULL,
                value5 = NULL,
                value6 = NULL,
                value7 = NULL,
                value8 = NULL,
                value9 = NULL,
                value10 = NULL,
                value11 = NULL,
                value12 = NULL,
                value13 = NULL,
                value14 = NULL,
                value15 = NULL,
                value16 = NULL,
                value17 = NULL,
                value18 = NULL,
                value19 = NULL,
                value20 = NULL,
                value21 = NULL,
                value22 = NULL,
                value23 = NULL,
                value24 = NULL,
                value25 = NULL,
                value26 = NULL,
                value27 = NULL,
                value28 = NULL,
                value29 = NULL,
                value30 = NULL;

         L_statement2 := 'select rt.pct, rt.diff_'||I_diff_z_no||', rt.diff_'||I_diff_y_no||', rt.diff_'||I_diff_x_no
                       ||' from diff_ratio_detail rt'
                       ||' where rt.diff_ratio_id = :I_diff_ratio_id'
                       ||' and NVL(rt.store, -999) = NVL(:I_store, -999)';

         if I_range_id is NOT NULL then
            L_statement2 := L_statement2
                          ||' and exists (select ''x'''
                                      ||' from diff_range_detail rg'
                                      ||' where rg.diff_range = :I_range_id'
                                      ||' and rt.diff_'||I_diff_x_no||' = rg.diff_'||I_range_x_no
                                      ||' and rt.diff_'||I_diff_y_no||' = rg.diff_'||I_range_y_no;

            if I_range_z_no is NOT NULL then
               L_statement2 := L_statement2
                               || ' and rt.diff_'||I_diff_z_no||'= rg.diff_'||I_range_z_no||')';
            else
               L_statement2 := L_statement2||')';
            end if;
         end if;

         if I_range_id is NOT NULL then
            EXECUTE IMMEDIATE L_statement2 USING I_diff_ratio_id, I_store, I_range_id;
            open C_XY_QTY for L_statement2 USING I_diff_ratio_id, I_store, I_range_id;
         else
            EXECUTE IMMEDIATE L_statement2 USING I_diff_ratio_id, I_store;
            open C_XY_QTY for L_statement2 USING I_diff_ratio_id, I_store;
         end if;
         LOOP
            fetch C_XY_QTY into L_xy_qty, L_diff_z, L_diff_y, L_diff_x;

            EXIT when C_XY_QTY%NOTFOUND;

            open C_SEQ_NO;
            fetch C_SEQ_NO into L_seq_no, L_diff_z;
            close C_SEQ_NO;

            L_statement3 := 'update po_diff_matrix_temp'
                          ||' set value'||L_seq_no||' = :L_xy_qty'
                          ||' where diff_y = :L_diff_y'
                          ||' and diff_z = :L_diff_z';

            EXECUTE IMMEDIATE L_statement3 USING L_xy_qty, L_diff_y, L_diff_z;
         end LOOP;

         close C_XY_QTY;
      end if;
   else  --- Not affecting Z
      if I_diff_x_no is not NULL then
         if I_diff_y_no is not NULL then  --- affecting the matrix as a whole
            --- Because it's affecting the matrix we'll overwrite all values currently there with NULL;
            update po_diff_matrix_temp
               set value1 = NULL,
                   value2 = NULL,
                   value3 = NULL,
                   value4 = NULL,
                   value5 = NULL,
                   value6 = NULL,
                   value7 = NULL,
                   value8 = NULL,
                   value9 = NULL,
                   value10 = NULL,
                   value11 = NULL,
                   value12 = NULL,
                   value13 = NULL,
                   value14 = NULL,
                   value15 = NULL,
                   value16 = NULL,
                   value17 = NULL,
                   value18 = NULL,
                   value19 = NULL,
                   value20 = NULL,
                   value21 = NULL,
                   value22 = NULL,
                   value23 = NULL,
                   value24 = NULL,
                   value25 = NULL,
                   value26 = NULL,
                   value27 = NULL,
                   value28 = NULL,
                   value29 = NULL,
                   value30 = NULL;

            L_statement2 := 'select pct, diff_'||I_diff_y_no||', diff_'||I_diff_x_no
                          ||' from diff_ratio_detail'
                          ||' where diff_ratio_id = :I_diff_ratio_id'
                          ||' and NVL(store, -999) = NVL(:I_store, -999)';

            EXECUTE IMMEDIATE L_statement2 USING I_diff_ratio_id, I_store;
            open C_XY_QTY for L_statement2 USING I_diff_ratio_id, I_store;

            LOOP
               fetch C_XY_QTY into L_xy_qty, L_diff_y, L_diff_x;

               EXIT when C_XY_QTY%NOTFOUND;

               for rec in C_SEQ_NO2 LOOP

                  L_statement3 := 'update po_diff_matrix_temp dmt'
                                ||' set dmt.value'||rec.seq_no||' = :L_xy_qty'
                                ||' where dmt.diff_y = :L_diff_y'
                                ||' and (dmt.diff_z = :L_diff_z'
                                ||' or :L_diff_z is NULL)';

                  if I_range_id is NOT NULL then
                     L_statement3 := L_statement3
                                   ||' and exists (select ''x'''
                                              ||' from diff_range_detail drd'
                                              ||' where drd.diff_range = :I_range_id'
                                              ||' and dmt.diff_y = drd.diff_'||I_range_y_no
                                              ||' and :L_diff_x = drd.diff_'||I_range_x_no;

                     if I_range_z_no is NOT NULL then
                        L_statement3 := L_statement3
                                      ||' and dmt.diff_z = drd.diff_'||I_range_z_no||')';
                     else
                        L_statement3 := L_statement || ')';
                     end if;
                  end if;
                  ---
                  if I_range_id is NOT NULL then
                     EXECUTE IMMEDIATE L_statement3 USING L_xy_qty, L_diff_y, rec.diff_z, rec.diff_z, I_range_id, L_diff_x;
                  else
                     EXECUTE IMMEDIATE L_statement3 USING L_xy_qty, L_diff_y, rec.diff_z, rec.diff_z;
                  end if;
               end LOOP;
            end LOOP;

            close C_XY_QTY;
         else -- neither y nor z are affect, just x
            open C_X;
            LOOP
               --- because the X dist. fields aren't on the database we will
               --- populate a pl/sql which the form will use to fill the fields.
               fetch C_X into O_diff_x_ratio(L_loop_counter);
               exit when C_X%NOTFOUND;
               L_loop_counter := L_loop_counter + 1;
            end LOOP;
            close C_X;
         end if;
      else  --- x and z are NULL, only y is affected.
         update po_diff_matrix_temp pdm
            set summary_y = (select pct
                               from diff_ratio_detail
                              where diff_1 = pdm.diff_y
                                and NVL(store, -999) = NVL(I_store, -999)
                                and diff_ratio_id = I_diff_ratio_id);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END APPLY_RATIO;
-------------------------------------------------------------------------------------------
FUNCTION REFRESH_MATRIX (O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_diff_z         IN     DIFF_Z_TEMP.DIFF_Z%TYPE,
                         I_x_count        IN     NUMBER)
   RETURN BOOLEAN IS
   L_program          VARCHAR2(100)     := 'PO_DIFF_MATRIX_SQL.REFRESH_MATRIX';
   L_statement        VARCHAR2(300)     := NULL;
BEGIN
   for i in 1 .. nvl(I_x_count,30) LOOP
      L_statement := 'update po_diff_matrix_temp '
                   ||'   set value'||i||' = NULL';

      if I_diff_z is not NULL then
         L_statement := L_statement
                    ||' where diff_z = :l_diff_z ';
         EXECUTE IMMEDIATE L_statement USING I_diff_z;
      else
         EXECUTE IMMEDIATE L_statement;
      end if;
      ---
   end LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_MATRIX;
-------------------------------------------------------------------------------
FUNCTION DELETE_TEMP_AND_WKSHT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_diff_x_no     IN     INTEGER,
                               I_diff_y_no     IN     INTEGER,
                               I_diff_z_no     IN     INTEGER,
                               I_order_no      IN     ORDLOC_WKSHT.ORDER_NO%TYPE,
                               I_where_clause  IN     FILTER_TEMP.WHERE_CLAUSE%TYPE)
   return BOOLEAN is

   L_inv_parm   VARCHAR2(30)      := NULL;
   L_program    VARCHAR2(100)     := 'PO_DIFF_MATRIX_SQL.DELETE_TEMP_AND_WKSHT';
   L_statement      VARCHAR2(4000);
   L_where_clause   VARCHAR2(4000);

BEGIN
   if I_diff_x_no is NULL then
      L_inv_parm := 'I_diff_x_no';
   elsif I_diff_y_no is NULL then
      L_inv_parm := 'I_diff_y_no';
   elsif I_order_no is NULL then
      L_inv_parm := 'I_order_no';
   end if;
   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- clear all temp tables used by podiffmx. No locking cursors needed
   delete from diff_x_temp;
   delete from po_diff_matrix_temp;
   delete from diff_z_temp;
   delete from diff_dist_matrix;

   if I_where_clause is NOT NULL then
      L_where_clause := ' and ' || I_where_clause;
   end if;

   -- delete the 'base' record from ordloc_wksht that was distributed using podiffmx
   -- no locking cursor is needed as the same order cannot be edited by >1 user

   L_statement := 'delete from ordloc_wksht '||
                  ' where order_no = :I_order_no '||
                  '   and diff_'||I_diff_x_no||' is NULL '||
                  '   and diff_'||I_diff_y_no||' is NULL ';

   if I_diff_z_no is not NULL then
      L_statement := L_statement||' and diff_'||I_diff_z_no||' is NULL ';
   end if;

   L_statement := L_statement||L_where_clause;

   EXECUTE IMMEDIATE L_statement USING I_order_no;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END DELETE_TEMP_AND_WKSHT;
-------------------------------------------------------------------------------
FUNCTION ORPHAN_VALUES_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist         IN OUT BOOLEAN,
                             O_z_seq_no      IN OUT DIFF_Z_TEMP.SEQ_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.ORPHAN_VALUES_EXIST';

   -- subquery in cursor ensures that 1st 'offending' record is found. Otherwise records from the
   -- 1st half of the union will always be returned first.

   cursor C_ORPHANS is
      select seq_no from
      (select d.seq_no
         from po_diff_matrix_temp p,
              diff_z_temp d
        where d.diff_z = p.diff_z
          and nvl(d.value, 0) > 0
        group by d.seq_no
       having NVL(sum(nvl(p.value1,0)  + nvl(p.value2,0)  + nvl(p.value3,0)  + nvl(p.value4,0)  + nvl(p.value5,0)  +
                      nvl(p.value6,0)  + nvl(p.value7,0)  + nvl(p.value8,0)  + nvl(p.value9,0)  + nvl(p.value10,0) +
                      nvl(p.value11,0) + nvl(p.value12,0) + nvl(p.value13,0) + nvl(p.value14,0) + nvl(p.value15,0) +
                      nvl(p.value16,0) + nvl(p.value17,0) + nvl(p.value18,0) + nvl(p.value19,0) + nvl(p.value20,0) +
                      nvl(p.value21,0) + nvl(p.value22,0) + nvl(p.value23,0) + nvl(p.value24,0) + nvl(p.value25,0) +
                      nvl(p.value26,0) + nvl(p.value27,0) + nvl(p.value28,0) + nvl(p.value29,0) + nvl(p.value30,0)), 0) = 0
    UNION ALL
       select d.seq_no
         from diff_z_temp d,
              po_diff_matrix_temp p
        where p.diff_z = d.diff_z
          and NVL(d.value, 0) = 0
        group by d.seq_no
       having NVL(sum(nvl(p.value1,0)  + nvl(p.value2,0)  + nvl(p.value3,0)  + nvl(p.value4,0)  + nvl(p.value5,0)  +
                      nvl(p.value6,0)  + nvl(p.value7,0)  + nvl(p.value8,0)  + nvl(p.value9,0)  + nvl(p.value10,0) +
                      nvl(p.value11,0) + nvl(p.value12,0) + nvl(p.value13,0) + nvl(p.value14,0) + nvl(p.value15,0) +
                      nvl(p.value16,0) + nvl(p.value17,0) + nvl(p.value18,0) + nvl(p.value19,0) + nvl(p.value20,0) +
                      nvl(p.value21,0) + nvl(p.value22,0) + nvl(p.value23,0) + nvl(p.value24,0) + nvl(p.value25,0) +
                      nvl(p.value26,0) + nvl(p.value27,0) + nvl(p.value28,0) + nvl(p.value29,0) + nvl(p.value30,0)), 0) > 0)
        order by 1;

BEGIN

   open C_ORPHANS;
   fetch C_ORPHANS into O_z_seq_no;
   if C_ORPHANS%FOUND then
      O_exist := TRUE;
   else
      O_exist := FALSE;
   end if;
   close C_ORPHANS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END ORPHAN_VALUES_EXIST;
-------------------------------------------------------------------------------
FUNCTION EMPTY_MATRIX(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists        IN OUT BOOLEAN)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.EMPTY_MATRIX';
   L_matrix_sum   PO_DIFF_MATRIX_TEMP.VALUE1%TYPE;

   cursor C_EMPTY_MATRIX is
      select NVL(sum(nvl(value1,0)  + nvl(value2,0)  + nvl(value3,0)  + nvl(value4,0)  + nvl(value5,0)  +
                 nvl(value6,0)  + nvl(value7,0)  + nvl(value8,0)  + nvl(value9,0)  + nvl(value10,0) +
                 nvl(value11,0) + nvl(value12,0) + nvl(value13,0) + nvl(value14,0) + nvl(value15,0) +
                 nvl(value16,0) + nvl(value17,0) + nvl(value18,0) + nvl(value19,0) + nvl(value20,0) +
                 nvl(value21,0) + nvl(value22,0) + nvl(value23,0) + nvl(value24,0) + nvl(value25,0) +
                 nvl(value26,0) + nvl(value27,0) + nvl(value28,0) + nvl(value29,0) + nvl(value30,0)), 0)
        from po_diff_matrix_temp;

BEGIN

   open C_EMPTY_MATRIX;
   fetch C_EMPTY_MATRIX into L_matrix_sum;
   close C_EMPTY_MATRIX;
   if L_matrix_sum = 0 then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
END EMPTY_MATRIX;
-------------------------------------------------------------------------------
FUNCTION CHECK_PCT_TOTAL(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_inv_total_exist IN OUT BOOLEAN,
                         O_z_seq_no        IN OUT DIFF_Z_TEMP.SEQ_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.CHECK_PCT_TOTAL';

   -- cursor will determine if any x/y matrices do not add up to a total of 100.

   cursor C_INV_PCT_TOTAL is
      select d.seq_no
        from po_diff_matrix_temp p,
             diff_z_temp d
       where d.diff_z = p.diff_z
         and nvl(d.value, 0) > 0
       group by d.seq_no
      having NVL(sum(nvl(p.value1,0)  + nvl(p.value2,0)  + nvl(p.value3,0)  + nvl(p.value4,0)  + nvl(p.value5,0)  +
                     nvl(p.value6,0)  + nvl(p.value7,0)  + nvl(p.value8,0)  + nvl(p.value9,0)  + nvl(p.value10,0) +
                     nvl(p.value11,0) + nvl(p.value12,0) + nvl(p.value13,0) + nvl(p.value14,0) + nvl(p.value15,0) +
                     nvl(p.value16,0) + nvl(p.value17,0) + nvl(p.value18,0) + nvl(p.value19,0) + nvl(p.value20,0) +
                     nvl(p.value21,0) + nvl(p.value22,0) + nvl(p.value23,0) + nvl(p.value24,0) + nvl(p.value25,0) +
                     nvl(p.value26,0) + nvl(p.value27,0) + nvl(p.value28,0) + nvl(p.value29,0) + nvl(p.value30,0)), 0) != 100;

BEGIN

   open C_INV_PCT_TOTAL;
   fetch C_INV_PCT_TOTAL into O_z_seq_no;
   if C_INV_PCT_TOTAL%FOUND then
      O_inv_total_exist := TRUE;
   else
      O_inv_total_exist := FALSE;
   end if;
   close C_INV_PCT_TOTAL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END CHECK_PCT_TOTAL;
--------------------------------------------------------------------------
FUNCTION GET_LEAST_VALUE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_least_value    OUT    DIFF_Z_TEMP.VALUE%TYPE,
                         I_diff_z         IN     DIFF_Z_TEMP.DIFF_Z%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(100)          := 'PO_DIFF_MATRIX_SQL.GET_LEAST_VALUE';
   L_least_value   po_diff_matrix_temp.value1%TYPE ;
   L_large_number  po_diff_matrix_temp.value1%TYPE  := 99999999.9999;

   cursor C_GET_LEAST_VALUE  is
     select MIN(least_value)
       from (select LEAST(DECODE(value1,NULL,L_large_number,0,L_large_number,value1),
                          DECODE(value2,NULL,L_large_number,0,L_large_number,value2),
                          DECODE(value3,NULL,L_large_number,0,L_large_number,value3),
                          DECODE(value4,NULL,L_large_number,0,L_large_number,value4),
                          DECODE(value5,NULL,L_large_number,0,L_large_number,value5),
                          DECODE(value6,NULL,L_large_number,0,L_large_number,value6),
                          DECODE(value7,NULL,L_large_number,0,L_large_number,value7),
                          DECODE(value8,NULL,L_large_number,0,L_large_number,value8),
                          DECODE(value9,NULL,L_large_number,0,L_large_number,value9),
                          DECODE(value10,NULL,L_large_number,0,L_large_number,value10),
                          DECODE(value11,NULL,L_large_number,0,L_large_number,value11),
                          DECODE(value12,NULL,L_large_number,0,L_large_number,value12),
                          DECODE(value13,NULL,L_large_number,0,L_large_number,value13),
                          DECODE(value14,NULL,L_large_number,0,L_large_number,value14),
                          DECODE(value15,NULL,L_large_number,0,L_large_number,value15),
                          DECODE(value16,NULL,L_large_number,0,L_large_number,value16),
                          DECODE(value17,NULL,L_large_number,0,L_large_number,value17),
                          DECODE(value18,NULL,L_large_number,0,L_large_number,value18),
                          DECODE(value19,NULL,L_large_number,0,L_large_number,value19),
                          DECODE(value20,NULL,L_large_number,0,L_large_number,value20),
                          DECODE(value21,NULL,L_large_number,0,L_large_number,value21),
                          DECODE(value22,NULL,L_large_number,0,L_large_number,value22),
                          DECODE(value23,NULL,L_large_number,0,L_large_number,value23),
                          DECODE(value24,NULL,L_large_number,0,L_large_number,value24),
                          DECODE(value25,NULL,L_large_number,0,L_large_number,value25),
                          DECODE(value26,NULL,L_large_number,0,L_large_number,value26),
                          DECODE(value27,NULL,L_large_number,0,L_large_number,value27),
                          DECODE(value28,NULL,L_large_number,0,L_large_number,value28),
                          DECODE(value29,NULL,L_large_number,0,L_large_number,value29),
                          DECODE(value30,NULL,L_large_number,0,L_large_number,value30)) least_value
               from po_diff_matrix_temp
              where diff_z = NVL(I_diff_z, diff_z));
BEGIN
   open C_GET_LEAST_VALUE ;
   fetch C_GET_LEAST_VALUE into L_least_value;
   close C_GET_LEAST_VALUE;
   --
   if L_least_value = L_large_number or L_least_value is null then
      O_least_value := 1;
   else
      O_least_value := L_least_value;
   end if;
   --
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_LEAST_VALUE;
-------------------------------------------------------------------------------
FUNCTION ROUND_TO_HUNDRED(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(100)          := 'PO_DIFF_MATRIX_SQL.ROUND_TO_HUNDRED';
   cursor C_GET_Z_SUM is
      select SUM(NVL(value1,0)+ NVL(value2,0) + NVL(value3,0) + NVL(value4,0) + NVL(value5,0)+
          NVL(value6,0) + NVL(value7,0) + NVL(value8,0) + NVL(value9,0) + NVL(value10,0)+
          NVL(value11,0)+ NVL(value12,0)+ NVL(value13,0) + NVL(value14,0) +NVL(value15,0)+
          NVL(value16,0)+ NVL(value17,0)+ NVL(value18,0) + NVL(value19,0) + NVL(value20,0)+
          NVL(value21,0)+ NVL(value22,0)+ NVL(value23,0) + NVL(value24,0) + NVL(value25,0)+
          NVL(value26,0)+ NVL(value27,0)+ NVL(value28,0) + NVL(value29,0) + NVL(value30,0)) value
    from po_diff_matrix_temp;

   cursor C_GET_DIFF_Z_ROW is
      select rowid ,p.*
        from po_diff_matrix_temp p
        where COALESCE(value1, value2, value3, value4, value5,
                       value6, value7, value8, value9, value10,
                       value11, value12, value13, value14, value15,
                       value16, value17, value18, value19, value20,
                       value21, value22, value23, value24, value25,
                       value26, value27, value28, value29, value30)
                       is NOT NULL
         and rownum =1;
   L_z_sum          NUMBER(20,4);
   L_delta          diff_z_temp.diff_z%TYPE;
   L_column_name    VARCHAR2(30);
   L_statement      VARCHAR2(1000);
 BEGIN
   open C_GET_Z_SUM ;
   fetch C_GET_Z_SUM into L_z_sum;
   close C_GET_Z_SUM;

   if L_z_sum != 100 then
      L_delta := 100 - L_z_sum;
      for rec in C_GET_DIFF_Z_ROW LOOP
         if rec.value1 is not NULL then
           L_column_name := 'VALUE1';
         elsif rec.value2 is not NULL then
            L_column_name := 'VALUE2';
         elsif rec.value3 is not NULL then
            L_column_name := 'VALUE3';
         elsif rec.value4 is not NULL then
            L_column_name := 'VALUE4';
         elsif rec.value5 is not NULL then
            L_column_name := 'VALUE5';
         elsif rec.value6 is not NULL then
            L_column_name := 'VALUE6';
         elsif rec.value7 is not NULL then
            L_column_name := 'VALUE7';
         elsif rec.value8 is not NULL then
            L_column_name := 'VALUE8';
         elsif rec.value9 is not NULL then
            L_column_name := 'VALUE9';
         elsif rec.value10 is not NULL then
            L_column_name := 'VALUE10';
         elsif rec.value11 is not NULL then
            L_column_name := 'VALUE11';
         elsif rec.value12 is not NULL then
            L_column_name := 'VALUE12';
         elsif rec.value13 is not NULL then
            L_column_name := 'VALUE13';
         elsif rec.value14 is not NULL then
            L_column_name := 'VALUE14';
         elsif rec.value15 is not NULL then
            L_column_name := 'VALUE15';
         elsif rec.value16 is not NULL then
            L_column_name := 'VALUE16';
         elsif rec.value17 is not NULL then
            L_column_name := 'VALUE17';
         elsif rec.value18 is not NULL then
            L_column_name := 'VALUE18';
         elsif rec.value19 is not NULL then
            L_column_name := 'VALUE19';
         elsif rec.value20 is not NULL then
            L_column_name := 'VALUE20';
         elsif rec.value21 is not NULL then
            L_column_name := 'VALUE21';
         elsif rec.value22 is not NULL then
            L_column_name := 'VALUE22';
         elsif rec.value23 is not NULL then
            L_column_name := 'VALUE23';
         elsif rec.value24 is not NULL then
            L_column_name := 'VALUE24';
         elsif rec.value25 is not NULL then
            L_column_name := 'VALUE25';
         elsif rec.value26 is not NULL then
            L_column_name := 'VALUE26';
         elsif rec.value27 is not NULL then
            L_column_name := 'VALUE27';
         elsif rec.value28 is not NULL then
            L_column_name := 'VALUE28';
         elsif rec.value29 is not NULL then
            L_column_name := 'VALUE29';
         elsif rec.value30 is not NULL then
            L_column_name := 'VALUE30';
         end if;
         L_statement  := 'update po_diff_matrix_temp set '||L_column_name ||'= '||L_column_name||' + :L_delta
                           where rowid =:row_id';
         EXECUTE IMMEDIATE L_statement USING L_delta,rec.rowid;
      end LOOP;

   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ROUND_TO_HUNDRED;
-------------------------------------------------------------------------------------------
FUNCTION COPY_DIFF_Z_MATRIX(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_copy_from_diff    IN     DIFF_IDS.DIFF_ID%TYPE,
                            I_matrix_mode       IN     VARCHAR2)
RETURN BOOLEAN IS
   L_program           VARCHAR2(100)     := 'PO_DIFF_MATRIX_SQL.COPY_DIFF_Z_MATRIX';
   L_copy_from_x       DIFF_X_TEMP.DIFF_X%TYPE;
   L_copy_from_value   PO_DIFF_MATRIX_TEMP.VALUE1%TYPE;
   L_copy_from_seq     DIFF_X_TEMP.SEQ_NO%TYPE;
   L_copy_from_diff_y  PO_DIFF_MATRIX_TEMP.DIFF_Y%TYPE;
   L_copy_to_seq       DIFF_X_TEMP.SEQ_NO%TYPE;
   L_copy_to_diff_z    DIFF_Z_TEMP.DIFF_Z%TYPE;
   ---
   L_fromval_statement VARCHAR2(2000);
   L_update_statement  VARCHAR2(2000);
   L_impacted_z_sum    NUMBER(20,4) := 0;
   L_copy_from_z_sum   NUMBER(20,4) := 0;
   cursor C_COPY_FROM_XS is
      select x.diff_x copy_from_x,
             x.seq_no copy_from_seq,
             m.diff_y
        from diff_x_temp x,
             po_diff_matrix_temp m
       where m.diff_z = I_copy_from_diff
         and m.diff_z = x.diff_z
       order by copy_from_x, copy_from_seq;

   cursor C_COPY_TO_XS is
      select x.diff_x copy_to_x,
             x.seq_no copy_to_seq,
             z.diff_z copy_to_diff_z
        from diff_x_temp x,
             diff_z_temp z
       where z.diff_z = x.diff_z
         and x.diff_x = L_copy_from_x
         and z.copy_ind = 'Y';

   TYPE Z_TEMP_PCT_RATIO_RECORD IS RECORD(diff_z    diff_z_temp.diff_z%TYPE,
                                            z_ratio_mutliplier     diff_z_temp.value%TYPE);
   TYPE  L_Z_TEMP_PCT_RATIO_TBL IS TABLE OF Z_TEMP_PCT_RATIO_RECORD INDEX BY PLS_INTEGER;
   L_z_temp_ratio_rec  L_Z_TEMP_PCT_RATIO_TBL;

  cursor C_GET_Z_MULTPLIER is
    select diff_z,
           value/SUM(value)  over () z_ratio_mutliplier
     from  diff_z_temp
    where (copy_ind ='Y'
           or diff_z = I_copy_from_diff)
      and value is not null
      and I_matrix_mode ='P'
    union all
   select d1.diff_z,
           d1.value/d2.value
     from  diff_z_temp d1,diff_z_temp d2
    where d1.copy_ind ='Y'
      and d2.diff_z = I_copy_from_diff
      and I_matrix_mode in('Q' ,'R')
      and d1.value is not null;

   cursor C_GET_IMPACTED_Z_SUM is
   select SUM(NVL(value1,0)+ NVL(value2,0) + NVL(value3,0) + NVL(value4,0) + NVL(value5,0)+
          NVL(value6,0) + NVL(value7,0) + NVL(value8,0) + NVL(value9,0) + NVL(value10,0)+
          NVL(value11,0)+ NVL(value12,0)+ NVL(value13,0) + NVL(value14,0) +NVL(value15,0)+
          NVL(value16,0)+ NVL(value17,0)+ NVL(value18,0) + NVL(value19,0) + NVL(value20,0)+
          NVL(value21,0)+ NVL(value22,0)+ NVL(value23,0) + NVL(value24,0) + NVL(value25,0)+
          NVL(value26,0)+ NVL(value27,0)+ NVL(value28,0) + NVL(value29,0) + NVL(value30,0)) value
    from po_diff_matrix_temp
   where diff_z in (select diff_z from diff_z_temp where (copy_ind ='Y' or diff_z = I_copy_from_diff));

  cursor C_GET_COPY_FROM_Z_SUM is
   select SUM(NVL(value1,0)+ NVL(value2,0) + NVL(value3,0) + NVL(value4,0) + NVL(value5,0)+
          NVL(value6,0) + NVL(value7,0) + NVL(value8,0) + NVL(value9,0) + NVL(value10,0)+
          NVL(value11,0)+ NVL(value12,0)+ NVL(value13,0) + NVL(value14,0) +NVL(value15,0)+
          NVL(value16,0)+ NVL(value17,0)+ NVL(value18,0) + NVL(value19,0) + NVL(value20,0)+
          NVL(value21,0)+ NVL(value22,0)+ NVL(value23,0) + NVL(value24,0) + NVL(value25,0)+
          NVL(value26,0)+ NVL(value27,0)+ NVL(value28,0) + NVL(value29,0) + NVL(value30,0)) value
    from po_diff_matrix_temp
   where diff_z = I_copy_from_diff;
BEGIN
   if I_copy_from_diff is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_copy_from_diff',
                                            L_program,
                                            NULL);
         return FALSE;
   end if;
   if I_matrix_mode is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_matrix_mode',
                                            L_program,
                                            NULL);
         return FALSE;
   end if;
   ---
   open C_GET_IMPACTED_Z_SUM;
   fetch C_GET_IMPACTED_Z_SUM into L_impacted_z_sum;
   close C_GET_IMPACTED_Z_SUM;

   for rec in C_COPY_FROM_XS LOOP
      --- Fetch all copy from information.  cursor loops through each diff_x, seq_no, diff_y value
      L_copy_from_x   := rec.copy_from_x;
      L_copy_from_seq := rec.copy_from_seq;
      L_copy_from_diff_y := rec.diff_y;
      ---
      -- fetch the value in the correct po_diff_matrix_temp column
      L_fromval_statement := 'select value'||L_copy_from_seq
                           ||'  from po_diff_matrix_temp'
                           ||' where diff_z = :l_copy_from_diff '
                           ||'   and diff_y = :l_copy_frm_diff_y ';
      ---
      EXECUTE IMMEDIATE L_fromval_statement into L_copy_from_value USING I_copy_from_diff,L_copy_from_diff_y;
      for recs in C_COPY_TO_XS LOOP
         --- Fetch all copy to info.  cursor loops for all diffys for each 'copy to' diffz
         L_copy_to_seq := recs.copy_to_seq;
         L_copy_to_diff_z := recs.copy_to_diff_z;

         L_update_statement :=  'update po_diff_matrix_temp '
                              ||'   set value'||L_copy_to_seq||' = '||nvl(to_char(L_copy_from_value),'to_number(NULL)')
                              ||' where diff_y = :l_copy_frm_diff_y '
                              ||'   and diff_z = :l_copy_to_dif_z ';
         EXECUTE IMMEDIATE L_update_statement USING L_copy_from_diff_y,L_copy_to_diff_z;

      end LOOP;
   end LOOP;
   --
   open C_GET_COPY_FROM_Z_SUM;
   fetch C_GET_COPY_FROM_Z_SUM into L_copy_from_z_sum;
   close C_GET_COPY_FROM_Z_SUM;

   open C_GET_Z_MULTPLIER;
   fetch C_GET_Z_MULTPLIER BULK COLLECT into L_z_temp_ratio_rec;
   close C_GET_Z_MULTPLIER;

   if I_matrix_mode  ='Q' or I_matrix_mode  ='R'  then
      L_impacted_z_sum := 1;
      L_copy_from_z_sum := 1;
   end if;
   FORALL indx in 1..L_z_temp_ratio_rec.COUNT
     update po_diff_matrix_temp
        set value1 = value1 *(L_z_temp_ratio_rec(indx).z_ratio_mutliplier  * ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value2 = value2 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value3 = value3 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value4 = value4 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value5 = value5 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value6 = value6 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value7 = value7 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value8 = value8 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value9 = value9 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value10 = value10 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value11 = value11 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value12 = value12 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value13 = value13 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value14 = value14 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value15 = value15 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value16 = value16 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value17 = value17 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value18 = value18 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value19 = value19 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value20 = value20 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value21 = value21 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value22 = value22 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value23 = value23 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value24 = value24 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value25 = value25 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value26 = value26 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value27 = value27 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value28 = value28 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value29 = value29 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum)),
                   value30 = value30 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier *  ( L_impacted_z_sum/L_copy_from_z_sum))
      where diff_z =  L_z_temp_ratio_rec(indx).diff_z;
   --
   if I_matrix_mode = 'P' then
       if ROUND_TO_HUNDRED(O_error_message) = FALSE then
          return FALSE;
       end if;
   end if;

   update diff_z_temp
      set copy_ind = 'N';

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END COPY_DIFF_Z_MATRIX;
--------------------------------------------------------------------------------------------
FUNCTION ADJUST_MATRIX_DIST_VALUE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_matrix_mode      IN     VARCHAR2,
                                  I_diff_z           IN     DIFF_Z_TEMP.DIFF_Z%TYPE,
                                  I_new_record_ind   IN     VARCHAR2,
                                  I_diff_z_old_value IN     DIFF_Z_TEMP.VALUE%TYPE)
RETURN BOOLEAN IS
   L_multiply_factor NUMBER(20,4);
   L_new_z_sum       NUMBER(20,4);
   L_diff_z_count    NUMBER(10);
   L_program         VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.ADJUST_MATRIX_DIST_VALUE';
   L_least_value     DIFF_Z_TEMP.VALUE%TYPE;
   L_inv_parm        VARCHAR2(30) := NULL;
   --
   cursor C_GET_DIFF_Z_SUM_FROM_MATRIX is
      select diff_z,
             SUM(NVL(value1, 0) + NVL(value2, 0) + NVL(value3, 0) + NVL(value4, 0) + NVL(value5, 0) +
                 NVL(value6, 0) + NVL(value7, 0) + NVL(value8, 0) + NVL(value9, 0) + NVL(value10, 0) +
                 NVL(value11, 0) + NVL(value12, 0) + NVL(value13, 0) + NVL(value14, 0) + NVL(value15, 0) +
                 NVL(value16, 0) + NVL(value17, 0) + NVL(value18, 0) + NVL(value19, 0) + NVL(value20, 0) +
                 NVL(value21, 0) + NVL(value22, 0) + NVL(value23, 0) + NVL(value24, 0) + NVL(value25, 0) +
                 NVL(value26, 0) + NVL(value27, 0) + NVL(value28, 0) + NVL(value29, 0) + NVL(value30, 0))
        from po_diff_matrix_temp
       where diff_z <> I_diff_z
       group by diff_z;


   cursor DIFF_Z_COUNT is
      select count(distinct diff_z)
        from po_diff_matrix_temp
        where COALESCE(value1, value2, value3, value4, value5,
                       value6, value7, value8, value9, value10,
                       value11, value12, value13, value14, value15,
                       value16, value17, value18, value19, value20,
                       value21, value22, value23, value24, value25,
                       value26, value27, value28, value29, value30)
                       is NOT NULL;

   cursor C_GET_CURRENT_Z_SUM is
      select SUM(NVL(value1, 0) + NVL(value2, 0) + NVL(value3, 0) + NVL(value4, 0) + NVL(value5, 0) +
                 NVL(value6, 0) + NVL(value7, 0) + NVL(value8, 0) + NVL(value9, 0) + NVL(value10, 0) +
                 NVL(value11, 0) + NVL(value12, 0) + NVL(value13, 0) + NVL(value14, 0) + NVL(value15, 0) +
                 NVL(value16, 0) + NVL(value17, 0) + NVL(value18, 0) + NVL(value19, 0) + NVL(value20, 0) +
                 NVL(value21, 0) + NVL(value22, 0) + NVL(value23, 0) + NVL(value24, 0) + NVL(value25, 0) +
                 NVL(value26, 0) + NVL(value27, 0) + NVL(value28, 0) + NVL(value29, 0) + NVL(value30, 0))
        from po_diff_matrix_temp
       where diff_z = I_diff_z;
   --
   TYPE Z_TEMP_PCT_RATIO_RECORD IS RECORD(diff_z    DIFF_Z_TEMP.DIFF_Z%TYPE,
                                          value     DIFF_Z_TEMP.VALUE%TYPE);
   TYPE  L_Z_TEMP_PCT_RATIO_TBL IS TABLE OF Z_TEMP_PCT_RATIO_RECORD  INDEX BY PLS_INTEGER;
   L_z_temp_ratio_rec  L_Z_TEMP_PCT_RATIO_TBL;
BEGIN
   if I_matrix_mode is NULL  then
      L_inv_parm :='I_matrix_mode';
   end if;
   if I_diff_z is NULL and
      I_matrix_mode != 'R' then
      L_inv_parm := 'I_diff_z';
   end if;
   if I_new_record_ind is NULL  then
      L_inv_parm :='I_new_record_ind';
   end if;
   if I_new_record_ind = 'N' and
      I_diff_z_old_value is NULL  then
      L_inv_parm :='I_diff_z_old_value';
   end if;
   if  L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_new_record_ind = 'Y' then
      if I_matrix_mode ='P' then
         open C_GET_DIFF_Z_SUM_FROM_MATRIX ;
         fetch C_GET_DIFF_Z_SUM_FROM_MATRIX  BULK COLLECT into L_z_temp_ratio_rec;
         close C_GET_DIFF_Z_SUM_FROM_MATRIX;
         ---
         FORALL indx in 1..L_z_temp_ratio_rec.COUNT
            update diff_z_temp
               set value = L_z_temp_ratio_rec(indx).value
             where diff_z = L_z_temp_ratio_rec(indx).diff_z;
         ---
         open DIFF_Z_COUNT ;
         fetch DIFF_Z_COUNT into L_diff_z_count;
         close DIFF_Z_COUNT;
         ---
         if L_diff_z_count != 0 then -- to avoid divide by zero error
            update diff_z_temp
               set value = decode ( diff_z ,I_diff_z, 100/L_diff_z_count,(value/100) *(100-100/L_diff_z_count));
            ---
            if NORMALIZE_PO_DIFF_MATRIX_TEMP(O_error_message,
                                             'Y') = FALSE then
               return FALSE;
            end if;
         end if;   
      elsif I_matrix_mode ='R' then
         if GET_LEAST_VALUE(O_error_message,
                            L_least_value,
                            I_diff_z) = FALSE then
            return FALSE;
         end if;
         ---
         if L_least_value != 1 and L_least_value != 0 then
            update po_diff_matrix_temp
               set value1 = value1 / L_least_value,
                   value2 = value2 / L_least_value,
                   value3 = value3 / L_least_value,
                   value4 = value4 / L_least_value,
                   value5 = value5 / L_least_value,
                   value6 = value6 / L_least_value,
                   value7 = value7 / L_least_value,
                   value8 = value8 / L_least_value,
                   value9 = value9 / L_least_value,
                   value10 = value10 / L_least_value,
                   value11 = value11 / L_least_value,
                   value12 = value12 / L_least_value,
                   value13 = value13 / L_least_value,
                   value14 = value14 / L_least_value,
                   value15 = value15 / L_least_value,
                   value16 = value16 / L_least_value,
                   value17 = value17 / L_least_value,
                   value18 = value18 / L_least_value,
                   value19 = value19 / L_least_value,
                   value20 = value20 / L_least_value,
                   value21 = value21 / L_least_value,
                   value22 = value22 / L_least_value,
                   value23 = value23 / L_least_value,
                   value24 = value24 / L_least_value,
                   value25 = value25 / L_least_value,
                   value26 = value26 / L_least_value,
                   value27 = value27 / L_least_value,
                   value28 = value28 / L_least_value,
                   value29 = value29 / L_least_value,
                   value30 = value30 / L_least_value
             where diff_z =  I_diff_z;
         end if;
      end if;
   else -- readjusting the x/y values and not the Z values.
      open C_GET_CURRENT_Z_SUM ;
      fetch C_GET_CURRENT_Z_SUM into L_new_z_sum;
      close C_GET_CURRENT_Z_SUM;
      ---
      if L_new_z_sum != 0 then
         L_multiply_factor := I_diff_z_old_value / L_new_z_sum;

         update po_diff_matrix_temp
            set value1 = value1 * L_multiply_factor,
                value2 = value2 * L_multiply_factor,
                value3 = value3 * L_multiply_factor,
                value4 = value4 * L_multiply_factor,
                value5 = value5 * L_multiply_factor,
                value6 = value6 * L_multiply_factor,
                value7 = value7 * L_multiply_factor,
                value8 = value8 * L_multiply_factor,
                value9 = value9 * L_multiply_factor,
                value10 = value10 * L_multiply_factor,
                value11 = value11 * L_multiply_factor,
                value12 = value12 * L_multiply_factor,
                value13 = value13 * L_multiply_factor,
                value14 = value14 * L_multiply_factor,
                value15 = value15 * L_multiply_factor,
                value16 = value16 * L_multiply_factor,
                value17 = value17 * L_multiply_factor,
                value18 = value18 * L_multiply_factor,
                value19 = value19 * L_multiply_factor,
                value20 = value20 * L_multiply_factor,
                value21 = value21 * L_multiply_factor,
                value22 = value22 * L_multiply_factor,
                value23 = value23 * L_multiply_factor,
                value24 = value24 * L_multiply_factor,
                value25 = value25 * L_multiply_factor,
                value26 = value26 * L_multiply_factor,
                value27 = value27 * L_multiply_factor,
                value28 = value28 * L_multiply_factor,
                value29 = value29 * L_multiply_factor,
                value30 = value30 * L_multiply_factor
          where diff_z =  I_diff_z;
      else
         if I_matrix_mode ='P' then
            if NORMALIZE_PO_DIFF_MATRIX_TEMP(O_error_message,
                                             'N') = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
   end if;
    --
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADJUST_MATRIX_DIST_VALUE;
------------------------------------------------------------------------------------------------
FUNCTION NORMALIZE_PO_DIFF_MATRIX_TEMP (O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_diff_z_populated_ind   IN     VARCHAR2)
RETURN BOOLEAN IS
   L_total         diff_z_temp.value%TYPE;
   L_diff_z_total  diff_z_temp.value%TYPE;
   L_program             VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.NORMALIZE_PO_DIFF_MATRIX_TEMP';
   ---
   cursor C_GET_DIFF_Z_SUM(L_diff_z diff_z_temp.diff_z%TYPE)is
      select SUM(NVL(value1, 0) + NVL(value2, 0) + NVL(value3, 0) + NVL(value4, 0) + NVL(value5, 0) +
                 NVL(value6, 0) + NVL(value7, 0) + NVL(value8, 0) + NVL(value9, 0) + NVL(value10, 0) +
                 NVL(value11, 0) + NVL(value12, 0) + NVL(value13, 0) + NVL(value14, 0) + NVL(value15, 0) +
                 NVL(value16, 0) + NVL(value17, 0) + NVL(value18, 0) + NVL(value19, 0) + NVL(value20, 0) +
                 NVL(value21, 0) + NVL(value22, 0) + NVL(value23, 0) + NVL(value24, 0) + NVL(value25, 0) +
                 NVL(value26, 0) + NVL(value27, 0) + NVL(value28, 0) + NVL(value29, 0) + NVL(value30, 0))
        from po_diff_matrix_temp
       where diff_z = L_diff_z;

   cursor C_GET_Z_MULTPLIER is
      select diff_z,
             value/SUM(value)  over () z_ratio_mutliplier
        from diff_z_temp
       where value is not NULL;

   cursor C_IS_SUM_XYZ_100 is
      select SUM(NVL(value1, 0) + NVL(value2, 0) + NVL(value3, 0) + NVL(value4, 0) + NVL(value5, 0) +
                 NVL(value6, 0) + NVL(value7, 0) + NVL(value8, 0) + NVL(value9, 0) + NVL(value10, 0) +
                 NVL(value11, 0) + NVL(value12, 0) + NVL(value13, 0) + NVL(value14, 0) + NVL(value15, 0) +
                 NVL(value16, 0) + NVL(value17, 0) + NVL(value18, 0) + NVL(value19, 0) + NVL(value20, 0) +
                 NVL(value21, 0) + NVL(value22, 0) + NVL(value23, 0) + NVL(value24, 0) + NVL(value25, 0) +
                 NVL(value26, 0) + NVL(value27, 0) + NVL(value28, 0) + NVL(value29, 0) + NVL(value30, 0))
        from po_diff_matrix_temp;
   ---
   TYPE Z_TEMP_RATIO_RECORD IS RECORD( diff_z                 DIFF_Z_TEMP.DIFF_Z%TYPE,
                                       z_ratio_mutliplier     DIFF_Z_TEMP.VALUE%TYPE);
   TYPE Z_TEMP_RATIO_TBL IS TABLE OF Z_TEMP_RATIO_RECORD  INDEX BY PLS_INTEGER;
   L_z_temp_ratio_rec  Z_TEMP_RATIO_TBL;
BEGIN
   if I_diff_z_populated_ind is NULL  then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_diff_z_populated_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   --make total po_diff_matrix_temp to 100
   open C_IS_SUM_XYZ_100;
   fetch C_IS_SUM_XYZ_100 INTO L_total;
   close C_IS_SUM_XYZ_100;
   ---
   if L_total != 100  and L_total != 0 THEN
      update po_diff_matrix_temp
         set value1 = value1 * 100 /L_total,
             value2 = value2 * 100 /L_total,
             value3 = value3 * 100 /L_total,
             value4 = value4 * 100 /L_total,
             value5 = value5 * 100 /L_total,
             value6 = value6 * 100 /L_total,
             value7 = value7 * 100 /L_total,
             value8 = value8 * 100 /L_total,
             value9 = value9 * 100 /L_total,
             value10 = value10 * 100 /L_total,
             value11 = value11 * 100 /L_total,
             value12 = value12 * 100 /L_total,
             value13 = value13 * 100 /L_total,
             value14 = value14 * 100 /L_total,
             value15 = value15 * 100 /L_total,
             value16 = value16 * 100 /L_total,
             value17 = value17 * 100 /L_total,
             value18 = value18 * 100 /L_total,
             value19 = value19 * 100 /L_total,
             value20 = value20 * 100 /L_total,
             value21 = value21 * 100 /L_total,
             value22 = value22 * 100 /L_total,
             value23 = value23 * 100 /L_total,
             value24 = value24 * 100 /L_total,
             value25 = value25 * 100 /L_total,
             value26 = value26 * 100 /L_total,
             value27 = value27 * 100 /L_total,
             value28 = value28 * 100 /L_total,
             value29 = value29 * 100 /L_total,
             value30 = value30 * 100 /L_total;
      end if;
      if I_diff_z_populated_ind ='Y' then
         open C_GET_Z_MULTPLIER;
         fetch C_GET_Z_MULTPLIER BULK COLLECT INTO L_z_temp_ratio_rec;
         close C_GET_Z_MULTPLIER;
         ---
         FOR indx in 1..L_z_temp_ratio_rec.COUNT  LOOP
            open C_GET_DIFF_Z_SUM(L_z_temp_ratio_rec(indx).diff_z);
            fetch C_GET_DIFF_Z_SUM INTO L_diff_z_total;
            close C_GET_DIFF_Z_SUM;
            ---
            if L_diff_z_total <> 0 then
               update po_diff_matrix_temp
                  set value1 = 100 * value1 *(L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value2 = 100 * value2 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value3 = 100 * value3 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value4 = 100 * value4 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value5 = 100 * value5 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value6 = 100 * value6 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value7 = 100 * value7 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value8 = 100 * value8 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value9 = 100 * value9 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value10 = 100 * value10 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value11 = 100 * value11 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value12 = 100 * value12 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value13 = 100 * value13 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value14 = 100 * value14 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value15 = 100 * value15 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value16 = 100 * value16 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value17 = 100 * value17 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value18 = 100 * value18 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value19 = 100 * value19 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value20 = 100 * value20 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value21 = 100 * value21 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value22 = 100 * value22 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value23 = 100 * value23 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value24 = 100 * value24 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value25 = 100 * value25 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value26 = 100 * value26 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value27 = 100 * value27 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value28 = 100 * value28 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value29 = 100 * value29 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total),
                      value30 = 100 * value30 * (L_z_temp_ratio_rec(indx).z_ratio_mutliplier/L_diff_z_total)
               where diff_z = L_z_temp_ratio_rec(indx).diff_z;
            end if;
         end LOOP;
      end if;
      if ROUND_TO_HUNDRED(O_error_message) = FALSE then
         return FALSE;
      end if;
  return TRUE;
EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END NORMALIZE_PO_DIFF_MATRIX_TEMP;
------------------------------------------------------------------------
FUNCTION CONVERT_TO_QTY(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_parent_total_qty IN     NUMBER)
RETURN BOOLEAN IS
   L_inv_parm            VARCHAR2(30)  := NULL;
   L_program             VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.CONVERT_TO_QTY';
   L_qty_factor          NUMBER(20,4)  := NULL;
   L_matrix_total        NUMBER(20,4)  := NULL;

   cursor C_GET_MATRIX_TOTAL is
      select SUM(NVL(value1, 0) + NVL(value2, 0) + NVL(value3, 0) + NVL(value4, 0) + NVL(value5, 0) +
                 NVL(value6, 0) + NVL(value7, 0) + NVL(value8, 0) + NVL(value9, 0) + NVL(value10, 0) +
                 NVL(value11, 0) + NVL(value12, 0) + NVL(value13, 0) + NVL(value14, 0) + NVL(value15, 0) +
                 NVL(value16, 0) + NVL(value17, 0) + NVL(value18, 0) + NVL(value19, 0) + NVL(value20, 0) +
                 NVL(value21, 0) + NVL(value22, 0) + NVL(value23, 0) + NVL(value24, 0) + NVL(value25, 0) +
                 NVL(value26, 0) + NVL(value27, 0) + NVL(value28, 0) + NVL(value29, 0) + NVL(value30, 0))
        from po_diff_matrix_temp;

   cursor C_GET_Z_VALUE is
      select diff_z ,
            SUM(NVL(value1, 0) + NVL(value2, 0) + NVL(value3, 0) + NVL(value4, 0) + NVL(value5, 0) +
                NVL(value6, 0) + NVL(value7, 0) + NVL(value8, 0) + NVL(value9, 0) + NVL(value10, 0) +
                NVL(value11, 0) + NVL(value12, 0) + NVL(value13, 0) + NVL(value14, 0) + NVL(value15, 0) +
                NVL(value16, 0) + NVL(value17, 0) + NVL(value18, 0) + NVL(value19, 0) + NVL(value20, 0) +
                NVL(value21, 0) + NVL(value22, 0) + NVL(value23, 0) + NVL(value24, 0) + NVL(value25, 0) +
                NVL(value26, 0) + NVL(value27, 0) + NVL(value28, 0) + NVL(value29, 0) + NVL(value30, 0)) value
        from po_diff_matrix_temp
        group by diff_z;

   L_z_qty  diff_z_temp.value%TYPE;
   L_diff_z diff_z_temp.diff_z%TYPE;
BEGIN
   if I_parent_total_qty is NULL then
      L_inv_parm := 'I_parent_total_qty';
   end if;
   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_GET_MATRIX_TOTAL;
   fetch C_GET_MATRIX_TOTAL INTO L_matrix_total;
   close C_GET_MATRIX_TOTAL;
   ---
   if L_matrix_total != 0 then
      L_qty_factor := I_parent_total_qty / L_matrix_total;
      update po_diff_matrix_temp
         set value1 = decode( ROUND ( value1 * L_qty_factor),0,1, ROUND ( value1 * L_qty_factor)) ,
             value2 = decode( ROUND ( value2 * L_qty_factor),0,1, ROUND ( value2 * L_qty_factor)) ,
             value3 = decode( ROUND ( value3 * L_qty_factor),0,1, ROUND ( value3 * L_qty_factor)),
             value4 = decode( ROUND ( value4 * L_qty_factor),0,1, ROUND ( value4 * L_qty_factor)) ,
             value5 = decode( ROUND ( value5 * L_qty_factor),0,1, ROUND ( value5 * L_qty_factor)) ,
             value6 = decode( ROUND ( value6 * L_qty_factor),0,1, ROUND ( value6 * L_qty_factor)) ,
             value7 = decode( ROUND ( value7 * L_qty_factor),0,1, ROUND ( value7 * L_qty_factor)) ,
             value8 = decode( ROUND ( value8 * L_qty_factor),0,1, ROUND ( value8 * L_qty_factor)) ,
             value9 = decode( ROUND ( value9 * L_qty_factor),0,1, ROUND ( value9 * L_qty_factor)) ,
             value10 = decode( ROUND ( value10 * L_qty_factor),0,1, ROUND ( value10 * L_qty_factor)) ,
             value11 = decode( ROUND ( value11 * L_qty_factor),0,1, ROUND ( value11 * L_qty_factor)) ,
             value12 = decode( ROUND ( value12 * L_qty_factor),0,1, ROUND ( value12 * L_qty_factor)) ,
             value13 = decode( ROUND ( value13 * L_qty_factor),0,1, ROUND ( value13 * L_qty_factor)),
             value14 = decode( ROUND ( value14 * L_qty_factor),0,1, ROUND ( value14 * L_qty_factor)) ,
             value15 = decode( ROUND ( value15 * L_qty_factor),0,1, ROUND ( value15 * L_qty_factor)) ,
             value16 = decode( ROUND ( value16 * L_qty_factor),0,1, ROUND ( value16 * L_qty_factor)) ,
             value17 = decode( ROUND ( value17 * L_qty_factor),0,1, ROUND ( value17 * L_qty_factor)) ,
             value18 = decode( ROUND ( value18 * L_qty_factor),0,1, ROUND ( value18 * L_qty_factor)) ,
             value19 = decode( ROUND ( value19 * L_qty_factor),0,1, ROUND ( value19 * L_qty_factor)) ,
             value20 = decode( ROUND ( value20 * L_qty_factor),0,1, ROUND ( value20 * L_qty_factor)) ,
             value21 = decode( ROUND ( value21 * L_qty_factor),0,1, ROUND ( value21 * L_qty_factor)) ,
             value22 = decode( ROUND ( value22 * L_qty_factor),0,1, ROUND ( value22 * L_qty_factor)) ,
             value23 = decode( ROUND ( value23 * L_qty_factor),0,1, ROUND ( value23 * L_qty_factor)),
             value24 = decode( ROUND ( value24 * L_qty_factor),0,1, ROUND ( value24 * L_qty_factor)) ,
             value25 = decode( ROUND ( value25 * L_qty_factor),0,1, ROUND ( value25 * L_qty_factor)) ,
             value26 = decode( ROUND ( value26 * L_qty_factor),0,1, ROUND ( value26 * L_qty_factor)) ,
             value27 = decode( ROUND ( value27 * L_qty_factor),0,1, ROUND ( value27 * L_qty_factor)) ,
             value28 = decode( ROUND ( value28 * L_qty_factor),0,1, ROUND ( value28 * L_qty_factor)) ,
             value29 = decode( ROUND ( value29 * L_qty_factor),0,1, ROUND ( value29 * L_qty_factor)) ,
             value30 = decode( ROUND ( value30 * L_qty_factor),0,1, ROUND ( value30 * L_qty_factor)) ;

      for rec in C_GET_Z_VALUE LOOP
         if rec.diff_z is NOT NULL then
            update diff_z_temp
               set value = rec.value
             where diff_z = rec.diff_z;
         end if;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONVERT_TO_QTY;
------------------------------------------------------------------------
FUNCTION GET_DIFF_Z_SUM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_z_sum          OUT    DIFF_Z_TEMP.VALUE%TYPE,
                        I_diff_z         IN     DIFF_Z_TEMP.DIFF_Z%TYPE )
RETURN BOOLEAN IS
   L_program       VARCHAR2(100)          := 'PO_DIFF_MATRIX_SQL.GET_DIFF_Z_SUM';
   cursor C_GET_DIFF_Z_SUM_FROM_MATRIX is
      select SUM(NVL(value1, 0) + NVL(value2, 0) + NVL(value3, 0) + NVL(value4, 0) + NVL(value5, 0) +
             NVL(value6, 0) + NVL(value7, 0) + NVL(value8, 0) + NVL(value9, 0) + NVL(value10, 0) +
             NVL(value11, 0) + NVL(value12, 0) + NVL(value13, 0) + NVL(value14, 0) + NVL(value15, 0) +
             NVL(value16, 0) + NVL(value17, 0) + NVL(value18, 0) + NVL(value19, 0) + NVL(value20, 0) +
             NVL(value21, 0) + NVL(value22, 0) + NVL(value23, 0) + NVL(value24, 0) + NVL(value25, 0) +
             NVL(value26, 0) + NVL(value27, 0) + NVL(value28, 0) + NVL(value29, 0) + NVL(value30, 0))
        from po_diff_matrix_temp
       where diff_z =NVL(I_diff_z,diff_z);
BEGIN
   open C_GET_DIFF_Z_SUM_FROM_MATRIX ;
   fetch C_GET_DIFF_Z_SUM_FROM_MATRIX into O_z_sum;
   close C_GET_DIFF_Z_SUM_FROM_MATRIX;
   if O_z_sum is null then
      O_z_sum := 0;
   end if;
   return TRUE;
 EXCEPTION
    when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
         return FALSE;
END GET_DIFF_Z_SUM;
----------------------------------------------------------------------------------------------------
FUNCTION APPLY_DIFF_RATIO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_diff_ratio_id   IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE,
                          I_store           IN     DIFF_RATIO_DETAIL.STORE%TYPE,
                          I_diff_x_no       IN     NUMBER,
                          I_diff_y_no       IN     NUMBER,
                          I_diff_z_no       IN     NUMBER,
                          I_diff_z_id       IN     DIFF_IDS.DIFF_ID%TYPE,
                          I_matrix_mode     IN     VARCHAR2,
                          I_range_x_no      IN     NUMBER,
                          I_range_y_no      IN     NUMBER,
                          I_range_z_no      IN     NUMBER,
                          I_range_id        IN     DIFF_RANGE_DETAIL.DIFF_RANGE%TYPE)

RETURN BOOLEAN IS
   TYPE QTY_CURSOR is REF CURSOR;
   C_Z_QTY            QTY_CURSOR;
   C_XYZ_QTY          QTY_CURSOR;
   L_program          VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.APPLY_DIFF_RATIO';
   L_z_qty            DIFF_RATIO_DETAIL.PCT%TYPE;
   L_xy_qty           DIFF_RATIO_DETAIL.PCT%TYPE;
   L_diff_z           DIFF_IDS.DIFF_ID%TYPE;
   L_diff_y           DIFF_IDS.DIFF_ID%TYPE;
   L_diff_x           DIFF_IDS.DIFF_ID%TYPE;
   L_statement        VARCHAR2(4000);
   L_statement2       VARCHAR2(4000);
   L_statement3       VARCHAR2(4000);
   L_seq_no           DIFF_X_TEMP.SEQ_NO%TYPE;
   L_inv_parm         VARCHAR2(30)      := NULL;
   cursor C_SEQ_NO is
      select seq_no, diff_z
        from diff_x_temp
       where diff_x = L_diff_x
         and (diff_z = L_diff_z or L_diff_z is NULL);
   L_diff_x_no        NUMBER(1);
   L_diff_y_no        NUMBER(1);
   L_diff_z_no        NUMBER(1);
BEGIN
   if I_diff_ratio_id is NULL then
      L_inv_parm := 'I_diff_ratio_id';
   elsif I_matrix_mode is NULL then
      L_inv_parm := 'I_matrix_mode';
   end if;
   if I_diff_z_id is NULL then
     if I_diff_x_no is NULL then
        L_inv_parm := 'I_diff_x_no';
     elsif I_diff_y_no is NULL then
        L_inv_parm := 'I_diff_y_no';
     elsif I_diff_z_no is NULL then
        L_inv_parm := 'I_diff_z_no';
     end if;
   elsif I_diff_z_no is NULL then
        L_inv_parm := 'I_diff_z_no';
   end if;
   if L_inv_parm is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   L_diff_z_no := I_diff_z_no;
   if I_diff_x_no is NULL then
      L_diff_x_no := 3;
   else
      L_diff_x_no := I_diff_x_no;
   end if;
   if I_diff_y_no is NULL then
      L_diff_y_no := 2;
   else
      L_diff_y_no := I_diff_y_no;
   end if;

   if I_diff_z_id is NOT NULL then
      --- update diff_z_temp with pct value  from diff_ratio_detail and then call the copy_diff_z  function
      update diff_z_temp
         set value = NULL;
      L_statement := 'select rt.pct, rt.diff_'||L_diff_z_no
                   ||' from diff_ratio_detail rt'
                   ||' where rt.diff_ratio_id = :I_diff_ratio_id'
                   ||' and NVL(rt.store, -999) = NVL(:I_store, -999)'
                   ||' and rt.diff_'||L_diff_z_no ||' is not null'
                   ||' and rt.diff_'||L_diff_z_no ||' in ( select diff_id  from diff_group_detail , diff_ratio_head h where h.diff_ratio_id = rt.diff_ratio_id and  diff_group_id = h.diff_group_1)'
                   ||' and rt.diff_'||L_diff_y_no|| ' is null'
                   ||' and rt.diff_'||L_diff_x_no||' is null';
      if I_range_id is NOT NULL  and
         I_range_z_no is NOT NULL then
         L_statement := L_statement||' and exists (select ''x'''
                        ||' from diff_range_detail rg'
                        ||' where rg.diff_range = :I_range_id'
                        ||' and rt.diff_'||I_range_z_no||' = rg.diff_'||I_range_z_no||')';
      end if;
      ---
      if I_range_id is NOT NULL and
         I_range_z_no is NOT NULL then
         EXECUTE IMMEDIATE L_statement USING I_diff_ratio_id, I_store, I_range_id;
         open C_Z_QTY for L_statement USING I_diff_ratio_id, I_store, I_range_id;
      else
         EXECUTE IMMEDIATE L_statement USING I_diff_ratio_id, I_store;
         open C_Z_QTY for L_statement USING I_diff_ratio_id, I_store;
      end if;
      ---
      LOOP
         fetch C_Z_QTY into L_z_qty, L_diff_z;
         EXIT when C_Z_QTY%NOTFOUND;
         update diff_z_temp
            set value = L_z_qty,
                copy_ind = DECODE(diff_z,I_diff_z_id ,'N','Y')
          where diff_z = L_diff_z;
      end LOOP;
      close C_Z_QTY;
      -- call copy diffZ.
      if COPY_DIFF_Z_MATRIX(O_error_message,
                            I_diff_z_id,
                            I_matrix_mode) = FALSE then
         return FALSE;
      end if;
   else --I_diff_z_id is NULL means update po_diff_matrix_temp by selecting from diff_ratio_detail
      update po_diff_matrix_temp
         set value1 = NULL,
             value2 = NULL,
             value3 = NULL,
             value4 = NULL,
             value5 = NULL,
             value6 = NULL,
             value7 = NULL,
             value8 = NULL,
             value9 = NULL,
             value10 = NULL,
             value11 = NULL,
             value12 = NULL,
             value13 = NULL,
             value14 = NULL,
             value15 = NULL,
             value16 = NULL,
             value17 = NULL,
             value18 = NULL,
             value19 = NULL,
             value20 = NULL,
             value21 = NULL,
             value22 = NULL,
             value23 = NULL,
             value24 = NULL,
             value25 = NULL,
             value26 = NULL,
             value27 = NULL,
             value28 = NULL,
             value29 = NULL,
             value30 = NULL;

      L_statement2 := 'select rt.pct, rt.diff_'||L_diff_z_no||', rt.diff_'||L_diff_y_no||', rt.diff_'||L_diff_x_no
                    ||' from diff_ratio_detail rt'
                    ||' where rt.diff_ratio_id = :I_diff_ratio_id'
                    ||' and NVL(rt.store, -999) = NVL(:I_store, -999)'
                    ||' and diff_'||L_diff_z_no || ' is not null'
                    ||' and diff_'||L_diff_z_no ||' in ( select diff_id  from diff_group_detail , diff_ratio_head h where h.diff_ratio_id = rt.diff_ratio_id and  diff_group_id in (h.diff_group_1,h.diff_group_2,h.diff_group_3))'
                    ||' and diff_'||L_diff_y_no || ' is not null'
                    ||' and diff_'||L_diff_y_no ||' in ( select diff_id  from diff_group_detail , diff_ratio_head h where h.diff_ratio_id = rt.diff_ratio_id and  diff_group_id in (h.diff_group_1,h.diff_group_2,h.diff_group_3))'
                    ||' and diff_'||L_diff_x_no|| ' is not null'
                    ||' and diff_'||L_diff_x_no ||' in ( select diff_id  from diff_group_detail , diff_ratio_head h where h.diff_ratio_id = rt.diff_ratio_id and  diff_group_id in (h.diff_group_1,h.diff_group_2,h.diff_group_3))';

      if I_range_id is NOT NULL then
         L_statement2 := L_statement2||' and exists (select ''x'''
                         ||' from diff_range_detail rg'
                         ||' where rg.diff_range = :I_range_id'
                         ||' and rt.diff_'||L_diff_x_no||' = rg.diff_'||I_range_x_no
                         ||' and rt.diff_'||L_diff_y_no||' = rg.diff_'||I_range_y_no;

         if I_range_z_no is NOT NULL then
            L_statement2 := L_statement2|| ' and rt.diff_'||L_diff_z_no||'= rg.diff_'||I_range_z_no||')';
         else
            L_statement2 := L_statement2||')';
         end if;
      end if;

      if I_range_id is NOT NULL then
         EXECUTE IMMEDIATE L_statement2 USING I_diff_ratio_id, I_store, I_range_id;
         open C_XYZ_QTY for L_statement2 USING I_diff_ratio_id, I_store, I_range_id;
      else
         EXECUTE IMMEDIATE L_statement2 USING I_diff_ratio_id, I_store;
         open C_XYZ_QTY for L_statement2 USING I_diff_ratio_id, I_store;
      end if;
      LOOP
         fetch C_XYZ_QTY into L_xy_qty, L_diff_z, L_diff_y, L_diff_x;

         EXIT when C_XYZ_QTY%NOTFOUND;

         open C_SEQ_NO;
         fetch C_SEQ_NO into L_seq_no, L_diff_z;
         close C_SEQ_NO;

         if L_seq_no is NOT NULL then
            L_statement3 := 'update po_diff_matrix_temp'
                          ||' set value'||L_seq_no||' = :L_xy_qty'
                          ||' where diff_y = :L_diff_y'
                          ||' and diff_z = :L_diff_z';
            
            EXECUTE IMMEDIATE L_statement3 USING L_xy_qty, L_diff_y, L_diff_z;
         end if;
      end LOOP;
      close C_XYZ_QTY;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END APPLY_DIFF_RATIO;
---------------------------------------------------------------------------------
FUNCTION APPLY_RATIO_TO_XY_DISTRIBUTION(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_diff_ratio_id   IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE,
                                        I_store           IN     DIFF_RATIO_DETAIL.STORE%TYPE,
                                        I_diff_z_id       IN     DIFF_Z_TEMP.DIFF_Z%TYPE,
                                        I_diff_x_no       IN     NUMBER,
                                        I_diff_y_no       IN     NUMBER,
                                        I_diff_z_no       IN     NUMBER,
                                        I_range_id        IN     DIFF_RANGE_DETAIL.DIFF_RANGE%TYPE,
                                        I_range_x_no      IN     NUMBER,
                                        I_range_y_no      IN     NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.APPLY_RATIO_TO_XY_DISTRIBUTION';
   TYPE QTY_CURSOR is REF CURSOR;
   C_XY_QTY           QTY_CURSOR;
   L_xy_qty           DIFF_RATIO_DETAIL.PCT%TYPE;
   L_diff_y           DIFF_IDS.DIFF_ID%TYPE;
   L_diff_x           DIFF_IDS.DIFF_ID%TYPE;
   L_statement        VARCHAR2(4000);
   L_statement2       VARCHAR2(4000);
   L_seq_no           DIFF_X_TEMP.SEQ_NO%TYPE;
   L_inv_parm         VARCHAR2(30)      := NULL;
   L_diff_x_no        NUMBER(1);
   L_diff_y_no        NUMBER(1);
   L_diff_z_no        NUMBER(1);
   cursor C_SEQ_NO is
      select seq_no
        from diff_x_temp
       where diff_x = L_diff_x
         and NVL(diff_z, '-999') = NVL(I_diff_z_id,'-999');
BEGIN


   if I_diff_x_no is NULL  and
      I_diff_y_no is NULL then
      L_inv_parm := 'I_diff_x_no or I_diff_y_no';
   elsif I_diff_ratio_id is NULL then
      L_inv_parm := 'I_diff_ratio_id';
   elsif I_diff_z_no is not NULL and
         I_diff_z_id is NULL then
      L_inv_parm := 'I_diff_z_id';
   end if;
   if I_range_id is NOT NULL and
      I_range_x_no is NULL and
      I_range_y_no is NULL then
      L_inv_parm := 'I_range_x_no or I_range_y_no ';
   end if;
   if L_inv_parm is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            L_inv_parm,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   --
   if I_diff_x_no is NULL then
      L_diff_x_no := 2;
   else
      L_diff_x_no := I_diff_x_no;
   end if;
   if I_diff_y_no is NULL then
      L_diff_y_no := 2;
   else
      L_diff_y_no := I_diff_y_no;
   end if;
   if I_diff_z_no is NULL then
      L_diff_z_no := 3;
   else
      L_diff_z_no := I_diff_z_no;
   end if;

   L_statement := 'select rt.pct,rt.diff_'||L_diff_y_no||', rt.diff_'||L_diff_x_no
                  ||' from diff_ratio_detail rt'
                  ||' where rt.diff_ratio_id = :I_diff_ratio_id'
                  ||' and NVL(rt.store, -999) = NVL(:I_store, -999)'
                  ||' and diff_'||L_diff_z_no || ' is null';
   if I_diff_x_no is not NULL and
      I_diff_y_no is not NULL then
      L_statement := L_statement||' and (diff_'||L_diff_y_no ||' is not null'
                     ||' and  diff_'||L_diff_y_no ||' in ( select diff_id  from diff_group_detail , diff_ratio_head h where h.diff_ratio_id = rt.diff_ratio_id and  diff_group_id in (h.diff_group_1,h.diff_group_2))'
                     ||' and diff_'||L_diff_x_no||' is not null'
                     ||' and  diff_'||L_diff_x_no ||' in ( select diff_id  from diff_group_detail , diff_ratio_head h where h.diff_ratio_id = rt.diff_ratio_id and  diff_group_id in (h.diff_group_1,h.diff_group_2)))';
   else
      L_statement := L_statement||' and ((diff_'||L_diff_y_no ||' is not null'
                     ||' and diff_'||L_diff_y_no ||' in ( select diff_id  from diff_group_detail , diff_ratio_head h where h.diff_ratio_id = rt.diff_ratio_id and  diff_group_id =h.diff_group_1))'
                     ||' or (diff_'||L_diff_x_no||' is not null '
                     ||' and diff_'||L_diff_x_no ||' in ( select diff_id  from diff_group_detail , diff_ratio_head h where h.diff_ratio_id = rt.diff_ratio_id and  diff_group_id =h.diff_group_1)))';
   end if;
   if I_range_id is NOT NULL then
         L_statement := L_statement||' and exists (select ''x'''
                       ||' from diff_range_detail rg'
                       ||' where rg.diff_range = :I_range_id';
         if I_range_x_no is NOT NULL then
            L_statement := L_statement||' and rt.diff_'||L_diff_x_no||' = rg.diff_'||I_range_x_no||')';
         else
            L_statement := L_statement||' and rt.diff_'||L_diff_y_no||' = rg.diff_'||I_range_y_no||')';
         end if;
   end if;
   if I_range_id is NOT NULL then
      EXECUTE IMMEDIATE L_statement USING I_diff_ratio_id, I_store, I_range_id;
      open C_XY_QTY for L_statement USING I_diff_ratio_id, I_store, I_range_id;
   else
      EXECUTE IMMEDIATE L_statement USING I_diff_ratio_id, I_store;
      open C_XY_QTY for L_statement USING I_diff_ratio_id, I_store;
   end if;
   -- clear the table xy_diff_ratio_temp
   delete from xy_diff_ratio_temp;
   LOOP
      fetch C_XY_QTY into L_xy_qty, L_diff_y, L_diff_x;
      EXIT when C_XY_QTY%NOTFOUND;
      if L_diff_x is NOT NULL AND
         L_diff_y is NULL  then -- diffs associated only for X dimension
         insert into xy_diff_ratio_temp (diff_type,
                                         diff_id,
                                         value)
                                         values(
                                         'X',
                                         L_diff_x,
                                         L_xy_qty);
      elsif L_diff_x is  NULL AND
         L_diff_y is NOT NULL  then --  diffs associated only for Y dimension
         insert into xy_diff_ratio_temp (diff_type,
                                         diff_id,
                                         value)
                                         values(
                                         'Y',
                                         L_diff_y,
                                         L_xy_qty);
      else -- diffs associated for both  XY dimension
         open C_SEQ_NO;
         fetch C_SEQ_NO into L_seq_no;
         close C_SEQ_NO;

         if L_seq_no is NOT NULL then
            if I_diff_z_id is not NULL then
                L_statement2 := 'update po_diff_matrix_temp'
                               ||' set value'||L_seq_no||' = :L_xy_qty'
                               ||' where diff_y = :L_diff_y'
                               ||' and diff_z = :I_diff_z_id';
               EXECUTE IMMEDIATE L_statement2 USING L_xy_qty, L_diff_y, I_diff_z_id;
            else
               L_statement2 := 'update po_diff_matrix_temp'
                               ||' set value'||L_seq_no||' = :L_xy_qty'
                               ||' where diff_y = :L_diff_y';
               EXECUTE IMMEDIATE L_statement2 USING L_xy_qty, L_diff_y;
            end if;
         end if;
      end if;
   end LOOP;
   close C_XY_QTY;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END APPLY_RATIO_TO_XY_DISTRIBUTION;
-----------------------------------------------------------------------------
FUNCTION IS_DIFF_ASSOCIATED_WITH_STORE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_store_associated  OUT    BOOLEAN,
                                       I_diff_ratio_id     IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(100) := 'PO_DIFF_MATRIX_SQL.IS_DIFF_ASSOCIATED_WITH_STORE';
   cursor C_IS_STORE_ASSOCIATED is
      select 'Y'
        from diff_ratio_detail
       where diff_ratio_id = I_diff_ratio_id
         and store is not NULL;
   L_dummy          VARCHAR2(1);
BEGIN
   open C_IS_STORE_ASSOCIATED ;
   fetch C_IS_STORE_ASSOCIATED into L_dummy;
   close C_IS_STORE_ASSOCIATED;

   if L_dummy = 'Y' then
      O_store_associated := TRUE;
   else
      O_store_associated := FALSE;
   end if;

   return TRUE;
EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END IS_DIFF_ASSOCIATED_WITH_STORE;
-----------------------------------------------------------------------------
END PO_DIFF_MATRIX_SQL;
/
