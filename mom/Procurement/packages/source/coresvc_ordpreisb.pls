CREATE OR REPLACE PACKAGE BODY CORESVC_ORD_PREISSUE_SQL AS
-------------------------------------------------------------------------------------------------------
FUNCTION GENERATE_ORDER_NOS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_businessObject   IN OUT   "RIB_OrdNumColDesc_REC",
                            I_SvcOpContext     IN       "RIB_ServiceOpContext_REC",
                            I_businessObject   IN       "RIB_OrdNumCriVo_REC")
RETURN BOOLEAN IS

   L_program           VARCHAR2(60)          := 'CORESVC_ORD_PREISSUE_SQL.GENERATE_ORDER_NOS';
   L_ordpre_rec        "RIB_OrdNumCriVo_REC" := NULL;
   L_vdate             DATE                  := get_vdate();
   L_max_quantity      SVC_ORDER_PARAMETER_CONFIG.MAX_ORDER_NO_QTY%TYPE;
   L_max_expiry_days   SVC_ORDER_PARAMETER_CONFIG.MAX_ORDER_EXPIRY_DAYS%TYPE;
   L_sups              V_SUPS%ROWTYPE;
   L_supp_exists       BOOLEAN;
   L_expiry_date       ORD_PREISSUE.EXPIRY_DATE%TYPE;
   L_supp_sites_ind    SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE;

BEGIN

   L_ordpre_rec := treat(I_businessObject AS "RIB_OrdNumCriVo_REC");

   if L_ordpre_rec is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   select max_order_no_qty,
          max_order_expiry_days
     into L_max_quantity,
          L_max_expiry_days
     from svc_order_parameter_config;
     
   select supplier_sites_ind
     into L_supp_sites_ind
     from system_options;

   if L_ordpre_rec.supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'Supplier');
      return FALSE;
   end if;

   if L_ordpre_rec.quantity is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'Quantity');
      return FALSE;
   end if;

   if L_ordpre_rec.expiry_days is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'Expiry Days');
      return FALSE;
   end if;

   if L_ordpre_rec.supplier is NOT NULL then
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_SUPPLIER(O_error_message,
                                                   L_supp_exists,
                                                   L_sups,
                                                   L_ordpre_rec.supplier) = FALSE then
         return FALSE;
      end if;
      --
      if L_supp_sites_ind = 'Y' then
         if L_supp_exists = FALSE then
            if L_sups.sup_name is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER_SITE',
                                                     NULL,
                                                     NULL,
                                                     NULL);
            end if;
            return FALSE;
         end if;
         if L_sups.supplier_parent is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER_SITE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      else
         if L_supp_exists = FALSE then
            return FALSE;
         end if;
      end if;
      --
      if L_sups.sup_status != 'A' then
         O_error_message := SQL_LIB.CREATE_MSG('INACTIVE_SUPPLIER',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   if L_ordpre_rec.expiry_days is NOT NULL then
      if L_ordpre_rec.expiry_days <= 0 then
         O_error_message := SQL_LIB.CREATE_MSG('DAYS_UNTIL_EXPIRE',
                                               L_ordpre_rec.expiry_days,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      if L_ordpre_rec.expiry_days > L_max_expiry_days then
         O_error_message := SQL_LIB.CREATE_MSG('EXCEED_MAX_EXPIRY_DAYS',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   if L_ordpre_rec.quantity is NOT NULL then
      if L_ordpre_rec.quantity <= 0 then
         O_error_message := SQL_LIB.CREATE_MSG('QTY_GREATER_ZERO',
                                               L_ordpre_rec.quantity,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      if L_ordpre_rec.quantity > L_max_quantity then
         O_error_message := SQL_LIB.CREATE_MSG('EXCEED_MAX_ORDER_QTY',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   if L_ordpre_rec.quantity > 0 then
      L_expiry_date := L_vdate + L_ordpre_rec.expiry_days;
      if ORDER_NUMBER_SQL.PREISSUE (O_error_message,
                                    O_businessObject,
                                    L_ordpre_rec.quantity,
                                    L_expiry_date,
                                    L_ordpre_rec.supplier,
                                    USER) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GENERATE_ORDER_NOS;
-------------------------------------------------------------------------------------------------------
END CORESVC_ORD_PREISSUE_SQL;
/
