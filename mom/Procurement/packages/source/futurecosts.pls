CREATE OR REPLACE PACKAGE FUTURE_COST_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
-- Procedure Name: PROCESS_COST_EVENTS
-- Purpose       : Calls the appropriate function based on cost event type to process event.
----------------------------------------------------------------------------------------
PROCEDURE PROCESS_COST_EVENTS(I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                              O_return_code              OUT NUMBER,
                              O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE);
----------------------------------------------------------------------------------------
-- Procedure Name: PROCESS_COST_EVENTS
-- Purpose       : Wrapper procedure to call FUTURE_COST_SQL.PROCESS_COST_EVENTS internally .
--                 This procedure is called from DBMS_SCHEDULER  
----------------------------------------------------------------------------------------
PROCEDURE PROCESS_COST_EVENTS(I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE);
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
-- Function Name:  PURGE_FUTURE_COST
-- Purpose      :  Purge data from the future cost table based.
----------------------------------------------------------------------------------------
FUNCTION PURGE_FUTURE_COST(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
-- Function Name:  PURGE_DEAL_ITEM_LOC_EXPLODE
-- Purpose      :  Purge data from the deal_item_loc_explode table.
----------------------------------------------------------------------------------------
FUNCTION PURGE_DEAL_ITEM_LOC_EXPLODE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN;     
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
-- compile private functions as public for unit testing
$if $$UTPLSQL=TRUE $then

   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_NIL
   -- Purpose      : Adds new item/location records into future cost table.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_NIL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MERGE_NIL_CC
   -- Purpose      : Adds future cost changes for new item/location.
   ----------------------------------------------------------------------------------------
   FUNCTION MERGE_NIL_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                         I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MERGE_NIL_RECLASS
   -- Purpose      : Adds future reclass records for new item/location.
   ----------------------------------------------------------------------------------------
   FUNCTION MERGE_NIL_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MERGE_DEAL
   -- Purpose      : Adds all applicable deal records for new item/location.
   ----------------------------------------------------------------------------------------
   FUNCTION MERGE_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_COST_CHANGE
   -- Purpose      : Calls appropriate function to process cost change based on the action type.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_COST_CHANGE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                I_action                IN     COST_EVENT.ACTION%TYPE,                               
                                I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: REMOVE_CC
   -- Purpose      : Removes cost changes from the future cost table.
   ----------------------------------------------------------------------------------------
   FUNCTION REMOVE_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                      I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: ADD_CC
   -- Purpose      : Adds cost changes to the future cost table.
   ----------------------------------------------------------------------------------------
   FUNCTION ADD_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                   I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_CC
   -- Purpose      : Populate future_cost_temp with future cost records related to the cost 
   --                change on item/supplier/country and item/supplier/country/location.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MERGE_CC
   -- Purpose      : Find future cost event records on the timeline on the same date or prior 
   --                to the cost change. Records on the same date as the cost change will have
   --                their cost values updated with the values on the cost change. If only 
   --                prior records exists, then insert a new row for the cost change.
   ----------------------------------------------------------------------------------------
   FUNCTION MERGE_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_RECLASS
   -- Purpose      : Calls appropriate function to process reclass based on the action type.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                            I_action                IN     COST_EVENT.ACTION%TYPE,                            
                            I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: ADD_RECLASS
   -- Purpose      : Insert rows into future cost records affected by reclass.
   ----------------------------------------------------------------------------------------
   FUNCTION ADD_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: REMOVE_RECLASS
   -- Purpose      : Remove reclass records from future cost table.
   ----------------------------------------------------------------------------------------
   FUNCTION REMOVE_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MERGE_RECLASS
   -- Purpose      : Insert a row for the reclass on the timeline or if a row exists on the
   --                same date as the reclass, update the row with the reclass number and
   --                the new merch hier.
   ----------------------------------------------------------------------------------------
   FUNCTION MERGE_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: ELC_DATE_TO_FC
   -- Purpose      : Insert a row for the ELC change on the timeline that is vdate, or if a 
   --                row exists on the same date then do not insert a row. 
   ----------------------------------------------------------------------------------------
   FUNCTION ELC_DATE_TO_FC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_MERGE_MERCH_HIER
   -- Purpose      : Explodes merchandise hierarchy changes to all affected 
   --                item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_MERGE_MERCH_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_MERGE_ORG_HIER
   -- Purpose      : Explodes organization hierarchy changes to all affected 
   --                item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_MERGE_ORG_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                   I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_MERGE_SUPP_HIER
   -- Purpose      : Explodes supplier hierarchy changes to all affected 
   --                item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_MERGE_SUPP_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                    I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_ELC
   -- Purpose      : Explodes ELC to all affected item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_ELC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_COST_ZONE
   -- Purpose      : Explodes cost zone changes to all affected item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_COST_ZONE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_ITEM_CZG
   -- Purpose      : Explodes item's cost zone group changes to all affected
   --                item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_ITEM_CZG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_DEAL
   -- Purpose      : Calls appropriate function to process deal-effect based on the action type.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                         I_action                IN     COST_EVENT.ACTION%TYPE,                         
                         I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_PRIMARY_PACK_COST
   -- Purpose      : Explodes primary pack cost changes to all affected 
   --                item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_PRIMARY_PACK_COST(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                      I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                                      I_persist_ind           IN     COST_EVENT.PERSIST_IND%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: REMOVE_DEAL
   -- Purpose      : Removes deals from the future cost table.
   ----------------------------------------------------------------------------------------
   FUNCTION REMOVE_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: ADD_DEAL
   -- Purpose      : Adds deals to the future cost table.
   ----------------------------------------------------------------------------------------
   FUNCTION ADD_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MODIFY_DEAL
   -- Purpose      : Modifies deals close date on future cost tables.
   ----------------------------------------------------------------------------------------
   FUNCTION MODIFY_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: DEAL_DATES_TO_FC
   -- Purpose      : Adds rows into future cost for the start and end (day after end) of
   --                all the deals affected by the current deal for roll forward to process.
   ----------------------------------------------------------------------------------------
   FUNCTION DEAL_DATES_TO_FC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: LOAD_DEALS
   -- Purpose      : Moves existing deal records to the deal gtt.
   ----------------------------------------------------------------------------------------
   FUNCTION LOAD_DEALS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: LOAD_FUTURE_COST_FOR_DEALS
   -- Purpose      : Gets future cost records covered by the deal being modified/removed 
   --                into the future cost global temporary table.
   ----------------------------------------------------------------------------------------
   FUNCTION LOAD_FUTURE_COST_FOR_DEALS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                       I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: SYNC_PRIMARY_PACK
   -- Purpose      : Create future_cost_temp rows for items that have primary costing packs 
   --                based off of the primary costing pack rows.
   ----------------------------------------------------------------------------------------
   FUNCTION SYNC_PRIMARY_PACK(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                             I_persist_ind           IN     COST_EVENT.PERSIST_IND%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: PROCESS_SUPP_CNTR
   -- Purpose      : Add/remove supplier/country records into future cost and future cost deal.
   ----------------------------------------------------------------------------------------
   FUNCTION PROCESS_SUPP_CNTR(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_action                IN     COST_EVENT.ACTION%TYPE,
                              I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ------------------------------------------------------------------------------------------------------- 
   -- Function Name : EXPLODE_DEAL_PASSTHRU 
   -- Purpose       : Calls appropriate function to process deal_passthru for the affected company stores.
   -------------------------------------------------------------------------------------------------------                           
   FUNCTION EXPLODE_DEAL_PASSTHRU(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                  I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;   
   ------------------------------------------------------------------------------------------------------- 
   -- Function Name : EXPLODE_TMPL_CHG 
   -- Purpose       : Calls appropriate function to process template changes for the affected company stores.
   -------------------------------------------------------------------------------------------------------                           
   FUNCTION EXPLODE_TMPL_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;   
   ------------------------------------------------------------------------------------------------------- 
   -- Function Name : EXPLODE_TMP_RELATIONSHIP_CHG 
   -- Purpose       : Calls appropriate function to process template relationship changes based on the action type 
   --                 for the company stores.
   -----------------------------------------------------------------------------------------------------------------                           
   FUNCTION EXPLODE_TMP_RELATIONSHIP_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                          I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                          I_action                IN     COST_EVENT.ACTION%TYPE,
                                          I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   
   -------------------------------------------------------------------------------------------------------
   -- Function Name : COMPUTE_CC_TAX
   -- Purpose       : Calls the tax engine to compute negotiated_item_cost, extended_base_cost and wac_tax
   --                 values for cost change related events.
   -----------------------------------------------------------------------------------------------------------------
   FUNCTION COMPUTE_CC_TAX(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------------------------------------------

   -----------------------------------------------------------------------------------------------------------------   
   -- Function Name : WRITE_ERROR 
   -- Purpose       : Writes error messages to the cost_event_result table.
   ----------------------------------------------------------------------------------------

   PROCEDURE WRITE_ERROR(I_error_message         IN RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                         I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE);
   ----------------------------------------------------------------------------------------
   -- Procedure Name: WRITE_SUCCESS 
   -- Purpose       : Writes success messages to the cost_event_result table.
   ----------------------------------------------------------------------------------------
   PROCEDURE WRITE_SUCCESS(I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE); 

   ----------------------------------------------------------------------------------------
   -- Function Name: GET_CURRENT_ELC
   -- Purpose      : This function gets the current elc records of the cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_CURRENT_ELC(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id   IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                            I_thread_id               IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: CALC_FUTURE_ELC
   -- Purpose      : This function calculates the new elc and hts records.
   ----------------------------------------------------------------------------------------
   FUNCTION CALC_FUTURE_ELC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: DEAL_THLD_CHG_DATE_TO_FC
   -- Purpose      : Insert a row on the vdate date when deal threshold is edited, or if a 
   --                row exists on the same date then do not insert a row. 
   ----------------------------------------------------------------------------------------
   FUNCTION DEAL_THLD_CHG_DATE_TO_FC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                     I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   --Function Name: RECALC_ELC
   --Purpose      : Function to calculate all item expenses and assessments.
   -------------------------------------------------------------------------------
   FUNCTION RECALC_ELC(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_est_value            IN OUT   NUMBER,
                       I_dtl_flag             IN       VARCHAR2,
                       I_comp_id              IN       ELC_COMP.COMP_ID%TYPE,
                       I_calc_type            IN       VARCHAR2,
                       I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                       I_supplier             IN       SUPS.SUPPLIER%TYPE,
                       I_item_exp_type        IN       ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                       I_item_exp_seq         IN       ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                       I_hts                  IN       HTS.HTS%TYPE,
                       I_import_country_id    IN       COUNTRY.COUNTRY_ID%TYPE,
                       I_origin_country_id    IN       COUNTRY.COUNTRY_ID%TYPE,
                       I_effect_from          IN       HTS.EFFECT_FROM%TYPE,
                       I_effect_to            IN       HTS.EFFECT_TO%TYPE,
                       I_new_cost             IN       FUTURE_COST.BASE_COST%TYPE DEFAULT NULL,
                       I_active_date          IN       DATE DEFAULT NULL,
                       I_location             IN       NUMBER DEFAULT NULL)
      RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   --Function Name: UPDATE_FC_TARIFF_RATES
   --Purpose      : Function to update future cost tariff rates.
   -----------------------------------------------------------------------------------------
   FUNCTION UPDATE_FC_TARIFF_RATES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_calc_type           IN       VARCHAR2,
                                   I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                                   I_seq_no              IN       ORDSKU_HTS.SEQ_NO%TYPE,
                                   I_item                IN       ITEM_MASTER.ITEM%TYPE,
                                   I_pack_item           IN       ITEM_MASTER.ITEM%TYPE,
                                   I_supplier            IN       SUPS.SUPPLIER%TYPE,
                                   I_hts                 IN       HTS.HTS%TYPE,
                                   I_import_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                                   I_origin_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                                   I_effect_from         IN       HTS.EFFECT_FROM%TYPE,
                                   I_effect_to           IN       HTS.EFFECT_TO%TYPE,
                                   I_comp_id             IN       ELC_COMP.COMP_ID%TYPE)
      RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name : EXPLODE_FC
   -- Purpose       : Explodes item/supp/county/loc for the change of costing location of a franchise store
   -------------------------------------------------------------------------------------------------------                           
   FUNCTION EXPLODE_FC(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;   
   ------------------------------------------------------------------------------------------------------- 

   FUNCTION MERGE_FC     (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ------------------------------------------------------------------------------------------------------- 
   -- Function Name : PROCESS_PRICE_CHG 
   -- Purpose       : Calls appropriate function to process price changes for the affected franchise stores
   --                 if a valid retail template is active during the same period
   -------------------------------------------------------------------------------------------------------                           
   FUNCTION PROCESS_PRICE_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;  
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_PC
   -- Purpose      : Populates future_cost_temp with future cost records related to the price 
   --                change for franchice stores whose pricing cost will be recalculated.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_PC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   
   ----------------------------------------------------------------------------------------
   -- Function Name: MERGE_PC
   -- Purpose      : Find future cost event records on the timeline on the same date or prior
   --                to the price change of franchise stores. Records on the same date as the 
   --                price change will have their pricing cost values updated  if a retail 
   --                based template is active during that time frame.
   ----------------------------------------------------------------------------------------
   FUNCTION MERGE_PC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   
$end
----------------------------------------------------------------------------------------
END FUTURE_COST_SQL;
/