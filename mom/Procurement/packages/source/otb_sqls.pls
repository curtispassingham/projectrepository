CREATE OR REPLACE PACKAGE OTB_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------
--These spec variables are used throughout the ordering
--dialogue.  They are populated by OTB_SQL.ORD_INFO.

   LP_order_type      ordhead.order_type%TYPE          := NULL;
   LP_order_eow_date  DATE                             := NULL;
   LP_period_ahead    NUMBER(10)               := NULL;
   LP_week_no         otb.week_no%TYPE                 := NULL;
   LP_month_no        otb.month_no%TYPE                := NULL;
   LP_half_no         otb.half_no%TYPE                 := NULL;
   LP_supplier        ordhead.supplier%TYPE            := NULL;
   LP_order_no        ordhead.order_no%TYPE            := NULL;
   LP_sku             item_master.item%TYPE            := NULL;

----------------------------------------------------------------------------
-- Function:  CALC_COMP_TOTAL (internal only)
-- Purpose:   Calculates the cost and retail for a item on an order.
-- Calls:     ORDER_CALC_SQL.ITEM_COMP_COST
--            CURRENCY_SQL.CONVERT_BY_LOCATION
-----------------------------------------------------------------------------
--Function Name : get_purge_date
--Purpose : This function gets the eow_date for the current 
--    week two halves ago.  This date is passed to 
--    otb_purge.pc as the purge guideline (any row with an
--    older eow_date is deleted).
--Calls : CAL_TO_454_HALF, 
--        HALF_TO_454_LDOH
-----------------------------------------------------------------------------
FUNCTION GET_PURGE_DATE (O_purge_eow      IN OUT  DATE,
                         O_error_message  IN OUT  VARCHAR2)
                         RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : otb_inserts
--Purpose : This function accepts order_type, dept, class, 
--    subclass, eow_date and budget_amt from otb_import.pc.  
--    Half_no, month_no and week_no are calculated by calling
--    CAL_TO_454_HALF and CAL_TO_454 using eow_date.
--    If a row exists on otb for the passed order type/dept/class
--    subclass/eow_date, then the budget_amt column for the 
--    specified order type is overwritten with the new value 
--    passed from the import file.  If no row is found by the
--    cursor, a new row is inserted with the passed values and
--    calculated date values (all other columns are inserted as 0). 
--Calls:  CAL_TO_454_HALF,
--        CAL_TO_454
-----------------------------------------------------------------------------
FUNCTION OTB_INSERTS (I_order_type      IN      VARCHAR2,
                      I_dept            IN      NUMBER,
                      I_class           IN      NUMBER,
                      I_subclass        IN      NUMBER,
                      I_eow_date        IN      DATE,
                      I_budget_amt      IN      NUMBER,
                      O_error_message   IN OUT  VARCHAR2)
                      RETURN BOOLEAN; 
-----------------------------------------------------------------------------
--Function Name : ord_info
--Purpose : This function populates several spec variables 
--          with information relevant to the order and used by
--          other ORD functions in this package.
--Calls : CAL_TO_454_LDOW, 
--        CAL_TO_454, 
--        CAL_TO_454_HALF
-----------------------------------------------------------------------------
FUNCTION ORD_INFO (I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
                   RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : ORD_CHECK
--Purpose : This function checks to see if an order will put any
--          subclasses on it over budget.  If so, a list is created and 
--          passed to the ordering form, which will alert the user.
--Internal: COMPARE_BUDGET
--Calls : OTB_SQL.ORD_INFO, 
--        SYSTEM_OPTIONS.GET_LC_OTB_IND,
--        OTB_SQL.CALC_COST_RETAIL
-----------------------------------------------------------------------------
FUNCTION ORD_CHECK (I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                    O_subclass_list   IN OUT   VARCHAR2,
                    O_excess_ind      IN OUT   VARCHAR2,
                    O_error_message   IN OUT   VARCHAR2)
                    RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : ORD_APPROVE
--Purpose       : This function calculates an order amount for each subclass
--                on the order and increments the approved_amt of the appropriate
--                column on the otb table (based on V_order_type).  If there is
--                no row on otb, a new one is inserted. 
--Internal      : 
--Calls         : OTB_SQL.INIT_ORD_BULK
--                OTB_SQL.BUILD_ORD_APPROVE
--                OTB_SQL.FLUSH_ORD_BULK
-----------------------------------------------------------------------------
FUNCTION ORD_APPROVE (I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                      O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : ORD_UNAPPROVE
--Purpose       : This function calculates an order amount for each subclass
--                on the order being cancelled and decrements the approved_amt
--                of the appropriate column on the otb table (based on 
--                V_order_type).  If there is no row on otb, the program is
--                exited successfully (nothing is done).
--Internal      : 
--Calls         : OTB_SQL.INIT_ORD_BULK
--                OTB_SQL.BUILD_ORD_UNAPPROVE
--                OTB_SQL.FLUSH_ORD_BULK
-----------------------------------------------------------------------------
FUNCTION ORD_UNAPPROVE (I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                        O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : ORD_CANCEL_REINSTATE
--Purpose       : This function calculates an order amount for each subclass
--                on the order being cancelled, decrements the approved_amt
--                of the appropriate column on the otb table (based on V_order_type),
--                and increments the cancel_amt on otb.  If there is no row on otb,
--                the program is exited successfully (nothing is done).
--Internal      : 
--Calls         : OTB_SQL.INIT_ORD_BULK
--                OTB_SQL.BUILD_ORD_CANCEL_REINSTATE
--                OTB_SQL.FLUSH_ORD_BULK
-----------------------------------------------------------------------------
FUNCTION ORD_CANCEL_REINSTATE  (O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no              IN     ordhead.order_no%TYPE,
                               I_item                  IN     ordloc.item%TYPE,
                               I_location              IN     ordloc.location%TYPE,
                               I_loc_type              IN     ordloc.loc_type%TYPE,
                               I_cancel_reinstate_qty  IN     ordloc.qty_cancelled%TYPE,
                               I_cancel_reinstate_flag IN     VARCHAR2)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : ord_receive
--Purpose : This function updates the OTB table when orders are received
--      by incrementing the approved column for any overage receipts 
--      and the receipts column for ALL receipts.
-----------------------------------------------------------------------------
FUNCTION ORD_RECEIVE(O_error_message   IN OUT VARCHAR2,
                     I_unit_retail     IN     ordloc.unit_retail%TYPE,
                     I_unit_cost_duty  IN     ordloc.unit_cost%TYPE,
                     I_order_no        IN     ordsku.order_no%TYPE,
                     I_dept            IN     deps.dept%TYPE,
                     I_class           IN     class.class%TYPE,
                     I_subclass        IN     subclass.subclass%TYPE,
                     I_receipt_qty     IN     ordloc.qty_received%TYPE,
                     I_approved_qty    IN     ordloc.qty_ordered%TYPE)
         RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Function Name: ITEM_COST (Internal Only)
-- Purpose:       calculates landed cost for all orders containing
--                a given item.
-- Called By:     OTB_SQL.RECLASS
-- Calls:         ORDER_CALC_SQL.ITEM_COMP_COST
-----------------------------------------------------------------------------
-- Function Name: OTB_item_RETAIL (Internal Only)
-- Purpose:       calculates total retail for all orders containing a given
--                item.
-- Called By:     OTB_SQL.RECLASS
-- Calls:         CURRENCY_SQL.CONVERT_BY_LOCATION
-----------------------------------------------------------------------------
--Function Name : RECLASS
--Purpose :       This function will update the OTB tables when an item is  
--                Reclassified 
--Internal:       CALC_VALUES,
--                RECLASS_SUB
--Calls :         OTB_SQL.ITEM_COST,
--                OTB_SQL.OTB_ITEM_RETAIL,
--                CURRENCY_SQL.CONVERT_BY_LOCATION
--                SYSTEM_OPTIONS_SQL.GET_LC_OTB_IND
-----------------------------------------------------------------------------
FUNCTION RECLASS(O_error_message IN OUT VARCHAR2,
                 O_locked        IN OUT BOOLEAN,
                 I_reclass_date  IN     DATE,
                 I_item          IN     ordsku.item%TYPE,
                 I_old_dept      IN     otb.dept%TYPE, 
                 I_old_class     IN     otb.class%TYPE, 
                 I_old_subclass  IN     otb.subclass%TYPE, 
                 I_new_dept      IN     otb.dept%TYPE, 
                 I_new_class     IN     otb.class%TYPE, 
                 I_new_subclass  IN     otb.subclass%TYPE)
                 RETURN BOOLEAN;

-----------------------------------------------------------------------------
FUNCTION BUILD_ORD_RECEIVE(O_error_message   IN OUT VARCHAR2,
                           I_unit_retail     IN     ORDLOC.UNIT_RETAIL%TYPE,
                           I_unit_cost_duty  IN     ORDLOC.UNIT_COST%TYPE,
                           I_order_no        IN     ORDSKU.ORDER_NO%TYPE,
                           I_dept            IN     DEPS.DEPT%TYPE,
                           I_class           IN     CLASS.CLASS%TYPE,
                           I_subclass        IN     SUBCLASS.SUBCLASS%TYPE,
                           I_receipt_qty     IN     ORDLOC.QTY_RECEIVED%TYPE,
                           I_approved_qty    IN     ORDLOC.QTY_ORDERED%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION INIT_ORD_RECEIVE(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION FLUSH_ORD_RECEIVE(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : INIT_ORD_BULK
--Purpose       : This function initializes local OTB package variables.
--Internal      :
--Calls         :
-----------------------------------------------------------------------------
FUNCTION INIT_ORD_BULK(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_type          IN     VARCHAR2,
                       I_otb_size      IN     NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : FLUSH_ORD_BULK
--Purpose       : This function inserts/updates OTB transactions queued in
--                the index-by tables.
--Internal      :
--Calls         :
-----------------------------------------------------------------------------
FUNCTION FLUSH_ORD_BULK(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_type               IN     VARCHAR2)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : GET_BULK_SIZE
--Purpose       : This function returns value of variable LP_otb_size_?
--                indicating the size of the index-by table. 
--Internal      :
--Calls         :
-----------------------------------------------------------------------------
FUNCTION GET_BULK_SIZE (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_otb_size      IN OUT NUMBER,
                        I_type          IN     VARCHAR2)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : BUILD_ORD_APPROVE
--Purpose       : This function calculates an order amount for each subclass
--                on the order and queues the approved_amt in the appropriate
--                otb index-by tables (based on V_order_type).
--                Queued OTB transactions in the index-by tables will be 
--                inserted/updated into OTB table through OTB_SQL.FLUSH_ORD_BULK.
--Internal      : APPROVE_INSERT
--Calls         : OTB_SQL.ORD_INFO, 
--                SYSTEM_OPTIONS.GET_LC_OTB_IND,
--                OTB_SQL.CALC_COST_RETAIL
-----------------------------------------------------------------------------
FUNCTION BUILD_ORD_APPROVE (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no           IN     ORDSKU.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : BUILD_ORD_UNAPPROVE
--Purpose       : This function calculates an order amount for each subclass
--                on the order being cancelled and queues the (negative) 
--                approved_amt in the appropriate otb index-by tables
--                (based on V_order_type).  Queued OTB transactions in the 
--                index-by tables will be updated (in this case, decremented)
--                from the OTB table through OTB_SQL.FLUSH_ORD_BULK.
--Internal      : UNAPPROVE_INSERT
--Calls         : OTB_SQL.ORD_INFO, 
--                SYSTEM_OPTIONS.GET_LC_OTB_IND,
--                OTB_SQL.CALC_COST_RETAIL
-----------------------------------------------------------------------------
FUNCTION BUILD_ORD_UNAPPROVE (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no        IN     ORDSKU.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : BUILD_ORD_CANCEL_REINSTATE
--Purpose       : This function calculates an order amount for each subclass
--                on the order being cancelled, queues the (negative)approved_amt
--                and (positive)cancel_amt in the appropriate  
--                otb index-by tables (based on V_order_type), And does the 
--                reverse logic if reinstating item back to the order. 
--                OTB transactions queued in the  index-by tables will be 
--                updated into the OTB table through OTB_SQL.FLUSH_ORD_BULK.
--Internal      : ORD_CANC_REIN_ITEM
--Calls         : OTB_SQL.ORD_INFO,
--                SYSTEM_OPTIONS_SQL.GET_LC_OTB_IND,
--                ITEM_ATTRIB_SQL.GET_MERCH_HIER,
--                ORDER_CALC_SQL.ITEM_COMP_COST,
--                OTB_SQL.ITEM_COST
--                CURRENCY_SQL.CONVERT_BY_LOCATION                    
-----------------------------------------------------------------------------
FUNCTION BUILD_ORD_CANCEL_REINSTATE (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                                     I_item                  IN     ORDLOC.item%TYPE,
                                     I_location              IN     ORDLOC.LOCATION%TYPE,
                                     I_loc_type              IN     ORDLOC.LOC_TYPE%TYPE,
                                     I_cancel_reinstate_qty  IN     ORDLOC.QTY_CANCELLED%TYPE,
                                     I_cancel_reinstate_flag IN     VARCHAR2)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : ORD_APPROVE_CASCADE
--Purpose       : This function calculates an order amount for each subclass
--                on the order and inserts into OTB_CASCADE_STG table. The the 
--                batch_ordcostcompupd.ksh will post the details in the 
--                staging table to the OTB table.
--Internal      : 
--Calls         : OTB_SQL.INIT_ORD_BULK
--                OTB_SQL.BUILD_ORD_APPROVE
--                OTB_SQL.FLUSH_ORD_BULK
-----------------------------------------------------------------------------
FUNCTION ORD_APPROVE_CASCADE (I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                              O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name : ORD_UNAPPROVE_CASCADE
--Purpose       : This function calculates an order amount for each subclass
--                on the order being unapproved inserts into OTB_CASCADE_STG table. 
--                The the batch_ordcostcompupd.ksh will post the details in the 
--                staging table to the OTB table..
--Internal      : 
--Calls         : OTB_SQL.INIT_ORD_BULK
--                OTB_SQL.BUILD_ORD_UNAPPROVE
--                OTB_SQL.FLUSH_ORD_BULK
-----------------------------------------------------------------------------
FUNCTION ORD_UNAPPROVE_CASCADE (I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                                O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
END;
/
