
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE UPDATE_BASE_COST AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- Function name: CHANGE_SUPPLIER
-- Called when changing the primary supplier for an item.
--------------------------------------------------------------------------------
FUNCTION CHANGE_SUPPLIER(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item              IN     ITEM_SUPPLIER.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                         I_process_children  IN     VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function name: CHANGE_COUNTRY
-- Called when changing the primary country for an item/supllier.
--------------------------------------------------------------------------------
FUNCTION CHANGE_COUNTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                        I_process_children  IN     VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function name: CHANGE_LOC
-- Called when changing the primary location of a item/supplier/country.
-----------------------------------------------------------------------------
FUNCTION CHANGE_LOC(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                    I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                    I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                    I_loc               IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                    I_process_children  IN     VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function name: CHG_ITEMLOC_PRIM_SUPP_CNTRY
-- Called when changing the prim_supp or prim_cntry on ITEM_LOC.
--------------------------------------------------------------------------------
FUNCTION CHG_ITEMLOC_PRIM_SUPP_CNTRY(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item                 IN     ITEM_LOC.ITEM%TYPE,
                                     I_loc                  IN     ITEM_LOC.LOC%TYPE,
                                     I_supplier             IN     ITEM_LOC.PRIMARY_SUPP%TYPE,
                                     I_origin_country_id    IN     ITEM_LOC.PRIMARY_CNTRY%TYPE,
                                     I_process_children     IN     VARCHAR2,
                                     I_cost_change          IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function name: CHANGE_COST
-- Called when changing the starting cost of an item
-----------------------------------------------------------------------------
FUNCTION CHANGE_COST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                     I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                     I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                     I_loc               IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                     I_process_children  IN     VARCHAR2,
                     I_update_cost_ind   IN     VARCHAR2,
                     I_cost_change       IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function name: CHANGE_ISC_COST
-- Called when changing the cost on supcntryloc.fmb before an item
-- is approved
-----------------------------------------------------------------------------
FUNCTION CHANGE_ISC_COST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_all_locs          IN     VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function name: BULK_ISC
-- Performs a bulk update of the unit_cost column in the item_supp_country table.
-------------------------------------------------------------------------------------
FUNCTION BULK_ISC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_isc_rows        IN       RMSSUB_XCOSTCHG.ROWID_TBL,
                  I_unit_cost       IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function name: BULK_ISC
-- Performs a bulk update of the unit_cost column in the item_supp_country table.
-------------------------------------------------------------------------------------
FUNCTION BULK_ISC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_items           IN       ITEM_TBL,
                  I_unit_cost       IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                  I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                  I_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function name: BULK_ISCL
-- Performs a bulk update of the unit_cost column in the item_supp_country_loc table.
-- API_ISCL_TEMP should be populated prior calling the function
-------------------------------------------------------------------------------------
FUNCTION BULK_ISCL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_unit_cost       IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function name: BULK_ISCL
-- Performs a bulk update of the unit_cost column in the item_supp_country_loc table.
-------------------------------------------------------------------------------------
FUNCTION BULK_ISCL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_items           IN       ITEM_TBL,
                   I_unit_cost       IN       ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                   I_supplier        IN       ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                   I_country         IN       ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function name: BULK_ITEMLOC_SOH
-- Performs a bulk update of the unit_cost column in the item_loc_soh table.
-------------------------------------------------------------------------------------
FUNCTION BULK_ITEMLOC_SOH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_items           IN       ITEM_TBL,
                          I_item_locs       IN       RMSSUB_XCOSTCHG.PRICE_HIST_RECTYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function name: FUNCTION UPDATE_SINGLE_ISCL
-- Performs a bulk update of the unit_cost column in the item_supp_country_loc table.
-------------------------------------------------------------------------------------
FUNCTION UPDATE_SINGLE_ISCL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_supplier        IN       ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                            I_country         IN       ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                            I_item            IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                            I_parent_id       IN       VARCHAR2,
                            I_loc             IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                            I_unit_cost       IN       ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function name: BULK_PACK_ISC
-- Performs a bulk update of the unit_cost column in the item_supp_country table.
-------------------------------------------------------------------------------------
FUNCTION BULK_PACK_ISC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_isc_rows        IN       RMSSUB_XCOSTCHG.ROWID_TBL,
                       I_packs           IN       ITEM_TBL,
                       I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                       I_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function name: BULK_PACK_ISCL
-- Performs a bulk update of the unit_cost column in the item_supp_country_loc table.
-- Prerequesite of the function: Populate API_ISCL_TEMP
-------------------------------------------------------------------------------------
FUNCTION BULK_PACK_ISCL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_packs           IN       ITEM_TBL,
                        I_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                        I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
FUNCTION BULK_PACK_ISCL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pack_rows       IN       RMSSUB_XCOSTCHG.ROWID_TBL,
                        I_packs           IN       ITEM_TBL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
END;
/
