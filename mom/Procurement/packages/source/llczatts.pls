
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE COST_ZONE_ATTRIB_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
--- Function Name:  GET_ZONE_GROUP_DESC
--- Purpose:        Fetches the cost zone group description for a cost zone group.
--- Calls:          <none>
--- Created:        11-MAR-97 By Carol Kowalski
---------------------------------------------------------------------------------------------
   FUNCTION GET_ZONE_GROUP_DESC( I_cost_zone_group      IN NUMBER,
                                 O_cost_zone_group_desc IN OUT VARCHAR2,
                                 O_error_message        IN OUT VARCHAR2) RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--- Function Name:  GET_ZONE_DESC
--- Purpose:        Fetches the cost zone description for a cost zone.
--- Calls:          <none>
--- Created:        11-MAR-97 By Carol Kowalski
---------------------------------------------------------------------------------------------
   FUNCTION GET_ZONE_DESC( I_cost_zone_group  IN NUMBER,
                           I_cost_zone_id     IN NUMBER,
                           O_cost_zone_desc   IN OUT VARCHAR2,
                           O_error_message    IN OUT VARCHAR2) RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--- Function Name:  GET_COST_LEVEL
--- Purpose:        Fetches the cost level for a cost zone group.
--- Calls:          <none>
--- Created:        24-MAR-97 By Carol Kowalski
---------------------------------------------------------------------------------------------
   FUNCTION GET_COST_LEVEL(I_cost_zone_group  IN     cost_zone.zone_group_id%TYPE,
                           O_cost_level       IN OUT cost_zone_group.cost_level%TYPE,
                           O_error_message    IN OUT VARCHAR2) RETURN BOOLEAN;


END;
/


