CREATE OR REPLACE PACKAGE RMSSUB_XORDER AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
   -- PACKAGE VARIABLES
----------------------------------------------------------------------------
   LP_cre_type         VARCHAR2(15) := 'xordercre';
   LP_mod_type         VARCHAR2(15) := 'xordermod';
   LP_del_type         VARCHAR2(15) := 'xorderdel';

   LP_dtl_cre_type     VARCHAR2(15) := 'xorderdtlcre';
   LP_dtl_mod_type     VARCHAR2(15) := 'xorderdtlmod';
   LP_dtl_del_type     VARCHAR2(15) := 'xorderdtldel';

   LP_recalc_dates     VARCHAR2(1)  := 'Y';
----------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2);
----------------------------------------------------------------------------
END RMSSUB_XORDER;
/