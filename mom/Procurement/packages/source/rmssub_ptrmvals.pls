
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_PAYTERM_VALIDATE AUTHID CURRENT_USER AS


---------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message OUT   RTK_ERRORS.RTK_TEXT%TYPE,  
                       O_dml_rec       OUT   TERMS_SQL.PAYTERM_REC,
                       I_message       IN    "RIB_PayTermDesc_REC",
                       I_message_type  IN	   VARCHAR2)
   RETURN BOOLEAN;  
---------------------------------------------------------------------
END;
/
