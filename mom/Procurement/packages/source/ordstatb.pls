CREATE OR REPLACE PACKAGE BODY ORDER_STATUS_SQL AS
-------------------------------------------------------------------------
FUNCTION RETURN_VAL (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_collection   IN OUT   NOCOPY OBJ_ERROR_TBL,
                     I_source             IN       VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION RETURN_VAL (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_collection   IN OUT   NOCOPY OBJ_ERROR_TBL,
                     I_source             IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS

   L_program         VARCHAR2(61)             := 'ORDER_STATUS_SQL.RETURN_VAL';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN
   if I_source = 'POI' then
      if O_error_message is NOT NULL then
         O_error_collection.EXTEND();
         O_error_collection(O_error_collection.COUNT) := O_error_message;
      end if;
   else
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END RETURN_VAL;
-------------------------------------------------------------------------------
FUNCTION CHECK_SUBMIT_APPROVE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_invalid_date       IN OUT   VARCHAR2,
                              O_store_closed_ind   IN OUT   VARCHAR2,
                              O_store_opened_ind   IN OUT   VARCHAR2,
                              O_clearance_ind      IN OUT   VARCHAR2,
                              O_wksht_recs_ind     IN OUT   VARCHAR2,
                              O_hts_ind            IN OUT   VARCHAR2,
                              O_valid_lc_ind       IN OUT   VARCHAR2,
                              O_scale_ind          IN OUT   VARCHAR2,
                              O_recalc_ind         IN OUT   VARCHAR2,
                              O_round_ind          IN OUT   VARCHAR2,
                              O_invalid_items      IN OUT   VARCHAR2,
                              O_error_collection   IN OUT   NOCOPY OBJ_ERROR_TBL,
                              I_source             IN       VARCHAR2 DEFAULT NULL,
                              I_order_no           IN       ORDHEAD.ORDER_NO%TYPE,
                              I_new_status         IN       ORDHEAD.STATUS%TYPE,
                              I_not_before_date    IN       ORDHEAD.NOT_BEFORE_DATE%TYPE,
                              I_not_after_date     IN       ORDHEAD.NOT_AFTER_DATE%TYPE,
                              I_supplier           IN       ORDHEAD.SUPPLIER%TYPE,
                              I_dept               IN       ORDHEAD.DEPT%TYPE,
                              I_check_loc_status   IN       VARCHAR2 DEFAULT      'N')
RETURN BOOLEAN IS

   L_function              VARCHAR2(50)  := 'ORDER_STATUS_SQL.CHECK_SUBMIT_APPROVE';
   L_explode_pack          ITEM_MASTER.ITEM%TYPE                            := NULL;
   L_item                  ITEM_MASTER.ITEM%TYPE                            := NULL;
   L_location              ORDLOC.LOCATION%TYPE                             := NULL;
   L_pack_qty              ORDLOC.QTY_ORDERED%TYPE                          := NULL;
   L_dept_level            PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE  := NULL;
   L_order_hts_ind         RTM_UNIT_OPTIONS.ORDER_HTS_IND%TYPE              := NULL;
   L_min_qty               SUP_INV_MGMT.MIN_CNSTR_VAL1%TYPE                 := NULL;
   L_ord_qty               ORDLOC.QTY_ORDERED%TYPE                          := NULL;
   L_min_flag              VARCHAR2(5)                                      := NULL;
   L_inactive_ind          VARCHAR2(1)                                      := NULL;
   L_min_ord_ind           SUP_INV_MGMT.MIN_CNSTR_LVL%TYPE                  := NULL;
   L_scale_cnstr_ind       ORD_INV_MGMT.SCALE_CNSTR_IND%TYPE                := NULL;
   L_rev_exists            VARCHAR2(1)                                      := NULL;
   L_wksht_exist           VARCHAR2(1)                                      := NULL;
   L_pack_exists           VARCHAR2(1)                                      := 'N';
   L_recalc_exists         BOOLEAN;
   L_elc_ind               SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_import_ind            SYSTEM_OPTIONS.IMPORT_IND%TYPE;
   L_ord_import_ind        ORDHEAD.IMPORT_ORDER_IND%TYPE;
   L_dummy                 VARCHAR2(1);
   L_items                 VARCHAR2(54);
   L_pack_no               ITEM_MASTER.ITEM%TYPE;
   L_pack_type             ITEM_MASTER.PACK_TYPE%TYPE;
   L_lc_ref_id             ORDLC.LC_REF_ID%TYPE;
   L_applicant             ORDLC.APPLICANT%TYPE;
   L_beneficiary           ORDLC.BENEFICIARY%TYPE;
   L_purchase_type         ORDHEAD.PURCHASE_TYPE%TYPE;
   L_fob_title_pass        ORDHEAD.FOB_TITLE_PASS%TYPE;
   L_fob_title_pass_desc   ORDHEAD.FOB_TITLE_PASS_DESC%TYPE;
   L_valid                 BOOLEAN;
   L_pack_ind              ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind          ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind         ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_vdate                 PERIOD.VDATE%TYPE  := GET_VDATE;
   L_ord_qty_rounded_up    BOOLEAN;
   ---
   L_return_code          VARCHAR2(5)                                       := 'FALSE';
   ---
   L_orig_ind             ORDHEAD.ORIG_IND%TYPE;
   ---
   L_bracket_ind          SUPS.BRACKET_COSTING_IND%TYPE ;

   L_deleted_hts_item     ITEM_MASTER.ITEM%TYPE;
   L_store                STORE.STORE%TYPE                                  := NULL;
   L_wh                   WH.WH%TYPE                                        := NULL;

   cursor C_CHECK_ORDHEAD is
      select orig_ind,
             purchase_type,
             backhaul_type,
             backhaul_allowance
        from ordhead
       where order_no = I_order_no;

   cursor C_CHECK_FOR_PACK is
      select 'Y'
        from ordsku o,
             item_master i
       where o.item     = i.item
         and i.pack_ind = 'Y'
         and o.order_no = I_order_no;

   cursor C_CHECK_PK_EXPLODE is
      select i.item
        from item_master i,
             ordsku o
       where i.item          = o.item
         and o.order_no      = I_order_no
         and i.pack_ind      = 'Y'
         and i.order_as_type = 'E';

   cursor C_CHECK_COST is
      select item
        from ordloc
       where order_no = I_order_no
         and unit_cost < 0;

   cursor C_DEPT_LEVEL is
      select dept_level_orders,
             order_hts_ind
        from procurement_unit_options,
             rtm_unit_options;

   cursor C_CHECK_SKUS is
      select i.item
        from item_master i,
             ordsku o
       where i.dept    != I_dept
         and i.item     = o.item
         and o.order_no = I_order_no;

   cursor C_CHECK_QTY is
      select NVL(SUM(o.qty_ordered),0) qty_ordered,
             i.item,
             i.min_order_qty,
             i.max_order_qty
        from ordloc o,
             item_supp_country i,
             ordsku s
       where o.order_no          = I_order_no
         and o.item              = i.item
         and i.supplier          = I_supplier
         and s.order_no          = o.order_no
         and s.origin_country_id = i.origin_country_id
         and s.item              = o.item
         and qty_ordered         > 0
    group by i.item,
             i.min_order_qty,
             i.max_order_qty;

   cursor C_CHECK_STORE_CLEAR is
      select 'Y'
        from item_loc i,
             ordloc o
       where i.loc       = o.location
         and i.item      = o.item
         and o.order_no  = I_order_no
         and i.clear_ind = 'Y';

   cursor C_CHECK_COMP_CLEAR is
      select 'Y'
        from item_loc i,
             v_packsku_qty v,
             ordloc o
       where i.loc       = o.location
         and i.item      = v.item
         and v.pack_no   = o.item
         and o.order_no  = I_order_no
         and i.clear_ind = 'Y';

   cursor C_CHECK_STORE_CLOSE_DATES is
      select 'Y'
        from ordloc o,
             store s
       where o.location = s.store
         and o.order_no = I_order_no
         and o.qty_ordered > 0
         and (s.store_close_date - NVL(s.stop_order_days,0)) < I_not_after_date;

   cursor C_CHECK_STORE_OPEN_DATES is
      select 'Y'
        from ordloc o,
             store s
       where o.location = s.store
         and o.order_no = I_order_no
         and o.qty_ordered > 0
         and (s.store_open_date - NVL(s.start_order_days,0)) > I_not_before_date;

   cursor C_CHECK_LOC_STATUS is
      select 'x'
        from ordloc o,
             item_loc i
       where o.location = i.loc
         and o.item     = i.item
         and o.order_no = I_order_no
         and o.qty_ordered > 0
         and i.status  != 'A';

   cursor C_INV_MGMT_INFO is
      select min_cnstr_lvl,
             scale_cnstr_ind,
             scale_cnstr_chg_ind
        from ord_inv_mgmt
       where order_no  = I_order_no;

   cursor C_WKSHT_RECS is
      select 'Y'
        from ordloc_wksht
       where order_no = I_order_no;

   cursor C_GET_SKU is
      select o.item
        from ordsku o,
             ordhead oh
       where o.order_no = I_order_no
         and o.order_no = oh.order_no
         and o.origin_country_id != oh.import_country_id;

   -- get components of a buyer pack item
   cursor C_GET_PACK is
      select v.item
        from v_packsku_qty v
       where v.pack_no = L_pack_no;

   cursor C_CHECK_PACK_HTS is
      select 'x'
        from ordsku_hts
       where order_no  = I_order_no
         and item      = L_item
         and pack_item = L_pack_no;

   cursor C_CHECK_PACK_HTS_STATUS is
      select 'x'
        from ordsku_hts
       where order_no  = I_order_no
         and item      = L_item
         and pack_item = L_pack_no
         and status   != 'A';

   -- non pack item or vendor pack item
   cursor C_CHECK_HTS is
      select 'x'
        from ordsku_hts
       where order_no   = I_order_no
         and item       = L_item
         and pack_item IS NULL;

   cursor C_CHECK_HTS_STATUS is
      select 'x'
        from ordsku_hts
       where order_no   = I_order_no
         and item       = L_item
         and pack_item IS NULL
         and status    != 'A';

   cursor C_LC_ORDER is
      select ol.lc_ref_id,
             ol.applicant,
             ol.beneficiary,
             oh.purchase_type,
             oh.fob_title_pass,
             oh.fob_title_pass_desc
        from ordhead oh,
             ordlc ol
       where oh.order_no = I_order_no
         and oh.order_no = ol.order_no;
   ---
   L_ordhead_rec   C_CHECK_ORDHEAD%ROWTYPE ;

   cursor C_LOCATION_CHECK is
      select location
        from ordloc
       where order_no = I_order_no;


BEGIN
   O_error_collection := NEW OBJ_ERROR_TBL();
   open C_CHECK_ORDHEAD;
   fetch C_CHECK_ORDHEAD into L_ordhead_rec;
   L_orig_ind := L_ordhead_rec.orig_ind;
   close C_CHECK_ORDHEAD;
   ---
   open C_CHECK_FOR_PACK;
   fetch C_CHECK_FOR_PACK into L_pack_exists;
   close C_CHECK_FOR_PACK;
   ---
   ---
   -- The order will only be rounded when this function is called with a status of 'S'ubmitted.
   -- This is done under the assumption that orders being submitted for approval are already in
   -- submitted status and therefore have already been rounded. If there is a need to approve an order
   -- without first submitting, the workaround would be to approve orders by calling this package with
   -- a 'S'ubmitted new status. While this approach increases coupling and breaks encapsulation, it is easy
   -- and preserves the performance gain of only calling the round function once per order.
   ---
   -- ensure that Not Before Date on order is later than the vdate.
   ---
   O_invalid_date := 'N';
   ---
   if L_vdate > I_not_before_date then
      O_invalid_date := 'Y';
   end if;
   ---
   if I_new_status = 'S' then
      open C_CHECK_COST;
      fetch C_CHECK_COST into L_item;
      ---
      if C_CHECK_COST%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NOT_SUBMIT_ORD',
                                               L_item,
                                               NULL,
                                               NULL);
         close C_CHECK_COST;
         if NOT RETURN_VAL (O_error_message,
                            O_error_collection,
                            I_source) then
            return FALSE;
         end if;
      end if;
      ---
      close C_CHECK_COST;
      ---

      if L_pack_exists = 'Y' then
         open C_CHECK_PK_EXPLODE;
         fetch C_CHECK_PK_EXPLODE into L_explode_pack;

         if C_CHECK_PK_EXPLODE%FOUND then
            O_error_message := SQL_LIB.CREATE_MSG('PACK_EXPL_BEF_ORD',
                                                  L_explode_pack,
                                                  NULL,
                                                  NULL);
            close C_CHECK_PK_EXPLODE;
            if NOT RETURN_VAL (O_error_message,
                               O_error_collection,
                               I_source) then
               return FALSE;
            end if;
         end if;
         close C_CHECK_PK_EXPLODE;
      end if;
      ---
      -- Rounds the entire order including allocations.
      ---
      if I_source is NULL or I_source <> 'POI' then
         if ROUNDING_SQL.ORDER_INTERFACE(O_error_message,
                                         L_ord_qty_rounded_up,
                                         I_order_no,
                                         I_supplier) = FALSE then
            if NOT RETURN_VAL (O_error_message,
                               O_error_collection,
                               I_source) then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      -- if L_ord_qty_rounded_up is TRUE then that means order qtys
      -- on the order were increased because the qty being shipped
      -- or on an appointment was greater than the qty ordered
      -- there is validation that states that this cannot happen.
      -- if L_ord_qty_rounded_up is TRUE, need to set O_rounding_ind to 'Y'
      -- indicating that a message should be displayed to the user
      -- so they are aware of this change.
      ---
      O_round_ind := 'N';
      ---
      if L_ord_qty_rounded_up = TRUE then
         O_round_ind := 'Y';
      end if;
   end if; --- I_new_status = 'S'
   ---
   open C_LC_ORDER;
   fetch C_LC_ORDER into L_lc_ref_id,
                         L_applicant,
                         L_beneficiary,
                         L_purchase_type,
                         L_fob_title_pass,
                         L_fob_title_pass_desc;
   close C_LC_ORDER;
   ---
   if L_lc_ref_id is not NULL then
      if LC_SQL.VALID_LC_ORDER(O_error_message,
                               L_valid,
                               L_applicant,
                               L_beneficiary,
                               L_purchase_type,
                               L_fob_title_pass,
                               L_fob_title_pass_desc,
                               L_lc_ref_id,
                               I_order_no) = FALSE then
         if NOT RETURN_VAL (O_error_message,
                            O_error_collection,
                            I_source) then
            return FALSE;
         end if;
      end if;
      ---
      if L_valid = FALSE then
         O_valid_lc_ind := 'N';
      else
         O_valid_lc_ind := 'Y';
      end if;
   else
      O_valid_lc_ind := 'O';
   end if; --- L_lc_ref_id NOT NULL/NULL
   ---
   open C_INV_MGMT_INFO;
   fetch C_INV_MGMT_INFO into L_min_ord_ind,
                              L_scale_cnstr_ind,
                              O_scale_ind;
   if C_INV_MGMT_INFO%NOTFOUND then
      L_min_ord_ind     := NULL;
      L_scale_cnstr_ind := 'N';
      O_scale_ind       := 'N';
   end if;
   close C_INV_MGMT_INFO;
   ---
   ---
   open C_DEPT_LEVEL;
   fetch C_DEPT_LEVEL into L_dept_level,
                           L_order_hts_ind;
   close C_DEPT_LEVEL;
   ---
   if L_dept_level = 'Y' and
      I_dept is not NULL then
      open C_CHECK_SKUS;
      fetch C_CHECK_SKUS into L_item;
      if C_CHECK_SKUS%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('MULT_DEPT_ORD_NOT_ALLOW',
                                               L_item,
                                               NULL,
                                               NULL);
         close C_CHECK_SKUS;
         if NOT RETURN_VAL (O_error_message,
                            O_error_collection,
                            I_source) then
            return FALSE;
         end if;
      end if;
      close C_CHECK_SKUS;
   end if;
   ---
   --- Vendor generated orders do not need to have the minimums checked.
   if L_orig_ind != 5 then
      FOR C_check_qty_rec in C_CHECK_QTY LOOP
         if C_check_qty_rec.min_order_qty is not NULL and
            C_check_qty_rec.max_order_qty is not NULL then
            if C_check_qty_rec.qty_ordered < C_check_qty_rec.min_order_qty or
               C_check_qty_rec.qty_ordered > C_check_qty_rec.max_order_qty then
               O_error_message := SQL_LIB.CREATE_MSG('ORDSKU_QTY_LIMIT1',
                                                     C_check_qty_rec.item,
                                                     TO_CHAR(C_check_qty_rec.min_order_qty),
                                                     TO_CHAR(C_check_qty_rec.max_order_qty));
               if NOT RETURN_VAL (O_error_message,
                                  O_error_collection,
                                  I_source) then
                  return FALSE;
               end if;
            end if;
      
         elsif C_check_qty_rec.min_order_qty is not NULL then
            if C_check_qty_rec.qty_ordered < C_check_qty_rec.min_order_qty then
               O_error_message := SQL_LIB.CREATE_MSG('ORDSKU_QTY_LIMIT2',
                                                     C_check_qty_rec.item,
                                                     TO_CHAR(C_check_qty_rec.min_order_qty),
                                                     NULL);
               if NOT RETURN_VAL (O_error_message,
                                  O_error_collection,
                                  I_source) then
                  return FALSE;
               end if;
            end if;
   
         elsif C_check_qty_rec.max_order_qty is not NULL then
            if C_check_qty_rec.qty_ordered > C_check_qty_rec.max_order_qty then
               O_error_message := SQL_LIB.CREATE_MSG('ORDSKU_QTY_LIMIT3',
                                                     C_check_qty_rec.item,
                                                     TO_CHAR(C_check_qty_rec.max_order_qty),
                                                     NULL);
               if NOT RETURN_VAL (O_error_message,
                                  O_error_collection,
                                  I_source) then
                  return FALSE;
               end if;
            end if;
         end if;
      END LOOP;
   end if;
   ---
   open C_CHECK_STORE_CLEAR;
   fetch C_CHECK_STORE_CLEAR into O_clearance_ind;
   close C_CHECK_STORE_CLEAR;
   ---
   if L_pack_exists = 'Y' then
      open C_CHECK_COMP_CLEAR;
      fetch C_CHECK_COMP_CLEAR into O_clearance_ind;
      close C_CHECK_COMP_CLEAR;
   end if;
   ---
   open C_CHECK_STORE_CLOSE_DATES;
   fetch C_CHECK_STORE_CLOSE_DATES into O_store_closed_ind;
   close C_CHECK_STORE_CLOSE_DATES;
   ---
   open C_CHECK_STORE_OPEN_DATES;
   fetch C_CHECK_STORE_OPEN_DATES into O_store_opened_ind;
   close C_CHECK_STORE_OPEN_DATES;
   ---
   if I_check_loc_status ='N' then
      open C_CHECK_LOC_STATUS;
      fetch C_CHECK_LOC_STATUS into L_inactive_ind;
      if C_CHECK_LOC_STATUS%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NOT_ACTIVE_ORD_ITEM',
                                               NULL,
                                               NULL,
                                               NULL);
         close C_CHECK_LOC_STATUS;
         if NOT RETURN_VAL (O_error_message,
                            O_error_collection,
                            I_source) then
            return FALSE;
         end if;
      end if;
      close C_CHECK_LOC_STATUS;
   end if;
   ---
   --- Vendor generated orders do not need to have the minimums checked.
   if L_orig_ind != 5 then
      if (L_min_ord_ind = 'O' or L_min_ord_ind ='L')then
         if I_source = 'POI' then
            if L_min_ord_ind = 'O' then
               CHECK_VEND_MIN(I_order_no,
                              -1,
                              -1,
                              L_min_qty,
                              L_ord_qty,
                              L_min_flag,
                              L_return_code,
                              O_error_message);
               if L_return_code = 'FALSE' or L_min_flag = 'TRUE' then
                  if NOT RETURN_VAL (O_error_message,
                                     O_error_collection,
                                     I_source) then
                     return FALSE;
                  end if;
               end if;
            elsif L_min_ord_ind = 'L' then
               FOR rec in C_LOCATION_CHECK loop
                  L_store := rec.location;
                  L_wh := -1;
                  CHECK_VEND_MIN(I_order_no,
                                 L_store,
                                 L_wh,
                                 L_min_qty,
                                 L_ord_qty,
                                 L_min_flag,
                                 L_return_code,
                                 O_error_message);
                  if L_return_code = 'FALSE' or L_min_flag = 'TRUE' then
                     if NOT RETURN_VAL (O_error_message,
                                        O_error_collection,
                                        I_source) then
                        return FALSE;
                     end if;
                  end if;
               end loop;
            end if;

         else
            CHECK_VEND_MIN(I_order_no,
                           -1,
                           -1,
                           L_min_qty,
                           L_ord_qty,
                           L_min_flag,
                           L_return_code,
                           O_error_message);
            if L_return_code = 'FALSE' then
               if NOT RETURN_VAL (O_error_message,
                                  O_error_collection,
                                  I_source) then
                  return FALSE;
               end if;
            end if;
            ---
            if L_min_flag = 'TRUE' then
               if NOT RETURN_VAL (O_error_message,
                                  O_error_collection,
                                  I_source) then
                  return FALSE;
               end if;
            end if;
         end if;
      end if;
   end if;
   ---
   if ORDER_ATTRIB_SQL.REPL_RESULTS_RECALC_EXIST(O_error_message,
                                                 L_recalc_exists,
                                                 I_order_no) = FALSE then
      if NOT RETURN_VAL (O_error_message,
                         O_error_collection,
                         I_source) then
         return FALSE;
      end if;
   end if;
   ---
   if L_recalc_exists then
      O_recalc_ind := 'Y';
   else
      O_recalc_ind := 'N';
   end if;
   ---
   open C_WKSHT_RECS;
   fetch C_WKSHT_RECS into O_wksht_recs_ind;
   close C_WKSHT_RECS;
   ---
   -- new IMPORTING functionality
   ---
   if SYSTEM_OPTIONS_SQL.GET_IMPORT_ELC_IND(O_error_message,
                                            L_import_ind,
                                            L_elc_ind) = FALSE then
      if NOT RETURN_VAL (O_error_message,
                         O_error_collection,
                         I_source) then
         return FALSE;
      end if;
   end if;
   ---
   if L_elc_ind = 'Y' and L_import_ind = 'Y' then
      if ORDER_ATTRIB_SQL.GET_IMPORT_IND(O_error_message,
                                         L_ord_import_ind,
                                         I_order_no) = FALSE then
         if NOT RETURN_VAL (O_error_message,
                            O_error_collection,
                            I_source) then
            return FALSE;
         end if;
      end if;
      ---
      if L_ord_import_ind = 'Y' then
      --loop through all items on the order and check if item has HTS codes
      --associated with it
         FOR C_get_item_rec in C_GET_SKU LOOP
            L_item      := C_get_item_rec.item;
            ---
            if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                             L_pack_ind,
                                             L_sellable_ind,
                                             L_orderable_ind,
                                             L_pack_type,
                                             L_item) = FALSE then
               if NOT RETURN_VAL (O_error_message,
                                  O_error_collection,
                                  I_source) then
                  return FALSE;
               end if;
            end if;
            ---
            if L_pack_ind = 'Y' and L_pack_type = 'B' then
               L_pack_no := L_item;
               ---
               -- loop through all component items of the pack to check if the
               -- items have HTS codes associated with them
               ---
               FOR C_get_pack_rec in C_GET_PACK LOOP
                  L_item := C_get_pack_rec.item;
                  ---
                  L_dummy := null;
                  if L_order_hts_ind = 'Y' then
                     open C_CHECK_PACK_HTS;
                     fetch C_CHECK_PACK_HTS into L_dummy;
                     if C_CHECK_PACK_HTS%NOTFOUND then
                        if LENGTH(L_items) > 54 - LENGTH('(Pack:);') - LENGTH(L_pack_no) - LENGTH(L_item) then
                           close C_CHECK_PACK_HTS;
                           Exit;
                        else
                           L_items := L_items ||L_item||'(Pack:'||L_pack_no||')'||';';
                        end if;
                        O_hts_ind := 'Y';
                     end if;
                     close C_CHECK_PACK_HTS;
                  end if;
                  ---
                  if L_dummy = 'x' or L_order_hts_ind = 'N' then
                  -- if the HTS records are found, check if it is in Approved
                  -- status. If not then it cannot be submitted.
                  -- if order_hts_ind = 'N' then we won't perform the above check
                  -- but we will still check for unapproved records.
                     open C_CHECK_PACK_HTS_STATUS;
                     fetch C_CHECK_PACK_HTS_STATUS into L_dummy;
                     ---
                     if C_CHECK_PACK_HTS_STATUS%FOUND then
                        if LENGTH(L_items) > 54 - LENGTH('(Pack:);') - LENGTH(L_pack_no) - LENGTH(L_item) then
                           close C_CHECK_PACK_HTS_STATUS;
                           Exit;
                        else
                           L_items := L_items ||L_item||'(Pack:'||L_pack_no||')'||';';
                        end if;
                        O_hts_ind := 'Y';
                     end if;
                     close C_CHECK_PACK_HTS_STATUS;
                  end if;
               END LOOP;
            else
               L_dummy := null;
               if L_order_hts_ind = 'Y' then
                  open C_CHECK_HTS;
                  fetch C_CHECK_HTS into L_dummy;
                  if C_CHECK_HTS%NOTFOUND then
                     if LENGTH(L_items) > 54 - LENGTH(';') - LENGTH(L_item) then
                        close C_CHECK_HTS;
                        Exit;
                     else
                        L_items := L_items ||L_item||';';
                     end if;
                     O_hts_ind := 'Y';
                  end if;
                  close C_CHECK_HTS;
               end if;
               ---
               if L_dummy = 'x' or L_order_hts_ind = 'N' then
                  ---
                  -- if the HTS records are found, check if it is in Approved
                  -- status. If not then it cannot be submitted.
                  -- if order_hts_ind = 'N' then we won't perform the above check
                  -- but we will still check for unapproved records.
                  open C_CHECK_HTS_STATUS;
                  fetch C_CHECK_HTS_STATUS into L_dummy;
                  ---
                  if C_CHECK_HTS_STATUS%FOUND then
                     if LENGTH(L_items) > 54 - LENGTH(';') - LENGTH(L_item) then
                        close C_CHECK_HTS_STATUS;
                        Exit;
                     else
                        L_items := L_items ||L_item||';';
                     end if;
                     O_hts_ind := 'Y';
                  end if;
                  close C_CHECK_HTS_STATUS;
               end if;
            end if;
         END LOOP;
         ---
         if O_hts_ind = 'Y' then
            O_invalid_items := SUBSTR(L_items, 1, LENGTH(L_items)-1);
         end if;
      end if;
   end if;
   ---
   --- Does the supplier use bracket costing
   if SUPP_ATTRIB_SQL.GET_BRACKET_COSTING_IND(O_error_message,
                                              L_bracket_ind,
                                              I_supplier) = FALSE then
      if NOT RETURN_VAL (O_error_message,
                         O_error_collection,
                         I_source) then
         return FALSE;
      end if;
   end if;
   --- Check bracket costing required column values
   --- Values come from C_CHECK_ORDHEAD at the beginning of program
   if L_bracket_ind = 'Y' then
      if L_ordhead_rec.purchase_type = 'BACK' then
         if L_ordhead_rec.backhaul_type = 'F' then
            if L_ordhead_rec.backhaul_allowance is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('FLAT_FEE_ALLOWANCE_REQ',
                                                     TO_CHAR(I_order_no),
                                                     NULL,
                                                     NULL);
               if NOT RETURN_VAL (O_error_message,
                                  O_error_collection,
                                  I_source) then
                  return FALSE;
               end if;
            end if ;
         elsif L_ordhead_rec.backhaul_type is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('BACKHAUL_TYPE_REQ',
                                                  TO_CHAR(I_order_no),
                                                  NULL,
                                                  NULL);
            if NOT RETURN_VAL (O_error_message,
                               O_error_collection,
                               I_source) then
               return FALSE;
            end if;
         end if ;
      elsif L_ordhead_rec.purchase_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('PURCHASE_TYPE_REQ',
                                               NULL,
                                               NULL,
                                               NULL);
         if NOT RETURN_VAL (O_error_message,
                            O_error_collection,
                            I_source) then
            return FALSE;
         end if;
      end if ;
   end if ; --- checking L_bracket_ind as Yes for the order supplier
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_SUBMIT_APPROVE;
-------------------------------------------------------------------------------
FUNCTION CHECK_LOC_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_approved_ord    IN OUT   VARCHAR2,
                          I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(50)  := 'ORDER_STATUS_SQL.CHECK_LOC_STATUS';
   L_inactive_ind   VARCHAR2(1)   := NULL;
   L_approved_ord   VARCHAR2(1)   := NULL;

   cursor C_CHECK_LOC_STATUS is
      select 'x'
        from ordloc o,
             item_loc i
       where o.location = i.loc
         and o.item     = i.item
         and o.order_no = I_order_no
         and i.status  != 'A';

   cursor C_APPROVED_ORDER is
      select 'x'
        from ordhead h
       where h. order_no = I_order_no
         and h.orig_approval_date IS NOT NULL;

BEGIN

   open C_CHECK_LOC_STATUS;

   fetch C_CHECK_LOC_STATUS into L_inactive_ind;

   if L_inactive_ind is NOT NULL then
      close C_CHECK_LOC_STATUS;

      open C_APPROVED_ORDER;

      fetch C_APPROVED_ORDER into L_approved_ord;

      if L_approved_ord is NOT NULL then
         O_approved_ord:='Y';
         close C_APPROVED_ORDER;
         return FALSE;
      else
         O_approved_ord:='N';
         close C_APPROVED_ORDER;
         return FALSE;
      end if;
   else
      close C_CHECK_LOC_STATUS;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_LOC_STATUS;
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_SHIPMENT(O_error_message IN OUT VARCHAR2,
                        O_err_flag      IN OUT BOOLEAN,
                        I_order_no      IN     ordloc.order_no%TYPE,
                        I_item          IN     ordloc.item%TYPE,
                        I_location      IN     ordloc.location%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50)            := 'ORDER_STATUS_SQL.CHECK_SHIPMENT';
   L_exists     VARCHAR2(1)             := NULL;

   cursor C_CHECK_SHIP is
      select 'x'
   from shipment s,
        shipsku k
       where s.order_no     = I_order_no
    and s.to_loc       = NVL(I_location, s.to_loc)
    and s.status_code in ('T', 'I', 'E', 'U')
    and s.shipment     = k.shipment
    and k.item         = NVL(I_item, k.item);

   cursor C_CHECK_EXP is
      select 'x'
   from shipment s,
        shipsku k
       where s.order_no     = I_order_no
    and s.to_loc       = NVL(I_location, s.to_loc)
    and s.status_code  = 'R'
    and s.shipment     = k.shipment
    and k.item         = NVL(I_item, k.item)
    group by k.item
    having (sum(nvl(qty_expected,0)) - sum(nvl(qty_received,0))) > 0;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_CHECK_SHIP','shipment','order no: '||TO_CHAR(I_order_no));
   open C_CHECK_SHIP;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_SHIP','shipment','order no: '||TO_CHAR(I_order_no));
   fetch C_CHECK_SHIP into L_exists;
   ---
   if C_CHECK_SHIP%FOUND then
      if I_item is NULL then
    O_error_message := SQL_LIB.CREATE_MSG('ORD_SHIPSKU_EXIST',
                  NULL, NULL, NULL);
      else
    O_error_message := SQL_LIB.CREATE_MSG('ORD_SHIP_EXIST',
                  NULL, NULL, NULL);
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_SHIP','shipment','order no: '||TO_CHAR(I_order_no));
      close C_CHECK_SHIP;
      O_err_flag := TRUE;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_SHIP','shipment','order no: '||TO_CHAR(I_order_no));
   close C_CHECK_SHIP;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_EXP','shipment','order no: '||TO_CHAR(I_order_no));
   open C_CHECK_EXP;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_EXP','shipment','order no: '||TO_CHAR(I_order_no));
   fetch C_CHECK_EXP into L_exists;
   ---
   if C_CHECK_EXP%FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('EXP_QTY_EXISTS',
                    NULL, NULL, NULL);
      ---
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXP','shipment','order no: '||TO_CHAR(I_order_no));
      close C_CHECK_EXP;
      O_err_flag := TRUE;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXP','shipment','order no: '||TO_CHAR(I_order_no));
   close C_CHECK_EXP;
   ---
   O_err_flag := FALSE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                    SQLERRM,
                    L_program,
                    TO_CHAR(SQLCODE));
      O_err_flag := FALSE;
      return FALSE;
END CHECK_SHIPMENT;
-------------------------------------------------------------------------
FUNCTION CANCEL_ALL(O_error_message     IN OUT   VARCHAR2,
                    I_order_no          IN       ORDLOC.ORDER_NO%TYPE,
                    I_cancel_code       IN       ORDLOC.CANCEL_CODE%TYPE,
                    I_cancel_id         IN       ORDLOC.CANCEL_ID%TYPE,
                    I_alloc_close_ind   IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_function              VARCHAR2(50)       := 'ORDER_STATUS_SQL.CANCEL_ALL';
   L_vdate                 PERIOD.VDATE%TYPE  := GET_VDATE;
   L_exist                 BOOLEAN;
   L_appr_ind              VARCHAR2(3);
   L_closed                BOOLEAN;
   L_pick_not_after_days   NUMBER;
   L_appt_qty              ORDLOC.QTY_ORDERED%TYPE;
   L_cancl_qty             ORDLOC.QTY_ORDERED%TYPE;
   L_tot_ord               ORDLOC.QTY_ORDERED%TYPE;
   L_exptd_recv_qty        ORDLOC.QTY_ORDERED%TYPE;
   L_total_ord_qty         ORDLOC.QTY_ORDERED%TYPE;
   L_location              WH.WH%TYPE;
   L_ship_sum              ORDLOC.QTY_ORDERED%TYPE := NULL;
   L_item                  ITEM_MASTER.ITEM%TYPE;
   L_exists                BOOLEAN;
   L_asn_exists            BOOLEAN;
   L_qty_ordered       ORDLOC.QTY_ORDERED%TYPE               := 0;
   L_qty_received      ORDLOC.QTY_RECEIVED%TYPE              := 0;


   cursor C_OTB is
      select item,
             location,
             loc_type,
             NVL(qty_received,0) qty_received,
             NVL(qty_cancelled,0) qty_cancelled,
             qty_ordered
        from ordloc
       where order_no = I_order_no
         and qty_ordered !=0;

   cursor C_PHYSICAL_WH is
      select physical_wh
        from wh
       where wh = L_location;

   cursor C_SHIPMENT_SUM is
      select  NVL(SUM(NVL(DECODE(shp.ship_origin,'3', shs.qty_received, shs.qty_expected), 0)), 0)
      from shipsku shs,
           shipment shp
       where shs.shipment = shp.shipment
         and shp.order_no = I_order_no
         and shp.to_loc = L_location
         and shp.status_code <> 'C'
         and shs.item = L_item;

   cursor C_GET_PHYS_QTY_ORD is
       select NVL(sum(qty_ordered),0),
              NVL(sum(qty_received),0)
       from ordloc
       where order_no = I_order_no
         and item     = L_item
         and location in (select wh
       from wh
       where physical_wh      = L_location
         and stockholding_ind = 'Y');

BEGIN
   ---
   SQL_LIB.SET_MARK('LOOP',
                    'C_OTB',
                    'ordloc',
                    'order no: '||TO_CHAR(I_order_no));
   FOR C_otb_rec in C_OTB LOOP
      L_location := c_otb_rec.location;
      L_item := c_otb_rec.item;
      if c_otb_rec.loc_type = 'W' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_PHYSICAL_WH',
                          'WH',
                          'WAREHOUSE: '||to_char(L_location));
         open c_physical_wh;

         SQL_LIB.SET_MARK('FETCH',
                          'C_PHYSICAL_WH',
                          'WH',
                          'WAREHOUSE: '||to_char(L_location));
         fetch c_physical_wh into L_location;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_PHYSICAL_WH',
                          'WH',
                          'WAREHOUSE: '||to_char(L_location));
         close c_physical_wh;
      end if;

      if ORDER_VALIDATE_SQL.VALIDATE_SHIP_QTY(O_error_message,
                                              L_appt_qty,
                                              I_order_no,
                                              C_otb_rec.item,
                                              L_location) = FALSE then
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('OPEN',
                       'C_SHIPMENT_SUM',
                       'SHIPMENT,SHIPSKU',
                       'Order No: '||to_char(I_order_no));
      open C_SHIPMENT_SUM;

      SQL_LIB.SET_MARK('FETCH',
                       'C_SHIPMENT_SUM',
                       'SHIPMENT,SHIPSKU',
                       'Order No: '||to_char(I_order_no));
      fetch C_SHIPMENT_SUM into L_ship_sum;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_SHIPMENT_SUM',
                       'SHIPMENT,SHIPSKU',
                       'Order No: '||to_char(I_order_no));
      close C_SHIPMENT_SUM;

      if C_otb_rec.loc_type = 'W' then
         open  C_GET_PHYS_QTY_ORD;
         fetch C_GET_PHYS_QTY_ORD into L_qty_ordered,L_qty_received;
         close C_GET_PHYS_QTY_ORD;
      else
         L_qty_ordered := NVL(C_otb_rec.qty_ordered, 0);
         L_qty_received :=  NVL(C_otb_rec.qty_received, 0);
      end if;


      if L_ship_sum <= L_qty_ordered then
         L_exptd_recv_qty := NVL(C_otb_rec.qty_received, 0);
      else
         L_exptd_recv_qty := NVL(C_otb_rec.qty_ordered, 0);
      end if;

      if NVL(C_otb_rec.qty_received, 0) > NVL(C_otb_rec.qty_ordered, 0) then
         L_exptd_recv_qty :=  NVL(C_otb_rec.qty_ordered, 0);
      end if;

      L_total_ord_qty := C_otb_rec.qty_cancelled + C_otb_rec.qty_ordered;
      L_cancl_qty := C_otb_rec.qty_cancelled + C_otb_rec.qty_ordered - L_exptd_recv_qty;

      if L_cancl_qty> 0 then
         --- OTB cancel qty should be C_otb_rec.qty_ordered - L_exptd_recv_qty, instead of L_cancl_qty
         --- i.e orderd_qty = 10; received_qty = 0 @ $1
         --- in cases were an item is cancelled using ordloc(cancel-item), cancel_qty = 2, otb.cancel_amt = 2
         --- in ordhead(cancel all items), cancel_qty passed to the otb function should be 8, so otb.cancel_amt = 2+8=
         --- but the old logic passes cancel_qty = 10, so otb.cancel_amt will erroneously be set to 2+10.
         if OTB_SQL.ORD_CANCEL_REINSTATE(O_error_message,
                                         I_order_no,
                                         C_otb_rec.item,
                                         C_otb_rec.location,
                                         C_otb_rec.loc_type,
                                         C_otb_rec.qty_ordered - L_exptd_recv_qty,
                                         'C') = FALSE then
             return FALSE;
         end if;
         ---
         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'ordloc',
                          'order no: '||TO_CHAR(I_order_no));

         update ordloc
            set qty_ordered   = NVL(L_total_ord_qty,0)- NVL(L_cancl_qty,0),
                qty_cancelled = L_cancl_qty,
                cancel_code   = I_cancel_code,
                cancel_date   = L_vdate,
                cancel_id     = I_cancel_id
          where order_no      = I_order_no
            and item          = C_otb_rec.item
            and location      = C_otb_rec.location
            and (NVL(qty_ordered,0) = 0
                 or qty_ordered > NVL(qty_received,0));
      end if;

   END LOOP;
   ---
   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'shipsku',
                    NULL);
   update shipsku
      set qty_received = 0
    where shipment in (select shipment
                         from shipment
                        where order_no    = I_order_no
                          and status_code = 'R')
      and qty_received is NULL;
   ---
   if I_cancel_id = 'ordautcl' then
      if CANCEL_ALLOC_SQL.CANCEL_SINGLE_ALLOC(O_error_message,
                                              I_order_no,
                                              I_cancel_id,
                                              I_alloc_close_ind) = FALSE then
         return FALSE;
      end if;
   else
      -- Check to see if any allocations exist on the order that have not been fully received.
      if ORDER_STATUS_SQL.CHECK_OPEN_ALLOC(O_error_message,
                                           L_exists,
                                           L_ASN_exists,
                                           I_order_no) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists then
         if CANCEL_ALLOC_SQL.CANCEL_SINGLE_ALLOC(O_error_message,
                                                 I_order_no,
                                                 I_cancel_id,
                                                 I_alloc_close_ind) = FALSE then
            return FALSE;
         end if;
      end if;
      if L_exists and L_ASN_exists then
         if CANCEL_ALLOC_SQL.CANCEL_ASN_ALLOC(O_error_message,
                                              I_order_no,
                                              I_alloc_close_ind) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   if DEAL_VALIDATE_SQL.DEAL_CALC_QUEUE_EXIST(O_error_message,
                                              L_exist,
                                              L_appr_ind,
                                              I_order_no) = FALSE then
      return FALSE;
   end if;

   if L_exist then
      if DELETE_DEAL_CALC_QUEUE(O_error_message,
                                I_order_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CANCEL_ALL;
-------------------------------------------------------------------------
FUNCTION REINSTATE_ALL(O_error_message IN OUT VARCHAR2,
                       I_order_no      IN     ordhead.order_no%TYPE)
   RETURN BOOLEAN IS

   L_function  VARCHAR2(50)  := 'ORDER_STATUS_SQL.REINSTATE_ALL';

   cursor C_OTB is
      select item,
             location,
             loc_type,
             NVL(qty_cancelled, 0) qty_cancelled
        from ordloc
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('LOOP','C_OTB','ordloc','order no: '||TO_CHAR(I_order_no));
   FOR C_otb_rec in C_OTB LOOP
      if C_otb_rec.qty_cancelled > 0 then
         if OTB_SQL.BUILD_ORD_CANCEL_REINSTATE(O_error_message,
                                               I_order_no,
                                               C_otb_rec.item,
                                               C_otb_rec.location,
                                               C_otb_rec.loc_type,
                                               C_otb_rec.qty_cancelled,
                                               'R') = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;
   ---
   SQL_LIB.SET_MARK('UPDATE',NULL,'ordloc','order no: '||TO_CHAR(I_order_no));
   update ordloc
      set qty_ordered   = qty_ordered + NVL(qty_cancelled,0),
          qty_cancelled = NULL,
          cancel_code   = NULL,
          cancel_date   = NULL,
          cancel_id     = NULL
    where order_no = I_order_no;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                    SQLERRM,
                    L_function,
                    TO_CHAR(SQLCODE));
      return FALSE;
END REINSTATE_ALL;
-------------------------------------------------------------------------
FUNCTION DELETE_WKSHT(O_error_message IN OUT VARCHAR2,
                      I_order_no      IN     ordhead.order_no%TYPE)
   RETURN BOOLEAN IS

   L_function  VARCHAR2(50)  := 'ORDER_STATUS_SQL.DELETE_WKSHT';

BEGIN
   SQL_LIB.SET_MARK('DELETE',NULL,'ordloc_wksht','order no: '||TO_CHAR(I_order_no));
   delete from ordloc_wksht
    where order_no = I_order_no;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                    SQLERRM,
                    L_function,
                    TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_WKSHT;
--------------------------------------------------------------------------------------------
FUNCTION AUTO_APPROVE_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                            I_supplier        IN       ORDHEAD.SUPPLIER%TYPE,
                            I_dept            IN       ORDHEAD.DEPT%TYPE,
                            I_not_aft_date    IN       ORDHEAD.NOT_AFTER_DATE%TYPE,
                            I_user_id         IN       ORDHEAD.ORIG_APPROVAL_ID%TYPE)
   RETURN BOOLEAN IS

   L_dept_level_orders   VARCHAR2(1);
   L_store_closed_ind    VARCHAR2(1);
   L_store_opened_ind    VARCHAR2(1);
   L_clearance_ind       VARCHAR2(1);
   L_wksht_recs_ind      VARCHAR2(1);
   L_hts_ind             VARCHAR2(1);
   L_valid_lc_ind        VARCHAR2(1);
   L_scale_ind           VARCHAR2(1);
   L_recalc_ind          VARCHAR2(1);
   L_round_ind           VARCHAR2(1);
   L_invalid_items       VARCHAR2(54);
   L_today_date          DATE := GET_VDATE;
   L_table               VARCHAR2(30);
   L_invalid_date        VARCHAR2(1)     := 'N';
   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_err_collect         OBJ_ERROR_TBL := NEW OBJ_ERROR_TBL();
   L_source              VARCHAR2(60) := NULL;

   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ORDHEAD is
      select 'x'
        from ordhead
       where order_no = I_order_no
         for update nowait;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---
   L_dept_level_orders  := L_system_options_row.dept_level_orders;
   ---
   if L_dept_level_orders = 'Y' then
      if I_dept is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_dept',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
   end if;
   ---
   if I_not_aft_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_not_aft_date',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_not_aft_date < L_today_date then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_not_aft_date',
                                            TO_CHAR(I_not_aft_date, 'DD-MON-RR'),
                                            'I_not_aft_date >= '||TO_CHAR(L_today_date, 'DD-MON-RR'));
      return FALSE;
   end if;
   ---
   if I_user_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_user_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if ORDER_STATUS_SQL.CHECK_SUBMIT_APPROVE(O_error_message,
                                            L_invalid_date,
                                            L_store_closed_ind,
                                            L_store_opened_ind,
                                            L_clearance_ind,
                                            L_wksht_recs_ind,
                                            L_hts_ind,
                                            L_valid_lc_ind,
                                            L_scale_ind,
                                            L_recalc_ind,
                                            L_round_ind,
                                            L_invalid_items,
                                            L_err_collect,
                                            L_source,
                                            I_order_no,
                                            'S',
                                            L_today_date,
                                            I_not_aft_date,
                                            I_supplier,
                                            I_dept) = FALSE then
      return FALSE;
   end if;
   ---
   if L_invalid_date = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_APPR_ORDER_DATE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'ordhead';
   open C_LOCK_ORDHEAD;
   close C_LOCK_ORDHEAD;
   ---
   SQL_LIB.SET_MARK('UPDATE', NULL, 'ordhead', 'order no: '||TO_CHAR(I_order_no));
   update ordhead
      set status               = 'A',
          orig_approval_id     = I_user_id,
          orig_approval_date   = L_today_date,
          last_update_id       = get_user,
          last_update_datetime = sysdate
    where order_no             = I_order_no;
   ---
   if OTB_SQL.ORD_APPROVE(I_order_no,
                          O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            TO_CHAR(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_STATUS_SQL.AUTO_APPROVE_ORDER',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END AUTO_APPROVE_ORDER;
-------------------------------------------------------------------------
FUNCTION SET_INIT_COST (O_error_message   IN OUT VARCHAR2,
                        I_supplier        IN     ordhead.supplier%TYPE,
                        I_order_no        IN     ordhead.order_no%TYPE,
                        I_order_currency  IN     ordhead.currency_code%TYPE DEFAULT NULL,
                        I_exchange_rate   IN     ordhead.exchange_rate%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_supp_currency     sups.currency_code%TYPE;
   L_unit_cost         item_supp_country.unit_cost%TYPE;
   L_unit_cost_init    ordloc.unit_cost_init%TYPE;
   L_item              ITEM_MASTER.ITEM%TYPE;
   L_location          ORDLOC.LOCATION%TYPE;
   ---
   L_table             VARCHAR2(30) := 'ORDLOC';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_GET_ORDER_INFO is
      select s.item,
        s.origin_country_id,
        l.location,
        i.unit_cost
   from ordsku s,
        ordloc l,
        ordhead o,
        item_supp_country_loc i
       where o.order_no          = I_order_no
    and o.order_no          = s.order_no
    and o.order_no          = l.order_no
    and s.item              = l.item
    and s.item              = i.item
    and o.supplier          = i.supplier
    and s.origin_country_id = i.origin_country_id
    and l.location          = i.loc;

   cursor C_LOCK_ORDLOC is
      select 'x'
   from ordloc
       where order_no = I_order_no
    and item     = L_item
    and location = L_location
    for update nowait;

BEGIN
   -- Get the supplier currency.
   if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
               L_supp_currency,
               I_supplier) = FALSE then
      return FALSE;
   end if;
   ---
   -- Get the information for this order.
   ---
   for rec in C_GET_ORDER_INFO loop
      L_unit_cost := rec.unit_cost;
      L_item      := rec.item;
      L_location  := rec.location;
      ---
      -- If the supplier currency is different than the order currency
      -- a conversion is neccessary.
      ---
      if L_supp_currency != I_order_currency then
    --- Convert from supplier currency to order currency.
    if CURRENCY_SQL.CONVERT(O_error_message,
             L_unit_cost,
             L_supp_currency,
             I_order_currency,
             L_unit_cost_init,
             'C',
             NULL,
             NULL,
             NULL,
             I_exchange_rate) = FALSE then
       return FALSE;
    end if;
      else
    -- No conversion is neccessary so use unit_cost retrieved from item_supp_country_loc
    L_unit_cost_init := L_unit_cost;
      end if;
      ---
      -- Update the ordsku table with unit_cost_init value.
      ---
      open C_LOCK_ORDLOC;
      close C_LOCK_ORDLOC;
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL, 'ordloc', 'order no: '||to_char(I_order_no));
      update ordloc
    set unit_cost_init = L_unit_cost_init
       where order_no = I_order_no
    and item     = rec.item
    and location = rec.location;
   end loop;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                   L_table,
                   to_char(I_order_no),
                   NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                    SQLERRM,
                   'ORDER_STATUS_SQL.AUTO_APPROVE_ORDER',
                    to_char(SQLCODE));
      return FALSE;
END SET_INIT_COST;
-------------------------------------------------------------------------
FUNCTION DELETE_DEAL_CALC_QUEUE(O_error_message IN OUT VARCHAR2,
                                I_order_no      IN     ordhead.order_no%TYPE)
   RETURN BOOLEAN IS

   L_function  VARCHAR2(50)  := 'ORDER_STATUS_SQL.DELETE_DEAL_CALC_QUEUE';
   ---
   L_table             VARCHAR2(30) := 'DEAL_CALC_QUEUE';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_DEAL_CALC_QUEUE is
      select 'x'
   from deal_calc_queue
       where order_no = I_order_no
    for update nowait;


BEGIN

   open C_LOCK_DEAL_CALC_QUEUE;
   close C_LOCK_DEAL_CALC_QUEUE;

   ---

   SQL_LIB.SET_MARK('DELETE',NULL,'deal_calc_queue','order no: '||TO_CHAR(I_order_no));
   delete from deal_calc_queue
    where order_no = I_order_no;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                   L_table,
                   to_char(I_order_no),
                   NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                    SQLERRM,
                    L_function,
                    TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_DEAL_CALC_QUEUE;
------------------------------------------------------------------------------------
FUNCTION CHECK_OPEN_ALLOC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT BOOLEAN,
                          O_ASN_exists      IN OUT BOOLEAN,
                          I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN is

   L_program   VARCHAR2(64) := 'ORDER_STATUS_SQL.CHECK_OPEN_ALLOC';
   L_exists    VARCHAR2(1);
   L_asn_exist VARCHAR2(1);

   cursor C_CHECK_ALLOC is
      select 'x'
        from alloc_header ah, alloc_detail ad
       where ah.order_no = I_order_no
         and ah.alloc_no = ad.alloc_no
         and nvl(qty_allocated,0) >= nvl(qty_transferred,0)
         and rownum = 1
      UNION ALL
      select 'x'
        from alloc_header
       where doc_type = 'ALLOC'
         and order_no in (select ah.alloc_no
                            from alloc_header ah, alloc_detail ad
                           where ah.order_no = I_order_no
                             and ah.alloc_no = ad.alloc_no
                             and nvl(qty_allocated,0) >= nvl(qty_transferred,0)
                             and rownum = 1);

   cursor C_CHECK_ASN_ALLOC is
      select 'x'
        from alloc_header
       where order_no = I_order_no
         and doc_type = 'ASN'
         and rownum = 1
      UNION ALL
      select 'x'
        from alloc_header
       where doc_type = 'ALLOC'
         and order_no in (select alloc_no
                            from alloc_header
                           where order_no = I_order_no
                             and doc_type = 'ASN'
                             and rownum = 1);

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC', NULL, NULL, NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_ALLOC',
                    'ALLOC_HEADER, ALLOC_DETAIL',
                    'order_no = '||to_char(I_order_no));
   open C_CHECK_ALLOC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_ALLOC',
                    'ALLOC_HEADER, ALLOC_DETAIL',
                    'order_no = '||to_char(I_order_no));
   fetch C_CHECK_ALLOC into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_ALLOC',
                    'ALLOC_HEADER, ALLOC_DETAIL',
                    'order_no = '||to_char(I_order_no));
   close C_CHECK_ALLOC;

   if L_exists is NOT NULL then
      O_exists := TRUE;
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_ASN_ALLOC',
                       'ALLOC_HEADER',
                       'order_no = '||to_char(I_order_no));
      open C_CHECK_ASN_ALLOC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_ASN_ALLOC',
                       'ALLOC_HEADER',
                       'order_no = '||to_char(I_order_no));
      fetch C_CHECK_ASN_ALLOC into L_asn_exist;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_ASN_ALLOC',
                       'ALLOC_HEADER',
                       'order_no = '||to_char(I_order_no));
      close C_CHECK_ASN_ALLOC;

      if L_asn_exist is NOT NULL then
         O_ASN_exists := TRUE;
      else
         O_ASN_exists := FALSE;
      end if;
   else
      O_exists := FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                   SQLERRM,
                   L_program,
                   to_char(SQLCODE));
      return FALSE;

END CHECK_OPEN_ALLOC;
-------------------------------------------------------------------------------------
FUNCTION UPDATE_ALLOC_STATUS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN is

   L_program   VARCHAR2(64) := 'ORDER_STATUS_SQL.UPDATE_ALLOC_STATUS';
   L_orig_ind  ORDHEAD.ORIG_IND%TYPE;


   cursor C_LOCK_ALLOC is
      select 'x'
   from alloc_header ah
       where order_no = I_order_no
       for update nowait;
 
   cursor C_orig_ind is
      select orig_ind
     from ordhead
    where order_no=I_order_no;
   
  

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC', NULL, NULL, NULL);
      return FALSE;
   end if;
   ---
   open C_LOCK_ALLOC;
   close C_LOCK_ALLOC;
   ---
  
   
   open C_orig_ind;
   fetch C_orig_ind into L_orig_ind;
   close C_orig_ind;

   if L_orig_ind!=0 then
   
      if WF_ALLOC_SQL.XDOCK_SYNC_F_ORDER(O_error_message,
                                        NULL,           --I_date
                                        I_order_no) = FALSE then
        return FALSE;
      end if;
      
      update alloc_header
         set status = 'A'
       where order_no = I_order_no
         and status not in ('A', 'C')
         and not exists(select 'X' 
                          from  alloc_detail ad, store s
                         where  ad.alloc_no = alloc_header.alloc_no
                           and  ad.to_loc     = s.store
                           and  ad.to_loc_type = 'S'
                           and  s.store_type = 'F'); 
   
   else
        update alloc_header
           set status = 'A'
         where order_no = I_order_no
           and status not in ('A', 'C');   
   end if;
 
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                   SQLERRM,
                   L_program,
                   to_char(SQLCODE));
      return FALSE;

END UPDATE_ALLOC_STATUS;
----------------------------------------------------------------------------------------------
FUNCTION INSERT_ORDER_REVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                               I_status          IN       ORDHEAD.STATUS%TYPE)

   RETURN BOOLEAN IS

   L_inv_param          VARCHAR2(30)            := NULL;
   L_program            TRAN_DATA.PGM_NAME%TYPE := 'ORDER_STATUS_SQL.INSERT_ORDER_REVISION';
   L_vdate        PERIOD.VDATE%TYPE       := GET_VDATE;
   L_exists             VARCHAR2(1)             := NULL;

   cursor C_ORDHEAD_REV_EXISTS is
      select 'X'
        from ordhead_rev
       where order_no = I_order_no
         and rev_no   = 0;

BEGIN
   ---Validate the Input parameters
   ---
   if I_order_no is NULL then
      L_inv_param := 'I_order_no';
   elsif I_status is NULL then
      L_inv_param := 'I_status';
   end if;

   if L_inv_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_ORDHEAD_REV_EXISTS','ordhead_rev',NULL);
   open C_ORDHEAD_REV_EXISTS;

   SQL_LIB.SET_MARK('FETCH','C_ORDHEAD_REV_EXISTS','ordhead_rev',NULL);
   fetch C_ORDHEAD_REV_EXISTS into L_exists;

   if (C_ORDHEAD_REV_EXISTS%NOTFOUND and
       I_status = 'A') then

      ---   Insert a record for ordhead_rev
      insert into ordhead_rev(rev_no,
                              origin_type,
                              order_no,
                              order_type,
                              not_before_date,
                              nbd_status,
                              not_after_date,
                              nad_status,
                              rev_date,
                              dept,
                              buyer,
                              supplier,
                              supp_add_seq_no,
                              loc_type,
                              location,
                              promotion,
                              qc_ind,
                              written_date,
                              otb_eow_date,
                              earliest_ship_date,
                              latest_ship_date,
                              close_date,
                              terms,
                              freight_terms,
                              orig_ind,
                              payment_method,
                              backhaul_type,
                              backhaul_allowance,
                              ship_method,
                              purchase_type,
                              status,
                              orig_approval_date,
                              orig_approval_id,
                              ship_pay_method,
                              fob_trans_res,
                              fob_trans_res_desc,
                              fob_title_pass,
                              fob_title_pass_desc,
                              edi_sent_ind,
                              edi_po_ind,
                              import_order_ind,
                              import_country_id,
                              po_ack_recvd_ind,
                              include_on_order_ind,
                              vendor_order_no,
                              exchange_rate,
                              factory,
                              agent,
                              discharge_port,
                              lading_port,
                              freight_contract_no,
                              po_type,
                              pre_mark_ind,
                              reject_code,
                              currency_code,
                              contract_no,
                              pickup_date,
                              pud_status,
                              split_ref_ordno,
                              pickup_loc,
                              pickup_no,
                              app_datetime,
                              comment_desc)
                      (select 0,
                              'V',
                              order_no,
                              order_type,
                              not_before_date,
                              0,
                              not_after_date,
                              0,
                              L_vdate,
                              dept,
                              buyer,
                              supplier,
                              supp_add_seq_no,
                              loc_type,
                              location,
                              promotion,
                              qc_ind,
                              written_date,
                              otb_eow_date,
                              earliest_ship_date,
                              latest_ship_date,
                              close_date,
                              terms,
                              freight_terms,
                              orig_ind,
                              payment_method,
                              backhaul_type,
                              backhaul_allowance,
                              ship_method,
                              purchase_type,
                              status,
                              orig_approval_date,
                              orig_approval_id,
                              ship_pay_method,
                              fob_trans_res,
                              fob_trans_res_desc,
                              fob_title_pass,
                              fob_title_pass_desc,
                              edi_sent_ind,
                              edi_po_ind,
                              import_order_ind,
                              import_country_id,
                              po_ack_recvd_ind,
                              include_on_order_ind,
                              vendor_order_no,
                              exchange_rate, 
                              factory,
                              agent,
                              discharge_port,
                              lading_port,
                              freight_contract_no,
                              po_type,
                              pre_mark_ind,
                              reject_code,
                              currency_code,
                              contract_no,
                              pickup_date,
                              0,
                              split_ref_ordno,
                              pickup_loc,
                              pickup_no,
                              app_datetime,
                              comment_desc
                         from ordhead
                        where order_no = I_order_no);
      ---
      ---   Insert a record for ordsku_rev
      insert into ordsku_rev(rev_no,
                             origin_type,
                             order_no,
                             item,
                             rev_date,
                             status,
                             origin_country_id,
                             earliest_ship_date,
                             latest_ship_date,
                             supp_pack_size,
                             non_scale_ind)
                     (select 0,
                             'V',
                             order_no,
                             item,
                             L_vdate,
                             1,
                             origin_country_id,
                             earliest_ship_date,
                             latest_ship_date,
                             supp_pack_size,
                             non_scale_ind
                        from ordsku
                       where order_no = I_order_no);
      ---
      ---   Insert a record for ordloc_rev
      insert into ordloc_rev(rev_no,
                             origin_type,
                             order_no,
                             item,
                             location,
                             loc_type,
                             unit_retail,
                             qty_ordered,
                             qty_received,
                             qty_cancelled,
                             cancel_code,
                             cancel_date,
                             cancel_id,
                             rev_date,
                             ship_date,
                             qty_status,
                             cost_status,
                             qty_prescaled,
                             unit_cost,
                             unit_cost_init,
                             cost_source,
                             non_scale_ind,
                             original_repl_qty,
                             tsf_po_link_no)
                     (select 0,
                             'V',
                             order_no,
                             item,
                             location,
                             loc_type,
                             unit_retail,
                             qty_ordered,
                             qty_received,
                             qty_cancelled,
                             cancel_code,
                             cancel_date,
                             cancel_id,
                             L_vdate,
                             NULL,
                             1,
                             1,
                             qty_prescaled,
                             unit_cost,
                             unit_cost_init,
                             cost_source,
                             non_scale_ind,
                             original_repl_qty,
                             tsf_po_link_no
                        from ordloc
                       where order_no = I_order_no);
      ---
      delete from rev_orders
       where order_no = I_order_no;

   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ORDHEAD_REV_EXISTS','ordhead_rev',NULL);
   close C_ORDHEAD_REV_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_ORDER_REVISION;
------------------------------------------------------------------------------------------------
FUNCTION CHECK_SHIPMENT_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
                               O_err_flag      IN OUT BOOLEAN)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50)            := 'ORDER_STATUS_SQL.CHECK_SHIPMENT_EXISTS';
   L_exists     VARCHAR2(1)             := NULL;

   cursor C_CHECK_SHIP is
      SELECT 'x'
        FROM shipment s,
             shipsku k
       WHERE s.order_no     = I_order_no
         AND s.status_code  != 'C'
         AND s.shipment     = k.shipment
         AND rownum         = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_CHECK_SHIP','shipment','order no: '||TO_CHAR(I_order_no));
   open C_CHECK_SHIP;
   ---
   SQL_LIB.SET_MARK('FETCH','C_CHECK_SHIP','shipment','order no: '||TO_CHAR(I_order_no));
   fetch C_CHECK_SHIP into L_exists;
   ---
   if C_CHECK_SHIP%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_SHIP','shipment','order no: '||TO_CHAR(I_order_no));
      close C_CHECK_SHIP;
      O_err_flag := TRUE;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_SHIP','shipment','order no: '||TO_CHAR(I_order_no));
   close C_CHECK_SHIP;
   ---
   O_err_flag := FALSE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      O_err_flag := FALSE;
      return FALSE;
END CHECK_SHIPMENT_EXISTS;
-------------------------------------------------------------------------
FUNCTION NEXT_TAX_EVENT_ID (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_tax_event_id IN OUT TAX_CALC_EVENT.TAX_EVENT_ID%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORDER_STATUS_SQL.NEXT_TAX_EVENT_ID';
   L_exists       VARCHAR2(1)  := 'N';
   L_tax_event_id TAX_CALC_EVENT.TAX_EVENT_ID%TYPE;
   L_wrap_seq_no  TAX_CALC_EVENT.TAX_EVENT_ID%TYPE;

   cursor C_NEXTVAL is
      SELECT tax_event_id_sequence.NEXTVAL
        FROM dual;

   cursor C_SEQ_EXISTS is
      SELECT 'Y'
        FROM tax_calc_event
       WHERE tax_event_id = L_tax_event_id;

BEGIN

   open C_NEXTVAL;
   fetch C_NEXTVAL into L_tax_event_id;
   close C_NEXTVAL;

   -- Save the first sequence value fetched.
   L_wrap_seq_no := L_tax_event_id;

   LOOP
      -- See if the value is already in use.  If it is not in use then
      -- it is valid and we return.
      L_exists := 'N';
      open  C_SEQ_EXISTS;
      fetch C_SEQ_EXISTS into L_exists;
      close C_SEQ_EXISTS;
      --
      if L_exists = 'N'  then
         O_tax_event_id := L_tax_event_id;
         return TRUE;
      end if;

      -- The value was already in use so fetch the next sequence value.
      open  C_NEXTVAL;
      fetch C_NEXTVAL into L_tax_event_id;
      close C_NEXTVAL;

      -- If the sequence cycled (looped around) and we are back to
      -- the first sequence value selected, then return with an error
      -- since all sequence values are already in use.
      if L_tax_event_id = L_wrap_seq_no then
         O_error_message := SQL_LIB.CREATE_MSG('NO_TAX_EVENT_ID',NULL,NULL,NULL);
         return FALSE;
      end if;

   END LOOP;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END NEXT_TAX_EVENT_ID;
-------------------------------------------------------------------------
FUNCTION INSERT_ORD_TAX_BREAKUP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50):= 'ORDER_STATUS_SQL.INSERT_ORD_TAX_BREAKUP';
   L_vdate             PERIOD.VDATE%TYPE  := GET_VDATE;
   L_run_type          TAX_EVENT_RUN_TYPE.RUN_TYPE%TYPE;
   L_tax_event_id      TAX_CALC_EVENT.TAX_EVENT_ID%TYPE;
   L_invalid_run_type  EXCEPTION;

   L_queue_name         VARCHAR2(200);
   L_enqueue_options    DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle     RAW(16);
   L_message            tax_event_msg ;
   L_owner              VARCHAR2(30);

   L_l10n_obj           L10N_OBJ := L10N_OBJ();

   cursor C_GET_OWNER is
      select table_owner
        from system_options;

   cursor C_GET_TAX_EVENT_RUN_TYPE is
      SELECT run_type
        FROM tax_event_run_type
       WHERE tax_event_type = 'PO';

BEGIN

   if ORDER_STATUS_SQL.NEXT_TAX_EVENT_ID(O_error_message,
                                         L_tax_event_id) = FALSE then
      return FALSE;
   end if;

   open C_GET_TAX_EVENT_RUN_TYPE;
   fetch C_GET_TAX_EVENT_RUN_TYPE into L_run_type;
   close C_GET_TAX_EVENT_RUN_TYPE;

   if L_run_type = 'SYNC' then
      L_l10n_obj.procedure_key := 'LOAD_ORDER_TAX_OBJECT';
      L_l10n_obj.doc_type := 'PO';
      L_l10n_obj.doc_id := I_order_no;

      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_l10n_obj) = FALSE then
         return FALSE;
      end if;

   elsif L_run_type = 'ASYNC' then
      INSERT INTO tax_calc_event(tax_event_id,
                                 tax_event_type,
                                 order_no,
                                 tax_event_result,
                                 create_datetime,
                                 create_id,
                                 last_update_datetime,
                                 last_update_id)
                         VALUES (L_tax_event_id,
                                 'PO',
                                 I_order_no,
                                 'N',
                                 L_vdate,
                                 USER,
                                 L_vdate,
                                 USER);

      open  C_GET_OWNER;
      fetch C_GET_OWNER into L_owner;
      close C_GET_OWNER;

      L_queue_name := L_owner || '.tax_event_queue';

      L_message := tax_event_msg(L_tax_event_id);

      DBMS_AQ.ENQUEUE(
         queue_name         => L_queue_name,
         enqueue_options    => L_enqueue_options,
         message_properties => L_message_properties,
         payload            => L_message,
         msgid              => L_message_handle);
   else
      raise L_invalid_run_type;
   end if;

return TRUE;

EXCEPTION
   WHEN L_invalid_run_type THEN
      O_error_message := SQL_LIB.CREATE_MSG('INV_TAX_EVENT_RUN_TYPE');
      return FALSE;
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_ORD_TAX_BREAKUP;
--------------------------------------------------------------------------
FUNCTION CANCEL_SHIPMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_function   VARCHAR2(50) := 'ORDER_STATUS_SQL.CANCEL_SHIPMENT';

   cursor C_CANCEL_SHIPMENT is
      select sh.shipment
        from shipment sh,
             shipsku sk
       where sh.order_no = I_order_no
         and sh.shipment = sk.shipment
         and ((sh.status_code = 'I')
               or(sh.status_code = 'R'
                  and NVL(sk.qty_received,0) < NVL(sk.qty_expected,0)));
BEGIN

   for C_cancel_rec in C_CANCEL_SHIPMENT LOOP

      update shipment
         set status_code = 'C'
       where order_no = I_order_no
         and shipment = C_cancel_rec.shipment;

   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CANCEL_SHIPMENT;
--------------------------------------------------------------------------
FUNCTION ADJUST_DSD_ORDER(O_error_message IN OUT VARCHAR2,
                          I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
                          I_item          IN     ORDLOC.ITEM%TYPE,
                          I_location      IN     ORDLOC.LOCATION%TYPE,
                          I_adjust_qty    IN     ORDLOC.QTY_RECEIVED%TYPE,
                          I_cancel_id     IN     ORDLOC.CANCEL_ID%TYPE)
RETURN BOOLEAN IS

   L_function       VARCHAR2(50)               := 'ORDER_STATUS_SQL.ADJUST_DSD_ORDER';
   L_cancel_qty_old ORDLOC.QTY_CANCELLED%TYPE  := NULL;
   L_order_qty_old  ORDLOC.QTY_ORDERED%TYPE    := NULL;
   L_order_qty_new  ORDLOC.QTY_ORDERED%TYPE    := NULL;
   L_cancel_qty_new ORDLOC.QTY_CANCELLED%TYPE  := NULL;
   L_receive_qty    ORDLOC.QTY_RECEIVED%TYPE   := NULL;
   L_cancel_date    ORDLOC.CANCEL_DATE%TYPE    := NULL;
   L_cancel_code    ORDLOC.CANCEL_CODE%TYPE    := NULL;
   L_cancel_id      ORDLOC.CANCEL_ID%TYPE      := NULL;
   L_table          VARCHAR2(50)               := NULL;
   L_rowid          ROWID;

   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_CANCEL_QTY is
      select nvl(qty_cancelled,0),
             nvl(qty_ordered,0),
             nvl(qty_received,0),
             rowid
        from ordloc
       where order_no = I_order_no
         and item = I_item
         and location = I_location
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_CANCEL_QTY','ordloc',
                    'order no: '||TO_CHAR(I_order_no)||
                    ', Item: '||I_item||
                    ', location: '||TO_CHAR(I_location));

   open C_GET_CANCEL_QTY;

   SQL_LIB.SET_MARK('FETCH','C_GET_CANCEL_QTY','ordloc',
                    'order no: '||TO_CHAR(I_order_no)||
                    ', Item: '||I_item||
                    ', location: '||TO_CHAR(I_location));

   fetch C_GET_CANCEL_QTY into L_cancel_qty_old,
                               L_order_qty_old,
                               L_receive_qty,
                               L_rowid;

   SQL_LIB.SET_MARK('CLOSE','C_GET_CANCEL_QTY','ordloc',
                    'order no: '||TO_CHAR(I_order_no)||
                    ', Item: '||I_item||
                    ', location: '||TO_CHAR(I_location));

   close C_GET_CANCEL_QTY;

   if (L_order_qty_old + L_cancel_qty_old) > 0 then
      ---
      if (L_receive_qty + I_adjust_qty) >= (L_order_qty_old + L_cancel_qty_old) then   -- Overage
         L_order_qty_new  := (L_order_qty_old + L_cancel_qty_old);
         L_cancel_qty_new := NULL;
      elsif (L_receive_qty + I_adjust_qty) < (L_order_qty_old + L_cancel_qty_old) then -- Sortage
         L_order_qty_new  := (L_receive_qty + I_adjust_qty);
         L_cancel_qty_new := (L_order_qty_old + L_cancel_qty_old) - (L_receive_qty + I_adjust_qty);
      end if;
      ---
      if L_cancel_qty_new > 0 then
         L_cancel_date := get_vdate;
         L_cancel_code := 'A';
         L_cancel_id   := I_cancel_id;
      else
         L_cancel_date := NULL;
         L_cancel_code := NULL;
         L_cancel_id   := NULL;
      end if;
      ---
      L_table := 'ordloc';
      SQL_LIB.SET_MARK('UPDATE',NULL,'ordloc',
                      'order no: '||TO_CHAR(I_order_no)||
                      ', item: '||I_item||
                      ', location: '||TO_CHAR(I_location));

      update ordloc
        set qty_ordered   = L_order_qty_new,
            qty_cancelled = L_cancel_qty_new,
            cancel_date   = L_cancel_date,
            cancel_code   = L_cancel_code,
            cancel_id     = L_cancel_id
      where rowid = L_rowid;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',L_table,
                                             TO_CHAR(I_order_no),
                                             I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADJUST_DSD_ORDER;
--------------------------------------------------------------------------
FUNCTION CHECK_SUBMIT_APPROVE_WRP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_invalid_date       IN OUT   VARCHAR2,
                                  O_store_closed_ind   IN OUT   VARCHAR2,
                                  O_store_opened_ind   IN OUT   VARCHAR2,
                                  O_clearance_ind      IN OUT   VARCHAR2,
                                  O_wksht_recs_ind     IN OUT   VARCHAR2,
                                  O_hts_ind            IN OUT   VARCHAR2,
                                  O_valid_lc_ind       IN OUT   VARCHAR2,
                                  O_scale_ind          IN OUT   VARCHAR2,
                                  O_recalc_ind         IN OUT   VARCHAR2,
                                  O_round_ind          IN OUT   VARCHAR2,
                                  O_invalid_items      IN OUT   VARCHAR2,
                                  O_error_collection   IN OUT   NOCOPY OBJ_ERROR_TBL,
                                  I_source             IN       VARCHAR2 DEFAULT NULL,
                                  I_order_no           IN       ORDHEAD.ORDER_NO%TYPE,
                                  I_new_status         IN       ORDHEAD.STATUS%TYPE,
                                  I_not_before_date    IN       ORDHEAD.NOT_BEFORE_DATE%TYPE,
                                  I_not_after_date     IN       ORDHEAD.NOT_AFTER_DATE%TYPE,
                                  I_supplier           IN       ORDHEAD.SUPPLIER%TYPE,
                                  I_dept               IN       ORDHEAD.DEPT%TYPE,
                                  I_check_loc_status   IN       VARCHAR2 DEFAULT      'N')
RETURN INTEGER IS
   L_function       VARCHAR2(50)               := 'ORDER_STATUS_SQL.CHECK_SUBMIT_APPROVE_WRP';
BEGIN


   if (ORDER_STATUS_SQL.CHECK_SUBMIT_APPROVE(O_error_message,
                                             O_invalid_date,
                                             O_store_closed_ind,
                                             O_store_opened_ind,
                                             O_clearance_ind,
                                             O_wksht_recs_ind,
                                             O_hts_ind,
                                             O_valid_lc_ind,
                                             O_scale_ind,
                                             O_recalc_ind,
                                             O_round_ind,
                                             O_invalid_items,
                                             O_error_collection,
                                             I_source,
                                             I_order_no,
                                             I_new_status,
                                             I_not_before_date,
                                             I_not_after_date,
                                             I_supplier,
                                             I_dept,
                                             I_check_loc_status) = FALSE)  then
    return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return 0;
END CHECK_SUBMIT_APPROVE_WRP;
----------------------------------------------------------------------------------------------
FUNCTION REJECT_PO_NOTIFICATION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN NUMBER IS

   L_program              VARCHAR2(61) := 'ORDER_STATUS_SQL.REJECT_PO_NOTIFICATION';
   L_task_flow_url        VARCHAR2(4000);
   
   L_notification_id      NUMBER       := RAF_NOTIFICATION_SEQ.NEXTVAL;
   L_notify_context       VARCHAR2(4000);
   L_notification_type    NUMBER       := 5347;
   L_severity             NUMBER       := 1;
   L_application_code     VARCHAR2(50) := 'Rms';
   L_notify_desc          VARCHAR2(255);
   L_notify_title         VARCHAR2(200);
   L_error_type           VARCHAR2(5)  := NULL;
   L_notification_active  VARCHAR2(1)  := 'U';
   L_notif_comnd_cls_name VARCHAR2(10) := null;
   L_create_date          TIMESTAMP    := to_timestamp(to_char(sysdate,'MM/DD/RRRR HH12:MI:SS'), 'MM/DD/RRRR HH12:MI:SS');
   L_recipent_id          VARCHAR2(64);
   L_created_by           VARCHAR2(64) := GET_USER; 
   L_launchable           VARCHAR2(1)  := 'Y';
   L_obj_ver_num          NUMBER       := 1;
   
   cursor C_GET_DESC_TL is
      select description
        from raf_notification_type_tl
       where notification_type = L_notification_type
         and language = userenv('LANG');
         
   cursor C_GET_RECIPENT_ID is
      select create_id
        from ordhead
       where order_no = I_order_no;
       
BEGIN
   open C_GET_RECIPENT_ID;
   fetch C_GET_RECIPENT_ID into L_recipent_id;
   close C_GET_RECIPENT_ID;
   --
   if upper(L_recipent_id) != upper(L_created_by) then
      L_task_flow_url := '/WEB-INF/oracle/retail/apps/rms/procurement/view/ordersetup/purchaseorder/header/flow/MaintainPurchaseOrderFlow.xml#MaintainPurchaseOrderFlow';
      L_task_flow_url := L_task_flow_url ||'|pmOrderNo='||I_order_no||'|pmOrderMode=EDIT|pmRandomNo='||to_char(sysdate,'MMDDRRRRHH24MISS');
   
      open C_GET_DESC_TL;
      fetch C_GET_DESC_TL into L_notify_title;
      close C_GET_DESC_TL;
      --
      L_notify_context := 'title=' || L_notify_title || '|url=' || L_task_flow_url;
      --
      L_notify_desc := SQL_LIB.CREATE_MSG('NOTIFICATION_REJECT_PO',I_order_no);
      SQL_LIB.API_MSG(L_error_type, L_notify_desc);
      --
      if RAF_NOTIFICATION_TASK_PKG.CREATE_NOTIFY_TASK_AUTOTRAN(O_error_message,
                                                               L_notification_id,
                                                               L_notify_context,
                                                               L_notification_type,
                                                               L_severity,
                                                               L_application_code,
                                                               L_notify_desc,
                                                               L_notification_active,
                                                               L_notif_comnd_cls_name,
                                                               L_create_date,
                                                               L_recipent_id,
                                                               L_created_by,
                                                               L_launchable,
                                                               L_obj_ver_num) = 0 then
         return 0;
      end if;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      if C_GET_DESC_TL%ISOPEN then close C_GET_DESC_TL; end if;
      if C_GET_RECIPENT_ID%ISOPEN then close C_GET_RECIPENT_ID; end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;
END REJECT_PO_NOTIFICATION;
--------------------------------------------------------------------------------
END ORDER_STATUS_SQL;
/
