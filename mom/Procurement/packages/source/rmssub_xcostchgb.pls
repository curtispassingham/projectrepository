CREATE OR REPLACE PACKAGE BODY RMSSUB_XCOSTCHG AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPEC
-------------------------------------------------------------------------------------------------------
   -- Function Name: CAPITALIZE_FIELDS
   -- Purpose      : This private function should convert all char fields from the message to uppercase.
-------------------------------------------------------------------------------------------------------
FUNCTION CAPITALIZE_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_message        IN OUT NOCOPY   "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2);
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2) IS

   PROGRAM_ERROR    EXCEPTION;

   L_program               VARCHAR2(50) := 'RMSSUB_XCOSTCHG.CONSUME';
   L_message               "RIB_XCostChgDesc_REC";
   L_costchg_rec           RMSSUB_XCOSTCHG.COST_CHANGE_RECTYPE;

BEGIN

   O_STATUS_CODE := API_CODES.SUCCESS;

   -- perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   if LOWER(I_message_type) = LP_mod_type then
      L_message := treat(I_MESSAGE AS "RIB_XCostChgDesc_REC");

      if CAPITALIZE_FIELDS(O_error_message,
                           L_message) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      
      select API_SEQ.NEXTVAL
        into RMSSUB_XCOSTCHG.LP_api_seq_no
        from sys.dual;
        
      -- Validate Message Contents
      if RMSSUB_XCOSTCHG_VALIDATE.CHECK_MESSAGE(O_error_message,
                                                L_costchg_rec,
                                                L_message) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- INSERT/UPDATE table
      if RMSSUB_XCOSTCHG_SQL.PERSIST(O_error_message,
                                     L_costchg_rec,
                                     L_message) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      --Delete api Temp tables
      if DELETE_API(O_error_message) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      --
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'), NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then
       RMSSUB_XCOSTCHG.LP_api_seq_no := NULL;
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);
   when OTHERS then
       RMSSUB_XCOSTCHG.LP_api_seq_no := NULL;
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);

END CONSUME;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION CAPITALIZE_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_message        IN OUT NOCOPY   "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'RMSSUB_XCOSTCHG.CAPITALIZE_FIELDS';

BEGIN

   IO_message.origin_country_id := UPPER(IO_message.origin_country_id);
   IO_message.diff_id           := UPPER(IO_message.diff_id);
   IO_message.currency_code     := UPPER(IO_message.currency_code);
   IO_message.hier_level        := UPPER(IO_message.hier_level);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CAPITALIZE_FIELDS;

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE BODY
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2) IS

   L_program VARCHAR2(50) := 'RMSSUB_XCOSTCHG.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);
EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_API(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XCOSTCHG.DELETE_API';

BEGIN
   ---
   delete from api_item where seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no;
   delete from api_iscl where seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no;
   delete from api_price_hist where seq_no  = RMSSUB_XCOSTCHG.LP_api_seq_no;
   ---
   RMSSUB_XCOSTCHG.LP_api_seq_no := NULL;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      RMSSUB_XCOSTCHG.LP_api_seq_no := NULL;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_API;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XCOSTCHG;
/
