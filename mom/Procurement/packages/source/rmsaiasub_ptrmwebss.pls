/******************************************************************************
* Service Name     : PayTermService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/PayTermService/v1
* Description      : $service.documentation
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE PAYTERMSERVICEPROVIDERIMPL AUTHID CURRENT_USER AS


/******************************************************************************
 *
 * Operation       : createPayTermDesc
 * Description     :  Create new PayTerm. This includes creation of new payment terms. 
 * 
 * Input           : "RIB_PayTermDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermDesc/v1
 * Description     : PayTermDesc object consists of inputs to terms,terms code,term desc .The PayTermDesc also contains the payterm xref keys that are used in the AIA integration layer for reference.
 * 
 * Output          : "RIB_PayTermRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermRef/v1
 * Description     : PayTermRef object consists of information about the created Payment Terms.This includes payterm,term sequence and Payterm Xref keys.The AIA Integration layer stores the payterm xref keys along with payterm Ids for reference.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the PayTerm already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createPayTermDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_PayTermDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_PayTermRef_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : createHeaderPayTermDesc
 * Description     : Create a header record for the new PayTerm. 
 * 
 * Input           : "RIB_PayTermDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermDesc/v1
 * Description     : PayTermDesc object consists of inputs to terms,terms code,term desc .The PayTermDesc also contains the payterm xref keys that are used in the AIA integration layer for reference.
 * 
 * Output          : "RIB_PayTermRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermRef/v1
 * Description     : PayTermRef object consists of information about the created Payment Terms.This includes payterm,term sequence and Payterm Xref keys.The AIA Integration layer stores the payterm xref keys along with payterm Ids for reference.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the PayTerm already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createHeaderPayTermDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_PayTermDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_PayTermRef_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : createDetailPayTermDesc
 * Description     : Create a detail record for the PayTerm 
 * 
 * Input           : "RIB_PayTermDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermDesc/v1
 * Description     : PayTermDesc object consists of inputs to terms,terms code,term desc .The PayTermDesc also contains the payterm xref keys that are used in the AIA integration layer for reference.
 * 
 * Output          : "RIB_PayTermRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermRef/v1
 * Description     : PayTermRef object consists of information about the created Payment Terms.This includes payterm,term sequence and Payterm Xref keys.The AIA Integration layer stores the payterm xref keys along with payterm Ids for reference.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the PayTerm already exist.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createDetailPayTermDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_PayTermDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_PayTermRef_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : updatePayTermDesc
 * Description     : Updates to PayTerm. 
 * 
 * Input           : "RIB_PayTermDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermDesc/v1
 * Description     : PayTermDesc object consists of inputs to terms,terms code,term desc .The PayTermDesc also contains the payterm xref keys that are used in the AIA integration layer for reference.
 * 
 * Output          : "RIB_PayTermDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermDesc/v1
 * Description     : PayTermDesc object consists of updated terms,terms code,term desc info.The PayTermDesc also contains the payterm xref keys that are used in the AIA integration layer for reference.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when the PayTerm is not found.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE updatePayTermDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_PayTermDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_PayTermDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : updateHeaderPayTermDesc
 * Description     : Updates to PayTerm header 
 * 
 * Input           : "RIB_PayTermDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermDesc/v1
 * Description     : PayTermDesc object consists of inputs to terms,terms code,term desc .The PayTermDesc also contains the payterm xref keys that are used in the AIA integration layer for reference.
 * 
 * Output          : "RIB_PayTermDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermDesc/v1
 * Description     : PayTermDesc object consists of updated terms,terms code,term desc info.The PayTermDesc also contains the payterm xref keys that are used in the AIA integration layer for reference.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when PayTerm is not found.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE updateHeaderPayTermDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_PayTermDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_PayTermDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : updateDetailPayTermDesc
 * Description     : Updates to PayTerm detail. 
 * 
 * Input           : "RIB_PayTermDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermDesc/v1
 * Description     : PayTermDesc object consists of inputs to terms,terms code,term desc .The PayTermDesc also contains the payterm xref keys that are used in the AIA integration layer for reference.
 * 
 * Output          : "RIB_PayTermDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermDesc/v1
 * Description     : PayTermDesc object consists of updated terms,terms code,term desc info.The PayTermDesc also contains the payterm xref keys that are used in the AIA integration layer for reference.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when PayTerm is not found.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE updateDetailPayTermDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_PayTermDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_PayTermDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : findPayTermDesc
 * Description     : Find PayTerms. 
 * 
 * Input           : "RIB_PayTermRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermRef/v1
 * Description     : PayTermRef object consists of information about the Payment Terms.This includes payterm,term sequence and Payterm Xref keys.The AIA Integration layer stores the payterm xref keys along with payterm Ids for reference..
 * 
 * Output          : "RIB_PayTermDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermDesc/v1
 * Description     : PayTermDesc object consists of information about terms,terms code,term desc .The PayTermDesc also contains the payterm xref keys that are used in the AIA integration layer for reference.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when PayTerm is not found.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE findPayTermDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_PayTermRef_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_PayTermDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : deletePayTermDesc
 * Description     : Delete the PayTerm. 
 * 
 * Input           : "RIB_PayTermRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermRef/v1
 * Description     : PayTermRef object consists of input to delete Payment Terms.This includes payterm id
 * 
 * Output          : "RIB_PayTermRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PayTermRef/v1
 * Description     : PayTermRef object consists of deleted Payment Terms information.This includes payterm id.
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityNotFoundWSFaultException
 * Description     : Throw this exception when PayTerm is not found.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE deletePayTermDesc(
                          I_serviceOperationContext IN  "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_PayTermRef_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_PayTermRef_REC"
                          );
/******************************************************************************/



END PayTermServiceProviderImpl;
/