
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_PAYTERM_VALIDATE AS


--------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This private function will check all required fields for the create and
   --                modify messages to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_PayTermDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will populate all the fields in the TERMS_SQL.PAYTERM_REC with
   --                the values from the create and modify RIB messages.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message      OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_dml_rec            OUT NOCOPY   TERMS_SQL.PAYTERM_REC,
                         I_message         IN              "RIB_PayTermDesc_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
   
-------------------------------------------------------------------------------------------------------
-- Function Name : FUNCTION CHECK_ENABLED 
--               : This function will verify that the enabled_flag is set correctly for the available
--                 activate range
-------------------------------------------------------------------------------------------------------

FUNCTION CHECK_ENABLED(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_message         IN       "RIB_PayTermDesc_REC")
   RETURN BOOLEAN;  
   
-------------------------------------------------------------------------------------------------------
-- Function Name : FUNCTION CHECK_TERMS_HEAD 
--               : This function checks existence of terms_head records before crating detail record

-------------------------------------------------------------------------------------------------------
  
FUNCTION CHECK_TERMS_HEAD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN       "RIB_PayTermDesc_REC",
                           I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;   
  
-------------------------------------------------------------------------------------------------------
-- Function Name : FUNCTION CHECK_TERMS_DETAIL 
--               : This function checks existence of terms_detail records before updating detail record

-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_TERMS_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_PayTermDesc_REC",
                            I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;   

-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message    OUT     RTK_ERRORS.RTK_TEXT%TYPE,  
                       O_dml_rec	      OUT     TERMS_SQL.PAYTERM_REC,
                       I_message            IN        "RIB_PayTermDesc_REC",
                       I_message_type	    IN	      VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_PAYTERM_VALIDATE.CHECK_MESSAGE';
  
BEGIN
   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message,
                                I_message_type) then
      return FALSE;
   end if;
   
   if not CHECK_ENABLED(O_error_message,   
                        I_message) then
      return FALSE;
   end if;   
   
   if not CHECK_TERMS_HEAD(O_error_message,
                           I_message,
                           I_message_type) then
      return FALSE;
   end if;
   
   if not CHECK_TERMS_DETAIL(O_error_message,
                             I_message,
                             I_message_type) then
         return FALSE;
   end if;
   
   if not POPULATE_RECORD(O_error_message,
                          O_dml_rec,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_PayTermDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_PAYTERM_VALIDATE.CHECK_REQUIRED_FIELDS';
   L_count        NUMBER       := 0;
   L_dml_rec      TERMS_SQL.PAYTERM_REC := NULL;
   L_valid        BOOLEAN := FALSE;


BEGIN

   if I_message.terms is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Terms');
      return FALSE;
   end if;
   
    if I_message.terms_code is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Terms Code');
         return FALSE;
   end if;
   
   if I_message.terms_desc is NULL then
        O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Terms Description ');
      return FALSE;
   end if;   
   
   if I_message.rank is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'rank');
      return FALSE;  
   end if;

   if I_message_type <> RMSSUB_PAYTERM.HDR_UPD then
      if I_message.paytermdtl_tbl is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC');
         return FALSE;
      end if;

      FOR i in I_message.paytermdtl_tbl.first..I_message.paytermdtl_tbl.last LOOP
         if I_message.paytermdtl_tbl(i).due_days is NULL
            and I_message.paytermdtl_tbl(i).due_dom is NOT NULL 
            and I_message.paytermdtl_tbl(i).due_mm_fwd is NOT NULL then
            L_valid := TRUE;
         elsif I_message.paytermdtl_tbl(i).due_days is NOT NULL
            and I_message.paytermdtl_tbl(i).due_dom is NULL 
            and I_message.paytermdtl_tbl(i).due_mm_fwd is NULL then
            L_valid := TRUE;
         elsif I_message.paytermdtl_tbl(i).due_days is NOT NULL
            and I_message.paytermdtl_tbl(i).due_dom is NOT NULL 
            and I_message.paytermdtl_tbl(i).due_mm_fwd is NOT NULL then
            L_valid := TRUE;
         else
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_DUE_DAYS_DATA');
            return false;
         end if;

         if I_message.paytermdtl_tbl(i).disc_dom is NOT NULL 
            and I_message.paytermdtl_tbl(i).disc_mm_fwd is NOT NULL 
            and I_message.paytermdtl_tbl(i).discdays is NULL then 
            L_valid := TRUE;
         elsif I_message.paytermdtl_tbl(i).disc_dom is NULL 
            and I_message.paytermdtl_tbl(i).disc_mm_fwd is NULL 
            and I_message.paytermdtl_tbl(i).discdays is NOT NULL then
            L_valid := TRUE;
         elsif I_message.paytermdtl_tbl(i).disc_dom is NULL 
            and I_message.paytermdtl_tbl(i).disc_mm_fwd is NULL 
            and I_message.paytermdtl_tbl(i).discdays is NULL then 
            L_valid := TRUE;
         elsif I_message.paytermdtl_tbl(i).disc_dom is NOT NULL 
            and I_message.paytermdtl_tbl(i).disc_mm_fwd is NOT NULL 
            and I_message.paytermdtl_tbl(i).discdays is NOT NULL then 
            L_valid := TRUE;
         else 
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_DISC_DAYS_DATA');
            return FALSE;
         end if;
        
         if I_message.paytermdtl_tbl(i).due_max_amount is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Due Max Amount');
            return FALSE;
         end if;

         if I_message.paytermdtl_tbl(i).percent is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Percent');
            return FALSE;
         end if;

         if I_message.paytermdtl_tbl(i).enabled_flag is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Enabled Flag');
            return FALSE;
         end if;

      end LOOP;
   end if;
   
   if L_valid = TRUE then
      return TRUE; 
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message      OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_dml_rec            OUT NOCOPY   TERMS_SQL.PAYTERM_REC,
                         I_message         IN              "RIB_PayTermDesc_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_PAYTERM_VALIDATE.POPULATE_RECORD';
   
   CURSOR c_hdr_chk IS
   select 'x'
     from terms_head
    where terms = I_message.terms;
   

BEGIN

   O_dml_rec.terms_head_row.terms        := I_message.terms;
   O_dml_rec.terms_code                  := I_message.terms_code;
   O_dml_rec.terms_desc                  := I_message.terms_desc;
   O_dml_rec.terms_head_row.rank         := I_message.rank;
   ---
   if I_message_type <> RMSSUB_PAYTERM.HDR_UPD then   

      -- initialize table
      O_dml_rec.terms_details := TERMS_SQL.TERMS_DETAIL_TBL();
   
      FOR i in I_message.paytermdtl_tbl.first..I_message.paytermdtl_tbl.last LOOP
         O_dml_rec.terms_details.EXTEND;
         O_dml_rec.terms_details(i).terms                := I_message.terms;
         O_dml_rec.terms_details(i).terms_seq            := I_message.paytermdtl_tbl(i).terms_seq;
         O_dml_rec.terms_details(i).duedays              := I_message.paytermdtl_tbl(i).due_days;
         O_dml_rec.terms_details(i).due_max_amount       := I_message.paytermdtl_tbl(i).due_max_amount;
         O_dml_rec.terms_details(i).due_dom              := I_message.paytermdtl_tbl(i).due_dom;
         O_dml_rec.terms_details(i).due_mm_fwd           := I_message.paytermdtl_tbl(i).due_mm_fwd;
         O_dml_rec.terms_details(i).discdays             := I_message.paytermdtl_tbl(i).discdays;
         O_dml_rec.terms_details(i).percent              := I_message.paytermdtl_tbl(i).percent;
         O_dml_rec.terms_details(i).disc_dom             := I_message.paytermdtl_tbl(i).disc_dom;
         O_dml_rec.terms_details(i).disc_mm_fwd          := I_message.paytermdtl_tbl(i).disc_mm_fwd;
         O_dml_rec.terms_details(i).cutoff_day           := I_message.paytermdtl_tbl(i).cutoff_day;
	   O_dml_rec.terms_details(i).fixed_date           := I_message.paytermdtl_tbl(i).fixed_date;         
	   O_dml_rec.terms_details(i).enabled_flag         := I_message.paytermdtl_tbl(i).enabled_flag;
	   O_dml_rec.terms_details(i).start_date_active    := I_message.paytermdtl_tbl(i).start_date_active; 
	   O_dml_rec.terms_details(i).end_date_active      := I_message.paytermdtl_tbl(i).end_date_active; 
	 
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------

FUNCTION CHECK_ENABLED(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_message         IN       "RIB_PayTermDesc_REC")
   RETURN BOOLEAN IS

   PROGRAM_ERROR   EXCEPTION;
   L_start_date    TERMS_DETAIL.START_DATE_ACTIVE%TYPE     := NULL;
   L_end_date      TERMS_DETAIL.START_DATE_ACTIVE%TYPE     := NULL;
   L_vdate         PERIOD.VDATE%TYPE                       := GET_VDATE;
   L_terms_seq     TERMS_DETAIL.TERMS_SEQ%TYPE             := NULL;
   
   L_program      VARCHAR2(50) := 'RMSSUB_PAYTERM_VALIDATE.CHECK_ENABLED';

   CURSOR c_get_daterange IS
      select start_date_active,
             end_date_active
        from terms_detail
       where terms = I_message.terms
         and terms_seq = L_terms_seq;

BEGIN
   ---
   FOR i in I_message.paytermdtl_tbl.first..I_message.paytermdtl_tbl.last LOOP
    

      L_terms_seq := I_message.paytermdtl_tbl(i).terms_seq;
     
      open c_get_daterange;
      fetch c_get_daterange into L_start_date,
                                 L_end_date;
      close c_get_daterange;

   --- If L_payrec.date is NOT NULL, it will be written ---
   
      L_start_date := NVL(I_message.paytermdtl_tbl(i).start_date_active, L_start_date);
      L_end_date   := NVL(I_message.paytermdtl_tbl(i).end_date_active, L_end_date);
   ---
      if L_start_date > L_end_date then
         raise PROGRAM_ERROR;
      end if;

   --- Does enabled_flag correspond to date range?
      if L_vdate < NVL(L_start_date, L_vdate) AND I_message.paytermdtl_tbl(i).enabled_flag = 'Y' then
         raise PROGRAM_ERROR;
      elsif L_vdate > NVL(L_end_date, L_vdate) AND I_message.paytermdtl_tbl(i).enabled_flag = 'Y' then
         raise PROGRAM_ERROR;
      elsif (L_vdate >= NVL(L_start_date, L_vdate - 1) AND
          L_vdate <= NVL(L_end_date, L_vdate + 1)) AND I_message.paytermdtl_tbl(i).enabled_flag = 'N' then
      raise PROGRAM_ERROR;
      end if;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when PROGRAM_ERROR then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_message.paytermdtl_tbl(i).enabled_flag',
                                            'Enabled_flag inconsistent with date range',
                                            L_program);
      return FALSE;
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                   SQLERRM,
                                   'RMSSUB_PAYTERM_VALIDATE.CHECK_ENABLED',
                                   to_char(SQLCODE));
      return FALSE;

END CHECK_ENABLED;

-------------------------------------------------------------------------------------------------------

FUNCTION CHECK_TERMS_HEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN       "RIB_PayTermDesc_REC",
                          I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS
   
      
    L_program      VARCHAR2(50) := 'RMSSUB_PAYTERM_VALIDATE.CHECK_TEMRS_HEAD';
    
    
    
 BEGIN
 
 if I_message_type in (RMSSUB_PAYTERM.HDR_UPD, RMSSUB_PAYTERM.DTL_ADD) then
 
    if TERMS_SQL.HEADER_EXISTS(O_error_message,
                              I_message.terms) = FALSE then
       return FALSE;
    end if;
 end if;
 
 return TRUE;
 

 EXCEPTION
    when OTHERS then
       O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
       return FALSE;
 
 END CHECK_TERMS_HEAD;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_TERMS_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_PayTermDesc_REC",
                            I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS
   
      
    L_program      VARCHAR2(50) := 'RMSSUB_PAYTERM_VALIDATE.CHECK_TEMRS_HEAD';
    L_terms_seq     TERMS_DETAIL.TERMS_SEQ%TYPE             := NULL;
    
    
 BEGIN
 
 if I_message_type = RMSSUB_PAYTERM.DTL_UPD then
    FOR i in I_message.paytermdtl_tbl.first..I_message.paytermdtl_tbl.last LOOP
       L_terms_seq := I_message.paytermdtl_tbl(i).terms_seq;
       if TERMS_SQL.DETAIL_EXISTS(O_error_message,
                                I_message.terms,
                                L_terms_seq) = FALSE then
          return FALSE;
       end if;
    END LOOP;
 end if;
 
 return TRUE;
 
 EXCEPTION
    when OTHERS then
       O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
       return FALSE;
 
 END CHECK_TERMS_DETAIL;
------------------------------------------------------------------------------------------------------- 

END RMSSUB_PAYTERM_VALIDATE;
/
