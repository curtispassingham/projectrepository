create or replace
PACKAGE BODY DEAL_ORD_LIB_SQL AS
---------------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
---------------------------------------------------------------------------------------------------------------
-- Function: EXTERNAL_SHARE_APPLY_DEALS
-- Purpose :
----------------------------------------------------------------------------------------------------------------
FUNCTION EXTERNAL_SHARE_APPLY_DEALS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_order_no              IN       ORDLOC.ORDER_NO%TYPE,
                                    I_order_appr_ind        IN       DEAL_CALC_QUEUE.ORDER_APPR_IND%TYPE,
                                    I_recalc_all_ind        IN       DEAL_CALC_QUEUE.RECALC_ALL_IND%TYPE,
                                    I_override_manual_ind   IN       DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.EXTERNAL_SHARE_APPLY_DEALS';

BEGIN
   if DEAL_ORD_LIB_SQL.LIB_INIT(O_error_message) = FALSE then
      return FALSE;
   end if;

   if DEAL_ORD_LIB_SQL.EXTERNAL_PROCESS(O_error_message,
                                        I_order_no,
                                        I_order_appr_ind,
                                        I_recalc_all_ind,
                                        I_override_manual_ind) = FALSE then
      return FALSE;
   end if;

   if LP_buyer_pack_detected != 0 then
      O_error_message := SQL_LIB.CREATE_MSG('ORD_DEAL_BUYER_PACKS',
                                            I_order_no,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message :=       SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                to_char(SQLCODE));
   return FALSE;
END EXTERNAL_SHARE_APPLY_DEALS;
---------------------------------------------------------------------------------------------------------------
-- Function: EXTERNAL_PROCESS
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION EXTERNAL_PROCESS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no              IN       ORDLOC.ORDER_NO%TYPE,
                          I_order_appr_ind        IN       DEAL_CALC_QUEUE.ORDER_APPR_IND%TYPE,
                          I_recalc_all_ind        IN       DEAL_CALC_QUEUE.RECALC_ALL_IND%TYPE,
                          I_override_manual_ind   IN       DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE)
RETURN BOOLEAN IS

   L_program                VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.EXTERNAL_PROCESS';
   L_order_currency_code   CURRENCIES.CURRENCY_CODE%TYPE;
   L_online_ind            NUMBER(1) := 1;
   L_rec_to_process        NUMBER := 1;
   L_import_country_id     ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   L_txn_discount_exists   NUMBER:= 0;
   L_seq_no                ORDLOC_DISCOUNT.SEQ_NO%TYPE := 1;
   L_disc_item_tbl_cnt     NUMBER:= 1;--Input as a Index
   L_Zero                  NUMBER:= 0;
   L_one                   NUMBER:= 1;
   L_return                BINARY_INTEGER;

   L_order_tbl          order_tbl;
   L_order_tbl_valid    order_tbl;
   L_item_cost_tbl      item_cost_tbl;
   L_discount_build_tbl discount_build_tbl;
   L_deal_tbl           deal_tbl;
   O_disc_item_tbl      disc_item_tbl;
   L_disc_item_tbl      disc_item_tbl;

   /* Driving cursor */
   cursor C_ORDER is
      SELECT I_order_no order_no,
             I_recalc_all_ind recalc_all_ind,
             I_override_manual_ind override_manual_ind,
             I_order_appr_ind order_appr_ind,
             o.supplier,
             o.import_order_ind,
             o.import_country_id,
             o.not_before_date,
             o.currency_code,
             o.status,
             NVL(o.exchange_rate, 0) exchange_rate,
             s.currency_code supp_currency_code,
             s.bracket_costing_ind
        FROM ordhead o,
             sups s
       WHERE o.order_no = I_order_no
         AND o.status   != 'C'
         AND s.supplier = o.supplier;

   /* Cursor to retrieve the number of decimals for the order currency. */
   /* Round to this number of decimals before updating the ordloc.unit_cost value. */
   cursor c_cost_dec is
      SELECT currency_cost_dec
        FROM currencies
       WHERE currency_code = LP_order_currency_code;
BEGIN
   if I_order_no is NULL then
      O_error_message :=       SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  I_order_no,
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;

   open C_ORDER;
   fetch C_ORDER BULK COLLECT into L_order_tbl;
   close C_ORDER;

    if L_order_tbl is null or L_order_tbl.count = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('ORDER_CLOSED',
                                            I_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if DEAL_ORD_LIB_SQL.CHECK_LOCK_AND_VALIDATE(O_error_message,
                                               L_online_ind,
                                               L_order_tbl,
                                               L_order_tbl_valid,/*Containes only valid order records and used in all the process*/
                                               L_rec_to_process) = FALSE then
      return FALSE;
   end if;

   LP_order_no               := L_order_tbl_valid(1).order_no;
   LP_order_supplier         := L_order_tbl_valid(1).supplier;
   LP_order_not_before_date  := L_order_tbl_valid(1).not_before_date;
   LP_order_appr_ind         := L_order_tbl_valid(1).order_appr_ind;
   LP_order_status           := L_order_tbl_valid(1).status;
   LP_order_currency_code    := L_order_tbl_valid(1).currency_code;
   LP_order_exchange_rate    := L_order_tbl_valid(1).exchange_rate;
   LP_supp_currency_code     := L_order_tbl_valid(1).supp_currency_code;
   L_import_country_id       := L_order_tbl_valid(1).import_country_id;

   open c_cost_dec;
   fetch c_cost_dec into LP_num_cost_decimals;
   close c_cost_dec;

   if LP_num_cost_decimals is null then
      O_error_message := SQL_LIB.CREATE_MSG('CUR_CODE_NOT',
                                            L_order_currency_code,
                                            I_order_no,
                                            NULL);
      return TRUE;
   end if;
   /* remove any existing ordloc_discount records and reset the unit cost */
   /* on ordloc to their original values.                                 */
   if DEAL_ORD_LIB_SQL.RESET_ORDER(O_error_message,
                                   L_item_cost_tbl,
                                   L_order_tbl_valid) = FALSE then
      return FALSE;
   end if;

   /* find all deals that are active for the order */
   if DEAL_ORD_LIB_SQL.GET_DEALS(O_error_message,
                                 L_deal_tbl) = FALSE then
      return FALSE;
   end if;

   /* create records on the ordloc_discount_build table for each */
   /* item/location on the order.                                */
   if DEAL_ORD_LIB_SQL.MAKE_ORDLOC_BUILD(O_error_message,
                                        I_override_manual_ind,
                                        L_discount_build_tbl) = FALSE then
      return FALSE;
   end if;

   /* get original order costs */
   if LP_order_status = 'A' then
      if DEAL_ORD_LIB_SQL.GET_OTB_COST(O_error_message,
                                       I_override_manual_ind,
                                       L_import_country_id,
                                       L_discount_build_tbl,
                                       L_Zero        /* 0 means to calculate otb_orig */
                                       ) = FALSE then
         return FALSE;
      end if;
   end if;

   /* For each of the deals found for the order, find all order items that */
   /* are affected, find the right threshold values, and insert into the   */
   /* ordloc_discount table. */

   L_return := DEAL_ORD_LIB_SQL.FIND_ITEMS_FOR_DEAL(O_error_message,
                                                    L_deal_tbl,
                                                    L_disc_item_tbl,
                                                    O_disc_item_tbl,--out
                                                    L_disc_item_tbl_cnt,
                                                    L_discount_build_tbl,
                                                    L_txn_discount_exists,
                                                    L_seq_no);
   if L_return < 0 then
      return FALSE;
   end if;

   /* update the ordloc table for off_invoice deal */
   if DEAL_ORD_LIB_SQL.CALCULATE_COST(O_error_message,
                                      O_disc_item_tbl,
                                      L_disc_item_tbl_cnt,
                                      L_order_tbl_valid(1).import_order_ind,
                                      L_discount_build_tbl) = FALSE then
      return FALSE;
   end if;

   if L_txn_discount_exists = 1 then
      L_return := DEAL_ORD_LIB_SQL.PROCESS_TXN_DISCOUNT(O_error_message,
                                                        L_deal_tbl,
                                                        L_discount_build_tbl,
                                                        L_seq_no);
      if L_return < 0 then
         return FALSE;
      end if;
   end if;

   if LP_order_status = 'A' then
      /* get final order cost */
      if DEAL_ORD_LIB_SQL.GET_OTB_COST(O_error_message,
                                       I_override_manual_ind,
                                       L_import_country_id,
                                       L_discount_build_tbl,
                                       L_one) = FALSE then
         return FALSE;
      end if;

      if DEAL_ORD_LIB_SQL.UPDATE_OTB(O_error_message,
                                     L_discount_build_tbl) = FALSE then
         return FALSE;
      end if;

      if DEAL_ORD_LIB_SQL.INSERT_REV_ORDERS(O_error_message,
                                            L_item_cost_tbl,
                                            L_discount_build_tbl) = FALSE then
         return FALSE;
      end if;
   end if;
   if DEAL_ORD_LIB_SQL.INSERT_L10N_DOC_DETAILS_GTT(O_error_message,
                                                  L_item_cost_tbl) = FALSE then
      return FALSE;
   end if;
   /* delete from ordloc_discount_temp where order_no = current order */
   if DEAL_ORD_LIB_SQL.DELETE_ORDLOC_DISC_BLD(O_error_message) = FALSE then
      return FALSE;
   end if;

   /* delete from order_deal_build where order_no = current order */
   if DEAL_ORD_LIB_SQL.DELETE_ORDER_DEAL_BUILD(O_error_message) = FALSE then
      return FALSE;
   end if;

   /* remove all records from ordloc_invc_cost where qty is 0 */
   if DEAL_ORD_LIB_SQL.CLEANUP_DISCOUNT_TABLES(O_error_message,
                                               L_order_tbl_valid) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
when OTHERS then
    O_error_message :=       SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                to_char(SQLCODE));
   return FALSE;
END EXTERNAL_PROCESS;
---------------------------------------------------------------------------------------------------------------
-- Function: LIB_INIT
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION LIB_INIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(40)  := 'DEAL_ORD_LIB_SQL.LIB_INIT';
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE;
BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;

   LP_deal_type_priority   := L_system_options_rec.deal_type_priority;
   LP_deal_age_priority    := L_system_options_rec.deal_age_priority;
   LP_elc_ind              := L_system_options_rec.elc_ind;
   LP_currency_code        := L_system_options_rec.currency_code;

   if LP_deal_age_priority = 'O' and LP_deal_type_priority= 'A' then
      LP_early_late_annual_prom := 1;
   elsif LP_deal_age_priority='O' and LP_deal_type_priority='P' then
      LP_early_late_prom_annual := 1;
   elsif LP_deal_age_priority='N' and LP_deal_type_priority='A' then
      LP_late_early_annual_prom := 1;
   elsif LP_deal_age_priority='N' and LP_deal_type_priority='P' then
      LP_late_early_prom_annual := 1;
   end if;

   LP_deal_ct                  :=0;
   LP_deal_size                :=0;
   LP_disct_bld_ct             :=0;
   LP_org_level_given_flag     :=0;
   LP_buyer_pack_detected      :=0;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LIB_INIT;
---------------------------------------------------------------------------------------------------------------
-- Function: CHECK_LOCK_AND_VALIDATE
-- Purpose : leave the orders to be unprocessed which is locked by an online user.
---------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LOCK_AND_VALIDATE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_online_ind        IN       NUMBER,
                                 I_order_rec         IN OUT   ORDER_TBL,
                                 I_order_rec_valid   IN OUT   ORDER_TBL,
                                 I_rec_to_process    IN OUT   NUMBER)
RETURN BOOLEAN IS

   L_program         VARCHAR2(40)  := 'DEAL_ORD_LIB_SQL.CHECK_LOCK_AND_VALIDATE';
   L_invalid_order   NUMBER(1);
   L_valid           NUMBER(1) := 0;
   L_order_no        ORDHEAD.ORDER_NO%TYPE;

   cursor c_validate_order is
     SELECT /*+ FIRST_ROWS */ 1
       FROM ordhead oh,
            ordloc  ol
      WHERE oh.order_no = L_order_no
        AND oh.order_no = ol.order_no
        AND (oh.contract_no IS NOT NULL
             OR NVL(ol.qty_received, 0) != 0)
        AND rownum = 1;
BEGIN
   for i in 1..I_rec_to_process loop
      L_invalid_order := 0;
      L_order_no := I_order_rec(i).order_no;

      open C_VALIDATE_ORDER;
      fetch C_VALIDATE_ORDER into L_invalid_order;
      close C_VALIDATE_ORDER;

      if L_invalid_order = 1 then
         DELETE
           FROM deal_calc_queue
          WHERE order_no = I_order_rec(i).order_no;
      end if;

      if I_online_ind != 1 then
         if ORDER_SETUP_SQL.LOCK_ORDHEAD(O_error_message,
                                         I_order_rec(i).order_no) = TRUE then
            L_valid := 1;
         else
            L_valid := 0;
         end if;
      else
         L_valid := 1;
      end if;

      if L_valid =1 and L_invalid_order !=1 then
         I_order_rec_valid(i) := I_order_rec(i);
      end if;

      I_rec_to_process  := i;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_LOCK_AND_VALIDATE;
---------------------------------------------------------------------------------------------------------------
-- Function: RESET_ORDER
-- Purpose : remove any existing ordloc_discount records and reset the unit cost
--           on ordloc to their original values.
---------------------------------------------------------------------------------------------------------------
FUNCTION RESET_ORDER(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_item_cost_tbl     IN OUT   ITEM_COST_TBL,
                     I_order_rec_valid   IN OUT   ORDER_TBL)
RETURN BOOLEAN IS

   L_program    VARCHAR2(40)  := 'DEAL_ORD_LIB_SQL.RESET_ORDER';
   L_order_no   ORDHEAD.ORDER_NO%TYPE;
   cursor c_last_calc_date is
      SELECT /*+ FIRST_ROWS */
             NVL(MAX(last_calc_date), NULL)
        FROM ordloc_discount
       WHERE order_no = L_order_no
         AND rownum = 1;
BEGIN
   FOR i IN I_order_rec_valid.FIRST..I_order_rec_valid.LAST LOOP
      L_order_no := I_order_rec_valid(i).order_no;
      if I_order_rec_valid(i).status !='A' OR (I_order_rec_valid(i).status ='A' and I_order_rec_valid(i).recalc_all_ind ='Y') Then
         /* order isn't approved, call update_supplier_cost() and ORDER_BRACKET_CALC_SQL.APPLY_BRACKETS() */
         if DEAL_ORD_LIB_SQL.UPDATE_SUPPLIER_COST(O_error_message,
                                                  1,
                                                  I_item_cost_tbl,
                                                  I_order_rec_valid(i).order_no,
                                                  I_order_rec_valid(i).supplier,
                                                  I_order_rec_valid(i).override_manual_ind)= FALSE then
            return FALSE;
         end if;
         if I_order_rec_valid(i).bracket_costing_ind ='Y' then
            if ORDER_BRACKET_CALC_SQL.APPLY_BRACKETS(O_error_message,
                                                     I_order_rec_valid(i).order_no,
                                                     I_order_rec_valid(i).override_manual_ind) = FALSE then
                return FALSE;
            end if;
         end if;

         if I_order_rec_valid(i).order_appr_ind ='Y' OR I_order_rec_valid(i).recalc_all_ind = 'Y' then
            UPDATE ordloc
               SET unit_cost_init    = unit_cost
             WHERE order_no          = I_order_rec_valid(i).order_no;
         end if;
      else
         /* at this point order is approved, order_appr_ind is N, and recalc_all_ind is N */
         /* simply copy over unit_cost_init into unit_cost */

         /* below call is made to populate the item cost array but not reset the ordloc costs */
         if DEAL_ORD_LIB_SQL.UPDATE_SUPPLIER_COST(O_error_message,
                                                  0,
                                                  I_item_cost_tbl,
                                                  I_order_rec_valid(i).order_no,
                                                  I_order_rec_valid(i).supplier,
                                                  I_order_rec_valid(i).override_manual_ind)= FALSE then
            return FALSE;
         end if;

         UPDATE ordloc
            SET unit_cost    = NVL(unit_cost_init,unit_cost),
                unit_cost_init = decode(unit_cost_init,null,unit_cost,unit_cost_init),
                cost_source  = 'NORM'
          WHERE order_no     = I_order_rec_valid(i).order_no
            AND (cost_source != 'MANL'
             OR (cost_source = 'MANL'
                AND I_order_rec_valid(i).override_manual_ind = 'Y'));

         /* find the max last_calc_date from ordloc_discount for current order.   */
         /* it will be used for get deals for orders which are in 'A'status */
         open C_LAST_CALC_DATE;
         fetch C_LAST_CALC_DATE into LP_last_calc_date;
         close C_LAST_CALC_DATE;
      end if;

      DELETE FROM ordloc_discount
       WHERE order_no = I_order_rec_valid(i).order_no;

      DELETE FROM ordhead_discount
       WHERE order_no = I_order_rec_valid(i).order_no;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RESET_ORDER;
---------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_SUPPLIER_COST
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_SUPPLIER_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_do_update             IN       NUMBER,
                              I_item_cost_tbl         IN OUT   ITEM_COST_TBL,
                              I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
                              I_supplier              IN       ORDHEAD.SUPPLIER%TYPE,
                              I_override_manual_ind   IN       DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(40)  := 'DEAL_ORD_LIB_SQL.UPDATE_SUPPLIER_COST';
   L_wf_order_no           ORDHEAD.WF_ORDER_NO%TYPE;
   L_item_count            NUMBER(20) := 0;
   L_unit_cost_supp_conv   ORDLOC.UNIT_COST%TYPE :=0;

   cursor c_get_wf_order is
      SELECT NVL(wf_order_no,'-1')
        FROM ordhead
       WHERE order_no = I_order_no;

   cursor c_item_loc_count is
      SELECT count(*)
        FROM ordloc
       WHERE order_no      = I_order_no
         AND (cost_source != 'MANL'
              OR (cost_source  = 'MANL'
                  AND I_override_manual_ind = 'Y'));

   cursor c_item_loc is
      SELECT ol.item,
             DECODE(NVL(wh.physical_wh, -999), NVL(wh.wh, -999), ol.location, wh.physical_wh) location,
             ol.location virtual_loc,
             i.unit_cost,
             ol.unit_cost old_unit_cost,
             I_order_no order_no,
             LP_num_cost_decimals num_cost_decimals
        FROM ordsku os,
             ordloc ol,
             ordhead oh,
             item_supp_country_loc i,
             wh
       WHERE os.order_no           = I_order_no
         AND (ol.cost_source      != 'MANL'
              OR (ol.cost_source   = 'MANL'
                  AND I_override_manual_ind = 'Y'))
         AND os.item               = i.item
         AND os.origin_country_id  = i.origin_country_id
         AND os.order_no           = ol.order_no
         AND os.item               = ol.item
         AND os.order_no           = oh.order_no
         AND ol.location           = i.loc
         AND ol.location           = wh.wh(+)
         AND i.supplier            = I_supplier
      ORDER BY 1, 2, 3;

   cursor c_franchise_item_loc is
      SELECT ol.item,
             DECODE(NVL(wh.physical_wh, -999), NVL(wh.wh, -999), ol.location, wh.physical_wh) location,
             ol.location virtual_loc,
             i.unit_cost,
             ol.unit_cost old_unit_cost,
             I_order_no order_no,
             LP_num_cost_decimals num_cost_decimals
        FROM ordsku os,
             ordloc ol,
             ordhead oh,
             item_supp_country_loc i,
             item_loc il,
             wh
       WHERE os.order_no           = I_order_no
         AND (ol.cost_source      != 'MANL'
              OR (ol.cost_source   = 'MANL'
                  AND I_override_manual_ind = 'Y'))
         AND os.item               = i.item
         AND os.item               = il.item
         AND os.origin_country_id  = i.origin_country_id
         AND os.order_no           = ol.order_no
         AND os.item               = ol.item
         AND os.order_no           = oh.order_no
         AND ol.location           = il.loc
         AND i.loc                 = il.costing_loc
         AND ol.location           = wh.wh(+)
         AND i.supplier            = I_supplier
      ORDER BY 1, 2, 3;

        /* It is crucial that the above cursor is ordered by item/physical loc/virtual loc
        since the ordloc_discount_build cursor is ordered the same way and the resulting
        data structures will then be comparable. (Necessary for insert_rev_orders().) */

BEGIN
   if I_order_no is not null and I_order_no != 0 then
      open c_item_loc_count;
      fetch c_item_loc_count into L_item_count;
      close c_item_loc_count;

      if L_item_count = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ORDER_ITEM',
                                               I_order_no,
                                               NULL,
                                               NULL);
         return TRUE;
      end if;
   end if;

   open c_get_wf_order;
   fetch c_get_wf_order into L_wf_order_no;
   close c_get_wf_order;

   if L_wf_order_no = '-1' then
      open C_ITEM_LOC;
      fetch C_ITEM_LOC BULK COLLECT into I_item_cost_tbl;
      close C_ITEM_LOC;
   else
      open C_FRANCHISE_ITEM_LOC;
      fetch C_FRANCHISE_ITEM_LOC BULK COLLECT into I_item_cost_tbl;
      close C_FRANCHISE_ITEM_LOC;
   end if;

   if I_do_update =1 then
      for i in I_item_cost_tbl.first..I_item_cost_tbl.last loop
          /* when supplier's currency code is different than order's,  */
         /* convert the unit_cost from supplier's currency to order's */
         if LP_order_currency_code != LP_supp_currency_code then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    I_item_cost_tbl(i).unit_cost,
                                    LP_supp_currency_code,
                                    LP_order_currency_code,
                                    L_unit_cost_supp_conv,
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL,
                                    LP_exchange_rate) = FALSE then
               return FALSE;
            end if;
            I_item_cost_tbl(i).unit_cost := L_unit_cost_supp_conv;
         end if;

            UPDATE ordloc
               SET unit_cost      = ROUND(I_item_cost_tbl(i).unit_cost, LP_num_cost_decimals),
                   cost_source    = 'NORM'
             WHERE item           = I_item_cost_tbl(i).item
               AND location       = I_item_cost_tbl(i).virtual_loc
               AND order_no       = I_item_cost_tbl(i).order_no;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_SUPPLIER_COST;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_DEALS
-- Purpose : find all deals that are active for the order
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEALS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_deal_tbl              IN OUT   DEAL_TBL)
RETURN BOOLEAN IS

   L_program                VARCHAR2(40)  := 'DEAL_ORD_LIB_SQL.GET_DEALS';
   L_deal_temp_tbl          deal_tbl;
   L_prev_po_deal_id        DEAL_DETAIL.DEAL_ID%TYPE := 0;
   L_exclusive_po_found     NUMBER(1) :=0;
   L_org_level_given_flag   NUMBER(1) :=0;
   L_deal_id                DEAL_DETAIL.DEAL_ID%TYPE   ;

   cursor C_DEAL_EXCLUSIVE_IND is
      SELECT /*+ FIRST_ROWS */ 'Y'
        FROM deal_detail
       WHERE deal_id = L_deal_id
         AND deal_class = 'EX'
         AND rownum = 1;

   cursor C_DEAL_EARLY_LATE_ANNUAL_PROM  is
      SELECT deal_id,
             deal_detail_id,
             numerictype type,
             currency_code,
             application_order,
             billing_type,
             deal_appl_timing,
             deal_class,
             threshold_limit_type,
             threshold_limit_uom,
             threshold_value_type,
             qty_thresh_buy_item,
             qty_thresh_buy_qty,
             qty_thresh_recur_ind,
             qty_thresh_buy_target,
             qty_thresh_get_item,
             qty_thresh_get_qty,
             qty_thresh_free_item_unit_cost,
             rebate_ind,
             rebate_purch_sales_ind,
             crdate create_date,
             get_free_discount,
             qty_thresh_get_type,
             qty_thresh_get_value,
             tran_discount_ind txn_discount_ind,
             'N' po_exclusive_ind
        FROM (SELECT DISTINCT dd.deal_id,
                     dd.deal_detail_id,
                     DECODE(dh.type, 'O', 3, 'A', 1, 'P',2) numerictype,
                     dh.currency_code,
                     dd.application_order,
                     dh.billing_type,
                     dh.deal_appl_timing,
                     dd.deal_class,
                     dh.threshold_limit_type,
                     dh.threshold_limit_uom,
                     dd.threshold_value_type,
                     dd.qty_thresh_buy_item,
                     dd.qty_thresh_buy_qty,
                     dd.qty_thresh_recur_ind,
                     dd.qty_thresh_buy_target,
                     dd.qty_thresh_get_item,
                     dd.qty_thresh_get_qty,
                     dd.qty_thresh_free_item_unit_cost,
                     dh.rebate_ind,
                     dh.rebate_purch_sales_ind,
                     dh.active_date crdate,
                     dd.qty_thresh_get_type,
                     dd.qty_thresh_get_value,
                     dd.get_free_discount,
                     dd.tran_discount_ind
                FROM deal_head dh,
                     deal_detail dd,
                     sups s
               WHERE dh.deal_id = dd.deal_id
                 AND dh.status = 'A'
                 AND dh.type != 'M'
                 AND s.supplier = LP_order_supplier
                 AND dh.supplier = NVL(s.supplier_parent, s.supplier)
                 AND trunc(dh.active_date) <= LP_order_not_before_date
                 AND (dh.close_date is NULL
                       OR trunc(dh.close_date) >= LP_order_not_before_date)
                 AND (dh.order_no is NULL
                      OR dh.order_no = LP_order_no)
                 AND (LP_order_status != 'A'
                      OR (LP_order_status = 'A'
                          AND (LP_order_appr_ind = 'Y'
                               OR (dh.recalc_approved_orders = 'Y'
                                   OR (LP_last_calc_date != 'NULL'
                                       AND dh.approval_date <= LP_last_calc_date)))))
               UNION ALL
              SELECT DISTINCT dd.deal_id,
                     dd.deal_detail_id,
                     DECODE(dh.type, 'O', 3, 'A', 1, 'P',2) numerictype,
                     dh.currency_code,
                     dd.application_order,
                     dh.billing_type,
                     dh.deal_appl_timing,
                     dd.deal_class,
                     dh.threshold_limit_type,
                     dh.threshold_limit_uom,
                     dd.threshold_value_type,
                     dd.qty_thresh_buy_item,
                     dd.qty_thresh_buy_qty,
                     dd.qty_thresh_recur_ind,
                     dd.qty_thresh_buy_target,
                     dd.qty_thresh_get_item,
                     dd.qty_thresh_get_qty,
                     dd.qty_thresh_free_item_unit_cost,
                     dh.rebate_ind,
                     dh.rebate_purch_sales_ind,
                     dh.active_date crdate,
                     dd.qty_thresh_get_type,
                     dd.qty_thresh_get_value,
                     dd.get_free_discount,
                     dd.tran_discount_ind
                FROM deal_head dh,
                     deal_detail dd,
                     item_supp_country isc
               WHERE dh.deal_id = dd.deal_id
                 AND dh.status = 'A'
                 AND dh.type != 'M'
                 AND (dh.supplier is NULL
                      AND isc.supplier = LP_order_supplier)
                 AND ((dh.partner_type = isc.supp_hier_type_1
                        AND dh.partner_id = isc.supp_hier_lvl_1)
                         OR (dh.partner_type = isc.supp_hier_type_2
                               AND dh.partner_id = isc.supp_hier_lvl_2)
                         OR (dh.partner_type = isc.supp_hier_type_3
                               AND dh.partner_id = isc.supp_hier_lvl_3))
                 AND trunc(dh.active_date) <= LP_order_not_before_date
                 AND (dh.close_date is NULL
                       OR trunc(dh.close_date) >= LP_order_not_before_date)
                 AND (dh.order_no is NULL
                      OR dh.order_no = LP_order_no)
                 AND (LP_order_status != 'A'
                      OR (LP_order_status = 'A'
                          AND (LP_order_appr_ind = 'Y'
                               OR (dh.recalc_approved_orders = 'Y'
                                   OR (LP_last_calc_date != 'NULL'
                                       AND dh.approval_date <= LP_last_calc_date))))))
            ORDER BY tran_discount_ind,
                     CASE
                        WHEN billing_type = 'OI'                      THEN 1
                        WHEN billing_type = 'BB' AND rebate_ind = 'N' THEN 2
                        WHEN billing_type = 'BB' AND rebate_ind = 'Y' THEN 3
                        WHEN billing_type = 'BBR'                     THEN 3
                        ELSE                                               4
                     END,
                     numerictype,
                     create_date,
                     deal_id,
                     application_order;

   cursor C_DEAL_EARLY_LATE_PROM_ANNUAL is
      SELECT deal_id,
             deal_detail_id,
             numerictype type,
             currency_code,
             application_order,
             billing_type,
             deal_appl_timing,
             deal_class,
             threshold_limit_type,
             threshold_limit_uom,
             threshold_value_type,
             qty_thresh_buy_item,
             qty_thresh_buy_qty,
             qty_thresh_recur_ind,
             qty_thresh_buy_target,
             qty_thresh_get_item,
             qty_thresh_get_qty,
             qty_thresh_free_item_unit_cost,
             rebate_ind,
             rebate_purch_sales_ind,
             crdate create_date,
             get_free_discount,
             qty_thresh_get_type,
             qty_thresh_get_value,
             tran_discount_ind txn_discount_ind,
             'N' po_exclusive_ind
        FROM (SELECT DISTINCT dd.deal_id,
                     dd.deal_detail_id,
                     DECODE(dh.type, 'O', 3, 'A', 1, 'P',2) numerictype,
                     dh.currency_code,
                     dd.application_order,
                     dh.billing_type,
                     dh.deal_appl_timing,
                     dd.deal_class,
                     dh.threshold_limit_type,
                     dh.threshold_limit_uom,
                     dd.threshold_value_type,
                     dd.qty_thresh_buy_item,
                     dd.qty_thresh_buy_qty,
                     dd.qty_thresh_recur_ind,
                     dd.qty_thresh_buy_target,
                     dd.qty_thresh_get_item,
                     dd.qty_thresh_get_qty,
                     dd.qty_thresh_free_item_unit_cost,
                     dh.rebate_ind,
                     dh.rebate_purch_sales_ind,
                     dh.active_date crdate,
                     dd.qty_thresh_get_type,
                     dd.qty_thresh_get_value,
                     dd.get_free_discount,
                     dd.tran_discount_ind
                FROM deal_head dh,
                     deal_detail dd,
                     sups s
               WHERE dh.deal_id = dd.deal_id
                 AND dh.status = 'A'
                 AND dh.type != 'M'
                 AND s.supplier = LP_order_supplier
                 AND dh.supplier = NVL(s.supplier_parent, s.supplier)
                 AND trunc(dh.active_date) <= LP_order_not_before_date
                 AND (dh.close_date is NULL
                       OR trunc(dh.close_date) >= LP_order_not_before_date)
                 AND (dh.order_no is NULL
                      OR dh.order_no = LP_order_no)
                 AND (LP_order_status != 'A'
                      OR (LP_order_status = 'A'
                          AND (LP_order_appr_ind = 'Y'
                               OR (dh.recalc_approved_orders = 'Y'
                                   OR (LP_last_calc_date != 'NULL'
                                       AND dh.approval_date <= LP_last_calc_date)))))
               UNION ALL
              SELECT DISTINCT dd.deal_id,
                     dd.deal_detail_id,
                     DECODE(dh.type, 'O', 3, 'A', 1, 'P',2) numerictype,
                     dh.currency_code,
                     dd.application_order,
                     dh.billing_type,
                     dh.deal_appl_timing,
                     dd.deal_class,
                     dh.threshold_limit_type,
                     dh.threshold_limit_uom,
                     dd.threshold_value_type,
                     dd.qty_thresh_buy_item,
                     dd.qty_thresh_buy_qty,
                     dd.qty_thresh_recur_ind,
                     dd.qty_thresh_buy_target,
                     dd.qty_thresh_get_item,
                     dd.qty_thresh_get_qty,
                     dd.qty_thresh_free_item_unit_cost,
                     dh.rebate_ind,
                     dh.rebate_purch_sales_ind,
                     dh.active_date crdate,
                     dd.qty_thresh_get_type,
                     dd.qty_thresh_get_value,
                     dd.get_free_discount,
                     dd.tran_discount_ind
                FROM deal_head dh,
                     deal_detail dd,
                     item_supp_country isc
               WHERE dh.deal_id = dd.deal_id
                 AND dh.status = 'A'
                 AND dh.type != 'M'
                 AND (dh.supplier is NULL
                      AND isc.supplier = LP_order_supplier)
                 AND ((dh.partner_type = isc.supp_hier_type_1
                        AND dh.partner_id = isc.supp_hier_lvl_1)
                         OR (dh.partner_type = isc.supp_hier_type_2
                               AND dh.partner_id = isc.supp_hier_lvl_2)
                         OR (dh.partner_type = isc.supp_hier_type_3
                               AND dh.partner_id = isc.supp_hier_lvl_3))
                 AND trunc(dh.active_date) <= LP_order_not_before_date
                 AND (dh.close_date is NULL
                       OR trunc(dh.close_date) >= LP_order_not_before_date)
                 AND (dh.order_no is NULL
                      OR dh.order_no = LP_order_no)
                 AND (LP_order_status != 'A'
                      OR (LP_order_status = 'A'
                          AND (LP_order_appr_ind = 'Y'
                               OR (dh.recalc_approved_orders = 'Y'
                                   OR (LP_last_calc_date != 'NULL'
                                       AND dh.approval_date <= LP_last_calc_date))))))
            ORDER BY tran_discount_ind,
                     CASE
                        WHEN billing_type = 'OI'                      THEN 1
                        WHEN billing_type = 'BB' AND rebate_ind = 'N' THEN 2
                        WHEN billing_type = 'BB' AND rebate_ind = 'Y' THEN 3
                        WHEN billing_type = 'BBR'                     THEN 3
                        ELSE                                               4
                     END,
                     numerictype,
                     create_date,
                     deal_id,
                     application_order;

   cursor C_DEAL_LATE_EARLY_ANNUAL_PROM is
      SELECT deal_id,
             deal_detail_id,
             numerictype type,
             currency_code,
             application_order,
             billing_type,
             deal_appl_timing,
             deal_class,
             threshold_limit_type,
             threshold_limit_uom,
             threshold_value_type,
             qty_thresh_buy_item,
             qty_thresh_buy_qty,
             qty_thresh_recur_ind,
             qty_thresh_buy_target,
             qty_thresh_get_item,
             qty_thresh_get_qty,
             qty_thresh_free_item_unit_cost,
             rebate_ind,
             rebate_purch_sales_ind,
             crdate create_date,
             get_free_discount,
             qty_thresh_get_type,
             qty_thresh_get_value,
             tran_discount_ind txn_discount_ind,
             'N' po_exclusive_ind
        FROM (SELECT DISTINCT dd.deal_id,
                     dd.deal_detail_id,
                     DECODE(dh.type, 'O', 3, 'A', 1, 'P',3) numerictype,
                     dh.currency_code,
                     dd.application_order,
                     dh.billing_type,
                     dh.deal_appl_timing,
                     dd.deal_class,
                     dh.threshold_limit_type,
                     dh.threshold_limit_uom,
                     dd.threshold_value_type,
                     dd.qty_thresh_buy_item,
                     dd.qty_thresh_buy_qty,
                     dd.qty_thresh_recur_ind,
                     dd.qty_thresh_buy_target,
                     dd.qty_thresh_get_item,
                     dd.qty_thresh_get_qty,
                     dd.qty_thresh_free_item_unit_cost,
                     dh.rebate_ind,
                     dh.rebate_purch_sales_ind,
                     dh.active_date crdate,
                     dd.qty_thresh_get_type,
                     dd.qty_thresh_get_value,
                     dd.get_free_discount,
                     dd.tran_discount_ind
                FROM deal_head dh,
                     deal_detail dd,
                     sups s
               WHERE dh.deal_id = dd.deal_id
                 AND dh.status = 'A'
                 AND dh.type != 'M'
                 AND s.supplier = LP_order_supplier
                 AND dh.supplier = NVL(s.supplier_parent, s.supplier)
                 AND trunc(dh.active_date) <= LP_order_not_before_date
                 AND (dh.close_date is NULL
                       OR trunc(dh.close_date) >= LP_order_not_before_date)
                 AND (dh.order_no is NULL
                      OR dh.order_no = LP_order_no)
                 AND (LP_order_status != 'A'
                      OR (LP_order_status = 'A'
                          AND (LP_order_appr_ind = 'Y'
                               OR (dh.recalc_approved_orders = 'Y'
                                   OR (LP_last_calc_date != 'NULL'
                                       AND dh.approval_date <= LP_last_calc_date)))))
               UNION ALL
              SELECT DISTINCT dd.deal_id,
                     dd.deal_detail_id,
                     DECODE(dh.type, 'O', 3, 'A', 1, 'P',3) numerictype,
                     dh.currency_code,
                     dd.application_order,
                     dh.billing_type,
                     dh.deal_appl_timing,
                     dd.deal_class,
                     dh.threshold_limit_type,
                     dh.threshold_limit_uom,
                     dd.threshold_value_type,
                     dd.qty_thresh_buy_item,
                     dd.qty_thresh_buy_qty,
                     dd.qty_thresh_recur_ind,
                     dd.qty_thresh_buy_target,
                     dd.qty_thresh_get_item,
                     dd.qty_thresh_get_qty,
                     dd.qty_thresh_free_item_unit_cost,
                     dh.rebate_ind,
                     dh.rebate_purch_sales_ind,
                     dh.active_date crdate,
                     dd.qty_thresh_get_type,
                     dd.qty_thresh_get_value,
                     dd.get_free_discount,
                     dd.tran_discount_ind
                FROM deal_head dh,
                     deal_detail dd,
                     item_supp_country isc
               WHERE dh.deal_id = dd.deal_id
                 AND dh.status = 'A'
                 AND dh.type != 'M'
                 AND (dh.supplier is NULL
                      AND isc.supplier = LP_order_supplier)
                 AND ((dh.partner_type = isc.supp_hier_type_1
                        AND dh.partner_id = isc.supp_hier_lvl_1)
                         OR (dh.partner_type = isc.supp_hier_type_2
                               AND dh.partner_id = isc.supp_hier_lvl_2)
                         OR (dh.partner_type = isc.supp_hier_type_3
                               AND dh.partner_id = isc.supp_hier_lvl_3))
                 AND trunc(dh.active_date) <= LP_order_not_before_date
                 AND (dh.close_date is NULL
                       OR trunc(dh.close_date) >= LP_order_not_before_date)
                 AND (dh.order_no is NULL
                      OR dh.order_no = LP_order_no)
                 AND (LP_order_status != 'A'
                      OR (LP_order_status = 'A'
                          AND (LP_order_appr_ind = 'Y'
                               OR (dh.recalc_approved_orders = 'Y'
                                   OR (LP_last_calc_date != 'NULL'
                                       AND dh.approval_date <= LP_last_calc_date))))))
            ORDER BY tran_discount_ind,
                     CASE
                        WHEN billing_type = 'OI'                      THEN 1
                        WHEN billing_type = 'BB' AND rebate_ind = 'N' THEN 2
                        WHEN billing_type = 'BB' AND rebate_ind = 'Y' THEN 3
                        WHEN billing_type = 'BBR'                     THEN 3
                        ELSE                                               4
                     END,
                     numerictype,
                     create_date desc,
                     deal_id,
                     application_order;

   cursor C_DEAL_LATE_EARLY_PROM_ANNUAL is
      SELECT deal_id,
             deal_detail_id,
             numerictype type,
             currency_code,
             application_order,
             billing_type,
             deal_appl_timing,
             deal_class,
             threshold_limit_type,
             threshold_limit_uom,
             threshold_value_type,
             qty_thresh_buy_item,
             qty_thresh_buy_qty,
             qty_thresh_recur_ind,
             qty_thresh_buy_target,
             qty_thresh_get_item,
             qty_thresh_get_qty,
             qty_thresh_free_item_unit_cost,
             rebate_ind,
             rebate_purch_sales_ind,
             crdate create_date,
             get_free_discount,
             qty_thresh_get_type,
             qty_thresh_get_value,
             tran_discount_ind txn_discount_ind,
             'N' po_exclusive_ind
        FROM (SELECT DISTINCT dd.deal_id,
                     dd.deal_detail_id,
                     DECODE(dh.type, 'O', 3, 'A', 1, 'P',2) numerictype,
                     dh.currency_code,
                     dd.application_order,
                     dh.billing_type,
                     dh.deal_appl_timing,
                     dd.deal_class,
                     dh.threshold_limit_type,
                     dh.threshold_limit_uom,
                     dd.threshold_value_type,
                     dd.qty_thresh_buy_item,
                     dd.qty_thresh_buy_qty,
                     dd.qty_thresh_recur_ind,
                     dd.qty_thresh_buy_target,
                     dd.qty_thresh_get_item,
                     dd.qty_thresh_get_qty,
                     dd.qty_thresh_free_item_unit_cost,
                     dh.rebate_ind,
                     dh.rebate_purch_sales_ind,
                     dh.active_date crdate,
                     dd.qty_thresh_get_type,
                     dd.qty_thresh_get_value,
                     dd.get_free_discount,
                     dd.tran_discount_ind
                FROM deal_head dh,
                     deal_detail dd,
                     sups s
               WHERE dh.deal_id = dd.deal_id
                 AND dh.status = 'A'
                 AND dh.type != 'M'
                 AND s.supplier = LP_order_supplier
                 AND dh.supplier = NVL(s.supplier_parent, s.supplier)
                 AND trunc(dh.active_date) <= LP_order_not_before_date
                 AND (dh.close_date is NULL
                       OR trunc(dh.close_date) >= LP_order_not_before_date)
                 AND (dh.order_no is NULL
                      OR dh.order_no = LP_order_no)
                 AND (LP_order_status != 'A'
                      OR (LP_order_status = 'A'
                          AND (LP_order_appr_ind = 'Y'
                               OR (dh.recalc_approved_orders = 'Y'
                                   OR (LP_last_calc_date != 'NULL'
                                       AND dh.approval_date <= LP_last_calc_date)))))
               UNION ALL
              SELECT DISTINCT dd.deal_id,
                     dd.deal_detail_id,
                     DECODE(dh.type, 'O', 3, 'A', 1, 'P',2) numerictype,
                     dh.currency_code,
                     dd.application_order,
                     dh.billing_type,
                     dh.deal_appl_timing,
                     dd.deal_class,
                     dh.threshold_limit_type,
                     dh.threshold_limit_uom,
                     dd.threshold_value_type,
                     dd.qty_thresh_buy_item,
                     dd.qty_thresh_buy_qty,
                     dd.qty_thresh_recur_ind,
                     dd.qty_thresh_buy_target,
                     dd.qty_thresh_get_item,
                     dd.qty_thresh_get_qty,
                     dd.qty_thresh_free_item_unit_cost,
                     dh.rebate_ind,
                     dh.rebate_purch_sales_ind,
                     dh.active_date crdate,
                     dd.qty_thresh_get_type,
                     dd.qty_thresh_get_value,
                     dd.get_free_discount,
                     dd.tran_discount_ind
                FROM deal_head dh,
                     deal_detail dd,
                     item_supp_country isc
               WHERE dh.deal_id = dd.deal_id
                 AND dh.status = 'A'
                 AND dh.type != 'M'
                 AND (dh.supplier is NULL
                      AND isc.supplier = LP_order_supplier)
                 AND ((dh.partner_type = isc.supp_hier_type_1
                        AND dh.partner_id = isc.supp_hier_lvl_1)
                         OR (dh.partner_type = isc.supp_hier_type_2
                               AND dh.partner_id = isc.supp_hier_lvl_2)
                         OR (dh.partner_type = isc.supp_hier_type_3
                               AND dh.partner_id = isc.supp_hier_lvl_3))
                 AND trunc(dh.active_date) <= LP_order_not_before_date
                 AND (dh.close_date is NULL
                       OR trunc(dh.close_date) >= LP_order_not_before_date)
                 AND (dh.order_no is NULL
                      OR dh.order_no = LP_order_no)
                 AND (LP_order_status != 'A'
                      OR (LP_order_status = 'A'
                          AND (LP_order_appr_ind = 'Y'
                               OR (dh.recalc_approved_orders = 'Y'
                                   OR (LP_last_calc_date != 'NULL'
                                       AND dh.approval_date <= LP_last_calc_date))))))
            ORDER BY tran_discount_ind,
                     case
                        WHEN billing_type = 'OI'                      THEN 1
                        WHEN billing_type = 'BB' AND rebate_ind = 'N' THEN 2
                        WHEN billing_type = 'BB' AND rebate_ind = 'Y' THEN 3
                        WHEN billing_type = 'BBR'                     THEN 3
                        ELSE                                               4
                     END,
                     numerictype,
                     create_date desc,
                     deal_id,
                     application_order;

BEGIN
   if LP_early_late_annual_prom =1 then
      open C_DEAL_EARLY_LATE_ANNUAL_PROM;
      fetch C_DEAL_EARLY_LATE_ANNUAL_PROM BULK COLLECT into I_deal_tbl;
      close C_DEAL_EARLY_LATE_ANNUAL_PROM;
      if I_deal_tbl.count =0 then
         return TRUE;
      end if;
   elsif LP_EARLY_LATE_PROM_ANNUAL = 1 then
      open C_DEAL_EARLY_LATE_PROM_ANNUAL;
      fetch C_DEAL_EARLY_LATE_PROM_ANNUAL BULK COLLECT into I_deal_tbl;
      close C_DEAL_EARLY_LATE_PROM_ANNUAL;
      if I_deal_tbl.count =0 then
         return TRUE;
      end if;
   elsif LP_LATE_EARLY_ANNUAL_PROM = 1 then
      open C_DEAL_LATE_EARLY_ANNUAL_PROM;
      fetch C_DEAL_LATE_EARLY_ANNUAL_PROM BULK COLLECT into I_deal_tbl;
      close C_DEAL_LATE_EARLY_ANNUAL_PROM;
      if I_deal_tbl.count =0 then
         return TRUE;
      end if;
   elsif  LP_late_early_prom_annual = 1 then
      open C_DEAL_LATE_EARLY_PROM_ANNUAL;
      fetch C_DEAL_LATE_EARLY_PROM_ANNUAL BULK COLLECT into I_deal_tbl;
      close C_DEAL_LATE_EARLY_PROM_ANNUAL;
      if I_deal_tbl.count =0 then
         return TRUE;
      end if;
   end if;

   for i in I_deal_tbl.first..I_deal_tbl.last loop
   /* if a new deal id is type 3, check whether it is exclusive */
      if I_deal_tbl(i).type = 3 then
         if L_prev_po_deal_id != I_deal_tbl(i).deal_id then
            L_exclusive_po_found := 0;
            L_deal_id := I_deal_tbl(i).deal_id;
            open C_DEAL_EXCLUSIVE_IND;
            fetch C_DEAL_EXCLUSIVE_IND  into I_deal_tbl(i).po_exclusive_ind;
            close C_DEAL_EXCLUSIVE_IND;

            if I_deal_tbl(i).po_exclusive_ind is null or I_deal_tbl(i).po_exclusive_ind ='N' then
               I_deal_tbl(i).po_exclusive_ind := 'N';
            else
               /* Set exclusivity flag */
               L_exclusive_po_found := 1;
            end if;

            L_prev_po_deal_id := I_deal_tbl(i).deal_id;

         else /* same deal id */
            if L_exclusive_po_found = 1 then
               I_deal_tbl(i).po_exclusive_ind := 'Y';
            else
               I_deal_tbl(i).po_exclusive_ind := 'N';
            end if;
         end if;
      end if;

      /* check if order-specific deal was found. There could be only one   */
      /* order-specific deal exists if there's any.  And one deal can have */
      /* several deal_detail components.                                   */
      /* type = 3 is PO-specific, reordered to process PO specific deal last in discount calculation */
      if I_deal_tbl(i).type = 3 then
         if I_deal_tbl(i).po_exclusive_ind = 'Y' then
           L_deal_temp_tbl(i) := I_deal_tbl(i);
        end if;
      end if;
   end loop;

   /* if there is an order-specific deal, it will be the ONLY deal that  */
   /* should be processed for this order; the others should be ignored. */
   if L_deal_temp_tbl.count > 0 then
      I_deal_tbl.delete;

      for i in L_deal_temp_tbl.first..L_deal_temp_tbl.last loop
         I_deal_tbl(i) := L_deal_temp_tbl(i);
      end loop;

   end if;

   for i in I_deal_tbl.first..I_deal_tbl.last loop
      INSERT INTO order_deal_build
                      (deal_id,
                       deal_detail_id)
                VALUES(I_deal_tbl(i).deal_id,
                       I_deal_tbl(i).deal_detail_id);

      /* check if the deal contains any organizational hierarchy infomation */
      if DEAL_ORD_LIB_SQL.ORG_LEVEL_GIVEN(O_error_message,
                                          I_deal_tbl(i).deal_id,
                                          I_deal_tbl(i).deal_detail_id,
                                          L_org_level_given_flag) = FALSE then
         return FALSE;
      end if;

      if L_org_level_given_flag = 1 then
         LP_org_level_given_flag := 1;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_DEALS;
---------------------------------------------------------------------------------------------------------------
-- Function: ORG_LEVEL_GIVEN
-- Purpose : find if the deal contains any organizational hierarchy infomation
---------------------------------------------------------------------------------------------------------------
FUNCTION ORG_LEVEL_GIVEN(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_deal_id                IN       DEAL_DETAIL.DEAL_ID%TYPE,
                         I_deal_detail_id         IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                         O_org_level_given_flag   IN OUT   NUMBER)
RETURN BOOLEAN is

   L_program                VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.ORG_LEVEL_GIVEN';
   L_org_level_given_flag   NUMBER(1) := 0;

   cursor C_ORG_LEVEL_EXIST is
      SELECT /*+ FIRST_ROWS */ 1
        FROM deal_itemloc
       WHERE deal_id           = I_deal_id
         AND deal_detail_id    = I_deal_detail_id
         AND NVL(org_level, 0) NOT IN (0, 5)
         AND ROWNUM = 1;
BEGIN
   open C_ORG_LEVEL_EXIST;
   fetch C_ORG_LEVEL_EXIST into L_org_level_given_flag;
   close C_ORG_LEVEL_EXIST;

   O_org_level_given_flag := L_org_level_given_flag;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ORG_LEVEL_GIVEN;
---------------------------------------------------------------------------------------------------------------
-- Function: MAKE_ORDLOC_BUILD
-- Purpose : create records on the ordloc_discount_build table for each
--           item/location on the order.
---------------------------------------------------------------------------------------------------------------
FUNCTION MAKE_ORDLOC_BUILD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_override_manual_ind   IN       DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE,
                           I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL)
RETURN BOOLEAN is

   L_program                VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.MAKE_ORDLOC_BUILD';
   L_prev_phys_loc          ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE;
   L_prev_item              ITEM_MASTER.ITEM%TYPE;
   L_qty_ordered            ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;

   cursor C_CHECK_BUYER_PACK is
      SELECT /*+ FIRST_ROWS */ 1
        FROM ordloc ol,
             item_master im
       WHERE ol.order_no             = LP_order_no
         AND ol.item                 = im.item
         AND im.pack_type = 'B'
         AND rownum       = 1;

 /* In this fetch we must build up org and merch hier! */
   cursor C_ORDLOC_DISCOUNT_BUILD is
      /* Non buyer pack item in the order */
      SELECT LP_order_no order_no,
             os.item,
             NULL pack_no,                     /* Buyer pack_no */
             os.origin_country_id,
             g.division,
             d.group_no,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.item_grandparent,
             im.diff_1,
             im.diff_2,
             im.diff_3,
             im.diff_4,
             NULL chain,
             NULL area,
             NULL region,
             NULL district,
             DECODE(NVL(wh.physical_wh, -999), NVL(wh.wh, -999), ol.location, wh.physical_wh) location,
             ol.loc_type,
             ol.location virtual_loc,
             ol.qty_ordered,
             im.cost_zone_group_id,
             d.otb_calc_type,
             ol.unit_cost,
             NULL unit_cost_init,
             NULL orig_otb,
             NULL new_otb,
             NULL exclusive_deal_applied,
             NULL merch_level,
             NULL org_level,
             NULL costing_loc,
             NULL costing_loc_type,
             NULL franchise_ind
        FROM ordloc ol,
             ordsku os,
             item_master im,
             deps d,
             groups g,
             item_supp_country isc,
             wh
       WHERE os.order_no             = LP_order_no
         AND os.order_no             = ol.order_no
         AND ol.qty_ordered          > 0
         AND ol.location             = wh.wh(+)
         AND os.item                 = ol.item
         AND os.item                 = isc.item
         AND os.origin_country_id    = isc.origin_country_id
         AND isc.supplier            = LP_order_supplier
         AND (ol.cost_source        != 'MANL'
              OR (ol.cost_source     = 'MANL' AND I_override_manual_ind = 'Y'))
         AND im.dept                 = d.dept
         AND d.group_no              = g.group_no
         AND os.item                 = im.item
         AND (im.pack_type IS NULL
              OR im.pack_type = 'V') /* pack_type in ordloc can only be type: NULL or V */
    ORDER BY 7, 16, 18;
BEGIN
   LP_buyer_pack_detected := 0;

   open C_CHECK_BUYER_PACK;
   fetch C_CHECK_BUYER_PACK into LP_buyer_pack_detected;
   close C_CHECK_BUYER_PACK;

   open C_ORDLOC_DISCOUNT_BUILD;
   fetch C_ORDLOC_DISCOUNT_BUILD BULK COLLECT into I_discount_build_tbl;
   close C_ORDLOC_DISCOUNT_BUILD;

   if I_discount_build_tbl.count >0 then
      if LP_org_level_given_flag = 1 then
         if DEAL_ORD_LIB_SQL.GET_ORG_HIER(O_error_message,
                                          I_discount_build_tbl) = FALSE then
            return FALSE;
         end if;
      else
         if DEAL_ORD_LIB_SQL.GET_COSTING_LOC(O_error_message,
                                             I_discount_build_tbl) = FALSE then
            return FALSE;
         end if;
      end if;

      for i in I_discount_build_tbl.first..I_discount_build_tbl.last loop
         I_discount_build_tbl(i).exclusive_deal_applied := 0;

         if I_discount_build_tbl(i).loc_type = 'W'
            and (I_discount_build_tbl(i).location = L_prev_phys_loc
            and I_discount_build_tbl(i).item = L_prev_item ) then
               I_discount_build_tbl(i-1).qty_ordered := I_discount_build_tbl(i).qty_ordered + I_discount_build_tbl(i-1).qty_ordered;
               L_prev_phys_loc := I_discount_build_tbl(i).location;
               L_prev_item     := I_discount_build_tbl(i).item;
         end if;
      end loop;

      for i in I_discount_build_tbl.first..I_discount_build_tbl.last loop
         INSERT INTO ordloc_discount_build
                     (order_no,
                      item,
                      pack_no,
                      origin_country_id,
                      unit_cost,
                      division,
                      group_no,
                      dept,
                      class,
                      subclass,
                      item_parent,
                      item_grandparent,
                      diff_1,
                      diff_2,
                      diff_3,
                      diff_4,
                      chain,
                      area,
                      region,
                      district,
                      location,
                      loc_type,
                      qty_ordered)
               VALUES(I_discount_build_tbl(i).order_no,
                      I_discount_build_tbl(i).item,
                      I_discount_build_tbl(i).pack_no,
                      I_discount_build_tbl(i).origin_country_id,
                      I_discount_build_tbl(i).unit_cost,
                      I_discount_build_tbl(i).division,
                      I_discount_build_tbl(i).group_no,
                      I_discount_build_tbl(i).dept,
                      I_discount_build_tbl(i).class,
                      I_discount_build_tbl(i).subclass,
                      I_discount_build_tbl(i).item_parent,
                      I_discount_build_tbl(i).item_grandparent,
                      I_discount_build_tbl(i).diff_1,
                      I_discount_build_tbl(i).diff_2,
                      I_discount_build_tbl(i).diff_3,
                      I_discount_build_tbl(i).diff_4,
                      I_discount_build_tbl(i).chain,
                      I_discount_build_tbl(i).area,
                      I_discount_build_tbl(i).region,
                      I_discount_build_tbl(i).district,
                      decode(I_discount_build_tbl(i).franchise_ind,
                            'Y',I_discount_build_tbl(i).costing_loc,
                             I_discount_build_tbl(i).location),
                      decode(I_discount_build_tbl(i).franchise_ind,
                             'Y',I_discount_build_tbl(i).costing_loc_type,
                             I_discount_build_tbl(i).loc_type),
                      I_discount_build_tbl(i).qty_ordered);
      end loop;
   end if;

   return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END MAKE_ORDLOC_BUILD;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_ORG_HIER
-- Purpose : Check if any deals have some organization hierarchy info,
--           go to find the org hier info for each item/location.
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_ORG_HIER(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL)
RETURN BOOLEAN is

   L_program       VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.GET_ORG_HIER';
   L_location      ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE;
   L_virtual_loc   ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE;
   L_item          ITEM_MASTER.ITEM%TYPE;
   L_store_type    STORE.STORE_TYPE%TYPE;

   cursor C_ORG_HIER is
      SELECT a.chain,
             r.area,
             d.region,
             s.district
        FROM area a,
             region r,
             district d,
             store s
       WHERE s.store    = L_location
         AND s.district = d.district
         AND d.region   = r.region
         AND r.area     = a.area
      union all
      SELECT NULL chain,
             NULL area,
             NULL region,
             NULL district
        FROM wh wh
       WHERE wh.wh            = L_virtual_loc
         AND (org_hier_type = 1 or org_hier_type is NULL)
         AND stockholding_ind = 'Y'
        union all
      SELECT wh.org_hier_value chain,
             NULL area,
             NULL region,
             NULL district
        FROM wh wh
       WHERE wh.wh            = L_virtual_loc
         AND org_hier_type    = 10
         AND stockholding_ind = 'Y'
      union all
      SELECT ar.chain chain,
             wh.org_hier_value  area,
             NULL region,
             NULL district
        FROM wh wh,
             area ar
       WHERE wh.wh            = L_virtual_loc
         AND wh.org_hier_type    = 20
         AND wh.stockholding_ind = 'Y'
         AND wh.org_hier_value = ar.area
      union all
      SELECT ar.chain chain,
             re.area  area,
             wh.org_hier_value region,
             NULL district
             FROM wh wh,
                  area ar,
                  region re
            WHERE wh.wh            = L_virtual_loc
              AND wh.org_hier_type    = 30
              AND wh.stockholding_ind = 'Y'
              AND wh.org_hier_value = re.region
              AND re.area     = ar.area
      union all
      SELECT ar.chain chain,
             re.area  area,
             di.region region,
             wh.org_hier_value district
             FROM wh wh,
                  area ar,
                  region re,
                  district di
            WHERE wh.wh            = L_virtual_loc
              AND wh.org_hier_type    = 40
              AND wh.stockholding_ind = 'Y'
              AND wh.org_hier_value = di.district
              AND di.region   = re.region
              AND re.area     = ar.area;

cursor c_org_hier_costing_loc is
      SELECT a.chain,
             r.area,
             d.region,
             s.district,
             il.costing_loc,
             il.costing_loc_type
        FROM area a,
             region r,
             district d,
             store s,
             item_loc il
       WHERE s.store    = il.costing_loc
         AND s.district = d.district
         AND d.region   = r.region
         AND r.area     = a.area
         AND il.item = L_item
         AND il.loc = L_location
      union all
      SELECT NULL chain,
             NULL area,
             NULL region,
             NULL district,
             wh.physical_wh,
             il.costing_loc_type
        FROM wh wh,
             item_loc il
       WHERE wh.wh            = il.costing_loc
         AND (org_hier_type = 1 or org_hier_type is NULL)
         AND stockholding_ind = 'Y'
         AND il.item = L_item
         AND il.loc = L_location
      union all
      SELECT wh.org_hier_value chain,
             NULL area,
             NULL region,
             NULL district,
             il.costing_loc,
             il.costing_loc_type
        FROM wh wh,
             item_loc il
       WHERE wh.wh = il.costing_loc
         AND wh.org_hier_type    = 10
         AND wh.stockholding_ind = 'Y'
         AND il.item = L_item
         AND il.loc = L_location
      union all
      SELECT ar.chain chain,
             wh.org_hier_value  area,
             NULL region,
             NULL district,
             il.costing_loc,
             il.costing_loc_type
        FROM wh wh,
             area ar,
             item_loc il
       WHERE wh.wh = il.costing_loc
         AND wh.org_hier_type    = 20
         AND wh.stockholding_ind = 'Y'
         AND wh.org_hier_value = ar.area
         AND il.item = L_item
         AND il.loc = L_location
      union all
      SELECT ar.chain chain,
             re.area  area,
             wh.org_hier_value region,
             NULL district,
             il.costing_loc,
             il.costing_loc_type
        FROM wh wh,
             area ar,
             region re,
             item_loc il
       WHERE wh.wh = il.costing_loc
         AND wh.org_hier_type    = 30
         AND wh.stockholding_ind = 'Y'
         AND wh.org_hier_value = re.region
         AND re.area     = ar.area
         AND il.item = L_item
         AND il.loc = L_location
      union all
      SELECT ar.chain chain,
             re.area  area,
             di.region region,
             wh.org_hier_value district,
             il.costing_loc,
             il.costing_loc_type
        FROM wh wh,
             area ar,
             region re,
             district di,
             item_loc il
       WHERE wh.wh = il.costing_loc
         AND wh.org_hier_type    = 40
         AND wh.stockholding_ind = 'Y'
         AND wh.org_hier_value = di.district
         AND di.region   = re.region
         AND re.area     = ar.area
         AND il.item = L_item
         AND il.loc = L_location;

   cursor C_GET_STORE_TYPE is
      SELECT store_type
        FROM store
       WHERE store = L_location;
BEGIN
   FOR i IN I_discount_build_tbl.FIRST..I_discount_build_tbl.LAST LOOP
      L_location      := I_discount_build_tbl(i).location;
      L_item          := I_discount_build_tbl(i).item;
      L_virtual_loc   := I_discount_build_tbl(i).virtual_loc;

      if I_discount_build_tbl(i).loc_type ='S' then
         open C_GET_STORE_TYPE;
         fetch C_GET_STORE_TYPE into L_store_type;
         close C_GET_STORE_TYPE;

         if L_store_type is null then
            O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_TYPE',
                                                  L_store_type,
                                                  NULL,
                                                  NULL);
            return FALSE;
         elsif  L_store_type ='F' then
            open C_ORG_HIER_COSTING_LOC;
            fetch C_ORG_HIER_COSTING_LOC
             into I_discount_build_tbl(i).chain,
                  I_discount_build_tbl(i).area,
                  I_discount_build_tbl(i).region,
                  I_discount_build_tbl(i).district,
                  I_discount_build_tbl(i).costing_loc,
                  I_discount_build_tbl(i).costing_loc_type ;

            I_discount_build_tbl(i).franchise_ind := 'Y';

            close C_ORG_HIER_COSTING_LOC;
         else
            open C_ORG_HIER;
            fetch C_ORG_HIER
             into I_discount_build_tbl(i).chain,
                  I_discount_build_tbl(i).area,
                  I_discount_build_tbl(i).region,
                  I_discount_build_tbl(i).district;
            close C_ORG_HIER;
         end if;
      else
         open C_ORG_HIER;
         fetch C_ORG_HIER
          into I_discount_build_tbl(i).chain,
               I_discount_build_tbl(i).area,
               I_discount_build_tbl(i).region,
               I_discount_build_tbl(i).district;
         close C_ORG_HIER;
      end if;
   end loop;

   return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ORG_HIER;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_COSTING_LOC
-- Purpose : get the costing location info for each item/location.
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_COSTING_LOC(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL)
RETURN BOOLEAN is

   L_program      VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.GET_COSTING_LOC';
   L_location     ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE;
   L_item         ITEM_MASTER.ITEM%TYPE;
   L_store_type   STORE.STORE_TYPE%TYPE;

cursor C_GET_COSTING_LOC is
      SELECT DECODE(NVL(wh.physical_wh, -999), NVL(wh.wh, -999), il.costing_loc, wh.physical_wh),
             il.costing_loc_type
        FROM wh wh,
             item_loc il
       WHERE il.costing_loc = wh.wh (+)
         AND il.item = L_item
         AND il.loc = L_location;

   cursor C_GET_STORE_TYPE is
      SELECT store_type
        FROM store
       WHERE store = L_location;
BEGIN
   FOR i IN I_discount_build_tbl.FIRST..I_discount_build_tbl.LAST LOOP
      if I_discount_build_tbl(i).loc_type ='S' then
         L_location   := I_discount_build_tbl(i).location;
         L_item       := I_discount_build_tbl(i).item;

         open C_GET_STORE_TYPE;
         fetch C_GET_STORE_TYPE into L_store_type;
         close C_GET_STORE_TYPE;

         if L_store_type is null then
            O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_TYPE',
                                                  L_store_type,
                                                  NULL,
                                                  NULL);
            return FALSE;
         elsif  L_store_type ='F' then
            open C_GET_COSTING_LOC;
            fetch C_GET_COSTING_LOC
             into I_discount_build_tbl(i).costing_loc,
                  I_discount_build_tbl(i).costing_loc_type ;

            I_discount_build_tbl(i).franchise_ind := 'Y';

            close C_GET_COSTING_LOC;
         end if;
      end if;
   end loop;

   return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_COSTING_LOC;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_OTB_COST
-- Purpose : get original order costs.
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_OTB_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_override_manual_ind   IN       DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE,
                      I_import_country_id     IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                      I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL,
                      I_otb_flag              IN OUT   NUMBER)
RETURN BOOLEAN is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.GET_OTB_COST';
   L_location             ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE;
   L_loc_type             ORDLOC_DISCOUNT_BUILD.LOC_TYPE%TYPE;
   L_cost_zone_group_id   ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE;
   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_comp_item            ITEM_MASTER.ITEM%TYPE := NULL;
   L_origin_country_id    ORDLOC_DISCOUNT_BUILD.ORIGIN_COUNTRY_ID%TYPE;
   L_zone_id              COST_ZONE_GROUP_LOC.ZONE_ID%TYPE;
   L_total_exp            NUMBER(20);
   L_exp_currency         CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate_exp    CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_total_dty            NUMBER(20);
   L_dty_currency         CURRENCIES.CURRENCY_CODE%TYPE;
   L_otb                  DISC_OTB_APPLY.ORIG_OTB%TYPE;

   cursor c_zone_id is
      SELECT zone_id
        FROM cost_zone_group_loc
       WHERE zone_group_id = L_cost_zone_group_id
         /* for land cost functionality, it will not be NULL */
         AND location      = L_location
         AND loc_type      = L_loc_type;
BEGIN

   if I_discount_build_tbl.count >0 then
      FOR i IN I_discount_build_tbl.FIRST..I_discount_build_tbl.LAST LOOP
         L_loc_type             := I_discount_build_tbl(i).loc_type;
         L_cost_zone_group_id   := I_discount_build_tbl(i).cost_zone_group_id;
         L_origin_country_id    := I_discount_build_tbl(i).origin_country_id;

         if LP_elc_ind = 'Y' and I_discount_build_tbl(i).otb_calc_type ='C' then
            /* check if buy pack component item, prepare right input for the following package call*/
            if I_discount_build_tbl(i).pack_no is null or I_discount_build_tbl(i).pack_no ='-999' then
               L_item        := I_discount_build_tbl(i).item;
               L_comp_item   := NULL;
            else
               L_item        := I_discount_build_tbl(i).pack_no;
               L_comp_item   := I_discount_build_tbl(i).item;
            end if;

            /* Below the location for virtual warehouses is copied for informational purposes only */
            /* we do not need to blow them out from the physical. CALC_TOTALS just needs it to be able */
            /* to find a unit cost on ordloc. Since all virtuals within a physical have the same cost, */
            /* it is enough to pass in any one of the virtuals for the physical we have stored in disct_bld. */
            if L_loc_type = 'W' then
               L_location := I_discount_build_tbl(i).virtual_loc;
            else
               L_location := I_discount_build_tbl(i).location;
            end if;

            open C_ZONE_ID;
            fetch C_ZONE_ID into L_zone_id;
            close C_ZONE_ID;

            if L_zone_id is null then
               L_zone_id := L_cost_zone_group_id;
            end if;

            if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                        L_otb,
                                        L_total_exp, /* in prim currency */
                                        L_exp_currency,
                                        L_exchange_rate_exp,
                                        L_total_dty,
                                        L_dty_currency,
                                        LP_order_no,
                                        L_item,
                                        L_comp_item,
                                        L_zone_id,
                                        L_location,
                                        Lp_order_supplier,
                                        L_origin_country_id,
                                        I_import_country_id,
                                        NULL) = FALSE then
               return FALSE;
            end if;
         else
            /* here the unit_cost is in order's currency, it will be convert to */
            /* prim currency in update_otb() */
            L_otb := I_discount_build_tbl(i).unit_cost;

            /* store the original otb and new otb in different elements, so we can */
            /* calculate the difference to be used in update_otb().                */
            if I_otb_flag = 0 then
               I_discount_build_tbl(i).orig_otb := (L_otb * I_discount_build_tbl(i).qty_ordered);
            else
               I_discount_build_tbl(i).new_otb := (L_otb * I_discount_build_tbl(i).qty_ordered);
            end if;
         end if;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_OTB_COST;
---------------------------------------------------------------------------------------------------------------
-- Function: FIND_ITEMS_FOR_DEAL
-- Purpose : For each of the deals found for the order, find all order items that
--           are affected, find the right threshold values, and insert into the
--           ordloc_discount table
---------------------------------------------------------------------------------------------------------------
FUNCTION FIND_ITEMS_FOR_DEAL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_deal_tbl              IN OUT   DEAL_TBL,
                             I_disc_item_tbl         IN OUT   DISC_ITEM_TBL,
                             O_disc_item_tbl         IN OUT   DISC_ITEM_TBL,--out
                             I_disc_item_tbl_cnt     IN OUT   NUMBER,
                             I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL,
                             I_txn_discount_exists   IN OUT   NUMBER,
                             I_seq_no                IN OUT   ORDLOC_DISCOUNT.SEQ_NO%TYPE)
RETURN BINARY_INTEGER is

   L_program               VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.FIND_ITEMS_FOR_DEAL';
   L_item_deal_tbl         item_deal_tbl;
   L_qty_ordered           ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_from_qty_ordered      ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_deal_thresh_val       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_amount_ordered        ORDLOC.UNIT_COST%TYPE:= 0;
   L_from_amount_ordered   ORDLOC.UNIT_COST%TYPE:= 0;
   L_discount_value        DEAL_THRESHOLD.VALUE%TYPE :=0;
   L_item_index            NUMBER :=0;
   L_cont_processing_ind   NUMBER :=0;
   L_fixed_amt_flag        NUMBER :=0;
   L_paid_ind              VARCHAR2(1);
   L_application_order     ORDLOC_DISCOUNT.APPLICATION_ORDER%TYPE := NULL;
   L_deal_tbl_array        NUMBER :=0;
   L_item_deal_tbl_array   NUMBER :=0;
   L_return                BINARY_INTEGER;

   cursor C_ITEMS_FOR_DEAL is
      SELECT rec_in.deal_id ,
             rec_in.deal_detail_id ,
             rec_in.item ,
             rec_in.pack_no ,
             DECODE(rec_in.wf_order_no, NULL, rec_in.location ,
                                              rec_in.orig_location) location,
             DECODE(rec_in.wf_order_no, NULL, rec_in.loc_type,
                                           rec_in.orig_loc_type) loc_type,
             rec_in.unit_cost ,
             rec_in.qty_ordered ,
             rec_in.origin_country_id ,
             rec_in.org_level ,
             rec_in.merch_level
        FROM (SELECT ord.deal_id ,
                     ord.deal_detail_id ,
                     odb.item ,
                     NVL(odb.pack_no,'-999') pack_no ,
                     odb.location ,
                     odb.loc_type ,
                     odb.unit_cost ,
                     odb.qty_ordered ,
                     odb.origin_country_id ,
                     NVL(di.org_level,0) org_level ,
                     di.merch_level ,
                     odb.order_no ,
                     NVL(odb.chain ,    -999) chain ,
                     NVL(odb.area ,     -999) area ,
                     NVL(odb.region ,   -999) region ,
                     NVL(odb.district , -999) district ,
                     NVL(odb.location , -999) loc ,
                     odb.division ,
                     odb.group_no ,
                     odb.dept ,
                     odb.class ,
                     odb.subclass ,
                     odb.item_parent ,
                     odb.diff_1 ,
                     odb.diff_2 ,
                     odb.diff_3 ,
                     odb.diff_4,
                     oh.location orig_location,
                     oh.loc_type orig_loc_type,
                     oh.wf_order_no
                FROM order_deal_build ord ,
                     deal_itemloc_div_grp di ,
                     ordloc_discount_build odb,
                     ordhead oh
               WHERE di.deal_id                                        = ord.deal_id
                 AND di.deal_detail_id                                 = ord.deal_detail_id
                 AND odb.order_no                                      = LP_order_no
                 AND oh.order_no                                       = odb.order_no
                 AND NVL(di.origin_country_id , odb.origin_country_id) = odb.origin_country_id
                 AND di.excl_ind                                       = 'N'
                 AND (   (NVL(di.org_level , 0) = 0)
                      OR (    1 = CASE WHEN NVL(di.org_level , 0) != 1 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 1 AND NVL(odb.chain , -999) = NVL(di.chain , NVL(odb.chain , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 2 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 2 AND NVL(odb.area , -999)     = NVL(di.area , NVL(odb.area , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 3 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 3 AND NVL(odb.region , -999)   = NVL(di.region , NVL(odb.region , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 4 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 4 AND NVL(odb.district , -999) = NVL(di.district , NVL(odb.district , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 5 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 5  AND NVL(odb.location , -999) = NVL(di.location , NVL(odb.location , -999)) THEN 1
                                       ELSE 0 END))
                 AND (   di.merch_level = 1
                      OR (    di.merch_level IN (2 , 3)
                          AND odb.division   = NVL(di.division , odb.division)
                          AND odb.group_no   = NVL(di.group_no , odb.group_no)))
              UNION ALL
              SELECT ord.deal_id ,
                     ord.deal_detail_id ,
                     odb.item ,
                     NVL(odb.pack_no,'-999') pack_no ,
                     odb.location ,
                     odb.loc_type ,
                     odb.unit_cost ,
                     odb.qty_ordered ,
                     odb.origin_country_id ,
                     NVL(di.org_level,0) org_level ,
                     di.merch_level ,
                     odb.order_no ,
                     NVL(odb.chain ,    -999) chain ,
                     NVL(odb.area ,     -999) area ,
                     NVL(odb.region ,   -999) region ,
                     NVL(odb.district , -999) district ,
                     NVL(odb.location , -999) loc ,
                     odb.division ,
                     odb.group_no ,
                     odb.dept ,
                     odb.class ,
                     odb.subclass ,
                     odb.item_parent ,
                     odb.diff_1 ,
                     odb.diff_2 ,
                     odb.diff_3 ,
                     odb.diff_4,
                     oh.location orig_location,
                     oh.loc_type orig_loc_type,
                     oh.wf_order_no
                FROM order_deal_build ord ,
                     deal_itemloc_dcs di ,
                     ordloc_discount_build odb,
                     ordhead oh
               WHERE di.deal_id                                        = ord.deal_id
                 AND di.deal_detail_id                                 = ord.deal_detail_id
                 AND odb.order_no                                      = LP_order_no
                 AND oh.order_no                                       = odb.order_no
                 AND NVL(di.origin_country_id , odb.origin_country_id) = odb.origin_country_id
                 AND di.excl_ind                                       = 'N'
                 AND (   (NVL(di.org_level , 0) = 0)
                      OR (    1 = CASE WHEN NVL(di.org_level , 0) != 1 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 1 AND NVL(odb.chain , -999) = NVL(di.chain , NVL(odb.chain , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 2 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 2 AND NVL(odb.area , -999)     = NVL(di.area , NVL(odb.area , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 3 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 3 AND NVL(odb.region , -999)   = NVL(di.region , NVL(odb.region , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 4 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 4 AND NVL(odb.district , -999) = NVL(di.district , NVL(odb.district , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 5 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 5  AND NVL(odb.location , -999) = NVL(di.location , NVL(odb.location , -999)) THEN 1
                                       ELSE 0 END))
                 AND (    di.merch_level IN (4 , 5 , 6)
                      AND odb.dept       = di.dept
                      AND odb.class      = NVL(di.class , odb.class)
                      AND odb.subclass   = NVL(di.subclass , odb.subclass))
              UNION ALL
              SELECT ord.deal_id ,
                     ord.deal_detail_id ,
                     odb.item ,
                     NVL(odb.pack_no,'-999') pack_no ,
                     odb.location ,
                     odb.loc_type ,
                     odb.unit_cost ,
                     odb.qty_ordered ,
                     odb.origin_country_id ,
                     NVL(di.org_level,0) org_level ,
                     di.merch_level ,
                     odb.order_no ,
                     NVL(odb.chain ,    -999) chain ,
                     NVL(odb.area ,     -999) area ,
                     NVL(odb.region ,   -999) region ,
                     NVL(odb.district , -999) district ,
                     NVL(odb.location , -999) loc ,
                     odb.division ,
                     odb.group_no ,
                     odb.dept ,
                     odb.class ,
                     odb.subclass ,
                     odb.item_parent ,
                     odb.diff_1 ,
                     odb.diff_2 ,
                     odb.diff_3 ,
                     odb.diff_4,
                     oh.location orig_location,
                     oh.loc_type orig_loc_type,
                     oh.wf_order_no
                FROM order_deal_build ord ,
                     deal_itemloc_parent_diff di ,
                     ordloc_discount_build odb,
                     ordhead oh
               WHERE di.deal_id                                        = ord.deal_id
                 AND di.deal_detail_id                                 = ord.deal_detail_id
                 AND odb.order_no                                      = LP_order_no
                 AND oh.order_no                                       = odb.order_no
                 AND NVL(di.origin_country_id , odb.origin_country_id) = odb.origin_country_id
                 AND di.excl_ind                                       = 'N'
                 AND (   (NVL(di.org_level , 0) = 0)
                      OR (    1 = CASE WHEN NVL(di.org_level , 0) != 1 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 1 AND NVL(odb.chain , -999) = NVL(di.chain , NVL(odb.chain , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 2 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 2 AND NVL(odb.area , -999)     = NVL(di.area , NVL(odb.area , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 3 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 3 AND NVL(odb.region , -999)   = NVL(di.region , NVL(odb.region , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 4 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 4 AND NVL(odb.district , -999) = NVL(di.district , NVL(odb.district , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 5 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 5  AND NVL(odb.location , -999) = NVL(di.location , NVL(odb.location , -999)) THEN 1
                                       ELSE 0 END))
                 AND di.merch_level  IN (7 , 8 , 9 , 10 , 11)
                 AND odb.item_parent                                                = di.item_parent
                 AND 1 = CASE WHEN di.merch_level  = 7 AND di.item_parent = odb.item_parent THEN 1
                              WHEN di.merch_level != 7 THEN 1
                              ELSE 0 END
                 AND 1 = CASE WHEN di.merch_level  = 8 AND di.item_parent = odb.item_parent AND di.diff_1 = odb.diff_1 THEN 1
                              WHEN di.merch_level != 8 THEN 1
                              ELSE 0 END
                 AND 1 = CASE WHEN di.merch_level  = 9 AND di.item_parent = odb.item_parent AND di.diff_2 = odb.diff_2 THEN 1
                              WHEN di.merch_level != 9 THEN 1
                              ELSE 0 END
                 AND 1 = CASE WHEN di.merch_level  = 10 AND di.item_parent = odb.item_parent AND di.diff_3 = odb.diff_3 THEN 1
                              WHEN di.merch_level != 10 THEN 1
                              ELSE 0 END
                 AND 1 = CASE WHEN di.merch_level  = 11 AND di.item_parent = odb.item_parent AND di.diff_4 = odb.diff_4 THEN 1
                              WHEN di.merch_level != 11 THEN 1
                              ELSE 0 END
              UNION ALL
              SELECT ord.deal_id ,
                     ord.deal_detail_id ,
                     odb.item ,
                     NVL(odb.pack_no,'-999') pack_no ,
                     odb.location ,
                     odb.loc_type ,
                     odb.unit_cost ,
                     odb.qty_ordered ,
                     odb.origin_country_id ,
                     NVL(di.org_level,0) org_level ,
                     di.merch_level ,
                     odb.order_no ,
                     NVL(odb.chain ,    -999) chain ,
                     NVL(odb.area ,     -999) area ,
                     NVL(odb.region ,   -999) region ,
                     NVL(odb.district , -999) district ,
                     NVL(odb.location , -999) loc ,
                     odb.division ,
                     odb.group_no ,
                     odb.dept ,
                     odb.class ,
                     odb.subclass ,
                     odb.item_parent ,
                     odb.diff_1 ,
                     odb.diff_2 ,
                     odb.diff_3 ,
                     odb.diff_4,
                     oh.location orig_location,
                     oh.loc_type orig_loc_type,
                     oh.wf_order_no
                FROM order_deal_build ord ,
                     deal_itemloc_item di ,
                     ordloc_discount_build odb,
                     ordhead oh
               WHERE di.deal_id                                        = ord.deal_id
                 AND di.deal_detail_id                                 = ord.deal_detail_id
                 AND odb.order_no                                      = LP_order_no
                 AND oh.order_no                                       = odb.order_no
                 AND NVL(di.origin_country_id , odb.origin_country_id) = odb.origin_country_id
                 AND di.excl_ind                                       = 'N'
                 AND (   (NVL(di.org_level , 0) = 0)
                      OR (    1 = CASE WHEN NVL(di.org_level , 0) != 1 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 1 AND NVL(odb.chain , -999) = NVL(di.chain , NVL(odb.chain , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 2 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 2 AND NVL(odb.area , -999)     = NVL(di.area , NVL(odb.area , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 3 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 3 AND NVL(odb.region , -999)   = NVL(di.region , NVL(odb.region , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 4 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 4 AND NVL(odb.district , -999) = NVL(di.district , NVL(odb.district , -999)) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di.org_level , 0) != 5 THEN 1
                                       WHEN NVL(di.org_level , 0)  = 5  AND NVL(odb.location , -999) = NVL(di.location , NVL(odb.location , -999)) THEN 1
                                       ELSE 0 END))
                 AND odb.item = di.item) rec_in
      WHERE NOT EXISTS
                (SELECT 'x'
                   FROM deal_itemloc_div_grp di1
                  WHERE di1.deal_id                                           = rec_in.deal_id
                    AND di1.deal_detail_id                                    = rec_in.deal_detail_id
                    AND rec_in.order_no                                       = LP_order_no
                    AND NVL(di1.origin_country_id , rec_in.origin_country_id) = rec_in.origin_country_id
                    AND di1.excl_ind                                          = 'Y'
                    /* Org level matches */
                    AND (   (NVL(di1.org_level , 0) = 0)
                         OR (    1 = CASE WHEN NVL(di1.org_level , 0) != 1 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 1 AND rec_in.chain    = NVL(di1.chain , rec_in.chain) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 2 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 2 AND rec_in.area     = NVL(di1.area , rec_in.area) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 3 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 3 AND rec_in.region   = NVL(di1.region , rec_in.region) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 4 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 4 AND rec_in.district = NVL(di1.district , rec_in.district) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 5 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 5  AND rec_in.loc     = NVL(di1.location , rec_in.loc) THEN 1
                                       ELSE 0 END))
                    /* Merch level matches */
                    AND di1.merch_level IN (2 , 3)
                    AND rec_in.division = NVL(di1.division , rec_in.division)
                    AND rec_in.group_no = NVL(di1.group_no , rec_in.group_no)
                    AND ROWNUM                                                       = 1 )
        AND NOT EXISTS
                (SELECT 'x'
                   FROM deal_itemloc_dcs di1
                  WHERE di1.deal_id                                           = rec_in.deal_id
                    AND di1.deal_detail_id                                    = rec_in.deal_detail_id
                    AND rec_in.order_no                                       = LP_order_no
                    AND NVL(di1.origin_country_id , rec_in.origin_country_id) = rec_in.origin_country_id
                    AND di1.excl_ind                                          = 'Y'
                    /* Org level matches */
                    AND (   (NVL(di1.org_level , 0) = 0)
                         OR (    1 = CASE WHEN NVL(di1.org_level , 0) != 1 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 1 AND rec_in.chain    = NVL(di1.chain , rec_in.chain) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 2 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 2 AND rec_in.area     = NVL(di1.area , rec_in.area) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 3 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 3 AND rec_in.region   = NVL(di1.region , rec_in.region) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 4 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 4 AND rec_in.district = NVL(di1.district , rec_in.district) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 5 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 5  AND rec_in.loc     = NVL(di1.location , rec_in.loc) THEN 1
                                       ELSE 0 END))
                    /* Merch level matches */
                    AND rec_in.dept     = di1.dept
                    AND rec_in.class    = NVL(di1.class , rec_in.class)
                    AND rec_in.subclass = NVL(di1.subclass , rec_in.subclass)
                    AND ROWNUM          = 1)
        AND NOT EXISTS
                (SELECT 'x'
                   FROM deal_itemloc_parent_diff di1
                  WHERE di1.deal_id                                           = rec_in.deal_id
                    AND di1.deal_detail_id                                    = rec_in.deal_detail_id
                    AND rec_in.order_no                                       = LP_order_no
                    AND NVL(di1.origin_country_id , rec_in.origin_country_id) = rec_in.origin_country_id
                    AND di1.excl_ind                                          = 'Y'
                    /* Org level matches */
                    AND (   (NVL(di1.org_level , 0) = 0)
                         OR (    1 = CASE WHEN NVL(di1.org_level , 0) != 1 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 1 AND rec_in.chain    = NVL(di1.chain , rec_in.chain) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 2 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 2 AND rec_in.area     = NVL(di1.area , rec_in.area) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 3 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 3 AND rec_in.region   = NVL(di1.region , rec_in.region) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 4 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 4 AND rec_in.district = NVL(di1.district , rec_in.district) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 5 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 5  AND rec_in.loc     = NVL(di1.location , rec_in.loc) THEN 1
                                       ELSE 0 END))
                    /* Merch level matches */
                    AND rec_in.item_parent                                         = di1.item_parent
                    AND 1 = CASE WHEN di1.merch_level = 7 AND di1.item_parent = rec_in.item_parent THEN 1
                                 WHEN di1.merch_level != 7 THEN 1
                                 ELSE 0 END
                    AND 1 = CASE WHEN di1.merch_level = 8 AND di1.item_parent = rec_in.item_parent AND di1.diff_1 = rec_in.diff_1 THEN 1
                                 WHEN di1.merch_level != 8 THEN 1
                                 ELSE 0 END
                    AND 1 = CASE WHEN di1.merch_level = 9 AND di1.item_parent = rec_in.item_parent AND di1.diff_2 = rec_in.diff_2 THEN 1
                                 WHEN di1.merch_level != 9 THEN 1
                                 ELSE 0 END
                    AND 1 = CASE WHEN di1.merch_level = 10 AND di1.item_parent = rec_in.item_parent AND di1.diff_3 = rec_in.diff_3 THEN 1
                                 WHEN di1.merch_level != 10 THEN 1
                                 ELSE 0 END
                    AND 1 = CASE WHEN di1.merch_level = 11 AND di1.item_parent = rec_in.item_parent AND di1.diff_4 = rec_in.diff_4 THEN 1
                                 WHEN di1.merch_level != 11 THEN 1
                                 ELSE 0 END
                    AND ROWNUM = 1)
        AND NOT EXISTS
                (SELECT 'x'
                   FROM deal_itemloc_item di1
                  WHERE di1.deal_id                                           = rec_in.deal_id
                    AND di1.deal_detail_id                                    = rec_in.deal_detail_id
                    AND rec_in.order_no                                       = LP_order_no
                    AND NVL(di1.origin_country_id , rec_in.origin_country_id) = rec_in.origin_country_id
                    AND di1.excl_ind                                          = 'Y'
                    /* Org level matches */
                    AND (   (NVL(di1.org_level , 0) = 0)
                         OR (    1 = CASE WHEN NVL(di1.org_level , 0) != 1 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 1 AND rec_in.chain    = NVL(di1.chain , rec_in.chain) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 2 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 2 AND rec_in.area     = NVL(di1.area , rec_in.area) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 3 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 3 AND rec_in.region   = NVL(di1.region , rec_in.region) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 4 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 4 AND rec_in.district = NVL(di1.district , rec_in.district) THEN 1
                                       ELSE 0 END
                          AND 1 = CASE WHEN NVL(di1.org_level , 0) != 5 THEN 1
                                       WHEN NVL(di1.org_level , 0)  = 5  AND rec_in.loc     = NVL(di1.location , rec_in.loc) THEN 1
                                       ELSE 0 END))
                    /* Merch level matches */
                    AND rec_in.item = di1.item
                    AND ROWNUM      = 1)
      ORDER BY rec_in.item, rec_in.pack_no, rec_in.location;
BEGIN
   open C_ITEMS_FOR_DEAL;
   fetch C_ITEMS_FOR_DEAL BULK COLLECT into L_item_deal_tbl;
   close C_ITEMS_FOR_DEAL;
   if L_item_deal_tbl.count > 0 then
      for i in I_deal_tbl.FIRST..I_deal_tbl.LAST loop
         L_deal_tbl_array := i;
         L_deal_thresh_val:= 0;
         for j in L_item_deal_tbl.FIRST..L_item_deal_tbl.LAST loop
            /* for each deal(dh.deal_id and dd.deal_detail_id) that was found to have something */
            /* to do with this order, find match item+location combos (in current order) for inserting into */
            /* ordloc_discount ss_dealordlib_table */
            if I_deal_tbl(i).deal_id = L_item_deal_tbl(j).deal_id and
               I_deal_tbl(i).deal_detail_id = L_item_deal_tbl(j).deal_detail_id then
               /* if deal is at transaction level, then break.  deals are ordered such
                  that all transactions come at end */
               if I_deal_tbl(i).txn_discount_ind ='Y' then
                  I_txn_discount_exists := 1;
                  return 0;
               else
                  /* if the deal is rebate, ignore it when it doesn't apply to purchases. */
                  if I_deal_tbl(i).rebate_ind ='Y'  and I_deal_tbl(i).rebate_purch_sales_ind != 'P' then
                     null;
                  else
                     /* since the threshold value for each item is the total number    */
                     /* ordered of all the items affected, thus here calc the total.   */
                     /* When deal_detail.threshold_limit_uom is not null, then need to */
                     /* convert the qty to deal's uom;                                 */
                     /* When deal_detail.threshold_limit_type is amount, then need to  */
                     /* convert the qty to monetary amount.                            */
                     /* below is only true for quantity */
                     if I_deal_tbl(i).threshold_limit_uom is not null then
                        L_from_qty_ordered := L_item_deal_tbl(j).qty_ordered;

                        if UOM_SQL.CONVERT(O_error_message,
                                        L_qty_ordered,           /* to_value   */
                                        I_deal_tbl(i).threshold_limit_uom, /* to_uom     */
                                        L_from_qty_ordered,         /* from_value */
                                        NULL,                    /* from_uom   */
                                        L_item_deal_tbl(j).item,
                                        LP_order_supplier,
                                        L_item_deal_tbl(j).origin_country_id) = FALSE then
                           return -1;
                        end if;

                        L_deal_thresh_val := L_deal_thresh_val + L_qty_ordered;
                     else
                        if I_deal_tbl(i).threshold_limit_type = 'A' then
                        /* convert to monetary amount and convert to deal's currency */
                           L_from_amount_ordered := (L_item_deal_tbl(j).unit_cost * L_item_deal_tbl(j).qty_ordered);

                           if LP_order_currency_code != I_deal_tbl(i).currency_code then
                              if CURRENCY_SQL.CONVERT(O_error_message,
                                                      L_from_amount_ordered,
                                                      LP_order_currency_code,
                                                      I_deal_tbl(i).currency_code,
                                                      L_amount_ordered,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL) = FALSE then
                                 return -1;
                              end if;
                              L_deal_thresh_val := L_deal_thresh_val + L_amount_ordered;
                           else
                              L_deal_thresh_val := L_deal_thresh_val +L_from_amount_ordered;
                           end if;
                        else
                           L_deal_thresh_val := L_deal_thresh_val + L_item_deal_tbl(j).qty_ordered;
                        end if;
                     end if;
                  end if;
               end if;

               if I_deal_tbl(i).threshold_value_type = 'Q' then
                  /* this discount value is not used in calc functions for Q type deals insert
                   0 into ordloc_discount so record will be removed if deal is not applied */
                  L_discount_value := 0;
               else
                  L_return := DEAL_ORD_LIB_SQL.GET_THRESHOLD_VALUE(O_error_message,
                                                                   L_deal_thresh_val,
                                                                   I_deal_tbl(i).deal_id,
                                                                   I_deal_tbl(i).deal_detail_id,
                                                                   I_deal_tbl(i).rebate_ind,
                                                                   L_discount_value);
                  if L_return < 0 then
                     return L_return;
                  elsif L_return = 1 then
                     continue;
                  end if;

                  if I_deal_tbl(i).threshold_value_type in ('A','F') and LP_order_currency_code != I_deal_tbl(i).currency_code then
                     /* convert from deal's currency to order's currency */
                     if CURRENCY_SQL.CONVERT(O_error_message,
                                             L_discount_value,--in
                                             I_deal_tbl(i).currency_code,
                                             LP_order_currency_code,
                                             L_discount_value,--out
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL) = FALSE then
                        return -1;
                     end if;
                  end if;

               end if;
            end if;
         end loop;

      /* process the fetched item_loc_deal array for the current deal */
         for k in L_item_deal_tbl.FIRST..L_item_deal_tbl.LAST loop
            if I_deal_tbl(i).deal_id = L_item_deal_tbl(k).deal_id and
               I_deal_tbl(i).deal_detail_id = L_item_deal_tbl(k).deal_detail_id then
               L_cont_processing_ind := 0;
               L_item_deal_tbl_array := k;
               if DEAL_ORD_LIB_SQL.GET_ITEM_INDEX(O_error_message,
                                                  I_discount_build_tbl,
                                                  L_item_deal_tbl(k).item,
                                                  L_item_deal_tbl(k).location,
                                                  L_item_index)= FALSE then
                  return -1;
               end if;

               if L_item_index != 0 and I_discount_build_tbl(L_item_index).exclusive_deal_applied != 0 then
               /* current item already has an exclusive deal.  Only the deal of the
                  highest merch level should be applied.  If the merch level of the current
                  deal is higher than that of the previously applied deal, then apply the
                  new deal.  Otherwise, skip this deal and continue (dh+dd+item+loc never gets
                  into odb). Note that the higher the merch level, the lower the merch level index.
               */
                  if I_deal_tbl(i).deal_class ='EX' then
                     if L_item_deal_tbl(k).merch_level <= I_discount_build_tbl(L_item_index).merch_level then
                        if L_item_deal_tbl(k).merch_level = I_discount_build_tbl(L_item_index).merch_level then
                           if L_item_deal_tbl(k).org_level < I_discount_build_tbl(L_item_index).org_level then
                              I_discount_build_tbl(L_item_index).org_level := L_item_deal_tbl(k).org_level;
                              L_cont_processing_ind := 1;
                           end if;
                        else
                           I_discount_build_tbl(L_item_index).org_level   := L_item_deal_tbl(k).org_level;
                           I_discount_build_tbl(L_item_index).merch_level := L_item_deal_tbl(k).merch_level;
                           L_cont_processing_ind := 1;
                        end if;
                     end if;
                  end if;
               else /* no exclusive deal was applied, so continue as normal. */
                  L_cont_processing_ind := 1;
               end if;
               /* check if the current deal is an exclusive deal, if it is:  */
               /* 1> delete all records for that item from ordloc_discount;  */
               /* 2> delete the item from ordloc_discount_build, */
               /* since the item won't be used any more(deals are ordered in importance, and exclusive deal is the only deal that needs to be applied to the item. */
               if L_cont_processing_ind = 1 then
                  if I_deal_tbl(i).deal_class ='EX' then
                     if DEAL_ORD_LIB_SQL.PROCESS_EXCLUSIVE_DEAL(O_error_message,
                                                                L_item_deal_tbl(k).item,
                                                                L_item_deal_tbl(k).location,
                                                                I_discount_build_tbl,
                                                                L_item_deal_tbl(k).merch_level,
                                                                L_item_deal_tbl(k).org_level)= FALSE then
                        return -1;
                     end if;
                  end if;

                  if I_disc_item_tbl_cnt >0 and I_deal_tbl(i).threshold_value_type ='F' then
                     L_fixed_amt_flag := 0;

                     if O_disc_item_tbl.count > 0 then
                        for m in O_disc_item_tbl.FIRST..O_disc_item_tbl.LAST loop
                           if O_disc_item_tbl(m).item = L_item_deal_tbl(k).item and
                              O_disc_item_tbl(m).pack_no = L_item_deal_tbl(k).pack_no and
                              O_disc_item_tbl(m).location = L_item_deal_tbl(k).location and
                              O_disc_item_tbl(m).location = 1 then

                              L_fixed_amt_flag := 1;
                              EXIT;
                           end if;
                        end loop;
                     end if;

                     if L_fixed_amt_flag = 1 then
                        continue;
                     end if;
                  end if;

                  if DEAL_ORD_LIB_SQL.GET_APPLICATION_ORDER(O_error_message,
                                                            L_item_deal_tbl(k).item,
                                                            L_item_deal_tbl(k).location,
                                                            L_application_order)= FALSE then
                        return -1;
                     end if;

                  if I_deal_tbl(i).billing_type = 'OI' and I_deal_tbl(i).deal_appl_timing = 'O' then
                     L_paid_ind := 'Y';
                  else
                     L_paid_ind := 'N';
                  end if;

                  if DEAL_ORD_LIB_SQL.STORE_DISC_ITEM(O_error_message,
                                                      L_item_deal_tbl,
                                                      L_item_deal_tbl_array,
                                                      I_deal_tbl,
                                                      L_deal_tbl_array,
                                                      O_disc_item_tbl,
                                                      I_disc_item_tbl_cnt)= FALSE then
                     return -1;
                  end if;

                  INSERT INTO ordloc_discount
                             (order_no,
                              seq_no,
                              item,
                              location,
                              deal_id,
                              deal_detail_id,
                              pack_no,
                              discount_value,
                              discount_type,
                              discount_amt_per_unit,
                              application_order,
                              paid_ind,
                              last_calc_date)
                      VALUES(LP_order_no,
                             I_seq_no,
                             L_item_deal_tbl(k).item,
                             L_item_deal_tbl(k).location,
                             I_deal_tbl(i).deal_id,
                             I_deal_tbl(i).deal_detail_id,
                             L_item_deal_tbl(k).pack_no,
                             L_discount_value,
                             I_deal_tbl(i).threshold_value_type,
                             0, /* calc_discount will update this */
                             L_application_order,
                             L_paid_ind,
                             LP_vdate);

                  I_seq_no := I_seq_no+1;

               end if;
            end if;
            L_item_index := 0;
         end loop;
      end loop;
   end if;

   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return -1;
END FIND_ITEMS_FOR_DEAL;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_THRESHOLD_VALUE
-- Purpose : to find the discount value given the order cost
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_THRESHOLD_VALUE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_deal_thresh_val   IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                             I_deal_id           IN       DEAL_DETAIL.DEAL_ID%TYPE,
                             I_deal_detail_id    IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                             I_rebate_ind        IN       DEAL_HEAD.REBATE_IND%TYPE,
                             I_discount_value    IN OUT   DEAL_THRESHOLD.VALUE%TYPE)
RETURN BINARY_INTEGER is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.GET_THRESHOLD_VALUE';
   L_discount_value       DEAL_THRESHOLD.VALUE%TYPE := NULL;
   L_upper_limit          DEAL_THRESHOLD.UPPER_LIMIT%TYPE := NULL;
   L_lower_limit          DEAL_THRESHOLD.LOWER_LIMIT%TYPE := NULL;

   cursor c_discount_value is
      SELECT /*+ FIRST_ROWS */
             value
        FROM deal_threshold
       WHERE deal_id        = I_deal_id
         AND deal_detail_id = I_deal_detail_id
         AND I_deal_thresh_val BETWEEN lower_limit AND upper_limit
         AND ROWNUM         = 1;

   cursor c_discount_max_value is
      SELECT /*+ FIRST_ROWS */
             value,
             upper_limit,
             lower_limit
        FROM deal_threshold a
       WHERE deal_id        = I_deal_id
         AND deal_detail_id = I_deal_detail_id
         AND 0=(select count(*)
                  from deal_threshold b
                 WHERE a.deal_id=b.deal_id
                   AND a.deal_detail_id=b.deal_detail_id
                   AND a.upper_limit < b.upper_limit)
         AND rownum=1;

   cursor c_discount_value_rebate is
      SELECT /*+ FIRST_ROWS */
             value
        FROM deal_threshold
       WHERE deal_id          = I_deal_id
         AND deal_detail_id   = I_deal_detail_id
         AND target_level_ind = 'Y'
         AND ROWNUM         = 1;
BEGIN
    LP_deal_threshold_value := I_deal_thresh_val;

   if I_rebate_ind ='N' then
      open c_discount_value;
      fetch c_discount_value into L_discount_value;
      close c_discount_value;

      if L_discount_value is null then
         open c_discount_max_value;
         fetch c_discount_max_value into L_discount_value,
                                         LP_upper_limit,
                                         LP_lower_limit;
         if c_discount_max_value%NOTFOUND then
            close c_discount_max_value;
            return 1;
         end if;

         close c_discount_max_value;
      end if;

      if LP_upper_limit < I_deal_thresh_val then
         I_discount_value := L_discount_value;
      else
         return 1;
      end if;
   else
      open c_discount_value_rebate;
      fetch c_discount_value_rebate into L_discount_value;
      close c_discount_value_rebate;

      if L_discount_value is not null then
         I_discount_value := L_discount_value;
      else
      I_discount_value := 0;
      end if;
   end if;

   return 0;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return -1;
END GET_THRESHOLD_VALUE;
---------------------------------------------------------------------------------------------------------------
-- Function: PROCESS_EXCLUSIVE_DEAL
-- Purpose : process exclusive deal
---------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_EXCLUSIVE_DEAL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                 IN       ORDLOC_DISCOUNT_BUILD.ITEM%TYPE,
                                I_location             IN       ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
                                I_discount_build_tbl   IN OUT   DISCOUNT_BUILD_TBL,
                                I_merch_level          IN OUT   DEAL_ITEMLOC.MERCH_LEVEL%TYPE,
                                I_org_level            IN OUT   DEAL_ITEMLOC.ORG_LEVEL%TYPE)
RETURN BOOLEAN is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.PROCESS_EXCLUSIVE_DEAL';
   L_item_index            NUMBER :=0;

BEGIN
   /* Note: "EX"clusive deals are exclusive to the item, not item+loc. (All locs must go with the item.) */
   DELETE FROM ordloc_discount
         WHERE order_no = LP_order_no
           AND item      = I_item
           AND location  = I_location;

   if DEAL_ORD_LIB_SQL.GET_ITEM_INDEX(O_error_message,
                                      I_discount_build_tbl,
                                      I_item,
                                      I_location,
                                      L_item_index)= FALSE then
      return FALSE;
   end if;

   I_discount_build_tbl(L_item_index).exclusive_deal_applied := 1;
   I_discount_build_tbl(L_item_index).org_level   := I_org_level;
   I_discount_build_tbl(L_item_index).merch_level := I_merch_level;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_EXCLUSIVE_DEAL;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_APPLICATION_ORDER
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_APPLICATION_ORDER(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item                IN       ORDLOC_DISCOUNT_BUILD.ITEM%TYPE,
                               I_location            IN       ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
                               I_application_order   IN OUT   ORDLOC_DISCOUNT.APPLICATION_ORDER%TYPE)
RETURN BOOLEAN is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.GET_APPLICATION_ORDER';

   cursor c_get_app_order is
      SELECT /*+ FIRST_ROWS */
             NVL(MAX(application_order), 0) + 1
        FROM ordloc_discount
       WHERE order_no  = LP_order_no
         AND item      = I_item
         AND location  = I_location
         AND rownum    = 1;
BEGIN
   open c_get_app_order;
   fetch c_get_app_order into I_application_order;
   close c_get_app_order;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_APPLICATION_ORDER;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_ITEM_INDEX
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_INDEX(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_discount_build_tbl   IN OUT   DISCOUNT_BUILD_TBL,
                        I_item                 IN       ORDLOC_DISCOUNT_BUILD.ITEM%TYPE,
                        I_location             IN       ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
                        I_item_index           IN OUT   NUMBER)
RETURN BOOLEAN is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.GET_ITEM_INDEX';

BEGIN
   for i in I_discount_build_tbl.FIRST..I_discount_build_tbl.LAST loop
      if I_discount_build_tbl(i).item = I_item
         and I_discount_build_tbl(i).location = I_location then
         I_item_index := i;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEM_INDEX;
---------------------------------------------------------------------------------------------------------------
-- Function: STORE_DISC_ITEM
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION STORE_DISC_ITEM(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item_deal_tbl         IN OUT   ITEM_DEAL_TBL,
                         I_item_deal_tbl_array   IN       NUMBER,
                         I_deal_tbl              IN OUT   DEAL_TBL,
                         I_deal_tbl_array        IN       NUMBER,
                         O_disc_item_tbl         IN OUT   DISC_ITEM_TBL,
                         I_disc_item_tbl_cnt     IN OUT   NUMBER)
RETURN BOOLEAN is

   L_program                VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.STORE_DISC_ITEM';
   L_unit_cost              ORDLOC.UNIT_COST%TYPE;
   L_fixed_amt_flag         NUMBER :=0;
   L_qty_thresh_get_value   DEAL_DETAIL.QTY_THRESH_GET_VALUE%TYPE :=0;
BEGIN
   if I_item_deal_tbl(I_item_deal_tbl_array).unit_cost != 0 then
   /* in oa_disc_item array, all amts are stored in order's currency */
   /* convert free item's unit_cost to order's currency if order's curr != deal's */
      if CURRENCY_SQL.CONVERT(O_error_message,
                              I_item_deal_tbl(I_item_deal_tbl_array).unit_cost,--in
                              I_deal_tbl(I_deal_tbl_array).currency_code,--from
                              LP_order_currency_code,--to
                              L_unit_cost,--out
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;
   else
      L_unit_cost :=I_item_deal_tbl(I_item_deal_tbl_array).unit_cost;
   end if;

   if I_deal_tbl(I_deal_tbl_array).threshold_value_type = 'F' then
      L_fixed_amt_flag := 1;
   else
      L_fixed_amt_flag := 0;
   end if;

   O_disc_item_tbl(I_disc_item_tbl_cnt).item                 := I_item_deal_tbl(I_item_deal_tbl_array).item;
   O_disc_item_tbl(I_disc_item_tbl_cnt).pack_no              := I_item_deal_tbl(I_item_deal_tbl_array).pack_no;
   O_disc_item_tbl(I_disc_item_tbl_cnt).qty_ordered          := I_item_deal_tbl(I_item_deal_tbl_array).qty_ordered;
   O_disc_item_tbl(I_disc_item_tbl_cnt).unit_cost            := I_item_deal_tbl(I_item_deal_tbl_array).unit_cost;
   O_disc_item_tbl(I_disc_item_tbl_cnt).origin_country_id    := I_item_deal_tbl(I_item_deal_tbl_array).origin_country_id;
   O_disc_item_tbl(I_disc_item_tbl_cnt).location             := I_item_deal_tbl(I_item_deal_tbl_array).location;
   O_disc_item_tbl(I_disc_item_tbl_cnt).loc_type             := I_item_deal_tbl(I_item_deal_tbl_array).loc_type;
   O_disc_item_tbl(I_disc_item_tbl_cnt).deal_id              := I_deal_tbl(I_deal_tbl_array).deal_id;
   O_disc_item_tbl(I_disc_item_tbl_cnt).deal_detail_id       := I_deal_tbl(I_deal_tbl_array).deal_detail_id;
   O_disc_item_tbl(I_disc_item_tbl_cnt).deal_class           := I_deal_tbl(I_deal_tbl_array).deal_class;
   O_disc_item_tbl(I_disc_item_tbl_cnt).qty_thresh_buy_item  := I_deal_tbl(I_deal_tbl_array).qty_thresh_buy_item;
   O_disc_item_tbl(I_disc_item_tbl_cnt).qty_thresh_buy_qty   := I_deal_tbl(I_deal_tbl_array).qty_thresh_buy_qty;
   O_disc_item_tbl(I_disc_item_tbl_cnt).qty_thresh_get_item  := I_deal_tbl(I_deal_tbl_array).qty_thresh_get_item;
   O_disc_item_tbl(I_disc_item_tbl_cnt).qty_thresh_get_qty   := I_deal_tbl(I_deal_tbl_array).qty_thresh_get_qty;
   O_disc_item_tbl(I_disc_item_tbl_cnt).get_free_discount    := I_deal_tbl(I_deal_tbl_array).get_free_discount;
   O_disc_item_tbl(I_disc_item_tbl_cnt).qty_thresh_recur_ind := I_deal_tbl(I_deal_tbl_array).qty_thresh_recur_ind;
   O_disc_item_tbl(I_disc_item_tbl_cnt).qty_thresh_get_type  := I_deal_tbl(I_deal_tbl_array).qty_thresh_get_type;

   if O_disc_item_tbl(I_disc_item_tbl_cnt).qty_thresh_get_type in ('A','F') and
      LP_order_currency_code != I_deal_tbl(I_deal_tbl_array).currency_code and
      I_deal_tbl(I_deal_tbl_array).qty_thresh_get_value != 0 then
      /* convert from deal's currency to order's currency */
      if CURRENCY_SQL.CONVERT(O_error_message,
                              I_deal_tbl(I_deal_tbl_array).qty_thresh_get_value,--in
                              I_deal_tbl(I_deal_tbl_array).currency_code,
                              LP_order_currency_code,
                              L_qty_thresh_get_value,--out
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;
   else
      L_qty_thresh_get_value  := I_deal_tbl(I_deal_tbl_array).qty_thresh_get_value;
   end if;

   --O_disc_item_tbl(I_disc_item_tbl_cnt).qty_thresh_get_type  := L_qty_thresh_get_value;
   O_disc_item_tbl(I_disc_item_tbl_cnt).fixed_amt_flag       := L_fixed_amt_flag;
   O_disc_item_tbl(I_disc_item_tbl_cnt).free_item_unit_cost  := L_unit_cost;

   I_disc_item_tbl_cnt := I_disc_item_tbl_cnt+1;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END STORE_DISC_ITEM;
---------------------------------------------------------------------------------------------------------------
-- Function: CALCULATE_COST
-- Purpose : update the ordloc table for off_invoice deal
---------------------------------------------------------------------------------------------------------------
FUNCTION CALCULATE_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_disc_item_tbl         IN OUT   DISC_ITEM_TBL,
                        I_disc_item_tbl_cnt     IN OUT   NUMBER,
                        I_import_order_ind      IN       ORDHEAD.IMPORT_ORDER_IND%TYPE,
                        I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL)
RETURN BOOLEAN is

   L_program                VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.CALCULATE_COST';
   L_unit_cost_init         ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE :=0;
   L_qty_ordered            ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE :=0;
   L_buy_qty                DEAL_DETAIL.QTY_THRESH_BUY_QTY%TYPE :=0;
   L_get_qty                DEAL_DETAIL.QTY_THRESH_GET_QTY%TYPE :=0;
   L_free_item_unit_cost    DEAL_DETAIL.QTY_THRESH_FREE_ITEM_UNIT_COST%TYPE:=0;
   L_deal_class             DEAL_DETAIL.DEAL_CLASS%TYPE;
   L_buy_item               DEAL_DETAIL.QTY_THRESH_BUY_ITEM%TYPE;
   L_get_item               DEAL_DETAIL.QTY_THRESH_GET_ITEM%TYPE;
   L_recur_ind              DEAL_DETAIL.QTY_THRESH_RECUR_IND%TYPE;
   L_qty_thresh_get_type    DEAL_DETAIL.QTY_THRESH_GET_TYPE%TYPE;
   L_qty_thresh_get_value   DEAL_DETAIL.QTY_THRESH_GET_VALUE%TYPE;
   L_get_free_discount      DEAL_DETAIL.GET_FREE_DISCOUNT%TYPE:=0;
   L_done_with_items        NUMBER := 0;
   L_total_qty_ordered      ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_no_deal_applied_ind    VARCHAR2(1) := 'N';
   L_unit_cost              ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE:=0;
   L_temp_value             NUMBER := 0;
   L_get_list_array_tbl     get_list_array_tbl;
   L_cut_off_costs_tbl      cut_off_costs_tbl;
   L_origin_country_id      ORDLOC_DISCOUNT_BUILD.ORIGIN_COUNTRY_ID%TYPE;

   cursor C_OFF_INVOICE_ITEM is
      SELECT item,
             pack_no,
             location,
             deal_id,
             deal_detail_id,
             discount_value,
             discount_type,
             paid_ind
        FROM ordloc_discount
       WHERE order_no       = LP_order_no
    ORDER BY seq_no;

    TYPE off_invoice_item_tbl is table of C_OFF_INVOICE_ITEM%ROWTYPE index by binary_integer;
    L_off_invoice_item_tbl off_invoice_item_tbl;
BEGIN
   open C_OFF_INVOICE_ITEM;
   fetch C_OFF_INVOICE_ITEM BULK COLLECT into L_off_invoice_item_tbl;
   close C_OFF_INVOICE_ITEM;

   if L_off_invoice_item_tbl.count is not null and L_off_invoice_item_tbl.count >0 then
      for i in L_off_invoice_item_tbl.FIRST..L_off_invoice_item_tbl.LAST loop
         for j in O_disc_item_tbl.FIRST..O_disc_item_tbl.LAST loop
            if O_disc_item_tbl(j).item =L_off_invoice_item_tbl(i).item and
               O_disc_item_tbl(j).pack_no =L_off_invoice_item_tbl(i).pack_no and
               O_disc_item_tbl(j).deal_id =L_off_invoice_item_tbl(i).deal_id and
               O_disc_item_tbl(j).deal_detail_id =L_off_invoice_item_tbl(i).deal_detail_id then

               if O_disc_item_tbl(j).location =L_off_invoice_item_tbl(i).location then
                  L_unit_cost_init         := O_disc_item_tbl(j).unit_cost;
                  L_qty_ordered            := O_disc_item_tbl(j).qty_ordered; /* loc_qty */
                  L_buy_qty                := O_disc_item_tbl(j).qty_thresh_buy_qty;
                  L_get_qty                := O_disc_item_tbl(j).qty_thresh_get_qty;
                  L_free_item_unit_cost    := O_disc_item_tbl(j).free_item_unit_cost;
                  L_deal_class             := O_disc_item_tbl(j).deal_class;
                  L_buy_item               := O_disc_item_tbl(j).qty_thresh_buy_item;
                  L_get_item               := O_disc_item_tbl(j).qty_thresh_get_item;
                  L_recur_ind              := O_disc_item_tbl(j).qty_thresh_recur_ind;
                  L_qty_thresh_get_type    := O_disc_item_tbl(j).qty_thresh_get_type;
                  L_qty_thresh_get_value   := O_disc_item_tbl(j).qty_thresh_get_value;
                  L_get_free_discount      := O_disc_item_tbl(j).get_free_discount;
                  L_done_with_items        := 1;
                  L_total_qty_ordered      := L_total_qty_ordered+O_disc_item_tbl(j).qty_ordered;/* deal_qty */
               else
                  L_total_qty_ordered      := L_total_qty_ordered+O_disc_item_tbl(j).qty_ordered;/* deal_qty */
               end if;
            elsif L_done_with_items = 1 then
               EXIT;
            end if;
         end loop;

         L_done_with_items := 0;

         if DEAL_ORD_LIB_SQL.CALC_DISCOUNT(O_error_message,
                                           L_off_invoice_item_tbl(i).item,
                                           L_off_invoice_item_tbl(i).pack_no,
                                           L_off_invoice_item_tbl(i).location,
                                           L_off_invoice_item_tbl(i).discount_value,
                                           L_off_invoice_item_tbl(i).discount_type,
                                           L_off_invoice_item_tbl(i).deal_id,
                                           L_off_invoice_item_tbl(i).deal_detail_id,
                                           L_off_invoice_item_tbl(i).paid_ind,
                                           L_deal_class,
                                           L_unit_cost_init,
                                           L_qty_ordered,
                                           L_total_qty_ordered,
                                           L_buy_item,
                                           L_buy_qty,
                                           L_get_item,
                                           L_get_qty,
                                           L_free_item_unit_cost,
                                           L_recur_ind,
                                           O_disc_item_tbl,
                                           I_disc_item_tbl_cnt,
                                           L_qty_thresh_get_type,
                                           L_qty_thresh_get_value,
                                           L_get_free_discount,
                                           L_get_list_array_tbl,--out
                                           I_discount_build_tbl,
                                           L_cut_off_costs_tbl,--out
                                           L_no_deal_applied_ind) = FALSE then
            return FALSE;
         end if;
         L_total_qty_ordered := 0;
      end loop;
   end if;

   if DEAL_ORD_LIB_SQL.APPLY_CUT_OFF_COSTS(O_error_message,
                                           L_get_list_array_tbl,
                                           L_cut_off_costs_tbl) = FALSE then
      return FALSE;
   end if;

   for i in 1..L_get_list_array_tbl.count loop
      if DEAL_ORD_LIB_SQL.CALCULATE_AVERAGE_UNIT_COST(O_error_message,
                                                      L_get_list_array_tbl(i).item,
                                                      L_get_list_array_tbl(i).location,
                                                      L_get_list_array_tbl,
                                                      L_unit_cost)= FALSE then
         return FALSE;
      end if;

      /* We need to update ioa_disct_bld so that the txn discount is applied to the discounted unit cost */
      for j in 1..I_discount_build_tbl.count loop
         L_temp_value := j;
         if I_discount_build_tbl(j).order_no = LP_order_no and
            I_discount_build_tbl(j).item = L_get_list_array_tbl(i).item and
            I_discount_build_tbl(j).location = L_get_list_array_tbl(i).location then

            I_discount_build_tbl(j).unit_cost := L_unit_cost;
            L_origin_country_id := I_discount_build_tbl(j).origin_country_id;
            EXIT;
         end if;
      end loop;

      if DEAL_ORD_LIB_SQL.CALCULATE_ORDLOC_UNIT_COST(O_error_message,
                                                     L_get_list_array_tbl(i).item,
                                                     L_get_list_array_tbl(i).location,
                                                     L_get_list_array_tbl,
                                                     L_unit_cost)= FALSE then
         return FALSE;
      end if;

      if DEAL_ORD_LIB_SQL.UPDATE_ORDLOC(O_error_message,
                                        L_get_list_array_tbl(i).item,
                                        L_get_list_array_tbl(i).location,
                                        I_discount_build_tbl(L_temp_value).loc_type,
                                        L_unit_cost)= FALSE then
         return FALSE;
      end if;
      if LP_elc_ind = 'Y' then
         if DEAL_ORD_LIB_SQL.APPLY_ELC(O_error_message,
                                       L_get_list_array_tbl(i).item,
                                       NULL,/* buyer pack functionality removed for now */
                                       L_get_list_array_tbl(i).location,
                                       I_import_order_ind,
                                       L_origin_country_id)= FALSE then
            return FALSE;
         end if;
      end if;

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALCULATE_COST;
---------------------------------------------------------------------------------------------------------------
-- Function: CALC_DISCOUNT
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION CALC_DISCOUNT(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item                   IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                       I_pack_no                IN       ORDLOC_DISCOUNT.PACK_NO%TYPE,
                       I_location               IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                       I_discount_value         IN       ORDLOC_DISCOUNT.DISCOUNT_VALUE%TYPE,
                       I_discount_type          IN       ORDLOC_DISCOUNT.DISCOUNT_TYPE%TYPE,
                       I_deal_id                IN OUT   DEAL_DETAIL.DEAL_ID%TYPE,
                       I_deal_detail_id         IN OUT   DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                       I_paid_ind               IN OUT   VARCHAR2,
                       I_deal_class             IN       DEAL_DETAIL.DEAL_CLASS%TYPE,
                       I_unit_cost_init         IN       ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                       I_qty_ordered            IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                       I_total_qty_ordered      IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                       I_buy_item               IN       DEAL_DETAIL.QTY_THRESH_BUY_ITEM%TYPE,
                       I_buy_qty                IN       DEAL_DETAIL.QTY_THRESH_BUY_QTY%TYPE,
                       I_get_item               IN       DEAL_DETAIL.QTY_THRESH_GET_ITEM%TYPE,
                       I_get_qty                IN       DEAL_DETAIL.QTY_THRESH_GET_QTY%TYPE,
                       I_free_item_unit_cost    IN       DEAL_DETAIL.QTY_THRESH_FREE_ITEM_UNIT_COST%TYPE,
                       I_recur_ind              IN       DEAL_DETAIL.QTY_THRESH_RECUR_IND%TYPE,
                       I_disc_item_tbl          IN OUT   DISC_ITEM_TBL,
                       I_disc_item_tbl_cnt      IN OUT   NUMBER,
                       I_qty_thresh_get_type    IN       DEAL_DETAIL.QTY_THRESH_GET_TYPE%TYPE,
                       I_qty_thresh_get_value   IN       DEAL_DETAIL.QTY_THRESH_GET_VALUE%TYPE,
                       I_get_free_discount      IN       DEAL_DETAIL.GET_FREE_DISCOUNT%TYPE,
                       I_get_list_array_tbl     IN OUT   GET_LIST_ARRAY_TBL,
                       I_discount_build_tbl     IN OUT   DISCOUNT_BUILD_TBL,
                       I_cut_off_costs_tbl      IN OUT   CUT_OFF_COSTS_TBL,
                       I_no_deal_applied_ind    IN OUT   VARCHAR2)
RETURN BOOLEAN is

   L_program                     VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.CALC_DISCOUNT';
   L_pror_unit_cost              ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_invc_unit_cost              ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_used_as_buy_item_ind        NUMBER := 0;
   L_prorated_deal_qty           ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_prorated_loc_qty            ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_no_deal_applied_ind         VARCHAR2(1) := 'N';
   L_unit_discount               ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_inverter                    NUMBER    := -1;
   L_which_list                  NUMBER    := 0;
   L_could_not_find_item_ind     VARCHAR2(1) := 'N';
   L_buy_unit_cost_before_deal   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_buy_unit_cost_after_deal    ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_buy_plus_get_qty            DEAL_DETAIL.QTY_THRESH_GET_QTY%TYPE := nvl(I_buy_qty + I_get_qty,0);
   L_additional_get_qty          DEAL_DETAIL.QTY_THRESH_GET_QTY%TYPE := 0;
   L_deal_get_qty                ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_loc_get_qty                 ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_get_item_unit_cost_init     ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_get_invc_unit_cost          ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_total_discount              ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_get_item_proportion         NUMBER    := 0;
   L_buy_item_proportion         NUMBER    := 0;
   L_buy_unit_discount           ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_get_unit_discount           ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_get_pror_unit_cost          ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_dummy                       NUMBER    := 0;
   L_yes_ind                     NUMBER    := 1;
   L_no_ind                      NUMBER    := 0;
   L_negative_val                NUMBER    := -1;
   L_get_item_qty_ordered        ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE :=0;
   L_total_get_qty_ordered       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE:=0;
   L_pror_get_unit_cost          ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_invc_get_unit_cost          ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_prorated_deal_get_qty       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_prorated_loc_get_qty        ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_get_unit_cost_before_deal   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_get_unit_cost_after_deal    ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_buy_unit_calc_cost          ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_get_unit_calc_cost          ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE := 0;
   L_pro_deal_get_qty            ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_pro_loc_get_qty             ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_pro_deal_get_get_qty        ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_pro_loc_get_get_qty         ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
BEGIN
   /* If not a buy-get deal, our task is simple... */
   if I_discount_type != 'Q' then
      /* populate/get available qty and its cost with/for current item */
      if DEAL_ORD_LIB_SQL.GET_PRORATED_CORE_INFO(O_error_message,
                                                 I_item,
                                                 I_location,
                                                 I_total_qty_ordered,
                                                 L_pror_unit_cost,
                                                 L_invc_unit_cost,
                                                 L_used_as_buy_item_ind,
                                                 L_prorated_deal_qty,
                                                 L_prorated_loc_qty,
                                                 I_deal_id,
                                                 I_deal_detail_id,
                                                 I_paid_ind,
                                                 I_disc_item_tbl,
                                                 I_disc_item_tbl_cnt,
                                                 I_get_list_array_tbl,
                                                 L_no_deal_applied_ind)= FALSE then
         return FALSE;
      end if;
      if I_discount_type = 'P' then /* % type discount */
         if I_deal_class = 'CU' then
            L_unit_discount := I_unit_cost_init * I_discount_value / 100;
         else
            L_unit_discount := L_invc_unit_cost * I_discount_value / 100;
         end if;
      elsif I_discount_type = 'A' then /* amount type discount */
         L_unit_discount := I_discount_value;
      elsif I_discount_type = 'F' then /* Fixed amount type discount */
         L_unit_discount := I_unit_cost_init - I_discount_value;
      end if;

      /* do not calculate any discount when the total ord.qty is less than the lower threshold limit qty. */
      if I_discount_type in ('P','A') and
         (LP_lower_limit >0 and LP_deal_threshold_value < LP_lower_limit) then
            L_unit_discount := 0;
      end if;

      if L_unit_discount <= 0 then
        LP_no_deal_applied_ind := 'Y';
      end if;
      /* assign the unit discount for the amount/percentage discount type if the total ordered qty more than
         the upper threshold limit. **/
      if I_discount_type in ('P','A') and
         (LP_upper_limit >0 and LP_deal_threshold_value > LP_upper_limit) then
            L_unit_discount := L_unit_discount *(LP_upper_limit/LP_deal_threshold_value);
      end if;

      /* update invc unit cost */
      L_invc_unit_cost := L_invc_unit_cost - L_unit_discount;
      /* if the invc unit cost turned negative, cut off the invc unit cost to 0 and */
      /* update the unit discount to only hold the discount that was actually applied */
      /* to the invc unit cost */

      if L_invc_unit_cost < 0 then
         L_unit_discount := L_unit_discount + L_invc_unit_cost;
         L_invc_unit_cost := 0;
      end if;

      /* update prorated unit cost */
      L_pror_unit_cost := L_pror_unit_cost - L_unit_discount;
      /* Here comes the fun part. The discount that was adjusted to be a discount that at most dropped the invc unit cost to 0, managed to drop the prorated */
      /* unit cost below 0. This is possible because our core node for this item/loc took on some of the buy get discounts of the get item that the invc unit */
      /* cost did not. Therefore to keep the prorated and invc cost in line, we need to push off the remainder of the prorated cost's discount to one of this */
      /* item's get item prorated unit cost. We will do this later, for now simply book keep the fact that there was outstanding discount here that will later */
      /* be added to this item's get items. Needless to say if the prorated cost dipped below 0 while the invc unit cost did not and there have been no buy get */
      /* deals applied to this item where this item was the buy item, we have a fatal error. */
      if L_pror_unit_cost < 0 then
         if DEAL_ORD_LIB_SQL.INSERT_CUT_OFF_COST(O_error_message,
                                                 I_cut_off_costs_tbl,
                                                 I_item,
                                                 I_location,
                                                 ((L_inverter*L_pror_unit_cost)*L_prorated_loc_qty),
                                                 L_could_not_find_item_ind)= FALSE then
            return FALSE;
         end if;

         if L_could_not_find_item_ind = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('CANNOT_FIND_MATCH',
                                                  'item: ' || I_item || ', location: ' || I_location || ' in I_cut_off_costs_tbl',
                                                  L_program||'and INSERT_CUT_OFF_COST',
                                                  NULL);
            return FALSE;
         end if;

         L_unit_discount := L_unit_discount+L_pror_unit_cost;
         L_pror_unit_cost := 0;
      end if;

      if DEAL_ORD_LIB_SQL.CALCULATE_AVERAGE_UNIT_COST(O_error_message,
                                                      I_item,
                                                      I_location,
                                                      I_get_list_array_tbl,
                                                      L_buy_unit_cost_before_deal)= FALSE then
         return FALSE;
      end if;

      if DEAL_ORD_LIB_SQL.SET_PRORATED_CORE_INFO(O_error_message,
                                                 I_item,
                                                 I_location,
                                                 L_pror_unit_cost,
                                                 L_unit_discount,
                                                 L_invc_unit_cost,
                                                 L_prorated_deal_qty,
                                                 L_prorated_loc_qty,
                                                 L_used_as_buy_item_ind,
                                                 I_paid_ind,
                                                 I_get_list_array_tbl)= FALSE then
         return FALSE;
      end if;

      if DEAL_ORD_LIB_SQL.CALCULATE_AVERAGE_UNIT_COST(O_error_message,
                                                      I_item,
                                                      I_location,
                                                      I_get_list_array_tbl,
                                                      L_buy_unit_cost_after_deal)= FALSE then
         return FALSE;
      end if;

      L_buy_unit_calc_cost := L_buy_unit_cost_before_deal - L_buy_unit_cost_after_deal;

      if DEAL_ORD_LIB_SQL.UPDATE_ORDLOC_DISCOUNT(O_error_message,
                                                 I_item,
                                                 I_pack_no,
                                                 I_location,
                                                 I_deal_id,
                                                 I_deal_detail_id,
                                                 L_negative_val, /* leave discount value on ordloc_discount alone, we only set the unit cost discount */
                                                 L_buy_unit_calc_cost) = FALSE then
         return FALSE;
      end if;
   else /* We do have a buy-get deal */
      if I_item = I_buy_item then /* This item is the buy item */
         /* populate/get available qty and its cost with/for current item */
         if DEAL_ORD_LIB_SQL.GET_PRORATED_CORE_INFO(O_error_message,
                                                    I_item,
                                                    I_location,
                                                    I_total_qty_ordered,
                                                    L_pror_unit_cost,
                                                    L_invc_unit_cost,
                                                    L_used_as_buy_item_ind,
                                                    L_prorated_deal_qty,
                                                    L_prorated_loc_qty,
                                                    I_deal_id,
                                                    I_deal_detail_id,
                                                    I_paid_ind,
                                                    I_disc_item_tbl,
                                                    I_disc_item_tbl_cnt,
                                                    I_get_list_array_tbl,
                                                    L_no_deal_applied_ind)= FALSE then
            return FALSE;
         end if;

         if DEAL_ORD_LIB_SQL.CALCULATE_AVERAGE_UNIT_COST(O_error_message,
                                                         I_item,
                                                         I_location,
                                                         I_get_list_array_tbl,
                                                         L_buy_unit_cost_before_deal)= FALSE then
            return FALSE;
         end if;

         if I_buy_item = I_get_item then /* buy item is same as get item  */
            /* If this item has been used as a buy item, we can not use it as a get item because that */
            /* would drop its available qty which means we would have to recalculate all previous deal */
            /* item combinations where this was the buy item. Therefore an item can only be used once as a  */
            /* buy item. */
            if L_used_as_buy_item_ind = 1 then
               LP_no_deal_applied_ind := 'Y';
               return TRUE;
            end if;

            if L_prorated_deal_qty < I_buy_qty then
               LP_no_deal_applied_ind := 'Y';
               return TRUE;
            end if;
            if I_recur_ind = 'N' then
               L_deal_get_qty := L_prorated_deal_qty - I_buy_qty ;

               if L_deal_get_qty > I_get_qty then
                  L_deal_get_qty := I_get_qty;
               end if;
            else
               L_deal_get_qty := floor(L_prorated_deal_qty/L_buy_plus_get_qty) * I_get_qty;
               L_additional_get_qty := (L_prorated_deal_qty - (L_buy_plus_get_qty *  floor(L_prorated_deal_qty/L_buy_plus_get_qty))) - I_buy_qty;

               if L_additional_get_qty >0 then
                  L_deal_get_qty := L_deal_get_qty+L_additional_get_qty;
               end if;
            end if;

            if L_deal_get_qty <= 0 then
               LP_no_deal_applied_ind := 'Y';
               return TRUE;
            end if;

            /* we need the free qty this loc is getting */
            L_loc_get_qty := L_deal_get_qty * (I_qty_ordered/I_total_qty_ordered);
            L_get_item_unit_cost_init := I_unit_cost_init;

            if I_qty_thresh_get_type = 'X' then
               L_unit_discount := I_free_item_unit_cost;
            elsif I_qty_thresh_get_type = 'P' then
               L_unit_discount := (I_unit_cost_init * I_qty_thresh_get_value / 100);
            elsif I_qty_thresh_get_type = 'A' then
               if I_qty_thresh_get_value > I_unit_cost_init then
                  L_unit_discount := I_unit_cost_init;
               else
                  L_unit_discount := I_qty_thresh_get_value;
               end if;
            elsif I_qty_thresh_get_type = 'F' then
               if I_qty_thresh_get_value > I_unit_cost_init then
                  L_unit_discount := 0;
               else
                  L_unit_discount := I_unit_cost_init - I_qty_thresh_get_value;
               end if;
            end if;

            if L_unit_discount <= 0 then
               LP_no_deal_applied_ind := 'Y';
               return TRUE;
            end if;

            /************************************************/
            /* We need to do proration for buy-get deals... */
            /************************************************/
            /* update get item's invoice unit cost */
            L_get_invc_unit_cost := I_unit_cost_init - L_unit_discount;

            if L_get_invc_unit_cost <= 0 then
               L_unit_discount := L_unit_discount+L_get_invc_unit_cost;
               L_get_invc_unit_cost := 0;
            end if;

            /* Below is the total savings amount we get from the free items. */
            /* This amount needs to get divided between the buy and get items based on their total costs' ratio. */
            /* (init unit cost * loc qty) */
            L_total_discount := L_unit_discount * L_loc_get_qty;

            /* if the get item is free item, use the deal's free item unit cost in the proportion calculation */
            if I_qty_thresh_get_type = 'X' then
               L_get_item_proportion := ((L_loc_get_qty * I_free_item_unit_cost) /
                                         (((L_prorated_loc_qty - L_loc_get_qty) * I_unit_cost_init) +
                                          (L_loc_get_qty * I_free_item_unit_cost)));

            else
               L_get_item_proportion := ((L_loc_get_qty * L_get_item_unit_cost_init) /
                                         (((L_prorated_loc_qty - L_loc_get_qty) * I_unit_cost_init) +
                                          (L_loc_get_qty * L_get_item_unit_cost_init)));
            end if;

            L_buy_item_proportion := 1- L_get_item_proportion;

            L_buy_unit_discount := (L_total_discount * L_buy_item_proportion) / (L_prorated_loc_qty - L_loc_get_qty);
            L_pror_unit_cost := L_pror_unit_cost-L_buy_unit_discount;

            L_get_unit_discount := ((L_total_discount * L_get_item_proportion) / L_loc_get_qty);
            L_get_pror_unit_cost := I_unit_cost_init - L_get_unit_discount;

            /* Update buy item's prorated unit cost. */
            /* If it turns negative, pack savings into get item's prorated cost. */
            if L_pror_unit_cost < 0 then
               if DEAL_ORD_LIB_SQL.INS_BUY_GET_INTO_CUT_OFF_COST(O_error_message,
                                                                 I_cut_off_costs_tbl,
                                                                 I_item,
                                                                 I_item,
                                                                 I_location,
                                                                 I_deal_id,
                                                                 I_paid_ind,
                                                                 ((L_inverter*L_pror_unit_cost)*L_prorated_loc_qty))= FALSE then
                  return FALSE;
               end if;

               L_buy_unit_discount := L_buy_unit_discount + L_pror_unit_cost;
               L_pror_unit_cost := 0;
            else
              /* we just want to make sure we keep track of buy get deals in the cut off */
              /* cost array, even if there is no cut off cost occurring right now, */
              /* it may occur later, past this buy get deal */
               if DEAL_ORD_LIB_SQL.INS_BUY_GET_INTO_CUT_OFF_COST(O_error_message,
                                                                 I_cut_off_costs_tbl,
                                                                 I_item,
                                                                 I_item,
                                                                 I_location,
                                                                 I_deal_id,
                                                                 I_paid_ind,
                                                                 0)= FALSE then
                  return FALSE;
               end if;
            end if;

            if DEAL_ORD_LIB_SQL.STORE_NEW_DISCOUNT_DETAILS(O_error_message,
                                                           I_item,
                                                           I_location,
                                                           L_dummy,
                                                           L_get_pror_unit_cost,
                                                           L_get_invc_unit_cost,
                                                           L_loc_get_qty,
                                                           L_deal_get_qty,
                                                           I_deal_id,
                                                           I_deal_detail_id,
                                                           I_paid_ind,
                                                           I_get_list_array_tbl) = FALSE then
               return FALSE;
            end if;
            L_pro_deal_get_qty := L_prorated_deal_qty - L_deal_get_qty;
            L_pro_loc_get_qty  := L_prorated_loc_qty - L_loc_get_qty;
            if DEAL_ORD_LIB_SQL.SET_PRORATED_CORE_INFO(O_error_message,
                                                       I_item,
                                                       I_location,
                                                       L_pror_unit_cost,
                                                       L_buy_unit_discount,
                                                       L_inverter, /* -1 means don't update the invc cost */
                                                       L_pro_deal_get_qty ,
                                                       L_pro_loc_get_qty,
                                                       L_yes_ind,
                                                       I_paid_ind,
                                                       I_get_list_array_tbl)= FALSE then
               return FALSE;
            end if;

            if DEAL_ORD_LIB_SQL.CALCULATE_AVERAGE_UNIT_COST(O_error_message,
                                                            I_item,
                                                            I_location,
                                                            I_get_list_array_tbl,
                                                            L_buy_unit_cost_after_deal)= FALSE then
               return FALSE;
            end if;

            L_buy_unit_calc_cost := L_buy_unit_cost_before_deal - L_buy_unit_cost_after_deal;

            if DEAL_ORD_LIB_SQL.UPDATE_ORDLOC_DISCOUNT(O_error_message,
                                                       I_item,
                                                       I_pack_no,
                                                       I_location,
                                                       I_deal_id,
                                                       I_deal_detail_id,
                                                       L_loc_get_qty,
                                                       L_buy_unit_calc_cost) = FALSE then
               return FALSE;
            end if;
         else/* buy item != get item */
            if L_prorated_deal_qty < I_buy_qty then
               LP_no_deal_applied_ind := 'Y';
               return TRUE;
            end if;

            if I_recur_ind = 'N' then
               if (L_prorated_deal_qty - I_buy_qty) >= 0 then
                  L_deal_get_qty := I_get_qty;
               end if;
            else
               L_deal_get_qty := floor(L_prorated_deal_qty/I_buy_qty) * I_get_qty;
            end if;

            if L_deal_get_qty = 0  then
               LP_no_deal_applied_ind := 'Y';
               return TRUE;
            end if;

            /* find get item's init unit cost */
            if DEAL_ORD_LIB_SQL.GET_ITEM_UNIT_COST(O_error_message,
                                                   I_get_item,
                                                   I_location,
                                                   L_get_item_unit_cost_init,
                                                   I_discount_build_tbl,
                                                   L_no_deal_applied_ind) = FALSE then
               return FALSE;
            end if;

            if L_no_deal_applied_ind = 'Y' then
               I_no_deal_applied_ind := 'Y';
               return TRUE;
            end if;

            /* find total get qty for this deal (independent of loc), and this loc's qty */
            if DEAL_ORD_LIB_SQL.GET_TOTAL_QTY(O_error_message,
                                              I_get_item,
                                              I_location,
                                              L_get_item_qty_ordered,
                                              L_total_get_qty_ordered,
                                              I_deal_id,
                                              I_deal_detail_id,
                                              I_disc_item_tbl,
                                              I_disc_item_tbl_cnt,
                                              L_no_deal_applied_ind) = FALSE then
               return FALSE;
            end if;

            if L_no_deal_applied_ind = 'Y' then
               I_no_deal_applied_ind := 'Y';
               return TRUE;
            end if;

           /* populate/get available qty and its cost with/for get item */
            if DEAL_ORD_LIB_SQL.GET_PRORATED_CORE_INFO(O_error_message,
                                                       I_get_item,
                                                       I_location,
                                                       L_total_get_qty_ordered,
                                                       L_pror_get_unit_cost,
                                                       L_invc_get_unit_cost,
                                                       L_used_as_buy_item_ind,
                                                       L_prorated_deal_get_qty,
                                                       L_prorated_loc_get_qty,
                                                       I_deal_id,
                                                       I_deal_detail_id,
                                                       I_paid_ind,
                                                       I_disc_item_tbl,
                                                       I_disc_item_tbl_cnt,
                                                       I_get_list_array_tbl,
                                                       L_no_deal_applied_ind)= FALSE then
               return FALSE;
            end if;

            /* get item was not on order/deal/loc */
            if L_no_deal_applied_ind = 'Y' then
               I_no_deal_applied_ind := 'Y';
               return TRUE;
            end if;

            /* If this item has been used as a buy item, we can not use it as a get item because that */
            /* would drop its available qty which means we would have to recalculate all previous deal */
            /* item combinations where this was the buy item. Therefore an item can only be used once as a  */
            /* buy item. */
            if L_used_as_buy_item_ind = 1 then
               I_no_deal_applied_ind := 'Y';
               return TRUE;
            end if;

            if DEAL_ORD_LIB_SQL.CALCULATE_AVERAGE_UNIT_COST(O_error_message,
                                                            I_get_item,
                                                            I_location,
                                                            I_get_list_array_tbl,
                                                            L_get_unit_cost_before_deal)= FALSE then
               return FALSE;
            end if;

            /* we need the free qty this loc is getting */
            /* this qty is obtained using the get item's distribution ratio */

            L_loc_get_qty := L_deal_get_qty * (L_get_item_qty_ordered/L_total_get_qty_ordered);

            if I_qty_thresh_get_type = 'X' then
               L_unit_discount := I_free_item_unit_cost;
            elsif I_qty_thresh_get_type = 'P' then
               L_unit_discount := (L_get_item_unit_cost_init * I_qty_thresh_get_value / 100);
            elsif I_qty_thresh_get_type = 'A' then
               if I_qty_thresh_get_value > L_get_item_unit_cost_init then
                  L_unit_discount := L_get_item_unit_cost_init;
               else
                  L_unit_discount := I_qty_thresh_get_value;
               end if;
            elsif I_qty_thresh_get_type = 'F' then
               if I_qty_thresh_get_value > L_get_item_unit_cost_init then
                  L_unit_discount := 0;
               else
                  L_unit_discount := L_get_item_unit_cost_init - I_qty_thresh_get_value;
               end if;
            end if;

            if L_unit_discount <= 0 then
              LP_no_deal_applied_ind := 'Y';
            end if;

            /* make sure we do not take out more free qty than the get item's available ordered qty for this loc */
            if L_loc_get_qty > L_prorated_loc_get_qty then
               L_loc_get_qty :=  L_prorated_loc_get_qty;
            end if;

            if L_deal_get_qty > L_prorated_deal_get_qty then
               L_deal_get_qty :=  L_prorated_deal_get_qty;
            end if;
            /* update unit cost */
            L_get_invc_unit_cost := L_get_item_unit_cost_init - L_unit_discount;
            if L_get_invc_unit_cost < 0 then
               L_unit_discount := L_unit_discount + L_get_invc_unit_cost;
               L_get_invc_unit_cost := 0;
            end if;
            /* Below is the total savings amount we get from the free items. */
            /* This amount needs to get divided between the buy and get items based on their total cost. */
            /* (init unit cost * loc qty) */
            L_total_discount := L_unit_discount * L_loc_get_qty;

            /* if the get item is free item, use the deal's free item unit cost in the proportion calculation */
            if I_qty_thresh_get_type = 'X' then

               L_get_item_proportion := (I_get_free_discount/100);
               /*If the buy item unit discount is greater than the buy item unit cost the total discount is distributed proportionally*/
               L_buy_item_proportion := 1 - L_get_item_proportion;
               L_buy_unit_discount := (L_total_discount * L_buy_item_proportion) / L_prorated_loc_qty;
               L_pror_unit_cost := L_pror_unit_cost - L_buy_unit_discount;

               if L_pror_unit_cost <0 then
                  L_get_item_proportion := ((L_loc_get_qty * I_free_item_unit_cost) /
                                            ((L_prorated_loc_qty * I_unit_cost_init) +
                                             (L_loc_get_qty * I_free_item_unit_cost)));
               end if;
            else
               L_get_item_proportion := ((L_loc_get_qty * L_get_item_unit_cost_init) /
                                         ((L_prorated_loc_qty * I_unit_cost_init) +
                                          (L_loc_get_qty * L_get_item_unit_cost_init)));
            end if;

            L_buy_item_proportion := 1 - L_get_item_proportion;
            L_buy_unit_discount := (L_total_discount * L_buy_item_proportion) / L_prorated_loc_qty;
            L_pror_unit_cost := L_pror_unit_cost - L_buy_unit_discount;

            L_get_unit_discount := ((L_total_discount * L_get_item_proportion) / L_loc_get_qty);
            L_get_pror_unit_cost := L_get_item_unit_cost_init - L_get_unit_discount;

            /* Update buy item's prorated unit cost. */
            /* If it turns negative, pack savings into get item's prorated cost. */
            if L_pror_unit_cost < 0 then
               if DEAL_ORD_LIB_SQL.INS_BUY_GET_INTO_CUT_OFF_COST(O_error_message,
                                                                 I_cut_off_costs_tbl,
                                                                 I_item,
                                                                 I_get_item,
                                                                 I_location,
                                                                 I_deal_id,
                                                                 I_paid_ind,
                                                                 ((L_inverter*L_pror_unit_cost)*L_prorated_loc_qty))= FALSE then
                  return FALSE;
               end if;

               L_buy_unit_discount := L_buy_unit_discount + L_pror_unit_cost;
               L_pror_unit_cost := 0;
            else
              /* we just want to make sure we keep track of buy get deals in the cut off */
              /* cost array, even if there is no cut off cost occurring right now, */
              /* it may occur later, past this buy get deal */
               if DEAL_ORD_LIB_SQL.INS_BUY_GET_INTO_CUT_OFF_COST(O_error_message,
                                                                 I_cut_off_costs_tbl,
                                                                 I_item,
                                                                 I_get_item,
                                                                 I_location,
                                                                 I_deal_id,
                                                                 I_paid_ind,
                                                                 0)= FALSE then
                  return FALSE;
               end if;
            end if;

            if DEAL_ORD_LIB_SQL.STORE_NEW_DISCOUNT_DETAILS(O_error_message,
                                                           I_get_item,
                                                           I_location,
                                                           L_dummy,
                                                           L_get_pror_unit_cost,
                                                           L_get_invc_unit_cost,
                                                           L_loc_get_qty,
                                                           L_deal_get_qty,
                                                           I_deal_id,
                                                           I_deal_detail_id,
                                                           I_paid_ind,
                                                           I_get_list_array_tbl) = FALSE then
               return FALSE;
            end if;

            if DEAL_ORD_LIB_SQL.SET_PRORATED_CORE_INFO(O_error_message,
                                                       I_item,
                                                       I_location,
                                                       L_pror_unit_cost,
                                                       L_buy_unit_discount,
                                                       L_inverter, /* -1 means don't update the invc cost */
                                                       L_prorated_deal_qty,
                                                       L_prorated_loc_qty,
                                                       L_yes_ind,
                                                       I_paid_ind,
                                                       I_get_list_array_tbl)= FALSE then
               return FALSE;
            end if;

            L_pro_deal_get_get_qty := L_prorated_deal_get_qty - L_deal_get_qty;
            L_pro_loc_get_get_qty  := L_prorated_loc_get_qty - L_loc_get_qty;

            if DEAL_ORD_LIB_SQL.SET_PRORATED_CORE_INFO(O_error_message,
                                                       I_get_item,
                                                       I_location,
                                                       L_pror_get_unit_cost,
                                                       L_no_ind,--0
                                                       L_inverter, /* -1 means don't update the invc cost */
                                                       L_pro_deal_get_get_qty,
                                                       L_pro_loc_get_get_qty,
                                                       L_no_ind,
                                                       I_paid_ind,
                                                       I_get_list_array_tbl)= FALSE then
               return FALSE;
            end if;

            if DEAL_ORD_LIB_SQL.CALCULATE_AVERAGE_UNIT_COST(O_error_message,
                                                            I_get_item,
                                                            I_location,
                                                            I_get_list_array_tbl,
                                                            L_get_unit_cost_after_deal)= FALSE then
               return FALSE;
            end if;

            L_get_unit_calc_cost := L_get_unit_cost_before_deal - L_get_unit_cost_after_deal;

            if DEAL_ORD_LIB_SQL.UPDATE_ORDLOC_DISCOUNT(O_error_message,
                                                       I_get_item,
                                                       NULL,
                                                       I_location,
                                                       I_deal_id,
                                                       I_deal_detail_id,
                                                       L_loc_get_qty,
                                                       L_get_unit_calc_cost) = FALSE then
               return FALSE;
            end if;

            if DEAL_ORD_LIB_SQL.CALCULATE_AVERAGE_UNIT_COST(O_error_message,
                                                            I_item,
                                                            I_location,
                                                            I_get_list_array_tbl,
                                                            L_buy_unit_cost_after_deal)= FALSE then
               return FALSE;
            end if;

            L_buy_unit_calc_cost := L_buy_unit_cost_before_deal - L_buy_unit_cost_after_deal;

            if DEAL_ORD_LIB_SQL.UPDATE_ORDLOC_DISCOUNT(O_error_message,
                                                       I_item,
                                                       NULL,
                                                       I_location,
                                                       I_deal_id,
                                                       I_deal_detail_id,
                                                       L_no_ind,--0
                                                       L_buy_unit_calc_cost) = FALSE then
               return FALSE;
            end if;
         end if;--I_buy_item = I_get_item
      else
        /* This item is the get item */
        /* It was/will be processed when its buy item is being processed */
         NULL;
      end if;--I_item = I_buy_item

   end if;--discount_type='Q'

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_DISCOUNT;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_PRORATED_CORE_INFO
-- Purpose : populate/get available qty and its cost with/for current item
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_PRORATED_CORE_INFO(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                   IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                I_location               IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                I_total_qty_ordered      IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                I_pror_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                I_invc_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                I_used_as_buy_item_ind   IN OUT   NUMBER,
                                I_prorated_deal_qty      IN OUT   ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                I_prorated_loc_qty       IN OUT   ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                I_deal_id                IN OUT   DEAL_DETAIL.DEAL_ID%TYPE,
                                I_deal_detail_id         IN OUT   DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                I_paid_ind               IN OUT   VARCHAR2,
                                I_disc_item_tbl          IN OUT   DISC_ITEM_TBL,
                                I_disc_item_tbl_cnt      IN OUT   NUMBER,
                                I_get_list_array_tbl     IN OUT   GET_LIST_ARRAY_TBL,
                                I_no_deal_applied_ind    IN OUT   VARCHAR2 )
RETURN BOOLEAN is

   L_program                   VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.GET_PRORATED_CORE_INFO';
   L_which_list                NUMBER :=0;
   L_could_not_find_item_ind   VARCHAR2(1) := 'N';
   L_out_not_find_item_ind     VARCHAR2(1) := 'N';

BEGIN
   if DEAL_ORD_LIB_SQL.GET_LIST_INDEX(O_error_message,
                                      I_get_list_array_tbl,
                                      I_item,
                                      I_location,
                                      L_which_list,
                                      L_could_not_find_item_ind) = FALSE then
      return FALSE;
   end if;

   /* if (ll_which_list == COULD_NOT_FIND_ITEM) then the item we were were looking for has not yet been processed.  Add original order info */
   if L_could_not_find_item_ind = 'Y' then
      if DEAL_ORD_LIB_SQL.INSERT_NEW_PRORATED_INFO(O_error_message,
                                                   I_item,
                                                   I_location,
                                                   I_total_qty_ordered,
                                                   I_deal_id,
                                                   I_deal_detail_id,
                                                   I_paid_ind,
                                                   L_which_list,
                                                   L_out_not_find_item_ind,
                                                   I_disc_item_tbl,
                                                   I_disc_item_tbl_cnt,
                                                   I_get_list_array_tbl) = FALSE then
         return FALSE;
      end if;

      if L_out_not_find_item_ind = 'Y' then
         I_no_deal_applied_ind := 'Y';
         return TRUE;
      end if;

   end if;

   I_pror_unit_cost        := I_get_list_array_tbl(L_which_list).get_list_vals(1).pror_unit_cost;
   I_invc_unit_cost        := I_get_list_array_tbl(L_which_list).get_list_vals(1).invc_unit_cost;
   I_prorated_deal_qty     := I_get_list_array_tbl(L_which_list).get_list_vals(1).deal_qty;
   I_prorated_loc_qty      := I_get_list_array_tbl(L_which_list).get_list_vals(1).loc_qty;
   I_used_as_buy_item_ind  := I_get_list_array_tbl(L_which_list).used_as_buy_item_ind;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PRORATED_CORE_INFO;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_LIST_INDEX
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_LIST_INDEX(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_get_list_array_tbl        IN OUT   GET_LIST_ARRAY_TBL,
                        I_item                      IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                        I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                        I_which_list                IN OUT   NUMBER,
                        I_could_not_find_item_ind   IN OUT   VARCHAR2)
RETURN BOOLEAN is

   L_program                VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.GET_LIST_INDEX';

BEGIN
   if I_get_list_array_tbl.count is not null and I_get_list_array_tbl.count >0 then
      for i in I_get_list_array_tbl.first..I_get_list_array_tbl.last loop
         if I_get_list_array_tbl(i).item = I_item and
            I_get_list_array_tbl(i).location = I_location then
            I_which_list := i;
            return TRUE;
         end if;
      end loop;
   end if;

   I_could_not_find_item_ind := 'Y';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_LIST_INDEX;
---------------------------------------------------------------------------------------------------------------
-- Function: INSERT_NEW_PRORATED_INFO
-- Purpose : item we were were looking for has not yet been processed.  Add original order info.
---------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_NEW_PRORATED_INFO(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_item                      IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                  I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                  I_total_qty_ordered         IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                  I_deal_id                   IN OUT   DEAL_DETAIL.DEAL_ID%TYPE,
                                  I_deal_detail_id            IN OUT   DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                  I_paid_ind                  IN OUT   VARCHAR2,
                                  I_which_list                IN OUT   NUMBER,
                                  I_could_not_find_item_ind   IN OUT   VARCHAR2,
                                  I_disc_item_tbl             IN OUT   DISC_ITEM_TBL,
                                  I_disc_item_tbl_cnt         IN OUT   NUMBER,
                                  I_get_list_array_tbl        IN OUT   GET_LIST_ARRAY_TBL)
RETURN BOOLEAN is

   L_program   VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.INSERT_NEW_PRORATED_INFO';

BEGIN
   for i in 1..I_disc_item_tbl_cnt LOOP
      if I_disc_item_tbl(i).item = I_item and
         I_disc_item_tbl(i).location = I_location and
         I_disc_item_tbl(i).deal_id = I_deal_id and
         I_disc_item_tbl(i).deal_detail_id = I_deal_detail_id then
            /* cumulate total qty_ordered including item and component item */
            if DEAL_ORD_LIB_SQL.STORE_NEW_DISCOUNT_DETAILS(O_error_message,
                                                           I_item,
                                                           I_location,
                                                           I_which_list,
                                                           I_disc_item_tbl(i).unit_cost,
                                                           I_disc_item_tbl(i).unit_cost,
                                                           I_disc_item_tbl(i).qty_ordered,
                                                           I_total_qty_ordered,
                                                           I_deal_id,
                                                           I_deal_detail_id,
                                                           I_paid_ind,
                                                           I_get_list_array_tbl) = FALSE then
               return FALSE;
            end if;
         return TRUE;
      end if;
   end LOOP;

   /* if reached this point, then item is not on order */
   I_could_not_find_item_ind := 'Y';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_NEW_PRORATED_INFO;
---------------------------------------------------------------------------------------------------------------
-- Function: STORE_NEW_DISCOUNT_DETAILS
-- Purpose : cumulate total qty_ordered including item and component item
---------------------------------------------------------------------------------------------------------------
FUNCTION STORE_NEW_DISCOUNT_DETAILS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item                   IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                    I_location               IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                    I_which_list             IN OUT   NUMBER,
                                    I_pror_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                    I_invc_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                    I_qty_ordered            IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                    I_total_qty_ordered      IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                    I_deal_id                IN       DEAL_DETAIL.DEAL_ID%TYPE,
                                    I_deal_detail_id         IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                    I_paid_ind               IN       VARCHAR2,
                                    I_get_list_array_tbl     IN OUT   GET_LIST_ARRAY_TBL)
RETURN BOOLEAN is

   L_program                   VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.STORE_NEW_DISCOUNT_DETAILS';
   L_which_list                NUMBER :=0;
   L_index                     NUMBER :=0;
   L_could_not_find_item_ind   VARCHAR2(1) := 'N';
BEGIN
   if DEAL_ORD_LIB_SQL.GET_LIST_INDEX(O_error_message,
                                      I_get_list_array_tbl,
                                      I_item,
                                      I_location,
                                      L_which_list,
                                      L_could_not_find_item_ind) = FALSE then
      return FALSE;
   end if;

   if L_could_not_find_item_ind = 'Y'  then
      L_which_list := I_get_list_array_tbl.count +1;
   end if;
        
   I_get_list_array_tbl(L_which_list).item := I_item;
   I_get_list_array_tbl(L_which_list).location := I_location;

   L_index := I_get_list_array_tbl(L_which_list).get_list_vals.count + 1;

   I_get_list_array_tbl(L_which_list).get_list_vals(L_index).deal_id        := I_deal_id;
   I_get_list_array_tbl(L_which_list).get_list_vals(L_index).deal_detail_id := I_deal_detail_id;
   I_get_list_array_tbl(L_which_list).get_list_vals(L_index).pror_unit_cost := I_pror_unit_cost;
   I_get_list_array_tbl(L_which_list).get_list_vals(L_index).invc_unit_cost := I_invc_unit_cost;
   I_get_list_array_tbl(L_which_list).get_list_vals(L_index).deal_qty       := I_total_qty_ordered;
   I_get_list_array_tbl(L_which_list).get_list_vals(L_index).loc_qty        := I_qty_ordered;

   /* if below is true, this function was called as part of a new insert */
   /* in that case, we need to initialize the ordloc cost */
   if L_index =1 then
      I_get_list_array_tbl(L_which_list).get_list_vals(L_index).pror_ordloc_unit_cost  := I_pror_unit_cost;
   else /* this function is inserting a non-core node */
      if I_paid_ind ='Y' then
         I_get_list_array_tbl(L_which_list).get_list_vals(L_index).pror_ordloc_unit_cost := I_pror_unit_cost;
      end if;
   end if;

   I_which_list := L_which_list;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END STORE_NEW_DISCOUNT_DETAILS;
---------------------------------------------------------------------------------------------------------------
-- Function: INSERT_CUT_OFF_COST
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_CUT_OFF_COST(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cut_off_costs_tbl         IN OUT   CUT_OFF_COSTS_TBL,
                             I_buy_item                  IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                             I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                             I_total_cut_off_cost        IN       ORDLOC.UNIT_COST%TYPE,
                             I_could_not_find_item_ind   IN OUT   VARCHAR2)
RETURN BOOLEAN is

   L_program                   VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.INSERT_CUT_OFF_COST';
   L_which_list                NUMBER :=0;
   L_could_not_find_item_ind   VARCHAR2(1) := 'N';

BEGIN
   if DEAL_ORD_LIB_SQL.GET_CUT_OFF_COST_INDEX(O_error_message,
                                              I_cut_off_costs_tbl,
                                              I_buy_item,
                                              I_location,
                                              L_which_list,
                                              L_could_not_find_item_ind) = FALSE then
      return FALSE;
   end if;

   if L_could_not_find_item_ind = 'Y' then
      I_could_not_find_item_ind := 'Y';
      return TRUE;
   end if;

   I_cut_off_costs_tbl(L_which_list).total_cut_off_cost := I_cut_off_costs_tbl(L_which_list).total_cut_off_cost + I_total_cut_off_cost;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_CUT_OFF_COST;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_CUT_OFF_COST_INDEX
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_CUT_OFF_COST_INDEX(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_cut_off_costs_tbl         IN OUT   CUT_OFF_COSTS_TBL,
                                I_buy_item                  IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                I_which_list                IN OUT   NUMBER,
                                I_could_not_find_item_ind   IN OUT   VARCHAR2)
RETURN BOOLEAN is

   L_program                VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.GET_CUT_OFF_COST_INDEX';

BEGIN
   if I_cut_off_costs_tbl.count is not null and I_cut_off_costs_tbl.count >0 then
      for i in I_cut_off_costs_tbl.first..I_cut_off_costs_tbl.last loop
         if I_cut_off_costs_tbl(i).buy_item = I_buy_item and
            I_cut_off_costs_tbl(i).location = I_location then
            I_which_list := i;
            return TRUE;
         end if;
      end loop;
   end if;

   I_could_not_find_item_ind := 'Y';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_CUT_OFF_COST_INDEX;
---------------------------------------------------------------------------------------------------------------
-- Function: CALCULATE_AVERAGE_UNIT_COST
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION CALCULATE_AVERAGE_UNIT_COST(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item                      IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                     I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                     I_get_list_array_tbl        IN OUT   GET_LIST_ARRAY_TBL,
                                     I_average_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE)
RETURN BOOLEAN is

   L_program                   VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.CALCULATE_AVERAGE_UNIT_COST';
   L_which_list                NUMBER :=0;
   L_could_not_find_item_ind   VARCHAR2(1) := 'N';
   L_this_cost                 ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE :=0;
   L_this_qty                  ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE :=0;
   L_total_cost                ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE :=0;
   L_total_qty                 ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE :=0;
BEGIN
   if DEAL_ORD_LIB_SQL.GET_LIST_INDEX(O_error_message,
                                      I_get_list_array_tbl,
                                      I_item,
                                      I_location,
                                      L_which_list,
                                      L_could_not_find_item_ind) = FALSE then
      return FALSE;
   end if;

   if L_could_not_find_item_ind = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_FIND_MATCH',
                                            'item: ' || I_item || ', location: ' || I_location || ' in cut_off_costs_tbl',
                                            L_program||'and GET_LIST_INDEX',
                                            NULL);
      return FALSE;
   end if;

   for i in I_get_list_array_tbl(L_which_list).get_list_vals.first..I_get_list_array_tbl(L_which_list).get_list_vals.last loop
      L_this_cost   := I_get_list_array_tbl(L_which_list).get_list_vals(i).pror_unit_cost;
      L_this_qty    := I_get_list_array_tbl(L_which_list).get_list_vals(i).loc_qty;
      L_total_cost  := L_total_cost + (L_this_cost * L_this_qty);
      L_total_qty   := L_total_qty + L_this_qty;
   end loop;

   I_average_unit_cost := L_total_cost/L_total_qty;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALCULATE_AVERAGE_UNIT_COST;
---------------------------------------------------------------------------------------------------------------
-- Function: SET_PRORATED_CORE_INFO
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION SET_PRORATED_CORE_INFO(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                   IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                I_location               IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                I_pror_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                I_unit_discount          IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                I_invc_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                I_prorated_deal_qty      IN OUT   ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                I_prorated_loc_qty       IN OUT   ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                I_used_as_buy_item_ind   IN OUT   NUMBER,
                                I_paid_ind               IN OUT   VARCHAR2,
                                I_get_list_array_tbl     IN OUT   GET_LIST_ARRAY_TBL)
RETURN BOOLEAN is

   L_program                   VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.SET_PRORATED_CORE_INFO';
   L_which_list                NUMBER :=0;
   L_index                     NUMBER :=0;
   L_could_not_find_item_ind   VARCHAR2(1) := 'N';
BEGIN
   if DEAL_ORD_LIB_SQL.GET_LIST_INDEX(O_error_message,
                                      I_get_list_array_tbl,
                                      I_item,
                                      I_location,
                                      L_which_list,
                                      L_could_not_find_item_ind) = FALSE then
      return FALSE;
   end if;

   if L_could_not_find_item_ind = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_FIND_MATCH',
                                            'item: ' || I_item || ', location: ' || I_location || ' in I_cut_off_costs_tbl',
                                            L_program||'and GET_LIST_INDEX',
                                            NULL);
      return FALSE;
   end if;

   if I_invc_unit_cost != -1 then
      I_get_list_array_tbl(L_which_list).get_list_vals(1).invc_unit_cost := I_invc_unit_cost;
   end if;

   I_get_list_array_tbl(L_which_list).get_list_vals(1).pror_unit_cost := I_pror_unit_cost;
   I_get_list_array_tbl(L_which_list).get_list_vals(1).deal_qty       := I_prorated_deal_qty;
   I_get_list_array_tbl(L_which_list).get_list_vals(1).loc_qty        := I_prorated_loc_qty;

   /* do not reset this indicator unless it is to be set to 1 */
   if I_used_as_buy_item_ind =1 then
      I_get_list_array_tbl(L_which_list).used_as_buy_item_ind  := I_used_as_buy_item_ind;
   end if;

   if I_paid_ind ='Y' then
      I_get_list_array_tbl(L_which_list).get_list_vals(1).pror_ordloc_unit_cost
       := I_get_list_array_tbl(L_which_list).get_list_vals(1).pror_ordloc_unit_cost - I_unit_discount;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_PRORATED_CORE_INFO;
---------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_ORDLOC_DISCOUNT
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC_DISCOUNT(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                    IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                I_pack_no                 IN       ORDLOC_DISCOUNT.PACK_NO%TYPE,
                                I_location                IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                I_deal_id                 IN       DEAL_DETAIL.DEAL_ID%TYPE,
                                I_deal_detail_id          IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                I_discount                IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                I_discount_amt_per_unit   IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE)

RETURN BOOLEAN IS
   L_program       VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.UPDATE_ORDLOC_DISCOUNT';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(30)  := 'ORDLOC_DISCOUNT';
   L_key1          VARCHAR2(100) := LP_order_no;
   L_key2          VARCHAR2(100) := I_item;
   L_key3          VARCHAR2(100) := I_location;
   L_key4          VARCHAR2(100) := I_deal_id;

   L_rowid         ROWID                     := NULL;


   cursor C_LOCK_ORDLOC_DISCOUNT is
      select rowid
        from ordloc_discount
       where order_no           = LP_order_no
         and item               = I_item
         and location           = I_location
         and NVL(pack_no, -999) = NVL(I_pack_no, -999)
         and deal_id            = I_deal_id
         and deal_detail_id     = I_deal_detail_id
         for update nowait;

BEGIN
   open C_LOCK_ORDLOC_DISCOUNT;
   fetch C_LOCK_ORDLOC_DISCOUNT into L_rowid;
   close C_LOCK_ORDLOC_DISCOUNT;

   if L_rowid is NOT NULL then

      SQL_LIB.SET_MARK('UPDATE',NULL,'ordloc_discount','rowid:'||L_rowid);
      UPDATE ordloc_discount
         SET discount_value        = DECODE(I_discount, -1, discount_value, I_discount),
             discount_amt_per_unit = I_discount_amt_per_unit
       WHERE rowid = L_rowid;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Order_No: '||L_key1||'Item: '||L_key2,
                                             'Location: '||L_key3||'Deal_id: '||L_key4);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_ORDLOC_DISCOUNT;
---------------------------------------------------------------------------------------------------------------
-- Function: INS_BUY_GET_INTO_CUT_OFF_COST
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION INS_BUY_GET_INTO_CUT_OFF_COST(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_cut_off_costs_tbl         IN OUT   CUT_OFF_COSTS_TBL,
                                       I_buy_item                  IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                       I_get_item                  IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                       I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                       I_deal_id                   IN       DEAL_DETAIL.DEAL_ID%TYPE,
                                       I_paid_ind                  IN       VARCHAR2,
                                       I_total_cut_off_cost        IN       ORDLOC.UNIT_COST%TYPE)
RETURN BOOLEAN is

   L_program                   VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.INS_BUY_GET_INTO_CUT_OFF_COST';
   L_which_list                NUMBER :=0;
   L_could_not_find_item_ind   VARCHAR2(1) := 'N';
   L_count                     NUMBER :=0;
   L_new_core_node             NUMBER :=0;
   L_index                     NUMBER :=0;

BEGIN

      L_count := I_cut_off_costs_tbl.count;

      if DEAL_ORD_LIB_SQL.GET_CUT_OFF_COST_INDEX(O_error_message,
                                                 I_cut_off_costs_tbl,
                                                 I_buy_item,
                                                 I_location,
                                                 L_which_list,
                                                 L_could_not_find_item_ind) = FALSE then
         return FALSE;
      end if;

      if L_could_not_find_item_ind = 'Y' then
         L_new_core_node := 1;
         L_which_list := L_count;

         L_count := L_count+1;
      end if;

      if I_cut_off_costs_tbl.count is not null and I_cut_off_costs_tbl.count > 0 then
         L_index := I_cut_off_costs_tbl(L_which_list).cut_off_nodes.count;  
      end if;

      if L_new_core_node = 1 then
         I_cut_off_costs_tbl(L_which_list).buy_item := I_buy_item;
         I_cut_off_costs_tbl(L_which_list).location := I_location;
         I_cut_off_costs_tbl(L_which_list).total_cut_off_cost := I_total_cut_off_cost;
      else
         I_cut_off_costs_tbl(L_which_list).total_cut_off_cost
            := I_cut_off_costs_tbl(L_which_list).total_cut_off_cost + I_total_cut_off_cost;
      end if;

      I_cut_off_costs_tbl(L_which_list).cut_off_nodes(L_index).get_item := I_get_item;
      I_cut_off_costs_tbl(L_which_list).cut_off_nodes(L_index).deal_id := I_deal_id;
      I_cut_off_costs_tbl(L_which_list).cut_off_nodes(L_index).paid_ind := I_paid_ind;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INS_BUY_GET_INTO_CUT_OFF_COST;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_ITEM_UNIT_COST
-- Purpose : find get item's init unit cost
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_UNIT_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item                  IN       ORDLOC_DISCOUNT_BUILD.ITEM%TYPE,
                            I_location              IN       ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
                            I_unit_cost             IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                            I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL,
                            I_no_deal_applied_ind   IN OUT   VARCHAR2)
RETURN BOOLEAN is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.GET_ITEM_UNIT_COST';

BEGIN
   if I_discount_build_tbl.count > 0 then
      for i in I_discount_build_tbl.FIRST..I_discount_build_tbl.LAST loop
         if I_discount_build_tbl(i).order_no = LP_order_no
            and I_discount_build_tbl(i).item = I_item
            and I_discount_build_tbl(i).location = I_location then
            I_unit_cost := I_discount_build_tbl(i).unit_cost;
            return TRUE;
         end if;
      end loop;
   end if;

   I_no_deal_applied_ind := 'Y';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEM_UNIT_COST;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_TOTAL_QTY
-- Purpose : find total get qty for this deal (independent of loc), and this loc's qty
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_QTY(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item                      IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                       I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                       I_get_item_qty_ordered      IN OUT   ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                       I_total_get_qty_ordered     IN OUT   ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                       I_deal_id                   IN       DEAL_DETAIL.DEAL_ID%TYPE,
                       I_deal_detail_id            IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                       I_disc_item_tbl             IN OUT   DISC_ITEM_TBL,
                       I_disc_item_tbl_cnt         IN OUT   NUMBER,
                       I_no_deal_applied_ind       IN OUT   VARCHAR2)
RETURN BOOLEAN is

   L_program          VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.GET_TOTAL_QTY';
   L_total_qty        ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE := 0;
   L_done_with_item   NUMBER :=0;
   L_found            NUMBER :=0;

BEGIN
   for i in 1..I_disc_item_tbl.count LOOP
      if I_disc_item_tbl(i).item = I_item and
         I_disc_item_tbl(i).deal_id = I_deal_id and
         I_disc_item_tbl(i).deal_detail_id = I_deal_detail_id then
            if I_disc_item_tbl(i).location = I_location then
               L_total_qty := L_total_qty + I_disc_item_tbl(i).qty_ordered;
               I_get_item_qty_ordered := I_disc_item_tbl(i).qty_ordered;
               L_found := 1;
            else
               L_total_qty := L_total_qty + I_disc_item_tbl(i).qty_ordered;
               L_done_with_item := 1;
            end if;
      elsif L_done_with_item = 1 then
         EXIT;
      end if;
   end LOOP;

   I_total_get_qty_ordered := L_total_qty;

   if L_found !=1 then
      I_no_deal_applied_ind := 'Y';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_TOTAL_QTY;
---------------------------------------------------------------------------------------------------------------
-- Function: APPLY_CUT_OFF_COSTS
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION APPLY_CUT_OFF_COSTS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_get_list_array_tbl     IN OUT   GET_LIST_ARRAY_TBL,
                             I_cut_off_costs_tbl      IN OUT   CUT_OFF_COSTS_TBL)
RETURN BOOLEAN is

   L_program                 VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.APPLY_CUT_OFF_COSTS';
   L_gla_index               NUMBER :=0;
   L_total_cut_off_cost      ORDLOC.UNIT_COST%TYPE;
   L_total_amt               ORDLOC.UNIT_COST%TYPE;
   L_unit_cost_before_deal   ORDLOC.UNIT_COST%TYPE;
   L_unit_cost_after_deal    ORDLOC.UNIT_COST%TYPE;
   L_could_not_find_item_ind   VARCHAR2(1) := 'N';
BEGIN
   if I_cut_off_costs_tbl.count is not null and I_cut_off_costs_tbl.count >0 then
      FOR i IN I_cut_off_costs_tbl.FIRST..I_cut_off_costs_tbl.LAST LOOP
         L_total_cut_off_cost := I_cut_off_costs_tbl(i).total_cut_off_cost;

         if ABS(L_total_cut_off_cost) < LP_zero_difference then
            EXIT;
         end if;

         for j in 1..I_cut_off_costs_tbl(i).cut_off_nodes.count loop
            if DEAL_ORD_LIB_SQL.GET_LIST_INDEX(O_error_message,
                                               I_get_list_array_tbl,
                                               I_cut_off_costs_tbl(i).cut_off_nodes(j).get_item,
                                               I_cut_off_costs_tbl(i).location,
                                               L_gla_index,
                                               L_could_not_find_item_ind) = FALSE then
               return FALSE;
            end if;

            for k in 1..I_get_list_array_tbl(L_gla_index).get_list_vals.count loop
               if I_get_list_array_tbl(L_gla_index).get_list_vals(k).deal_id = I_cut_off_costs_tbl(i).cut_off_nodes(j).deal_id
                  and j = k then

                  L_total_amt := I_get_list_array_tbl(L_gla_index).get_list_vals(k).pror_unit_cost *
                                 I_get_list_array_tbl(L_gla_index).get_list_vals(k).loc_qty;

                  if DEAL_ORD_LIB_SQL.CALCULATE_AVERAGE_UNIT_COST(O_error_message,
                                                                  I_get_list_array_tbl(L_gla_index).item,
                                                                  I_get_list_array_tbl(L_gla_index).location,
                                                                  I_get_list_array_tbl,
                                                                  L_unit_cost_before_deal)= FALSE then
                     return FALSE;
                  end if;

                  L_total_amt := L_total_amt - L_total_cut_off_cost;

                  /* This below statement may be an issue. Due to the use of doubles, ld_total_amt may be below 0 but due to */
                  /* rounding doubles by the hardware, it may for all intents and purposes actually be 0 (i.e.: -0.00000000315). */
                  /* So far this was not an issue, but if problems arise from here, round the ls_total_amt to 4 decimals */
                  /* one step before below comparison. Hopefully that will solve the problem... */

                  if ABS(L_total_amt) < LP_zero_difference then
                     L_total_amt := 0;
                  end if;

                  if L_total_amt < 0 then
                     L_total_cut_off_cost := (L_total_amt * -1);
                     L_total_amt := 0;
                  else
                     L_total_cut_off_cost := 0;
                  end if;

                  I_get_list_array_tbl(L_gla_index).get_list_vals(k).pror_unit_cost :=
                     L_total_amt / I_get_list_array_tbl(L_gla_index).get_list_vals(k).loc_qty;

                  if I_cut_off_costs_tbl(i).cut_off_nodes(j).paid_ind = 'Y' then
                     I_get_list_array_tbl(L_gla_index).get_list_vals(k).pror_ordloc_unit_cost :=
                        I_get_list_array_tbl(L_gla_index).get_list_vals(k).pror_unit_cost;
                  end if;

                  if DEAL_ORD_LIB_SQL.CALCULATE_AVERAGE_UNIT_COST(O_error_message,
                                                                  I_get_list_array_tbl(L_gla_index).item,
                                                                  I_get_list_array_tbl(L_gla_index).location,
                                                                  I_get_list_array_tbl,
                                                                  L_unit_cost_after_deal)= FALSE then
                     return FALSE;
                  end if;

                  if DEAL_ORD_LIB_SQL.ADD_TO_ORDLOC_DISCOUNT(O_error_message,
                                                             I_get_list_array_tbl(L_gla_index).item,
                                                             NULL,
                                                             I_get_list_array_tbl(L_gla_index).location,
                                                             I_get_list_array_tbl(L_gla_index).get_list_vals(k).deal_id,
                                                             I_get_list_array_tbl(L_gla_index).get_list_vals(k).deal_detail_id,
                                                             (L_unit_cost_before_deal - L_unit_cost_after_deal))= FALSE then
                     return FALSE;
                  end if;
               end if;
            end LOOP; --k

            if L_total_cut_off_cost = 0 then
               EXIT;
            end if;

         end LOOP; --j

         if L_total_cut_off_cost != 0 then
            O_error_message :=       SQL_LIB.CREATE_MSG('NO_REAPPLY_CO_COST',
                                                        I_cut_off_costs_tbl(i).buy_item,
                                                        L_program,
                                                        NULL);
            return FALSE;
         end if;

      end LOOP; --i

   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END APPLY_CUT_OFF_COSTS;
---------------------------------------------------------------------------------------------------------------
-- Function: ADD_TO_ORDLOC_DISCOUNT
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION ADD_TO_ORDLOC_DISCOUNT(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                    IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                I_pack_no                 IN       ORDLOC_DISCOUNT.PACK_NO%TYPE,
                                I_location                IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                I_deal_id                 IN       DEAL_DETAIL.DEAL_ID%TYPE,
                                I_deal_detail_id          IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                I_discount_amt_per_unit   IN       ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.ADD_TO_ORDLOC_DISCOUNT';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(30)  := 'ORDLOC_DISCOUNT';
   L_key1          VARCHAR2(100) := LP_order_no;
   L_key2          VARCHAR2(100) := I_item;
   L_key3          VARCHAR2(100) := I_location;
   L_key4          VARCHAR2(100) := I_deal_id;

   L_rowid         ROWID                     := NULL;


   cursor C_LOCK_ORDLOC_DISCOUNT is
      select rowid
        from ordloc_discount
       where order_no           = LP_order_no
         and item               = I_item
         and location           = I_location
         and NVL(pack_no, -999) = NVL(I_pack_no, -999)
         and deal_id            = I_deal_id
         and deal_detail_id     = I_deal_detail_id
         for update nowait;

BEGIN

   open C_LOCK_ORDLOC_DISCOUNT;
   fetch C_LOCK_ORDLOC_DISCOUNT into L_rowid;
   close C_LOCK_ORDLOC_DISCOUNT;

   if L_rowid is NOT NULL then

      SQL_LIB.SET_MARK('UPDATE',NULL,'ordloc_discount','rowid:'||L_rowid);
      UPDATE ordloc_discount
         SET discount_amt_per_unit = (discount_amt_per_unit + I_discount_amt_per_unit)
       WHERE rowid = L_rowid;

   end if;

   return TRUE;

EXCEPTION
when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Order_No: '||L_key1||'Item: '||L_key2,
                                             'Location: '||L_key3||'Deal_id: '||L_key4);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_TO_ORDLOC_DISCOUNT;
---------------------------------------------------------------------------------------------------------------
-- Function: CALCULATE_ORDLOC_UNIT_COST
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION CALCULATE_ORDLOC_UNIT_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item                 IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                    I_location             IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                    I_get_list_array_tbl   IN OUT   GET_LIST_ARRAY_TBL,
                                    I_unit_cost            IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE)
RETURN BOOLEAN is

   L_program                   VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.CALCULATE_ORDLOC_UNIT_COST';
   L_which_list                NUMBER :=0;
   L_could_not_find_item_ind   VARCHAR2(1) := 'N';
   L_this_cost                 ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE :=0;
   L_this_qty                  ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE :=0;
   L_total_cost                ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE :=0;
   L_total_qty                 ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE :=0;
BEGIN
   if DEAL_ORD_LIB_SQL.GET_LIST_INDEX(O_error_message,
                                      I_get_list_array_tbl,
                                      I_item,
                                      I_location,
                                      L_which_list,
                                      L_could_not_find_item_ind) = FALSE then
      return FALSE;
   end if;

   if L_could_not_find_item_ind = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_FIND_MATCH',
                                            'item: ' || I_item || ', location: ' || I_location || ' in cut_off_costs_tbl',
                                            L_program||'and GET_LIST_INDEX',
                                            NULL);
      return FALSE;
   end if;

   for i in I_get_list_array_tbl(L_which_list).get_list_vals.first..I_get_list_array_tbl(L_which_list).get_list_vals.last loop
      if I_get_list_array_tbl(L_which_list).get_list_vals(i).pror_ordloc_unit_cost != LP_not_paid then
         L_this_cost   := I_get_list_array_tbl(L_which_list).get_list_vals(i).pror_ordloc_unit_cost;
         L_this_qty    := I_get_list_array_tbl(L_which_list).get_list_vals(i).loc_qty;
         L_total_cost  := L_total_cost + (L_this_cost * L_this_qty);
         L_total_qty   := L_total_qty + L_this_qty;
      end if;
   end loop;

   I_unit_cost := L_total_cost/L_total_qty;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALCULATE_ORDLOC_UNIT_COST;
---------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_ORDLOC
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item            IN       ORDLOC_DISCOUNT_BUILD.ITEM%TYPE,
                       I_location        IN       ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
                       I_loc_type        IN       ORDLOC_DISCOUNT_BUILD.LOC_TYPE%TYPE,
                       I_unit_cost       IN       ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.UPDATE_ORDLOC';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(30)  := 'ORDLOC';
   L_key1          VARCHAR2(100) := LP_order_no;
   L_key2          VARCHAR2(100) := I_item;
   L_key3          VARCHAR2(100) := I_location;

   L_rowid         ROWID                     := NULL;


   cursor C_LOCK_ORDLOC_DISCOUNT_S is
      select rowid
        from ordloc
       where order_no           = LP_order_no
         and item               = I_item
         and location           = I_location
         for update nowait;

   cursor C_LOCK_ORDLOC_DISCOUNT_W is
      select rowid
        from ordloc
       where order_no           = LP_order_no
         and item               = I_item
         and location in (SELECT ol.location
                              FROM wh,
                                   ordloc ol
                             WHERE wh.physical_wh = I_location
                               AND wh.wh          = ol.location
                               AND ol.order_no    = LP_order_no
                               AND ol.item        = I_item)
         for update nowait;

BEGIN
   if I_loc_type = 'S' then
      open C_LOCK_ORDLOC_DISCOUNT_S;
      fetch C_LOCK_ORDLOC_DISCOUNT_S into L_rowid;
      close C_LOCK_ORDLOC_DISCOUNT_S;

      if L_rowid is NOT NULL then
         SQL_LIB.SET_MARK('UPDATE',NULL,'ordloc','rowid:'||L_rowid);

         UPDATE ordloc
            SET unit_cost   = ROUND(I_unit_cost, LP_num_cost_decimals),
                cost_source = DECODE((unit_cost - I_unit_cost), 0, cost_source, 'DEAL')
          WHERE rowid = L_rowid;
      end if;
   else
      open C_LOCK_ORDLOC_DISCOUNT_W;
      fetch C_LOCK_ORDLOC_DISCOUNT_W into L_rowid;
      close C_LOCK_ORDLOC_DISCOUNT_W;

      if L_rowid is NOT NULL then
         SQL_LIB.SET_MARK('UPDATE',NULL,'ordloc','rowid:'||L_rowid);

         UPDATE ordloc
            SET unit_cost   = ROUND(I_unit_cost, LP_num_cost_decimals),
                cost_source = DECODE((unit_cost - I_unit_cost), 0, cost_source, 'DEAL')
          WHERE rowid = L_rowid;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Order_No: '||L_key1||'Item: '||L_key2,
                                             'Location: '||L_key3);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_ORDLOC;
---------------------------------------------------------------------------------------------------------------
-- Function: APPLY_ELC
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION APPLY_ELC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_item                IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                   I_pack_no             IN       ORDLOC_DISCOUNT.PACK_NO%TYPE,
                   I_location            IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                   I_import_order_ind    IN       ORDHEAD.IMPORT_ORDER_IND%TYPE,
                   I_origin_country_id   IN       ORDLOC_DISCOUNT_BUILD.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN is

   L_program      VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.APPLY_ELC';
   L_pwh          WH.PHYSICAL_WH%TYPE;
   L_loc_type     VARCHAR2(1) := NULL;
   L_location     ITEM_LOC.LOC%TYPE := NULL;

   cursor C_PWH IS
      SELECT physical_wh
       FROM wh
       WHERE wh = I_location;

   cursor C_ITEM_LOC is
      SELECT ol.location
        FROM wh,
             ordloc ol
       WHERE wh.physical_wh = L_pwh
         AND wh.wh          = ol.location
         AND ol.order_no    = LP_order_no
         AND ol.item        = I_item
         AND L_loc_type='W'
      UNION
      SELECT I_location FROM DUAL where L_loc_type='S';

   TYPE c_item_loc_tbl is table of C_ITEM_LOC%ROWTYPE index by binary_integer;
   L_c_item_loc_tbl c_item_loc_tbl;
BEGIN
   open C_PWH;
   fetch C_PWH into L_pwh;
   if C_PWH%NOTFOUND then
     L_loc_type := 'S';
   else
     L_loc_type := 'W';
   end if;
   CLOSE C_PWH;

   open C_ITEM_LOC;
   fetch C_ITEM_LOC BULK COLLECT into L_c_item_loc_tbl;
   close C_ITEM_LOC;

   if L_c_item_loc_tbl is null or L_c_item_loc_tbl.count = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('NO_REC_TO_PROCESS',
                                            'Order : '||LP_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   for i in L_c_item_loc_tbl.first..L_c_item_loc_tbl.last loop
      if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                'PE',
                                I_item,
                                LP_order_supplier,
                                NULL,
                                NULL,
                                LP_order_no,
                                NULL,
                                NULL,
                                NULL,
                                L_c_item_loc_tbl(i).location,
                                NULL,
                                NULL,
                                I_origin_country_id,
                                NULL,
                                NULL) = FALSE then
         return FALSE;
      end if;
      /* update the order/items assessments when this an import order */
      if I_import_order_ind = 'Y' then
         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PA',
                                   I_item,
                                   LP_order_supplier,
                                   NULL,
                                   NULL,
                                   LP_order_no,
                                   NULL,
                                   NULL,
                                   NULL,
                                   L_c_item_loc_tbl(i).location,
                                   NULL,
                                   NULL,
                                   I_origin_country_id,
                                   NULL,
                                   NULL) = FALSE then
            return FALSE;
         end if;
         /* update the order/items expenses again for dependencies on the */
         /* updated assessments                                           */
         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PE',
                                   I_item,
                                   LP_order_supplier,
                                   NULL,
                                   NULL,
                                   LP_order_no,
                                   NULL,
                                   NULL,
                                   NULL,
                                   L_c_item_loc_tbl(i).location,
                                   NULL,
                                   NULL,
                                   I_origin_country_id,
                                   NULL,
                                   NULL) = FALSE then
            return FALSE;
         end if;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END APPLY_ELC;
---------------------------------------------------------------------------------------------------------------
-- Function: PROCESS_TXN_DISCOUNT
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TXN_DISCOUNT(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_deal_tbl              IN OUT   DEAL_TBL,
                              I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL,
                              I_seq_no                IN OUT  ORDLOC_DISCOUNT.SEQ_NO%TYPE)
RETURN BINARY_INTEGER is

   L_program             VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.PROCESS_TXN_DISCOUNT';
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);

   L_import_order_ind    ORDHEAD.IMPORT_ORDER_IND%TYPE;
   L_total_order_cost    ORDLOC.UNIT_COST%TYPE := 0;
   L_discount_value      ORDLOC_DISCOUNT.DISCOUNT_VALUE%TYPE;
   L_item_discount       ORDLOC.UNIT_COST%TYPE;
   L_total_discount      ORDLOC.UNIT_COST%TYPE;
   L_application_order   ORDLOC_DISCOUNT.APPLICATION_ORDER%TYPE := NULL;
   L_paid_ind            VARCHAR2(1);
   L_item                ORDLOC.item%TYPE;
   L_location            ORDLOC.location%TYPE;
   L_table               VARCHAR2(30)  := 'ORDLOC';
   L_key1                VARCHAR2(100) := LP_order_no;
   L_key2                VARCHAR2(100) := NULL;
   L_key3                VARCHAR2(100) := NULL;
   L_rowid               ROWID         := NULL;
   L_return              BINARY_INTEGER;


   cursor C_LOCK_ORDLOC_DISCOUNT_S is
      select rowid
        from ordloc
       where order_no           = LP_order_no
         and item               = L_item
         and location           = L_location
         for update nowait;

   cursor C_LOCK_ORDLOC_DISCOUNT_W is
      select rowid
        from ordloc
       where order_no           = LP_order_no
         and item               = L_item
         and location in (SELECT ol.location
                              FROM wh,
                                   ordloc ol
                             WHERE wh.physical_wh = L_location
                               AND wh.wh          = ol.location
                               AND ol.order_no    = LP_order_no
                               AND ol.item        = L_item
                               AND wh.wh         != wh.physical_wh)
         for update nowait;

   cursor c_import_order is
      SELECT import_order_ind
        FROM ordhead
       WHERE order_no = LP_order_no;
BEGIN
   /* for each transaction-level discount */
   for i in I_deal_tbl.FIRST..I_deal_tbl.LAST loop
      if I_deal_tbl(i).txn_discount_ind = 'Y' then
         if L_total_order_cost = 0 then
            /* sum up the order cost for this order */
            for L_disc_rec in 1..I_discount_build_tbl.count loop
               if I_discount_build_tbl(L_disc_rec).order_no = LP_order_no then
                  /* PO specific deals can have Cumulative deal components and these must have the
                   * discount based on the undiscounted cost.
                   * All other transaction-level discounts are Cascading and are based on the
                   * already discounted amount*/
                   if I_deal_tbl(i).deal_class = 'CU' and I_deal_tbl(i).type = 3 then
                      L_total_order_cost := L_total_order_cost + (I_discount_build_tbl(L_disc_rec).unit_cost_init
                                           * I_discount_build_tbl(L_disc_rec).qty_ordered) ;
                   else
                      L_total_order_cost := L_total_order_cost + (I_discount_build_tbl(L_disc_rec).unit_cost
                                           * I_discount_build_tbl(L_disc_rec).qty_ordered) ;
                   end if;
               end if;
            end loop;
         end if;

         if L_discount_value = 0 then
         /* convert from order's currency to deal's currency */
            if LP_order_currency_code != I_deal_tbl(i).currency_code then
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_total_order_cost,--in
                                       LP_order_currency_code,--from
                                       I_deal_tbl(i).currency_code,--to
                                       L_total_order_cost,--out
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL) = FALSE then
                  return -1;
               end if;
            end if;

            /* find the discount value given the order cost */
             L_return := DEAL_ORD_LIB_SQL.GET_THRESHOLD_VALUE(O_error_message,
                                                              L_total_order_cost,
                                                              I_deal_tbl(i).deal_id,
                                                              I_deal_tbl(i).deal_detail_id,
                                                              'N',
                                                              L_discount_value);
             if L_return < 0 then
                return L_return;
             elsif L_return = 1 then
                continue;
             end if;

            if I_deal_tbl(i).threshold_value_type = 'A' and LP_order_currency_code != I_deal_tbl(i).currency_code then
               /* convert from deal's currency to order's currency */
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_discount_value,--in
                                       I_deal_tbl(i).currency_code,
                                       LP_order_currency_code,
                                       L_discount_value,--out
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL) = FALSE then
                  return -1;
               end if;
            end if;
         end if;

         /* calculate total discount */
         if I_deal_tbl(i).threshold_value_type = 'A' then
            L_total_discount := L_discount_value;
         elsif I_deal_tbl(i).threshold_value_type = 'P' then
            L_total_discount := L_discount_value * L_total_order_cost /100;
         end if;

         if L_total_discount > L_total_order_cost then
            L_total_discount := L_total_order_cost;
         end if;

        /* insert discount into ordhead_discount */
         INSERT INTO ordhead_discount
                    (order_no,
                     deal_id,
                     deal_detail_id,
                     total_discount_amt)
             VALUES(LP_order_no,
                    I_deal_tbl(i).deal_id,
                    I_deal_tbl(i).deal_detail_id,
                    L_total_discount);

         if I_deal_tbl(i).billing_type = 'OI' and I_deal_tbl(i).deal_appl_timing = 'O' then
            L_paid_ind := 'Y';
         elsif I_deal_tbl(i).billing_type = 'P' then
            L_paid_ind := 'N';
         end if;

         /* assign portion of order discount to each item */
         for L_disc_rec in 1..I_discount_build_tbl.count loop
            if I_discount_build_tbl(L_disc_rec).order_no = LP_order_no then
               if I_deal_tbl(i).threshold_value_type = 'A' then
                  if I_deal_tbl(i).deal_class = 'CU' and I_deal_tbl(i).type = 3 then
                     L_item_discount :=  ((L_total_discount *
                                          ((I_discount_build_tbl(L_disc_rec).unit_cost_init * I_discount_build_tbl(L_disc_rec).qty_ordered)
                                           / L_total_order_cost))
                                         / I_discount_build_tbl(L_disc_rec).qty_ordered);
                  else
                     L_item_discount := ((L_total_discount *
                                          ((I_discount_build_tbl(L_disc_rec).unit_cost * I_discount_build_tbl(L_disc_rec).qty_ordered)
                                           / L_total_order_cost))
                                         / I_discount_build_tbl(L_disc_rec).qty_ordered);
                  end if;
               elsif I_deal_tbl(i).threshold_value_type = 'P' then
                  if I_deal_tbl(i).deal_class = 'CU' and I_deal_tbl(i).type = 3 then
                     L_item_discount := I_discount_build_tbl(L_disc_rec).unit_cost_init * (L_discount_value /100);
                  else
                     L_item_discount := I_discount_build_tbl(L_disc_rec).unit_cost * (L_discount_value /100);
                  end if;
               end if;

               /*Subtract the discount from the unit cost to keep it up-to-date for subsequent transaction level deals */
               I_discount_build_tbl(L_disc_rec).unit_cost :=
                                  I_discount_build_tbl(L_disc_rec).unit_cost - L_item_discount;

               if DEAL_ORD_LIB_SQL.GET_APPLICATION_ORDER(O_error_message,
                                                         I_discount_build_tbl(L_disc_rec).item,
                                                         I_discount_build_tbl(L_disc_rec).location,
                                                         L_application_order)= FALSE then
                  return -1;
               end if;

               /* update ordloc_discount and ordloc with item discount
                  calculated above */
               /* ordloc discount holds physical whs! */
               INSERT INTO ordloc_discount
                          (order_no,
                           seq_no,
                           item,
                           location,
                           deal_id,
                           deal_detail_id,
                           discount_value,
                           discount_type,
                           discount_amt_per_unit,
                           application_order,
                           paid_ind,
                           last_calc_date)
                   VALUES(LP_order_no,
                          I_seq_no,
                          I_discount_build_tbl(L_disc_rec).item,
                          I_discount_build_tbl(L_disc_rec).location,
                          I_deal_tbl(i).deal_id,
                          I_deal_tbl(i).deal_detail_id,
                          L_discount_value,
                          I_deal_tbl(i).threshold_value_type,
                          L_item_discount,
                          L_application_order,
                          L_paid_ind,
                          LP_vdate);

               if L_paid_ind = 'Y' then
                  L_item     := I_discount_build_tbl(L_disc_rec).item;
                  L_location := I_discount_build_tbl(L_disc_rec).location;
                  L_key2     := L_item;
                  L_key3     := L_location;

                  if I_discount_build_tbl(L_disc_rec).loc_type = 'S' then
                     open C_LOCK_ORDLOC_DISCOUNT_S;
                     fetch C_LOCK_ORDLOC_DISCOUNT_S into L_rowid;
                     close C_LOCK_ORDLOC_DISCOUNT_S;

                     if L_rowid is NOT NULL then
                        SQL_LIB.SET_MARK('UPDATE',NULL,'ordloc','rowid:'||L_rowid);

                        UPDATE ordloc
                           SET unit_cost   = ROUND(unit_cost - L_item_discount, LP_num_cost_decimals),
                               cost_source = DECODE(L_item_discount, 0, cost_source, 'DEAL')
                         WHERE rowid = L_rowid;
                     end if;
                  else
                     open C_LOCK_ORDLOC_DISCOUNT_W;
                     fetch C_LOCK_ORDLOC_DISCOUNT_W into L_rowid;
                     close C_LOCK_ORDLOC_DISCOUNT_W;

                     if L_rowid is NOT NULL then
                        SQL_LIB.SET_MARK('UPDATE',NULL,'ordloc','rowid:'||L_rowid);

                        UPDATE ordloc
                           SET unit_cost   = ROUND(unit_cost - L_item_discount, LP_num_cost_decimals),
                               cost_source = DECODE(L_item_discount, 0, cost_source, 'DEAL')
                         WHERE rowid = L_rowid;
                     end if;
                  end if;

                  if LP_elc_ind = 'Y' then
                     L_import_order_ind := 'N';
                     open C_IMPORT_ORDER;
                     fetch C_IMPORT_ORDER into L_import_order_ind;
                     close C_IMPORT_ORDER;

                     if DEAL_ORD_LIB_SQL.APPLY_ELC(O_error_message,
                                                   I_discount_build_tbl(L_disc_rec).item,
                                                   NULL,
                                                   I_discount_build_tbl(L_disc_rec).location,
                                                   L_import_order_ind,
                                                   I_discount_build_tbl(L_disc_rec).origin_country_id)= FALSE then
                        return -1;
                     end if;
                  end if;
               end if;
               I_seq_no := I_seq_no + 1;

            end if;/* end if order_no matches */
         end loop; /* end for each discount */
         L_total_order_cost := 0;
         L_discount_value   := 0;

      end if;/* end if txn_discount_ind = Y */
   end LOOP;/* end for each deal */

   return 0;

EXCEPTION
when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Order_No: '||L_key1||'Item: '||L_key2,
                                             'Location: '||L_key3);
      return -1;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return -1;
END PROCESS_TXN_DISCOUNT;
---------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_OTB
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_OTB(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL)
RETURN BOOLEAN is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.UPDATE_OTB';
   L_cost_diff_prim       DISC_OTB_APPLY.ORIG_OTB%TYPE;
   L_dept                 ITEM_MASTER.DEPT%TYPE;
   L_class                ITEM_MASTER.CLASS%TYPE;
   L_subclass             ITEM_MASTER.SUBCLASS%TYPE;
   L_convert_flag         NUMBER(1):= 0;
   L_order_exchange_rate  ORDHEAD.EXCHANGE_RATE%TYPE := NULL;

BEGIN
   FOR i IN 1..I_discount_build_tbl.count LOOP
      /* cumulate all the cost_diff for the same item */
      L_cost_diff_prim := L_cost_diff_prim +(I_discount_build_tbl(i).new_otb - I_discount_build_tbl(i).orig_otb);

      /* for each item, update otb once */
      if (i = 1 OR (i > 1 and (I_discount_build_tbl(i-1).item != I_discount_build_tbl(i).item ))) then
         L_dept       := I_discount_build_tbl(i).dept;
         L_class      := I_discount_build_tbl(i).class;
         L_subclass   := I_discount_build_tbl(i).subclass;

         if LP_elc_ind = 'Y' and I_discount_build_tbl(i).otb_calc_type = 'C' then
            L_convert_flag := 0;
         else
            L_convert_flag := 1;
         end if;

         if LP_order_exchange_rate = 0 then
            L_order_exchange_rate := NULL;
         else
            L_order_exchange_rate := LP_order_exchange_rate;
         end if;

         if L_convert_flag = 1 then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_cost_diff_prim,     /* in: order currency */
                                    LP_order_currency_code,
                                    NULL,
                                    L_cost_diff_prim,     /* out: prim currency */
                                    NULL,
                                    NULL,
                                    NULL,
                                    L_order_exchange_rate,
                                    NULL) = FALSE then
               return FALSE;
            end if;
         end if;

         if OTB_SQL.ORD_RECEIVE(O_error_message,
                                0,
                                L_cost_diff_prim, /* prim currency */
                                LP_order_no,
                                L_dept,
                                L_class,
                                L_subclass,
                                0,
                                1) = FALSE then
            return FALSE;
         end if;

         /* reset cost_diff for next new item */
         L_cost_diff_prim := 0;
      end if;
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_OTB;

---------------------------------------------------------------------------------------------------------------
-- Function: INSERT_REV_ORDERS
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_REV_ORDERS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item_cost            IN OUT   ITEM_COST_TBL,
                           I_discount_build_tbl   IN OUT   DISCOUNT_BUILD_TBL)
RETURN BOOLEAN is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.INSERT_REV_ORDERS';
   L_dummy                NUMBER(1):= 0;
   L_cost_changed         NUMBER(1):= 0;
   L_lc_cost_changed      NUMBER(1):= 0;
   L_total_cost           ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE;
   L_total_qty_ordered    ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE;
   L_average_cost         ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE;
   L_lc_exists            NUMBER(1):= 0;
   j_index                NUMBER:= 1;
   L_sec_last_array       NUMBER:= 0;

   cursor C_CHECK_REV_ORDERS is
      select /*+ FIRST_ROWS */ 1
        from rev_orders
      where order_no = LP_order_no
        and rownum   = 1;

   cursor C_LC_EXISTS is
      select /*+ FIRST_ROWS */ 1
        from lc_detail
      where order_no = LP_order_no
        and rownum   = 1;
BEGIN
   open C_LC_EXISTS;
   fetch C_LC_EXISTS into L_dummy;
   close C_LC_EXISTS;

   if L_dummy = 1 then
      L_lc_exists := 1;
   end if;

   j_index := 1;

   if I_discount_build_tbl.count >0 then
      for i in I_discount_build_tbl.FIRST..I_discount_build_tbl.LAST loop
         L_total_cost         := L_total_cost + (I_discount_build_tbl(i).unit_cost * I_discount_build_tbl(i).qty_ordered);
         L_total_qty_ordered  := L_total_qty_ordered + (I_discount_build_tbl(i).qty_ordered);

         if i > 1 then
            /* if there's a new item and lc exits for this order, */
            /* call update_lc() to deal with lc tables             */
            if I_discount_build_tbl(i).item != I_discount_build_tbl(i-1).item and L_lc_exists =1 then
               L_average_cost := (L_total_cost / L_total_qty_ordered);
               if DEAL_ORD_LIB_SQL.UPDATE_LC(O_error_message,
                                             I_discount_build_tbl(i-1).item,
                                             I_item_cost(i-1).location,
                                             L_average_cost,
                                             L_lc_cost_changed)= FALSE then
                  return FALSE;
               end if;
               L_lc_cost_changed   := 0;
               L_total_cost        := 0;
               L_total_qty_ordered := 0;
            end if;
         end if;

         /* below situation occurs if a physical warehouse is present in a multichannel env */
         if I_item_cost.NEXT(j_index - 1) is not NULL and
            I_item_cost(i).location = I_item_cost(j_index).virtual_loc then
            /* check if the physical wh cost is different than component virtual wh cost */
            if I_discount_build_tbl(i).unit_cost != I_item_cost(j_index).old_unit_cost then
               L_cost_changed    := 1;
               L_lc_cost_changed := 1;
            end if;

            if(I_item_cost.NEXT(j_index) is not NULL) then
               j_index := j_index + 1;

               /* forward counter for item_cost array to skip to next location that is not part of the
                  discount build struct's physical wh  */

               WHILE I_item_cost.NEXT(j_index - 1) is not NULL
                   and I_discount_build_tbl(i).location =  I_item_cost(j_index).location
                   and I_discount_build_tbl(i).item = I_item_cost(j_index).item
                   and j_index < I_item_cost.count - 1 LOOP
                      j_index := j_index + 1;
               end loop;
            end if;

         end if;

         if I_discount_build_tbl(i).unit_cost != I_item_cost(i).old_unit_cost then
            L_cost_changed    := 1;
            L_lc_cost_changed := 1;
         end if;

         j_index := j_index + 1;
         L_sec_last_array := i-1;
      end loop;
   end if;


   /* if lc exists for this order, call update_lc() for the last item */
   if L_lc_exists = 1 then
      L_average_cost := (L_total_cost / L_total_qty_ordered);
      if DEAL_ORD_LIB_SQL.UPDATE_LC(O_error_message,
                                    I_discount_build_tbl(L_sec_last_array).item,
                                    I_item_cost(L_sec_last_array).location,
                                    L_average_cost,
                                    L_lc_cost_changed)= FALSE then
         return FALSE;
      end if;
   end if;

   if L_cost_changed = 1 then
      /* we have found a cost that changed */
      open C_CHECK_REV_ORDERS;
      fetch C_CHECK_REV_ORDERS into L_dummy;
      close C_CHECK_REV_ORDERS;

      if L_dummy = 0 then
         INSERT INTO rev_orders(order_no)
             VALUES (LP_order_no);
      end if;

      if L_lc_exists = 1 then
         if LC_SQL.ORDER_UPDATE_LC(O_error_message,
                                   LP_order_no)= FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_REV_ORDERS;
---------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_LC
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_LC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_item              IN       ORDLOC_DISCOUNT_BUILD.ITEM%TYPE,
                   I_location          IN       ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
                   I_average_cost      IN       ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                   I_lc_cost_changed   IN       NUMBER)
RETURN BOOLEAN IS
   L_program       VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.UPDATE_LC';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(30)  := 'ORD_LC_AMENDMENTS';
   L_key1          VARCHAR2(100) := LP_order_no;
   L_key2          VARCHAR2(100) := I_item;
   L_key3          VARCHAR2(100) := I_location;

   L_rowid         ROWID  := NULL;

   cursor C_ORD_LC_AMENDMENTS is
      select rowid
       from ord_lc_amendments
      where order_no = LP_order_no
        and item = I_item
        and change_type = 'OIC'
        for update nowait;
BEGIN
   if I_lc_cost_changed = 1 then /* cost changed for this item on current order  */
      open C_ORD_LC_AMENDMENTS;
      fetch C_ORD_LC_AMENDMENTS into L_rowid;
      close C_ORD_LC_AMENDMENTS;

      if L_rowid is NOT NULL then
         SQL_LIB.SET_MARK('UPDATE',NULL,'ord_lc_amendments','rowid:'||L_rowid);

         UPDATE ord_lc_amendments
            SET new_value   = I_average_cost
          WHERE rowid = L_rowid;
      else
            INSERT INTO ord_lc_amendments
                 VALUES (LP_order_no,
                         I_item,
                         'OIC',
                         I_average_cost,
                         LP_vdate);
      end if;
   else /* cost did not change for this item on current order */
      DELETE FROM ord_lc_amendments
       WHERE order_no = LP_order_no
         AND item = I_item
         AND change_type = 'OIC';
   end if;

   return TRUE;

EXCEPTION
when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Order_No: '||L_key1||'Item: '||L_key2,
                                             'Location: '||L_key3);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_LC;
---------------------------------------------------------------------------------------------------------------
-- Function: INSERT_L10N_DOC_DETAILS_GTT
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_L10N_DOC_DETAILS_GTT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item_cost       IN OUT   ITEM_COST_TBL)
RETURN BOOLEAN is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.INSERT_L10N_DOC_DETAILS_GTT';
   L_item_count           NUMBER := 0;

BEGIN
   L_item_count := I_item_cost.count;

   if L_item_count >0 then
      for i in I_item_cost.first..I_item_cost.last loop
         INSERT INTO l10n_doc_details_gtt
                     (doc_id,
                      doc_type,
                      item,
                      supplier,
                      location,
                      loc_type,
                      country_id)
              (SELECT ol.order_no,
                      'PO',
                      ol.item,
                      oh.supplier,
                      ol.location,
                      ol.loc_type,
                      oh.import_country_id
                 FROM ordhead oh,
                      ordloc ol
                WHERE oh.order_no = ol.order_no
                  AND ol.order_no = I_item_cost(i).order_no
                  AND ol.item = I_item_cost(i).item
                  AND ol.location = I_item_cost(i).location
                  AND ol.unit_cost != I_item_cost(i).old_unit_cost
                  AND EXISTS (SELECT '1'
                                FROM ord_tax_breakup ot
                               WHERE ot.order_no = I_item_cost(i).order_no
                                 AND ot.location = I_item_cost(i).location
                                 AND ot.item = I_item_cost(i).item));
      end loop;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_L10N_DOC_DETAILS_GTT;
---------------------------------------------------------------------------------------------------------------
-- Function: DELETE_ORDLOC_DISC_BLD
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORDLOC_DISC_BLD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.DELETE_ORDLOC_DISC_BLD';
BEGIN
   DELETE FROM ordloc_discount_build
         WHERE order_no = LP_order_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ORDLOC_DISC_BLD;
---------------------------------------------------------------------------------------------------------------
-- Function: DELETE_ORDER_DEAL_BUILD
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORDER_DEAL_BUILD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.DELETE_ORDER_DEAL_BUILD';
BEGIN
   DELETE FROM order_deal_build;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ORDER_DEAL_BUILD;
---------------------------------------------------------------------------------------------------------------
-- Function: CLEANUP_DISCOUNT_TABLES
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION CLEANUP_DISCOUNT_TABLES(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_order_rec_valid   IN OUT   ORDER_TBL)
RETURN BOOLEAN is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.CLEANUP_DISCOUNT_TABLES';
BEGIN
   for i in I_order_rec_valid.first..I_order_rec_valid.last loop
      if I_order_rec_valid.count > 0 then
         DELETE FROM ordloc_invc_cost
               WHERE order_no = I_order_rec_valid(i).order_no
                 AND qty      = 0;

         DELETE FROM ordloc_discount
               WHERE order_no = I_order_rec_valid(i).order_no
                 AND discount_value        = 0
                 AND discount_amt_per_unit = 0;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CLEANUP_DISCOUNT_TABLES;
---------------------------------------------------------------------------------------------------------------
-- Function: DELETE_DCQ
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DCQ(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_order_rec_valid   IN OUT   ORDER_TBL)
RETURN BOOLEAN is

   L_program              VARCHAR2(100) := 'DEAL_ORD_LIB_SQL.DELETE_DCQ';
BEGIN
   for i in I_order_rec_valid.first..I_order_rec_valid.last loop
      if I_order_rec_valid.count > 0 then
         DELETE FROM deal_calc_queue
               WHERE order_no = I_order_rec_valid(i).order_no;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_DCQ;
---------------------------------------------------------------------------------------------------------------
END DEAL_ORD_LIB_SQL;
/