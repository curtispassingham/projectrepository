CREATE OR REPLACE PACKAGE DEAL_ACTUAL_FORECAST_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------------------------------------------
-- Function Name:  APPLY_TOTAL  
-- Purpose      :  This function is called by the form DEALPERF.FMB  
--                 It updates the deal_actuals_forecast table if the fixed indicator is not checked. 
-----------------------------------------------------------------------------------------------------------------------------
FUNCTION APPLY_TOTAL(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_deal_id                  IN     DEAL_ACTUALS_FORECAST.DEAL_ID%TYPE,
                     I_deal_detail_id           IN     DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                     I_baseline_turnover        IN     DEAL_ACTUALS_FORECAST.BASELINE_TURNOVER%TYPE,
                     I_budget_turnover          IN     DEAL_ACTUALS_FORECAST.BUDGET_TURNOVER%TYPE,
                     I_actual_forecast_turnover IN     DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------------------------------
-- Function Name:   CREATE_TEMPLATE  
-- Purpose      :  This function is called by the form DEALMAIN.FMB  
--                 It will create template rows on deal_actuals_forecast 
-----------------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_TEMPLATE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_deal_id                IN     DEAL_ACTUALS_FORECAST.DEAL_ID%TYPE,
                         I_active_date            IN     DEAL_HEAD.ACTIVE_DATE%TYPE,
                         I_close_date             IN     DEAL_HEAD.CLOSE_DATE%TYPE,
                         I_deal_detail_id         IN     DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                         I_deal_reporting_level   IN     DEAL_HEAD.DEAL_REPORTING_LEVEL%TYPE,
                         I_bbd_add_rep_days       IN     DEAL_HEAD.BBD_ADD_REP_DAYS%TYPE)
   RETURN BOOLEAN;   
-----------------------------------------------------------------------------------------------------------------------------
-- Function Name: GET_ATTRIB 
-- Purpose:       Called by DEALMAIN.FMB, This function return all the attributes of the deal_actuals_forecast table 
--                for a given deal.
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB(O_error_message                 IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists                        IN OUT BOOLEAN,
                    O_deal_actuals_forecast_rec     IN OUT DEAL_ACTUALS_FORECAST%ROWTYPE,
                    I_deal_id                       IN     DEAL_ACTUALS_FORECAST.DEAL_ID%TYPE,
                    I_deal_detail_id                IN     DEAL_ACTUALS_FORECAST.DEAL_DETAIL_ID%TYPE,
                    I_reporting_date                IN     DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE)

RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------------
-- Function Name: DELETE RECORD 
-- Purpose:       Called by DEALMAIN.FMB,This function Deletes all rows on the deal_actuals_forecast table 
--                for each deal_id passed in to it.
-------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_RECORD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_deal_id         IN DEAL_ACTUALS_FORECAST.DEAL_ID%TYPE,
                       I_deal_detail_id  IN DEAL_ACTUALS_FORECAST.DEAL_DETAIL_ID%TYPE DEFAULT NULL)

RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name: PERC_UPLIFT
-- Purpose:      Called by DEALMAIN.FMB, This function applies a baseline growth rate to each 
--               baseline turnover figure for a deal,storing this into the budget_turnover 
--                column for for each row in deal_actuals_forecast for the deal id passed in     
-------------------------------------------------------------------------------------------------------------
FUNCTION PERC_UPLIFT(O_error_message                 IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_deal_id                       IN     DEAL_ACTUALS_FORECAST.DEAL_ID%TYPE,
                     I_deal_detail_id                IN     DEAL_ACTUALS_FORECAST.DEAL_DETAIL_ID%TYPE,
                     I_target_baseline_growth_rate   IN     NUMBER)

RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name : COPY_BUDGET
-- Purpose       : Called by DEALMAIN.FMB, This function will copy BUDGET_TURNOVER and INCOME 
--                 to the deal_actuals_forecast table.The update is for all rows specified in 
--                 the update WHERE clause, which is why the primary key columns are not referenced.
-------------------------------------------------------------------------------------------------------------
FUNCTION COPY_BUDGET (O_error_message                 IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_deal_id                       IN     DEAL_ACTUALS_FORECAST.DEAL_ID%TYPE,
                      I_deal_detail_id                IN     DEAL_ACTUALS_FORECAST.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Function Name : ACTUALS_EXIST
-- Purpose       : Called by DEALMAIN.FMB, This function will check to see if actual income has been received.
-------------------------------------------------------------------------------------------------------------
FUNCTION ACTUALS_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists        IN OUT BOOLEAN,
                       I_deal_id       IN     DEAL_ACTUALS_FORECAST.DEAL_ID%TYPE)

RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-- Function Name : GET_ACTUALS_TOTAL
-- Purpose       : Called by DEALPERF.FMB, This function will get the actuals totals from the 
--                 DEAL_ACTUAL_FORECASTS table, if it is more than the total being entered then
--                 the form will output an error.
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_ACTUALS_TOTAL(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_total          IN OUT NUMBER,
                           I_deal_id        IN     DEAL_ACTUALS_FORECAST.DEAL_ID%TYPE,
                           I_deal_detail_id IN     DEAL_ACTUALS_FORECAST.DEAL_DETAIL_ID%TYPE)

RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------

END DEAL_ACTUAL_FORECAST_SQL;
/