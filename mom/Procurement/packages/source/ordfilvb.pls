
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ORDER_FILTER_VALIDATE_SQL AS

LP_dummy      VARCHAR2(1);

-----------------------------------------------------------------------------------
--- Function:   FILTER_ITEM_PARENT
--- Purpose:    Validates the a filtered item parent 
--- Called By: ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_ITEM_PARENT (O_error_message IN OUT   VARCHAR2,
              I_order_no      IN ORDLOC_WKSHT.ORDER_NO%TYPE,
              I_item    IN ORDLOC_WKSHT.ITEM%TYPE,
              I_item_parent   IN ORDLOC_WKSHT.ITEM_PARENT%TYPE)
return BOOLEAN is

   cursor C_CHECK_PARENT is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         and item_parent = I_item_parent
         and (I_item is null or item = I_item);

BEGIN
   if I_order_no is NULL or I_item_parent is NULL then
     O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_PARENT;
   
   fetch C_CHECK_PARENT into LP_dummy;
   ---
   if C_CHECK_PARENT%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NOT_VALID_PARENT');
      ---
      close C_CHECK_PARENT;
      return FALSE;
   end if;
   ---
   close C_CHECK_PARENT;
   ---
   return TRUE;
EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_FILTER_VALIDATE_SQL.FILTER_ITEM_PARENT',
                                             to_char(SQLCODE));
      RETURN FALSE;
END FILTER_ITEM_PARENT;
-----------------------------------------------------------------------------------
--- Function:  FILTER_item
--- Purpose:   Validates the :B_filter_head.TI_item field
--- Called By: ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_ITEM (O_error_message  IN OUT   VARCHAR2,
                     I_order_no     IN ORDLOC_WKSHT.ORDER_NO%TYPE,
                     I_item      IN ORDLOC_WKSHT.ITEM%TYPE,
                     I_item_parent  IN ORDLOC_WKSHT.ITEM_PARENT%TYPE)
return BOOLEAN is

cursor C_CHECK_ITEM is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         and item = I_item
         and (I_item_parent is NULL or item_parent = I_item_parent);

BEGIN
   if I_order_no is NULL or I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_ITEM;
   ---
   fetch C_CHECK_ITEM into LP_dummy;
   ---
   if C_CHECK_ITEM%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM');
      ---
      close C_CHECK_ITEM;
      return FALSE;
   end if;
   ---
   close C_CHECK_ITEM;
   ---
   return TRUE;
EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_FILTER_VALIDATE_SQL.FILTER_ITEM',
                                             to_char(SQLCODE));
      RETURN FALSE;
END FILTER_ITEM;
-----------------------------------------------------------------------------------
--- Function:  FILTER_DIFF1
--- Purpose:   Validates the :B_filter_head.TI_diff_1 field
--- Called By: ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_DIFF1 (O_error_message IN OUT   VARCHAR2,
                       I_order_no   IN ORDLOC_WKSHT.ORDER_NO%TYPE,
                       I_item    IN ORDLOC_WKSHT.ITEM%TYPE,
                       I_item_parent   IN ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                       I_diff_1     IN ORDLOC_WKSHT.DIFF_1%TYPE)
return BOOLEAN is

cursor C_CHECK_DIFF1 is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         and diff_1 = I_diff_1
         and (I_item is null or item = I_item)
         and (I_item_parent is null or item_parent = I_item_parent);

BEGIN
   if I_order_no is NULL or I_diff_1 is NULL then
     O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_DIFF1;
   ---
   fetch C_CHECK_DIFF1 into LP_dummy;
   ---
   if C_CHECK_DIFF1%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID');
      ---
      close C_CHECK_DIFF1;
      return FALSE;
   end if;
   ---
   close C_CHECK_DIFF1;
   ---
   return TRUE;
EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_FILTER_VALIDATE_SQL.FILTER_COLOR',
                                             to_char(SQLCODE));
      RETURN FALSE;
END FILTER_DIFF1;

-----------------------------------------------------------------------------------
--- Function:  FILTER_STORE_GRADE
--- Purpose:   Validates the :B_filter_head.TI_store_grade field
--- Called By: ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_STORE_GRADE (O_error_message IN OUT   VARCHAR2,
                             I_order_no      IN ORDLOC_WKSHT.ORDER_NO%TYPE,
                             I_item    IN ORDLOC_WKSHT.ITEM%TYPE,
                             I_item_parent   IN ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                             I_store_grade   IN ORDLOC_WKSHT.STORE_GRADE%TYPE)
return BOOLEAN is

   cursor C_CHECK_STORE_GRADE is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         and store_grade = I_store_grade
         and (I_item is null or item = I_item)
         and (I_item_parent is null or item_parent = I_item_parent);

BEGIN
   if I_order_no is NULL or I_store_grade is NULL then
     O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_STORE_GRADE;
   ---
   fetch C_CHECK_STORE_GRADE into LP_dummy;
   ---
   if C_CHECK_STORE_GRADE%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('STORE_GRADE_ERROR');
      ---
      close C_CHECK_STORE_GRADE;
      return FALSE;
   end if;
   ---
   close C_CHECK_STORE_GRADE;
   ---
   return TRUE;

EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_FILTER_VALIDATE_SQL.FILTER_STORE_GRADE',
                                             to_char(SQLCODE));
      RETURN FALSE;
END FILTER_STORE_GRADE;

-----------------------------------------------------------------------------------
--- Function:  FILTER_GROUP_NO
--- Purpose:   Validates the :B_filter_head.TI_store_grade_group_no field
--- Called By: ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_GROUP_NO (O_error_message    IN OUT  VARCHAR2,
                          O_store_grade_group_desc IN OUT  STORE_GRADE_GROUP.STORE_GRADE_GROUP_DESC%TYPE,
                          I_order_no         IN ORDLOC_WKSHT.ORDER_NO%TYPE,
                          I_item       IN ORDLOC_WKSHT.ITEM%TYPE,
                          I_item_parent         IN ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                          I_store_grade_group_id   IN ORDLOC_WKSHT.STORE_GRADE_GROUP_ID%TYPE)
return BOOLEAN IS


cursor C_CHECK_GROUP_NO is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         and store_grade_group_id = I_store_grade_group_id
         and (I_item is null or item = I_item)
         and (I_item_parent is null or item_parent = I_item_parent);

BEGIN
   if I_order_no is NULL or I_store_grade_group_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_GROUP_NO;
   ---
   fetch C_CHECK_GROUP_NO into LP_dummy;
   ---
   if C_CHECK_GROUP_NO%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_GROUP_NO');
      ---
      close C_CHECK_GROUP_NO;
      return FALSE;
   end if;
   ---
   close C_CHECK_GROUP_NO;
   ---
   if STORE_GRADE_VALIDATE_SQL.GET_GROUP_DESC(O_error_message,
                                              O_store_grade_group_desc,
                                              I_store_grade_group_id) = FALSE then
         return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_FILTER_VALIDATE_SQL.FILTER_GROUP_NO',
                                             to_char(SQLCODE));
      RETURN FALSE;
END FILTER_GROUP_NO;
-----------------------------------------------------------------------------------
--- Function:  FILTER_DIFF2
--- Purpose:   Validates the :B_filter_head.TI_diff_2 field
--- Called By: ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_DIFF2 (O_error_message IN OUT   VARCHAR2,
                       I_order_no   IN ORDLOC_WKSHT.ORDER_NO%TYPE,
                       I_item    IN ORDLOC_WKSHT.ITEM%TYPE,
                       I_item_parent   IN ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                       I_diff_2     IN ORDLOC_WKSHT.DIFF_2%TYPE)
return BOOLEAN IS


cursor C_CHECK_DIFF is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         and diff_2 = I_diff_2
         and (I_item is null or item = I_item)
         and (I_item_parent is null or item_parent = I_item_parent);

BEGIN
   if I_order_no is NULL or I_diff_2 is NULL then
     O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_DIFF;
   ---
   fetch C_CHECK_DIFF into LP_dummy;
   ---
   if C_CHECK_DIFF%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID');
      ---
      close C_CHECK_DIFF;
      return FALSE;
   end if;
   ---
   close C_CHECK_DIFF;
   ---
   return TRUE;
EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_FILTER_VALIDATE_SQL.FILTER_SIZE1',
                                             to_char(SQLCODE));
      RETURN FALSE;
END FILTER_DIFF2;
-----------------------------------------------------------------------------------
--- Function:  FILTER_LOCATION
--- Purpose:   Validates the :B_filter_head.TI_location field
--- Called By: ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_LOCATION (O_error_message IN OUT   VARCHAR2,
                          I_order_no      IN ORDLOC_WKSHT.ORDER_NO%TYPE,
                          I_item    IN ORDLOC_WKSHT.ITEM%TYPE,
                          I_item_parent      IN ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                          I_location      IN ORDLOC_WKSHT.LOCATION%TYPE,
                          I_loc_type      IN ORDLOC_WKSHT.LOC_TYPE%TYPE)
return BOOLEAN IS

   cursor C_CHECK_LOCATION is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         and location = I_location
         and loc_type = I_loc_type
         and (I_item is null or item = I_item)
         and (I_item_parent is null or item_parent = I_item_parent);

BEGIN
  if I_order_no is NULL or I_location is NULL or I_loc_type is NULL then
     O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_LOCATION;
   ---
   fetch C_CHECK_LOCATION into LP_dummy;
   ---
   if C_CHECK_LOCATION%NOTFOUND then
      ---
      if I_loc_type = 'S' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE');
         ---
         close C_CHECK_LOCATION;
         return FALSE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_WH',
                                               NULL,
                                               NULL,
                                               NULL);
         close C_CHECK_LOCATION;
         return FALSE;
      end if;
      ---
   end if;
   ---
   close C_CHECK_LOCATION;
   ---
   return TRUE;

EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_FILTER_VALIDATE_SQL.FILTER_LOCATION',
                                             to_char(SQLCODE));
      RETURN FALSE;
END FILTER_LOCATION;
-----------------------------------------------------------------------------------
--- Function:  FILTER_STANDARD_UOM
--- Purpose:   Validates the standard_oum
--- Called By: ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_STANDARD_UOM (O_error_message   IN OUT   VARCHAR2,
                              I_order_no  IN ORDLOC_WKSHT.ORDER_NO%TYPE,
                              I_item      IN ORDLOC_WKSHT.ITEM%TYPE,
                              I_item_parent  IN ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                              I_standard_uom IN ORDLOC_WKSHT.STANDARD_UOM%TYPE)
return BOOLEAN IS


 cursor C_STANDARD_UOM is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         and standard_uom = I_standard_uom 
         and (I_item is null or item = I_item)
         and (I_item_parent is null or item_parent = I_item_parent);

BEGIN
  if I_order_no is NULL or I_standard_uom is NULL then
     O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   open C_STANDARD_UOM;
   ---
   fetch C_STANDARD_UOM into LP_dummy;
   ---
   if C_STANDARD_UOM%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_STAND_UOM');
      ---
      close C_STANDARD_UOM;
      return FALSE;
   end if;
   --- 
   close C_STANDARD_UOM;
   ---
   return TRUE;
EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_FILTER_VALIDATE_SQL.FILTER_STANDARD_UOM',
                                             to_char(SQLCODE));
      RETURN FALSE;
END FILTER_STANDARD_UOM;
-----------------------------------------------------------------------------------
--- Function:  FILTER_UOP
--- Purpose:   Validates the uop
--- Called By: ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_UOP (O_error_message   IN OUT   VARCHAR2,
                     I_order_no     IN ORDLOC_WKSHT.ORDER_NO%TYPE,
                     I_item      IN ORDLOC_WKSHT.ITEM%TYPE,
                     I_item_parent  IN ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                     I_uop    IN CODE_DETAIL.CODE_DESC%TYPE)
return BOOLEAN IS
   
   L_dummy   UOM_CLASS_TL.UOM_DESC_TRANS%TYPE;

   cursor C_CHECK_UOP is
         select 'x'
           from ordloc_wksht ow, 
                code_detail cd
          where ow.order_no = I_order_no
            and cd.code_type = 'CASN'
            and ow.uop = cd.code
            and upper(cd.code_desc) = upper(I_uop)
            and (I_item is null or item = I_item)
            and (I_item_parent is null or item_parent = I_item_parent);

   cursor C_CHECK_UOP_STANDARD is
         select 'x'
           from ordloc_wksht ow
          where ow.order_no = I_order_no
            and ow.uop = I_uop
            and (I_item is null or item = I_item)
            and (I_item_parent is null or item_parent = I_item_parent);

BEGIN
   if I_order_no is NULL or I_uop is NULL then
     O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   if UOM_SQL.GET_DESC(O_error_message,
                       L_dummy,
                       I_uop) = FALSE then  
                      --uop is invalid or case
      open C_CHECK_UOP;
      fetch C_CHECK_UOP into LP_dummy;
      ---
      if C_CHECK_UOP%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_UOP');
         ---
         close C_CHECK_UOP;
         return FALSE;
      end if;
      ---
      close C_CHECK_UOP;
      ---
   else
      --uop is a standard uom 
      open C_CHECK_UOP_STANDARD;
      fetch C_CHECK_UOP_STANDARD into LP_dummy;
      ---
      if C_CHECK_UOP_STANDARD%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_UOP');
         ---
         close C_CHECK_UOP_STANDARD;
         return FALSE;
      end if;
      ---
      close C_CHECK_UOP_STANDARD;
      ---
      ---
   end if;
   ---
   return TRUE;
EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_FILTER_VALIDATE_SQL.FILTER_UOP',
                                             to_char(SQLCODE));
      RETURN FALSE;
END FILTER_UOP;
-----------------------------------------------------------------------------------
--- Function:  FILTER_DIFF3
--- Purpose:   Validates the :B_filter_head.TI_diff_3 field
--- Called By: ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_DIFF3 (O_error_message    IN OUT   VARCHAR2,
                       I_order_no         IN       ORDLOC_WKSHT.ORDER_NO%TYPE,
                       I_item             IN       ORDLOC_WKSHT.ITEM%TYPE,
                       I_item_parent      IN       ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                       I_diff_3           IN       ORDLOC_WKSHT.DIFF_3%TYPE)
return BOOLEAN IS


cursor C_CHECK_DIFF is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         and diff_3 = I_diff_3
         and (I_item is null or item = I_item)
         and (I_item_parent is null or item_parent = I_item_parent);

BEGIN
   if I_order_no is NULL or I_diff_3 is NULL then
     O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_DIFF;
   ---
   fetch C_CHECK_DIFF into LP_dummy;
   ---
   if C_CHECK_DIFF%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID');
      ---
      close C_CHECK_DIFF;
      return FALSE;
   end if;
   ---
   close C_CHECK_DIFF;
   ---
   return TRUE;
EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_FILTER_VALIDATE_SQL.FILTER_DIFF3',
                                             to_char(SQLCODE));
      RETURN FALSE;
END FILTER_DIFF3;
-----------------------------------------------------------------------------------
--- Function:  FILTER_DIFF4
--- Purpose:   Validates the :B_filter_head.TI_diff_4 field
--- Called By: ordmtxws.fmb
-----------------------------------------------------------------------------------
FUNCTION FILTER_DIFF4 (O_error_message    IN OUT   VARCHAR2,
                       I_order_no         IN       ORDLOC_WKSHT.ORDER_NO%TYPE,
                       I_item             IN       ORDLOC_WKSHT.ITEM%TYPE,
                       I_item_parent      IN       ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                       I_diff_4           IN       ORDLOC_WKSHT.DIFF_4%TYPE)
return BOOLEAN IS


cursor C_CHECK_DIFF is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         and diff_4 = I_diff_4
         and (I_item is null or item = I_item)
         and (I_item_parent is null or item_parent = I_item_parent);

BEGIN
   if I_order_no is NULL or I_diff_4 is NULL then
     O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_DIFF;
   ---
   fetch C_CHECK_DIFF into LP_dummy;
   ---
   if C_CHECK_DIFF%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID');
      ---
      close C_CHECK_DIFF;
      return FALSE;
   end if;
   ---
   close C_CHECK_DIFF;
   ---
   return TRUE;
EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_FILTER_VALIDATE_SQL.FILTER_DIFF4',
                                             to_char(SQLCODE));
      RETURN FALSE;
END FILTER_DIFF4;
-----------------------------------------------------------------------------------
END ORDER_FILTER_VALIDATE_SQL;
/