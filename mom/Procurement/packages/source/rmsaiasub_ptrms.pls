CREATE OR REPLACE PACKAGE RMSAIASUB_PAYTERM AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
HDR_ADD  CONSTANT  VARCHAR2(15) := 'paytermcre';
HDR_UPD  CONSTANT  VARCHAR2(15) := 'paytermmod';
DTL_ADD  CONSTANT  VARCHAR2(15) := 'paytermdtlcre';
DTL_UPD  CONSTANT  VARCHAR2(15) := 'paytermdtlmod';
--------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code            OUT  VARCHAR2,
                  O_error_message          OUT  VARCHAR2,
                  O_rib_paytermref_rec     OUT  "RIB_PayTermRef_REC",
                  I_message             IN      "RIB_PayTermDesc_REC",
                  I_message_type        IN      VARCHAR2);
--------------------------------------------------------------------------------
END RMSAIASUB_PAYTERM;

/
