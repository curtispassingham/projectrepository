CREATE OR REPLACE PACKAGE BODY ORDER_CALC_SQL AS
   ---
   LP_elc_ind              SYSTEM_OPTIONS.ELC_IND%TYPE   := NULL;
   LP_supplier             SUPS.SUPPLIER%TYPE;
   LP_supp_currency_code   SUPS.CURRENCY_CODE%TYPE;
-------------------------------------------------------------------------
FUNCTION TOTAL_COST_RETAIL(O_error_message                IN OUT   VARCHAR2,
                           O_total_cost_ord               IN OUT   NUMBER,
                           O_total_cost_prim              IN OUT   NUMBER,
                           O_total_outstand_cost_ord      IN OUT   NUMBER,
                           O_total_outstand_cost_prim     IN OUT   NUMBER,
                           O_total_cancel_cost_ord        IN OUT   NUMBER,
                           O_total_cancel_cost_prim       IN OUT   NUMBER,
                           O_total_retail_incl_vat_ord    IN OUT   NUMBER,
                           O_total_retail_incl_vat_prim   IN OUT   NUMBER,
                           O_total_retail_excl_vat_ord    IN OUT   NUMBER,
                           O_total_retail_excl_vat_prim   IN OUT   NUMBER,
                           O_total_landed_cost_ord        IN OUT   NUMBER,
                           O_total_landed_cost_prim       IN OUT   NUMBER,
                           O_total_expense_ord            IN OUT   NUMBER,
                           O_total_expense_prim           IN OUT   NUMBER,
                           O_total_duty_ord               IN OUT   NUMBER,
                           O_total_duty_prim              IN OUT   NUMBER,
                           O_brkt_pct_off                 IN OUT   NUMBER,
                           O_qty                          IN OUT   NUMBER,
                           O_total_cost_incl_tax_ord      IN OUT   NUMBER,
                           O_total_cost_incl_tax_prim     IN OUT   NUMBER,
                           I_order_no                     IN       ORDHEAD.ORDER_NO%TYPE,
                           I_exchange_rate                IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                           I_currency_code                IN       CURRENCIES.CURRENCY_CODE%TYPE,
                           I_import_country               IN       COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program                    VARCHAR2(64) := 'ORDER_CALC_SQL.TOTAL_COST_RETAIL';
   L_duty                       NUMBER;
   L_dty_currency               CURRENCIES.CURRENCY_CODE%TYPE;
   L_expense                    NUMBER;
   L_exp_currency               CURRENCIES.CURRENCY_CODE%TYPE;
   L_landed_cost                NUMBER;
   L_lc_currency                CURRENCIES.CURRENCY_CODE%TYPE;
   L_unit_retail                ORDLOC.UNIT_RETAIL%TYPE;
   L_unit_retail_excl_vat       ORDLOC.UNIT_RETAIL%TYPE;
   L_qty                        ORDLOC.QTY_ORDERED%TYPE;
   L_item                       ITEM_MASTER.ITEM%TYPE;
   L_dept                       DEPS.DEPT%TYPE;
   L_class                      CLASS.CLASS%TYPE;
   L_subclass                   SUBCLASS.SUBCLASS%TYPE;
   L_loc_type                   ORDLOC.LOC_TYPE%TYPE;
   L_location                   ORDLOC.LOCATION%TYPE;
   L_loc_currency               CURRENCIES.CURRENCY_CODE%TYPE;
   L_base_currency              CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate              ORDHEAD.EXCHANGE_RATE%TYPE;
   L_exchange_rate_exp          ORDHEAD.EXCHANGE_RATE%TYPE;
   L_currency_code              ORDHEAD.CURRENCY_CODE%TYPE;
   L_zone_id                    COST_ZONE_GROUP_LOC.ZONE_ID%TYPE;
   L_origin_country_id          ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_import_country_id          ORDHEAD.IMPORT_COUNTRY_ID%TYPE := I_import_country;
   L_store_ind                  VARCHAR2(1);
   L_zone_group_id              COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   L_prescale_cost_ord          ORDLOC.UNIT_COST%TYPE;
   L_unit_cost                  ORDLOC.UNIT_COST%TYPE;
   L_total_cost                 ORDLOC.UNIT_COST%TYPE;

   L_unit_retail_excl_vat_loc   ORDLOC.UNIT_COST%TYPE;
   L_unit_retail_incl_vat_loc   ORDLOC.UNIT_COST%TYPE;
   L_unit_retail_excl_vat_prim  ORDLOC.UNIT_COST%TYPE;
   L_unit_retail_incl_vat_prim  ORDLOC.UNIT_COST%TYPE;
   L_unit_retail_incl_vat_ord   ORDLOC.UNIT_COST%TYPE;
   L_unit_retail_excl_vat_ord   ORDLOC.UNIT_COST%TYPE;

   L_total_cost_incl_tax_ord    ORD_TAX_BREAKUP.TOTAL_TAX_AMT%TYPE  := 0;

   L_retail_tax_rec             OBJ_TAX_RETAIL_ADD_REMOVE_REC := OBJ_TAX_RETAIL_ADD_REMOVE_REC();
   L_tax_retail_tbl             OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();
   L_import_type                ORDHEAD.IMPORT_TYPE%TYPE;
   L_import_id                  ORDHEAD.IMPORT_ID%TYPE;
   L_exchange_type              CURRENCY_RATES.EXCHANGE_TYPE%TYPE:= NULL;
   L_ord_exchange_rate          ORDHEAD.EXCHANGE_RATE%TYPE;

   cursor C_ORDLOC is
      select o.item, o.unit_cost,
             o.location,
             o.unit_retail,
             o.qty_ordered,
             o.loc_type,
             s.origin_country_id,
             oh.supplier,
             im.pack_ind,
             im.sellable_ind,
             im.orderable_ind,
             im.pack_type
        from ordhead oh,
             ordloc o,
             ordsku s,
             item_master im
       where oh.order_no = I_order_no
         and oh.order_no = o.order_no
         and o.order_no = s.order_no
         and o.item = im.item
         and o.item = s.item
       order by 2;

   TYPE ordlocTYPE IS TABLE OF C_ORDLOC%ROWTYPE INDEX BY BINARY_INTEGER;
   LP_ordloc ordlocTYPE;

   cursor C_IMPORT_COUNTRY is
      select import_country_id
        from ordhead
       where order_no = I_order_no;

   cursor C_TOT_COST_ALL_TAXES is
      select SUM(NVL(otb.total_tax_amt,0))
        from vat_codes vc,
             ord_tax_breakup otb,
             ordloc ol
       where otb.order_no = I_order_no
         and otb.tax_code = vc.vat_code
         and vc.incl_nic_ind = 'N'
         and ol.order_no = otb.order_no
         and ol.item = otb.item
         and ol.location = otb.location
       group by otb.order_no;

   cursor C_CHECK_PO_EXCHANGE_TYPE is
      select r.exchange_type
        from currencies c,
             currency_rates r
       where c.currency_code = L_dty_currency
         and c.currency_code = r.currency_code
         and r.exchange_type = 'P'
         and r.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.exchange_type = 'P'
                                    and cr.currency_code = L_dty_currency
                                    and cr.effective_date <= GET_VDATE);

BEGIN

   O_total_cost_ord               := 0;
   O_total_cost_prim              := 0;
   O_total_outstand_cost_ord      := 0;
   O_total_outstand_cost_prim     := 0;
   O_total_cancel_cost_ord        := 0;
   O_total_cancel_cost_prim       := 0;
   O_total_retail_incl_vat_ord    := 0;
   O_total_retail_incl_vat_prim   := 0;
   O_total_retail_excl_vat_ord    := 0;
   O_total_retail_excl_vat_prim   := 0;
   O_total_duty_ord               := 0;
   O_total_duty_prim              := 0;
   O_total_expense_ord            := 0;
   O_total_expense_prim           := 0;
   O_total_landed_cost_ord        := 0;
   O_total_landed_cost_prim       := 0;
   O_qty                          := 0;
   O_total_cost_incl_tax_ord      := 0;
   O_total_cost_incl_tax_prim     := 0;

   --- get exchange rate and currency
   if I_exchange_rate is NULL or I_currency_code is NULL then
      if ORDER_ATTRIB_SQL.GET_CURRENCY_RATE (O_error_message,
                                             L_currency_code,
                                             L_exchange_rate,
                                             I_order_no) = FALSE then
         return FALSE;
      end if;
   else
      L_currency_code := I_currency_code;
      L_exchange_rate := I_exchange_rate;
   end if;
   ---
   --- retrieve base currency
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE (O_error_message,
                                        L_base_currency) = FALSE then
      return FALSE;
   end if;
   ---
   --call total_costs function to get total order cost,
   --total cancelled cost and outstanding order cost
   if  TOTAL_COSTS(O_error_message,
                   O_total_cost_ord,
                   L_prescale_cost_ord,
                   O_total_outstand_cost_ord,
                   O_total_cancel_cost_ord,
                   I_order_no,
                   NULL,
                   NULL)= FALSE then
       return FALSE;
   end if;
   ---
   --- convert order's total cost from order to primary currency.
   if CURRENCY_SQL.CONVERT (O_error_message,
                            O_total_cost_ord,
                            L_currency_code,
                            L_base_currency,
                            O_total_cost_prim,
                            'N',
                            NULL,
                            NULL,
                            L_exchange_rate,
                            NULL) = FALSE then
      return FALSE;
   end if;
   ---
   --- convert order's outstanding cost from order to primary currency.
   if CURRENCY_SQL.CONVERT (O_error_message,
                            O_total_outstand_cost_ord,
                            L_currency_code,
                            L_base_currency,
                            O_total_outstand_cost_prim,
                            'N',
                            NULL,
                            NULL,
                            L_exchange_rate,
                            NULL) = FALSE then
      return FALSE;
   end if;
   ---
   --- convert order's cancel cost from order to primary currency.
   if CURRENCY_SQL.CONVERT (O_error_message,
                            O_total_cancel_cost_ord,
                            L_currency_code,
                            L_base_currency,
                            O_total_cancel_cost_prim,
                            'N',
                            NULL,
                            NULL,
                            L_exchange_rate,
                            NULL) = FALSE then
      return FALSE;
   end if;
   ---

   -- only get elc_ind if LP_elc_ind not populated already  --
   ---
   if LP_elc_ind is NULL then
      if SYSTEM_OPTIONS_SQL.GET_ELC_IND (O_error_message,
                                         LP_elc_ind) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_import_country is NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_IMPORT_COUNTRY', 'ORDHEAD', 'Order no: ' ||
                       TO_CHAR(I_order_no));
      open C_IMPORT_COUNTRY;
      SQL_LIB.SET_MARK('FETCH', 'C_IMPORT_COUNTRY', 'ORDHEAD', 'Order no: ' ||
                       TO_CHAR(I_order_no));
      fetch C_IMPORT_COUNTRY into L_import_country_id;
      if C_IMPORT_COUNTRY%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG ('ERR_RETRIEVE_IMPORT_CO',
                                                'Order no: '|| I_order_no,NULL,NULL);
         SQL_LIB.SET_MARK('CLOSE','C_IMPORT_COUNTRY','ORDHEAD', 'Order no: ' ||
                          TO_CHAR(I_order_no));
         close C_IMPORT_COUNTRY;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_IMPORT_COUNTRY','ORDHEAD','Order no: ' ||
                       TO_CHAR(I_order_no));
      close C_IMPORT_COUNTRY;
   end if;
   ---
   -- Bulk collect all ORDLOC records, along with some additional information (see C_ORDLOC cursor)
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_ORDLOC', 'ORDHEAD, ORDSKU, ORDLOC, ITEM_MASTER', 'Order ' || TO_CHAR(I_order_no));
   open C_ORDLOC;
   SQL_LIB.SET_MARK('FETCH', 'C_ORDLOC', 'ORDHEAD, ORDSKU, ORDLOC, ITEM_MASTER', 'Order ' || TO_CHAR(I_order_no));
   fetch C_ORDLOC BULK COLLECT into LP_ordloc;
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDLOC', 'ORDHEAD, ORDSKU, ORDLOC, ITEM_MASTER', 'Order ' || TO_CHAR(I_order_no));
   close C_ORDLOC;

   if LP_ordloc is NOT NULL and LP_ordloc.COUNT > 0 then
      FOR a IN 1 .. LP_ordloc.COUNT LOOP

         L_item              := LP_ordloc(a).item;
         L_location          := LP_ordloc(a).location;
         L_unit_retail       := LP_ordloc(a).unit_retail;
         L_qty               := LP_ordloc(a).qty_ordered;
         L_loc_type          := LP_ordloc(a).loc_type;
         L_origin_country_id := LP_ordloc(a).origin_country_id;
         L_unit_cost         := LP_ordloc(a).unit_cost;
         L_total_cost        := L_unit_cost * L_qty;
         ---

         if LP_elc_ind = 'Y' then
            if ELC_CALC_SQL.CALC_ORDER_TOTALS (O_error_message,
                                               L_landed_cost,
                                               L_expense,
                                               L_exp_currency,
                                               L_exchange_rate_exp,
                                               L_duty,
                                               L_dty_currency,
                                               I_order_no,
                                               L_item,
                                               LP_ordloc(a).pack_ind,
                                               LP_ordloc(a).sellable_ind,
                                               LP_ordloc(a).orderable_ind,
                                               LP_ordloc(a).pack_type,
                                               LP_ordloc(a).qty_ordered,
                                               NULL,
                                               NULL,
                                               L_location,
                                               LP_ordloc(a).supplier,
                                               L_origin_country_id,
                                               L_import_country_id,
                                               L_total_cost) = FALSE then
               return FALSE;
            end if;
            ---
            if CURRENCY_SQL.CONVERT (O_error_message,
                                     L_expense,
                                     L_exp_currency,
                                     L_base_currency,
                                     L_expense,
                                     'N',
                                     NULL,
                                     NULL,
                                     L_exchange_rate_exp,
                                     NULL) = FALSE then
               return FALSE;
            end if;
            ---
            if L_dty_currency = L_currency_code then
               L_exchange_type := NULL ;
               L_ord_exchange_rate := L_exchange_rate;
            else
               SQL_LIB.SET_MARK('OPEN',
                                'C_CHECK_PO_EXCHANGE_TYPE',
                                'CURRENCY_RATES',
                                'Currency: '||L_dty_currency);
               open C_CHECK_PO_EXCHANGE_TYPE;

               SQL_LIB.SET_MARK('FETCH',
                                'C_CHECK_PO_EXCHANGE_TYPE',
                                'CURRENCY_RATES',
                                'Currency: '||L_dty_currency);
               fetch C_CHECK_PO_EXCHANGE_TYPE into L_exchange_type;

               SQL_LIB.SET_MARK('CLOSE',
                                'C_CHECK_PO_EXCHANGE_TYPE',
                                'CURRENCY_RATES',
                                'Currency: '||L_dty_currency);
               close C_CHECK_PO_EXCHANGE_TYPE;
               L_ord_exchange_rate := NULL;
            end if;
            ---
            if CURRENCY_SQL.CONVERT (O_error_message,
                                     L_duty,
                                     L_dty_currency,
                                     L_base_currency,
                                     L_duty,
                                     'N',
                                     NULL,
                                     L_exchange_type,
                                     L_ord_exchange_rate,
                                     NULL,
                                    'N') = FALSE then
               return FALSE;
            end if;
            ---
            O_total_expense_prim     := O_total_expense_prim     + L_expense;
            O_total_duty_prim        := O_total_duty_prim        + L_duty ;
            O_total_landed_cost_prim := O_total_landed_cost_prim + L_landed_cost;

         end if;
         ---
         O_qty := O_qty + L_qty;
         ---

         if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                           L_item,
                                           L_dept,
                                           L_class,
                                           L_subclass) = FALSE then
            return FALSE;
         end if;

         L_retail_tax_rec.I_item                := L_item;
         L_retail_tax_rec.I_dept                := L_dept;
         L_retail_tax_rec.I_class               := L_class;
         L_retail_tax_rec.I_location            := L_location;
         L_retail_tax_rec.I_loc_type            := L_loc_type;
         L_retail_tax_rec.I_effective_from_date := GET_VDATE;
         L_retail_tax_rec.I_amount              := L_unit_retail;
         ---
         L_tax_retail_tbl.delete;
         L_tax_retail_tbl.extend;
         L_tax_retail_tbl(1) := L_retail_tax_rec;

         -- Get the inclusive and exclusive retail values  (location currency)
         if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                             L_tax_retail_tbl) = FALSE then
            return FALSE;
         end if;

         L_unit_retail_incl_vat_loc := L_tax_retail_tbl(1).O_amount;

         ---
         if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                                             L_tax_retail_tbl) = FALSE then
            return FALSE;
         end if;

         L_unit_retail_excl_vat_loc := L_tax_retail_tbl(1).O_amount;

         -- Ordloc for a franchise order contains retail in Costing Loc Currency
         -- So change Location from Franchise to Costing Loc for Conversion
         if ORDER_SQL.GET_DEFAULT_IMP_EXP(O_error_message,
                                          L_import_id,
                                          L_import_type,
                                          I_order_no) = FALSE then
            return FALSE;
         end if;
         if L_import_type = 'F' then
            L_location := L_import_id;
            if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                            L_loc_type,
                                            L_import_id) = FALSE then
               return FALSE;
            end if;
         end if;
         if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                      L_location,
                                      L_loc_type,
                                      NULL,
                                      L_loc_currency) = FALSE then
            return FALSE;
         end if;

         -- convert retails to primary currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             L_location,
                                             L_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_unit_retail_incl_vat_loc,
                                             L_unit_retail_incl_vat_prim,
                                             'N',
                                             NULL,
                                             NULL) = FALSE then
               return FALSE;
         end if;
         ---
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             L_location,
                                             L_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_unit_retail_excl_vat_loc,
                                             L_unit_retail_excl_vat_prim,
                                             'N',
                                             NULL,
                                             NULL) = FALSE then
               return FALSE;
         end if;

         ---
         O_total_retail_excl_vat_prim := O_total_retail_excl_vat_prim + (L_unit_retail_excl_vat_prim * L_qty);
         O_total_retail_incl_vat_prim := O_total_retail_incl_vat_prim + (L_unit_retail_incl_vat_prim * L_qty);

   if L_loc_currency != L_currency_code then
      ---
      -- convert order's retail with vat from primary to order currency.
      ---
      if CURRENCY_SQL.CONVERT (O_error_message,
                               L_unit_retail_incl_vat_prim,
                               L_base_currency,
                               L_currency_code,
                               L_unit_retail_incl_vat_ord,
                               'N',
                               NULL,
                               NULL,
                               NULL,
                               L_exchange_rate) = FALSE then
         return FALSE;
      end if;
      ---
      --- convert order's retail without vat from primary to order currency.
      if CURRENCY_SQL.CONVERT (O_error_message,
                               L_unit_retail_excl_vat_prim,
                               L_base_currency,
                               L_currency_code,
                               L_unit_retail_excl_vat_ord,
                               'N',
                               NULL,
                               NULL,
                               NULL,
                               L_exchange_rate) = FALSE then
         return FALSE;
      end if;
   else
         L_unit_retail_incl_vat_ord := L_unit_retail_incl_vat_loc;
         L_unit_retail_excl_vat_ord := L_unit_retail_excl_vat_loc;
   end if;
   ---
   O_total_retail_excl_vat_ord := O_total_retail_excl_vat_ord + (L_unit_retail_excl_vat_ord * L_qty);
   O_total_retail_incl_vat_ord := O_total_retail_incl_vat_ord + (L_unit_retail_incl_vat_ord * L_qty);

   END LOOP;
  end if;
   --- only convert the landed cost components if system ELC_IND = Y.
   if LP_elc_ind = 'Y' then
      ---
      --- convert order's expense from primary to order currency.
      if O_total_expense_prim != 0 then
         if CURRENCY_SQL.CONVERT (O_error_message,
                                  O_total_expense_prim,
                                  L_base_currency,
                                  L_currency_code,
                                  O_total_expense_ord,
                                  'N',
                                  NULL,
                                  NULL,
                                  NULL,
                                  L_exchange_rate) = FALSE then
            return FALSE;
         end if;
      end if;

      ---
      --- convert order's duty from primary to order currency.
      if O_total_duty_prim != 0 then
         if CURRENCY_SQL.CONVERT (O_error_message,
                                  O_total_duty_prim,
                                  L_base_currency,
                                  L_currency_code,
                                  O_total_duty_ord,
                                  'N',
                                  NULL,
                                  NULL,
                                  NULL,
                                  L_exchange_rate,
                                  'N') = FALSE then
            return FALSE;
         end if;
      end if;
      O_total_landed_cost_ord :=O_total_cost_ord+O_total_expense_ord+O_total_duty_ord ;
   else
      ---
      -- If the ELC ind is 'N' still need to pass out an
      -- Estimated Landed Cost value.  With no components, the ELC
      -- is simply equal to the order cost.
      ---
      O_total_landed_cost_ord  := O_total_cost_ord;
      O_total_landed_cost_prim := O_total_cost_prim;
   end if;
   -- call brkt_pct_off to get total discount percent for the order.
   if BRKT_PCT_OFF(O_error_message,
                   O_brkt_pct_off,
                   I_order_no) = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_TOT_COST_ALL_TAXES','VAT_CODES,ORD_TAX_BREAKUP,ORDLOC',NULL);
   open C_TOT_COST_ALL_TAXES;
   SQL_LIB.SET_MARK('FETCH','C_TOT_COST_ALL_TAXES','VAT_CODES,ORD_TAX_BREAKUP,ORDLOC',NULL);
   fetch C_TOT_COST_ALL_TAXES into L_total_cost_incl_tax_ord;
   SQL_LIB.SET_MARK('CLOSE','C_TOT_COST_ALL_TAXES','VAT_CODES,ORD_TAX_BREAKUP,ORDLOC',NULL);
   close C_TOT_COST_ALL_TAXES;

   O_total_cost_incl_tax_ord := O_total_landed_cost_ord + NVL(L_total_cost_incl_tax_ord,0);

   if O_total_cost_incl_tax_ord !=0 then
      if CURRENCY_SQL.CONVERT (O_error_message,
                               O_total_cost_incl_tax_ord,
                               L_currency_code,
                               L_base_currency,
                               O_total_cost_incl_tax_prim,
                               'N',
                               NULL,
                               NULL,
                               L_exchange_rate,
                               NULL) = FALSE then
          return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END TOTAL_COST_RETAIL;
--------------------------------------------------------------------------
FUNCTION ITEM_TOTAL_RETAIL(I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
                           I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                           O_total_retail_ord      IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           O_outstand_retail_ord   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           O_error_message         IN OUT   VARCHAR2)
RETURN BOOLEAN IS

   EXIT_ALL             EXCEPTION;
   L_program            VARCHAR2(64) := 'ORDER_CALC_SQL.ITEM_TOTAL_RETAIL';
   L_loc_ind            ORDLOC.LOC_TYPE%TYPE;
   L_loc                ORDLOC.LOCATION%TYPE;
   L_unit_retail_loc    ORDLOC.UNIT_RETAIL%TYPE;
   L_unit_retail_ord    ORDLOC.UNIT_RETAIL%TYPE;
   L_qty_ord            ORDLOC.QTY_ORDERED%TYPE;
   L_qty_not_received   ORDLOC.QTY_ORDERED%TYPE;
   L_count              NUMBER(4);

   cursor C_ORDLOC is
      select loc_type,
             location,
             unit_retail,
             qty_ordered,
             DECODE(SIGN(qty_ordered - NVL(qty_received,0)), -1, 0,
                         qty_ordered - NVL(qty_received,0))
        from ordloc
       where item     = I_item
         and order_no = I_order_no;

BEGIN
   O_total_retail_ord := 0;
   O_outstand_retail_ord := 0;
   L_count := 0;

   SQL_LIB.SET_MARK('OPEN','C_ORDLOC','ORDLOC',NULL);
   open C_ORDLOC;
   LOOP
      SQL_LIB.SET_MARK('FETCH','C_ORDLOC','ORDLOC',NULL);
      fetch C_ORDLOC into L_loc_ind,
                          L_loc,
                          L_unit_retail_loc,
                          L_qty_ord,
                          L_qty_not_received;
      if C_ORDLOC%NOTFOUND and L_count = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RET_ORDLOC',
                                               'Order no: '|| I_order_no,'Item: ' || I_item,NULL);
         SQL_LIB.SET_MARK('CLOSE','C_ORDLOC', 'ORDLOC',NULL);
         close C_ORDLOC;
         return FALSE;
      elsif C_ORDLOC%NOTFOUND and L_count >= 1 then
         EXIT;
      end if;

      L_count := L_count + 1;

      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          L_loc,
                                          L_loc_ind,
                                          NULL,
                                          I_order_no,
                                          'O',
                                          NULL,
                                          L_unit_retail_loc,
                                          L_unit_retail_ord,
                                          'N',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;

      O_total_retail_ord := O_total_retail_ord +
                                 (L_qty_ord * L_unit_retail_ord);
      O_outstand_retail_ord := O_outstand_retail_ord +
                                 (L_qty_not_received * L_unit_retail_ord);
   END LOOP;
   SQL_LIB.SET_MARK('CLOSE','C_ORDLOC','ORDLOC',NULL);
   close C_ORDLOC;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_TOTAL_RETAIL;
-------------------------------------------------------------------------
FUNCTION TOTAL_COSTS(O_error_message       IN OUT   VARCHAR2,
                     O_total_cost_ord      IN OUT   ORDLOC.UNIT_COST%TYPE,
                     O_prescale_cost_ord   IN OUT   ORDLOC.UNIT_COST%TYPE,
                     O_outstand_cost_ord   IN OUT   ORDLOC.UNIT_COST%TYPE,
                     O_cancel_cost_ord     IN OUT   ORDLOC.UNIT_COST%TYPE,
                     I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                     I_item                IN       ITEM_MASTER.ITEM%TYPE,
                     I_location            IN       ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN IS

   EXIT_ALL          EXCEPTION;
   L_program         VARCHAR2(64) := 'ORDER_CALC_SQL.TOTAL_COSTS';

   cursor C_COST is
      select SUM(NVL(unit_cost, 0) * NVL(qty_ordered, 0)),
             SUM(NVL(unit_cost, 0) * DECODE(SIGN(nvl(qty_ordered,0)-
                                            NVL(qty_received, 0 )),-1, 0,
                                            NVL(qty_ordered, 0) -
                                            NVL(qty_received, 0 ))),
             SUM(NVL(unit_cost, 0) * NVL(qty_cancelled, 0)),
             SUM(NVL(unit_cost, 0) * NVL(qty_prescaled, 0))
        from ordloc
       where order_no = I_order_no;

   cursor C_LOCATION_ORDER is
      select SUM(NVL(unit_cost, 0) * NVL(qty_ordered, 0)),
             SUM(NVL(unit_cost, 0) * NVL(qty_prescaled, 0))
        from ordloc
       where order_no   = I_order_no
         and location   = I_location;

   cursor C_ITEM_ORDER is
      select SUM(NVL(unit_cost, 0) * NVL(qty_ordered, 0)),
             SUM(NVL(unit_cost, 0) * NVL(qty_prescaled, 0))
        from ordloc
       where order_no = I_order_no
         and item     = I_item;

BEGIN
   if I_order_no is NOT NULL then
      if I_item is NULL and I_location is NULL then
         SQL_LIB.SET_MARK('OPEN','C_COST','ORDLOC',NULL);
         open C_COST;
         SQL_LIB.SET_MARK('FETCH','C_COST','ORDLOC',NULL);
         fetch C_COST into O_total_cost_ord,
                           O_outstand_cost_ord,
                           O_cancel_cost_ord,
                           O_prescale_cost_ord;

         if C_COST%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('ORDER_NOT_FOUND',
                                                  TO_CHAR(I_order_no),
                                                  NULL,
                                                  NULL);
            SQL_LIB.SET_MARK('CLOSE','C_COST','ORDLOC',NULL);
            close C_COST;
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE','C_COST','ORDLOC',NULL);
         close C_COST;

         return TRUE;
      end if;
      ---
      if I_item is NULL and I_location is NOT NULL then
         SQL_LIB.SET_MARK('OPEN','C_LOCATION_ORDER','ORDLOC',NULL);
         open C_LOCATION_ORDER;
         SQL_LIB.SET_MARK('FETCH','C_LOCATION_ORDER','ORDLOC',NULL);
         fetch C_LOCATION_ORDER into O_total_cost_ord,
                                     O_prescale_cost_ord;

         if C_LOCATION_ORDER%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('ORDER_NOT_FOUND',
                                                  TO_CHAR(I_order_no),
                                                  NULL,
                                                  NULL);
            SQL_LIB.SET_MARK('CLOSE','C_LOCATION_ORDER','ORDLOC',NULL);
            close C_LOCATION_ORDER;
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE','C_LOCATION_ORDER','ORDLOC',NULL);
         close C_LOCATION_ORDER;

         return TRUE;
      end if;
      ---
      if I_item is NOT NULL and I_location is NULL then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_ORDER','ORDLOC',NULL);
         open C_ITEM_ORDER;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_ORDER','ORDLOC',NULL);
         fetch C_ITEM_ORDER into O_total_cost_ord,
                                 O_prescale_cost_ord;

         if C_ITEM_ORDER%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('ORDER_NOT_FOUND',
                                                  TO_CHAR(I_order_no),
                                                  NULL,
                                                  NULL);
            SQL_LIB.SET_MARK('CLOSE','C_ITEM_ORDER','ORDLOC',NULL);
            close C_ITEM_ORDER;
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_ORDER','ORDLOC',NULL);
         close C_ITEM_ORDER;

         return TRUE;
      end if;
      ---
   else
      O_error_message := sql_lib.create_msg('NO_ORD_TO_SEARCH',
                                            TO_CHAR(I_order_no),
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END TOTAL_COSTS;
-------------------------------------------------------------------------
FUNCTION BRKT_PCT_OFF(O_error_message   IN OUT   VARCHAR2,
                      O_brkt_pct_off    IN OUT   NUMBER,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   EXIT_ALL             EXCEPTION;
   L_program            VARCHAR2(64) := 'ORDER_CALC_SQL.BRKT_PCT_OFF';
   L_ordloc_cost        ORDLOC.UNIT_COST%TYPE;
   L_item_supplier_cost ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_supplier           SUPS.SUPPLIER%TYPE    := NULL;
   L_status             ORDHEAD.STATUS%TYPE   := NULL;

   cursor C_ORDER_SUPPLIER is
      select supplier,
             status
        from ordhead
       where ordhead.order_no = I_order_no;

   cursor C_COST is
      select SUM(NVL(o.unit_cost, 0) * NVL(o.qty_ordered, 0)),
             SUM(NVL(i.unit_cost, 0) * NVL(o.qty_ordered, 0))
        from ordloc o,
             ordsku s,
             item_supp_country_loc i
       where o.item              = s.item
         and s.item              = i.item
         and i.supplier          = L_supplier
         and s.origin_country_id = i.origin_country_id
         and s.order_no          = o.order_no
         and s.order_no          = I_order_no
         and i.loc               = o.location;

   cursor C_OLD_COST is
      select SUM(NVL(unit_cost, 0) * NVL(qty_ordered, 0)),
             SUM(NVL(unit_cost_init, 0) * NVL(qty_ordered, 0))
        from ordloc
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ORDER_SUPPLIER','ORDHEAD',NULL);
   open C_ORDER_SUPPLIER;
   SQL_LIB.SET_MARK('FETCH','C_ORDER_SUPPLIER','ORDHEAD',NULL);
   fetch C_ORDER_SUPPLIER into L_supplier,
                               L_status;
   SQL_LIB.SET_MARK('CLOSE','C_ORDER_SUPPLIER','ORDHEAD',NULL);
   close C_ORDER_SUPPLIER;
   ---
   if L_status = 'W' then
      SQL_LIB.SET_MARK('OPEN','C_COST','ORDLOC,ORDSKU,ITEM_SUPP_COUNTRY',NULL);
      open C_COST;
      SQL_LIB.SET_MARK('FETCH','C_COST','ORDLOC,ORDSKU,ITEM_SUPP_COUNTRY',NULL);
      fetch C_COST into L_ordloc_cost,
                        L_item_supplier_cost;
      SQL_LIB.SET_MARK('CLOSE','C_COST','ORDLOC,ORDSKU,ITEM_SUPP_COUNTRY',NULL);
      close C_COST;
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          L_supplier,
                                          'V',
                                          NULL,
                                          I_order_no,
                                          'O',
                                          NULL,
                                          L_item_supplier_cost,
                                          L_item_supplier_cost,
                                          'N',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
   else
      SQL_LIB.SET_MARK('OPEN','C_OLD_COST','ORDLOC',NULL);
      open C_OLD_COST;
      SQL_LIB.SET_MARK('FETCH','C_OLD_COST','ORDLOC',NULL);
      fetch C_OLD_COST into L_ordloc_cost,
                            L_item_supplier_cost;
      SQL_LIB.SET_MARK('CLOSE','C_OLD_COST','ORDLOC',NULL);
      close C_OLD_COST;
   end if;
   ---
   if L_item_supplier_cost = 0 then
      O_brkt_pct_off := NULL;
   else
      O_brkt_pct_off := -1 * (((L_ordloc_cost - L_item_supplier_cost ) /
                                                L_item_supplier_cost) * 100);
   end if;
   if O_brkt_pct_off <= 0 then
      O_brkt_pct_off := 0;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BRKT_PCT_OFF;
--------------------------------------------------------------------------
FUNCTION ITEM_LOC_COSTS(O_error_message            IN OUT   VARCHAR2,
                        O_landed_cost_ord          IN OUT   NUMBER,
                        O_landed_cost_prim         IN OUT   NUMBER,
                        O_total_landed_cost_ord    IN OUT   NUMBER,
                        O_total_landed_cost_prim   IN OUT   NUMBER,
                        O_expense_ord              IN OUT   NUMBER,
                        O_expense_prim             IN OUT   NUMBER,
                        O_total_expense_ord        IN OUT   NUMBER,
                        O_total_expense_prim       IN OUT   NUMBER,
                        O_duty_ord                 IN OUT   NUMBER,
                        O_duty_prim                IN OUT   NUMBER,
                        O_total_duty_ord           IN OUT   NUMBER,
                        O_total_duty_prim          IN OUT   NUMBER,
                        O_qty                      IN OUT   ORDLOC.QTY_ORDERED%TYPE,
                        I_order_no                 IN       ORDHEAD.ORDER_NO%TYPE,
                        I_exchange_rate            IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                        I_currency_code            IN       ORDHEAD.CURRENCY_CODE%TYPE,
                        I_item                     IN       ITEM_MASTER.ITEM%TYPE,
                        I_location                 IN       ORDLOC.LOCATION%TYPE,
                        I_loc_type                 IN       ORDLOC.LOC_TYPE%TYPE,
                        I_order_qty                IN       ORDLOC.QTY_ORDERED%TYPE,
                        I_supplier                 IN       SUPS.SUPPLIER%TYPE,
                        I_origin_country           IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                        I_import_country           IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(40) := 'ORDER_CALC_SQL.ITEM_LOC_COSTS';
   L_exists             BOOLEAN;
   L_zone_id            COST_ZONE_GROUP_LOC.ZONE_ID%TYPE;
   L_exp_currency       CURRENCIES.CURRENCY_CODE%TYPE;
   L_dty_currency       CURRENCIES.CURRENCY_CODE%TYPE;
   L_primary_currency   CURRENCIES.CURRENCY_CODE%TYPE;
   L_import_country_id  ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   L_order_total_exp    NUMBER := 0;
   L_order_total_dty    NUMBER := 0;
   L_order_total_elc    NUMBER := 0;
   L_order_total_qty    ORDLOC.QTY_ORDERED%TYPE := 0;
   L_cost_zone_group_id ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE;
   L_exchange_rate      ORDHEAD.EXCHANGE_RATE%TYPE;
   L_exchange_rate_exp  ORDHEAD.EXCHANGE_RATE%TYPE;
   L_currency_code      ORDHEAD.CURRENCY_CODE%TYPE;
   L_landed_cost        NUMBER := 0;
   L_expense            NUMBER := 0;
   L_duty               NUMBER := 0;
   L_qty                ORDLOC.QTY_ORDERED%TYPE;
   L_pack_ind           ITEM_MASTER.PACK_IND%TYPE;
   L_pack_type          ITEM_MASTER.PACK_TYPE%TYPE := 'V';
   L_location           ORDLOC.LOCATION%TYPE;
   L_origin_country_id  COUNTRY.COUNTRY_ID%TYPE;
   L_sellable_ind       ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind      ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_exchange_type      CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;
   L_cost_ord           ORDLOC.UNIT_COST%TYPE ;
   L_ord_exchange_rate  ORDHEAD.EXCHANGE_RATE%TYPE;

   cursor C_ORDLOC is
      select location,
             DECODE(qty_ordered,0,nvl(qty_received,0),qty_ordered) qty_ordered
        from ordloc
       where order_no = I_order_no
         and item     = I_item;

   cursor C_IMPORT_COUNTRY is
      select import_country_id
        from ordhead
       where order_no = I_order_no;

   cursor C_CHECK_PO_EXCHANGE_TYPE is
      select r.exchange_type
        from currencies c,
             currency_rates r
       where c.currency_code  = L_dty_currency
         and c.currency_code  = r.currency_code
         and r.exchange_type  = 'P'
         and r.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.exchange_type   = 'P'
                                    and cr.currency_code   = L_dty_currency
                                    and cr.effective_date <= GET_VDATE);

BEGIN
   O_landed_cost_ord        := 0;
   O_landed_cost_prim       := 0;
   O_total_landed_cost_ord  := 0;
   O_total_landed_cost_prim := 0;
   O_expense_ord            := 0;
   O_expense_prim           := 0;
   O_total_duty_ord         := 0;
   O_total_duty_prim        := 0;
   O_qty                    := 0;
   ---
   if I_order_qty = 0 then
      return TRUE;
   end if;
   ---
   if (I_exchange_rate is NULL) or (I_currency_code is NULL) then
      if ORDER_ATTRIB_SQL.GET_CURRENCY_RATE (O_error_message,
                                             L_currency_code,
                                             L_exchange_rate,
                                             I_order_no) = FALSE then
         return FALSE;
      end if;
   else
      L_currency_code := I_currency_code;
      L_exchange_rate := I_exchange_rate;
   end if;
   ---
   -- Only get elc_ind if LP_elc_ind not populated already  --
   if LP_elc_ind is NULL then
      if SYSTEM_OPTIONS_SQL.GET_ELC_IND (O_error_message,
                                         LP_elc_ind) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_import_country is NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_IMPORT_COUNTRY', 'ORDHEAD', TO_CHAR(I_order_no));
      open C_IMPORT_COUNTRY;
      SQL_LIB.SET_MARK('FETCH', 'C_IMPORT_COUNTRY', 'ORDHEAD', TO_CHAR(I_order_no));
      fetch C_IMPORT_COUNTRY into L_import_country_id;
      if C_IMPORT_COUNTRY%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_IMPORT_CO',
                                               'Order no: '|| I_order_no, NULL, NULL);
         SQL_LIB.SET_MARK('CLOSE', 'C_IMPORT_COUNTRY', 'ORDHEAD', TO_CHAR(I_order_no));
         close C_IMPORT_COUNTRY;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_IMPORT_COUNTRY', 'ORDHEAD', TO_CHAR(I_order_no));
      close C_IMPORT_COUNTRY;
   else
      L_import_country_id := I_import_country;
   end if;
   ---
   if I_origin_country is NULL then
      if ORDER_ITEM_ATTRIB_SQL.GET_ORIGIN_COUNTRY (O_error_message,
                                                   L_exists,
                                                   L_origin_country_id,
                                                   I_order_no,
                                                   I_item) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists != TRUE then
         O_error_message := SQL_LIB.CREATE_MSG ('ERR_RETRIEVE_ORIGIN_CO',
                                                'Order no: '|| I_order_no,NULL,NULL);
         return FALSE;
      end if;
   else
      L_origin_country_id := I_origin_country;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE (O_error_message,
                                        L_primary_currency) = FALSE then
      return FALSE;
   end if;
   ---
   if I_location is NOT NULL then
      if LP_elc_ind = 'Y' then
	     ---
         if L_order_total_elc is null then
               L_order_total_elc := 0;
         end if;
         ---
         if L_order_total_exp is null then
             L_order_total_exp := 0;
         end if;
         ---
         if L_order_total_dty is null then
             L_order_total_dty := 0;
         end if;
		 ---
         if ELC_CALC_SQL.CALC_TOTALS (O_error_message,
                                      L_landed_cost,
                                      L_expense,
                                      L_exp_currency,
                                      L_exchange_rate_exp,
                                      L_duty,
                                      L_dty_currency,
                                      I_order_no,
                                      I_item,
                                      NULL,
                                      NULL,
                                      I_location,
                                      I_supplier,
                                      L_origin_country_id,
                                      L_import_country_id,
                                      NULL) = FALSE then
            return FALSE;
         end if;
         ---
         --- processing for expense fields
         if CURRENCY_SQL.CONVERT (O_error_message,
                                  L_expense,
                                  L_exp_currency,
                                  L_primary_currency,
                                  L_expense,
                                  'N',
                                  NULL,
                                  NULL,
                                  L_exchange_rate_exp,
                                  NULL) = FALSE then
            return FALSE;
         end if;
         O_expense_prim       := L_expense;
         O_total_expense_prim := O_expense_prim * I_order_qty;
         ---
         if CURRENCY_SQL.CONVERT (O_error_message,
                                  L_expense,
                                  L_primary_currency,
                                  L_currency_code,
                                  O_expense_ord,
                                  'N',
                                  NULL,
                                  NULL,
                                  NULL,
                                  L_exchange_rate) = FALSE then
            return FALSE;
         end if;
         O_total_expense_ord := O_expense_ord * I_order_qty;
         ---
         --- processing for duty fields
         if L_currency_code = L_dty_currency then
            L_ord_exchange_rate := L_exchange_rate;
            L_exchange_type     := NULL;
         else
            L_ord_exchange_rate := NULL;
            SQL_LIB.SET_MARK('OPEN',
                             'C_CHECK_PO_EXCHANGE_TYPE',
                             'CURRENCY_RATES',
                             'Currency: '||L_dty_currency);
            open C_CHECK_PO_EXCHANGE_TYPE;

            SQL_LIB.SET_MARK('FETCH',
                             'C_CHECK_PO_EXCHANGE_TYPE',
                             'CURRENCY_RATES',
                             'Currency: '||L_dty_currency);
            fetch C_CHECK_PO_EXCHANGE_TYPE into L_exchange_type;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHECK_PO_EXCHANGE_TYPE',
                             'CURRENCY_RATES',
                             'Currency: '||L_dty_currency);
            close C_CHECK_PO_EXCHANGE_TYPE;
         end if;
         if CURRENCY_SQL.CONVERT (O_error_message,
                                  L_duty,
                                  L_dty_currency,
                                  L_primary_currency,
                                  L_duty,
                                  'N',
                                  NULL,
                                  L_exchange_type,
                                  L_ord_exchange_rate,
                                  NULL,
                                  'N') = FALSE then
            return FALSE;
         end if;
         O_duty_prim       := L_duty;
         O_total_duty_prim := O_duty_prim * I_order_qty;
         ---
         if CURRENCY_SQL.CONVERT (O_error_message,
                                  L_duty,
                                  L_primary_currency,
                                  L_currency_code,
                                  O_duty_ord,
                                  'N',
                                  NULL,
                                  NULL,
                                  NULL,
                                  L_exchange_rate,
                                  'N') = FALSE then
            return FALSE;
         end if;
         ---
         O_total_duty_ord := O_duty_ord * I_order_qty;
      else
         ---
         -- If ELC is 'N', need to retrieve landed cost which is simply the
         -- order cost.
         ---
         if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(O_error_message,
                                                L_exists,
                                                L_landed_cost,
                                                I_order_no,
                                                I_item,
                                                NULL,
                                                L_location) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- processing for landed cost fields
      if LP_elc_ind = 'Y' then
         O_landed_cost_prim       := L_landed_cost;
         O_total_landed_cost_prim := O_landed_cost_prim * I_order_qty;
         if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(O_error_message,
                                                L_exists,
                                                L_cost_ord,
                                                I_order_no,
                                                I_item,
                                                NULL,
                                                L_location) = FALSE then
            return FALSE;
         end if;
         O_landed_cost_ord        := L_cost_ord+ O_duty_ord + O_expense_ord ;
         O_total_landed_cost_ord  := O_landed_cost_ord*I_order_qty ;
      else
         O_landed_cost_ord        :=L_landed_cost;
         O_total_landed_cost_ord  := O_landed_cost_ord * I_order_qty;
         if CURRENCY_SQL.CONVERT (O_error_message,
                                  L_landed_cost,
                                  L_currency_code  ,                             
                                  L_primary_currency,
                                  O_landed_cost_prim,
                                  'N',
                                  NULL,
                                  NULL,
                                  L_exchange_rate,
                                  NULL) = FALSE then
            return FALSE;
         end if;
         O_total_landed_cost_prim := O_landed_cost_prim * I_order_qty;
      end if;

   else -- if I_location is null
      FOR C_ORDLOC_REC IN C_ORDLOC LOOP
         L_location := C_ORDLOC_REC.location;
         L_qty      := C_ORDLOC_REC.qty_ordered;
         ---

         if LP_elc_ind = 'Y' then
            if ELC_CALC_SQL.CALC_TOTALS (O_error_message,
                                         L_landed_cost,
                                         L_expense,
                                         L_exp_currency,
                                         L_exchange_rate_exp,
                                         L_duty,
                                         L_dty_currency,
                                         I_order_no,
                                         I_item,
                                         NULL,
                                         NULL,
                                         L_location,
                                         I_supplier,
                                         L_origin_country_id,
                                         L_import_country_id,
                                         NULL) = FALSE then
               return FALSE;
            end if;
            ---
            if CURRENCY_SQL.CONVERT (O_error_message,
                                     L_expense,
                                     L_exp_currency,
                                     L_primary_currency,
                                     L_expense,
                                     'N',
                                     NULL,
                                     NULL,
                                     L_exchange_rate_exp,
                                     NULL) = FALSE then
               return FALSE;
            end if;
            ---
            if L_currency_code = L_dty_currency then
               L_ord_exchange_rate := L_exchange_rate;
               L_exchange_type     := NULL;
            else 
                L_ord_exchange_rate := NULL;
                SQL_LIB.SET_MARK('OPEN',
                                 'C_CHECK_PO_EXCHANGE_TYPE',
                                 'CURRENCY_RATES',
                                 'Currency: '||L_dty_currency);
                open C_CHECK_PO_EXCHANGE_TYPE;
                
                SQL_LIB.SET_MARK('FETCH',
                                 'C_CHECK_PO_EXCHANGE_TYPE',
                                 'CURRENCY_RATES',
                                 'Currency: '||L_dty_currency);
                fetch C_CHECK_PO_EXCHANGE_TYPE into L_exchange_type;
                
                SQL_LIB.SET_MARK('CLOSE',
                                 'C_CHECK_PO_EXCHANGE_TYPE',
                                 'CURRENCY_RATES',
                                 'Currency: '||L_dty_currency);
                close C_CHECK_PO_EXCHANGE_TYPE;
            end if;
            if CURRENCY_SQL.CONVERT (O_error_message,
                                     L_duty,
                                     L_dty_currency,
                                     L_primary_currency,
                                     L_duty,
                                     'N',
                                     NULL,
                                     L_exchange_type,
                                     L_ord_exchange_rate,
                                     NULL,
                                    'N') = FALSE then
               return FALSE;
            end if;
            ---

            L_order_total_elc := L_order_total_elc + (L_landed_cost * L_qty);
            L_order_total_exp := L_order_total_exp + (L_expense     * L_qty);
            L_order_total_dty := L_order_total_dty + (L_duty        * L_qty);

         else
            ---
            -- If ELC is 'N', need to retrieve landed cost which is simply the
            -- order cost.
            ---
            if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(O_error_message,
                                                   L_exists,
                                                   L_landed_cost,
                                                   I_order_no,
                                                   I_item,
                                                   NULL,
                                                   L_location) = FALSE then
               return FALSE;
            end if;
            ---
            L_order_total_elc := L_order_total_elc + (L_landed_cost * L_qty);
         end if;
         L_order_total_qty := L_order_total_qty + L_qty;
      END LOOP;
      ---
      if L_order_total_qty = 0 then
         O_landed_cost_ord        := 0;
         O_landed_cost_prim       := 0;
         O_total_landed_cost_ord  := 0;
         O_total_landed_cost_prim := 0;
         O_expense_ord            := 0;
         O_expense_prim           := 0;
         O_total_duty_ord         := 0;
         O_total_duty_prim        := 0;
         O_qty                    := 0;
         return TRUE;
      end if;
      ---
      -- processing landed cost fields (need to do this regardless of the ELC ind.
      -- when ELC ind is 'N', landed cost is equal to the order cost.
      ---
      if LP_elc_ind = 'Y' then
         ---
         --- processing expense fields
         O_total_expense_prim := L_order_total_exp;
         O_expense_prim       := O_total_expense_prim / L_order_total_qty;
         ---
         if CURRENCY_SQL.CONVERT (O_error_message,
                                  L_order_total_exp,
                                  L_primary_currency,
                                  L_currency_code,
                                  O_total_expense_ord,
                                  'N',
                                  NULL,
                                  NULL,
                                  NULL,
                                  L_exchange_rate) = FALSE then
            return FALSE;
         end if;
         O_expense_ord := O_total_expense_ord / L_order_total_qty;
         ---
         --- processing duty fields
         O_total_duty_prim := L_order_total_dty;
         O_duty_prim       := O_total_duty_prim / L_order_total_qty;
         ---
         if CURRENCY_SQL.CONVERT (O_error_message,
                                  L_order_total_dty,
                                  L_primary_currency,
                                  L_currency_code,
                                  O_total_duty_ord,
                                  'N',
                                  NULL,
                                  NULL,
                                  NULL,
                                  L_exchange_rate,
                                  'N') = FALSE then
            return FALSE;
         end if;
         O_duty_ord := O_total_duty_ord / L_order_total_qty;
         ---
         if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(O_error_message,
                                                L_exists,
                                                L_cost_ord,
                                                I_order_no,
                                                I_item,
                                                NULL,
                                                NULL) = FALSE then
            return FALSE;
         end if;
         O_total_landed_cost_prim := L_order_total_elc;
         O_landed_cost_prim       := O_total_landed_cost_prim / L_order_total_qty;
         O_landed_cost_ord        := L_cost_ord+ O_duty_ord + O_expense_ord ;
         O_total_landed_cost_ord  := O_landed_cost_ord*L_order_total_qty ;
      else
         O_total_landed_cost_ord  :=L_order_total_elc ;
         O_landed_cost_ord        := O_total_landed_cost_ord / L_order_total_qty;
          if CURRENCY_SQL.CONVERT (O_error_message,
                                   L_order_total_elc,
                                   L_currency_code,
                                   L_primary_currency,
                                   O_total_landed_cost_prim,
                                   'N',
                                   NULL,
                                   NULL,
                                   L_exchange_rate,
                                   NULL) = FALSE then
             return FALSE;
          end if;
          ---
         O_landed_cost_prim       := O_total_landed_cost_prim / L_order_total_qty;
      end if; --- if elc ind = Y
      O_qty := L_order_total_qty;
   end if; --- I_location is NULL/NOT NULL

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END ITEM_LOC_COSTS;
------------------------------------------------------------------------------------------
FUNCTION CALC_VALUES(O_error_message              IN OUT VARCHAR2,
                     O_total_retail_incl_vat_prim IN OUT ORDLOC.UNIT_RETAIL%TYPE,
                     O_total_retail_excl_vat_prim IN OUT ORDLOC.UNIT_RETAIL%TYPE,
                     O_total_landed_cost_prim     IN OUT NUMBER,
                     I_order_no                   IN     ORDHEAD.ORDER_NO%TYPE,
                     I_item                       IN     ITEM_MASTER.ITEM%TYPE,
                     I_pack_item                  IN     ITEM_MASTER.ITEM%TYPE,
                     I_location                   IN     ORDLOC.LOCATION%TYPE,
                     I_loc_type                   IN     ORDLOC.LOC_TYPE%TYPE,
                     I_supplier                   IN     SUPS.SUPPLIER%TYPE,
                     I_origin_country_id          IN     COUNTRY.COUNTRY_ID%TYPE,
                     I_import_country_id          IN     COUNTRY.COUNTRY_ID%TYPE,
                     I_unit_retail                IN     ORDLOC.UNIT_RETAIL%TYPE,
                     I_unit_cost                  IN     ORDLOC.UNIT_RETAIL%TYPE,
                     I_qty_ordered                IN     ORDLOC.QTY_ORDERED%TYPE,
                     I_elc_ind                    IN     SYSTEM_OPTIONS.ELC_IND%TYPE,
                     I_exchange_rate              IN     CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                     I_currency_code_ord          IN     CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program                     VARCHAR2(64)             := 'ORDER_CALC_SQL.CALC_VALUES';
   L_total_landed_cost_prim      NUMBER                   := 0;
   L_total_retail_incl_vat_prim  ORDLOC.UNIT_RETAIL%TYPE  := 0;
   L_total_retail_excl_vat_prim  ORDLOC.UNIT_RETAIL%TYPE  := 0;
   L_unit_retail_excl_vat        ORDLOC.UNIT_RETAIL%TYPE  := 0;
   L_unit_retail                 ORDLOC.UNIT_RETAIL%TYPE  := 0;
   L_unit_retail_loc             ORDLOC.UNIT_RETAIL%TYPE  := 0;
   L_unit_cost_prim              ORDLOC.UNIT_COST%TYPE    := I_unit_cost;
   L_landed_cost                 NUMBER                   := 0;
   L_duty                        NUMBER;
   L_expense                     NUMBER;
   L_dty_currency                CURRENCIES.CURRENCY_CODE%TYPE;
   L_exp_currency                CURRENCIES.CURRENCY_CODE%TYPE;
   L_currency_code_loc           CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate_exp           CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_store_ind                   VARCHAR2(1);
   L_item                        ITEM_MASTER.ITEM%TYPE;
   L_comp_item                   ITEM_MASTER.ITEM%TYPE;
   L_dept                        ITEM_MASTER.DEPT%TYPE;
   L_class                       ITEM_MASTER.CLASS%TYPE;
   L_subclass                    ITEM_MASTER.SUBCLASS%TYPE;
   L_location                    ORDLOC.LOCATION%TYPE;
   L_loc_type                    ORDLOC.LOC_TYPE%TYPE;

   L_unit_retail_incl_vat_loc    ORDLOC.UNIT_COST%TYPE;
   L_unit_retail_excl_vat_loc    ORDLOC.UNIT_COST%TYPE;
   L_unit_retail_incl_vat_prim   ORDLOC.UNIT_COST%TYPE;
   L_unit_retail_excl_vat_prim   ORDLOC.UNIT_COST%TYPE;

   L_retail_tax_rec              OBJ_TAX_RETAIL_ADD_REMOVE_REC := OBJ_TAX_RETAIL_ADD_REMOVE_REC();
   L_tax_retail_tbl              OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();

BEGIN

   if I_pack_item is NULL then
      L_item := I_item;
   else
      L_item      := I_pack_item;
      L_comp_item := I_item;
   end if;

   if I_location is NOT NULL then
      L_location := I_location;
   end if;

   if I_loc_type is NOT NULL then
      L_loc_type := I_loc_type;
   end if;
   ---
   if I_elc_ind = 'Y' then
      if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                  L_landed_cost,
                                  L_expense,
                                  L_exp_currency,
                                  L_exchange_rate_exp,
                                  L_duty,
                                  L_dty_currency,
                                  I_order_no,
                                  L_item,
                                  L_comp_item,
                                  NULL,
                                  I_location,
                                  I_supplier,
                                  I_origin_country_id,
                                  I_import_country_id,
                                  NULL) = FALSE then
         return FALSE;
      end if;
      ---
      O_total_landed_cost_prim := O_total_landed_cost_prim + (L_landed_cost * I_qty_ordered);
   else
      if CURRENCY_SQL.CONVERT(O_error_message,
                              I_unit_cost,
                              I_currency_code_ord,
                              NULL,
                              L_unit_cost_prim,
                              'C',
                              NULL,
                              NULL,
                              I_exchange_rate,
                              NULL) = FALSE then
         return FALSE;
      end if;
      ---
      O_total_landed_cost_prim := L_unit_cost_prim;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                     L_item,
                                     L_dept,
                                     L_class,
                                     L_subclass) = FALSE then
      return FALSE;
   end if;
   ---

   L_retail_tax_rec.I_item                := L_item;
   L_retail_tax_rec.I_dept                := L_dept;
   L_retail_tax_rec.I_class               := L_class;
   L_retail_tax_rec.I_location            := L_location;
   L_retail_tax_rec.I_loc_type            := L_loc_type;
   L_retail_tax_rec.I_effective_from_date := GET_VDATE;
   L_retail_tax_rec.I_amount              := I_unit_retail;
   ---
   L_tax_retail_tbl.delete;
   L_tax_retail_tbl.extend;
   L_tax_retail_tbl(1) := L_retail_tax_rec;

   -- Get the inclusive and exclusive retail values
   if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                       L_tax_retail_tbl) = FALSE then
      return FALSE;
   end if;

   L_unit_retail_incl_vat_loc := L_tax_retail_tbl(1).O_amount;

   ---
   if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                                       L_tax_retail_tbl) = FALSE then
      return FALSE;
   end if;

   L_unit_retail_excl_vat_loc := L_tax_retail_tbl(1).O_amount;

   -- convert totals to primary currency
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       L_location,
                                       L_loc_type,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       L_unit_retail_incl_vat_loc,
                                       L_unit_retail_incl_vat_prim,
                                       'N',
                                       NULL,
                                       NULL) = FALSE then
         return FALSE;
   end if;
   ---
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       L_location,
                                       L_loc_type,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       L_unit_retail_excl_vat_loc,
                                       L_unit_retail_excl_vat_prim,
                                       'N',
                                       NULL,
                                       NULL) = FALSE then
         return FALSE;
   end if;

   O_total_retail_incl_vat_prim := O_total_retail_incl_vat_prim + (L_unit_retail_incl_vat_prim * I_qty_ordered);
   O_total_retail_excl_vat_prim := O_total_retail_excl_vat_prim + (L_unit_retail_excl_vat_prim * I_qty_ordered);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END CALC_VALUES;
------------------------------------------------------------------------------------------
FUNCTION ORD_ITEM_RETAIL(O_error_message             IN OUT VARCHAR2,
                         O_pct_markup                IN OUT NUMBER,
                         O_unit_retail_incl_vat_prim IN OUT ORDLOC.UNIT_RETAIL%TYPE,
                         O_unit_retail_excl_vat_prim IN OUT ORDLOC.UNIT_RETAIL%TYPE,
                         O_unit_landed_cost_prim     IN OUT NUMBER,
                         I_order_no                  IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item                      IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item                 IN     ITEM_MASTER.ITEM%TYPE,
                         I_exchange_rate             IN     CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                         I_currency_code_ord         IN     CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program                     VARCHAR2(64)             := 'ORDER_CALC_SQL.ORD_ITEM_RETAIL';
   L_total_landed_cost_prim      NUMBER                   := 0;
   L_total_retail_incl_vat_prim  ORDLOC.UNIT_RETAIL%TYPE  := 0;
   L_total_retail_excl_vat_prim  ORDLOC.UNIT_RETAIL%TYPE  := 0;
   L_unit_retail                 ORDLOC.UNIT_RETAIL%TYPE  := 0;
   L_selling_unit_retail         ORDLOC.UNIT_RETAIL%TYPE  := 0;
   L_selling_uom                 UOM_CLASS.UOM%TYPE;
   L_av_cost                     ITEM_LOC_SOH.AV_COST%TYPE;
   L_unit_cost                   ORDLOC.UNIT_COST%TYPE;
   L_cost                        ORDLOC.UNIT_COST%TYPE;
   L_unit_retail_excl_vat        ORDLOC.UNIT_RETAIL%TYPE;

   L_item                        ITEM_MASTER.ITEM%TYPE;
   L_qty                         ORDLOC.QTY_ORDERED%TYPE;
   L_total_qty                   ORDLOC.QTY_ORDERED%TYPE;
   L_packsku_qty                 ORDLOC.QTY_ORDERED%TYPE;
   L_loc_type                    ORDLOC.LOC_TYPE%TYPE;
   L_location                    ORDLOC.LOCATION%TYPE;
   L_exchange_rate               CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_currency_code_ord           CURRENCIES.CURRENCY_CODE%TYPE        := NULL;
   L_origin_country_id           COUNTRY.COUNTRY_ID%TYPE              := NULL;
   L_import_country_id           COUNTRY.COUNTRY_ID%TYPE              := NULL;
   L_supplier                    SUPS.SUPPLIER%TYPE;
   L_exists                      BOOLEAN;
   ---
   cursor C_GET_INFO is
      select o.import_country_id,
             k.origin_country_id,
             o.supplier,
             s.elc_ind
        from ordhead o,
             ordsku k,
             system_options s
       where o.order_no = I_order_no
         and o.order_no = k.order_no
         and k.item     = NVL(I_pack_item, I_item);

   cursor C_ORDLOC is
      select unit_retail,
             unit_cost,
             qty_ordered,
             location,
             loc_type
        from ordloc
       where order_no = I_order_no
         and item     = L_item;

   cursor C_ORDLOC_PACK is
      select o.qty_ordered * v.qty qty_ordered,
             o.location,
             o.loc_type
        from ordloc o,
             v_packsku_qty v
       where o.order_no = I_order_no
         and o.item     = I_pack_item
         and v.pack_no  = I_pack_item
         and v.item     = I_item;

BEGIN
   O_pct_markup := 0;
   ---
   -- get exchange rate and currency
   ---
   if I_exchange_rate is NULL or I_currency_code_ord is NULL then
      if ORDER_ATTRIB_SQL.GET_CURRENCY_RATE(O_error_message,
                                            L_currency_code_ord,
                                            L_exchange_rate,
                                            I_order_no) = FALSE then
         return FALSE;
      end if;
   else
      L_currency_code_ord := I_currency_code_ord;
      L_exchange_rate     := I_exchange_rate;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_INFO','ORDHEAD,ORDSKU,SYSTEM_OPTIONS',NULL);
   open C_GET_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_INFO','ORDHEAD,ORDSKU,SYSTEM_OPTIONS',NULL);
   fetch C_GET_INFO into L_import_country_id,
                         L_origin_country_id,
                         L_supplier,
                         LP_elc_ind;
   SQL_LIB.SET_MARK('CLOSE','C_GET_INFO','ORDHEAD,ORDSKU,SYSTEM_OPTIONS',NULL);
   close C_GET_INFO;
   ---
   if ORDER_ITEM_ATTRIB_SQL.GET_QTY_ORDERED(O_error_message,
                                            L_exists,
                                            L_total_qty,
                                            I_order_no,
                                            I_item,
                                            I_pack_item) = FALSE then
      return FALSE;
   end if;
   ---
   if I_pack_item is NULL then
      L_item := I_item;
      ---
      FOR C_rec in C_ORDLOC LOOP
         L_qty         := C_rec.qty_ordered;
         L_unit_retail := C_rec.unit_retail;
         L_unit_cost   := C_rec.unit_cost;
         L_loc_type    := C_rec.loc_type;
         L_location    := C_rec.location;
         ---

         -- CALC_VALUES expects unit cost and retail passed in at the order currency
         if CALC_VALUES(O_error_message,
                        L_total_retail_incl_vat_prim,
                        L_total_retail_excl_vat_prim,
                        L_total_landed_cost_prim,
                        I_order_no,
                        I_item,
                        NULL,
                        L_location,
                        L_loc_type,
                        L_supplier,
                        L_origin_country_id,
                        L_import_country_id,
                        L_unit_retail,
                        L_unit_cost,
                        L_qty,
                        LP_elc_ind,
                        L_exchange_rate,
                        L_currency_code_ord) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   else
      L_item := I_pack_item;
      ---
      FOR C_rec in C_ORDLOC_PACK LOOP
         L_qty         := C_rec.qty_ordered;
         L_loc_type    := C_rec.loc_type;
         L_location    := C_rec.location;
         ---
         if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                     I_item,
                                                     L_location,
                                                     L_loc_type,
                                                     L_av_cost,
                                                     L_cost,
                                                     L_unit_retail,
                                                     L_selling_unit_retail,
                                                     L_selling_uom) = FALSE then
            return FALSE;
         end if;
         ---

         -- CALC_VALUES expects unit cost and retail passed in at the order currency
         if CALC_VALUES(O_error_message,
                        L_total_retail_incl_vat_prim,
                        L_total_retail_excl_vat_prim,
                        L_total_landed_cost_prim,
                        I_order_no,
                        I_item,
                        I_pack_item,
                        L_location,
                        L_loc_type,
                        L_supplier,
                        L_origin_country_id,
                        L_import_country_id,
                        L_unit_retail,
                        L_cost,
                        L_qty,
                        LP_elc_ind,
                        L_exchange_rate,
                        L_currency_code_ord) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;
   ---
   if L_total_qty = 0 then
      O_unit_retail_incl_vat_prim    := 0;
      O_unit_retail_excl_vat_prim    := 0;
      O_unit_landed_cost_prim        := 0;
   else
      O_unit_retail_incl_vat_prim := L_total_retail_incl_vat_prim / L_total_qty;
      O_unit_retail_excl_vat_prim := L_total_retail_excl_vat_prim / L_total_qty;
      O_unit_landed_cost_prim     := L_total_landed_cost_prim     / L_total_qty;
   end if;
   ---
   if MARKUP_SQL.CALC_MARKUP_PERCENT(O_error_message,
                                     O_pct_markup,
                                     'R',
                                     O_unit_landed_cost_prim,
                                     O_unit_retail_excl_vat_prim) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END ORD_ITEM_RETAIL;
-----------------------------------------------------------------------------------
FUNCTION ITEM_DIFF_COSTS(O_error_message     IN OUT VARCHAR2,
                         O_nopack_ord_cost   IN OUT ORDLOC.UNIT_COST%TYPE,
                         O_nopack_prim_cost  IN OUT ORDLOC.UNIT_COST%TYPE,
                         O_pack_ord_cost     IN OUT ORDLOC.UNIT_COST%TYPE,
                         O_pack_prim_cost    IN OUT ORDLOC.UNIT_COST%TYPE,
                         I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_diff              IN     ITEM_MASTER.DIFF_1%TYPE)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'ORDER_CALC_SQL.ITEM_DIFF_COSTS';
   L_supplier            ORDHEAD.SUPPLIER%TYPE         := NULL;
   L_currency_code       ORDHEAD.CURRENCY_CODE%TYPE    := NULL;
   L_unit_cost           ORDLOC.UNIT_COST%TYPE;
   L_qty_ordered         ORDLOC.QTY_ORDERED%TYPE;
   L_nopack_ord_cost     ORDLOC.UNIT_COST%TYPE;
   L_vdate               PERIOD.VDATE%TYPE;
   L_pack_no             ITEM_MASTER.ITEM%TYPE;
   L_sku                 ITEM_MASTER.ITEM%TYPE;
   L_pack_qty            V_PACKSKU_QTY.QTY%TYPE;
   L_origin_country_id   COUNTRY.COUNTRY_ID%TYPE;
   L_total_cost_pack     NUMBER;
   L_sum_cost            NUMBER  := 0;
   L_order_cost          NUMBER;

   cursor C_GET_SUP_OC is
      select o.supplier,
             o.currency_code
        from ordhead o
       where o.order_no = I_order_no;

   cursor C_TOT_UNIT_COST is
      select sum(o.qty_ordered * o.unit_cost)
        from item_master i,
             ordloc o
       where o.order_no = I_order_no
         and ((i.item_parent = I_item
               and (i.diff_1 = I_diff or
                    i.diff_2 = I_diff or
                    i.diff_3 = I_diff or
                    i.diff_4 = I_diff))
             or (i.item_grandparent = I_item
                 and (i.diff_1 = I_diff or
                      i.diff_2 = I_diff or
                      i.diff_3 = I_diff or
                      i.diff_4 = I_diff)))
         and i.item = o.item;


   cursor C_TOT_PACK_COST is
      select v.pack_no,
             v.qty,
             v.item,
             l.unit_cost unit_cost,
             o.origin_country_id,
             SUM(l.qty_ordered) qty_ordered
        from v_packsku_qty v,
             ordsku o,
             ordloc l,
             item_master i
       where o.order_no         = I_order_no
         and i.item             = v.item
         and v.pack_no          = o.item
         and o.item             = l.item
         and o.order_no         = l.order_no
         and ((i.item_parent    = I_item
               and (i.diff_1    = I_diff or
                    i.diff_2    = I_diff or
                    i.diff_3    = I_diff or
                    i.diff_4    = I_diff))
             or (i.item_grandparent = I_item
                 and (i.diff_1  = I_diff or
                      i.diff_2  = I_diff or
                      i.diff_3  = I_diff or
                      i.diff_4  = I_diff)))
    group by v.pack_no,
             v.qty,
             v.item,
             l.unit_cost,
             o.origin_country_id;

BEGIN
   --retrieve the supplier and currency from the ordhead table.  They will be used later.
   SQL_LIB.SET_MARK('OPEN', 'C_GET_SUP_OC' , 'ORDHEAD', 'ORDER: ' ||to_char(I_order_no));
   open C_GET_SUP_OC;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_SUP_OC', 'ORDHEAD', 'ORDER: ' ||to_char(I_order_no));
   fetch C_GET_SUP_OC into L_supplier, L_currency_code;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_SUP_OC', 'ORDHEAD', 'ORDER: ' ||to_char(I_order_no));
   close C_GET_SUP_OC;

   --retrieve the total unit cost for O_nopack_ord_cost
   SQL_LIB.SET_MARK('OPEN',
                    'C_TOT_UNIT_COST',
                    'ORDSKU, ORDLOC, ITEM_MASTER',
                    'ORDER: '||to_char(I_order_no)||
                    ', Item: '||I_item||
                    ', Diff: '||I_diff);
   open C_TOT_UNIT_COST;
   SQL_LIB.SET_MARK('FETCH',
                    'C_TOT_UNIT_COST',
                    'ORDSKU, ORDLOC, ITEM_MASTER',
                    'ORDER: '||to_char(I_order_no)||
                    ', Item: '||I_item||
                    ', Diff: '||I_diff);
   fetch C_TOT_UNIT_COST into O_nopack_ord_cost;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_TOT_UNIT_COST',
                    'ORDSKU, ORDLOC, ITEM_MASTER',
                    'ORDER: '||to_char(I_order_no)||
                    ', Item: '||I_item||
                    ', Diff: '||I_diff);
   close C_TOT_UNIT_COST;

   --retrieve the primary unit cost for O_nopack_prim_cost
   if DATES_SQL.GET_VDATE(O_error_message,
                          L_vdate) = FALSE then
      return FALSE;
   end if;
   if CURRENCY_SQL.CONVERT(O_error_message,
                           O_nopack_ord_cost,
                           L_currency_code,
                           NULL,
                           O_nopack_prim_cost,
                           'C',
                           L_vdate,
                           'O') = FALSE then
      return FALSE;
   end if;

   --retrieve the total cost of the pack components for O_pack_ord_cost
   FOR recs in C_TOT_PACK_COST LOOP
      L_pack_no           := recs.pack_no;
      L_pack_qty          := recs.qty;
      L_sku               := recs.item;
      L_unit_cost         := recs.unit_cost;
      L_origin_country_id := recs.origin_country_id;
      L_qty_ordered       := recs.qty_ordered;

      if PACKITEM_ATTRIB_SQL.GET_ORDER_COMP_COST(O_error_message,
                                                 L_pack_no,
                                                 L_unit_cost,
                                                 L_sku,
                                                 L_supplier,
                                                 L_origin_country_id,
                                                 L_order_cost) = FALSE then
         return FALSE;
      end if;
      L_total_cost_pack := (L_order_cost) * (L_qty_ordered) * (L_pack_qty);
      L_sum_cost := (L_sum_cost) + (L_total_cost_pack);
   END LOOP;
   O_pack_ord_cost := NVL(O_nopack_ord_cost,0) + NVL(L_sum_cost,0);

   --retrieve the primary cost of the pack components for O_pack_prim_cost
   if CURRENCY_SQL.CONVERT(O_error_message,
                           O_pack_ord_cost,
                           L_currency_code,
                           NULL,
                           O_pack_prim_cost,
                           'C',
                           L_vdate,
                           'O') = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END ITEM_DIFF_COSTS;
-----------------------------------------------------------------------------------
FUNCTION SUPPLIER_COST_UNITS(O_error_message      IN OUT VARCHAR2,
                             O_supp_prescale_cost IN OUT ORDLOC.UNIT_COST%TYPE,
                             O_supp_order_cost    IN OUT ORDLOC.UNIT_COST%TYPE,
                             O_prescale_cost      IN OUT ORDLOC.UNIT_COST%TYPE,
                             O_order_cost         IN OUT ORDLOC.UNIT_COST%TYPE,
                             O_qty_prescaled      IN OUT ORDLOC.QTY_PRESCALED%TYPE,
                             O_qty_ordered        IN OUT ORDLOC.QTY_ORDERED%TYPE,
                             O_supp_item_cost     IN OUT ORDLOC.UNIT_COST%TYPE,
                             O_ord_item_cost      IN OUT ORDLOC.UNIT_COST%TYPE,
                             I_order_no           IN     ORDHEAD.ORDER_NO%TYPE,
                             I_item               IN     ITEM_MASTER.ITEM%TYPE,
                             I_location           IN     ORDLOC.LOCATION%TYPE,
                             I_alloc_no           IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                             I_supplier           IN     SUPS.SUPPLIER%TYPE,
                             I_order_status       IN     ORDHEAD.STATUS%TYPE,
                             I_ord_currency_code  IN     CURRENCIES.CURRENCY_CODE%TYPE,
                             I_ord_exchange_rate  IN     CURRENCY_RATES.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'ORDER_CALC_SQL.SUPPLIER_COST_UNITS';
   L_order_status        ORDHEAD.STATUS%TYPE;
   L_supplier            SUPS.SUPPLIER%TYPE;
   L_ord_currency_code   CURRENCIES.CURRENCY_CODE%TYPE;
   L_ord_exchange_rate   CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_supp_currency_code  CURRENCIES.CURRENCY_CODE%TYPE;
   L_loc_type            ORDLOC.LOC_TYPE%TYPE;

   cursor C_ORDHEAD is
      select status,
             supplier,
             currency_code,
             exchange_rate
        from ordhead
       where order_no = I_order_no;
   ---

   cursor C_ORD_UNAPP is
      select SUM(qty_prescaled * i.unit_cost),
             SUM(qty_ordered   * i.unit_cost),
             SUM(qty_prescaled * o.unit_cost),
             SUM(qty_ordered   * o.unit_cost),
             SUM(qty_prescaled),
             SUM(qty_ordered)
        from ordloc o,
             ordsku s,
             item_supp_country_loc i
       where o.order_no          = I_order_no
         and o.order_no          = s.order_no
         and o.item              = i.item
         and o.item              = s.item
         and i.supplier          = L_supplier
         and i.loc               = o.location
         and i.origin_country_id = s.origin_country_id;
   ---
   cursor C_ORD_APP is
      select SUM(qty_prescaled * unit_cost_init),
             SUM(qty_ordered   * unit_cost_init),
             SUM(qty_prescaled * unit_cost),
             SUM(qty_ordered   * unit_cost),
             SUM(qty_prescaled),
             SUM(qty_ordered)
        from ordloc
       where order_no    = I_order_no;
   ---
   cursor C_ORD_ITEM_UNAPP is
      select SUM(qty_prescaled * i.unit_cost),
             SUM(qty_ordered   * i.unit_cost),
             SUM(qty_prescaled * o.unit_cost),
             SUM(qty_ordered   * o.unit_cost),
             SUM(qty_prescaled),
             SUM(qty_ordered),
             i.unit_cost,
             o.unit_cost
        from ordloc o,
             ordsku s,
             item_supp_country_loc i
       where o.order_no          = I_order_no
         and o.item              = I_item
         and o.order_no          = s.order_no
         and o.item              = s.item
         and o.item              = i.item
         and i.supplier          = L_supplier
         and o.location          = i.loc
         and i.origin_country_id = s.origin_country_id
       group by i.unit_cost,
                o.unit_cost;
   ---

   cursor C_ORD_ITEM_APP is
      select SUM(qty_prescaled * unit_cost_init),
             SUM(qty_ordered   * unit_cost_init),
             SUM(qty_prescaled * unit_cost),
             SUM(qty_ordered   * unit_cost),
             SUM(qty_prescaled),
             SUM(qty_ordered),
             unit_cost_init,
             unit_cost
        from ordloc
       where order_no = I_order_no
         and item     = I_item
       group by unit_cost_init,
                unit_cost;
   ---
   cursor C_ORD_LOC_UNAPP is
      select SUM(qty_prescaled * i.unit_cost),
             SUM(qty_ordered   * i.unit_cost),
             SUM(qty_prescaled * o.unit_cost),
             SUM(qty_ordered   * o.unit_cost),
             SUM(qty_prescaled),
             SUM(qty_ordered)
        from ordloc o,
             ordsku s,
             item_supp_country_loc i
       where o.order_no          = I_order_no
         and o.location          = I_location
         and o.order_no          = s.order_no
         and o.item              = s.item
         and o.item              = i.item
         and i.supplier          = I_supplier
         and o.location          = i.loc
         and i.origin_country_id = s.origin_country_id
    group by 1, 2, 3, 4, 5, 6
      UNION
      select SUM(qty_prescaled * i.unit_cost),
             SUM(qty_ordered   * i.unit_cost),
             SUM(qty_prescaled * o.unit_cost),
             SUM(qty_ordered   * o.unit_cost),
             SUM(qty_prescaled),
             SUM(qty_ordered)
        from ordloc o,
             ordsku s,
             item_supp_country_loc i,
             wh
       where o.order_no          = I_order_no
         and o.order_no          = s.order_no
         and o.item              = s.item
         and o.item              = i.item
         and i.supplier          = I_supplier
         and o.location          = i.loc
         and (wh.physical_wh     = I_location
              and wh.stockholding_ind = 'Y')
         and i.loc               = wh.wh
         and i.origin_country_id = s.origin_country_id
    group by 7, 8, 9, 10, 11, 12;
  ---

   cursor C_ORD_LOC_APP is
      select SUM(qty_prescaled * unit_cost_init),
             SUM(qty_ordered   * unit_cost_init),
             SUM(qty_prescaled * unit_cost),
             SUM(qty_ordered   * unit_cost),
             SUM(qty_prescaled),
             SUM(qty_ordered)
        from ordloc o
       where o.order_no            = I_order_no
         and o.location            = I_location
    group by 1, 2, 3, 4, 5, 6
      UNION
      select SUM(qty_prescaled * unit_cost_init),
             SUM(qty_ordered   * unit_cost_init),
             SUM(qty_prescaled * unit_cost),
             SUM(qty_ordered   * unit_cost),
             SUM(qty_prescaled),
             SUM(qty_ordered)
        from ordloc o,
             wh
       where o.order_no            = I_order_no
         and o.location            = wh.wh
         and (wh.physical_wh       = I_location
             and wh.stockholding_ind = 'Y')
    group by 7, 8, 9, 10, 11, 12;

   ---
   cursor C_ORD_ITEM_LOC_UNAPP is
      select qty_prescaled * i.unit_cost,
             qty_ordered   * i.unit_cost,
             qty_prescaled * o.unit_cost,
             qty_ordered   * o.unit_cost,
             qty_prescaled,
             qty_ordered,
             i.unit_cost,
             o.unit_cost
       from  ordloc o,
             ordsku s,
             item_supp_country_loc i
       where o.order_no          = I_order_no
         and o.item              = I_item
         and o.location          = I_location
         and o.order_no          = s.order_no
         and o.item              = s.item
         and o.item              = i.item
         and o.location          = i.loc
         and i.supplier          = I_supplier
         and i.origin_country_id = s.origin_country_id
       UNION
      select qty_prescaled * i.unit_cost,
             qty_ordered   * i.unit_cost,
             qty_prescaled * o.unit_cost,
             qty_ordered   * o.unit_cost,
             qty_prescaled,
             qty_ordered,
             i.unit_cost,
             o.unit_cost
       from ordloc o,
            ordsku s,
            item_supp_country_loc i,
            wh
      where o.order_no          = I_order_no
        and o.item              = I_item
        and o.order_no          = s.order_no
        and o.item              = s.item
        and o.item              = i.item
        and o.location          = i.loc
        and i.supplier          = I_supplier
        and (wh.physical_wh     = I_location
             and wh.stockholding_ind = 'Y')
        and o.location          = wh.wh
        and i.origin_country_id = s.origin_country_id;
   ---
   cursor C_ORD_ITEM_LOC_APP is
      select qty_prescaled * unit_cost_init,
             qty_ordered   * unit_cost_init,
             qty_prescaled * unit_cost,
             qty_ordered   * unit_cost,
             qty_prescaled,
             qty_ordered,
             unit_cost_init,
             unit_cost
        from ordloc o
       where order_no              = I_order_no
         and item                  = I_item
         and location              = I_location
      UNION
      select qty_prescaled * unit_cost_init,
             qty_ordered   * unit_cost_init,
             qty_prescaled * unit_cost,
             qty_ordered   * unit_cost,
             qty_prescaled,
             qty_ordered,
             unit_cost_init,
             unit_cost
        from ordloc o,
             wh
       where o.order_no              = I_order_no
         and o.item                  = I_item
         and (wh.physical_wh         = I_location
             and wh.stockholding_ind = 'Y')
         and o.location              = wh.wh;
   ---
   cursor C_ORD_ALLOC_UNAPP is
      select a.qty_prescaled * i.unit_cost,
             a.qty_allocated * i.unit_cost,
             a.qty_prescaled * o.unit_cost,
             a.qty_allocated * o.unit_cost,
             a.qty_prescaled,
             a.qty_allocated,
             i.unit_cost,
             o.unit_cost
        from alloc_detail a,
             ordloc o,
             ordsku s,
             item_supp_country_loc i
       where a.to_loc            = I_location
         and a.to_loc_type       = 'S'
         and a.alloc_no          = I_alloc_no
         and o.order_no          = I_order_no
         and o.order_no          = s.order_no
         and o.item              = I_item
         and o.item              = i.item
         and o.item              = s.item
         and i.supplier          = L_supplier
         and o.location          = i.loc
         and o.location          = a.to_loc
         and s.origin_country_id = i.origin_country_id;
    ----
   cursor C_ORD_ALLOC_APP is
      select a.qty_prescaled * o.unit_cost_init,
             a.qty_allocated * o.unit_cost_init,
             a.qty_prescaled * o.unit_cost,
             a.qty_allocated * o.unit_cost,
             a.qty_prescaled,
             a.qty_allocated,
             o.unit_cost_init,
             o.unit_cost
        from alloc_detail a,
             ordloc o
       where a.to_loc       = I_location
         and a.to_loc_type  = 'S'
         and a.to_loc       = o.location
         and a.alloc_no     = I_alloc_no
         and o.order_no     = I_order_no
         and o.item         = I_item;

BEGIN

   O_supp_prescale_cost      := NULL;
   O_supp_order_cost         := NULL;
   O_prescale_cost           := NULL;
   O_order_cost              := NULL;
   O_qty_prescaled           := NULL;
   O_qty_ordered             := NULL;
   O_supp_item_cost          := NULL;
   O_ord_item_cost           := NULL;

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_order_status is NULL or I_supplier is NULL or I_ord_currency_code is NULL or I_ord_exchange_rate is NULL then
      SQL_LIB.SET_MARK('OPEN','C_ORDHEAD','ordhead','Order No: '||TO_CHAR(I_order_no));
      open C_ORDHEAD;
      SQL_LIB.SET_MARK('FETCH','C_ORDHEAD','ordhead','Order No: '||TO_CHAR(I_order_no));
      fetch C_ORDHEAD into L_order_status,
                           L_supplier,
                           L_ord_currency_code,
                           L_ord_exchange_rate;
      if C_ORDHEAD%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_ORDHEAD','ordhead','Order No: '||TO_CHAR(I_order_no));
         close C_ORDHEAD;
         raise NO_DATA_FOUND;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_ORDHEAD','ordhead','Order No: '||TO_CHAR(I_order_no));
      close C_ORDHEAD;
   else
      L_order_status      := I_order_status;
      L_supplier          := I_supplier;
      L_ord_currency_code := I_ord_currency_code;
      L_ord_exchange_rate := I_ord_exchange_rate;
   end if;
   ---
   --- for unapproved/unclosed order
   if L_order_status != 'A' and L_order_status != 'C' then
      --- order/alloc/location
      if I_alloc_no is NOT NULL and I_item is NOT NULL and I_location is NOT NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORD_ALLOC_UNAPP',
                          'alloc_detail, ordsku,ordloc,item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Alloc No: '||to_char(I_alloc_no)||
                          ', Item: '||I_item||
                          ', Location: '||to_char(I_location));
         open C_ORD_ALLOC_UNAPP;
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_ORD_ALLOC_UNAPP',
                          'alloc_detail, ordsku,ordloc,item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Alloc No: '||to_char(I_alloc_no)||
                          ', Item: '||I_item||
                          ', Location: '||to_char(I_location));
         fetch C_ORD_ALLOC_UNAPP into O_supp_prescale_cost,
                                      O_supp_order_cost,
                                      O_prescale_cost,
                                      O_order_cost,
                                      O_qty_prescaled,
                                      O_qty_ordered,
                                      O_supp_item_cost,
                                      O_ord_item_cost;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORD_ALLOC_UNAPP',
                          'alloc_detail, ordsku,ordloc,item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Alloc No: '||to_char(I_alloc_no)||
                          ', Item: '||I_item||
                          ', Location: '||to_char(I_location));
         close C_ORD_ALLOC_UNAPP;
      --- order
      elsif I_item is NULL and I_location is NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORD_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no));
         open C_ORD_UNAPP;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ORD_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no));
         fetch C_ORD_UNAPP into O_supp_prescale_cost,
                                O_supp_order_cost,
                                O_prescale_cost,
                                O_order_cost,
                                O_qty_prescaled,
                                O_qty_ordered;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORD_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no));
         close C_ORD_UNAPP;
      --- order/item
      elsif I_item is NOT NULL and I_location is NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORD_ITEM_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Item: '||I_item);
         open C_ORD_ITEM_UNAPP;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ORD_ITEM_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Item: '||I_item);
         fetch C_ORD_ITEM_UNAPP into O_supp_prescale_cost,
                                     O_supp_order_cost,
                                     O_prescale_cost,
                                     O_order_cost,
                                     O_qty_prescaled,
                                     O_qty_ordered,
                                     O_supp_item_cost,
                                     O_ord_item_cost;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORD_ITEM_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Item: '||I_item);
         close C_ORD_ITEM_UNAPP;
      --- order/location
      elsif I_item is NULL and I_location is NOT NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORD_LOC_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Location: '||to_char(I_location));
         open C_ORD_LOC_UNAPP;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ORD_LOC_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Location: '||to_char(I_location));
         fetch C_ORD_LOC_UNAPP into O_supp_prescale_cost,
                                    O_supp_order_cost,
                                    O_prescale_cost,
                                    O_order_cost,
                                    O_qty_prescaled,
                                    O_qty_ordered;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORD_LOC_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Location: '||to_char(I_location));
         close C_ORD_LOC_UNAPP;
      --- order/item/location
      elsif I_item is NOT NULL and I_location is NOT NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORD_ITEM_LOC_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Item: '||I_item||
                          ', Location: '||to_char(I_location));
         open C_ORD_ITEM_LOC_UNAPP;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ORD_ITEM_LOC_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Item: '||I_item||
                          ', Location: '||to_char(I_location));
         fetch C_ORD_ITEM_LOC_UNAPP into O_supp_prescale_cost,
                                         O_supp_order_cost,
                                         O_prescale_cost,
                                         O_order_cost,
                                         O_qty_prescaled,
                                         O_qty_ordered,
                                         O_supp_item_cost,
                                         O_ord_item_cost;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORD_ITEM_LOC_UNAPP',
                          'ordloc, ordsku, item_supp_country_loc',
                          'Order No: '||to_char(I_order_no)||
                          ', Item: '||I_item||
                          ', Location: '||to_char(I_location));
         close C_ORD_ITEM_LOC_UNAPP;
      end if;
      --- get supplier currency_code
      if (LP_supplier is NULL) or (LP_supp_currency_code is NULL) or (LP_supplier != L_supplier) then
         if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                              L_supp_currency_code,
                                              L_supplier) = FALSE then
            return FALSE;
         end if;
         LP_supplier           := L_supplier;
         LP_supp_currency_code := L_supp_currency_code;
      end if;
      --- convert to order currency if necessary
      if L_ord_currency_code != LP_supp_currency_code then
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 O_supp_prescale_cost,
                                 LP_supp_currency_code,
                                 L_ord_currency_code,
                                 O_supp_prescale_cost,
                                 'C',
                                 NULL,
                                 'P',
                                 NULL,
                                 L_ord_exchange_rate) = FALSE then
            return FALSE;
         end if;
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 O_supp_order_cost,
                                 LP_supp_currency_code,
                                 L_ord_currency_code,
                                 O_supp_order_cost,
                                 'C',
                                 NULL,
                                 'P',
                                 NULL,
                                 L_ord_exchange_rate) = FALSE then
            return FALSE;
         end if;
         if O_supp_item_cost is not null then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    O_supp_item_cost,
                                    LP_supp_currency_code,
                                    L_ord_currency_code,
                                    O_supp_item_cost,
                                    'C',
                                    NULL,
                                    'P',
                                    NULL,
                                    L_ord_exchange_rate) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
   ---
   --- Approved/closed order
   else
      --- order/alloc/location
      if I_alloc_no is NOT NULL and I_item is NOT NULL and I_location is NOT NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORD_ALLOC_APP',
                          'alloc_detail, ordloc',
                          'Order No: '||to_char(I_order_no)||
                          ', Alloc No: '||to_char(I_alloc_no)||
                          ', Item: '||I_item||
                          ', Location: '||to_char(I_location));
         open C_ORD_ALLOC_APP;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ORD_ALLOC_APP',
                          'alloc_detail, ordloc',
                          'Order No: '||to_char(I_order_no)||
                          ', Alloc No: '||to_char(I_alloc_no)||
                          ', Item: '||I_item||
                          ', Location: '||to_char(I_location));
         fetch C_ORD_ALLOC_APP into O_supp_prescale_cost,
                                    O_supp_order_cost,
                                    O_prescale_cost,
                                    O_order_cost,
                                    O_qty_prescaled,
                                    O_qty_ordered,
                                    O_supp_item_cost,
                                    O_ord_item_cost;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORD_ALLOC_APP',
                          'alloc_detail, ordloc',
                          'Order No: '||to_char(I_order_no)||
                          ', Alloc No: '||to_char(I_alloc_no)||
                          ', Item: '||I_item||
                          ', Location: '||to_char(I_location));
         close C_ORD_ALLOC_APP;
      --- order
      elsif I_item is NULL and I_location is NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORD_APP',
                          'ordloc',
                          'Order No: '||to_char(I_order_no));
         open C_ORD_APP;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ORD_APP',
                          'ordloc',
                          'Order No: '||to_char(I_order_no));
         fetch C_ORD_APP into O_supp_prescale_cost,
                              O_supp_order_cost,
                              O_prescale_cost,
                              O_order_cost,
                              O_qty_prescaled,
                              O_qty_ordered;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORD_APP',
                          'ordloc',
                          'Order No: '||to_char(I_order_no));
         close C_ORD_APP;
      --- order/item
      elsif I_item is NOT NULL and I_location is NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORD_ITEM_APP',
                          'ordloc',
                          'Order No: '||to_char(I_order_no)||
                          ', Item: '||I_item);
         open C_ORD_ITEM_APP;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ORD_ITEM_APP',
                          'ordloc',
                          'Order No: '||to_char(I_order_no)||
                          ', Item: '||I_item);
         fetch C_ORD_ITEM_APP into O_supp_prescale_cost,
                                   O_supp_order_cost,
                                   O_prescale_cost,
                                   O_order_cost,
                                   O_qty_prescaled,
                                   O_qty_ordered,
                                   O_supp_item_cost,
                                   O_ord_item_cost;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORD_ITEM_APP',
                          'ordloc',
                          'Order No: '||to_char(I_order_no)||
                          ', Item: '||I_item);
         close C_ORD_ITEM_APP;
      --- order/location
      elsif I_item is NULL and I_location is NOT NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORD_LOC_APP',
                          'ordloc',
                          'Order No: '||to_char(I_order_no)||
                          ', Location: '||to_char(I_location));
         open C_ORD_LOC_APP;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ORD_LOC_APP',
                          'ordloc',
                          'Order No: '||to_char(I_order_no)||
                          ', Location: '||to_char(I_location));
         fetch C_ORD_LOC_APP into O_supp_prescale_cost,
                                  O_supp_order_cost,
                                  O_prescale_cost,
                                  O_order_cost,
                                  O_qty_prescaled,
                                  O_qty_ordered;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORD_LOC_APP',
                          'ordloc',
                          'Order No: '||to_char(I_order_no)||
                          ', Location: '||to_char(I_location));
         close C_ORD_LOC_APP;
      --- order/item/location
      elsif I_item is NOT NULL and I_location is NOT NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORD_ITEM_LOC_APP',
                          'ordloc',
                          'Order No: '||to_char(I_order_no)||
                          ', Item: '||I_item||
                          ', Location: '||to_char(I_location));
         open C_ORD_ITEM_LOC_APP;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ORD_ITEM_LOC_APP',
                          'ordloc',
                          'Order No: '||to_char(I_order_no)||
                          ', Item: '||I_item||
                          ', Location: '||to_char(I_location));
         fetch C_ORD_ITEM_LOC_APP into O_supp_prescale_cost,
                                       O_supp_order_cost,
                                       O_prescale_cost,
                                       O_order_cost,
                                       O_qty_prescaled,
                                       O_qty_ordered,
                                       O_supp_item_cost,
                                       O_ord_item_cost;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORD_ITEM_LOC_APP',
                          'ordloc',
                          'Order No: '||to_char(I_order_no)||
                          ', Item: '||I_item||
                          ', Location: '||to_char(I_location));
         close C_ORD_ITEM_LOC_APP;
      end if;
   end if;

   ---
   return TRUE;

EXCEPTION
   when NO_DATA_FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CURSOR',
                                            'C_ORDHEAD',
                                            L_program,
                                            NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END SUPPLIER_COST_UNITS;
-------------------------------------------------------------------------------------
FUNCTION CALC_HEADER_DATES(O_error_message     IN OUT VARCHAR2,
                           O_pickup_date       IN OUT ORDHEAD.PICKUP_DATE%TYPE,
                           O_not_before_date   IN OUT ORDHEAD.NOT_BEFORE_DATE%TYPE,
                           O_not_after_date    IN OUT ORDHEAD.NOT_AFTER_DATE%TYPE,
                           I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                           I_supplier          IN     SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   L_program                VARCHAR2(64)             := 'ORDER_CALC_SQL.CALC_HEADER_DATES';
   L_vdate                  ORDHEAD.PICKUP_DATE%TYPE := GET_VDATE;
   L_min_lead_time          ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE;
   L_min_pickup_lead_time   ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE;
   L_max_lead_time          ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE;
   L_max_pickup_lead_time   ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE;
   L_ordsku_exist           VARCHAR2(1) := 'N';
   L_def_lead_time          SUPS.DEFAULT_ITEM_LEAD_TIME%TYPE;

   cursor C_ORDSKU_EXIST is
      select 'Y'
        from ordsku
       where order_no = I_order_no
         and rownum = 1;

   cursor C_GET_LEAD_TIME is
      select MIN(NVL(i.lead_time,0)),
             MAX(NVL(i.lead_time,0))
        from item_supp_country i,
             ordsku o
       where i.supplier          = I_supplier
         and i.origin_country_id = o.origin_country_id
         and o.order_no          = I_order_no
         and o.item              = i.item;

   cursor C_GET_PICKUP_LEAD_TIME is
      select /*+ LEADING(o l) */
             MIN(NVL(i.pickup_lead_time,0)),
             MAX(NVL(i.pickup_lead_time,0))
        from item_supp_country_loc i,
             ordsku o,
             ordloc l
       where i.supplier          = I_supplier
         and i.origin_country_id = o.origin_country_id
         and o.order_no          = I_order_no
         and o.order_no          = l.order_no
         and o.item              = l.item
         and l.location          = i.loc
         and o.item              = i.item;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            'Order no: ' || I_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_pickup_date     := L_vdate;
      O_not_before_date := L_vdate;
      O_not_after_date  := L_vdate;
      ---
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ORDSKU_EXIST','ORDSKU',NULL);
   open C_ORDSKU_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_ORDSKU_EXIST','ORDSKU',NULL);
   fetch C_ORDSKU_EXIST into L_ordsku_exist;
   SQL_LIB.SET_MARK('CLOSE','C_ORDSKU_EXIST','ORDSKU',NULL);
   close C_ORDSKU_EXIST;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY,ORDSKU',NULL);
   open C_GET_LEAD_TIME;
   SQL_LIB.SET_MARK('FETCH','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY,ORDSKU',NULL);
   fetch C_GET_LEAD_TIME into L_min_lead_time,
                              L_max_lead_time;
   SQL_LIB.SET_MARK('CLOSE','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY,ORDSKU',NULL);
   close C_GET_LEAD_TIME;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PICKUP_LEAD_TIME','ITEM_SUPP_COUNTRY,ORDSKU,ORDLOC',NULL);
   open C_GET_PICKUP_LEAD_TIME;
   SQL_LIB.SET_MARK('FETCH','C_GET_PICKUP_LEAD_TIME','ITEM_SUPP_COUNTRY,ORDSKU,ORDLOC',NULL);
   fetch C_GET_PICKUP_LEAD_TIME into L_min_pickup_lead_time,
                                     L_max_pickup_lead_time;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PICKUP_LEAD_TIME','ITEM_SUPP_COUNTRY,ORDSKU,ORDLOC',NULL);
   close C_GET_PICKUP_LEAD_TIME;
   ---
   if L_min_lead_time is NULL then
      L_min_lead_time := 0;
   end if;
   ---
   if L_max_lead_time is NULL then
      L_max_lead_time := 0;
   end if;
   ---
   if L_min_pickup_lead_time is NULL then
      L_min_pickup_lead_time := 0;
   end if;
   ---
   if L_max_pickup_lead_time is NULL then
      L_max_pickup_lead_time := 0;
   end if;
   ---
   if SUPP_ATTRIB_SQL.GET_LEAD_TIME (O_error_message,
                                     L_def_lead_time,
                                     I_supplier) = FALSE then
      return FALSE;
   end if;
   ---
   if L_def_lead_time is NULL then
      L_def_lead_time := 0;
   end if;
   ---
   if L_ordsku_exist = 'Y' then
      O_pickup_date     := L_vdate + L_max_lead_time;
      O_not_before_date := L_vdate + L_min_lead_time + L_min_pickup_lead_time;
      O_not_after_date  := L_vdate + L_max_lead_time + L_max_pickup_lead_time;
   else
      O_pickup_date     := L_vdate + L_def_lead_time;
      O_not_before_date := L_vdate + L_def_lead_time;
      O_not_after_date  := L_vdate + L_def_lead_time;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_HEADER_DATES;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ORDLOC_VWH_COSTS(O_error_message   IN OUT   VARCHAR2,
                                I_order_no        IN       ORDLOC.ORDER_NO%TYPE)
     RETURN BOOLEAN is

   L_program         VARCHAR2(64)           := 'ORDER_CALC_SQL.CHECK_ORDLOC_VWH_COSTS';
   L_item            ITEM_MASTER.ITEM%TYPE;

   cursor C_CHECK_ORDLOC_VWH_COSTS is
      select o.item
        from ordloc o,
             wh w
       where o.location = w.wh
         and o.order_no = I_order_no
         and exists (select 'x'
                       from ordloc o2,
                            wh w2
                      where o2.location = w2.wh
                        and o2.order_no = I_order_no
                        and w2.physical_wh = w.physical_wh
                        and o2.item = o.item
                        and o.unit_cost != o2.unit_cost);

BEGIN
   -- This function will query all of the items on the order with VWHs as locations to
   -- ensure that the unit cost is the same for each VWH associated with the same
   -- PWH. If not, the item in question is returned with the error message.

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            'Order no: '|| I_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_ORDLOC_VWH_COSTS','ORDLOC, WH',NULL);
   open C_CHECK_ORDLOC_VWH_COSTS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_ORDLOC_VWH_COSTS','ORDLOC, WH',NULL);
   fetch C_CHECK_ORDLOC_VWH_COSTS into L_item;
   ---
   if C_CHECK_ORDLOC_VWH_COSTS%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_ORDLOC_VWH_COSTS','ORDLOC, WH',NULL);
      close C_CHECK_ORDLOC_VWH_COSTS;
      return TRUE;
   else
      O_error_message := SQL_LIB.CREATE_MSG ('UPDATE_VWH_COSTS',
                                             L_item,
                                             NULL,
                                             NULL);
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_ORDLOC_VWH_COSTS','ORDLOC, WH',NULL);
      close C_CHECK_ORDLOC_VWH_COSTS;
      return FALSE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            sqlerrm,
                                            L_program,
                                            NULL);
      return FALSE;

END CHECK_ORDLOC_VWH_COSTS;
------------------------------------------------------------------------------------------
FUNCTION CALC_CONTRACT_ORDER_DATES(O_error_message     IN OUT   VARCHAR2,
                                   O_pickup_date       IN OUT   ORDHEAD.PICKUP_DATE%TYPE,
                                   O_not_before_date   IN OUT   ORDHEAD.NOT_BEFORE_DATE%TYPE,
                                   O_not_after_date    IN OUT   ORDHEAD.NOT_AFTER_DATE%TYPE,
                                   O_valid_ind         IN OUT   BOOLEAN,
                                   I_start_date        IN       CONTRACT_HEADER.START_DATE%TYPE,
                                   I_end_date          IN       CONTRACT_HEADER.END_DATE%TYPE,
                                   I_order_no          IN       ORDHEAD.ORDER_NO%TYPE,
                                   I_supplier          IN       SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   L_program                VARCHAR2(64)             := 'ORDER_CALC_SQL.CALC_CONTRACT_ORDER_DATES';
   L_vdate                  ORDHEAD.PICKUP_DATE%TYPE := GET_VDATE;
   L_min_lead_time          ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE;
   L_min_pickup_lead_time   ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE;
   L_max_lead_time          ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE;
   L_max_pickup_lead_time   ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE;
   L_temp_nad               ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_nad                    ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_ordsku_exist           VARCHAR2(1) := 'N';
   L_def_lead_time          SUPS.DEFAULT_ITEM_LEAD_TIME%TYPE;

   cursor C_ORDSKU_EXIST is
      select 'Y'
        from ordsku
       where order_no = I_order_no
         and rownum = 1;

   cursor C_GET_LEAD_TIME is
      select MIN(NVL(i.lead_time,0)),
             MAX(NVL(i.lead_time,0))
        from item_supp_country i,
             ordsku o
       where i.supplier          = I_supplier
         and i.origin_country_id = o.origin_country_id
         and o.order_no          = I_order_no
         and o.item              = i.item;

   cursor C_GET_PICKUP_LEAD_TIME is
      select /*+ LEADING(o l) */
             MIN(NVL(i.pickup_lead_time,0)),
             MAX(NVL(i.pickup_lead_time,0))
        from item_supp_country_loc i,
             ordsku o,
             ordloc l
       where i.supplier          = I_supplier
         and i.origin_country_id = o.origin_country_id
         and o.order_no          = I_order_no
         and o.order_no          = l.order_no
         and o.item              = l.item
         and l.location          = i.loc
         and o.item              = i.item;

BEGIN
   O_valid_ind := TRUE;

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            'Order no: '|| I_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_start_date is NULL or I_end_date is NULL then
      O_pickup_date     := NVL(I_start_date, L_vdate);
      O_not_before_date := NVL(I_start_date, L_vdate);
      O_not_after_date  := NVL(I_end_date, L_vdate);
      ---
      return TRUE;
   end if;
   ---
   if I_supplier is NULL then
      O_pickup_date     := I_start_date;
      O_not_before_date := I_start_date;
      O_not_after_date  := I_end_date;
      ---
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ORDSKU_EXIST','ORDSKU',NULL);
   open C_ORDSKU_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_ORDSKU_EXIST','ORDSKU',NULL);
   fetch C_ORDSKU_EXIST into L_ordsku_exist;
   SQL_LIB.SET_MARK('CLOSE','C_ORDSKU_EXIST','ORDSKU',NULL);
   close C_ORDSKU_EXIST;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY,ORDSKU',NULL);
   open C_GET_LEAD_TIME;
   SQL_LIB.SET_MARK('FETCH','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY,ORDSKU',NULL);
   fetch C_GET_LEAD_TIME into L_min_lead_time,
                              L_max_lead_time;
   SQL_LIB.SET_MARK('CLOSE','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY,ORDSKU',NULL);
   close C_GET_LEAD_TIME;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PICKUP_LEAD_TIME','ITEM_SUPP_COUNTRY,ORDSKU,ORDLOC',NULL);
   open C_GET_PICKUP_LEAD_TIME;
   SQL_LIB.SET_MARK('FETCH','C_GET_PICKUP_LEAD_TIME','ITEM_SUPP_COUNTRY,ORDSKU,ORDLOC',NULL);
   fetch C_GET_PICKUP_LEAD_TIME into L_min_pickup_lead_time,
                                     L_max_pickup_lead_time;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PICKUP_LEAD_TIME','ITEM_SUPP_COUNTRY,ORDSKU,ORDLOC',NULL);
   close C_GET_PICKUP_LEAD_TIME;
   ---
   if L_min_lead_time is NULL then
      L_min_lead_time := 0;
   end if;
   ---
   if L_max_lead_time is NULL then
      L_max_lead_time := 0;
   end if;
   ---
   if L_min_pickup_lead_time is NULL then
      L_min_pickup_lead_time := 0;
   end if;
   ---
   if L_max_pickup_lead_time is NULL then
      L_max_pickup_lead_time := 0;
   end if;
   ---
   if SUPP_ATTRIB_SQL.GET_LEAD_TIME(O_error_message,
                                    L_def_lead_time,
                                    I_supplier) = FALSE then
      return FALSE;
   end if;
   ---
   if L_def_lead_time is NULL then
      L_def_lead_time := 0;
   end if;
   ---
   ---calculate pickup and not_before dates
   if L_vdate <= I_start_date then
      if L_ordsku_exist = 'Y' then
         O_pickup_date     := I_start_date + L_min_lead_time;
         O_not_before_date := I_start_date + L_min_lead_time + L_min_pickup_lead_time;
      else
         O_pickup_date     := I_start_date + L_def_lead_time;
         O_not_before_date := I_start_date + L_def_lead_time;
      end if;
      ---
      L_nad                := I_start_date;
   else
      if L_ordsku_exist = 'Y' then
         O_pickup_date     := L_vdate + L_min_lead_time;
         O_not_before_date := L_vdate + L_min_lead_time + L_min_pickup_lead_time;
      else
         O_pickup_date     := L_vdate + L_def_lead_time;
         O_not_before_date := L_vdate + L_def_lead_time;
      end if;
      ---
      L_nad                := L_vdate;
   end if;

   --- check and see if new calculated dates are between the contract dates
   if (O_pickup_date < I_start_date) or (O_pickup_date > I_end_date) then
      if O_pickup_date > I_end_date then
         O_valid_ind  := FALSE;
      end if;

      if I_start_date >= L_vdate then
         O_pickup_date := I_start_date;
      else
         O_pickup_date := L_vdate;
      end if;
   end if;

   if (O_not_before_date < I_start_date) or (O_not_before_date > I_end_date) then
      if O_not_before_date > I_end_date then
         O_valid_ind := FALSE;
      end if;

      if (I_start_date >= L_vdate) and (I_start_date >= O_pickup_date) then
         O_not_before_date := I_start_date;
      else
         O_not_before_date := O_pickup_date;
      end if;
   end if;

   ---calculate not_after_date
   if L_ordsku_exist = 'Y' then
      L_temp_nad := L_nad + L_max_lead_time + L_max_pickup_lead_time;
   else
      L_temp_nad := L_nad + L_def_lead_time;
   end if;
   ---
   if (I_end_date <= L_temp_nad) and (I_end_date >= L_vdate) and (I_end_date >= O_not_before_date) then
      O_not_after_date := I_end_date;
      O_valid_ind      := FALSE;
   else
      O_not_after_date := L_temp_nad;
   end if;
   --- check and see if not_after_date is valid.
   if (O_not_after_date < I_start_date) or
       (O_not_after_date > I_end_date) or
       (O_not_after_date < O_not_before_date) then
      if O_not_after_date > I_end_date then
         O_valid_ind := FALSE;
      end if;

      if (I_end_date >= L_vdate) and (I_end_date >= O_not_before_date) then
         O_not_after_date := I_end_date;
      else
         O_not_after_date := O_not_before_date;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_CONTRACT_ORDER_DATES;
------------------------------------------------------------------------------------------
FUNCTION CALCULATE_DEPOSIT_COMP_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_item_cost           IN OUT   ORDLOC.UNIT_COST%TYPE,
                                     I_item                IN       ITEM_MASTER.ITEM%TYPE,
                                     I_order_status        IN       ORDHEAD.STATUS%TYPE,
                                     I_supp_pack_size      IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                                     I_supplier            IN       SUPS.SUPPLIER%TYPE,
                                     I_origin_country_id   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                     I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                                     I_pack_no             IN       PACKITEM.PACK_NO%TYPE,
                                     I_location            IN       ORDLOC.LOCATION%TYPE,
                                     I_order_curr          IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                     I_exchange_rate       IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)    := 'ORDER_CALC_SQL.CALCULATE_DEPOSIT_COMP_COST';
   L_supplier_currency  SUPS.CURRENCY_CODE%TYPE;
   L_item_cost          ORDLOC.UNIT_COST%TYPE;

BEGIN

   -- initialize the output parameter to zero
   O_item_cost := 0;

   --check for input parameters
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            'Item: ' || I_item,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_order_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            'Order Status: '|| I_order_status,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_supp_pack_size is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            'Supplier Pack Size: '|| I_supp_pack_size,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            'Supplier: ' || I_supplier,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            'Origin Country: '|| I_origin_country_id,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_order_status in ('W', 'S') then

      --determine the supplier's currency
      if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                           L_supplier_currency,
                                           I_supplier)= FALSE then
         return FALSE;
      end if;

      --retrieve item's unit cost
      if ITEM_SUPP_COUNTRY_SQL.GET_UNIT_COST(O_error_message,
                                             L_item_cost,
                                             I_item,
                                             I_supplier,
                                             I_origin_country_id)= FALSE then
         return FALSE;
      end if;

      --convert the unit cost to the order's currency
      if CURRENCY_SQL.CONVERT (O_error_message,
                               L_item_cost,
                               L_supplier_currency,
                               I_order_curr,
                               O_item_cost,
                               'N',
                               NULL,
                               NULL,
                               NULL,
                               I_exchange_rate) = FALSE then
         return FALSE;
      end if;
   else
      if ORDER_ITEM_ATTRIB_SQL.OLD_UNIT_COST (O_error_message,
                                              O_item_cost,
                                              I_pack_no,
                                              I_item,
                                              I_order_no,
                                              I_location,
                                              I_supplier,
                                              I_origin_country_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CALCULATE_DEPOSIT_COMP_COST;
------------------------------------------------------------------------------------------
FUNCTION CALCULATE_DEPOSIT_PACK_COMP(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_pack_comp_tbl       IN OUT   ORDER_CALC_SQL.ORDSKU_COMPLEX_PACK_TBL,
                                     I_pack_no             IN       PACKITEM.PACK_NO%TYPE,
                                     I_order_status        IN       ORDHEAD.STATUS%TYPE,
                                     I_supp_pack_size      IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                                     I_supplier            IN       SUPS.SUPPLIER%TYPE,
                                     I_origin_country_id   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                     I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                                     I_location            IN       ORDLOC.LOCATION%TYPE,
                                     I_order_curr          IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                     I_exchange_rate       IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)            := 'ORDER_CALC_SQL.CALCULATE_DEPOSIT_PACK_COMP';
   L_index              BINARY_INTEGER          := 0;
   L_comp_cost          ORDLOC.UNIT_COST%TYPE   := 0;
   L_exists             BOOLEAN;
   L_comp_qty_ordered   ORDLOC.QTY_ORDERED%TYPE := 0;
   L_tbl_index          NUMBER := 0;
   L_last_rec           NUMBER := 0;
   L_pack_cost          ORDLOC.UNIT_COST%TYPE   := 0;
   L_pack_cost_prorate  ORDLOC.UNIT_COST%TYPE   := 0;
   L_final_index        NUMBER := 0;

   L_container_index               NUMBER := 0;
   L_container_uop_qty_ordered     ORDLOC.QTY_ORDERED%TYPE := 0;
   L_container_supplier_unit_cost  ORDLOC.UNIT_COST%TYPE := 0;
   L_container_uop_order_cost      ORDLOC.UNIT_COST%TYPE := 0;

   L_contents_index                NUMBER := 0;
   L_contents_uop_qty_ordered      ORDLOC.QTY_ORDERED%TYPE := 0;
   L_contents_supplier_unit_cost   ORDLOC.UNIT_COST%TYPE := 0;
   L_contents_cost                 ORDLOC.UNIT_COST%TYPE := 0;
   L_contents_uop_order_cost       ORDLOC.UNIT_COST%TYPE := 0;

   L_crate_index                   NUMBER := 0;
   L_crate_uop_qty_ordered         ORDLOC.QTY_ORDERED%TYPE := 0;
   L_crate_supplier_unit_cost      ORDLOC.UNIT_COST%TYPE := 0;
   L_crate_cost                    ORDLOC.UNIT_COST%TYPE := 0;
   L_crate_uop_order_cost          ORDLOC.UNIT_COST%TYPE := 0;

   L_contents_crate_cost           ORDLOC.UNIT_COST%TYPE := 0;
   L_contents_percentage           NUMBER := 0;
   L_crate_percentage              NUMBER := 0;

   cursor C_PACK_COMP is
      select pb.item,
             im.deposit_item_type
        from packitem_breakout pb,
             item_master im
       where pb.pack_no = I_pack_no
         and pb.item = im.item
       order by pb.item;

   cursor C_PACK_COST is
      select unit_cost
        from ordloc
       where order_no = I_order_no
         and item = I_pack_no;

BEGIN

   -- Check for the input parameters
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            'Pack No: ' || I_pack_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_order_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            'Order Status: '|| I_order_status,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_supp_pack_size is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            'Supplier Pack Size: '|| I_supp_pack_size,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   --retrieve the cost of the pack
   --in the order
   SQL_LIB.SET_MARK('OPEN',
                    'C_PACK_COST',
                    'ITEM_MASTER',
                    NULL);
   open C_PACK_COST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_PACK_COST',
                    'ITEM_MASTER',
                    NULL);
   fetch C_PACK_COST into L_pack_cost;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_PACK_COST',
                    'ITEM_MASTER',
                    NULL);
   close C_PACK_COST;

   --initialize table-type variable
   O_pack_comp_tbl.delete;

   FOR rec in C_PACK_COMP LOOP
      if rec.item is NOT NULL then
         L_index := L_index + 1;
         O_pack_comp_tbl(L_index).item := rec.item;
         O_pack_comp_tbl(L_index).deposit_item_type := rec.deposit_item_type;
      end if;

      -- calculate the cost of the item
      if CALCULATE_DEPOSIT_COMP_COST(O_error_message,
                                     O_pack_comp_tbl(L_index).supplier_unit_cost,
                                     O_pack_comp_tbl(L_index).item,
                                     I_order_status,
                                     I_supp_pack_size,
                                     I_supplier,
                                     I_origin_country_id,
                                     I_order_no,
                                     I_pack_no,
                                     I_location,
                                     I_order_curr,
                                     I_exchange_rate) = FALSE then
         return FALSE;
      end if;

      -- retrieve the qty ordered for the item
      if ORDER_ITEM_ATTRIB_SQL.GET_QTY_ORDERED(O_error_message,
                                               L_exists,
                                               L_comp_qty_ordered,
                                               I_order_no,
                                               O_pack_comp_tbl(L_index).item,
                                               I_pack_no) = FALSE then
         return FALSE;
      end if;

      -- divide the qty ordered for the item by the supp pack size
      -- to get the quantity ordered UOP
      O_pack_comp_tbl(L_index).uop_qty_ordered := L_comp_qty_ordered/I_supp_pack_size;

   END LOOP;

   -- traverse through all the rows and determine which item
   -- is the deposit container item
   L_tbl_index := O_pack_comp_tbl.first;

   while L_tbl_index is NOT NULL LOOP
      if O_pack_comp_tbl(L_tbl_index).deposit_item_type = 'A' then

         --assign values for container items
         --also assign the value of the index to a flag
         L_container_index         := L_tbl_index;
         L_container_uop_qty_ordered    := O_pack_comp_tbl(L_tbl_index).uop_qty_ordered;
         L_container_supplier_unit_cost := O_pack_comp_tbl(L_tbl_index).supplier_unit_cost;

      elsif O_pack_comp_tbl(L_tbl_index).deposit_item_type = 'E' then

         -- assign values for content items
         -- also assign the value of the index to a flag
         L_contents_index              := L_tbl_index;
         L_contents_uop_qty_ordered    := O_pack_comp_tbl(L_tbl_index).uop_qty_ordered;
         L_contents_supplier_unit_cost := O_pack_comp_tbl(L_tbl_index).supplier_unit_cost;

      elsif O_pack_comp_tbl(L_tbl_index).deposit_item_type = 'Z' then

         -- assign values for crate items
         -- also assign the value of the index to a flag
         L_crate_index              := L_tbl_index;
         L_crate_uop_qty_ordered    := O_pack_comp_tbl(L_tbl_index).uop_qty_ordered;
         L_crate_supplier_unit_cost := O_pack_comp_tbl(L_tbl_index).supplier_unit_cost;

      end if;

      exit when L_tbl_index = O_pack_comp_tbl.last;

      L_tbl_index := O_pack_comp_tbl.next(L_tbl_index);

   end LOOP;

   -------------------------------------------
   --the following are the main calculations--
   -------------------------------------------

   --retrieve the uop order cost for the container item

   L_container_uop_order_cost := L_container_uop_qty_ordered * L_container_supplier_unit_cost;

   --subtract the container item's uop order cost from the pack's cost
   --to determine the cost to be prorated among the contents and
   --crate items

   L_pack_cost_prorate := L_pack_cost - L_container_uop_order_cost;

   --determine how to prorate the remaining pack cost
   --to the content and crate items


   L_contents_cost := L_contents_uop_qty_ordered * L_contents_supplier_unit_cost;
   L_crate_cost    := L_crate_uop_qty_ordered * L_crate_supplier_unit_cost;

   L_contents_crate_cost := L_contents_cost + L_crate_cost;

   if L_contents_crate_cost > 0 then
      --determine much of the cost should be assigned to the contents and the crate
      L_contents_percentage := (L_contents_cost/L_contents_crate_cost); --* 100;
      L_crate_percentage    := (L_crate_cost/L_contents_crate_cost); --* 100;
   end if;

   --multiply this percentage to the calculated pack cost to prorate
   if L_contents_uop_qty_ordered > 0 then
      L_contents_uop_order_cost := (L_contents_percentage * L_pack_cost_prorate)/L_contents_uop_qty_ordered;
   end if;

   if L_crate_uop_qty_ordered > 0 then
      L_crate_uop_order_cost    := (L_crate_percentage * L_pack_cost_prorate)/ L_crate_uop_qty_ordered;
   end if;

   --the following code assigns the calculated values
   --to the columns in the tbl-type output variable
   L_final_index := O_pack_comp_tbl.first;

   while L_final_index is NOT NULL LOOP

      if L_container_index = L_final_index then

         O_pack_comp_tbl(L_final_index).uop_order_cost := L_container_supplier_unit_cost;

      elsif L_contents_index = L_final_index then

         O_pack_comp_tbl(L_final_index).uop_order_cost := L_contents_uop_order_cost;

      elsif L_crate_index = L_final_index then

         O_pack_comp_tbl(L_final_index).uop_order_cost := L_crate_uop_order_cost;

      end if;

      exit when L_final_index = O_pack_comp_tbl.last;

      L_final_index := O_pack_comp_tbl.next(L_final_index);

   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALCULATE_DEPOSIT_PACK_COMP;
------------------------------------------------------------------------------------------
FUNCTION ITEMLOC_COST_RETAIL(O_error_message                IN OUT   VARCHAR2,
                             O_total_cost_ord               IN OUT   ORDLOC.UNIT_COST%TYPE,
                             O_total_cost_prim              IN OUT   ORDLOC.UNIT_COST%TYPE,
                             O_total_retail_incl_vat_ord    IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                             O_total_retail_incl_vat_prim   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                             O_total_retail_excl_vat_ord    IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                             O_total_retail_excl_vat_prim   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                             O_total_landed_cost_ord        IN OUT   NUMBER,
                             O_total_landed_cost_prim       IN OUT   NUMBER,
                             O_total_expense_ord            IN OUT   NUMBER,
                             O_total_expense_prim           IN OUT   NUMBER,
                             O_total_duty_ord               IN OUT   NUMBER,
                             O_total_duty_prim              IN OUT   NUMBER,
                             I_order_no                     IN       ORDHEAD.ORDER_NO%TYPE,
                             I_location                     IN       ORDLOC.LOCATION%TYPE,
                             I_item                         IN       ORDLOC.ITEM%TYPE,
                             I_qty                          IN       ORDLOC.QTY_ORDERED%TYPE,
                             I_exchange_rate                IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                             I_currency_code                IN       CURRENCIES.CURRENCY_CODE%TYPE,
                             I_import_country               IN       COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program                    VARCHAR2(64) := 'ORDER_CALC_SQL.ITEMLOC_COST_RETAIL';
   L_duty                       NUMBER;
   L_dty_currency               CURRENCIES.CURRENCY_CODE%TYPE;
   L_expense                    NUMBER;
   L_exp_currency               CURRENCIES.CURRENCY_CODE%TYPE;
   L_landed_cost                NUMBER;
   L_lc_currency                CURRENCIES.CURRENCY_CODE%TYPE;
   L_loc_currency               CURRENCIES.CURRENCY_CODE%TYPE;
   L_unit_retail                ORDLOC.UNIT_RETAIL%TYPE;

   L_qty                        ORDLOC.QTY_ORDERED%TYPE;
   L_item                       ITEM_MASTER.ITEM%TYPE;
   L_dept                       DEPS.DEPT%TYPE;
   L_class                      CLASS.CLASS%TYPE;
   L_class_vat_ind              CLASS.CLASS_VAT_IND%TYPE;
   L_subclass                   SUBCLASS.SUBCLASS%TYPE;
   L_loc_type                   ORDLOC.LOC_TYPE%TYPE;
   L_location                   ORDLOC.LOCATION%TYPE;
   L_base_currency              CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate              ORDHEAD.EXCHANGE_RATE%TYPE;
   L_exchange_rate_exp          ORDHEAD.EXCHANGE_RATE%TYPE;
   L_currency_code              ORDHEAD.CURRENCY_CODE%TYPE;
   L_zone_id                    COST_ZONE_GROUP_LOC.ZONE_ID%TYPE;
   L_origin_country_id          ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_import_country_id          ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   L_store_ind                  VARCHAR2(1);
   L_zone_group_id              COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   L_prescale_cost_ord          ORDLOC.UNIT_COST%TYPE;
   L_unit_cost                  ORDLOC.UNIT_COST%TYPE;
   L_total_cost                 ORDLOC.UNIT_COST%TYPE;


   L_total_retail_incl_vat_loc  ORDLOC.UNIT_COST%TYPE;
   L_total_retail_excl_vat_loc  ORDLOC.UNIT_COST%TYPE;

   L_retail_tax_rec             OBJ_TAX_RETAIL_ADD_REMOVE_REC := OBJ_TAX_RETAIL_ADD_REMOVE_REC();
   L_tax_retail_tbl             OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();

   cursor C_ORDLOC is
      select o.item, o.unit_cost,
             o.location,
             o.unit_retail,
             NVL(I_qty, o.qty_ordered) qty_ordered,
             o.loc_type,
             s.origin_country_id,
             oh.supplier,
             im.pack_ind,
             im.sellable_ind,
             im.orderable_ind,
             im.pack_type
        from ordhead oh,
             ordloc o,
             ordsku s,
             item_master im
       where oh.order_no = I_order_no
         and oh.order_no = o.order_no
         and o.item = I_item
         and o.location = I_location
         and o.order_no = s.order_no
         and o.item = im.item
         and o.item = s.item
       order by 2;

   TYPE ordlocTYPE IS TABLE OF C_ORDLOC%ROWTYPE INDEX BY BINARY_INTEGER;
   LP_ordloc ordlocTYPE;

   cursor C_IMPORT_COUNTRY is
      select import_country_id
        from ordhead
       where order_no = I_order_no;

BEGIN

   O_total_cost_ord               := 0;
   O_total_cost_prim              := 0;
   O_total_retail_incl_vat_ord    := 0;
   O_total_retail_incl_vat_prim   := 0;
   O_total_retail_excl_vat_ord    := 0;
   O_total_retail_excl_vat_prim   := 0;
   O_total_duty_ord               := 0;
   O_total_duty_prim              := 0;
   O_total_expense_ord            := 0;
   O_total_expense_prim           := 0;
   O_total_landed_cost_ord        := 0;
   O_total_landed_cost_prim       := 0;

   --- get exchange rate and currency
   if I_exchange_rate is NULL or I_currency_code is NULL then
      if ORDER_ATTRIB_SQL.GET_CURRENCY_RATE (O_error_message,
                                             L_currency_code,
                                             L_exchange_rate,
                                             I_order_no) = FALSE then
         return FALSE;
      end if;
   else
      L_currency_code := I_currency_code;
      L_exchange_rate := I_exchange_rate;
   end if;
   ---
   --- retrieve base currency
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE (O_error_message,
                                        L_base_currency) = FALSE then
      return FALSE;
   end if;
   ---

   -- only get elc_ind if LP_elc_ind not populated already  --
   ---
   if LP_elc_ind is NULL then
      if SYSTEM_OPTIONS_SQL.GET_ELC_IND (O_error_message,
                                         LP_elc_ind) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_import_country is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_IMPORT_COUNTRY',
                       'ORDHEAD',
                       'Order no: ' || TO_CHAR(I_order_no));
      open C_IMPORT_COUNTRY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_IMPORT_COUNTRY',
                       'ORDHEAD',
                       'Order no: ' || TO_CHAR(I_order_no));
      fetch C_IMPORT_COUNTRY into L_import_country_id;
      if C_IMPORT_COUNTRY%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG ('ERR_RETRIEVE_IMPORT_CO',
                                                'Order no: '|| I_order_no,
                                                NULL,
                                                NULL);
         SQL_LIB.SET_MARK('CLOSE',
                          'C_IMPORT_COUNTRY',
                          'ORDHEAD',
                          'Order no: ' || TO_CHAR(I_order_no));
         close C_IMPORT_COUNTRY;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_IMPORT_COUNTRY',
                       'ORDHEAD',
                       'Order no: ' || TO_CHAR(I_order_no));
   end if;
   ---
   -- Bulk collect all ORDLOC records, along with some additional information (see C_ORDLOC cursor)
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ORDLOC',
                    'ORDHEAD, ORDSKU, ORDLOC, ITEM_MASTER',
                    'Order ' || TO_CHAR(I_order_no));
   open C_ORDLOC;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ORDLOC',
                    'ORDHEAD, ORDSKU, ORDLOC, ITEM_MASTER',
                    'Order ' || TO_CHAR(I_order_no));
   fetch C_ORDLOC BULK COLLECT into LP_ordloc;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORDLOC',
                    'ORDHEAD, ORDSKU, ORDLOC, ITEM_MASTER',
                    'Order ' || TO_CHAR(I_order_no));
   close C_ORDLOC;

   L_item              := LP_ordloc(1).item;
   L_location          := LP_ordloc(1).location;
   L_unit_retail       := LP_ordloc(1).unit_retail;
   L_qty               := LP_ordloc(1).qty_ordered;
   L_loc_type          := LP_ordloc(1).loc_type;
   L_origin_country_id := LP_ordloc(1).origin_country_id;
   L_unit_cost         := LP_ordloc(1).unit_cost;
   L_total_cost        := L_unit_cost * L_qty;
   ---
   O_total_cost_ord := L_total_cost;
   --- convert order's total cost from order to primary currency.
   if CURRENCY_SQL.CONVERT (O_error_message,
                            O_total_cost_ord,
                            L_currency_code,
                            L_base_currency,
                            O_total_cost_prim,
                            'N',
                            NULL,
                            NULL,
                            L_exchange_rate,
                            NULL) = FALSE then
      return FALSE;
   end if;
   ---
   if LP_elc_ind = 'Y' then
      if ELC_CALC_SQL.CALC_ORDER_TOTALS (O_error_message,
                                         L_landed_cost,
                                         L_expense,
                                         L_exp_currency,
                                         L_exchange_rate_exp,
                                         L_duty,
                                         L_dty_currency,
                                         I_order_no,
                                         L_item,
                                         LP_ordloc(1).pack_ind,
                                         LP_ordloc(1).sellable_ind,
                                         LP_ordloc(1).orderable_ind,
                                         LP_ordloc(1).pack_type,
                                         LP_ordloc(1).qty_ordered,
                                         NULL,
                                         NULL,
                                         L_location,
                                         LP_ordloc(1).supplier,
                                         L_origin_country_id,
                                         I_import_country,
                                         L_total_cost) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.CONVERT (O_error_message,
                               L_expense,
                               L_exp_currency,
                               L_base_currency,
                               L_expense,
                               'N',
                               NULL,
                               NULL,
                               L_exchange_rate_exp,
                               NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.CONVERT (O_error_message,
                               L_duty,
                               L_dty_currency,
                               L_base_currency,
                               L_duty,
                               'N',
                               NULL,
                               NULL,
                               NULL,
                               NULL) = FALSE then
         return FALSE;
      end if;
      ---
      O_total_expense_prim     := O_total_expense_prim     + L_expense;
      O_total_duty_prim        := O_total_duty_prim        + L_duty ;
      O_total_landed_cost_prim := O_total_landed_cost_prim + L_landed_cost;

   end if;
   ---

   if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                     L_item,
                                     L_dept,
                                     L_class,
                                     L_subclass) = FALSE then
      return FALSE;
   end if;
   ---

   L_retail_tax_rec.I_item                := L_item;
   L_retail_tax_rec.I_dept                := L_dept;
   L_retail_tax_rec.I_class               := L_class;
   L_retail_tax_rec.I_location            := L_location;
   L_retail_tax_rec.I_loc_type            := L_loc_type;
   L_retail_tax_rec.I_effective_from_date := GET_VDATE;
   L_retail_tax_rec.I_amount              := L_unit_retail;
   ---
   L_tax_retail_tbl.delete;
   L_tax_retail_tbl.extend;
   L_tax_retail_tbl(1) := L_retail_tax_rec;

   -- Get the inclusive and exclusive retail values
   if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                       L_tax_retail_tbl) = FALSE then
      return FALSE;
   end if;

   L_total_retail_incl_vat_loc := L_tax_retail_tbl(1).O_amount * L_qty;

   ---
   if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                                       L_tax_retail_tbl) = FALSE then
      return FALSE;
   end if;

   L_total_retail_excl_vat_loc := L_tax_retail_tbl(1).O_amount * L_qty;

   -- convert totals to primary currency
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       L_location,
                                       L_loc_type,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       L_total_retail_incl_vat_loc,
                                       O_total_retail_incl_vat_prim,
                                       'N',
                                       NULL,
                                       NULL) = FALSE then
         return FALSE;
   end if;
   ---
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       L_location,
                                       L_loc_type,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       L_total_retail_excl_vat_loc,
                                       O_total_retail_excl_vat_prim,
                                       'N',
                                       NULL,
                                       NULL) = FALSE then
         return FALSE;
   end if;

   ---
   -- convert order's retail with vat from primary to order currency.
   ---
   if CURRENCY_SQL.CONVERT (O_error_message,
                            O_total_retail_incl_vat_prim,
                            L_base_currency,
                            L_currency_code,
                            O_total_retail_incl_vat_ord,
                            'N',
                            NULL,
                            NULL,
                            NULL,
                            L_exchange_rate) = FALSE then
      return FALSE;
   end if;
   ---
   --- convert order's retail without vat from primary to order currency.
   if CURRENCY_SQL.CONVERT (O_error_message,
                            O_total_retail_excl_vat_prim,
                            L_base_currency,
                            L_currency_code,
                            O_total_retail_excl_vat_ord,
                            'N',
                            NULL,
                            NULL,
                            NULL,
                            L_exchange_rate) = FALSE then
      return FALSE;
   end if;
   ---
   --- only convert the landed cost components if system ELC_IND = Y.
   if LP_elc_ind = 'Y' then
      --- convert order's landed cost from primary to order currency.
      if O_total_landed_cost_prim != 0 then
         if CURRENCY_SQL.CONVERT (O_error_message,
                                  O_total_landed_cost_prim,
                                  L_base_currency,
                                  L_currency_code,
                                  O_total_landed_cost_ord,
                                  'N',
                                  NULL,
                                  NULL,
                                  NULL,
                                  L_exchange_rate) = FALSE then
            return FALSE;
         end if;
      end if;

      ---
      --- convert order's expense from primary to order currency.
      if O_total_expense_prim != 0 then
         if CURRENCY_SQL.CONVERT (O_error_message,
                                  O_total_expense_prim,
                                  L_base_currency,
                                  L_currency_code,
                                  O_total_expense_ord,
                                  'N',
                                  NULL,
                                  NULL,
                                  NULL,
                                  L_exchange_rate) = FALSE then
            return FALSE;
         end if;
      end if;

      ---
      --- convert order's duty from primary to order currency.
      if O_total_duty_prim != 0 then
         if CURRENCY_SQL.CONVERT (O_error_message,
                                  O_total_duty_prim,
                                  L_base_currency,
                                  L_currency_code,
                                  O_total_duty_ord,
                                  'N',
                                  NULL,
                                  NULL,
                                  NULL,
                                  L_exchange_rate) = FALSE then
            return FALSE;
         end if;
      end if;
   else
      ---
      -- If the ELC ind is 'N' still need to pass out an
      -- Estimated Landed Cost value.  With no components, the ELC
      -- is simply equal to the order cost.
      ---
      O_total_landed_cost_ord  := O_total_cost_ord;
      O_total_landed_cost_prim := O_total_cost_prim;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ITEMLOC_COST_RETAIL;
------------------------------------------------------------------------------------------
FUNCTION RECALC_EXCHANGE_RATE(O_error_message   IN OUT   VARCHAR2,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                              I_exchange_rate   IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                              I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'ORDER_CALC_SQL.RECALC_EXCHANGE_RATE';
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE;
   L_exchange_rate        ORDHEAD.EXCHANGE_RATE%TYPE;
   L_exchange_rate_exp    ORDHEAD.EXCHANGE_RATE%TYPE;
   L_currency_code        ORDHEAD.CURRENCY_CODE%TYPE;
   L_base_currency        CURRENCIES.CURRENCY_CODE%TYPE;
   L_supp_cost            ORDLOC.UNIT_COST%TYPE := 0;
   L_rowid                ROWID;
   L_supplier             SUPS.SUPPLIER%TYPE;
   L_supp_currency        CURRENCIES.CURRENCY_CODE%TYPE;
   O_exists               BOOLEAN;

   -- Currently contracts are not handled.
   -- This is because ordhead form doesn't permit modifying exchange rate for contract orders.
   cursor C_ORDLOC is
      select iscl.unit_cost, ol.rowid
        from ordloc ol, ordsku os, item_supp_country_loc iscl
       where ol.order_no = I_order_no
         and ol.cost_source not in ('CONT','MANL')
         and os.order_no = ol.order_no
         and os.item = ol.item
         and iscl.item = ol.item
         and iscl.supplier = L_supplier
         and iscl.origin_country_id = os.origin_country_id
         and iscl.loc = ol.location;

BEGIN

   -- get exchange rate and currency
   if ORDER_ATTRIB_SQL.GET_CURRENCY_RATE (O_error_message,
                                          L_currency_code,
                                          L_exchange_rate,
                                          I_order_no) = FALSE then
         return FALSE;
   end if;
   -- This code is added to skip for processing when exchange rate is not changed.
   if (NVL(I_exchange_rate, L_exchange_rate) = L_exchange_rate) and
      (NVL(I_currency_code, L_currency_code) = L_currency_code) then
      return TRUE;
   end if;

   L_currency_code := NVL(I_currency_code, L_currency_code);
   L_exchange_rate := NVL(I_exchange_rate, L_exchange_rate);
   --
   -- retrieve base currency
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE (O_error_message,
                                        L_base_currency) = FALSE then
      return FALSE;
   end if;

   -- This is used to update the ordloc_exp exchange rate to the exchange rate in the order
   -- Update is done only if ORDER_EXCH_IND is 'Y'
   -- Expenses and assessments are re calculated
   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;

   update ordhead
      set exchange_rate        = L_exchange_rate,
          last_update_id       = get_user,
          last_update_datetime = sysdate
    where order_no             = I_order_no
      and currency_code        = L_currency_code;

   -- Getting the order supplier
   if ORDER_ATTRIB_SQL.GET_SUPPLIER(O_error_message,
                                    O_exists,
                                    L_supplier,
                                    I_order_no) = FALSE then
      return FALSE;
   end if;

   -- Getting the currency code for the supplier
   if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                        L_supp_currency,
                                        L_supplier) = FALSE then
      return FALSE;
   end if;

   -- If supplier currency is not same as the order currency then
   -- change the unit cost of the item wrt new exchange rate
   if L_supp_currency != L_currency_code then
      for rec in C_ORDLOC loop
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 rec.unit_cost,   --supplier cost
                                 L_supp_currency, --supplier currency
                                 L_currency_code, --order currency
                                 L_supp_cost,
                                 'C',
                                 NULL,
                                 NULL,
                                 NULL,
                                 L_exchange_rate  --order exchange rate
                                 ) = FALSE then
             return FALSE;
         end if;
         ---
         update ordloc
            set unit_cost = L_supp_cost, cost_source = 'NORM'
          where rowid = rec.rowid;
      end LOOP;
   end if;
   ---
   if L_system_options_rec.order_exch_ind = 'Y' then
      update ordloc_exp
         set exchange_rate = L_exchange_rate
       where order_no      = I_order_no
         and comp_currency = L_currency_code;
   end if;
   ---
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PE',
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             I_order_no,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL) = FALSE then
      return FALSE;
   end if;
   ---
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                            'PA',
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             I_order_no,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END RECALC_EXCHANGE_RATE;
------------------------------------------------------------------------------------------
FUNCTION ROUND_ORDER(O_error_message   IN OUT   VARCHAR2,
                     I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                     I_supplier        IN       ORDHEAD.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'ORDER_CALC_SQL.ROUND_ORDER';
   L_total_ord_qty_table  ROUNDING_SQL.TOTAL_ORD_QTY_TABLETYPE;
   L_covered_inbound      BOOLEAN;

   cursor C_GET_ORDLOC is
      select item,
             location,
             qty_ordered
        from ordloc
       where order_no = I_order_no;

   TYPE item_tab IS TABLE OF ITEM_MASTER.ITEM%TYPE;
   L_item_tab item_tab;
   TYPE location_tab IS TABLE OF ORDLOC.LOCATION%TYPE;
   L_location_tab location_tab;
   TYPE total_order_qty_tab IS TABLE OF ORDLOC.QTY_ORDERED%TYPE;
   L_total_order_qty_tab total_order_qty_tab;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            'Order no: '|| I_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ORDLOC',
                    'ORDLOC',
                    NULL);

   open C_GET_ORDLOC;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ORDLOC',
                    'ORDLOC',
                    NULL);
   fetch C_GET_ORDLOC bulk collect into  L_item_tab,
                                         L_location_tab,
                                         L_total_order_qty_tab;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ORDLOC',
                    'ORDLOC',
                    NULL);
   close C_GET_ORDLOC;

   if L_item_tab is not null and L_item_tab.count > 0 then
      FOR i in L_item_tab.first..L_item_tab.last  LOOP
         L_total_ord_qty_table(i).item := L_item_tab(i);
         L_total_ord_qty_table(i).location := L_location_tab(i);
         L_total_ord_qty_table(i).total_order_qty := L_total_order_qty_tab(i);
      END LOOP;
   end if;
   if ROUNDING_SQL.ORDER_INTERFACE(O_error_message,
                                   L_covered_inbound,
                                   I_order_no,
                                   I_supplier,
                                   L_total_ord_qty_table,
                                   'N',
                                   'N') = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ROUND_ORDER;
------------------------------------------------------------------------------------------
PROCEDURE QUERY_ORDSKU (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_ordsku_rec          IN OUT   ORDSKU_REC,
                        I_supplier            IN       ITEM_SUPPLIER.SUPPLIER%TYPE,
                        I_exchange_rate       IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                        I_fash_prepack_ind    IN       VARCHAR2,
                        I_import_country_id   IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                        I_currency_ord        IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                        I_currency_prim       IN       CURRENCY_RATES.CURRENCY_CODE%TYPE)
IS
   L_program      VARCHAR2(64) := 'ORDER_CALC_SQL.QUERY_ORDSKU';
   L_ordsku_rec   ORDSKU_REC := O_ordsku_rec;

   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_exists               BOOLEAN;
   L_supplier_cost        ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_supplier_currency    SUPS.CURRENCY_CODE%TYPE;
   L_supplier_case_cost   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_qty                  ORDLOC.QTY_ORDERED%TYPE;
   L_exchange_rate_ord    ORDHEAD.EXCHANGE_RATE%TYPE;
   L_currency_ord         CURRENCIES.CURRENCY_CODE%TYPE;
   L_case_desc            CODE_DETAIL.CODE_DESC%TYPE;
   L_unit_elc_ord         ORDLOC.UNIT_COST%TYPE;
   L_unit_elc_prim        ORDLOC.UNIT_COST%TYPE;
   L_unit_expense_ord     ORDLOC.UNIT_COST%TYPE;
   L_unit_expense_prim    ORDLOC.UNIT_COST%TYPE;
   L_unit_duty_ord        ORDLOC.UNIT_COST%TYPE;
   L_unit_duty_prim       ORDLOC.UNIT_COST%TYPE;
   L_pallet_desc          CODE_DETAIL.CODE_DESC%TYPE;
   L_inner_desc           CODE_DETAIL.CODE_DESC%TYPE;
   L_supp_pack_size       ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_inner_pack_size      ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE;
   L_order_status         ORDHEAD.STATUS%TYPE;
   L_qty_shipped          ORDLOC.QTY_ORDERED%TYPE;
   L_qty_prescaled        ORDLOC.QTY_ORDERED%TYPE;
   L_qty_cancelled        ORDLOC.QTY_ORDERED%TYPE;
   L_item_master          ITEM_MASTER%ROWTYPE;
   L_default_uop          ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE;
   L_default_po_cost      COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE;
   L_orig_approval_date   ORDHEAD.ORIG_APPROVAL_DATE%TYPE;
   ---
   cursor C_PACK_TMPL_QTY is
      select SUM(ptd.qty)
        from pack_tmpl_detail ptd,
             packitem pi
       where pi.pack_tmpl_id = ptd.pack_tmpl_id
         and pi.pack_no = L_ordsku_rec.item;

   cursor C_SUPP_INFO is
      select isp.vpn,
             SUM(iscl.unit_cost * ol.qty_ordered) / DECODE(SUM(ol.qty_ordered),0,1,SUM(ol.qty_ordered)),
             s.currency_code
        from item_supplier isp,
             item_supp_country_loc iscl,
             sups s,
             ordloc ol
       where isp.item     = L_ordsku_rec.item
         and isp.supplier = I_supplier
         and isp.item     = iscl.item
         and isp.supplier = iscl.supplier
         and iscl.origin_country_id = L_ordsku_rec.origin_country_id
         and isp.supplier = s.supplier
         and ol.order_no        = L_ordsku_rec.order_no
         and ol.location        = iscl.loc
         and ol.item            = iscl.item
    group by isp.vpn, s.currency_code;

   cursor C_OLD_UNIT_COST is
      select SUM(l.unit_cost_init * DECODE(l.qty_ordered,0,l.qty_received,l.qty_ordered)) / DECODE(SUM(l.qty_ordered),0,DECODE(SUM(l.qty_received),0,1,SUM(l.qty_received)),SUM(l.qty_ordered))
        from ordloc l,
             ordsku s
       where s.item              = L_ordsku_rec.item
         and s.item              = l.item
         and s.origin_country_id = L_ordsku_rec.origin_country_id
         and s.order_no          = L_ordsku_rec.order_no
         and s.order_no          = l.order_no;

   cursor C_OLD_SUPP_INFO is
      select item_supplier.vpn, sups.currency_code
        from item_supplier,
             sups
       where item_supplier.item = L_ordsku_rec.item
         and item_supplier.supplier = I_supplier
         and item_supplier.supplier = sups.supplier;

   cursor C_ORD_INFO is
      select orig_approval_date
        from ordhead
       where order_no = L_ordsku_rec.order_no;

   cursor C_GET_TOTAL_ORDER_COST(I_order_no ordloc.order_no%type,I_item ordloc.item%type) is
     select nvl(sum(ol.qty_ordered*ol.unit_cost),0) TotalOrderCost
       from ordloc ol,ordsku os
      where ol.order_no = os.order_no
        and ol.item = os.item
        and ol.order_no = I_order_no
        and ol.item =I_item ;

  cursor C_GET_TOTAL_ALLOCATED_QTY(I_order_no ordsku.order_no%type,I_item ordsku.item%type)  is
     select sum(nvl(ad.qty_allocated,0))
       from alloc_detail ad,alloc_header ah,ordloc ol
      where ah.alloc_no = ad.alloc_no
        and ah.order_no = ol.order_no
        and ah.wh = ol.location
        and ah.item = ol.item
        and ah.order_no = I_order_no
        and ol.item = I_item;
   ---
BEGIN
   --check required parameters
   if L_ordsku_rec.order_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'L_ordsku_rec.order_no',
                                           L_program,
                                           NULL);
      return;
   end if;
   if L_ordsku_rec.item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'L_ordsku_rec.item',
                                           L_program,
                                           NULL);
      return;
   end if;
   if L_ordsku_rec.origin_country_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'L_ordsku_rec.origin_country_id',
                                           L_program,
                                           NULL);
      return;
   end if;
   if L_ordsku_rec.supp_pack_size is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'L_ordsku_rec.supp_pack_size',
                                           L_program,
                                           NULL);
      return;
   end if;
   if I_supplier is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_supplier',
                                           L_program,
                                           NULL);
      return;
   end if;
   if I_exchange_rate is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_exchange_rate',
                                           L_program,
                                           NULL);
      return;
   end if;
   if I_import_country_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_import_country_id',
                                           L_program,
                                           NULL);
      return;
   end if;
   if I_currency_ord is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_currency_ord',
                                           L_program,
                                           NULL);
      return;
   end if;
   if I_currency_prim is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_currency_prim',
                                           L_program,
                                           NULL);
      return;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_error_message,
                                      L_item_master,
                                      L_ordsku_rec.item) = FALSE then
      return;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_DESC(L_error_message,
                               L_ordsku_rec.ti_item_desc,
                               L_ordsku_rec.item) = FALSE then
         return;
   end if;
   ---
   --L_ordsku_rec.ti_item_desc := L_item_master.item_desc;
   L_ordsku_rec.ti_standard_uom := L_item_master.standard_uom;
   L_ordsku_rec.ti_deposit_item_type := L_item_master.deposit_item_type;
   ---
   open C_GET_TOTAL_ORDER_COST(L_ordsku_rec.order_no,L_ordsku_rec.item);
   fetch C_GET_TOTAL_ORDER_COST into L_ordsku_rec.ti_total_order_cost_ord;
   close C_GET_TOTAL_ORDER_COST;
   ---
   open C_GET_TOTAL_ALLOCATED_QTY(L_ordsku_rec.order_no,L_ordsku_rec.item);
   fetch C_GET_TOTAL_ALLOCATED_QTY into  L_ordsku_rec.ti_total_allocated_qty;
   close C_GET_TOTAL_ALLOCATED_QTY;
   ---
    --- convert from order currency to primary currency
   if CURRENCY_SQL.CONVERT (L_error_message,
                            L_ordsku_rec.ti_total_order_cost_ord,
                            I_currency_ord,
                            I_currency_prim,
                            L_ordsku_rec.ti_total_order_cost_prim,
                            'N',
                            NULL,
                            NULL,
                           I_exchange_rate,
                            NULL) = FALSE then
      return;
   end if;
   ---
   if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(L_error_message,
                                          L_exists,
                                          L_ordsku_rec.ti_unit_cost,
                                          L_ordsku_rec.order_no,
                                          L_ordsku_rec.item,
                                          NULL,
                                          NULL) = FALSE then
      return;
   end if;
   L_ordsku_rec.ti_orig_cost := L_ordsku_rec.ti_unit_cost;
   ---
   if L_ordsku_rec.ref_item is NULL then
      L_ordsku_rec.ti_ref_item_desc := L_ordsku_rec.ti_item_desc;
   else
      if ITEM_ATTRIB_SQL.GET_DESC(L_error_message,
                                  L_ordsku_rec.ti_ref_item_desc,
                                  L_ordsku_rec.ref_item) = FALSE then
         return;
      end if;
   end if;
   ---
   --- fetch the order currency and exchange rate
   if ORDER_ATTRIB_SQL.GET_CURRENCY_RATE(L_error_message,
                                         L_currency_ord,
                                         L_exchange_rate_ord,
                                         L_ordsku_rec.order_no) = FALSE then
      return;
   end if;
   ---
   --- calculate the total quantity ordered/received for order_no/item
   if ORDER_ITEM_ATTRIB_SQL.GET_QTYS(L_error_message,
                                     L_exists,
                                     L_ordsku_rec.ti_qty_ordered,
                                     L_qty_shipped,
                                     L_ordsku_rec.ti_qty_prescaled,
                                     L_ordsku_rec.ti_qty_received,
                                     L_qty_cancelled,
                                     L_ordsku_rec.order_no,
                                     L_ordsku_rec.item) = FALSE then
      return;
   end if;
   ---
   if I_fash_prepack_ind = 'Y' then
      open C_PACK_TMPL_QTY;
      fetch C_PACK_TMPL_QTY into L_ordsku_rec.ti_pack_tmpl_qty;
      close C_PACK_TMPL_QTY;
   end if;
   ---
   -- populate the primary unit cost
   if CURRENCY_SQL.CONVERT(L_error_message,
                           L_ordsku_rec.ti_unit_cost,
                           I_currency_ord,
                           I_currency_prim,
                           L_ordsku_rec.ti_unit_cost_prim,
                           'C',
                           NULL,
                           NULL,
                           I_exchange_rate,
                           NULL) = FALSE then
      return;
   end if;
   ---
   if ORDER_CALC_SQL.ITEM_LOC_COSTS(L_error_message,
                                    L_unit_elc_ord,
                                    L_unit_elc_prim,
                                    L_ordsku_rec.ti_total_elc_ord,
                                    L_ordsku_rec.ti_total_elc_prim,
                                    L_unit_expense_ord,
                                    L_unit_expense_prim,
                                    L_ordsku_rec.ti_total_expenses_ord,
                                    L_ordsku_rec.ti_total_expenses_prim,
                                    L_unit_duty_ord,
                                    L_unit_duty_prim,
                                    L_ordsku_rec.ti_total_duty_ord,
                                    L_ordsku_rec.ti_total_duty_prim,
                                    L_qty,
                                    L_ordsku_rec.order_no,
                                    I_exchange_rate,
                                    I_currency_ord,
                                    L_ordsku_rec.item,
                                    NULL,
                                    NULL,
                                    L_ordsku_rec.ti_qty_ordered,
                                    I_supplier,
                                    L_ordsku_rec.origin_country_id,
                                    NULL) = FALSE then
      return;
   end if;
   ---
   -- populate the VPN and UNIT_COST fields
   -- if the order has orig_approval_date as null then status cursor C_SUPP_INFO can be used.
   -- if the order is approved then C_OLD_UNIT_COST and C_OLD_SUPP_INFO needs to be used.
   ---
   if ORDER_ATTRIB_SQL.GET_STATUS(L_error_message,
                                  L_exists,
                                  L_order_status,
                                  L_ordsku_rec.order_no) = FALSE then
      return;
   end if;
   ---
   if (L_order_status = 'W' or L_order_status = 'S')  then

      if ITEM_COST_SQL.GET_DEFAULT_PO_COST(L_error_message,
                                            L_default_po_cost,
                                            I_import_country_id,
                                            NULL) = FALSE then
          return;
      end if;
   end if;
   open  C_ORD_INFO;
   fetch C_ORD_INFO into L_orig_approval_date;
   close C_ORD_INFO;

   if (L_orig_approval_date is NULL)  then

      open  C_SUPP_INFO;
      fetch C_SUPP_INFO into L_ordsku_rec.ti_vpn, L_supplier_cost, L_supplier_currency;
      close C_SUPP_INFO;
      ---
      L_supplier_case_cost := L_supplier_cost * L_ordsku_rec.supp_pack_size;
      --- convert from supplier currency to order currency
      if L_supplier_currency!=I_currency_ord then
         if CURRENCY_SQL.CONVERT (L_error_message,
                                  L_supplier_case_cost,
                                  L_supplier_currency,
                                  I_currency_ord,
                                  L_ordsku_rec.ti_supplier_cost_ord,
                                 'N',
                                  NULL,
                                  NULL,
                                  NULL,
                                  I_exchange_rate) = FALSE then

            return;
         end if;
      else
         L_ordsku_rec.ti_supplier_cost_ord:= L_supplier_case_cost;
      end if;
   else
      open C_OLD_SUPP_INFO;
      fetch C_OLD_SUPP_INFO into L_ordsku_rec.ti_vpn, L_supplier_currency;
      close C_OLD_SUPP_INFO;
      ---
      open C_OLD_UNIT_COST;
      fetch C_OLD_UNIT_COST into L_supplier_cost;
      close C_OLD_UNIT_COST;
      ---
      L_ordsku_rec.ti_supplier_cost_ord := L_supplier_cost * L_ordsku_rec.supp_pack_size;
   end if;
   ---
   -- convert from the supplier currency to the primary currency
   ---
   --- convert from order currency to primary currency
   if CURRENCY_SQL.CONVERT (L_error_message,
                            L_ordsku_rec.ti_supplier_cost_ord,
                            I_currency_ord,
                            I_currency_prim,
                            L_ordsku_rec.ti_supplier_cost_prim,
                            'N',
                            NULL,
                            NULL,
                           I_exchange_rate,
                            NULL) = FALSE then
      return;
   end if;
   ---
   if ORDER_ITEM_ATTRIB_SQL.GET_COST_SOURCE(L_error_message,
                                            L_exists,
                                            L_ordsku_rec.ti_cost_source_base,
                                            L_ordsku_rec.order_no,
                                            L_ordsku_rec.item,
                                             NULL) = FALSE then
      return;
   end if;
   ---
   -- decode and populate the cost source
   if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                 'OICS',
                                 L_ordsku_rec.ti_cost_source_base,
                                 L_ordsku_rec.ti_cost_source) = FALSE then
      return;
   end if;
   ---
   if SUPP_ITEM_ATTRIB_SQL.GET_PACK_SIZES(L_error_message,
                                          L_supp_pack_size,
                                          L_inner_pack_size,
                                          L_pallet_desc,
                                          L_case_desc,
                                          L_inner_desc,
                                          L_ordsku_rec.item,
                                          I_supplier,
                                          L_ordsku_rec.origin_country_id) = FALSE then
      return;
   end if;
   ---
   if L_ordsku_rec.supp_pack_size = 1 then
      if ITEM_SUPP_COUNTRY_SQL.GET_DEFAULT_UOP(L_error_message,
                                               L_default_uop,
                                               L_ordsku_rec.item,
                                               I_supplier,
                                               L_ordsku_rec.origin_country_id) = FALSE then
         return;
      end if;

      if L_default_uop = L_ordsku_rec.ti_standard_uom then
         if UOM_SQL.GET_DESC(L_error_message,
                             L_ordsku_rec.ti_unit_of_purch,
                             L_ordsku_rec.ti_standard_uom) = FALSE then
            L_ordsku_rec.ti_unit_of_purch := NULL;
            return;
         end if;
      else
         L_ordsku_rec.ti_unit_of_purch := L_case_desc;
      end if;
   else  --does not equal to 1
      L_ordsku_rec.ti_unit_of_purch := L_case_desc;
   end if;
   ---
   L_ordsku_rec.ti_unit_elc_ord       := L_unit_elc_ord      * L_ordsku_rec.supp_pack_size;
   L_ordsku_rec.ti_unit_elc_prim      := L_unit_elc_prim     * L_ordsku_rec.supp_pack_size;
   L_ordsku_rec.ti_unit_expense_ord   := L_unit_expense_ord  * L_ordsku_rec.supp_pack_size;
   L_ordsku_rec.ti_unit_expense_prim  := L_unit_expense_prim * L_ordsku_rec.supp_pack_size;
   L_ordsku_rec.ti_unit_duty_ord      := L_unit_duty_ord     * L_ordsku_rec.supp_pack_size;
   L_ordsku_rec.ti_unit_duty_prim     := L_unit_duty_prim    * L_ordsku_rec.supp_pack_size;
   ---
   O_ordsku_rec := L_ordsku_rec;

   return;

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return;

END QUERY_ORDSKU;
------------------------------------------------------------------------------------------
FUNCTION QUERY_ORDSKU(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
					  O_ordsku_rec        IN OUT WRP_ORDSKU_REC,
					  I_supplier          IN ITEM_SUPPLIER.SUPPLIER%TYPE,
					  I_exchange_rate     IN CURRENCY_RATES.EXCHANGE_RATE%TYPE,
					  I_fash_prepack_ind  IN VARCHAR2,
					  I_import_country_id IN ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
					  I_currency_ord      IN CURRENCY_RATES.CURRENCY_CODE%TYPE,
					  I_currency_prim     IN CURRENCY_RATES.CURRENCY_CODE%TYPE)
  RETURN INTEGER
IS
  L_program VARCHAR2(64) := 'ORDER_CALC_SQL.QUERY_ORDSKU';
  L_temp_ordsku_rec ORDSKU_REC;
BEGIN
  L_temp_ordsku_rec. item                   := O_ordsku_rec.item ;
  L_temp_ordsku_rec. ti_item_desc           := O_ordsku_rec.item_desc ;
  L_temp_ordsku_rec. origin_country_id      := O_ordsku_rec.origin_country_id ;
  L_temp_ordsku_rec. ti_supplier_cost_ord   := O_ordsku_rec.supplier_cost_ord ;
  L_temp_ordsku_rec. ti_cost_source         := O_ordsku_rec.cost_source ;
  L_temp_ordsku_rec. ti_unit_elc_ord        := O_ordsku_rec.unit_elc_ord ;
  L_temp_ordsku_rec. ti_unit_of_purch       := O_ordsku_rec.unit_of_purch ;
  L_temp_ordsku_rec. non_scale_ind          := O_ordsku_rec.non_scale_ind ;
  L_temp_ordsku_rec. ti_qty_received        := O_ordsku_rec.qty_received ;
  L_temp_ordsku_rec. ti_unit_expense_ord    := O_ordsku_rec.unit_expense_ord ;
  L_temp_ordsku_rec. ti_unit_expense_prim   := O_ordsku_rec.unit_expense_prim ;
  L_temp_ordsku_rec. ti_total_expenses_ord  := O_ordsku_rec.total_expenses_ord ;
  L_temp_ordsku_rec. ti_total_expenses_prim := O_ordsku_rec.total_expenses_prim ;
  L_temp_ordsku_rec. ti_unit_duty_ord       := O_ordsku_rec.unit_duty_ord ;
  L_temp_ordsku_rec. ti_unit_duty_prim      := O_ordsku_rec.unit_duty_prim ;
  L_temp_ordsku_rec. ti_total_duty_ord      := O_ordsku_rec.total_duty_ord ;
  L_temp_ordsku_rec. ti_total_duty_prim     := O_ordsku_rec.total_duty_prim ;
  L_temp_ordsku_rec. ti_unit_elc_prim       := O_ordsku_rec.unit_elc_prim ;
  L_temp_ordsku_rec. ti_total_elc_ord       := O_ordsku_rec.total_elc_ord ;
  L_temp_ordsku_rec. ti_total_elc_prim      := O_ordsku_rec.total_elc_prim ;
  L_temp_ordsku_rec. earliest_ship_date     := O_ordsku_rec.earliest_ship_date ;
  L_temp_ordsku_rec. latest_ship_date       := O_ordsku_rec.latest_ship_date ;
  L_temp_ordsku_rec. ref_item               := O_ordsku_rec.ref_item ;
  L_temp_ordsku_rec. ti_ref_item_desc       := O_ordsku_rec.ref_item_desc ;
  L_temp_ordsku_rec. ti_unit_cost_prim      := O_ordsku_rec.unit_cost_prim ;
  L_temp_ordsku_rec. ti_supplier_cost_prim  := O_ordsku_rec.supplier_cost_prim ;
  L_temp_ordsku_rec. ti_unit_cost           := O_ordsku_rec.unit_cost ;
  L_temp_ordsku_rec. ti_vpn                 := O_ordsku_rec.vpn ;
  L_temp_ordsku_rec. ti_qty_ordered         := O_ordsku_rec.qty_ordered ;
  L_temp_ordsku_rec. ti_pack_tmpl_qty       := O_ordsku_rec.pack_tmpl_qty ;
  L_temp_ordsku_rec. supp_pack_size         := O_ordsku_rec.supp_pack_size ;
  L_temp_ordsku_rec. ti_standard_uom        := O_ordsku_rec.standard_uom ;
  L_temp_ordsku_rec. ti_qty_prescaled       := O_ordsku_rec.qty_prescaled ;
  L_temp_ordsku_rec. pickup_loc             := O_ordsku_rec.pickup_loc ;
  L_temp_ordsku_rec. pickup_no              := O_ordsku_rec.pickup_no ;
  L_temp_ordsku_rec. order_no               := O_ordsku_rec.order_no ;
  L_temp_ordsku_rec. ti_cost_source_base    := O_ordsku_rec.cost_source_base ;
  L_temp_ordsku_rec. ti_orig_cost           := O_ordsku_rec.orig_cost ;
  L_temp_ordsku_rec. ti_deposit_item_type   := O_ordsku_rec.deposit_item_type ;
  L_temp_ordsku_rec. ti_total_order_cost_ord    := O_ordsku_rec.total_order_cost_ord ;
  L_temp_ordsku_rec. ti_total_allocated_qty := O_ordsku_rec.total_allocated_qty ;
   L_temp_ordsku_rec. ti_total_order_cost_prim    := O_ordsku_rec.total_order_cost_prim ;
  QUERY_ORDSKU(O_error_message, L_temp_ordsku_rec, I_supplier, I_exchange_rate, I_fash_prepack_ind, I_import_country_id, I_currency_ord, I_currency_prim);
  IF O_error_message IS NOT NULL THEN
    RETURN 0;
  END IF;
  O_ordsku_rec. item               := L_temp_ordsku_rec. item ;
  O_ordsku_rec.item_desc           := L_temp_ordsku_rec. ti_item_desc ;
  O_ordsku_rec. origin_country_id  := L_temp_ordsku_rec. origin_country_id ;
  O_ordsku_rec.supplier_cost_ord   := L_temp_ordsku_rec. ti_supplier_cost_ord ;
  O_ordsku_rec.cost_source         := L_temp_ordsku_rec. ti_cost_source ;
  O_ordsku_rec.unit_elc_ord        := L_temp_ordsku_rec. ti_unit_elc_ord ;
  O_ordsku_rec.unit_of_purch       := L_temp_ordsku_rec. ti_unit_of_purch ;
  O_ordsku_rec. non_scale_ind      := L_temp_ordsku_rec. non_scale_ind ;
  O_ordsku_rec.qty_received        := L_temp_ordsku_rec. ti_qty_received ;
  O_ordsku_rec.unit_expense_ord    := L_temp_ordsku_rec. ti_unit_expense_ord ;
  O_ordsku_rec.unit_expense_prim   := L_temp_ordsku_rec. ti_unit_expense_prim ;
  O_ordsku_rec.total_expenses_ord  := L_temp_ordsku_rec. ti_total_expenses_ord ;
  O_ordsku_rec.total_expenses_prim := L_temp_ordsku_rec. ti_total_expenses_prim ;
  O_ordsku_rec.unit_duty_ord       := L_temp_ordsku_rec. ti_unit_duty_ord ;
  O_ordsku_rec.unit_duty_prim      := L_temp_ordsku_rec. ti_unit_duty_prim ;
  O_ordsku_rec.total_duty_ord      := L_temp_ordsku_rec. ti_total_duty_ord ;
  O_ordsku_rec.total_duty_prim     := L_temp_ordsku_rec. ti_total_duty_prim ;
  O_ordsku_rec.unit_elc_prim       := L_temp_ordsku_rec. ti_unit_elc_prim ;
  O_ordsku_rec.total_elc_ord       := L_temp_ordsku_rec. ti_total_elc_ord ;
  O_ordsku_rec.total_elc_prim      := L_temp_ordsku_rec. ti_total_elc_prim ;
  O_ordsku_rec. earliest_ship_date := L_temp_ordsku_rec. earliest_ship_date ;
  O_ordsku_rec. latest_ship_date   := L_temp_ordsku_rec. latest_ship_date ;
  O_ordsku_rec. ref_item           := L_temp_ordsku_rec. ref_item ;
  O_ordsku_rec.ref_item_desc       := L_temp_ordsku_rec. ti_ref_item_desc ;
  O_ordsku_rec.unit_cost_prim      := L_temp_ordsku_rec. ti_unit_cost_prim ;
  O_ordsku_rec.supplier_cost_prim  := L_temp_ordsku_rec. ti_supplier_cost_prim ;
  O_ordsku_rec.unit_cost           := L_temp_ordsku_rec. ti_unit_cost ;
  O_ordsku_rec.vpn                 := L_temp_ordsku_rec. ti_vpn ;
  O_ordsku_rec.qty_ordered         := L_temp_ordsku_rec. ti_qty_ordered ;
  O_ordsku_rec.pack_tmpl_qty       := L_temp_ordsku_rec. ti_pack_tmpl_qty ;
  O_ordsku_rec. supp_pack_size     := L_temp_ordsku_rec. supp_pack_size ;
  O_ordsku_rec.standard_uom        := L_temp_ordsku_rec. ti_standard_uom ;
  O_ordsku_rec.qty_prescaled       := L_temp_ordsku_rec. ti_qty_prescaled ;
  O_ordsku_rec. pickup_loc         := L_temp_ordsku_rec. pickup_loc ;
  O_ordsku_rec. pickup_no          := L_temp_ordsku_rec. pickup_no ;
  O_ordsku_rec. order_no           := L_temp_ordsku_rec. order_no ;
  O_ordsku_rec.cost_source_base    := L_temp_ordsku_rec. ti_cost_source_base ;
  O_ordsku_rec.orig_cost           := L_temp_ordsku_rec. ti_orig_cost ;
  O_ordsku_rec.deposit_item_type   := L_temp_ordsku_rec. ti_deposit_item_type ;
  O_ordsku_rec.total_order_cost_ord    := L_temp_ordsku_rec. ti_total_order_cost_ord;
  O_ordsku_rec.total_order_cost_prim    := L_temp_ordsku_rec. ti_total_order_cost_prim;
  O_ordsku_rec.total_allocated_qty := L_temp_ordsku_rec. ti_total_allocated_qty;
  RETURN 1;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', sqlerrm, L_program, NULL);
  RETURN 0;
END QUERY_ORDSKU;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ORDLOC_COSTS(O_error_message   IN OUT   VARCHAR2,
                            I_order_no        IN       ORDLOC.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64)   := 'ORDER_CALC_SQL.CHECK_ORDLOC_COSTS';
   L_item             ITEM_MASTER.ITEM%TYPE;
   L_imp_ord_ind      ORDHEAD.IMPORT_ORDER_IND%TYPE   := 'N';

   cursor C_GET_IMP_IND is
      select 'Y'
        from ordhead
       where order_no = I_order_no
         and import_order_ind = 'Y'
         and import_type in ('M','X')
         and import_id is not null;

   cursor C_CHECK_ORDLOC_COSTS is
      select item
        from (select order_no,
                     item,
                     unit_cost,
                     min(unit_cost) over (partition by order_no, item) cost_min
                from ordloc
               where order_no = I_order_no)
       where unit_cost <> cost_min;

BEGIN
   -- This function will check if all the locations of the item has the same unit cost,
   -- for an import order and if REIM is used as an external invoice matching system.

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_IMP_IND','ORDHEAD',NULL);
   open C_GET_IMP_IND;

   SQL_LIB.SET_MARK('FETCH','C_GET_IMP_IND','ORDHEAD',NULL);
   fetch C_GET_IMP_IND into L_imp_ord_ind;

   SQL_LIB.SET_MARK('CLOSE','C_GET_IMP_IND','ORDHEAD',NULL);
   close C_GET_IMP_IND;

   if NVL(L_imp_ord_ind, 'N') = 'N' then
      return TRUE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_ORDLOC_COSTS','ORDLOC',NULL);
   open C_CHECK_ORDLOC_COSTS;

   SQL_LIB.SET_MARK('FETCH','C_CHECK_ORDLOC_COSTS','ORDLOC',NULL);
   fetch C_CHECK_ORDLOC_COSTS into L_item;

   SQL_LIB.SET_MARK('CLOSE','C_CHECK_ORDLOC_COSTS','ORDLOC',NULL);

   if C_CHECK_ORDLOC_COSTS%NOTFOUND then
      close C_CHECK_ORDLOC_COSTS;
      return TRUE;
   else
      O_error_message := SQL_LIB.CREATE_MSG('UPDATE_LOC_COSTS',
                                            L_item,
                                            NULL,
                                            NULL);
      close C_CHECK_ORDLOC_COSTS;
      return FALSE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            sqlerrm,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_ORDLOC_COSTS;
------------------------------------------------------------------------------------------
-- Function:  CALC_DEPOSIT_PACK_COMP_WRP
-- Purpose:   This is a wrapper function for CALCULATE_DEPOSIT_PACK_COMP that returns a
--            database object type instead of a PL/SQL type to be called from Java wrappers.
------------------------------------------------------------------------------------------
FUNCTION CALC_DEPOSIT_PACK_COMP_WRP (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_pack_comp_tbl       IN OUT   WRP_ORDSKU_COMPLEX_PACK_TBL,
                                     I_pack_no             IN       PACKITEM.PACK_NO%TYPE,
                                     I_order_status        IN       ORDHEAD.STATUS%TYPE,
                                     I_supp_pack_size      IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                                     I_supplier            IN       SUPS.SUPPLIER%TYPE,
                                     I_origin_country_id   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                     I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                                     I_location            IN       ORDLOC.LOCATION%TYPE,
                                     I_order_curr          IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                     I_exchange_rate       IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE)
RETURN INTEGER IS
  L_program                   VARCHAR2(64) := 'ORDER_CALC_SQL.CALC_DEPOSIT_PACK_COMP_WRP';
  L_ordsku_complex_pack_tbl   ORDER_CALC_SQL.ORDSKU_COMPLEX_PACK_TBL;
BEGIN

   if CALCULATE_DEPOSIT_PACK_COMP(O_error_message,
                                  L_ordsku_complex_pack_tbl,
                                  I_pack_no,
                                  I_order_status,
                                  I_supp_pack_size,
                                  I_supplier,
                                  I_origin_country_id,
                                  I_order_no,
                                  I_location,
                                  I_order_curr,
                                  I_exchange_rate) = FALSE then
      return 0;
   end if;

   if L_ordsku_complex_pack_tbl is not NULL and L_ordsku_complex_pack_tbl.COUNT > 0 then
      O_pack_comp_tbl := WRP_ORDSKU_COMPLEX_PACK_TBL();
      for i in L_ordsku_complex_pack_tbl.FIRST .. L_ordsku_complex_pack_tbl.LAST loop
         O_pack_comp_tbl.extend;
         O_pack_comp_tbl(i) := WRP_ORDSKU_COMPLEX_PACK_REC(L_ordsku_complex_pack_tbl(i).item,
                                                           L_ordsku_complex_pack_tbl(i).deposit_item_type,
                                                           L_ordsku_complex_pack_tbl(i).uop_order_cost,
                                                           L_ordsku_complex_pack_tbl(i).supplier_unit_cost,
                                                           L_ordsku_complex_pack_tbl(i).uop_qty_ordered);
      end loop;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            sqlerrm,
                                            L_program,
                                            NULL);
      return 0;
END CALC_DEPOSIT_PACK_COMP_WRP;
------------------------------------------------------------------------------------------
FUNCTION ITEM_LOC_COSTS(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_landed_cost_ord           IN OUT   NUMBER,
                        O_landed_cost_prim          IN OUT   NUMBER,
                        O_landed_cost_local         IN OUT   NUMBER,
                        O_total_landed_cost_ord     IN OUT   NUMBER,
                        O_total_landed_cost_prim    IN OUT   NUMBER,
                        O_total_landed_cost_local   IN OUT   NUMBER,
                        O_expense_ord               IN OUT   NUMBER,
                        O_expense_prim              IN OUT   NUMBER,
                        O_expense_local             IN OUT   NUMBER,
                        O_total_expense_ord         IN OUT   NUMBER,
                        O_total_expense_prim        IN OUT   NUMBER,
                        O_total_expense_local       IN OUT   NUMBER,
                        O_duty_ord                  IN OUT   NUMBER,
                        O_duty_prim                 IN OUT   NUMBER,
                        O_duty_local                IN OUT   NUMBER,
                        O_total_duty_ord            IN OUT   NUMBER,
                        O_total_duty_prim           IN OUT   NUMBER,
                        O_total_duty_local          IN OUT   NUMBER,
                        O_qty                       IN OUT   ORDLOC.QTY_ORDERED%TYPE,
                        I_order_no                  IN       ORDHEAD.ORDER_NO%TYPE,
                        I_exchange_rate             IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                        I_currency_code             IN       ORDHEAD.CURRENCY_CODE%TYPE,
                        I_item                      IN       ITEM_MASTER.ITEM%TYPE,
                        I_location                  IN       ORDLOC.LOCATION%TYPE,
                        I_loc_type                  IN       ORDLOC.LOC_TYPE%TYPE,
                        I_order_qty                 IN       ORDLOC.QTY_ORDERED%TYPE,
                        I_supplier                  IN       SUPS.SUPPLIER%TYPE,
                        I_origin_country            IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                        I_import_country            IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(40) := 'ORDER_CALC_SQL.ITEM_LOC_COSTS';
   L_primary_currency   CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate      ORDHEAD.EXCHANGE_RATE%TYPE;
   L_local_currency_code  ORDHEAD.CURRENCY_CODE%TYPE;

   cursor C_GET_STORE_CURRENCY(P_location STORE.STORE%type) is
      select currency_code
       from store
      where store = P_location;

   cursor C_GET_WH_CURRENCY(P_location WH.Wh%type) is
     select currency_code
       from wh
      where wh = P_location;

BEGIN

   if I_location is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_loc_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if ITEM_LOC_COSTS(O_error_message,
                      O_landed_cost_ord,
                      O_landed_cost_prim,
                      O_total_landed_cost_ord,
                      O_total_landed_cost_prim,
                      O_expense_ord,
                      O_expense_prim,
                      O_total_expense_ord,
                      O_total_expense_prim,
                      O_duty_ord,
                      O_duty_prim,
                      O_total_duty_ord,
                      O_total_duty_prim,
                      O_qty,
                      I_order_no,
                      I_exchange_rate,
                      I_currency_code,
                      I_item,
                      I_location,
                      I_loc_type ,
                      I_order_qty ,
                      I_supplier,
                      I_origin_country,
                      I_import_country) = FALSE then
         return false;
   end if;

   O_landed_cost_local       := 0;
   O_total_landed_cost_local := 0;
   O_expense_local           := 0;
   O_total_duty_local        := 0;
   ---
   if LP_elc_ind is NULL then
      if SYSTEM_OPTIONS_SQL.GET_ELC_IND (O_error_message,
                                         LP_elc_ind) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_loc_type = 'S' then
      ---
      open C_GET_STORE_CURRENCY(I_location);
      fetch C_GET_STORE_CURRENCY into L_local_currency_code;
      close C_GET_STORE_CURRENCY;
      ---
   elsif I_loc_type = 'W' then
      ---
      open C_GET_WH_CURRENCY(I_location);
      fetch C_GET_WH_CURRENCY into L_local_currency_code;
      close C_GET_WH_CURRENCY;
      ---
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE (O_error_message,
                                        L_primary_currency) = FALSE then
      return FALSE;
   end if;
   ---
   if LP_elc_ind = 'Y' then
   ---
      if CURRENCY_SQL.CONVERT (O_error_message,
                               O_expense_prim,
                               L_primary_currency,
                               L_local_currency_code,
                               O_expense_local,
                               'N',
                               NULL,
                               NULL,
                               NULL,
                               L_exchange_rate) = FALSE then
         return FALSE;
      end if;
      O_total_expense_local := O_expense_local * I_order_qty;
      ---
      if CURRENCY_SQL.CONVERT (O_error_message,
                               O_duty_prim,
                               L_primary_currency,
                               L_local_currency_code,
                               O_duty_local,
                               'N',
                               NULL,
                               NULL,
                               NULL,
                               L_exchange_rate) = FALSE then
         return FALSE;
      end if;
      ---
      O_total_duty_local := O_duty_local * I_order_qty;
	  ---
      if CURRENCY_SQL.CONVERT (O_error_message,
                               O_landed_cost_prim,
                               L_primary_currency,
                               L_local_currency_code,
                               O_landed_cost_local,
                               'N',
                               NULL,
                               NULL,
                               NULL,
                               L_exchange_rate) = FALSE then
         return FALSE;
      end if;
      ---
      O_total_landed_cost_local := O_landed_cost_local * I_order_qty;
      ---
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END ITEM_LOC_COSTS;
------------------------------------------------------------------------------------------------
END;
/
