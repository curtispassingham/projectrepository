
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORDER_DUE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------------------
-- Function Name: TOTAL_ESO_SSO
-- Purpose:       Sums the ESO(Estimated Stock Out) and the ASO(Accepted Stock Out) for an 
--                order, an order/item or an order/location. 
-------------------------------------------------------------------------------------------------------
FUNCTION   TOTAL_ESO_ASO    (O_error_message 	      IN OUT 	 VARCHAR2,
			     O_estimated_stock_out    IN OUT     repl_results.estimated_stock_out%TYPE,
                             O_accepted_stock_out     IN OUT     repl_results.accepted_stock_out%TYPE,
                             I_order_no               IN         repl_results.order_no%TYPE,
			     I_item		      IN	 repl_results.item%TYPE,
			     I_location		      IN 	 repl_results.location%TYPE) 
                             RETURN BOOLEAN; 
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-- Function Name: ORDER_DUE
-- Purpose:       Determines if an order as a whole is considered due.
-------------------------------------------------------------------------------------------------------
FUNCTION   ORDER_DUE        (O_error_message          IN OUT     VARCHAR2,
                             O_order_due              IN OUT     BOOLEAN,
                             I_order_no               IN         ordloc.order_no%TYPE)
                             RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-- Function Name: COUNT_ITEM_LOCS 
-- Purpose:       Retrieves the total number of locations with a recommended order quantity 
--                and total number of locations due for an order, order/item or order/location.	
-------------------------------------------------------------------------------------------------------
FUNCTION   COUNT_ITEM_LOCS  (O_error_message 	      IN OUT 	 VARCHAR2,
			     O_item_locs_total        IN OUT     NUMBER,
                             O_item_locs_due          IN OUT     NUMBER,
                             I_order_no               IN         repl_results.order_no%TYPE,
  			     I_item		      IN 	 repl_results.item%TYPE,
			     I_location		      IN 	 repl_results.location%TYPE) 
                             RETURN BOOLEAN; 
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
--Function Name: GET_DUE_ORD_IND
--Purpose:       Retrieves the due order process and due order indicators from the order 
--		 inventory management table for a passed in order.
-------------------------------------------------------------------------------------------------------
FUNCTION    GET_DUE_ORD_IND  (O_error_message	      IN OUT	 VARCHAR2,
			      O_due_ord_process_ind   IN OUT	 ord_inv_mgmt.due_ord_process_ind%TYPE,
			      O_due_ord_ind	      IN OUT	 ord_inv_mgmt.due_ord_ind%TYPE,
			      I_order_no	      IN	 repl_results.order_no%TYPE)
			      RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END ORDER_DUE_SQL;
/
