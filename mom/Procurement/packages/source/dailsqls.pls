CREATE OR REPLACE PACKAGE DEAL_ACTUALS_ITEM_LOC_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------
FUNCTION GET_NEXT_DAI_ID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_dai_id        IN OUT DEAL_ACTUALS_ITEM_LOC.DAI_ID%TYPE)
         RETURN BOOLEAN;
------------------------------------------------------------------
-- FUNCTION: INSERT_DEAL_BB_RCPT_SALES_TEMP
-- Purpose:  This function inserts billback rebate Sales and 
--           Receipt type of deal into temporary table 
------------------------------------------------------------------
FUNCTION INSERT_DEAL_BB_RCPT_SALES_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_next_eom_date IN DATE)
         RETURN BOOLEAN;
------------------------------------------------------------------
-- FUNCTION: INSERT_DEAL_BB_NO_REBATE_TEMP
-- Purpose:  This function inserts billback NO Rebate type of 
--           deal into temporary table 
------------------------------------------------------------------
FUNCTION INSERT_DEAL_BB_NO_REBATE_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
         RETURN BOOLEAN;
------------------------------------------------------------------
-- FUNCTION: DEAL_BB_REBATE_PO_TEMP
-- Purpose:  This function inserts billback rebate PO type of 
--           deal into temporary table
------------------------------------------------------------------
FUNCTION DEAL_BB_REBATE_PO_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
         RETURN BOOLEAN;
------------------------------------------------------------------
END DEAL_ACTUALS_ITEM_LOC_SQL;
/
