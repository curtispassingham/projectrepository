
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DEAL_DETAIL_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------
-- Filename: dealdetails/b.pls
-- Purpose : This is used to retrieve, update, add, delete data from/into
--         : DEAL_DETAIL table
-- Author  : Bong Gaspar (Accenture - ERC)
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
-- Function Name: GET_ATTRIB
-- Purpose      : Retrieves a row of data in the DEAL_DETAIL table given a
--              : deal_id and a deal_detail_id. This function is called by
--              : dealprom.fmb
----------------------------------------------------------------------------------
FUNCTION GET_ATTRIB (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists            IN OUT   BOOLEAN,
                     O_deal_detail_rec   IN OUT   DEAL_DETAIL%ROWTYPE,
                     I_deal_id           IN       DEAL_HEAD.DEAL_ID%TYPE,
                     I_deal_detail_id    IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
END DEAL_DETAIL_SQL;
/
