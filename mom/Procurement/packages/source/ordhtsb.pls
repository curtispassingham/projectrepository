CREATE OR REPLACE PACKAGE BODY ORDER_HTS_SQL AS
-------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ(O_error_message IN OUT VARCHAR2,
                      O_seq_no        IN OUT ORDSKU_HTS.SEQ_NO%TYPE,
                      I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(50) := 'ORDER_HTS_SQL.GET_NEXT_SEQ';

   cursor C_MAX_SEQ_NO is
      select nvl(MAX(seq_no),0) + 1
        from ordsku_hts
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_MAX_SEQ_NO','ORDSKU_HTS',
                    'Order number: ' || to_char(I_order_no));
   open C_MAX_SEQ_NO;
   ---
   SQL_LIB.SET_MARK('FETCH','C_MAX_SEQ_NO','ORDSKU_HTS',
                    'Order number: ' || to_char(I_order_no));
   fetch C_MAX_SEQ_NO into O_seq_no;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_MAX_SEQ_NO','ORDSKU_HTS',
                    'Order number: ' || to_char(I_order_no));
   close C_MAX_SEQ_NO;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_SEQ;
-----------------------------------------------------------------------------------------
FUNCTION INSERT_HTS_ASSESS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                           I_item            IN     ITEM_MASTER.ITEM%TYPE,
                           I_pack_item       IN     ITEM_MASTER.ITEM%TYPE,
                           I_com             IN     COUNTRY.COUNTRY_ID%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(40) := 'ORDER_HTS_SQL.INSERT_HTS_ASSESS';
   L_import_country_id  COUNTRY.COUNTRY_ID%TYPE;
   L_origin_country_id  COUNTRY.COUNTRY_ID%TYPE;
   L_not_after_date     ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_written_date       ORDHEAD.WRITTEN_DATE%TYPE;
   L_seq_no             ORDSKU_HTS.SEQ_NO%TYPE;
   L_import_hts_date    SYSTEM_OPTIONS.IMPORT_HTS_DATE%TYPE;
   L_hts_date           ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_pickup_date        ORDHEAD.PICKUP_DATE%TYPE;
   L_not_before_date    ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_hts_tracking_level SYSTEM_OPTIONS.HTS_TRACKING_LEVEL%TYPE;
   L_supplier           ORDHEAD.SUPPLIER%TYPE;

   L_exists              BOOLEAN;
   ---
   cursor C_SEQ_NO is
      select nvl(max(seq_no), 0)
        from ordsku_hts
       where order_no = I_order_no;

   cursor C_GET_IMPORT_HTS_DATE is
      select import_hts_date,
             hts_tracking_level
        from system_options;

   cursor C_GET_IMP_ORIGIN_CTRY is
      select oh.import_country_id,
             os.origin_country_id,
             oh.not_after_date,
             oh.written_date,
             oh.supplier
        from ordhead oh, 
             ordsku os
       where oh.order_no = I_order_no
         and oh.order_no = os.order_no
         and os.item     = NVL(I_pack_item, I_item);

   cursor C_HTS_CHAPTER is 
      select distinct h.chapter
        from hts h,
             ordsku_hts osh
       where osh.order_no          = I_order_no
         and osh.hts               = h.hts
         and osh.import_country_id = L_import_country_id   
         and osh.item              = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_IMPORT_HTS_DATE','SYSTEM_OPTIONS', NULL);
   open C_GET_IMPORT_HTS_DATE;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_IMPORT_HTS_DATE','SYSTEM_OPTIONS', NULL);
   fetch C_GET_IMPORT_HTS_DATE into L_import_hts_date,
                                    L_hts_tracking_level;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_IMPORT_HTS_DATE','SYSTEM_OPTIONS', NULL);
   close C_GET_IMPORT_HTS_DATE;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDSKU_HTS', 'ORDER : '||to_char(I_order_no));
   open C_SEQ_NO;
   SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDSKU_HTS', 'ORDER : '||to_char(I_order_no));
   fetch C_SEQ_NO into L_seq_no;
   SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO', 'ORDSKU_HTS', 'ORDER : '||to_char(I_order_no));
   close C_SEQ_NO;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_IMP_ORIGIN_CTRY','ORDHEAD, ORDSKU', NULL);
   open C_GET_IMP_ORIGIN_CTRY;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_IMP_ORIGIN_CTRY','ORDHEAD, ORDSKU', NULL);
   fetch C_GET_IMP_ORIGIN_CTRY into L_import_country_id,
                                    L_origin_country_id,
                                    L_not_after_date,
                                    L_written_date,
                                    L_supplier;
   if C_GET_IMP_ORIGIN_CTRY%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORD_ITEM', NULL, NULL, NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_IMP_ORIGIN_CTRY','ORDHEAD, ORDSKU', NULL);
      close C_GET_IMP_ORIGIN_CTRY;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_IMP_ORIGIN_CTRY','ORDHEAD, ORDSKU', NULL);
   close C_GET_IMP_ORIGIN_CTRY;
   ---
   if L_import_hts_date = 'N' then
      if L_not_after_date is NULL then
         if ORDER_CALC_SQL.CALC_HEADER_DATES(O_error_message,
                                             L_pickup_date,
                                             L_not_before_date,
                                             L_hts_date,
                                             I_order_no,
                                             NULL) = FALSE then
            return FALSE;
         end if;
      else
         L_hts_date := L_not_after_date;
      end if;
   else
      L_hts_date := L_written_date;
   end if;
   ---
   if L_hts_tracking_level = 'M' then
      if I_com is null then
         if ITEM_SUPP_MANU_COUNTRY_SQL.GET_PRIMARY_MANU_COUNTRY(O_error_message,
                                                                L_exists,
                                                                L_origin_country_id,
                                                                I_item,
                                                                L_supplier) = FALSE then
            return FALSE;
         end if;
      else
         L_origin_country_id := I_com;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS', NULL);
   insert into ordsku_hts(order_no,
                          seq_no,
                          item,
                          pack_item,
                          hts,
                          import_country_id,
                          origin_country_id,
                          effect_from,
                          effect_to,
                          status)
      select I_order_no,
             L_seq_no + rownum,
             I_item,
             I_pack_item,
             hts,
             i.import_country_id,
             i.origin_country_id,
             i.effect_from,
             i.effect_to,
             i.status
        from item_hts i
       where item = I_item
         and import_country_id = L_import_country_id
         and origin_country_id = L_origin_country_id
         and i.effect_from    <= L_hts_date
         and i.effect_to      >= L_hts_date
         and not exists(select 'Y'
                          from ordsku_hts o
                         where o.order_no         = I_order_no
                           and o.item             = I_item
                           and ((o.pack_item      = I_pack_item and I_pack_item is not NULL)
                                or (I_pack_item is NULL and o.pack_item is NULL))
                           and o.hts              = i.hts);
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
   insert into ordsku_hts_assess(order_no,
                                 seq_no,
                                 comp_id,
                                 cvb_code,
                                 comp_rate,
                                 per_count,
                                 per_count_uom,
                                 est_assess_value,
                                 nom_flag_1,
                                 nom_flag_2,
                                 nom_flag_3,
                                 nom_flag_4,
                                 nom_flag_5,
                                 display_order)
      select I_order_no,
             oh.seq_no,
             it.comp_id,
             it.cvb_code,
             it.comp_rate,
             it.per_count,
             it.per_count_uom,
             it.est_assess_value,
             it.nom_flag_1,
             it.nom_flag_2,
             it.nom_flag_3,
             it.nom_flag_4,
             it.nom_flag_5,
             it.display_order
        from item_hts_assess it, 
             ordsku_hts oh,
             elc_comp elc
       where oh.order_no          = I_order_no
         and it.item              = I_item
         and it.import_country_id = L_import_country_id
         and it.origin_country_id = L_origin_country_id
         and it.comp_id           = elc.comp_id
         and oh.item              = it.item
         and oh.hts               = it.hts
         and oh.effect_from       = it.effect_from
         and oh.effect_to         = it.effect_to
         and not exists(select 'Y'
                          from ordsku_hts_assess o
                         where o.order_no = I_order_no
                           and o.seq_no   = oh.seq_no
                           and o.comp_id  = it.comp_id);
   
   ---
   -- Loop through all the chapters associated with the item
   -- and default HTS Chapter Docs. 
   ---
   FOR C_chapter in C_HTS_CHAPTER LOOP
      if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                    'HTSC',
                                    'POIT',
                                    C_chapter.chapter,
                                    I_order_no,
                                    L_import_country_id,
                                    I_item) = FALSE then
         return FALSE;
      end if;
   END LOOP;  -- C_HTS_CHAPTER LOOP 
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_HTS_ASSESS;
-------------------------------------------------------------------------------
FUNCTION HTS_EXIST(O_error_message     IN OUT VARCHAR2,
                   O_exists            IN OUT BOOLEAN,
                   I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                   I_item              IN     ITEM_MASTER.ITEM%TYPE,
                   I_pack_item         IN     ITEM_MASTER.ITEM%TYPE,
                   I_hts               IN     HTS.HTS%TYPE)
RETURN BOOLEAN IS
   L_exists  VARCHAR2(1);
   L_program VARCHAR2(50) := 'ORDER_HTS_SQL.HTS_EXIST';

   cursor C_CHECK_EXISTS is
      select 'Y'
        from ordsku_hts
       where order_no          = I_order_no
         and item              = I_item
         and hts               = I_hts
         and (pack_item        = I_pack_item 
          or (pack_item is NULL and I_pack_item is NULL));

BEGIN
   O_exists := TRUE;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_EXISTS','ORDSKU_HTS',NULL);
   open C_CHECK_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_EXISTS','ORDSKU_HTS',NULL);
   fetch C_CHECK_EXISTS into L_exists;

   if C_CHECK_EXISTS%NOTFOUND then
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXISTS','ORDSKU_HTS',NULL);
   close C_CHECK_EXISTS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END HTS_EXIST;

-------------------------------------------------------------------------------
FUNCTION ASSESS_EXIST(O_error_message     IN OUT VARCHAR2,
                      O_exists            IN OUT BOOLEAN,
                      I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                      I_seq_no            IN     ORDSKU_HTS_ASSESS.SEQ_NO%TYPE,
                      I_comp_id           IN     ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN IS
   L_exists  VARCHAR2(1);
   L_program VARCHAR2(50) := 'ORDER_HTS_SQL.ASSESS_EXIST';

   cursor C_CHECK_EXISTS is
      select 'Y'
        from ordsku_hts_assess
       where order_no = I_order_no
         and seq_no   = I_seq_no
         and comp_id  = I_comp_id;

BEGIN
   O_exists := TRUE;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_EXISTS','ORDSKU_HTS_ASSESS',
                    'Order Number: ' || to_char(I_order_no) ||
                    ', Sequence Number: ' || to_char(I_seq_no) ||
                    ', Component: ' || I_comp_id);
   open C_CHECK_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_EXISTS','ORDSKU_HTS_ASSESS',
                    'Order Number: ' || to_char(I_order_no) ||
                    ', Sequence Number: ' || to_char(I_seq_no) ||
                    ', Component: ' || I_comp_id);
   fetch C_CHECK_EXISTS into L_exists;

   if C_CHECK_EXISTS%NOTFOUND then
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXISTS','ORDSKU_HTS_ASSESS',
                    'Order Number: ' || to_char(I_order_no) ||
                    ', Sequence Number: ' || to_char(I_seq_no) ||
                    ', Component: ' || I_comp_id);
   close C_CHECK_EXISTS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ASSESS_EXIST;
-------------------------------------------------------------------------------
FUNCTION DELETE_ASSESS(O_error_message  IN OUT VARCHAR2,
                       I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                       I_seq_no         IN     ORDSKU_HTS_ASSESS.SEQ_NO%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'ORDER_HTS_SQL.DELETE_ASSESS';
   L_table       VARCHAR2(30) := 'ORDSKU_HTS_ASSESS';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ORDSKU_HTS_ASSESS is
      select 'Y'
        from ordsku_hts_assess
       where order_no  = I_order_no
         and seq_no    = I_seq_no
         for update nowait; 

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDSKU_HTS_ASSESS','ORDSKU_HTS_ASSESS',
                    'Order number: ' || to_char(I_order_no) ||  
                    ', Sequence: ' || to_char(I_seq_no));
   open C_LOCK_ORDSKU_HTS_ASSESS;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDSKU_HTS_ASSESS','ORDSKU_HTS_ASSESS',
                    'Order number: ' || to_char(I_order_no) ||  
                    ', Sequence: ' || to_char(I_seq_no));
   close C_LOCK_ORDSKU_HTS_ASSESS;

   SQL_LIB.SET_MARK('DELETE',NULL,'ORDSKU_HTS_ASSESS','Order number: ' || 
                    to_char(I_order_no) ||', Sequence: ' || to_char(I_seq_no));
   delete from ordsku_hts_assess
         where order_no = I_order_no
           and seq_no   = I_seq_no;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             to_char(I_order_no),
                                             to_char(I_seq_no));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_ASSESS;
-------------------------------------------------------------------------------
FUNCTION GET_HTS_DETAILS(O_error_message        IN OUT VARCHAR2,
                         O_tariff_treatment     IN OUT HTS_TARIFF_TREATMENT.TARIFF_TREATMENT%TYPE,
                         O_qty_1                IN OUT NUMBER,
                         O_qty_2                IN OUT NUMBER,
                         O_qty_3                IN OUT NUMBER,
                         O_units_1              IN OUT HTS.UNITS_1%TYPE,
                         O_units_2              IN OUT HTS.UNITS_2%TYPE,
                         O_units_3              IN OUT HTS.UNITS_3%TYPE,
                         O_specific_rate        IN OUT HTS_TARIFF_TREATMENT.SPECIFIC_RATE%TYPE,
                         O_av_rate              IN OUT HTS_TARIFF_TREATMENT.AV_RATE%TYPE,
                         O_other_rate           IN OUT HTS_TARIFF_TREATMENT.OTHER_RATE%TYPE,
                         O_cvd_case_no          IN OUT HTS_CVD.CASE_NO%TYPE,
                         O_ad_case_no           IN OUT HTS_AD.CASE_NO%TYPE,
                         O_duty_comp_code       IN OUT HTS.DUTY_COMP_CODE%TYPE,
                         I_item                 IN     ITEM_MASTER.ITEM%TYPE,
                         I_order_no             IN     ORDHEAD.ORDER_NO%TYPE,
                         I_hts                  IN     HTS.HTS%TYPE,
                         I_import_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                         I_duty_calc_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                         I_effect_from          IN     HTS.EFFECT_FROM%TYPE,
                         I_effect_to            IN     HTS.EFFECT_TO%TYPE,
                         I_pack_item            IN     ITEM_MASTER.ITEM%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
   L_supplier            SUPS.SUPPLIER%TYPE;
   L_qty_1               NUMBER;
   L_qty_2               NUMBER;
   L_qty_3               NUMBER;
   L_order_qty           ORDLOC.QTY_ORDERED%TYPE;
   L_origin_country_id   COUNTRY.COUNTRY_ID%TYPE;
   L_manu_country_id     COUNTRY.COUNTRY_ID%TYPE;
   L_system_options_rec  SYSTEM_OPTIONS%ROWTYPE;
   L_program             VARCHAR2(50) := 'ORDER_HTS_SQL.GET_HTS_DETAILS';
   L_clearing_zone_id    ORDHEAD.CLEARING_ZONE_ID%TYPE;

   cursor C_GET_ORDER_QTY is
      select SUM(qty_ordered)
        from ordloc
       where order_no = I_order_no
         and item     = I_item;

   cursor C_GET_ORD_SUPP is
      select supplier
        from ordhead
       where order_no = I_order_no;

   cursor C_GET_ORD_SUPP_CTRY is
      select oh.supplier,
             os.origin_country_id
        from ordhead oh,
             ordsku os
       where oh.order_no = I_order_no
         and oh.order_no = os.order_no
         and os.item    = NVL(I_pack_item,I_item);

   cursor C_GET_CLEARING_ZONE is
      select clearing_zone_id
        from ordhead
       where order_no = I_order_no;

BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;
   ---
   if L_system_options_rec.hts_tracking_level = 'M' then
      SQL_LIB.SET_MARK('OPEN','C_GET_ORD_SUPP_CTRY','ORDHEAD',
                       'Order Number: ' || to_char(I_order_no));
      open C_GET_ORD_SUPP_CTRY;
      SQL_LIB.SET_MARK('FETCH','C_GET_ORD_SUPP_CTRY','ORDHEAD',
                       'Order Number: ' || to_char(I_order_no));
      fetch C_GET_ORD_SUPP_CTRY into L_supplier, L_origin_country_id;

      if C_GET_ORD_SUPP_CTRY%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_ORD_SUPP_CTRY','ORDHEAD',
                          'Order Number: ' || to_char(I_order_no));
         close C_GET_ORD_SUPP_CTRY;
         O_error_message := SQL_LIB.CREATE_MSG('NO_ORDSKU_HTS_DETL',NULL,NULL,NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_GET_ORD_SUPP_CTRY','ORDHEAD',
                       'Order Number: ' || to_char(I_order_no));
      close C_GET_ORD_SUPP_CTRY;
      L_manu_country_id := I_duty_calc_country_id;
   else
      SQL_LIB.SET_MARK('OPEN','C_GET_ORD_SUPP','ORDHEAD',
                       'Order Number: ' || to_char(I_order_no));
      open C_GET_ORD_SUPP;
      SQL_LIB.SET_MARK('FETCH','C_GET_ORD_SUPP','ORDHEAD',
                       'Order Number: ' || to_char(I_order_no));
      fetch C_GET_ORD_SUPP into L_supplier;

      if C_GET_ORD_SUPP%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_ORD_SUPP','ORDHEAD',
                          'Order Number: ' || to_char(I_order_no));
         close C_GET_ORD_SUPP;
         O_error_message := SQL_LIB.CREATE_MSG('SUPP_NOT_EXIST',NULL,NULL,NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_GET_ORD_SUPP','ORDHEAD',
                       'Order Number: ' || to_char(I_order_no));
      close C_GET_ORD_SUPP;
      L_origin_country_id := I_duty_calc_country_id;
   end if;
   ---

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_CLEARING_ZONE',
                    'ORDHEAD',
                    'Order Number: ' || TO_CHAR(I_order_no));
   open C_GET_CLEARING_ZONE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_CLEARING_ZONE',
                    'ORDHEAD',
                    'Order Number: ' || TO_CHAR(I_order_no));
   fetch C_GET_CLEARING_ZONE into L_clearing_zone_id;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_CLEARING_ZONE',
                    'ORDHEAD',
                    'Order Number: ' || TO_CHAR(I_order_no));
   close C_GET_CLEARING_ZONE;

   if ITEM_HTS_SQL.GET_HTS_DETAILS(O_error_message,
                                   O_tariff_treatment,
                                   L_qty_1,
                                   L_qty_2,
                                   L_qty_3,
                                   O_units_1,
                                   O_units_2,
                                   O_units_3,
                                   O_specific_rate,
                                   O_av_rate,
                                   O_other_rate,
                                   O_cvd_case_no,
                                   O_ad_case_no,
                                   O_duty_comp_code,
                                   I_item,
                                   L_supplier,
                                   I_hts,
                                   I_import_country_id,
                                   L_origin_country_id,
                                   L_manu_country_id,
                                   I_effect_from,
                                   I_effect_to,
                                   L_clearing_zone_id) = FALSE then
      return FALSE;
   end if;

   --- Get the order quantity to evaluate the order item record.
   SQL_LIB.SET_MARK('OPEN','C_GET_ORDER_QTY','ORDLOC',
                    'Order Number: ' || to_char(I_order_no) ||
                    ', Item: ' || I_item);
   open C_GET_ORDER_QTY;
   SQL_LIB.SET_MARK('FETCH','C_GET_ORDER_QTY','ORDLOC',
                    'Order Number: ' || to_char(I_order_no) ||
                    ', Item: ' || I_item);
   fetch C_GET_ORDER_QTY into L_order_qty;

   if C_GET_ORDER_QTY%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_GET_ORDER_QTY','ORDLOC',
                       'Order Number: ' || to_char(I_order_no) ||
                       ', Item: ' || I_item);
      close C_GET_ORDER_QTY;
      O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_QTY2',to_char(I_order_no),NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_GET_ORDER_QTY','ORDLOC',
                    'Order Number: ' || to_char(I_order_no) ||
                    ', Item: ' || I_item);
   close C_GET_ORDER_QTY;
   
   O_qty_1 := L_order_qty * L_qty_1;
   O_qty_2 := L_order_qty * L_qty_2;
   O_qty_3 := L_order_qty * L_qty_3;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            to_char(SQLCODE));
      return FALSE;
END GET_HTS_DETAILS;
--------------------------------------------------------------------------------
FUNCTION DEFAULT_ASSESS(O_error_message         IN OUT VARCHAR2,
                        I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                        I_seq_no                IN     ORDSKU_HTS.SEQ_NO%TYPE,
                        I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                        I_hts                   IN     HTS.HTS%TYPE,
                        I_import_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                        I_origin_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                        I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                        I_effect_to             IN     HTS.EFFECT_TO%TYPE)
   RETURN BOOLEAN IS
   
   L_program            VARCHAR2(65) := 'ORDER_HTS_SQL.DEFAULT_ASSESS';
   L_tariff_treatment   HTS_TARIFF_TREATMENT.TARIFF_TREATMENT%TYPE;
   L_qty_1              NUMBER;
   L_qty_2              NUMBER;
   L_qty_3              NUMBER;
   L_units_1            HTS.UNITS_1%TYPE;
   L_units_2            HTS.UNITS_2%TYPE;
   L_units_3            HTS.UNITS_3%TYPE;
   L_specific_rate      HTS_TARIFF_TREATMENT.SPECIFIC_RATE%TYPE := 0;
   L_av_rate            HTS_TARIFF_TREATMENT.AV_RATE%TYPE       := 0;
   L_other_rate         HTS_TARIFF_TREATMENT.OTHER_RATE%TYPE    := 0;
   L_comp_rate          ELC_COMP.COMP_RATE%TYPE                 := 0;
   L_per_count          ELC_COMP.PER_COUNT%TYPE;
   L_per_count_uom      ELC_COMP.PER_COUNT_UOM%TYPE;
   L_comp_id            ELC_COMP.COMP_ID%TYPE;
   L_cvd_case_no        HTS_CVD.CASE_NO%TYPE;
   L_ad_case_no         HTS_AD.CASE_NO%TYPE;
   L_duty_comp_code     HTS.DUTY_COMP_CODE%TYPE;
   L_tax_comp_code      HTS_TAX.TAX_COMP_CODE%TYPE;
   L_tax_type           HTS_TAX.TAX_TYPE%TYPE;
   L_tax_av_rate        HTS_TAX.TAX_AV_RATE%TYPE;
   L_tax_specific_rate  HTS_TAX.TAX_SPECIFIC_RATE%TYPE;
   L_fee_comp_code      HTS_FEE.FEE_COMP_CODE%TYPE;
   L_fee_type           HTS_FEE.FEE_TYPE%TYPE;
   L_fee_av_rate        HTS_FEE.FEE_AV_RATE%TYPE;
   L_fee_specific_rate  HTS_FEE.FEE_SPECIFIC_RATE%TYPE;
   L_pack_item          ORDSKU_HTS.PACK_ITEM%TYPE               := NULL;
   L_hts_exists         BOOLEAN;

   cursor C_GET_CVD_RATE is
      select rate
        from hts_cvd
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and origin_country_id = I_origin_country_id
         and case_no           = L_cvd_case_no;

   cursor C_GET_AD_RATE is 
      select rate
        from hts_ad
       where hts = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and origin_country_id = I_origin_country_id
         and case_no           = L_ad_case_no;

   cursor C_GET_TAX_INFO is 
      select tax_type,
             tax_comp_code,
             tax_specific_rate,
             tax_av_rate
        from hts_tax
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to;

   cursor C_GET_FEE_INFO is 
      select fee_type,
             fee_comp_code,
             fee_specific_rate,
             fee_av_rate
        from hts_fee
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to;

   cursor C_GET_PACK is 
      select pack_item
        from ordsku_hts
       where order_no          = I_order_no
         and seq_no            = I_seq_no
         and hts               = I_hts;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_PACK', 'ORDSKU_HTS', NULL);
   open C_GET_PACK;
   SQL_LIB.SET_MARK('FETCH','C_GET_PACK', 'ORDSKU_HTS', NULL);
   fetch C_GET_PACK into L_pack_item;
   if C_GET_PACK%NOTFOUND then
      L_pack_item := NULL;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PACK', 'ORDSKU_HTS', NULL);
   close C_GET_PACK;

   if GET_HTS_DETAILS(O_error_message,
                      L_tariff_treatment,
                      L_qty_1,
                      L_qty_2,
                      L_qty_3,
                      L_units_1,
                      L_units_2,
                      L_units_3,
                      L_specific_rate,
                      L_av_rate,
                      L_other_rate,
                      L_cvd_case_no,
                      L_ad_case_no,
                      L_duty_comp_code,
                      I_item,
                      I_order_no,
                      I_hts,
                      I_import_country_id,
                      I_origin_country_id,
                      I_effect_from,
                      I_effect_to,
                      L_pack_item) = FALSE then
      return FALSE;
   end if;
   ---   
   -- Insert Assessments with the Always Default Indicator set to 'Y'.

   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
   insert into ordsku_hts_assess(order_no,
                                 seq_no,
                                 comp_id,
                                 cvb_code,
                                 comp_rate,
                                 per_count,
                                 per_count_uom,
                                 est_assess_value,
                                 nom_flag_1,
                                 nom_flag_2,
                                 nom_flag_3,
                                 nom_flag_4,
                                 nom_flag_5,
                                 display_order)
      select I_order_no,
             I_seq_no,
             elc.comp_id,
             elc.cvb_code,
             elc.comp_rate,
             elc.per_count,
             elc.per_count_uom,
             0,
             elc.nom_flag_1,
             elc.nom_flag_2,
             elc.nom_flag_3,
             elc.nom_flag_4,
             elc.nom_flag_5,
             elc.display_order 
        from elc_comp elc
       where elc.comp_type          = 'A'
         and elc.import_country_id  = I_import_country_id
         and elc.always_default_ind = 'Y'
         and not exists (select 'Y'
                           from ordsku_hts_assess it
                          where it.order_no = I_order_no
                            and it.seq_no   = I_seq_no
                            and it.comp_id  = elc.comp_id);
   ---
   -- Insert the Duty Assessments if it is configurable.

   if ITEM_HTS_SQL.COMPUTATION_CODE_CONFIGURABLE(O_error_message,
                                                 L_hts_exists,
                                                 I_import_country_id,
                                                 L_duty_comp_code) = FALSE then
      return FALSE;
   end if;

   if L_hts_exists then
      SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
      insert into ordsku_hts_assess (order_no,
                                     seq_no,
                                     comp_id,
                                     cvb_code,
                                     comp_rate,
                                     per_count,
                                     per_count_uom,
                                     est_assess_value,
                                     nom_flag_1,
                                     nom_flag_2,
                                     nom_flag_3,
                                     nom_flag_4,
                                     nom_flag_5,
                                     display_order)
                              select I_order_no,
                                     I_seq_no,
                                     elc.comp_id,
                                     elc.cvb_code,
                                     NVL(decode(hc.comp_rate_code, 'A', L_av_rate,
                                                                   'S', L_specific_rate,
                                                                   'O', L_other_rate),
                                                                   elc.comp_rate),  -- comp_rate
                                     decode(hc.comp_uom_code, NULL, NULL, elc.per_count),             -- per_count
                                     decode(hc.comp_uom_code, '1', NVL(L_units_1, elc.per_count_uom),
                                                              '2', NVL(L_units_2, elc.per_count_uom),
                                                              '3', NVL(L_units_3, elc.per_count_uom),
                                                              NULL),          -- per_count_uom
                                     0,
                                     elc.nom_flag_1,
                                     elc.nom_flag_2,
                                     elc.nom_flag_3,
                                     elc.nom_flag_4,
                                     elc.nom_flag_5,
                                     elc.display_order
                                from hts_computation hc, 
                                     elc_comp elc
                               where hc.computation_code   = L_duty_comp_code
                                 and hc.import_country_id   = I_import_country_id
                                 and elc.comp_id            = 'DTY' || hc.computation_code || hc.comp_seq || hc.import_country_id
                                 and elc.comp_type          = 'A'
                                 and elc.import_country_id  =  hc.import_country_id
                                 and not exists (select 'Y'
                                                   from ordsku_hts_assess it
                                                  where it.order_no = I_order_no
                                                    and it.seq_no   = I_seq_no
                                                    and it.comp_id  = elc.comp_id);
   else
 
      if L_duty_comp_code = '0' then
         L_comp_rate      := 0;
         L_per_count      := NULL;
         L_per_count_uom  := NULL;
      elsif L_duty_comp_code in ('1','3','4','6','C') then
         L_comp_rate     := L_specific_rate;
         L_per_count     := 1;
         L_per_count_uom := L_units_1;   
      elsif L_duty_comp_code in ('2','5','E') then
         L_comp_rate     := L_specific_rate;
         L_per_count     := 1;
         L_per_count_uom := L_units_2;   
      elsif L_duty_comp_code in ('7','9') then
         L_comp_rate     := L_av_rate;
         L_per_count     := NULL;
         L_per_count_uom := NULL;   
      elsif L_duty_comp_code = 'D' then
         L_comp_rate     := L_specific_rate;
         L_per_count     := 1;
         L_per_count_uom := L_units_3;
      end if;
      ---
      L_comp_id := 'DTY'||L_duty_comp_code||'A'||I_import_country_id;
   ---
   if L_duty_comp_code in ('0','1','2','3','4','5','6','7','9','C','D','E') then
      SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
      insert into ordsku_hts_assess (order_no,
                                     seq_no,
                                     comp_id,
                                     cvb_code,
                                     comp_rate,
                                     per_count,
                                     per_count_uom,
                                     est_assess_value,
                                     nom_flag_1,
                                     nom_flag_2,
                                     nom_flag_3,
                                     nom_flag_4,
                                     nom_flag_5,
                                     display_order)
         select I_order_no,
                I_seq_no,
                elc.comp_id,
                elc.cvb_code,
                NVL(L_comp_rate, elc.comp_rate),
                L_per_count,
                decode(L_duty_comp_code, '0', NULL, 
                                         '7', NULL, 
                                         '9', NULL, 
                                         NVL(L_per_count_uom, elc.per_count_uom)),
                0,
                elc.nom_flag_1,
                elc.nom_flag_2,
                elc.nom_flag_3,
                elc.nom_flag_4,
                elc.nom_flag_5,
                elc.display_order             
           from elc_comp elc
          where elc.comp_id = L_comp_id
            and elc.comp_type = 'A'
            and elc.import_country_id = I_import_country_id      
            and not exists (select 'Y'
                              from ordsku_hts_assess it
                             where it.order_no = I_order_no
                               and it.seq_no   = I_seq_no
                               and it.comp_id  = elc.comp_id);
   end if;
   ---
   if L_duty_comp_code in ('3','6') then
      L_comp_rate     := L_other_rate;
      L_per_count     := 1;
      L_per_count_uom := L_units_2;
   end if;
   ---
   if L_duty_comp_code in ('4','5','D') then
      L_comp_rate     := L_av_rate;
      L_per_count     := NULL;
      L_per_count_uom := NULL;
   end if;
   ---
   if L_duty_comp_code = 'E' then
      L_comp_rate     := L_other_rate;
      L_per_count     := 1;
      L_per_count_uom := L_units_3;
   end if;
   ---
   L_comp_id := 'DTY'||L_duty_comp_code||'B'||I_import_country_id;
   ---
   if L_duty_comp_code in ('3','4','5','6','D','E') then
      SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
      insert into ordsku_hts_assess (order_no,
                                     seq_no,
                                     comp_id,
                                     cvb_code,
                                     comp_rate,
                                     per_count,
                                     per_count_uom,
                                     est_assess_value,
                                     nom_flag_1,
                                     nom_flag_2,
                                     nom_flag_3,
                                     nom_flag_4,
                                     nom_flag_5,
                                     display_order)
         select I_order_no,
                I_seq_no,
                elc.comp_id,
                elc.cvb_code,
                NVL(L_comp_rate, elc.comp_rate),
                L_per_count,
                decode(L_duty_comp_code, '4', NULL, 
                                         '5', NULL, 
                                         'D', NULL, 
                                         NVL(L_per_count_uom, elc.per_count_uom)),
                0,
                elc.nom_flag_1,
                elc.nom_flag_2,
                elc.nom_flag_3,
                elc.nom_flag_4,
                elc.nom_flag_5,
                elc.display_order 
           from elc_comp elc
          where elc.comp_id = L_comp_id
            and elc.comp_type = 'A'
            and elc.import_country_id = I_import_country_id
            and not exists (select 'Y'
                              from ordsku_hts_assess it
                             where it.order_no = I_order_no
                               and it.seq_no   = I_seq_no
                               and it.comp_id  = elc.comp_id);
   end if;
   ---
   L_comp_id := 'DTY'||L_duty_comp_code||'C'||I_import_country_id;
   ---
   if L_duty_comp_code in ('6','E') then
      SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
      insert into ordsku_hts_assess (order_no,
                                     seq_no,
                                     comp_id,
                                     cvb_code,
                                     comp_rate,
                                     per_count,
                                     per_count_uom,
                                     est_assess_value,
                                     nom_flag_1,
                                     nom_flag_2,
                                     nom_flag_3,
                                     nom_flag_4,
                                     nom_flag_5,
                                     display_order)
         select I_order_no,
                I_seq_no,
                elc.comp_id,
                elc.cvb_code,
                NVL(L_av_rate, elc.comp_rate),
                NULL,
                NULL,
                0,
                elc.nom_flag_1,
                elc.nom_flag_2,
                elc.nom_flag_3,
                elc.nom_flag_4,
                elc.nom_flag_5,
                elc.display_order
           from elc_comp elc
          where elc.comp_id = L_comp_id
            and elc.comp_type = 'A'
            and elc.import_country_id = I_import_country_id
            and not exists(select 'Y'
                             from ordsku_hts_assess it
                            where it.order_no = I_order_no
                              and it.seq_no   = I_seq_no
                              and it.comp_id  = elc.comp_id);
   end if;
end if; 
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
   insert into ordsku_hts_assess (order_no,
                                  seq_no,
                                  comp_id,
                                  cvb_code,
                                  comp_rate,
                                  per_count,
                                  per_count_uom,
                                  est_assess_value,
                                  nom_flag_1,
                                  nom_flag_2,
                                  nom_flag_3,
                                  nom_flag_4,
                                  nom_flag_5,
                                  display_order)
      select I_order_no,
             I_seq_no,
             elc.comp_id,
             elc.cvb_code,
             100,
             NULL,
             NULL,
             0,
             elc.nom_flag_1,
             elc.nom_flag_2,
             elc.nom_flag_3,
             elc.nom_flag_4,
             elc.nom_flag_5,
             elc.display_order 
        from elc_comp elc
       where elc.comp_id = 'DUTY'||I_import_country_id
         and elc.comp_type = 'A'
         and elc.import_country_id = I_import_country_id
         and not exists(select 'Y'
                          from ordsku_hts_assess it
                         where it.order_no = I_order_no
                           and it.seq_no   = I_seq_no
                           and it.comp_id  = elc.comp_id);

   -- Insert the Tax Assessments if it is configurable.
   ---
   for T_rec in C_GET_TAX_INFO loop
      L_tax_type      := T_rec.tax_type;
      L_tax_comp_code := T_rec.tax_comp_code;
      L_specific_rate := T_rec.tax_specific_rate;
      L_av_rate       := T_rec.tax_av_rate;
      ---
      if ITEM_HTS_SQL.COMPUTATION_CODE_CONFIGURABLE(O_error_message,
                                                    L_hts_exists,
                                                    I_import_country_id,
                                                    L_tax_comp_code) = FALSE then



         return FALSE;
      end if;

      if L_hts_exists then
         SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
         insert into ordsku_hts_assess (order_no,
                                        seq_no,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        per_count,
                                        per_count_uom,
                                        est_assess_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order)
                                 select I_order_no,
                                        I_seq_no,
                                        elc.comp_id,
                                        elc.cvb_code,
                                        NVL(decode(hc.comp_rate_code, 'A', L_av_rate,
                                                                      'S', L_specific_rate),
                                                                      elc.comp_rate),  -- comp_rate
                                        decode(hc.comp_uom_code, NULL, NULL, elc.per_count),             -- per_count
                                        decode(hc.comp_uom_code, '1', NVL(L_units_1, elc.per_count_uom),
                                                                 '2', NVL(L_units_2, elc.per_count_uom),
                                                                 '3', NVL(L_units_3, elc.per_count_uom),
                                                                 NULL),          -- per_count_uom
                                        0,
                                        elc.nom_flag_1,
                                        elc.nom_flag_2,
                                        elc.nom_flag_3,
                                        elc.nom_flag_4,
                                        elc.nom_flag_5,
                                        elc.display_order
                                   from hts_computation hc, 
                                        elc_comp elc
                                  where hc.computation_code   = L_tax_comp_code
                                    and hc.import_country_id  = I_import_country_id
                                    and elc.comp_id           = L_tax_type || hc.computation_code || hc.comp_seq || hc.import_country_id
                                    and elc.comp_type         = 'A'
                                    and elc.import_country_id =  hc.import_country_id
                                    and not exists (select 'Y'
                                                      from ordsku_hts_assess it
                                                     where it.order_no = I_order_no
                                    and it.seq_no   = I_seq_no
                                    and it.comp_id  = elc.comp_id);

      else

         if L_tax_comp_code in ('1','4','C') then
            L_comp_rate     := L_specific_rate;
            L_per_count     := 1;
            L_per_count_uom := L_units_1;      
         elsif L_tax_comp_code in ('2','5') then
            L_comp_rate     := L_specific_rate;
            L_per_count     := 1;
            L_per_count_uom := L_units_2;      
         elsif L_tax_comp_code in ('7','9') then
            L_comp_rate     := L_av_rate;
            L_per_count     := NULL;
            L_per_count_uom := NULL;      
         elsif L_tax_comp_code = 'D' then
            L_comp_rate     := L_specific_rate;
            L_per_count     := 1;
            L_per_count_uom := L_units_3;
         end if;
      ---
      L_comp_id := L_tax_type||L_tax_comp_code||'A'||I_import_country_id;
      ---
      if L_tax_comp_code in ('1','2','4','5','7','9','C','D') then
         SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
         insert into ordsku_hts_assess (order_no,
                                        seq_no,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        per_count,
                                        per_count_uom,
                                        est_assess_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order)
            select I_order_no,
                   I_seq_no,
                   elc.comp_id,
                   elc.cvb_code,
                   NVL(L_comp_rate, elc.comp_rate),
                   L_per_count,
                   decode(L_tax_comp_code, '7', NULL, 
                                           '9', NULL, 
                                            NVL(L_per_count_uom, elc.per_count_uom)),
                   0,
                   elc.nom_flag_1,
                   elc.nom_flag_2,
                   elc.nom_flag_3,
                   elc.nom_flag_4,
                   elc.nom_flag_5,
                   elc.display_order 
              from elc_comp elc
             where elc.comp_id = L_comp_id
               and elc.comp_type = 'A'
               and elc.import_country_id = I_import_country_id
               and not exists(select 'Y'
                                from ordsku_hts_assess it
                               where it.order_no = I_order_no
                                 and it.seq_no   = I_seq_no
                                 and it.comp_id  = elc.comp_id);
      end if;
      ---
      L_comp_id := L_tax_type||L_tax_comp_code||'B'||I_import_country_id;
      ---
      if L_tax_comp_code in ('4','5','D') then
         SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
         insert into ordsku_hts_assess (order_no,
                                        seq_no,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        per_count,
                                        per_count_uom,
                                        est_assess_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order)
            select I_order_no,
                   I_seq_no,
                   elc.comp_id,
                   elc.cvb_code,
                   NVL(L_av_rate, elc.comp_rate),
                   NULL,
                   NULL,
                   0,
                   elc.nom_flag_1,
                   elc.nom_flag_2,
                   elc.nom_flag_3,
                   elc.nom_flag_4,
                   elc.nom_flag_5,
                   elc.display_order             
              from elc_comp elc
             where elc.comp_id = L_comp_id
               and elc.comp_type = 'A'
               and elc.import_country_id = I_import_country_id
               and not exists(select 'Y'
                                from ordsku_hts_assess it
                               where it.order_no = I_order_no
                                 and it.seq_no   = I_seq_no
                                 and it.comp_id  = elc.comp_id);
      end if;
      end if;
   end loop;
   ---
   -- Insert the Fee Assessments if it is configurable.
   ---
   for F_rec in C_GET_FEE_INFO loop
      L_fee_type      := F_rec.fee_type;
      L_fee_comp_code := F_rec.fee_comp_code;
      L_specific_rate := F_rec.fee_specific_rate;
      L_av_rate       := F_rec.fee_av_rate;
      ---
      if ITEM_HTS_SQL.COMPUTATION_CODE_CONFIGURABLE(O_error_message,
                                                    L_hts_exists,
                                                    I_import_country_id,
                                                    L_fee_comp_code) = FALSE then



         return FALSE;
      end if;

      if L_hts_exists then
         SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
         insert into ordsku_hts_assess (order_no,
                                        seq_no,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        per_count,
                                        per_count_uom,
                                        est_assess_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order)
                                 select I_order_no,
                                        I_seq_no,
                                        elc.comp_id,
                                        elc.cvb_code,
                                        NVL(decode(hc.comp_rate_code, 'A', L_av_rate,
                                                                      'S', L_specific_rate),
                                                                       elc.comp_rate),  -- comp_rate                
                                        decode(hc.comp_uom_code, NULL, NULL, elc.per_count),             -- per_count
                                        decode(hc.comp_uom_code, '1', NVL(L_units_1, elc.per_count_uom),
                                                                 '2', NVL(L_units_2, elc.per_count_uom),
                                                                 '3', NVL(L_units_3, elc.per_count_uom),
                                               NULL),          -- per_count_uom
                                        0,
                                        elc.nom_flag_1,
                                        elc.nom_flag_2,
                                        elc.nom_flag_3,
                                        elc.nom_flag_4,
                                        elc.nom_flag_5,
                                        elc.display_order
                                   from hts_computation hc, 
                                        elc_comp elc
                                  where hc.computation_code  = L_fee_comp_code
                                    and hc.import_country_id  = I_import_country_id
                                    and elc.comp_id           = L_fee_type || hc.computation_code || hc.comp_seq || hc.import_country_id
                                    and elc.comp_type         = 'A'
                                    and elc.import_country_id =  hc.import_country_id
                                    and not exists (select 'Y'
                                                      from ordsku_hts_assess it
                                                     where it.order_no = I_order_no
                                                       and it.seq_no   = I_seq_no
                                                       and it.comp_id  = elc.comp_id);

      else

         if L_fee_comp_code in ('1','4','C') then
            L_comp_rate     := L_specific_rate;
            L_per_count     := 1;
            L_per_count_uom := L_units_1;      
         elsif L_fee_comp_code in ('2','5') then
            L_comp_rate     := L_specific_rate;
            L_per_count     := 1;
            L_per_count_uom := L_units_2;     
         elsif L_fee_comp_code in ('7','9') then
            L_comp_rate     := L_av_rate;
            L_per_count     := NULL;
            L_per_count_uom := NULL;      
         elsif L_fee_comp_code = 'D' then
            L_comp_rate     := L_specific_rate;
            L_per_count     := 1;
            L_per_count_uom := L_units_3;
         end if;
      ---
      L_comp_id := L_fee_type||L_fee_comp_code||'A'||I_import_country_id;
      ---
      if L_fee_comp_code in ('1','2','4','5','7','9','C','D') then
         SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
         insert into ordsku_hts_assess (order_no,
                                        seq_no,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        per_count,
                                        per_count_uom,
                                        est_assess_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order)
            select I_order_no,
                   I_seq_no,
                   elc.comp_id,
                   elc.cvb_code,
                   NVL(L_comp_rate, elc.comp_rate),
                   L_per_count,
                   decode(L_fee_comp_code, '7', NULL, 
                                           '9', NULL, 
                                            NVL(L_per_count_uom, elc.per_count_uom)),
                   0,
                   elc.nom_flag_1,
                   elc.nom_flag_2,
                   elc.nom_flag_3,
                   elc.nom_flag_4,
                   elc.nom_flag_5,
                   elc.display_order 
              from elc_comp elc
             where elc.comp_id = L_comp_id
               and elc.comp_type = 'A'
               and elc.import_country_id = I_import_country_id
               and not exists(select 'Y'
                                from ordsku_hts_assess it
                               where it.order_no = I_order_no
                                 and it.seq_no   = I_seq_no
                                 and it.comp_id  = elc.comp_id);
      end if;
      ---
      L_comp_id := L_fee_type||L_fee_comp_code||'B'||I_import_country_id;
      ---
      if L_fee_comp_code in ('4','5','D') then
         SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
         insert into ordsku_hts_assess (order_no,
                                        seq_no,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        per_count,
                                        per_count_uom,
                                        est_assess_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order)
            select I_order_no,
                   I_seq_no,
                   elc.comp_id,
                   elc.cvb_code,
                   NVL(L_av_rate, elc.comp_rate),
                   NULL,
                   NULL,
                   0,
                   elc.nom_flag_1,
                   elc.nom_flag_2,
                   elc.nom_flag_3,
                   elc.nom_flag_4,
                   elc.nom_flag_5,
                   elc.display_order 
              from elc_comp elc
             where elc.comp_id = L_comp_id
               and elc.comp_type = 'A'
               and elc.import_country_id = I_import_country_id
               and not exists(select 'Y'
                                from ordsku_hts_assess it
                               where it.order_no = I_order_no
                                 and it.seq_no   = I_seq_no
                                 and it.comp_id  = elc.comp_id);
      end if;
      end if;
   end loop;
   ---
   -- Insert the CVD (Countervailing) Assessments 
   -- from the Estimated Landed Cost Components table.
   ---
   if L_cvd_case_no is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_CVD_RATE', 'HTS_CVD', NULL);
      open C_GET_CVD_RATE;
      SQL_LIB.SET_MARK('FETCH','C_GET_CVD_RATE', 'HTS_CVD', NULL);
      fetch C_GET_CVD_RATE into L_comp_rate;
      if C_GET_CVD_RATE%FOUND then
         SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
         insert into ordsku_hts_assess(order_no,
                                       seq_no,
                                       comp_id,
                                       cvb_code,
                                       comp_rate,
                                       per_count,
                                       per_count_uom,
                                       est_assess_value,
                                       nom_flag_1,
                                       nom_flag_2,
                                       nom_flag_3,
                                       nom_flag_4,
                                       nom_flag_5,
                                       display_order)
            select I_order_no,
                   I_seq_no,
                   elc.comp_id,
                   elc.cvb_code,
                   NVL(L_comp_rate, elc.comp_rate),
                   NULL,
                   NULL,
                   0,
                   elc.nom_flag_1,
                   elc.nom_flag_2,
                   elc.nom_flag_3,
                   elc.nom_flag_4,
                   elc.nom_flag_5,
                   elc.display_order 
              from elc_comp elc
             where elc.comp_id           = 'CVD'||I_import_country_id
               and elc.comp_type         = 'A'
               and elc.import_country_id = I_import_country_id
               and not exists(select 'Y'
                                from ordsku_hts_assess it
                               where it.order_no = I_order_no
                                 and it.seq_no   = I_seq_no
                                 and it.comp_id  = elc.comp_id);
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_CVD_RATE', 'HTS_CVD', NULL);
      close C_GET_CVD_RATE;
   end if; -- L_cvd_case_no is not NULL
   ---
   -- Insert the AD (Anti-Dumping) Assessments 
   -- from the Estimated Landed Cost Components table.
   ---
   if L_ad_case_no is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_AD_RATE', 'HTS_AD', NULL);
      open C_GET_AD_RATE;
      SQL_LIB.SET_MARK('FETCH','C_GET_AD_RATE', 'HTS_AD', NULL);
      fetch C_GET_AD_RATE into L_comp_rate;
      if C_GET_AD_RATE%FOUND then
         SQL_LIB.SET_MARK('INSERT', NULL,'ORDSKU_HTS_ASSESS', NULL);
         insert into ordsku_hts_assess(order_no,
                                       seq_no,
                                       comp_id,
                                       cvb_code,
                                       comp_rate,
                                       per_count,
                                       per_count_uom,
                                       est_assess_value,
                                       nom_flag_1,
                                       nom_flag_2,
                                       nom_flag_3,
                                       nom_flag_4,
                                       nom_flag_5,
                                       display_order)
            select I_order_no,
                   I_seq_no,
                   elc.comp_id,
                   elc.cvb_code,
                   NVL(L_comp_rate, elc.comp_rate),
                   NULL,
                   NULL,
                   0,
                   elc.nom_flag_1,
                   elc.nom_flag_2,
                   elc.nom_flag_3,
                   elc.nom_flag_4,
                   elc.nom_flag_5,
                   elc.display_order 
              from elc_comp elc
             where elc.comp_id           = 'AD'||I_import_country_id
               and elc.comp_type         = 'A'
               and elc.import_country_id = I_import_country_id
               and not exists(select 'Y'
                                from ordsku_hts_assess it
                               where it.order_no = I_order_no
                                 and it.seq_no   = I_seq_no
                                 and it.comp_id  = elc.comp_id);
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_AD_RATE', 'HTS_AD', NULL);
      close C_GET_AD_RATE;
   end if;  -- if L_ad_case_no is not NULL
   ---
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_ASSESS;
--------------------------------------------------------------------------------
FUNCTION DEFAULT_CALC_ASSESS(O_error_message         IN OUT VARCHAR2,
                             I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                             I_seq_no                IN     ORDSKU_HTS.SEQ_NO%TYPE,
                             I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                             I_hts                   IN     HTS.HTS%TYPE,
                             I_import_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_origin_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                             I_effect_to             IN     HTS.EFFECT_TO%TYPE)
   RETURN BOOLEAN IS
   
   L_program            VARCHAR2(65) := 'ORDER_HTS_SQL.DEFAULT_CALC_ASSESS';

BEGIN
   ---
   -- Default in all of the assessments for the HTS code passed into the function.
   ---
   if DEFAULT_ASSESS(O_error_message,
                     I_order_no,
                     I_seq_no,
                     I_item,
                     I_hts,
                     I_import_country_id,
                     I_origin_country_id,
                     I_effect_from,
                     I_effect_to) = FALSE then
      return FALSE;
   end if;
   ---
   -- Need to calculate all of the assessments.

   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PA',
                             I_item,
                             NULL,
                             NULL,
                             NULL,
                             I_order_no,
                             I_seq_no,
                             NULL,
                             NULL,
                             I_hts,
                             I_import_country_id,
                             I_origin_country_id,
                             I_effect_from,
                             I_effect_to) = FALSE then
      return FALSE;
   end if;

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_CALC_ASSESS;
--------------------------------------------------------------------------------
FUNCTION DEFAULT_ITEM_ASSESS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                             I_seq_no                IN     ORDSKU_HTS.SEQ_NO%TYPE,
                             I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                             I_hts                   IN     HTS.HTS%TYPE,
                             I_import_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_origin_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                             I_effect_to             IN     HTS.EFFECT_TO%TYPE)
   RETURN BOOLEAN IS
   
   L_program            VARCHAR2(65)             := 'ORDER_HTS_SQL.DEFAULT_ITEM_ASSESS';

BEGIN

   -- Default in all of the assessments for the HTS code from item passed into the function.
   
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDSKU_HTS_ASSESS', NULL);
   insert into ordsku_hts_assess(order_no,
                                 seq_no,
                                 comp_id,
                                 cvb_code,
                                 comp_rate,
                                 per_count,
                                 per_count_uom,
                                 est_assess_value,
                                 nom_flag_1,
                                 nom_flag_2,
                                 nom_flag_3,
                                 nom_flag_4,
                                 nom_flag_5,
                                 display_order)
                          select I_order_no,
                                 I_seq_no,
                                 it.comp_id,
                                 it.cvb_code,
                                 it.comp_rate,
                                 it.per_count,
                                 it.per_count_uom,
                                 it.est_assess_value,
                                 it.nom_flag_1,
                                 it.nom_flag_2,
                                 it.nom_flag_3,
                                 it.nom_flag_4,
                                 it.nom_flag_5,
                                 it.display_order
                            from item_hts_assess it, 
                                 ordsku_hts oh,
                                 elc_comp elc
                           where oh.order_no          = I_order_no
                             and it.item              = I_item
                             and it.import_country_id = I_import_country_id
                             and it.origin_country_id = I_origin_country_id
                             and it.hts               = I_hts
                             and it.effect_from       = I_effect_from
                             and it.effect_to         = I_effect_to
                             and it.comp_id           = elc.comp_id
                             and oh.item              = it.item                             
                             and oh.hts               = it.hts
                             and oh.effect_from       = it.effect_from
                             and oh.effect_to         = it.effect_to
                             and oh.import_country_id = it.import_country_id
                             and oh.origin_country_id = it.origin_country_id                     
                             and not exists(select 'Y'
                                              from ordsku_hts_assess oha
                                             where oha.order_no = I_order_no
                                               and oha.seq_no   = I_seq_no
                                               and oha.comp_id  = it.comp_id);
   ---
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PA',
                             I_item,
                             NULL,
                             NULL,
                             NULL,
                             I_order_no,
                             I_seq_no,
                             NULL,
                             NULL,
                             I_hts,
                             I_import_country_id,
                             I_origin_country_id,
                             I_effect_from,
                             I_effect_to) = FALSE then
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_ITEM_ASSESS;
-----------------------------------------------------------------------------------
FUNCTION DEFAULT_HTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                     I_item              IN     ITEM_MASTER.ITEM%TYPE,
                     I_component_item    IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS
   
   L_program            VARCHAR2(65)             := 'ORDER_HTS_SQL.DEFAULT_HTS';
   L_item               ITEM_MASTER.ITEM%TYPE;
   L_packsku            ITEM_MASTER.ITEM%TYPE;
   L_buyer_pack         VARCHAR2(1)              := 'N';

   cursor C_BUYER_PACK is
      select 'Y' 
        from item_master
       where item      = L_item
         and pack_ind  = 'Y'
         and pack_type = 'B';

   cursor C_PACKSKU is
      select item
        from v_packsku_qty
       where pack_no = L_item;

   cursor C_GET_SKUS is
      select item, 
             origin_country_id
        from ordsku
       where order_no = I_order_no;
BEGIN
   if I_item is not NULL then
      L_item := I_item;
      ---
      SQL_LIB.SET_MARK('OPEN','C_BUYER_PACK','PACKHEAD','Pack no: '||L_item);
      open C_BUYER_PACK;
      SQL_LIB.SET_MARK('FETCH','C_BUYER_PACK','PACKHEAD','Pack no: '||L_item);
      fetch C_BUYER_PACK into L_buyer_pack;
      SQL_LIB.SET_MARK('CLOSE','C_BUYER_PACK','PACKHEAD','Pack no: '||L_item);
      close C_BUYER_PACK;
      ---
      if L_buyer_pack = 'N' then
         if ORDER_HTS_SQL.INSERT_HTS_ASSESS(O_error_message,
                                            I_order_no,
                                            L_item,
                                            NULL) = FALSE then
            return FALSE;
         end if;   
      else  -- L_buyer_pack = 'Y'
         if I_component_item is not NULL then
            if ORDER_HTS_SQL.INSERT_HTS_ASSESS(O_error_message,
                                               I_order_no,
                                               I_component_item,
                                               L_item) = FALSE then
               return FALSE;
            end if;
         else
            for C_rec in C_PACKSKU loop
               L_packsku := C_rec.item;
               ---
               if ORDER_HTS_SQL.INSERT_HTS_ASSESS(O_error_message,
                                                  I_order_no,
                                                  L_packsku,
                                                  L_item) = FALSE then
                  return FALSE;
               end if;
            end loop;
         end if;  ---I_component_item
      end if;   ---L_buyer_pack = 'N'
   else -- I_item is NULL
      for C_rec in C_GET_SKUS loop
         L_item       := C_rec.item;
         L_buyer_pack := 'N';
         ---
         SQL_LIB.SET_MARK('OPEN','C_BUYER_PACK','PACKHEAD','Pack no: '||L_item);
         open C_BUYER_PACK;
         SQL_LIB.SET_MARK('FETCH','C_BUYER_PACK','PACKHEAD','Pack no: '||L_item);
         fetch C_BUYER_PACK into L_buyer_pack;
         SQL_LIB.SET_MARK('CLOSE','C_BUYER_PACK','PACKHEAD','Pack no: '||L_item);
         close C_BUYER_PACK;
         ---
         if L_buyer_pack = 'N' then
            if ORDER_HTS_SQL.INSERT_HTS_ASSESS(O_error_message,
                                               I_order_no,
                                               L_item,
                                               NULL) = FALSE then
               return FALSE;
            end if;      
         else  -- L_buyer_pack = 'Y'
            for C_rec in C_PACKSKU loop
               L_packsku := C_rec.ITEM;
               ---
               if ORDER_HTS_SQL.INSERT_HTS_ASSESS(O_error_message,
                                                  I_order_no,
                                                  L_packsku,
                                                  L_item) = FALSE then
                  return FALSE;
               end if;
            end loop;
         end if;  -- L_buyer_pack
      end loop;
   end if;  -- I_item is NULL/not NULL
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_HTS;
-----------------------------------------------------------------------------------------
FUNCTION DEFAULT_CALC_HTS(O_error_message     IN OUT VARCHAR2,
                          I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_component_item    IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS
   
    cursor C_BUYER_PACK is 
       select 'Y'  
         from item_master 
        where item      = I_item 
          and pack_ind  = 'Y' 
          and pack_type = 'B'; 
       
    L_program  VARCHAR2(65) := 'ORDER_HTS_SQL.DEFAULT_CALC_HTS'; 
    L_buyer_pack VARCHAR2(1):= 'N'; 
    L_item     ITEM_MASTER.ITEM%TYPE:= NULL; 
    L_packitem ITEM_MASTER.ITEM%TYPE:= NULL;

BEGIN
   ---
   -- Default the HTS codes for the Order/Item passed into the function.
   ---
   if DEFAULT_HTS(O_error_message,
                  I_order_no,
                  I_item,
                  I_component_item) = FALSE then
      return FALSE;
   end if;
   ---
   -- Need to calculate all of the assessments.
   SQL_LIB.SET_MARK('OPEN','C_BUYER_PACK','ITEM_MASTER','Pack no: '||I_item); 
   open C_BUYER_PACK; 
   SQL_LIB.SET_MARK('FETCH','C_BUYER_PACK','ITEM_MASTER','Pack no: '||I_item); 
   fetch C_BUYER_PACK into L_buyer_pack; 
   SQL_LIB.SET_MARK('CLOSE','C_BUYER_PACK','ITEM_MASTER','Pack no: '||I_item); 
   close C_BUYER_PACK; 

   if L_buyer_pack = 'Y' then   
    L_packitem := I_item;  
    L_item:= NULL; 
   else  
    L_packitem := NULL;  
    L_item:= I_item; 
   end if; 
   --    

   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PA',
                             L_item,
                             NULL,
                             NULL,
                             NULL,
                             I_order_no,
                             NULL,
                             L_packitem,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_CALC_HTS;
--------------------------------------------------------------------------------------
FUNCTION GET_UOM_RATE_VALUE(O_error_message    IN OUT  VARCHAR2,
                            O_exists           IN OUT  BOOLEAN,
                            O_per_count_uom    IN OUT  UOM_CLASS.UOM%TYPE,
                            O_comp_rate        IN OUT  ELC_COMP.COMP_RATE%TYPE,
                            O_est_assess_value IN OUT  ORDSKU_HTS_ASSESS.EST_ASSESS_VALUE%TYPE,
                            I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                            I_item             IN      ITEM_MASTER.ITEM%TYPE,
                            I_pack_item        IN      ITEM_MASTER.ITEM%TYPE,
                            I_hts              IN      HTS.HTS%TYPE,
                            I_comp_id          IN      ELC_COMP.COMP_ID%TYPE,
                            I_currency_code    IN      CURRENCIES.CURRENCY_CODE%TYPE,
                            I_exchange_rate    IN      CURRENCY_RATES.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(50)                     := 'ORDER_HTS_SQL.GET_UOM_RATE_VALUE';
   L_currency_code CURRENCIES.CURRENCY_CODE%TYPE    := NULL;

   cursor C_GET_UOM_RATE_VALUE is
      select oa.per_count_uom,
             oa.comp_rate / NVL(oa.per_count,1),
             oa.est_assess_value
        from ordsku_hts_assess oa,
             ordsku_hts o
       where o.order_no   = I_order_no
         and o.item       = I_item
         and (o.pack_item = I_pack_item
          or (o.pack_item is NULL and I_pack_item is NULL))
         and o.hts        = I_hts
         and o.order_no   = oa.order_no
         and o.seq_no     = oa.seq_no
         and oa.comp_id   = I_comp_id;

   cursor C_GET_CURRENCY is
      select comp_currency
        from elc_comp
       where comp_id = I_comp_id;

BEGIN
   O_exists := TRUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_UOM_RATE_VALUE','ORDSKU_HTS_ASSESS',NULL);
   open C_GET_UOM_RATE_VALUE;
   SQL_LIB.SET_MARK('FETCH','C_GET_UOM_RATE_VALUE','ORDSKU_HTS_ASSESS',NULL);
   fetch C_GET_UOM_RATE_VALUE into O_per_count_uom,
                                   O_comp_rate,
                                   O_est_assess_value;
   if C_GET_UOM_RATE_VALUE%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_GET_UOM_RATE_VALUE','ORDSKU_HTS_ASSESS',NULL);
      close C_GET_UOM_RATE_VALUE;
      O_exists := FALSE;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_UOM_RATE_VALUE','ORDSKU_HTS_ASSESS',NULL);
   close C_GET_UOM_RATE_VALUE;
   ---
   if I_currency_code is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_CURRENCY','ELC_COMP','Comp ID: ' || I_comp_id);
      open C_GET_CURRENCY;
      SQL_LIB.SET_MARK('FETCH','C_GET_CURRENCY','ELC_COMP','Comp ID: ' || I_comp_id);
      fetch C_GET_CURRENCY into L_currency_code;
      ---   
      if C_GET_CURRENCY%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_CURRENCY','ELC_COMP','Comp ID: ' || I_comp_id);
         close C_GET_CURRENCY;
         O_error_message := SQL_LIB.CREATE_MSG('COMP_NOT_EXIST',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_CURRENCY','ELC_COMP','Comp ID: ' || I_comp_id);
      close C_GET_CURRENCY;
      ---
      if CURRENCY_SQL.CONVERT(O_error_message,
                              O_comp_rate,
                              L_currency_code,
                              I_currency_code,
                              O_comp_rate,
                              'C',
                              NULL,
                              NULL,
                              NULL,
                              I_exchange_rate) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.CONVERT(O_error_message,
                              O_est_assess_value,
                              L_currency_code,
                              I_currency_code,
                              O_est_assess_value,
                              'C',
                              NULL,
                              NULL,
                              NULL,
                              I_exchange_rate) = FALSE then
         return FALSE;
      end if;               
   end if;
   ---                                                        
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_UOM_RATE_VALUE;
--------------------------------------------------------------------------------------
FUNCTION CHECK_ORDLOC_WKSHT(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT VARCHAR2,
                            I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   
   L_program       VARCHAR2(50)   := 'ORDER_HTS_SQL.CHECK_EXISTS';
   L_check_exist      VARCHAR2(1) := NULL;
  
   cursor C_CHECK_ORDLOC_WKSHT is
      select 'X'
        from ordloc_wksht olw,
             ordhead oh
       where olw.order_no = I_order_no
         and olw.origin_country_id != oh.import_country_id
         and olw.order_no = oh.order_no;     

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_ORDLOC_WKSHT',
                    'ORDLOC_WKSHT',
                    NULL);
   open C_CHECK_ORDLOC_WKSHT;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_ORDLOC_WKSHT',
                    'ORDLOC_WKSHT',
                    NULL);                    
   fetch C_CHECK_ORDLOC_WKSHT into L_check_exist;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_ORDLOC_WKSHT',
                    'ORDLOC_WKSHT',
                    NULL);
   close C_CHECK_ORDLOC_WKSHT; 
   
   O_exists := L_check_exist;
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END CHECK_ORDLOC_WKSHT;      
--------------------------------------------------------------------------------------    
FUNCTION CHECK_HTS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists          IN OUT VARCHAR2,
                   I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   
   L_program       VARCHAR2(50)   := 'ORDER_HTS_SQL.CHECK_EXISTS';
   L_check_exist      VARCHAR2(1) := NULL;
  
   cursor C_CHECK_HTS is
      select 'X'
        from v_ORDSKU vo,
             ordhead oh
       where vo.order_no = I_order_no
         and vo.order_no = oh.order_no
         and oh.import_country_id != vo.origin_country_id;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_HTS',
                    'V_ORDSKU',
                    NULL);
   open C_CHECK_HTS;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_HTS',
                    'V_ORDSKU',
                    NULL);                    
   fetch C_CHECK_HTS into L_check_exist;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_HTS',
                    'V_ORDSKU',
                    NULL);
   close C_CHECK_HTS; 
   
   O_exists := L_check_exist;
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END CHECK_HTS;      
--------------------------------------------------------------------------------------
FUNCTION DEFAULT_COM_HTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack              IN     ITEM_MASTER.ITEM%TYPE,
                         I_com               IN     COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)   := 'ORDER_HTS_SQL.DEFAULT_COM_HTS';
   L_buyer_pack     VARCHAR2(1)              := 'N';
   TYPE T_rowid_tab is TABLE of ROWID;
   L_rowid_tab      T_rowid_tab;
   L_rowid_tab1     T_rowid_tab;

   cursor C_LOCK_ORDSKU_HTS_ASSESS is
      select rowid
        from ordsku_hts_assess osa
       where osa.order_no = I_order_no
         and exists (select 1
                       from ordsku_hts osh
                      where osh.order_no = osa.order_no
                        and osh.item = I_item
                        and (osh.pack_item = I_pack
                             or (osh.pack_item is NULL and I_pack is NULL))
                        and osh.seq_no = osa.seq_no)
         for update nowait;

   cursor C_LOCK_ORDSKU_HTS is
      select rowid
        from ordsku_hts
       where order_no = I_order_no
         and item = I_item
         and (pack_item = I_pack
              or (pack_item is NULL and I_pack is NULL))
         for update nowait;

BEGIN
---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDSKU_HTS_ASSESS','ORDSKU_HTS_ASSESS',
                    'Order number: ' || to_char(I_order_no) ||  
                    ', Item: ' || I_item);
   open C_LOCK_ORDSKU_HTS_ASSESS;

   SQL_LIB.SET_MARK('FETCH','C_LOCK_ORDSKU_HTS_ASSESS','ORDSKU_HTS_ASSESS',
                    'Order number: ' || to_char(I_order_no) ||  
                    ', Item: ' || I_item);
   fetch C_LOCK_ORDSKU_HTS_ASSESS
         BULK COLLECT INTO L_rowid_tab;

   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDSKU_HTS_ASSESS','ORDSKU_HTS_ASSESS',
                    'Order number: ' || to_char(I_order_no) ||  
                    ', Item: ' || I_item);
   close C_LOCK_ORDSKU_HTS_ASSESS;

   FORALL i in L_rowid_tab.FIRST..L_rowid_tab.LAST
      delete from ordsku_hts_assess where rowid = L_rowid_tab(i);
---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDSKU_HTS','ORDSKU_HTS',
                    'Order number: ' || to_char(I_order_no) ||  
                    ', Item: ' || I_item);
   open C_LOCK_ORDSKU_HTS;

   SQL_LIB.SET_MARK('FETCH','C_LOCK_ORDSKU_HTS','ORDSKU_HTS',
                    'Order number: ' || to_char(I_order_no) ||  
                    ', Item: ' || I_item);
   fetch C_LOCK_ORDSKU_HTS
         BULK COLLECT INTO L_rowid_tab1;

   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDSKU_HTS','ORDSKU_HTS',
                    'Order number: ' || to_char(I_order_no) ||  
                    ', Item: ' || I_item);
   close C_LOCK_ORDSKU_HTS;

   FORALL i in L_rowid_tab1.FIRST..L_rowid_tab1.LAST
      delete from ordsku_hts where rowid = L_rowid_tab1(i);
---
   if ORDER_HTS_SQL.INSERT_HTS_ASSESS(O_error_message,
                                      I_order_no,
                                      I_item,
                                      I_pack,
                                      I_com) = FALSE then
      return FALSE;
   end if;   

   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PA',
                             I_item,
                             NULL,
                             NULL,
                             NULL,
                             I_order_no,
                             NULL,
                             I_pack,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_COM_HTS;
--------------------------------------------------------------------------------------
END ORDER_HTS_SQL;
/
