



CREATE OR REPLACE PACKAGE ORDER_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
   -- PACKAGE RECORD TYPES
----------------------------------------------------------------------------
-- for bulk inserting into ORDLOC 
TYPE ordloc_rec IS RECORD (order_no	            ORDHEAD.ORDER_NO%TYPE,
                           items	            ITEM_TBL,
                           locations                LOC_TBL,
                           loc_types                LOC_TYPE_TBL,
                           unit_retails             UNIT_RETAIL_TBL,                            
                           ordered_qtys             QTY_TBL,                        
                           prescaled_qtys           QTY_TBL,    -- set to order qty                      
                           cancelled_qtys           QTY_TBL,                          
                           cancel_codes             INDICATOR_TBL,  -- 'B'                             
                           cancel_dates             DATE_TBL,       -- sysdate                 
                           cancel_ids               USERID_TBL, -- 'RIBUSER'                              
                           unit_costs               UNIT_COST_TBL,                              
                           cost_sources             CODE_TBL,    -- 'NORM'                
                           non_scale_inds           INDICATOR_TBL,    -- 'Y'
                           estimated_instock_date   DATE_TBL);

-- for bulk inserting into ORDSKU 
TYPE ordsku_rec IS RECORD (order_no             ORDHEAD.ORDER_NO%TYPE,
                           items                ITEM_TBL,                            
                           ref_items            ITEM_TBL,                       
                           origin_country_ids   COUNTRY_TBL,               
                           earliest_ship_dates  DATE_TBL,             
                           latest_ship_dates    DATE_TBL,               
                           supp_pack_sizes      QTY_TBL,                 
                           non_scale_inds       INDICATOR_TBL); -- 'Y'                          

TYPE order_rec IS RECORD(ordhead_row   ORDHEAD%ROWTYPE,
                         ordskus       ORDSKU_REC,
                         ordlocs       ORDLOC_REC,
                         supplier_upd  VARCHAR2(1) default 'N',
                         status_upd    VARCHAR2(1) default 'N',
                         otb_eow_upd   VARCHAR2(1) default 'N',
                         new_locs      LOC_TBL,
                         new_loc_types LOC_TYPE_TBL);

----------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
----------------------------------------------------------------------------
-- Function    : INSERT_ORDER
-- Purpose     : This public function will insert records to ORDHEAD, ORDSKU, 
--               ORDLOC tables.
----------------------------------------------------------------------------
FUNCTION INSERT_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_rec       IN       ORDER_SQL.ORDER_REC)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function    : INSERT_ORDSKUS
-- Purpose     : This private function will bulk insert ORDSKU records.
----------------------------------------------------------------------------
FUNCTION INSERT_ORDSKUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordskus         IN       ORDER_SQL.ORDSKU_REC)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function    : INSERT_ORDLOCS
-- Purpose     : This private function will bulk insert ORDLOC records.
----------------------------------------------------------------------------
FUNCTION INSERT_ORDLOCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordlocs         IN       ORDER_SQL.ORDLOC_REC)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function    : UPDATE_ORDHEAD
-- Purpose     : This public function will update an ordhead record.
----------------------------------------------------------------------------
FUNCTION UPDATE_ORDHEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordhead_row     IN       ORDHEAD%ROWTYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function    : UPDATE_ORDSKUS
-- Purpose     : This public function will bulk update ordsku records.
----------------------------------------------------------------------------
FUNCTION UPDATE_ORDSKUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordskus         IN       ORDER_SQL.ORDSKU_REC)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function    : UPDATE_ORDLOCS
-- Purpose     : This public function will bulk update ordloc records.
----------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordlocs         IN       ORDER_SQL.ORDLOC_REC)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function    : DELETE_ORDER
-- Purpose     : This public function will delete ordhead, ordsku, ordloc
--               records associated with an order.
----------------------------------------------------------------------------
FUNCTION DELETE_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function    : DELETE_ORDSKUS
-- Purpose     : This public function will bulk delete ordsku records.
----------------------------------------------------------------------------
FUNCTION DELETE_ORDSKUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordskus         IN       ORDER_SQL.ORDSKU_REC)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function    : DELETE_ORDLOCS
-- Purpose     : This public function will bulk delete ordloc records.
----------------------------------------------------------------------------
FUNCTION DELETE_ORDLOCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_ordlocs         IN       ORDER_SQL.ORDLOC_REC)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function: GET_DEFAULT_IMPORT_ID
-- Purpose : This function will return importer or exporter and 
--           it's type associated for the given import order.
---------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_IMP_EXP (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_import_id       IN OUT   ORDHEAD.IMPORT_ID%TYPE,
                              O_import_type     IN OUT   ORDHEAD.IMPORT_TYPE%TYPE,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE) RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function: GET_DEFAULT_ROUT_LOC
-- Purpose : This function will return routing location associated for the 
--           given import order.
-----------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_ROUT_LOC (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_routing_loc_id   IN OUT   ORDHEAD.ROUTING_LOC_ID%TYPE,
                               I_order_no         IN       ORDHEAD.ORDER_NO%TYPE) RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function: DEL_ORDHEAD_ATTRIB
-- Purpose : This function will delete the utilization code for a
--           given order.
-----------------------------------------------------------------------------------------
FUNCTION DEL_ORDHEAD_ATTRIB(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no         IN       ORDHEAD_L10N_EXT.ORDER_NO%TYPE) RETURN BOOLEAN; 
-----------------------------------------------------------------------------------------

END ORDER_SQL;
/
