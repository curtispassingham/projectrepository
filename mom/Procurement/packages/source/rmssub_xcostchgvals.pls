
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XCOSTCHG_VALIDATE AUTHID CURRENT_USER AS


-----------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_costchg_rec     OUT    NOCOPY   RMSSUB_XCOSTCHG.COST_CHANGE_RECTYPE,
                       I_message         IN              "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION BUILD_PACK_PRICE_RECORDS(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_price_hist_rec      OUT    NOCOPY   RMSSUB_XCOSTCHG.PRICE_HIST_RECTYPE,
                                  O_isc_rows            OUT    NOCOPY   RMSSUB_XCOSTCHG.ROWID_TBL,
                                  I_packs               IN              ITEM_TBL,
                                  I_hier_locs           IN              LOC_TBL,
                                  I_hier_level          IN              VARCHAR2,
                                  I_supplier            IN              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                  I_origin_country_id   IN              ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                  I_unit_cost           IN              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                  I_currency_code       IN              STORE.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
END RMSSUB_XCOSTCHG_VALIDATE;
/
