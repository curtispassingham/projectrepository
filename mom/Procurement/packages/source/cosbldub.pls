CREATE OR REPLACE PACKAGE BODY COST_BUILDUP_SQL AS

TYPE CLASS_TBL      is TABLE of WF_COST_RELATIONSHIP.CLASS%TYPE;
TYPE SUBCLASS_TBL   is TABLE of WF_COST_RELATIONSHIP.SUBCLASS%TYPE;
TYPE TEMPL_ID_TBL   is TABLE of WF_COST_RELATIONSHIP.TEMPL_ID%TYPE;

LP_dept_tbl              DEPT_TBL       := DEPT_TBL();
LP_class_tbl             CLASS_TBL      := CLASS_TBL();
LP_subclass_tbl          SUBCLASS_TBL   := SUBCLASS_TBL();
LP_loc_tbl               LOC_TBL        := LOC_TBL();
LP_start_date_tbl        DATE_TBL       := DATE_TBL();
LP_end_date_tbl          DATE_TBL       := DATE_TBL();
LP_templ_id_tbl          TEMPL_ID_TBL   := TEMPL_ID_TBL();
LP_cstrel_count          INTEGER;
LP_item_tbl              ITEM_TBL       := ITEM_TBL();

-------------------------------------------------------------------------------------------------------
-- Function Name  :  GET_NEXT_TEMPLATE
-- Purpose        :  This new function will be called from the costbdutpl.fmb form to fetch the
--                   next available template id.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_TEMPLATE(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                           O_templ_id        IN OUT    WF_COST_BUILDUP_TMPL_HEAD.TEMPL_ID%TYPE)
return BOOLEAN IS

   L_program   VARCHAR2(62)   := 'COST_BUILDUP_SQL.GET_NEXT_TEMPLATE';

   cursor C_GET_NEXT_TEMPL_ID is
      select WF_TEMPL_ID_SEQ.NEXTVAL
        from sys.dual;

BEGIN
   open C_GET_NEXT_TEMPL_ID;
   fetch C_GET_NEXT_TEMPL_ID into O_templ_id;
   close C_GET_NEXT_TEMPL_ID;

   return TRUE;

EXCEPTION
    WHEN OTHERS THEN
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;
END GET_NEXT_TEMPLATE;
-------------------------------------------------------------------------------------------------------
-- Function Name  : COST_RELATION_OVERLAP_CHECK
-- Purpose        : This function will take the given dept, class(optional), subclass(optional),
--                  organizational group type and group value, start and end dates and check to see if
--                  any records exist that overlap.  If an overlap is found an overlap exists
--                  O_overlap_exists should be returned TRUE.
-------------------------------------------------------------------------------------------------------
FUNCTION COST_RELATION_OVERLAP_CHECK(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_overlap_exists  IN OUT    BOOLEAN,
                                     I_dept            IN        WF_COST_RELATIONSHIP.DEPT%TYPE,
                                     I_class           IN        WF_COST_RELATIONSHIP.CLASS%TYPE,
                                     I_subclass        IN        WF_COST_RELATIONSHIP.SUBCLASS%TYPE,
                                     I_group_type      IN        CODE_DETAIL.CODE%TYPE,
                                     I_group_value     IN        VARCHAR2,
                                     I_start_date      IN        WF_COST_RELATIONSHIP.START_DATE%TYPE,
                                     I_end_date        IN        WF_COST_RELATIONSHIP.END_DATE%TYPE,
                                     I_rowid           IN        VARCHAR2,
                                     I_item            IN        WF_COST_RELATIONSHIP.ITEM%TYPE)
return BOOLEAN IS

   L_program   VARCHAR2(64) := 'COST_BUILDUP_SQL.COST_RELATION_OVERLAP_CHECK';
   L_exists    VARCHAR2(1)  := NULL;

   cursor C_OVERLAP_EXISTS is
      select 'x'
        from wf_cost_relationship w,
             (select location store
                from loc_list_detail
               where loc_list = I_group_value
                 and loc_type = 'S'
                 and I_group_type = 'L'
               UNION ALL
              select store
                from v_store
               where area = I_group_value
                 and I_group_type = 'A'
               UNION ALL
              select store
                from v_store
               where region = I_group_value
                 and I_group_type = 'R'
               UNION ALL
              select store
                from v_store
               where district = I_group_value
                 and I_group_type = 'D'
               UNION ALL
              select location store
                from cost_zone_group_loc
               where zone_group_id = I_group_value
                 and loc_type = 'S'
                 and I_group_type = 'C'
               UNION ALL
              select store
                from v_store
               where store = I_group_value
                 and I_group_type = 'S'
               UNION ALL
              select store
                from v_store
               where I_group_type = 'AS') s
       where s.store      = w.location
         and w.dept       = I_dept
         and w.class      = NVL(I_class, -1)
         and w.subclass   = NVL(I_subclass, -1)
         and ((w.start_date <= I_start_date and
               w.end_date   >= I_start_date) or
              (w.start_date <= I_end_date and
               w.end_date   >= I_end_date)or
              (w.start_date > I_start_date and
               w.end_date < I_end_date))
         and ((I_rowid is NULL) or
               (I_rowid IS NOT NULL and
                w.rowid <> chartorowid(I_rowid)))
         and NVL(w.item,'-1') = NVL(I_item,'-1')
         and rownum = 1;

BEGIN

   -- check required parameters
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_class is NULL and
      I_subclass is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_class',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_group_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_group_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_group_type != 'AS' and
      I_group_value is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_group_value',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_start_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_start_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_end_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_end_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   open C_OVERLAP_EXISTS;
   fetch C_OVERLAP_EXISTS into L_exists;
   close C_OVERLAP_EXISTS;

   if L_exists is NULL then
      O_overlap_exists := FALSE;
   else
      O_overlap_exists := TRUE;
   end if;

return TRUE;

EXCEPTION
    WHEN OTHERS THEN
       if C_OVERLAP_EXISTS%ISOPEN then
          close C_OVERLAP_EXISTS;
       end if;
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
        return FALSE;
END COST_RELATION_OVERLAP_CHECK;
-------------------------------------------------------------------------------------------------------
-- Function Name  : COST_RELATION_EXPLODE
-- Purpose        : This function will take the given dept, class(optional), subclass(optional),
--                  organizational group type and group values and overwrite indicator and explode that
--                  information down to the subclass/wholesale location relationship level.
--                  The overwrite indicator will indicate whether to overwrite any overlaps or to explode
--                  all the values but don't overwrite the overlaps (if the overwrite indicator is NULL
--                  then no overlaps were found).
-------------------------------------------------------------------------------------------------------
FUNCTION COST_RELATION_EXPLODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_overwrite_ind   IN       VARCHAR2,
                               I_dept            IN       WF_COST_RELATIONSHIP.DEPT%TYPE,
                               I_class           IN       WF_COST_RELATIONSHIP.CLASS%TYPE,
                               I_subclass        IN       WF_COST_RELATIONSHIP.SUBCLASS%TYPE,
                               I_group_type      IN       CODE_DETAIL.CODE%TYPE,
                               I_group_value     IN       VARCHAR2,
                               I_start_date      IN       WF_COST_RELATIONSHIP.START_DATE%TYPE,
                               I_end_date        IN       WF_COST_RELATIONSHIP.END_DATE%TYPE,
                               I_templ_id        IN       WF_COST_RELATIONSHIP.TEMPL_ID%TYPE,
                               I_rowid           IN       VARCHAR2,
                               I_item            IN       WF_COST_RELATIONSHIP.ITEM%TYPE)
return BOOLEAN IS

   L_program        VARCHAR2(64) := 'COST_BUILDUP_SQL.COST_RELATION_EXPLODE';

   cursor C_GET_CST_REL is
      select o.dept,
             o.class,
             o.subclass,
             s.store location,
             I_start_date start_date,
             I_end_date end_date,
             I_templ_id templ_id,
             o.item item
        from (select l.location store,
                     v.store_type
                from loc_list_detail l,
                     v_store v
               where l.location = v.store
                 and l.loc_list = I_group_value
                 and l.loc_type = 'S'
                 and v.store_type = 'F'
                 and I_group_type = 'L'
               UNION ALL
              select store,
                     store_type
                from v_store
               where area = I_group_value
                 and store_type = 'F'
                 and I_group_type = 'A'
               UNION ALL
              select store,
                     store_type
                from v_store
               where region = I_group_value
                 and store_type = 'F'
                 and I_group_type = 'R'
               UNION ALL
              select store,
                     store_type
                from v_store
               where district = I_group_value
                 and store_type =  'F'
                 and I_group_type = 'D'
               UNION ALL
              select c.location store,
                     v.store_type
                from cost_zone_group_loc c,
                     v_store v
               where c.location = v.store
                 and c.zone_group_id = I_group_value
                 and c.loc_type = 'S'
                 and v.store_type = 'F'
                 and I_group_type = 'C'
               UNION ALL
              select store,
                     store_type
                from v_store
               where store = I_group_value
                 and store_type = 'F'
                 and I_group_type = 'S'
               UNION ALL
              select store,
                     store_type
                from v_store
               where store_type = 'F'
                 and I_group_type = 'AS') s,
              (select dept,
                      null as class,
                      null as subclass,
                      null as item
                 from v_deps
                where I_item is null
                  and dept     = I_dept
                  and I_class is null
                  and I_subclass is null
               union all
               select dept,
                      class,
                      null as subclass,
                      null as item
                 from v_class
                where I_item is null
                  and dept     = I_dept
                  and class    = I_class
                  and I_class is not null
                  and I_subclass is null
               union all
               select dept,
                      class,
                      subclass,
                      null as item
                 from v_subclass
                where I_item is null
                  and dept     = I_dept
                  and class    = I_class
                  and subclass = I_subclass
                  and I_class is not null
                  and I_subclass is not null
               union all
               select dept,
                      class,
                      subclass,
                      item
                 from v_item_master
                where item = I_item
                  and I_item is not null) o;

BEGIN
   -- check required parameters
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_class is NULL and
      I_subclass is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_class',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_group_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_group_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_group_type != 'AS' and
      I_group_value is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_group_value',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_start_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_start_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_end_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_end_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_templ_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_templ_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_GET_CST_REL;
   ---
   fetch C_GET_CST_REL BULK COLLECT into LP_dept_tbl,
                                         LP_class_tbl,
                                         LP_subclass_tbl,
                                         LP_loc_tbl,
                                         LP_start_date_tbl,
                                         LP_end_date_tbl,
                                         LP_templ_id_tbl,
                                         LP_item_tbl;
   ---
   close C_GET_CST_REL;
   ---

   if I_rowid is not NULL then
      update wf_cost_relationship
         set start_date  = I_start_date,
             end_date    = I_end_date
       where rowid = chartorowid(I_rowid);
       return TRUE;
   end if;
   ---

   if I_overwrite_ind ='Y' then
      FORALL i in 1..LP_dept_tbl.LAST
         delete wf_cost_relationship wf
          where wf.dept     = LP_dept_tbl(i)
            and wf.class    = NVL(LP_class_tbl(i),'-1')
            and wf.subclass = NVL(LP_subclass_tbl(i),'-1')
            and wf.location = LP_loc_tbl(i)
            and wf.item     = NVL(LP_item_tbl(i),'-1')
            and wf.start_date between LP_start_date_tbl(i) and LP_end_date_tbl(i)
            and wf.end_date between LP_start_date_tbl(i) and LP_end_date_tbl(i);

      --Update End_Date
      FORALL i in 1..LP_dept_tbl.LAST
         update wf_cost_relationship wf
            set wf.end_date = LP_start_date_tbl(i)-1
          where wf.dept     = LP_dept_tbl(i)
            and wf.class    = NVL(LP_class_tbl(i),'-1')
            and wf.subclass = NVL(LP_subclass_tbl(i),'-1')
            and wf.location = LP_loc_tbl(i)
            and wf.item     = NVL(LP_item_tbl(i),'-1')
            and wf.start_date < LP_start_date_tbl(i)
            and ( wf.end_date >= LP_start_date_tbl(i)
                 and wf.end_date <= LP_end_date_tbl(i));

      --Insert new record for old template
      FORALL i in 1..LP_dept_tbl.LAST
         insert into wf_cost_relationship wf
         select dept, class, subclass, location,start_date,LP_start_date_tbl(i)-1,templ_id, item
           from wf_cost_relationship wf
          where wf.dept     = LP_dept_tbl(i)
            and wf.class    = NVL(LP_class_tbl(i),'-1')
            and wf.subclass = NVL(LP_subclass_tbl(i),'-1')
            and wf.location = LP_loc_tbl(i)
            and NVL(wf.item,'-1')     = NVL(LP_item_tbl(i),'-1')
            and (( LP_start_date_tbl(i) between wf.start_date and wf.end_date)
            and  (LP_end_date_tbl(i) between wf.start_date and wf.end_date));

      --Update Start_Date
      FORALL i in 1..LP_dept_tbl.LAST
         update wf_cost_relationship wf
            set wf.start_date    = LP_end_date_tbl(i)+1
          where wf.dept     = LP_dept_tbl(i)
            and wf.class    = NVL(LP_class_tbl(i),'-1')
            and wf.subclass = NVL(LP_subclass_tbl(i),'-1')
            and wf.location = LP_loc_tbl(i)
            and wf.item     = NVL(LP_item_tbl(i),'-1')
            and ((wf.start_date <= LP_end_date_tbl(i) and wf.end_date > LP_end_date_tbl(i))
              or (wf.start_date > LP_start_date_tbl(i) and wf.end_date < LP_start_date_tbl(i)));

      ---
      FORALL i in 1..LP_dept_tbl.LAST
         merge into wf_cost_relationship wf
              using dual
                 on (wf.dept     = LP_dept_tbl(i) and
                     wf.class    = NVL(LP_class_tbl(i),'-1') and
                     wf.subclass = NVL(LP_subclass_tbl(i),'-1')and
                     wf.location = LP_loc_tbl(i) and
                     NVL(wf.item,'-1') = NVL(LP_item_tbl(i),'-1') and
                     (wf.start_date = LP_start_date_tbl(i) and
                      wf.end_date   = LP_end_date_tbl(i)) )
               when NOT matched then
             insert (wf.dept,
                     wf.class,
                     wf.subclass,
                     wf.location,
                     wf.start_date,
                     wf.end_date,
                     wf.templ_id,
                     wf.item)
              values(LP_dept_tbl(i),
                     NVL(LP_class_tbl(i),'-1'),
                     NVL(LP_subclass_tbl(i),'-1'),
                     LP_loc_tbl(i),
                     LP_start_date_tbl(i),
                     LP_end_date_tbl(i),
                     LP_templ_id_tbl(i),
                     NVL(LP_item_tbl(i),'-1'));

   else --I_overwrite_ind ='N'

      FORALL i in 1..LP_dept_tbl.LAST
         merge into wf_cost_relationship wf
              using dual
                 on (wf.dept     = LP_dept_tbl(i) and
                     wf.class    = NVL(LP_class_tbl(i),'-1') and
                     wf.subclass = NVL(LP_subclass_tbl(i),'-1') and
                     wf.location = LP_loc_tbl(i) and
                     NVL(wf.item,'-1')     = NVL(LP_item_tbl(i),'-1')and
                     ((wf.start_date <= LP_start_date_tbl(i) and
                       wf.end_date   >= LP_start_date_tbl(i)) or
                     (wf.start_date <= LP_end_date_tbl(i) and
                      wf.end_date  >= LP_end_date_tbl(i)) or
                     (wf.start_date > LP_start_date_tbl(i) and
                      wf.end_date < LP_end_date_tbl(i))))
               when matched then
             update
                set wf.start_date  = LP_start_date_tbl(i),
                    wf.end_date    = LP_end_date_tbl(i)
              where I_overwrite_ind ='Y'
               when NOT matched then
             insert (wf.dept,
                     wf.class,
                     wf.subclass,
                     wf.location,
                     wf.start_date,
                     wf.end_date,
                     wf.templ_id,
                     wf.item)
              values(LP_dept_tbl(i),
                     NVL(LP_class_tbl(i),'-1'),
                     NVL(LP_subclass_tbl(i),'-1'),
                     LP_loc_tbl(i),
                     LP_start_date_tbl(i),
                     LP_end_date_tbl(i),
                     LP_templ_id_tbl(i),
                  NVL(LP_item_tbl(i),'-1'));
   end if;

   return TRUE;

EXCEPTION
    WHEN DUP_VAL_ON_INDEX then
       O_error_message := SQL_LIB.CREATE_MSG('COST_RELN_CONFLICT',
                                             NULL,
                                             NULL,
                                             NULL);
       return FALSE;
    WHEN OTHERS THEN
       if C_GET_CST_REL%ISOPEN then
          close C_GET_CST_REL;
       end if;
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
       return FALSE;
END COST_RELATION_EXPLODE;
-------------------------------------------------------------------------------------------------------
-- Function Name  : TEMPL_RELATION_EXISTS
-- Purpose        : This function will check if the cost buildup template ID being passed in already
--                  exists in the WF_COST_RELATIONSHIP table.
-------------------------------------------------------------------------------------------------------
FUNCTION TEMPL_RELATION_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_templ_exists    IN OUT   BOOLEAN,
                               I_cbldup_tmpl_id  IN       WF_COST_RELATIONSHIP.TEMPL_ID%TYPE)
return BOOLEAN is

   L_program   VARCHAR2(64) := 'COST_BUILDUP_SQL.TEMPL_RELATION_EXISTS';
   L_dummy     VARCHAR2(1);

   cursor C_TMPL_REL_EXISTS is
      select 'x'
        from wf_cost_relationship
       where templ_id = I_cbldup_tmpl_id
         and rownum = 1;

BEGIN

   if I_cbldup_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cbldup_tmpl_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   open C_TMPL_REL_EXISTS;
   fetch C_TMPL_REL_EXISTS into L_dummy;
   close C_TMPL_REL_EXISTS;

   if L_dummy is NOT NULL then
      O_templ_exists := TRUE;
   else
      O_templ_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
    WHEN OTHERS THEN
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;
END TEMPL_RELATION_EXISTS;
-------------------------------------------------------------------------------------------------------
-- Function Name  : DEL_COST_TEMPLATE
-- Purpose        : This function will delete a cost template and any associated cost relationships.
-------------------------------------------------------------------------------------------------------
FUNCTION DEL_COST_TEMPLATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_templ_id        IN       WF_COST_BUILDUP_TMPL_HEAD.TEMPL_ID%TYPE)
return BOOLEAN is

   L_program           VARCHAR2(64) := 'COST_BUILDUP_SQL.DEL_COST_TEMPLATE';
   L_cost_rel_exists   VARCHAR2(1);
   L_wf_ord_exists     VARCHAR2(1);
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   L_table             VARCHAR2(30);

   cursor C_LOCK_COST_REL is
      select 'x'
        from wf_cost_relationship
       where templ_id = I_templ_id
         for update nowait;

   cursor C_LOCK_COST_TEMPL_HD_TL is
      select 'x'
        from wf_cost_buildup_tmpl_hd_tl
       where templ_id = I_templ_id
         for update nowait;

   cursor C_LOCK_COST_TEMPL_HEAD is
      select 'x'
        from wf_cost_buildup_tmpl_head
       where templ_id = I_templ_id
         for update nowait;

   cursor C_LOCK_COST_TEMPL_DTL_TL is
      select 'x'
        from wf_cost_buildup_tmpl_dtl_tl
       where templ_id = I_templ_id
         for update nowait;

   cursor C_LOCK_COST_TEMPL_DETAIL is
      select 'x'
        from wf_cost_buildup_tmpl_detail
       where templ_id = I_templ_id
         for update nowait;

   cursor C_CHECK_WF_ORD_DETAIL is
      select 'x'
        from wf_order_detail
       where templ_id = I_templ_id
         and rownum = 1;

BEGIN
   if I_templ_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_templ_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   open C_CHECK_WF_ORD_DETAIL;
   fetch C_CHECK_WF_ORD_DETAIL into L_wf_ord_exists;
   close C_CHECK_WF_ORD_DETAIL;
   ---
   if L_wf_ord_exists is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('WF_ORD_EXISTS',
                                            I_templ_id,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_LOCK_COST_REL;
   close C_LOCK_COST_REL;
   L_table :=  'WF_COST_RELATIONSHIP';
   ---
   delete from wf_cost_relationship
         where templ_id = I_templ_id;
   ---
   open C_LOCK_COST_TEMPL_DTL_TL;
   close C_LOCK_COST_TEMPL_DTL_TL;
   L_table :=  'WF_COST_BUILDUP_TMPL_DTL_TL';
   ---
   delete from wf_cost_buildup_tmpl_dtl_tl
         where templ_id = I_templ_id;
   ---
   open C_LOCK_COST_TEMPL_DETAIL;
   close C_LOCK_COST_TEMPL_DETAIL;
   L_table :=  'WF_COST_BUILDUP_TMPL_DETAIL';
   ---
   delete from wf_cost_buildup_tmpl_detail
         where templ_id = I_templ_id;
   ---
   open C_LOCK_COST_TEMPL_HD_TL;
   close C_LOCK_COST_TEMPL_HD_TL;
   L_table :=  'WF_COST_BUILDUP_TMPL_HD_TL';
   ---
   delete from wf_cost_buildup_tmpl_hd_tl
         where templ_id = I_templ_id;
   ---
   open C_LOCK_COST_TEMPL_HEAD;
   close C_LOCK_COST_TEMPL_HEAD;
   L_table :=  'WF_COST_BUILDUP_TMPL_HEAD';
   ---
   delete from wf_cost_buildup_tmpl_head
         where templ_id = I_templ_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_templ_id),
                                            NULL);
      return FALSE;                                      
    when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;
END DEL_COST_TEMPLATE;
-------------------------------------------------------------------------------------------------------
-- Function Name  : VALIDATE_TEMPL_ID
-- Purpose        : This function will check if a record already exists in the WF_COST_BUILDUP_TMPL_HEAD
--                  table given a tmpl_id.
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TEMPL_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_templ_desc      IN OUT   WF_COST_BUILDUP_TMPL_HEAD.TEMPL_DESC%TYPE,
                           I_templ_id        IN       WF_COST_BUILDUP_TMPL_HEAD.TEMPL_ID%TYPE)
return BOOLEAN is

   L_program        VARCHAR2(64) := 'COST_BUILDUP_SQL.VALIDATE_TEMPL_ID';

   cursor C_TMPL_EXISTS is
      select templ_desc
        from wf_cost_buildup_tmpl_head
       where templ_id = I_templ_id;

BEGIN
   if I_templ_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_templ_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_TMPL_EXISTS;
   fetch C_TMPL_EXISTS into O_templ_desc;
   close C_TMPL_EXISTS;

   if O_templ_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TEMPL_ID',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;
END VALIDATE_TEMPL_ID;
-------------------------------------------------------------------------------------------------------
-- Function Name  : COST_COMP_EXISTS
-- Purpose        : This function will be validate if the cost comp id already exists in the
--                  WF_COST_BUILDUP_TMPL_DETAIL table given the templ id.
-------------------------------------------------------------------------------------------------------
FUNCTION COST_COMP_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT   BOOLEAN,
                          I_cost_comp_id    IN       WF_COST_BUILDUP_TMPL_DETAIL.COST_COMP_ID%TYPE,
                          I_templ_id        IN       WF_COST_BUILDUP_TMPL_DETAIL.TEMPL_ID%TYPE)
return BOOLEAN is

   L_program         VARCHAR2(64) := 'COST_BUILDUP_SQL.COST_COMP_EXISTS';
   L_exists          VARCHAR2(1)  := NULL;

   cursor C_CHECK_EXISTS is
      select 'x'
        from wf_cost_buildup_tmpl_detail
       where cost_comp_id = NVL(I_cost_comp_id, cost_comp_id)
         and templ_id = I_templ_id;

BEGIN
   if I_templ_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_templ_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_CHECK_EXISTS;
   fetch C_CHECK_EXISTS into L_exists;
   close C_CHECK_EXISTS;

   if L_exists is NULL then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
    WHEN OTHERS THEN
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;
END COST_COMP_EXISTS;
-------------------------------------------------------------------------------------------------------
-- Function Name  : INSERT_COMP_DETAIL
-- Purpose        : This function will be used to insert into the WF_COST_BUILDUP_TMPL_DETAIL table
--                  used in the costbdutpld.fmb form.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_COMP_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_comp_id    IN       WF_COST_BUILDUP_TMPL_DETAIL.COST_COMP_ID%TYPE,
                            I_templ_id        IN       WF_COST_BUILDUP_TMPL_DETAIL.TEMPL_ID%TYPE)
return BOOLEAN is

   L_program        VARCHAR2(64) := 'COST_BUILDUP_SQL.INSERT_COMP_DETAIL';
   L_exists         BOOLEAN;
   L_comp_desc      ELC_COMP.COMP_DESC%TYPE;
   L_comp_type      ELC_COMP.COMP_TYPE%TYPE;
   L_expense_type   ELC_COMP.EXPENSE_TYPE%TYPE;
   L_assess_type    ELC_COMP.ASSESS_TYPE%TYPE;
   L_import_country ELC_COMP.IMPORT_COUNTRY_ID%TYPE;
   L_cvb_code       CVB_HEAD.CVB_CODE%TYPE;
   L_comp_rate      ELC_COMP.COMP_RATE%TYPE;
   L_calc_basis     ELC_COMP.CALC_BASIS%TYPE;
   L_cost_basis     ELC_COMP.COST_BASIS%TYPE;
   L_display_order  ELC_COMP.DISPLAY_ORDER%TYPE;
   L_comp_currency  CURRENCIES.CURRENCY_CODE%TYPE;
   L_per_count      ELC_COMP.PER_COUNT%TYPE;
   L_per_count_uom  ELC_COMP.PER_COUNT_UOM%TYPE;
   L_nom_flag_1     ELC_COMP.NOM_FLAG_1%TYPE;
   L_nom_flag_2     ELC_COMP.NOM_FLAG_2%TYPE;
   L_nom_flag_3     ELC_COMP.NOM_FLAG_3%TYPE;
   L_nom_flag_4     ELC_COMP.NOM_FLAG_4%TYPE;
   L_nom_flag_5     ELC_COMP.NOM_FLAG_5%TYPE;

BEGIN
   if I_cost_comp_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cost_comp_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_templ_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_templ_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if ELC_SQL.GET_COMP_DETAILS(O_error_message,
                               L_exists,
                               L_comp_desc,
                               L_comp_type,
                               L_expense_type,
                               L_assess_type,
                               L_import_country,
                               L_cvb_code,
                               L_comp_rate,
                               L_calc_basis,
                               L_cost_basis,
                               L_display_order,
                               L_comp_currency,
                               L_per_count,
                               L_per_count_uom,
                               L_nom_flag_1,
                               L_nom_flag_2,
                               L_nom_flag_3,
                               L_nom_flag_4,
                               L_nom_flag_5,
                               I_cost_comp_id) = FALSE then
      return FALSE;
   end if;
   ---
   insert into wf_cost_buildup_tmpl_detail(templ_id,
                                           cost_comp_id,
                                           description,
                                           calc_basis,
                                           comp_rate,
                                           per_count,
                                           per_count_uom,
                                           comp_currency)
                                   values (I_templ_id,
                                           I_cost_comp_id,
                                           L_comp_desc,
                                           L_calc_basis,
                                           L_comp_rate,
                                           L_per_count,
                                           L_per_count_uom,
                                           L_comp_currency);

   return TRUE;

EXCEPTION
    WHEN OTHERS THEN
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;
END INSERT_COMP_DETAIL;
-------------------------------------------------------------------------------------------------------
-- Procedure Name : GET_CST_RELATION
-- Purpose        : This new procedure will be used to return records from the Cost Relationship
--                  table given the criteria in the search block of the new Wholesale Cost
--                  Relationship form (costbdurel.fmb).
-------------------------------------------------------------------------------------------------------
PROCEDURE GET_CST_RELATION(O_wf_costrel_tbl   IN OUT   COST_BUILDUP_SQL.WF_CSTREL_TBL,
                           I_dept             IN       WF_COST_RELATIONSHIP.DEPT%TYPE,
                           I_class            IN       WF_COST_RELATIONSHIP.CLASS%TYPE,
                           I_subclass         IN       WF_COST_RELATIONSHIP.SUBCLASS%TYPE,
                           I_location         IN       WF_COST_RELATIONSHIP.LOCATION%TYPE,
                           I_start_date       IN       WF_COST_RELATIONSHIP.START_DATE%TYPE,
                           I_end_date         IN       WF_COST_RELATIONSHIP.END_DATE%TYPE,
                           I_templ_id         IN       WF_COST_RELATIONSHIP.TEMPL_ID%TYPE,
                           I_order_by         IN       VARCHAR2,
                           I_item             IN       WF_COST_RELATIONSHIP.ITEM%TYPE)
IS

   L_program         VARCHAR2(64) := 'COST_BUILDUP_SQL.GET_CST_RELATION';
   L_select          VARCHAR2(32767);
   L_where           VARCHAR2(32767);
   L_order_by        VARCHAR2(2000);
   L_cursor          VARCHAR2(32767);
   L_start_date      VARCHAR2(8);
   L_end_date        VARCHAR2(8);
   C_GET_WF_CSTREL   SYS_REFCURSOR;

BEGIN

   L_select := 'select wr.dept,'||
               '       decode(wr.class,'''||-1||''''||',NULL,wr.class),'||
               '       decode(wr.subclass,'''||-1||''''||',NULL,wr.subclass),'||
               '       decode(wr.item,'''||-1||''''||',NULL,wr.item),'||
               '       wr.location,'||
               '       wr.start_date,'||
               '       wr.end_date,'||
               '       wr.templ_id,'||
               '       wh.templ_desc,'||
               '       ''N'','||
               '       wr.rowid,'||
               '       NULL,'||
               '       ''TRUE'''||
               '        from wf_cost_relationship wr,'||
               '             wf_cost_buildup_tmpl_head wh';

   L_where := ' where wr.templ_id = wh.templ_id ';

   if I_dept is NOT NULL then
      L_where := L_where||' and wr.dept = '||to_char(I_dept)||' ';
   end if;
   ---
      if I_class is NOT NULL then
      L_where := L_where||' and wr.class = '||to_char(I_class)||' ';
   end if;
   ---
   if I_subclass is NOT NULL then
      L_where := L_where||' and wr.subclass = '||to_char(I_subclass)||' ';
   end if;
   ---
   if I_location is NOT NULL then
      L_where := L_where||' and wr.location = '||to_char(I_location)||' ';
   end if;
   ---
   if I_templ_id is NOT NULL then
      L_where := L_where||' and wr.templ_id = '||to_char(I_templ_id) ||' ';
   end if;
   ---
   if I_item is NOT NULL then
      L_where := L_where||' and wr.item = '''||I_item||''''||' ';
   end if;
   ---
   if I_end_date is NOT NULL then
      if I_start_date is NULL then
         L_end_date   := to_char(I_end_date,'YYYYMMDD');
         L_where := L_where||' and wr.end_date <= to_date('''||L_end_date||''',''YYYYMMDD'') ';
      else
         L_start_date := to_char(I_start_date,'YYYYMMDD');
         L_end_date   := to_char(I_end_date,'YYYYMMDD');
         L_where := L_where||' and ((wr.start_date >= to_date('''||L_start_date||''',''YYYYMMDD'') and '||
                             '       (wr.end_date <= to_date('''||L_end_date||''',''YYYYMMDD''))))';
      end if;
   else
      if I_start_date is NOT NULL then
         L_start_date := to_char(I_start_date,'YYYYMMDD');
         L_where := L_where||' and wr.start_date >= to_date('''||L_start_date||''',''YYYYMMDD'') ';
      end if;
   end if;

   if I_order_by is NOT NULL then
      L_order_by := ' order by '||I_order_by;
   end if;

   L_cursor := L_select||L_where||L_order_by;

   open C_GET_WF_CSTREL FOR L_cursor;
   fetch C_GET_WF_CSTREL BULK COLLECT into O_wf_costrel_tbl;
   close C_GET_WF_CSTREL;

   return;

EXCEPTION
   when OTHERS then
      if C_GET_WF_CSTREL%ISOPEN then
         close C_GET_WF_CSTREL;
      end if;
      O_wf_costrel_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                              SQLERRM,
                                                              L_program,
                                                              TO_CHAR(SQLCODE));
      O_wf_costrel_tbl(1).return_code := 'FALSE';
      return;
END GET_CST_RELATION;
-------------------------------------------------------------------------------------------------------
-- Function Name  : CALCULATE_UPCHARGES
-- Purpose        : This function will calculate the upcharge expense for an item.
-------------------------------------------------------------------------------------------------------
FUNCTION CALCULATE_UPCHARGES(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_est_upchrg_exp     IN OUT   NUMBER,
                             I_templ_id           IN       WF_COST_BUILDUP_TMPL_HEAD.TEMPL_ID%TYPE,
                             I_item               IN       ITEM_MASTER.ITEM%TYPE,
                             I_supplier           IN       SUPS.SUPPLIER%TYPE,
                             I_origin_country_id  IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_acquisition_cost   IN       FUTURE_COST.ACQUISITION_COST%TYPE,
                             I_supp_currency_code IN       SUPS.CURRENCY_CODE%TYPE)
return BOOLEAN is

   L_program             VARCHAR2(64) := 'COST_BUILDUP_SQL.CALCULATE_UPCHARGES';

   L_cost_comp_id        WF_COST_BUILDUP_TMPL_DETAIL.COST_COMP_ID%TYPE;
   L_calc_basis          WF_COST_BUILDUP_TMPL_DETAIL.CALC_BASIS%TYPE;
   L_comp_rate           WF_COST_BUILDUP_TMPL_DETAIL.COMP_RATE%TYPE;
   L_per_count           WF_COST_BUILDUP_TMPL_DETAIL.PER_COUNT%TYPE;
   L_per_count_uom       WF_COST_BUILDUP_TMPL_DETAIL.PER_COUNT_UOM%TYPE;
   L_comp_currency       WF_COST_BUILDUP_TMPL_DETAIL.COMP_CURRENCY%TYPE;

   L_dimension_ind       VARCHAR2(1) := NULL;
   L_est_upchrg_exp      NUMBER;
   L_est_upchrg_exp_loc  NUMBER;

   L_standard_uom        UOM_CLASS.UOM%TYPE;
   L_standard_class      UOM_CLASS.UOM_CLASS%TYPE;

   L_per_unit_value      NUMBER   := 0;
   L_value               NUMBER   := 0;

   L_uom                 UOM_CLASS.UOM%TYPE;
   L_uom_class           UOM_CLASS.UOM_CLASS%TYPE;

   L_supp_pack_size      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_ship_carton_wt      ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_weight_uom          ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_ship_carton_len     ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_ship_carton_hgt     ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE;
   L_ship_carton_wid     ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE;
   L_dimension_uom       ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_liquid_volume       ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE;
   L_liquid_volume_uom   ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE;


   cursor C_GET_TEMPL_DTL is
      select cost_comp_id,
             calc_basis,
             comp_rate,
             per_count,
             per_count_uom,
             comp_currency
        from wf_cost_buildup_tmpl_detail
       where templ_id = I_templ_id;

 cursor C_GET_DIMENSION is
      select i.supp_pack_size,
             id.weight,
             id.weight_uom,
             id.length,
             id.height,
             id.width,
             id.lwh_uom,
             id.liquid_volume,
             id.liquid_volume_uom
       from  item_supp_country i,
             item_supp_country_dim id
       where i.item = id.item
       and   i.supplier = id.supplier
       and   i.origin_country_id = id.origin_country
       and   id.dim_object = 'CA'
       and   i.item       = I_item
         and ((i.supplier = I_supplier
               and I_supplier is not NULL)
          or (i.primary_supp_ind = 'Y'
              and I_supplier is NULL))
         and ((i.origin_country_id = I_origin_country_id
               and I_origin_country_id is not NULL)
          or (i.primary_country_ind = 'Y'
              and I_origin_country_id is NULL));

   cursor C_GET_MISC_VALUE is
      select value
        from item_supp_uom
       where item     = I_item
         and supplier = I_supplier
         and uom      = L_per_count_uom;

   cursor C_CHK_CALC_BASIS is
      select 'x'
        from wf_cost_buildup_tmpl_detail
       where templ_id   = I_templ_id
         and calc_basis = 'S'
         and rownum     = 1;

BEGIN
   O_est_upchrg_exp := 0;

   open C_CHK_CALC_BASIS;
   fetch C_CHK_CALC_BASIS into L_dimension_ind;
   close C_CHK_CALC_BASIS;

   if L_dimension_ind is NOT NULL then
      open C_GET_DIMENSION;
      fetch C_GET_DIMENSION into L_supp_pack_size,
                                 L_ship_carton_wt,
                                 L_weight_uom,
                                 L_ship_carton_len,
                                 L_ship_carton_hgt,
                                 L_ship_carton_wid,
                                 L_dimension_uom,
                                 L_liquid_volume,
                                 L_liquid_volume_uom;
      close C_GET_DIMENSION;
   end if;

   FOR rec in C_GET_TEMPL_DTL LOOP
      L_cost_comp_id  := rec.cost_comp_id;
      L_calc_basis    := rec.calc_basis;
      L_comp_rate     := rec.comp_rate;
      L_per_count     := rec.per_count;
      L_per_count_uom := rec.per_count_uom;
      L_comp_currency := rec.comp_currency;

      L_est_upchrg_exp := 0;
      L_value          := 0;

      if L_calc_basis = 'V' then
         L_est_upchrg_exp := I_acquisition_cost * (L_comp_rate/100);

      elsif L_calc_basis = 'S' then
         if UOM_SQL.GET_CLASS(O_error_message,
                              L_uom_class,
                              L_per_count_uom) = FALSE then
           return FALSE;
         end if;
         ---
         if L_uom_class = 'QTY' then
            if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                                L_standard_uom,
                                                L_standard_class,
                                                L_per_unit_value, -- standard UOM conversion factor
                                                I_item,
                                                'N') = FALSE then
               return FALSE;
            end if;
            ---
            if L_per_unit_value is NULL then
               if L_standard_uom <> 'EA' then
                  L_per_unit_value := 0;
               else
                  L_per_unit_value := 1;
               end if;
            end if;
            ---
            L_uom := 'EA';

         elsif L_uom_class = 'PACK' then
            L_value := 1/L_supp_pack_size;

         elsif L_uom_class = 'MISC' then
            open C_GET_MISC_VALUE;
            fetch C_GET_MISC_VALUE into L_value;
            close C_GET_MISC_VALUE;
            ---
            if L_value is NULL then
               L_value := 0;
            end if;

         elsif L_uom_class = 'MASS' then
            if L_ship_carton_wt is NULL then
               L_per_unit_value := 0;
            else
               L_per_unit_value := L_ship_carton_wt/L_supp_pack_size;
               L_uom := L_weight_uom;
            end if;

         elsif L_uom_class = 'LVOL' then
            if L_liquid_volume_uom is NULL then
               L_per_unit_value := 0;
            else
               L_per_unit_value := L_liquid_volume/L_supp_pack_size;
               L_uom := L_liquid_volume_uom;
            end if;

         elsif L_uom_class = 'VOL' then
            if L_ship_carton_len is NULL or
               L_ship_carton_wid is NULL or
               L_ship_carton_hgt is NULL then
               L_per_unit_value := 0;
            else
               ---
               -- Get the per unit ship carton volume.
               ---
               L_per_unit_value := (L_ship_carton_len * L_ship_carton_hgt * L_ship_carton_wid)
                                    /L_supp_pack_size;
               ---
               L_uom := L_dimension_uom||'3';
            end if;

         elsif L_uom_class = 'AREA' then
            if L_ship_carton_len is NULL or
               L_ship_carton_wid is NULL then
               L_per_unit_value := 0;
            else
               ---
               -- Get the per unit ship carton area.
               ---
               L_per_unit_value := (L_ship_carton_len * L_ship_carton_wid)/L_supp_pack_size;
               ---
               L_uom := L_dimension_uom||'2';
            end if;

         elsif L_uom_class = 'DIMEN' then
            if L_ship_carton_len is NULL then
               L_per_unit_value := 0;
            else
               ---
               -- Get the per unit ship carton length.
               ---
               L_per_unit_value := L_ship_carton_len/L_supp_pack_size;
               ---
               L_uom := L_dimension_uom;
            end if;
         end if;
         ---
         if L_uom_class in ('VOL','AREA','DIMEN','QTY','MASS','LVOL') then
            if L_per_unit_value != 0 then
               if UOM_SQL.WITHIN_CLASS(O_error_message,
                                       L_value,
                                       L_per_count_uom,
                                       L_per_unit_value,
                                       L_uom,
                                       L_uom_class) = FALSE then
                  return FALSE;
               end if;
            else
               L_value := 0;
            end if;
         end if;
         ---
         L_est_upchrg_exp := L_value * (L_comp_rate/L_per_count);

         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_est_upchrg_exp,
                                 L_comp_currency,
                                 I_supp_currency_code,
                                 L_est_upchrg_exp_loc,
                                 NULL,
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;

         L_est_upchrg_exp := L_est_upchrg_exp_loc;
      end if;
      ---
      --calculate the running total for the upcharge expense
      ---
      O_est_upchrg_exp := O_est_upchrg_exp + L_est_upchrg_exp;
   END LOOP;

   return TRUE;

EXCEPTION
    WHEN OTHERS THEN
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;
END CALCULATE_UPCHARGES;
-------------------------------------------------------------------------------------------------------
-- Function Name  : APPLY_COST_TEMPLATE
-- Purpose        : This new function will be called from the wfcostcalc.pc and elccostcalc.pc batch programs
--                  to calculate the total applicable cost template value.
-------------------------------------------------------------------------------------------------------
FUNCTION APPLY_COST_TEMPLATE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_est_cost_temp      IN OUT   NUMBER,
                             I_item               IN       ITEM_MASTER.ITEM%TYPE,
                             I_supplier           IN       SUPS.SUPPLIER%TYPE,
                             I_origin_country_id  IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_location           IN       ITEM_LOC.LOC%TYPE,
                             I_acquisition_cost   IN       FUTURE_COST.ACQUISITION_COST%TYPE,
                             I_date               IN       WF_COST_RELATIONSHIP.START_DATE%TYPE,
                             I_supp_currency_code IN       SUPS.CURRENCY_CODE%TYPE)
return BOOLEAN is

   L_program             VARCHAR2(64) := 'COST_BUILDUP_SQL.APPLY_COST_TEMPLATE';
   L_templ_id            WF_COST_BUILDUP_TMPL_HEAD.TEMPL_ID%TYPE;
   L_first_applied       WF_COST_BUILDUP_TMPL_HEAD.FIRST_APPLIED%TYPE;
   L_margin_pct          WF_COST_BUILDUP_TMPL_HEAD.MARGIN_PCT%TYPE;
   L_upchrg_ind          VARCHAR2(1);

   L_est_margin_exp      NUMBER;
   L_est_upchrg_exp      NUMBER;
   L_margin_acquistion   NUMBER;
   L_upchrge_acquistion  NUMBER;

   cursor C_GET_TEMPL_HEAD is
      select wb.templ_id,
             wb.first_applied,
             wb.margin_pct
        from wf_cost_buildup_tmpl_head wb,
             wf_cost_relationship wc,
             item_master im
       where wb.templ_id  = wc.templ_id
         and wc.dept      = im.dept
         and wc.class     = im.class
         and wc.subclass  = im.subclass
         and im.item      = I_item
         and wc.location  = I_location
         and I_date between wc.start_date and wc.end_date;

   cursor C_CHECK_TMPL_DETAIL is
      select 'x'
        from wf_cost_buildup_tmpl_detail
       where templ_id = L_templ_id;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;


   if I_acquisition_cost is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_acquisition_cost',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- get header information including the MARGIN_PCT
   -- value to determine if the cost template has margin
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TEMPL_HEAD',
                    'WF_COST_BUILDUP_TMPL_HEAD',
                    NULL);
   open C_GET_TEMPL_HEAD;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TEMPL_HEAD',
                    'WF_COST_BUILDUP_TMPL_HEAD',
                    NULL);
   ---
   fetch C_GET_TEMPL_HEAD into L_templ_id,
                               L_first_applied,
                               L_margin_pct;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TEMPL_HEAD',
                    'WF_COST_BUILDUP_TMPL_HEAD',
                    NULL);
   close C_GET_TEMPL_HEAD;

   -- check if WF_COST_BUILDUP_TMPL_DETAIL contains records
   -- to determine if the cost template has upcharges
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_TMPL_DETAIL',
                    'WF_COST_BUILDUP_TMPL_DETAIL',
                    NULL);
   open C_CHECK_TMPL_DETAIL;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_TMPL_DETAIL',
                    'WF_COST_BUILDUP_TMPL_DETAIL',
                    NULL);
   ---
   fetch C_CHECK_TMPL_DETAIL into L_upchrg_ind;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_TMPL_DETAIL',
                    'WF_COST_BUILDUP_TMPL_DETAIL',
                    NULL);
   close C_CHECK_TMPL_DETAIL;

   if L_first_applied = 'M' then
      L_est_margin_exp := I_acquisition_cost * (L_margin_pct/100);
      ---
      if L_upchrg_ind is NOT NULL then
         ---
         L_margin_acquistion := I_acquisition_cost + L_est_margin_exp;
         if COST_BUILDUP_SQL.CALCULATE_UPCHARGES(O_error_message,
                                                 L_est_upchrg_exp,
                                                 L_templ_id,
                                                 I_item,
                                                 I_supplier,
                                                 I_origin_country_id,
                                                 L_margin_acquistion,
                                                 I_supp_currency_code) = FALSE then
            return FALSE;
         end if;
         ---
         O_est_cost_temp := L_est_margin_exp + L_est_upchrg_exp;
      else
         O_est_cost_temp := L_est_margin_exp;
      end if;

   elsif L_first_applied = 'U' then
      if COST_BUILDUP_SQL.CALCULATE_UPCHARGES(O_error_message,
                                              L_est_upchrg_exp,
                                              L_templ_id,
                                              I_item,
                                              I_supplier,
                                              I_origin_country_id,
                                              I_acquisition_cost,
                                              I_supp_currency_code) = FALSE then
            return FALSE;
      end if;
      ---
      if L_margin_pct is NOT NULL then
         L_upchrge_acquistion := I_acquisition_cost + L_est_upchrg_exp;
         L_est_margin_exp := L_upchrge_acquistion * (L_margin_pct/100);
         O_est_cost_temp := L_est_upchrg_exp + L_est_margin_exp;
      else
         O_est_cost_temp := L_est_upchrg_exp;
      end if;
   end if;

   return TRUE;

EXCEPTION
    WHEN OTHERS THEN
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;
END APPLY_COST_TEMPLATE;
-------------------------------------------------------------------------------------------------------
-- Function Name : DEL_COST_REL
-- Purpose        : This function will delete from the WF_COST_RELATIONSHIP table.
-------------------------------------------------------------------------------------------------------
FUNCTION DEL_COST_REL(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_wf_costrel_tbl   IN      COST_BUILDUP_SQL.WF_CSTREL_TBL) RETURN BOOLEAN
IS

   L_program   VARCHAR2(64) := 'DEL_COST_REL';
   L_index     BINARY_INTEGER := 0;
   L_del_index BINARY_INTEGER := 0;

BEGIN
   LP_cstrel_count := I_wf_costrel_tbl.count;

   LP_dept_tbl.delete;
   LP_class_tbl.delete;
   LP_subclass_tbl.delete;
   LP_loc_tbl.delete;
   LP_start_date_tbl.delete;
   LP_end_date_tbl.delete;
   LP_item_tbl.delete;
   ---
   LP_dept_tbl.extend(LP_cstrel_count);
   LP_class_tbl.extend(LP_cstrel_count);
   LP_subclass_tbl.extend(LP_cstrel_count);
   LP_loc_tbl.extend(LP_cstrel_count);
   LP_start_date_tbl.extend(LP_cstrel_count);
   LP_end_date_tbl.extend(LP_cstrel_count);
   LP_item_tbl.extend(LP_cstrel_count);

   L_index     := I_wf_costrel_tbl.FIRST;

   WHILE L_index is NOT NULL LOOP
      L_del_index := L_del_index + 1;
      LP_dept_tbl(L_del_index)           := I_wf_costrel_tbl(L_index).dept;
      LP_class_tbl(L_del_index)          := I_wf_costrel_tbl(L_index).class;
      LP_subclass_tbl(L_del_index)       := I_wf_costrel_tbl(L_index).subclass;
      LP_loc_tbl(L_del_index)            := I_wf_costrel_tbl(L_index).location;
      LP_start_date_tbl(L_del_index)     := I_wf_costrel_tbl(L_index).start_date;
      LP_end_date_tbl(L_del_index)       := I_wf_costrel_tbl(L_index).end_date;
      LP_item_tbl(L_del_index)           := I_wf_costrel_tbl(L_index).item;

      L_index := I_wf_costrel_tbl.NEXT(L_index);
   END LOOP;

   if L_del_index > 0 then

     SQL_LIB.SET_MARK('DELETE',
                      NULL,
                      'WF_COST_RELATIONSHIP',
                      NULL);

      FORALL i in 1..L_del_index
         delete from wf_cost_relationship
               where dept       = LP_dept_tbl(i)
                 and class      = NVL(LP_class_tbl(i),'-1')
                 and subclass   = NVL(LP_subclass_tbl(i),'-1')
                 and location   = LP_loc_tbl(i)
                 and start_date = LP_start_date_tbl(i)
                 and end_date   = LP_end_date_tbl(i)
                 and item       = NVL(LP_item_tbl(i),'-1')
                 and (end_date < get_vdate or start_date > get_vdate);

      FORALL i in 1..L_del_index
         update wf_cost_relationship
            set end_date   = get_vdate
          where dept       = LP_dept_tbl(i)
            and class      = NVL(LP_class_tbl(i),'-1')
            and subclass   = NVL(LP_subclass_tbl(i),'-1')
            and location   = LP_loc_tbl(i)
            and start_date = LP_start_date_tbl(i)
            and end_date   = LP_end_date_tbl(i)
            and item       = NVL(LP_item_tbl(i),'-1')
            and start_date <= get_vdate
            and end_date   > get_vdate;
   end if;


   return TRUE;

EXCEPTION
    WHEN OTHERS THEN
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;

END DEL_COST_REL;
-------------------------------------------------------------------------------------------------------
-- Function Name : DUP_RELN_EXISTS
-- Purpose       : This function will check if duplicate record already exists in WF_COST_RELATIONSHIP
-- for the passed in department,classs,subclass,location,start_date and end_date.
-------------------------------------------------------------------------------------------------------
FUNCTION DUP_RELN_EXISTS (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_dup_reln_exists IN OUT  BOOLEAN,
                          I_dept            IN      WF_COST_RELATIONSHIP.DEPT%TYPE,
                          I_class           IN      WF_COST_RELATIONSHIP.CLASS%TYPE,
                          I_subclass        IN      WF_COST_RELATIONSHIP.SUBCLASS%TYPE,
                          I_location        IN      WF_COST_RELATIONSHIP.LOCATION%TYPE,
                          I_start_date      IN      WF_COST_RELATIONSHIP.START_DATE%TYPE,
                          I_end_date        IN      WF_COST_RELATIONSHIP.END_DATE%TYPE,
                          I_item            IN      WF_COST_RELATIONSHIP.ITEM%TYPE)
return BOOLEAN is

   L_program   VARCHAR2(64) := 'COST_BUILDUP_SQL.DUP_RELN_EXISTS';
   L_exists    VARCHAR2(1)  := NULL;

   cursor C_DUP_RELN_EXISTS is
      select 'x'
        from wf_cost_relationship w
       where w.dept    = I_dept
         and w.class   = NVL(I_class,'-1')
         and w.subclass = NVL(I_subclass,'-1')
         and w.location = I_location
         and w.start_date = I_start_date
         and w.end_date   = I_end_date
         and NVL(w.item,'-1') = NVL(I_item,'-1')
         and rownum = 1;

BEGIN

   -- check required parameters
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_class',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_subclass',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_start_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_start_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_end_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_end_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   open C_DUP_RELN_EXISTS;
   fetch C_DUP_RELN_EXISTS into L_exists;
   close C_DUP_RELN_EXISTS;

   if L_exists is NULL then
      O_dup_reln_exists := FALSE;
   else
      O_dup_reln_exists := TRUE;
   end if;

return TRUE;

EXCEPTION
    WHEN OTHERS THEN
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
        return FALSE;
END DUP_RELN_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_FIRSTAPPLIED_VALUE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_first_applied   IN OUT  WF_COST_BUILDUP_TMPL_HEAD.FIRST_APPLIED%TYPE,
                                I_templ_id        IN      WF_COST_RELATIONSHIP.TEMPL_ID%TYPE)
 RETURN BOOLEAN AS

 L_program   VARCHAR2(64) := 'COST_BUILDUP_SQL.GET_FIRSTAPPLIED_VALUE';

 cursor c_get_applied is
    select first_applied
      from wf_cost_buildup_tmpl_head
     where templ_id = I_templ_id;

BEGIN

   open c_get_applied;
   fetch c_get_applied into O_first_applied;
   close c_get_applied;

   return TRUE;


EXCEPTION
   WHEN OTHERS THEN
      if c_get_applied%ISOPEN then
         close c_get_applied;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END  GET_FIRSTAPPLIED_VALUE;
--------------------------------------------------------------------------------------------------
FUNCTION CREATE_FC_FOR_COST_TMPL_RELSHP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'COST_BUILDUP_SQL.CREATE_FC_FOR_COST_TMPL_RELSHP';
   L_error_message RTK_ERRORS.RTK_TEXT%TYPE;
   L_reln_tbl      OBJ_TEMPL_RELN_EVENT_TBL;
   L_reln_rec      OBJ_TEMPL_RELN_EVENT_REC;
   L_action        COST_EVENT.ACTION%TYPE;
   L_user_id       COST_EVENT.USER_ID%TYPE :=  USER;
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;


   cursor C_ACTION is
      select distinct action
        from gtt_wf_cost_relationship;

   cursor C_DATA is
      select dept,
             class,
             subclass,
             location,
             old_start_date,
             old_end_date,
             new_start_date,
             new_end_date,
             templ_id,
             decode(item,'-1',NULL,item) item
       from  gtt_wf_cost_relationship
      where  action = L_action;

BEGIN
   L_reln_tbl := new OBJ_TEMPL_RELN_EVENT_TBL();
   FOR rec in C_ACTION LOOP
      L_action := rec.action;
      L_reln_tbl.DELETE;
      FOR rec1 in C_DATA LOOP
         L_reln_rec := new OBJ_TEMPL_RELN_EVENT_REC(rec1.dept,
                                                    rec1.class,
                                                    rec1.subclass,
                                                    rec1.location,
                                                    rec1.old_start_date,
                                                    rec1.new_start_date,
                                                    rec1.old_end_date,
                                                    rec1.new_end_date,
                                                    rec1.templ_id,
                                                    rec1.item);

         L_reln_tbl.EXTEND;
         L_reln_tbl(L_reln_tbl.COUNT) := L_reln_rec;
      END LOOP;
      if L_reln_tbl.count > 0 AND L_reln_tbl is not null then
         if FUTURE_COST_EVENT_SQL.ADD_COST_TMPL_RELATIONSHIP_CHG(O_error_message,
                                                                 L_cost_event_process_id,
                                                                 L_reln_tbl,
                                                                 L_action,
                                                                 L_user_id) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_FC_FOR_COST_TMPL_RELSHP;
---------------------------------------------------------------------------------------------
FUNCTION DEL_COST_REL_WRP(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_wf_costrel_tbl   IN       WRP_WF_COST_RELSHP_TBL)
   RETURN INTEGER IS

   L_program          VARCHAR2(64) := 'COST_BUILDUP_SQL.DEL_COST_REL_WRP';
   L_wf_costrel_tbl   COST_BUILDUP_SQL.WF_CSTREL_TBL;

BEGIN

   if I_wf_costrel_tbl is NOT NULL and I_wf_costrel_tbl.COUNT > 0 then

      ---
      FOR i IN 1..I_wf_costrel_tbl.COUNT LOOP
         L_wf_costrel_tbl(i).dept       := I_wf_costrel_tbl(i).dept;
         L_wf_costrel_tbl(i).class      := I_wf_costrel_tbl(i).class;
         L_wf_costrel_tbl(i).subclass   := I_wf_costrel_tbl(i).subclass;
         L_wf_costrel_tbl(i).item       := I_wf_costrel_tbl(i).item;
         L_wf_costrel_tbl(i).location   := I_wf_costrel_tbl(i).location;
         L_wf_costrel_tbl(i).start_date := I_wf_costrel_tbl(i).start_date;
         L_wf_costrel_tbl(i).end_date   := I_wf_costrel_tbl(i).end_date;
      END LOOP;
      ---

      if COST_BUILDUP_SQL.DEL_COST_REL(O_error_message,
                                       L_wf_costrel_tbl) = FALSE then
         return 0;
      end if;
   end if;

   return 1;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return 0;
END DEL_COST_REL_WRP;
---------------------------------------------------------------------------------------------
FUNCTION DEL_COST_TEMP_COMP(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_templ_id         IN       WF_COST_BUILDUP_TMPL_DETAIL.TEMPL_ID%TYPE,
                            I_cost_comp_id     IN       WF_COST_BUILDUP_TMPL_DETAIL.COST_COMP_ID%TYPE)
 RETURN BOOLEAN is

   L_program           VARCHAR2(64) := 'COST_BUILDUP_SQL.DEL_COST_TEMP_COMP';
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   L_table             VARCHAR2(30);

   cursor C_LOCK_COST_BUILDUP_TMPL_DTL is
      select 'x'
        from wf_cost_buildup_tmpl_detail
       where templ_id = I_templ_id
         and cost_comp_id = I_cost_comp_id
         for update nowait;

   cursor C_LOCK_CST_BUILDUP_TMPL_DTL_TL is
      select 'x'
        from wf_cost_buildup_tmpl_dtl_tl
       where templ_id = I_templ_id
         and cost_comp_id = I_cost_comp_id
         for update nowait;
BEGIN
   if I_templ_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_templ_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_cost_comp_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cost_comp_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_LOCK_CST_BUILDUP_TMPL_DTL_TL;
   close C_LOCK_CST_BUILDUP_TMPL_DTL_TL;
   L_table :=  'WF_COST_BUILDUP_TMPL_DTL_TL';
   ---
   delete from wf_cost_buildup_tmpl_dtl_tl
         where templ_id = I_templ_id
           and cost_comp_id = I_cost_comp_id;
   ---           
   open C_LOCK_COST_BUILDUP_TMPL_DTL;
   close C_LOCK_COST_BUILDUP_TMPL_DTL;
   L_table :=  'WF_COST_BUILDUP_TMPL_DETAIL';
   ---
   delete from wf_cost_buildup_tmpl_detail
         where templ_id = I_templ_id
           and cost_comp_id = I_cost_comp_id;
   return TRUE;

   EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_templ_id),
                                            I_cost_comp_id);
      return FALSE;
    when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;
    END DEL_COST_TEMP_COMP;
 ---------------------------------------------------------------------------------------------
END;
/
