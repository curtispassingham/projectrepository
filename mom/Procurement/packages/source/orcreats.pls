CREATE OR REPLACE PACKAGE ORDER_CREATE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------
-- Function Name: CREATE_ORDER
-- Purpose  : for automatic PO, creates the order record on ordhead
-- Calls    : SUPP_ATTRIB_SQL.ORDER_SUP_DETAILS
--          : TICKET_SQL.APPROVE, SQL_LIB.GET_USER_NAME
-----------------------------------------------------------------------
FUNCTION CREATE_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_approve_ind     IN OUT   VARCHAR2,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                      I_supplier        IN       SUPS.SUPPLIER%TYPE,
                      I_dept            IN       DEPS.DEPT%TYPE,
                      I_order_curr      IN       CURRENCIES.CURRENCY_CODE%TYPE,
                      I_vdate           IN       DATE,
                      I_order_type      IN       ORDHEAD.ORDER_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function Name: ORDER_ITEM
-- Purpose  : creates the order-location and the order-item records
--            after the header record is created in CREATE_ORDER
-- Calls    : none
-----------------------------------------------------------------------
FUNCTION ORDER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                    I_vdate           IN       DATE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
END ORDER_CREATE_SQL;
/
