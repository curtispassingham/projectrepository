CREATE OR REPLACE PACKAGE FUTURE_COST_THREAD_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------- 
-- Procedure Name:  PROCESS_COST_EVENTS
-- Purpose       :  Calls the appropriate threading function based on cost event type.
----------------------------------------------------------------------------------------
PROCEDURE PROCESS_COST_EVENTS(I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              O_return_code             IN OUT NUMBER,
                              O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_override_event_run_type IN     VARCHAR2 DEFAULT 'N');
----------------------------------------------------------------------------------------
-- Procedure Name: PROCESS_COST_EVENTS
-- Purpose       : Wrapper procedure to call FUTURE_COST_THREAD_SQL.PROCESS_COST_EVENTS internally .
--                 This procedure is called from DBMS_SCHEDULER  
-----------------------------------------------------------------------------------------
PROCEDURE PROCESS_COST_EVENTS(I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE);
----------------------------------------------------------------------------------------

-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
-- compile private functions as public for unit testing
$if $$UTPLSQL=TRUE $then
   FUNCTION REFRESH_THREAD_TABLE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   --
   FUNCTION GET_ITEMLOC_NIL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_COST_CHANGE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                    I_action                IN     COST_EVENT.ACTION%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                I_action                IN         COST_EVENT.ACTION%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_MERGE_MERCH_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_MERGE_ORG_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_MERGE_SUPP_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_ELC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_COST_ZONE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_ITEM_CZG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_action                IN     COST_EVENT.ACTION%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_PRIMARY_PACK_COST(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_TEMPLATE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_TEMPLATE_RELN(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION GET_ITEMLOC_DEAL_PASSTHRU(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION PROCESS_SUPP_CNTR(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_action                IN     COST_EVENT.ACTION%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION THREAD_ITEM_LOC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---
   FUNCTION SCHEDULE_THREADS(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_override_event_run_type IN     VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;                             
   ---
   PROCEDURE WRITE_ERROR(I_error_message         IN RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE);
   --
   FUNCTION GET_ITEMLOC_FC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ---                      
                         
$end
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
-- Procedure Name:  INSERT_BATCH_THREADS
-- Purpose       :  Inserts the thread values into COST_EVENT_THREAD to be used by the 
--                  future cost engine batch process.
----------------------------------------------------------------------------------------
FUNCTION INSERT_BATCH_THREADS(I_thread_num            IN     NUMBER,
                              O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;                        
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
-- Function Name:  ENQUEUE_COST_EVENT_THREAD
-- Purpose      :  Whenever the cost_engine is run in ASYNC mode this function
--                 enqueues the message for dequeuing it later.
-------------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_COST_EVENT_THREAD(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_cost_event_process_id  IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                   I_thread_id              IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
END FUTURE_COST_THREAD_SQL;
/