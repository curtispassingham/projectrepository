CREATE OR REPLACE PACKAGE DEAL_INCOME_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------------------
   TYPE deal_threshold_rec IS RECORD 
        (lower_limit DEAL_THRESHOLD.LOWER_LIMIT%TYPE,
         upper_limit DEAL_THRESHOLD.UPPER_LIMIT%TYPE,
         total_ind   DEAL_THRESHOLD.TOTAL_IND%TYPE,
         value       DEAL_THRESHOLD.VALUE%TYPE
         );
   TYPE deal_threshold_tbl IS TABLE of deal_threshold_rec INDEX BY BINARY_INTEGER;

   TYPE deal_component_actuals_rec IS RECORD
        (deal_id                    DEAL_DETAIL.DEAL_ID%TYPE,
         deal_detail_id             DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
         reporting_date             VARCHAR2(10),
         component_actuals          NUMBER(20,4),
         component_actuals_unit     NUMBER(20,4),
         component_actuals_revenue  NUMBER(20,4), 
         order_no                   DEAL_ACTUALS_ITEM_LOC.ORDER_NO%TYPE
        );

   TYPE deal_component_actuals_tbl IS TABLE of deal_component_actuals_rec INDEX BY BINARY_INTEGER;

   TYPE turnover_itemloc_td_rec IS RECORD
        (deal_id                            DEAL_ACTUALS_ITEM_LOC.DEAL_ID%TYPE,
         deal_detail_id                     DEAL_ACTUALS_ITEM_LOC.DEAL_DETAIL_ID%TYPE,
         item                               DEAL_ACTUALS_ITEM_LOC.ITEM%TYPE,
         loc_type                           DEAL_ACTUALS_ITEM_LOC.LOC_TYPE%TYPE,
         location                           DEAL_ACTUALS_ITEM_LOC.LOCATION%TYPE,
         order_no                           DEAL_ACTUALS_ITEM_LOC.ORDER_NO%TYPE,
         reporting_date                     VARCHAR2(10),
         total_actual_turnover_itemloc      DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_REVENUE%TYPE
        );

   TYPE turnover_itemloc_td_tbl IS TABLE of turnover_itemloc_td_rec INDEX BY BINARY_INTEGER;

   TYPE daf_totals_rec IS RECORD
        (baseline_turnover               DEAL_ACTUALS_FORECAST.BASELINE_TURNOVER%TYPE,
         budget_turnover                 DEAL_ACTUALS_FORECAST.BUDGET_TURNOVER%TYPE,
         budget_income                   DEAL_ACTUALS_FORECAST.BUDGET_INCOME%TYPE,
         actual_forecast_turnover        DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE,
         actual_forecast_income          DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_INCOME%TYPE,
         actual_forecast_trend_turnover  DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TREND_TURNOVER%TYPE,
         actual_forecast_trend_income    DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TREND_INCOME%TYPE,
         actual_income                   DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE
        );

   TYPE daf_totals_tbl IS TABLE OF daf_totals_rec INDEX BY BINARY_INTEGER;

   TYPE deal_head_rec IS RECORD
        (rowid                          VARCHAR2(19),
         currency_code                  DEAL_HEAD.CURRENCY_CODE%TYPE,
         billing_type                   DEAL_HEAD.BILLING_TYPE%TYPE,
         deal_appl_timing               DEAL_HEAD.DEAL_APPL_TIMING%TYPE,
         threshold_limit_type           DEAL_HEAD.THRESHOLD_LIMIT_TYPE%TYPE,
         threshold_limit_uom            DEAL_HEAD.THRESHOLD_LIMIT_UOM%TYPE,
         rebate_ind                     DEAL_HEAD.REBATE_IND%TYPE,
         rebate_calc_type               DEAL_HEAD.REBATE_CALC_TYPE%TYPE,
         growth_rebate_ind              DEAL_HEAD.GROWTH_REBATE_IND%TYPE,
         rebate_purch_sales_ind         DEAL_HEAD.REBATE_PURCH_SALES_IND%TYPE,
         bill_back_method               DEAL_HEAD.BILL_BACK_METHOD%TYPE,
         deal_income_calculation        DEAL_HEAD.DEAL_INCOME_CALCULATION%TYPE,
         stock_ledger_ind               DEAL_HEAD.STOCK_LEDGER_IND%TYPE,
         include_vat_ind                DEAL_HEAD.INCLUDE_VAT_IND%TYPE,
         status                         DEAL_HEAD.STATUS%TYPE,
         growth_rate_to_date            DEAL_HEAD.GROWTH_RATE_TO_DATE%TYPE,
         turnover_to_date               DEAL_HEAD.TURNOVER_TO_DATE%TYPE,
         actual_monies_earned_to_date   DEAL_HEAD.ACTUAL_MONIES_EARNED_TO_DATE%TYPE
        );

   TYPE deal_head_tbl IS TABLE of deal_head_rec INDEX BY BINARY_INTEGER;

   TYPE deal_detail_rec IS RECORD
        (rowid                           VARCHAR2(19),
         calc_to_zero_ind                DEAL_DETAIL.CALC_TO_ZERO_IND%TYPE,
         threshold_value_type            DEAL_DETAIL.THRESHOLD_VALUE_TYPE%TYPE,
         total_forecast_units            DEAL_DETAIL.TOTAL_FORECAST_UNITS%TYPE,
         total_forecast_revenue          DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE,
         total_budget_turnover           DEAL_DETAIL.TOTAL_BUDGET_TURNOVER%TYPE,
         total_actual_forecast_turnover  DEAL_DETAIL.TOTAL_ACTUAL_FORECAST_TURNOVER%TYPE,
         total_baseline_growth_budget    DEAL_DETAIL.TOTAL_BASELINE_GROWTH_BUDGET%TYPE,
         total_baseline_growth_act_for   DEAL_DETAIL.TOTAL_BASELINE_GROWTH_ACT_FOR%TYPE,
         growth_rate_to_date             DEAL_DETAIL.GROWTH_RATE_TO_DATE%TYPE
        );

   TYPE deal_detail_tbl IS TABLE of deal_detail_rec INDEX BY BINARY_INTEGER;

   TYPE all_periods_turnover_rec IS RECORD
        (actual_forecast_turnover  DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE,
         actual_forecast_ind       DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_IND%TYPE,
         reporting_date            VARCHAR2(10)
        );

   TYPE all_periods_turnover_tbl IS TABLE of all_periods_turnover_rec INDEX BY BINARY_INTEGER;

   TYPE daf_details_rec IS RECORD 
        (deal_id          DEAL_HEAD.DEAL_ID%TYPE,
         deal_detail_id   DEAL_ACTUALS_ITEM_LOC.DEAL_DETAIL_ID%TYPE,
         reporting_date   DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE,
         actual_turnover  NUMBER(20,4),
         actual_income    DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE
        );

   TYPE daf_details_tbl IS TABLE of daf_details_rec INDEX BY BINARY_INTEGER;
---------------------------------------------------------------------------------------------------------------
-- Function: EXTERNAL_APPLY_DEAL_PERF
-- Purpose : This function is the external interface function that will be called from the online modules;
--           The calling module will pass through the deal id and deal detail id along with a set of Y/N flags
--           which will identify the internal functionality that is required.
---------------------------------------------------------------------------------------------------------------
FUNCTION EXTERNAL_APPLY_DEAL_PERF(O_error_message                     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_deal_id                           IN       VARCHAR2,
                                  I_deal_detail_id                    IN       VARCHAR2,
                                  IO_amt_per_unit                     IN OUT   VARCHAR2,
                                  I_old_period_turnover               IN       VARCHAR2,
                                  I_new_period_turnover               IN       VARCHAR2,
                                  I_convert_amt_per_unit              IN       VARCHAR2,
                                  I_update_forecast_unit_amt          IN       VARCHAR2,
                                  I_update_actual_fixed_totals        IN       VARCHAR2,
                                  I_upd_deal_detail_actual_total      IN       VARCHAR2,
                                  I_update_budget_fixed_totals        IN       VARCHAR2,
                                  I_upd_deal_detail_budget_total      IN       VARCHAR2,
                                  I_update_total_baseline             IN       VARCHAR2,
                                  I_update_turnover_trend             IN       VARCHAR2,
                                  I_forecast_income_calc              IN       VARCHAR2,
                                  I_deal_to_date_calcs                IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: CONVERT_AMT_PER_UNIT
-- Purpose : This function will retrieve the deal component details and calculate the conversion amount per
--           unit, it will return the calculated value to the caller.
---------------------------------------------------------------------------------------------------------------
FUNCTION CONVERT_AMT_PER_UNIT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_amt_per_unit      IN OUT   DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE,
                              I_deal_id           IN       DEAL_DETAIL.DEAL_ID%TYPE,
                              I_deal_detail_id    IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_FORECAST_UNIT_AMT
-- Purpose : This function will update the total forecast revenue or total forecast units columns on the
--           DEAL_DETAIL table. The actual field to be updated is determined by the threshold_limit_type for
--           the deal, the calculation will use the total forecast valu from the table and the passed
--           amt per unit parameter.
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_FORECAST_UNIT_AMT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_deal_id           IN       DEAL_DETAIL.DEAL_ID%TYPE,
                                  I_deal_detail_id    IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                  I_amt_per_unit      IN       DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE)
RETURN BOOLEAN;

/* Below code is commented because it is not tested and not been called by any of the screen though it is called via batch and
   same logic is written in dealinclib.pc file.
----------------------------------------------------------------------------------------------------------------
-- Function: ACTUAL_INCOME_CALC
-- Purpose : This function will calculate income based upon the actuals turnover values from the
--           DEAL_ACTUALS_ITEM_LOC table. The calculation performed will be determined by the deal income
--           calculation type. The results of the calculations will be returned to the caller
--           NOTE: The function expects that the turnover values passed in will be in the
--           deal currency and hence it will not perform any currency coversion
----------------------------------------------------------------------------------------------------------------
FUNCTION ACTUAL_INCOME_CALC(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_actual_income            IN OUT   DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE,
                            I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                            I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                            I_threshold_limit_type     IN       DEAL_HEAD.THRESHOLD_LIMIT_TYPE%TYPE,
                            I_threshold_value_type     IN       DEAL_DETAIL.THRESHOLD_VALUE_TYPE%TYPE,
                            I_rebate_calc_type         IN       DEAL_HEAD.REBATE_CALC_TYPE%TYPE,
                            I_calc_to_zero_ind         IN       DEAL_DETAIL.CALC_TO_ZERO_IND%TYPE,
                            I_total_actual_fixed_ind   IN       DEAL_DETAIL.TOTAL_ACTUAL_FIXED_IND%TYPE,
                            I_deal_income_calculation  IN       DEAL_HEAD.DEAL_INCOME_CALCULATION%TYPE,
                            I_actual_turnover_units    IN       DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_UNITS%TYPE,
                            I_actual_turnover_revenue  IN       DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_REVENUE%TYPE,
                            I_act_for_turnover_total   IN       GTT_DEALINC_DEALS.ACT_FOR_TURNOVER_TOTAL%TYPE,
                            I_rebate_ind               IN       DEAL_HEAD.REBATE_IND%TYPE,
                            I_final_period_ind         IN       VARCHAR2,
                            I_reporting_date           IN       DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE,
                            I_order_no                 IN       DEAL_ACTUALS_ITEM_LOC.ORDER_NO%TYPE,
                            I_item                     IN       DEAL_ACTUALS_ITEM_LOC.ITEM%TYPE,
                            I_loc_type                 IN       DEAL_ACTUALS_ITEM_LOC.LOC_TYPE%TYPE,
                            I_location                 IN       DEAL_ACTUALS_ITEM_LOC.LOCATION%TYPE,
                            I_curr_forecast_turnover   IN       DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE)
RETURN BOOLEAN; */
----------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_ACTUAL_FIXED_TOTALS
-- Purpose : This recalculates and updates the forecast periods in response to a change made to the
--           actual/forecast value in the reporting period. It is called when the fixed total flag is set to
--           ensure forecasts are adjusted so that they remain in line with the total value. NOTE: If the
--           current actuals exceed the forecast total then all forecasts are set to zero and the total is
--           updated with the sum of the actuals regardless of the fixed indicator being set.
----------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ACTUAL_FIXED_TOTALS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                                    I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                    I_old_period_turnover      IN       DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE,
                                    I_new_period_turnover      IN       DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_DEAL_DETAIL_ACTUAL_TOTALS
-- Purpose : This recalculates the deal totals by summing up the forecast periods, it then updates the
--           DEAL_DETAIL row totals with the summed values.
----------------------------------------------------------------------------------------------------------------
FUNCTION UPD_DEAL_DETAIL_ACTUAL_TOTALS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                                       I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_BUDGET_FIXED_TOTALS
-- Purpose : This recalculates and updates the forecast periods in response to a change made to the
--           budget value in the reporting period. It is called when the fixed total flag is set to
--           ensure forecasts are adjusted so that they remain in line with the total value. NOTE: If the
--           current budget exceed the forecast total then all forecasts are set to zero and the total is
--           updated with the sum of the budgets regardless of the fixed indicator being set.
----------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_BUDGET_FIXED_TOTALS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                                    I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                    I_old_period_turnover      IN       DEAL_DETAIL.TOTAL_BUDGET_TURNOVER%TYPE,
                                    I_new_period_turnover      IN       DEAL_DETAIL.TOTAL_BUDGET_TURNOVER%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_DEAL_DETAIL_BUDGET_TOTALS
-- Purpose : This recalculates the deal totals by summing up the forecast periods, it then updates the
--           DEAL_DETAIL row totals with the summed values.
----------------------------------------------------------------------------------------------------------------
FUNCTION UPD_DEAL_DETAIL_BUDGET_TOTALS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                                       I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_TOTAL_BASELINE
-- Purpose : This recalculates the baseline growth % in response to a change made to the deal totals.
----------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_TOTAL_BASELINE(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                               I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_TURNOVER_TREND
-- Purpose : This recalculates the baseline growth % in response to a change made to the deal totals.
----------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_TURNOVER_TREND(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                               I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function: FORECAST_INCOME_CALC
-- Purpose : This recalculates the baseline growth % in response to a change made to the deal totals.
----------------------------------------------------------------------------------------------------------------
FUNCTION FORECAST_INCOME_CALC(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                              I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                              I_amt_per_unit             IN       DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function: DEAL_TO_DATE_CALCS
-- Purpose : This recalculates the deal to date budget growth rate, using the SUMs of the
--           actual turnover and budgeted turnover values for actuals only at both deal
--           and deal component level.
----------------------------------------------------------------------------------------------------------------
FUNCTION DEAL_TO_DATE_CALCS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function: CALCULATE_INCOME
-- Purpose : This recalculates the deal to date budget growth rate, using the SUMs of the
--           actual turnover and budgeted turnover values for actuals only at both deal
--           and deal component level.
----------------------------------------------------------------------------------------------------------------
FUNCTION CALCULATE_INCOME(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_income                   IN OUT   DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE,
                          I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                          I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                          I_threshold_limit_type     IN       DEAL_HEAD.THRESHOLD_LIMIT_TYPE%TYPE,
                          I_threshold_value_type     IN       DEAL_DETAIL.THRESHOLD_VALUE_TYPE%TYPE,
                          I_rebate_calc_type         IN       DEAL_HEAD.REBATE_CALC_TYPE%TYPE,
                          I_calc_to_zero_ind         IN       DEAL_DETAIL.CALC_TO_ZERO_IND%TYPE,
                          I_deal_income_calculation  IN       DEAL_HEAD.DEAL_INCOME_CALCULATION%TYPE,
                          I_rebate_ind               IN       DEAL_HEAD.REBATE_IND%TYPE,
                          I_period_threshold_value   IN       DEAL_THRESHOLD.UPPER_LIMIT%TYPE,
                          I_threshold_value          IN       DEAL_THRESHOLD.UPPER_LIMIT%TYPE,
                          I_amt_per_unit             IN       DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE,
                          I_weighted_amt_per_unit    IN       DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE)


RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
-- Function: GET_DEAL_THRESHOLDS
-- Purpose : 
----------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_THRESHOLDS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_thresholds           IN OUT   DEAL_THRESHOLD_TBL,
                             O_threshold_count      IN OUT   DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE,
                             I_deal_id              IN       DEAL_HEAD.DEAL_ID%TYPE,
                             I_deal_detail_id       IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------------------
END DEAL_INCOME_SQL;
/