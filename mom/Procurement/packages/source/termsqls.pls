CREATE OR REPLACE PACKAGE TERMS_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------

TYPE terms_detail_tbl IS TABLE OF TERMS_DETAIL%ROWTYPE;
TYPE payterm_rec      IS RECORD(terms_head_row   TERMS_HEAD%ROWTYPE,
                                terms_details    TERMS_DETAIL_TBL,
                                terms_code       TERMS_HEAD_TL.TERMS_CODE%TYPE,
                                terms_desc       TERMS_HEAD_TL.TERMS_DESC%TYPE);

--------------------------------------------------------------------------------
-- Function    : HEADER_EXISTS
-- Purpose     : Checks for the existance of terms_head record before inserting
--               updating terms_detail table.
--------------------------------------------------------------------------------
FUNCTION HEADER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_terms           IN       TERMS_HEAD.TERMS%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function    : DETAIL_EXISTS
-- Purpose     : Checks for the existance of terms_head and terms_detail before
--               updating terms_detail table.
--------------------------------------------------------------------------------
FUNCTION DETAIL_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_terms           IN       TERMS_HEAD.TERMS%TYPE,
                       I_terms_seq       IN       TERMS_DETAIL.TERMS_SEQ%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION MERGE_HEADER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_paytermrec    IN     PAYTERM_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION MERGE_DETAIL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_paytermrec    IN     PAYTERM_REC)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION GET_NEXT_TERMS_VAL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_terms          IN OUT  TERMS_HEAD.TERMS%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
END TERMS_SQL;

/
