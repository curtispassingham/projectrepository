
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORDREV_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------
-- Function Name:  get_old_new_dates
-- Purpose:        Returns the old and new date information from
--                 ordhead_rev for an order that is being revised.
--------------------------------------------------------------------------
FUNCTION GET_OLD_NEW_DATES(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                           I_rev_no           IN       ORDHEAD_REV.REV_NO%TYPE,
                           I_rev_type         IN       ORDLOC_REV.ORIGIN_TYPE%TYPE,
                           O_old_not_before   IN OUT   ORDHEAD.NOT_BEFORE_DATE%TYPE,
                           O_old_not_after    IN OUT   ORDHEAD.NOT_AFTER_DATE%TYPE,
                           O_old_pickup       IN OUT   ORDHEAD.PICKUP_DATE%TYPE,
                           O_new_not_before   IN OUT   ORDHEAD.NOT_BEFORE_DATE%TYPE,
                           O_new_not_after    IN OUT   ORDHEAD.NOT_AFTER_DATE%TYPE,
                           O_new_pickup       IN OUT   ORDHEAD.PICKUP_DATE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name:  qty_change
-- Purpose:        Performs logic necessary to accept an order revision
--                 with a quantity change.
--------------------------------------------------------------------------
FUNCTION QTY_CHANGE(O_error_message IN OUT VARCHAR2,
                    I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
                    I_rev_no        IN     ORDLOC_REV.REV_NO%TYPE,
                    I_rev_type      IN     ORDLOC_REV.ORIGIN_TYPE%TYPE,
                    I_item          IN     ORDLOC.ITEM%TYPE,
                    I_location      IN     ORDLOC.LOCATION%TYPE,
                    I_loc_type      IN     ORDLOC.LOC_TYPE%TYPE,
                    I_old_qty       IN     ORDLOC.QTY_RECEIVED%TYPE,
                    I_new_qty       IN     ORDLOC.QTY_RECEIVED%TYPE,
                    I_cancel_id     IN     ORDLOC.CANCEL_ID%TYPE)
                    RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name:  cost_change
-- Purpose:        Performs logic necessary to accept an order revision
--                 with a cost change.
--------------------------------------------------------------------------
FUNCTION COST_CHANGE(O_error_message IN OUT VARCHAR2,
                     I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
                     I_rev_no        IN     ORDLOC_REV.REV_NO%TYPE,
                     I_rev_type      IN     ORDLOC_REV.ORIGIN_TYPE%TYPE,
                     I_item          IN     ORDLOC.ITEM%TYPE,
                     I_location      IN     ORDLOC.LOCATION%TYPE,
                     I_loc_type      IN     ORDLOC.LOC_TYPE%TYPE,
                     I_cost_source   IN     ORDLOC.COST_SOURCE%TYPE,
                     I_new_cost      IN     ORDLOC.UNIT_COST%TYPE)
                     RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name:  date_change
-- Purpose:        Performs logic necessary to accept an order revision
--                 with date change(s).
--------------------------------------------------------------------------
FUNCTION DATE_CHANGE(O_error_message  IN OUT VARCHAR2,
                     I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                     I_old_not_before IN     ORDHEAD.NOT_BEFORE_DATE%TYPE,
                     I_old_not_after  IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                     I_old_pickup     IN     ORDHEAD.PICKUP_DATE%TYPE,
                     I_not_before     IN     ORDHEAD.NOT_BEFORE_DATE%TYPE,
                     I_not_after      IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                     I_pickup         IN     ORDHEAD.PICKUP_DATE%TYPE)
                     RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name:  chk_ord_close_method
-- Purpose:        Checks whether an EDI update should be sent for a closed PO.
--                 True if a revision has been sent to the vendor,
--                 and all the PO quantities have been set to 0 since
--                 the last time a revision was sent.
--------------------------------------------------------------------------
FUNCTION CHK_ORD_CLOSE_METHOD (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_edi_to_send_flag  OUT    BOOLEAN,
                               I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
                               RETURN BOOLEAN;
--------------------------------------------------------------------------
END;
/
