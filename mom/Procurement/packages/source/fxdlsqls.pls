CREATE OR REPLACE PACKAGE FIXED_DEAL_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------
-- Name:       CHECK_DATES
-- Purpose:    Check the before and after dates from the fixdeal form.  Used
--             in the dynamic where clause of the form.
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION CHECK_DATES (I_deal_number   IN   NUMBER,
                      I_before_date   IN   DATE,
                      I_after_date    IN   DATE)
RETURN NUMBER;

PRAGMA RESTRICT_REFERENCES(check_dates,WNDS,WNPS);

-----------------------------------------------------------------------------
-- Name:       NEXT_DEAL_NUMBER
-- Purpose:    This deal number sequence generator will return the next deal
--             number to the calling program/procedure.
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION NEXT_DEAL_NUMBER( O_error_message   IN OUT   VARCHAR2,
                           O_deal_number     IN OUT   NUMBER)
RETURN BOOLEAN;
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Name:       GET_DEAL_DESC
-- Purpose:    This function will return a deal description for a passed in
--             deal number.
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION GET_DEAL_DESC(O_error_message   IN OUT   VARCHAR2,
                       O_deal_desc       IN OUT   FIXED_DEAL.DEAL_DESC%TYPE,
                       I_deal_no         IN       FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Name:       CREATE_SCHEDULE
-- Purpose:    This function will populate the fixed_deal_dates table based
--             on the input parameters
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION CREATE_SCHEDULE(O_error_message   IN OUT   VARCHAR2,
                         I_deal_no         IN       FIXED_DEAL.DEAL_NO%TYPE,
                         I_start_date      IN       FIXED_DEAL.COLLECT_START_DATE%TYPE,
                         I_periods         IN       FIXED_DEAL.COLLECT_PERIODS%TYPE,
                         I_collect_by      IN       FIXED_DEAL.COLLECT_BY%TYPE,
                         I_deal_amt        IN       FIXED_DEAL.FIXED_DEAL_AMT%TYPE,
                         I_currency_code   IN       FIXED_DEAL.CURRENCY_CODE%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Name:       CREATE_MERCH_LOC
-- Purpose:    This function will populate the fixed_deal_merch_loc table based
--             on the input parameters
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION CREATE_MERCH_LOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   VARCHAR2,
                           I_group_type      IN       FIXED_DEAL_MERCH_LOC.LOC_TYPE%TYPE,
                           I_group_value     IN       VARCHAR2,
                           I_deal_no         IN       FIXED_DEAL_MERCH_LOC.DEAL_NO%TYPE,
                           I_seq_no          IN       FIXED_DEAL_MERCH_LOC.SEQ_NO%TYPE,
                           I_contrib_ratio   IN       FIXED_DEAL_MERCH_LOC.CONTRIB_RATIO%TYPE,
                           I_partner         IN       FIXED_DEAL.PARTNER_ID%TYPE,
                           I_partner_type    IN       FIXED_DEAL.PARTNER_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Name:       MERCH_LOC_EXISTS
-- Purpose:    This function will check if records exist in fixed_deal_merch_loc
--             table based on the input parameters
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION MERCH_LOC_EXISTS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           I_deal_no         IN       FIXED_DEAL_MERCH_LOC.DEAL_NO%TYPE,
                           I_seq_no          IN       FIXED_DEAL_MERCH_LOC.SEQ_NO%TYPE,
                           I_location        IN       FIXED_DEAL_MERCH_LOC.LOCATION%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Name:       MERCH_LOC_EXISTS
-- Purpose:    This function will delete records from fixed_deal_merch_loc and
--             fixed_deal_merch table based on the input parameters, once it has
--             obtained lock on these tables
-- Created By: Retek
-----------------------------------------------------------------------------
FUNCTION DELETE_RECORDS  (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_deal_no         IN       FIXED_DEAL_MERCH_LOC.DEAL_NO%TYPE,
                          I_seq_no          IN       FIXED_DEAL_MERCH_LOC.SEQ_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Name:       GET_SEQ_NO
-- Purpose:    This function will retrieve the next available sequence number
--             from FIXED_DEAL_MERCH_ID_SEQ
-----------------------------------------------------------------------------
FUNCTION GET_SEQ_NO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_seq_id          IN OUT   FIXED_DEAL_MERCH.SEQ_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Name:       MERCH_DEAL_EXISTS
-- Purpose:    This function will determine if a fixed_deal_merch record exists
--             and if it does returns the merch level for this deal.
-----------------------------------------------------------------------------
FUNCTION MERCH_DEAL_EXISTS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   BOOLEAN,
                            I_deal_no         IN       FIXED_DEAL_MERCH.DEAL_NO%TYPE,
                            I_dept            IN       FIXED_DEAL_MERCH.DEPT%TYPE,
                            I_class           IN       FIXED_DEAL_MERCH.CLASS%TYPE,
                            I_subclass        IN       FIXED_DEAL_MERCH.SUBCLASS%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name:       DELETE_DEAL_DATES
-- Purpose:    This function deletes records from FIXED_DEAL_DATES tables based
--             on the passed fixed deal no.
-----------------------------------------------------------------------------
FUNCTION DELETE_DEAL_DATES (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_deal_no         IN       FIXED_DEAL_MERCH.DEAL_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name:       CHECK_DEAL_EXTRACTED
-- Purpose:    This function checks whether the passed fixed deal no has been extracted for
--             any of the collecting periods.
-----------------------------------------------------------------------------
FUNCTION CHECK_DEAL_EXTRACTED (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   BOOLEAN,
                               I_deal_no         IN       FIXED_DEAL_DATES.DEAL_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION SET_EXT_IND_INACT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_deal_no         IN       FIXED_DEAL_DATES.DEAL_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name:       CHECK_SAME_VAT_REGION
-- Purpose:    This function checks whether the supplier's vat region is the same with
--             the merch location's vat_region. If not the same, VAT is set to zero.
------------------------------------------------------------------------------------
FUNCTION CHECK_SAME_VAT_REGION(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_same_vat_region   IN OUT   VARCHAR2,
                               I_deal_no           IN       FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Name:       CHECK_VAT_REGION_COUNT
-- Purpose:    This function checks if the vat_region for each location is the same.
--             If the count is greater than one then user needs to ensure that only
--             one vat region exists for all the fixed deal's location.
------------------------------------------------------------------------------------
FUNCTION CHECK_VAT_REGION_COUNT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_vat_region_count   IN OUT   VARCHAR2,
                                I_deal_no            IN       FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
END FIXED_DEAL_SQL;
/
