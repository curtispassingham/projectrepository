
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORD_INV_MGMT_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------
-- POP_ORDIVMGT
-- This function is called from the ordivmgt form to populate non-base
-- table items in the Order Inventory Management Form (ordivmgt.fmb)
---------------------------------------------------------------------------------------
FUNCTION POP_ORDIVMGT(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_qty_prescaled1      IN OUT NUMBER,
                      O_qty_ordered1        IN OUT NUMBER,
                      O_qty_prescaled2      IN OUT NUMBER,
                      O_qty_ordered2        IN OUT NUMBER,
                      O_qty_prescaled3      IN OUT NUMBER,
                      O_qty_ordered3        IN OUT NUMBER,
                      O_qty_prescaled4      IN OUT NUMBER,
                      O_qty_ordered4        IN OUT NUMBER,
                      O_qty_prescaled5      IN OUT NUMBER,
                      O_qty_ordered5        IN OUT NUMBER,
                      O_qty_prescaled6      IN OUT NUMBER,
                      O_qty_ordered6        IN OUT NUMBER,
                      O_supp_prescale_cost  IN OUT NUMBER,
                      O_supp_order_cost     IN OUT NUMBER,
                      O_prescaled_cost      IN OUT NUMBER,
                      O_cost                IN OUT NUMBER,
                      O_eso                 IN OUT NUMBER,
                      O_aso                 IN OUT NUMBER,
                      O_item_locs_total     IN OUT NUMBER,
                      O_item_locs_due       IN OUT NUMBER,
                      I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                      I_supplier            IN     ORDHEAD.SUPPLIER%TYPE,
                      I_item                IN     ITEM_MASTER.ITEM%TYPE,
                      I_location            IN     ORDLOC.LOCATION%TYPE,
                      I_due_ord_process_ind IN     ORD_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE,
                      I_cnstr_type1         IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                      I_cnstr_type2         IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                      I_cnstr_type3         IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                      I_cnstr_type4         IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                      I_cnstr_type5         IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                      I_cnstr_type6         IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                      I_cnstr_uom1          IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                      I_cnstr_uom2          IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                      I_cnstr_uom3          IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                      I_cnstr_uom4          IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                      I_cnstr_uom5          IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                      I_cnstr_uom6          IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name:  CNSTR_ORD_QTYS
-- Description:    Used to retrieve the total order and pre-scaled order quantities
-- for a specificed constraint type.  
---------------------------------------------------------------------------------------
FUNCTION CNSTR_ORD_QTYS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_qty_prescaled IN OUT NUMBER,
                        O_qty_ordered   IN OUT NUMBER,
                        I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                        I_supplier      IN     ORDHEAD.SUPPLIER%TYPE,
                        I_item          IN     ITEM_MASTER.ITEM%TYPE,
                        I_location      IN     ORDLOC.LOCATION%TYPE,
                        I_cnstr_type    IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                        I_cnstr_uom     IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name:  GET_SINGLE_LOC_IND
-- Description:    Retrieves the single location indicator from ord_inv_mgmt for an order.
---------------------------------------------------------------------------------------
FUNCTION GET_SINGLE_LOC_IND(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_single_loc_ind   IN OUT   ORD_INV_MGMT.SINGLE_LOC_IND%TYPE,              
                            I_order_no         IN       ORD_INV_MGMT.ORDER_NO%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name:  FILE_ID_EXISTS
-- Description:    Checks if the entered file ID is valid.
---------------------------------------------------------------------------------------
FUNCTION FILE_ID_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists           IN OUT   BOOLEAN,
                        I_file_id          IN       ORD_INV_MGMT.FILE_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name:  GET_POOL_SUPP_FILE_ID
-- Description:    Retrieves the pool supplier and file id from ORD_INV_MGMT for the 
--                 specified order.
---------------------------------------------------------------------------------------
FUNCTION GET_POOL_SUPP_FILE_ID(O_error_message   IN OUT   VARCHAR2,
                               O_pool_supplier   IN OUT   ORD_INV_MGMT.POOL_SUPPLIER%TYPE,
                               O_file_id         IN OUT   ORD_INV_MGMT.FILE_ID%TYPE,
                               I_order_no        IN       ORD_INV_MGMT.ORDER_NO%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name:  GET_TRUCK_SPLIT_IND
-- Description:    This function will check if the specified order is valid to be split.
---------------------------------------------------------------------------------------
FUNCTION GET_TRUCK_SPLIT_IND(O_error_message     IN OUT   VARCHAR2,
                             O_truck_split_ind   IN OUT   ORD_INV_MGMT.TRUCK_SPLIT_IND%TYPE,
                             I_order_no          IN       ORD_INV_MGMT.ORDER_NO%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: AMOUNT_CONSTRAINT_EXISTS
-- Description:  This function will check whether ord_inv_mgmt records exists.
---------------------------------------------------------------------------------------
FUNCTION AMOUNT_CONSTRAINT_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists         IN OUT VARCHAR2,
                                  I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN ;
---------------------------------------------------------------------------------------
FUNCTION DELETE_ORD_INV_MGMT(O_error_message    IN OUT      VARCHAR2,
                             I_order_no         IN          ORD_INV_MGMT.order_no%TYPE)

RETURN BOOLEAN;
---------------------------------------------------------------------------------------
END ORD_INV_MGMT_SQL;
/
