CREATE OR REPLACE PACKAGE BODY RMSSUB_XCOSTCHG_VALIDATE AS

   LP_no_loc         BOOLEAN;
   LP_multi_loc      BOOLEAN;
   LP_single_loc     BOOLEAN;

   LP_im_rec         ITEM_ATTRIB_SQL.API_ITEM_REC;
   LP_diff_column    VARCHAR2(30) := NULL;

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This private function will check all required fields to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: ORGHIER_EXISTS
   -- Purpose      : This private function will check all hier_values to ensure that they are valid.
-------------------------------------------------------------------------------------------------------
FUNCTION ORGHIER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_message         IN       "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_CURRENCY_CODE
   -- Purpose      : This private function will check the currency code for the supplier in the message.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CURRENCY_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_message         IN       "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_STATUS
   -- Purpose      : This private function checks the status of the item in the message.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_status          IN       ITEM_MASTER.STATUS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_PACK_TYPE
   -- Purpose      : This private function checks the pack type.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_PACK_TYPE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_pack_type       IN       ITEM_MASTER.PACK_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_ISC
   -- Purpose      : This private function will verify that the item/supplier/country on the message
   --                exists in the item_supp_country table.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ISC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_item            IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                   I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                   I_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: DIFF_ITEM_EXIST
   -- Purpose      : This private function validates that child items exists for the diff value in the
   --                message.
-------------------------------------------------------------------------------------------------------
FUNCTION DIFF_ITEMS_EXIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item              IN       ITEM_MASTER.ITEM%TYPE,
                          I_diff_id           IN       DIFF_IDS.DIFF_ID%TYPE,
                          I_parent_item_ind   IN       BOOLEAN)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: VALIDATE_ITEMS
   -- Purpose      : This private function validates that all items exist in a single location or at least one
   --                item exists in all locations.
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_message         IN       "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This private function populates the fields, tables and records in COST_CHANGE_RECTYPE
   --                record with values from the RIB message and from dynamic queries.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_costchg_rec     OUT    NOCOPY   RMSSUB_XCOSTCHG.COST_CHANGE_RECTYPE,
                         I_message         IN              "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_ISCL_TBL
   -- Purpose      : This private function should populate the rowids in the iscl_rows_tbl node of the
   --                COST_CHANGE_RECTYPE record.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ISCL_TBL(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                           O_iscl_rows       OUT    NOCOPY   RMSSUB_XCOSTCHG.ROWID_TBL,
                           O_iscl_items      OUT    NOCOPY   ITEM_TBL,
                           O_iscl_locs       OUT    NOCOPY   LOC_TBL,
                           I_items           IN              ITEM_TBL,
                           I_locs            IN              LOC_TBL,
                           I_supplier        IN              SUPS.SUPPLIER%TYPE,
                           I_country         IN              COUNTRY.COUNTRY_ID%TYPE)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_ISC_TBL
   -- Purpose      : This private function should populate the isc_rows_tbl node of the
   --                COST_CHANGE_RECTYPE record.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ISC_TBL(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                          O_isc_rows        OUT    NOCOPY   RMSSUB_XCOSTCHG.ROWID_TBL,
                          I_items           IN              ITEM_TBL,
                          I_locs            IN              LOC_TBL,
                          I_supplier        IN              SUPS.SUPPLIER%TYPE,
                          I_country         IN              COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_PRICE_HIST
   -- Purpose      : This private function should populate  the item_tbl, loc_tbl, the unit_retail_tbl
   --                and unit_cost_tbl in the price_hist_rec node of the COST_CHANGE_RECTYPE record.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_PRICE_HIST(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                             O_price_hist_rec      OUT    NOCOPY   RMSSUB_XCOSTCHG.PRICE_HIST_RECTYPE,
                             O_soh_items           OUT    NOCOPY   ITEM_TBL,
                             I_items               IN              ITEM_TBL,
                             I_tran_items          IN              ITEM_TBL,
                             I_locs                IN              LOC_TBL,
                             I_supplier            IN              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                             I_origin_country_id   IN              ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_unit_cost           IN              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                             I_currency_code       IN              STORE.CURRENCY_CODE%TYPE,
                             I_hier_level          IN              VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_PACKS_TBL
   -- Purpose      : This private function populate the packs_tbl node of the COST_CHANGE_RECTYPE record.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_PACKS_TBL(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                            O_packs_tbl           OUT    NOCOPY   ITEM_TBL,
                            I_items               IN              ITEM_TBL,
                            I_supplier            IN              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                            I_origin_country_id   IN              ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_PACKS_ISC_TBL
   -- Purpose      : This private function should populate the O_isc_tbl.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_PACKS_ISC_TBL(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                                O_isc_tbl             OUT    NOCOPY   RMSSUB_XCOSTCHG.ROWID_TBL,
                                I_packs               IN              ITEM_TBL,
                                I_locs                IN              LOC_TBL,
                                I_supplier            IN              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                I_origin_country_id   IN              ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_PACKS_PRICE_HIST
   -- Purpose      : This private function populates the item_tbl, loc_tbl, the unit_retail_tbl, and the
   --                unit_cost_tbl in a price_hist_rec for packs.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_PACKS_PRICE_HIST(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_price_hist_rec      OUT    NOCOPY   RMSSUB_XCOSTCHG.PRICE_HIST_RECTYPE,
                                   I_packs               IN              ITEM_TBL,
                                   I_locs                IN              LOC_TBL,
                                   I_hier_level          IN              VARCHAR2,
                                   I_supplier            IN              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                   I_origin_country_id   IN              ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                   I_unit_cost           IN              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                   I_currency_code       IN              STORE.CURRENCY_CODE%TYPE)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: GET_ALL_LOCS
   -- Purpose      : This private function should retrieve the location subquery and populate local
   --                instance of loc_tbl with the hier values from the message.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ALL_LOCS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                      O_locs            OUT    NOCOPY   LOC_TBL,
                      I_hier_level      IN              VARCHAR2,
                      I_hier_value      IN              "RIB_XCostChgHrDtl_TBL")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ALL_ITEMS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_items           OUT    NOCOPY   ITEM_TBL,
                       O_tran_items      OUT    NOCOPY   ITEM_TBL,
                       I_item            IN              ITEM_MASTER.ITEM%TYPE,
                       I_diff_id         IN              DIFF_IDS.DIFF_ID%TYPE,
                       I_include_parent  IN              BOOLEAN)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_costchg_rec     OUT    NOCOPY   RMSSUB_XCOSTCHG.COST_CHANGE_RECTYPE,
                       I_message         IN              "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50)  := 'RMSSUB_XCOSTCHG_VALIDATE.CHECK_MESSAGE';
   L_exists        BOOLEAN       := FALSE;
   L_parent_item   BOOLEAN       := FALSE;

BEGIN

   LP_im_rec      := NULL; --ITEM_ATTRIB_SQL.API_ITEM_REC ;
   LP_diff_column := NULL;

   LP_no_loc      := FALSE;
   LP_multi_loc   := FALSE;
   LP_single_loc  := FALSE;

   if NOT CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;
   ---

   if NOT ORGHIER_EXISTS(O_error_message,
                         I_message) then
      return FALSE;
   end if;
   ---

   if NOT CHECK_CURRENCY_CODE(O_error_message,
                              I_message) then
      return FALSE;
   end if;
   ---

   if NOT ITEM_ATTRIB_SQL.GET_API_INFO(O_error_message,
                                       LP_im_rec,
                                       I_message.item) then
      return FALSE;
   end if;
   ---

   if NOT CHECK_STATUS(O_error_message,
                       LP_im_rec.status) then
      return FALSE;
   end if;
   ---

   if LP_im_rec.pack_ind = 'Y' then
      if NOT CHECK_PACK_TYPE(O_error_message,
                             LP_im_rec.pack_type) then
         return FALSE;
      end if;
   end if;
   ---

   if LP_im_rec.item_level < LP_im_rec.tran_level then
      L_parent_item := TRUE;
   end if;

   if I_message.diff_id IS NOT NULL then
      if NOT DIFF_ITEMS_EXIST(O_error_message,
                              I_message.item,
                              I_message.diff_id,
                              L_parent_item) then
         return FALSE;
      end if;
   end if;
   ---

   if L_parent_item = FALSE and
      I_message.hier_level IN ('S', 'W') and
      I_message.xcostchghrdtl_tbl IS NOT NULL and
      I_message.xcostchghrdtl_tbl.COUNT > 0 then
      if VALIDATE_ITEMS(O_error_message,
                        I_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---

   if NOT POPULATE_RECORD(O_error_message,
                          O_costchg_rec,
                          I_message) then
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION BUILD_PACK_PRICE_RECORDS(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_price_hist_rec      OUT    NOCOPY   RMSSUB_XCOSTCHG.PRICE_HIST_RECTYPE,
                                  O_isc_rows            OUT    NOCOPY   RMSSUB_XCOSTCHG.ROWID_TBL,
                                  I_packs               IN              ITEM_TBL,
                                  I_hier_locs           IN              LOC_TBL,
                                  I_hier_level          IN              VARCHAR2,
                                  I_supplier            IN              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                  I_origin_country_id   IN              ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                  I_unit_cost           IN              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                  I_currency_code       IN              STORE.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XCOSTCHG_VALIDATE.BUILD_PACK_PRICE_RECORDS';

   TAB_packs      ITEM_TBL := ITEM_TBL();
   
   cursor C_FILTER_DUP_PACKS is
      select distinct value(xpack)
        from TABLE(CAST (I_packs AS ITEM_TBL)) xpack;

BEGIN

   open C_FILTER_DUP_PACKS;
   fetch C_FILTER_DUP_PACKS BULK COLLECT into TAB_packs;
   close C_FILTER_DUP_PACKS;

   if NOT POPULATE_PACKS_PRICE_HIST(O_error_message,
                                    O_price_hist_rec,
                                    TAB_packs,
                                    I_hier_locs,
                                    I_hier_level,
                                    I_supplier,
                                    I_origin_country_id,
                                    I_unit_cost,
                                    I_currency_code) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END BUILD_PACK_PRICE_RECORDS;
---------------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XCOSTCHG_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.unit_cost IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Unit Cost', NULL, NULL);
      return FALSE;
   end if;

   if I_message.origin_country_id IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Origin country ID', NULL, NULL);
      return FALSE;
   end if;

   if I_message.currency_code IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Currency Code', NULL, NULL);
      return FALSE;
   end if;

   if I_message.recalc_ord_ind IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Recalc Ord Ind', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
---------------------------------------------------------------------------------------------------------
FUNCTION ORGHIER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_message         IN       "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'RMSSUB_XCOSTCHG_VALIDATE.ORGHIER_EXISTS';

   TAB_hier_locs   LOC_TBL      := LOC_TBL(NULL);

   L_exists        BOOLEAN      := FALSE;
   L_store_exists  BOOLEAN      := FALSE;
   L_wh_exists     BOOLEAN      := FALSE;
   L_type          NUMBER;

BEGIN

   if I_message.hier_level is NOT NULL then
      if I_message.hier_level NOT IN (ORGANIZATION_SQL.LP_chain,
                                      ORGANIZATION_SQL.LP_area,
                                      ORGANIZATION_SQL.LP_region,
                                      ORGANIZATION_SQL.LP_district,
                                      ORGANIZATION_SQL.LP_store,
                                      'W') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE', 'Hier Level', I_message.hier_level, NULL);
         return FALSE;
      end if;
      
      if I_message.xcostchghrdtl_tbl IS NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   if I_message.xcostchghrdtl_tbl IS NOT NULL and I_message.xcostchghrdtl_tbl.COUNT > 0 then
      if I_message.hier_level IS NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Hier Level', NULL, NULL);
         return FALSE;
      end if;

      -- Populate local instance of loc_tbl
      FOR i IN I_message.xcostchghrdtl_tbl.FIRST..I_message.xcostchghrdtl_tbl.LAST LOOP
         -- This will remove the extra NULL row when populating the table
         if i > I_message.xcostchghrdtl_tbl.FIRST then
            TAB_hier_locs.EXTEND();
         end if;
         --
         TAB_hier_locs(i) := I_message.xcostchghrdtl_tbl(i).hier_value;
      END LOOP;

      if I_message.hier_level = ORGANIZATION_SQL.LP_chain then
         L_type := 10;
      elsif I_message.hier_level = ORGANIZATION_SQL.LP_area then
         L_type := 20;
      elsif I_message.hier_level = ORGANIZATION_SQL.LP_region then
         L_type := 30;
      elsif I_message.hier_level = ORGANIZATION_SQL.LP_district then
         L_type := 40;
      end if;

      FOR j IN TAB_hier_locs.FIRST..TAB_hier_locs.LAST LOOP
         if I_message.hier_level NOT IN ('S', 'W') then
            if NOT ORGANIZATION_VALIDATE_SQL.EXIST(O_error_message,
                                                   L_type,
                                                   TAB_hier_locs(j),
                                                   L_exists) then
               return FALSE;
            end if;

            if NOT L_exists then
               O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE',
                                                     'Hier Value',
                                                     TAB_hier_locs(j),
                                                     NULL);
               return FALSE;
            end if;

         elsif I_message.hier_level = 'S' then
            if NOT STORE_VALIDATE_SQL.EXIST(O_error_message,
                                            TAB_hier_locs(j),
                                            L_store_exists) then
               return FALSE;
            end if;

            if NOT L_store_exists then
               O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE',
                                                     'Hier Value',
                                                     TAB_hier_locs(j),
                                                     NULL);
               return FALSE;
            end if;

         elsif I_message.hier_level = 'W' then
            if NOT WAREHOUSE_VALIDATE_SQL.EXIST(O_error_message,
                                                TAB_hier_locs(j),
                                                L_wh_exists) then
               return FALSE;
            end if;

            if NOT L_wh_exists then
               O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE',
                                                     'Hier Value',
                                                     TAB_hier_locs(j),
                                                     NULL);
               return FALSE;
            end if;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END ORGHIER_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CURRENCY_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_message         IN       "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)            := 'RMSSUB_XCOSTCHG_VALIDATE.CHECK_CURRENCY_CODE';
   L_currency_code   SUPS.CURRENCY_CODE%TYPE := NULL;

BEGIN

   if NOT SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                            L_currency_code,
                                            I_message.supplier) then
      return FALSE;
   end if;
   ---
   if L_currency_code IS NULL OR L_currency_code != I_message.currency_code then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CURR_CODE', NULL, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_CURRENCY_CODE;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_status          IN       ITEM_MASTER.STATUS%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)            := 'RMSSUB_XCOSTCHG_VALIDATE.CHECK_STATUS';

BEGIN

   if I_status != 'A' then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_STATUS', NULL, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_STATUS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_PACK_TYPE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_pack_type       IN       ITEM_MASTER.PACK_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)  := 'RMSSUB_XCOSTCHG_VALIDATE.CHECK_PACK_TYPE';

BEGIN

   if I_pack_type = 'B' then
      O_error_message := SQL_LIB.CREATE_MSG('CNT_CHANGE_COST', NULL, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_PACK_TYPE;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ISC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_item            IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                   I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                   I_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)

   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)  := 'RMSSUB_XCOSTCHG_VALIDATE.CHECK_ISC';
   L_exist           BOOLEAN       := FALSE;

BEGIN

   if NOT SUPP_ITEM_SQL.ITEM_SUPP_COUNTRY_EXISTS(O_error_message,
                                                 L_exist,
                                                 I_item,
                                                 I_supplier,
                                                 I_country) then
      return FALSE;
   end if;

   if NOT L_exist then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MESSAGE', 'Item, supplier and country combination', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_ISC;
---------------------------------------------------------------------------------------------------------
FUNCTION DIFF_ITEMS_EXIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item              IN       ITEM_MASTER.ITEM%TYPE,
                          I_diff_id           IN       DIFF_IDS.DIFF_ID%TYPE,
                          I_parent_item_ind   IN       BOOLEAN)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'RMSSUB_XCOSTCHG_VALIDATE.DIFF_ITEMS_EXIST';

   L_exist          BOOLEAN  := FALSE;
   

BEGIN

   if I_parent_item_ind then
      if NOT ITEM_ATTRIB_SQL.ITEM_DIFF_EXISTS(O_error_message,
                                              L_exist,
                                              LP_diff_column,
                                              I_item,
                                              I_diff_id) then
         return FALSE;
      end if;

      if NOT L_exist then
         O_error_message := SQL_LIB.CREATE_MSG('STYLE_COLOR_NOT_EXIST', NULL, NULL, NULL);
         return FALSE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('NO_DIFF_BELOW_ITEM1',I_item, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END DIFF_ITEMS_EXIST;
---------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_message         IN       "RIB_XCostChgDesc_REC")

   RETURN BOOLEAN IS

   L_program          VARCHAR2(50)   := 'RMSSUB_XCOSTCHG_VALIDATE.VALIDATE_ITEMS';
   TAB_hier_locs      LOC_TBL        := LOC_TBL(NULL);
   L_exists           BOOLEAN        := FALSE;

BEGIN

   -- Populate local instance of loc_tbl
   FOR i IN I_message.xcostchghrdtl_tbl.FIRST..I_message.xcostchghrdtl_tbl.LAST LOOP
      -- This will remove the extra NULL row when populating the table
      if i > I_message.xcostchghrdtl_tbl.FIRST then
         TAB_hier_locs.EXTEND();
      end if;
      ---
      TAB_hier_locs(i) := I_message.xcostchghrdtl_tbl(i).hier_value;
   END LOOP;

   -- Validation for single store OR wh to ensure that the location
   -- passed contain the item to be updated
   if TAB_hier_locs.COUNT > 0 then
      FOR i IN TAB_hier_locs.FIRST..TAB_hier_locs.LAST LOOP
         if NOT ITEM_SUPP_COUNTRY_LOC_SQL.LOC_EXISTS(O_error_message,
                                                     L_exists,
                                                     I_message.item,
                                                     I_message.supplier,
                                                     I_message.origin_country_id,
                                                     TAB_hier_locs(i)) then
            return FALSE;
         end if;

         if L_exists = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('LOC_MUST_EXIST_FOR_ITEM',
                                                  TAB_hier_locs(i),
                                                  I_message.item,
                                                  NULL);
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END VALIDATE_ITEMS;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_costchg_rec     OUT    NOCOPY   RMSSUB_XCOSTCHG.COST_CHANGE_RECTYPE,
                         I_message         IN              "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)   := 'RMSSUB_XCOSTCHG_VALIDATE.POPULATE_RECORD';

   TAB_locs          LOC_TBL        := LOC_TBL();
   TAB_items         ITEM_TBL       := ITEM_TBL();
   TAB_tran_items    ITEM_TBL       := ITEM_TBL();

   L_item_bind_no    VARCHAR2(1)    := '1';
   L_diff_bind_no    VARCHAR2(1)    := '2';
   L_table_bind_no   VARCHAR2(1)    := '3';

   L_main_tab_item   VARCHAR2(50)   := NULL;
   L_main_query_tab  VARCHAR2(50)   := 'iscl.loc';

   L_loc_subquery    VARCHAR2(1000) := NULL;
   L_item_subquery   VARCHAR2(2000);
   L_store_subquery  VARCHAR2(2000);

   L_include_parent  BOOLEAN;
   

BEGIN

   O_costchg_rec.items_no_locs := NULL;

   -- Initialize global variables
   LP_no_loc     := FALSE;
   LP_single_loc := FALSE;
   LP_multi_loc  := FALSE;   

   -- Process Locations
   if I_message.xcostchghrdtl_tbl IS NULL OR I_message.xcostchghrdtl_tbl.COUNT = 0 then
      LP_no_loc := TRUE;
   else
      if NOT GET_ALL_LOCS(O_error_message,
                          TAB_locs,          -- This contains all stores or wh
                          I_message.hier_level,
                          I_message.xcostchghrdtl_tbl) then
        return FALSE;
      end if;

      if TAB_locs.COUNT = 1 then
         ---
         LP_single_loc := TRUE;
      else   --this includes single location from store level with diff_id
         LP_multi_loc := TRUE;
      end if;
   end if;

   -- Process Items
   if LP_im_rec.item_level < LP_im_rec.tran_level then
      L_include_parent := TRUE;
      O_costchg_rec.parent_item_ind := 'Y';
   else
      L_include_parent := FALSE;
      O_costchg_rec.parent_item_ind := 'N';
   end if;
   ---

   if NOT GET_ALL_ITEMS(O_error_message,
                        TAB_items,      -- used to update isc/iscl etc
                        TAB_tran_items, -- used to popluate price hist
                        I_message.item,
                        I_message.diff_id,
                        L_include_parent) then
      return FALSE;
   end if;
   

   -- Main Processing
   if LP_no_loc then
      if NOT CHECK_ISC(O_error_message,
                       I_message.item,
                       I_message.supplier,
                       I_message.origin_country_id) then
         return FALSE;
      end if;

      O_costchg_rec.single_itemloc_ind := 'N';
      O_costchg_rec.items_no_locs      := TAB_items;

   else
      O_costchg_rec.single_itemloc_ind := 'N';

      -- Get ISCL Records
      if NOT POPULATE_ISCL_TBL(O_error_message,
                               O_costchg_rec.iscl_rows_tbl,
                               O_costchg_rec.loc_items,
                               O_costchg_rec.hier_values,
                               TAB_items,
                               TAB_locs,
                               I_message.supplier,
                               I_message.origin_country_id) then
         return FALSE;
      end if;
   end if;

   -- Process cost changes for price_hist and packs
   if NOT POPULATE_PRICE_HIST(O_error_message,
                              O_costchg_rec.price_hist_rec,
                              O_costchg_rec.soh_items,
                              TAB_items,                -- Used to populate SOH for single and multi locs
                              TAB_tran_items,           -- Used to populate price hist and no locs
                              TAB_locs,
                              I_message.supplier,
                              I_message.origin_country_id,
                              I_message.unit_cost,
                              I_message.currency_code,
                              I_message.hier_level) then
      return FALSE;
   end if;

   if NOT POPULATE_PACKS_TBL(O_error_message,
                             O_costchg_rec.packs,
                             TAB_items,
                             I_message.supplier,
                             I_message.origin_country_id) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_ALL_LOCS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                      O_locs            OUT    NOCOPY   LOC_TBL,
                      I_hier_level      IN              VARCHAR2,
                      I_hier_value      IN              "RIB_XCostChgHrDtl_TBL")
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)   := 'RMSSUB_XCOSTCHG_VALIDATE.GET_ALL_LOCS';
   L_loc_subquery    VARCHAR2(1000) := NULL;
   L_table_bind_no   VARCHAR2(1)    := '1';

   TAB_hier_value     LOC_TBL        := LOC_TBL(); 


BEGIN

   -- Convert location table from message to LOC_TBL type
   FOR i IN I_hier_value.FIRST..I_hier_value.LAST LOOP
      TAB_hier_value.EXTEND();      
      TAB_hier_value(i) := I_hier_value(i).hier_value;
   END LOOP;
    
   -- Process locations table
   if I_hier_level = 'W' then
      if I_hier_value.COUNT = 1 then
         L_loc_subquery := 'select wh'||
                            ' from wh' ||
                           ' where wh = :' || L_table_bind_no;
         
      else
         L_loc_subquery := 'select wh' ||
                            ' from wh,' ||
                            ' TABLE (CAST(:' || L_table_bind_no || ' AS LOC_TBL))xloc' ||
                           ' where wh = value(xloc)';         
      end if;
   else
      if NOT ORGANIZATION_SQL.ORG_HIER_SUBQUERY(O_error_message,
                                                L_loc_subquery,
                                                L_table_bind_no,
                                                I_hier_level,
                                                TAB_hier_value,
                                                NULL) then
         return FALSE;
      end if;
      ---      
   end if;

   if I_hier_value.COUNT = 1 then
      EXECUTE IMMEDIATE L_loc_subquery BULK COLLECT INTO O_locs
         USING TAB_hier_value(1);
   else
      EXECUTE IMMEDIATE L_loc_subquery BULK COLLECT INTO O_locs
         USING TAB_hier_value;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_ALL_LOCS;
---------------------------------------------------------------------------------------------------------
FUNCTION GET_ALL_ITEMS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_items           OUT    NOCOPY   ITEM_TBL,
                       O_tran_items      OUT    NOCOPY   ITEM_TBL,
                       I_item            IN              ITEM_MASTER.ITEM%TYPE,
                       I_diff_id         IN              DIFF_IDS.DIFF_ID%TYPE,
                       I_include_parent  IN              BOOLEAN)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)   := 'RMSSUB_XCOSTCHG_VALIDATE.GET_ALL_ITEMS';

   L_item_subquery   VARCHAR2(1000) := NULL;
   L_item_bind_no    VARCHAR2(1)    := '1';
   L_diff_bind_no    VARCHAR2(1)    := '2';
   


BEGIN
   
   -- Create the table to contain all associated items for the passed in item
   if NOT ITEM_ATTRIB_SQL.ITEM_SUBQUERY(O_error_message,
                                        L_item_subquery,
                                        LP_im_rec.item_level,
                                        LP_im_rec.tran_level,
                                        LP_diff_column,
                                        NULL,
                                        L_item_bind_no,
                                        L_diff_bind_no,
                                        I_include_parent) then
      return FALSE;
   end if;

   if I_include_parent and I_diff_id IS NOT NULL then
      if LP_im_rec.tran_level - LP_im_rec.item_level = 2 then
         EXECUTE IMMEDIATE L_item_subquery BULK COLLECT INTO O_items
            USING I_item,
                  I_diff_id,
                  I_item,
                  I_item,
                  I_diff_id;
      else
         EXECUTE IMMEDIATE L_item_subquery BULK COLLECT INTO O_items
            USING I_item,
                  I_diff_id,
                  I_item;
      end if;
   elsif I_include_parent  then       -- without diff
      if LP_im_rec.tran_level - LP_im_rec.item_level = 2 then
         EXECUTE IMMEDIATE L_item_subquery BULK COLLECT INTO O_items
            USING I_item,
                  I_item,
                  I_item;
      else
         EXECUTE IMMEDIATE L_item_subquery BULK COLLECT INTO O_items
            USING I_item,
                  I_item;
      end if;
   else                           -- non parent item
      EXECUTE IMMEDIATE L_item_subquery BULK COLLECT INTO O_items
         USING I_item;
   end if;

   if O_items IS NULL OR O_items.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE', 'Item', I_item, NULL);
      return FALSE;
   end if;

   -- Create the table to contain only transaction level items for price hist
   O_tran_items    := ITEM_TBL();
   L_item_subquery := NULL;
   ---
   if I_include_parent then
      if NOT ITEM_ATTRIB_SQL.ITEM_SUBQUERY(O_error_message,
                                           L_item_subquery,
                                           LP_im_rec.item_level,
                                           LP_im_rec.tran_level,
                                           LP_diff_column,
                                           NULL,
                                           L_item_bind_no,
                                           L_diff_bind_no,
                                           FALSE) then
         return FALSE;
      end if;
      ---
      if I_diff_id IS NOT NULL then
         EXECUTE IMMEDIATE L_item_subquery BULK COLLECT INTO O_tran_items
            USING I_item,
                  I_diff_id;
      else      
         EXECUTE IMMEDIATE L_item_subquery BULK COLLECT INTO O_tran_items
            USING I_item;
      end if;
   else
      O_tran_items := O_items;
   end if;


   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_ALL_ITEMS;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ISCL_TBL(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                           O_iscl_rows       OUT    NOCOPY   RMSSUB_XCOSTCHG.ROWID_TBL,
                           O_iscl_items      OUT    NOCOPY   ITEM_TBL,
                           O_iscl_locs       OUT    NOCOPY   LOC_TBL,
                           I_items           IN              ITEM_TBL,
                           I_locs            IN              LOC_TBL,
                           I_supplier        IN              SUPS.SUPPLIER%TYPE,
                           I_country         IN              COUNTRY.COUNTRY_ID%TYPE)

   RETURN BOOLEAN IS

   L_program          VARCHAR2(50)   := 'RMSSUB_XCOSTCHG_VALIDATE.POPULATE_ISCL_TBL';

   cursor C_GET_ISCL_ITEMS is
      select distinct item
        from api_iscl
       where seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no;

   cursor C_GET_ISCL_LOCS is
      select distinct loc
        from api_iscl
       where seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no;

BEGIN

   if NOT LP_no_loc then
      FOR j in I_items.first..I_items.last LOOP
         FORALL i in I_locs.first..I_locs.last
            insert into api_iscl (iscl_rowid,
                                  item,
                                  loc,
                                  primary_loc_ind,
                                  seq_no)
               select iscl.rowid,
                      iscl.item,
                      iscl.loc,
                      iscl.primary_loc_ind,
                      RMSSUB_XCOSTCHG.LP_api_seq_no
                 from item_supp_country_loc iscl
                where iscl.loc = I_locs(i)
                  and iscl.origin_country_id = I_country
                  and iscl.supplier = I_supplier
                  and iscl.item = I_items(j);
      end LOOP;
      ---
      -- Get all valid items for in ISCL for processing orders
      open C_GET_ISCL_ITEMS;
      fetch C_GET_ISCL_ITEMS BULK COLLECT into O_iscl_items;
      close C_GET_ISCL_ITEMS;

      -- Check if there is at least one item-loc to process 
      if O_iscl_items is NULL or O_iscl_items.count = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE',
                                               'Hier Value',
                                               I_locs(1),
                                               NULL);
         return FALSE;
      end if;
      
      -- Get all valid locations for packs processing
      open C_GET_ISCL_LOCS;
      fetch C_GET_ISCL_LOCS BULK COLLECT into O_iscl_locs;
      close C_GET_ISCL_LOCS;

   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_ISCL_TBL;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_ISC_TBL(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                          O_isc_rows        OUT    NOCOPY   RMSSUB_XCOSTCHG.ROWID_TBL,
                          I_items           IN              ITEM_TBL,
                          I_locs            IN              LOC_TBL,
                          I_supplier        IN              SUPS.SUPPLIER%TYPE,
                          I_country         IN              COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50)  := 'RMSSUB_XCOSTCHG_VALIDATE.POPULATE_ISC_TBL';

   L_isc_query        VARCHAR2(2000);
   L_item_subquery    VARCHAR2(1000);

   L_supp_bind_no     VARCHAR2(1)   := '2';
   L_country_bind_no  VARCHAR2(1)   := '3';


BEGIN

   -- Create the Query
   L_isc_query := 'select isc.rowid' ||
                   ' from item_supp_country isc,' ||
                        ' item_supp_country_loc iscl,' ||
                        ' TABLE(CAST(:1 AS ITEM_TBL)) xitem';

   if LP_multi_loc then
      L_isc_query := L_isc_query ||
                       ', TABLE(CAST(:2 AS LOC_TBL)) xloc';
      ---
      L_supp_bind_no     := '3';
      L_country_bind_no  := '4';
   end if;

   L_isc_query := L_isc_query ||   
                  ' where isc.supplier = :'|| L_supp_bind_no ||
                    ' and isc.origin_country_id = :'|| L_country_bind_no ||
                    ' and iscl.primary_loc_ind = ''Y' ||''''||
                    ' and isc.item = iscl.item' ||
                    ' and isc.item = value(xitem)' ||
                    ' and isc.supplier = iscl.supplier' ||
                    ' and isc.origin_country_id = iscl.origin_country_id ';

   if LP_multi_loc then
      L_isc_query := L_isc_query ||
                    ' and iscl.loc = value(xloc)';
   elsif LP_single_loc then
      L_isc_query := L_isc_query ||
                    ' and iscl.loc = :4 ';
   end if;

   -- Populate the ISC table
   if LP_multi_loc then
      EXECUTE IMMEDIATE L_isc_query BULK COLLECT INTO O_isc_rows
            USING I_items,
                  I_locs,
                  I_supplier,
                  I_country;
   elsif LP_single_loc then
      EXECUTE IMMEDIATE L_isc_query BULK COLLECT INTO O_isc_rows
            USING I_items,
                  I_supplier,
                  I_country,
                  I_locs(1);
   end if;
   ---

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_ISC_TBL;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_PRICE_HIST(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                             O_price_hist_rec      OUT    NOCOPY   RMSSUB_XCOSTCHG.PRICE_HIST_RECTYPE,
                             O_soh_items           OUT    NOCOPY   ITEM_TBL,
                             I_items               IN              ITEM_TBL,
                             I_tran_items          IN              ITEM_TBL,
                             I_locs                IN              LOC_TBL,
                             I_supplier            IN              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                             I_origin_country_id   IN              ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_unit_cost           IN              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                             I_currency_code       IN              STORE.CURRENCY_CODE%TYPE,
                             I_hier_level          IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(50)  := 'RMSSUB_XCOSTCHG_VALIDATE.POPULATE_PRICE_HIST';

   L_cost_retail_ind     VARCHAR2(1)  := 'C';

   cursor C_SOH_ITEMS is
      select item
        from api_item
       where seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no;

   cursor C_PRICE_HIST is
      select item,
             loc,
             loc_type,
             unit_cost,
             unit_retail
        from api_price_hist
       where seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no;
        
   
BEGIN

   -- Set table depending on message hier type
   if I_hier_level is NULL then
      FORALL i in I_items.first..I_items.last      
         insert into api_price_hist (item,
                                     loc,
                                     loc_type,
                                     unit_cost,
                                     unit_retail,
                                     seq_no)
         select il.item,
                il.loc,
                il.loc_type,
                decode(v_loc.currency_code, I_currency_code, I_unit_cost,
                       CURRENCY_SQL.CONVERT_VALUE(L_cost_retail_ind,
                                                  v_loc.currency_code,
                                                  I_currency_code,
                                                  I_unit_cost)) unit_cost,
                il.unit_retail,
                RMSSUB_XCOSTCHG.LP_api_seq_no
           from item_loc il,
                (select store loc,
                        currency_code
                   from store
                 union all
                 select wh loc,
                        currency_code
                   from wh) v_loc
          where il.loc = v_loc.loc
            and il.item = I_items(i)
            and il.primary_supp = I_supplier
            and il.primary_cntry = I_origin_country_id;

   elsif I_hier_level = 'W' then
      FORALL i in I_items.first..I_items.last      
         insert into api_price_hist (item,
                                     loc,
                                     loc_type,
                                     unit_cost,
                                     unit_retail,
                                     seq_no)
         select il.item,
                il.loc,
                il.loc_type,
                decode(wh.currency_code, I_currency_code, I_unit_cost,
                       CURRENCY_SQL.CONVERT_VALUE(L_cost_retail_ind,
                                                  wh.currency_code,
                                                  I_currency_code,
                                                  I_unit_cost)) unit_cost,
                il.unit_retail,
                RMSSUB_XCOSTCHG.LP_api_seq_no
           from item_loc il,
                wh,
                TABLE(CAST(I_locs AS LOC_TBL)) xloc
          where il.loc = wh.wh
            and il.item = I_items(i)
            and wh.wh = value(xloc)
            and il.primary_supp = I_supplier
            and il.primary_cntry = I_origin_country_id;
   else
      FORALL i in I_items.first..I_items.last      
         insert into api_price_hist (item,
                                     loc,
                                     loc_type,
                                     unit_cost,
                                     unit_retail,
                                     seq_no)
         select il.item,
                il.loc,
                il.loc_type,
                decode(s.currency_code, I_currency_code, I_unit_cost,
                       CURRENCY_SQL.CONVERT_VALUE(L_cost_retail_ind,
                                                  s.currency_code,
                                                  I_currency_code,
                                                  I_unit_cost)) unit_cost,
                il.unit_retail,
                RMSSUB_XCOSTCHG.LP_api_seq_no
           from item_loc il,
                store s,
                TABLE(CAST(I_locs AS LOC_TBL)) xloc
          where il.loc = s.store
            and il.item = I_items(i)
            and s.store = value(xloc)
            and il.primary_supp = I_supplier
            and il.primary_cntry = I_origin_country_id;
   end if;

   -- Build Query for SOH items
   if not SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) then
        return FALSE;
   end if;
   
   if NOT LP_no_loc then
      FORALL i in I_tran_items.first..I_tran_items.last
         insert into api_item (item,
                               seq_no)
            select distinct il.item,
                   RMSSUB_XCOSTCHG.LP_api_seq_no
              from item_loc il,
                   TABLE(CAST(I_locs AS LOC_TBL)) xloc
             where il.loc = value(xloc)
               and il.primary_supp = I_supplier
               and il.primary_cntry = I_origin_country_id
               and il.item = I_tran_items(i);
   else
      FORALL i in I_tran_items.first..I_tran_items.last
         insert into api_item (item,
                               seq_no)
            select distinct il.item,
                   RMSSUB_XCOSTCHG.LP_api_seq_no
              from item_loc il
             where il.primary_supp = I_supplier
               and il.primary_cntry = I_origin_country_id
               and il.item = I_tran_items(i);
            
   end if;

   open C_SOH_ITEMS;
   fetch C_SOH_ITEMS BULK COLLECT into O_soh_items;
   close C_SOH_ITEMS;

   SQL_LIB.SET_MARK('OPEN',
                    'C_PRICE_HIST',
                    'item',
                    null);
   open C_PRICE_HIST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_PRICE_HIST',
                    'item',
                    null);
   fetch C_PRICE_HIST BULK COLLECT into O_price_hist_rec.items,
                                        O_price_hist_rec.locs,
                                        O_price_hist_rec.loc_type,
                                        O_price_hist_rec.unit_costs,
                                        O_price_hist_rec.retail_prices;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_PRICE_HIST',
                    'item',
                    null);
   close C_PRICE_HIST;
   
   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_PRICE_HIST;

---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_PACKS_ISC_TBL(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                                O_isc_tbl             OUT    NOCOPY   RMSSUB_XCOSTCHG.ROWID_TBL,
                                I_packs               IN              ITEM_TBL,
                                I_locs                IN              LOC_TBL,
                                I_supplier            IN              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                I_origin_country_id   IN              ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50)   := 'RMSSUB_XCOSTCHG_VALIDATE.POPULATE_PACKS_ISC_TBL';

   cursor C_ISC_UNIT_COST_API is
      select isc.rowid 
        from item_supp_country isc,
             api_iscl a
       where isc.origin_country_id = I_origin_country_id
         and isc.supplier = I_supplier
         and a.item = isc.item
         and a.primary_loc_ind = 'Y'
         and a.seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no;

   cursor C_ISC_UNIT_COST is
      select isc.rowid
        from item_supp_country isc,
             TABLE(CAST(I_packs AS ITEM_TBL)) xpack
       where isc.origin_country_id = I_origin_country_id
         and isc.supplier = I_supplier
         and isc.item = value(xpack);
         

BEGIN

   if I_packs is NULL or I_packs.count = 0 then                -- No Locs
      open C_ISC_UNIT_COST_API;
      fetch C_ISC_UNIT_COST_API BULK COLLECT into O_isc_tbl;
      close C_ISC_UNIT_COST_API;
   else                                                        -- Multi/Single Locs
      open C_ISC_UNIT_COST;
      fetch C_ISC_UNIT_COST BULK COLLECT into O_isc_tbl;
      close C_ISC_UNIT_COST;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_PACKS_ISC_TBL;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_PACKS_PRICE_HIST(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_price_hist_rec      OUT    NOCOPY   RMSSUB_XCOSTCHG.PRICE_HIST_RECTYPE,
                                   I_packs               IN              ITEM_TBL,
                                   I_locs                IN              LOC_TBL,
                                   I_hier_level          IN              VARCHAR2,
                                   I_supplier            IN              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                   I_origin_country_id   IN              ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                   I_unit_cost           IN              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                   I_currency_code       IN              STORE.CURRENCY_CODE%TYPE)

   RETURN BOOLEAN IS

   L_program          VARCHAR2(50)  := 'RMSSUB_XCOSTCHG_VALIDATE.POPULATE_PACKS_PRICE_HIST';

   L_packs_query         VARCHAR2(2000);

   L_currency_bind_no    VARCHAR2(1) := '1';
   L_unit_cost_bind_no   VARCHAR2(1) := '2';
   L_cost_retl_bind_no   VARCHAR2(1) := '3';
   L_supp_bind_no        VARCHAR2(1) := '5';
   L_country_bind_no     VARCHAR2(1) := '6';
   
   

   L_cost_retail_ind     VARCHAR2(1) := 'C';

   L_table               VARCHAR2(10);
   L_table_loc           VARCHAR2(20);
   L_table_alias         VARCHAR2(10);

BEGIN

   -- Reset API PRICE HIST TABLE for packs 
   delete api_price_hist where seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no;
 
   -- Set table depending on message hier type
   if I_hier_level is NULL then
      FORALL i in I_packs.first..I_packs.last      
         insert into api_price_hist (item,
                                     loc,
                                     loc_type,
                                     unit_cost,
                                     unit_retail,
                                     seq_no)
         select il.item,
                il.loc,
                il.loc_type,
                decode(v_loc.currency_code, I_currency_code, I_unit_cost,
                       CURRENCY_SQL.CONVERT_VALUE(L_cost_retail_ind,
                                                  v_loc.currency_code,
                                                  I_currency_code,
                                                  I_unit_cost)) unit_cost,
                il.unit_retail,
                RMSSUB_XCOSTCHG.LP_api_seq_no
           from item_loc il,
                (select store loc,
                        currency_code
                   from store
                 union all
                 select wh loc,
                        currency_code
                   from wh) v_loc,
                TABLE(CAST(I_locs AS LOC_TBL)) xloc
          where il.loc = v_loc.loc            
            and v_loc.loc = value(xloc)
            and il.primary_supp = I_supplier
            and il.primary_cntry = I_origin_country_id
            and il.item = I_packs(i);
   elsif I_hier_level = 'W' then
      FORALL i in I_packs.first..I_packs.last      
         insert into api_price_hist (item,
                                     loc,
                                     loc_type,
                                     unit_cost,
                                     unit_retail,
                                     seq_no)
         select il.item,
                il.loc,
                il.loc_type,
                decode(wh.currency_code, I_currency_code, I_unit_cost,
                       CURRENCY_SQL.CONVERT_VALUE(L_cost_retail_ind,
                                                  wh.currency_code,
                                                  I_currency_code,
                                                  I_unit_cost)) unit_cost,
                il.unit_retail,
                RMSSUB_XCOSTCHG.LP_api_seq_no
           from item_loc il,
                wh,
                TABLE(CAST(I_locs AS LOC_TBL)) xloc
          where il.loc = wh.wh
            and wh.wh = value(xloc)
            and il.primary_supp = I_supplier
            and il.primary_cntry = I_origin_country_id
            and il.item = I_packs(i);
   else
      FORALL i in I_packs.first..I_packs.last      
         insert into api_price_hist (item,
                                     loc,
                                     loc_type,
                                     unit_cost,
                                     unit_retail,
                                     seq_no)
         select il.item,
                il.loc,
                il.loc_type,
                decode(s.currency_code, I_currency_code, I_unit_cost,
                       CURRENCY_SQL.CONVERT_VALUE(L_cost_retail_ind,
                                                  s.currency_code,
                                                  I_currency_code,
                                                  I_unit_cost)) unit_cost,
                il.unit_retail,
                RMSSUB_XCOSTCHG.LP_api_seq_no
           from item_loc il,
                store s,
                TABLE(CAST(I_locs AS LOC_TBL)) xlocs
          where il.loc = s.store
            and s.store = value(xlocs)
            and il.primary_supp = I_supplier
            and il.primary_cntry = I_origin_country_id
            and il.item = I_packs(i);
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_PACKS_PRICE_HIST;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_PACKS_TBL(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                            O_packs_tbl           OUT    NOCOPY   ITEM_TBL,
                            I_items               IN              ITEM_TBL,
                            I_supplier            IN              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                            I_origin_country_id   IN              ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)    := 'RMSSUB_XCOSTCHG_VALIDATE.POPULATE_PACKS_TBL';
   L_packs_query     VARCHAR2(2000);
   L_parent_item     BOOLEAN;
   L_isc_exist       BOOLEAN         := TRUE;

BEGIN

   -- Build the Packs subquery
   IF NOT ITEM_ATTRIB_SQL.PACK_SUBQUERY(O_error_message,
                                        L_packs_query,
                                        L_isc_exist,
                                        I_items.COUNT) THEN
      return FALSE;
   END IF;
   
   -- Execute the query and populate the output table
   EXECUTE IMMEDIATE L_packs_query BULK COLLECT INTO O_packs_tbl
      USING I_items,
            I_supplier,
            I_origin_country_id;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_PACKS_TBL;
---------------------------------------------------------------------------------------------------------
END RMSSUB_XCOSTCHG_VALIDATE;
/
