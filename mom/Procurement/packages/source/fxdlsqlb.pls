CREATE OR REPLACE PACKAGE BODY FIXED_DEAL_SQL AS

RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

------------------------------------------------------------------------------
FUNCTION CHECK_DATES (I_deal_number   IN   NUMBER,
                      I_before_date   IN   DATE,
                      I_after_date    IN   DATE)
RETURN NUMBER IS


   L_dates_passed    BOOLEAN := FALSE;
   L_before_passed   BOOLEAN := FALSE;
   L_date_exist      VARCHAR2(1);

   CURSOR C_DEAL_AFTER IS
      SELECT 'x'
        FROM fixed_deal_dates
       WHERE deal_no      = I_deal_number
         AND collect_date > I_after_date;

   CURSOR C_DEAL_BEFORE IS
      SELECT 'x'
        FROM fixed_deal_dates
       WHERE deal_no      = I_deal_number
         AND collect_date < I_before_date;

   CURSOR C_DEAL_BOTH IS
      SELECT 'x'
        FROM fixed_deal_dates
       WHERE deal_no      = I_deal_number
         AND collect_date > I_after_date
         AND collect_date < I_before_date;


BEGIN

   if I_before_date is NULL then
      if I_after_date is NULL then
         --
         --- Both null, return true
         --
         return 1;
      else
         --
         --- After selection only
         --
         open C_DEAL_AFTER;

         fetch C_DEAL_AFTER into L_date_exist;

         if (C_DEAL_AFTER%notfound) THEN
             close C_DEAL_AFTER;
             return 0;
         end if;

         close C_DEAL_AFTER;

      end if;     -- if after date is NULL
   else           -- if before date is NULL

      if I_after_date is NULL then
         --
         --- Before date only
         --
         open C_DEAL_BEFORE;

         fetch C_DEAL_BEFORE into L_date_exist;

         if (C_DEAL_BEFORE%notfound) THEN
             close C_DEAL_BEFORE;
             return 0;
         end if;

         close C_DEAL_BEFORE;
      else
         --
         --- Both dates were passed in
         --
         open C_DEAL_BOTH;

         fetch C_DEAL_BOTH into L_date_exist;

         if (C_DEAL_BOTH%notfound) THEN
             close C_DEAL_BOTH;
             return 0;
         end if;

         close C_DEAL_BOTH;
      end if;

   end if;        -- if before date is NULL

   return 1;

EXCEPTION
   when OTHERS then

      close C_DEAL_AFTER;
      close C_DEAL_BEFORE;
      close C_DEAL_BOTH;

   return 0;
END CHECK_DATES;
------------------------------------------------------------------------------

FUNCTION NEXT_DEAL_NUMBER( O_error_message   IN OUT   VARCHAR2,
                           O_deal_number     IN OUT   NUMBER)
RETURN BOOLEAN IS

   L_wrap_sequence_number   COMPETITOR.COMPETITOR%TYPE;
   L_first_time             VARCHAR2(3)       := 'Yes';
   L_counter                VARCHAR2(1);

   CURSOR c_deal_sequence IS
      SELECT deal_sequence.NEXTVAL seq_no
        FROM dual;

   CURSOR C_DEAL_EXISTS IS
      SELECT 'x'
        FROM fixed_deal
       WHERE deal_no = O_deal_number
    union
      SELECT 'x'
        FROM deal_head
       WHERE deal_id = O_deal_number;

BEGIN

   FOR rec IN c_deal_sequence LOOP

      O_deal_number := rec.seq_no;

      if (L_first_time = 'Yes') THEN
         L_wrap_sequence_number := O_deal_number;
         L_first_time := 'No';
      elsif (O_deal_number = L_wrap_sequence_number) THEN
         O_error_message:= sql_lib.create_msg('NO_DEAL_NUMBERS',
                                                 NULL,NULL,NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('OPEN', 'C_DEAL_EXISTS', 'FIXED_DEAL',
                       'DEAL_NO: '|| to_char(O_deal_number));
      open C_DEAL_EXISTS;

      SQL_LIB.SET_MARK('FETCH', 'C_DEAL_EXISTS', 'FIXED_DEAL',
                       'DEAL_NO: '|| to_char(O_deal_number));
      fetch C_DEAL_EXISTS into L_counter;

      if (C_DEAL_EXISTS%notfound) THEN
         SQL_LIB.SET_MARK('CLOSE', 'C_DEAL_EXISTS', 'FIXED_DEAL',
                          'DEAL_NO: '|| to_char(O_deal_number));
         close C_DEAL_EXISTS;
         return TRUE;
      end if;

      SQL_LIB.SET_MARK('CLOSE', 'C_DEAL_EXISTS', 'FIXED_DEAL',
                       'DEAL_NO: '|| to_char(O_deal_number));
      close C_DEAL_EXISTS;

   END LOOP;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'FIXED_DEAL_SQL.NEXT_DEAL_NUMBER',
                                             to_char(SQLCODE));
   return FALSE;
END NEXT_DEAL_NUMBER;

------------------------------------------------------------------------------

FUNCTION GET_DEAL_DESC(O_error_message   IN OUT   VARCHAR2,
                       O_deal_desc       IN OUT   FIXED_DEAL.DEAL_DESC%TYPE,
                       I_deal_no         IN       FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN IS

   cursor C_DEAL_DESC is
      select deal_desc
        from fixed_deal
       where deal_no = I_deal_no;

BEGIN

   if I_deal_no is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM', 'I_deal_no',
                                           'NULL', 'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_DEAL_DESC','FIXED_DEAL',
                    'DEAL_NO: '|| I_deal_no);
   open C_DEAL_DESC;

   SQL_LIB.SET_MARK('FETCH','C_DEAL_DESC','FIXED_DEAL',
                    'DEAL_NO: '|| I_deal_no);
   fetch C_DEAL_DESC into O_deal_desc;

   if C_DEAL_DESC%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_DEAL_DESC','FIXED_DEAL',
                    'DEAL_NO: '|| I_deal_no);
      close C_DEAL_DESC;
      ---
      
   else
      SQL_LIB.SET_MARK('CLOSE','C_DEAL_DESC','FIXED_DEAL',
                    'DEAL_NO: '|| I_deal_no);
      close C_DEAL_DESC;
      ---
      O_error_message:= sql_lib.create_msg('INV_DEAL',
                                           to_char(I_deal_no),
                                           NULL,NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'FIXED_DEAL_SQL.GET_DEAL_DESC',
                                             to_char(SQLCODE));
   RETURN FALSE;
END GET_DEAL_DESC;

------------------------------------------------------------------------------

FUNCTION INSERT_PERIOD(O_error_message   IN OUT   VARCHAR2,
                       I_deal_no         IN       FIXED_DEAL_DATES.DEAL_NO%TYPE,
                       I_start_date      IN       FIXED_DEAL_DATES.COLLECT_DATE%TYPE,
                       I_deal_amt        IN       FIXED_DEAL_DATES.FIXED_DEAL_AMT%TYPE,
                       I_periods         IN       FIXED_DEAL.COLLECT_PERIODS%TYPE,
                       I_increment       IN       FIXED_DEAL.COLLECT_PERIODS%TYPE,
                       I_currency_code   IN       FIXED_DEAL.CURRENCY_CODE%TYPE)
RETURN BOOLEAN IS

   L_counter  FIXED_DEAL.COLLECT_PERIODS%TYPE := 0;

BEGIN

   LOOP

      SQL_LIB.SET_MARK('INSERT',NULL,'FIXED_DEAL_DATES','DEAL_NO: '||I_deal_no);
      insert into fixed_deal_dates (deal_no,
                                    collect_date,


                                    fixed_deal_amt,
                                    currency_code)
                            values (I_deal_no,
                                    ADD_MONTHS(I_start_date, I_increment * L_counter),


                                    I_deal_amt,
                                    I_currency_code);

      L_counter := L_counter + 1;

      if L_counter = I_periods then
         EXIT;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'FIXED_DEAL_SQL.INSERT_PERIOD',
                                             to_char(SQLCODE));
   RETURN FALSE;
END INSERT_PERIOD;

------------------------------------------------------------------------------

FUNCTION CREATE_SCHEDULE(O_error_message   IN OUT   VARCHAR2,
                         I_deal_no         IN       FIXED_DEAL.DEAL_NO%TYPE,
                         I_start_date      IN       FIXED_DEAL.COLLECT_START_DATE%TYPE,
                         I_periods         IN       FIXED_DEAL.COLLECT_PERIODS%TYPE,
                         I_collect_by      IN       FIXED_DEAL.COLLECT_BY%TYPE,
                         I_deal_amt        IN       FIXED_DEAL.FIXED_DEAL_AMT%TYPE,
                         I_currency_code   IN       FIXED_DEAL.CURRENCY_CODE%TYPE)
RETURN BOOLEAN IS

   cursor C_LOCK_FIXED_DEAL_DATES is
      select 'x'
        from fixed_deal_dates
       where deal_no = I_deal_no
         for update nowait;

BEGIN

   -- make sure input parameters are populated --
   if I_deal_no is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM', 'I_deal_no',
                                           'NULL', 'NOT NULL');
      return FALSE;
   end if;
   if I_start_date is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM', 'I_start_date',
                                           'NULL', 'NOT NULL');
      return FALSE;
   end if;
   if I_periods is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM', 'I_periods',
                                           'NULL', 'NOT NULL');
      return FALSE;
   end if;
   if I_collect_by is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM', 'I_collect_by',
                                           'NULL', 'NOT NULL');
      return FALSE;
   end if;
   if I_deal_amt is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM', 'I_deal_amt',
                                           'NULL', 'NOT NULL');
      return FALSE;
   end if;
   if I_currency_code is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM', 'I_currency_code',
                                           'NULL', 'NOT NULL');
      return FALSE;
   end if;


   -- delete any existing fixed_deal_dates records --
   SQL_LIB.SET_MARK('OPEN','C_LOCK_FIXED_DEAL_DATES','FIXED_DEAL_DATES','DEAL_NO: '||I_deal_no);
   open  C_LOCK_FIXED_DEAL_DATES;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_FIXED_DEAL_DATES','FIXED_DEAL_DATES','DEAL_NO: '||I_deal_no);
   close C_LOCK_FIXED_DEAL_DATES;

   SQL_LIB.SET_MARK('DELETE',NULL,'FIXED_DEAL_DATES','DEAL_NO: '||I_deal_no);
   delete from fixed_deal_dates
    where deal_no = I_deal_no;

   -- insert new fixed_deal_dates records based on input parameters --
   if I_collect_by = 'D' or I_collect_by = 'M' then

      if not INSERT_PERIOD(O_error_message,
                           I_deal_no,
                           I_start_date,
                           I_deal_amt,
                           I_periods,
                           1,                       -- monthly or only once for 'D'ate collections
                           I_currency_code) then
         return FALSE;
      end if;

   elsif I_collect_by = 'Q' then

      if not INSERT_PERIOD(O_error_message,
                           I_deal_no,
                           I_start_date,
                           I_deal_amt,
                           I_periods,
                           3,                        -- every three months or quarterly
                           I_currency_code) then
         return FALSE;
      end if;

   elsif I_collect_by = 'A' then

      if not INSERT_PERIOD(O_error_message,
                           I_deal_no,
                           I_start_date,
                           I_deal_amt,
                           I_periods,
                           12,                      -- every twelve months or annually
                           I_currency_code) then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('DELRECS_REC_LOC',
                                          'FIXED_DEAL_DATES',
                                          I_deal_no);
      return FALSE;

   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'FIXED_DEAL_SQL.CREATE_SCHEDULE',
                                             to_char(SQLCODE));
   RETURN FALSE;
END CREATE_SCHEDULE;

------------------------------------------------------------------------------
FUNCTION CREATE_MERCH_LOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   VARCHAR2,
                           I_group_type      IN       FIXED_DEAL_MERCH_LOC.LOC_TYPE%TYPE,
                           I_group_value     IN       VARCHAR2,
                           I_deal_no         IN       FIXED_DEAL_MERCH_LOC.DEAL_NO%TYPE,
                           I_seq_no          IN       FIXED_DEAL_MERCH_LOC.SEQ_NO%TYPE,
                           I_contrib_ratio   IN       FIXED_DEAL_MERCH_LOC.CONTRIB_RATIO%TYPE,
                           I_partner         IN       FIXED_DEAL.PARTNER_ID%TYPE,
                           I_partner_type    IN       FIXED_DEAL.PARTNER_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program                     VARCHAR2(64) := 'FIXED_DEAL_SQL.CREATE_MERCH_LOC';
   L_system_options_rec          SYSTEM_OPTIONS%ROWTYPE;
   L_exists                      BOOLEAN;
   L_company_store               BOOLEAN;
   L_store_name                  STORE.STORE_NAME%TYPE;
   L_valid_supp_loc_ou           BOOLEAN;
   L_loc_type                    ITEM_LOC.LOC_TYPE%TYPE;

   cursor C_STORE_CLASS is
      select store
        from v_store vs
       where store_class = I_group_value
         and (I_partner_type <> 'S' or
              L_system_options_rec.org_unit_ind = 'N' or
              exists (select 1
                        from partner_org_unit pou,
                             sups s
                       where (s.supplier = I_partner or s.supplier_parent = I_partner)
                         and pou.partner = s.supplier
                         and pou.partner_type in ('S','U')
                         and pou.org_unit_id = vs.org_unit_id
                         and rownum = 1));

   cursor C_STORE_DISTRICT is
      select store
        from v_store vs
       where district = I_group_value
         and (I_partner_type <> 'S' or
              L_system_options_rec.org_unit_ind = 'N' or
              exists (select 1
                        from partner_org_unit pou,
                             sups s
                       where (s.supplier = I_partner or s.supplier_parent = I_partner)
                         and pou.partner = s.supplier
                         and pou.partner_type in ('S','U')
                         and pou.org_unit_id = vs.org_unit_id
                         and rownum = 1));

   cursor C_SH_STORE_AREA is
      select sh.store
        from store_hierarchy sh,
             v_store vs
       where sh.area = I_group_value
         and sh.store = vs.store
         and (I_partner_type <> 'S' or
              L_system_options_rec.org_unit_ind = 'N' or
              exists (select 1
                        from partner_org_unit pou,
                             sups s
                       where (s.supplier = I_partner or s.supplier_parent = I_partner)
                         and pou.partner = s.supplier
                         and pou.partner_type in ('S','U')
                         and pou.org_unit_id = vs.org_unit_id
                         and rownum = 1));

   cursor C_SH_STORE_REGION is
      select sh.store
        from store_hierarchy sh,
             v_store vs
       where sh.region = I_group_value
         and sh.store = vs.store
         and (I_partner_type <> 'S' or
              L_system_options_rec.org_unit_ind = 'N' or
              exists (select 1
                        from partner_org_unit pou,
                             sups s
                       where (s.supplier = I_partner or s.supplier_parent = I_partner)
                         and pou.partner = s.supplier
                         and pou.partner_type in ('S','U')
                         and pou.org_unit_id = vs.org_unit_id
                         and rownum = 1));

   cursor C_LOC_LIST_DETAIL_LOCATION is
      select location
        from loc_list_detail l,
             v_wh v
       where l.location = v.wh
         and loc_type             = 'W'
         and loc_list             = I_group_value
         and stockholding_ind = 'Y'
         and (I_partner_type <> 'S' or
              L_system_options_rec.org_unit_ind = 'N' or
              exists (select 1
                        from partner_org_unit pou,
                             sups s
                       where (s.supplier = I_partner or s.supplier_parent = I_partner)
                         and pou.partner = s.supplier
                         and pou.partner_type in ('S','U')
                         and pou.org_unit_id = v.org_unit_id
                         and rownum = 1));

   cursor C_LOC_LIST_DETAIL_LOCATION_S is
      select l.location
        from loc_list_detail l,
             v_store vs
       where l.loc_list = I_group_value
         and l.loc_type = 'S'
         and l.location = vs.store
         and (I_partner_type <> 'S' or
              L_system_options_rec.org_unit_ind = 'N' or
              exists (select 1
                        from partner_org_unit pou,
                             sups s
                       where (s.supplier = I_partner or s.supplier_parent = I_partner)
                         and pou.partner = s.supplier
                         and pou.partner_type in ('S','U')
                         and pou.org_unit_id = vs.org_unit_id
                         and rownum = 1));

   cursor C_STORE_TRANSFER_ZONE is
      select store
        from v_store vs
       where transfer_zone = I_group_value
         and (I_partner_type <> 'S' or
              L_system_options_rec.org_unit_ind = 'N' or
              exists (select 1
                        from partner_org_unit pou,
                             sups s
                       where (s.supplier = I_partner or s.supplier_parent = I_partner)
                         and pou.partner = s.supplier
                         and pou.partner_type in ('S','U')
                         and pou.org_unit_id = vs.org_unit_id
                         and rownum = 1));

   cursor C_LOC_TRAIT is
      select lt.store
        from loc_traits_matrix lt,
             v_store vs
       where lt.loc_trait = I_group_value
         and lt.store = vs.store
         and (I_partner_type <> 'S' or
              L_system_options_rec.org_unit_ind = 'N' or
              exists (select 1
                        from partner_org_unit pou,
                             sups s
                       where (s.supplier = I_partner or s.supplier_parent = I_partner)
                         and pou.partner = s.supplier
                         and pou.partner_type in ('S','U')
                         and pou.org_unit_id = vs.org_unit_id
                         and rownum = 1));

   cursor C_STORE_UNION_WH is
      select store loc,
             'S' loc_type
        from v_store vs
       where (I_partner_type <> 'S' or
              L_system_options_rec.org_unit_ind = 'N' or
              exists (select 1
                        from partner_org_unit pou,
                             sups s
                       where (s.supplier = I_partner or s.supplier_parent = I_partner)
                         and pou.partner = s.supplier
                         and pou.partner_type in ('S','U')
                         and pou.org_unit_id = vs.org_unit_id
                         and rownum = 1))
       union all
      select wh loc,
             'W' loc_type
        from v_wh v
       where stockholding_ind = 'Y'
         and (I_partner_type <> 'S' or
              L_system_options_rec.org_unit_ind = 'N' or
              exists (select 1
                        from partner_org_unit pou,
                             sups s
                       where (s.supplier = I_partner or s.supplier_parent = I_partner)
                         and pou.partner = s.supplier
                         and pou.partner_type in ('S','U')
                         and pou.org_unit_id = v.org_unit_id
                         and rownum = 1));

   cursor C_STORE is
      select store
        from v_store vs
       where vs.store_type = 'C'
         and (I_partner_type <> 'S' or
              L_system_options_rec.org_unit_ind = 'N' or
              exists (select 1
                        from partner_org_unit pou,
                             sups s
                       where (s.supplier = I_partner or s.supplier_parent = I_partner)
                         and pou.partner = s.supplier
                         and pou.partner_type in ('S','U')
                         and pou.org_unit_id = vs.org_unit_id
                         and rownum = 1));

   cursor C_WH is
      select wh
        from v_wh v
       where stockholding_ind = 'Y'
         and (I_partner_type <> 'S' or
              L_system_options_rec.org_unit_ind = 'N' or
              exists (select 1
                        from partner_org_unit pou,
                             sups s
                       where (s.supplier = I_partner or s.supplier_parent = I_partner)
                         and pou.partner = s.supplier
                         and pou.partner_type in ('S','U')
                         and pou.org_unit_id = v.org_unit_id
                         and rownum = 1));


BEGIN

   ---
   if I_group_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_group_type',
                                           NULL,
                                           NULL);
      return FALSE;
   end if;
   ---
   if I_deal_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_deal_no',
                                           NULL,
                                           NULL);
      return FALSE;
   end if;
   ---
   if I_seq_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_seq_no',
                                           NULL,
                                           NULL);
      return FALSE;
   end if;
   ---
   if I_contrib_ratio is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_contrib_ratio',
                                           NULL,
                                           NULL);
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;
   ---
   O_exists := 'N';

   if I_group_type in ('S','W','E','PW','DW') then
      if FIXED_DEAL_SQL.MERCH_LOC_EXISTS (O_error_message,
                                          L_exists,
                                          I_deal_no,
                                          I_seq_no,
                                          I_group_value) = FALSE then
         return FALSE;
      end if;

      if L_exists then
         O_error_message := SQL_LIB.CREATE_MSG('LOC_EXISTS',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      ---
      if I_partner_type = 'S'
      and L_system_options_rec.org_unit_ind = 'Y' then
         ---
         if I_group_type in ('PW','DW') then
            L_loc_type := 'W';
         else
            L_loc_type := I_group_type;
         end if;
         ---
         if SET_OF_BOOKS_SQL.CHECK_SUPP_SINGLE_LOC(O_error_message,
                                                   L_valid_supp_loc_ou,
                                                   I_partner,
                                                   I_group_value,
                                                   L_loc_type) = FALSE then
            return FALSE;
         end if;
         ---
         if L_valid_supp_loc_ou = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   if I_group_type = 'S' then
      insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                        seq_no,
                                        loc_type,
                                        location,
                                        contrib_ratio)
                                values (I_deal_no,
                                        I_seq_no,
                                        I_group_type,
                                        I_group_value,
                                        I_contrib_ratio);

   elsif I_group_type = 'W' or I_group_type = 'E' then
      insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                        seq_no,
                                        loc_type,
                                        location,
                                        contrib_ratio)
                                values (I_deal_no,
                                        I_seq_no,
                                        I_group_type,
                                        I_group_value,
                                        I_contrib_ratio);

   elsif I_group_type = 'PW' or I_group_type = 'DW' then
      insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                        seq_no,
                                        loc_type,
                                        location,
                                        contrib_ratio)
                                values (I_deal_no,
                                        I_seq_no,
                                        'W',
                                        I_group_value,
                                        I_contrib_ratio);

   elsif I_group_type = 'C' then
      for c_store_class_rec in C_STORE_CLASS
      LOOP
         BEGIN
            insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                              seq_no,
                                              loc_type,
                                              location,
                                              contrib_ratio)
                                      values (I_deal_no,
                                              I_seq_no,
                                              'S',
                                              c_store_class_rec.store,
                                              I_contrib_ratio);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_exists := 'Y';
         END;
      END LOOP;

   elsif I_group_type = 'D' then
      for c_store_district_rec in C_STORE_DISTRICT
      LOOP
         BEGIN
            insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                              seq_no,
                                              loc_type,
                                              location,
                                              contrib_ratio)
                                      values (I_deal_no,
                                              I_seq_no,
                                              'S',
                                              c_store_district_rec.store,
                                              I_contrib_ratio);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_exists := 'Y';
         END;
      END LOOP;

   elsif I_group_type = 'A' then
      for c_sh_store_area_rec in C_SH_STORE_AREA
      LOOP
         BEGIN
            insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                              seq_no,
                                              loc_type,
                                              location,
                                              contrib_ratio)
                                      values (I_deal_no,
                                              I_seq_no,
                                              'S',
                                              c_sh_store_area_rec.store,
                                              I_contrib_ratio);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_exists := 'Y';
         END;
      END LOOP;

   elsif I_group_type = 'R' then
      for c_ch_store_region_rec in C_SH_STORE_REGION
      LOOP
         BEGIN
            insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                              seq_no,
                                              loc_type,
                                              location,
                                              contrib_ratio)
                                      values (I_deal_no,
                                              I_seq_no,
                                              'S',
                                              c_ch_store_region_rec.store,
                                              I_contrib_ratio);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_exists := 'Y';
         END;
      END LOOP;

   elsif I_group_type = 'LLW' then
      for c_loc_list_detail_location_rec in C_LOC_LIST_DETAIL_LOCATION
      LOOP
         BEGIN
            insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                              seq_no,
                                              loc_type,
                                              location,
                                              contrib_ratio)
                                      values (I_deal_no,
                                              I_seq_no,
                                              'W',
                                              c_loc_list_detail_location_rec.location,
                                              I_contrib_ratio);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_exists := 'Y';
         END;
      END LOOP;

   elsif I_group_type = 'LLS' then
      for c_loc_lt_detail_loc_s_rec in C_LOC_LIST_DETAIL_LOCATION_S
      LOOP
         BEGIN
            insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                              seq_no,
                                              loc_type,
                                              location,
                                              contrib_ratio)
                                      values (I_deal_no,
                                              I_seq_no,
                                              'S',
                                              c_loc_lt_detail_loc_s_rec.location,
                                              I_contrib_ratio);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_exists := 'Y';
         END;
      END LOOP;

   elsif I_group_type = 'T' then
      for c_store_transfer_zone_rec in C_STORE_TRANSFER_ZONE
      LOOP
         BEGIN
            insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                              seq_no,
                                              loc_type,
                                              location,
                                              contrib_ratio)
                                      values (I_deal_no,
                                              I_seq_no,
                                              'S',
                                              c_store_transfer_zone_rec.store,
                                              I_contrib_ratio);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_exists := 'Y';
         END;
      END LOOP;

   elsif I_group_type = 'L' then
      for c_loc_trait_rec in C_LOC_TRAIT
      LOOP
         BEGIN
            insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                              seq_no,
                                              loc_type,
                                              location,
                                              contrib_ratio)
                                      values (I_deal_no,
                                              I_seq_no,
                                              'S',
                                              c_loc_trait_rec.store,
                                              I_contrib_ratio);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_exists := 'Y';
         END;
      END LOOP;

   elsif I_group_type = 'AL' then
      for c_store_union_wh_rec in C_STORE_UNION_WH
      LOOP
         BEGIN
            insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                              seq_no,
                                              loc_type,
                                              location,
                                              contrib_ratio)
                                      values (I_deal_no,
                                              I_seq_no,
                                              c_store_union_wh_rec.loc_type,
                                              c_store_union_wh_rec.loc,
                                              I_contrib_ratio);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_exists := 'Y';
         END;
      END LOOP;

   elsif I_group_type = 'AS' then
      for c_store_rec in C_STORE
      LOOP
         BEGIN
            insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                              seq_no,
                                              loc_type,
                                              location,
                                              contrib_ratio)
                                      values (I_deal_no,
                                              I_seq_no,
                                              'S',
                                              c_store_rec.store,
                                              I_contrib_ratio);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_exists := 'Y';
         END;
      END LOOP;

   elsif I_group_type = 'AW' then
      for c_wh_rec in C_WH
      LOOP
         BEGIN
            insert into FIXED_DEAL_MERCH_LOC (deal_no,
                                              seq_no,
                                              loc_type,
                                              location,
                                              contrib_ratio)
                                      values (I_deal_no,
                                              I_seq_no,
                                              'W',
                                              c_wh_rec.wh,
                                              I_contrib_ratio);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               O_exists := 'Y';
         END;
      END LOOP;

   else
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_group_type',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('DELRECS_REC_LOC',
                                          'FIXED_DEAL_MERCH_LOC',
                                          I_deal_no);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CREATE_MERCH_LOC;

----------------------------------------------------------------------------------------------
FUNCTION MERCH_LOC_EXISTS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           I_deal_no         IN       FIXED_DEAL_MERCH_LOC.DEAL_NO%TYPE,
                           I_seq_no          IN       FIXED_DEAL_MERCH_LOC.SEQ_NO%TYPE,
                           I_location        IN       FIXED_DEAL_MERCH_LOC.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(32) := 'FIXED_DEAL_SQL.MERCH_LOC_EXISTS';
   L_deal     FIXED_DEAL_MERCH_LOC.DEAL_NO%TYPE;

   cursor C_DEAL_MERCH_LOC (I_sequence_no IN FIXED_DEAL_MERCH_LOC.SEQ_NO%TYPE) is
      select distinct deal_no
        from fixed_deal_merch_loc
       where deal_no   = I_deal_no
         and seq_no    = I_sequence_no
         and (location is null or
              location = nvl(I_location,location));

   cursor C_DEAL is
      select seq_no
        from fixed_deal_merch
       where deal_no = I_deal_no;

BEGIN

   O_exists := TRUE;

   if I_deal_no is NULL then
      O_error_message:= sql_lib.create_msg('REQUIRED_INPUT_IS_NULL', 'I_deal_no',L_program, NULL);
      return FALSE;
   end if;

   if I_seq_no is NULL then

      FOR L_deal_rec IN C_DEAL LOOP

         SQL_LIB.SET_MARK('OPEN','C_DEAL_MERCH_LOC','fixed_deal_merch_loc','NULL');
         open C_DEAL_MERCH_LOC (L_deal_rec.seq_no);

         SQL_LIB.SET_MARK('FETCH','C_DEAL_MERCH_LOC', 'fixed_deal_merch_loc','NULL');
         fetch C_DEAL_MERCH_LOC into L_deal;

         if C_DEAL_MERCH_LOC%NOTFOUND then
            O_exists := FALSE;
            SQL_LIB.SET_MARK('CLOSE','C_DEAL_MERCH_LOC', 'fixed_deal_merch_loc','NULL');
            close C_DEAL_MERCH_LOC;
            EXIT;
         else
            SQL_LIB.SET_MARK('CLOSE','C_DEAL_MERCH_LOC', 'fixed_deal_merch_loc','NULL');
            close C_DEAL_MERCH_LOC;
         end if;


      END LOOP;

   else

      SQL_LIB.SET_MARK('OPEN','C_DEAL_MERCH_LOC','fixed_deal_merch_loc','NULL');
      open C_DEAL_MERCH_LOC (I_seq_no);

      SQL_LIB.SET_MARK('FETCH','C_DEAL_MERCH_LOC', 'fixed_deal_merch_loc','NULL');
      fetch C_DEAL_MERCH_LOC into L_deal;

      if C_DEAL_MERCH_LOC%NOTFOUND then
         O_exists := FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_DEAL_MERCH_LOC', 'fixed_deal_merch_loc','NULL');
      close C_DEAL_MERCH_LOC;

   end if;


   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('DELRECS_REC_LOC',
                                          'FIXED_DEAL_MERCH_LOC',
                                          I_deal_no);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END MERCH_LOC_EXISTS;

----------------------------------------------------------------------------------------------

FUNCTION DELETE_RECORDS  (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_deal_no         IN       FIXED_DEAL_MERCH_LOC.DEAL_NO%TYPE,
                          I_seq_no          IN       FIXED_DEAL_MERCH_LOC.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   cursor C_LOCK_FIXED_DEAL_MERCH_LOC is
      select 'x'
        from fixed_deal_merch_loc
       where deal_no = I_deal_no
         and seq_no  = nvl(I_seq_no,seq_no)
         for update nowait;

   cursor C_LOCK_FIXED_DEAL_MERCH is
      select 'x'
        from fixed_deal_merch
       where deal_no = I_deal_no
         and seq_no  = nvl(I_seq_no,seq_no)
         for update nowait;

   L_table   VARCHAR2(20);
   L_program VARCHAR2(30)  := 'FIXED_DEAL_SQL.DELETE_RECORDS';

BEGIN

   L_table := 'FIXED_DEAL_MERCH_LOC';

   open C_LOCK_FIXED_DEAL_MERCH_LOC;
   close C_LOCK_FIXED_DEAL_MERCH_LOC;

   L_table := 'FIXED_DEAL_MERCH';

   open C_LOCK_FIXED_DEAL_MERCH;
   close C_LOCK_FIXED_DEAL_MERCH;

   delete fixed_deal_merch_loc
    where deal_no = I_deal_no
      and seq_no = nvl(I_seq_no,seq_no);

   delete fixed_deal_merch
    where deal_no = I_deal_no
      and seq_no = nvl(I_seq_no,seq_no);

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('DELRECS_REC_LOC',
                                            L_table,
                                            I_deal_no);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END DELETE_RECORDS;
-----------------------------------------------------------------------------

FUNCTION GET_SEQ_NO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_seq_id          IN OUT   FIXED_DEAL_MERCH.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_first_time    VARCHAR2(1)                    := 'Y';
   L_wrap_seq_no   FIXED_DEAL_MERCH.SEQ_NO%TYPE;
   L_exists        VARCHAR2(1)                    := 'N' ;
   L_program       VARCHAR2(30)                   := 'FIXED_DEAL_SQL.GET_SEQ_NO';

   cursor C_SEQ_EXISTS is
      select 'Y'
        from fixed_deal_merch
       where seq_no = O_seq_id
         and rownum = 1;

   cursor C_SELECT_NEXTVAL is
      select fixed_deal_merch_id_seq.NEXTVAL
        from dual;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_select_nextval',
                    'dual',
                    'NULL');
   open C_SELECT_NEXTVAL;
   ---
   LOOP
      SQL_LIB.SET_MARK('FETCH',
                       'C_select_nextval',
                       'dual',
                       'NULL');
      fetch C_SELECT_NEXTVAL into O_seq_id;
      ---
      if L_first_time = 'Y' then
         L_wrap_seq_no   := O_seq_id;
         L_first_time    := 'N';
      elsif (O_seq_id = L_wrap_seq_no) then
         O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO',
                                               NULL,
                                               NULL,
                                               NULL);
         SQL_LIB.SET_MARK('CLOSE',
                          'C_select_nextval',
                          'dual',
                          'NULL');
         close C_SELECT_NEXTVAL;
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_seq_exists',
                       'dual',
                       'NULL');
      open C_SEQ_EXISTS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_seq_exists',
                       'dual',
                       'NULL');
      fetch C_SEQ_EXISTS into L_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_seq_exists',
                       'dual',
                       'NULL');
      close C_SEQ_EXISTS;
      ---
      if L_exists = 'N'  then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_select_nextval',
                          'dual',
                          NULL);
         close C_SELECT_NEXTVAL;
         return TRUE;
      end if;
      ---
   END LOOP;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;

END;
-----------------------------------------------------------------------------
FUNCTION MERCH_DEAL_EXISTS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   BOOLEAN,
                            I_deal_no         IN       FIXED_DEAL_MERCH.DEAL_NO%TYPE,
                            I_dept            IN       FIXED_DEAL_MERCH.DEPT%TYPE,
                            I_class           IN       FIXED_DEAL_MERCH.CLASS%TYPE,
                            I_subclass        IN       FIXED_DEAL_MERCH.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(32) := 'FIXED_DEAL_SQL.MERCH_DEAL_EXISTS';
   L_deal     FIXED_DEAL_MERCH_LOC.DEAL_NO%TYPE;

   cursor C_DEAL_MERCH is
      select distinct deal_no
        from fixed_deal_merch
       where deal_no   = I_deal_no
         and (dept is null or
              dept     = nvl(I_dept,dept))
         and (class is null or
              class    = nvl(I_class,class))
         and (subclass is null or
              subclass = nvl(I_subclass,subclass));

BEGIN

   O_exists := TRUE;

   if I_deal_no is NULL then
      O_error_message:= sql_lib.create_msg('REQUIRED_INPUT_IS_NULL', 'I_deal_no',L_program, NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_DEAL_MERCH','fixed_deal_merch','NULL');
   open C_DEAL_MERCH;

   SQL_LIB.SET_MARK('FETCH','C_DEAL_MERCH', 'fixed_deal_merch','NULL');
   fetch C_DEAL_MERCH into L_deal;

   if C_DEAL_MERCH%NOTFOUND then
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_DEAL_MERCH', 'fixed_deal_merch','NULL');
   close C_DEAL_MERCH;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END MERCH_DEAL_EXISTS;
-----------------------------------------------------------------------------
FUNCTION DELETE_DEAL_DATES (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_deal_no         IN       FIXED_DEAL_MERCH.DEAL_NO%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(32) := 'FIXED_DEAL_SQL.DELETE_DEAL_DATES';

   cursor C_LOCK_FIXED_DEAL_DATES is
      select 'x'
        from fixed_deal_dates
       where deal_no = I_deal_no
         for update nowait;

BEGIN

   if I_deal_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_deal_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_FIXED_DEAL_DATES',
                    'FIXED_DEAL_DATES',
                    'DEAL_NO: '||I_deal_no);
   open C_LOCK_FIXED_DEAL_DATES;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_FIXED_DEAL_DATES',
                    'FIXED_DEAL_DATES',
                    'DEAL_NO: '||I_deal_no);
   close C_LOCK_FIXED_DEAL_DATES;

   delete from fixed_deal_dates
    where deal_no = I_deal_no;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_DEAL_DATES;
-----------------------------------------------------------------------------
FUNCTION CHECK_DEAL_EXTRACTED (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   BOOLEAN,
                               I_deal_no         IN       FIXED_DEAL_DATES.DEAL_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(32) := 'FIXED_DEAL_SQL.DELETE_DEAL_DATES';
   L_deal_no   FIXED_DEAL_DATES.DEAL_NO%TYPE;

   cursor C_CHECK_DEAL_EXTRACTED is
      select distinct deal_no
        from fixed_deal_dates
       where deal_no = I_deal_no
         and extracted_ind = 'Y';

BEGIN

   O_exists := TRUE;

   if I_deal_no is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_deal_no',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   open C_CHECK_DEAL_EXTRACTED;
   fetch C_CHECK_DEAL_EXTRACTED into L_deal_no;
   if C_CHECK_DEAL_EXTRACTED%NOTFOUND then
      O_exists := FALSE;
   end if;
   close C_CHECK_DEAL_EXTRACTED;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_DEAL_EXTRACTED;
-----------------------------------------------------------------------------
FUNCTION SET_EXT_IND_INACT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_deal_no         IN       FIXED_DEAL_DATES.DEAL_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(32) := 'FIXED_DEAL_SQL.SET_EXT_IND_INACT';
   L_deal_no   FIXED_DEAL_DATES.DEAL_NO%TYPE;
   L_vdate     PERIOD.VDATE%TYPE := GET_VDATE;

   cursor C_LOCK_DATES is
     select 'x'
       from fixed_deal_dates
      where deal_no = I_deal_no
        for update nowait;

BEGIN

   ---
   open C_LOCK_DATES;
   ---
   close C_LOCK_DATES;
   ---

   update fixed_deal_dates
      set extracted_ind = 'I'
    where deal_no = I_deal_no
      and collect_date < L_vdate
      and extracted_ind ='N';

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END SET_EXT_IND_INACT;
-----------------------------------------------------------------------------
FUNCTION CHECK_SAME_VAT_REGION(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_same_vat_region   IN OUT   VARCHAR2,
                               I_deal_no           IN       FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(40)   := 'FIXED_DEAL_SQL.CHECK_SAME_VAT_REGION';

   cursor C_SAME_VAT_REGION is
      select 'Y'
        from fixed_deal fd,
             fixed_deal_merch_loc fdm,
             sups sp
       where fd.deal_no  = I_deal_no
         and fd.deal_no = fdm.deal_no
         and fd.supplier = sp.supplier
         and sp.vat_region in (select vat_region
                                 from store
                                where store = fdm.location
                                  and fdm.loc_type = 'S'
                               union all
                               select vat_Region
                                 from wh
                                where wh = fdm.location
                                  and fdm.loc_type = 'W')
         and rownum = 1;

BEGIN

   O_same_vat_region := 'N';

   SQL_LIB.SET_MARK('OPEN','C_SAME_VAT_REGION','fixed_deal_merch','NULL');
   open C_SAME_VAT_REGION;

   SQL_LIB.SET_MARK('FETCH','C_SAME_VAT_REGION', 'fixed_deal_merch','NULL');
   fetch C_SAME_VAT_REGION into O_same_vat_region;

   SQL_LIB.SET_MARK('CLOSE','C_SAME_VAT_REGION', 'fixed_deal_merch','NULL');
   close C_SAME_VAT_REGION;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END CHECK_SAME_VAT_REGION;
-----------------------------------------------------------------------------
FUNCTION CHECK_VAT_REGION_COUNT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_vat_region_count   IN OUT   VARCHAR2,
                                I_deal_no            IN       FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(40)   := 'FIXED_DEAL_SQL.CHECK_VAT_REGION_COUNT';

   cursor C_VAT_REGION_COUNT is
      select count(distinct(vat_region))
        from (select s.vat_region
                from fixed_deal_merch_loc fdml,
                     store s
               where fdml.deal_no = I_deal_no
                 and fdml.location = s.store
               union all
              select wh.vat_region
                from fixed_deal_merch_loc fdml,
                     wh
               where fdml.deal_no = I_deal_no
                 and fdml.location = wh.wh);

BEGIN

   O_vat_region_count := 0;

   SQL_LIB.SET_MARK('OPEN','C_VAT_REGION_COUNT','fixed_deal_merch_loc','NULL');
   open C_VAT_REGION_COUNT;

   SQL_LIB.SET_MARK('FETCH','C_VAT_REGION_COUNT', 'fixed_deal_merch_loc','NULL');
   fetch C_VAT_REGION_COUNT into O_vat_region_count;

   SQL_LIB.SET_MARK('CLOSE','C_VAT_REGION_COUNT', 'fixed_deal_merch_loc','NULL');
   close C_VAT_REGION_COUNT;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END CHECK_VAT_REGION_COUNT;
-----------------------------------------------------------------------------
END FIXED_DEAL_SQL;
/
