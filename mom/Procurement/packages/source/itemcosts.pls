CREATE OR REPLACE PACKAGE ITEM_COST_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------------------
LP_SVAT                VARCHAR2(6)   := 'SVAT';
LP_GTAX                VARCHAR2(6)   := 'GTAX';
LP_SALES               VARCHAR2(6)   := 'SALES';
LP_ISC                 VARCHAR2(15)  := 'ITEMSUPPCTRY';
LP_ISCL                VARCHAR2(15)  := 'ITEMSUPPCTRYLOC';
LP_TAX_SUPPLIER_TYPE   VARCHAR2(3)   := 'SP';
LP_TAX_STORE_TYPE      VARCHAR2(3)   := 'ST';
LP_TAX_WH_TYPE         VARCHAR2(3)   := 'WH';
LP_TAX_EXTFIN_TYPE     VARCHAR2(3)   := 'E';
LP_RECLASS_DOC_TYPE    VARCHAR2(3)   := 'RL';
------------------------------------------------------------------------------------------------------------
TYPE item_cost_info_rec IS RECORD(item                     ITEM_COST_HEAD.ITEM%TYPE,
                                  supplier                 ITEM_COST_HEAD.SUPPLIER%TYPE,
                                  origin_country_id        ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                                  delivery_country_id      ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE,
                                  nic_static_ind           ITEM_COST_HEAD.NIC_STATIC_IND%TYPE,
                                  base_cost                ITEM_COST_HEAD.BASE_COST%TYPE,
                                  negotiated_item_cost     ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE,
                                  extended_base_cost       ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE,
                                  inclusive_cost           ITEM_COST_HEAD.INCLUSIVE_COST%TYPE,
                                  default_po_cost          COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE,
                                  default_deal_cost        COUNTRY_ATTRIB.DEFAULT_DEAL_COST%TYPE,
                                  default_cost_comp_cost   COUNTRY_ATTRIB.DEFAULT_COST_COMP_COST%TYPE);
------------------------------------------------------------------------------------------------------------
TYPE item_cost_detail_rec IS RECORD(cond_type             ITEM_COST_DETAIL.COND_TYPE%TYPE,
                                    cond_value            ITEM_COST_DETAIL.COND_VALUE%TYPE,
                                    applied_on            ITEM_COST_DETAIL.APPLIED_ON%TYPE,
                                    modified_taxable_base ITEM_COST_DETAIL.MODIFIED_TAXABLE_BASE%TYPE,
                                    comp_rate             ITEM_COST_DETAIL.COMP_RATE%TYPE,
                                    calc_basis            ITEM_COST_DETAIL.CALCULATION_BASIS%TYPE,
                                    recoverable_amount    ITEM_COST_DETAIL.RECOVERABLE_AMOUNT%TYPE,
                                    seq_num               NUMBER(2),
                                    error_message         RTK_ERRORS.RTK_TEXT%TYPE,
                                    return_code           VARCHAR2(5));
TYPE item_cost_detail_tbl IS TABLE OF item_cost_detail_rec
   INDEX BY BINARY_INTEGER;
------------------------------------------------------------------------------------------------------------
TYPE item_add_rec is RECORD(item                 ITEM_SUPPLIER.ITEM%TYPE,
                            supplier             ITEM_SUPPLIER.SUPPLIER%TYPE,
                            origin_country       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                            vpn                  ITEM_SUPPLIER.VPN%TYPE,
                            consignment_rate     ITEM_SUPPLIER.CONSIGNMENT_RATE%TYPE,
                            pallet_name          ITEM_SUPPLIER.PALLET_NAME%TYPE,
                            case_name            ITEM_SUPPLIER.CASE_NAME%TYPE,
                            inner_name           ITEM_SUPPLIER.INNER_NAME%TYPE,
                            unit_cost            ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                            lead_time            ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
                            supp_pack_size       ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                            inner_pack_size      ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                            round_lvl            ITEM_SUPP_COUNTRY.ROUND_LVL%TYPE,
                            round_to_inner_pct   ITEM_SUPP_COUNTRY.ROUND_TO_INNER_PCT%TYPE,
                            round_to_case_pct    ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT%TYPE,
                            round_to_layer_pct   ITEM_SUPP_COUNTRY.ROUND_TO_LAYER_PCT%TYPE,
                            round_to_pallet_pct  ITEM_SUPP_COUNTRY.ROUND_TO_PALLET_PCT%TYPE,
                            packing_method       ITEM_SUPP_COUNTRY.PACKING_METHOD%TYPE,
                            default_uop          ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE,
                            ti                   ITEM_SUPP_COUNTRY.TI%TYPE,
                            hi                   ITEM_SUPP_COUNTRY.HI%TYPE,
                            length               ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE,
                            width                ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE,
                            height               ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE,
                            lwh_uom              UOM_CLASS.UOM%TYPE,
                            weight               ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE,
                            weight_uom           UOM_CLASS.UOM%TYPE,
                            primary_case_size    ITEM_SUPPLIER.PRIMARY_CASE_SIZE%TYPE,
                            tolerance_type       ITEM_SUPP_COUNTRY.TOLERANCE_TYPE%TYPE, 
                            max_tolerance        ITEM_SUPP_COUNTRY.MAX_TOLERANCE%TYPE,
                            min_tolerance        ITEM_SUPP_COUNTRY.MIN_TOLERANCE%TYPE,
                            cost_uom             ITEM_SUPP_COUNTRY.COST_UOM%TYPE);
------------------------------------------------------------------------------------------------------------
-- Function Name : CHECK_DUP_ITEM_COST
-- Purpose       : This function checks if any record exists in ITEM_COST_HEAD for the input item,
--                 supplier,origin country and delivery country.
------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DUP_ITEM_COST(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists                IN OUT BOOLEAN,
                             I_item                  IN     ITEM_COST_HEAD.ITEM%TYPE,
                             I_supplier              IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                             I_origin_country_id     IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                             I_delivery_country_id   IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : PRIMARY_DLVY_CTRY_EXISTS
-- Purpose       : This function will return the primary delivery country for  the input item,
--                 supplier and origin country.If primary delivery country does not exist, it will return
--                 NULL.
------------------------------------------------------------------------------------------------------------
FUNCTION PRIMARY_DLVY_CTRY_EXISTS(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_prim_delivery_country_id   IN OUT ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE,
                                  I_item                       IN     ITEM_COST_HEAD.ITEM%TYPE,
                                  I_supplier                   IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                                  I_origin_country_id          IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : CHECK_DLVY_CTRY_EXISTS
-- Purpose       : This function checks if any record exists in ITEM_COST_HEAD for the input
--                 delivery country
------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DLVY_CTRY_EXISTS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists                IN OUT BOOLEAN,
                                I_delivery_country_id   IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : CHANGE_PRIM_DLVY_CTRY
-- Purpose       : This function changes the primary delivery country indicator in ITEM_COST_HEAD
--                 if primary delivery country exists for input item,supplier and origin country. If primary
--                 delivery country does not exist, function does nothing.
------------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_PRIM_DLVY_CTRY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item                IN     ITEM_COST_HEAD.ITEM%TYPE,
                               I_supplier            IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                               I_origin_country_id   IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : DEL_ITEM_COST
-- Purpose       : This function deletes from ITEM_COST_HEAD for input item,supplier,origin country and delivery country
------------------------------------------------------------------------------------------------------------
FUNCTION DEL_ITEM_COST(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item                  IN     ITEM_COST_HEAD.ITEM%TYPE,
                       I_supplier              IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                       I_origin_country_id     IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                       I_delivery_country_id   IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : GET_INCL_NIC_IND
-- Purpose       : This function retrieves the INCL_NIC_IND from the VAT_CODES table for a given VAT_CODE.
------------------------------------------------------------------------------------------------------------
FUNCTION GET_INCL_NIC_IND(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_incl_nic_ind    IN OUT VAT_CODES.INCL_NIC_IND%TYPE,
                          I_vat_code        IN     VAT_CODES.VAT_CODE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : GET_DLVY_CTRY_INFO
-- Purpose       : This function fetches default location and item cost indicator values(inclusive/exclusive tax) of
--                 the entered delivery country from the country_attributes table.
------------------------------------------------------------------------------------------------------------
FUNCTION GET_DLVY_CTRY_INFO (O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_default_location         IN OUT COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                             O_loc_type                 IN OUT COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE,
                             O_item_cost_tax_incl_ind   IN OUT COUNTRY_ATTRIB.ITEM_COST_TAX_INCL_IND%TYPE,
                             I_delivery_country_id      IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : GET_NIC_STATIC_IND
-- Purpose       : This function fetches nic_static_ind from item_cost_head for input item,supplier,
--                 origin country and delivery countrye.
------------------------------------------------------------------------------------------------------------
FUNCTION GET_NIC_STATIC_IND (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_nic_static_ind        IN OUT ITEM_COST_HEAD.NIC_STATIC_IND%TYPE,
                             I_item                  IN     ITEM_COST_HEAD.ITEM%TYPE,
                             I_supplier              IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                             I_origin_country_id     IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                             I_delivery_country_id   IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : COMPUTE_ITEM_COST
-- Purpose       : This function will calculate item cost based on tax details
------------------------------------------------------------------------------------------------------------
FUNCTION COMPUTE_ITEM_COST (O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_base_cost                   IN OUT ITEM_COST_HEAD.BASE_COST%TYPE,
                            O_extended_base_cost          IN OUT ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE,
                            O_inclusive_cost              IN OUT ITEM_COST_HEAD.INCLUSIVE_COST%TYPE,
                            O_negotiated_item_cost        IN OUT ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE,
                            I_item                        IN     ITEM_COST_HEAD.ITEM%TYPE,
                            I_nic_static_ind              IN     ITEM_COST_HEAD.NIC_STATIC_IND%TYPE,
                            I_supplier                    IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                            I_location                    IN     COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                            I_loc_type                    IN     COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE,
                            I_calling_form                IN     VARCHAR2 DEFAULT LP_ISC,
                            I_origin_country_id           IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE DEFAULT NULL,
                            I_delivery_country_id         IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE DEFAULT NULL,
                            I_prim_dlvy_ctry_ind          IN     ITEM_COST_HEAD.PRIM_DLVY_CTRY_IND%TYPE DEFAULT NULL,
                            I_update_itemcost_ind         IN     VARCHAR2 DEFAULT 'N',
                            I_update_itemcost_child_ind   IN     VARCHAR2 DEFAULT 'N',
                            I_insert_item_supplier_ind    IN     VARCHAR2 DEFAULT 'N',
                            I_item_add_rec                IN     ITEM_ADD_REC DEFAULT NULL,
                            I_item_cost_tax_incl_ind      IN     COUNTRY_ATTRIB.ITEM_COST_TAX_INCL_IND%TYPE DEFAULT 'Y')
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : COMPUTE_ITEM_COST_BULK
-- Purpose       : This function will calculate item cost based on tax details
------------------------------------------------------------------------------------------------------------
FUNCTION COMPUTE_ITEM_COST_BULK(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_comp_item_cost_tbl         IN OUT OBJ_COMP_ITEM_COST_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : GET_COST_INFO
-- Purpose       : This function will retrieve the cost information (NIC, BC, EBC, IC, NIC is static indicator),
--                 default cost indicators for the primary delivery country of a given item/supplier/origin country
--                 combination, or for the primary delivery country of the primary supplier/origin country of the item if
--                 the supplier and origin country is not passed in.
------------------------------------------------------------------------------------------------------------
FUNCTION GET_COST_INFO (O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_item_cost_info_rec   IN OUT ITEM_COST_INFO_REC,
                        I_item                 IN     ITEM_COST_HEAD.ITEM%TYPE,
                        I_supplier             IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                        I_origin_country_id    IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : UPD_ITEM_SUPP_COUNTRY_COST
-- Purpose       : This function will update the costing information of the primary delivery country
--                 in ITEM_SUPP_COUNTRY table.
------------------------------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_SUPP_COUNTRY_COST (O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item                   IN     ITEM_COST_HEAD.ITEM%TYPE,
                                     I_supplier               IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                                     I_origin_country_id      IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                                     I_location               IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                                     I_base_cost              IN     ITEM_COST_HEAD.BASE_COST%TYPE,
                                     I_extended_base_cost     IN     ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE,
                                     I_negotiated_item_cost   IN     ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE,
                                     I_inclusive_cost         IN     ITEM_COST_HEAD.INCLUSIVE_COST%TYPE,
                                     I_callingform            IN     VARCHAR2,
								     I_default_down_ind       IN     VARCHAR2 DEFAULT 'Y')

RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_SUPP_COUNTRY_COST (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item                   IN       ITEM_COST_HEAD.ITEM%TYPE,
                                     I_supplier               IN       ITEM_COST_HEAD.SUPPLIER%TYPE,
                                     I_origin_country_id      IN       ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                                     I_location               IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                                     I_unit_cost              IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                     I_callingform            IN       VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : QUERY_PROCEDURE
-- Purpose       : This function will query from item_cost_detail table based on values passed for
--                 item,supplier,origin country id and delivery country id
------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (O_item_cost_detail_tbl     IN OUT ITEM_COST_DETAIL_TBL,
                           I_item                     IN     ITEM_COST_HEAD.ITEM%TYPE,
                           I_supplier                 IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                           I_origin_country_id        IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                           I_delivery_country_id      IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE,
                           I_loc                      IN     COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                           I_loc_type                 IN     COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE,
                           IO_base_cost               IN OUT ITEM_COST_HEAD.BASE_COST%TYPE,
                           IO_negotiated_item_cost    IN OUT ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE,
                           IO_extended_base_cost      IN OUT ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE,
                           IO_inclusive_cost          IN OUT ITEM_COST_HEAD.INCLUSIVE_COST%TYPE,
                           I_input_cost               IN     ITEM_COST_HEAD.BASE_COST%TYPE,
                           I_calling_form             IN     VARCHAR2,
                           I_item_cost_tax_incl_ind   IN     COUNTRY_ATTRIB.ITEM_COST_TAX_INCL_IND%TYPE);
------------------------------------------------------------------------------------------------------------
-- Function Name : GET_ITEM_COST_HEAD
-- Purpose       : This function will return ITEM_COST_HEAD row for passed in item,supplier,
--                 origin country id and delivery country id
------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_COST_HEAD (O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_item_cost_head_row     IN OUT ITEM_COST_HEAD%ROWTYPE,
                             I_item                   IN     ITEM_COST_HEAD.ITEM%TYPE,
                             I_supplier               IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                             I_origin_country_id      IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                             I_delivery_country_id    IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : GET_DEFAULT_PO_COST
-- Purpose       : This function will return default_po_cost row for passed in country or location
------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_PO_COST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_default_po_cost IN OUT COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE,
                             I_country_id      IN     COUNTRY_ATTRIB.COUNTRY_ID%TYPE,
                             I_loc             IN     COUNTRY_ATTRIB.DEFAULT_LOC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : UPDATE_ITEM_COST
-- Purpose       : This function will recalculate BC, EBC, NIC and IC and update the same
--                 ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_LOC and ITEM_COST_HEAD.
------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_COST (O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item                 IN     ITEM_COST_HEAD.ITEM%TYPE,
                           I_delivery_country_id  IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                           I_default_location     IN     COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                           I_default_loc_type     IN     COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : ITEM_COST_EXIST
-- Purpose       : This function checks if any record exists in ITEM_COST_HEAD for the input item and
--                 supplier.
------------------------------------------------------------------------------------------------------------
FUNCTION ITEM_COST_EXIST(O_error_message       IN OUT   VARCHAR2,
                         O_exists              IN OUT   BOOLEAN,
                         I_item                IN       ITEM_COST_HEAD.ITEM%TYPE,
                         I_supplier            IN       ITEM_COST_HEAD.SUPPLIER%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : BUYER_PACK_COST
-- Purpose       : This function checks if any record exists in ITEM_COST_HEAD for the input buyer pack item
--                and supplier,if not creates one.This is called from itemsupcntry.fmb
-----------------------------------------------------------------------------------------------------------
FUNCTION BUYER_PACK_COST(O_error_message       IN OUT   VARCHAR2,
                         I_item                IN       ITEM_COST_HEAD.ITEM%TYPE,
                         I_supplier            IN       ITEM_COST_HEAD.SUPPLIER%TYPE,
                         I_origin_country      IN       ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
 RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : DEF_ITEM_COST
-- Purpose       : This function checks if any record exists in ITEM_COST_HEAD for the input 
--                 item and sync up ISC and corresponding ICH/ICD.
-----------------------------------------------------------------------------------------------------------
FUNCTION DEF_ITEM_COST(O_error_message               IN OUT   VARCHAR2,
                       I_item                        IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                       I_supplier                    IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                       I_unit_cost                   IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                       I_origin_country              IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       I_update_itemcost_child_ind   IN       VARCHAR2 DEFAULT 'N')
 return BOOLEAN;
------------------------------------------------------------------------------------------------------------
-- Function Name : GET_COST_INFO_WRP
-- Purpose  : This is a wrapper function that returns a database
--            type rec object instead of a PLSQL type to be called from Java wrappers.
------------------------------------------------------------------------------------------------------------
FUNCTION GET_COST_INFO_WRP(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_item_cost_info_rec   IN OUT WRP_ITEM_COST_INFO_REC,
                           I_item                 IN     ITEM_COST_HEAD.ITEM%TYPE,
                           I_supplier             IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                           I_origin_country_id    IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------------------------------------
FUNCTION GET_COST_INFO_BOOL_WRP(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_item_cost_info_rec   IN OUT WRP_ITEM_COST_INFO_REC,
                                I_item                 IN     ITEM_COST_HEAD.ITEM%TYPE,
                                I_supplier             IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                                I_origin_country_id    IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
   return NUMBER;
------------------------------------------------------------------------------------------------------------
FUNCTION COMPUTE_ITEM_COST_WRP(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_base_cost                   IN OUT ITEM_COST_HEAD.BASE_COST%TYPE,
                               O_extended_base_cost          IN OUT ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE,
                               O_inclusive_cost              IN OUT ITEM_COST_HEAD.INCLUSIVE_COST%TYPE,
                               O_negotiated_item_cost        IN OUT ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE,
                               I_item                        IN     ITEM_COST_HEAD.ITEM%TYPE,
                               I_nic_static_ind              IN     ITEM_COST_HEAD.NIC_STATIC_IND%TYPE,
                               I_supplier                    IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                               I_location                    IN     COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                               I_loc_type                    IN     COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE,
                               I_calling_form                IN     VARCHAR2,
                               I_origin_country_id           IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                               I_delivery_country_id         IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE,
                               I_prim_dlvy_ctry_ind          IN     ITEM_COST_HEAD.PRIM_DLVY_CTRY_IND%TYPE,
                               I_update_itemcost_ind         IN     VARCHAR2,
                               I_update_itemcost_child_ind   IN     VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
END ITEM_COST_SQL;
/