CREATE OR REPLACE PACKAGE BODY STORE_GRADE_SQL AS
---------------------------------------------------------------------------------------------
-- Function Name: FINISH_ORDER
-- Purpose:       This function will distribute an item by store grade on the basis of quantity, 
--                percentage or ratio.It will update the orderloc_wksht table based on  distribution 
--                information present in the Store_grade_dist_temp table.
---------------------------------------------------------------------------------------------
FUNCTION FINISH_ORDER( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                       I_dist_uom_type   IN       ORDLOC_WKSHT.UOP%TYPE,
                       I_dist_uom        IN       ORDLOC_WKSHT.UOP%TYPE,
                       I_purch_uom       IN       ORDLOC_WKSHT.UOP%TYPE,
                       I_distribute_by   IN       VARCHAR2)
RETURN BOOLEAN IS
   L_statement           VARCHAR2(4000);
   L_where_clause        FILTER_TEMP.WHERE_CLAUSE%TYPE;
   L_ratio               NUMBER;
   L_sum_ratio           VARCHAR2(30);
   L_order_no            ORDHEAD.ORDER_NO%TYPE;
   L_qty_string          VARCHAR2(2000);
   L_calc_qty_string     VARCHAR2(2000);
   L_act_qty_string      VARCHAR2(2000);
   L_var_qty_string      VARCHAR2(2000);
   L_wksht_qty_string    VARCHAR2(2000);
   L_program             VARCHAR2(30) :='STORE_GRADE_SQL.FINISH_ORDER';

   cursor C_RATIO is
      select SUM(NVL(dist_ratio,0))
        from store_grade_dist_temp
       where order_no = I_order_no;
  
   cursor C_FETCH_WHERE is
      select where_clause
        from filter_temp
       where form_name = 'ORDMTXWS'
         and unique_key = I_order_no;
   
BEGIN  
   
    ---
   if I_order_no is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_order_no', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_dist_uom_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_dist_uom_type', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_dist_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_dist_uom', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_purch_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_purch_uom', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_distribute_by is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_distribute_by', 
                                             L_program, 
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_FETCH_WHERE;
   fetch C_FETCH_WHERE into L_where_clause;
   close C_FETCH_WHERE;
   ---
   if L_where_clause is not NULL then
      L_where_clause := 'and '||L_where_clause;
   end if;
   ---
   open C_RATIO;
   fetch C_RATIO into L_ratio;
   close C_RATIO;
   ---
   L_sum_ratio := to_char(L_ratio);
   L_order_no := I_order_no;
    ---
   --If the dist_uom_type is 'C', the unit of purchase must be case.
   --If the dist_uom_type is either 'S'UOM or 'E'aches, the uop
   --may be either the SUOM or case.  If the uop is case and the dist_uom_type
   --is either 'S' or 'E', quantity entered by the user should be divided by the
   --supp_pack_size.
   ---
   if I_dist_uom_type = 'C' then
      ---
      if I_distribute_by = 'Q' then
         L_qty_string := 'store_grade_dist_temp.dist_qty ';
      elsif I_distribute_by = 'R' then
         L_qty_string := '((store_grade_dist_temp.dist_ratio * ordloc_wksht.wksht_qty)/ '||L_sum_ratio||') ';
      elsif I_distribute_by = 'D' then
         L_qty_string := '((store_grade_dist_temp.dist_pct * ordloc_wksht.wksht_qty)/100)';
      end if;
      --
      L_wksht_qty_string := 'ROUND(('||L_qty_string||'))';
      L_calc_qty_string := '('||L_qty_string ||' * ordloc_wksht.supp_pack_size)';
      L_act_qty_string := '('||L_wksht_qty_string ||' * ordloc_wksht.supp_pack_size)';
   else
      ---
       if I_dist_uom = I_purch_uom then
         ---
         if I_distribute_by = 'Q' then
            L_qty_string := 'store_grade_dist_temp.dist_qty ';
         elsif I_distribute_by = 'R' then
            L_qty_string := '((store_grade_dist_temp.dist_ratio * ordloc_wksht.wksht_qty)/ '||L_sum_ratio||')';
         elsif I_distribute_by = 'D' then
            L_qty_string := '((store_grade_dist_temp.dist_pct * ordloc_wksht.wksht_qty)/100)';
         end if;
         ---
         if I_dist_uom_type = 'E' then
            L_wksht_qty_string := 'ROUND(to_number('||L_qty_string||'))';
            L_calc_qty_string := L_qty_string;
            L_act_qty_string := L_qty_string;
         elsif I_dist_uom_type = 'S' then
            L_wksht_qty_string := 'ROUND('||L_qty_string||')';
            L_calc_qty_string := L_qty_string;
            L_act_qty_string := L_qty_string;
         end if;
         ---
      elsif I_dist_uom != I_purch_uom then
         ---
         if I_distribute_by = 'Q' then
            L_qty_string := 'store_grade_dist_temp.dist_qty ';
         elsif I_distribute_by = 'R' then
            L_qty_string := '((store_grade_dist_temp.dist_ratio * ordloc_wksht.act_qty)/ '||L_sum_ratio||') ';
         elsif I_distribute_by = 'D' then
            L_qty_string := '((store_grade_dist_temp.dist_pct * ordloc_wksht.act_qty)/100)';
         end if;
         ---
         if I_dist_uom_type = 'E' then
            L_wksht_qty_string := 'ROUND(('||L_qty_string||')/ordloc_wksht.supp_pack_size)';
            L_calc_qty_string := L_qty_string;
            L_act_qty_string := L_qty_string;
         elsif I_dist_uom_type = 'S' then
            L_wksht_qty_string := 'ROUND(('||L_qty_string||')/ordloc_wksht.supp_pack_size)';
            L_calc_qty_string := L_qty_string;
            L_act_qty_string := L_qty_string;
         end if;
         ---
       end if;  ---
    end if;   
   L_var_qty_string := '((('||L_act_qty_string ||'-'||L_calc_qty_string ||')*100)/'||'DECODE('||L_calc_qty_string ||',0, 1)'||')';      
   ---
   if I_distribute_by = 'R' then
      ---
      if UPDATE_OBJECT_SQL.UPDATE_ORDER_GRADE( O_error_message,
                                               L_order_no,
                                               I_distribute_by,
                                               L_sum_ratio,
                                               L_where_clause,
                                               I_dist_uom_type) = FALSE then
        return FALSE;
      end if;
      ---
   else
      ---
      if UPDATE_OBJECT_SQL.UPDATE_ORDER_GRADE( O_error_message,
                                               L_order_no,
                                               I_distribute_by,
                                               NULL,
                                               L_where_clause,
                                               I_dist_uom_type) = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   ---
      
   L_statement := 'insert into ordloc_wksht(order_no, '||
                                         'item_parent, '||
                                         'item, '||
                                         'ref_item, '||
                                         'diff_1, '||
                                         'diff_2, '||
                                         'diff_3, '||
                                         'diff_4, '||
                                         'store_grade, '||
                                         'store_grade_group_id, '||
                                         'loc_type, '||
                                         'location, '||
                                         'calc_qty, '||
                                         'act_qty, '||
                                         'standard_uom, '||
                                         'variance_qty, '||
                                         'origin_country_id, '||
                                         'wksht_qty, '||
                                         'uop, '||
                                         'supp_pack_size) '||
                                  'select ordloc_wksht.order_no, '||
                                         'ordloc_wksht.item_parent, '||
                                         'ordloc_wksht.item, '||
                                         'ordloc_wksht.ref_item, '||
                                         'ordloc_wksht.diff_1, '||
                                         'ordloc_wksht.diff_2, '||
                                         'ordloc_wksht.diff_3, '||
                                         'ordloc_wksht.diff_4, '||
                                         'store_grade_dist_temp.store_grade, '||
                                         'store_grade_dist_temp.store_grade_group_id, '||
                                         'ordloc_wksht.loc_type, '||
                                         'ordloc_wksht.location, '||
                                         L_act_qty_string ||', ' ||
                                         L_calc_qty_string ||', ' ||
                                         'ordloc_wksht.standard_uom, '||
                                         L_var_qty_string ||', ' ||
                                         'ordloc_wksht.origin_country_id, '||
                                         L_wksht_qty_string||', ' ||
                                         'ordloc_wksht.uop, ' ||
                                         'ordloc_wksht.supp_pack_size '||
                    'from ordloc_wksht, store_grade_dist_temp '||
                   'where ordloc_wksht.order_no = store_grade_dist_temp.order_no '||
                     'and ordloc_wksht.order_no = '||to_char(L_order_no)||' ' || L_where_clause||' '||
                       ' and not exists (select ' || '''x''' || ' from ordloc_wksht o2 ' ||
                                         'where o2.order_no = ordloc_wksht.order_no ' ||
                                           'and nvl(o2.item_parent, -1) = nvl(ordloc_wksht.item_parent, -1) ' ||
                                           'and nvl(o2.item, -1) = nvl(ordloc_wksht.item, -1) ' ||
                                           'and nvl(o2.diff_1, -1) = nvl(ordloc_wksht.diff_1, -1) ' ||
                                           'and nvl(o2.diff_2, -1) = nvl(ordloc_wksht.diff_2, -1) ' ||
                                           'and nvl(o2.diff_3, -1) = nvl(ordloc_wksht.diff_3, -1) ' ||
                                           'and nvl(o2.diff_4, -1) = nvl(ordloc_wksht.diff_4, -1) ' ||
                                           'and nvl(o2.loc_type, -1) = nvl(ordloc_wksht.loc_type, -1) ' ||
                                           'and nvl(o2.location, -1) = nvl(ordloc_wksht.location, -1) ' ||
                                           'and o2.store_grade_group_id = store_grade_dist_temp.store_grade_group_id '||
                                           'and o2.store_grade = store_grade_dist_temp.store_grade);';
   
   if EXECUTE_SQL.EXECUTE_SQL(O_error_message, L_statement) = FALSE then
      return FALSE;
   end if; 
   ---
   L_statement := 'delete from ordloc_wksht where order_no = '||to_char(L_order_no) ||
                  ' and ordloc_wksht.store_grade is NULL '||L_where_clause||';';
   ---
   
   if EXECUTE_SQL.EXECUTE_SQL(O_error_message, L_statement) = FALSE then
      return FALSE;
  end if;
  
  delete from store_grade_dist_temp;
  
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END FINISH_ORDER;
-------------------------------------------------------------------------------------------------------------------------
END STORE_GRADE_SQL;
/