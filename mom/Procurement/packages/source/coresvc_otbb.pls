CREATE OR REPLACE PACKAGE BODY CORESVC_OTB AS
   cursor C_SVC_OTB(I_process_id NUMBER,
                    I_chunk_id   NUMBER) is
      select pk_otb.rowid        as pk_otb_rid,
             st.rowid            as st_rid,
             otb_scl_fk.rowid    as otb_scl_fk_rid,
             st.a_budget_amt,
             st.b_budget_amt,
             st.n_budget_amt,
             st.eow_date,
             st.subclass,
             st.class,
             st.dept,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)    as action,
             st.process$status
        from svc_otb st,
             otb pk_otb,
             subclass otb_scl_fk,
             dual
       where st.process_id   = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.dept         = pk_otb.dept (+)
         and st.class        = pk_otb.class (+)
         and st.subclass     = pk_otb.subclass (+)
         and st.eow_date     = pk_otb.eow_date (+)
         and st.subclass     = otb_scl_fk.subclass (+)
         and st.class        = otb_scl_fk.class (+)
         and st.dept         = otb_scl_fk.dept (+);
   TYPE LP_errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab     LP_errors_tab_typ;
   TYPE LP_s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab LP_s9t_errors_tab_typ;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS( I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet     IN   VARCHAR2,
                           I_row_seq   IN   NUMBER,
                           I_col       IN   VARCHAR2,
                           I_sqlcode   IN   NUMBER,
                           I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := S9T_ERRORS_SEQ.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(CASE
                                                                        WHEN I_sqlcode IS NULL THEN
                                                                             I_sqlerrm
                                                                        ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                        END);
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
----------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
-------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   NUMBER) IS
   L_sheets     s9t_pkg.names_map_typ;
   L_otb_cols   s9t_pkg.names_map_typ;
BEGIN
   L_sheets              := S9T_PKG.GET_SHEET_NAMES(I_file_id);
   L_otb_cols            := S9T_PKG.GET_COL_NAMES(I_file_id,
                                                  OTB_sheet);
   OTB$ACTION            := L_otb_cols('ACTION');
   OTB$A_BUDGET_AMT      := L_otb_cols('A_BUDGET_AMT');
   OTB$B_BUDGET_AMT      := L_otb_cols('B_BUDGET_AMT');
   OTB$N_BUDGET_AMT      := L_otb_cols('N_BUDGET_AMT');
   OTB$EOW_DATE          := L_otb_cols('EOW_DATE');
   OTB$SUBCLASS          := L_otb_cols('SUBCLASS');
   OTB$CLASS             := L_otb_cols('CLASS');
   OTB$DEPT              := L_otb_cols('DEPT');
END POPULATE_NAMES;
--------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_OTB( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = OTB_sheet )
   select s9t_row(s9t_cells(CORESVC_OTB.action_mod ,
                            dept,
                            class,
                            subclass,
                            eow_date,
                            n_budget_amt,
                            a_budget_amt,
                            b_budget_amt ))
     from otb 
    where eow_date >= GET_VDATE;
END POPULATE_OTB;
------------------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := S9T_FOLDER_SEQ.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG ;
   L_file.add_sheet(OTB_sheet);
   L_file.sheets(l_file.get_sheet_index(OTB_sheet)).column_headers := s9t_cells(  'ACTION' ,
                                                                                  'DEPT' ,  
                                                                                  'CLASS',
                                                                                  'SUBCLASS',
                                                                                  'EOW_DATE',
                                                                                  'N_BUDGET_AMT',
                                                                                  'A_BUDGET_AMT',
                                                                                  'B_BUDGET_AMT');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
------------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64):= 'CORESVC_OTB.CREATE_S9T';
   L_file      s9t_file;                                  
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_OTB(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:= S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
----------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_OTB( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                           I_process_id   IN   SVC_OTB.PROCESS_ID%TYPE) IS
   TYPE svc_otb_col_typ IS TABLE OF SVC_OTB%ROWTYPE;
   L_temp_rec           SVC_OTB%ROWTYPE;
   svc_otb_col          svc_otb_col_typ := NEW svc_otb_col_typ();
   L_process_id         SVC_OTB.PROCESS_ID%TYPE;
   L_error              BOOLEAN:= FALSE;
   L_default_rec        SVC_OTB%ROWTYPE;
	
   cursor C_MANDATORY_IND is
      select a_budget_amt_mi,
             b_budget_amt_mi,
             n_budget_amt_mi,
             eow_date_mi,
             subclass_mi,
             class_mi,
             dept_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key          = template_key
                 and wksht_key             = 'OTB_BUDGET'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('A_BUDGET_AMT' as a_budget_amt,
                                            'B_BUDGET_AMT' as b_budget_amt,
                                            'N_BUDGET_AMT' as n_budget_amt,
                                            'EOW_DATE'     as eow_date,
                                            'SUBCLASS'     as subclass,
                                            'CLASS'        as class,
                                            'DEPT'         as dept,
                                             null          as dummy));
      L_mi_rec C_MANDATORY_IND%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
		L_table         VARCHAR2(30)   := 'SVC_OTB';
      L_pk_columns    VARCHAR2(255)  := 'Department,Class,Subclass,Week Ending';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select  a_budget_amt_dv,
                       b_budget_amt_dv,
                       n_budget_amt_dv,
                       eow_date_dv,
                       subclass_dv,
                       class_dv,
                       dept_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key        = template_key
                          and wksht_key           = 'OTB_BUDGET'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'A_BUDGET_AMT' as a_budget_amt,
                                                      'B_BUDGET_AMT' as b_budget_amt,
                                                      'N_BUDGET_AMT' as n_budget_amt,
                                                      'EOW_DATE'     as eow_date,
                                                      'SUBCLASS'     as subclass,
                                                      'CLASS'        as class,
                                                      'DEPT'         as dept,
                                                       NULL          as dummy)))
   LOOP
      BEGIN
         L_default_rec.a_budget_amt := rec.a_budget_amt_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'OTB ' ,
                            NULL,
                           'A_BUDGET_AMT ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.b_budget_amt := rec.b_budget_amt_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'OTB ' ,
                            NULL,
                           'B_BUDGET_AMT ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.n_budget_amt := rec.n_budget_amt_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'OTB ' ,
                            NULL,
                           'N_BUDGET_AMT ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.eow_date := rec.eow_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'OTB ' ,
                            NULL,
                           'EOW_DATE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.subclass := rec.subclass_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'OTB ' ,
                            NULL,
                           'SUBCLASS ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.class := rec.class_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'OTB ' ,
                            NULL,
                           'CLASS ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.dept := rec.dept_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'OTB ' ,
                            NULL,
                           'DEPT ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(OTB$ACTION)                    as action,
                      r.get_cell(OTB$A_BUDGET_AMT)              as a_budget_amt,
                      r.get_cell(OTB$B_BUDGET_AMT)              as b_budget_amt,
                      r.get_cell(OTB$N_BUDGET_AMT)              as n_budget_amt,
                      r.get_cell(OTB$EOW_DATE)                  as eow_date,
                      r.get_cell(OTB$SUBCLASS)                  as subclass,
                      r.get_cell(OTB$CLASS)                     as class,
                      r.get_cell(OTB$DEPT)                      as dept,
                      r.get_row_seq()                           as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(OTB_sheet))
   LOOP
	   L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OTB_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.a_budget_amt := rec.a_budget_amt;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OTB_sheet,
                            rec.row_seq,
                            'A_BUDGET_AMT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.b_budget_amt := rec.b_budget_amt;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OTB_sheet, 
                            rec.row_seq,
                            'B_BUDGET_AMT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.n_budget_amt := rec.n_budget_amt;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OTB_sheet,
                            rec.row_seq,
                            'N_BUDGET_AMT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.eow_date := rec.eow_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OTB_sheet,
                            rec.row_seq,
                            'EOW_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.subclass := rec.subclass;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OTB_sheet,
                            rec.row_seq,
                            'SUBCLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.class := rec.class;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OTB_sheet,
                            rec.row_seq,
                            'CLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.dept := rec.dept;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OTB_sheet,
                            rec.row_seq,
                            'DEPT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_OTB.action_new then
         L_temp_rec.a_budget_amt := NVL( L_temp_rec.a_budget_amt,L_default_rec.a_budget_amt);
         L_temp_rec.b_budget_amt := NVL( L_temp_rec.b_budget_amt,L_default_rec.b_budget_amt);
         L_temp_rec.n_budget_amt := NVL( L_temp_rec.n_budget_amt,L_default_rec.n_budget_amt);
         L_temp_rec.eow_date     := NVL( L_temp_rec.eow_date,L_default_rec.eow_date);
         L_temp_rec.subclass     := NVL( L_temp_rec.subclass,L_default_rec.subclass);
         L_temp_rec.class        := NVL( L_temp_rec.class,L_default_rec.class);
         L_temp_rec.dept         := NVL( L_temp_rec.dept,L_default_rec.dept);
      end if;
      if not ( L_temp_rec.dept is NOT NULL and
               L_temp_rec.class is NOT NULL and
               L_temp_rec.subclass is NOT NULL and
               L_temp_rec.eow_date is NOT NULL and
               1 = 1)then
         WRITE_S9T_ERROR( I_file_id,
                       OTB_sheet,
                       rec.row_seq,
                       NULL,
                       NULL,
                       SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_otb_col.extend();
         svc_otb_col(svc_otb_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_otb_col.COUNT SAVE EXCEPTIONS
      merge into SVC_OTB st
      using(select
                  (case
                   when L_mi_rec.a_budget_amt_mi    = 'N'
                    and svc_otb_col(i).action = CORESVC_OTB.action_mod
                    and s1.a_budget_amt IS NULL
                   then mt.a_budget_amt
                   else s1.a_budget_amt
                   end) as a_budget_amt,
                  (case
                   when L_mi_rec.b_budget_amt_mi    = 'N'
                    and svc_otb_col(i).action = CORESVC_OTB.action_mod
                    and s1.b_budget_amt IS NULL
                   then mt.b_budget_amt
                   else s1.b_budget_amt
                   end) as b_budget_amt,
                  (case
                   when L_mi_rec.n_budget_amt_mi    = 'N'
                    and svc_otb_col(i).action = CORESVC_OTB.action_mod
                    and s1.n_budget_amt IS NULL
                   then mt.n_budget_amt
                   else s1.n_budget_amt
                   end) as n_budget_amt,
                  (case
                   when L_mi_rec.eow_date_mi    = 'N'
                    and svc_otb_col(i).action = CORESVC_OTB.action_mod
                    and s1.eow_date IS NULL
                   then mt.eow_date
                   else s1.eow_date
                   end) as eow_date,
                  (case
                   when L_mi_rec.subclass_mi    = 'N'
                    and svc_otb_col(i).action = CORESVC_OTB.action_mod
                    and s1.subclass IS NULL
                   then mt.subclass
                   else s1.subclass
                   end) as subclass,
                  (case
                   when L_mi_rec.class_mi    = 'N'
                    and svc_otb_col(i).action = CORESVC_OTB.action_mod
                    and s1.class IS NULL
                   then mt.class
                   else s1.class
                   end) as class,
                  (case
                   when L_mi_rec.dept_mi    = 'N'
                    and svc_otb_col(i).action = CORESVC_OTB.action_mod
                    and s1.dept IS NULL
                   then mt.dept
                   else s1.dept
                   end) as dept,
                  null as dummy
              from (select svc_otb_col(i).a_budget_amt as a_budget_amt,
                           svc_otb_col(i).b_budget_amt as b_budget_amt,
                           svc_otb_col(i).n_budget_amt as n_budget_amt,
                           svc_otb_col(i).eow_date     as eow_date,
                           svc_otb_col(i).subclass     as subclass,
                           svc_otb_col(i).class        as class,
                           svc_otb_col(i).dept         as dept,
                           null as dummy
                    from dual ) s1,
                            OTB mt
             where mt.dept (+)         = s1.dept       and
                   mt.class (+)        = s1.class      and
                   mt.subclass (+)     = s1.subclass   and
                   mt.eow_date (+)     = s1.eow_date   and
                   1 = 1 )sq
                on (st.dept          = sq.dept     and
                    st.class         = sq.class    and
                    st.subclass      = sq.subclass and
                    st.eow_date      = sq.eow_date and
                    svc_otb_col(i).ACTION IN (CORESVC_OTB.action_mod,CORESVC_OTB.action_del))
      when matched then
      update
         set process_id        = svc_otb_col(i).process_id ,
             chunk_id          = svc_otb_col(i).chunk_id ,
             row_seq           = svc_otb_col(i).row_seq ,
             action            = svc_otb_col(i).action ,
             process$status    = svc_otb_col(i).process$status ,
             n_budget_amt      = sq.n_budget_amt ,
             b_budget_amt      = sq.b_budget_amt ,
             a_budget_amt      = sq.a_budget_amt ,
             create_id         = svc_otb_col(i).create_id ,
             create_datetime   = svc_otb_col(i).create_datetime ,
             last_upd_id       = svc_otb_col(i).last_upd_id ,
             last_upd_datetime = svc_otb_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             a_budget_amt ,
             b_budget_amt ,
             n_budget_amt ,
             eow_date ,
             subclass ,
             class ,
             dept ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_otb_col(i).process_id ,
             svc_otb_col(i).chunk_id ,
             svc_otb_col(i).row_seq ,
             svc_otb_col(i).action ,
             svc_otb_col(i).process$status ,
             sq.a_budget_amt ,
             sq.b_budget_amt ,
             sq.n_budget_amt ,
             sq.eow_date ,
             sq.subclass ,
             sq.class ,
             sq.dept ,
             svc_otb_col(i).create_id ,
             svc_otb_col(i).create_datetime ,
             svc_otb_col(i).last_upd_id ,
             svc_otb_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
				   if L_error_code=1 then
				      L_error_code:=NULL;
				      L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
				   end if;
               WRITE_S9T_ERROR( I_file_id,
                                OTB_sheet,
                                svc_otb_col(sql%bulk_exceptions(i).error_index).row_seq,
                                NULL,
                                L_error_code,
                                L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_OTB;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT       RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count     IN OUT       NUMBER,
                      I_file_id         IN           S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id      IN           NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64) := 'CORESVC_OTB.PROCESS_S9T';
   L_file           s9t_file;
   L_sheets         s9t_pkg.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	    EXCEPTION;
   PRAGMA	    EXCEPTION_INIT(MAX_CHAR, -01706);   
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_OTB(I_file_id,
                      I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
	 insert into s9t_errors
	      values Lp_s9t_errors_tab(i);

      update svc_process_tracker
	 set status = 'PE',
	     file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_OTB_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error           IN OUT   BOOLEAN,
                         I_rec             IN       C_SVC_OTB%ROWTYPE)                       
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_OTB.PROCESS_OTB_VAL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_OTB';
   O_eow_date      DATE;
   L_dept_name     V_DEPS.DEPT_NAME%TYPE; 
   L_class         V_CLASS.CLASS_NAME%TYPE;
   L_subclass      V_SUBCLASS.SUB_NAME%TYPE;   
   L_valid         BOOLEAN                           := FALSE;
   
BEGIN
   if I_rec.action = action_new then
	     if NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_DEPS(O_error_message,
                                                      L_valid,
                                                      L_dept_name,
                                                      I_rec.dept) 
		   or (L_valid = FALSE) then													   
           WRITE_ERROR(I_rec.process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_rec.chunk_id,
                       L_table,
                       I_rec.row_seq,
                       'DEPT',
                       O_error_message);
            O_error :=TRUE;
         end if;
         if NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_CLASS(O_error_message,
                                                       L_valid,
                                                       L_class,
                                                       I_rec.dept,
                                                       I_rec.class) 
            or (L_valid = FALSE) then													
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'Department,Class',
                        O_error_message);
            O_error :=TRUE;
         end if;
		 if NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_SUBCLASS(O_error_message,
                                                          L_valid,
                                                          L_subclass,
                                                          I_rec.dept,
                                                          I_rec.class,
                                                          I_rec.subclass) 
            or (L_valid = FALSE) then													   
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'Department,Class,Subclass',
                        O_error_message);
            O_error :=TRUE;
         end if;
	  end if;
   if I_rec.action = action_new then
      if DATES_SQL.GET_EOW_DATE(O_error_message,
                                O_eow_date,
                                I_rec.eow_date) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'EOW_DATE',
                     O_error_message);
         O_error := TRUE;
      end if;
      if (O_eow_date != I_rec.eow_date) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'EOW_DATE',
                     'INV_EOW_DATE');
         O_error := TRUE;
      end if;
   end if;   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_OTB_VAL;
--------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_OTB_INS( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,  
                       I_otb_temp_rec      IN       OTB%ROWTYPE)                                  
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_OTB.EXEC_OTB_INS';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_OTB';
BEGIN
   insert
     into otb
   values I_otb_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_OTB_INS;
---------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_OTB_UPD( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,  
                       I_otb_temp_rec      IN       OTB%ROWTYPE)
                        
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_OTB.EXEC_OTB_UPD';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_OTB';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);
   cursor C_OTB_LOCK is 
      select 'X'
        from otb
       where dept    = I_otb_temp_rec.dept
         and class    = I_otb_temp_rec.class
         and subclass = I_otb_temp_rec.subclass
         and eow_date = I_otb_temp_rec.eow_date
         for update nowait;
BEGIN
   open C_OTB_LOCK;
   close C_OTB_LOCK;
   update otb
      set row = I_otb_temp_rec
    where 1 = 1
      and dept     = I_otb_temp_rec.dept
      and class    = I_otb_temp_rec.class
      and subclass = I_otb_temp_rec.subclass
      and eow_date = I_otb_temp_rec.eow_date;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_otb_temp_rec.dept,  
                                             I_otb_temp_rec.class);
      return FALSE;											 
   when OTHERS then
      if C_OTB_LOCK%ISOPEN then
	     close C_OTB_LOCK;
	  end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_OTB_UPD;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_OTB( O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE, 
                      I_process_id       IN       SVC_OTB.PROCESS_ID%TYPE,
                      I_chunk_id         IN       SVC_OTB.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_OTB.PROCESS_OTB';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_OTB';
   L_error         BOOLEAN;
   L_process_error BOOLEAN                           := FALSE;
   L_otb_temp_rec  OTB%ROWTYPE;
   L_dd            NUMBER(2);
   L_mm            NUMBER(2);
   L_yyyy          NUMBER(4);
   L_out_dd        NUMBER(2);
   L_out_yyyy      NUMBER(4);
   L_return        VARCHAR2(5);
   L_error_454     VARCHAR2(255);
BEGIN
   FOR rec IN C_SVC_OTB(I_process_id,
                        I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.pk_otb_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                    'Department,Class,Subclass,Week Ending',
                    'PK_OTB');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod)
         and rec.pk_otb_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                    'Department,Class,Subclass,Week Ending',
                    'PK_OTB_MISSING');
         L_error := TRUE;
      end if;
      if PROCESS_OTB_VAL( O_error_message,
                          L_error,
                          rec ) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;
      if NOT L_error then
         L_otb_temp_rec.eow_date                  := rec.eow_date;
         L_otb_temp_rec.subclass                  := rec.subclass;
         L_otb_temp_rec.class                     := rec.class;
         L_otb_temp_rec.dept                      := rec.dept;
         L_otb_temp_rec.n_budget_amt              := NVL(rec.n_budget_amt,0);
         L_otb_temp_rec.n_receipts_amt            := 0;
         L_otb_temp_rec.n_approved_amt            := 0;
         L_otb_temp_rec.a_budget_amt              := NVL(rec.a_budget_amt,0);
         L_otb_temp_rec.a_receipts_amt            := 0;
         L_otb_temp_rec.a_approved_amt            := 0;
         L_otb_temp_rec.b_budget_amt              := NVL(rec.b_budget_amt,0);
         L_otb_temp_rec.b_receipts_amt            := 0;
         L_otb_temp_rec.b_approved_amt            := 0;
         L_otb_temp_rec.cancel_amt                := 0;
         L_dd                                     := to_number(to_char(L_otb_temp_rec.eow_date,'DD'),'09');
	     L_mm                                     := to_number(to_char(L_otb_temp_rec.eow_date,'MM'),'09');
	     L_yyyy                                   := to_number(to_char(L_otb_temp_rec.eow_date,'YYYY'),'0999');
         --call to generate week_no and month_no
         CAL_TO_454 (L_dd, 
                     L_mm, 
                     L_yyyy,
                     L_out_dd, 
                     L_otb_temp_rec.week_no,
                     L_otb_temp_rec.month_no, 
                     L_out_yyyy,
                     L_return, 
                     L_error_454);
         if L_return = 'FALSE' then
   	        WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'EOW_DATE',
                        L_error_454);
            L_process_error :=TRUE;   
         end if;
         --call to generate half_no
         CAL_TO_454_HALF (L_dd,
                          L_mm, 
                          L_yyyy, 
                          L_otb_temp_rec.half_no, 
                          L_otb_temp_rec.month_no,
                          L_return, 
                          L_error_454);
         if L_return = 'FALSE' then
   	        WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'EOW_DATE',
                        L_error_454);
            L_process_error :=TRUE;   
         end if;
         if(L_otb_temp_rec.n_budget_amt < 0) then
            L_otb_temp_rec.n_budget_amt := 0;
         end if;
         if(L_otb_temp_rec.a_budget_amt < 0) then
            L_otb_temp_rec.a_budget_amt := 0;
         end if;
         if(L_otb_temp_rec.b_budget_amt < 0) then
            L_otb_temp_rec.b_budget_amt := 0;
         end if;
         if rec.action = action_new then
            if EXEC_OTB_INS( O_error_message,  
                             L_otb_temp_rec )=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_OTB_UPD( O_error_message, 
                             L_otb_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_OTB;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete from svc_otb 
      where process_id = I_process_id;

END;
---------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_OTB.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS'; 
BEGIN
   LP_errors_tab := NEW LP_errors_tab_typ();
   if PROCESS_OTB( O_error_message,
                   I_process_id,
                   I_chunk_id)=FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW LP_errors_tab_typ();
   
   if O_error_count    = 0 THEN
      L_process_status := 'PS';
	else
	   L_process_status := 'PE';
	end if;
		
   update svc_process_tracker
		  set status = (CASE
			              when status = 'PE'
			              then 'PE'
                    else L_process_status
                    END),
		      action_date = SYSDATE
		where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);     
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
----------------------------------------------------------------------------------------------
END CORESVC_OTB;
/