CREATE OR REPLACE PACKAGE ORDER_ATTRIB_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------
-- Function Name: FREIGHT_TERMS_DESC
-- Purpose: accepts freight code and returns freight terms description
-------------------------------------------------------------------
FUNCTION FREIGHT_TERMS_DESC(O_error_message   IN OUT   VARCHAR2,
                            I_code            IN       VARCHAR2,
                            O_desc            IN OUT   VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: PAY_TERMS_DESC
-- Purpose: accepts freight code and returns freight terms code and description
-------------------------------------------------------------------
FUNCTION PAY_TERMS_DESC(O_error_message   IN OUT   VARCHAR2,
                        I_code            IN       VARCHAR2,
                        O_terms_code      IN OUT   VARCHAR2,
                        O_desc            IN OUT   VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function : GET_PO_TYPE_DESC
-- Purpose  : to get the purchase order type given a PO type
--                indicator.
---------------------------------------------------------------------
FUNCTION GET_PO_TYPE_DESC(O_error_message   IN OUT   VARCHAR2,
                          I_PO_TYPE         IN       VARCHAR2,
                          O_PO_TYPE_DESC    IN OUT   VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : DELIVERY_MONTH
-- Purpose  : return the order's delivery month
----------------------------------------------------------------------
FUNCTION DELIVERY_MONTH(O_error_message   IN OUT   VARCHAR2,
                       O_delivery_month   IN OUT   NUMBER,
                       I_not_after_date   IN OUT   ORDHEAD.NOT_AFTER_DATE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------
-- Function : GET_TOTAL_UOM
-- Purpose  : This function will return the total quantityand/or the
--            prescaled qty for an order, an order/item, an order/location
--            or an order/item/location for a specified UOM.  The
--            I_order_qty_type parameter determines if the quantity should
--            be retrieved only for the order quantity (O), only for the
---           pre-scaled quantity (P) or for both (B).
---------------------------------------------------------------------
FUNCTION GET_TOTAL_UOM(I_order_no         IN       ORDLOC.ORDER_NO%TYPE,
                       I_item             IN       ORDLOC.ITEM%TYPE,
                       I_location         IN       ORDLOC.LOCATION%TYPE,
                       I_uom              IN       UOM_CLASS.UOM%TYPE,
                       I_supplier         IN       SUPS.SUPPLIER%TYPE,
                       I_order_qty_type   IN       VARCHAR2,
                       O_quantity         IN OUT   ORDLOC.QTY_ORDERED%TYPE,
                       O_qty_prescaled    IN OUT   ORDLOC.QTY_PRESCALED%TYPE,
                       O_error_message    IN OUT   VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_COUNTRY_OF_ORIGIN
-- Purpose  : Determines the origin country at the PO header level
--            based on the country of origins at the PO item level.
---------------------------------------------------------------------
FUNCTION GET_COUNTRY_OF_ORIGIN (O_error_message       IN OUT   VARCHAR2,
                                O_country_of_origin   IN OUT   ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                I_order_no            IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_WRITTEN_DATE
-- Purpose  : Retrieve the written date off of the ordhead table for a given order
---------------------------------------------------------------------
FUNCTION GET_WRITTEN_DATE (O_error_message   IN OUT   VARCHAR2,
                           O_written_date    IN OUT   ORDHEAD.WRITTEN_DATE%TYPE,
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_CURRENCY_RATE
-- Purpose  : Retrieve the currency code and exchange rate off of ordhead
-- for a given order
---------------------------------------------------------------------
FUNCTION GET_CURRENCY_RATE (O_error_message   IN OUT   VARCHAR2,
                            O_currency_code   IN OUT   ORDHEAD.CURRENCY_CODE%TYPE,
                            O_exchange_rate   IN OUT   ORDHEAD.EXCHANGE_RATE%TYPE,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_DIS_PORT
-- Purpose  : Retrieves the discharge port off of ordhead for a given order
---------------------------------------------------------------------
FUNCTION GET_DIS_PORT(O_error_message    IN OUT   VARCHAR2,
                      O_discharge_port   IN OUT   ORDHEAD.DISCHARGE_PORT%TYPE,
                      I_order_no         IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_LAD_PORT
-- Purpose  : Retrieves the lading port off of ordhead for a given order
---------------------------------------------------------------------
FUNCTION GET_LAD_PORT(O_error_message   IN OUT   VARCHAR2,
                      O_lading_port     IN OUT   ORDHEAD.LADING_PORT%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : ITEMS_EXIST
-- Purpose  : Checks if items exist against the order for a given order
---------------------------------------------------------------------
FUNCTION ITEMS_EXIST(O_error_message   IN OUT   VARCHAR2,
                     O_exists          IN OUT   BOOLEAN,
                     I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_SHIP_DATES
-- Purpose  : Retrieves the earliest and latest ship dates for an order
---------------------------------------------------------------------
FUNCTION GET_SHIP_DATES(O_error_message        IN OUT   VARCHAR2,
                        O_earliest_ship_date   IN OUT   ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
                        O_latest_ship_date     IN OUT   ORDHEAD.LATEST_SHIP_DATE%TYPE,
                        I_order_no             IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_RECEIPT_DATES
-- Purpose  : This function will return the not before and not after
--            dates for a given order number.
---------------------------------------------------------------------
FUNCTION GET_RECEIPT_DATES (O_error_message     IN OUT   VARCHAR2,
                            O_not_before_date   IN OUT   ORDHEAD.NOT_BEFORE_DATE%TYPE,
                            O_not_after_date    IN OUT   ORDHEAD.NOT_AFTER_DATE%TYPE,
                            I_order_no          IN       ORDHEAD.ORDER_NO%TYPE)
      RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_IMPORT_IND
-- Purpose  : Retrieves the import indicator for an order
---------------------------------------------------------------------
FUNCTION GET_IMPORT_IND(O_error_message      IN OUT   VARCHAR2,
                        O_import_order_ind   IN OUT   ORDHEAD.IMPORT_ORDER_IND%TYPE,
                        I_order_no           IN       ORDHEAD.ORDER_NO%TYPE )
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_IMPORT_COUNTRY
-- Purpose  : Retrieves the import country for an order
---------------------------------------------------------------------
FUNCTION GET_IMPORT_COUNTRY(O_error_message       IN OUT   VARCHAR2,
                            O_import_country_id   IN OUT   ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                            I_order_no            IN       ORDHEAD.ORDER_NO%TYPE )
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_PREPACK_IND
-- Purpose  : This function returns the fash_prepack_ind if exists.
----------------------------------------------------------------------
FUNCTION GET_PREPACK_IND(O_error_message   IN OUT   VARCHAR2,
                         O_exists          IN OUT   BOOLEAN,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------
-- Function : GET_TOTAL_UNITS
-- Purpose : This function gets the total qty ordered or allocated and
--           the total pre-scaled qty for a given order, order/item,
--           order/item/location, or order/alloc/item/location combination.
----------------------------------------------------------------------
FUNCTION GET_TOTAL_UNITS(O_error_message         IN OUT   VARCHAR2,
                         O_total_alloc_ord_qty   IN OUT   ORDLOC.QTY_ORDERED%TYPE,
                         O_total_prescale_qty    IN OUT   ORDLOC.QTY_PRESCALED%TYPE,
                         I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
                         I_alloc_no              IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                         I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                         I_location              IN       ORDLOC.LOCATION%TYPE)
    RETURN BOOLEAN;
----------------------------------------------------------------------
-- Name:       ORDER_ITEM_EXISTS
-- Purpose:    Check for the existence of orders that contain the item
--             that is passed into the function.
----------------------------------------------------------------------
FUNCTION ORDER_ITEM_EXISTS (O_error_message   IN OUT   VARCHAR2,
                            O_exists          IN OUT   BOOLEAN,
                            I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function : GET_STATUS
-- Purpose  : Retreives status of order based on passed-in Order No.
---------------------------------------------------------------------
FUNCTION GET_STATUS(O_error_message   IN OUT   VARCHAR2,
                      O_exists        IN OUT   BOOLEAN,
                      O_status        IN OUT   ORDHEAD.STATUS%TYPE,
                      I_order_no      IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_SUPPLIER
-- Purpose  : Retreives Supplier of order based on passed-in Order No.
---------------------------------------------------------------------
FUNCTION GET_SUPPLIER(O_error_message   IN OUT   VARCHAR2,
                      O_exists          IN OUT   BOOLEAN,
                      O_supplier        IN OUT   ORDHEAD.SUPPLIER%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_DEPT
-- Purpose  : Retreives Dept of order based on passed-in Order No.
---------------------------------------------------------------------
FUNCTION GET_DEPT(O_error_message   IN OUT   VARCHAR2,
                  O_exists          IN OUT   BOOLEAN,
                  O_dept            IN OUT   ORDHEAD.DEPT%TYPE,
                  I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_BANK_LC_ID
-- Purpose  : Retrieves the Bank LC ID for the letter of credit to which a PO is
--            attached.
---------------------------------------------------------------------
FUNCTION GET_BANK_LC_ID(O_error_message   IN OUT   VARCHAR2,
                        O_bank_lc_id      IN OUT   LC_HEAD.BANK_LC_ID%TYPE,
                        I_order_no        IN       ORDHEAD.ORDER_NO%TYPE )
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_PAYMENT_METHOD
-- Purpose  : Retrieves the payment method for an order.
---------------------------------------------------------------------
FUNCTION GET_PAYMENT_METHOD(O_error_message    IN OUT   VARCHAR2,
                            O_payment_method   IN OUT   ORDHEAD.PAYMENT_METHOD%TYPE,
                            I_order_no         IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : LC_INFO_EXISTS
-- Purpose  : Determines if letter of credit information has been entered for the
--            Purchase Order.
---------------------------------------------------------------------
FUNCTION LC_INFO_EXISTS(O_error_message   IN OUT   VARCHAR2,
                        O_exists          IN OUT   BOOLEAN,
                        I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : VALID_ORDLC_LC_REF_ID
-- Purpose  : Determines if an lc_ref_id is valid to attach to an order.
--            This package is used in the Order-Letter of Credit (ordlc) form
--            to validate an attached lc_ref_id.
---------------------------------------------------------------------
FUNCTION VALID_ORDLC_LC_REF_ID(O_error_message   IN OUT   VARCHAR2,
                               O_valid           IN OUT   BOOLEAN,
                               I_lc_ref_id       IN       ORDLC.LC_REF_ID%TYPE,
                               I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                               I_beneficiary     IN       ORDLC.BENEFICIARY%TYPE,
                               I_applicant       IN       ORDLC.APPLICANT%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------
-- Function : DELETE_ORDLC
-- Purpose  : Delete the ordlc record for an order.
---------------------------------------------------------------------
FUNCTION DELETE_ORDLC(O_error_message   IN OUT   VARCHAR2,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function     : PREV_APPROVED
-- Purpose      : This function checks to see if an order has been
--                previously approved or not.
--------------------------------------------------------------------------------------
FUNCTION PREV_APPROVED(O_error_message   IN OUT   VARCHAR2,
                       O_approved        IN OUT   VARCHAR2,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: CHILDREN_ITEMS_EXIST
-- Purpose: checks to see if skus or a pack that contains sub-tran level items
--          exist on an order
------------------------------------------------------------------------------------
FUNCTION CHILDREN_ITEMS_EXIST(O_error_message   IN OUT   VARCHAR2,
                              O_exist           IN OUT   BOOLEAN,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
      RETURN BOOLEAN;
-------------------------------------------------------------------------------------
-- Function     : REPL_RESULTS_EXIST
-- Purpose      : This function will check for the existence of a record on the table
--                repl_results according to the input variables entered.  If order_no/
--                item/loc, or order_no entered combination exists, then O_exist will pass
--                out a boolean TRUE (otherwise FALSE is passed out).
----------------------------------------------------------------------------------
FUNCTION REPL_RESULTS_EXIST(O_error_message   IN OUT   VARCHAR2,
                            O_exist           IN OUT   BOOLEAN,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                            I_item            IN       ITEM_MASTER.ITEM%TYPE,
                            I_location        IN       ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function     : REPL_RESULTS_RECALC_EXIST
-- Purpose      : This function will check the repl_results table to determine if
--                there are any records for the inputted order number that need to
--                be recalculated. The recalc_type field is checked to see if the
--                value is 'Q' (entered order qty) or 'A' (changed replenishment
--                parameter).  If one exists then O_exist returns TRUE.
----------------------------------------------------------------------------------
FUNCTION REPL_RESULTS_RECALC_EXIST(O_error_message   IN OUT   VARCHAR2,
                                   O_exist           IN OUT   BOOLEAN,
                                   I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function     : ORDSKU_DISCOUNT_EXIST
-- Purpose      : This function will take the inputted order/item/location and
--                check to see if records exist on the ordloc_discount table.
--                O_exist will  return TRUE if they do and FALSE if they don't
--                exist.
----------------------------------------------------------------------------------
FUNCTION ORDLOC_DISCOUNT_EXIST(O_error_message   IN OUT   VARCHAR2,
                               O_exist           IN OUT   BOOLEAN,
                               I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                               I_item            IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                               I_location        IN       ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
--Function     : GET_TOTAL_CASES
-- Purpose      : This function gets the total cases and the total
--                pre-scaled cases for a given order
--                (I_item and I_location are both NULL),
--                order/item (I_item is NOT NULL and I_location is NULL),
--                order/location (I_item is NULL and I_location is NOT NULL)
--                or order/item/location (both I_item and I_loaction are not
--                NULL).  I_order_no is the only required parameter.
----------------------------------------------------------------------------------
FUNCTION GET_TOTAL_CASES(O_error_message          IN OUT   VARCHAR2,
                         O_total_cases            IN OUT   ORDLOC.QTY_ORDERED%TYPE,
                         O_total_prescale_cases   IN OUT   ORDLOC.QTY_PRESCALED%TYPE,
                         I_order_no               IN       ORDHEAD.ORDER_NO%TYPE,
                         I_item                   IN       ITEM_MASTER.ITEM%TYPE,
                         I_location               IN       ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function     : GET_TOTAL_PALLETS
-- Purpose      : This function gets the total pallets and the total
--                pre-scaled pallets for a given order
--                (I_item and I_location are both NULL),
--                order/item (I_item is NOT NULL and I_location is NULL),
--                order/location (I_item is NULL and I_location is NOT NULL)
--                or order/item/location (both I_item and I_loaction are not
--                NULL).  I_order_no is the only required parameter.
---------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_PALLETS(O_error_message            IN OUT   VARCHAR2,
                           O_total_pallets            IN OUT   NUMBER,
                           O_total_prescale_pallets   IN OUT   NUMBER,
                           I_order_no                 IN       ORDHEAD.ORDER_NO%TYPE,
                           I_supplier                 IN       ORDHEAD.SUPPLIER%TYPE,
                           I_item                     IN       ITEM_MASTER.ITEM%TYPE,
                           I_location                 IN       ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function     : SCALING_ELIGIBILITY
-- Purpose      : This package will determine if an order is eligible for scaling.
--                This function also returns the order status (scaling only
--                allowed in 'W'orksheet).  It also checks to make sure that
--                nothing in the order has been received and sends and indicator
--                back if this is the case.
--------------------------------------------------------------------------------
FUNCTION SCALING_ELIGIBILITY(O_error_message     IN OUT   VARCHAR2,
                             O_scaling_status    IN OUT   BOOLEAN,
                             O_partial_receipt   IN OUT   BOOLEAN,
                             O_order_status      IN OUT   ordhead.status%TYPE,
                             I_order_no          IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function     : CHECK_TEMP_TABLES
-- Purpose      : This package will determine if there is a record on the
--                ordsku_temp table for the order_no passed in.  If there is,
--                then O_exist = TRUE.
--------------------------------------------------------------------------------
FUNCTION CHECK_TEMP_TABLES(O_error_message   IN OUT   VARCHAR2,
                           O_exist           IN OUT   BOOLEAN,
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function     : ORD_INV_MGMT_EXIST
-- Purpose      : This package will determine if there is a record on the
--                ord_inv_mgmt table for the order_no passed in.  If there is,
--                then O_exist = TRUE.
--------------------------------------------------------------------------------
FUNCTION ORD_INV_MGMT_EXIST(O_error_message   IN OUT   VARCHAR2,
                            O_exist           IN OUT   BOOLEAN,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function     : GET_SCALE_CNSTR_IND
-- Purpose      : This package will fetch back the scale_cnstr_ind from the
--                ord_inv_mgmt table for the inputted order number.  If the order
--                is not found then the O_scale_cnstr_ind will be passed out as 'N'.
--------------------------------------------------------------------------------
FUNCTION GET_SCALE_CNSTR_IND(O_error_message     IN OUT   VARCHAR2,
                             O_scale_cnstr_ind   IN OUT   ord_inv_mgmt.scale_cnstr_ind%TYPE,
                             I_order_no          IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function     : CHECK_COST_SOURCES
-- Purpose      : This package will check if any skus on ordloc for an entered
--                order number have a cost_source of 'MANL' (manual).  If the a
--                record is found then O_manual_exist will be passed back as TRUE.
--------------------------------------------------------------------------------
FUNCTION CHECK_COST_SOURCES(O_error_message   IN OUT   VARCHAR2,
                            O_manual_exist    IN OUT   BOOLEAN,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: GET_SUPP_DATE
-- Purpose:       returns supplier and not before date
--------------------------------------------------------------------------------------------
FUNCTION GET_SUPP_DATE(O_error_message     IN OUT   VARCHAR2,
                       O_supplier          IN OUT   ORDHEAD.SUPPLIER%TYPE,
                       O_not_before_date   IN OUT   ORDHEAD.NOT_BEFORE_DATE%TYPE,
                       I_order_no          IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name   :  GET_ORIG_IND
-- Purpose         :  Returns the orig_ind from ordhead.
--------------------------------------------------------------------------------------------
FUNCTION GET_ORIG_IND(O_error_message   IN OUT   VARCHAR2,
                      O_orig_ind        IN OUT   ordhead.orig_ind%TYPE,
                      I_order_no        IN       ordhead.order_no%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name   :  GET_COMMENTS
-- Purpose         :  This function will used to return the value in the Comments
--                    column on the ordhead table for a passed order number.
--------------------------------------------------------------------------------------------
FUNCTION GET_COMMENTS(O_error_message   IN OUT   VARCHAR2,
                      O_comment_desc    IN OUT   ORDHEAD.COMMENT_DESC%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name   :  UPDATE_COMMENTS
-- Purpose         :  This function will used to the update the value in the Comments
--                    column on the ordhead table for a passed order number.
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_COMMENTS(O_error_message   IN OUT   VARCHAR2,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                         I_comment_desc    IN       ORDHEAD.COMMENT_DESC%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Function Name    :  ORDHEAD_DISCOUNT_EXIST
--Purpose          :  This functio will check to see if ther eis a record on the table
--                    ORDHEAD_DISCOUNT for a given order number.
---------------------------------------------------------------------------------------------
FUNCTION ORDHEAD_DISCOUNT_EXIST(O_error_message   IN OUT   VARCHAR2,
                                O_exist           IN OUT   BOOLEAN,
                                I_order_no        IN       ORDHEAD_DISCOUNT.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------------------
--Function Name    :  GET_SUPP_MFG
--Purpose          :  This function will get the mfg_id and related ind based on the
--                    passed in order number.
---------------------------------------------------------------------------------------------
FUNCTION GET_MFG_REL_IND(O_error_message   IN OUT   VARCHAR2,
                         O_mfg_id          IN OUT   SUP_IMPORT_ATTR.MFG_ID%TYPE,
                         O_related_ind     IN OUT   SUP_IMPORT_ATTR.related_ind%TYPE,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function     : CHECK_BUYER_PACKS
-- Purpose      : This package will check if any items on ordloc for an entered
--                order number have are buyer packs.  If the a
--                record is found then O_buyer_packs_exist will be passed back as TRUE.
---------------------------------------------------------------------------------------------
FUNCTION  CHECK_BUYER_PACKS(O_error_message       IN OUT   VARCHAR2,
                            O_buyer_packs_exist   IN OUT   BOOLEAN,
                            I_order_no            IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION  GET_DATES (O_error_message       IN OUT   VARCHAR2,
                     O_written_date        IN OUT   ORDHEAD.WRITTEN_DATE%TYPE,
                     O_not_before_date     IN OUT   ORDHEAD.NOT_BEFORE_DATE%TYPE,
                     O_not_after_date      IN OUT   ORDHEAD.NOT_AFTER_DATE%TYPE,
                     O_otb_eow_date        IN OUT   ORDHEAD.OTB_EOW_DATE%TYPE,
                     O_ealiest_ship_date   IN OUT   ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
                     O_latest_ship_date    IN OUT   ORDHEAD.LATEST_SHIP_DATE%TYPE,
                     O_close_date          IN OUT   ORDHEAD.CLOSE_DATE%TYPE,
                     I_order_no            IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function     : CHECK_CONTRACT_ORDER
-- Purpose      : This function will check if the order is associated with a contract.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_CONTRACT_ORDER(O_error_message     IN OUT   VARCHAR2,
                              O_contract_exists   IN OUT   BOOLEAN,
                              I_order_no          IN       ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name    :  MULTIPLE_LOCS_EXIST
--Purpose          :  This function will determine whether more than one location is
--                    attached to an order. If only one location is attached to the order,
--                    it will return that location.
--                    In a multi-channel environment, a single location order implies one
--                    physical warehouse on the order.  Therefore, the order could have multiple
--                    virtual warehouses on it, however if they all exist in the same physical,
--                    the order is still considered a one location order and it is the
--                    physical warehouse that is passed back from the function.  If the order only
--                    has one virtual warehouse on it, both the virtual warehouse and it's physical
--                    warehouse will be passed back.  In a non multi-channel environment, there
--                    is no distinction between physical and virtual warehouses.  Stores behave
--                    the same way in a multi-channel and non multi-channel environment.
---------------------------------------------------------------------------------------------
FUNCTION MULTIPLE_LOCS_EXIST(O_error_message         IN OUT   VARCHAR2,
                             O_location              IN OUT   ORDLOC.LOCATION%TYPE,
                             O_loc_type              IN OUT   ORDLOC.LOC_TYPE%TYPE,
                             O_multiple_locs_exist   IN OUT   BOOLEAN,
                             O_virtual_wh            IN OUT   WH.WH%TYPE,
                             I_order_no              IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN  BOOLEAN;
---------------------------------------------------------------------------------------------
--Function:  GET_ITEM_LOC_DEAL_DISCOUNTS
--Purpose:   This function will calculate the total transactional and non-transactional
--           level discounts that have been applied to a PO
--------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOC_DEAL_DISCOUNTS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_total_tran_disc       IN OUT   ORDLOC_DISCOUNT.DISCOUNT_AMT_PER_UNIT%TYPE,
                                     O_total_non_tran_disc   IN OUT   ORDLOC_DISCOUNT.DISCOUNT_AMT_PER_UNIT%TYPE,
                                     I_order_no              IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_STATCASE(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_total_statcase            IN OUT   NUMBER,
                            O_total_prescale_statcase   IN OUT   NUMBER,
                            I_order_no                  IN       ORDHEAD.ORDER_NO%TYPE,
                            I_supplier                  IN       ORDHEAD.SUPPLIER%TYPE,
                            I_item                      IN       ITEM_MASTER.ITEM%TYPE,
                            I_location                  IN       ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : GET_LOCATION
-- Purpose  : Retreives Location Type and Location
--            of the order based on passed-in Order No.
---------------------------------------------------------------------
FUNCTION GET_LOCATION(O_error_message   IN OUT   VARCHAR2,
                      O_exists          IN OUT   BOOLEAN,
                      O_loc_type        IN OUT   ORDHEAD.LOC_TYPE%TYPE,
                      O_location        IN OUT   ORDHEAD.LOCATION%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function Name: PAY_TERMS_CODE
-- Purpose: validates the enter terms code is valid and returns the description.
-------------------------------------------------------------------------------------------------
FUNCTION PAY_TERMS_CODE(O_error_message   IN OUT   VARCHAR2,
                        O_terms           IN OUT   TERMS.TERMS%TYPE,
                        O_terms_desc      IN OUT   TERMS.TERMS_DESC%TYPE,
                        O_terms_code      IN OUT   TERMS.TERMS_CODE%TYPE,
                        I_code            IN       TERMS.TERMS_CODE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function Name: GET_ORDER_TYPE
-- Purpose: retreives an order type for an order from ordhead.
-------------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_TYPE(O_error_message   IN OUT   VARCHAR2,
                        O_order_type      IN OUT   ORDHEAD.ORDER_TYPE%TYPE,
                        I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: GET_ORDHEAD_ROW
-- Purpose: retreives the ordhead row for an order.
-------------------------------------------------------------------------------------------------
FUNCTION GET_ORDHEAD_ROW(O_error_message   IN OUT          VARCHAR2,
                         O_ordhead_row        OUT NOCOPY   ORDHEAD%ROWTYPE,
                         I_order_no        IN              ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: GET_ORDSKU_ROW
-- Purpose: retreives the ordsku row for an order/item.
-------------------------------------------------------------------------------------------------
FUNCTION GET_ORDSKU_ROW(O_error_message   IN OUT          VARCHAR2,
                        O_exists          IN OUT          BOOLEAN,
                        O_ordsku_row         OUT NOCOPY   ORDSKU%ROWTYPE,
                        I_order_no        IN              ORDSKU.ORDER_NO%TYPE,
                        I_item            IN              ORDSKU.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function Name: GET_ORDLOC_ROW
-- Purpose: retreives the ordloc row for an order/item/location.
-------------------------------------------------------------------------------------------------
FUNCTION GET_ORDLOC_ROW(O_error_message   IN OUT          VARCHAR2,
                        O_exists          IN OUT          BOOLEAN,
                        O_ordloc_row         OUT NOCOPY   ORDLOC%ROWTYPE,
                        I_order_no        IN              ORDLOC.ORDER_NO%TYPE,
                        I_item            IN              ORDLOC.ITEM%TYPE,
                        I_location        IN              ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------------
-- Function Name: ORD_ONLY_ITEM
-- Purpose      : This function will determine if the inputted order contains
--                an orderable-only non-transformed item.
--------------------------------------------------------------------------------------------------
FUNCTION ORD_ONLY_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists          IN OUT   BOOLEAN,
                       I_order_no        IN       ORDLOC.ORDER_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------------
-- Function: COMPLEX_PACK_EXISTS
-- Purpose : This new function will determine if a PO contains a complex pack.
-- This is needed for the ORDLOC.FMB form in enabling / disabling the new button "Pack Components".
--------------------------------------------------------------------------------------------------
FUNCTION COMPLEX_PACK_EXISTS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_order           IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------------------------
-- Function: PARTIAL_RECEIVED_ORDER
-- Purpose : This new function will determine if a PO has been partially received or not none has
--           been received yet.
-- This is needed for the ORDHEAD.FMB form in enabling the estimated_in_stock_date.
--------------------------------------------------------------------------------------------------
FUNCTION PARTIAL_NONE_RCVD_ORD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists          IN OUT   BOOLEAN,
                                I_order           IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name: FREIGHT_TERMS
-- Purpose: accepts freight code and returns freight terms description and validates if freight terms
-- is inactive based on start date, end date and enabled flag.
-------------------------------------------------------------------
FUNCTION FREIGHT_TERMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_code            IN       VARCHAR2,
                       O_desc            IN OUT   VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function: ORDER_LOCATION_EXISTS
-- Purpose : This function will determine if a location exists for an order number
--------------------------------------------------------------------------------------------------
FUNCTION ORDER_LOCATION_EXISTS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists          IN OUT   BOOLEAN,
                                I_order           IN       ORDLOC.ORDER_NO%TYPE,
                                I_location        IN       ORDLOC.LOCATION%TYPE,
                                I_loc_type        IN       ORDLOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function: CUSTOMER_LOCS_EXISTS
-- Purpose : This function will determine if there are any Wholesale/Franchise transfers linked
-- for the order number
--------------------------------------------------------------------------------------------------
FUNCTION CUSTOMER_LOCS_EXISTS (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cust_loc_exits   IN OUT   BOOLEAN,
                               I_order_no         IN       ORDLOC.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function : GET_CLEARING_ZONE
-- Purpose  : Retrieves the Clearing Zone off of ordhead for a given order
--------------------------------------------------------------------------------------------------
FUNCTION GET_CLEARING_ZONE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_clearing_zone_id   IN OUT   ORDHEAD.CLEARING_ZONE_ID%TYPE,
                           I_order_no           IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function : GET_DEFAULT_CLEARING_ZONE_ID
-- Purpose  : Retrieves the default Clearing Zone for a given order/import country
--------------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_CLEARING_ZONE_ID(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_clearing_zone_id    IN OUT   ORDHEAD.CLEARING_ZONE_ID%TYPE,
                                      I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                                      I_import_country_id   IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function : MULTIPLE_LOC_TYPE_EXISTS
-- Purpose  : Checks whether there are multiple loc_type for a specific order and counts
--            the number of loc_types present.
--------------------------------------------------------------------------------------------------
FUNCTION MULTIPLE_LOC_TYPE_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_wh_exists          OUT   BOOLEAN,
                                  O_Loc_exists         OUT   BOOLEAN,
                                  I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function : CHECK_SUPP_LOC_OU
-- Purpose  : Checks whether there are any locations those belong to different Org Unit than 
--            the Org Unit to which the supplier belongs.
--------------------------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_LOC_OU(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_same_ou         IN OUT   BOOLEAN,
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                           I_supplier        IN       SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function Name: GET_PURCHASE_TYPE
-- Purpose: Retrieves the Purchase Type for an order from ordhead.
--------------------------------------------------------------------------------------------------
FUNCTION GET_PURCHASE_TYPE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_purchase_type   IN OUT   ORDHEAD.PURCHASE_TYPE%TYPE,
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function Name: GET_ORDER_INFO
-- Purpose: Retrieves the order type, status, contract number, department, location type and
--          location for a given order.
--------------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_order_type      IN OUT   ORDHEAD.PURCHASE_TYPE%TYPE,
                        O_status          IN OUT   ORDHEAD.STATUS%TYPE,
                        O_contract_no     IN OUT   ORDHEAD.CONTRACT_NO%TYPE,
                        O_dept            IN OUT   ORDHEAD.DEPT%TYPE,
                        O_loc_type        IN OUT   ORDHEAD.LOC_TYPE%TYPE,
                        O_location        IN OUT   ORDHEAD.LOCATION%TYPE,
                        O_loc_ind         IN OUT   ORDHEAD.LOC_TYPE%TYPE,
                        I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function Name: ORDLOC_WKSHT_EXIST
-- Purpose: Checks if a record exists in the ORDLOC_WKSHT table for a given order.
--------------------------------------------------------------------------------------------------
FUNCTION ORDLOC_WKSHT_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   VARCHAR2,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
END ORDER_ATTRIB_SQL;
/
