
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE UPDATE_OBJECT_SQL AUTHID CURRENT_USER AS


---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ORDER_GRADE
-- Purpose:       To update existing records on the ORDLOC_WKSHT table with new quantities
--                added during the store grade distribution process.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_GRADE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                            I_type            IN     VARCHAR2,
                            I_sum_ratio       IN     NUMBER, 
                            I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                            I_uop_type        IN     VARCHAR2)
                      RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ORDER_LOC
-- Purpose:       To update existing records on the ORDLOC_WKSHT table with new quantities
--                added during the location distribution process.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_LOC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                          I_type            IN     VARCHAR2,
                          I_sum_ratio       IN     NUMBER, 
                          I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                          I_uop_type        IN     VARCHAR2)
                      RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ORDER_DIFF
-- Purpose:       To update existing records on the ORDLOC_WKSHT table with new quantities
--                added during the diff distribution process.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_DIFF(O_error_message   IN OUT VARCHAR2,
                           I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                           I_type            IN     VARCHAR2,
                           I_sum_ratio       IN     NUMBER, 
                           I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                           I_uop_type        IN     VARCHAR2,
                           I_diff_1_or_2     IN     NUMBER)
                      RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_CONTRACT_LOC
-- Purpose:       To update existing records on the CONTRACT_MATRIX_TEMP table with new 
--                quantities added during the location distribution process.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_CONTRACT_LOC (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_contract_no     IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
                              I_type            IN     VARCHAR2,
                              I_sum_ratio       IN     NUMBER,
                              I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE) RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_CONTRACT_DIFF
-- Purpose:       To update existing records on the CONTRACT_MATRIX_TEMP table with new 
--                quantities added during the diff distribution process.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_CONTRACT_DIFF (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_contract_no     IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
                               I_type            IN     VARCHAR2,
                               I_sum_ratio       IN     NUMBER,
                               I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                               I_diff_1234       IN     NUMBER) RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_CONTRACT_DATE
-- Purpose:       To update existing records on the CONTRACT_MATRIX_TEMP table with new 
--                quantities added during the date distribution process.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_CONTRACT_DATE (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_contract_no     IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
                               I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE) RETURN BOOLEAN;

END UPDATE_OBJECT_SQL;
/
