CREATE OR REPLACE PACKAGE BODY RMSSUB_DSDDEALS AS

LP_sales_audit_ind     system_options.sales_audit_ind%TYPE     := NULL;

PROGRAM_ERROR    EXCEPTION;
----------------------------------------------------------------------------------------
FUNCTION COMPLETE_TRANSACTION(O_error_message        IN OUT  VARCHAR2,
                              I_store                IN      store.store%TYPE,
                              I_supplier             IN      sups.supplier%TYPE,
                              I_dept                 IN      deps.dept%TYPE,
                              I_invoice_ind          IN      VARCHAR2,
                              I_currency_code        IN      sups.currency_code%TYPE,
                              I_paid_ind             IN      INVC_HEAD.PAID_IND%TYPE,
                              I_deals_ind            IN      VARCHAR2,
                              I_ext_ref_no           IN      INVC_HEAD.EXT_REF_NO%TYPE,
                              I_proof_of_delivery_no IN      INVC_HEAD.PROOF_OF_DELIVERY_NO%TYPE,
                              I_payment_ref_no       IN      INVC_HEAD.PAYMENT_REF_NO%TYPE,
                              I_payment_date         IN      INVC_HEAD.PAYMENT_DATE%TYPE,
                              I_order_no             IN      ORDHEAD.ORDER_NO%TYPE,
                              I_shipment             IN      SHIPMENT.SHIPMENT%TYPE,
                              I_invc_id              IN      invc_non_merch_temp.invc_id%TYPE,
                              I_receipt_date         IN      SHIPMENT.RECEIVE_DATE%TYPE,
                              I_qty_sum              IN      shipsku.qty_received%TYPE,
                              I_cost_sum             IN      item_supp_country.unit_cost%TYPE,
                              I_ext_receipt_no       IN      shipment.ext_ref_no_in%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS (O_status               IN OUT  VARCHAR2,
                         IO_error_message       IN OUT  VARCHAR2,
                         I_cause                IN      VARCHAR2,
                         I_program              IN      VARCHAR2);
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code               IN OUT  VARCHAR2,
                  O_error_message             IN OUT  VARCHAR2,
                  I_message                   IN      RIB_OBJECT,
                  I_message_type              IN      VARCHAR2)
IS
   L_system_opt_rec   SYSTEM_OPTIONS%ROWTYPE;

   L_rib_dsddealsdesc_rec      "RIB_DSDDealsDesc_REC" := NULL;

BEGIN

   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message_type is NULL or lower(I_message_type) != 'dsddealscre' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MESSAGE_TYPE', I_message_type, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   L_rib_dsddealsdesc_rec := treat(I_MESSAGE AS "RIB_DSDDealsDesc_REC");

   if L_rib_dsddealsdesc_rec is NULL or L_rib_dsddealsdesc_rec.DSDDeals_TBL is NULL then
      O_status_code := API_CODES.SUCCESS;
      return;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_opt_rec) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   LP_sales_audit_ind    := L_system_opt_rec.sales_audit_ind;

   FOR dsdCnt IN L_rib_dsddealsdesc_rec.DSDDeals_TBL.FIRST..L_rib_dsddealsdesc_rec.DSDDeals_TBL.LAST LOOP
      if COMPLETE_TRANSACTION(O_error_message,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).store,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).supplier,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).dept,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).invc_ind,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).currency_code,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).paid_ind,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).deals_ind,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).ext_ref_no,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).proof_of_delivery_no,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).payment_ref_no,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).payment_date,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).order_no,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).shipment,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).invc_id,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).receipt_date,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).qty_sum,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).cost_sum,
                              L_rib_dsddealsdesc_rec.DSDDeals_TBL(dsdCnt).ext_receipt_no) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   END LOOP;
   -- If no error is raised, then the subscription has completed successfully.
   O_status_code := API_CODES.SUCCESS;
EXCEPTION
   when PROGRAM_ERROR then
     HANDLE_ERRORS(O_status_code,
                   O_error_message,
                   API_LIBRARY.FATAL_ERROR,
                   'RMSSUB_DSD.CONSUME');
   when OTHERS then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     'RMSSUB_DSD.CONSUME');
END CONSUME;
-----------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status               IN OUT  VARCHAR2,
                        IO_error_message       IN OUT  VARCHAR2,
                        I_cause                IN      VARCHAR2,
                        I_program              IN      VARCHAR2)
IS
BEGIN
   API_LIBRARY.HANDLE_ERRORS(O_status,
                             IO_error_message,
                             I_cause,
                             I_program);
EXCEPTION
   when OTHERS then
      IO_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_DSD.HANDLE_ERRORS',
                                             to_char(SQLCODE));

     API_LIBRARY.HANDLE_ERRORS(O_status,
                               IO_error_message,
                               API_LIBRARY.FATAL_ERROR,
                               'RMSSUB_DSD.HANDLE_ERRORS');

END HANDLE_ERRORS;
-----------------------------------------------------------------------------------------
FUNCTION COMPLETE_TRANSACTION(O_error_message        IN OUT  VARCHAR2,
                              I_store                IN      store.store%TYPE,
                              I_supplier             IN      sups.supplier%TYPE,
                              I_dept                 IN      deps.dept%TYPE,
                              I_invoice_ind          IN      VARCHAR2,
                              I_currency_code        IN      sups.currency_code%TYPE,
                              I_paid_ind             IN      INVC_HEAD.PAID_IND%TYPE,
                              I_deals_ind            IN      VARCHAR2,
                              I_ext_ref_no           IN      INVC_HEAD.EXT_REF_NO%TYPE,
                              I_proof_of_delivery_no IN      INVC_HEAD.PROOF_OF_DELIVERY_NO%TYPE,
                              I_payment_ref_no       IN      INVC_HEAD.PAYMENT_REF_NO%TYPE,
                              I_payment_date         IN      INVC_HEAD.PAYMENT_DATE%TYPE,
                              I_order_no             IN      ORDHEAD.ORDER_NO%TYPE,
                              I_shipment             IN      SHIPMENT.SHIPMENT%TYPE,
                              I_invc_id              IN      invc_non_merch_temp.invc_id%TYPE,
                              I_receipt_date         IN      SHIPMENT.RECEIVE_DATE%TYPE,
                              I_qty_sum              IN      shipsku.qty_received%TYPE,
                              I_cost_sum             IN      item_supp_country.unit_cost%TYPE,
                              I_ext_receipt_no       IN      shipment.ext_ref_no_in%TYPE)
RETURN BOOLEAN
IS
   L_approve_ind    VARCHAR2(1)   := 'N';
   L_closed         BOOLEAN       := FALSE;
   L_invc_id        invc_non_merch_temp.invc_id%TYPE := I_invc_id;

   L_dummy          VARCHAR2(1)   := NULL;
   L_receipt_date   SHIPMENT.RECEIVE_DATE%TYPE := NVL(I_receipt_date, GET_VDATE);

   L_bill_to_loc        SHIPMENT.BILL_TO_LOC%TYPE        := NULL;
   L_bill_to_loc_type   SHIPMENT.BILL_TO_LOC_TYPE%TYPE   := NULL;
   L_phy_wh             WH.PHYSICAL_WH%TYPE              := NULL;

   /** used in loop **/
   type l_item_type is table of ordsku.item%TYPE index by binary_integer;
   L_item  l_item_type;
   type l_ref_item_type is table of ordsku.ref_item%TYPE index by binary_integer;
   L_ref_item l_ref_item_type;
   type l_qty_received_type is table of ordloc.qty_received%TYPE index by binary_integer;
   L_qty_received l_qty_received_type;
   type l_unit_cost_type is table of ordloc.unit_cost%TYPE index by binary_integer;
   L_unit_cost l_unit_cost_type;
   type l_unit_retail_type is table of ordloc.unit_retail%TYPE index by binary_integer;
   L_unit_retail l_unit_retail_type;
   type l_qty_ordered_type is table of ordloc.qty_ordered%TYPE index by binary_integer;
   L_qty_ordered l_qty_ordered_type;
   type l_weight_received_type is table of shipsku.weight_received%TYPE index by binary_integer;
   L_weight_received l_weight_received_type;
   type l_weight_received_uom_type is table of shipsku.weight_received_uom%TYPE index by binary_integer;
   L_weight_received_uom l_weight_received_uom_type;
   type l_seq_no_type is table of shipsku.seq_no%TYPE index by binary_integer;
   L_seq_no l_seq_no_type;
   L_prev_item     ordsku.item%TYPE := NULL;
   j  NUMBER(10) := 0;

   cursor C_CHECK_PROCESSED is
      select 'x'
        from shipment
       where shipment = I_shipment;

   cursor C_SEQ_ITEMS is
      select os.item,
             os.ref_item,
             ol.qty_received,
             ol.unit_cost,
             ol.unit_retail,
             ol.qty_ordered,
             ot.weight_received,
             ot.weight_received_uom
        from ordsku os,
             ordloc ol,
             ordauto_temp ot
       WHERE os.order_no = I_order_no
         AND os.order_no = ol.order_no
         AND os.item = ol.item
         --
         AND ot.order_no = ol.order_no
         AND ot.item = ol.item
         AND ot.store = ol.location
      order by os.item;

   cursor C_BILL_TO_LOC is
      select physical_wh
        from wh
       where wh = L_bill_to_loc;

BEGIN
   open C_CHECK_PROCESSED;
   fetch C_CHECK_PROCESSED into L_dummy;
   close C_CHECK_PROCESSED;

   if L_dummy = 'x' then
      return true;
   end if;

   /** apply deals if deals indicator is 'Y' **/
   if (I_deals_ind = 'Y') then
      O_error_message := ' ';
      if (APPLY_DEALS_TO_ORDER(O_error_message,
                               I_order_no,
                               'Y',
                               'Y',
                               'N')) != 0 then
         return FALSE;
      end if;
      O_error_message := NULL;
   end if;

   -- Retrieve the import details of an order
   if ORDER_SQL.GET_DEFAULT_IMP_EXP(O_error_message,
                                    L_bill_to_loc,
                                    L_bill_to_loc_type,
                                    I_order_no) = FALSE then
      return FALSE;
   end if;

   if L_bill_to_loc is NOT NULL then
      open C_BILL_TO_LOC;
      fetch C_BILL_TO_LOC into L_phy_wh;
      close C_BILL_TO_LOC;

      L_bill_to_loc_type := 'W';
   else
      L_bill_to_loc_type := 'S';
   end if;


   INSERT INTO shipment(shipment,
                        order_no,
                        bol_no,
                        asn,
                        ship_date,
                        receive_date,
                        est_arr_date,
                        ship_origin,
                        status_code,
                        invc_match_status,
                        invc_match_date,
                        to_loc,
                        to_loc_type,
                        from_loc,
                        from_loc_type,
                        courier,
                        no_boxes,
                        ext_ref_no_in,
                        ext_ref_no_out,
                        comments,
                        bill_to_loc,
                        bill_to_loc_type)
                 VALUES(I_shipment,
                        I_order_no,
                        NULL,
                        NULL,
                        L_receipt_date,
                        NULL,
                        NULL,
                        '2',
                        'I',
                        'U',
                        NULL,
                        I_store,
                        'S',
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        I_ext_receipt_no,
                        NULL,
                        NULL,
                        NVL(L_phy_wh,I_store),
                        L_bill_to_loc_type);

   for rec in C_SEQ_ITEMS loop
      L_item(j)         := rec.item;
      L_ref_item(j)     := rec.ref_item;
      L_qty_received(j) := rec.qty_received;
      L_unit_cost(j)    := rec.unit_cost;
      L_unit_retail(j)  := rec.unit_retail;
      L_qty_ordered(j)  := rec.qty_ordered;

      L_weight_received(j)      := rec.weight_received;
      L_weight_received_uom(j)  := rec.weight_received_uom;

      if L_prev_item = L_item(j) then
         L_seq_no(j) := L_seq_no(j-1) + 1;
      else
         L_seq_no(j) := 1;
      end if;

      L_prev_item := L_item(j);
      j := j + 1;
   end loop; /** end C_SEQ_ITEMS loop **/

   if L_item.first is not null then
      forall i in L_item.first..L_item.last
         /** insert shipsku records **/
         INSERT INTO shipsku
                    (shipment,
                     seq_no,
                     item,
                     distro_no,
                     ref_item,
                     carton,
                     inv_status,
                     status_code,
                     qty_received,
                     unit_cost,
                     unit_retail,
                     qty_expected,
                     match_invc_id,
                     weight_received,
                     weight_received_uom)
              VALUES(I_shipment,
                     L_seq_no(i),
                     L_item(i),
                     NULL,
                     L_ref_item(i),
                     NULL,               /* carton is initialized null */
                     -1,
                     'A',                /* status code */
                     L_qty_received(i),
                     L_unit_cost(i),
                     L_unit_retail(i),
                     L_qty_ordered(i),
                     NULL,
                     L_weight_received(i),
                     L_weight_received_uom(i));
   end if;

   /* delete the po from the ordauto_temp table */
   delete from ordauto_temp
    where order_no = I_order_no;

   /** receive the order **/
   if ORDER_RCV_SQL.RECEIVE_AUTO_PO(O_error_message,
                                    I_order_no,
                                    I_shipment,
                                    I_store,
                                    L_receipt_date,
                                    'N') = FALSE then  --Pass in 'N'. Do not create invoice in RECEIVE_AUTO_PO().
                                                       --Invoice creation will be handled by DIRECT_STORE_INVOICE_SQL.CREATE_INVOICE().
      return FALSE;
   end if;

   /** approve the order **/
   if APPT_DOC_CLOSE_SQL.CLOSE_PO(O_error_message,
                                  L_closed,
                                  I_order_no) = FALSE then
      return FALSE;
   end if;

   /** Create invoice if:                                            **/
   /**  1) invoice matching is on (ext_invc_match_ind = 'Y')         **/
   /**  2) invoice indicator from input file is 'Y'                  **/
   /**  3) paid indicator is 'N', or paid indicator is 'Y' and sales **/
   /**     audit is NOT turned on (sales_audit_ind = 'N')            **/
   if ((I_invoice_ind = 'Y') and
       ((I_paid_ind = 'N') or
        ((I_paid_ind = 'Y') and (LP_sales_audit_ind = 'N')))) then
      if DIRECT_STORE_INVOICE_SQL.CREATE_INVOICE
                                 (O_error_message,
                                  L_invc_id,
                                  I_order_no,
                                  I_supplier,
                                  L_receipt_date,
                                  I_cost_sum,
                                  I_qty_sum,
                                  I_store,
                                  I_paid_ind,
                                  I_ext_ref_no,
                                  I_proof_of_delivery_no,
                                  I_payment_ref_no,
                                  I_payment_date) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;
END COMPLETE_TRANSACTION;
-----------------------------------------------------------------------------------------
END RMSSUB_DSDDEALS;
/
