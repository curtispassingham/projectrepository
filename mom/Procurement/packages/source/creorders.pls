
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE CREATE_ORDER_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------
FUNCTION INIT(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
FUNCTION RESET(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
FUNCTION PROCESS_HEAD(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_order_no             IN OUT  ordhead.order_no%TYPE,
                      I_store                IN      store.store%TYPE,
                      I_supplier             IN      sups.supplier%TYPE,
                      I_dept                 IN      deps.dept%TYPE,
                      I_origin_country_id    IN      item_supp_country.origin_country_id%TYPE,
                      I_currency_code        IN      sups.currency_code%TYPE,
                      I_paid_ind             IN      INVC_HEAD.PAID_IND%TYPE,
                      I_deals_ind            IN      VARCHAR2,
                      I_invoice_ind          IN      VARCHAR2,
                      I_ext_ref_no           IN      INVC_HEAD.EXT_REF_NO%TYPE,
                      I_proof_of_delivery_no IN      INVC_HEAD.PROOF_OF_DELIVERY_NO%TYPE,
                      I_payment_ref_no       IN      INVC_HEAD.PAYMENT_REF_NO%TYPE,
                      I_ext_receipt_no       IN      SHIPMENT.EXT_REF_NO_IN%TYPE,
                      I_order_type           IN      ORDHEAD.ORDER_TYPE%TYPE,
                      I_dept_level_orders    IN      PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION COMPLETE_DSD_TRANSACTION(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_dsd_deals_rec           OUT  "RIB_DSDDeals_REC",
                                  I_order_no             IN      ordhead.order_no%TYPE,
                                  I_store                IN      store.store%TYPE,
                                  I_supplier             IN      sups.supplier%TYPE,
                                  I_dept                 IN      deps.dept%TYPE,
                                  I_invoice_ind          IN      VARCHAR2,
                                  I_currency_code        IN      sups.currency_code%TYPE,
                                  I_paid_ind             IN      INVC_HEAD.PAID_IND%TYPE,
                                  I_deals_ind            IN      VARCHAR2,
                                  I_ext_ref_no           IN      INVC_HEAD.EXT_REF_NO%TYPE,
                                  I_proof_of_delivery_no IN      INVC_HEAD.PROOF_OF_DELIVERY_NO%TYPE,
                                  I_payment_ref_no       IN      INVC_HEAD.PAYMENT_REF_NO%TYPE,
                                  I_payment_date         IN      INVC_HEAD.PAYMENT_DATE%TYPE,
                                  I_ext_receipt_no       IN      SHIPMENT.EXT_REF_NO_IN%TYPE,
                                  I_receipt_date         IN      SHIPMENT.RECEIVE_DATE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION COMPLETE_TRANSACTION(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION SET_ORDER_DATES(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                         I_need_date        IN      ORDSKU.LATEST_SHIP_DATE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_NMEXP(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_non_merch_code       IN      invc_non_merch_temp.non_merch_code%TYPE,
                       I_non_merch_amt        IN      invc_non_merch_temp.non_merch_amt%TYPE,
                       I_vat_code             IN      invc_non_merch_temp.vat_code%TYPE,
                       I_service_perf_ind     IN      invc_non_merch_temp.service_perf_ind%TYPE,
                       I_store                IN      store.store%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DETAIL(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no             IN      ORDHEAD.ORDER_NO%TYPE,
                        I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                        I_qty_received         IN      SHIPSKU.QTY_RECEIVED%TYPE,
                        I_store                IN      STORE.STORE%TYPE,
                        I_supplier             IN      SUPS.SUPPLIER%TYPE,
                        I_dept                 IN      DEPS.DEPT%TYPE,
                        I_origin_country_id    IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                        I_currency_code        IN      SUPS.CURRENCY_CODE%TYPE,
                        I_item_loc_status      IN      ITEM_LOC.STATUS%TYPE,
                        I_weight_received      IN      SHIPSKU.WEIGHT_RECEIVED%TYPE,
                        I_weight_received_uom  IN      SHIPSKU.WEIGHT_RECEIVED_UOM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DETAIL(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no             IN      ORDHEAD.ORDER_NO%TYPE,
                        I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                        I_qty_received         IN      SHIPSKU.QTY_RECEIVED%TYPE,
                        I_store                IN      STORE.STORE%TYPE,
                        I_supplier             IN      SUPS.SUPPLIER%TYPE,
                        I_dept                 IN      DEPS.DEPT%TYPE,
                        I_origin_country_id    IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                        I_currency_code        IN      SUPS.CURRENCY_CODE%TYPE,
                        I_item_loc_status      IN      ITEM_LOC.STATUS%TYPE,
                        I_weight_received      IN      SHIPSKU.WEIGHT_RECEIVED%TYPE,
                        I_weight_received_uom  IN      SHIPSKU.WEIGHT_RECEIVED_UOM%TYPE,
                        I_unit_cost            IN      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
END CREATE_ORDER_SQL;
/
