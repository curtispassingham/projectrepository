
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ORDER_DUE_SQL AS
--------------------------------------------------------------------------------------------
FUNCTION   TOTAL_ESO_ASO        (O_error_message 	   IN OUT   VARCHAR2,
		                 O_estimated_stock_out     IN OUT   repl_results.estimated_stock_out%TYPE,
                                 O_accepted_stock_out      IN OUT   repl_results.accepted_stock_out%TYPE,
                                 I_order_no                IN       repl_results.order_no%TYPE,
	                         I_item	      	           IN       repl_results.item%TYPE,
		                 I_location	           IN       repl_results.location%TYPE)
            			RETURN BOOLEAN is

   cursor C_ORDER is
      select sum(nvl(estimated_stock_out,0)), 
             sum(nvl(accepted_stock_out,0))
        from repl_results
       where order_no = I_order_no;

   cursor C_ITEM is
      select sum(nvl(estimated_stock_out,0)), 
             sum(nvl(accepted_stock_out,0))
        from repl_results
       where order_no = I_order_no
         and     item = I_item;

   cursor C_LOCATION is
      select sum(nvl(estimated_stock_out,0)), 
             sum(nvl(accepted_stock_out,0))
        from repl_results
       where order_no = I_order_no
         and location = I_location;
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_order_no',
                                            'NULL', 'NOT NULL');
      return FALSE;
   end if;

   if I_item is not NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_ITEM', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Item: ' || I_item);
      open C_ITEM;
      SQL_LIB.SET_MARK('FETCH', 'C_ITEM', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Item: ' || I_item);
      fetch C_ITEM into O_estimated_stock_out,
           		O_accepted_stock_out;
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Item: ' || I_item);
      close C_ITEM;
      RETURN TRUE;

   elsif I_location is not NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_LOCATION', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Location: ' || to_char(I_location));
      open C_LOCATION;
      SQL_LIB.SET_MARK('FETCH', 'C_LOCATION', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Location: ' || to_char(I_location));
      fetch C_LOCATION into O_estimated_stock_out,
      			    O_accepted_stock_out;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOCATION', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Location: ' || to_char(I_location));
      close C_LOCATION;
      RETURN TRUE;

   else
      SQL_LIB.SET_MARK('OPEN', 'C_ORDER', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no)); 
      open C_ORDER;
      SQL_LIB.SET_MARK('FETCH', 'C_ORDER', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no)); 
      fetch C_ORDER into O_estimated_stock_out,
                         O_accepted_stock_out;
      SQL_LIB.SET_MARK('CLOSE', 'C_ORDER', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no)); 
      close C_ORDER;
      RETURN TRUE;
   end if;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_DUE_SQL.TOTAL_ESO_ASO', 
                                            to_char(SQLCODE));
      return FALSE;
END TOTAL_ESO_ASO;
--------------------------------------------------------------------------------------------
FUNCTION   ORDER_DUE        (O_error_message          IN OUT     VARCHAR2,
                             O_order_due              IN OUT     BOOLEAN,
                             I_order_no               IN         ordloc.order_no%TYPE)
                             RETURN BOOLEAN is
   O_estimated_stock_out    repl_results.estimated_stock_out%TYPE;
   O_accepted_stock_out     repl_results.accepted_stock_out%TYPE;
   L_order_level	    ord_inv_mgmt.due_ord_lvl%TYPE;
   L_due_ind		    repl_results.due_ind%TYPE;
   L_error_message          VARCHAR2(255);

   cursor C_DUE_ORDER_LEVEL is
      select due_ord_lvl
        from ord_inv_mgmt
       where order_no = I_order_no;

   cursor C_DUE_IND is
      select 'x'
        from repl_results
       where order_no = I_order_no
         and due_ind  = 'Y';
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_order_no',
                                            'NULL', 'NOT NULL');
      return FALSE;
   end if;
   
   O_order_due := FALSE;
 
   SQL_LIB.SET_MARK('OPEN', 'C_DUE_ORDER_LEVEL', 'ORD_INV_MGMT', 'Order no: ' ||
                    to_char(I_order_no));    
   open C_DUE_ORDER_LEVEL;
   SQL_LIB.SET_MARK('FETCH', 'C_DUE_ORDER_LEVEL', 'ORD_INV_MGMT', 'Order no: ' ||
                    to_char(I_order_no)); 
   fetch C_DUE_ORDER_LEVEL into L_order_level;
   if C_DUE_ORDER_LEVEL%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_DUE_ORDER_LEVEL', 'ORD_INV_MGMT', 'Order no: ' ||
                       to_char(I_order_no)); 
      close C_DUE_ORDER_LEVEL;
      RETURN TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_DUE_ORDER_LEVEL', 'ORD_INV_MGMT', 'Order no: ' ||
                       to_char(I_order_no)); 
   close C_DUE_ORDER_LEVEL;

   if L_order_level = 'I' then
      SQL_LIB.SET_MARK('OPEN', 'C_DUE_IND', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Due ind: ' || ('Y'));    
      open C_DUE_IND;
      SQL_LIB.SET_MARK('FETCH', 'C_DUE_IND', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Due ind: ' || ('Y'));    
      fetch C_DUE_IND into L_due_ind;
      SQL_LIB.SET_MARK('CLOSE', 'C_DUE_IND', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Due ind: ' || ('Y'));    
      close C_DUE_IND;
      
      if L_due_ind = 'x' then
         O_order_due := TRUE;
      end if;            
            
   elsif L_order_level = 'O' then
      if ORDER_DUE_SQL.TOTAL_ESO_ASO(O_error_message,
                                     O_estimated_stock_out,
                                     O_accepted_stock_out,
                                     I_order_no,
                                     NULL,
                                     NULL) = FALSE then
         RETURN FALSE;
      end if;
      if O_estimated_stock_out > O_accepted_stock_out then
         O_order_due := TRUE;
      end if;
   end if;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_DUE_SQL.ORDER_DUE', 
                                            to_char(SQLCODE));

      RETURN FALSE;
END ORDER_DUE;
-------------------------------------------------------------------------------------------
FUNCTION   COUNT_ITEM_LOCS        (O_error_message     IN OUT  VARCHAR2,
	                           O_item_locs_total   IN OUT  NUMBER,
                                   O_item_locs_due     IN OUT  NUMBER,
                                   I_order_no          IN      repl_results.order_no%TYPE,
		                   I_item	       IN      repl_results.item%TYPE,
		                   I_location	       IN      repl_results.location%TYPE)
                                  RETURN BOOLEAN is
   cursor C_COUNT_ORDER is
     select count (*)
       from repl_results
      where order_no  = I_order_no
        and order_roq > 0;

   cursor C_COUNT_ITEM is
     select count (*)
       from repl_results
      where order_no  = I_order_no
        and item      = I_item
        and order_roq > 0;

   cursor C_COUNT_LOCATION is
     select count (*)
       from repl_results
      where order_no  = I_order_no
        and location  = I_location
        and order_roq > 0;

   cursor C_COUNT_DUE_ORDER is
     select count (*)
       from repl_results
      where order_no  = I_order_no
        and due_ind   = 'Y'
        and order_roq > 0;

   cursor C_COUNT_DUE_ITEM is
     select count (*)
       from repl_results
      where order_no  = I_order_no
        and item      = I_item
        and due_ind   = 'Y'
        and order_roq > 0;

   cursor C_COUNT_DUE_LOCATION is
     select count (*)
       from repl_results
      where order_no  = I_order_no
        and location  = I_location
        and due_ind   = 'Y'
        and order_roq > 0;
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_order_no',
                                            'NULL', 'NOT NULL');
      return FALSE;
   end if;

   O_item_locs_total := 0;
   O_item_locs_due := 0;

   if I_item is not NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_ITEM', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Item: ' || I_item);
      open C_COUNT_ITEM;
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_ITEM', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Item: ' || I_item);
      fetch C_COUNT_ITEM into O_item_locs_total;
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_ITEM', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Item: ' || I_item);
      close C_COUNT_ITEM;

      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_DUE_ITEM', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
		       'Item: ' || I_item ||
                       'Due ind: ' || ('Y')); 
      open C_COUNT_DUE_ITEM;
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_DUE_ITEM', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
		       'Item: ' || I_item ||
                       'Due ind: ' || ('Y')); 
      fetch C_COUNT_DUE_ITEM into O_item_locs_due;
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_DUE_ITEM', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
		       'Item: ' || I_item ||
                       'Due ind: ' || ('Y')); 
      close C_COUNT_DUE_ITEM;
   elsif I_location is not NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_LOCATION', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Location: ' || to_char(I_location));
      open C_COUNT_LOCATION;
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_LOCATION', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Location: ' || to_char(I_location));
      fetch C_COUNT_LOCATION into O_item_locs_total;
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_LOCATION', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Location: ' || to_char(I_location));
      close C_COUNT_LOCATION;

      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_DUE_LOCATION', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
		       'Location: ' || to_char(I_location) ||
                       'Due ind: ' || ('Y')); 
      open C_COUNT_DUE_LOCATION;
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_DUE_LOCATION', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
		       'Location: ' || to_char(I_location) ||
                       'Due ind: ' || ('Y')); 
      fetch C_COUNT_DUE_LOCATION into O_item_locs_due;
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_DUE_LOCATION', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
		       'Location: ' || to_char(I_location) ||
                       'Due ind: ' || ('Y')); 
      close C_COUNT_DUE_LOCATION;
   else   
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_ORDER', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no));
      open C_COUNT_ORDER;
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_ORDER', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no));
      fetch C_COUNT_ORDER into O_item_locs_total;
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_ORDER', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no)); 
      close C_COUNT_ORDER;
   
      SQL_LIB.SET_MARK('OPEN', 'C_COUNT_DUE_ORDER', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Due ind: ' || ('Y')); 
      open C_COUNT_DUE_ORDER;
      SQL_LIB.SET_MARK('FETCH', 'C_COUNT_DUE_ORDER', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Due ind: ' || ('Y')); 
      fetch C_COUNT_DUE_ORDER into O_item_locs_due;
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_DUE_ORDER', 'REPL_RESULTS', 'Order no: ' ||
                       to_char(I_order_no) ||
                       'Due ind: ' || ('Y')); 
      close C_COUNT_DUE_ORDER;
   end if;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_DUE_SQL.COUNT_ITEM_LOCS', 
                                            to_char(SQLCODE));

      RETURN FALSE;
END COUNT_ITEM_LOCS;
-------------------------------------------------------------------------------------------
FUNCTION   GET_DUE_ORD_IND  (O_error_message          IN OUT     VARCHAR2,
			     O_due_ord_process_ind    IN OUT     ord_inv_mgmt.due_ord_process_ind%TYPE,
                             O_due_ord_ind            IN OUT     ord_inv_mgmt.due_ord_ind%TYPE,
                             I_order_no               IN         repl_results.order_no%TYPE)
                             RETURN BOOLEAN is

   cursor C_DUE_ORDER_IND is
      select due_ord_process_ind,
             due_ord_ind
        from ord_inv_mgmt
       where order_no = I_order_no;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_order_no',
                                            'NULL', 'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_DUE_ORDER_IND', 'ORD_INV_MGMT', 'Order no: ' ||
                    to_char(I_order_no));
   open C_DUE_ORDER_IND;
   SQL_LIB.SET_MARK('FETCH', 'C_DUE_ORDER_IND', 'ORD_INV_MGMT', 'Order no: ' ||
                    to_char(I_order_no));
   fetch C_DUE_ORDER_IND into O_due_ord_process_ind,
			      O_due_ord_ind;
   if C_DUE_ORDER_IND%NOTFOUND then
      O_due_ord_ind := 'N';
      O_due_ord_process_ind := 'N';
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_DUE_ORDER_IND', 'ORD_INV_MGMT', 'Order no: ' ||
                    to_char(I_order_no));
   close C_DUE_ORDER_IND;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_DUE_SQL.GET_DUE_ORD_IND', 
                                            to_char(SQLCODE));

      RETURN FALSE;
END GET_DUE_ORD_IND;
---------------------------------------------------------------------------------------------
END ORDER_DUE_SQL;
/