CREATE OR REPLACE PACKAGE BODY COST_EVENT_LOG_SQL AS
--------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENTS (IO_cost_event_table IN OUT COST_EVENT_TBL_TYPE,
                           I_event_id          IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_event_type        IN     COST_EVENT.EVENT_TYPE%TYPE,
                           I_status            IN     COST_EVENT_RESULT.STATUS%TYPE,
                           I_user_id           IN     COST_EVENT.USER_ID%TYPE,
                           I_event_date_from   IN     COST_EVENT.CREATE_DATETIME%TYPE,
                           I_event_date_to     IN     COST_EVENT.CREATE_DATETIME%TYPE)
IS

   L_program                   VARCHAR2(60)                          := 'COST_EVENT_LOG_SQL.GET_COST_EVENTS';
   L_cost_event_process_id     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE := NULL;

   cursor C_GET_COST_EVENT is
      select ce.cost_event_process_id, 
             cc.event_desc,
             ce.event_type,
             NULL,  /* default value for status */
             ce.user_id,
             ce.create_datetime,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event ce, 
             cost_event_run_type_config cc
       where ce.event_type =   cc.event_type
         and ce.cost_event_process_id = NVL(I_event_id, ce.cost_event_process_id)
         and ce.event_type = NVL(I_event_type, ce.event_type)
         and ce.user_id = NVL (I_user_id, ce.user_id)
         and ce.create_datetime >= NVL(I_event_date_from, ce.create_datetime)
         and ce.create_datetime <= NVL(I_event_date_to, ce.create_datetime)
         and exists (select 'x' 
                       from cost_event_result cer 
                      where cer.cost_event_process_id = ce.cost_event_process_id 
                        and cer.status = NVL(I_status, cer.status)
                        and rownum = 1);

   cursor C_GET_COST_EVENT_STATUS is
      select cd.code_desc
        from cost_event_result cr,
             code_detail cd
       where cost_event_process_id = L_cost_event_process_id
         and cr.status = cd.code
         and cr.status = NVL(I_status, cr.status)
         and cd.code_type = 'CES'
       order by DECODE(cr.status, 'E', 1, 'R', 2, 'N', 3, 'C', 4);

BEGIN

   open  C_GET_COST_EVENT;
   fetch C_GET_COST_EVENT BULK COLLECT into IO_cost_event_table;
   close C_GET_COST_EVENT;

   for i IN 1..IO_cost_event_table.COUNT loop
      L_cost_event_process_id :=  IO_cost_event_table(i).cost_event_process_id;
      ---
      open C_GET_COST_EVENT_STATUS;
      fetch C_GET_COST_EVENT_STATUS into IO_cost_event_table(i).status_desc;
      close C_GET_COST_EVENT_STATUS;
      ---               
   end loop;

EXCEPTION
   when OTHERS then
      IO_cost_event_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE));
      IO_cost_event_table(1).return_code := 'FALSE';
      return;
      
END GET_COST_EVENTS;
--------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_RESULT (IO_cost_event_result_table IN OUT COST_EVENT_RESULT_TBL_TYPE,
                                 I_event_id                 IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                 I_event_thread_id          IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                                 I_status                   IN     COST_EVENT_RESULT.STATUS%TYPE)

IS
   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_RESULT';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_RESULT is
      select cr.cost_event_process_id, 
             cr.thread_id,
             TO_CHAR(cr.create_datetime, 'YYYYMMDD'),
             cc.event_desc,
             cr.status, 
             cd.code_desc,
             cr.error_message,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event ce,
             cost_event_result cr,
             cost_event_run_type_config cc,
             code_detail cd 
       where ce.cost_event_process_id = cr.cost_event_process_id
         and ce.event_type =   cc.event_type
         and cr.status = cd.code
         and cd.code_type = 'CES'
         and ce.cost_event_process_id = NVL(I_event_id, ce.cost_event_process_id)
         and cr.thread_id = NVL(I_event_thread_id, cr.thread_id)
         and cr.status = NVL(I_status, cr.status);

BEGIN 

   open  C_GET_COST_EVENT_RESULT;
   fetch C_GET_COST_EVENT_RESULT bulk collect into IO_cost_event_result_table;
   close C_GET_COST_EVENT_RESULT;

EXCEPTION
   when OTHERS then
      IO_cost_event_result_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                        SQLERRM,
                                                                        L_program,
                                                                        to_char(SQLCODE));
      IO_cost_event_result_table(1).return_code := 'FALSE';
      return;
      
END GET_COST_EVENT_RESULT;
--------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_THREAD (IO_cost_event_thread_table IN OUT COST_EVENT_THREAD_TBL_TYPE,
                                 I_event_id                 IN     COST_EVENT_THREAD.COST_EVENT_PROCESS_ID%TYPE,
                                 I_event_thread_id          IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_THREAD';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_THREAD is
      select ct.cost_event_process_id, 
             ct.thread_id,
             ct.item,
             ct.supplier,
             ct.origin_country_id,
             ct.location,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_thread ct
       where ct.cost_event_process_id = NVL(I_event_id, ct.cost_event_process_id)
         and ct.thread_id = NVL(I_event_thread_id, ct.thread_id);

BEGIN

   open  C_GET_COST_EVENT_THREAD;
   fetch C_GET_COST_EVENT_THREAD bulk collect into IO_cost_event_thread_table;
   close C_GET_COST_EVENT_THREAD;

EXCEPTION
   when OTHERS then
      IO_cost_event_thread_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                        SQLERRM,
                                                                        L_program,
                                                                        to_char(SQLCODE));
      IO_cost_event_thread_table(1).return_code := 'FALSE';
      return;
      
END GET_COST_EVENT_THREAD;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_SUPPC_DETAIL (IO_supp_country_table IN OUT SUPP_COUNTRY_TBL_TYPE,
                                       I_event_id            IN     COST_EVENT_SUPP_COUNTRY.COST_EVENT_PROCESS_ID%TYPE)
IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_SUPPC_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_SUPPC is
      select cs.cost_event_process_id, 
             cs.item,
             im.item_desc,
             cs.supplier,
             s.sup_name,
             cs.origin_country_id,
             c.country_desc,
             cs.location,
             vl.location_name,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_supp_country cs,
             item_master im,
             sups s,
             country c,
             v_location vl
       where cs.cost_event_process_id = NVL(I_event_id, cs.cost_event_process_id)
         and cs.item = im.item         
         and cs.supplier = s.supplier
         and cs.origin_country_id = c.country_id
         and cs.location = vl.location_id;
   
BEGIN

   open  C_GET_COST_EVENT_SUPPC;
   fetch C_GET_COST_EVENT_SUPPC bulk collect into IO_supp_country_table;
   close C_GET_COST_EVENT_SUPPC;

EXCEPTION
   when OTHERS then
      IO_supp_country_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE));
      IO_supp_country_table(1).return_code := 'FALSE';
      return;
      
END GET_COST_EVENT_SUPPC_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_CC_DETAIL (IO_cost_change_table IN OUT COST_CHANGE_TBL_TYPE,
                                    I_event_id           IN     COST_EVENT_COST_CHG.COST_EVENT_PROCESS_ID%TYPE)
IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_CC_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_CC is
      select cc.cost_event_process_id,
             cc.cost_change,
             ch.cost_change_desc,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_cost_chg cc,
             cost_susp_sup_head ch
       where cc.cost_event_process_id = NVL(I_event_id, cc.cost_event_process_id)
         and ch.cost_change = cc.cost_change;

BEGIN

   open  C_GET_COST_EVENT_CC;
   fetch C_GET_COST_EVENT_CC bulk collect into IO_cost_change_table;
   close C_GET_COST_EVENT_CC;

EXCEPTION
   when OTHERS then
      IO_cost_change_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE));
      IO_cost_change_table(1).return_code := 'FALSE';
      return;
      
END GET_COST_EVENT_CC_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_NIL_DETAIL(IO_new_itemloc_table IN OUT NEW_ITEM_LOC_TBL_TYPE,
                                    I_event_id           IN     COST_EVENT_NIL.COST_EVENT_PROCESS_ID%TYPE)
IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_NIL_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_NIL is
      select cn.cost_event_process_id,
             cn.item,
             im.item_desc,
             cn.location,
             vl.location_name,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_nil cn,
             item_master im,
             v_location vl
       where cn.cost_event_process_id = NVL(I_event_id, cn.cost_event_process_id)
         and cn.item = im.item
         and cn.location = vl.location_id;

BEGIN

   open  C_GET_COST_EVENT_NIL;
   fetch C_GET_COST_EVENT_NIL bulk collect into IO_new_itemloc_table;
   close C_GET_COST_EVENT_NIL;
   
EXCEPTION
   when OTHERS then
      IO_new_itemloc_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE));
      IO_new_itemloc_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_NIL_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_RECLASS_DETAIL(IO_reclass_table IN OUT RECLASS_TBL_TYPE,
                                        I_event_id       IN     COST_EVENT_RECLASS.COST_EVENT_PROCESS_ID%TYPE)
IS
   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_RECLASS_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_RECLASS is
      select cr.cost_event_process_id,
             cr.reclass_no,
             rh.reclass_desc,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_reclass cr,        
             reclass_head rh
       where cr.cost_event_process_id = NVL(I_event_id, cr.cost_event_process_id)
         and cr.reclass_no = rh.reclass_no;

BEGIN

   open  C_GET_COST_EVENT_RECLASS;
   fetch C_GET_COST_EVENT_RECLASS bulk collect into IO_reclass_table;
   close C_GET_COST_EVENT_RECLASS;
   
EXCEPTION
   when OTHERS then
      IO_reclass_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                              SQLERRM,
                                                              L_program,
                                                              to_char(SQLCODE));
      IO_reclass_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_RECLASS_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_MERCH_DETAIL(IO_merch_hier_table IN OUT MERCH_HIER_TBL_TYPE,
                                      I_event_id          IN     COST_EVENT_MERCH_HIER.COST_EVENT_PROCESS_ID%TYPE)
IS
   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_MERCH_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_MERCH is
      select cm.cost_event_process_id,
             cm.new_division,
             nd.div_name,
             cm.old_division,
             od.div_name,
             cm.new_group_no,
             ng.group_name,
             cm.old_group_no,
             og.group_name,
             cm.dept,
             d.dept_name,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_merch_hier cm,
             division nd,  /* new division */
             division od,  /* old division */
             groups ng,
             groups og, 
             deps d
       where cm.cost_event_process_id = NVL(I_event_id, cm.cost_event_process_id)
         and cm.new_division = nd.division(+) 
         and cm.old_division = od.division(+)
         and cm.new_group_no =  ng.group_no(+)
         and cm.old_group_no = og.group_no(+)
         and cm.dept = d.dept(+);

BEGIN

   open  C_GET_COST_EVENT_MERCH;
   fetch C_GET_COST_EVENT_MERCH bulk collect into IO_merch_hier_table;
   close C_GET_COST_EVENT_MERCH;

EXCEPTION
   when OTHERS then
      IO_merch_hier_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE));
      IO_merch_hier_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_MERCH_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_DEAL_DETAIL(IO_deal_table       IN OUT DEAL_TBL_TYPE,
                                     I_event_id          IN     COST_EVENT_DEAL.COST_EVENT_PROCESS_ID%TYPE)
IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_DEAL_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_DEAL is
      select cd.cost_event_process_id,
             dh.deal_id,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_deal cd,        
             deal_head dh
       where cd.cost_event_process_id = NVL(I_event_id, cd.cost_event_process_id)
         and cd.deal_id = dh.deal_id;
BEGIN

   open  C_GET_COST_EVENT_DEAL;
   fetch C_GET_COST_EVENT_DEAL bulk collect into IO_deal_table;
   close C_GET_COST_EVENT_DEAL;

EXCEPTION
   when OTHERS then
      IO_deal_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE));
      IO_deal_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_DEAL_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_ITC_ZONE_DETAIL(IO_item_cost_zone_table       IN OUT ITEM_COST_ZONE_TBL_TYPE,
                                         I_event_id                   IN     COST_EVENT_ITEM_COST_ZONE.COST_EVENT_PROCESS_ID%TYPE)
IS
   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_ITC_ZONE_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_ITC_ZONE is
      select ci.cost_event_process_id,
             ci.item,
             im.item_desc,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_item_cost_zone  ci,
             item_master im
       where ci.cost_event_process_id = NVL(I_event_id, ci.cost_event_process_id)
         and ci.item = im.item;
BEGIN

   open  C_GET_COST_EVENT_ITC_ZONE;
   fetch C_GET_COST_EVENT_ITC_ZONE bulk collect into IO_item_cost_zone_table;
   close C_GET_COST_EVENT_ITC_ZONE;
   
EXCEPTION
   when OTHERS then
      IO_item_cost_zone_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE));
      IO_item_cost_zone_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_ITC_ZONE_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_PPACK_DETAIL (IO_prim_pack_table            IN OUT PRIM_PACK_TBL_TYPE,
                                       I_event_id                    IN     COST_EVENT_PRIM_PACK.COST_EVENT_PROCESS_ID%TYPE)
IS
   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_PPACK_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_PPACK is
      select cp.cost_event_process_id,
             cp.item,
             im.item_desc,
             cp.location,
             vl.location_name,
             cp.pack_no,
             ip.item_desc,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_prim_pack cp,
             item_master im,
             item_master ip,
             v_location vl
       where cp.cost_event_process_id = NVL(I_event_id, cp.cost_event_process_id)
         and cp.item = im.item
         and cp.pack_no = ip.item
         and cp.location = vl.location_id;

BEGIN

   open  C_GET_COST_EVENT_PPACK;
   fetch C_GET_COST_EVENT_PPACK bulk collect into IO_prim_pack_table;
   close C_GET_COST_EVENT_PPACK;

EXCEPTION
   when OTHERS then
      IO_prim_pack_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE));
      IO_prim_pack_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_PPACK_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_ORG_HIER_DETAIL (IO_org_hier_table             IN OUT ORG_HIER_TBL_TYPE,
                                          I_event_id                    IN     COST_EVENT_ORG_HIER.COST_EVENT_PROCESS_ID%TYPE)
IS
   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_ORG_HIER_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_ORG_HIER is
      select co.cost_event_process_id,
             co.new_chain,
             nc.chain_name,
             co.old_chain,
             oc.chain_name,
             co.new_area,
             na.area_name,
             co.old_area,
             oa.area_name,
             co.new_region,
             nr.region_name,
             co.old_region,
             lr.region_name,
             co.new_district,
             nd.district_name,
             co.old_district,
             od.district_name,
             co.location,
             st.store_name  location_name,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_org_hier co,
             chain nc,
             chain oc,
             area na,
             area oa,
             region nr,
             region lr,
             district nd,  /* new district */
             district od,  /* old district */
             store st
       where co.cost_event_process_id = NVL(I_event_id, co.cost_event_process_id)
         and co.new_chain = nc.chain(+)
         and co.old_chain = oc.chain(+)
         and co.new_area = na.area(+)
         and co.old_area = oa.area(+)
         and co.new_region = nr.region(+)
         and co.old_region = lr.region(+)
         and co.new_district = nd.district(+)
         and co.old_district = od.district(+)
         and co.location = st.store(+)
      union all 
      select co.cost_event_process_id,
             NULL new_chain,
             NULL chain_name,
             NULL old_chain,
             NULL chain_name,
             NULL new_area,
             NULL area_name,
             NULL old_area,
             NULL area_name,
             NULL new_region,
             NULL region_name,
             NULL old_region,
             NULL region_name,
             NULL new_district,
             NULL district_name,
             NULL old_district,
             NULL district_name,
             co.location,
             wh.wh_name location_name,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_org_hier co,
             wh wh
       where co.cost_event_process_id = NVL(I_event_id, co.cost_event_process_id)
         and co.location =wh.wh
         and (org_hier_type = 1 or org_hier_type = 50 or org_hier_type is null)
         and stockholding_ind = 'Y'
      union all
      select co.cost_event_process_id,
             co.new_chain,
             nc.chain_name,
             co.old_chain,
             oc.chain_name,
             NULL new_area,
             NULL area_name,
             NULL old_area,
             NULL area_name,
             NULL new_region,
             NULL region_name,
             NULL old_region,
             NULL region_name,
             NULL new_district,
             NULL district_name,
             NULL old_district,
             NULL district_name,
             co.location,
             wh.wh_name location_name,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_org_hier co,
             chain nc,
             chain oc,
             wh wh
       where co.cost_event_process_id = NVL(I_event_id, co.cost_event_process_id)
         and co.new_chain = nc.chain(+)
         and co.old_chain = oc.chain(+)
         and co.location =wh.wh
         and org_hier_type = 10
         and stockholding_ind = 'Y'
      union all
      select co.cost_event_process_id,
             co.new_chain,
             nc.chain_name,
             co.old_chain,
             oc.chain_name,
             co.new_area,
             na.area_name,
             co.old_area,
             oa.area_name,
             NULL new_region,
             NULL region_name,
             NULL old_region,
             NULL region_name,
             NULL new_district,
             NULL district_name,
             NULL old_district,
             NULL district_name,
             co.location,
             wh.wh_name location_name,
             NULL, /* default error message */
             NULL  /* default return code */
             from cost_event_org_hier co,
             chain nc,
             chain oc,
             area na,
             area oa,
             wh wh
       where co.cost_event_process_id = NVL(I_event_id, co.cost_event_process_id)
         and co.new_chain = nc.chain(+)
         and co.old_chain = oc.chain(+)
         and co.new_area = na.area(+)
         and co.old_area = oa.area(+)
         and co.location =wh.wh
         and org_hier_type = 20
         and stockholding_ind = 'Y'
      union all
      select co.cost_event_process_id,
             co.new_chain,
             nc.chain_name,
             co.old_chain,
             oc.chain_name,
             co.new_area,
             na.area_name,
             co.old_area,
             oa.area_name,
             co.new_region,
             nr.region_name,
             co.old_region,
             lr.region_name,
             NULL new_district,
             NULL district_name,
             NULL old_district,
             NULL district_name,
             co.location,
             wh.wh_name location_name,
             NULL, /* default error message */
             NULL  /* default return code */
             from cost_event_org_hier co,
             chain nc,
             chain oc,
             area na,
             area oa,
             region nr,
             region lr,
             wh wh
       where co.cost_event_process_id = NVL(I_event_id, co.cost_event_process_id)
         and co.new_chain = nc.chain(+)
         and co.old_chain = oc.chain(+)
         and co.new_area = na.area(+)
         and co.old_area = oa.area(+)
         and co.new_region = nr.region(+)
         and co.old_region = lr.region(+)
         and co.location =wh.wh
         and org_hier_type = 30
         and stockholding_ind = 'Y'
      union all
      select co.cost_event_process_id,
             co.new_chain,
             nc.chain_name,
             co.old_chain,
             oc.chain_name,
             co.new_area,
             na.area_name,
             co.old_area,
             oa.area_name,
             co.new_region,
             nr.region_name,
             co.old_region,
             lr.region_name,
             co.new_district,
             nd.district_name,
             co.old_district,
             od.district_name,
             co.location location,
             wh.wh_name location_name,
             NULL, /* default error message */
             NULL  /* default return code */
             from cost_event_org_hier co,
             chain nc,
             chain oc,
             area na,
             area oa,
             region nr,
             region lr,
             wh wh,
             district od,
             district nd
       where co.cost_event_process_id = NVL(I_event_id, co.cost_event_process_id)
         and co.new_chain = nc.chain(+)
         and co.old_chain = oc.chain(+)
         and co.new_area = na.area(+)
         and co.old_area = oa.area(+)
         and co.new_region = nr.region(+)
         and co.old_region = lr.region(+)
         and co.new_district = nd.district(+)
         and co.old_district = od.district(+)
         and co.location =wh.wh
         and org_hier_type = 40
         and stockholding_ind = 'Y';
                                                    
BEGIN

   open C_GET_COST_EVENT_ORG_HIER;
   fetch C_GET_COST_EVENT_ORG_HIER bulk collect into IO_org_hier_table;
   close C_GET_COST_EVENT_ORG_HIER;
   
EXCEPTION
   when OTHERS then
      IO_org_hier_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE));
      IO_org_hier_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_ORG_HIER_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_LOC_COST_ZONE_DETAIL (IO_location_cost_zone_table   IN OUT LOCATION_COST_ZONE_TBL_TYPE,
                                         I_event_id                    IN     COST_EVENT_COST_ZONE.COST_EVENT_PROCESS_ID%TYPE)
IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_LOC_COST_ZONE_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_LOC_COST_ZONE is
      select cc.cost_event_process_id,
             cc.zone_group_id,
             cg.description,
             cc.new_zone_id,
             nc.description,
             cc.old_zone_id,
             oc.description,
             cc.location,
             vl.location_name,             
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_cost_zone cc,
             cost_zone_group cg,
             cost_zone nc,             
             cost_zone oc,
             v_location vl                          
       where cc.cost_event_process_id = NVL(I_event_id, cc.cost_event_process_id)
        and cc.zone_group_id = cg.zone_group_id (+)
        and (cc.new_zone_id= nc.zone_id(+) and
             cc.zone_group_id = nc.zone_group_id (+))
        and (cc.old_zone_id = oc.zone_id (+) and
             cc.zone_group_id = oc.zone_group_id (+))
        and cc.location = vl.location_id (+);
BEGIN

   open C_GET_COST_LOC_COST_ZONE;
   fetch C_GET_COST_LOC_COST_ZONE bulk collect into IO_location_cost_zone_table;
   close C_GET_COST_LOC_COST_ZONE;
   
EXCEPTION
   when OTHERS then
      IO_location_cost_zone_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                         SQLERRM,
                                                                         L_program,
                                                                         to_char(SQLCODE));
      IO_location_cost_zone_table(1).return_code := 'FALSE';
      return;
END GET_COST_LOC_COST_ZONE_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_SP_HIER_DETAIL (IO_supp_hier_table            IN OUT SUPP_HIER_TBL_TYPE,
                                         I_event_id                    IN     COST_EVENT_SUPP_HIER.COST_EVENT_PROCESS_ID%TYPE)
IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_SP_HIER_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_SP_HIER is
      select cs.cost_event_process_id,
             cs.item,
             cs.supplier,
             cs.origin_country_id,
             cs.location,
             cs.new_supp_hier_lvl_1,
             cs.old_supp_hier_lvl_1,
             cs.new_supp_hier_lvl_2,
             cs.old_supp_hier_lvl_2,
             cs.new_supp_hier_lvl_3,
             cs.old_supp_hier_lvl_3,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_supp_hier cs
       where cs.cost_event_process_id = NVL(I_event_id, cs.cost_event_process_id);

BEGIN

   open C_GET_COST_SP_HIER;
   fetch C_GET_COST_SP_HIER bulk collect into IO_supp_hier_table;
   close C_GET_COST_SP_HIER;   

EXCEPTION
   when OTHERS then
      IO_supp_hier_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE));
      IO_supp_hier_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_SP_HIER_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_ELC_DETAIL (IO_elc_table            IN OUT ELC_TBL_TYPE,
                                     I_event_id              IN     COST_EVENT_ELC.COST_EVENT_PROCESS_ID%TYPE)
IS


   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_ELC_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_ELC is
      select ce.cost_event_process_id,
             ce.item,
             im.item_desc,
             ce.supplier,
             s.sup_name,
             ce.origin_country_id,
             c.country_desc,
             ce.cost_zone_group,
             cg.description,
             ce.cost_zone,
             cz.description,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_elc ce,
             item_master im,
             sups s,
             country c,
             cost_zone_group cg,
             cost_zone cz
       where ce.cost_event_process_id = NVL(I_event_id, ce.cost_event_process_id)
         and ce.item = im.item
         and ce.supplier = s.supplier
         and ce.origin_country_id = c.country_id
         and ce.cost_zone_group = cg.zone_group_id
         and cz.zone_group_id = cg.zone_group_id
         and ce.cost_zone = cz.zone_id;

BEGIN

   open C_GET_COST_EVENT_ELC;
   fetch C_GET_COST_EVENT_ELC bulk collect into IO_elc_table;
   close C_GET_COST_EVENT_ELC;   

EXCEPTION
   when OTHERS then
      IO_elc_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                           SQLERRM,
                                                           L_program,
                                                           to_char(SQLCODE));
      IO_elc_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_ELC_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_TEMPLATE_DETAIL (IO_template_table  IN OUT TEMPL_TBL_TYPE,
                                          I_event_id         IN     COST_EVENT_COST_TMPL.COST_EVENT_PROCESS_ID%TYPE)
IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_TEMPLATE_DETAIL';
   
   cursor C_GET_COST_EVENT_TEMPL_DETAIL is
      select ct.cost_event_process_id,
             ct.templ_id,
             tm.templ_desc,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_cost_tmpl ct,
             wf_cost_buildup_tmpl_head tm
       where ct.cost_event_process_id = NVL(I_event_id, ct.cost_event_process_id)
         and ct.templ_id = tm.templ_id;

BEGIN
   
   open  C_GET_COST_EVENT_TEMPL_DETAIL;
   fetch C_GET_COST_EVENT_TEMPL_DETAIL bulk collect into IO_template_table;
   close C_GET_COST_EVENT_TEMPL_DETAIL;

EXCEPTION
   when OTHERS then
      IO_template_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                SQLERRM,
                                                                L_program,
                                                                to_char(SQLCODE));
      IO_template_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_TEMPLATE_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_T_RELN_DETAIL (IO_costbldup_table IN OUT COSTBLDUP_TBL_TYPE,
                                        I_event_id         IN     COST_EVENT_COST_RELATIONSHIP.COST_EVENT_PROCESS_ID%TYPE)
IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_COSTBLDUP_DETAIL';
   
   cursor C_GET_COST_BUILDUP_REL_DETAIL is
      select cr.cost_event_process_id,
             cr.dept,
             cr.class,
             cr.subclass,
             cr.location,
             cr.templ_id,
             tm.templ_desc,
             cr.old_start_date,
             cr.old_end_date,
             cr.new_start_date,
             cr.new_end_date,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_cost_relationship cr,
             wf_cost_buildup_tmpl_head tm
       where cr.cost_event_process_id = NVL(I_event_id, cr.cost_event_process_id)
         and cr.templ_id = tm.templ_id;

BEGIN
   
   open  C_GET_COST_BUILDUP_REL_DETAIL;
   fetch C_GET_COST_BUILDUP_REL_DETAIL bulk collect into IO_costbldup_table;
   close C_GET_COST_BUILDUP_REL_DETAIL;

EXCEPTION
   when OTHERS then
      IO_costbldup_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                SQLERRM,
                                                                L_program,
                                                                to_char(SQLCODE));
      IO_costbldup_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_T_RELN_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_DP_DETAIL (IO_dealpassthru_table IN OUT DEALPASSTHRU_TBL_TYPE,
                                    I_event_id            IN     COST_EVENT_DEAL_PASSTHRU.COST_EVENT_PROCESS_ID%TYPE)
IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_DP_DETAIL';
   
    cursor C_GET_COST_EVENT_DP_DETAIL is
      select cd.cost_event_process_id,
             cd.dept,
             cd.supplier,
             cd.loc_type,
             cd.location,
             cd.costing_loc,
             wh.wh_name,
             cd.passthru_pct,
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_deal_passthru cd,
             v_wh wh            
       where cd.cost_event_process_id = NVL(I_event_id, cd.cost_event_process_id)
         and cd.costing_loc = wh.wh;

BEGIN
   
   open  C_GET_COST_EVENT_DP_DETAIL;
   fetch C_GET_COST_EVENT_DP_DETAIL bulk collect into IO_dealpassthru_table;
   close C_GET_COST_EVENT_DP_DETAIL;

EXCEPTION
   when OTHERS then
      IO_dealpassthru_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                   SQLERRM,
                                                                   L_program,
                                                                   to_char(SQLCODE));
      IO_dealpassthru_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_DP_DETAIL;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_AUDIT_DETAIL (IO_audit_table          IN OUT AUDIT_TBL_TYPE,
                                       I_event_id              IN     COST_EVENT_RESULT.COST_EVENT_PROCESS_ID%TYPE)
IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_AUDIT_DETAIL';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_AUDIT is
      select cr.cost_event_process_id, 
             cr.thread_id,
             cr.attempt_num,
             cr.error_message,
             cr.retry_user_id,
             TO_CHAR(cr.create_datetime, 'YYYYMMDD'),
             NULL, /* default error message */
             NULL  /* default return code */
        from cost_event_result cr       
       where cr.cost_event_process_id = NVL(I_event_id, cr.cost_event_process_id);

BEGIN

   open C_GET_COST_EVENT_AUDIT;
   fetch C_GET_COST_EVENT_AUDIT bulk collect into IO_audit_table ;
   close C_GET_COST_EVENT_AUDIT;

EXCEPTION
   when OTHERS then
      IO_audit_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE));
      IO_audit_table(1).return_code := 'FALSE';
      return;
END GET_COST_EVENT_AUDIT_DETAIL;
----------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_process_tbl       IN       COST_EVENT_DET_TBL)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.PROCESS';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN

   ---
   for i IN 1..I_process_tbl.COUNT loop
      if FUTURE_COST_EVENT_SQL.REPROCESS_COST_EVENT (L_error_message,
                                                     I_process_tbl(i).cost_event_process_id,
                                                     I_process_tbl(i).thread_id) = FALSE then
         O_error_message := L_error_message;
         return FALSE;
      end if;                        
   end loop;
   ---
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS;
----------------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_COST_EVENT_CC (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_cc_exists         OUT      BOOLEAN,
                              I_cost_change       IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                              I_status            IN       COST_SUSP_SUP_HEAD.STATUS%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.CHECK_COST_EVENT_CC';
   L_invalid_param  VARCHAR2(30)             := NULL;
   L_error_message  RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_cc_exist       VARCHAR2(1)              := NULL;

        
    cursor C_FC_CC_EXISTS is
       select 'x'
        from future_cost
       where cost_change = I_cost_change
         and rownum = 1; 
       
    cursor C_FC_WKS_CC_EXISTS is
       select 'x'
        from future_cost_workspace
       where cost_change = I_cost_change
         and rownum = 1;

BEGIN
   --
   if I_cost_change is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cost_change',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_status',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_cc_exists := FALSE;
   ---
   if I_status in ('W','S') then
      SQL_LIB.SET_MARK('OPEN',
                       'C_FC_WKS_CC_EXISTS',
                       'FUTURE_COST_WORKSPACE',
                       I_cost_change);
      open C_FC_WKS_CC_EXISTS;
      --
      SQL_LIB.SET_MARK('FETCH',
                       'C_FC_WKS_CC_EXISTS',
                       'FUTURE_COST_WORKSPACE',
                       I_cost_change);
      fetch C_FC_WKS_CC_EXISTS into L_cc_exist;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FC_WKS_CC_EXISTS',
                       'FUTURE_COST_WORKSPACE',
                       I_cost_change);
      close C_FC_WKS_CC_EXISTS;
      
   elsif I_status in ('A','E') then
      SQL_LIB.SET_MARK('OPEN',
                       'C_FC_CC_EXISTS',
                       'FUTURE_COST',
                       I_cost_change);
      open C_FC_CC_EXISTS;
      --
      SQL_LIB.SET_MARK('FETCH',
                       'C_FC_CC_EXISTS',
                       'FUTURE_COST',
                       I_cost_change);
      fetch C_FC_CC_EXISTS into L_cc_exist;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_FC_CC_EXISTS',
                       'FUTURE_COST',
                       I_cost_change);
      close C_FC_CC_EXISTS;
   end if; 
   ---
   if L_cc_exist = 'x' then
         O_cc_exists := TRUE;
   end if;
   ---
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END CHECK_COST_EVENT_CC;
----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_COST_EVENT_TYPE_DESC (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_cc_type_desc      OUT      COST_EVENT_RUN_TYPE_CONFIG.EVENT_DESC%TYPE,
                                   O_event_run_type    OUT      COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE,
                                   I_event_type        IN       COST_EVENT_RUN_TYPE_CONFIG.EVENT_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_COST_EVENT_TYPE_DESC';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_COST_EVENT_TYPE is
      select cc.event_desc,
             cc.event_run_type
        from cost_event_run_type_config cc
       where cc.event_type = I_event_type;

BEGIN
     --
   if I_event_type is NULL then
      --
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_event_type,
                                            L_program,
                                            NULL);
      return FALSE;
      --
   else
      --
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_COST_EVENT_TYPE',
                       'COST_EVENT_COST_CHG',
                       I_event_type);
      open C_GET_COST_EVENT_TYPE;
      --
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_COST_EVENT_TYPE',
                       'COST_EVENT_COST_CHG',
                       I_event_type);
      fetch C_GET_COST_EVENT_TYPE into O_cc_type_desc,
                                       O_event_run_type;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_COST_EVENT_TYPE',
                       'COST_EVENT_COST_CHG',
                       I_event_type);
      close C_GET_COST_EVENT_TYPE;
      --
   end if;
   ---
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END GET_COST_EVENT_TYPE_DESC;
----------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COST_EVENT_PROCESS_ID (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_exists            OUT      BOOLEAN,                                         
                                         I_event_id          IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.VALIDATE_COST_EVENT_PROCESS_ID';
   L_invalid_param     VARCHAR2(30)             := NULL;
   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_cost_event_exist  VARCHAR2(1)              := NULL;

   cursor C_COST_EVENT_EXISTS is
      select 'x'
        from cost_event ce
       where ce.cost_event_process_id = I_event_id;

BEGIN

   --
   if I_event_id is NULL then
      --
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_event_id,
                                            L_program,
                                            NULL);
      return FALSE;
      --
   else
      --
      O_exists := FALSE;
      --
      SQL_LIB.SET_MARK('OPEN',
                       'C_COST_EVENT_EXISTS',
                       'COST_EVENT',
                       I_event_id);
      open C_COST_EVENT_EXISTS;
      --
      SQL_LIB.SET_MARK('FETCH',
                       'C_COST_EVENT_EXISTS',
                       'COST_EVENT',
                       I_event_id);
      fetch C_COST_EVENT_EXISTS into L_cost_event_exist;
      --
      if L_cost_event_exist = 'x' then
         O_exists := TRUE;
      end if;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_COST_EVENT_EXISTS',
                       'COST_EVENT',
                       I_event_id);
      close C_COST_EVENT_EXISTS;
      --
   end if;
   ---
   return TRUE;   
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END VALIDATE_COST_EVENT_PROCESS_ID;
----------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COST_EVENT_THREAD (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists            OUT      BOOLEAN,                                         
                                     I_event             IN       COST_EVENT_THREAD.COST_EVENT_PROCESS_ID%TYPE,
                                     I_thread_id         IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.VALIDATE_USER_ID';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_thread_exist      VARCHAR2(1)            := NULL;

   cursor C_COST_EVENT_THREAD_EXISTS is
      select 'x'
        from cost_event_result cr
       where cr.cost_event_process_id = I_event
         and cr.thread_id = I_thread_id;

BEGIN
   --
   if I_event is NULL then
      --
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_event,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   --
   if I_thread_id is NULL then
      --
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_thread_id,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   --
   O_exists := FALSE;
   --
   SQL_LIB.SET_MARK('OPEN',
                    'C_COST_EVENT_THREAD_EXISTS',
                    'COST_EVENT_THREAD',
                    I_event);
   open C_COST_EVENT_THREAD_EXISTS;
   --
   SQL_LIB.SET_MARK('FETCH',
                    'C_COST_EVENT_THREAD_EXISTS',
                    'COST_EVENT_THREAD',
                    I_event);
   fetch C_COST_EVENT_THREAD_EXISTS into L_thread_exist;
   --
   if L_thread_exist = 'x' then
      O_exists := TRUE;
   end if;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_COST_EVENT_THREAD_EXISTS',
                    'COST_EVENT_THREAD',
                    I_event);
   close C_COST_EVENT_THREAD_EXISTS;
   --
   return TRUE;   
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END VALIDATE_COST_EVENT_THREAD;
----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOCS (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        IO_item_loc_tbl     IN OUT   OBJ_ITEMLOC_TBL,                                         
                        I_item              IN       ITEM_MASTER.ITEM%TYPE,
                        I_default_down_ind  IN       VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60)             := 'COST_EVENT_LOG_SQL.GET_ITEM_LOCS';
   L_invalid_param   VARCHAR2(30)             := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   cursor C_GET_ITEM_LOCS is
      select OBJ_ITEMLOC_REC(il.item,
                             il.loc)
        from item_loc il,
             item_master im
       where il.item        = im.item
         and im.item        = I_item
         and im.item_level  = im.tran_level
		 and il.loc_type   != 'E';
                
   cursor C_GET_ITEM_CHILDERN_LOCS is
      select OBJ_ITEMLOC_REC(il.item,
                             il.loc)
        from item_loc il,
             item_master im
       where il.item              = im.item
         and (im.item             = I_item
          or  im.item_parent      = I_item
          or  im.item_grandparent = I_item)
         and im.item_level        = im.tran_level
         and im.status            = 'A'
         and il.loc_type         != 'E';		 

BEGIN
   ---
   if I_item is NULL then
   --
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_item,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   --
   if I_default_down_ind = 'N' then 
      open  C_GET_ITEM_LOCS;
      fetch C_GET_ITEM_LOCS bulk collect into IO_item_loc_tbl;
      close C_GET_ITEM_LOCS;
   else 
      open  C_GET_ITEM_CHILDERN_LOCS;
      fetch C_GET_ITEM_CHILDERN_LOCS bulk collect into IO_item_loc_tbl;
      close C_GET_ITEM_CHILDERN_LOCS;
   end if;
   ---
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END GET_ITEM_LOCS;
----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOCS_WRP(
    O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    IO_item_loc_tbl    IN OUT OBJ_ITEMLOC_TBL,
    I_item             IN ITEM_MASTER.ITEM%TYPE,
    I_default_down_ind IN VARCHAR2 DEFAULT 'N')
  RETURN INTEGER
IS
  L_program VARCHAR2(60) := 'COST_EVENT_LOG_SQL.GET_ITEM_LOCS_WRP';
BEGIN
  ---
  IF GET_ITEM_LOCS (O_error_message, IO_item_loc_tbl, I_item, I_default_down_ind)=FALSE THEN
    RETURN 0;
  END IF;
  ---
  RETURN 1;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN 0;
END GET_ITEM_LOCS_WRP;
----------------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_IED_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_KEY%TYPE,
                         I_item          IN     ITEM_EXP_DETAIL.ITEM%TYPE,
                         I_supplier      IN     ITEM_EXP_DETAIL.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(62) := 'INSERT_IED_TEMP';
   L_exist              VARCHAR2(1)  := NULL;

   cursor C_IED_EXIST is
      select 'x'
        from item_exp_detail
       where item = I_item
         and supplier = I_supplier
         and rownum = 1;

BEGIN

   open C_IED_EXIST;
   fetch C_IED_EXIST into L_exist;
   ---
   if C_IED_EXIST%NOTFOUND then
      close C_IED_EXIST;
      return TRUE;
   end if;
   ---
   close C_IED_EXIST;

   if L_exist is not NULL then
      ---
      insert into gtt_item_exp_detail(item,
                                      supplier,
                                      item_exp_type,
                                      item_exp_seq,
                                      comp_id,
                                      cvb_code,
                                      comp_currency,
                                      comp_rate,
                                      per_count,
                                      per_count_uom,
                                      est_exp_value,
                                      nom_flag_1,
                                      nom_flag_2,
                                      nom_flag_3,
                                      nom_flag_4,
                                      nom_flag_5)
                               select item,
                                      supplier,
                                      item_exp_type,
                                      item_exp_seq,
                                      comp_id,
                                      cvb_code,
                                      comp_currency,
                                      comp_rate,
                                      per_count,
                                      per_count_uom,
                                      est_exp_value,
                                      nom_flag_1,
                                      nom_flag_2,
                                      nom_flag_3,
                                      nom_flag_4,
                                      nom_flag_5
                                 from item_exp_detail
                                where item = I_item
                                  and supplier = I_supplier;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_IED_TEMP;
----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_EXP_ROWS(O_error_message     IN OUT RTK_ERRORS.RTK_KEY%TYPE,
                      O_elc_changes       IN OUT OBJ_ELC_COST_EVENT_TBL,
                      I_item              IN     ITEM_EXP_DETAIL.ITEM%TYPE,
                      I_supplier          IN     ITEM_EXP_DETAIL.SUPPLIER%TYPE)
                      
   RETURN BOOLEAN IS

   L_program            VARCHAR2(62) := 'GET_EXP_ROWS';
   L_ied_exist          VARCHAR2(1)  := NULL;
   L_ieh_exist          VARCHAR2(1)  := NULL;
   L_gied_exist         VARCHAR2(1)  := NULL;
   L_ieh_exist_z        VARCHAR2(1)  := NULL;
   
   TYPE TBL_ITEM               IS TABLE OF ITEM_EXP_DETAIL.ITEM%TYPE INDEX BY BINARY_INTEGER;
   TYPE TBL_SUPPLIER           IS TABLE OF ITEM_EXP_DETAIL.SUPPLIER%TYPE INDEX BY BINARY_INTEGER;
   TYPE TBL_ORIGIN_COUNTRY_ID  IS TABLE OF ITEM_EXP_HEAD.ORIGIN_COUNTRY_ID%TYPE INDEX BY BINARY_INTEGER;
   TYPE TBL_ZONE_ID            IS TABLE OF ITEM_EXP_HEAD.ZONE_ID%TYPE INDEX BY BINARY_INTEGER;
   TYPE TBL_ZONE_GROUP_ID      IS TABLE OF ITEM_EXP_HEAD.ZONE_GROUP_ID%TYPE INDEX BY BINARY_INTEGER;

   L_item_tbl                TBL_ITEM;             
   L_supplier_tbl            TBL_SUPPLIER;         
   L_origin_country_id_tbl   TBL_ORIGIN_COUNTRY_ID;
   L_zone_id_tbl             TBL_ZONE_ID;          
   L_zone_group_id_tbl       TBL_ZONE_GROUP_ID;

   cursor C_IED_EXIST is
      select 'x'
        from item_exp_detail id
       where item = I_item
         and supplier = I_supplier
         and rownum = 1;

   cursor C_GIED_EXIST is
      select 'x'
        from gtt_item_exp_detail gi,
             item_exp_head ih
       where gi.item = I_item
         and gi.supplier = I_supplier
         and gi.item = ih.item
         and gi.supplier = ih.supplier
         and rownum = 1;
  
   cursor C_IEH_EXIST is
      select 'x'
        from item_exp_head ih
       where item = I_item
         and supplier = I_supplier
         and rownum = 1;

   cursor C_IEH_EXIST_Z is
      select 'x'
        from gtt_item_exp_detail gtt
       where gtt.item = I_item
         and gtt.supplier = I_supplier
         and gtt.item_exp_type = 'Z'
         and gtt.item_exp_seq not in 
             (select ih.item_exp_seq
                from item_exp_head ih
               where ih.item = I_item
                 and ih.supplier = I_supplier
                 and item_exp_type = 'Z')
         and rownum = 1;
     
BEGIN

   open C_IED_EXIST;
   fetch C_IED_EXIST into L_ied_exist;
   close C_IED_EXIST;

   open C_GIED_EXIST;
   fetch C_GIED_EXIST into L_gied_exist;
   close C_GIED_EXIST;

   open C_IEH_EXIST;
   fetch C_IEH_EXIST into L_ieh_exist;
   close C_IEH_EXIST;

   open C_IEH_EXIST_Z;
   fetch C_IEH_EXIST_Z into L_ieh_exist_z;
   close C_IEH_EXIST_Z;   
   
   ---      
   if L_ieh_exist is null or L_ieh_exist_z is not null then
       select OBJ_ELC_COST_EVENT_REC(c.item,
                                     c.supplier,
                                     c.origin_country_id,
                                     c.cost_zone_group_id,
                                     c.zone_id) 
                   bulk collect into O_elc_changes
                       from (select isc.item,
                                    isc.supplier, 
                                    isc.origin_country_id,
                                    NULL cost_zone_group_id,
                                    NULL zone_id 
                               from item_supp_country isc
                              where isc.item = I_item
                                and isc.supplier = I_supplier) c;
    
   else      
      if L_gied_exist is null and L_ied_exist is null then
         return TRUE;
      end if;
      ---
      if L_gied_exist is null and L_ied_exist is not null then

         select OBJ_ELC_COST_EVENT_REC(c.item,
                                       c.supplier,
                                       c.origin_country_id,
                                       c.zone_group_id,
                                       c.zone_id)
                     bulk collect into O_elc_changes 
                                  from (select distinct ieh.item,
                                               ieh.supplier,
                                               CASE 
                                                  WHEN ieh.item_exp_type = 'C' then ieh.origin_country_id
                                                  ELSE NULL
                                               END origin_country_id,
                                               CASE 
                                                  WHEN ieh.item_exp_type = 'Z' then ieh.zone_group_id
                                                  ELSE NULL
                                               END zone_group_id,
                                               CASE 
                                                  WHEN ieh.item_exp_type = 'Z' then ieh.zone_id
                                                  ELSE NULL
                                               END zone_id
                                          from item_exp_detail ied,
                                               item_exp_head ieh
                                         where ied.item = I_item
                                           and ied.supplier = I_supplier
                                           and ied.item = ieh.item
                                           and ied.supplier = ieh.supplier
                                           and ied.item_exp_type = ieh.item_exp_type
                                           and ied.item_exp_seq = ieh.item_exp_seq) c;

      elsif L_gied_exist is not null and L_ied_exist is null then
      
         select OBJ_ELC_COST_EVENT_REC(c.item,
                                       c.supplier,
                                       c.origin_country_id,
                                       c.zone_group_id,
                                       c.zone_id)
                     bulk collect into O_elc_changes                                                                     
                                  from (select distinct ieh.item,
                                               ieh.supplier,
                                               CASE 
                                                  WHEN ieh.item_exp_type = 'C' then ieh.origin_country_id
                                                  ELSE NULL
                                               END origin_country_id,
                                               CASE 
                                                  WHEN ieh.item_exp_type = 'Z' then ieh.zone_group_id
                                                  ELSE NULL
                                               END zone_group_id,
                                               CASE 
                                                  WHEN ieh.item_exp_type = 'Z' then ieh.zone_id
                                                  ELSE NULL
                                               END zone_id
                                          from gtt_item_exp_detail gied,
                                               item_exp_head ieh
                                         where gied.item = I_item
                                           and gied.supplier = I_supplier
                                           and gied.item = ieh.item
                                           and gied.supplier = ieh.supplier
                                           and gied.item_exp_type = ieh.item_exp_type
                                           and gied.item_exp_seq = ieh.item_exp_seq) c;
            
      else
      
         select OBJ_ELC_COST_EVENT_REC(c.item,
                                       c.supplier,
                                       c.origin_country_id,
                                       c.zone_group_id,
                                       c.zone_id)
                     bulk collect into O_elc_changes       
                                  from (select distinct ieh.item,
                                                        ieh.supplier,
                                                        CASE 
                                                           WHEN ieh.item_exp_type = 'C' then ieh.origin_country_id
                                                           ELSE NULL
                                                        END origin_country_id,
                                                        CASE 
                                                           WHEN ieh.item_exp_type = 'Z' then ieh.zone_group_id
                                                           ELSE NULL
                                                        END zone_group_id,
                                                        CASE 
                                                           WHEN ieh.item_exp_type = 'Z' then ieh.zone_id
                                                           ELSE NULL
                                                        END zone_id
                                                   from item_exp_detail ied,
                                                        gtt_item_exp_detail gied,
                                                        item_exp_head ieh
                                                  where ied.item = I_item
                                                    and ied.supplier = I_supplier
                                                    and ied.item = ieh.item
                                                    and ied.supplier = ieh.supplier
                                                    and ied.item = gied.item
                                                    and ied.supplier = gied.supplier
                                                    and ((ied.item_exp_type = gied.item_exp_type and
                                                          ied.item_exp_seq = gied.item_exp_seq and
                                                          (((ied.comp_id = gied.comp_id and
                                                             ied.comp_id is not null and
                                                             gied.comp_id is not null) and
                                                            (NVL(ied.cvb_code, 'x')        !=   NVL(gied.cvb_code, 'x')        or
                                                             NVL(ied.comp_rate, -1)       !=   NVL(gied.comp_rate, -1)         or
                                                             NVL(ied.per_count, -1)       !=   NVL(gied.per_count, -1)         or
                                                             NVL(ied.per_count_uom, 'x')   !=   NVL(gied.per_count_uom, 'x')   or
                                                             NVL(ied.comp_currency, 'x')   !=   NVL(gied.comp_currency, 'x')   or
                                                             NVL(ied.est_exp_value, -1)   !=   NVL(gied.est_exp_value, -1)     or
                                                             NVL(ied.nom_flag_1, 'x')      !=   NVL(gied.nom_flag_1, 'x')      or
                                                             NVL(ied.nom_flag_2, 'x')      !=   NVL(gied.nom_flag_2, 'x')      or
                                                             NVL(ied.nom_flag_3, 'x')      !=   NVL(gied.nom_flag_3, 'x')      or
                                                             NVL(ied.nom_flag_4, 'x')      !=   NVL(gied.nom_flag_4, 'x')      or
                                                             NVL(ied.nom_flag_5, 'x')      !=   NVL(gied.nom_flag_5, 'x'))) or
                                                            (not exists (select 'x'
                                                                           from gtt_item_exp_detail gtt
                                                                          where ied.item = gtt.item
                                                                            and ied.supplier = gtt.supplier
                                                                            and ied.item_exp_type = gtt.item_exp_type
                                                                            and ied.item_exp_seq = gtt.item_exp_seq
                                                                            and ied.comp_id = gtt.comp_id)) or
                                                            (not exists (select 'x'
                                                                           from item_exp_detail ie
                                                                          where ie.item = gied.item
                                                                            and ie.supplier = gied.supplier
                                                                            and ie.item_exp_type = gied.item_exp_type
                                                                            and ie.item_exp_seq = gied.item_exp_seq
                                                                            and ie.comp_id = gied.comp_id)))) or
                                                         (not exists (select 'x'
                                                                        from gtt_item_exp_detail gtt1
                                                                       where ied.item = gtt1.item
                                                                         and ied.supplier = gtt1.supplier
                                                                         and ied.item_exp_type = gtt1.item_exp_type
                                                                         and ied.item_exp_seq = gtt1.item_exp_seq)) or 
                                                         (not exists (select 'x'
                                                                        from item_exp_detail ied1
                                                                       where ied1.item = gied.item
                                                                         and ied1.supplier = gied.supplier
                                                                         and ied1.item_exp_type = gied.item_exp_type
                                    and ied1.item_exp_seq = gied.item_exp_seq)))) c;
      
      end if;
   end if;   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_EXP_ROWS;
----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_HTS_ROWS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_elc_changes         IN OUT   OBJ_ELC_COST_EVENT_TBL,
                      I_item                IN       ITEM_HTS_ASSESS.ITEM%TYPE)

   RETURN BOOLEAN IS

   L_program        VARCHAR2(62)   := 'COST_EVENT_LOG_SQL.GET_HTS_ROWS';
   L_iha_exist      VARCHAR2(1)    := NULL;
   L_giha_exist     VARCHAR2(1)    := NULL;
   L_ih_exist       VARCHAR2(1)    := NULL;
   L_ih_not_exist   VARCHAR2(1)    := NULL;

   cursor C_IHA_EXIST is
      select 'x'
        from item_hts_assess
       where item = I_item
         and rownum = 1;

   cursor C_GIHA_EXIST is
      select 'x'
        from gtt_item_hts_assess giha,
             item_hts ih
       where ih.item = giha.item
         and ih.hts = giha.hts
         and ih.import_country_id = giha.import_country_id
         and ih.origin_country_id = giha.origin_country_id
         and giha.item = I_item
         and rownum = 1;

   cursor C_IH_EXIST is
      select 'x'
        from item_hts ih
       where ih.item = I_item
         and rownum = 1;

   cursor C_IH_NOT_EXIST is
      select 'x'
        from gtt_item_hts_assess gtt
       where gtt.item = I_item
         and not exists (select 'x'
                           from item_hts ih
                          where ih.item = gtt.item
                            and ih.hts = gtt.hts
                            and ih.origin_country_id = gtt.origin_country_id
                            and ih.import_country_id = gtt.import_country_id)
         and rownum = 1;


BEGIN

   open C_IHA_EXIST;
   fetch C_IHA_EXIST into L_iha_exist;
   close C_IHA_EXIST;
   ---
   open C_GIHA_EXIST;
   fetch C_GIHA_EXIST into L_giha_exist;
   close C_GIHA_EXIST;
   ---
   open C_IH_EXIST;
   fetch C_IH_EXIST into L_ih_exist;
   close C_IH_EXIST;
   ---
   open C_IH_NOT_EXIST;
   fetch C_IH_NOT_EXIST into L_ih_not_exist;
   close C_IH_NOT_EXIST;
   ---

   if L_ih_exist is NULL or L_ih_not_exist is not NULL then
       select OBJ_ELC_COST_EVENT_REC(c.item,
                                     c.supplier,
                                     c.origin_country_id,
                                     c.cost_zone_group_id,
                                     c.zone_id)
                   bulk collect into O_elc_changes
                                from (select isc.item,
                                             isc.supplier,
                                             isc.origin_country_id,
                                             NULL cost_zone_group_id,
                                             NULL zone_id
                                        from item_supp_country isc
                                       where isc.item = I_item) c;
   else
      if L_iha_exist is NULL and L_giha_exist is NULL then
         return TRUE;
      end if;
      ---
      if L_giha_exist is NULL and L_iha_exist is not NULL then
         select OBJ_ELC_COST_EVENT_REC(c.item,
                                       c.supplier,
                                       c.origin_country_id,
                                       c.cost_zone_group_id,
                                       c.zone_id)
                     bulk collect into O_elc_changes
                                  from (select distinct ih.item,
                                               its.supplier,
                                               ih.origin_country_id,
                                               NULL cost_zone_group_id,
                                               NULL zone_id
                                          from item_hts_assess iha,
                                               item_hts ih,
                                               item_supplier its
                                         where iha.item = I_item
                                           and iha.item = ih.item
                                           and iha.item = its.item
                                           and iha.hts = ih.hts
                                           and iha.import_country_id = ih.import_country_id
                                           and iha.origin_country_id = ih.origin_country_id) c;
      elsif L_giha_exist is not NULL and L_iha_exist is NULL then
         select OBJ_ELC_COST_EVENT_REC(c.item,
                                       c.supplier,
                                       c.origin_country_id,
                                       c.cost_zone_group_id,
                                       c.zone_id)
                     bulk collect into O_elc_changes
                                  from (select distinct ih.item,
                                               its.supplier,
                                               ih.origin_country_id,
                                               NULL cost_zone_group_id,
                                               NULL zone_id
                                          from gtt_item_hts_assess giha,
                                               item_hts ih,
                                               item_supplier its
                                         where giha.item = I_item
                                           and giha.item = ih.item
                                           and giha.item = its.item
                                           and giha.hts = ih.hts
                                           and giha.import_country_id = ih.import_country_id
                                           and giha.origin_country_id = ih.origin_country_id) c;
      else
         select OBJ_ELC_COST_EVENT_REC(c.item,
                                       c.supplier,
                                       c.origin_country_id,
                                       c.cost_zone_group_id,
                                       c.zone_id)
                     bulk collect into O_elc_changes
                                  from (select distinct ih.item,
                                               its.supplier,
                                               ih.origin_country_id,
                                               NULL cost_zone_group_id,
                                               NULL zone_id
                                          from gtt_item_hts_assess giha,
                                               item_hts_assess iha,
                                               item_hts ih,
                                               item_supplier its
                                         where giha.item = I_item
                                           and giha.item = ih.item
                                           and giha.item = its.item
                                           and giha.hts = ih.hts
                                           and giha.import_country_id = ih.import_country_id
                                           and giha.origin_country_id = ih.origin_country_id
                                           and giha.item = iha.item
                                           and (   ((    iha.comp_id = giha.comp_id
                                                     and giha.hts = iha.hts
                                                     and giha.import_country_id = iha.import_country_id
                                                     and giha.origin_country_id = iha.origin_country_id
                                                     and iha.comp_id is not NULL
                                                     and giha.comp_id is not NULL)
                                                     and (   NVL(iha.cvb_code, 'x')        !=   NVL(giha.cvb_code, 'x')
                                                          or NVL(iha.comp_rate, -1)        !=   NVL(giha.comp_rate, -1)
                                                          or NVL(iha.per_count, -1)        !=   NVL(giha.per_count, -1)
                                                          or NVL(iha.per_count_uom, 'x')   !=   NVL(giha.per_count_uom, 'x')
                                                          or NVL(iha.est_assess_value, -1) !=   NVL(giha.est_assess_value, -1)
                                                          or NVL(iha.nom_flag_1, 'x')      !=   NVL(giha.nom_flag_1, 'x')
                                                          or NVL(iha.nom_flag_2, 'x')      !=   NVL(giha.nom_flag_2, 'x')
                                                          or NVL(iha.nom_flag_3, 'x')      !=   NVL(giha.nom_flag_3, 'x')
                                                          or NVL(iha.nom_flag_4, 'x')      !=   NVL(giha.nom_flag_4, 'x')
                                                          or NVL(iha.nom_flag_5, 'x')      !=   NVL(giha.nom_flag_5, 'x')))
                                                or (not exists (select 'x'
                                                                 from item_hts_assess iha1
                                                                where iha1.item = giha.item
                                                                  and iha1.hts = giha.hts
                                                                  and iha1.import_country_id = giha.import_country_id
                                                                  and iha1.origin_country_id = giha.origin_country_id
                                                                  and iha1.comp_id = giha.comp_id))
                                                or (not exists (select 'x'
                                                                  from gtt_item_hts_assess gtt
                                                                 where gtt.item = iha.item
                                                                   and gtt.hts = iha.hts
                                                                   and gtt.import_country_id = iha.import_country_id
                                                                   and gtt.origin_country_id = iha.origin_country_id
                                                                   and gtt.comp_id = iha.comp_id)))) c;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_HTS_ROWS;
----------------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_IHA_TEMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item            IN       ITEM_HTS_ASSESS.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(62) := 'COST_EVENT_LOG_SQL.INSERT_IHA_TEMP';
   L_exist              VARCHAR2(1)  := NULL;

   cursor C_IHA_EXIST is
      select 'x'
        from item_hts_assess
       where item = I_item
         and rownum = 1;

BEGIN

   open C_IHA_EXIST;
   fetch C_IHA_EXIST into L_exist;
   ---
   if C_IHA_EXIST%NOTFOUND then
      close C_IHA_EXIST;
      return TRUE;
   end if;
   ---
   close C_IHA_EXIST;

   if L_exist is not NULL then
      ---
      insert into gtt_item_hts_assess(item,
                                      hts,
                                      import_country_id,
                                      origin_country_id,
                                      effect_from_date,
                                      effect_to_date,
                                      comp_id,
                                      cvb_code,
                                      comp_rate,
                                      per_count,
                                      per_count_uom,
                                      est_assess_value,
                                      nom_flag_1,
                                      nom_flag_2,
                                      nom_flag_3,
                                      nom_flag_4,
                                      nom_flag_5,
                                      display_order,
                                      create_datetime,
                                      last_update_datetime,
                                      last_update_id)
                               select item,
                                      hts,
                                      import_country_id,
                                      origin_country_id,
                                      effect_from,
                                      effect_to,
                                      comp_id,
                                      cvb_code,
                                      comp_rate,
                                      per_count,
                                      per_count_uom,
                                      est_assess_value,
                                      nom_flag_1,
                                      nom_flag_2,
                                      nom_flag_3,
                                      nom_flag_4,
                                      nom_flag_5,
                                      display_order,
                                      create_datetime,
                                      last_update_datetime,
                                      last_update_id
                                 from item_hts_assess
                                where item = I_item;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_IHA_TEMP;
----------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_WRP (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_process_tbl     IN       WRP_COST_EVENT_DET_TBL)
   RETURN INTEGER IS
   L_program      VARCHAR2(60) := 'COST_EVENT_LOG_SQL.PROCESS_WRP';
   L_process_tbl  COST_EVENT_LOG_SQL.COST_EVENT_DET_TBL;
BEGIN
   ---
   if I_process_tbl.COUNT > 0 and I_process_tbl is NOT NULL then
      FOR i IN 1..I_process_tbl.COUNT LOOP
         L_process_tbl(i).cost_event_process_id := I_process_tbl(i).cost_event_process_id;
         L_process_tbl(i).thread_id  := I_process_tbl(i).thread_id;
      END LOOP;
      
      if COST_EVENT_LOG_SQL.PROCESS(O_error_message,
                                    L_process_tbl) = FALSE then
         return 0;
      end if;
   end if;
   return 1;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return 0;
END PROCESS_WRP;
-----------------------------------------------------------------------------------------------------------------------------
END;
/