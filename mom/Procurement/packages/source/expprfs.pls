CREATE OR REPLACE PACKAGE EXP_PROF_SQL AUTHID CURRENT_USER AS

TYPE EXP_PROF_DETAIL_TBL IS TABLE OF EXP_PROF_DETAIL%ROWTYPE;

-------------------------------------------------------------------------------
--Function Name:  PROF_HEAD_EXISTS
--Purpose      :  Validate the given expense profile header exists.
-------------------------------------------------------------------------------
FUNCTION PROF_HEAD_EXIST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists            IN OUT BOOLEAN,
                         I_exp_prof_type     IN     EXP_PROF_HEAD.EXP_PROF_TYPE%TYPE,
                         I_module            IN     EXP_PROF_HEAD.MODULE%TYPE,
                         I_key_value_1       IN     EXP_PROF_HEAD.KEY_VALUE_1%TYPE,
                         I_key_value_2       IN     EXP_PROF_HEAD.KEY_VALUE_2%TYPE,
                         I_zone_group_id     IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                         I_zone_id           IN     COST_ZONE.ZONE_ID%TYPE,
                         I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                         I_lading_port       IN     OUTLOC.OUTLOC_ID%TYPE,
                         I_discharge_port    IN     OUTLOC.OUTLOC_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  PROF_PTNR_HEAD_EXIST
--Purpose      :  Validate that a partner profile exists for the given supplier.
-------------------------------------------------------------------------------
FUNCTION PROF_PTNR_HEAD_EXIST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists            IN OUT BOOLEAN,
                              I_supplier          IN     SUP_IMPORT_ATTR.SUPPLIER%TYPE)
RETURN BOOLEAN;                              
-------------------------------------------------------------------------------
--Function Name:  PROF_DETAILS_EXIST
--Purpose      :  Validate the given expense component exists for the 
--                profile key and component id passed in.
-------------------------------------------------------------------------------
FUNCTION PROF_DETAIL_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists        IN OUT BOOLEAN,
                            I_exp_prof_key  IN     EXP_PROF_HEAD.EXP_PROF_KEY%TYPE,
                            I_comp_id       IN     ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN; 

-------------------------------------------------------------------------------
--Function Name:  GET_NEXT_PROF
--Purpose      :  Retrieve the next expense profile key. 
-------------------------------------------------------------------------------
FUNCTION GET_NEXT_PROF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exp_prof_key  IN OUT EXP_PROF_HEAD.EXP_PROF_KEY%TYPE)RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  BASE_PROF_CHANGED
--Purpose      :  Sets base profile indicators for given profile type, module, 
--                key value 1 and key value 2 to 'N' then checks if base 
--                profile indicator of passed-in record is 'Y' and updates 
--                the record accordingly.
-------------------------------------------------------------------------------
FUNCTION BASE_PROF_CHANGED(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_exp_prof_key      IN     EXP_PROF_HEAD.EXP_PROF_KEY%TYPE,
                           I_exp_prof_type     IN     EXP_PROF_HEAD.EXP_PROF_TYPE%TYPE,
                           I_module            IN     EXP_PROF_HEAD.MODULE%TYPE,
                           I_key_value_1       IN     EXP_PROF_HEAD.KEY_VALUE_1%TYPE,
                           I_key_value_2       IN     EXP_PROF_HEAD.KEY_VALUE_2%TYPE,
                           I_base_prof_ind     IN     EXP_PROF_HEAD.BASE_PROF_IND%TYPE)
RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  DEL_PROF_HEAD
--Purpose      :  Delete expense profile header records that have no details
-------------------------------------------------------------------------------
FUNCTION DEL_PROF_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_exp_prof_type IN     EXP_PROF_HEAD.EXP_PROF_TYPE%TYPE,
                       I_module        IN     EXP_PROF_HEAD.MODULE%TYPE,
                       I_key_value_1   IN     EXP_PROF_HEAD.KEY_VALUE_1%TYPE,
                       I_key_value_2   IN     EXP_PROF_HEAD.KEY_VALUE_2%TYPE)
RETURN BOOLEAN; 
-------------------------------------------------------------------------------
-- Function Name: LOCK_PROF_DETAILS
-- Purpose      : This function locks expense prof detail records. 
-------------------------------------------------------------------------------
FUNCTION LOCK_PROF_DETAILS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_exp_prof_key     IN      EXP_PROF_HEAD.EXP_PROF_KEY%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
--Function Name:  DEL_PROF_DETAILS
--Purpose      :  Delete expense profile detail records.
-------------------------------------------------------------------------------
FUNCTION DEL_PROF_DETAILS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_exp_prof_key  IN     EXP_PROF_HEAD.EXP_PROF_KEY%TYPE)
RETURN BOOLEAN; 
----------------------------------------------------------------------------------
-- Function Name: CHECK_HEADER_NO_DETAILS
-- Purpose      : Checks for Expense Profile Header records that do not have any 
--                associated Expense Profile Detail records.  Sets O_exists to 
--                TRUE if records found with no detail records.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_HEADER_NO_DETAILS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists            IN OUT BOOLEAN,
                                 I_exp_prof_type     IN     EXP_PROF_HEAD.EXP_PROF_TYPE%TYPE,
                                 I_module            IN     EXP_PROF_HEAD.MODULE%TYPE,
                                 I_key_value_1       IN     EXP_PROF_HEAD.KEY_VALUE_1%TYPE,
                                 I_key_value_2       IN     EXP_PROF_HEAD.KEY_VALUE_2%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name:  BASE_PROF_EXISTS
--Purpose      :  Determine if a base profile exists for a particular module, key_value_1,
--                key_value_2, and type (Country or Zone).
------------------------------------------------------------------------------------------
FUNCTION BASE_PROF_EXIST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists            IN OUT BOOLEAN,
                         I_exp_prof_type     IN     EXP_PROF_HEAD.EXP_PROF_TYPE%TYPE,
                         I_module            IN     EXP_PROF_HEAD.MODULE%TYPE,
                         I_key_value_1       IN     EXP_PROF_HEAD.KEY_VALUE_1%TYPE,
                         I_key_value_2       IN     EXP_PROF_HEAD.KEY_VALUE_2%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
--Function Name:  GET_EXP_PROF_DETAIL
--Purpose      :  Retrieve Expense Profile Detail records from EXP_PROF_DETAIL for the 
--                specified parameters that correspond to record(s) in the EXP_PROF_HEAD
--                table. If no input parameters are specified, all details are returned.
------------------------------------------------------------------------------------------
FUNCTION GET_EXP_PROF_DETAIL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exp_prof_detail_tbl   IN OUT   NOCOPY EXP_PROF_SQL.EXP_PROF_DETAIL_TBL,
                             I_exp_prof_type         IN       EXP_PROF_HEAD.EXP_PROF_TYPE%TYPE DEFAULT NULL,
                             I_module                IN       EXP_PROF_HEAD.MODULE%TYPE DEFAULT NULL,
                             I_key_value_1           IN       EXP_PROF_HEAD.KEY_VALUE_1%TYPE DEFAULT NULL,
                             I_key_value_2           IN       EXP_PROF_HEAD.KEY_VALUE_2%TYPE DEFAULT NULL,
                             I_zone_group_id         IN       EXP_PROF_HEAD.ZONE_GROUP_ID%TYPE DEFAULT NULL,
                             I_zone_id               IN       EXP_PROF_HEAD.ZONE_ID%TYPE DEFAULT NULL,
                             I_origin_country_id     IN       EXP_PROF_HEAD.ORIGIN_COUNTRY_ID%TYPE DEFAULT NULL,
                             I_lading_port           IN       EXP_PROF_HEAD.LADING_PORT%TYPE DEFAULT NULL,
                             I_discharge_port        IN       EXP_PROF_HEAD.DISCHARGE_PORT%TYPE DEFAULT NULL,
                             I_base_prof_ind         IN       EXP_PROF_HEAD.DISCHARGE_PORT%TYPE DEFAULT NULL)
RETURN BOOLEAN;

END EXP_PROF_SQL;
/


