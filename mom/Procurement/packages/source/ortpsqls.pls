
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORDER_TYPE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--        Name: GET_ORDER_TYPE_DESC
--     Purpose: Accepts an order type as the input and fetches the associated
--              order type description from the order_types table.
--  Created By: Kevin Hearnen 13-Jan-2000
-- Modified By: Kevin Hearnen 13-Jan-2000
-------------------------------------------------------------------------------
FUNCTION GET_ORDER_TYPE_DESC(O_error_message    IN OUT VARCHAR2,
                             O_order_type_desc  IN OUT ORDER_TYPES_TL.ORDER_TYPE_DESC%TYPE,
                             I_order_type       IN     ORDER_TYPES.ORDER_TYPE%TYPE)
                             RETURN BOOLEAN;
-------------------------------------------------------------------------------
END ORDER_TYPE_SQL;
/
