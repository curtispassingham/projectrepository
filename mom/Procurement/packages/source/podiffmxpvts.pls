SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE PO_DIFF_MATRIX_PIVOT_SQL  AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--- FUNCTION: POPULATE_PIVOT
--- PURPOSE: This function populates PO_DIFF_MATRIX_PIVOT_TEMP table by selecting records from PO_DIFF_MATRIX_TEMP table.
-----------------------------------------------------------------------------
FUNCTION POPULATE_PIVOT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- FUNCTION: APPLY_PIVOT
--- PURPOSE: This function applies the latest data in to PO_DIFF_MATRIX_TEMP from PO_DIFF_MATRIX _PIVOT_TEMP
-----------------------------------------------------------------------------
FUNCTION APPLY_PIVOT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- FUNCTION: DELETE_PIVOT
--- PURPOSE: This function deletes data from PO_DIFF_MATRIX _PIVOT_TEMP table before exit from screen.
-----------------------------------------------------------------------------
FUNCTION DELETE_PIVOT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

END PO_DIFF_MATRIX_PIVOT_SQL; 
/  