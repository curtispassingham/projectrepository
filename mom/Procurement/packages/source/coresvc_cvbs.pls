CREATE OR REPLACE PACKAGE CORESVC_CVB AUTHID CURRENT_USER AS

   template_key                     CONSTANT VARCHAR2(255):= 'CVB_DATA';
   template_category                CONSTANT VARCHAR2(255):= 'RMSIMP';
   action_new                       VARCHAR2(25)          := 'NEW';
   action_mod                       VARCHAR2(25)          := 'MOD';
   action_del                       VARCHAR2(25)          := 'DEL';
   CVB_HEAD_sheet                   VARCHAR2(255)         := 'CVB_HEAD';
   CVB_HEAD$Action                  NUMBER                := 1;
   CVB_HEAD$CVB_CODE                NUMBER                := 2;
   CVB_HEAD$CVB_DESC                NUMBER                := 3;
   CVB_HEAD$NOM_FLAG_1              NUMBER                := 4;
   CVB_HEAD$NOM_FLAG_2              NUMBER                := 5;
   CVB_HEAD$NOM_FLAG_3              NUMBER                := 6;
   CVB_HEAD$NOM_FLAG_4              NUMBER                := 7;
   CVB_HEAD$NOM_FLAG_5              NUMBER                := 8;
   CVB_DETAIL_sheet                 VARCHAR2(255)         := 'CVB_DETAIL';
   CVB_DETAIL$ACTION                NUMBER                := 1;
   CVB_DETAIL$CVB_CODE              NUMBER                := 2;
   CVB_DETAIL$COMP_ID               NUMBER                := 3;
   CVB_DETAIL$COMBO_OPER            NUMBER                := 4;
   CVB_HEAD_TL_sheet                VARCHAR2(255)         := 'CVB_HEAD_TL';
   CVB_HEAD_TL$Action               NUMBER                := 1;
   CVB_HEAD_TL$LANG                 NUMBER                := 2;
   CVB_HEAD_TL$CVB_CODE             NUMBER                := 3;
   CVB_HEAD_TL$CVB_DESC             NUMBER                := 4;
--------------------------------------------------------------------------------
   TYPE CVB_HEAD_rec_tab   is TABLE OF CVB_HEAD%ROWTYPE;
   TYPE CVB_DETAIL_rec_tab is TABLE OF CVB_DETAIL%ROWTYPE;
--------------------------------------------------------------------------------
   sheet_name_trans S9T_PKG.TRANS_MAP_TYP;
   action_column    VARCHAR2(255) := 'ACTION';
--------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN     CHAR DEFAULT 'N')
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_count   IN OUT NUMBER,
                        I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id    IN     NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------
END CORESVC_CVB;
/