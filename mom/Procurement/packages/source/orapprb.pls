CREATE OR REPLACE PACKAGE BODY ORDER_APPROVE_SQL AS
-------------------------------------------------------------------------------
FUNCTION PACK_SKU(I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'ORDER_APPROVE_SQL.PACK_SKU';
   L_return_code         VARCHAR2(5);
   L_dummy               VARCHAR2(1);
   L_exchange_rate       ORDHEAD.EXCHANGE_RATE%TYPE;
   L_order_curr_code     ORDHEAD.CURRENCY_CODE%TYPE;
   L_import_order_ind    ORDHEAD.IMPORT_ORDER_IND%TYPE;
   L_import_country_id   ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   L_supplier_curr_code  CURRENCIES.CURRENCY_CODE%TYPE;
   L_elc_ind             SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_import_ind          SYSTEM_OPTIONS.IMPORT_IND%TYPE;
   L_chapter             HTS_CHAPTER.CHAPTER%TYPE;
   L_item                ITEM_MASTER.ITEM%TYPE;
   L_pack_sku_qty        V_PACKSKU_QTY.QTY%TYPE;
   L_pack_no             ITEM_MASTER.ITEM%TYPE;
   L_origin_country_id   COUNTRY.COUNTRY_ID%TYPE;
   L_unit_cost           ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_ord_cost            ORDLOC.UNIT_COST%TYPE;
   L_pack_amt            ORDLOC.UNIT_COST%TYPE;
   L_supp_pack_size      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_unit_retail         ORDLOC.UNIT_RETAIL%TYPE;
   L_selling_unit_retail ORDLOC.UNIT_RETAIL%TYPE;
   L_selling_uom         UOM_CLASS.UOM%TYPE;
   L_location            ORDLOC.LOCATION%TYPE;
   L_loc_type            ORDLOC.LOC_TYPE%TYPE;
   L_qty_ordered         ORDLOC.QTY_ORDERED%TYPE;
   L_qty_prescaled       ORDLOC.QTY_PRESCALED%TYPE;
   L_qty_received        ORDLOC.QTY_RECEIVED%TYPE;
   L_last_received       ORDLOC.LAST_RECEIVED%TYPE;
   L_store               STORE.STORE%TYPE;
   L_wh                  WH.WH%TYPE;
   L_av_cost             ITEM_LOC_SOH.AV_COST%TYPE;
   L_unit_cost_loc       ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_selling_retail      ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_supplier            ORDHEAD.SUPPLIER%TYPE;
   L_latest_date         ORDSKU.LATEST_SHIP_DATE%TYPE;
   L_earliest_date       ORDSKU.EARLIEST_SHIP_DATE%TYPE;
   L_rowid               ROWID;
   L_zone_group_id       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   L_exists              BOOLEAN;
   L_ordsku_exists       BOOLEAN;   
   L_prev_pack_no        ITEM_MASTER.ITEM%TYPE := 0;
   L_loc_non_scale_ind   ORDLOC.NON_SCALE_IND%TYPE;
   L_item_non_scale_ind  ORDSKU.NON_SCALE_IND%TYPE;
	L_table               VARCHAR2(30);
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
   cursor C_PACK_SKUS is 
      select v.item,
             v.qty,
             v.pack_no,
             s.origin_country_id,
             s.earliest_ship_date,
             s.latest_ship_date,
             s.non_scale_ind
        from v_packsku_qty v,
             item_master i,
             ordsku s
       where s.item          = v.pack_no
         and s.order_no      = I_order_no
         and i.item          = v.pack_no
         and i.order_as_type = 'E'
    order by v.item;

   cursor C_ORDHEAD_INFO is
      select exchange_rate,
             currency_code,
             supplier,
             import_country_id,
             import_order_ind
        from ordhead
       where order_no = I_order_no;

   cursor C_ORDLOC is
      select location,
             loc_type,
             unit_cost,
             qty_ordered,
             qty_prescaled,
             non_scale_ind
        from ordloc
       where order_no = I_order_no
         and item     = L_pack_no;

   cursor C_LOCK_ORDLOC_UPDATE is
      select 'Y'
        from ordloc
       where order_no = I_order_no
         and item     = L_item
         and location = L_location
         for update nowait;

   cursor C_ITEM_SUPP_COUNTRY is
      select supp_pack_size
        from item_supp_country
       where item              = L_item
         and supplier          = L_supplier
         and origin_country_id = L_origin_country_id;
 
   cursor C_ITEM_SUPP_COUNTRY_LOC is
      select unit_cost
        from item_supp_country_loc
       where item              = L_item
         and supplier          = L_supplier
         and origin_country_id = L_origin_country_id
         and loc               = L_location;

   cursor C_ORDSKU is
      select 'x'
        from ordsku
       where order_no = I_order_no
         and item     = L_item;

   cursor C_SUM_PACKSKU_COST is
      select SUM(it.unit_cost * v.qty)
        from item_supp_country_loc it,
             v_packsku_qty v
       where it.supplier          = L_supplier
         and it.origin_country_id = L_origin_country_id
         and it.item              = v.item
         and v.pack_no            = L_pack_no
         and it.loc               = L_location;

   cursor C_LOCK_ORDLOC_EXP is
      select 'Y' 
        from ordloc_exp
       where order_no      = I_order_no
         and item          = L_item
         and (pack_item    = L_pack_no
              or pack_item is NULL)
         for update nowait;

   cursor C_LOCK_ORDSKU_HTS_ASSESS is
      select 'Y'
        from ordsku_hts_assess
       where order_no = I_order_no
         and seq_no in (select seq_no
                          from ordsku_hts
                         where order_no  = I_order_no
                           and item      = L_item
                           and pack_item = L_pack_no)
         for update nowait;

   cursor C_LOCK_ORDSKU_HTS is
      select 'Y'
        from ordsku_hts
       where order_no  = I_order_no
         and item      = L_item
         and pack_item = L_pack_no
         for update nowait;
 
   cursor C_HTS is
      select distinct h.chapter
        from ordsku_hts oh,
             hts h
       where oh.order_no         = I_order_no
         and oh.item             = L_item
         and oh.pack_item       is NULL
         and oh.hts              = h.hts
         and h.import_country_id = L_import_country_id;

   cursor C_LOCK_ORDLOC_CFA_EXT is
      select 'Y'
        from ordloc_cfa_ext
       where order_no = I_order_no
         and item in (select item
                        from item_master
                       where order_as_type = 'E')
         for update nowait;

   cursor C_LOCK_ORDLOC is
      select 'Y'
        from ordloc
       where order_no = I_order_no
         and item in (select item
                        from item_master
                       where order_as_type = 'E')
         for update nowait;

   cursor C_LOCK_REQ_DOC is
      select 'Y'
        from req_doc
       where module      = 'POIT'
         and key_value_1 = to_char(I_order_no)
         and key_value_2 in (select item
                               from item_master
                              where order_as_type = 'E')
         for update nowait;

   cursor C_LOCK_TIMELINE is
      select 'Y'
        from timeline
       where timeline_type = 'POIT'
         and key_value_1   = to_char(I_order_no)
         and key_value_2 in (select item
                               from item_master
                              where order_as_type = 'E')
         for update nowait;

   cursor C_LOCK_ORDSKU_CFA_EXT is 
      select 'Y'
        from ordsku_cfa_ext
       where order_no = I_order_no
         and item in (select item
                        from item_master
                       where order_as_type = 'E')
         for update nowait;

   cursor C_LOCK_ORDSKU is 
      select 'Y'
        from ordsku
       where order_no = I_order_no
         and item in (select item
                        from item_master
                       where order_as_type = 'E')
         for update nowait;
             
BEGIN
   if SYSTEM_OPTIONS_SQL.GET_IMPORT_ELC_IND(O_error_message,
                                            L_import_ind,
                                            L_elc_ind) = FALSE then
      return FALSE;
   end if;
   ---
   -- get order information
   ---
   open C_ORDHEAD_INFO;
   fetch C_ORDHEAD_INFO into L_exchange_rate,
                             L_order_curr_code,
                             L_supplier,
                             L_import_country_id,
                             L_import_order_ind;
   if C_ORDHEAD_INFO%NOTFOUND then
      close C_ORDHEAD_INFO;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   close C_ORDHEAD_INFO;
   ---
   -- get supplier information
   ---
   if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                L_supplier,
                                'V',
                                NULL,
                                L_supplier_curr_code) = FALSE then
      return FALSE;
   end if;
   ---
   FOR C_PACK_SKUS_REC in C_PACK_SKUS LOOP
      L_item               := C_PACK_SKUS_REC.item;
      L_pack_sku_qty       := C_PACK_SKUS_REC.qty;
      L_pack_no            := C_PACK_SKUS_REC.pack_no;
      L_origin_country_id  := C_PACK_SKUS_REC.origin_country_id;
      L_earliest_date      := C_PACK_SKUS_REC.earliest_ship_date;
      L_latest_date        := C_PACK_SKUS_REC.latest_ship_date;
      L_item_non_scale_ind := C_PACK_SKUS_REC.non_scale_ind;
      ---

      if L_import_country_id != L_origin_country_id then 
         L_import_order_ind := 'Y';
      end if;

      if L_elc_ind = 'Y' then
		   L_table := 'ordloc_exp';
         open C_LOCK_ORDLOC_EXP;
         close C_LOCK_ORDLOC_EXP;
         ---
         -- Only delete from ordloc_exp all expense records for the pack component item
         -- where the item exists on either ordloc_exp or ordloc as a stand alone item
         -- for the same location.  Otherwise simply set the pack_item to NULL
         -- and leave the expenses for the component item as they were. (see the
         -- update below)
         ---
         delete from ordloc_exp o
          where o.order_no  = I_order_no
            and o.item      = L_item
            and o.pack_item = L_pack_no
            and exists (select 'x'
                          from ordloc ol
                         where ol.order_no = I_order_no
                           and ol.item     = L_item
                           and ol.location = o.location);
         ---
         update ordloc_exp
            set pack_item = NULL
          where order_no  = I_order_no
            and item      = L_item
            and pack_item = L_pack_no;
      end if;    -- L_elc_ind = 'Y'
      ---
      open  C_ORDSKU;
      fetch C_ORDSKU into L_dummy;
      if C_ORDSKU%NOTFOUND then
         L_ordsku_exists := FALSE;
      else
         L_ordsku_exists := TRUE;
      end if;
      close C_ORDSKU;
      ---
      if L_ordsku_exists = FALSE then
         open  C_ITEM_SUPP_COUNTRY;
         fetch C_ITEM_SUPP_COUNTRY into L_supp_pack_size;
         close C_ITEM_SUPP_COUNTRY;
         ---
         insert into ordsku (order_no,
                             item,
                             ref_item,
                             origin_country_id,
                             earliest_ship_date,
                             latest_ship_date,
                             supp_pack_size,
                             non_scale_ind)                     
                      values (I_order_no,
                              L_item,
                              NULL,
                              L_origin_country_id,
                              L_earliest_date,
                              L_latest_date,
                              L_supp_pack_size,
                              L_item_non_scale_ind);
         ---
         if (L_elc_ind = 'Y' and L_import_ind = 'Y' and L_import_order_ind = 'Y') then
               L_table := 'ordsku_hts';
					open C_LOCK_ORDSKU_HTS;
               close C_LOCK_ORDSKU_HTS;
               update ordsku_hts
                  set pack_item = NULL
                where order_no  = I_order_no
                  and item      = L_item
                  and pack_item = L_pack_no;
         end if; 
      else -- L_ordsku_exists = TRUE 
         if (L_elc_ind = 'Y' and L_import_ind = 'Y' and L_import_order_ind = 'Y') then
            ---
            -- delete child records first
            ---
				L_table := 'ordsku_hts_assess';
            open C_LOCK_ORDSKU_HTS_ASSESS;
            close C_LOCK_ORDSKU_HTS_ASSESS;
            delete from ordsku_hts_assess
             where order_no = I_order_no
               and seq_no in (select seq_no
                                from ordsku_hts
                               where order_no  = I_order_no
                                 and item      = L_item
                                 and pack_item = L_pack_no);
            ---
				L_table := 'ordsku_hts';
            open C_LOCK_ORDSKU_HTS;
            close C_LOCK_ORDSKU_HTS;
            delete from ordsku_hts
             where order_no  = I_order_no
               and item      = L_item
               and pack_item = L_pack_no;

         end if;
      end if;  -- L_ordsku_exists
      ---     
      FOR C_ORDLOC_REC in C_ORDLOC LOOP
         L_location          := C_ORDLOC_REC.location;
         L_loc_type          := C_ORDLOC_REC.loc_type;
         L_qty_ordered       := C_ORDLOC_REC.qty_ordered;
         L_qty_prescaled     := C_ORDLOC_REC.qty_prescaled;
         L_loc_non_scale_ind := C_ORDLOC_REC.non_scale_ind;
         L_ord_cost          := C_ORDLOC_REC.unit_cost;
         --- 
         L_table := 'ordloc';			
         open C_LOCK_ORDLOC_UPDATE;
         close C_LOCK_ORDLOC_UPDATE;   
         update ordloc
            set qty_ordered   = qty_ordered   + (L_pack_sku_qty  * L_qty_ordered),
                qty_prescaled = qty_prescaled + (L_qty_prescaled * L_pack_sku_qty)
          where order_no = I_order_no
            and item     = L_item
            and location = L_location;
         ---
         if SQL%NOTFOUND THEN
            if L_loc_type = 'S' then 
               L_store   := L_location;
               L_wh      := -1;
            else
               L_store   := -1;
               L_wh      := L_location;
            end if;
            ---
            -- Find the pro-rated Order unit cost in order currency.
            ---
            if PACKITEM_ATTRIB_SQL.GET_ORDER_COMP_COST(O_error_message,
                                                       L_pack_no,
                                                       L_ord_cost,
                                                       L_item,
                                                       L_supplier,
                                                       L_origin_country_id,
                                                       L_location,
                                                       L_unit_cost) = FALSE then
               return FALSE;
            end if;
            ---
            if NOT ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                            L_item,
                                                            L_location,
                                                            L_loc_type,
                                                            L_av_cost,
                                                            L_unit_cost_loc,
                                                            L_unit_retail,
                                                            L_selling_retail,
                                                            L_selling_uom) then
               return FALSE;
            end if;
            ---
            insert into ordloc (order_no,
                                item,
                                location,
                                loc_type,
                                unit_retail,
                                qty_ordered,
                                qty_prescaled,
                                qty_received,
                                last_received,
                                qty_cancelled,
                                cancel_code,
                                cancel_date,
                                cancel_id,
                                original_repl_qty,
                                unit_cost,
                                unit_cost_init,
                                cost_source,
                                non_scale_ind) 
               values (I_order_no,
                       L_item,
                       L_location,
                       L_loc_type,
                       L_unit_retail,
                       (L_qty_ordered   * L_pack_sku_qty),
                       (L_qty_prescaled * L_pack_sku_qty),
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,        -- original_repl_qty
                       L_unit_cost, -- unit_cost
                       L_unit_cost, -- unit_cost_init
                       'NORM',
                       L_loc_non_scale_ind);

         end if;  -- end ordloc%notfound
      END LOOP;  --  end C_ORDLOC loop
      ---
      -- Rec'd Docs
      ---
      if L_prev_pack_no != L_pack_no then
         --- check if docs exist for the pack item on the 
         --- purchase order.
         if DOCUMENTS_SQL.REQ_DOCS_EXIST_MOD_KEY(O_error_message,
                                                 L_exists,
                                                 'POIT',
                                                 to_char(I_order_no),
                                                 L_pack_no,
                                                 NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- if docs do exist for the pack item on the purchase order
      -- and the component sku does not appear as a stand alone on ordsku,
      -- then default the docs to the component skus. 
      ---
      if L_exists = TRUE and L_ordsku_exists = FALSE then
         if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                       'POIT',
                                       'POIT',
                                       to_char(I_order_no),     
                                       to_char(I_order_no),
                                       L_pack_no,
                                       L_item) = FALSE then
            return FALSE;
         end if;
      elsif L_exists = FALSE and L_ordsku_exists = FALSE then
         ---
         -- if docs do not exist for the pack item on the purchase
         -- order and the component sku does not appear as a stand
         -- alone on ordsku,  default docs that were set up for the component
         -- skus in item maintenance.
         ---
         if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                       'IT',
                                       'POIT',
                                       L_item,
                                       to_char(I_order_no),
                                       NULL,
                                       L_item) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if L_ordsku_exists = FALSE then
         if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                       'CTRY',
                                       'POIT',
                                       L_origin_country_id,
                                       to_char(I_order_no),
                                       NULL,
                                       L_item) = FALSE then
            return FALSE;
         end if;
         ---
         if L_elc_ind = 'Y' and L_import_ind = 'Y' and L_import_order_ind = 'Y' then
            FOR C_HTS_REC in C_HTS LOOP
               L_chapter := C_HTS_REC.chapter;
               if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                             'HTSC',
                                             'POIT',
                                             L_chapter,
                                             to_char(I_order_no),
                                             L_import_country_id,
                                             L_item) = FALSE then
                  return FALSE;
               end if;
            END LOOP;
         end if; 
      end if;  -- L_ordsku_exists = TRUE 
      ---
      if L_prev_pack_no != L_pack_no then
         ---
         -- check if timelines exist for the pack item on the 
         -- purchase order.
         ---
         if TIMELINE_SQL.TIMELINES_EXIST(O_error_message,
                                         L_exists,
                                         'POIT',
                                         to_char(I_order_no),
                                         L_pack_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- if timelines do exist for the pack item and the component sku does
      -- not exist as a stand alone on ordsku, then default them to the
      -- component items.
      ---
      if L_exists = TRUE and L_ordsku_exists = FALSE then
         if TIMELINE_SQL.GET_DEFAULTS(O_error_message,
                                      'POIT',
                                      'POIT',
                                      to_char(I_order_no),     
                                      to_char(I_order_no), 
                                      L_pack_no,
                                      L_item) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      L_prev_pack_no := L_pack_no;
   END LOOP; -- end C_pack_skus
   ---
   if L_elc_ind = 'Y' then
      if L_import_ind = 'Y' and L_import_order_ind = 'Y' then
         --- recalculate HTS assessments on the order
         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PA',
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   I_order_no,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   L_import_country_id,
                                   L_origin_country_id,
                                   NULL,
                                   NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- if system elc ind = 'Y', calculate the expenses
      -- and assessments on the order.
      ---
      if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                'PE',
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                I_order_no,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                L_import_country_id,
                                L_origin_country_id,
                                NULL,
                                NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if L_import_ind = 'Y' and L_import_order_ind = 'Y' then
         ---
         -- recalculate HTS assessments on the order again to
         -- pick up newly calculated expense values.
         ---
         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PA',
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   I_order_no,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   L_import_country_id,
                                   L_origin_country_id,
                                   NULL,
                                   NULL) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;  --- L_elc_ind = 'Y'
   --- 
   -- Logistics Modification
   ---
   if WO_SQL.EXPLD_WO_PACK(O_error_message,
                           I_order_no) = FALSE then
      return FALSE;
   end if;
   ---
	L_table := 'ordsku_cfa_ext';
   open C_LOCK_ORDLOC_CFA_EXT;
   close C_LOCK_ORDLOC_CFA_EXT; 
   delete from ordloc_cfa_ext
         where order_no = I_order_no
           and item in (select item
                          from item_master
                         where pack_ind = 'Y'
                           and order_as_type = 'E');
   ---
	L_table := 'ordloc';
   open C_LOCK_ORDLOC;
   close C_LOCK_ORDLOC; 
   delete from ordloc 
         where order_no = I_order_no
           and item in (select item
                          from item_master
                         where pack_ind = 'Y'
                           and order_as_type = 'E');
   ---
	L_table := 'req_doc';
   open C_LOCK_REQ_DOC;
   close C_LOCK_REQ_DOC;
   delete from req_doc
    where module      = 'POIT'
      and key_value_1 = to_char(I_order_no)
      and key_value_2 in (select item
                          from item_master
                         where pack_ind = 'Y'
                           and order_as_type = 'E');
   ---
	L_table := 'timeline';
   open C_LOCK_TIMELINE;
   close C_LOCK_TIMELINE;
   delete from timeline
         where timeline_type = 'POIT'
           and key_value_1   = to_char(I_order_no)
           and key_value_2  in (select item
                                  from item_master
                                 where pack_ind      = 'Y'
                                   and order_as_type = 'E');
   ---
	L_table := 'ordsku_cfa_ext';
   open C_LOCK_ORDSKU_CFA_EXT;
   close C_LOCK_ORDSKU_CFA_EXT;
   delete from ordsku_cfa_ext
         where order_no = I_order_no
           and item in (select item
                          from item_master
                         where pack_ind = 'Y'
                           and order_as_type = 'E');
   ---
	L_table := 'ordsku';
   open C_LOCK_ORDSKU;
   close C_LOCK_ORDSKU;
   delete from ordsku 
         where order_no = I_order_no
           and item in (select item
                          from item_master
                         where pack_ind = 'Y'
                           and order_as_type = 'E');
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('POS_TABLE_LOCKED',
                                         L_table,
                                         to_char(I_order_no),
                                         NULL);
      return FALSE;														
   when OTHERS then
	  O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
															  SQLERRM,
															  L_program,
															  to_char(SQLCODE));
      return FALSE;
END PACK_SKU;
-------------------------------------------------------------------------------
FUNCTION APPROVAL_PRIVS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_approval_privs   IN OUT   BOOLEAN)
   return BOOLEAN is

   L_dummy   VARCHAR2(1);
  
   cursor C_APPR_PRIV is
      select 'x'
        from rtk_role_privs p, 
             sec_user_role r,
             sec_user u
       where p.role = r.role
         and r.user_seq = u.user_seq
         and GET_USER = DECODE(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), SYS_CONTEXT('USERENV', 'CLIENT_INFO')), NULL, u.database_user_id, u.application_user_id);

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_APPR_PRIV','RTK_ROLE_PRIVS, SEC_USER_ROLE, SEC_USER', NULL);
   open C_APPR_PRIV;
   SQL_LIB.SET_MARK('FETCH','C_APPR_PRIV','RTK_ROLE_PRIVS, SEC_USER_ROLE, SEC_USER', NULL);
   fetch C_APPR_PRIV into L_dummy;
   ---
   O_approval_privs := C_APPR_PRIV%FOUND;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_APPR_PRIV','RTK_ROLE_PRIVS, SEC_USER_ROLE, SEC_USER', NULL);
   close C_APPR_PRIV;   

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_APPROVE_SQL.APPROVAL_PRIVS',
                                            to_char(SQLCODE));
      return FALSE;
END APPROVAL_PRIVS;
-------------------------------------------------------------------------------
FUNCTION GET_APPROVAL_AMT(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_approval_amt   IN OUT   RTK_ROLE_PRIVS.ORD_APPR_AMT%TYPE)
   return BOOLEAN is

   -- if a user is NOT associated with any role, then return 0 for O_approval_amt
   cursor C_CHECK_ROLE_ZERO_AMT is
      select 0 
        from sec_user su
       where GET_USER = DECODE(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), SYS_CONTEXT('USERENV', 'CLIENT_INFO')), NULL, su.database_user_id, su.application_user_id)
         and not exists (select 'x' 
                           from sec_user_role sur
                          where sur.user_seq = su.user_seq
                            and rownum = 1);

   -- if a user is associated with a role with a NULL ord_appr_amt, then return 9999999999999999 for O_approval_amt
   cursor C_CHECK_ROLE is
      select max(NVL(rrp.ord_appr_amt, 9999999999999999))
        from rtk_role_privs rrp,
             sec_user_role sur,
             sec_user su
       where rrp.role = sur.role
         and sur.user_seq = su.user_seq
         and GET_USER = DECODE(NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), SYS_CONTEXT('USERENV', 'CLIENT_INFO')), NULL, su.database_user_id, su.application_user_id);

BEGIN

   open C_CHECK_ROLE_ZERO_AMT;
   fetch C_CHECK_ROLE_ZERO_AMT into O_approval_amt;
   close C_CHECK_ROLE_ZERO_AMT;   

   if O_approval_amt is NULL then
      open C_CHECK_ROLE;
      fetch C_CHECK_ROLE into O_approval_amt;
      close C_CHECK_ROLE;   
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_APPROVE_SQL.GET_APPROVAL_AMT',
                                            to_char(SQLCODE));
      return FALSE;
END GET_APPROVAL_AMT;
-------------------------------------------------------------------------------
END;
/