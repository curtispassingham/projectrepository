create or replace PACKAGE BODY MGNIMPACT_SQL AS
---------------------------------------------------------------------------------------------
-- Purpose: The Package contains functions used by the margin impact form mgnimpact.fmb
---------------------------------------------------------------------------------------------

PROCEDURE GET_MRGN_IMPACT_INFO(IO_margin_impact_tbl    IN OUT  MARGIN_IMPACT_TBL,
                               I_item                  IN      ITEM_MASTER.ITEM%TYPE,
                               I_supplier              IN      SUPS.SUPPLIER%TYPE,
                               I_origin_country        IN      COUNTRY.COUNTRY_ID%TYPE,
                               I_cost_change_level     IN      VARCHAR2,
                               I_cost_change           IN      COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                               I_status                IN      COST_SUSP_SUP_HEAD.STATUS%TYPE,
                               I_active_date           IN      COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                               I_cost_event_process_id IN      COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
---------------------------------------------------------------------------------------------
-- Procedure Name: GET_MRGN_IMPACT_INFO
-- Purpose: The function is called by the form mgnimpact.fmb. The function retrieves the
--          margin impact information and returns the records into the table margin_impact_tbl.
---------------------------------------------------------------------------------------------
IS

   L_program                VARCHAR2(64)   := 'MGNIMPACT_SQL.GET_MRGN_IMPACT_INFO';
   L_vdate                  PERIOD.VDATE%TYPE := GET_VDATE;
   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;
   L_dept                   ITEM_MASTER.DEPT%TYPE;
   L_error_message          RTK_ERRORS.RTK_TEXT%TYPE;
   L_location               ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_active_date            RPM_FUTURE_RETAIL.ACTION_DATE%TYPE;
   L_cost_event_process_id  COST_EVENT.COST_EVENT_PROCESS_ID%TYPE := NULL;

   L_class                  ITEM_MASTER.CLASS%TYPE;
   L_subclass               ITEM_MASTER.SUBCLASS%TYPE;
   L_regular_zone_group     RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE;
   L_clearance_zone_group   RPM_MERCH_RETAIL_DEF.CLEARANCE_ZONE_GROUP%TYPE;
   L_markup_calc_type       RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE;
   L_markup_percent         RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE;

   L_get_vat                VARCHAR2(1) := 'Y';
   L_pack_ind               ITEM_MASTER.PACK_IND%TYPE;
   L_class_vat_ind          CLASS.CLASS_VAT_IND%TYPE;
   L_unit_retail            ITEM_LOC.UNIT_RETAIL%TYPE;
   L_standard_uom           UOM_CLASS.UOM%TYPE;
   L_selling_uom            UOM_CLASS.UOM%TYPE;
   L_qty                    NUMBER;
   L_supplier_currency      CURRENCIES.CURRENCY_CODE%TYPE;
   L_vat_region             VAT_REGION.VAT_REGION%TYPE;
   L_vat_code               VAT_CODE_RATES.VAT_CODE%TYPE;
   L_vat_rate               VAT_CODE_RATES.VAT_RATE%TYPE;
   L_loc_type               FUTURE_COST.LOC_TYPE%TYPE;
   L_sellable_ind           ITEM_MASTER.SELLABLE_IND%TYPE;
   L_bud_mkup               DEPS.BUD_MKUP%TYPE;
   L_bud_int                DEPS.BUD_INT%TYPE;
   L_rpm_ind                varchar2(1):=NULL;
   L_currency_code          store.currency_code%TYPE;
   L_markup_type  DEPS.MARKUP_CALC_TYPE%TYPE;
   
   cursor C_RPM_IND  is 
      select RPM_IND 
	     from 
	    system_options;		  
   
   cursor C_CURRENCY_CODE is  
      select currency_code From store Where store = L_location
       union
      select currency_code from wh where wh = L_location; 		
		 
   cursor C_ITEM_LOC_RETAIL is
   select inner.selling_unit_retail/inner.exchange_rate selling_retail,
          inner.selling_uom selling_uom
     from (select distinct il.selling_uom,
                  il.selling_unit_retail,
                  first_value(mv.exchange_rate) over
                     (order by mv.effective_date desc) exchange_rate
             from item_loc            il,
                  mv_currency_conversion_rates mv
            where il.item = I_item
              and il.loc = L_location
              and mv.from_currency = L_currency_code
              and mv.to_currency = L_supplier_currency
              and mv.exchange_type = 'C'
              and mv.effective_date <= L_active_date) inner; 
   

   -- Gets the last cost event process id that ran successfully for the cost change
   
   cursor C_GET_CE_PROC_ID is
      select cost_event_process_id
        from (select ce.cost_event_process_id,
                     rank() over (order by ce.create_datetime desc) rank_date
                from cost_event ce,
                     cost_event_cost_chg cc,
                     cost_event_result cr
               where ce.cost_event_process_id = cc.cost_event_process_id
                 and ce.action                = 'ADD'
                 and ce.persist_ind           = 'N'
                 and cc.cost_change           = I_cost_change
                 and ce.cost_event_process_id = cr.cost_event_process_id
                 and cr.status                = 'C') all_rec
       where rank_date = 1;

   -- cursor to fetch sellable indicator for an item
   cursor C_GET_SELLABLE_IND is
      select im.sellable_ind
        from item_master im
       where im.item = I_item;

   -- cursor to fetch the margin impact records for the location level cost change.
   cursor C_MGN_IMPACT_LOC is
   select loc_type,
          location,
          loc_name,
          active_date,
          currency_code,
          pricing_cost,
          unit_retail,
          margin_percent,
          error_message,
          return_code
     from (select cd.code_desc loc_type,
                  fc.location,
                  loc.loc_name,
                  fc.active_date,
                  fc.pricing_cost,
                  fc.currency_code,
                  NULL unit_retail,
                  NULL margin_percent,
                  NULL error_message,
                  NULL return_code,
                  date_ind,
                  rank()
                  over (partition by fc.location,date_ind
                  order by fc.active_date) rank_date_asc,
                  rank()
                  over (partition by fc.location,date_ind
                  order by fc.active_date desc) rank_date_desc
             from (select future_cost.item,
                          future_cost.loc_type,
                          future_cost.location,
                          future_cost.active_date,
                          future_cost.currency_code,
                          future_cost.pricing_cost,
                          case when future_cost.active_date > I_active_date then 1
                               when future_cost.active_date = I_active_date then 0
                               else -1 end date_ind
                     from future_cost
                    where future_cost.item = I_item
                      and future_cost.supplier = I_supplier
                      and future_cost.origin_country_id = I_origin_country
                      and I_status in ('A', 'E')
                   UNION
                   select future_cost_workspace.item,
                          future_cost_workspace.loc_type,
                          future_cost_workspace.location,
                          future_cost_workspace.active_date,
                          future_cost_workspace.currency_code,
                          future_cost_workspace.pricing_cost,
                          case when future_cost_workspace.active_date > I_active_date then 1
                               when future_cost_workspace.active_date = I_active_date then 0
                               else -1 end date_ind
                     from future_cost_workspace
                    where future_cost_workspace.cost_event_process_id = L_cost_event_process_id
                      and future_cost_workspace.item = I_item
                      and future_cost_workspace.supplier = I_supplier
                      and future_cost_workspace.origin_country_id = I_origin_country
                      and I_status in ('W', 'S')
                  ) fc,
                  (select store location,
                          store_name loc_name
                     from v_store_tl
                   UNION ALL
                   select wh location,
                          wh_name loc_name
                     from v_wh_tl
                  ) loc,
                  code_detail cd
            where fc.location = loc.location
              and fc.loc_type = cd.code
              and cd.code_type = 'LOTP')
    where ( date_ind in (0, 1)  or
           (date_ind =-1 and rank_date_desc <= L_system_options_rec.margin_impact_hist_recs))
    order by location,active_date;

   -- cursor to fetch the margin impact records for the supplier level cost change.
   cursor C_MGN_IMPACT_SUPP is
   select loc_type,
          location,
          loc_name,
          active_date,
          currency_code,
          pricing_cost,
          unit_retail,
          margin_percent,
          error_message,
          return_code
     from (select cd.code_desc loc_type,
                  fc.location,
                  loc.loc_name,
                  fc.active_date,
                  fc.currency_code,
                  fc.pricing_cost,
                  NULL unit_retail,
                  NULL margin_percent,
                  NULL error_message,
                  NULL return_code,
                  date_ind,
                  rank()
                  over (partition by fc.location,date_ind
                  order by fc.active_date) rank_date_asc,
                  rank()
                  over (partition by fc.location,date_ind
                  order by fc.active_date desc) rank_date_desc
             from (select future_cost.item,
                          future_cost.loc_type,
                          future_cost.location,
                          future_cost.active_date,
                          future_cost.currency_code,
                          future_cost.pricing_cost,
                          case when future_cost.active_date > I_active_date then 1
                               when future_cost.active_date = I_active_date then 0
                               else -1 end date_ind
                     from future_cost
                    where future_cost.item = I_item
                      and future_cost.supplier = I_supplier
                      and future_cost.origin_country_id = I_origin_country
                      and I_status in ('A', 'E')
                    UNION
                   select future_cost_workspace.item,
                          future_cost_workspace.loc_type,
                          future_cost_workspace.location,
                          future_cost_workspace.active_date,
                          future_cost_workspace.currency_code,
                          future_cost_workspace.pricing_cost,
                          case when future_cost_workspace.active_date > I_active_date then 1
                               when future_cost_workspace.active_date = I_active_date then 0
                               else -1 end date_ind
                     from future_cost_workspace
                    where future_cost_workspace.cost_event_process_id = L_cost_event_process_id
                      and future_cost_workspace.item = I_item
                      and future_cost_workspace.supplier = I_supplier
                      and future_cost_workspace.origin_country_id = I_origin_country
                      and I_status in ('W', 'S')
                  ) fc,
                  (select store location,
                          store_name loc_name
                     from v_store_tl
                   UNION ALL
                   select wh location,
                          wh_name loc_name
                     from v_wh_tl
                  ) loc,
                  item_supp_country_loc iscl,
                  code_detail cd
            where fc.item = iscl.item
              and iscl.supplier = I_supplier
              and iscl.origin_country_id = I_origin_country
              and fc.location = iscl.loc
              and iscl.primary_loc_ind = 'Y'
              and loc.location = iscl.loc
              and fc.loc_type = cd.code
              and cd.code_type = 'LOTP')
    where ( date_ind in (0,1)  or
           (date_ind =-1 and rank_date_desc <= L_system_options_rec.margin_impact_hist_recs) )
    order by location,active_date;

   cursor C_GET_ITEM_ATTRIBUTE is
      select dept,
             class,
             subclass,
             standard_uom,
             pack_ind
        from item_master
       where item = I_item;

   
   
   --- Cursor to get Selling unit retail on supplier currency
   cursor C_FUT_RETAIL is
      select inner.selling_retail / inner.exchange_rate selling_retail,
             inner.selling_uom selling_uom
        from (select rf.selling_uom,
                     rf.action_date,
                     rf.selling_retail,
                     first_value(mv.exchange_rate) over
                        (partition by rf.item, rf.location, rf.selling_uom, rf.selling_retail_currency, rf.action_date
                         order by mv.effective_date desc) exchange_rate
                from rpm_future_retail            rf,
                     mv_currency_conversion_rates mv
               where rf.item = I_item
                 and rf.location = L_location
                 and rf.action_date <= L_active_date
                 and mv.from_currency = rf.selling_retail_currency
                 and mv.to_currency = L_supplier_currency
                 and mv.exchange_type = 'C'
                 and mv.effective_date <= rf.action_date) inner
       order by inner.action_date desc;

   cursor C_GET_CLASS_VAT_IND is
      select c.class_vat_ind
        from class c
       where c.class = L_class
         and c.dept  = L_dept;

BEGIN

   if I_item is null then
      IO_margin_impact_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                  'I_item',
                                                                  L_program,
                                                                  NULL);
      IO_margin_impact_tbl(1).return_code := 'FALSE';
      return;
   end if;
   --
   if I_supplier is null then
      IO_margin_impact_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                  'I_supplier',
                                                                  L_program,
                                                                  NULL);
      IO_margin_impact_tbl(1).return_code := 'FALSE';
      return;
   end if;
   --
   if I_origin_country is null then
      IO_margin_impact_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                  'I_origin_country',
                                                                  L_program,
                                                                  NULL);
      IO_margin_impact_tbl(1).return_code := 'FALSE';
      return;
   end if;
   --
   if I_cost_change_level is null then
      IO_margin_impact_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                  'I_cost_change_level',
                                                                  L_program,
                                                                  NULL);
      IO_margin_impact_tbl(1).return_code := 'FALSE';
      return;
   end if;
   --
   if I_status is null then
      IO_margin_impact_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                  'I_status',
                                                                  L_program,
                                                                  NULL);
      IO_margin_impact_tbl(1).return_code := 'FALSE';
      return;
   end if;
   --
   if I_active_date is null then
      IO_margin_impact_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                  'I_active_date',
                                                                  L_program,
                                                                  NULL);
      IO_margin_impact_tbl(1).return_code := 'FALSE';
      return;
   end if;
   --
   -- when the last cost_event_process_id is unknown then look for the last cost
   -- change cost event that ran successfully.
   if I_cost_event_process_id is null and
      I_status in ('W', 'S') then
      SQL_LIB.SET_MARK('OPEN' ,
                       'C_GET_CE_PROC_ID',
                       'COST_EVENT',
                       'Cost Change: '||I_cost_change);
      open C_GET_CE_PROC_ID;
      ---
      SQL_LIB.SET_MARK('FETCH' ,
                       'C_GET_CE_PROC_ID',
                       'COST_EVENT',
                       'Cost Change: '||I_cost_change);
      fetch C_GET_CE_PROC_ID into L_cost_event_process_id;
      ---
      SQL_LIB.SET_MARK('CLOSE' ,
                       'C_GET_CE_PROC_ID',
                       'COST_EVENT',
                       'Cost Change: '||I_cost_change);
      close C_GET_CE_PROC_ID;
   else
      L_cost_event_process_id := I_cost_event_process_id;
   end if;

   -- Get the number of historical events from the system options table.
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            L_system_options_rec)=FALSE then
      IO_margin_impact_tbl(1).return_code  := 'FALSE';
      IO_margin_impact_tbl(1).error_message := L_error_message;
      return;
   end if;

   -- Get item attributes from the ITEM_MASTER table.
   SQL_LIB.SET_MARK('OPEN' ,
                    'C_GET_ITEM_ATTRIBUTE',
                    'ITEM_MASTER',
                    'Item: '||I_item);
   open C_GET_ITEM_ATTRIBUTE;
   ---
   SQL_LIB.SET_MARK('FETCH' ,
                    'C_GET_ITEM_ATTRIBUTE',
                    'ITEM_MASTER',
                    'Item: '||I_item);
   fetch C_GET_ITEM_ATTRIBUTE into L_dept,
                                   L_class,
                                   L_subclass,
                                   L_standard_uom,
                                   L_pack_ind;
   ---
   if C_GET_ITEM_ATTRIBUTE%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE' ,
                       'C_GET_ITEM_ATTRIBUTE',
                       'ITEM_MASTER',
                       'Item: '||I_item);
      close C_GET_ITEM_ATTRIBUTE;
      ---
      IO_margin_impact_tbl(1).return_code  := 'FALSE';
      IO_margin_impact_tbl(1).error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                                                  NULL,
                                                                  NULL,
                                                                  NULL);
      return;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE' ,
                    'C_GET_ITEM_ATTRIBUTE',
                    'ITEM_MASTER',
                    'Item: '||I_item);

   close C_GET_ITEM_ATTRIBUTE;
   --- Get markup calc type
   
   open C_RPM_IND;
   fetch C_RPM_IND into L_rpm_ind;
   close C_RPM_IND;

   
   ---
   if L_rpm_ind = 'Y' then  
      if MERCH_RETAIL_API_SQL.GET_MERCH_PRICING_DEFS(L_error_message,
                                                     L_dept,
                                                     L_class,
                                                     L_subclass,
                                                     L_regular_zone_group,
                                                     L_clearance_zone_group,
                                                     L_markup_calc_type,
                                                     L_markup_percent) = FALSE then
         IO_margin_impact_tbl(1).return_code  := 'FALSE';
         IO_margin_impact_tbl(1).error_message := L_error_message;
         return;
      end if;
   else           
      if DEPT_ATTRIB_SQL.GET_MARKUP(L_error_message,
                                    L_markup_type,
                                    L_bud_int,
                                    L_bud_mkup,
                                    L_dept) = FALSE then
         IO_margin_impact_tbl(1).return_code  := 'FALSE';
         IO_margin_impact_tbl(1).error_message := L_error_message;
         return;
      end if;
      if L_markup_type = 'C' then
         L_markup_calc_type:=0;
      else
         L_markup_calc_type:=1;
      end if;  
   end if;
   --
       
   --
   if I_cost_change_level = 'L' then
      open  C_MGN_IMPACT_LOC;
      fetch C_MGN_IMPACT_LOC bulk collect into IO_margin_impact_tbl;
      close C_MGN_IMPACT_LOC;
   elsif I_cost_change_level = 'S' then
      open  C_MGN_IMPACT_SUPP;
      fetch C_MGN_IMPACT_SUPP bulk collect into IO_margin_impact_tbl;
      close C_MGN_IMPACT_SUPP;
   end if;
   ---
   open C_GET_CLASS_VAT_IND;
   fetch C_GET_CLASS_VAT_IND into L_class_vat_ind;
   close C_GET_CLASS_VAT_IND;
   ---
   open C_GET_SELLABLE_IND;
   fetch C_GET_SELLABLE_IND into L_sellable_ind;
   close C_GET_SELLABLE_IND;
   ---
   if L_pack_ind = 'Y' and L_sellable_ind = 'N' then
      L_get_vat := 'N';
   else
      L_get_vat := 'Y';
   end if;
   ---Get Supplier Currency
   if CURRENCY_SQL.GET_CURR_LOC(L_error_message,
                                to_char(I_supplier),
                                'V',
                                NULL,
                                L_supplier_currency) = FALSE then
      IO_margin_impact_tbl(1).return_code  := 'FALSE';
      IO_margin_impact_tbl(1).error_message := L_error_message;
      return;
   end if;
   -- Calculate the margin percentage
   for i in 1..IO_margin_impact_tbl.COUNT
   loop

      L_location := IO_margin_impact_tbl(i).location;
      L_active_date := IO_margin_impact_tbl(i).effective_date;
      L_vat_rate    := NULL;
      L_qty         := 0;
      L_selling_uom := NULL;
      --- Get location type
      if LOCATION_ATTRIB_SQL.GET_TYPE(L_error_message,
                                      L_loc_type,
                                      L_location) = FALSE then
         IO_margin_impact_tbl(1).return_code  := 'FALSE';
         IO_margin_impact_tbl(1).error_message := L_error_message;
         return;
      end if;
      ---
      if L_rpm_ind ='N' then
         open C_CURRENCY_CODE;
         fetch C_CURRENCY_CODE into L_currency_code;
         close C_CURRENCY_CODE;
      end if; 
      ---
	  --- 
	  if L_rpm_ind = 'Y' then 
	     SQL_LIB.SET_MARK('OPEN' ,
                          'C_FUT_RETAIL',
                          'RPM_FUTURE_RETAIL',
                          'Item: '||I_item);
         open C_FUT_RETAIL;
         ---
         SQL_LIB.SET_MARK('FETCH' ,
                         'C_FUT_RETAIL',
                         'RPM_FUTURE_RETAIL',
                         'Item: '||I_item);
         fetch C_FUT_RETAIL into L_unit_retail,
                                 L_selling_uom;
         ---
         SQL_LIB.SET_MARK('CLOSE' ,
                          'C_FUT_RETAIL',
                          'RPM_FUTURE_RETAIL',
                          'Item: '||I_item);
         close  C_FUT_RETAIL;
	  else	    
		 SQL_LIB.SET_MARK('OPEN' ,
                          'C_ITEM_LOC_RETAIL',
                          'ITEM_LOC',
                          'Item: '||I_item);
         open C_ITEM_LOC_RETAIL;
         ---
         SQL_LIB.SET_MARK('FETCH' ,
                         'C_ITEM_LOC_RETAIL',
                         'ITEM_LOC',
                         'Item: '||I_item);
         fetch C_ITEM_LOC_RETAIL into L_unit_retail,
                                 L_selling_uom;
         ---
         SQL_LIB.SET_MARK('CLOSE' ,
                          'C_ITEM_LOC_RETAIL',
                          'ITEM_LOC',
                          'Item: '||I_item);
         close  C_ITEM_LOC_RETAIL;
		 
      end if ;  
      -- End of change
      -- If the selling UOM and standard UOM are different, we need to use a converted value
      -- (i.e. convert the selling unit retail in the selling UOM to the selling unit retail
      -- in the standard UOM) when computing the MARGIN%.
      if L_selling_uom <> L_standard_uom then
         if UOM_SQL.CONVERT(L_error_message,
                            L_qty,
                            L_standard_uom,
                            1,
                            L_selling_uom,
                            I_item,
                            I_supplier,
                            I_origin_country) = FALSE then
            IO_margin_impact_tbl(1).return_code  := 'FALSE';
            IO_margin_impact_tbl(1).error_message := L_error_message;
            return;
         end if;
         --- The selling unit retail in standard UOM
         if L_qty <> 0 then
            L_unit_retail := L_unit_retail / L_qty;
         end if;
         ---
      end if;
      --- Unit retail displayed is VAT inclusive
      IO_margin_impact_tbl(i).unit_retail := L_unit_retail;

      if (L_system_options_rec.default_tax_type= 'SVAT' and
         L_class_vat_ind = 'Y' and
         L_get_vat = 'Y') then
         ---
         if VAT_SQL.GET_VAT_RATE(L_error_message,
                                 L_vat_region,
                                 L_vat_code,
                                 L_vat_rate,
                                 I_item,
                                 L_dept,
                                 L_loc_type,
                                 L_location,
                                 L_active_date,
                                 'R',
                                 case when L_loc_type = 'S' then --store_ind
                                    'Y'
                                 else
                                    'N'
                                 end ) = FALSE then
            IO_margin_impact_tbl(1).return_code  := 'FALSE';
            IO_margin_impact_tbl(1).error_message := L_error_message;
            return;
         end if;
         ---
         --- Exclude applicable VAT from the Unit retail before calculating MARGIN%
         if L_vat_rate is not null then
            L_unit_retail := L_unit_retail / (1 + L_vat_rate/100);
         end if;
         ---
      end if;
      ---
      --L_markup_calc_type = 0 is Cost Markup and 1 is Retail Markup
      ---
      if (L_markup_calc_type = 0 and NVL(IO_margin_impact_tbl(i).pricing_cost,0) != 0) then
         IO_margin_impact_tbl(i).margin_percent := round(100*(L_unit_retail - IO_margin_impact_tbl(i).pricing_cost)/IO_margin_impact_tbl(i).pricing_cost,2);
      elsif (L_markup_calc_type = 1 and NVL(IO_margin_impact_tbl(i).unit_retail,0) != 0) then
         IO_margin_impact_tbl(i).margin_percent := round(100*(L_unit_retail - IO_margin_impact_tbl(i).pricing_cost)/IO_margin_impact_tbl(i).unit_retail,2);
      end if;


   end loop;


EXCEPTION
   when OTHERS then
      IO_margin_impact_tbl(1).return_code  := 'FALSE';
      IO_margin_impact_tbl(1).error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 'MGNIMPACT_SQL.GET_MRGN_IMPACT_INFO',
                                                                 to_char(SQLCODE));
END GET_MRGN_IMPACT_INFO;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_FC_WORKSPACE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_change      IN     COST_EVENT_COST_CHG.COST_CHANGE%TYPE)

   RETURN BOOLEAN IS
---------------------------------------------------------------------------------------------
-- Procedure Name: DELETE_FC_WORKSPACE
-- Purpose: The function is called by the form mgnimpact.fmb. The function deletes the records
--          from FUTURE_COST_EVENT_WORKSPACE table for a cost change passed to the function.
---------------------------------------------------------------------------------------------
   L_program          VARCHAR2(64)   := 'MGNIMPACT_SQL.DELETE_FC_WORKSPACE';
   L_table            VARCHAR2(30);
   record_locked      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(record_locked, -54);

   cursor C_LOCK_FC_WORKSPACE is
      select 'Y' from future_cost_workspace fcw
       where exists (select 'X'
                       from cost_event_cost_chg
                      where cost_event_process_id = fcw.cost_event_process_id
                        and cost_change = I_cost_change)
         for update nowait;

BEGIN
   --
   if I_cost_change is NULL then
      O_error_message := 'INV_PARAMS';
      return FALSE;
   end if;
   --
   L_table := 'FUTURE_COST_WORKSPACE';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_FC_WORKSPACE',
                    'FUTURE_COST_WORKSPACE',
                    'Cost change: '||I_cost_change);
   open C_LOCK_FC_WORKSPACE;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_FC_WORKSPACE',
                    'FUTURE_COST_WORKSPACE',
                    'Cost change: '||I_cost_change);
   close C_LOCK_FC_WORKSPACE;
   ---

   delete from future_cost_workspace fcw
    where exists (select 'X'
                    from cost_event_cost_chg
                   where cost_event_process_id = fcw.cost_event_process_id
                     and cost_change = I_cost_change);

   return TRUE;
   --
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_cost_change);
      return FALSE;
      --
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END DELETE_FC_WORKSPACE;

END MGNIMPACT_SQL;
/