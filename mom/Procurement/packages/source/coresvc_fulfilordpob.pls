CREATE OR REPLACE PACKAGE BODY CORESVC_FULFILORD_PO AS

TYPE ORDHEAD_TBL is TABLE of ORDHEAD%ROWTYPE;
TYPE ORDSKU_TBL is TABLE of ORDSKU%ROWTYPE;
TYPE ORDLOC_TBL is TABLE of ORDLOC%ROWTYPE;
TYPE ORDCUST_HEADER_TBL is TABLE of ORDCUST%ROWTYPE;
TYPE ORDCUST_DETAIL_TBL is TABLE of ORDCUST_DETAIL%ROWTYPE;

TYPE ITEM_LOC_REC is RECORD (item               ITEM_LOC.ITEM%TYPE,
                             loc                ITEM_LOC.LOC%TYPE);
TYPE ITEM_LOC_TBL is TABLE of ITEM_LOC_REC;

TYPE PO_CANCEL_HDR_REC is RECORD (order_no    ORDHEAD.ORDER_NO%TYPE,
                                  location    ITEM_LOC.LOC%TYPE);

TYPE PO_CANCEL_DTL_REC is RECORD (item              ITEM_TBL,
                                  cancel_qty_suom   QTY_TBL);

TYPE PO_CANCEL_REC is RECORD (header_rec        PO_CANCEL_HDR_REC,
                              item_rec          PO_CANCEL_DTL_REC);
TYPE PO_CANCEL_TBL is TABLE of PO_CANCEL_REC;

TYPE ITEM_STATUS_REC is RECORD (fulfilord_id   SVC_FULFILORD.FULFILORD_ID%TYPE,
                                all_item_cnt    NUMBER,
                                inact_item_cnt  NUMBER,
                                status          VARCHAR2(1));

TYPE ITEM_STATUS_TAB is TABLE of ITEM_STATUS_REC;

LP_vdate                  DATE := GET_VDATE();
LP_so_ship_days           SYSTEM_OPTIONS.LATEST_SHIP_DAYS%TYPE;
LP_base_country_id        SYSTEM_OPTIONS.BASE_COUNTRY_ID%TYPE;
LP_import_ind             SYSTEM_OPTIONS.IMPORT_IND%TYPE;
LP_elc_ind                SYSTEM_OPTIONS.ELC_IND%TYPE;
LP_supplier_sites_ind     SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE;
LP_user                   SVCPROV_CONTEXT.USER_NAME%TYPE := GET_USER;
LP_import_item_ind        VARCHAR2(1);
LP_ordcust_partial_ind    VARCHAR2(1);
LP_ordcust_l10n_ext_tab   L10N_ORDCUST_EXT_TBL := L10N_ORDCUST_EXT_TBL();
LP_ordhead                ORDHEAD_TBL := ORDHEAD_TBL();
LP_ordcust_hdr            ORDCUST_HEADER_TBL := ORDCUST_HEADER_TBL();
LP_ordcust_dtl            ORDCUST_DETAIL_TBL := ORDCUST_DETAIL_TBL();
LP_ordsku                 ORDSKU_TBL := ORDSKU_TBL();
LP_ordloc                 ORDLOC_TBL := ORDLOC_TBL();
LP_new_item_loc           ITEM_LOC_TBL := ITEM_LOC_TBL();
LP_cancel_tbl             PO_CANCEL_TBL := PO_CANCEL_TBL();
LP_chk_status             ITEM_STATUS_TAB := ITEM_STATUS_TAB();

----------------------------------------------------------------------------------------
--Name     : VALIDATE_CREATE_PO
--Function : This function will perform additional validations specific for Drop Ship and
--           Multi-site POs on the header and detail values of the Customer Order request
--           message.  Validation includes supplier availability for direct store delivery,
--           item/supplier availability for direct customer delivery, and item status in
--           the fulfillment location.  If any error is found, the ERROR_MSG column in the
--           staging tables will be populated.
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_CREATE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : POPULATE_CREATE_REC
--Function : This function will populate the PL/SQL global collections with validated data
--           from the staging tables to facilitate bulk persistence.
----------------------------------------------------------------------------------------
FUNCTION POPULATE_CREATE_REC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_ordcust_ids     IN OUT   ID_TBL,
                             I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Name     :PERSIST_CREATE_PO
-- Function :This function will create the actual order records for Drop Ship and Multi-
--           site POs in the ORDHEAD, ORDSKU, ORDLOC, ORDCUST and ORDCUST_DETAIL tables.
--           This will also perform the other processing for the PO such as applying
--           default expenses, apply off invoice deals and creating item/loc records if
--           none is existing yet for the item/s and location in the order.
----------------------------------------------------------------------------------------
FUNCTION PERSIST_CREATE_PO(O_error_message   IN   OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name     : VALIDATE_CANCEL_PO
--Function : This private function will check if the order and item to be cancelled are
--           valid.  If any error is found, the ERROR_MSG column in the staging tables
--           will be populated, and the package will return FALSE.
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_CANCEL_PO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name      : POPULATE_CANCEL_REC
--Function  : This function will populate the PL/SQL global collection with validated
--            data form the staging tables to facilitate bulk persistence for
--            cancelling /modifying the customer orders.
----------------------------------------------------------------------------------------
FUNCTION POPULATE_CANCEL_REC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Name      : PERSIST_CANCEL_PO
--Function  : This function will update the order header and detail records based on the
--            cancel/modification message.  This can result to a partial deletion of the
--            line item quantity, full cancellation of the line item quantity and
--            cancellation of the entire order.
----------------------------------------------------------------------------------------
FUNCTION PERSIST_CANCEL_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION CREATE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_ordcust_ids     IN OUT   ID_TBL,
                   I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                   I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(50)     := 'CORESVC_FULFILORD_PO.CREATE_PO';
   L_table            VARCHAR2(30);
   L_invalid_param    VARCHAR2(30)  := NULL;
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);
   L_l10n_obj         L10N_OBJ          := L10N_OBJ();

   cursor C_LOCK_SVC_FULFILORD is
      select 'x'
        from svc_fulfilord
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
         and process_status in (CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED,CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE)
         for update nowait;

BEGIN

   -- initialize package global collections to prevent data from being persistent between calls
   LP_ordcust_l10n_ext_tab := L10N_ORDCUST_EXT_TBL();
   LP_ordhead              := ORDHEAD_TBL();
   LP_ordcust_hdr          := ORDCUST_HEADER_TBL();
   LP_ordcust_dtl          := ORDCUST_DETAIL_TBL();
   LP_ordsku               := ORDSKU_TBL();
   LP_ordloc               := ORDLOC_TBL();
   LP_new_item_loc         := ITEM_LOC_TBL();
   LP_cancel_tbl           := PO_CANCEL_TBL();
   LP_chk_status           := ITEM_STATUS_TAB();

   if I_process_id is NULL then
      L_invalid_param := 'I_process_id';
   elsif I_chunk_id is NULL then
      L_invalid_param := 'I_chunk_id';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   if VALIDATE_CREATE_PO (O_error_message,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if POPULATE_CREATE_REC (O_error_message,
                           O_ordcust_ids,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_CREATE_PO (O_error_message) = FALSE then
      return FALSE;
   end if;

   -- delete package global collections to prevent data from being persistent between calls
   LP_ordcust_l10n_ext_tab.DELETE;
   LP_ordhead.DELETE;
   LP_ordcust_hdr.DELETE;
   LP_ordcust_dtl.DELETE;
   LP_ordsku.DELETE;
   LP_ordloc.DELETE;
   LP_new_item_loc.DELETE;
   LP_cancel_tbl.DELETE;
   LP_chk_status.DELETE;

   L_table := 'SVC_FULFILORD';
   open C_LOCK_SVC_FULFILORD;
   close C_LOCK_SVC_FULFILORD;

   update svc_fulfilord
      set process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED,
          last_update_id = LP_user,
          last_update_datetime = SYSDATE
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
      and process_status in (CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED,CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE);
   --   
   L_l10n_obj.procedure_key := 'UPDATE_SVC_BRFULFILORD';
   L_l10n_obj.process_id    := I_process_id;
   --
   if L10N_SQL.EXEC_FUNCTION(O_error_message,
                            L_l10n_obj) = FALSE then
     return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_PO;
----------------------------------------------------------------------------------------
FUNCTION CANCEL_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                   I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(50)     := 'CORESVC_FULFILORD_PO.CANCEL_PO';
   L_invalid_param    VARCHAR2(30)     := NULL;
   L_table            VARCHAR2(30);
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_FULFILORDREF is
      select 'x'
        from svc_fulfilordref
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
         and process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         for update nowait;

BEGIN

   -- initialize package global collections to prevent data from being persistent between calls
   LP_ordcust_l10n_ext_tab := L10N_ORDCUST_EXT_TBL();
   LP_ordhead              := ORDHEAD_TBL();
   LP_ordcust_hdr          := ORDCUST_HEADER_TBL();
   LP_ordcust_dtl          := ORDCUST_DETAIL_TBL();
   LP_ordsku               := ORDSKU_TBL();
   LP_ordloc               := ORDLOC_TBL();
   LP_new_item_loc         := ITEM_LOC_TBL();
   LP_cancel_tbl           := PO_CANCEL_TBL();
   LP_chk_status           := ITEM_STATUS_TAB();

   if I_process_id is NULL then
      L_invalid_param := 'I_process_id';
   elsif I_chunk_id is NULL then
      L_invalid_param := 'I_chunk_id';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   if VALIDATE_CANCEL_PO (O_error_message,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if POPULATE_CANCEL_REC (O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PERSIST_CANCEL_PO (O_error_message)= FALSE then
      return FALSE;
   end if;

   -- delete package global collections to prevent data from being persistent between calls
   LP_ordcust_l10n_ext_tab.DELETE;
   LP_ordhead.DELETE;
   LP_ordcust_hdr.DELETE;
   LP_ordcust_dtl.DELETE;
   LP_ordsku.DELETE;
   LP_ordloc.DELETE;
   LP_new_item_loc.DELETE;
   LP_cancel_tbl.DELETE;
   LP_chk_status.DELETE;

   L_table := 'SVC_FULFILORDREF';
   open C_LOCK_SVC_FULFILORDREF;
   close C_LOCK_SVC_FULFILORDREF;

   update svc_fulfilordref
      set process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED,
          last_update_id = LP_user,
          last_update_datetime = SYSDATE
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
      and process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CANCEL_PO;
----------------------------------------------------------------------------------------
--  PRIVATE FUNCTIONS
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_CREATE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50)               := 'CORESVC_FULFILORD_PO.VALIDATE_CREATE_PO';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_table           VARCHAR2(30);
   L_org_unit_ind    SYSTEM_OPTIONS.ORG_UNIT_IND%TYPE;

   cursor C_LOCK_SVC_FULFILORD is
      select 'x'
        from svc_fulfilord sf
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilorddtl sd
                      where sd.error_msg is NOT NULL
                        and sd.fulfilord_id = sf.fulfilord_id
                        and sd.process_id = sf.process_id
                        and sd.chunk_id = sf.chunk_id
                        and rownum = 1)
         for update nowait;

   cursor C_CHK_STATUS is
    select s.fulfilord_id,
           count(d.item) all_item,
           i.inv_item_cnt
      from svc_fulfilord s,
           svc_fulfilorddtl d,
          (select count(sd.item) inv_item_cnt,
                  sf.fulfilord_id,
                  sf.process_id,
                  sf.chunk_id
             from svc_fulfilord sf,
                  svc_fulfilorddtl sd
            where sf.process_id = I_process_id
              and sf.chunk_id = I_chunk_id
              and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
              and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
              and sf.process_id = sd.process_id
              and sf.chunk_id = sd.chunk_id
              and sf.fulfilord_id = sd.fulfilord_id
              and sd.item_status = CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE
            group by sf.fulfilord_id,sf.process_id,sf.chunk_id) i
      where s.process_id = I_process_id
        and s.chunk_id = I_chunk_id
        and s.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
        and s.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
        and s.process_id = d.process_id
        and s.chunk_id = d.chunk_id
        and s.fulfilord_id = d.fulfilord_id
        and s.process_id = i.process_id(+)
        and s.chunk_id = i.chunk_id(+)
        and s.fulfilord_id = i.fulfilord_id(+)
      group by s.fulfilord_id,
               i.inv_item_cnt
      order by s.fulfilord_id;

   cursor C_GET_SYS_OPTN is
      select latest_ship_days,
             base_country_id,
             import_ind,
             elc_ind,
             supplier_sites_ind,
             org_unit_ind
        from system_options;

   TYPE STATUS_REC is TABLE OF C_CHK_STATUS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_chk_status   STATUS_REC;

BEGIN

   open C_GET_SYS_OPTN;
   fetch C_GET_SYS_OPTN into LP_so_ship_days,
                             LP_base_country_id,
                             LP_import_ind,
                             LP_elc_ind,
                             LP_supplier_sites_ind,
                             L_org_unit_ind;
   close C_GET_SYS_OPTN;

   merge into svc_fulfilord sv
   using (select *
            from (select svcf.process_id,
                         svcf.chunk_id,
                         svcf.fulfilord_id,
                          -- validate if SYSTEM_OPTIONS.SUPPLIER_SITE_IND is Y, supplier_parent should not be null.
                         case
                             when (LP_supplier_sites_ind = 'Y' and s.supplier_parent is NULL) then
                                SQL_LIB.GET_MESSAGE_TEXT('INV_SUPPLIER_SITE_CO', svcf.customer_order_no, svcf.fulfill_order_no, NULL)||';'
                         end ||
                          -- validate if supplier is Active
                         case
                             when s.sup_status != 'A' then
                                SQL_LIB.GET_MESSAGE_TEXT('SUPP_NOT_ACTIVE_CO', svcf.customer_order_no, svcf.fulfill_order_no, NULL)||';'
                         end ||
                         -- org_unit_id of the supplier (source_loc_id) and the fulfillment location (fulfill_loc_id) should be the same
                         -- when the fulfillment location is a company store. In case of fulfillment at a franchise store, item/franchise
                         -- store's costing loc will be used for org_unit_id validation. It will be done in a separate merge statement.
                         case
                            when svcf.source_loc_id is NOT NULL and L_org_unit_ind = 'Y' and
                                 st.store_type = 'C' and --filter company stores only
                                 NOT exists (select 'x'
                                               from partner_org_unit p
                                              where p.partner = svcf.source_loc_id
                                                and p.org_unit_id = st.org_unit_id
                                                and rownum = 1)  then
                               SQL_LIB.GET_MESSAGE_TEXT('NO_SAME_ORG_CO', svcf.customer_order_no, svcf.fulfill_order_no, NULL)||';'
                          end ||
                          -- validate if fulfill_loc_type ='S' (physical store), the supplier
                          -- (source_loc_id) should be direct to store delivery
                          case
                             when svcf.fulfill_loc_type = 'S' and s.dsd_ind != 'Y' then
                                SQL_LIB.GET_MESSAGE_TEXT('SUPP_NOT_DSD_CO', svcf.customer_order_no, svcf.fulfill_order_no, NULL)||';'
                          end error_message
                    from svc_fulfilord svcf,
                         (select supplier,
                                 dsd_ind,
                                 sup_status,
                                 supplier_parent
                            from sups) s,
                         store st
                   where svcf.process_id = I_process_id
                     and svcf.chunk_id = I_chunk_id
                     and svcf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                     and svcf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
                     and svcf.source_loc_id = s.supplier
                     and svcf.fulfill_loc_id = st.store
                     )
             where error_message is NOT NULL) inner
      on (sv.process_id = inner.process_id
          and sv.chunk_id = inner.chunk_id
          and sv.fulfilord_id = inner.fulfilord_id
          and sv.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
          and inner.error_message IS NOT NULL)
    when matched then
      update set sv.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
                 sv.error_msg = substr(sv.error_msg || inner.error_message, 1, 2000),
                 sv.last_update_id = LP_user,
                 sv.last_update_datetime = SYSDATE;

   if SQL%ROWCOUNT > 0 then

      select substr(error_msg, 1, instr(error_msg, ';')-1)
        into L_error_message
        from svc_fulfilord
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and error_msg is NOT NULL
         and rownum = 1;
      O_error_message := L_error_message;
      return FALSE;
   end if;

   -- Franchise store validation
   -- 1) All item/fulfill_loc_id (franchise store) should have the same costing_loc for a given customer fulfillment order
   -- 2) The costing location's org_unit_id should match the source_loc_id (supplier)'s org_unit_id
   merge into svc_fulfilord sv
   using (select *
            from (select distinct svcf.process_id,
                         svcf.chunk_id,
                         svcf.fulfilord_id,
                         -- validate that all items on a customer order have the same costing_loc at the fulfillment location
                         case
                            when
                               count (distinct svcf.costing_loc) over (partition by svcf.process_id, svcf.chunk_id, svcf.fulfilord_id) > 1 then
                                  SQL_LIB.GET_MESSAGE_TEXT('ONE_COST_LOC_FPO_CO', svcf.customer_order_no, svcf.fulfill_order_no, NULL) || ';'
                            end ||
                         --org unit id of the supplier and location (costing_loc) must be the same
                         case
                            when svcf.source_loc_id is NOT NULL and L_org_unit_ind = 'Y' and
                                 NOT exists (select 'x'
                                               from partner_org_unit p,
                                                    store st
                                              where p.partner = svcf.source_loc_id
                                                and st.store = svcf.costing_loc
                                                and p.org_unit_id = st.org_unit_id
                                                and rownum = 1
                                             union all
                                             select 'x'
                                               from partner_org_unit p,
                                                    wh wh
                                              where p.partner = svcf.source_loc_id
                                                and wh.wh = svcf.costing_loc
                                                and p.org_unit_id = wh.org_unit_id
                                                and rownum = 1)  then
                               SQL_LIB.GET_MESSAGE_TEXT('NO_SAME_ORG_CO_FPO', svcf.customer_order_no, svcf.fulfill_order_no, NULL)||';'
                          end error_msg
                    from (select s.process_id,
                                 s.chunk_id,
                                 s.customer_order_no,
                                 s.fulfill_order_no,
                                 s.fulfilord_id,
                                 s.source_loc_id,
                                 s.fulfill_loc_id,
                                 il.costing_loc
                            from svc_fulfilord s,
                                 svc_fulfilorddtl sd,
                                 item_loc il,
                                 store st
                           where s.process_id = I_process_id
                             and s.chunk_id = I_chunk_id
                             and s.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                             and s.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
                             and s.process_id = sd.process_id
                             and s.chunk_id = sd.chunk_id
                             and s.fulfilord_id = sd.fulfilord_id
                             and s.fulfill_loc_id = st.store
                             and st.store_type = 'F'
                             and il.item = sd.item
                             and il.loc = s.fulfill_loc_id
                             and il.status = 'A') svcf   --a view of franchise customer order detail with costing location
                    )
           where error_msg is NOT NULL) inner
      on (    sv.process_id       = inner.process_id
          and sv.chunk_id         = inner.chunk_id
          and sv.fulfilord_id     = inner.fulfilord_id
          and sv.tran_type        = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
          and inner.error_msg IS NOT NULL)
    when matched then
      update set sv.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
                 sv.error_msg = substr(sv.error_msg || inner.error_msg, 1, 2000),
                 sv.last_update_id = LP_user,
                 sv.last_update_datetime = SYSDATE;

   if SQL%ROWCOUNT > 0 then
      select substr(error_msg, 1, instr(error_msg, ';')-1)
        into L_error_message
        from svc_fulfilord
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and error_msg is NOT NULL
         and rownum = 1;
      O_error_message := L_error_message;
      return FALSE;
   end if;

   merge into svc_fulfilorddtl sfd
   using (select *
            from (select sf.process_id,
                         sf.chunk_id,
                         sf.fulfilord_id,
                         sf.item,
                         -- validate that item should be orderable
                         case
                            when im.orderable_ind = 'N' then
                               SQL_LIB.GET_MESSAGE_TEXT('WRONG_ORDERABLE_IND_CO', s.customer_order_no, s.fulfill_order_no, NULL) || ';'
                         end ||
                         -- validate if item is supplied by supplier
                         case
                            when s.source_loc_id is NOT NULL and
                                 NOT exists (select 'x'
                                               from item_supplier i
                                              where i.item = sf.item
                                                and i.supplier = s.source_loc_id) then
                               SQL_LIB.GET_MESSAGE_TEXT('NO_ITEM_SUPP_PO_CO', s.customer_order_no, s.fulfill_order_no, NULL) || ';'
                         end ||
                         -- validate if pack item is not ordered as Each
                         case
                            when im.pack_ind = 'Y' and im.order_as_type = 'E' then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_ORDER_AS_TYPE_CO', sf.item, s.customer_order_no, s.fulfill_order_no) || ';'
                         end error_msg
                    from svc_fulfilord s,
                         svc_fulfilorddtl sf,
                         item_master im
                   where s.process_id = I_process_id
                     and s.chunk_id = I_chunk_id
                     and s.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                     and s.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
                     and s.process_id = sf.process_id
                     and s.chunk_id = sf.chunk_id
                     and s.fulfilord_id = sf.fulfilord_id
                     and sf.item = im.item
                     and im.status = 'A')
           where error_msg is NOT NULL) inner
      on (sfd.process_id = inner.process_id
          and sfd.chunk_id = inner.chunk_id
          and sfd.fulfilord_id = inner.fulfilord_id
          and sfd.item = inner.item)
    when matched then
      update set sfd.error_msg = substr(sfd.error_msg || inner.error_msg, 1, 2000),
                 sfd.last_update_id = LP_user,
                 sfd.last_update_datetime = SYSDATE;

   if SQL%ROWCOUNT > 0 then
      L_table := 'SVC_FULFILORD';
      open C_LOCK_SVC_FULFILORD;
      close C_LOCK_SVC_FULFILORD;

      update svc_fulfilord sf
         set sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
             sf.error_msg = substr(sf.error_msg || SQL_LIB.GET_MESSAGE_TEXT('ERROR_SVC_DETAIL', sf.customer_order_no, sf.fulfill_order_no, NULL)||';', 1, 2000),
             sf.last_update_id = LP_user,
             sf.last_update_datetime = sysdate
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilorddtl sd
                      where sf.fulfilord_id = sd.fulfilord_id
                        and sf.process_id = sd.process_id
                        and sf.chunk_id = sd.chunk_id
                        and sd.error_msg is NOT NULL
                        and rownum = 1);

      select substr(error_msg, 1, instr(error_msg, ';')-1)
        into L_error_message
        from svc_fulfilorddtl
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and error_msg is NOT NULL
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   merge into svc_fulfilorddtl svd
   using (select svfd.process_id,
                 svfd.chunk_id,
                 svfd.fulfilord_id,
                 svfd.item
            from svc_fulfilorddtl svfd,
                 svc_fulfilord svcf,
                 item_supplier isup
           where svfd.process_id = I_process_id
             and svfd.chunk_id = I_chunk_id
             and svfd.process_id = svcf.process_id
             and svfd.chunk_id = svcf.chunk_id
             and svfd.fulfilord_id = svcf.fulfilord_id
             and svcf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
             and svcf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
             and svfd.item = isup.item
             and svcf.source_loc_id = isup.supplier
             and ((svcf.fulfill_loc_type = 'V'
                  and isup.direct_ship_ind != 'Y')or
                   exists( select 'X' from item_loc il 
                            where il.item = isup.item 
                              and il.loc = svcf.fulfill_loc_id
                              and svfd.item = il.item                              
                              and il.loc_type = 'S' 
                              and il.status in ('C','I','D'))))inner
           on (svd.process_id = inner.process_id
          and svd.chunk_id = inner.chunk_id
          and svd.fulfilord_id = inner.fulfilord_id
          and svd.item = inner.item)
    when matched then
      update set svd.item_status = CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE,
                 svd.last_update_id = LP_user,
                 svd.last_update_datetime = SYSDATE;

   merge into svc_fulfilorddtl svd
   using (select svfd.process_id,
                 svfd.chunk_id,
                 svfd.fulfilord_id,
                 svfd.item
            from svc_fulfilorddtl svfd,
                 svc_fulfilord svcf,
                 item_supplier isup,
                 item_master im
           where svfd.process_id = I_process_id
             and svfd.chunk_id = I_chunk_id
             and svfd.process_id = svcf.process_id
             and svfd.chunk_id = svcf.chunk_id
             and svfd.fulfilord_id = svcf.fulfilord_id
             and svcf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
             and svcf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
             and svfd.item = im.item
             and im.container_item is NOT NULL
             and im.container_item = isup.item
             and svcf.source_loc_id = isup.supplier
             and ((svcf.fulfill_loc_type = 'V'
                  and isup.direct_ship_ind != 'Y')or
                  exists( select 'X' from item_loc il
                           where il.loc = svcf.fulfill_loc_id
                             and il.item = im.container_item
                             and il.loc_type = 'S'
                             and il.status in ('C','I','D'))))inner
      on (svd.process_id = inner.process_id
          and svd.chunk_id = inner.chunk_id
          and svd.fulfilord_id = inner.fulfilord_id
          and svd.item = inner.item)
    when matched then
      update set svd.item_status = CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE,
                 svd.last_update_id = LP_user,
                 svd.last_update_datetime = SYSDATE;

   open C_CHK_STATUS;
   fetch C_CHK_STATUS BULK COLLECT INTO L_chk_status;
   close C_CHK_STATUS;

   if L_chk_status is NOT NULL and L_chk_status.COUNT > 0 then
      LP_chk_status := ITEM_STATUS_TAB();

      FOR rec in L_chk_status.FIRST..L_chk_status.LAST LOOP
         LP_chk_status.EXTEND();
         LP_chk_status(LP_chk_status.COUNT).fulfilord_id := L_chk_status(rec).fulfilord_id;
         if NVL(L_chk_status(rec).inv_item_cnt,0) = 0 then
            -- no invalid items found in SVC_FULFILORDDTL table, ORDCUST status is 'C'
            LP_chk_status(LP_chk_status.COUNT).status := CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED;
         elsif L_chk_status(rec).all_item = L_chk_status(rec).inv_item_cnt then
            -- all item found in SVC_FULFILORDDTL table are invalid , ORDCUST status is 'X'
            LP_chk_status(LP_chk_status.COUNT).status := CORESVC_FULFILORD.ORDCUST_STATUS_NOT_CRE;
            update svc_fulfilord
               set process_status = CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE,
                   last_update_id = LP_user,
                   last_update_datetime = SYSDATE
             where process_id = I_process_id
               and chunk_id = I_chunk_id
               and process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
               and tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
               and fulfilord_id = L_chk_status(rec).fulfilord_id
               and exists (select 'x'
                             from svc_fulfilorddtl
                            where process_id = I_process_id
                              and chunk_id = I_chunk_id
                              and fulfilord_id = L_chk_status(rec).fulfilord_id
                              and item_status is NOT NULL);
         elsif L_chk_status(rec).all_item > L_chk_status(rec).inv_item_cnt then
            -- some items found in SVC_FULFILORDDTL table are invalid , ORDCUST status is 'P'
            LP_chk_status(LP_chk_status.COUNT).status := CORESVC_FULFILORD.ORDCUST_STATUS_PARTIAL;
         end if;
      END LOOP;
   end if; -- if L_chk_status is NOT NULL

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_CREATE_PO;
----------------------------------------------------------------------------------------
FUNCTION POPULATE_CREATE_REC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_ordcust_ids     IN OUT   ID_TBL,
                             I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(50)     := 'CORESVC_FULFILORD_PO.POPULATE_CREATE_REC';

   L_order_no                   ORDHEAD.ORDER_NO%TYPE;
   L_invalid_param              VARCHAR2(30)  := NULL;
   L_currency_code              SUPS.CURRENCY_CODE%TYPE;
   L_terms                      SUPS.TERMS%TYPE;
   L_freight_terms              SUPS.FREIGHT_TERMS%TYPE;
   L_sup_status                 SUPS.SUP_STATUS%TYPE;
   L_qc_ind                     SUPS.QC_IND%TYPE;
   L_edi_po_ind                 SUPS.EDI_PO_IND%TYPE;
   L_pre_mark_ind               SUPS.PRE_MARK_IND%TYPE;
   L_ship_method                SUPS.SHIP_METHOD%TYPE;
   L_payment_method             SUPS.PAYMENT_METHOD%TYPE;
   L_latest_ship_date           DATE;
   L_otb_eow_date               DATE;
   L_delivery_date              DATE;
   L_exchange_rate              CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_fulfilord_id               SVC_FULFILORDDTL.FULFILORD_ID%TYPE;
   L_chk_status                 VARCHAR2(1);
   L_item                       SVC_FULFILORDDTL.ITEM%TYPE;
   L_loc                        SVC_FULFILORD.FULFILL_LOC_ID%TYPE;
   L_loc_type                   SVC_FULFILORD.FULFILL_LOC_TYPE%TYPE;
   L_origin_cty                 ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_loc_id                     ADDR.KEY_VALUE_1%TYPE;
   L_supplier                   SUPS_IMP_EXP.SUPPLIER%TYPE;
   L_import_id                  SUPS_IMP_EXP.IMPORT_ID%TYPE;
   L_import_type                SUPS_IMP_EXP.IMPORT_TYPE%TYPE;
   L_count_item                 NUMBER;
   L_count_status               NUMBER;
   L_routing_loc_id             SUPS_ROUTING_LOC.ROUTING_LOC_ID%TYPE;
   L_exists                     BOOLEAN;
   L_outloc_id                  OUTLOC.OUTLOC_ID%TYPE;
   L_outloc_desc                OUTLOC.OUTLOC_DESC%TYPE;
   L_unit_cost                  ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE;
   L_item_ranged                VARCHAR2(1);
   L_ordcust_ind                VARCHAR2(1) := 'N';
   L_franchise_po               VARCHAR2(1) := 'N';
   L_costing_loc                ITEM_LOC.COSTING_LOC%TYPE := NULL;
   L_supp_pack_size             ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE:=1;

   cursor C_GET_HDR_REC is
      select svf.process_id,
             svf.chunk_id,
             svf.process_status,
             svf.action_type,
             svf.tran_type,
             svf.fulfilord_id,
             svf.customer_order_no,
             svf.fulfill_order_no,
             svf.source_loc_type,
             svf.source_loc_id,
             svf.fulfill_loc_type,
             svf.fulfill_loc_id,
             svf.partial_delivery_ind,
             svf.delivery_type,
             svf.carrier_code,
             svf.carrier_service_code,
             svf.consumer_delivery_date,
             svf.consumer_delivery_time,
             svf.delivery_charges,
             svf.delivery_charges_curr,
             svf.comments,
             sfo.customer_no,
             sfo.deliver_first_name,
             sfo.deliver_phonetic_first,
             sfo.deliver_last_name,
             sfo.deliver_phonetic_last,
             sfo.deliver_preferred_name,
             sfo.deliver_company_name,
             sfo.deliver_add1,
             sfo.deliver_add2,
             sfo.deliver_add3,
             sfo.deliver_county,
             sfo.deliver_city,
             sfo.deliver_state,
             sfo.deliver_country_id,
             sfo.deliver_post,
             sfo.deliver_jurisdiction,
             sfo.deliver_phone,
             sfo.bill_first_name,
             sfo.bill_phonetic_first,
             sfo.bill_last_name,
             sfo.bill_phonetic_last,
             sfo.bill_preferred_name,
             sfo.bill_company_name,
             sfo.bill_add1,
             sfo.bill_add2,
             sfo.bill_add3,
             sfo.bill_county,
             sfo.bill_city,
             sfo.bill_state,
             sfo.bill_country_id,
             sfo.bill_post,
             sfo.bill_jurisdiction,
             sfo.bill_phone
        from svc_fulfilord svf,
             svc_fulfilordcust sfo
       where svf.process_id = I_process_id
         and svf.chunk_id = I_chunk_id
         and svf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and svf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
         and svf.process_id = sfo.process_id
         and svf.chunk_id = sfo.chunk_id
         and svf.fulfilord_id = sfo.fulfilord_id
       order by 1,2,6;

   cursor C_GET_HDR_REC_REJ is
      select svf.process_id,
             svf.chunk_id,
             svf.process_status,
             svf.action_type,
             svf.tran_type,
             svf.fulfilord_id,
             svf.customer_order_no,
             svf.fulfill_order_no,
             svf.source_loc_type,
             svf.source_loc_id,
             svf.fulfill_loc_type,
             svf.fulfill_loc_id,
             svf.partial_delivery_ind,
             svf.delivery_type,
             svf.carrier_code,
             svf.carrier_service_code,
             svf.consumer_delivery_date,
             svf.consumer_delivery_time,
             svf.delivery_charges,
             svf.delivery_charges_curr,
             svf.comments,
             sfo.customer_no,
             sfo.deliver_first_name,
             sfo.deliver_phonetic_first,
             sfo.deliver_last_name,
             sfo.deliver_phonetic_last,
             sfo.deliver_preferred_name,
             sfo.deliver_company_name,
             sfo.deliver_add1,
             sfo.deliver_add2,
             sfo.deliver_add3,
             sfo.deliver_county,
             sfo.deliver_city,
             sfo.deliver_state,
             sfo.deliver_country_id,
             sfo.deliver_post,
             sfo.deliver_jurisdiction,
             sfo.deliver_phone,
             sfo.bill_first_name,
             sfo.bill_phonetic_first,
             sfo.bill_last_name,
             sfo.bill_phonetic_last,
             sfo.bill_preferred_name,
             sfo.bill_company_name,
             sfo.bill_add1,
             sfo.bill_add2,
             sfo.bill_add3,
             sfo.bill_county,
             sfo.bill_city,
             sfo.bill_state,
             sfo.bill_country_id,
             sfo.bill_post,
             sfo.bill_jurisdiction,
             sfo.bill_phone
        from svc_fulfilord svf,
             svc_fulfilordcust sfo
       where svf.process_id = I_process_id
         and svf.chunk_id = I_chunk_id
         and svf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_INACTIVE
         and svf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
         and svf.process_id = sfo.process_id
         and svf.chunk_id = sfo.chunk_id
         and svf.fulfilord_id = sfo.fulfilord_id
       order by 1,2,6;

   cursor C_GET_SVC_FUL_DTL is
      select process_id,
             chunk_id,
             fulfilord_id,
             item_status,
             item,
             ref_item,
             order_qty_suom,
             standard_uom,
             transaction_uom,
             substitute_ind,
             unit_retail,
             retail_curr,
             available_qty,
             error_msg
        from svc_fulfilorddtl
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and fulfilord_id = L_fulfilord_id
         and item_status is NULL;

   cursor C_GET_PRIM_ORIG_CTY is
      select origin_country_id
        from item_supp_country
       where item = L_item
         and supplier = L_supplier
         and primary_country_ind = 'Y';

   cursor C_GET_COUNTRY_LOC is
      select country_id
        from addr
       where key_value_1 = to_char(L_loc)
         and addr_type = '01'
         and primary_addr_ind = 'Y';

   cursor C_GET_IMP is
      select import_id,
             import_type
        from sups_imp_exp
       where supplier = L_supplier
         and default_ind = 'Y'
         and import_type = 'M';

   cursor C_GET_ROUTING_LOC is
      select routing_loc_id
        from sups_routing_loc
       where supplier = L_supplier;

   cursor C_GET_UNIT_COST_LOC (p_loc SVC_FULFILORD.FULFILL_LOC_ID%TYPE) is
      select unit_cost
        from item_supp_country_loc
       where item = L_item
         and supplier = L_supplier
         and loc = p_loc;

   cursor C_GET_UNIT_COST is
      select unit_cost
        from item_supp_country
       where item = L_item
         and supplier = L_supplier;

   cursor C_GET_CONTAINER_ITEM is
      select distinct(i.container_item) container_item,
             sum(sd.order_qty_suom) order_qty_suom
        from svc_fulfilorddtl sd,
             item_master i
       where sd.process_id = I_process_id
         and sd.chunk_id = I_chunk_id
         and sd.fulfilord_id = L_fulfilord_id
         and sd.item_status is NULL
         and sd.item = i.item
         and EXISTS (select 'x'
                       from item_master im
                      where im.item = sd.item
                        and im.deposit_item_type = 'E')
       group by container_item;

   cursor C_CHK_IF_FRANCHISE_STORE is
      select decode(st.store_type, 'F', 'Y', 'N')
        from store st
       where st.store = L_loc;

   cursor C_GET_COSTING_LOC is
      select costing_loc
        from item_loc
       where item = L_item
         and loc = L_loc;

   TYPE SVC_REC is TABLE OF C_GET_HDR_REC%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE SVC_DTL is TABLE OF C_GET_SVC_FUL_DTL%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE CON_REC is TABLE OF C_GET_CONTAINER_ITEM%ROWTYPE INDEX BY BINARY_INTEGER;

   L_svc_hdr_rec                SVC_REC;
   L_svc_hdr_rec_rej            SVC_REC;
   L_svc_ful_dtl                SVC_DTL;
   L_container_rec              CON_REC;

BEGIN

   if I_process_id is NULL then
      L_invalid_param := 'I_process_id';
   elsif I_chunk_id is NULL then
      L_invalid_param := 'I_chunk_id';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   open C_GET_HDR_REC;
   fetch C_GET_HDR_REC BULK COLLECT into L_svc_hdr_rec;
   close C_GET_HDR_REC;

   if L_svc_hdr_rec is NOT NULL and L_svc_hdr_rec.COUNT > 0 then

      LP_ordhead                := ORDHEAD_TBL();
      LP_ordcust_hdr            := ORDCUST_HEADER_TBL();
      LP_ordcust_dtl            := ORDCUST_DETAIL_TBL();
      LP_ordsku                 := ORDSKU_TBL();
      LP_ordloc                 := ORDLOC_TBL();
      LP_ordcust_l10n_ext_tab   := L10N_ORDCUST_EXT_TBL();
      LP_new_item_loc           := ITEM_LOC_TBL();
      O_ordcust_ids             := ID_TBL();

      FOR rec in L_svc_hdr_rec.FIRST..L_svc_hdr_rec.LAST LOOP

         L_order_no := NULL;
         if ORDER_NUMBER_SQL.NEXT_ORDER_NUMBER(O_error_message,
                                               L_order_no) = FALSE then
            return FALSE;
         end if;

         O_ordcust_ids.EXTEND();
         O_ordcust_ids(O_ordcust_ids.COUNT) := ordcust_seq.nextval;

         L_supplier     := L_svc_hdr_rec(rec).source_loc_id;

         if SUPP_ATTRIB_SQL.ORDER_SUP_DETAILS (O_error_message,
                                               L_currency_code ,
                                               L_terms         ,
                                               L_freight_terms ,
                                               L_sup_status    ,
                                               L_qc_ind        ,
                                               L_edi_po_ind    ,
                                               L_pre_mark_ind  ,
                                               L_ship_method   ,
                                               L_payment_method,
                                               L_supplier) = FALSE then
            return FALSE;
         end if;

         if CURRENCY_SQL.GET_RATE(O_error_message,
                                  L_exchange_rate,
                                  L_currency_code,
                                  'P',
                                  LP_vdate) = FALSE then
            return FALSE;
         end if;

         if L_svc_hdr_rec(rec).consumer_delivery_date is NULL then
            L_delivery_date := LP_vdate;
         else
            L_delivery_date := L_svc_hdr_rec(rec).consumer_delivery_date;
         end if;

         if DATES_SQL.GET_EOW_DATE(O_error_message,
                                   L_otb_eow_date,
                                   L_delivery_date) = FALSE then
            return FALSE;
         end if;

         L_fulfilord_id := L_svc_hdr_rec(rec).fulfilord_id;
         L_loc          := L_svc_hdr_rec(rec).fulfill_loc_id;
         L_loc_type     := L_svc_hdr_rec(rec).fulfill_loc_type;

         L_latest_ship_date := L_delivery_date + LP_so_ship_days;

         --Building an order header record
         LP_ordhead.EXTEND();

         LP_ordhead(LP_ordhead.COUNT).order_no               := L_order_no;
         LP_ordhead(LP_ordhead.COUNT).order_type             := 'CO';
         LP_ordhead(LP_ordhead.COUNT).dept                   := NULL;
         LP_ordhead(LP_ordhead.COUNT).buyer                  := NULL;
         LP_ordhead(LP_ordhead.COUNT).supplier               := L_supplier;
         LP_ordhead(LP_ordhead.COUNT).supp_add_seq_no        := 1;
         LP_ordhead(LP_ordhead.COUNT).loc_type               := 'S';
         LP_ordhead(LP_ordhead.COUNT).location               := L_loc;
         LP_ordhead(LP_ordhead.COUNT).promotion              := NULL;
         LP_ordhead(LP_ordhead.COUNT).qc_ind                 := L_qc_ind;
         LP_ordhead(LP_ordhead.COUNT).written_date           := LP_vdate;
         LP_ordhead(LP_ordhead.COUNT).not_before_date        := L_delivery_date;
         LP_ordhead(LP_ordhead.COUNT).not_after_date         := L_delivery_date;
         LP_ordhead(LP_ordhead.COUNT).otb_eow_date           := L_otb_eow_date;
         LP_ordhead(LP_ordhead.COUNT).earliest_ship_date     := L_delivery_date;
         LP_ordhead(LP_ordhead.COUNT).latest_ship_date       := L_latest_ship_date;
         LP_ordhead(LP_ordhead.COUNT).close_date             := NULL;
         LP_ordhead(LP_ordhead.COUNT).terms                  := L_terms;
         LP_ordhead(LP_ordhead.COUNT).freight_terms          := L_freight_terms;
         LP_ordhead(LP_ordhead.COUNT).orig_ind               := RMSSUB_XORDER_DEFAULT.DEFAULT_ORIG_IND;
         LP_ordhead(LP_ordhead.COUNT).payment_method         := L_payment_method;
         LP_ordhead(LP_ordhead.COUNT).backhaul_type          := NULL;
         LP_ordhead(LP_ordhead.COUNT).backhaul_allowance     := NULL;
         LP_ordhead(LP_ordhead.COUNT).ship_method            := L_ship_method;
         LP_ordhead(LP_ordhead.COUNT).purchase_type          := NULL;
         LP_ordhead(LP_ordhead.COUNT).status                 := 'A';
         LP_ordhead(LP_ordhead.COUNT).orig_approval_date     := LP_vdate;
         LP_ordhead(LP_ordhead.COUNT).orig_approval_id       := LP_user;
         LP_ordhead(LP_ordhead.COUNT).ship_pay_method        := NULL;
         LP_ordhead(LP_ordhead.COUNT).fob_trans_res          := NULL;
         LP_ordhead(LP_ordhead.COUNT).fob_trans_res_desc     := NULL;
         LP_ordhead(LP_ordhead.COUNT).fob_title_pass         := NULL;
         LP_ordhead(LP_ordhead.COUNT).fob_title_pass_desc    := NULL;
         LP_ordhead(LP_ordhead.COUNT).edi_sent_ind           := 'N';
         LP_ordhead(LP_ordhead.COUNT).edi_po_ind             := L_edi_po_ind;
         LP_ordhead(LP_ordhead.COUNT).import_order_ind       := NULL;
         LP_ordhead(LP_ordhead.COUNT).import_country_id      := NULL;
         LP_ordhead(LP_ordhead.COUNT).po_ack_recvd_ind       := 'N';
         LP_ordhead(LP_ordhead.COUNT).include_on_order_ind   := 'N';
         LP_ordhead(LP_ordhead.COUNT).vendor_order_no        := NULL;
         LP_ordhead(LP_ordhead.COUNT).exchange_rate          := L_exchange_rate;
         LP_ordhead(LP_ordhead.COUNT).factory                := NULL;
         LP_ordhead(LP_ordhead.COUNT).agent                  := NULL;
         LP_ordhead(LP_ordhead.COUNT).discharge_port         := NULL;
         LP_ordhead(LP_ordhead.COUNT).lading_port            := NULL;
         LP_ordhead(LP_ordhead.COUNT).freight_contract_no    := NULL;
         LP_ordhead(LP_ordhead.COUNT).po_type                := NULL;
         LP_ordhead(LP_ordhead.COUNT).pre_mark_ind           := L_pre_mark_ind;
         LP_ordhead(LP_ordhead.COUNT).currency_code          := L_currency_code;
         LP_ordhead(LP_ordhead.COUNT).reject_code            := NULL;
         LP_ordhead(LP_ordhead.COUNT).contract_no            := NULL;
         LP_ordhead(LP_ordhead.COUNT).last_sent_rev_no       := NULL;
         LP_ordhead(LP_ordhead.COUNT).split_ref_ordno        := NULL;
         LP_ordhead(LP_ordhead.COUNT).pickup_loc             := NULL;
         LP_ordhead(LP_ordhead.COUNT).pickup_no              := NULL;
         LP_ordhead(LP_ordhead.COUNT).pickup_date            := LP_vdate;
         LP_ordhead(LP_ordhead.COUNT).app_datetime           := NULL;
         LP_ordhead(LP_ordhead.COUNT).comment_desc           := NULL;
         LP_ordhead(LP_ordhead.COUNT).partner_type_1         := NULL;
         LP_ordhead(LP_ordhead.COUNT).partner1               := NULL;
         LP_ordhead(LP_ordhead.COUNT).partner_type_2         := NULL;
         LP_ordhead(LP_ordhead.COUNT).partner2               := NULL;
         LP_ordhead(LP_ordhead.COUNT).partner_type_3         := NULL;
         LP_ordhead(LP_ordhead.COUNT).partner3               := NULL;
         LP_ordhead(LP_ordhead.COUNT).item                   := NULL;
         LP_ordhead(LP_ordhead.COUNT).import_id              := NULL;
         LP_ordhead(LP_ordhead.COUNT).import_type            := NULL;
         LP_ordhead(LP_ordhead.COUNT).routing_loc_id         := NULL;
         LP_ordhead(LP_ordhead.COUNT).clearing_zone_id       := NULL;
         LP_ordhead(LP_ordhead.COUNT).delivery_supplier      := NULL;
         LP_ordhead(LP_ordhead.COUNT).triangulation_ind      := 'N';
         LP_ordhead(LP_ordhead.COUNT).create_id              := LP_user;
         LP_ordhead(LP_ordhead.COUNT).create_datetime        := SYSDATE;
         LP_ordhead(LP_ordhead.COUNT).last_update_id         := LP_user;
         LP_ordhead(LP_ordhead.COUNT).last_update_datetime   := SYSDATE;
 
         if LP_elc_ind = 'Y' then
            if SUPP_ATTRIB_SQL.ORDER_IMPORT_DETAILS(O_error_message,
                                                    LP_ordhead(LP_ordhead.COUNT).agent,
                                                    LP_ordhead(LP_ordhead.COUNT).lading_port,
                                                    LP_ordhead(LP_ordhead.COUNT).discharge_port,
                                                    LP_ordhead(LP_ordhead.COUNT).factory,
                                                    LP_ordhead(LP_ordhead.COUNT).partner_type_1,
                                                    LP_ordhead(LP_ordhead.COUNT).partner1,
                                                    LP_ordhead(LP_ordhead.COUNT).partner_type_2,
                                                    LP_ordhead(LP_ordhead.COUNT).partner2,
                                                    LP_ordhead(LP_ordhead.COUNT).partner_type_3,
                                                    LP_ordhead(LP_ordhead.COUNT).partner3,
                                                    L_supplier) = FALSE then
               return FALSE;
            end if;
         end if;

         -- Building ordcust header record

         LP_ordcust_hdr.EXTEND();
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).ordcust_no                 := O_ordcust_ids(O_ordcust_ids.COUNT);
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).order_no                   := L_order_no;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).tsf_no                     := NULL;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).source_loc_type            := L_svc_hdr_rec(rec).source_loc_type;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).source_loc_id              := L_supplier;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).fulfill_loc_type           := L_loc_type;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).fulfill_loc_id             := L_loc;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).customer_no                := L_svc_hdr_rec(rec).customer_no;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).customer_order_no          := L_svc_hdr_rec(rec).customer_order_no;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).fulfill_order_no           := L_svc_hdr_rec(rec).fulfill_order_no;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).partial_delivery_ind       := 'N';
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).delivery_type              := L_svc_hdr_rec(rec).delivery_type;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).carrier_code               := NULL;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).carrier_service_code       := NULL;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).consumer_delivery_date     := L_svc_hdr_rec(rec).consumer_delivery_date;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).consumer_delivery_time     := L_svc_hdr_rec(rec).consumer_delivery_time;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_first_name            := L_svc_hdr_rec(rec).bill_first_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_phonetic_first        := L_svc_hdr_rec(rec).bill_phonetic_first;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_last_name             := L_svc_hdr_rec(rec).bill_last_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_phonetic_last         := L_svc_hdr_rec(rec).bill_phonetic_last;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_preferred_name        := L_svc_hdr_rec(rec).bill_preferred_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_company_name          := L_svc_hdr_rec(rec).bill_company_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_add1                  := L_svc_hdr_rec(rec).bill_add1;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_add2                  := L_svc_hdr_rec(rec).bill_add2;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_add3                  := L_svc_hdr_rec(rec).bill_add3;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_county                := L_svc_hdr_rec(rec).bill_county;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_city                  := L_svc_hdr_rec(rec).bill_city;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_state                 := L_svc_hdr_rec(rec).bill_state;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_country_id            := L_svc_hdr_rec(rec).bill_country_id;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_post                  := L_svc_hdr_rec(rec).bill_post;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_jurisdiction          := L_svc_hdr_rec(rec).bill_jurisdiction;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_phone                 := L_svc_hdr_rec(rec).bill_phone;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_first_name         := L_svc_hdr_rec(rec).deliver_first_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_phonetic_first     := L_svc_hdr_rec(rec).deliver_phonetic_first;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_last_name          := L_svc_hdr_rec(rec).deliver_last_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_phonetic_last      := L_svc_hdr_rec(rec).deliver_phonetic_last;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_preferred_name     := L_svc_hdr_rec(rec).deliver_preferred_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_company_name       := L_svc_hdr_rec(rec).deliver_company_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_add1               := L_svc_hdr_rec(rec).deliver_add1;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_add2               := L_svc_hdr_rec(rec).deliver_add2;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_add3               := L_svc_hdr_rec(rec).deliver_add3;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_city               := L_svc_hdr_rec(rec).deliver_city;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_state              := L_svc_hdr_rec(rec).deliver_state;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_country_id         := L_svc_hdr_rec(rec).deliver_country_id;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_post               := L_svc_hdr_rec(rec).deliver_post;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_county             := L_svc_hdr_rec(rec).deliver_county;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_jurisdiction       := L_svc_hdr_rec(rec).deliver_jurisdiction;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_phone              := L_svc_hdr_rec(rec).deliver_phone;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_charge             := L_svc_hdr_rec(rec).delivery_charges;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_charge_curr        := L_svc_hdr_rec(rec).delivery_charges_curr;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).comments                   := L_svc_hdr_rec(rec).comments;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).create_datetime            := SYSDATE;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).create_id                  := LP_user;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).last_update_datetime       := SYSDATE;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).last_update_id             := LP_user;
         L_ordcust_ind := 'Y';
         if LP_chk_status is NOT NULL and LP_chk_status.COUNT > 0 then
            FOR rec in LP_chk_status.FIRST..LP_chk_status.LAST LOOP
               if LP_chk_status(rec).fulfilord_id = L_fulfilord_id then
                  LP_ordcust_hdr(LP_ordcust_hdr.COUNT).status   := LP_chk_status(rec).status;
               end if;
            END LOOP;
         end if;


         LP_ordcust_l10n_ext_tab.EXTEND();
         LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT)                 := L10N_ORDCUST_EXT_REC();
         LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).procedure_key   := 'CREATE_ORDCUST_L10N_EXT';
         LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).ordcust_no      := O_ordcust_ids(O_ordcust_ids.COUNT);
         LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).process_id      := I_process_id;
         LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).chunk_id        := I_chunk_id;
         LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).reference_id    := L_fulfilord_id;
         LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).source_entity   := 'SUPP';
         LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).source_id       := L_supplier;
         LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).doc_type        := 'PO';
         LP_ordcust_l10n_ext_tab(LP_ordcust_l10n_ext_tab.COUNT).doc_id          := L_order_no;
         open C_GET_SVC_FUL_DTL;
         fetch C_GET_SVC_FUL_DTL BULK COLLECT into L_svc_ful_dtl;
         close C_GET_SVC_FUL_DTL;

         LP_import_item_ind := 'N';

         open C_CHK_IF_FRANCHISE_STORE;
         fetch C_CHK_IF_FRANCHISE_STORE into L_franchise_po;
         close C_CHK_IF_FRANCHISE_STORE;

         if L_svc_ful_dtl is NOT NULL and L_svc_ful_dtl.COUNT > 0 then
            FOR dtl_rec in L_svc_ful_dtl.FIRST..L_svc_ful_dtl.LAST LOOP
               L_item := L_svc_ful_dtl(dtl_rec).item;

               L_origin_cty := NULL;
               open C_GET_PRIM_ORIG_CTY;
               fetch C_GET_PRIM_ORIG_CTY into L_origin_cty ;
               close C_GET_PRIM_ORIG_CTY;

               if LP_import_item_ind != 'Y' then

                  L_loc_id := NULL;
                  open C_GET_COUNTRY_LOC;
                  fetch C_GET_COUNTRY_LOC into L_loc_id;
                  close C_GET_COUNTRY_LOC;

                  if L_loc_id is NULL then
                     L_loc_id := LP_base_country_id;
                  end if;

                  if L_origin_cty != L_loc_id then
                     LP_import_item_ind := 'Y';
                     L_import_id       := NULL;
                     L_import_type     := NULL;

                     open C_GET_IMP;
                     fetch C_GET_IMP into L_import_id,
                                          L_import_type;
                     close C_GET_IMP;

                     L_routing_loc_id := NULL;
                     open C_GET_ROUTING_LOC;
                     fetch C_GET_ROUTING_LOC into L_routing_loc_id;
                     close C_GET_ROUTING_LOC;

                     LP_ordhead(LP_ordhead.COUNT).import_order_ind    := 'Y';
                     LP_ordhead(LP_ordhead.COUNT).import_country_id   := L_loc_id;
                     LP_ordhead(LP_ordhead.COUNT).import_id           := L_import_id;
                     LP_ordhead(LP_ordhead.COUNT).import_type         := L_import_type;
                     LP_ordhead(LP_ordhead.COUNT).routing_loc_id      := L_routing_loc_id;

                     if OUTSIDE_LOCATION_SQL.GET_PRIM_CLEAR_ZONE_IMP_CTRY(O_error_message,
                                                                          L_exists,
                                                                          L_outloc_id,
                                                                          L_outloc_desc,
                                                                          L_loc_id) = FALSE then
                         return FALSE;
                     end if;

                     if L_exists then
                        LP_ordhead(LP_ordhead.COUNT).clearing_zone_id   := L_outloc_id;
                     else
                        LP_ordhead(LP_ordhead.COUNT).clearing_zone_id   := NULL;
                     end if;
                  else
                     LP_ordhead(LP_ordhead.COUNT).import_order_ind    := 'N';
                     LP_ordhead(LP_ordhead.COUNT).import_country_id   := LP_base_country_id;
                  end if;
               end if;

               L_unit_cost := NULL;

               --get the costing loc as import id for franchise stores.
               --Only one fetch is necessary since costing loc is same through all items corresponding
               --in svc_fulfilorddtl for franchise stores's svc_fulfilord.fulfill_loc_id.
               if L_franchise_po = 'Y' then
                  if dtl_rec = 1 then
                     open C_GET_COSTING_LOC;
                     fetch C_GET_COSTING_LOC into L_costing_loc;
                     close C_GET_COSTING_LOC;
                  end if;
                  LP_ordhead(LP_ordhead.COUNT).import_id   := L_costing_loc;
                  LP_ordhead(LP_ordhead.COUNT).import_type := 'F';

                  open C_GET_UNIT_COST_LOC (L_costing_loc);
                  fetch C_GET_UNIT_COST_LOC into L_unit_cost;
                  close C_GET_UNIT_COST_LOC;
               else
                  open C_GET_UNIT_COST_LOC (L_loc);
                  fetch C_GET_UNIT_COST_LOC into L_unit_cost;
                  close C_GET_UNIT_COST_LOC;
               end if;

               if L_unit_cost is NULL then
                  open C_GET_UNIT_COST;
                  fetch C_GET_UNIT_COST into L_unit_cost;
                  close C_GET_UNIT_COST;
               end if;

               -- Building ordcust detail record
               LP_ordcust_dtl.EXTEND();
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).ordcust_no             := O_ordcust_ids(O_ordcust_ids.COUNT);
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).item                   := L_item;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).ref_item               := L_svc_ful_dtl(dtl_rec).ref_item;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).qty_ordered_suom       := L_svc_ful_dtl(dtl_rec).order_qty_suom;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).standard_uom           := L_svc_ful_dtl(dtl_rec).standard_uom;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).transaction_uom        := L_svc_ful_dtl(dtl_rec).transaction_uom;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).substitute_allowed_ind := 'N';
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).unit_retail            := L_svc_ful_dtl(dtl_rec).unit_retail;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).retail_currency_code   := L_svc_ful_dtl(dtl_rec).retail_curr;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).create_id              := LP_user;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).create_datetime        := SYSDATE;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).last_update_id         := LP_user;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).last_update_datetime   := SYSDATE;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).original_item          := NULL;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).qty_cancelled_suom     := NULL;
               LP_ordcust_dtl(LP_ordcust_dtl.COUNT).comments               := NULL;

               -- Building order detail record
               LP_ordsku.EXTEND();
               LP_ordsku(LP_ordsku.COUNT).order_no                         := L_order_no;
               LP_ordsku(LP_ordsku.COUNT).item                             := L_item;
               LP_ordsku(LP_ordsku.COUNT).ref_item                         := L_svc_ful_dtl(dtl_rec).ref_item;
               LP_ordsku(LP_ordsku.COUNT).origin_country_id                := L_origin_cty;
               LP_ordsku(LP_ordsku.COUNT).earliest_ship_date               := L_delivery_date;
               LP_ordsku(LP_ordsku.COUNT).latest_ship_date                 := L_latest_ship_date;
               LP_ordsku(LP_ordsku.COUNT).supp_pack_size                   := L_supp_pack_size;
               LP_ordsku(LP_ordsku.COUNT).non_scale_ind                    := 'N';
               LP_ordsku(LP_ordsku.COUNT).pickup_loc                       := NULL;
               LP_ordsku(LP_ordsku.COUNT).pickup_no                        := NULL;

               -- Building ordloc detail record
               LP_ordloc.EXTEND();
               LP_ordloc(LP_ordloc.COUNT).order_no                         := L_order_no;
               LP_ordloc(LP_ordloc.COUNT).item                             := L_item;
               LP_ordloc(LP_ordloc.COUNT).location                         := L_loc;
               LP_ordloc(LP_ordloc.COUNT).loc_type                         := 'S';
               LP_ordloc(LP_ordloc.COUNT).unit_retail                      := 0;
               LP_ordloc(LP_ordloc.COUNT).qty_ordered                      := L_svc_ful_dtl(dtl_rec).order_qty_suom;
               LP_ordloc(LP_ordloc.COUNT).qty_prescaled                    := L_svc_ful_dtl(dtl_rec).order_qty_suom;
               LP_ordloc(LP_ordloc.COUNT).unit_cost                        := L_unit_cost;
               LP_ordloc(LP_ordloc.COUNT).unit_cost_init                   := L_unit_cost;
               LP_ordloc(LP_ordloc.COUNT).cost_source                      := 'NORM';
               LP_ordloc(LP_ordloc.COUNT).non_scale_ind                    := 'N';
               LP_ordloc(LP_ordloc.COUNT).qty_received                     := NULL;
               LP_ordloc(LP_ordloc.COUNT).last_received                    := NULL;
               LP_ordloc(LP_ordloc.COUNT).last_rounded_qty                 := L_svc_ful_dtl(dtl_rec).order_qty_suom;
               LP_ordloc(LP_ordloc.COUNT).last_grp_rounded_qty             := L_svc_ful_dtl(dtl_rec).order_qty_suom;
               LP_ordloc(LP_ordloc.COUNT).qty_cancelled                    := NULL;
               LP_ordloc(LP_ordloc.COUNT).cancel_code                      := NULL;
               LP_ordloc(LP_ordloc.COUNT).cancel_date                      := NULL;
               LP_ordloc(LP_ordloc.COUNT).cancel_id                        := NULL;
               LP_ordloc(LP_ordloc.COUNT).original_repl_qty                := NULL;
               LP_ordloc(LP_ordloc.COUNT).tsf_po_link_no                   := NULL;

               -- Get all item/fulfill location.
               LP_new_item_loc.EXTEND();
               LP_new_item_loc(LP_new_item_loc.COUNT).item       := L_item;
               LP_new_item_loc(LP_new_item_loc.COUNT).loc        := L_loc;
            END LOOP;
         end if; -- end of detail record

         -- Get all container item record
         open C_GET_CONTAINER_ITEM;
         fetch C_GET_CONTAINER_ITEM BULK COLLECT into L_container_rec;
         close C_GET_CONTAINER_ITEM;

         if L_container_rec is NOT NULL and L_container_rec.COUNT > 0 then
            FOR dtl_rec in L_container_rec.FIRST..L_container_rec.LAST LOOP
               L_item := L_container_rec(dtl_rec).container_item;
               L_origin_cty := NULL;
               open C_GET_PRIM_ORIG_CTY;
               fetch C_GET_PRIM_ORIG_CTY into L_origin_cty;
               close C_GET_PRIM_ORIG_CTY;
               -- Building order detail record
               LP_ordsku.EXTEND();
               LP_ordsku(LP_ordsku.COUNT).order_no                         := L_order_no;
               LP_ordsku(LP_ordsku.COUNT).item                             := L_item;
               LP_ordsku(LP_ordsku.COUNT).ref_item                         := NULL;
               LP_ordsku(LP_ordsku.COUNT).origin_country_id                := L_origin_cty;
               LP_ordsku(LP_ordsku.COUNT).earliest_ship_date               := L_delivery_date;
               LP_ordsku(LP_ordsku.COUNT).latest_ship_date                 := L_latest_ship_date;
               LP_ordsku(LP_ordsku.COUNT).supp_pack_size                   := L_supp_pack_size;
               LP_ordsku(LP_ordsku.COUNT).non_scale_ind                    := 'N';
               LP_ordsku(LP_ordsku.COUNT).pickup_loc                       := NULL;
               LP_ordsku(LP_ordsku.COUNT).pickup_no                        := NULL;

               L_unit_cost := NULL;
               if L_franchise_po = 'Y' then
                  open C_GET_UNIT_COST_LOC (L_costing_loc);
                  fetch C_GET_UNIT_COST_LOC into L_unit_cost;
                  close C_GET_UNIT_COST_LOC;
               else
                  open C_GET_UNIT_COST_LOC (L_loc);
                  fetch C_GET_UNIT_COST_LOC into L_unit_cost;
                  close C_GET_UNIT_COST_LOC;
               end if;

               if L_unit_cost is NULL then
                  open C_GET_UNIT_COST;
                  fetch C_GET_UNIT_COST into L_unit_cost;
                  close C_GET_UNIT_COST;
               end if;

               -- Building ordloc detail record
               LP_ordloc.EXTEND();
               LP_ordloc(LP_ordloc.COUNT).order_no                         := L_order_no;
               LP_ordloc(LP_ordloc.COUNT).item                             := L_item;
               LP_ordloc(LP_ordloc.COUNT).location                         := L_loc;
               LP_ordloc(LP_ordloc.COUNT).loc_type                         := 'S';
               LP_ordloc(LP_ordloc.COUNT).unit_retail                      := 0;
               LP_ordloc(LP_ordloc.COUNT).qty_ordered                      := L_container_rec(dtl_rec).order_qty_suom;
               LP_ordloc(LP_ordloc.COUNT).qty_prescaled                    := L_container_rec(dtl_rec).order_qty_suom;
               LP_ordloc(LP_ordloc.COUNT).unit_cost                        := L_unit_cost;
               LP_ordloc(LP_ordloc.COUNT).unit_cost_init                   := L_unit_cost;
               LP_ordloc(LP_ordloc.COUNT).cost_source                      := 'NORM';
               LP_ordloc(LP_ordloc.COUNT).non_scale_ind                    := 'N';
               LP_ordloc(LP_ordloc.COUNT).qty_received                     := NULL;
               LP_ordloc(LP_ordloc.COUNT).last_received                    := NULL;
               LP_ordloc(LP_ordloc.COUNT).last_rounded_qty                 := L_container_rec(dtl_rec).order_qty_suom;
               LP_ordloc(LP_ordloc.COUNT).last_grp_rounded_qty             := L_container_rec(dtl_rec).order_qty_suom;
               LP_ordloc(LP_ordloc.COUNT).qty_cancelled                    := NULL;
               LP_ordloc(LP_ordloc.COUNT).cancel_code                      := NULL;
               LP_ordloc(LP_ordloc.COUNT).cancel_date                      := NULL;
               LP_ordloc(LP_ordloc.COUNT).cancel_id                        := NULL;
               LP_ordloc(LP_ordloc.COUNT).original_repl_qty                := NULL;
               LP_ordloc(LP_ordloc.COUNT).tsf_po_link_no                   := NULL;

               -- Get all item/fulfill location.
               LP_new_item_loc.EXTEND();
               LP_new_item_loc(LP_new_item_loc.COUNT).item       := L_item;
               LP_new_item_loc(LP_new_item_loc.COUNT).loc        := L_loc;
            END LOOP;
         end if; -- end of container detail record
      END LOOP; -- end of header record
   end if; -- end of 'V'alidated record

   --For customer order POs that are fully rejected (all items in the order are invalid).
   open C_GET_HDR_REC_REJ;
   fetch C_GET_HDR_REC_REJ BULK COLLECT into L_svc_hdr_rec_rej;
   close C_GET_HDR_REC_REJ;

   if L_svc_hdr_rec_rej is NOT NULL and L_svc_hdr_rec_rej.COUNT > 0 then
      -- initialize collection LP_ordcust and O_ordcust_ids if this is the first time
      -- the said collections are populated.
      if L_ordcust_ind = 'N' then
         LP_ordcust_hdr  := ORDCUST_HEADER_TBL();
         O_ordcust_ids   := ID_TBL();
      end if;

      FOR i in L_svc_hdr_rec_rej.FIRST..L_svc_hdr_rec_rej.LAST LOOP
         O_ordcust_ids.EXTEND();
         O_ordcust_ids(O_ordcust_ids.COUNT) := ordcust_seq.nextval;
         LP_ordcust_hdr.EXTEND();
         -- Build ordcust header only
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).ordcust_no                 := O_ordcust_ids(O_ordcust_ids.COUNT);
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).status                     := CORESVC_FULFILORD.ORDCUST_STATUS_NOT_CRE;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).order_no                   := NULL;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).tsf_no                     := NULL;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).source_loc_type            := L_svc_hdr_rec_rej(i).source_loc_type;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).source_loc_id              := L_svc_hdr_rec_rej(i).source_loc_id;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).fulfill_loc_type           := L_svc_hdr_rec_rej(i).fulfill_loc_type;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).fulfill_loc_id             := L_svc_hdr_rec_rej(i).fulfill_loc_id;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).customer_no                := L_svc_hdr_rec_rej(i).customer_no;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).customer_order_no          := L_svc_hdr_rec_rej(i).customer_order_no;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).fulfill_order_no           := L_svc_hdr_rec_rej(i).fulfill_order_no;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).partial_delivery_ind       := 'N';
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).delivery_type              := L_svc_hdr_rec_rej(i).delivery_type;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).carrier_code               := NULL;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).carrier_service_code       := NULL;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).consumer_delivery_date     := L_delivery_date;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).consumer_delivery_time     := L_svc_hdr_rec_rej(i).consumer_delivery_time;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_first_name            := L_svc_hdr_rec_rej(i).bill_first_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_phonetic_first        := L_svc_hdr_rec_rej(i).bill_phonetic_first;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_last_name             := L_svc_hdr_rec_rej(i).bill_last_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_phonetic_last         := L_svc_hdr_rec_rej(i).bill_phonetic_last;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_preferred_name        := L_svc_hdr_rec_rej(i).bill_preferred_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_company_name          := L_svc_hdr_rec_rej(i).bill_company_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_add1                  := L_svc_hdr_rec_rej(i).bill_add1;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_add2                  := L_svc_hdr_rec_rej(i).bill_add2;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_add3                  := L_svc_hdr_rec_rej(i).bill_add3;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_county                := L_svc_hdr_rec_rej(i).bill_county;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_city                  := L_svc_hdr_rec_rej(i).bill_city;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_state                 := L_svc_hdr_rec_rej(i).bill_state;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_country_id            := L_svc_hdr_rec_rej(i).bill_country_id;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_post                  := L_svc_hdr_rec_rej(i).bill_post;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_jurisdiction          := L_svc_hdr_rec_rej(i).bill_jurisdiction;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).bill_phone                 := L_svc_hdr_rec_rej(i).bill_phone;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_first_name         := L_svc_hdr_rec_rej(i).deliver_first_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_phonetic_first     := L_svc_hdr_rec_rej(i).deliver_phonetic_first;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_last_name          := L_svc_hdr_rec_rej(i).deliver_last_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_phonetic_last      := L_svc_hdr_rec_rej(i).deliver_phonetic_last;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_preferred_name     := L_svc_hdr_rec_rej(i).deliver_preferred_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_company_name       := L_svc_hdr_rec_rej(i).deliver_company_name;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_add1               := L_svc_hdr_rec_rej(i).deliver_add1;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_add2               := L_svc_hdr_rec_rej(i).deliver_add2;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_add3               := L_svc_hdr_rec_rej(i).deliver_add3;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_city               := L_svc_hdr_rec_rej(i).deliver_city;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_state              := L_svc_hdr_rec_rej(i).deliver_state;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_country_id         := L_svc_hdr_rec_rej(i).deliver_country_id;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_post               := L_svc_hdr_rec_rej(i).deliver_post;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_county             := L_svc_hdr_rec_rej(i).deliver_county;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_jurisdiction       := L_svc_hdr_rec_rej(i).deliver_jurisdiction;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_phone              := L_svc_hdr_rec_rej(i).deliver_phone;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_charge             := L_svc_hdr_rec_rej(i).delivery_charges;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).deliver_charge_curr        := L_svc_hdr_rec_rej(i).delivery_charges_curr;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).comments                   := L_svc_hdr_rec_rej(i).comments;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).create_datetime            := SYSDATE;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).create_id                  := LP_user;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).last_update_datetime       := SYSDATE;
         LP_ordcust_hdr(LP_ordcust_hdr.COUNT).last_update_id             := LP_user;
      END LOOP;
   end if; -- end of 'I'nactive records

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POPULATE_CREATE_REC;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_CREATE_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(50)     := 'CORESVC_FULFILORD_PO.PERSIST_CREATE_PO';
   L_appr_ind                  VARCHAR2(3);
   L_exists                    BOOLEAN;
   L_approved                  VARCHAR2(1);
   L_l10n_obj                  L10N_OBJ          := L10N_OBJ();
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE := ' ';
   L_unit_retail               ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_unit_retail_loc   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_loc           ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_units_loc           ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail_loc     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom_loc     ITEM_LOC.MULTI_SELLING_UOM%TYPE;
   L_estimated_instock_date    ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE;
   L_supplier                  ORDHEAD.SUPPLIER%TYPE;
   L_deposit_item_type         ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE;
   L_item                      ITEM_MASTER.ITEM%TYPE;
   L_total_ord_qty             rounding_sql.total_ord_qty_tabletype;
   L_covered_inbound           BOOLEAN;

   cursor C_GET_DEP_ITEM_TYPE is
      select deposit_item_type
        from item_master
       where item = L_item;

BEGIN

   if LP_new_item_loc is NOT NULL and LP_new_item_loc.COUNT > 0 then
      FOR i in LP_new_item_loc.FIRST..LP_new_item_loc.LAST LOOP
         if NEW_ITEM_LOC (O_error_message,              --O_error_message
                          LP_new_item_loc(i).item,      --I_item
                          LP_new_item_loc(i).loc,       --I_location
                          NULL,                         --I_item_parent
                          NULL,                         --I_item_grandparent
                          'S',                          --I_loc_type
                          NULL,                         --I_short_desc
                          NULL,                         --I_dept
                          NULL,                         --I_class
                          NULL,                         --I_subclass
                          NULL,                         --I_item_level
                          NULL,                         --I_tran_level
                          NULL,                         --I_item_status
                          NULL,                         --I_waste_type
                          NULL,                         --I_daily_waste_pct
                          NULL,                         --I_sellable_ind
                          NULL,                         --I_orderable_ind
                          NULL,                         --I_pack_ind
                          NULL,                         --I_pack_type
                          NULL,                         --I_unit_cost_loc
                          NULL,                         --I_unit_retail_loc
                          NULL,                         --I_selling_retail_loc
                          NULL,                         --I_selling_uom
                          NULL,                         --I_item_loc_status
                          NULL,                         --I_taxable_ind
                          NULL,                         --I_ti
                          NULL,                         --I_hi
                          NULL,                         --I_store_ord_mult
                          NULL,                         --I_meas_of_each
                          NULL,                         --I_meas_of_price
                          NULL,                         --I_uom_of_price
                          NULL,                         --I_primary_variant
                          NULL,                         --I_primary_supp
                          NULL,                         --I_primary_cntry
                          NULL,                         --I_local_item_desc
                          NULL,                         --I_local_short_desc
                          NULL,                         --I_primary_cost_pack
                          NULL,                         --I_receive_as_type
                          NULL,                         --I_date
                          NULL) = FALSE then            --I_default_to_children
            return FALSE;
         end if;
      END LOOP;
   end if;

   if LP_ordhead is NOT NULL and LP_ordhead.COUNT > 0 then
      FORALL i in LP_ordhead.FIRST..LP_ordhead.LAST
         insert into ordhead values LP_ordhead(i);
   end if;

   if LP_ordsku is NOT NULL and LP_ordsku.COUNT > 0 then
      FORALL i in LP_ordsku.FIRST..LP_ordsku.LAST
         insert into ordsku values LP_ordsku(i);
   end if;
   --
   if LP_ordloc is NOT NULL and LP_ordloc.COUNT > 0 then
      FOR i in LP_ordloc.FIRST..LP_ordloc.LAST LOOP
         L_deposit_item_type := NULL;

         open C_GET_DEP_ITEM_TYPE;
         fetch C_GET_DEP_ITEM_TYPE into L_deposit_item_type;
         close C_GET_DEP_ITEM_TYPE;

         L_unit_retail  := 0;
         if L_deposit_item_type is NULL or L_deposit_item_type = 'E' then
            if PRICING_ATTRIB_SQL.GET_RETAIL(O_error_message,
                                             L_unit_retail,
                                             L_selling_unit_retail_loc,
                                             L_selling_uom_loc,
                                             L_multi_units_loc,
                                             L_multi_unit_retail_loc,
                                             L_multi_selling_uom_loc,
                                             LP_ordloc(i).item,
                                             LP_ordloc(i).loc_type,
                                             LP_ordloc(i).location) = FALSE then
                return FALSE;
            end if;
         end if;

         FOR j in LP_ordhead.FIRST..LP_ordhead.LAST LOOP
            if LP_ordhead(j).order_no = LP_ordloc(i).order_no then
               L_supplier := LP_ordhead(j).supplier;
            end if;
         END LOOP;

         -- get estimated_instock_date
         L_estimated_instock_date := NULL;
         if ORDER_INSTOCK_DATE_SQL.GET_INSTOCK_DT(O_error_message,
                                                  L_estimated_instock_date,
                                                  LP_ordloc(i).order_no,
                                                  LP_ordloc(i).item,
                                                  LP_ordloc(i).location,
                                                  L_supplier)= FALSE then
            return FALSE;
         end if;

         insert into ordloc (order_no,
                             item,
                             location,
                             loc_type,
                             unit_retail,
                             qty_ordered,
                             qty_prescaled,
                             qty_received,
                             last_received,
                             last_rounded_qty,
                             last_grp_rounded_qty,
                             qty_cancelled,
                             cancel_code,
                             cancel_date,
                             cancel_id,
                             original_repl_qty,
                             unit_cost,
                             unit_cost_init,
                             cost_source,
                             non_scale_ind,
                             tsf_po_link_no,
                             estimated_instock_date)
                     values (LP_ordloc(i).order_no,
                             LP_ordloc(i).item,
                             LP_ordloc(i).location,
                             LP_ordloc(i).loc_type,
                             L_unit_retail,
                             LP_ordloc(i).qty_ordered,
                             LP_ordloc(i).qty_prescaled,
                             LP_ordloc(i).qty_received,
                             LP_ordloc(i).last_received,
                             LP_ordloc(i).last_rounded_qty,
                             LP_ordloc(i).last_grp_rounded_qty,
                             LP_ordloc(i).qty_cancelled,
                             LP_ordloc(i).cancel_code,
                             LP_ordloc(i).cancel_date,
                             LP_ordloc(i).cancel_id,
                             LP_ordloc(i).original_repl_qty,
                             LP_ordloc(i).unit_cost,
                             LP_ordloc(i).unit_cost_init,
                             LP_ordloc(i).cost_source,
                             LP_ordloc(i).non_scale_ind,
                             LP_ordloc(i).tsf_po_link_no,
                             L_estimated_instock_date);
      END LOOP;
   end if;
   --
   if LP_ordcust_hdr is NOT NULL and LP_ordcust_hdr.COUNT > 0 then
      FORALL i in LP_ordcust_hdr.FIRST..LP_ordcust_hdr.LAST
         insert into ordcust values LP_ordcust_hdr(i);
   end if;
   --
   if LP_ordcust_dtl is NOT NULL and LP_ordcust_dtl.COUNT > 0 then
      FORALL i in LP_ordcust_dtl.FIRST..LP_ordcust_dtl.LAST
         insert into ordcust_detail values LP_ordcust_dtl(i);
   end if;
   --

   if LP_ordloc is NOT NULL and LP_ordloc.COUNT > 0 then
      -- Default elc and hts to the items
      FOR i in LP_ordloc.FIRST..LP_ordloc.LAST LOOP
         if LP_elc_ind = 'Y' then
            if ORDER_EXPENSE_SQL.INSERT_COST_COMP(O_error_message,
                                                  LP_ordloc(i).order_no,
                                                  LP_ordloc(i).item,
                                                  NULL,
                                                  NULL,
                                                  NULL) = FALSE then
               return FALSE;
            end if;

            if LP_import_ind = 'Y' and LP_import_item_ind = 'Y' then
               if ORDER_HTS_SQL.DEFAULT_CALC_HTS (O_error_message,
                                                  LP_ordloc(i).order_no,
                                                  LP_ordloc(i).item,
                                                  NULL) = FALSE then
                  return FALSE;
               end if;
            end if;

            if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                      'PE',
                                      LP_ordloc(i).item,
                                      NULL,
                                      NULL,
                                      NULL,
                                      LP_ordloc(i).order_no,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL) = FALSE then
               return FALSE;
            end if;
         end if;

         --round order qty
         L_total_ord_qty(i).item            := LP_ordloc(i).item;
         L_total_ord_qty(i).location        := LP_ordloc(i).location;
         L_total_ord_qty(i).total_order_qty := LP_ordloc(i).qty_ordered;
         if ROUNDING_SQL.ORDER_INTERFACE(O_error_message,
                                         L_covered_inbound,
                                         LP_ordloc(i).order_no,
                                         L_supplier,
                                         L_total_ord_qty,
                                         'N',
                                         'N') = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;

   if LP_ordhead is NOT NULL and LP_ordhead.COUNT > 0 then
      FOR i in LP_ordhead.FIRST..LP_ordhead.LAST LOOP

         if ORDER_STATUS_SQL.INSERT_ORDER_REVISION(O_error_message,
                                                   LP_ordhead(i).order_no,
                                                   'A') = FALSE then
            return FALSE;
         end if;

         if ORDER_DIST_SQL.REDIST_ORDER(O_error_message,
                                        LP_ordhead(i).order_no) = FALSE then
            return FALSE;
         end if;

         -- Insert records into the ORDHEAD_L10N_EXT table for utilization code of Drop Ship
         -- and Multi-site POs
         L_l10n_obj.doc_type      := 'PO';
         L_l10n_obj.procedure_key := 'CREATE_TRAN_UTIL_CODE';
         L_l10n_obj.doc_id        := LP_ordhead(i).order_no;
         L_l10n_obj.country_id    := LP_ordhead(i).import_country_id;

         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_obj) = FALSE then
            return FALSE;
         end if;

         if DEAL_VALIDATE_SQL.DEAL_CALC_QUEUE_EXIST(O_error_message,
                                                    L_exists,
                                                    L_appr_ind,
                                                    LP_ordhead(i).order_no) = FALSE then
            return FALSE;
         end if;

         if NOT L_exists then
            L_approved := 'N';
         else
            L_approved := L_appr_ind;
         end if;

         /*Insert the order in the deal queue, and the deal batch will process it in the nightly batch run.*/
         if DEAL_SQL.INSERT_DEAL_CALC_QUEUE(O_error_message,
                                            LP_ordhead(i).order_no,
                                            'N', -- recalc_ind
                                            'N', -- override_manual_ind
                                            L_approved) = FALSE then
            return FALSE;
         end if;

         /*Create a franchise order*/
         if RMSSUB_XORDER_SQL.CREATE_F_ORDER(O_error_message,
                                             LP_ordhead(i).order_no,
                                             LP_ordhead(i).status) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;

   if LP_ordcust_l10n_ext_tab is NOT NULL and LP_ordcust_l10n_ext_tab.COUNT > 0 then
      if ORDCUST_ATTRIB_SQL.PERSIST_ORDCUST_L10N_EXT(O_error_message,
                                                     LP_ordcust_l10n_ext_tab) = FALSE then
         return FALSE;
      end if;
   end if;
   
   --Tax Breakups incurred for the Customer Order
   if LP_ordhead is NOT NULL and LP_ordhead.count > 0 then
      FOR i IN LP_ordhead.first..LP_ordhead.last
      LOOP
         if ORDER_STATUS_SQL.INSERT_ORD_TAX_BREAKUP(O_error_message,
                                                    LP_ordhead(i).order_no) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PERSIST_CREATE_PO;
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_CANCEL_PO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50)                := 'CORESVC_FULFILORD_PO.VALIDATE_CANCEL_PO';
   L_table           VARCHAR2(30);
   L_invalid_param   VARCHAR2(30)                := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_CLOSE_PO is
      select 'x'
        from svc_fulfilordref sf
       where sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
         and exists (select 'x'
                       from ordhead o,
                            ordcust oc
                      where o.order_no = oc.order_no
                        and oc.customer_order_no = sf.customer_order_no
                        and oc.fulfill_order_no = sf.fulfill_order_no
                        and oc.source_loc_type = sf.source_loc_type
                        and oc.source_loc_id = sf.source_loc_id
                        and oc.fulfill_loc_type = sf.fulfill_loc_type
                        and oc.fulfill_loc_id = sf.fulfill_loc_id
                        and o.status = 'C'
                        and rownum = 1)
         for update nowait;

   cursor C_LOCK_SVC_ERR is
      select 'x'
        from svc_fulfilordref sf
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilorddtlref sd
                      where sd.error_msg is NOT NULL
                        and sd.fulfilordref_id = sf.fulfilordref_id
                        and sd.process_id = sf.process_id
                        and sd.chunk_id = sf.chunk_id
                        and rownum = 1)
         for update nowait;

BEGIN

   if I_process_id is NULL then
      L_invalid_param := 'I_process_id';
   elsif I_chunk_id is NULL then
      L_invalid_param := 'I_chunk_id';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   -- set process_status to 'C'ompleted if ordhead.status = 'C'
   L_table := 'SVC_FULFILORDREF';
   open C_LOCK_SVC_CLOSE_PO;
   close C_LOCK_SVC_CLOSE_PO;

   update svc_fulfilordref sf
      set sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_COMPLETED
    where sf.process_id = I_process_id
      and sf.chunk_id = I_chunk_id
      and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
      and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
      and exists (select 'x'
                    from ordhead o,
                         ordcust oc
                   where o.order_no = oc.order_no
                     and oc.customer_order_no = sf.customer_order_no
                     and oc.fulfill_order_no = sf.fulfill_order_no
                     and oc.source_loc_type = sf.source_loc_type
                     and oc.source_loc_id = sf.source_loc_id
                     and oc.fulfill_loc_type = sf.fulfill_loc_type
                     and oc.fulfill_loc_id = sf.fulfill_loc_id
                     and o.status = 'C'
                     and rownum = 1);

   merge into svc_fulfilordref sf
   using (select *
            from (select s.process_id,
                         s.chunk_id,
                         s.fulfilordref_id,
                         case
                            when oh.order_no is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('NO_ORDER_FOUND_CO', s.customer_order_no, s.fulfill_order_no, NULL)||';'
                         end error_msg
                    from svc_fulfilordref s,
                         ordcust oc,
                         ordhead oh
                   where s.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
                     and s.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                     and s.chunk_id = I_chunk_id
                     and s.process_id = I_process_id
                     and s.customer_order_no = oc.customer_order_no(+)
                     and s.fulfill_order_no = oc.fulfill_order_no(+)
                     and s.source_loc_type = oc.source_loc_type(+)
                     and s.source_loc_id = oc.source_loc_id(+)
                     and s.fulfill_loc_type = oc.fulfill_loc_type(+)
                     and s.fulfill_loc_id = oc.fulfill_loc_id(+)
                     and oc.order_no = oh.order_no(+)
                     and oc.fulfill_loc_id = oh.location(+)
                     and oc.source_loc_id = oh.supplier(+))
           where error_msg is NOT NULL)inner
   on (sf.process_id = inner.process_id
       and sf.chunk_id = inner.chunk_id
       and sf.fulfilordref_id = inner.fulfilordref_id)
   when matched then
      update set sf.error_msg = substr(sf.error_msg || inner.error_msg,1,2000),
                 sf.process_status = 'E',
                 sf.last_update_id = LP_user,
                 sf.last_update_datetime = SYSDATE;

   if SQL%ROWCOUNT > 0 then
      select substr(error_msg, 1, instr(error_msg, ';')-1)
        into L_error_message
        from svc_fulfilordref
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and error_msg is NOT NULL
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   merge into svc_fulfilorddtlref sdf
   using (select *
            from (select sd.process_id,
                         sd.chunk_id,
                         sd.fulfilordref_id,
                         sd.item,
                         -- for records with a corresponding ordcust record, validate that the item exists in ordcust_detail
                         case
                            when NOT exists (select 'x'
                                               from ordcust_detail od
                                              where od.item = sd.item
                                                and od.ordcust_no = oc.ordcust_no
                                                and rownum = 1) then
                            SQL_LIB.GET_MESSAGE_TEXT('INV_PO_ITEM_CO', sf.customer_order_no, sf.fulfill_order_no)||';'
                         end error_msg
                      from svc_fulfilorddtlref sd,
                           svc_fulfilordref sf,
                           ordloc ol,
                           ordcust oc
                     where sf.process_id = I_process_id
                       and sf.chunk_id = I_chunk_id
                       and sf.process_id = sd.process_id
                       and sf.chunk_id = sd.chunk_id
                       and sf.fulfilordref_id = sd.fulfilordref_id
                       and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
                       and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
                       and sf.customer_order_no = oc.customer_order_no
                       and sf.fulfill_order_no = oc.fulfill_order_no
                       and sf.source_loc_type = oc.source_loc_type
                       and sf.source_loc_id = oc.source_loc_id
                       and sf.fulfill_loc_type = oc.fulfill_loc_type
                       and sf.fulfill_loc_id = oc.fulfill_loc_id
                       and oc.order_no = ol.order_no)
           where error_msg is NOT NULL
             and rownum = 1) inner
   on (sdf.process_id = inner.process_id
       and sdf.chunk_id = inner.chunk_id
       and sdf.fulfilordref_id = inner.fulfilordref_id
       and sdf.item = inner.item)
   when matched then
      update set sdf.error_msg = substr(sdf.error_msg || inner.error_msg, 1, 2000),
                 sdf.last_update_id = LP_user,
                 sdf.last_update_datetime = SYSDATE;

   if SQL%ROWCOUNT > 0 then

      L_table := 'SVC_FULFILORDREF';
      open C_LOCK_SVC_ERR;
      close C_LOCK_SVC_ERR;

      update svc_fulfilordref sf
         set sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
             sf.error_msg = substr(sf.error_msg || SQL_LIB.GET_MESSAGE_TEXT('ERROR_SVC_DETAIL', sf.customer_order_no, sf.fulfill_order_no, NULL)||';', 1, 2000),
             sf.last_update_id = LP_user,
             sf.last_update_datetime = SYSDATE
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilorddtlref sd
                      where sd.error_msg is NOT NULL
                        and sd.fulfilordref_id = sf.fulfilordref_id
                        and sd.process_id = sf.process_id
                        and sd.chunk_id = sf.chunk_id
                        and rownum = 1);

      select substr(error_msg, 1, instr(error_msg, ';')-1)
        into L_error_message
        from svc_fulfilorddtlref
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and error_msg is NOT NULL
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   return TRUE;


EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_CANCEL_PO;
----------------------------------------------------------------------------------------
FUNCTION POPULATE_CANCEL_REC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50)     := 'CORESVC_FULFILORD_PO.POPULATE_CANCEL_REC';
   L_invalid_param   VARCHAR2(30)     := NULL;
   L_index           BINARY_INTEGER   := 0;

   cursor C_GET_HDR_REC is
      select oc.order_no,
             sf.fulfilordref_id,
             sf.fulfill_loc_id location
        from ordcust oc,
             svc_fulfilordref sf
       where sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sf.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
         and sf.customer_order_no = oc.customer_order_no
         and sf.fulfill_order_no = oc.fulfill_order_no
         and sf.fulfill_loc_id = oc.fulfill_loc_id
         and oc.order_no is NOT NULL;

   cursor C_GET_DTL_REC (I_fulfilordref_id   SVC_FULFILORDREF.FULFILORDREF_ID%TYPE) is
      -- get detail records without shipment
      select sd.item,
             sd.cancel_qty_suom
        from svc_fulfilorddtlref sd,
             svc_fulfilordref sr,
             ordcust oc,
             ordcust_detail od,
             ordloc ol,
             ordhead oh
       where sr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sr.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
         and sr.process_id = I_process_id
         and sr.chunk_id = I_chunk_id
         and sr.process_id = sd.process_id
         and sr.chunk_id = sd.chunk_id
         and sr.fulfilordref_id = I_fulfilordref_id
         and sr.fulfilordref_id = sd.fulfilordref_id
         and sr.customer_order_no = oc.customer_order_no
         and sr.fulfill_order_no = oc.fulfill_order_no
         and sr.source_loc_type = oc.source_loc_type
         and sr.source_loc_id = oc.source_loc_id
         and sr.fulfill_loc_type = oc.fulfill_loc_type
         and sr.fulfill_loc_id = oc.fulfill_loc_id
         and oc.ordcust_no = od.ordcust_no
         and oc.order_no = oh.order_no
         and oh.order_no = ol.order_no
         and od.item = ol.item
         and ol.item = sd.item
         and oc.fulfill_loc_id = ol.location
         and ((NVL(ol.qty_received,0) < ol.qty_ordered) or
              (ol.qty_ordered = 0 and oh.status = 'A'))
         and ((sd.cancel_qty_suom <= ol.qty_ordered) or
              (ol.qty_ordered = 0 and oh.status = 'A') or
              (ol.qty_cancelled > 0 AND
               ol.qty_cancelled != NVL(od.qty_cancelled_suom,0) AND
               oh.status = 'A'))
         and NOT EXISTS (select 'x'
                           from shipment s,
                                shipsku sk
                          where s.order_no = ol.order_no
                            and s.shipment = sk.shipment
                            and sk.item = ol.item)
       union
      -- get detail records with shipment
      select sd.item,
             sd.cancel_qty_suom
        from svc_fulfilorddtlref sd,
             svc_fulfilordref sr,
             ordcust oc,
             ordcust_detail od,
             (select o.order_no,
                     o.item,
                     o.location,
                     (o.qty_ordered - sum(sk.qty_expected)) tot_qty
                from ordloc o,
                     shipment sm,
                     shipsku sk
               where o.order_no = sm.order_no
                 and sm.shipment = sk.shipment
                 and sk.item = o.item
               group by o.order_no,
                        o.item,
                        o.location,
                        o.qty_ordered) s
       where sr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED
         and sr.tran_type = RMS_CONSTANTS.CUST_ORD_TRAN_TYPE_PO
         and sr.process_id = I_process_id
         and sr.chunk_id = I_chunk_id
         and sr.process_id = sd.process_id
         and sr.chunk_id = sd.chunk_id
         and sr.fulfilordref_id = I_fulfilordref_id
         and sr.fulfilordref_id = sd.fulfilordref_id
         and sr.customer_order_no = oc.customer_order_no
         and sr.fulfill_order_no = oc.fulfill_order_no
         and sr.source_loc_type = oc.source_loc_type
         and sr.source_loc_id = oc.source_loc_id
         and sr.fulfill_loc_type = oc.fulfill_loc_type
         and sr.fulfill_loc_id = oc.fulfill_loc_id
         and oc.ordcust_no = od.ordcust_no
         and oc.order_no = s.order_no
         and od.item = s.item
         and sd.item  = od.item
         and sr.fulfill_loc_id = oc.fulfill_loc_id
         and oc.fulfill_loc_id = s.location
         and sd.cancel_qty_suom <= s.tot_qty;

   TYPE CANCEL_HDR_REC is TABLE OF C_GET_HDR_REC%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE CANCEL_DTL_REC is TABLE OF C_GET_DTL_REC%ROWTYPE INDEX BY BINARY_INTEGER;

   L_cancel_hdr_rec    CANCEL_HDR_REC;
   L_cancel_dtl_rec    CANCEL_DTL_REC;

BEGIN

   if I_process_id is NULL then
      L_invalid_param := 'I_process_id';
   elsif I_chunk_id is NULL then
      L_invalid_param := 'I_chunk_id';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   open C_GET_HDR_REC;
   fetch C_GET_HDR_REC BULK COLLECT INTO L_cancel_hdr_rec;
   close C_GET_HDR_REC;

   if L_cancel_hdr_rec is NOT NULL and L_cancel_hdr_rec.COUNT > 0 then
      LP_cancel_tbl := PO_CANCEL_TBL();
      FOR i in L_cancel_hdr_rec.FIRST .. L_cancel_hdr_rec.LAST LOOP
         open C_GET_DTL_REC(L_cancel_hdr_rec(i).fulfilordref_id);
         fetch C_GET_DTL_REC BULK COLLECT INTO L_cancel_dtl_rec;
         close C_GET_DTL_REC;

         if L_cancel_dtl_rec is NOT NULL and L_cancel_dtl_rec.COUNT > 0 then
            FOR j in L_cancel_dtl_rec.FIRST..L_cancel_dtl_rec.LAST LOOP
               --if this is the first detail record build the header cancel record and initialize the detail collection
               if j=1 then
                  LP_cancel_tbl.EXTEND();
                  LP_cancel_tbl(LP_cancel_tbl.COUNT).header_rec.order_no := L_cancel_hdr_rec(i).order_no;
                  LP_cancel_tbl(LP_cancel_tbl.COUNT).header_rec.location := L_cancel_hdr_rec(i).location;
                  -- build the detail cancel record
                  LP_cancel_tbl(LP_cancel_tbl.COUNT).item_rec.item := ITEM_TBL();
                  LP_cancel_tbl(LP_cancel_tbl.COUNT).item_rec.cancel_qty_suom := QTY_TBL();
                  --
               end if;
               -- build the detail record
               LP_cancel_tbl(LP_cancel_tbl.COUNT).item_rec.item.EXTEND();
               LP_cancel_tbl(LP_cancel_tbl.COUNT).item_rec.cancel_qty_suom.EXTEND();

               L_index := LP_cancel_tbl(LP_cancel_tbl.COUNT).item_rec.item.COUNT;
               LP_cancel_tbl(LP_cancel_tbl.COUNT).item_rec.item(L_index) := L_cancel_dtl_rec(j).item;
               LP_cancel_tbl(LP_cancel_tbl.COUNT).item_rec.cancel_qty_suom(L_index) := L_cancel_dtl_rec(j).cancel_qty_suom;
            END LOOP;
         end if; -- if L_cancel_dtl_rec.COUNT > 0 then
      END LOOP;
   end if; -- if L_cancel_hdr_rec.COUNT > 0 then

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POPULATE_CANCEL_REC;
----------------------------------------------------------------------------------------
FUNCTION PERSIST_CANCEL_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(50)     := 'CORESVC_FULFILORD_PO.PERSIST_CANCEL_PO';
   L_table             VARCHAR2(30);
   L_tot_qty_ordered   ORDLOC.QTY_ORDERED%TYPE;
   L_container_item    ITEM_MASTER.CONTAINER_ITEM%TYPE;
   L_ol_qty_ordered    ORDLOC.QTY_ORDERED%TYPE;
   L_od_qty_ordered    ORDCUST_DETAIL.QTY_ORDERED_SUOM%TYPE;
   L_ol_qty_can        ORDLOC.QTY_CANCELLED%TYPE;
   L_od_qty_can        ORDCUST_DETAIL.QTY_CANCELLED_SUOM%TYPE;
   L_wf_order_no       ORDHEAD.WF_ORDER_NO%TYPE;
   L_status            ORDHEAD.STATUS%TYPE;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ORDLOC(I_order_no     ORDLOC.ORDER_NO%TYPE,
                        I_item         ORDLOC.ITEM%TYPE,
                        I_location     ORDLOC.LOCATION%TYPE) is
      select 'x'
        from ordloc
       where order_no = I_order_no
         and item = I_item
         and location = I_location
         for update nowait;

   cursor C_LOCK_ORDCUST_DETAIL(I_order_no   ORDCUST.ORDER_NO%TYPE,
                                I_item       ORDCUST_DETAIL.ITEM%TYPE) is
      select 'x'
        from ordcust_detail
       where ordcust_no = (select ordcust_no
                             from ordcust
                            where order_no = I_order_no)
         and item = I_item
         for update nowait;

   cursor C_LOCK_ORDHEAD (I_order_no   ORDLOC.ORDER_NO%TYPE) is
      select 'x'
        from ordhead
       where order_no = I_order_no;

   cursor C_CHK_QTY_ORDERED (I_order_no   ORDLOC.ORDER_NO%TYPE) is
      select sum(qty_ordered)
        from ordloc
       where order_no = I_order_no
    group by qty_ordered;

   cursor C_GET_CONTAINER (I_item   ITEM_MASTER.ITEM%TYPE) is
      select container_item
        from item_master
       where item = I_item;

   cursor C_CHECK_CAN_QTY (I_order_no   ORDHEAD.ORDER_NO%TYPE,
                           I_item       ORDLOC.ITEM%TYPE,
                           I_location   ORDLOC.LOCATION%TYPE) is
      select ol.qty_ordered,
             od.qty_ordered_suom,
             NVL(ol.qty_cancelled,0) ol_qty_can,
             NVL(od.qty_cancelled_suom,0) od_qty_can
        from ordhead oh,
             ordloc ol,
             ordcust oc,
             ordcust_detail od
       where oh.order_no = I_order_no
         and oh.order_no = ol.order_no
         and oh.location = I_location
         and ol.item = I_item
         and oh.location = ol.location
         and oh.order_no = oc.order_no
         and oh.location = oc.fulfill_loc_id
         and oc.ordcust_no = od.ordcust_no
         and od.item = ol.item;
BEGIN

   if LP_cancel_tbl is NOT NULL and LP_cancel_tbl.COUNT > 0 then
      FOR i in LP_cancel_tbl.FIRST..LP_cancel_tbl.LAST LOOP
         if LP_cancel_tbl(i).item_rec.item is NOT NULL and LP_cancel_tbl(i).item_rec.item.COUNT > 0 then
            FOR j in LP_cancel_tbl(i).item_rec.item.FIRST..LP_cancel_tbl(i).item_rec.item.LAST LOOP

               L_ol_qty_can  := 0;
               L_od_qty_can  := 0;

               open C_CHECK_CAN_QTY(LP_cancel_tbl(i).header_rec.order_no,
                                    LP_cancel_tbl(i).item_rec.item(j),
                                    LP_cancel_tbl(i).header_rec.location);
               fetch C_CHECK_CAN_QTY into L_ol_qty_ordered,
                                          L_od_qty_ordered,
                                          L_ol_qty_can,
                                          L_od_qty_can;
               close C_CHECK_CAN_QTY;

               if L_ol_qty_can = L_od_qty_can then
               -- no ediupack updates
                  L_ol_qty_ordered := L_ol_qty_ordered - LP_cancel_tbl(i).item_rec.cancel_qty_suom(j);
                  L_ol_qty_can     := L_ol_qty_can + LP_cancel_tbl(i).item_rec.cancel_qty_suom(j);
                  L_od_qty_can     := L_od_qty_can + LP_cancel_tbl(i).item_rec.cancel_qty_suom(j);
               elsif L_ol_qty_can = L_od_qty_ordered and L_ol_qty_ordered = 0 then
               -- PO was fully cancelled via ediupack
                  L_od_qty_can := L_od_qty_ordered;
               elsif (L_ol_qty_can != L_od_qty_can and L_ol_qty_ordered != 0) then
               -- PO was partially cancelled via ediupack
                  L_od_qty_can := L_od_qty_can + LP_cancel_tbl(i).item_rec.cancel_qty_suom(j);
               end if;

               L_table := 'ORDLOC';
               open C_LOCK_ORDLOC(LP_cancel_tbl(i).header_rec.order_no,
                                  LP_cancel_tbl(i).item_rec.item(j),
                                  LP_cancel_tbl(i).header_rec.location);
               close C_LOCK_ORDLOC;

               update ordloc
                  set qty_ordered = L_ol_qty_ordered,
                      last_rounded_qty = L_ol_qty_ordered,
                      last_grp_rounded_qty = L_ol_qty_ordered,
                      qty_cancelled = L_ol_qty_can,
                      cancel_code = 'B',
                      cancel_date = LP_vdate,
                      cancel_id = LP_user
                where order_no = LP_cancel_tbl(i).header_rec.order_no
                  and item = LP_cancel_tbl(i).item_rec.item(j)
                  and location = LP_cancel_tbl(i).header_rec.location;

               L_table := 'ORDCUST_DETAIL';
               open C_LOCK_ORDCUST_DETAIL(LP_cancel_tbl(i).header_rec.order_no,
                                          LP_cancel_tbl(i).item_rec.item(j));
               close C_LOCK_ORDCUST_DETAIL;

               update ordcust_detail
                  set qty_cancelled_suom = L_od_qty_can,
                      last_update_id = LP_user,
                      last_update_datetime = SYSDATE
                where ordcust_no = (select ordcust_no
                                      from ordcust
                                     where order_no = LP_cancel_tbl(i).header_rec.order_no)
                   and item = LP_cancel_tbl(i).item_rec.item(j);

               -- if item is a deposit item, cancel the container items as well

               L_container_item := NULL;

               open C_GET_CONTAINER(LP_cancel_tbl(i).item_rec.item(j));
               fetch C_GET_CONTAINER into L_container_item;
               close C_GET_CONTAINER;

               if L_container_item is NOT NULL then
                  update ordloc
                     set qty_ordered = L_ol_qty_ordered,
                         last_rounded_qty = L_ol_qty_ordered,
                         last_grp_rounded_qty = L_ol_qty_ordered
                   where order_no = LP_cancel_tbl(i).header_rec.order_no
                     and location = LP_cancel_tbl(i).header_rec.location
                     and item = L_container_item;
               end if;

            END LOOP;
         end if;

         -- if the sum of the qty_ordered of all items in the order is zero, update
         -- ordhead.status = 'C'
         L_tot_qty_ordered := 0;
         open C_CHK_QTY_ORDERED (LP_cancel_tbl(i).header_rec.order_no);
         fetch C_CHK_QTY_ORDERED into L_tot_qty_ordered;
         close C_CHK_QTY_ORDERED;

         if NVL(L_tot_qty_ordered,0) = 0 then
            open C_LOCK_ORDHEAD(LP_cancel_tbl(i).header_rec.order_no);
            close C_LOCK_ORDHEAD;

            update ordhead
               set status = 'C',
                   close_date = LP_vdate,
                   last_update_id = get_user,
                   last_update_datetime = sysdate
             where order_no = LP_cancel_tbl(i).header_rec.order_no;
         end if;

         if WF_PO_SQL.SYNC_F_ORDER(O_error_message,
                                   L_wf_order_no,
                                   L_status,
                                   RMS_CONSTANTS.WF_ACTION_UPDATE,
                                   LP_cancel_tbl(i).header_rec.order_no,
                                   NULL) = FALSE then
            return FALSE;
         end if;

      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PERSIST_CANCEL_PO;
----------------------------------------------------------------------------------------
END CORESVC_FULFILORD_PO;
/
