CREATE OR REPLACE PACKAGE LC_APPLY_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------
-- Function Name: VALID_LCORDAPP_ID
-- Purpose:       Determines if a given LC Ref ID is a valid letter of credit in that
--                it can be attached to an order. 
--------------------------------------------------------------------------------------
FUNCTION VALID_LCORDAPP_ID(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%type,
                           O_exists          IN OUT BOOLEAN,
                           I_lc_ref_id       IN     LC_HEAD.LC_REF_ID%TYPE,
                           I_applicant       IN     LC_HEAD.APPLICANT%TYPE,
                           I_beneficiary     IN     LC_HEAD.BENEFICIARY%TYPE,
                           I_issuing_bank    IN     LC_HEAD.ISSUING_BANK%TYPE,
                           I_purchase_type   IN     LC_HEAD.PURCHASE_TYPE%TYPE,
                           I_fob_title_pass  IN     LC_HEAD.FOB_TITLE_PASS%TYPE,
                           I_fob_title_desc  IN     LC_HEAD.FOB_TITLE_PASS_DESC%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: WRITE_LCHEAD
-- Purpose:       Writes a record to the lc_head table based on the information
--                passed in, on the ordlc table, on the ordhead table, and on the
--                sup_import_attr table. 
--------------------------------------------------------------------------------------
FUNCTION WRITE_LCHEAD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%type,
                      I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                      I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE,
                      I_form_type      IN     LC_HEAD.FORM_TYPE%TYPE,
                      I_lc_type        IN     LC_HEAD.LC_TYPE%TYPE,
                      I_issuing_bank   IN     LC_HEAD.ISSUING_BANK%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: PROCESS_LCORDAPP
-- Purpose:       This function will first verify that a Purchase Order-Letter of 
--                Credit relationship can be created.  If so, the Purchase Order is 
--                attached to a Letter of Credit.
--------------------------------------------------------------------------------------
FUNCTION PROCESS_LCORDAPP(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%type,
                          O_error_flag    IN OUT  BOOLEAN,
                          I_issuing_bank  IN      LC_HEAD.ISSUING_BANK%TYPE,
                          I_currency_code IN      LC_HEAD.CURRENCY_CODE%TYPE) 
                       RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: RECALC_LCORDAPP_PROJ
-- Purpose:       This function recalculates the Projected Total Line of Credit,
--                Outstanding Credit, and Open Line of Credit for the lcordapp
--                form.
--------------------------------------------------------------------------------------
FUNCTION RECALC_LCORDAPP_PROJ(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%type,
                              O_line_of_credit        IN OUT  PARTNER.LINE_OF_CREDIT%TYPE,
                              O_outstand_credit       IN OUT  PARTNER.OUTSTAND_CREDIT%TYPE,
                              O_open_credit           IN OUT  PARTNER.OPEN_CREDIT%TYPE,
                              O_line_of_credit_prim   IN OUT  PARTNER.LINE_OF_CREDIT%TYPE,
                              O_outstand_credit_prim  IN OUT  PARTNER.OUTSTAND_CREDIT%TYPE,
                              O_open_credit_prim      IN OUT  PARTNER.OPEN_CREDIT%TYPE,
			      I_issuing_bank          IN      PARTNER.PARTNER_ID%TYPE)
                       RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: CHECK_LADING_PORT
-- Purpose      : This function checks if the input lading port is same as
--                the lading port for the order no attached to LC
--------------------------------------------------------------------------------------
FUNCTION CHECK_LADING_PORT(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%type,
                           O_warning_flg           IN OUT  VARCHAR2,
                           I_lc_ref_id             IN      LC_DETAIL.LC_REF_ID%TYPE,
                           I_lading_port           IN      ORDHEAD.LADING_PORT%TYPE)
                    RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: CHECK_DISCHARGE_PORT
-- Purpose      : This function checks if the input discharge port is same as
--                the discharge port for the order no attached to LC
--------------------------------------------------------------------------------------
FUNCTION CHECK_DISCHARGE_PORT(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%type,
                              O_warning_flg           IN OUT  VARCHAR2,
                              I_lc_ref_id             IN      LC_DETAIL.LC_REF_ID%TYPE,
                              I_discharge_port        IN      ORDHEAD.DISCHARGE_PORT%TYPE)
                       RETURN BOOLEAN;
--------------------------------------------------------------------------------------
END LC_APPLY_SQL;
/
