
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_PAYTERM_SQL AS

-----------------------------------------------------------------------
FUNCTION PERSIST(O_error_message        OUT    RTK_ERRORS.RTK_TEXT%TYPE,  
                 I_message            IN       TERMS_SQL.PAYTERM_REC,
                 I_message_type       IN       VARCHAR2)
   RETURN BOOLEAN IS
   
  L_program      VARCHAR2(50) := 'RMSSUB_PAYTERM_SQL.PERSIST';


BEGIN

   if TERMS_SQL.MERGE_HEADER(O_error_message,
                             I_message) = FALSE then
      return FALSE;
   end if;

   if TERMS_SQL.MERGE_DETAIL(O_error_message,
                             I_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
---------------------------------------------------------------------
END;
/
