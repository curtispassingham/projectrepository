SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY PO_DIFF_MATRIX_PIVOT_SQL AS
----------------------------------------------------------------------------------------------
FUNCTION POPULATE_PIVOT(O_ERROR_MESSAGE IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(100) := 'PO_DIFF_MATRIX_PIVOT_SQL.POPULATE_PIVOT';
BEGIN
   -- clear the  po_diff_matrix_pivot_temp table before populating it.
   delete from po_diff_matrix_pivot_temp;
   insert into po_diff_matrix_pivot_temp(diff_z,
                                         diff_y,
                                         diff_x,
                                         x_seq_no,
                                         x_ref_column_name,
                                         value)                                    
                                  select pdmt.diff_z diff_z,
                                         pdmt.diff_y diff_y,
                                         x.diff_x   diff_x,
                                         x.seq_no   x_seq_no,
                                         pdmt.column_name x_ref_column_name ,
                                         pdmt.value   value 
                                     from (select diff_z, 
                                                  diff_y, 
                                                  column_name, 
                                                  value 
                                             from po_diff_matrix_temp 
                                                  UNPIVOT INCLUDE NULLS(value FOR column_name IN("VALUE1",
                                                                                                 "VALUE2",
                                                                                                 "VALUE3",
                                                                                                 "VALUE4",
                                                                                                 "VALUE5",
                                                                                                 "VALUE6",
                                                                                                 "VALUE7",
                                                                                                 "VALUE8",
                                                                                                 "VALUE9",
                                                                                                 "VALUE10",
                                                                                                 "VALUE11",
                                                                                                 "VALUE12",
                                                                                                 "VALUE13",
                                                                                                 "VALUE14",
                                                                                                 "VALUE15",
                                                                                                 "VALUE16",
                                                                                                 "VALUE17",
                                                                                                 "VALUE18",
                                                                                                 "VALUE19",
                                                                                                 "VALUE20",
                                                                                                 "VALUE21",
                                                                                                 "VALUE22",
                                                                                                 "VALUE23",
                                                                                                 "VALUE24",
                                                                                                 "VALUE25",
                                                                                                 "VALUE26",
                                                                                                 "VALUE27",
                                                                                                 "VALUE28",
                                                                                                 "VALUE29",
                                                                                                 "VALUE30"))
                                           )pdmt,
                                          diff_x_temp x
                                   where  nvl(x.diff_z,'-9999') = nvl(pdmt.diff_z,'-9999')
                                     and  x.seq_no              = substr(pdmt.column_name,6)
                                    order by diff_z , 
                                             diff_y ,
                                             seq_no;
   return TRUE;
EXCEPTION
  when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
END POPULATE_PIVOT;
----------------------------------------------------------------------------------------------
FUNCTION APPLY_PIVOT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
L_program     VARCHAR2(100) := 'PO_DIFF_MATRIX_PIVOT_SQL.APPLY_PIVOT';
BEGIN
   --- updating the po_diff_matrix_temp table with the latest values from po_diff_matrix_pivot_temp
    merge into po_diff_matrix_temp pdmt
    using (select diff_y,
                  diff_z,
                 max(decode(x_ref_column_name,'VALUE1',value)) VALUE1,
                 max(decode(x_ref_column_name,'VALUE2',value)) VALUE2,
                 max(decode(x_ref_column_name,'VALUE3',value)) VALUE3,
                 max(decode(x_ref_column_name,'VALUE4',value)) VALUE4,
                 max(decode(x_ref_column_name,'VALUE5',value)) VALUE5,
                 max(decode(x_ref_column_name,'VALUE6',value)) VALUE6,
                 max(decode(x_ref_column_name,'VALUE7',value)) VALUE7,
                 max(decode(x_ref_column_name,'VALUE8',value)) VALUE8,
                 max(decode(x_ref_column_name,'VALUE9',value)) VALUE9,
                 max(decode(x_ref_column_name,'VALUE10',value)) VALUE10,
                 max(decode(x_ref_column_name,'VALUE11',value)) VALUE11,
                 max(decode(x_ref_column_name,'VALUE12',value)) VALUE12,
                 max(decode(x_ref_column_name,'VALUE13',value)) VALUE13,
                 max(decode(x_ref_column_name,'VALUE14',value)) VALUE14,
                 max(decode(x_ref_column_name,'VALUE15',value)) VALUE15,
                 max(decode(x_ref_column_name,'VALUE16',value)) VALUE16,
                 max(decode(x_ref_column_name,'VALUE17',value)) VALUE17,
                 max(decode(x_ref_column_name,'VALUE18',value)) VALUE18,
                 max(decode(x_ref_column_name,'VALUE19',value)) VALUE19,
                 max(decode(x_ref_column_name,'VALUE20',value)) VALUE20,
                 max(decode(x_ref_column_name,'VALUE21',value)) VALUE21,
                 max(decode(x_ref_column_name,'VALUE22',value)) VALUE22,
                 max(decode(x_ref_column_name,'VALUE23',value)) VALUE23,
                 max(decode(x_ref_column_name,'VALUE24',value)) VALUE24,
                 max(decode(x_ref_column_name,'VALUE25',value)) VALUE25,
                 max(decode(x_ref_column_name,'VALUE26',value)) VALUE26,
                 max(decode(x_ref_column_name,'VALUE27',value)) VALUE27,
                 max(decode(x_ref_column_name,'VALUE28',value)) VALUE28,
                 max(decode(x_ref_column_name,'VALUE29',value)) VALUE29,
                 max(decode(x_ref_column_name,'VALUE30',value)) VALUE30
          from po_diff_matrix_pivot_temp
          group by diff_z,diff_y
        )use_this
   on ( pdmt.diff_y = use_this.diff_y
       and NVL(pdmt.diff_z, '-999') = NVL(use_this.diff_z,'-999'))
   when matched then
   update set pdmt.value1 = use_this.value1,
              pdmt.value2 = use_this.value2,
              pdmt.value3 = use_this.value3,
              pdmt.value4 = use_this.value4,
              pdmt.value5 = use_this.value5,
              pdmt.value6 = use_this.value6,
              pdmt.value7 = use_this.value7,
              pdmt.value8 = use_this.value8,
              pdmt.value9 = use_this.value9,
              pdmt.value10 = use_this.value10,
              pdmt.value11 = use_this.value11,
              pdmt.value12 = use_this.value12,
              pdmt.value13 = use_this.value13,
              pdmt.value14 = use_this.value14,
              pdmt.value15 = use_this.value15,
              pdmt.value16 = use_this.value16,
              pdmt.value17 = use_this.value17,
              pdmt.value18 = use_this.value18,
              pdmt.value19 = use_this.value19,
              pdmt.value20 = use_this.value20,
              pdmt.value21 = use_this.value21,
              pdmt.value22 = use_this.value22,
              pdmt.value23 = use_this.value23,
              pdmt.value24 = use_this.value24,
              pdmt.value25 = use_this.value25,
              pdmt.value26 = use_this.value26,
              pdmt.value27 = use_this.value27,
              pdmt.value28 = use_this.value28,
              pdmt.value29 = use_this.value29,
              pdmt.value30 = use_this.value30;
   return TRUE;           
EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
END APPLY_PIVOT;
----------------------------------------------------------------------------------------------
FUNCTION DELETE_PIVOT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
L_program     VARCHAR2(100) := 'PO_DIFF_MATRIX_PIVOT_SQL.DELETE_PIVOT';
BEGIN
   -- clear the  po_diff_matrix_pivot_temp tables used by po diff distribution matrix screen.
   delete from po_diff_matrix_pivot_temp;
   return TRUE;
EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
END DELETE_PIVOT;
----------------------------------------------------------------------------------------------
END PO_DIFF_MATRIX_PIVOT_SQL;
/