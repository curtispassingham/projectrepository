
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSMFM_ORDER AUTHID CURRENT_USER AS

/*--- message type parameters ---*/
HDR_ADD     CONSTANT  VARCHAR2(64) := 'POCre';
HDR_UPD     CONSTANT  VARCHAR2(64) := 'POHdrMod';
HDR_DEL     CONSTANT  VARCHAR2(64) := 'PODel';
DTL_ADD     CONSTANT  VARCHAR2(64) := 'PODTLCre';
DTL_UPD     CONSTANT  VARCHAR2(64) := 'PODTLMod';
DTL_DEL     CONSTANT  VARCHAR2(64) := 'PODTLDel';

FAMILY      CONSTANT  RIB_SETTINGS.FAMILY%TYPE := 'order';

TYPE ordhead_msg_rectype IS RECORD
                    (ordhead_rec               ORDHEAD%ROWTYPE,
                     doc_type                  VARCHAR2(1),
                     order_type_desc           CODE_DETAIL.CODE_DESC%TYPE,
                     dept_name                 DEPS.DEPT_NAME%TYPE,
                     buyer_name                BUYER.BUYER_NAME%TYPE,
                     promotion_desc            RPM_PROMO.NAME%TYPE,  -- map to NAME instead of DESCRIPTION on the RPM_PROMO table
                     terms_code                TERMS.TERMS_CODE%TYPE,
                     payment_method_desc       CODE_DETAIL.CODE_DESC%TYPE,
                     backhaul_type_desc        CODE_DETAIL.CODE_DESC%TYPE,
                     ship_method_desc          CODE_DETAIL.CODE_DESC%TYPE,
                     purchase_type_desc        CODE_DETAIL.CODE_DESC%TYPE,
                     ship_pay_method_desc      CODE_DETAIL.CODE_DESC%TYPE,
                     fob_trans_res_code_desc   CODE_DETAIL.CODE_DESC%TYPE,
                     fob_title_pass_code_desc  CODE_DETAIL.CODE_DESC%TYPE,
                     factory_desc              PARTNER.PARTNER_DESC%TYPE,
                     agent_desc                PARTNER.PARTNER_DESC%TYPE,
                     discharge_port_desc       OUTLOC.OUTLOC_DESC%TYPE,
                     lading_port_desc          OUTLOC.OUTLOC_DESC%TYPE,
                     po_type_desc              PO_TYPE.PO_TYPE_DESC%TYPE);
--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_message         OUT VARCHAR2,
                I_message_type          IN  ORDER_MFQUEUE.MESSAGE_TYPE%TYPE,
                I_order_no              IN  ORDHEAD.ORDER_NO%TYPE,
                I_order_type            IN  ORDHEAD.ORDER_TYPE%TYPE,
                I_order_header_status   IN  ORDHEAD.STATUS%TYPE,
                I_supplier              IN  ORDHEAD.SUPPLIER%TYPE,
                I_item                  IN  ORDLOC.ITEM%TYPE,
                I_location              IN  ORDLOC.LOCATION%TYPE,
                I_loc_type              IN  ORDLOC.LOC_TYPE%TYPE,
                I_physical_location     IN  ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1);
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT);
--------------------------------------------------------------------------------
END RMSMFM_ORDER;
/
