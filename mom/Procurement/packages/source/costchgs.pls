CREATE OR REPLACE PACKAGE COST_CHANGE_SQL AUTHID CURRENT_USER AS

 -------------------------------------------------------------------------------------
 -- Function Name: LOCK_COST_CHANGE
 -- Purpose : This function is used to lock the COST_SUSP_SUP_HEAD table
 -- Created : 15-DEC-99 by Zak Koudjeti
 --------------------------------------------------------------------------------------
 FUNCTION LOCK_COST_CHANGE (O_error_message IN OUT VARCHAR2,
                            I_cost_change   IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
                            RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function Name: POP_TEMP_DETAIL
 -- Purpose : This function will populate the COST_CHANGE_TEMP table
 -- Created : 05-FEB-2001 by Rachel Rushing
 --------------------------------------------------------------------------------------
 FUNCTION POP_TEMP_DETAIL (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists           IN OUT BOOLEAN,
                           I_mode             IN     VARCHAR2,
                           I_cost_change      IN     COST_CHANGE_TEMP.COST_CHANGE%TYPE,
                           I_supplier         IN     SUPS.SUPPLIER%TYPE,
                           I_origin_country   IN     COUNTRY.COUNTRY_ID%TYPE,
                           I_item             IN     ITEM_MASTER.ITEM%TYPE,
                           I_delivery_country IN     COUNTRY.COUNTRY_ID%TYPE)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function Name: POP_TEMP_DETAIL_LOC
 -- Purpose : This function will populate the COST_CHANGE_LOC_TEMP table
 -- Created : 07-FEB-2001 by Rachel Rushing
 --------------------------------------------------------------------------------------
 FUNCTION POP_TEMP_DETAIL_LOC (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists           IN OUT BOOLEAN,
                               I_mode             IN     VARCHAR2,
                               I_cost_change      IN     COST_CHANGE_TEMP.COST_CHANGE%TYPE,
                               I_supplier         IN     SUPS.SUPPLIER%TYPE,
                               I_origin_country   IN     COUNTRY.COUNTRY_ID%TYPE,
                               I_item             IN     ITEM_MASTER.ITEM%TYPE,
                               I_reason           IN     COST_SUSP_SUP_HEAD.REASON%TYPE,
                               I_delivery_country IN     COUNTRY.COUNTRY_ID%TYPE DEFAULT NULL)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function Name: APPLY_CHANGE
 -- Purpose : This function is used to apply mass change to the Cost_Change_Temp table
 --           based on a change percentage or amount.
 -- Created : 06-MAR-2001 by Rachel Rushing
 --------------------------------------------------------------------------------------
 FUNCTION APPLY_CHANGE (O_error_message  IN OUT VARCHAR2,
                        I_change_type    IN     VARCHAR2,
                        I_change_amount  IN     NUMBER,
                        I_currency_code  IN     SUPS.CURRENCY_CODE%TYPE DEFAULT NULL)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function Name: APPLY_CHANGE_LOC
 -- Purpose : This function is used to apply mass change to the Cost_Change_Loc_temp
 --           table based on a passed item, supplier, origin country, location, bracket
 --           value, and change percentage or amount.
 -- Created : 07-FEB-2001 by Rachel Rushing
 --------------------------------------------------------------------------------------
 FUNCTION APPLY_CHANGE_LOC (O_error_message     IN OUT VARCHAR2,
                            I_supplier          IN     COST_CHANGE_LOC_TEMP.SUPPLIER%TYPE,
                            I_country           IN     COST_CHANGE_LOC_TEMP.ORIGIN_COUNTRY_ID%TYPE,
                            I_item              IN     COST_CHANGE_LOC_TEMP.ITEM%TYPE,
                            I_loc_type          IN     COST_CHANGE_LOC_TEMP.LOC_TYPE%TYPE,
                            I_location          IN     VARCHAR2,
                            I_bracket_value     IN     COST_CHANGE_LOC_TEMP.BRACKET_VALUE1%TYPE,
                            I_change_type       IN     VARCHAR2,
                            I_change_amount     IN     NUMBER,
                            I_currency_code     IN     SUPS.CURRENCY_CODE%TYPE DEFAULT NULL)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function Name: WH_BRACKET_EXISTS
 -- Purpose : This function returns a BOOLEAN value indicating whether or not the passed
 --           in warehouse and bracket value exists.
 -- Created : 07-FEB-2001 by Rachel Rushing
 --------------------------------------------------------------------------------------
 FUNCTION WH_BRACKET_EXISTS
          (O_error_message  IN OUT VARCHAR2,
           O_exists         IN OUT BOOLEAN,
           I_item           IN     COST_SUSP_SUP_DETAIL_LOC.ITEM%TYPE,
           I_supplier       IN     COST_SUSP_SUP_DETAIL_LOC.SUPPLIER%TYPE,
           I_origin_country IN     COST_SUSP_SUP_DETAIL_LOC.ORIGIN_COUNTRY_ID%TYPE,
           I_warehouse      IN     COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE,
           I_bracket_value  IN     COST_SUSP_SUP_DETAIL_LOC.BRACKET_VALUE1%TYPE)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function Name: UPDATE_CC_DETAIL_TEMP
 -- Purpose : This function keeps the COST_CHANGE_DETAIL_TEMP table in sync with the
 --           COST_CHANGE_DETAIL_LOC_TEMP table.
 -- Created : 07-FEB-2001 by Rachel Rushing
 --------------------------------------------------------------------------------------
 FUNCTION UPDATE_CC_DETAIL_TEMP (O_error_message    IN OUT VARCHAR2,
                                 I_item             IN     ITEM_MASTER.ITEM%TYPE,
                                 I_supplier         IN     SUPS.SUPPLIER%TYPE,
                                 I_origin_country   IN     COUNTRY.COUNTRY_ID%TYPE,
                                 I_delivery_country IN     COUNTRY.COUNTRY_ID%TYPE)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function Name: INSERT_UPDATE_COST_CHANGE
 -- Purpose : This function inserts cost change detail records from data on the cost
 --           change temporary tables.
 -- Created : 07-FEB-2001 by Rachel Rushing
 --------------------------------------------------------------------------------------
 FUNCTION INSERT_UPDATE_COST_CHANGE
                   (O_error_message  IN OUT VARCHAR2,
                    I_cost_change    IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function Name: DELETE_COST_CHANGE_TEMP
 -- Purpose : This function deletes all records from the COST_CHANGE_TEMP table for a
 --           specified cost_change.
 -- Created : 07-FEB-2001 by Rachel Rushing
 --------------------------------------------------------------------------------------
 FUNCTION DELETE_COST_CHANGE_TEMP
                   (O_error_message  IN OUT VARCHAR2,
                    I_cost_change    IN     COST_CHANGE_TEMP.COST_CHANGE%TYPE)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function Name: DELETE_COST_CHANGE_LOC_TEMP
 -- Purpose : This function deletes all records from the COST_CHANGE_TEMP table for a
 --           specified cost_change.
 --------------------------------------------------------------------------------------
 FUNCTION DELETE_COST_CHANGE_LOC_TEMP
                   (O_error_message  IN OUT VARCHAR2,
                    I_cost_change    IN     COST_CHANGE_LOC_TEMP.COST_CHANGE%TYPE)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function: CHECK_COST_CONFLICTS
 -- Purpose : Checks for cost change conflicts with the passed in price/cost change
 --           number on the passed in active date.
 -- Called functions: None
 -- Created : Rachel Rushing
 -- Note:     This function was moved from pcstats/b.pls, modified and renamed.  The
 --           original function was named 'CHECK_COST_RETAIL_CONFLICTS' and was created
 --           by Amy Selby on 17-OCT-96
 --------------------------------------------------------------------------------------
 FUNCTION CHECK_COST_CONFLICTS (O_error_message  IN OUT VARCHAR2,
                                O_conflicts      IN OUT BOOLEAN,
                                I_cost_change    IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                                I_active_date    IN     COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE)
                                RETURN BOOLEAN;
 ------------------------------------------------------------------------------------------
 -- Function Name: VALIDATE_COSTS
 -- Purpose : This function verifies that all costs on COST_CHANGE_TEMP, if I_level is 'C',
 --           or COST_CHANGE_LOC_TEMP, if I_level is 'L', are greater than zero for the
 --           passed in cost change.
 ------------------------------------------------------------------------------------------
 FUNCTION VALIDATE_COSTS(O_error_message  IN OUT VARCHAR2,
                         O_valid          IN OUT BOOLEAN,
                         I_cost_change    IN     COST_CHANGE_TEMP.COST_CHANGE%TYPE,
                         I_level          IN     VARCHAR2)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function Name: POP_FOR_ITEMLIST
 -- Purpose : This function will populate the COST_CHANGE_TEMP table for an itemlist.
 --           Buyer packs will be filtered out of the itemlist if they exist.
 --------------------------------------------------------------------------------------
 FUNCTION POP_FOR_ITEMLIST(O_error_message    IN OUT VARCHAR2,
                           O_exists           IN OUT BOOLEAN,
                           I_mode             IN     VARCHAR2,
                           I_cost_change      IN     COST_CHANGE_TEMP.COST_CHANGE%TYPE,
                           I_supplier         IN     SUPS.SUPPLIER%TYPE,
                           I_origin_country   IN     COUNTRY.COUNTRY_ID%TYPE,
                           I_itemlist         IN     SKULIST_HEAD.SKULIST%TYPE,
                           I_delivery_country IN     COUNTRY.COUNTRY_ID%TYPE)
    RETURN BOOLEAN;
 ------------------------------------------------------------------------------------------
 -- Function Name: CC_TEMP_LOCS_EXIST
 -- Purpose : This function queries the cost_change_loc_temp table to check if locations
 --           exist based on cost change and optionally supplier, item, country.
 -- Created : Pat Repinski 13-JUN-2001
 ------------------------------------------------------------------------------------------
 FUNCTION CC_TEMP_LOCS_EXIST (O_error_message    IN OUT VARCHAR2,
                              O_exist            IN OUT BOOLEAN,
                              I_cost_change      IN     COST_CHANGE_TEMP.COST_CHANGE%TYPE,
                              I_item             IN     ITEM_MASTER.ITEM%TYPE,
                              I_supplier         IN     SUPS.SUPPLIER%TYPE,
                              I_origin_country   IN     COUNTRY.COUNTRY_ID%TYPE,
                              I_delivery_country IN     COUNTRY.COUNTRY_ID%TYPE)
    RETURN BOOLEAN;
 -------------------------------------------------------------------------------------
 -- Function Name:  CC_SUPPLIER
 -- Purpose : This function returns the supplier for a cost change based on cost change.
 -- Created : Pat Repinski 21-JUN-2001
 ------------------------------------------------------------------------------------------
 FUNCTION CC_SUPPLIER (O_error_message  IN OUT VARCHAR2,
                       O_supplier       IN OUT SUPS.SUPPLIER%TYPE,
                       I_cost_change    IN     COST_CHANGE_TEMP.COST_CHANGE%TYPE)

    RETURN BOOLEAN;
 -------------------------------------------------------------------------------------
 -- Function Name:  CC_ITEM
 -- Purpose : This function returns the item for a cost change based on cost change.
 -- Created : Pat Repinski 21-JUN-2001
 -------------------------------------------------------------------------------------
 FUNCTION CC_ITEM (O_error_message  IN OUT VARCHAR2,
                   O_item           IN OUT ITEM_MASTER.ITEM%TYPE,
                   I_cost_change    IN     COST_CHANGE_TEMP.COST_CHANGE%TYPE)

    RETURN BOOLEAN;
 -------------------------------------------------------------------------------------
 -- Function Name:  BC_UNIT_COST
 -- Purpose : This function returns the unit cost for each bracket based on item,
 --           supplier, country and bracket values.
 -- Created : Pat Repinski 21-JUN-2001
 -------------------------------------------------------------------------------------
 FUNCTION BC_UNIT_COST (O_error_message  IN OUT VARCHAR2,
                        O_unit_cost      IN OUT ITEM_LOC_SOH.UNIT_COST%TYPE,
                        I_supplier       IN     SUPS.SUPPLIER%TYPE,
                        I_origin_country IN     COUNTRY.COUNTRY_ID%TYPE,
                        I_item           IN     ITEM_MASTER.ITEM%TYPE,
                        I_bracket        IN     ITEM_SUPP_COUNTRY_BRACKET_COST.BRACKET_VALUE1%TYPE,
                        I_location       IN     WH.PHYSICAL_WH%TYPE)
    RETURN BOOLEAN;
 -------------------------------------------------------------------------------------
 -- Function Name:  COST_CHANGE_TEMP_EXISTS
 -- Purpose : This function checks to ensure that a new cost has been added to records
 --           in one of the temp tables for the cost change before submitting it.
 -- Created : Pat Repinski 22-JUN-2001
 -------------------------------------------------------------------------------------
 FUNCTION COST_CHANGE_TEMP_EXISTS (O_error_message  IN OUT VARCHAR2,
                                   O_exist          IN OUT BOOLEAN,
                                   I_cost_change    IN     COST_CHANGE_TEMP.COST_CHANGE%TYPE)
    RETURN BOOLEAN;
 -------------------------------------------------------------------------------------
 -- Function Name:  DELETE_ALL_TEMP
 -- Purpose : This function reduces database access from the suppsku form by combining the
 --           calls to the DELETE_COST_CHANGE_TEMP and DELETE_COST_CHANGE_LOC_TEMP fnctions
 --           for cancelling the cost change in the suppsku form.
 -- Created : Pat Repinski 22-JUN-2001
 ------------------------------------------------------------------------------------------
 FUNCTION DELETE_ALL_TEMP(O_error_message  IN OUT VARCHAR2,
                          I_cost_change    IN     COST_CHANGE_LOC_TEMP.COST_CHANGE%TYPE)
    RETURN BOOLEAN;
 ------------------------------------------------------------------------------------------
 -- Function Name:  COST_CHANGE_LOCATIONS_EXISTS
 -- Purpose : This function checks the cost_susp_sup_detail_loc to verify locations exist.
 --           This function will be called from suppsku in VIEW mode to determine if the
 --           user can enter the locations screen.
 -------------------------------------------------------------------------------------
 FUNCTION COST_CHANGE_LOCATIONS_EXISTS (O_error_message       IN OUT VARCHAR2,
                                        O_exist               IN OUT BOOLEAN,
                                        I_item                IN     ITEM_MASTER.ITEM%TYPE,
                                        I_supplier            IN     SUPS.SUPPLIER%TYPE,
                                        I_origin_country      IN     COUNTRY.COUNTRY_ID%TYPE,
                                        I_cost_change         IN     COST_CHANGE_TEMP.COST_CHANGE%TYPE,
                                        I_delivery_country_id IN     COUNTRY.COUNTRY_ID%TYPE)
    RETURN BOOLEAN;
 -------------------------------------------------------------------------------------
 -- Function Name:  UPDATE_RECALC_ORD_IND_AT_LOC
 -- Purpose : This function checks will set the recalculate order indicator to Yes
 --           for all location records of an item, supplier, country.
 -------------------------------------------------------------------------------------
 FUNCTION UPDATE_RECALC_ORD_IND_AT_LOC(O_error_message       IN OUT VARCHAR2,
                                       I_item                IN     ITEM_MASTER.ITEM%TYPE,
                                       I_supplier            IN     SUPS.SUPPLIER%TYPE,
                                       I_origin_country      IN     COUNTRY.COUNTRY_ID%TYPE,
                                       I_cost_change         IN     COST_CHANGE_TEMP.COST_CHANGE%TYPE,
                                       I_recalc_ord_ind      IN     COST_CHANGE_TEMP.RECALC_ORD_IND%TYPE,
                                       I_delivery_country_id IN     COUNTRY.COUNTRY_ID%TYPE)
    RETURN BOOLEAN;
 -------------------------------------------------------------------------------------
 -- Function Name: DELETE_DETAIL_LOC
 -- Purpose : This function deletes all records from the COST_CHANGE_TEMP table for the
 --           specified parameters.  If only the cost change is passed in, then all the
 --           records for the cost change will be deleted.  Item, supplier, and country
 --           are optional.
 --------------------------------------------------------------------------------------
 FUNCTION DELETE_DETAIL_LOC
                   (O_error_message     IN OUT VARCHAR2,
                    I_cost_change       IN     COST_CHANGE_LOC_TEMP.COST_CHANGE%TYPE,
                    I_item              IN     ITEM_MASTER.ITEM%TYPE,
                    I_supplier          IN     SUPS.SUPPLIER%TYPE,
                    I_origin_country    IN     COUNTRY.COUNTRY_ID%TYPE,
                    I_delivery_country  IN     COUNTRY.COUNTRY_ID%TYPE)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function: CHECK_BUYER_PACK_CONFLICTS
 -- Purpose : Checks for cost change conflicts for buyer pack with the passed in price/cost change
 --           number on the passed in active date.
 --------------------------------------------------------------------------------------
 FUNCTION CHECK_BUYER_PACK_CONFLICTS (O_error_message  IN OUT VARCHAR2,
                                      O_conflicts      IN OUT BOOLEAN,
                                      I_cost_change    IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                                      I_active_date    IN     COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE)
                                      RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function: CREATE_RCA_COST_CHG
 -- Purpose : This function will create records to be picked up by the general supplier
 --           cost update batch program.
 --------------------------------------------------------------------------------------
 FUNCTION CREATE_RCA_COST_CHG (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cost_change_no       IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                               I_reason               IN       COST_SUSP_SUP_HEAD.REASON%TYPE,
                               I_active_date          IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                               I_status               IN       COST_SUSP_SUP_HEAD.STATUS%TYPE,
                               I_cost_change_origin   IN       COST_SUSP_SUP_HEAD.COST_CHANGE_ORIGIN%TYPE,
                               I_create_date          IN       COST_SUSP_SUP_HEAD.CREATE_DATE%TYPE,
                               I_create_id            IN       COST_SUSP_SUP_HEAD.CREATE_ID%TYPE,
                               I_approval_date        IN       COST_SUSP_SUP_HEAD.APPROVAL_DATE%TYPE,
                               I_approval_id          IN       COST_SUSP_SUP_HEAD.APPROVAL_ID%TYPE)
    RETURN BOOLEAN;
 ------------------------------------------------------------------------------------------
 --------------------------------------------------------------------------------------
 -- Function: COST_CHANGE_EXISTS
 -- Purpose : This function will check for existing cost change records.
 --------------------------------------------------------------------------------------------
 FUNCTION COST_CHANGE_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists           IN OUT   BOOLEAN,
                             I_supplier         IN       COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                             I_item             IN       COST_SUSP_SUP_DETAIL.ITEM%TYPE,
                             I_location         IN       COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE,
                             I_effective_date   IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------------
 --------------------------------------------------------------------------------------
 -- Function: APPLY_TO_ALL_LOCS
 -- Purpose : This function will create cost change detail records for multiple locations
 --           on an order.
 --------------------------------------------------------------------------------------------
 FUNCTION APPLY_TO_ALL_LOCS (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_conflict        IN OUT  VARCHAR,
                             I_order_no        IN      ORDHEAD.ORDER_NO%TYPE,
                             I_item            IN      ORDLOC.ITEM%TYPE,
                             I_bracket_value   IN      SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                             I_change_amt      IN      COST_CHANGE_LOC_TEMP.UNIT_COST_NEW%TYPE,
                             I_change_no       IN      COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------------

 --------------------------------------------------------------------------------------
 -- Name    : POP_TEMP_DETAIL_SEC
 -- Purpose : This function will populate the COST_CHANGE_TEMP table
 --           depending on the privilege granted to the user's security group
 --------------------------------------------------------------------------------------
 FUNCTION POP_TEMP_DETAIL_SEC (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists             IN OUT   BOOLEAN,
                               I_mode               IN       VARCHAR2,
                               I_cost_change        IN       COST_CHANGE_TEMP.COST_CHANGE%TYPE,
                               I_supplier           IN       SUPS.SUPPLIER%TYPE,
                               I_origin_country     IN       COUNTRY.COUNTRY_ID%TYPE,
                               I_item               IN       ITEM_MASTER.ITEM%TYPE,
                               I_expand_parent      IN       VARCHAR2,
                               I_delivery_country   IN       COUNTRY.COUNTRY_ID%TYPE)
    RETURN BOOLEAN;

 --------------------------------------------------------------------------------------
 -- Name    : POP_TEMP_DETAIL_LOC_SEC
 -- Purpose : This function will populate the COST_CHANGE_LOC_TEMP table
 --           depending on the privilege granted to the user's security group
 --------------------------------------------------------------------------------------
 FUNCTION POP_TEMP_DETAIL_LOC_SEC (O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists             IN OUT  BOOLEAN,
                                   O_v_locs_exist       IN OUT  VARCHAR2,
                                   I_mode               IN      VARCHAR2,
                                   I_cost_change        IN      COST_CHANGE_TEMP.COST_CHANGE%TYPE,
                                   I_supplier           IN      SUPS.SUPPLIER%TYPE,
                                   I_origin_country     IN      COUNTRY.COUNTRY_ID%TYPE,
                                   I_item               IN      ITEM_MASTER.ITEM%TYPE,
                                   I_reason             IN      COST_SUSP_SUP_HEAD.REASON%TYPE,
                                   I_delivery_country   IN      COUNTRY.COUNTRY_ID%TYPE,
                                   I_default_po_cost    IN      COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE)
    RETURN BOOLEAN;

 ------------------------------------------------------------------------------------------
 -- Function: CHECK_COST_CHANGE_ORIGIN
 -- Purpose : Checks if more than one item was added to a cost change that originally had a
 --           'SUPP' origin.  If more than one exists, then the origin is updated.
 --           Note that this call is made prior
 ------------------------------------------------------------------------------------------
 FUNCTION CHECK_COST_CHANGE_ORIGIN (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_update_to_sup    IN OUT   BOOLEAN,
                                    I_cost_change_no   IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
    RETURN BOOLEAN;
 ------------------------------------------------------------------------------------------
 -- Function: CHECK_RECALC_ORD_IND_STATUS
 -- Purpose : This function will retrieve the most recent Recalc_Ord_Ind value, ON or OFF.
 --           The value retrieved will be used in Program Unit code to determine whether or
 --           not to check Recalc_Order checkbox.
 ------------------------------------------------------------------------------------------
 FUNCTION RECALC_ORD_IND_STATUS (O_error_message IN OUT     VARCHAR2,
            O_recalc_ord_ind_status OUT   COST_SUSP_SUP_DETAIL_LOC.RECALC_ORD_IND%TYPE,
                              I_cost_change   IN         COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE)
    RETURN BOOLEAN;
 ------------------------------------------------------------------------------------------
 -- Function Name: DELETE_COST_CHANGE_LOC_TEMP
 -- Purpose :This overloaded function deletes all records from the COST_CHANGE_LOC_TEMP table for a
 --          specified item, supplier and origin country.
 --------------------------------------------------------------------------------------
 FUNCTION DELETE_COST_CHANGE_LOC_TEMP (O_error_message    IN OUT VARCHAR2,
                                       I_item             IN     ITEM_MASTER.ITEM%TYPE,
                                       I_supplier         IN     SUPS.SUPPLIER%TYPE,
                                       I_origin_country   IN     COUNTRY.COUNTRY_ID%TYPE,
                                       I_delivery_country IN     COUNTRY.COUNTRY_ID%TYPE)
    RETURN BOOLEAN;
 --------------------------------------------------------------------------------------
 -- Function Name: CHECK_CC_ITEM_DUP
 -- Purpose :This function checks if the item is already existing in the cost change.
 ------------------------------------------------------------------------------------------
 FUNCTION CHECK_CC_ITEM_DUP (O_error_message  IN OUT VARCHAR2,
                             O_exists         IN OUT BOOLEAN,
                             I_cost_change    IN     COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                             I_item           IN     ITEM_MASTER.ITEM%TYPE)
    RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: CHECK_CC_USER_SEC
-- Purpose :This function checks if the user have access to all items in the cost change.
------------------------------------------------------------------------------------------
FUNCTION CHECK_CC_USER_SEC (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_cost_change       IN OUT   NUMBER,
                            O_security_exists   IN OUT   NUMBER,
                            I_cost_change       IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                            I_country           IN       COST_SUSP_SUP_DETAIL.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION APPLY_CHANGE (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_change_type      IN       VARCHAR2,
                       I_change_amount    IN       NUMBER,
                       I_effective_date   IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                       I_cc_basis         IN       VARCHAR2,
                       I_currency_code    IN       SUPS.CURRENCY_CODE%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: APPLY_CHANGE_LOC
-- Purpose : This function is used to apply mass change to the Cost_Change_Loc_temp
--           table based on a passed item, supplier, origin country, location, bracket
--           value, and change percentage or amount.
--           Changes are based on input parameter:
--           'U' - changes applied to Unit Cost
--           'F' - changes applied to future cost on effective date (I_effective_date)
--------------------------------------------------------------------------------------
FUNCTION APPLY_CHANGE_LOC (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_supplier         IN       COST_CHANGE_LOC_TEMP.SUPPLIER%TYPE,
                           I_country          IN       COST_CHANGE_LOC_TEMP.ORIGIN_COUNTRY_ID%TYPE,
                           I_item             IN       COST_CHANGE_LOC_TEMP.ITEM%TYPE,
                           I_loc_type         IN       COST_CHANGE_LOC_TEMP.LOC_TYPE%TYPE,
                           I_location         IN       VARCHAR2,
                           I_bracket_value    IN       COST_CHANGE_LOC_TEMP.BRACKET_VALUE1%TYPE,
                           I_change_type      IN       VARCHAR2,
                           I_change_amount    IN       NUMBER,
                           I_effective_date   IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                           I_cc_basis         IN       VARCHAR2,
                           I_currency_code    IN       SUPS.CURRENCY_CODE%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: FUTURE_COST_CHANGE_EXISTS
-- Purpose : This function will check for existing cost change records.
--           If the record exists, based on the reason_code fetched, the incoming
--           cost change would be rejected, updated or inserted
--------------------------------------------------------------------------------------
FUNCTION FUTURE_COST_CHANGE_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists           IN OUT   BOOLEAN,
                                   O_update           IN OUT   BOOLEAN,
                                   O_cost_change      IN OUT   COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                                   I_supplier         IN       COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                                   I_item             IN       COST_SUSP_SUP_DETAIL.ITEM%TYPE,
                                   I_location         IN       COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE,
                                   I_effective_date   IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: UPDATE_COST_CHANGE
-- Purpose : This function will update the cost detail in COST_SUSP_SUP_DETAIL_LOC
--           table for the incoming RCA
--------------------------------------------------------------------------------------
FUNCTION UPDATE_COST_CHANGE (O_error_message  IN OUT VARCHAR2,
                             I_cost_change    IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: APPLY_CHANGE
-- Purpose : This function is an overloaded version to apply cost changes at the
--           item/supplier/country level. This is used for the ADF screen.
--------------------------------------------------------------------------------------
FUNCTION APPLY_CHANGE (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_change_type        IN       VARCHAR2,
                       I_change_amount      IN       NUMBER,
                       I_effective_date     IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                       I_cc_basis           IN       VARCHAR2,
                       I_supplier           IN       COST_CHANGE_TEMP.SUPPLIER%TYPE,
                       I_country            IN       COST_CHANGE_TEMP.ORIGIN_COUNTRY_ID%TYPE,
                       I_item               IN       COST_CHANGE_TEMP.ITEM%TYPE,
                       I_delivery_country   IN       COST_CHANGE_TEMP.DELIVERY_COUNTRY_ID%TYPE,
                       I_currency_code      IN       SUPS.CURRENCY_CODE%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: APPLY_CHANGE
-- Purpose : This function is an overloaded version to apply cost changes at the
--           item/supplier/country level. This is used for the ADF screen.
--------------------------------------------------------------------------------------
FUNCTION APPLY_CHANGE (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_change_type        IN       VARCHAR2,
                       I_change_amount      IN       NUMBER,
                       I_supplier           IN       COST_CHANGE_TEMP.SUPPLIER%TYPE,
                       I_country            IN       COST_CHANGE_TEMP.ORIGIN_COUNTRY_ID%TYPE,
                       I_item               IN       COST_CHANGE_TEMP.ITEM%TYPE,
                       I_delivery_country   IN       COST_CHANGE_TEMP.DELIVERY_COUNTRY_ID%TYPE,
                       I_currency_code      IN       SUPS.CURRENCY_CODE%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: CUSTOM_VAL
-- Purpose : This function will be used for validationg the custom function.
--------------------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_cost_change       IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                    I_supplier          IN       COST_SUSP_SUP_DETAIL.SUPPLIER%TYPE,
                    I_item              IN       COST_SUSP_SUP_DETAIL.ITEM%TYPE,
                    I_location          IN       COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE,
                    I_effective_date    IN       COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE,
                    I_function_key      IN       VARCHAR2,
                    I_seq_no            IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END COST_CHANGE_SQL;
/