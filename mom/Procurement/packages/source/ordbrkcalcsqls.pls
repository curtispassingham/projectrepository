
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORDER_BRACKET_CALC_SQL AUTHID CURRENT_USER AS                                                                                       
-----------------------------------------------------------------------------                                           
-- Name:       APPLY_BRACKETS                                                    
--             This function will determine what level the brackets will be applied
--             and call UPDATE_ORDER_COST to update the order with the bracket cost.                                                                                      
-----------------------------------------------------------------------------                                           
FUNCTION APPLY_BRACKETS(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,                                                          
                        I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,                                             
                        I_override_manual_ind IN     DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE)                          
RETURN BOOLEAN;                                                                                                         
-----------------------------------------------------------------------------
-- Name:       UPDATE_ORDER_COST
--             This function updates the order with the bracket cost.                                                                                      
-----------------------------------------------------------------------------                                                   
FUNCTION UPDATE_ORDER_COST(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,                                                            
                           I_bracket_value1      IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,                                
                           I_bracket_value2      IN     SUP_BRACKET_COST.BRACKET_VALUE2%TYPE,                                
                           I_dept                IN     SUP_BRACKET_COST.DEPT%TYPE,                                          
                           I_location            IN     SUP_BRACKET_COST.LOCATION%TYPE,                                      
                           I_supplier            IN     SUP_BRACKET_COST.SUPPLIER%TYPE,                                      
                           I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,                                               
                           I_purchase_type       IN     ORDHEAD.PURCHASE_TYPE%TYPE,
                           I_override_manual_ind IN     DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE)                                                
RETURN BOOLEAN;                                                                                                         
-----------------------------------------------------------------------------        
-- Name:       APPLY_THRESHOLDS                                                   
--             This function will determine if an order can be scaled up to
--             another bracket based on the threshold percent.                                                                                      
-----------------------------------------------------------------------------                                           
FUNCTION APPLY_THRESHOLDS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,                                                             
                          O_bracket_value1 IN OUT SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                          O_bracket_type1  IN OUT SUP_INV_MGMT.BRACKET_TYPE1%TYPE,
                          O_bracket_uom1   IN OUT SUP_INV_MGMT.BRACKET_UOM1%TYPE,                                 
                          O_bracket_value2 IN OUT SUP_BRACKET_COST.BRACKET_VALUE2%TYPE,
                          O_bracket_type2  IN OUT SUP_INV_MGMT.BRACKET_TYPE2%TYPE,
                          O_bracket_uom2   IN OUT SUP_INV_MGMT.BRACKET_UOM2%TYPE,                                 
                          O_next_bracket   IN OUT BOOLEAN,
                          I_truck_total    IN     NUMBER,  
                          I_total_ord_qty  IN     NUMBER,
                          I_max_bracket    IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                          I_prim_or_sec    IN     VARCHAR2,                                                                                                                      
                          I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)                                              
RETURN BOOLEAN;                                                                                                         
-----------------------------------------------------------------------------        
-- Name:       GET_QUANTITY_ORDERED
--             This function will return the order quantity in the primary and
--             secondary brackets for the bracket uom.                                                                                      
-----------------------------------------------------------------------------                                           
FUNCTION GET_QUANTITY_ORDERED(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,                                                        
                              O_qty_ord_uom1    IN OUT ORDLOC.QTY_ORDERED%TYPE,                                         
                              O_qty_ord_uom2    IN OUT ORDLOC.QTY_ORDERED%TYPE,                                         
                              O_sup_dept_seq_no IN OUT SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE,                               
                              I_dept            IN     SUP_BRACKET_COST.DEPT%TYPE,                                      
                              I_location        IN     SUP_BRACKET_COST.LOCATION%TYPE,                                  
                              I_supplier        IN     SUP_BRACKET_COST.SUPPLIER%TYPE,                                  
                              I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)                                           
RETURN BOOLEAN;                                                                                                         
-----------------------------------------------------------------------------                                           
END ORDER_BRACKET_CALC_SQL;                                                                                             
/
