CREATE OR REPLACE PACKAGE COST_ZONE_SQL AUTHID CURRENT_USER AS

TYPE ZONE_ID_REC is RECORD (zone_id     COST_ZONE.ZONE_ID%TYPE,
                            zone_desc   COST_ZONE.DESCRIPTION%TYPE);
TYPE ZONE_ID_TBL is TABLE OF ZONE_ID_REC index by BINARY_INTEGER;


---------------------------------------------------------------------------------------------
--- Function Name:  INSERT_COST_ZONE_FOR_VWH
--- Purpose:        Inserts cost zone informations for VWH into cost_zone_group_loc.
--- Calls:          COSTZONE.FMB
--- Created:        28-Aug-2001 By Shaam A Khan
---------------------------------------------------------------------------------------------
FUNCTION INSERT_COST_ZONE_FOR_VWH(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_zone_group               IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                                  I_zone                     IN       COST_ZONE.ZONE_ID%TYPE,
                                  I_pwh                      IN       WH.PHYSICAL_WH%TYPE,
                                  I_primary_discharge_port   IN       COST_ZONE_GROUP_LOC.PRIMARY_DISCHARGE_PORT%TYPE DEFAULT NULL)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
--- Function Name:  DELETE_COST_ZONE_FOR_VWH
--- Purpose:        Deletes cost zone informations for VWH from cost_zone_group_loc.
--- Calls:          COSTZONE.FMB
--- Created:        28-Aug-2001 By Shaam A Khan
---------------------------------------------------------------------------------------------
FUNCTION DELETE_COST_ZONE_FOR_VWH(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_records_deleted   IN OUT   BOOLEAN,
                                  I_zone_group        IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                                  I_zone              IN       COST_ZONE.ZONE_ID%TYPE,
                                  I_pwh               IN       WH.PHYSICAL_WH%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
--- Function Name:  COST_ZONE_EXIST
--- Purpose:        Check if a duplicate location exists on cost_zone_group_loc.
--- Calls:          COSTZONE.FMB
--- Created:        01-MAR-2002 By Rebecca Sabir
---------------------------------------------------------------------------------------------
FUNCTION COST_ZONE_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_zone_group      IN       COST_ZONE_GROUP_LOC.ZONE_GROUP_ID%TYPE,
                         I_zone_id         IN       COST_ZONE_GROUP_LOC.ZONE_ID%TYPE DEFAULT NULL,
                         I_location        IN       COST_ZONE_GROUP_LOC.LOCATION%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
--- Function Name:  GET_COST_ZONE_ID
--- Purpose:        Returns back the passed in zone id or the distinct zone ids in cost_zone table
---                 when zone id is not passed in.
--- Calls:          store.fmb, warehse.fmb
--- Created:        26-MAY-2008
---------------------------------------------------------------------------------------------
FUNCTION GET_COST_ZONE_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_zone_desc       IN OUT   COST_ZONE.DESCRIPTION%TYPE,
                          O_exists          IN OUT   BOOLEAN,
                          I_zone_id         IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------

FUNCTION INSERT_COST_ZONE_LOC(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_zone_group               IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                              I_zone                     IN       COST_ZONE.ZONE_ID%TYPE,
                              I_location                 IN       COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                              I_loc_type                 IN       COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                              I_primary_discharge_port   IN       COST_ZONE_GROUP_LOC.PRIMARY_DISCHARGE_PORT%TYPE DEFAULT NULL)
   return BOOLEAN;

---------------------------------------------------------------------------------------------

FUNCTION DELETE_COST_ZONE_LOC(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_zone_group               IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                              I_zone                     IN       COST_ZONE.ZONE_ID%TYPE,
                              I_location                 IN       COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                              I_loc_type                 IN       COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE)
   return BOOLEAN;

----------------------------------------------------------------------------------------------

FUNCTION DISCHARGE_PORT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   BOOLEAN,
                               O_outloc_desc     IN OUT   OUTLOC.OUTLOC_DESC%TYPE,
                               I_outloc_id       IN       OUTLOC.OUTLOC_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------------------

FUNCTION GET_PRIM_DISCHRG_PORT(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_primary_discharge_port   IN OUT   COST_ZONE_GROUP_LOC.PRIMARY_DISCHARGE_PORT%TYPE,
                               I_zone_group_id            IN OUT   COST_ZONE_GROUP_LOC.ZONE_GROUP_ID%TYPE,
                               I_zone_id                  IN       COST_ZONE_GROUP_LOC.ZONE_ID%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------------------

FUNCTION GET_FIRST_COST_ZONE_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_zone_id         IN OUT   COST_ZONE.ZONE_ID%TYPE,
                                O_zone_desc       IN OUT   COST_ZONE.DESCRIPTION%TYPE,
                                I_zone_group      IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                                I_location        IN       COST_ZONE_GROUP_LOC.LOCATION%TYPE)
   return BOOLEAN;

---------------------------------------------------------------------------------------------
FUNCTION DELETE_COST_ZONE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_zone_group_id   IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                          I_zone_id         IN       COST_ZONE_GROUP_LOC.ZONE_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------
FUNCTION INSERT_COSTZONE_LOC(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_zone_group_id           IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                             I_zone_group_desc         IN       COST_ZONE_GROUP.DESCRIPTION%TYPE,
                             I_currency_code           IN       COST_ZONE.CURRENCY_CODE%TYPE,
                             I_like_zone_group_id      IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
END COST_ZONE_SQL;
   
/
