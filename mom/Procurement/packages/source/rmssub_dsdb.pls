CREATE OR REPLACE PACKAGE BODY RMSSUB_DSD AS

PROGRAM_ERROR    EXCEPTION;
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS (O_status               IN OUT  VARCHAR2,
                         IO_error_message       IN OUT  VARCHAR2,
                         I_cause                IN      VARCHAR2,
                         I_program              IN      VARCHAR2);
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_NO(O_error_message        IN OUT VARCHAR2,
                      O_order_no             IN OUT ordhead.order_no%TYPE,
                      I_ext_receipt_no       IN     shipment.ext_ref_no_in%TYPE,
                      I_store                IN     store.store%TYPE,
                      I_supplier             IN     sups.supplier%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code        IN OUT   VARCHAR2,
                  O_error_message      IN OUT   VARCHAR2,
                  I_message            IN       RIB_OBJECT,
                  I_message_type       IN       VARCHAR2,
                  O_rib_dsddeals_rec      OUT   RIB_OBJECT)
IS
   dsdCnt      BINARY_INTEGER := 0;
   dsdDtlCnt   BINARY_INTEGER := 0;
   dsdNmCnt    BINARY_INTEGER := 0;

   L_rib_dsddesc_rec     "RIB_DSDReceiptDesc_REC"                          := NULL;
   L_DSDDealsRec         "RIB_DSDDealsDesc_REC"                            := NULL;
   L_DSDDeals_TBL        "RIB_DSDDeals_TBL"                                := NULL;
   L_dsd_deals_rec       "RIB_DSDDeals_REC"                                := NULL;
   L_order_no            ORDHEAD.ORDER_NO%TYPE                             := NULL;
   L_vdate               PERIOD.VDATE%TYPE                                 := NULL;
   L_dept_level_orders   PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE   := NULL;
   L_rib_otb_tbl         "RIB_OTB_TBL"                                     := NULL;
   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;

BEGIN
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if CREATE_ORDER_SQL.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message_type is NULL or lower(I_message_type) not in (DSD_CRE, DSD_MOD) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MESSAGE_TYPE', I_message_type, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   L_rib_dsddesc_rec := treat(I_MESSAGE AS "RIB_DSDReceiptDesc_REC");

   if L_rib_dsddesc_rec is NULL or L_rib_dsddesc_rec.DSDReceipt_TBL is NULL then
      O_status_code := API_CODES.SUCCESS;
      return;
   end if;

   if L_rib_dsddesc_rec.DSDReceipt_TBL.LAST > 0 then
      L_DSDDeals_TBL := "RIB_DSDDeals_TBL"();
      L_DSDDeals_TBL.extend(L_rib_dsddesc_rec.DSDReceipt_TBL.LAST);
      L_DSDDealsRec := "RIB_DSDDealsDesc_REC"(1, NULL);
   end if;
   
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   L_dept_level_orders  := L_system_options_row.dept_level_orders; 

   FOR dsdCnt IN L_rib_dsddesc_rec.DSDReceipt_TBL.FIRST..L_rib_dsddesc_rec.DSDReceipt_TBL.LAST LOOP

      if CREATE_ORDER_SQL.RESET(O_error_message) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if lower(I_message_type) = DSD_CRE then

         if CREATE_ORDER_SQL.PROCESS_HEAD(O_error_message,
                                          L_order_no,
                                          L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).store,
                                          L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).supplier,
                                          CASE WHEN L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).dept = 0
                                             THEN
                                                NULL
                                             ELSE
                                                L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).dept
                                          END,
                                          L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).origin_country_id,
                                          L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).currency_code,
                                          NVL(L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).paid_ind,'N'),
                                          L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).deals_ind,
                                          L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).invc_ind,
                                          L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).ext_ref_no,
                                          L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).proof_of_delivery_no,
                                          L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).payment_ref_no,
                                          L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).ext_receipt_no,
                                          'DSD',
                                          L_dept_level_orders) = FALSE then
            RAISE PROGRAM_ERROR;
         end if;

         if L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDNonMerch_TBL is not NULL then
           FOR dsdNmCnt IN L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDNonMerch_TBL.FIRST..L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDNonMerch_TBL.LAST LOOP
              if CREATE_ORDER_SQL.PROCESS_NMEXP(O_error_message,
                                                L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDNonMerch_TBL(dsdNmCnt).non_merch_code,
                                                L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDNonMerch_TBL(dsdNmCnt).non_merch_amt,
                                                L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDNonMerch_TBL(dsdNmCnt).vat_code,
                                                L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDNonMerch_TBL(dsdNmCnt).service_perf_ind,
                                                L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).store) = FALSE then
                 raise PROGRAM_ERROR;
              end if;
           END LOOP;
         end if;

         if L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL is not NULL then
           FOR dsdDtlCnt IN L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL.FIRST..L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL.LAST LOOP
              if CREATE_ORDER_SQL.PROCESS_DETAIL(O_error_message,
                                                 L_order_no,
                                                 L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL(dsdDtlCnt).item,
                                                 L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL(dsdDtlCnt).qty_received,
                                                 L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).store,
                                                 L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).supplier,
                                                 CASE WHEN L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).dept = 0
                                                    THEN
                                                       NULL
                                                    ELSE
                                                       L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).dept
                                                 END,
                                                 L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).origin_country_id,
                                                 L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).currency_code,
                                                 NULL,                                                         -- item_loc_status
                                                 L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL(dsdDtlCnt).weight,
                                                 L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL(dsdDtlCnt).weight_uom,
                                                 L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL(dsdDtlCnt).unit_cost) = FALSE then
                 raise PROGRAM_ERROR;
              end if;
           END LOOP;
         end if;

         if CREATE_ORDER_SQL.COMPLETE_DSD_TRANSACTION(O_error_message,
                                                      L_dsd_deals_rec,
                                                      L_order_no,
                                                      L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).store,
                                                      L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).supplier,
                                                      CASE WHEN L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).dept = 0
                                                         THEN
                                                            NULL
                                                         ELSE
                                                            L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).dept
                                                      END,
                                                      L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).invc_ind,
                                                      L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).currency_code,
                                                      NVL( L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).paid_ind,'N'),
                                                      L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).deals_ind,
                                                      L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).ext_ref_no,
                                                      L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).proof_of_delivery_no,
                                                      L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).payment_ref_no,
                                                      L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).payment_date,
                                                      L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCNT).ext_receipt_no,
                                                      L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCNT).receipt_date) = FALSE then
            raise PROGRAM_ERROR;
         end if;

         L_DSDDeals_TBL(dsdCnt) := L_dsd_deals_rec;

      elsif lower(I_message_type) = DSD_MOD then

         L_vdate := GET_VDATE;

         -- Get existing RMS order number
         if GET_ORDER_NO(O_error_message,
                         L_order_no,
                         L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).ext_receipt_no,
                         L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).store,
                         L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).supplier) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         --- complete receipt mod
         if L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL is not NULL then

            if ORDER_RCV_SQL.INIT_PO_ASN_LOC_GROUP(O_error_message) = FALSE then
               raise PROGRAM_ERROR;
            end if;

            FOR dsdDtlCnt IN L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL.FIRST..L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL.LAST LOOP
               /* 
                During DSD adjustment SIM only sends delta quantity.
                1) When adjustment qty is -ve then increase qty_cancelled and reduce qty_ordered.
                   PO stays in Closed status and it will not show the adjustment qty as On-Order qty.
                2) When adjustment qty is +ve then increase qty_ordered and adjust qty_cancelled.
               */
               if ORDER_STATUS_SQL.ADJUST_DSD_ORDER
                                            (O_error_message,
                                             L_order_no,
                                             L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL(dsdDtlCnt).item,
                                             L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).store,
                                             L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL(dsdDtlCnt).qty_received,
                                             'RMSSUB_DSD')= FALSE then
                  raise PROGRAM_ERROR;   
               end if;
   
               if ORDER_RCV_SQL.PO_LINE_ITEM(O_error_message,
                                             L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).store,
                                             L_order_no,
                                             L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL(dsdDtlCnt).item,
                                             L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL(dsdDtlCnt).qty_received,
                                             'A',
                                             L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).receipt_date,
                                             L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).ext_receipt_no,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             'ATS',
                                             L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL(dsdDtlCnt).unit_cost,
                                             NULL,
                                             L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL(dsdDtlCnt).weight,
                                             L_rib_dsddesc_rec.DSDReceipt_TBL(dsdCnt).DSDDtl_TBL(dsdDtlCnt).weight_uom,
                                             'N') = FALSE then
                  raise PROGRAM_ERROR;
               end if;
            END LOOP;

            ---  Assumption: OTB logic will never be needed for DSD create or modify messages.
            if ORDER_RCV_SQL.FINISH_PO_ASN_LOC_GROUP(O_error_message,
                                                     L_rib_otb_tbl) = FALSE then
               raise PROGRAM_ERROR;
            end if;

         end if;

      else
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC',I_message_type,
                                               I_message_type,'RMSSUB_DSD.CONSUME');
         raise PROGRAM_ERROR;
      end if;
   END LOOP;

   L_DSDDealsRec.DSDDeals_TBL := L_DSDDeals_TBL;

   if lower(I_message_type) = DSD_CRE then
      O_rib_dsddeals_rec := L_DSDDealsRec;
   else
      O_rib_dsddeals_rec := NULL;
   end if;

   -- If no error is raised, then the subscription has completed successfully.
   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
     HANDLE_ERRORS(O_status_code,
                   O_error_message,
                   API_LIBRARY.FATAL_ERROR,
                   'RMSSUB_DSD.CONSUME');
   when OTHERS then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     'RMSSUB_DSD.CONSUME');
END CONSUME;
-----------------------------------------------------------------------------------------
FUNCTION GET_ORDER_NO(O_error_message        IN OUT VARCHAR2,
                      O_order_no             IN OUT ordhead.order_no%TYPE,
                      I_ext_receipt_no       IN     shipment.ext_ref_no_in%TYPE,
                      I_store                IN     store.store%TYPE,
                      I_supplier             IN     sups.supplier%TYPE)
RETURN BOOLEAN IS

   L_module VARCHAR2(50)  := 'RMSSUB_DSD.GET_ORDER_NO';

   cursor C_GET_ORDER_NO IS
      SELECT sh.order_no
        FROM shipment sh,
             ordhead oh
       WHERE sh.order_no      = oh.order_no
         AND sh.ext_ref_no_in = I_ext_receipt_no
         AND sh.to_loc        = I_store
         AND sh.to_loc_type   = 'S'
         AND oh.supplier      = I_supplier;

BEGIN

   open C_GET_ORDER_NO;
   fetch C_GET_ORDER_NO into O_order_no;
   if O_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_ORD_FOR_RCPT', I_ext_receipt_no, NULL, NULL);
      return FALSE;
   end if;
   close C_GET_ORDER_NO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_ORDER_NO;
-----------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status               IN OUT  VARCHAR2,
                        IO_error_message       IN OUT  VARCHAR2,
                        I_cause                IN      VARCHAR2,
                        I_program              IN      VARCHAR2)
IS
BEGIN
   API_LIBRARY.HANDLE_ERRORS(O_status,
                             IO_error_message,
                             I_cause,
                             I_program);
EXCEPTION
   when OTHERS then
      IO_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_DSD.HANDLE_ERRORS',
                                             to_char(SQLCODE));

     API_LIBRARY.HANDLE_ERRORS(O_status,
                               IO_error_message,
                               API_LIBRARY.FATAL_ERROR,
                               'RMSSUB_DSD.HANDLE_ERRORS');

END HANDLE_ERRORS;
-----------------------------------------------------------------------------------------
END RMSSUB_DSD;
/
