create or replace PACKAGE BODY DISTRIBUTE_LOCATION_SQL AS
---------------------------------------------------------------------------------------------
-- Function Name: FINISH_ORDER
-- Purpose:       This function will distribute an item by location on the basis of quantity,
--                percentage or ratio.It will update the orderloc_wksht table based on  distribution
--                information present in the LOCATION_DIST_TEMP table.
---------------------------------------------------------------------------------------------
FUNCTION FINISH_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                      I_dist_uom_type   IN       ORDLOC_WKSHT.UOP%TYPE,
                      I_dist_uom        IN       ORDLOC_WKSHT.UOP%TYPE,
                      I_purch_uom       IN       ORDLOC_WKSHT.UOP%TYPE,
                      I_distribute_by   IN       VARCHAR2)
RETURN BOOLEAN is
   L_statement VARCHAR2(9000);
   L_where_clause FILTER_TEMP.WHERE_CLAUSE%TYPE;
   L_ratio            NUMBER;
   L_sum_ratio        VARCHAR2(30);
   L_qty_string       VARCHAR2(2000);
   L_calc_qty_string  VARCHAR2(2000);
   L_act_qty_string   VARCHAR2(2000);
   L_var_qty_string   VARCHAR2(2000);
   L_wksht_qty_string VARCHAR2(2000);
   L_r_ratio          VARCHAR2(30);
   L_dividend         ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_new_wksht_qty    ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_exists           BOOLEAN;
   L_uom_class        UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor      ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_standard_uom     UOM_CLASS.UOM%TYPE;
   L_item             ORDLOC_WKSHT.ITEM%TYPE;
   L_item_parent      ORDLOC_WKSHT.ITEM_PARENT%TYPE;
   L_order_item       ORDLOC_WKSHT.ITEM%TYPE;
   L_program VARCHAR2(100) :='DISTRIBUTE_LOCATION_SQL.FINISH_ORDER';

   cursor C_RATIO is
     Select sum(nvl(dist_ratio,0))
       from location_dist_temp
      where order_no = I_order_no;

   cursor C_FETCH_WHERE is
     select where_clause
       from filter_temp
      where lower(form_name) = 'ordmtxws'
        and unique_key  = I_order_no;

   cursor C_GET_ITEM is
     select item,
            item_parent
       from ordloc_wksht
      where order_no = I_order_no
        and rownum = 1;

BEGIN
   ---
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_dist_uom_type is NULL then
      O_error_message  := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_dist_uom_type',
                                              L_program,
                                              NULL);
      return FALSE;
   end if;
   ---
   if I_dist_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dist_uom', 
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_purch_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_purch_uom',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_distribute_by is NULL then
      O_error_message  := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_distribute_by',
                                              L_program,
                                              NULL);
      return FALSE;
   end if; 
   ---
  
   ---
   open C_FETCH_WHERE;
   fetch C_FETCH_WHERE INTO L_where_clause;
   close C_FETCH_WHERE;
   ---

   ---
   open C_RATIO;
   fetch C_RATIO INTO L_ratio;
   close C_RATIO;
   ---

   ---
   open C_GET_ITEM;
   fetch C_GET_ITEM into L_item,
                         L_item_parent;
   close C_GET_ITEM;

   if L_item is NOT NULL then
      L_order_item := L_item;
   else
      L_order_item := L_item_parent;
   end if;
   ---
   
   L_sum_ratio  := TO_CHAR(L_ratio);

   if L_where_clause is NOT NULL then
      L_where_clause  := 'and '||L_where_clause;
   end if;
   ---
   if I_dist_uom_type= 'C' then
      if I_distribute_by     = 'Q' then
         L_qty_string       := 'location_dist_temp.dist_qty ';
      elsif I_distribute_by  = 'R' then
         L_qty_string       := '((location_dist_temp.dist_ratio * ordloc_wksht.wksht_qty)/ '||L_sum_ratio||')';
      elsif I_distribute_by  = 'D' then
         L_qty_string       := '((location_dist_temp.dist_pct * ordloc_wksht.wksht_qty)/100) ';
      end if;
      ---
      L_wksht_qty_string := 'ROUND('||L_qty_string||')';
      L_calc_qty_string  := '('||L_qty_string ||' * ordloc_wksht.supp_pack_size)';
      L_act_qty_string   := '('||L_wksht_qty_string ||' * ordloc_wksht.supp_pack_size)';
   else
      if I_dist_uom = I_purch_uom then 
         if I_distribute_by    = 'Q' then
            L_qty_string       := 'location_dist_temp.dist_qty ';
         elsif I_distribute_by = 'R' then
            L_qty_string       := '((location_dist_temp.dist_ratio * ordloc_wksht.wksht_qty)/ '||L_sum_ratio||')';
         elsif I_distribute_by = 'D' then
            L_qty_string       := '((location_dist_temp.dist_pct * ordloc_wksht.wksht_qty)/100) ';
         end if;
         --
         if UOM_SQL.GET_CLASS(O_error_message,
                              L_uom_class,
                              I_purch_uom) = FALSE then
            return FALSE;
         end if;

         if L_uom_class = 'MASS' then
            L_wksht_qty_string := 'ROUND('||L_qty_string||', 4)';
         else
            L_wksht_qty_string := 'ROUND('||L_qty_string||')';
         end if;

         L_calc_qty_string  := L_qty_string;
         L_act_qty_string   := L_qty_string;

      elsif I_dist_uom != I_purch_uom then
         if I_distribute_by    = 'Q' then
            L_qty_string       := 'location_dist_temp.dist_qty ';
         elsif I_distribute_by = 'R' then
            L_qty_string       := '((location_dist_temp.dist_ratio * ordloc_wksht.act_qty)/ '||L_sum_ratio||')';
         elsif I_distribute_by = 'D' then
            L_qty_string       := '((location_dist_temp.dist_pct * ordloc_wksht.act_qty)/100) ';
         end if;

         if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                             L_standard_uom,
                                             L_uom_class,
                                             L_conv_factor,
                                             L_order_item,
                                             'Y') = FALSE then
            return FALSE;
         end if;

         if L_uom_class = 'MASS' then
            L_wksht_qty_string := 'ROUND(('||L_qty_string||')/ordloc_wksht.supp_pack_size,4)';
         else
            L_wksht_qty_string := 'ROUND(('||L_qty_string||')/ordloc_wksht.supp_pack_size)';
         end if;

         L_calc_qty_string  := L_qty_string;
         L_act_qty_string   := L_qty_string;
      end if; 
   end if;
   ---
   L_var_qty_string := '((('||L_act_qty_string ||'-'||L_calc_qty_string ||')*100)/'||'DECODE('||L_calc_qty_string||',0,1)'||')';   
   if I_distribute_by  = 'R' then
      if UPDATE_OBJECT_SQL.UPDATE_ORDER_LOC(O_error_message,
                                        I_order_no,
                                        I_distribute_by,
                                        L_sum_ratio,
                                        L_where_clause,
                                        I_dist_uom_type) = FALSE then
          return FALSE;
      end if;
   elsif I_distribute_by  = 'Q' then
      if UPDATE_OBJECT_SQL.UPDATE_ORDER_LOC(O_error_message,
                                            I_order_no,
                                            'Q',
                                            NULL,
                                            L_where_clause, 
                                            I_dist_uom_type) = FALSE then
        return FALSE;
      end if;
   elsif I_distribute_by   = 'D' then
      if UPDATE_OBJECT_SQL.UPDATE_ORDER_LOC(O_error_message,
                                            I_order_no,
                                            I_distribute_by,
                                            NULL,
                                            L_where_clause,
                                            I_dist_uom_type) = FALSE then
        return FALSE;
      end if;
   end if;

   L_statement := 'insert into ordloc_wksht(order_no, '|| 'item_parent, '|| 'item,
                                            '|| 'ref_item, '|| 'diff_1, '|| 'diff_2, '|| 'diff_3, '|| 'diff_4,
                                            '|| 'store_grade, '|| 'store_grade_group_id, '|| 'loc_type,
                                            '|| 'location, '|| 'calc_qty, '|| 'act_qty, '|| 'standard_uom,
                                            '|| 'variance_qty, '|| 'origin_country_id, '|| 'wksht_qty,
                                            '|| 'uop, '|| 'supp_pack_size) '|| 'select ordloc_wksht.order_no,
                                            '|| 'ordloc_wksht.item_parent, '|| 'ordloc_wksht.item, '|| 'ordloc_wksht.ref_item,
                                            '|| 'ordloc_wksht.diff_1, '|| 'ordloc_wksht.diff_2, '|| 'ordloc_wksht.diff_3,
                                            '|| 'ordloc_wksht.diff_4, '|| 'ordloc_wksht.store_grade, '|| 'ordloc_wksht.store_grade_group_id,
                                            '|| 'location_dist_temp.loc_type, '|| 'location_dist_temp.location,
                                            '|| L_calc_qty_string ||',' || L_act_qty_string ||',' || 'ordloc_wksht.standard_uom,
                                             '|| L_var_qty_string ||',' || 'ordloc_wksht.origin_country_id,
                                             '|| L_wksht_qty_string||',' || 'ordloc_wksht.uop, ' || 'ordloc_wksht.supp_pack_size '||
                                            'from ordloc_wksht, location_dist_temp '|| 'where ordloc_wksht.order_no = location_dist_temp.order_no '|| 'and 
                                             ordloc_wksht.order_no = '||TO_CHAR(I_order_no)||' ' || L_where_clause||' '|| 'and
                                             not exists (select ' || '''x''' || ' from ordloc_wksht o2 ' || 'where o2.order_no = ordloc_wksht.order_no 
                                             ' || 'and nvl(o2.item,-1) = nvl(ordloc_wksht.item,-1) '|| 'and nvl(o2.item_parent,-1) = nvl(ordloc_wksht.item_parent,-1) 
                                             '|| 'and nvl(o2.diff_1,-1) = nvl(ordloc_wksht.diff_1,-1) ' || 'and nvl(o2.diff_2,-1) = nvl(ordloc_wksht.diff_2,-1) 
                                             ' || 'and nvl(o2.diff_3,-1) = nvl(ordloc_wksht.diff_3,-1) ' || 'and nvl(o2.diff_4,-1) = nvl(ordloc_wksht.diff_4,-1) 
                                             ' || 'and nvl(o2.store_grade_group_id,-1) = nvl(ordloc_wksht.store_grade_group_id,-1) 
                                             ' || 'and nvl(o2.store_grade,-1) = nvl(ordloc_wksht.store_grade,-1) 
                                             ' || 'and o2.loc_type = location_dist_temp.loc_type ' || 'and o2.location = location_dist_temp.location);';
   if EXECUTE_SQL.EXECUTE_SQL(O_error_message,
                              L_statement) = FALSE then
      return FALSE;
   end if;
---
   L_statement := 'delete from ordloc_wksht where order_no = '||TO_CHAR(I_order_no)|| ' and ordloc_wksht.location is NULL '|| L_where_clause ||';';
   if EXECUTE_SQL.EXECUTE_SQL(O_error_message,
                              L_statement) = FALSE then
      return FALSE;
   end if; 

   DELETE FROM location_dist_temp;   

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
       return FALSE;
END FINISH_ORDER;
-------------------------------------------------------------------------------------------
FUNCTION FINISH_CONTRACT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_contract_no     IN       CONTRACT_HEADER.CONTRACT_NO%TYPE,
                         I_distribute_by   IN       VARCHAR2)
RETURN BOOLEAN  is
   
   L_dist_pct location_dist_temp.dist_pct%TYPE     := NULL;
   L_total_pct location_dist_temp.dist_pct%TYPE    := 0;
   L_ratio location_dist_temp.dist_ratio%TYPE      := NULL;
   L_record    VARCHAR2(1)                         := NULL;
   L_statement VARCHAR2(9000)                      := NULL;
   L_where_clause FILTER_TEMP.WHERE_CLAUSE%TYPE    := NULL;
   L_exists  BOOLEAN;
   L_program VARCHAR2(100)                          :='DISTRIBUTE_LOCATION_SQL.FINISH_CONTRACT';

   cursor C_WHERE_CLAUSE is
      select f.where_clause
        from filter_temp f
       where f.unique_key = I_contract_no
         and lower(f.form_name)  = 'cntrdist';
  
   cursor C_RATIO is
      select sum(dist_ratio)
        from location_dist_temp
       where contract_no = I_contract_no;

BEGIN
  ---
   if I_contract_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_contract_no', 
                                             L_program, 
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_distribute_by is NULL then
     O_error_message  := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_distribute_by', 
                                             L_program,
                                             NULL);
      return FALSE;
   end if; 
  ---
   open c_where_clause;
   fetch c_where_clause INTO L_where_clause;
   close c_where_clause;
---
   if L_where_clause is NOT NULL then
      L_where_clause  := 'and '||L_where_clause;
   end if;

   if I_distribute_by = 'Q' then
      if UPDATE_OBJECT_SQL.UPDATE_CONTRACT_LOC(O_error_message,
                                                I_contract_no,
                                                'Q',
                                                NULL,
                                                L_where_clause) = FALSE then
         return FALSE;
      end if;
      L_statement := 'insert into CONTRACT_MATRIX_TEMP(contract_no, item_grandparent, item_parent, item,
                                                       ' || 'ref_item, diff_1, diff_2, diff_3, diff_4, '|| 'loc_type, location, ready_date,
                                                       qty,unit_cost) '|| 'select CONTRACT_MATRIX_TEMP.contract_no, CONTRACT_MATRIX_TEMP.item_grandparent, '|| 'CONTRACT_MATRIX_TEMP.item_parent,
                                                        '|| 'CONTRACT_MATRIX_TEMP.item, CONTRACT_MATRIX_TEMP.ref_item, '|| 'CONTRACT_MATRIX_TEMP.diff_1, CONTRACT_MATRIX_TEMP.diff_2,
                                                        '|| 'CONTRACT_MATRIX_TEMP.diff_3, CONTRACT_MATRIX_TEMP.diff_4, '|| 'LOCATION_DIST_TEMP.loc_type, LOCATION_DIST_TEMP.location,
                                                        '|| 'CONTRACT_MATRIX_TEMP.ready_date, TRUNC(LOCATION_DIST_TEMP.dist_qty),
                                                        '|| 'CONTRACT_MATRIX_TEMP.unit_cost '|| 'from CONTRACT_MATRIX_TEMP, LOCATION_DIST_TEMP
                                                        '|| 'where CONTRACT_MATRIX_TEMP.contract_no = '||TO_CHAR(I_contract_no)||' '|| 'and LOCATION_DIST_TEMP.contract_no = '||TO_CHAR(I_contract_no)||' '||L_where_clause||' '|| '
                                                        and not exists (select ''x'' from contract_matrix_temp c2 ' ||
                                                        'where c2.contract_no = contract_matrix_temp.contract_no ' || 
                                                        'and nvl(c2.item, -1) = nvl(contract_matrix_temp.item,-1) 
                                                        '|| 'and nvl(c2.item_parent,-1) = nvl(contract_matrix_temp.item_parent,-1) '|| 
                                                        'and nvl(c2.item_grandparent,-1) = nvl(contract_matrix_temp.item_grandparent,-1) 
                                                       '|| 'and nvl(c2.diff_1,-1) = nvl(contract_matrix_temp.diff_1,-1) ' || 'and nvl(c2.diff_2,-1) = nvl(contract_matrix_temp.diff_2,-1) 
                                                       ' || 'and nvl(c2.diff_3,-1) = nvl(contract_matrix_temp.diff_3,-1) ' || 'and nvl(c2.diff_4,-1) = nvl(contract_matrix_temp.diff_4,-1) 
                                                       ' || 'and (c2.ready_date = contract_matrix_temp.ready_date or ' || '(c2.ready_date is NULL and contract_matrix_temp.ready_date is NULL)) 
                                                        '|| 'and c2.loc_type = location_dist_temp.loc_type '|| 'and c2.location = location_dist_temp.location);';
     
   elsif I_distribute_by  = 'D' then
      if UPDATE_OBJECT_SQL.UPDATE_CONTRACT_LOC(O_error_message, 
                                               I_contract_no,
                                               'D', 
                                               NULL,
                                               L_where_clause) = FALSE then  
          return FALSE;
      end if;

      L_statement := 'insert into CONTRACT_MATRIX_TEMP(contract_no, item_grandparent, item_parent, item, ref_item,
                                                      diff_1, diff_2, diff_3, diff_4, '|| 'loc_type, location, ready_date,
                                                      qty,unit_cost) '|| 'select CONTRACT_MATRIX_TEMP.contract_no,
                                                      CONTRACT_MATRIX_TEMP.item_grandparent, '|| 'CONTRACT_MATRIX_TEMP.item_parent,
                                                      ' || 'CONTRACT_MATRIX_TEMP.item, CONTRACT_MATRIX_TEMP.ref_item, '|| 'CONTRACT_MATRIX_TEMP.diff_1,
                                                      CONTRACT_MATRIX_TEMP.diff_2, '|| 'CONTRACT_MATRIX_TEMP.diff_3,
                                                      CONTRACT_MATRIX_TEMP.diff_4, '|| 'LOCATION_DIST_TEMP.loc_type,
                                                      '|| 'LOCATION_DIST_TEMP.location, CONTRACT_MATRIX_TEMP.ready_date,
                                                      '|| 'TRUNC((CONTRACT_MATRIX_TEMP.qty * LOCATION_DIST_TEMP.dist_pct)/100),
                                                      '|| 'CONTRACT_MATRIX_TEMP.unit_cost '|| 'from CONTRACT_MATRIX_TEMP,
                                                      LOCATION_DIST_TEMP '|| 'where CONTRACT_MATRIX_TEMP.contract_no = '||TO_CHAR(I_contract_no)||' '|| 'and LOCATION_DIST_TEMP.contract_no = '||TO_CHAR(I_contract_no)||' '||L_where_clause||' '|| 'and not exists (select ''x'' from contract_matrix_temp c2 ' ||
                                                       'where c2.contract_no = contract_matrix_temp.contract_no
                                                       ' || 'and nvl(c2.item,-1) = nvl(contract_matrix_temp.item,-1)
                                                       '|| 'and nvl(c2.item_parent,-1) = nvl(contract_matrix_temp.item_parent,-1)
                                                       '|| 'and nvl(c2.item_grandparent,-1) = nvl(contract_matrix_temp.item_grandparent,-1) '|| 'and nvl(c2.diff_1,-1) = nvl(contract_matrix_temp.diff_1,-1) ' || 'and nvl(c2.diff_2,-1) = nvl(contract_matrix_temp.diff_2,-1) ' || 'and nvl(c2.diff_3,-1) = nvl(contract_matrix_temp.diff_3,-1) ' || 'and nvl(c2.diff_4,-1) = nvl(contract_matrix_temp.diff_4,-1) ' || 'and (c2.ready_date = contract_matrix_temp.ready_date or ' || '(c2.ready_date is NULL and contract_matrix_temp.ready_date is NULL)) '|| 'and c2.loc_type = location_dist_temp.loc_type '|| 'and c2.location = location_dist_temp.location);';  elsif I_distribute_by = 'R' then
      

      open c_ratio;
      fetch c_ratio INTO l_ratio;
      close c_ratio;
      --
      if UPDATE_OBJECT_SQL.UPDATE_CONTRACT_LOC(O_error_message,
                                                I_contract_no,
                                                'R',
                                                L_ratio,
                                                L_where_clause) = FALSE then
         return FALSE;
      end if;
      
      L_statement :='insert into CONTRACT_MATRIX_TEMP(contract_no, item_grandparent, item_parent, item, ref_item, diff_1, diff_2, diff_3,
                                                       diff_4, '|| 'loc_type,location, ready_date,qty,unit_cost) '|| 'select CONTRACT_MATRIX_TEMP.contract_no,
                                                       CONTRACT_MATRIX_TEMP.item_grandparent, '|| 'CONTRACT_MATRIX_TEMP.item_parent, ' || 'CONTRACT_MATRIX_TEMP.item,
                                                       CONTRACT_MATRIX_TEMP.ref_item, '|| 'CONTRACT_MATRIX_TEMP.diff_1, CONTRACT_MATRIX_TEMP.diff_2, '|| 'CONTRACT_MATRIX_TEMP.diff_3,
                                                       CONTRACT_MATRIX_TEMP.diff_4, '|| 'LOCATION_DIST_TEMP.loc_type, '|| 'LOCATION_DIST_TEMP.location,
                                                       CONTRACT_MATRIX_TEMP.ready_date,'|| 'TRUNC((CONTRACT_MATRIX_TEMP.qty/'||TO_CHAR(L_ratio)||') 
                                                       * LOCATION_DIST_TEMP.dist_ratio),'|| 'CONTRACT_MATRIX_TEMP.unit_cost '|| 'from CONTRACT_MATRIX_TEMP,
                                                         LOCATION_DIST_TEMP '|| 'where CONTRACT_MATRIX_TEMP.contract_no = '||TO_CHAR(I_contract_no)||' '|| 'and 
                                                         LOCATION_DIST_TEMP.contract_no = '||TO_CHAR(I_contract_no)||' '||L_where_clause||' '||
                                                        'and not exists (select ''x'' from contract_matrix_temp c2 ' || 'where c2.contract_no = contract_matrix_temp.contract_no ' || 'and nvl(c2.item,-1) = nvl(contract_matrix_temp.item,-1) 
                                                        '|| 'and nvl(c2.item_parent,-1) = nvl(contract_matrix_temp.item_parent,-1) 
                                                        '|| 'and nvl(c2.item_grandparent,-1) = nvl(contract_matrix_temp.item_grandparent,-1) 
                                                        '|| 'and nvl(c2.diff_1,-1) = nvl(contract_matrix_temp.diff_1,-1) ' || 'and nvl(c2.diff_2,-1) = nvl(contract_matrix_temp.diff_2,-1) ' || 'and nvl(c2.diff_3,-1) = nvl(contract_matrix_temp.diff_3,-1) 
                                                       ' || 'and nvl(c2.diff_4,-1) = nvl(contract_matrix_temp.diff_4,-1) ' || 'and (c2.ready_date = contract_matrix_temp.ready_date or ' || '(c2.ready_date is NULL and contract_matrix_temp.ready_date is NULL)) '|| 'and c2.loc_type = location_dist_temp.loc_type '|| 'and c2.location = location_dist_temp.location);';
   end if;

   if EXECUTE_SQL.EXECUTE_SQL(O_error_message, 
                               L_statement) = FALSE then
      return FALSE;
   end if;

   L_statement   := 'delete from contract_matrix_temp where contract_no = '||TO_CHAR(I_contract_no)||' '|| 'and location is NULL '||L_where_clause||';';
   
   if EXECUTE_SQL.EXECUTE_SQL(O_error_message, 
                              L_statement) = FALSE then
      return FALSE;
   end if;

   DELETE FROM LOCATION_DIST_TEMP;

   if CONTRACT_SQL.LOCK_CONTRACT(O_error_message,
                                  I_contract_no) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END FINISH_CONTRACT;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_error_type      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_country_id      IN OUT   COUNTRY.COUNTRY_ID%TYPE,
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                           I_row_number      IN       VARCHAR2,
                           I_contract_no     IN       ORDHEAD.CONTRACT_NO%TYPE,
                           I_loc_type        IN       LOCATION_DIST_TEMP.LOC_TYPE%TYPE,
                           I_location        IN       LOCATION_DIST_TEMP.LOCATION%TYPE,
                           I_supplier        IN       ITEM_SUPPLIER.SUPPLIER%TYPE,
                           I_physical_wh     IN       V_WH.PHYSICAL_WH%TYPE,
                           I_before_date     IN       DATE,
                           I_after_date      IN       DATE)
RETURN BOOLEAN  is

   L_stockholding_ind     WH.STOCKHOLDING_IND%TYPE;
   L_exists               BOOLEAN;
   L_valid                BOOLEAN;
   L_wh                   V_WH%ROWTYPE;
   L_location_name        WH.WH_NAME%TYPE;
   L_loc_country_id       COUNTRY.COUNTRY_ID%TYPE          := NULL;
   L_import_id            ORDHEAD.IMPORT_ID%TYPE           := NULL;
   L_import_type          ORDHEAD.IMPORT_TYPE%TYPE         := NULL;
   L_import_country_id    ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   L_contract_no          ORDHEAD.CONTRACT_NO%TYPE         := NULL;
   L_store_row            STORE%ROWTYPE;
   L_exists_store         BOOLEAN;
   L_loc_exists           BOOLEAN;
   L_program              VARCHAR2(100)                    :='DISTRIBUTE_LOCATION_SQL.VALIDATE_LOCATION';
   L_set_of_books_desc    FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE;
   L_set_of_books_id      FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE;
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_sys_import_ind       VARCHAR2(1)                      := 'N';

   cursor C_GET_ORD_INFO is
      select import_country_id,
             contract_no
        from ordhead
       where order_no = I_order_no;

BEGIN

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_location',
                                              L_program,
                                              NULL);
      return FALSE;
   end if;

   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_location is NOT NULL then

      if LOCATION_ATTRIB_SQL.GET_LOC_COUNTRY(O_error_message,
                                              L_loc_country_id,
                                              I_location) = FALSE then
         return FALSE;
      end if;

      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_row) = FALSE then
         return FALSE;
      end if;

      L_sys_import_ind := L_system_options_row.import_ind;

      if I_loc_type = 'W' then
         if not FILTER_LOV_VALIDATE_SQL.VALIDATE_WH(O_error_message,
                                                     L_valid,
                                                     L_wh,
                                                     I_location) or not L_valid then
            return true;
         end if;
      elsif not FILTER_LOV_VALIDATE_SQL.VALIDATE_LOCATION(O_error_message,
                                                          L_valid,
                                                          L_location_name,
                                                          L_stockholding_ind,
                                                          I_location) OR NOT L_valid then
         return FALSE;
      end if;

      if  L_stockholding_ind = 'N' OR L_wh.stockholding_ind = 'N' then
          O_error_message    := 'STOCKHOLD_STORE_WAREHOUSE';
          return FALSE;
      end if;

      if I_loc_type = 'S' then
         if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                      L_exists_store,
                                      L_store_row,
                                      I_location) = FALSE then
            return FALSE;
         end if;
         if L_store_row.store_type   = 'F' then
            O_error_message         :='F_STORE_INVALID';
            return FALSE;
         end if;
      end if;

      if I_physical_wh is NOT NULL then
         if WH_ATTRIB_SQL.VWH_EXISTS_IN_PWH(O_error_message,
                                             L_exists,
                                             I_location,
                                             I_physical_wh) = FALSE then
            return FALSE;
         end if;
         if L_exists = FALSE then
            O_error_message := 'INV_VWH_FOR_PWH2';
            return FALSE;
         end if;
      end if;

      if I_order_no is not NULL then
         if ORDER_SQL.GET_DEFAULT_IMP_EXP(O_error_message,
                                          L_import_id,
                                          L_import_type,
                                          I_order_no) = FALSE then
            return FALSE;
         end if;
      end if;

      if L_system_options_row.org_unit_ind = 'Y' then
         if L_import_id is NULL then
            if SET_OF_BOOKS_SQL.CHECK_SUPP_SINGLE_LOC(O_error_message,
                                                      L_exists,
                                                      I_supplier,
                                                      I_location,
                                                      I_loc_type) = FALSE then
               return FALSE;
            end if;

            if L_exists = FALSE then
               return FALSE;
            end if;
         end if;

         if SET_OF_BOOKS_SQL.GET_SOB_LOC(O_error_message,
                                         L_set_of_books_desc,
                                         L_set_of_books_id,
                                         I_location,
                                         I_loc_type) = FALSE then
           return FALSE;
         end if;
      end if;

      if I_loc_type  = 'S' then
         if CHECK_STORE_STATUS(O_error_message,
                                O_error_type,
                                I_location,
                                I_before_date,
                                I_after_date) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_order_no is NOT NULL then
         if ORDER_ITEM_ATTRIB_SQL.ORDLOC_EXISTS(O_error_message,
                                                L_loc_exists,
                                                I_order_no) = FALSE then
            return FALSE;
         end if;

         if I_country_id <> L_loc_country_id then

            open C_GET_ORD_INFO;
            fetch C_GET_ORD_INFO into L_import_country_id,
                                      L_contract_no;
            close C_GET_ORD_INFO;

            if L_sys_import_ind = 'Y' or L_contract_no is NOT NULL then
               O_error_message := 'LOC_COUNTRY';
               return FALSE;
            else
               if L_loc_exists or I_row_number > '1' then
                  if L_import_country_id != '99' then
                     if ORDER_SETUP_SQL.UPDATE_IMPORT_COUNTRY(O_error_message,
                                                              I_order_no,
                                                              '99') = FALSE then
                        return FALSE;
                     end if;
                  end if;
               elsif I_row_number = '1' then
                  I_country_id := L_loc_country_id;
                  if ORDER_SETUP_SQL.UPDATE_IMPORT_COUNTRY(O_error_message,
                                                           I_order_no,
                                                           L_loc_country_id) = FALSE then
                     return FALSE;
                  end if;
               end if;  --if L_loc_exists or I_row_number > '1'
            end if;  -- if L_sys_import_ind = Y
         end if; --if I_country_id <> L_loc_country_id
      end if;  --if I_order_no is NOT NULL

   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_LOCATION;
-----------------------------------------------------------------------------
FUNCTION CHECK_STORE_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error_type      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_store           IN       STORE.STORE%TYPE ,
                            I_before_date     IN       DATE,
                            I_after_date      IN       DATE)
RETURN BOOLEAN  is
   L_nbd_open_close_ind VARCHAR2(1);
   L_nad_open_close_ind VARCHAR2(1);
   L_error_message      VARCHAR2(255);
   L_program            VARCHAR2(300) := 'DISTRIBUTE_LOCATION_SQL.CHECK_STORE_STATUS';

BEGIN

   if STORE_ATTRIB_SQL.ACTIVE(I_store,
                              I_before_date,
                              L_nbd_open_close_ind, 
                              O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if L_nbd_open_close_ind = 'C' then
      return FALSE;
   end if;
   
   if STORE_ATTRIB_SQL.ACTIVE(I_store,
                              I_after_date,
                              L_nad_open_close_ind,
                              O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if L_nad_open_close_ind = 'C' then
      return FALSE;
   end if;
   if L_nbd_open_close_ind = 'N' then
      O_error_message      :='Confirm';
      return TRUE;
   end if;
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR', 
                                              SQLERRM, 
                                              L_program,
                                              TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_STORE_STATUS;
-----------------------------------------------------------------------------
END;
/