CREATE OR REPLACE PACKAGE BODY PayTermServiceProviderImpl AS
--------------------------------------------------------------------------------
-- This package is CREATED using SPECS FROM PayTermServiceProviderImplSpec.sql  
--------------------------------------------------------------------------------
PROCEDURE createPayTermDesc(I_serviceOperationContext  IN      "RIB_ServiceOpContext_REC",
                            I_businessObject           IN      "RIB_PayTermDesc_REC",
                            O_serviceOperationStatus      OUT  "RIB_ServiceOpStatus_REC",
                            O_businessObject              OUT  "RIB_PayTermRef_REC")
IS

   L_program              VARCHAR2(65) := 'PayTermServiceProviderImpl.createPayTermDesc';
   L_message_type         VARCHAR2(15) := RMSAIASUB_PAYTERM.HDR_ADD;
   L_status_code          VARCHAR2(1);
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   RMSAIASUB_PAYTERM.CONSUME(L_status_code,
                             L_error_message,
                             O_businessObject,
                             I_businessObject,
                             L_message_type);

   RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                      L_status_code,
                                      L_error_message,
                                      L_program);

EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));

      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END createPayTermDesc;

--------------------------------------------------------------------------------
-- NOTE: The createHeaderPayTermDesc procedure call is not currently supported.
--------------------------------------------------------------------------------
PROCEDURE createHeaderPayTermDesc(I_serviceOperationContext  IN      "RIB_ServiceOpContext_REC",
                                  I_businessObject           IN      "RIB_PayTermDesc_REC",
                                  O_serviceOperationStatus      OUT  "RIB_ServiceOpStatus_REC",
                                  O_businessObject              OUT  "RIB_PayTermRef_REC")
IS

   L_program              VARCHAR2(65) := 'PayTermServiceProviderImpl.createHeaderPayTermDesc';
   L_status_code          VARCHAR2(1)  := NULL;
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceOperationStatus,
                                        L_error_message,
                                        L_program);

EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));

      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END createHeaderPayTermDesc;

--------------------------------------------------------------------------------
PROCEDURE createDetailPayTermDesc(I_serviceOperationContext  IN      "RIB_ServiceOpContext_REC",
                                  I_businessObject           IN      "RIB_PayTermDesc_REC",
                                  O_serviceOperationStatus      OUT  "RIB_ServiceOpStatus_REC",
                                  O_businessObject              OUT  "RIB_PayTermRef_REC")
IS

   L_program              VARCHAR2(65) := 'PayTermServiceProviderImpl.createDetailPayTermDesc';
   L_status_code          VARCHAR2(1);
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_message_type         VARCHAR2(15) := RMSAIASUB_PAYTERM.DTL_ADD;

BEGIN

   RMSAIASUB_PAYTERM.CONSUME(L_status_code,
                             L_error_message,
                             O_businessObject,
                             I_businessObject,
                             L_message_type);

   RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                      L_status_code,
                                      L_error_message,
                                      L_program);

EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));

      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END createDetailPayTermDesc;

--------------------------------------------------------------------------------
PROCEDURE updatePayTermDesc(I_serviceOperationContext  IN      "RIB_ServiceOpContext_REC",
                            I_businessObject           IN      "RIB_PayTermDesc_REC",
                            O_serviceOperationStatus      OUT  "RIB_ServiceOpStatus_REC",
                            O_businessObject              OUT  "RIB_PayTermDesc_REC")
IS

   L_program              VARCHAR2(65) := 'PayTermServiceProviderImpl.updatePayTermDesc';
   L_status_code          VARCHAR2(1);
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_message_type         VARCHAR2(15) := RMSAIASUB_PAYTERM.DTL_UPD;
   L_RIB_PayTermRef_REC   "RIB_PayTermRef_REC";

BEGIN

   RMSAIASUB_PAYTERM.CONSUME(L_status_code,
                             L_error_message,
                             L_RIB_PayTermRef_REC,
                             I_businessObject,
                             L_message_type);

   -- For update messages send back a RIB_PayTermDesc_REC, so use I_businessObject.
   O_businessObject := I_businessObject;

   RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                      L_status_code,
                                      L_error_message,
                                      L_program);

EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));

      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END updatePayTermDesc;

--------------------------------------------------------------------------------
PROCEDURE updateHeaderPayTermDesc(I_serviceOperationContext  IN      "RIB_ServiceOpContext_REC",
                                  I_businessObject           IN      "RIB_PayTermDesc_REC",
                                  O_serviceOperationStatus      OUT  "RIB_ServiceOpStatus_REC",
                                  O_businessObject              OUT  "RIB_PayTermDesc_REC")
IS

   L_program              VARCHAR2(65) := 'PayTermServiceProviderImpl.updateHeaderPayTermDesc';
   L_status_code          VARCHAR2(1);
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_message_type         VARCHAR2(15) := RMSAIASUB_PAYTERM.HDR_UPD;
   L_RIB_PayTermRef_REC   "RIB_PayTermRef_REC";

BEGIN


   RMSAIASUB_PAYTERM.CONSUME(L_status_code,
                             L_error_message,
                             L_RIB_PayTermRef_REC,
                             I_businessObject,
                             L_message_type);

   -- For update messages send back a RIB_PayTermDesc_REC, so use I_businessObject.
   O_businessObject := I_businessObject;

   RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                      L_status_code,
                                      L_error_message,
                                      L_program);

EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));

      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END updateHeaderPayTermDesc;

--------------------------------------------------------------------------------
PROCEDURE updateDetailPayTermDesc(I_serviceOperationContext  IN      "RIB_ServiceOpContext_REC",
                                  I_businessObject           IN      "RIB_PayTermDesc_REC",
                                  O_serviceOperationStatus      OUT  "RIB_ServiceOpStatus_REC",
                                  O_businessObject              OUT  "RIB_PayTermDesc_REC")
IS

   L_program              VARCHAR2(65) := 'PayTermServiceProviderImpl.updateDetailPayTermDesc';
   L_status_code          VARCHAR2(1);
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_message_type         VARCHAR2(15) := RMSAIASUB_PAYTERM.DTL_UPD;
   L_RIB_PayTermRef_REC   "RIB_PayTermRef_REC";

BEGIN

   RMSAIASUB_PAYTERM.CONSUME(L_status_code,
                             L_error_message,
                             L_RIB_PayTermRef_REC,
                             I_businessObject,
                             L_message_type);

   -- For update messages send back a RIB_PayTermDesc_REC, so use I_businessObject.
   O_businessObject := I_businessObject;

   RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                      L_status_code,
                                      L_error_message,
                                      L_program);

EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));

      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END updateDetailPayTermDesc;

--------------------------------------------------------------------------------
-- NOTE: The findPayTermDesc procedure call is not currently supported.
--------------------------------------------------------------------------------
PROCEDURE findPayTermDesc(I_serviceOperationContext  IN      "RIB_ServiceOpContext_REC",
                          I_businessObject           IN      "RIB_PayTermRef_REC",
                          O_serviceOperationStatus      OUT  "RIB_ServiceOpStatus_REC",
                          O_businessObject              OUT  "RIB_PayTermDesc_REC")
IS

   L_program              VARCHAR2(65) := 'PayTermServiceProviderImpl.findPayTermDesc';
   L_status_code          VARCHAR2(1)  := NULL;
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
 
BEGIN

   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceOperationStatus,
                                        L_error_message,
                                        L_program);

EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));

      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END findPayTermDesc;

--------------------------------------------------------------------------------
-- NOTE: The deletePayTermDesc procedure call is not currently supported.
--------------------------------------------------------------------------------
PROCEDURE deletePayTermDesc(I_serviceOperationContext  IN      "RIB_ServiceOpContext_REC",
                            I_businessObject           IN      "RIB_PayTermRef_REC",
                            O_serviceOperationStatus      OUT  "RIB_ServiceOpStatus_REC",
                            O_businessObject              OUT  "RIB_PayTermRef_REC")
IS

   L_program              VARCHAR2(65) := 'PayTermServiceProviderImpl.deletePayTermDesc';
   L_status_code          VARCHAR2(1)  := NULL;
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceOperationStatus,
                                                        L_error_message,
                                                        L_program);

EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));

      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END deletePayTermDesc;

--------------------------------------------------------------------------------
END PayTermServiceProviderImpl;

/
