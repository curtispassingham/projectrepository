CREATE OR REPLACE PACKAGE BODY FDL_TRANDATA_SQL AS
--------------------------------------------------------------------------------------------------------------------
--- Procedure Name: QUERY_PROCEDURE
--- Purpose:        This procedure will query FIXED_DEAL_GL_REF_DATA table and populate the above declared 
---                 table of records that will be used as the 'base table' in the Fixed Deal Transaction Data From.
--------------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (IO_fdl_trandata_tbl     IN OUT   FDL_TRANDATA_SQL.FDL_TRANDATA_TBL,
                           I_deal_no               IN       FIXED_DEAL_GL_REF_DATA.DEAL_NO%TYPE,
                           I_supplier              IN       DEAL_HEAD.SUPPLIER%TYPE,
                           I_dept                  IN       FIXED_DEAL_GL_REF_DATA.DEPT%TYPE,
                           I_class                 IN       FIXED_DEAL_GL_REF_DATA.CLASS%TYPE,
                           I_subclass              IN       FIXED_DEAL_GL_REF_DATA.SUBCLASS%TYPE,
                           I_loc_type              IN       FIXED_DEAL_GL_REF_DATA.LOC_TYPE%TYPE,
                           I_location              IN       FIXED_DEAL_GL_REF_DATA.LOCATION%TYPE,
                           I_collect_date_start    IN       FIXED_DEAL_GL_REF_DATA.COLLECT_DATE%TYPE,
                           I_collect_date_end      IN       FIXED_DEAL_GL_REF_DATA.COLLECT_DATE%TYPE,
                           I_reference_trace_id    IN       FIXED_DEAL_GL_REF_DATA.REFERENCE_TRACE_ID%TYPE)
IS

   cursor C_FDL_GL_REF_DATA is
      select DEAL_NO,
             FIXED_DEAL_AMT,
             COLLECT_DATE,
             DEPT,
             CLASS,
             SUBCLASS,
             LOC_TYPE,
             LOCATION,
             CONTRIB_RATIO,
             CONTRIB_AMOUNT,
             SET_OF_BOOKS_ID,
             REFERENCE_TRACE_ID,
             null,
             null
        from fixed_deal_gl_ref_data
       where deal_no = NVL(I_deal_no, deal_no)
         and dept = NVL(I_dept, dept)
         and class = NVL(I_class, class)
         and subclass = NVL(I_subclass, subclass)
         and loc_type = NVL(I_loc_type, loc_type)
         and location = NVL(I_location, location)
         and (collect_date between NVL(I_collect_date_start, collect_date) and NVL(I_collect_date_end, collect_date))
         and reference_trace_id = NVL(I_reference_trace_id, reference_trace_id);

   cursor C_FDL_GL_REF_DATA_SUP is
      select DEAL_NO,
             FIXED_DEAL_AMT,
             COLLECT_DATE,
             DEPT,
             CLASS,
             SUBCLASS,
             LOC_TYPE,
             LOCATION,
             CONTRIB_RATIO,
             CONTRIB_AMOUNT,
             SET_OF_BOOKS_ID,
             REFERENCE_TRACE_ID,
             null,
             null
        from fixed_deal_gl_ref_data
       where deal_no in (select deal_no 
                           from fixed_deal 
                          where supplier = I_supplier)
         and dept = NVL(I_dept, dept)
         and class = NVL(I_class, class)
         and subclass = NVL(I_subclass, subclass)
         and loc_type = NVL(I_loc_type, loc_type)
         and location = NVL(I_location, location)
         and (collect_date between NVL(I_collect_date_start, collect_date) and NVL(I_collect_date_end, collect_date))
         and reference_trace_id = NVL(I_reference_trace_id, reference_trace_id);

BEGIN

   if I_supplier is NOT NULL and 
      I_deal_no is NULL then
      open C_FDL_GL_REF_DATA_SUP;
      fetch C_FDL_GL_REF_DATA_SUP BULK COLLECT into IO_fdl_trandata_tbl;
      close C_FDL_GL_REF_DATA_SUP;
   else
      open C_FDL_GL_REF_DATA;
      fetch C_FDL_GL_REF_DATA BULK COLLECT into IO_fdl_trandata_tbl;
      close C_FDL_GL_REF_DATA; 
   end if;
      

EXCEPTION
   when OTHERS then
      IO_fdl_trandata_tbl(1).return_code   := 'FALSE';
      IO_fdl_trandata_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 'FDL_TRANDATA_SQL.QUERY_PROCEDURE',
                                                                 to_char(SQLCODE));
END QUERY_PROCEDURE;

--------------------------------------------------------------------------------------------------------------------
--- Procedure Name: GET_MIN_DATE
--- Purpose:        This procedure will query FIXED_DEAL_GL_REF_DATA table to get the minimum possible date for 
---                 collect_date
--------------------------------------------------------------------------------------------------------------------
FUNCTION GET_MIN_DATE (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_min_date              IN OUT   FIXED_DEAL_GL_REF_DATA.COLLECT_DATE%TYPE)
RETURN BOOLEAN IS

   cursor C_MIN_COLLECT_DATE is
      select min(collect_date)
        from fixed_deal_gl_ref_data;
BEGIN

   -- Get the minimum processed_date from sa_gl_ref_data table.
   open C_MIN_COLLECT_DATE;
   fetch C_MIN_COLLECT_DATE into O_min_date;
   close C_MIN_COLLECT_DATE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'FDL_TRANDATA_SQL.GET_MIN_DATE',
                                             to_char(SQLCODE));
      return FALSE;
END GET_MIN_DATE;

--------------------------------------------------------------------------------------------------------------------
--- Procedure Name: GET_DEAL_SUPPLIER
--- Purpose:        This procedure will query DEAL_HEAD table to get the supplier info of the current deal
--------------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_SUPPLIER (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_supplier              IN OUT   DEAL_HEAD.SUPPLIER%TYPE,
                            I_deal_no               IN       DEAL_HEAD.DEAL_ID%TYPE)
RETURN BOOLEAN IS

   cursor C_DEAL_SUPPLIER is
      select supplier
        from fixed_deal
       where deal_no = I_deal_no;
BEGIN

   -- Get the supplier information about the deal
   open C_DEAL_SUPPLIER;
   fetch C_DEAL_SUPPLIER into O_supplier;
   close C_DEAL_SUPPLIER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'FDL_TRANDATA_SQL.GET_DEAL_SUPPLIER',
                                             to_char(SQLCODE));
      return FALSE;
END GET_DEAL_SUPPLIER;

--------------------------------------------------------------------------------------------------------------------
END FDL_TRANDATA_SQL;
/