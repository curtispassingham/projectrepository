CREATE OR REPLACE PACKAGE SUP_CONSTRAINTS_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                 GLOBALS                                   --
-------------------------------------------------------------------------------

/* Ranking Directions */
LP_RANK_DIR_UP          NUMBER := 0;
LP_RANK_DIR_DOWN        NUMBER := 1;
LP_RANK_DIR_STOP        NUMBER := 2;
LP_RANK_DIR_NO          NUMBER := 3;

LP_DAY                  NUMBER := 0;
LP_CASE                 NUMBER := 1;

-------------------------------------------------------------------------------
--                                DATA TYPES                                 --
-------------------------------------------------------------------------------
TYPE RANK_PRIORITY_REC IS RECORD(
   rank_code             VARCHAR2(2),
   rank_order            NUMBER,
   accept_ind            VARCHAR2(1)
);
TYPE RANK_PRIORITY_TBL IS TABLE OF RANK_PRIORITY_REC 
   INDEX BY BINARY_INTEGER;

TYPE EVAL_SYSTEM_REC IS RECORD(
   rank_1               VARCHAR2(2),
   rank_2               VARCHAR2(2),
   direction            NUMBER
);
TYPE EVAL_SYSTEM_TBL IS TABLE OF EVAL_SYSTEM_REC 
   INDEX BY BINARY_INTEGER;

TYPE SCALING_REC IS RECORD(
   ordloc_exist          VARCHAR2(1),
   item                  ITEM_MASTER.ITEM%TYPE,
   physical_wh           WH.WH%TYPE,
   source_wh             WH.WH%TYPE,
   loc_type              ITEM_LOC.LOC_TYPE%TYPE,
   location              ITEM_LOC.LOC%TYPE,
   rounding_seq          WH.ROUNDING_SEQ%TYPE,
   qty_prescaled         ORDLOC.QTY_PRESCALED%TYPE,
   orig_raw_roq_pack     REPL_RESULTS.ORIG_RAW_ROQ_PACK%TYPE,
   item_non_scale_ind    VARCHAR2(1),
   loc_non_scale_ind     VARCHAR2(1),
   pack_ind              ITEM_MASTER.PACK_IND%TYPE,
   dept                  ITEM_MASTER.DEPT%TYPE,
   sup_unit_cost         ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
   sup_currency_code     SUPS.CURRENCY_CODE%TYPE,
   ord_currency_code     ORDHEAD.CURRENCY_CODE%TYPE,
   exchange_rate         ORDHEAD.EXCHANGE_RATE%TYPE,
   item_min_order_qty    ITEM_SUPP_COUNTRY.MIN_ORDER_QTY%TYPE,
   item_max_order_qty    ITEM_SUPP_COUNTRY.MAX_ORDER_QTY%TYPE,
   ship_carton_len       ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE,
   ship_carton_hgt       ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE,
   ship_carton_wid       ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE,
   dimension_uom         ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
   ship_carton_wt        ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE,
   weight_uom            ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
   stat_cube             ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE,
   supp_pack_size        ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
   pallet_size           ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
   tier                  ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
   inner_pack_size       ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
   master_item           REPL_RESULTS.MASTER_ITEM%TYPE,
   item_type             REPL_RESULTS.ITEM_TYPE%TYPE,
   stock_cat             REPL_RESULTS.STOCK_CAT%TYPE,
   repl_method           REPL_RESULTS.REPL_METHOD%TYPE,
   due_ind               REPL_RESULTS.DUE_IND%TYPE,--1699
   order_point           REPL_RESULTS.ORDER_POINT%TYPE,
   order_written_date    ORDHEAD.WRITTEN_DATE%TYPE,
   order_lead_time_date  ORDHEAD.WRITTEN_DATE%TYPE,
   order_upto_point_date ORDHEAD.WRITTEN_DATE%TYPE,
   max_scale_down_day    REPL_RESULTS.REVIEW_TIME%TYPE,
   season_id             REPL_RESULTS.SEASON_ID%TYPE,
   phase_id              REPL_RESULTS.PHASE_ID%TYPE,
   max_scale_value       REPL_RESULTS.MAX_SCALE_VALUE%TYPE,
   /* fields fetched for order writeout */
   supplier              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
   origin_country_id     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
   import_order_ind      ORDHEAD.IMPORT_ORDER_IND%TYPE,
   import_country_id     ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
   earliest_ship_date    ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
   latest_ship_date      ORDHEAD.LATEST_SHIP_DATE%TYPE,
   alloc_no              ALLOC_HEADER.ALLOC_NO%TYPE,
   qty_ordered           ORDLOC.QTY_ORDERED%TYPE, /* qty ordered or allocated */
   /* rounding, bracket costing, and multichannel-related variables */
   round_lvl             ITEM_SUPP_COUNTRY_LOC.ROUND_LVL%TYPE,
   round_to_inner_pct    ITEM_SUPP_COUNTRY_LOC.ROUND_TO_INNER_PCT%TYPE,
   round_to_case_pct     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_CASE_PCT%TYPE,
   round_to_layer_pct    ITEM_SUPP_COUNTRY_LOC.ROUND_TO_LAYER_PCT%TYPE,
   round_to_pallet_pct   ITEM_SUPP_COUNTRY_LOC.ROUND_TO_PALLET_PCT%TYPE,
   /* derived fields */
   non_scale_ind         VARCHAR2(1),
   store_close_date      STORE.STORE_CLOSE_DATE%TYPE,
   store_ord_mult        ITEM_LOC.STORE_ORD_MULT%TYPE,
   buyer_pack_ind        VARCHAR2(1),
   uom_conv_factor       ITEM_MASTER.UOM_CONV_FACTOR%TYPE,
   min_scale_value       ORDLOC.QTY_PRESCALED%TYPE,
   volume                ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE,
   ord_unit_cost         ORDLOC.UNIT_COST%TYPE,  /* unit_cost in order and scaling currency code */
   pack_item_qty         PACKITEM.PACK_QTY%TYPE, /* pack_item_qty for simple pack */
   break_pack_ind        VARCHAR2(1),            /* break_pack_ind of source_wh for xdock */
    /* scaling solution */
   scaled_ind            VARCHAR2(1),              /* flag for writing out scaled solutions */
   qty_scaled_sol        ORDLOC.QTY_PRESCALED%TYPE /* scaled qty for acceptable solution */            
);
TYPE SCALING_TBL IS TABLE OF SCALING_REC 
   INDEX BY BINARY_INTEGER;

   
TYPE DATE_TBL IS TABLE OF DAILY_ITEM_FORECAST.DATA_DATE%TYPE
   INDEX BY BINARY_INTEGER;
TYPE SALES_TBL IS TABLE OF DAILY_ITEM_FORECAST.FORECAST_SALES%TYPE
   INDEX BY BINARY_INTEGER;
   
TYPE FORECAST_ITEM_LOC_REC IS RECORD(
   item                  ITEM_LOC.ITEM%TYPE,
   location              ITEM_LOC.LOC%TYPE,
   physical_wh           WH.PHYSICAL_WH%TYPE,
   forecast_start_date   DAILY_ITEM_FORECAST.DATA_DATE%TYPE,
   forecast_end_date     DAILY_ITEM_FORECAST.DATA_DATE%TYPE,
   max_days              NUMBER,
   total_eow_fetched     NUMBER,
   eow_date              SUP_CONSTRAINTS_SQL.DATE_TBL,
   sales                 SUP_CONSTRAINTS_SQL.SALES_TBL,
   total_day_fetched     NUMBER,
   data_date             SUP_CONSTRAINTS_SQL.DATE_TBL,
   day_sales             SUP_CONSTRAINTS_SQL.SALES_TBL
);
TYPE FORECAST_ITEM_LOC_TBL IS TABLE OF FORECAST_ITEM_LOC_REC 
   INDEX BY BINARY_INTEGER;
   
TYPE SCALE_CNSTR_SUPER_REC IS RECORD(
   location                  ITEM_LOC.LOC%TYPE,/* "-1" if order-level scaling */
   scale_cnstr_type          ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
   scale_cnstr_super_min     ORD_INV_MGMT.SCALE_CNSTR_MIN_VAL1%TYPE,
   scale_cnstr_super_min_tol ORD_INV_MGMT.SCALE_CNSTR_MIN_TOL1%TYPE,
   scale_cnstr_super_max     ORD_INV_MGMT.SCALE_CNSTR_MAX_VAL1%TYPE,
   scale_cnstr_super_max_tol ORD_INV_MGMT.SCALE_CNSTR_MAX_TOL1%TYPE
);
TYPE SCALE_CNSTR_SUPER_TBL IS TABLE OF SCALE_CNSTR_SUPER_REC 
   INDEX BY BINARY_INTEGER;
TYPE SCALE_CNSTR_SUPER_TBL2 IS TABLE OF SCALE_CNSTR_SUPER_TBL 
   INDEX BY BINARY_INTEGER;

/* 
 * Data record for each item-loc in day-of-supply and case-pallet scaling:
 * 1) qty_scaled_suom is the scaling qty in standard uom; 
 * 2) qty_scaled_suom_rnd is the scaling qty evaluated to pallet rounding 
 *    in standard uom; 
 * 3) qty_scaled_suom_rnd usually equals to qty_scaled_suom except during 
 *    case-pallet scaling and round_lvl = "B", in which case qty_scaled_suom
 *    holds the value of case scaling; qty_scaled_suom_rnd holds the value
 *    of case scaling after pallet evaluation; 
 * 4) qty_scaled_suom_rnd is used for evaluating against the scaling and 
 *    non-scaling constraints, and will be the scaled order quantity. 
 * 5) similar to scale_loc, for day-of-supply scaling, qty_scaled_xxx tbl 
 *    is sized to max_scale_iteration , and index 1 stores the prescaled qty;
 *    for case-pallet scaling
 * 6) index of qty_scaled_xxx corresponds to the index of solution tbl, 
 *    which means if the best solution day is day i, the order qty for item/loc
 *    will be qty_scaled_suom_rnd(i). 
 */
TYPE QTY_SCALED_TBL is TABLE OF ORDLOC.QTY_PRESCALED%TYPE
   INDEX BY BINARY_INTEGER;
TYPE VARCHAR_TBL is TABLE OF VARCHAR2(100)
   INDEX BY BINARY_INTEGER;

TYPE SCALE_ITEM_LOC_REC is RECORD(
   item                  ITEM_LOC.ITEM%TYPE,
   location              ITEM_LOC.LOC%TYPE,
   physical_wh           ITEM_LOC.LOC%TYPE,
   rounding_seq          WH.ROUNDING_SEQ%TYPE,
   qty_scaled_suom       SUP_CONSTRAINTS_SQL.QTY_SCALED_TBL,
   qty_scaled_suom_rnd   SUP_CONSTRAINTS_SQL.QTY_SCALED_TBL,
   message               SUP_CONSTRAINTS_SQL.VARCHAR_TBL
);
TYPE SCALE_ITEM_LOC_TBL IS TABLE OF SCALE_ITEM_LOC_REC 
   INDEX BY BINARY_INTEGER;


/*    
 * Table for solution in day-of-supply and case-pallet scaling 
 * 1) for day-of-supply scaling, solution table is sized to max_scale_iteration
 *    + 1; index 1 stores the prescaled ranking; 
 * 2) for case-pallet scaling, solution Table is sized to max_scale_iteration *
 *    Total records + 1 since solution needs to be ranked separately for
 *    each item/loc in each iteration; index 1 stores the ranking after solution
 *    day rounding; 
 */
TYPE SOLUTION_DAY_REC IS RECORD(
   qty_scaled            SUP_CONSTRAINTS_SQL.QTY_SCALED_TBL, /* total scaled qty for loc in cnstr type */ 
   cnstr_rank            SUP_CONSTRAINTS_SQL.VARCHAR_TBL,    /* RL,YL,YH,RH,GX */
   rank_codes            VARCHAR2(2),                        /* GG,GY,YG,...,G,Y,R */ 
   rank_order            NUMBER,                             /* 0,1,2,... */
   accept_ind            VARCHAR2(1)                         /* Y,N */
);
TYPE SOLUTION_DAY_TBL IS TABLE OF SOLUTION_DAY_REC 
   INDEX BY BINARY_INTEGER;

   
TYPE SCALE_LOC_REC IS RECORD(
   location              ITEM_LOC.LOC%TYPE,
   loc_type              ITEM_LOC.LOC_TYPE%TYPE,
   physical_wh           WH.PHYSICAL_WH%TYPE,
   rounding_seq          WH.ROUNDING_SEQ%TYPE,
   direction             NUMBER, /* scaling direction: UP,DOWN,STOP */
   has_source            NUMBER,
   solution_day          SOLUTION_DAY_TBL,
   last_index            NUMBER,
   sol_index             NUMBER,
   pwh_index             NUMBER,
   scaled_ind            NUMBER
);
TYPE SCALE_LOC_TBL IS TABLE OF SCALE_LOC_REC 
   INDEX BY BINARY_INTEGER;


TYPE SCALE_CNSTR_REC IS RECORD(
   scale_cnstr_type      ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
   scale_cnstr_uom       ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
   scale_cnstr_curr      ORD_INV_MGMT.SCALE_CNSTR_CURR1%TYPE,
   scale_cnstr_min_val   ORD_INV_MGMT.SCALE_CNSTR_MIN_VAL1%TYPE,
   scale_cnstr_max_val   ORD_INV_MGMT.SCALE_CNSTR_MAX_VAL1%TYPE,
   scale_cnstr_min_tol   ORD_INV_MGMT.SCALE_CNSTR_MIN_TOL1%TYPE,
   scale_cnstr_max_tol   ORD_INV_MGMT.SCALE_CNSTR_MAX_TOL1%TYPE
);
TYPE SCALE_CNSTR_TBL IS TABLE OF SCALE_CNSTR_REC 
   INDEX BY BINARY_INTEGER;
   
TYPE ORD_INV_MGMT_REC IS RECORD(
   scale_cnstr_ind       ORD_INV_MGMT.SCALE_CNSTR_IND%TYPE, 
   scale_cnstr_lvl       ORD_INV_MGMT.SCALE_CNSTR_LVL%TYPE,  
   scale_cnstr_obj       ORD_INV_MGMT.SCALE_CNSTR_OBJ%TYPE,   
   scale_cnstr_type1     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE, 
   scale_cnstr_uom1      ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,  
   scale_cnstr_curr1     ORD_INV_MGMT.SCALE_CNSTR_CURR1%TYPE, 
   scale_cnstr_min_val1  ORD_INV_MGMT.SCALE_CNSTR_MIN_VAL1%TYPE,
   scale_cnstr_max_val1  ORD_INV_MGMT.SCALE_CNSTR_MAX_VAL1%TYPE, 
   scale_cnstr_min_tol1  ORD_INV_MGMT.SCALE_CNSTR_MIN_TOL1%TYPE,  
   scale_cnstr_max_tol1  ORD_INV_MGMT.SCALE_CNSTR_MAX_TOL1%TYPE,  
   scale_cnstr_type2     ORD_INV_MGMT.SCALE_CNSTR_TYPE2%TYPE, 
   scale_cnstr_uom2      ORD_INV_MGMT.SCALE_CNSTR_UOM2%TYPE,  
   scale_cnstr_curr2     ORD_INV_MGMT.SCALE_CNSTR_CURR2%TYPE, 
   scale_cnstr_min_val2  ORD_INV_MGMT.SCALE_CNSTR_MIN_VAL2%TYPE,
   scale_cnstr_max_val2  ORD_INV_MGMT.SCALE_CNSTR_MAX_VAL2%TYPE,
   scale_cnstr_min_tol2  ORD_INV_MGMT.SCALE_CNSTR_MIN_TOL2%TYPE,
   scale_cnstr_max_tol2  ORD_INV_MGMT.SCALE_CNSTR_MAX_TOL2%TYPE,
   max_scaling_iterations ORD_INV_MGMT.MAX_SCALING_ITERATIONS%TYPE,
   due_ord_process_ind   ORD_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE, 
   mult_vehicle_ind      ORD_INV_MGMT.MULT_VEHICLE_IND%TYPE
);
TYPE ORD_INV_MGMT_TBL IS TABLE OF ORD_INV_MGMT_REC
   INDEX BY BINARY_INTEGER;

/* 
 * driv_cursor_index is the index for the x-docked location in the driving
 * cursor Table.
 */
TYPE XDOCK_TO_LOC_REC IS RECORD(
   xdock_loc             REPL_ITEM_LOC.LOCATION%TYPE,
   item                  REPL_ITEM_LOC.ITEM%TYPE,
   driv_cursor_index     NUMBER
);
TYPE XDOCK_TO_LOC_TBL IS TABLE OF XDOCK_TO_LOC_REC
   INDEX BY BINARY_INTEGER;
/* 
 * For repl order, if qty_prescaled is negative for all x-docked locations,
 * there is no ordloc record (therefore no entry in the driving cursor) for
 * the source_wh. After scaling,if any of the x-docked location scales to 
 * a positive quantity, add an entry to the end of the driving cursor for
 * the source_wh/item, so that an ordloc record can be written during the 
 * writeout process.
 *
 * residual_qty is the difference between the total allocation qty in store_
 * ord_mult before and after case rounding. This number plus the total allocation
 * qty plus the dir-to-wh qty is the total qty ordered to the warehouse.
 */
TYPE XDOCK_FROM_LOC_REC IS RECORD(
   source_wh             REPL_ITEM_LOC.SOURCE_WH%TYPE,
   item                  REPL_ITEM_LOC.ITEM%TYPE,
   driv_cursor_index     NUMBER,
   break_pack_ind        VARCHAR2(1),
   residual_qty          REPL_ITEM_LOC.TERMINAL_STOCK_QTY%TYPE,
   xdock_to_loc          SUP_CONSTRAINTS_SQL.XDOCK_TO_LOC_TBL
);
TYPE XDOCK_FROM_LOC_TBL IS TABLE OF XDOCK_FROM_LOC_REC
   INDEX BY BINARY_INTEGER;

TYPE ITEM_LIST_TBL IS TABLE OF ITEM_MASTER.ITEM%TYPE
   INDEX BY BINARY_INTEGER;

TYPE ALLOC_LIST_TBL IS TABLE OF ALLOC_HEADER.ALLOC_NO%TYPE
   INDEX BY BINARY_INTEGER;   
-------------------------------------------------------------------------------
--                             PUBLIC FUNCTIONS                              --
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- Function Name: SCALING_CNSTR
-- Purpose      : This function will perform scaling for the passed Order No . 
--                Called from function SCALE_CONSTRAINTS_SQL which is called 
--                from Purchase Order screen
-------------------------------------------------------------------------------
FUNCTION SCALING_CNSTR(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no        IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BINARY_INTEGER;
-------------------------------------------------------------------------------

END SUP_CONSTRAINTS_SQL;
/