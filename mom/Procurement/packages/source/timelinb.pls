
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY TIMELINE_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION NEXT_TMLN_NO( O_timeline_no   IN OUT timeline_head.timeline_no%TYPE,
                       O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   L_timeline_no TIMELINE_HEAD.TIMELINE_NO%TYPE := 0;
   L_first_time  VARCHAR2(3)                 := 'YES';
   L_wrap_seq_no TIMELINE_HEAD.TIMELINE_NO%TYPE := 0;
   L_program     VARCHAR2(64)                := 'TIMELINE_SQL.NEXT_TMLN_NO';
   L_exists      VARCHAR2(1)                 := 'N';

   cursor C_EXIST_TIMELINE (I_timeline IN timeline.timeline_no%TYPE) is
      select 'X'
        from timeline_head
       where timeline_no = I_timeline;

BEGIN

   LOOP
      SQL_LIB.SET_MARK('SELECT', NULL, 'SYS.DUAL', NULL);
      select TIMELINE_SEQUENCE.NEXTVAL
        into L_timeline_no
        from sys.dual;
      if (L_first_time = 'YES') then
         L_wrap_seq_no := L_timeline_no;
         L_first_time  := 'NO';
      elsif (L_timeline_no = L_wrap_seq_no) then
         O_error_message := SQL_LIB.CREATE_MSG('NO_TIMELINE_AVAIL', NULL, NULL, NULL);
         return FALSE;
      end if;

      O_timeline_no := L_timeline_no;
      SQL_LIB.SET_MARK('OPEN', 'C_EXIST_TIMELINE', 'TIMELINE_HEAD', to_char(O_timeline_no));
      open C_EXIST_TIMELINE(O_timeline_no);
      SQL_LIB.SET_MARK('FETCH', 'C_EXIST_TIMELINE', 'TIMELINE_HEAD', to_char(O_timeline_no));
      fetch C_EXIST_TIMELINE into L_exists;
      ---
      if (C_EXIST_TIMELINE%notfound) then
         SQL_LIB.SET_MARK('CLOSE', 'C_EXIST_TIMELINE', 'TIMELINE_HEAD', to_char(O_timeline_no));
         close C_EXIST_TIMELINE;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_EXIST_TIMELINE', 'TIMELINE_HEAD', to_char(O_timeline_no));
      close C_EXIST_TIMELINE;
      -- if Timeline number is unique, return true
   END LOOP;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;
END NEXT_TMLN_NO;

---------------------------------------------------------------------------------------------
FUNCTION NEXT_STEP_NO( O_step_no       IN OUT timeline_step_comp.step_no%TYPE,
                       O_error_message IN OUT VARCHAR2,
                       I_timeline_type IN     timeline_step_comp.timeline_type%TYPE)
   RETURN BOOLEAN IS

   L_step_no   timeline_steps.step_no%TYPE := 0;
   L_program   VARCHAR2(64)                := 'TIMELINE_SQL.NEXT_STEP_NO';

   cursor C_STEP_NO is
      select nvl(max(step_no),0)
        from timeline_step_comp
       where timeline_type = I_timeline_type;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_STEP_NO', 'TIMELINE_STEP_COMP', NULL);
   open  C_STEP_NO;
   SQL_LIB.SET_MARK('FETCH', 'C_STEP_NO', 'TIMELINE_STEP_COMP', NULL);
   fetch C_STEP_NO into L_step_no;
   SQL_LIB.SET_MARK('CLOSE', 'C_STEP_NO', 'TIMELINE_STEP_COMP', NULL);
   close C_STEP_NO;
   --- if there are no existing step number for this timeline, then return 1
   if L_step_no = 0 then
      O_step_no := 1;
   else
      O_step_no := L_step_no + 1;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;
END NEXT_STEP_NO;

---------------------------------------------------------------------------------------------
FUNCTION GET_BASE_DESC( I_timeline_no   IN     timeline_head.timeline_no%TYPE,
                        O_base_code     IN OUT timeline_head.timeline_base%TYPE,
                        O_base_desc     IN OUT code_detail.code_desc%TYPE,
                        O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(64)   := 'TIMELINE_SQL.GET_BASE_DESC';

   cursor C_BASE_EXISTS is
      select timeline_base
        from timeline_head
       where timeline_no = I_timeline_no;

   cursor C_BASE_DESC is
      select code_desc
        from v_code_detail_tl
       where code_type in ('TLBC')
         and code = O_base_code;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_BASE_EXISTS', 'TIMELINE_HEAD', to_char(I_timeline_no));
   open  C_BASE_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_BASE_EXISTS', 'TIMELINE_HEAD', to_char(I_timeline_no));
   fetch C_BASE_EXISTS into O_base_code;

   if C_BASE_EXISTS%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_BASE_EXISTS', 'TIMELINE_HEAD', to_char(I_timeline_no));
      close C_BASE_EXISTS;
      O_error_message := SQL_LIB.CREATE_MSG('INV_TMLN_NO', NULL, NULL, NULL);
      return FALSE;

   -- Timeline Base exists, close cursor
   else
      SQL_LIB.SET_MARK('CLOSE', 'C_BASE_EXISTS', 'TIMELINE_HEAD', to_char(I_timeline_no));
      close C_BASE_EXISTS;
   end if;


   -- Get the description for the timeline base
   if O_base_code is not NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_BASE_DESC', 'V_CODE_DETAIL_TL', to_char(I_timeline_no));
      open  C_BASE_DESC;
      SQL_LIB.SET_MARK('FETCH', 'C_BASE_DESC', 'V_CODE_DETAIL_TL', to_char(I_timeline_no));
      fetch C_BASE_DESC into O_base_desc;

      if C_BASE_DESC%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE', 'C_BASE_DESC', 'V_CODE_DETAIL_TL', to_char(I_timeline_no));
         close C_BASE_DESC;
         O_error_message := SQL_LIB.CREATE_MSG('NO_CODE_DESC_FOUND', NULL, NULL, NULL);
         return FALSE;
      else
         SQL_LIB.SET_MARK('CLOSE', 'C_BASE_DESC', 'V_CODE_DETAIL_TL', to_char(I_timeline_no));
         close C_BASE_DESC;
         return TRUE;
      end if;

   -- timeline base is NULL, but function succeeded
   else
      return TRUE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;
END GET_BASE_DESC;

---------------------------------------------------------------------------------------------
FUNCTION GET_TMLN_DESC( I_timeline_no      IN     timeline_head.timeline_no%TYPE,
                        O_timeline_desc    IN OUT timeline_head.timeline_desc%TYPE,
                        O_error_message    IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'TIMELINE_SQL.GET_TMLN_DESC';

   cursor C_TIMELINE_DESC is
      select timeline_desc
        from v_timeline_head_tl
       where timeline_no = I_timeline_no;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_TIMELINE_DESC', 'V_TIMELINE_HEAD_TL', to_char(I_timeline_no));
   open  C_TIMELINE_DESC;
   SQL_LIB.SET_MARK('FETCH', 'C_TIMELINE_DESC', 'V_TIMELINE_HEAD_TL', to_char(I_timeline_no));
   fetch C_TIMELINE_DESC into O_timeline_desc;
   if C_TIMELINE_DESC%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_TIMELINE_DESC', 'V_TIMELINE_HEAD_TL', to_char(I_timeline_no));
      close C_TIMELINE_DESC;
      O_error_message := SQL_LIB.CREATE_MSG('NO_TIMELINE_DESC_FOUND', NULL, NULL, NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE', 'C_TIMELINE_DESC', 'V_TIMELINE_HEAD_TL', to_char(I_timeline_no));
      close C_TIMELINE_DESC;
      return TRUE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;
END GET_TMLN_DESC;

---------------------------------------------------------------------------------------------
FUNCTION GET_STEP_DESC  ( I_step_no       IN     timeline_step_comp.step_no%TYPE,
                          I_timeline_type IN     timeline_step_comp.timeline_type%TYPE,
                          O_step_desc     IN OUT timeline_step_comp.step_desc%TYPE,
                          O_error_message IN OUT VARCHAR2 )
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64)  := 'TIMELINE_SQL.GET_STEP_DESC';

   cursor C_STEP_DESC is
      select step_desc
        from v_timeline_step_comp_tl
       where timeline_type = I_timeline_type
         and step_no = I_step_no;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_STEP_DESC', 'V_TIMELINE_STEP_COMP_TL', I_timeline_type);
   open C_STEP_DESC;
   SQL_LIB.SET_MARK('FETCH', 'C_STEP_DESC', 'V_TIMELINE_STEP_COMP_TL', I_timeline_type);
   fetch C_STEP_DESC into O_step_desc;
   if C_STEP_DESC%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_STEP_DESC', 'V_TIMELINE_STEP_COMP_TL', I_timeline_type);
      close C_STEP_DESC;
      O_error_message := SQL_LIB.CREATE_MSG('STEP_DESC_NOTFOUND', NULL, NULL, NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE', 'C_STEP_DESC', 'V_TIMELINE_STEP_COMP_TL', I_timeline_type);
      close C_STEP_DESC;
      return TRUE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;
END GET_STEP_DESC;

---------------------------------------------------------------------------------------------
FUNCTION GET_ORIG_DATE ( I_timeline_no   IN     timeline_steps.timeline_no%TYPE,
                         I_step_no       IN     timeline_steps.step_no%TYPE,
                         I_timeline_type IN     timeline_steps.timeline_type%TYPE,
                         I_base_date     IN     DATE,
                         O_orig_date     IN OUT DATE,
                         O_error_message IN OUT VARCHAR2 )
   RETURN BOOLEAN IS

   L_days_comp     timeline_steps.days_completed%TYPE := NULL;
   L_program       VARCHAR2(64)                       := 'TIMELINE_SQL.GET_ORIG_DATE';

   cursor C_DAYS_COMP is
      select days_completed
        from timeline_steps
       where timeline_no   = I_timeline_no
         and step_no       = I_step_no
         and timeline_type = I_timeline_type;

BEGIN

   -- Fetch the Days Completed from Timeline_Steps table
   SQL_LIB.SET_MARK('OPEN', 'C_DAYS_COMP', 'TIMELINE_STEPS', to_char(I_timeline_no));
   open C_DAYS_COMP;
   SQL_LIB.SET_MARK('FETCH', 'C_DAYS_COMP', 'TIMELINE_STEPS', to_char(I_timeline_no));
   fetch C_DAYS_COMP into L_days_comp;
   if C_DAYS_COMP%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_DAYS_COMP', 'TIMELINE_STEPS', to_char(I_timeline_no));
      close C_DAYS_COMP;
      O_error_message := SQL_LIB.CREATE_MSG('DAYS_COMP_NOTFOUND', NULL, NULL, NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE', 'C_DAYS_COMP', 'TIMELINE_STEPS', to_char(I_timeline_no));
      close C_DAYS_COMP;
      -- Calculate the Original Date
      if (I_base_date is NULL) then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_INPUT', L_program, to_char(I_base_date), NULL);
         return FALSE;
      else
         O_orig_date := I_base_date + L_days_comp;
         return TRUE;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACAKGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;
END GET_ORIG_DATE;

---------------------------------------------------------------------------------------------
FUNCTION GET_BASE_DATE( I_order_no      IN     ordhead.order_no%TYPE,
                        I_timeline_base IN     timeline_head.timeline_base%TYPE,
                        O_base_date     IN OUT DATE,
                        O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'TIMELINE_SQL.GET_BASE_DATE';

   cursor C_GET_APD is
      select orig_approval_date
        from ordhead
       where ordhead.order_no = I_order_no;

   cursor C_GET_NBD is
      select not_before_date
        from ordhead
       where ordhead.order_no = I_order_no;

   cursor C_GET_NAD is
      select not_after_date
        from ordhead
       where ordhead.order_no = I_order_no;

   cursor C_GET_WRD is
      select written_date
        from ordhead
       where ordhead.order_no = I_order_no;

BEGIN
   --- Timeline based on Approved Date
   if I_timeline_base = 'APD' then
      SQL_LIB.SET_MARK('OPEN', 'C_GET_APD', 'ORDHEAD', to_char(I_order_no));
      open C_GET_APD;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_APD', 'ORDHEAD', to_char(I_order_no));
      fetch C_GET_APD into O_base_date;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_APD', 'ORDHEAD', to_char(I_order_no));
      close C_GET_APD;
      return TRUE;

   --- Timeline based on Not Before Date
   elsif I_timeline_base = 'NBD' then
      SQL_LIB.SET_MARK('OPEN', 'C_GET_NBD', 'ORDHEAD', to_char(I_order_no));
      open C_GET_NBD;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_NBD', 'ORDHEAD', to_char(I_order_no));
      fetch C_GET_NBD into O_base_date;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_NBD', 'ORDHEAD', to_char(I_order_no));
      close C_GET_NBD;
      return TRUE;

   --- Timeline based on Note After Date
   elsif I_timeline_base = 'NAD' then
      SQL_LIB.SET_MARK('OPEN', 'C_GET_NAD', 'ORDHEAD', to_char(I_order_no));
      open C_GET_NAD;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_NAD', 'ORDHEAD', to_char(I_order_no));
      fetch C_GET_NAD into O_base_date;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_NAD', 'ORDHEAD', to_char(I_order_no));
      close C_GET_NAD;
      return TRUE;

   --- Timeline based on Written Date
   elsif I_timeline_base = 'WRD' then
      SQL_LIB.SET_MARK('OPEN', 'C_GET_WRD', 'ORDHEAD', to_char(I_order_no));
      open C_GET_WRD;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_WRD', 'ORDHEAD', to_char(I_order_no));
      fetch C_GET_WRD into O_base_date;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_WRD', 'ORDHEAD', to_char(I_order_no));
      close C_GET_WRD;
      return TRUE;

   else
      O_base_date := NULL;
      return TRUE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;
END GET_BASE_DATE;

---------------------------------------------------------------------------------------------
FUNCTION GET_BASE_DATE_FROM_ORIG( O_error_message IN OUT VARCHAR2,
              O_base_date     IN OUT DATE,
              I_timeline_no   IN     timeline_steps.timeline_no%TYPE,
              I_step_no       IN     timeline_steps.step_no%TYPE,
              I_timeline_type IN     timeline_steps.timeline_type%TYPE,
              I_orig_date     IN     DATE)
   RETURN BOOLEAN IS

   L_days_comp     timeline_steps.days_completed%TYPE := NULL;
   L_program       VARCHAR2(64)                       := 'TIMELINE_SQL.GET_BASE_DATE_FROM_ORIG';

   cursor C_DAYS_COMP is
      select days_completed
        from timeline_steps
       where timeline_no   = I_timeline_no
         and step_no       = I_step_no
         and timeline_type = I_timeline_type;

BEGIN

   -- Fetch the Days Completed from Timeline_Steps table
   SQL_LIB.SET_MARK('OPEN', 'C_DAYS_COMP', 'TIMELINE_STEPS', to_char(I_timeline_no));
   open C_DAYS_COMP;
   SQL_LIB.SET_MARK('FETCH', 'C_DAYS_COMP', 'TIMELINE_STEPS', to_char(I_timeline_no));
   fetch C_DAYS_COMP into L_days_comp;
   if C_DAYS_COMP%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_DAYS_COMP', 'TIMELINE_STEPS', to_char(I_timeline_no));
      close C_DAYS_COMP;
      O_error_message := SQL_LIB.CREATE_MSG('DAYS_COMP_NOTFOUND', NULL, NULL, NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE', 'C_DAYS_COMP', 'TIMELINE_STEPS', to_char(I_timeline_no));
      close C_DAYS_COMP;
      -- Calculate the Base Date
      if (I_orig_date is NULL) then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_INPUT', L_program, to_char(O_base_date), NULL);
         return FALSE;
      else
         O_base_date := I_orig_date - L_days_comp;
         return TRUE;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('FUNCTION_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;
END GET_BASE_DATE_FROM_ORIG;

---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STEP( O_error_message IN OUT  VARCHAR2,
         O_exists       IN OUT BOOLEAN,
         I_step_no      IN     timeline.step_no%TYPE,
         I_timeline_no  IN     timeline.timeline_no%TYPE)
   RETURN BOOLEAN IS

   cursor C_STEP_EXISTS is
      select 'Y'
        from timeline
       where timeline_no = NVL(I_timeline_no, timeline_no)
         and step_no = I_step_no;

   L_exists VARCHAR2(1):=  'X';
   L_program   VARCHAR2(30):=    'VALIDATE_STEP';

BEGIN
   open C_STEP_EXISTS;
   fetch C_STEP_EXISTS into L_exists;
   close C_STEP_EXISTS;
   if L_exists = 'Y' THEN
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('FUNCTION_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END VALIDATE_STEP;

---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STEP_TYPE( O_error_message IN OUT VARCHAR2,
                             O_exists        IN OUT BOOLEAN,
                             O_step_desc     IN OUT TIMELINE_STEP_COMP.STEP_DESC%TYPE,
                             I_timeline_type IN     TIMELINE_STEP_COMP.TIMELINE_TYPE%TYPE,
                             I_step_no       IN     TIMELINE_STEP_COMP.STEP_NO%TYPE)
   return BOOLEAN IS

   cursor C_STEP_EXISTS is
      select 'Y', step_desc
        from timeline_step_comp
       where timeline_type = I_timeline_type
         and step_no       = I_step_no;

   L_exists    VARCHAR2(1)  := 'X';
   L_program   VARCHAR2(30) := 'VALIDATE_STEP_TYPE';

BEGIN
   open C_STEP_EXISTS;
   fetch C_STEP_EXISTS into L_exists, O_step_desc;
   close C_STEP_EXISTS;
   if L_exists = 'Y' THEN
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('FUNCTION_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END VALIDATE_STEP_TYPE;

---------------------------------------------------------------------------------------------
FUNCTION TIMELINE_COUNT(O_error_message IN OUT  VARCHAR2,
         O_num_timelines  IN OUT NUMBER,
         O_timeline_no    IN OUT timeline.timeline_no%TYPE,
         I_key_value_1    IN     timeline.key_value_1%TYPE)
   RETURN BOOLEAN IS

   cursor C_TIMELINE_NO is
      select distinct(timeline_no)
        from timeline
       where key_value_1 = I_key_value_1;

BEGIN
   O_timeline_no  := NULL;
   O_num_timelines   := 0;

   open C_TIMELINE_NO;
   fetch C_TIMELINE_NO into O_timeline_no;
   if C_TIMELINE_NO%NOTFOUND then
      close C_TIMELINE_NO;
   else
      O_num_timelines := 1;
      fetch C_TIMELINE_NO into O_timeline_no;
      if C_TIMELINE_NO%NOTFOUND then
         close C_TIMELINE_NO;
      else
         close C_TIMELINE_NO;
         O_num_timelines := 2;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM;
      return FALSE;
END TIMELINE_COUNT;

---------------------------------------------------------------------------------------------
FUNCTION TIMELINE_DATES(O_error_message IN OUT  VARCHAR2,
         O_original_date IN OUT  DATE,
         O_revised_date IN OUT   DATE,
         O_actual_date  IN OUT   DATE,
         I_timeline_no  IN    timeline.timeline_no%TYPE,
         I_key_value_1  IN    timeline.key_value_1%TYPE,
         I_key_value_2  IN    timeline.key_value_2%TYPE,
         I_step_no   IN timeline.step_no%TYPE)

   RETURN BOOLEAN IS

   cursor C_STEP_DATES is
      select actual_date,
             revised_date,
             original_date
        from timeline
       where timeline_no = I_timeline_no
         and key_value_1 = I_key_value_1
         and (key_value_2 = I_key_value_2
          or  key_value_2 is NULL)
         and step_no = I_step_no;

BEGIN

   open C_STEP_DATES;
   fetch C_STEP_DATES into O_actual_date, O_revised_date, O_original_date;
   close C_STEP_DATES;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQLERRM;
      return FALSE;

END TIMELINE_DATES;

-------------------------------------------------------------------------------------------
FUNCTION TIMELINES_EXIST (O_error_message IN OUT VARCHAR2,
                          O_exists    IN OUT BOOLEAN,
                          I_timeline_type IN     TIMELINE.TIMELINE_TYPE%TYPE,
                          I_key_value_1   IN     TIMELINE.KEY_VALUE_1%TYPE,
                          I_key_value_2   IN     TIMELINE.KEY_VALUE_2%TYPE)
   RETURN BOOLEAN IS

   cursor C_TIMELINES_EXIST is
      select 'Y'
        from timeline
       where timeline_type = I_timeline_type
         and key_value_1   = I_key_value_1
         and ((key_value_2 = I_key_value_2) or
              (key_value_2 is NULL and I_key_value_2 is NULL))
         and ROWNUM = 1;

   L_exists      VARCHAR2(1);
   L_program     VARCHAR2(64) := 'TIMELINE_SQL.TIMELINES_EXIST';

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_TIMELINES_EXIST', 'TIMELINE',
                    'Timeline Type: '||I_timeline_type||
                    ', Key Value 1: '||I_key_value_1||
                    ', Key Value 2: '||I_key_value_2);
   open C_TIMELINES_EXIST;

   SQL_LIB.SET_MARK('FETCH', 'C_TIMELINES_EXIST', 'TIMELINE',
                    'Timeline Type: '||I_timeline_type||
                    ', Key Value 1: '||I_key_value_1||
                    ', Key Value 2: '||I_key_value_2);
   fetch C_TIMELINES_EXIST into L_exists;

   if C_TIMELINES_EXIST%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_TIMELINES_EXIST', 'TIMELINE',
                    'Timeline Type: '||I_timeline_type||
                    ', Key Value 1: '||I_key_value_1||
                    ', Key Value 2: '||I_key_value_2);
   close C_TIMELINES_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END TIMELINES_EXIST;

---------------------------------------------------------------------------------
FUNCTION LOCK_TIMELINES   (O_error_message  IN OUT  VARCHAR2,
                           I_timeline_type  IN      TIMELINE.TIMELINE_TYPE%TYPE,
                           I_key_value_1    IN      TIMELINE.KEY_VALUE_1%TYPE,
                           I_key_value_2    IN      TIMELINE.KEY_VALUE_2%TYPE)
   RETURN BOOLEAN is
      RECORD_LOCKED          EXCEPTION;
      PRAGMA                 EXCEPTION_INIT(Record_locked, -54);
      L_program              VARCHAR2(40) := 'TIMELINE_SQL.LOCK_TIMELINES';

   cursor C_LOCK_TIMELINES is
      select 'Y'
        from timeline
       where timeline_type = I_timeline_type
         and key_value_1   = I_key_value_1
         and ((key_value_2 = I_key_value_2) or
              (I_key_value_2 is NULL))
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_TIMELINES', 'TIMELINE',
                    'Timeline Type: ' || I_timeline_type ||
                    ', Key Value 1: ' || I_key_value_1 ||
                    ', Key Value 2: ' || I_key_value_2);
   open C_LOCK_TIMELINES;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_TIMELINES', 'TIMELINE',
                    'Timeline Type: '|| I_timeline_type ||
                    ', Key Value 1: '|| I_key_value_1 ||
                    ', Key Value 2: '|| I_key_value_2);
   close C_LOCK_TIMELINES;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG ('TABLE_LOCKED', 'TIMELINE',
                                             I_timeline_type, I_key_value_1);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR', SQLERRM,
                                             L_program, to_char(SQLCODE));
      return FALSE;

END LOCK_TIMELINES;
---------------------------------------------------------------------------------
FUNCTION DELETE_TIMELINES (O_error_message   IN OUT  VARCHAR2,
                           I_timeline_type   IN      TIMELINE.TIMELINE_TYPE%TYPE,
                           I_key_value_1     IN      TIMELINE.KEY_VALUE_1%TYPE,
                           I_key_value_2     IN      TIMELINE.KEY_VALUE_2%TYPE)
   RETURN BOOLEAN IS
      L_program     VARCHAR2(40) := 'TIMELINE_SQL.DELETE_TIMELINES';
BEGIN

   SQL_LIB.SET_MARK ('DELETE', NULL, 'TIMELINE',
                     'Timeline Type: ' || I_timeline_type ||
                     ', Key Value 1: ' || I_key_value_1 ||
                     ', Key Value 2: ' || I_key_value_2);
   delete from timeline
         where timeline_type = I_timeline_type
           and key_value_1   = I_key_value_1
           and ((key_value_2 = I_key_value_2) or
                (I_key_value_2 is NULL));

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END DELETE_TIMELINES;
---------------------------------------------------------------------------------
FUNCTION GET_DEFAULTS (O_error_message      IN OUT VARCHAR2,
                       I_from_timeline_type IN     TIMELINE.TIMELINE_TYPE%TYPE,
                       I_to_timeline_type   IN     TIMELINE.TIMELINE_TYPE%TYPE,
                       I_from_key_value_1   IN     TIMELINE.KEY_VALUE_1%TYPE,
                       I_to_key_value_1     IN     TIMELINE.KEY_VALUE_1%TYPE,
                       I_from_key_value_2   IN     TIMELINE.KEY_VALUE_2%TYPE,
                       I_to_key_value_2     IN     TIMELINE.KEY_VALUE_2%TYPE)
RETURN BOOLEAN IS
   L_next_key      TIMELINE.TIMELINE_KEY%TYPE;
   L_timeline      TIMELINE.TIMELINE_NO%TYPE;
   L_prev_timeline TIMELINE.TIMELINE_NO%TYPE;
   L_base_date     TIMELINE.BASE_DATE%TYPE;
   L_step_no       TIMELINE.STEP_NO%TYPE;
   L_display_seq   TIMELINE.DISPLAY_SEQ%TYPE;
   L_original_date TIMELINE.ORIGINAL_DATE%TYPE;
   L_revised_date  TIMELINE.REVISED_DATE%TYPE;
   L_actual_date   TIMELINE.ACTUAL_DATE%TYPE;
   L_comment_desc  TIMELINE.COMMENT_DESC%TYPE;
   L_exists        VARCHAR2(1);
   L_first_time    BOOLEAN      := TRUE;
   L_program       VARCHAR2(40) := 'TIMELINE_SQL.GET_DEFAULTS';

   cursor C_TIMELINES is
      select timeline_no,
             base_date,
             step_no,
             display_seq,
             original_date,
             revised_date,
             actual_date,
             comment_desc
        from timeline
       where timeline_type = I_from_timeline_type
         and key_value_1   = I_from_key_value_1
         and ((key_value_2 = I_from_key_value_2)
          or  (key_value_2 is NULL and I_from_key_value_2 is NULL))
    order by 1;

   cursor C_CHECK_EXISTS is
      select 'Y'
        from timeline
       where timeline_no   = L_timeline
         and timeline_type = I_to_timeline_type
         and key_value_1   = I_to_key_value_1
         and ((key_value_2 = I_to_key_value_2)
          or  (key_value_2 is NULL and I_to_key_value_2 is NULL));

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_TIMELINES','TIMELINE',
                    'Timeline type: ' || I_from_timeline_type ||
                    ', Key value 1: ' || I_from_key_value_1 ||
                    ', Key value 2: ' || I_from_key_value_2);
   FOR C_TIMELINES_REC in C_TIMELINES LOOP
      L_timeline      := C_TIMELINES_REC.timeline_no;
      L_base_date     := C_TIMELINES_REC.base_date;
      L_step_no       := C_TIMELINES_REC.step_no;
      L_display_seq   := C_TIMELINES_REC.display_seq;
      L_original_date := C_TIMELINES_REC.original_date;
      L_revised_date  := C_TIMELINES_REC.revised_date;
      L_actual_date   := C_TIMELINES_REC.actual_date;
      L_comment_desc  := C_TIMELINES_REC.comment_desc;

      if (L_timeline != L_prev_timeline or
          L_first_time = TRUE) then
         L_first_time := FALSE;
         L_exists := 'N';

         SQL_LIB.SET_MARK('OPEN','C_CHECK_EXISTS','TIMELINE',NULL);
         open C_CHECK_EXISTS;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_EXISTS','TIMELINE',NULL);
         fetch C_CHECK_EXISTS into L_exists;
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXISTS','TIMELINE',NULL);
         close C_CHECK_EXISTS;
      end if;

      if L_exists = 'N' then
         if TIMELINE_SQL.NEXT_TMLN_NO(L_next_key,
                                      O_error_message) = FALSE then
            return FALSE;
         end if;

         SQL_LIB.SET_MARK('INSERT',NULL,'TIMELINE',NULL);
         insert into timeline (timeline_key,
                               timeline_no,
                               timeline_type,
                               key_value_1,
                               key_value_2,
                               base_date,
                               step_no,
                               display_seq,
                               original_date,
                               revised_date,
                               actual_date,
                               comment_desc,
                               create_datetime,
                               last_update_datetime,
                               last_update_id,
                               create_id)
                       values (L_next_key,
                               L_timeline,
                               I_to_timeline_type,
                               I_to_key_value_1,
                               I_to_key_value_2,
                               L_base_date,
                               L_step_no,
                               L_display_seq,
                               L_original_date,
                               L_revised_date,
                               L_actual_date,
                               L_comment_desc,
                               sysdate,
                               sysdate,
                               get_user,
                               get_user);
      end if;

      L_prev_timeline := L_timeline;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEFAULTS;
----------------------------------------------------------------
FUNCTION CALC_DATES(O_error_message IN OUT   VARCHAR2,
               I_timeline_no  IN    timeline.timeline_no%TYPE,
               I_key_value_1  IN    timeline.key_value_1%TYPE,
               I_key_value_2  IN    timeline.key_value_2%TYPE,
               I_difference   IN NUMBER)
   RETURN BOOLEAN is

   L_program   VARCHAR2(50)   := 'TIMELINE_SQL.CALC_DATES';


BEGIN

   update timeline
      set revised_date = (Original_date + I_difference)
       where timeline_no = I_timeline_no
         and key_value_1 = I_key_value_1
         and (key_value_2 = I_key_value_2
          or  key_value_2 is NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('FUNCTION_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END CALC_DATES;
----------------------------------------------------------------
FUNCTION CALC_DATES_NEW(O_error_message IN OUT  VARCHAR2,
               I_timeline_no  IN    timeline.timeline_no%TYPE,
               I_key_value_1  IN    timeline.key_value_1%TYPE,
               I_key_value_2  IN    timeline.key_value_2%TYPE,
               I_difference   IN NUMBER,
                        I_step_no         IN    timeline.step_no%TYPE)
   RETURN BOOLEAN is

   L_program   VARCHAR2(50)   := 'TIMELINE_SQL.CALC_DATES';
   L_sequence_no      timeline.display_seq%TYPE;

   cursor C_GET_SEQUENCE_NO is
      select display_seq
        from timeline
       where timeline_no = I_timeline_no
         and key_value_1 = I_key_value_1
         and (key_value_2 = I_key_value_2
          or  key_value_2 is NULL)
         and step_no = I_step_no;

BEGIN
   open C_GET_SEQUENCE_NO;
   fetch C_GET_SEQUENCE_NO into L_sequence_no;
   close C_GET_SEQUENCE_NO;

   update timeline
      set revised_date = (Original_date + I_difference)
       where timeline_no = I_timeline_no
         and key_value_1 = I_key_value_1
         and (key_value_2 = I_key_value_2
          or  key_value_2 is NULL)
         and display_seq > L_sequence_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('FUNCTION_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END CALC_DATES_NEW;

--------------------------------------------------------------------------------
FUNCTION DELETE_SELECTED_TIMELINE(O_error_message   IN OUT VARCHAR2,
                                  I_timeline_no     IN     TIMELINE.TIMELINE_NO%TYPE,
                                  I_key_value_1     IN     TIMELINE.KEY_VALUE_1%TYPE,
                                  I_key_value_2     IN     TIMELINE.KEY_VALUE_2%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(40) := 'TIMELINE_SQL.DELETE_TIMELINES';

BEGIN
   SQL_LIB.SET_MARK ('DELETE', NULL, 'TIMELINE',
                     'Timeline No: ' || I_timeline_no ||
                     ', Key Value 1: ' || I_key_value_1 ||
                     ', Key Value 2: ' || I_key_value_2);
   delete from timeline
           where timeline_no = I_timeline_no
           and key_value_1   = I_key_value_1
           and ((key_value_2 = I_key_value_2) or
                (I_key_value_2 is NULL));

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END DELETE_SELECTED_TIMELINE;

--------------------------------------------------------------------------------
FUNCTION INSERT_DOWN_PARENT (O_error_message  IN OUT VARCHAR2,
                           I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(65) := 'TIMELINE_SQL.INSERT_DOWN_PARENT';
   L_table         VARCHAR2(65);

         
BEGIN

   L_table := 'TIMELINE';

   SQL_LIB.SET_MARK('INSERT', NULL, 'TIMELINE',
                'Key_value_1: ' || I_item);

   insert into timeline (timeline_key,
                         timeline_no,
                         timeline_type,
                         key_value_1,
                         key_value_2,
                         base_date,
                         step_no,
                         display_seq,
                         original_date,
                         revised_date,
                         actual_date,
                         reason_code,
                         comment_desc,
                         create_datetime,
                         last_update_datetime,
                         last_update_id,
                         create_id)
      select
             timeline_sequence.nextval,
             t.timeline_no,
             t.timeline_type,
             im.item,
             t.key_value_2,
             t.base_date,
             t.step_no,
             t.display_seq,
             t.original_date,
             t.revised_date,
             t.actual_date,
             t.reason_code,
             t.comment_desc,
             sysdate,
             sysdate,
             get_user,
             get_user
        from timeline t,
             item_master im
       where t.key_value_1 = I_item
         and (item_parent  = t.key_value_1
          or im.item_grandparent = t.key_value_1)
         and im.item_level <= im.tran_level
         and timeline_type = 'IT'
         and not exists (select key_value_1 
                           from timeline t2 
                          where t2.key_value_1 = im.item
                            and nvl(t2.key_value_2,'0') = nvl(t.key_value_2,'0')
                            and t2.timeline_no = t.timeline_no
                            and t2.step_no = t.step_no 
                            and t2.timeline_type = 'IT' );


   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_DOWN_PARENT;

---------------------------------------------------------------------------------
FUNCTION COPY_DOWN_PARENT (O_error_message  IN OUT VARCHAR2,
                           I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(65) := 'TIMELINE_SQL.COPY_DOWN_PARENT';
   L_table         VARCHAR2(65);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_TIMELINE is
      select 'x'
        from TIMELINE
       where key_value_1  in (select item
                        from item_master
                       where (item_parent = I_item
                          or item_grandparent = I_item)
                         and item_level <= tran_level)
                         and timeline_type = 'IT'
         for update nowait;
         
   cursor C_PARENT_TIMELINE is
      select *
        from TIMELINE
       where key_value_1  = I_item;


BEGIN

   L_table := 'TIMELINE';

   open  C_LOCK_TIMELINE;
   close C_LOCK_TIMELINE;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'TIMELLINE',
                'Key_value_1: ' || I_item);
                
   for C_PARENT_TIMELINE_REC in C_PARENT_TIMELINE loop
      update timeline
         set base_date = C_PARENT_TIMELINE_REC.base_date,
             step_no = C_PARENT_TIMELINE_REC.step_no,
             display_seq = C_PARENT_TIMELINE_REC.display_seq,
             original_date = C_PARENT_TIMELINE_REC.original_date,
             revised_date = C_PARENT_TIMELINE_REC.revised_date,
             actual_date = C_PARENT_TIMELINE_REC.actual_date,
             reason_code = C_PARENT_TIMELINE_REC.reason_code,
             comment_desc = C_PARENT_TIMELINE_REC.comment_desc,
             last_update_datetime = sysdate,
             last_update_id = get_user
       where nvl(key_value_2,'0') = nvl(C_PARENT_TIMELINE_REC.key_value_2,'0')
         and timeline_no = C_PARENT_TIMELINE_REC.timeline_no
         and step_no = C_PARENT_TIMELINE_REC.step_no                
         and timeline_type = 'IT'
         and key_value_1 in (select item 
                               from item_master
                              where (item_parent = I_item
                                 or item_grandparent = I_item)
                                and item_level <= tran_level);
   end loop;

   SQL_LIB.SET_MARK('DELETE', NULL, 'TIMELLINE',
                'Key_value_1: ' || I_item);

   delete timeline t
       where key_value_1  in (select item
                        from item_master
                       where (item_parent = I_item
                          or item_grandparent = I_item)
                         and item_level <= tran_level)
                         and timeline_type = 'IT'
                         and not exists (select key_value_1 
                                           from timeline t2 
                                          where t2.key_value_1 = I_item
                                            and nvl(t2.key_value_2,'0') = nvl(t.key_value_2,'0')
                                            and t2.timeline_no = t.timeline_no
                                            and t2.step_no = t.step_no 
                                            and t2.timeline_type = 'IT' );
 
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'TIMELINE',
                'Key_value_1: ' || I_item);

   insert into timeline (timeline_key,
                         timeline_no,
                         timeline_type,
                         key_value_1,
                         key_value_2,
                         base_date,
                         step_no,
                         display_seq,
                         original_date,
                         revised_date,
                         actual_date,
                         reason_code,
                         comment_desc,
                         create_datetime,
                         last_update_datetime,
                         last_update_id)
      select
             timeline_sequence.nextval,
             t.timeline_no,
             t.timeline_type,
             im.item,
             t.key_value_2,
             t.base_date,
             t.step_no,
             t.display_seq,
             t.original_date,
             t.revised_date,
             t.actual_date,
             t.reason_code,
             t.comment_desc,
             sysdate,
             sysdate,
             get_user
        from timeline t,
             item_master im
       where t.key_value_1 = I_item
         and (item_parent  = t.key_value_1
          or im.item_grandparent = t.key_value_1)
         and im.item_level <= im.tran_level
         and timeline_type = 'IT'
         and not exists (select key_value_1 
                           from timeline t2 
                          where t2.key_value_1 = im.item
                            and nvl(t2.key_value_2,'0') = nvl(t.key_value_2,'0')
                            and t2.timeline_no = t.timeline_no
                            and t2.step_no = t.step_no 
                            and t2.timeline_type = 'IT' );


   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('TABLE_LOCKED',
                                           L_table,
                                           I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END COPY_DOWN_PARENT;
--------------------------------------------------------------------------------------------

FUNCTION VALIDATE_STEP_TYPE( O_error_message IN OUT VARCHAR2,
                             O_exists       IN OUT BOOLEAN,
                             I_timeline_type IN     TIMELINE_STEP_COMP.TIMELINE_TYPE%TYPE,
                             I_step_no       IN     TIMELINE_STEP_COMP.STEP_NO%TYPE)
   RETURN BOOLEAN IS

   cursor C_STEP_EXISTS is
      select 'Y'
        from timeline_steps
       where timeline_type = I_timeline_type
         and step_no       = I_step_no;

   L_exists    VARCHAR2(1)  := 'X';
   L_program   VARCHAR2(30) := 'VALIDATE_STEP_TYPE';

BEGIN
   open C_STEP_EXISTS;
   fetch C_STEP_EXISTS into L_exists;
   close C_STEP_EXISTS;
   if L_exists = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END VALIDATE_STEP_TYPE;
---------------------------------------------------------------------------------------------
END;
/


