CREATE OR REPLACE PACKAGE ADD_LINE_ITEM_SQL AUTHID CURRENT_USER IS

-------------------------------------------------------------------------------
--                                 GLOBALS                                   --
-------------------------------------------------------------------------------
LP_vdate                   PERIOD.VDATE%TYPE;
LP_repl_order_days         INV_MOVE_UNIT_OPTIONS.REPL_ORDER_DAYS%TYPE;
LP_elc_ind                 FUNCTIONAL_CONFIG_OPTIONS.ELC_IND%TYPE;
LP_alloc_method            INV_MOVE_UNIT_OPTIONS.ALLOC_METHOD%TYPE;
LP_base_country_id         SYSTEM_CONFIG_OPTIONS.BASE_COUNTRY_ID%TYPE;
LP_latest_ship_days        PROCUREMENT_UNIT_OPTIONS.LATEST_SHIP_DAYS%TYPE;
LP_fob_title_pass          RTM_UNIT_OPTIONS.FOB_TITLE_PASS%TYPE;
LP_fob_title_pass_desc     RTM_UNIT_OPTIONS.FOB_TITLE_PASS_DESC%TYPE;
LP_max_scale_iterations    INV_MOVE_UNIT_OPTIONS.MAX_SCALING_ITERATIONS%TYPE;
LP_batch_ind               VARCHAR2(1);
LP_ord_appr_amt_code       VARCHAR2(1);
LP_import_ind              VARCHAR2(1);
LP_locked_ind              VARCHAR2(1);


-------------------------------------------------------------------------------
--                                DATA TYPES                                 --
-------------------------------------------------------------------------------

TYPE CNST_UOM_TBL  IS TABLE OF SUP_INV_MGMT.TRUCK_CNSTR_UOM1%TYPE INDEX BY BINARY_INTEGER;
TYPE CNST_VAL_TBL  IS TABLE OF SUP_INV_MGMT.TRUCK_CNSTR_VAL1%TYPE INDEX BY BINARY_INTEGER;
TYPE CNST_TYPE_TBL IS TABLE OF SUP_INV_MGMT.TRUCK_CNSTR_TYPE1%TYPE INDEX BY BINARY_INTEGER;
TYPE CNST_QTY_TBL IS TABLE OF NUMBER(12,4) INDEX BY BINARY_INTEGER;

TYPE DRIV_CUR_INFO_PROCESS_REC IS RECORD
(
   source_type           REPL_RESULTS.SOURCE_TYPE%TYPE,
   supplier              REPL_RESULTS.PRIMARY_REPL_SUPPLIER%TYPE,
   dept                  REPL_RESULTS.DEPT%TYPE,
   order_status          VARCHAR2(1),
   item                  REPL_RESULTS.ITEM%TYPE,
   pack_ind              REPL_RESULTS.ITEM_TYPE%TYPE,
   loc                   REPL_RESULTS.LOCATION%TYPE,
   phy_loc               REPL_RESULTS.PHYSICAL_WH%TYPE,
   repl_wh_link          WH.REPL_WH_LINK%TYPE,
   loc_type              REPL_RESULTS.LOC_TYPE%TYPE,
   due_ind               VARCHAR2(1),
   xdock_ind             VARCHAR2(1),
   xdock_store           NUMBER(10),
   contract              ORDHEAD.CONTRACT_NO%TYPE,
   contract_type         VARCHAR2(2),
   qty                   REPL_RESULTS.ORDER_ROQ%TYPE,
   last_rounded_qty      REPL_RESULTS.LAST_ROUNDED_QTY%TYPE,
   last_grp_rounded_qty  REPL_RESULTS.LAST_GRP_ROUNDED_QTY%TYPE,
   ctry                  BUYER_WKSHT_MANUAL.ORIGIN_COUNTRY_ID%TYPE,
   unit_cost             REPL_RESULTS.UNIT_COST%TYPE,
   unit_cost_init        REPL_RESULTS.SUPP_UNIT_COST%TYPE,
   supp_lead_time        BUYER_WKSHT_MANUAL.SUPP_LEAD_TIME%TYPE,
   pickup_lead_time      BUYER_WKSHT_MANUAL.PICKUP_LEAD_TIME%TYPE,
   non_scale_ind         REPL_RESULTS.NON_SCALING_IND%TYPE,
   pack_size             REPL_RESULTS.CASE_SIZE%TYPE,
   eso                   NUMBER(5),
   aso                   NUMBER(5),
   tsf_po_link           REPL_RESULTS.TSF_PO_LINK_NO%TYPE,
   rr_rowid              VARCHAR2(30),
   ir_rowid              VARCHAR2(30),
   bw_rowid              VARCHAR2(30),
   pool_supp             REPL_RESULTS.PRIMARY_REPL_SUPPLIER%TYPE,
   file_id               ORD_INV_MGMT.FILE_ID%TYPE,
   contract_terms        VARCHAR2(15),
   contract_approval_ind VARCHAR2(1),
   contract_header_rowid VARCHAR2(30),
   split_ref_ord_no      ORDHEAD.SPLIT_REF_ORDNO%TYPE,
   splitting_increment   NUMBER(10),
   round_pct             NUMBER(6),
   model_qty             NUMBER(10),
   cnst_qty              CNST_QTY_TBL,
   cnst_map              CNST_QTY_TBL,
   ib_days_to_event      NUMBER(4),
   loc_country           SYSTEM_OPTIONS.BASE_COUNTRY_ID%TYPE,
   costing_loc           REPL_RESULTS.LOCATION%TYPE,
   store_type            VARCHAR2(6)
);

TYPE DRIV_CUR_INFO_PROCESS_TBL IS TABLE of DRIV_CUR_INFO_PROCESS_REC INDEX BY BINARY_INTEGER;

TYPE ORDLOC_REC IS RECORD
(
   source_type             REPL_RESULTS.SOURCE_TYPE%TYPE,
   loc                     ORDLOC.LOCATION%TYPE,
   phy_loc                 REPL_RESULTS.PHYSICAL_WH%TYPE,
   repl_wh_link            WH.REPL_WH_LINK%TYPE,
   loc_type                ORDLOC.LOC_TYPE%TYPE,
   due_ind                 VARCHAR2(1),
   item                    ORDLOC.ITEM%TYPE,
   pack_ind                VARCHAR2(1),
   xdock_ind               VARCHAR2(1),
   xdock_store             NUMBER(10), 
   non_scale_ind           ORDLOC.NON_SCALE_IND%TYPE,
   qty                     ORDLOC.QTY_ORDERED%TYPE,
   last_rounded_qty        ORDLOC.LAST_ROUNDED_QTY%TYPE,
   last_grp_rounded_qty    ORDLOC.LAST_GRP_ROUNDED_QTY%TYPE,

   /* splitting specific variables */
   cnst_qty                CNST_QTY_TBL,
   cnst_map                CNST_QTY_TBL,

   unit_cost               REPL_RESULTS.UNIT_COST%TYPE,
   unit_cost_init          REPL_RESULTS.SUPP_UNIT_COST%TYPE,
   cost_source             ORDLOC.COST_SOURCE%TYPE,
   tsf_po_link             ORDLOC.TSF_PO_LINK_NO%TYPE,
   rr_rowid                VARCHAR2(30),
   ir_rowid                VARCHAR2(30),
   bw_rowid                VARCHAR2(30),

   /* at least one item/location must have a positive order qty or
      be due for the item to be added to the order, this will track that */
   write_ind               VARCHAR2(1)
);

TYPE ORDLOC_TBL IS TABLE of ORDLOC_REC INDEX BY BINARY_INTEGER;

/* holds information needed to create an ORDSKU record */
TYPE ORDSKU_REC IS RECORD
(
   item                    ORDSKU.ITEM%TYPE,
   ctry                    ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
   pack_size               REPL_RESULTS.CASE_SIZE%TYPE,
   non_scale_ind           ORDLOC.NON_SCALE_IND%TYPE,
   splitting_increment     NUMBER(10),
   round_pct               NUMBER(6),
   model_qty               NUMBER(10),
   prorate_qty             ORDLOC.QTY_ORDERED%TYPE,
   fraction                NUMBER(12,4),
   min_supp_lead_time      NUMBER(4),
   max_supp_lead_time      NUMBER(4),

   /* at least one item/location must have a positive order qty or
      be due for the item to be added to the order depending on
      the sup_inv_mgmt.due_ord_process_ind, this will track that */
   write_ind               VARCHAR2(1),
   ordloc_tbl              ADD_LINE_ITEM_SQL.ORDLOC_TBL
);

TYPE ORDSKU_TBL IS TABLE of ORDSKU_REC INDEX BY BINARY_INTEGER;

/* holds supplier/dept info associated with each ordhead record */
TYPE SUPPLIER_INFO_REC IS RECORD
(
   qc_ind                  SUPS.QC_IND%TYPE,
   terms                   SUPS.TERMS%TYPE,
   freight_terms           SUPS.FREIGHT_TERMS%TYPE,
   payment_method          SUPS.PAYMENT_METHOD%TYPE,
   ship_method             SUPS.SHIP_METHOD%TYPE,
   edi_po_ind              SUPS.EDI_PO_IND%TYPE,
   currency_code           SUPS.CURRENCY_CODE%TYPE,
   pre_mark_ind            SUPS.PRE_MARK_IND%TYPE,
   inv_mgmt_lvl            SUPS.INV_MGMT_LVL%TYPE,
   dept_level_ord          VARCHAR2(1),
   agent                   SUP_IMPORT_ATTR.AGENT%TYPE,   
   discharge_port          SUP_IMPORT_ATTR.DISCHARGE_PORT%TYPE,
   lading_port             SUP_IMPORT_ATTR.LADING_PORT%TYPE,
   seq_no                  ADDR.SEQ_NO%TYPE,
   buyer                   DEPS.BUYER%TYPE,

   /* SUPS_INV_MGMT info */
   single_loc_ind          SUP_INV_MGMT.SINGLE_LOC_IND%TYPE,
   due_ord_process_ind     SUP_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE,
   due_ord_ind             SUP_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE,
   due_ord_lvl             SUP_INV_MGMT.DUE_ORD_LVL%TYPE,
   non_due_ord_create_ind  SUP_INV_MGMT.NON_DUE_ORD_CREATE_IND%TYPE,
   min_cnstr_lvl           SUP_INV_MGMT.MIN_CNSTR_LVL%TYPE,
   scale_cnstr_ind         SUP_INV_MGMT.SCALE_CNSTR_IND%TYPE,
   truck_split_ind         SUP_INV_MGMT.TRUCK_SPLIT_IND%TYPE,
   truck_method            SUP_INV_MGMT.TRUCK_SPLIT_METHOD%TYPE,
   cnst_type               ADD_LINE_ITEM_SQL.CNST_TYPE_TBL,
   cnst_uom                ADD_LINE_ITEM_SQL.CNST_UOM_TBL,
   cnst_value              ADD_LINE_ITEM_SQL.CNST_VAL_TBL,
   cnst_threshold          CNST_QTY_TBL,

   purchase_type           SUP_INV_MGMT.PURCHASE_TYPE%TYPE,
   pickup_loc              SUP_INV_MGMT.PICKUP_LOC%TYPE,

   sup_inv_mgmt_rowid      VARCHAR2(30),
   ord_inv_mgmt_rowid      VARCHAR2(30),

   /* only used/populated in split_po.pc */
   total_cnst_qty          CNST_QTY_TBL,
   cnst_value_reset        CNST_QTY_TBL,
   ltl_flag                NUMBER(1),
   /* determines which truck constraint is more constraining */
   cnst_index              NUMBER(4),

   /* Additional partners */
   partner_type_1          VARCHAR2(6),   
   partner_1               VARCHAR2(10),
   partner_type_2          VARCHAR2(6),
   partner_2               VARCHAR2(10),
   partner_type_3          VARCHAR2(6),
   partner_3               VARCHAR2(10),
   factory                 VARCHAR2(10)
);

TYPE SUPPLIER_INFO_TBL IS TABLE of SUPPLIER_INFO_REC INDEX BY BINARY_INTEGER;

/* holds information needed to create an ORDHEAD record */
TYPE ORDHEAD_REC IS RECORD
(
   order_no               ORDHEAD.ORDER_NO%TYPE,
   split_ref_ord_no       ORDHEAD.SPLIT_REF_ORDNO%TYPE,
   supplier               ORDHEAD.SUPPLIER%TYPE,
   pool_supp              ORDHEAD.SUPPLIER%TYPE,
   file_id                ORD_INV_MGMT.FILE_ID%TYPE,
   import_order           ORDHEAD.IMPORT_ORDER_IND%TYPE,
   order_status           ORDHEAD.STATUS%TYPE,

   /* needed here for insert and single dept orders */
   dept                   ORDHEAD.DEPT%TYPE,

   /* needed here to deal with single location orders */
   order_loc              ORDHEAD.LOCATION%TYPE,
   order_loc_type         ORDHEAD.LOC_TYPE%TYPE,

   contract               ORDHEAD.CONTRACT_NO%TYPE,
   contract_type          VARCHAR2(2),
   contract_terms         VARCHAR2(15),
   contract_approval_ind  VARCHAR2(1),
   contract_header_rowid  VARCHAR2(30),

   /* these are updated for each item/location added to the order */
   min_supp_lead_time     NUMBER(4),
   max_supp_lead_time     NUMBER(4),
   min_pickup_lead_time   NUMBER(4),
   max_pickup_lead_time   NUMBER(4),

   /* these are used to determine if the order as a whole is due, if */
   /* due ordering is on and the due order level is 'O'rder */
   eso                    VARCHAR2(4),
   aso                    VARCHAR2(4),

   /* at least one item/location must have a positive order qty for
      the order to be written */
   write_ind              VARCHAR2(1),

   /* ONLY used when due order processing is on, and the due
      order level is 'I', then it is used what to write to
      the ord_inv_mgmt.due_ind field  */
   itemloc_due_ind        VARCHAR2(1),

   /* set when atleast one line item is sourced from investment buy */
   ib_flag                VARCHAR2(4),

  /* used only when importer/exporter entity used for routing of orders */
   import_id              ORDHEAD.IMPORT_ID%TYPE,
   routing_loc_id         ORDHEAD.ROUTING_LOC_ID%TYPE,

   /* set when clearing zone is setup for the import country and the import_order_ind = ‘Y’*/
   clearing_zone_id       ORDHEAD.CLEARING_ZONE_ID%TYPE,

   /* holds import country id for the order */
   import_country_id      ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
   sup_info               ADD_LINE_ITEM_SQL.SUPPLIER_INFO_REC,
   ordsku_tbl             ADD_LINE_ITEM_SQL.ORDSKU_TBL
);

TYPE ORDHEAD_TBL IS TABLE of ORDHEAD_REC INDEX BY BINARY_INTEGER;

-------------------------------------------------------------------------------
--                             PUBLIC FUNCTIONS                              --
-------------------------------------------------------------------------------

--------------------------------------------------------------------------
FUNCTION SET_GLOBALS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
FUNCTION ADD_TO_ORDER(I_ordhead_tbl        IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                      I_get_cur_info       IN OUT  ADD_LINE_ITEM_SQL.DRIV_CUR_INFO_PROCESS_REC,
                      I_get_sup_info       IN      ADD_LINE_ITEM_SQL.SUPPLIER_INFO_REC,
                      I_order_no           IN      ORDHEAD.ORDER_NO%TYPE,
                      O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
FUNCTION GET_INV_MGMT(I_sup_info      IN  OUT   ADD_LINE_ITEM_SQL.SUPPLIER_INFO_REC,
                      I_order_no      IN        ORDHEAD.ORDER_NO%TYPE,
                      O_pool_supp     IN OUT    ORDHEAD.SUPPLIER%TYPE,
                      O_file_id       IN OUT    ORD_INV_MGMT.FILE_ID%TYPE,
                      O_error_message IN OUT    RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
FUNCTION GET_SUP_INFO(I_supp_info        IN  OUT   ADD_LINE_ITEM_SQL.SUPPLIER_INFO_REC,
                      I_contract         IN        ORDHEAD.CONTRACT_NO%TYPE,
                      I_supplier         IN        ORDHEAD.SUPPLIER%TYPE,
                      I_dept             IN        ORDHEAD.DEPT%TYPE,
                      I_loc              IN        ORDHEAD.LOCATION%TYPE,
                      O_error_message    IN OUT    RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
END ADD_LINE_ITEM_SQL;
/