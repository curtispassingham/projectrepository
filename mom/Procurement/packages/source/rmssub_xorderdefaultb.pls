
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XORDER_DEFAULT AS

FUNCTION DEFAULT_ORIGIN_COUNTRY_ID(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_origin_country_id  IN OUT ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                   IO_exists             IN OUT BOOLEAN,
                                   I_item                IN     ITEM_SUPPLIER.ITEM%TYPE,
                                   I_supplier            IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(100) := 'RMSSUB_XORDER_DEFAULT.DEFAULT_ORIGIN_COUTRY_ID';

BEGIN
   if not ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                    IO_exists,
                                                    IO_origin_country_id,
                                                    I_item,
                                                    I_supplier) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_ORIGIN_COUNTRY_ID;

----------------------------------------------------------------------------
END RMSSUB_XORDER_DEFAULT;
/
