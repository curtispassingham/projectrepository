CREATE OR REPLACE PACKAGE ORDER_ITEM_ATTRIB_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------
-- Function Name: GET_REF_ITEM
-- Purpose:       This function will return the ref_item
--                associated with the item passed in I_item
--                as recorded on the ordsku table.
------------------------------------------------------------------------
FUNCTION GET_REF_ITEM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_ref_item       IN OUT ORDSKU.REF_ITEM%TYPE,
                      I_order_no       IN     ORDSKU.ORDER_NO%TYPE,
                      I_item           IN     ORDSKU.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_ALLOC_QTY
-- Purpose:       This function will return the total quantity
--                allocated for an order/item/location.
------------------------------------------------------------------------
FUNCTION GET_ALLOC_QTY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_qty_allocated IN OUT ORDLOC.QTY_ORDERED%TYPE,
                       I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
                       I_item          IN     ORDLOC.ITEM%TYPE,
                       I_location      IN     ORDLOC.LOCATION%TYPE,
                       I_loc_type      IN     ORDLOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_ORIGIN_COUNTRY
-- Purpose:       This function will be used to retrieve PO item
--                origin country id from the PO Item table (ordsku) for the
--                PO and Item passed in.
------------------------------------------------------------------------
FUNCTION GET_ORIGIN_COUNTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists            IN OUT BOOLEAN,
                            O_origin_country_id IN OUT ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                            I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                            I_item              IN     ORDSKU.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_HTS_ORIGIN_COUNTRY
-- Purpose:       This function will be used to retrieve PO item
--                origin country id from the PO Item HTS table (ordsku_hts) for the
--                PO and Item passed in.
------------------------------------------------------------------------
FUNCTION GET_HTS_ORIGIN_COUNTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists            IN OUT BOOLEAN,
                                O_origin_country_id IN OUT ORDSKU_HTS.ORIGIN_COUNTRY_ID%TYPE,
                                I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                I_item              IN     ORDSKU_HTS.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: ITEM_EXISTS
-- Purpose:       This function will be used to determine if an item already
--                exists on the Order Item table (ordsku).If order_no is NULL, then
--                the function will determine if passed-in item exists on an order.
------------------------------------------------------------------------
FUNCTION ITEM_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists            IN OUT BOOLEAN,
                     I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                     I_item              IN     ORDSKU.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: ORDER_EXISTS
-- Purpose:       This function will be used to determine if an order
--                exists on the Order Item table (ordsku).
------------------------------------------------------------------------
FUNCTION ORDER_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists            IN OUT BOOLEAN,
                      I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: ITEM_RECD
-- Purpose:       This function will be used to determine if a specific item on
--                a purchase order has been recieved against or if any receipts
--                have been made against the purchase order.
------------------------------------------------------------------------
FUNCTION ITEM_RECD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_item_recd      IN OUT BOOLEAN,
                   I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                   I_item           IN     ORDSKU.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: HTS_EXISTS
-- Purpose:       This function will be used to determine if a specific item on
--                a purchase order has HTS codes associated with it.
------------------------------------------------------------------------
FUNCTION HTS_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists            IN OUT BOOLEAN,
                    I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                    I_item              IN     ORDSKU.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_ORDSKU_PACK_SIZE
-- Purpose:       This function will retrieve the supplier pack size from
--                the ordsku table for the sku and order number passed in.
------------------------------------------------------------------------
FUNCTION GET_ORDSKU_PACK_SIZE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_supp_pack_size IN OUT ORDSKU.SUPP_PACK_SIZE%TYPE,
                              I_item           IN     ORDSKU.ITEM%TYPE,
                              I_order_no       IN     ORDSKU.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_QTY_RECEIVED
-- Purpose:       This function will return the total received qty
--                for an order/item combination.
--                If the item is a buyer pack, it should be passed into
--                the pack_item parameter and the component item within the
--                pack should be passed in I_item.
------------------------------------------------------------------------
FUNCTION GET_QTY_RECEIVED (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists        IN OUT BOOLEAN,
                           O_qty_received  IN OUT ORDLOC.QTY_RECEIVED%TYPE,
                           I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                           I_item          IN     ITEM_MASTER.ITEM%TYPE,
                           I_pack_item     IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_QTY_ORDERED
-- Purpose:       This function will return the total ordered qty
--                for an order/item combination.
--                If the item is a buyer pack, it should be passed into
--                the pack_item parameter and the component item within the
--                pack should be passed in I_item.
------------------------------------------------------------------------
FUNCTION GET_QTY_ORDERED(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists        IN OUT BOOLEAN,
                         O_qty_ordered   IN OUT ORDLOC.QTY_ORDERED%TYPE,
                         I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item          IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item     IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_QTY_SHIPPED
-- Purpose:       This function will return the total shipped qty
--                for an order/item combination.
--                If the item is a buyer pack, it should be passed into
--                the pack_item parameter and the component item within the
--                pack should be passed in I_item.
------------------------------------------------------------------------
FUNCTION GET_QTY_SHIPPED(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists        IN OUT BOOLEAN,
                         O_qty_shipped   IN OUT SHIPSKU.QTY_EXPECTED%TYPE,
                         I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item          IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item     IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_UNIT_COST
-- Purpose:       This function will return the unit cost
--                for an order/item or order/item/location combination.
--                If the item is a buyer pack, it should be passed into
--                the pack_item parameter and the component item within the
--                pack should be passed in I_item.
------------------------------------------------------------------------
FUNCTION GET_UNIT_COST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists        IN OUT BOOLEAN,
                       O_unit_cost     IN OUT ORDLOC.UNIT_COST%TYPE,
                       I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE,
                       I_pack_item     IN     ITEM_MASTER.ITEM%TYPE,
                       I_location      IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_SUPP_PACK_SIZE
-- Purpose:       This function will be used to retrieve PO item
--                supp pack size from the PO Item table (ordsku) for the
--                PO and Item passed in.
------------------------------------------------------------------------
FUNCTION GET_SUPP_PACK_SIZE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists            IN OUT BOOLEAN,
                            O_supp_pack_size    IN OUT ORDSKU.SUPP_PACK_SIZE%TYPE,
                            I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                            I_item              IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: ORDER_ITEM_EXISTS
-- Purpose:       This function will be used to determine if the passed-in order
--                exists on the Order Item table (ordsku) for the passed-in
--                item.
------------------------------------------------------------------------
FUNCTION ORDER_ITEM_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists            IN OUT BOOLEAN,
                           I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                           I_item              IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_VALID_DIFF1
-- Purpose      : This function will validate and retrieve the description
--                for diff_1 values
------------------------------------------------------------------------
FUNCTION GET_VALID_DIFF1(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT BOOLEAN,
                         O_diff_desc       IN OUT DIFF_IDS.DIFF_DESC%TYPE,
                         I_diff1           IN      DIFF_IDS.DIFF_ID%TYPE,
                         I_item_parent     IN     ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_VALID_DIFF2
-- Purpose      : This function will validate and retrieve the description
--                for diff_2 values
------------------------------------------------------------------------
FUNCTION GET_VALID_DIFF2(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT BOOLEAN,
                         O_diff_desc       IN OUT DIFF_IDS.DIFF_DESC%TYPE,
                         I_diff2           IN      DIFF_IDS.DIFF_ID%TYPE,
                         I_item_parent     IN     ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_ORDER_ITEM_COUNTRY
-- Purpose      : This function will retrieve the origin country id from ordsku
------------------------------------------------------------------------
FUNCTION GET_ORDER_ITEM_COUNTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists            IN OUT BOOLEAN,
                                O_origin_country_id IN OUT ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                I_item              IN     ORDSKU.ITEM%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_HTS_ORDER_ITEM_COUNTRY
-- Purpose      : This function will retrieve the origin country id from ordsku_hts
------------------------------------------------------------------------
FUNCTION GET_HTS_ORDER_ITEM_COUNTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists            IN OUT BOOLEAN,
                                    O_origin_country_id IN OUT ORDSKU_HTS.ORIGIN_COUNTRY_ID%TYPE,
                                    I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                    I_item              IN     ORDSKU_HTS.ITEM%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_COST_SOURCE
-- Purpose:       This function will retrieve the cost source for a order/item
--                or an order/item/location combination.
------------------------------------------------------------------------
FUNCTION GET_COST_SOURCE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists        IN OUT BOOLEAN,
                         O_cost_source   IN OUT  ORDLOC.COST_SOURCE%TYPE,
                         I_order_no      IN      ORDSKU.ORDER_NO%TYPE,
                         I_item          IN      ORDSKU.ITEM%TYPE,
                         I_location      IN      ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
--- Function Name: GET_UNITS_COST
--- Purpose:  This function will retrieve the total pre-scaled order units and
---           order units and total pre-scaled order cost and total order cost
---           for an order/item/location or order/allocation/location.
------------------------------------------------------------------------
FUNCTION GET_UNITS_COST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_prescaled_units IN OUT ORDLOC.QTY_PRESCALED%TYPE,
                        O_actual_units    IN OUT ORDLOC.QTY_ORDERED%TYPE,
                        O_prescaled_cost  IN OUT ORDLOC.UNIT_COST%TYPE,
                        O_actual_cost     IN OUT ORDLOC.UNIT_COST%TYPE,
                        I_order_no        IN     ORDLOC.ORDER_NO%TYPE,
                        I_alloc_no        IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                        I_item            IN     ORDLOC.ITEM%TYPE,
                        I_location        IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
--- Function Name: GET_ORDSKU_NON_SCALING_IND
--- Purpose:  This function will retrieve the non scale ind
------------------------------------------------------------------------
FUNCTION GET_ORDSKU_NON_SCALING_IND(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_non_scale_ind     IN OUT ORDSKU.NON_SCALE_IND%TYPE,
                                    I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                    I_item              IN     ORDSKU.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_QTYS
-- Purpose:       This function will return order qtys for the passed in order/item.
------------------------------------------------------------------------
FUNCTION GET_QTYS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_exists        IN OUT BOOLEAN,
                  O_qty_ordered   IN OUT ORDLOC.QTY_ORDERED%TYPE,
                  O_qty_shipped   IN OUT SHIPSKU.QTY_EXPECTED%TYPE,
                  O_qty_prescaled IN OUT ORDLOC.QTY_PRESCALED%TYPE,
                  O_qty_received  IN OUT ORDLOC.QTY_RECEIVED%TYPE,
                  O_qty_cancelled IN OUT ORDLOC.QTY_CANCELLED%TYPE,
                  I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                  I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: UPDATE_UNIT_COST
-- Purpose:       This function will update the standard UOM unit cost
--                for all locations for the passed in order/item
--                combination.
------------------------------------------------------------------------
FUNCTION UPDATE_UNIT_COST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                          I_item          IN     ITEM_MASTER.ITEM%TYPE,
                          I_unit_cost     IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                          I_cost_source   IN     ORDLOC.COST_SOURCE%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: COST_SOURCE_EXISTS
-- Purpose:       This function check for the existence of a record on the
--                order/item/location table where the cost source is the
--                value passed in.
------------------------------------------------------------------------
FUNCTION COST_SOURCE_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists        IN OUT BOOLEAN,
                            I_order_no      IN     ORDSKU.ORDER_NO%TYPE,
                            I_item          IN     ORDSKU.ITEM%TYPE,
                            I_cost_source   IN     ORDLOC.COST_SOURCE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: UPDATE_PICKUP_LOC
-- Purpose:       This function will be used to update the PO/Item
--                pickup location on the PO Item table (ordsku) for the
--                PO/pickup loc passed in.
------------------------------------------------------------------------
FUNCTION UPDATE_PICKUP_LOC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_pickup_loc    IN     ORDHEAD.PICKUP_LOC%TYPE,
                           I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: UPDATE_PICKUP_NO
-- Purpose:       This function will be used to update the PO/Item
--                pickup number on the PO Item table (ordsku) for the
--                PO/pickup loc passed in.
------------------------------------------------------------------------
FUNCTION UPDATE_PICKUP_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_pickup_no     IN     ORDHEAD.PICKUP_NO%TYPE,
                          I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: ALLOCS_EXIST
-- Purpose:       This function will be used to find out if any
--                allocations exist for the order/item combination
------------------------------------------------------------------------
FUNCTION ALLOCS_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists        IN OUT BOOLEAN,
                      I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                      I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_UNIT_COST_INIT
-- Purpose:       This function will return the initial unit cost
--                for an order/item or order/item/location combination.
------------------------------------------------------------------------
FUNCTION GET_UNIT_COST_INIT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists        IN OUT BOOLEAN,
                            O_unit_cost     IN OUT ORDLOC.UNIT_COST%TYPE,
                            I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                            I_item          IN     ITEM_MASTER.ITEM%TYPE,
                            I_location      IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_ITEM_INFO
-- Purpose:       This function will return the unit cost, qty_ordered,
--                origin_country_id, supp_pack_size for an
--                order/item/location combination.
------------------------------------------------------------------------
FUNCTION GET_ITEM_INFO(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_unit_cost         IN OUT ORDLOC.UNIT_COST%TYPE,
                       O_qty_ordered       IN OUT ORDLOC.QTY_ORDERED%TYPE,
                       O_supp_pack_size    IN OUT ORDSKU.SUPP_PACK_SIZE%TYPE,
                       O_origin_country_id IN OUT COUNTRY.COUNTRY_ID%TYPE,
                       I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                       I_item              IN     ITEM_MASTER.ITEM%TYPE,
                       I_location          IN     ORDLOC.LOCATION%TYPE,
                       I_origin_type       IN     ORDLOC_REV.ORIGIN_TYPE%TYPE,
                       I_rev_no            IN     ORDLOC_REV.REV_NO%TYPE,
                       I_rev_date          IN     ORDLOC_REV.REV_DATE%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_PREV_QTY_INFO
-- Purpose:       This function will return the unit cost, previously
--                ordered qty for an order/item/location/revision_no combination.
------------------------------------------------------------------------
FUNCTION GET_PREV_QTY_INFO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_unit_cost     IN OUT ORDLOC.UNIT_COST%TYPE,
                           O_qty_ordered   IN OUT ORDLOC.QTY_ORDERED%TYPE,
                           I_revision_no   IN     ORDLOC_REV.REV_NO%TYPE,
                           I_origin_type   IN     ORDLOC_REV.ORIGIN_TYPE%TYPE,
                           I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                           I_item          IN     ITEM_MASTER.ITEM%TYPE,
                           I_location      IN     ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: ORDLOC_EXISTS
-- Purpose:       This function will be used to determine if an order
--                exists on the Order Location table (ordloc).
------------------------------------------------------------------------
FUNCTION ORDLOC_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists            IN OUT BOOLEAN,
                       I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------

-- Function Name: ORD_NO_EXISTS
-- Purpose:       This function will be used to determine if an order
--                exists on the Order table (ordhead) using v_ordhead.
------------------------------------------------------------------------
FUNCTION ORD_NO_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid          IN OUT  BOOLEAN,
                       O_ordhead        IN OUT  V_ORDHEAD%ROWTYPE,
                       I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------

------------------------------------------------------------------------
-- Function Name: OLD_UNIT_COSTS
-- Purpose: This funciton provides the old unit cost.     
------------------------------------------------------------------------
FUNCTION OLD_UNIT_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_supplier_cost       IN OUT   ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                       I_pack_item           IN       PACKITEM_BREAKOUT.PACK_NO%TYPE,
                       I_item                IN       ORDSKU.ITEM%TYPE,
                       I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                       I_location            IN       ORDHEAD.LOCATION%TYPE,
                       I_supplier            IN       ORDHEAD.SUPPLIER%TYPE,
                       I_origin_country_id   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;                       

------------------------------------------------------------------------
END ORDER_ITEM_ATTRIB_SQL;
/
