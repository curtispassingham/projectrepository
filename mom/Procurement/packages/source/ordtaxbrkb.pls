CREATE OR REPLACE PACKAGE BODY ORDTAXBRK_SQL AS
------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE_ENTITY (IO_po_tax_breakup_entity_tbl   IN OUT   PO_TAX_BREAKUP_ENTITY_REC_TBL,
                                  I_order_no                     IN       ORDHEAD.ORDER_NO%TYPE)
IS

   L_program     VARCHAR2(65)   := 'ORDTAXBRK_SQL.QUERY_PROCEDURE_ENTITY';

   cursor C_PO_TAX_BKUP_ENTITY_QRY is
      select distinct 
	         case
                when s.supplier_sites_ind = 'Y' then
                   (select code_desc from code_detail where code_type = 'SSTY' and code = 'SS')
                when s.supplier_sites_ind = 'N' then
                   (select code_desc from code_detail where code_type = 'SSTY' and code = 'SUPP')
             end from_entity_type,
	         ohd.supplier from_entity_id,
             sp.sup_name from_entity_description,
             loc.loc_desc to_entity_type,
             olo.location to_entity_id,
             loc.loc_name to_entity_description,
             loc.loc_type loc_type,
             NULL error_message,
             'TRUE' return_code
        from ordhead ohd,
             ordloc olo,
             v_sups_tl sp,
             (select s.store location,
                     (select code_desc from v_code_detail_tl where code_type = 'LOTP' and code = 'S') loc_desc,
                     s.store_name loc_name,
                     'S' loc_type
                from v_store vs,
                     store s
               where s.store = vs.store
               union all
              select wh.wh location,
                     (select code_desc from v_code_detail_tl where code_type = 'LOTP' and code = 'W') loc_desc,
                     wh.wh_name loc_name,
                     'W' loc_type
                from v_wh,
                     wh
               where v_wh.wh = wh.wh) loc,
               system_options s
       where olo.order_no      = I_order_no
         and ohd.order_no      = olo.order_no
         and sp.supplier     = ohd.supplier
         and loc.location      = olo.location
    order by ohd.supplier;

BEGIN

   if I_order_no is NULL then
      IO_po_tax_breakup_entity_tbl(1).return_code   := 'FALSE';
      IO_po_tax_breakup_entity_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                       'I_order_no',
                                                                        L_program,
                                                                        NULL);
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_PO_TAX_BKUP_ENTITY_QRY',
                       'ORDLOC',
                       'ORDER_NO: ' || I_order_no);
      open  C_PO_TAX_BKUP_ENTITY_QRY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_PO_TAX_BKUP_ENTITY_QRY',
                       'ORDLOC',
                       'ORDER_NO: ' || I_order_no);
      fetch C_PO_TAX_BKUP_ENTITY_QRY BULK COLLECT into IO_po_tax_breakup_entity_tbl;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_PO_TAX_BKUP_ENTITY_QRY',
                       'ORDLOC',
                       'ORDER_NO: ' || I_order_no);
      close C_PO_TAX_BKUP_ENTITY_QRY;
   end if;
EXCEPTION
   when OTHERS then
      IO_po_tax_breakup_entity_tbl(1).return_code   := 'FALSE';
      IO_po_tax_breakup_entity_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                       SQLERRM,
                                                                       L_program,
                                                                       to_char(SQLCODE));
END QUERY_PROCEDURE_ENTITY;
------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE_ITEM (IO_po_tax_breakup_item_tbl   IN OUT   PO_TAX_BREAKUP_ITEM_REC_TBL,
                                I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
                                I_location              IN       ORDLOC.LOCATION%TYPE,
                                I_loc_type              IN       ORDLOC.LOC_TYPE%TYPE)
IS

   L_program     VARCHAR2(65)   := 'ORDTAXBRK_SQL.QUERY_PROCEDURE_ITEM';

   cursor C_PO_TAX_BKUP_ITEM_QRY is
      select otb.item,
             im.item_desc,
             otb.currency_code currency,
             ---
             -- When default_po_cost is 'BC', unit_cost on PO is already tax exclusive.
             -- When default_po_cost is 'NIC' (i.e. NOT 'BC'), unit_cost on PO already includes NIC taxes,
             -- which is identified with 'incl_nic_ind' of 'Y' on the VAT_CODES table. Need to subtract that
             -- portion of the tax (tax_help1.total_incl_tax_amt) from the PO unit_cost to get the tax exclusive unit_cost.
             ---
             decode(ca.default_po_cost,'BC',olo.unit_cost,(olo.unit_cost - sum(decode(olo.qty_ordered, 0, 0,tax_help1.total_incl_tax_amt/olo.qty_ordered)))) unit_excl_tax_amt,   
             ---
             -- When default_po_cost is 'BC', unit_cost on PO contains NO tax at all. Need to add ALL 
             -- non-recoverable taxes (regardless of incl_nic_ind on VAT_CODES) to get the tax inclusive unit cost.
             -- When default_po_cost is 'NIC' (i.e. NOT 'BC'), unit_cost on PO already includes NIC taxes. 
             -- Need to add the non-NIC/non-recoverable taxes to the unit_cost on PO to get the tax inclusive unit cost.
             ---
             decode(ca.default_po_cost,'BC',olo.unit_cost + SUM(decode(olo.qty_ordered, 0, 0, (nvl(otb.total_tax_amt,0)/olo.qty_ordered) - (nvl(otb.recoverable_amount,0))/olo.qty_ordered)),   
                                            olo.unit_cost + SUM(decode(olo.qty_ordered, 0, 0, (tax_help1.total_excl_tax_amt/olo.qty_ordered) - (tax_help1.recoverable_excl_amount/olo.qty_ordered)))) unit_incl_tax_amt, 
             SUM(decode(olo.qty_ordered, 0, 0, nvl(otb.total_tax_amt,0)/olo.qty_ordered)) unit_tax_amt,
             olo.qty_ordered quantity,
			 SUM(decode(olo.qty_ordered, 0, 0, otb.total_tax_amt)) total_tax_amt,
             NULL error_message,
             'TRUE' return_code              
        from ord_tax_breakup    otb,
             v_item_master_tl   im,
             ordloc         olo,
             ordhead        oh,
             country_attrib ca,
             (select brk.order_no,
                     brk.item,
                     brk.location,
                     brk.location_type,
                     brk.tax_code,
                     DECODE(vc.incl_nic_ind, 'Y', nvl(brk.total_tax_amt,0),0) total_incl_tax_amt, --This holds the total NIC tax for the tax code. Total in the sense of order qty.   
                     DECODE(vc.incl_nic_ind, 'N', nvl(brk.total_tax_amt,0),0) total_excl_tax_amt, --This holds the total non-NIC tax for the tax code. Total in the sense of order qty.   
                     DECODE(vc.incl_nic_ind, 'Y', nvl(brk.recoverable_amount,0),0) recoverable_incl_amount, --This holds the total recoverable tax included in NIC for the tax code. Total in the sense of order qty.
                     DECODE(vc.incl_nic_ind, 'N', nvl(brk.recoverable_amount,0),0) recoverable_excl_amount  --This holds the total recoverable tax NOT included in NIC for the tax code. Total in the sense of order qty.
                from ord_tax_breakup brk,
                     vat_codes vc
               where brk.order_no = I_order_no
                 and brk.location = I_location
                 and brk.location_type = I_loc_type
                 and brk.tax_code = vc.vat_code) tax_help1
       where otb.order_no      = I_order_no
         and olo.order_no      = otb.order_no
         and oh.order_no       = otb.order_no
         and otb.location      = I_location
         and olo.location      = otb.location
         and otb.location_type = I_loc_type
         and olo.loc_type = otb.location_type
         and olo.item          = otb.item
         and im.item           = otb.item
         and oh.import_country_id = ca.country_id
         and otb.order_no = tax_help1.order_no
         and otb.item = tax_help1.item
         and otb.location = tax_help1.location
         and otb.location_type = tax_help1.location_type
         and otb.tax_code = tax_help1.tax_code
    group by otb.item, im.item_desc, otb.currency_code, ca.default_po_cost, olo.unit_cost, olo.qty_ordered;   

BEGIN

   if I_order_no is NULL then
      IO_po_tax_breakup_item_tbl(1).return_code   := 'FALSE';
      IO_po_tax_breakup_item_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                        'I_order_no',
                                                                        L_program,
                                                                        NULL);
   elsif I_location is NULL then
      IO_po_tax_breakup_item_tbl(1).return_code   := 'FALSE';
      IO_po_tax_breakup_item_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                        'I_location',
                                                                         L_program,
                                                                         NULL);
   elsif I_loc_type is NULL then
      IO_po_tax_breakup_item_tbl(1).return_code   := 'FALSE';
      IO_po_tax_breakup_item_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                        'I_loc_type',
                                                                         L_program,
                                                                         NULL);
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_PO_TAX_BKUP_ITEM_QRY',
                       'ORD_TAX_BREAKUP,ITEM_MASTER,ORDLOC',
                       'ORDER_NO: ' || I_order_no);
      open  C_PO_TAX_BKUP_ITEM_QRY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_PO_TAX_BKUP_ITEM_QRY',
                       'ORD_TAX_BREAKUP,ITEM_MASTER,ORDLOC',
                       'ORDER_NO: ' || I_order_no);
      fetch C_PO_TAX_BKUP_ITEM_QRY BULK COLLECT into IO_po_tax_breakup_item_tbl;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_PO_TAX_BKUP_ITEM_QRY',
                       'ORD_TAX_BREAKUP,ITEM_MASTER,ORDLOC',
                       'ORDER_NO: ' || I_order_no);
      close C_PO_TAX_BKUP_ITEM_QRY;
   end if;
EXCEPTION
   when OTHERS then
      IO_po_tax_breakup_item_tbl(1).return_code   := 'FALSE';
      IO_po_tax_breakup_item_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                        SQLERRM,
                                                                        L_program,
                                                                        to_char(SQLCODE));
END QUERY_PROCEDURE_ITEM; 
------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE_TAX(IO_po_tax_breakup_tbl   IN OUT     PO_TAX_BREAKUP_REC_TBL,
                                I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
                                I_item                  IN       ORDLOC.ITEM%TYPE,
                                I_location              IN       ORDLOC.LOCATION%TYPE,
                                I_loc_type              IN       ORDLOC.LOC_TYPE%TYPE)
IS

   L_program     VARCHAR2(65)   := 'ORDTAXBRK_SQL.QUERY_PROCEDURE_TAX';
   L_pack_ind    ITEM_MASTER.PACK_IND%TYPE := 'N';

   cursor C_pack_ind is
      select pack_ind
        from item_master
       where item = I_item;

   cursor C_PO_TAX_BKUP_QRY is
      select otb.tax_code,
             otb.tax_rate,
             otb.calculation_basis,
             otb.per_count,
             otb.per_count_uom,
             NVL(otb.total_tax_amt,0)/(olo.qty_ordered) unit_tax_amt,
             DECODE(L_pack_ind,'Y',(NVL(otb.taxable_base, 0) / olo.qty_ordered),(NVL(otb.taxable_base, 0) / olo.qty_ordered)),
             DECODE(L_pack_ind,'Y',(NVL(otb.modified_taxable_base, 0) / olo.qty_ordered),(NVL(otb.modified_taxable_base, 0) / olo.qty_ordered)),
             NVL(otb.recoverable_amount,0)/olo.qty_ordered,
             SUM(NVL(otb.total_tax_amt,0)) total_tax_amt,             
             NULL error_message,
             'TRUE' return_code
        from ord_tax_breakup otb,
             ordloc olo
       where olo.order_no      = I_order_no
         and otb.order_no      = olo.order_no
         and otb.item          = I_item
         and otb.item          = olo.item
         and otb.location      = I_location
         and otb.location      = olo.location
         and otb.location_type      = I_loc_type
         and otb.location_type      = olo.loc_type
    group by otb.tax_code, otb.tax_rate, otb.calculation_basis, otb.per_count, otb.per_count_uom,
             otb.total_tax_amt, olo.qty_ordered, otb.taxable_base, otb.modified_taxable_base, otb.recoverable_amount;
             
BEGIN

   if I_order_no is NULL then
      IO_po_tax_breakup_tbl(1).return_code   := 'FALSE';
      IO_po_tax_breakup_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                   'I_order_no',
                                                                   L_program,
                                                                   NULL);
   elsif I_item is NULL then
      IO_po_tax_breakup_tbl(1).return_code   := 'FALSE';
      IO_po_tax_breakup_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                   'I_item',
                                                                    L_program,
                                                                    NULL);
   elsif I_location is NULL then
      IO_po_tax_breakup_tbl(1).return_code   := 'FALSE';
      IO_po_tax_breakup_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                   'I_location',
                                                                    L_program,
                                                                    NULL);
   elsif I_loc_type is NULL then
      IO_po_tax_breakup_tbl(1).return_code   := 'FALSE';
      IO_po_tax_breakup_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                   'I_loc_type',
                                                                    L_program,
                                                                    NULL);
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_pack_ind',
                       'ITEM_MASTER',
                       'ITEM: ' || I_item);
      open  C_pack_ind;
      SQL_LIB.SET_MARK('FETCH',
                       'C_pack_ind',
                       'ITEM_MASTER',
                       'ITEM: ' || I_item);
      fetch C_pack_ind into L_pack_ind;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_pack_ind',
                       'ITEM_MASTER',
                       'ITEM: ' || I_item);
      close C_pack_ind;
      
      SQL_LIB.SET_MARK('OPEN',
                       'C_PO_TAX_BKUP_QRY',
                       'ORD_TAX_BREAKUP,ORDLOC',
                       'ORDER_NO: ' || I_order_no);
      open  C_PO_TAX_BKUP_QRY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_PO_TAX_BKUP_QRY',
                       'ORD_TAX_BREAKUP,ORDLOC',
                       'ORDER_NO: ' || I_order_no);
      fetch C_PO_TAX_BKUP_QRY BULK COLLECT into IO_po_tax_breakup_tbl;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_PO_TAX_BKUP_QRY',
                       'ORD_TAX_BREAKUP,ORDLOC',
                       'ORDER_NO: ' || I_order_no);
      close C_PO_TAX_BKUP_QRY;
 
   end if;
 
EXCEPTION
   when OTHERS then
      IO_po_tax_breakup_tbl(1).return_code   := 'FALSE';
      IO_po_tax_breakup_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    to_char(SQLCODE));
END QUERY_PROCEDURE_TAX;    
------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORDTAXBRK(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no       IN       ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN is
       
   L_program        VARCHAR2(255) := 'ORDTAXBRK_SQL.DELETE_ORDTAXBRK';
   
   cursor C_LOCK_ORD_TAX_BREAKUP is
      select 'x'
        from ord_tax_breakup
       where order_no = I_order_no
      for update nowait;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
      
   delete from ORD_TAX_BREAKUP
    where order_no = I_order_no;
   
   return TRUE;
       
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_ORDTAXBRK;     
------------------------------------------------------------------------------------------------------------
END ORDTAXBRK_SQL;
/