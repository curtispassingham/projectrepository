create or replace
PACKAGE DEAL_ORD_LIB_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------------------
   TYPE item_cost_rec IS RECORD
        (item                ITEM_MASTER.ITEM%TYPE,
         location            ORDLOC.LOCATION%TYPE,
         virtual_loc         ORDLOC.LOCATION%TYPE,
         unit_cost           ORDLOC.UNIT_COST%TYPE,
         old_unit_cost       ORDLOC.UNIT_COST%TYPE,
         order_no            ORDLOC.ORDER_NO%TYPE,
         num_cost_decimals   ORDLOC.UNIT_COST%TYPE
        );
   TYPE item_cost_tbl IS TABLE of item_cost_rec INDEX BY BINARY_INTEGER;

   TYPE order_rec IS RECORD
        (order_no                ORDHEAD.ORDER_NO%TYPE,
         recalc_all_ind          DEAL_CALC_QUEUE.RECALC_ALL_IND%TYPE,
         override_manual_ind     DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE,
         order_appr_ind          DEAL_CALC_QUEUE.ORDER_APPR_IND%TYPE,
         supplier                ORDHEAD.SUPPLIER%TYPE,
         import_order_ind        ORDHEAD.IMPORT_ORDER_IND%TYPE,
         import_country_id       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
         not_before_date         ORDHEAD.NOT_BEFORE_DATE%TYPE,
         currency_code           ORDHEAD.CURRENCY_CODE%TYPE,
         status                  ORDHEAD.STATUS%TYPE,
         exchange_rate           ORDHEAD.EXCHANGE_RATE%TYPE,
         supp_currency_code      ORDHEAD.CURRENCY_CODE%TYPE,
         bracket_costing_ind     SUPS.BRACKET_COSTING_IND%TYPE
        );
   TYPE order_tbl IS TABLE of order_rec INDEX BY BINARY_INTEGER;

   TYPE deal_rec IS RECORD
        (deal_id                              DEAL_DETAIL.DEAL_ID%TYPE,
         deal_detail_id                       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
         type                                 DEAL_HEAD.TYPE%TYPE,
         currency_code                        DEAL_HEAD.CURRENCY_CODE%TYPE,
         application_order                    DEAL_DETAIL.APPLICATION_ORDER%TYPE,
         billing_type                         DEAL_HEAD.BILLING_TYPE%TYPE,
         deal_appl_timing                     DEAL_HEAD.DEAL_APPL_TIMING%TYPE,
         deal_class                           DEAL_DETAIL.DEAL_CLASS%TYPE,
         threshold_limit_type                 DEAL_HEAD.THRESHOLD_LIMIT_TYPE%TYPE,
         threshold_limit_uom                  DEAL_HEAD.THRESHOLD_LIMIT_UOM%TYPE,
         threshold_value_type                 DEAL_DETAIL.THRESHOLD_VALUE_TYPE%TYPE,
         qty_thresh_buy_item                  DEAL_DETAIL.QTY_THRESH_BUY_ITEM%TYPE,
         qty_thresh_buy_qty                   DEAL_DETAIL.QTY_THRESH_BUY_QTY%TYPE,
         qty_thresh_recur_ind                 DEAL_DETAIL.QTY_THRESH_RECUR_IND%TYPE,
         qty_thresh_buy_target                DEAL_DETAIL.QTY_THRESH_BUY_TARGET%TYPE,
         qty_thresh_get_item                  DEAL_DETAIL.QTY_THRESH_GET_ITEM%TYPE,
         qty_thresh_get_qty                   DEAL_DETAIL.QTY_THRESH_GET_QTY%TYPE,
         qty_thresh_free_item_unit_cost       DEAL_DETAIL.QTY_THRESH_FREE_ITEM_UNIT_COST%TYPE,
         rebate_ind                           DEAL_HEAD.REBATE_IND%TYPE,
         rebate_purch_sales_ind               DEAL_HEAD.REBATE_PURCH_SALES_IND%TYPE,
         create_date                          DEAL_HEAD.CREATE_DATETIME%TYPE,
         get_free_discount                    DEAL_DETAIL.GET_FREE_DISCOUNT%TYPE,
         qty_thresh_get_type                  DEAL_DETAIL.QTY_THRESH_GET_TYPE%TYPE,
         qty_thresh_get_value                 DEAL_DETAIL.QTY_THRESH_GET_VALUE%TYPE,
         txn_discount_ind                     DEAL_DETAIL.TRAN_DISCOUNT_IND%TYPE,
         po_exclusive_ind                     VARCHAR2(1)
        );
   TYPE deal_tbl IS TABLE of deal_rec INDEX BY BINARY_INTEGER;

   TYPE discount_build_rec IS RECORD
        (order_no                 ORDLOC_DISCOUNT_BUILD.ORDER_NO%TYPE,
         item                     ITEM_MASTER.ITEM%TYPE,
         pack_no                  ORDLOC_DISCOUNT_BUILD.PACK_NO%TYPE,
         origin_country_id        ORDLOC_DISCOUNT_BUILD.ORIGIN_COUNTRY_ID%TYPE,
         division                 ORDLOC_DISCOUNT_BUILD.DIVISION%TYPE,
         group_no                 ORDLOC_DISCOUNT_BUILD.GROUP_NO%TYPE,
         dept                     ORDLOC_DISCOUNT_BUILD.DEPT%TYPE,
         class                    ORDLOC_DISCOUNT_BUILD.CLASS%TYPE,
         subclass                 ORDLOC_DISCOUNT_BUILD.SUBCLASS%TYPE,
         item_parent              ORDLOC_DISCOUNT_BUILD.ITEM_PARENT%TYPE,
         item_grandparent         ORDLOC_DISCOUNT_BUILD.ITEM_GRANDPARENT%TYPE,
         diff_1                   ORDLOC_DISCOUNT_BUILD.DIFF_1%TYPE,
         diff_2                   ORDLOC_DISCOUNT_BUILD.DIFF_2%TYPE,
         diff_3                   ORDLOC_DISCOUNT_BUILD.DIFF_3%TYPE,
         diff_4                   ORDLOC_DISCOUNT_BUILD.DIFF_4%TYPE,
         chain                    ORDLOC_DISCOUNT_BUILD.CHAIN%TYPE,
         area                     ORDLOC_DISCOUNT_BUILD.AREA%TYPE,
         region                   ORDLOC_DISCOUNT_BUILD.REGION%TYPE,
         district                 ORDLOC_DISCOUNT_BUILD.DISTRICT%TYPE,
         location                 ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
         loc_type                 ORDLOC_DISCOUNT_BUILD.LOC_TYPE%TYPE,
         virtual_loc              ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
         qty_ordered              ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
         cost_zone_group_id       ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE,
         otb_calc_type            DEPS.OTB_CALC_TYPE%TYPE,
         unit_cost                ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
         unit_cost_init           ORDLOC.UNIT_COST_INIT%TYPE,
         orig_otb                 DISC_OTB_APPLY.ORIG_OTB%TYPE,
         new_otb                  DISC_OTB_APPLY.NEW_OTB%TYPE,
         exclusive_deal_applied   NUMBER(20,4),
         merch_level              DEAL_ITEMLOC.MERCH_LEVEL%TYPE,
         org_level                DEAL_ITEMLOC.ORG_LEVEL%TYPE,
         costing_loc              ITEM_LOC.COSTING_LOC%TYPE,
         costing_loc_type         ITEM_LOC.COSTING_LOC_TYPE%TYPE,
         franchise_ind            VARCHAR2(1)
        );
   TYPE discount_build_tbl IS TABLE OF discount_build_rec INDEX BY BINARY_INTEGER;

   TYPE item_deal_rec IS RECORD
        (deal_id             DEAL_DETAIL.DEAL_ID%TYPE,
         deal_detail_id      DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
         item                ITEM_MASTER.ITEM%TYPE,
         pack_no             ORDLOC_DISCOUNT_BUILD.PACK_NO%TYPE,
         location            ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
         loc_type            ORDLOC_DISCOUNT_BUILD.LOC_TYPE%TYPE,
         unit_cost           ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
         qty_ordered         ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
         origin_country_id   ORDLOC_DISCOUNT_BUILD.ORIGIN_COUNTRY_ID%TYPE,
         org_level           DEAL_ITEMLOC.ORG_LEVEL%TYPE,
         merch_level         DEAL_ITEMLOC.MERCH_LEVEL%TYPE
        );
   TYPE item_deal_tbl IS TABLE of item_deal_rec INDEX BY BINARY_INTEGER;

   TYPE disc_item_rec IS RECORD
        (item                   ITEM_MASTER.ITEM%TYPE,
         pack_no                ORDLOC_DISCOUNT_BUILD.PACK_NO%TYPE,
         unit_cost              ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
         qty_ordered            ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
         origin_country_id      ORDLOC_DISCOUNT_BUILD.ORIGIN_COUNTRY_ID%TYPE,
         location               ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
         loc_type               ORDLOC_DISCOUNT_BUILD.LOC_TYPE%TYPE,
         deal_id                DEAL_DETAIL.DEAL_ID%TYPE,
         deal_detail_id         DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
         deal_class             DEAL_DETAIL.DEAL_CLASS%TYPE,
         qty_thresh_buy_item    DEAL_DETAIL.QTY_THRESH_BUY_ITEM%TYPE,
         qty_thresh_buy_qty     DEAL_DETAIL.QTY_THRESH_BUY_QTY%TYPE,
         qty_thresh_get_item    DEAL_DETAIL.QTY_THRESH_GET_ITEM%TYPE,
         qty_thresh_get_qty     DEAL_DETAIL.QTY_THRESH_GET_QTY%TYPE,
         free_item_unit_cost    DEAL_DETAIL.QTY_THRESH_FREE_ITEM_UNIT_COST%TYPE,
         qty_thresh_recur_ind   DEAL_DETAIL.QTY_THRESH_RECUR_IND%TYPE,
         fixed_amt_flag         NUMBER(20,4),
         get_free_discount      DEAL_DETAIL.GET_FREE_DISCOUNT%TYPE,
         qty_thresh_get_type    DEAL_DETAIL.QTY_THRESH_GET_TYPE%TYPE,
         qty_thresh_get_value   DEAL_DETAIL.QTY_THRESH_GET_VALUE%TYPE
        );
   TYPE disc_item_tbl IS TABLE of disc_item_rec INDEX BY BINARY_INTEGER;

   TYPE get_list_rec IS RECORD
        (deal_id                 DEAL_DETAIL.DEAL_ID%TYPE,
         deal_detail_id          DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
         invc_unit_cost          ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
         pror_unit_cost          ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
         deal_qty                ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
         loc_qty                 ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
         pror_ordloc_unit_cost   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE
        );
   TYPE get_list_tbl IS TABLE of get_list_rec INDEX BY BINARY_INTEGER;

   TYPE get_list_array_rec IS RECORD
        (item                   ITEM_MASTER.ITEM%TYPE,
         location               ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
         used_as_buy_item_ind   NUMBER,
         get_list_vals          get_list_tbl
        );
   TYPE get_list_array_tbl IS TABLE of get_list_array_rec INDEX BY BINARY_INTEGER;

   TYPE cut_off_node_rec IS RECORD
        (get_item   ITEM_MASTER.ITEM%TYPE,
         deal_id    DEAL_DETAIL.DEAL_ID%TYPE,
         paid_ind   VARCHAR2(1),
         rownum     NUMBER(20,4)
        );
   TYPE cut_off_node_tbl IS TABLE of cut_off_node_rec INDEX BY BINARY_INTEGER;

   TYPE cut_off_costs_rec IS RECORD
        (buy_item               ITEM_MASTER.ITEM%TYPE,
         location               ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
         total_cut_off_cost     ORDLOC.UNIT_COST%TYPE,
         cut_off_nodes          cut_off_node_tbl
        );
   TYPE cut_off_costs_tbl IS TABLE of cut_off_costs_rec INDEX BY BINARY_INTEGER;

LP_num_cost_decimals        CURRENCIES.currency_cost_dec%TYPE;
LP_early_late_annual_prom   NUMBER(20,4) :=0;
LP_early_late_prom_annual   NUMBER(20,4) :=0;
LP_late_early_annual_prom   NUMBER(20,4) :=0;
LP_late_early_prom_annual   NUMBER(20,4) :=0;
LP_buyer_pack_detected      NUMBER(1)    :=0;
LP_exchange_rate            ORDHEAD.EXCHANGE_RATE%TYPE;
LP_order_no                 ORDHEAD.ORDER_NO%TYPE;
LP_order_supplier           ORDHEAD.SUPPLIER%TYPE;
LP_order_not_before_date    ORDHEAD.NOT_BEFORE_DATE%TYPE;
LP_order_appr_ind           DEAL_CALC_QUEUE.ORDER_APPR_IND%TYPE;
LP_order_status             ORDHEAD.STATUS%TYPE;
LP_order_currency_code      ORDHEAD.CURRENCY_CODE%TYPE;
LP_order_exchange_rate      ORDHEAD.EXCHANGE_RATE%TYPE;
LP_supp_currency_code       ORDHEAD.CURRENCY_CODE%TYPE;
LP_last_calc_date           ORDLOC_DISCOUNT.LAST_CALC_DATE%TYPE;
LP_deal_type_priority       SYSTEM_OPTIONS.DEAL_TYPE_PRIORITY%TYPE;
LP_deal_age_priority        SYSTEM_OPTIONS.DEAL_AGE_PRIORITY%TYPE;
LP_elc_ind                  SYSTEM_OPTIONS.ELC_IND%TYPE;
LP_currency_code            SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;
LP_vdate                    PERIOD.VDATE%TYPE := GET_VDATE;
LP_deal_ct                  NUMBER   :=0;
LP_deal_size                NUMBER   :=0;
LP_disct_bld_ct             NUMBER   :=0;
LP_org_level_given_flag     NUMBER(1)   :=0;
LP_upper_limit              DEAL_THRESHOLD.UPPER_LIMIT%TYPE := 0;
LP_lower_limit              DEAL_THRESHOLD.LOWER_LIMIT%TYPE := 0;
LP_deal_threshold_value     ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE;
LP_could_not_find_item      NUMBER(1)   := -3;
LP_no_deal_applied          NUMBER(1)   := 1;
LP_no_deal_applied_ind      VARCHAR2(1) := 'N';
LP_zero_difference          NUMBER(2,6) := 0.00001;
LP_not_paid                 NUMBER(1)   := -2;
---------------------------------------------------------------------------------------------------------------
-- Function: EXTERNAL_SHARE_APPLY_DEALS
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION EXTERNAL_SHARE_APPLY_DEALS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_order_no              IN       ORDLOC.ORDER_NO%TYPE,
                                    I_order_appr_ind        IN       DEAL_CALC_QUEUE.ORDER_APPR_IND%TYPE,
                                    I_recalc_all_ind        IN       DEAL_CALC_QUEUE.RECALC_ALL_IND%TYPE,
                                    I_override_manual_ind   IN       DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: EXTERNAL_PROCESS
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION EXTERNAL_PROCESS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no              IN       ORDLOC.ORDER_NO%TYPE,
                          I_order_appr_ind        IN       DEAL_CALC_QUEUE.ORDER_APPR_IND%TYPE,
                          I_recalc_all_ind        IN       DEAL_CALC_QUEUE.RECALC_ALL_IND%TYPE,
                          I_override_manual_ind   IN       DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: LIB_INIT
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION LIB_INIT(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: CHECK_LOCK_AND_VALIDATE
-- Purpose : leave the orders to be unprocessed which is locked by an online user.
---------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LOCK_AND_VALIDATE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_online_ind        IN       NUMBER,
                                 I_order_rec         IN OUT   ORDER_TBL,
                                 I_order_rec_valid   IN OUT   ORDER_TBL,
                                 I_rec_to_process    IN OUT   NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: RESET_ORDER
-- Purpose : remove any existing ordloc_discount records and reset the unit cost
--           on ordloc to their original values.
---------------------------------------------------------------------------------------------------------------
FUNCTION RESET_ORDER(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_item_cost_tbl     IN OUT   ITEM_COST_TBL,
                     I_order_rec_valid   IN OUT   ORDER_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_SUPPLIER_COST
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_SUPPLIER_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_do_update             IN       NUMBER,
                              I_item_cost_tbl         IN OUT   ITEM_COST_TBL,
                              I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
                              I_supplier              IN       ORDHEAD.SUPPLIER%TYPE,
                              I_override_manual_ind   IN       DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_DEALS
-- Purpose : find all deals that are active for the order
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEALS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_deal_tbl              IN OUT   DEAL_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: ORG_LEVEL_GIVEN
-- Purpose : find if the deal contains any organizational hierarchy infomation
---------------------------------------------------------------------------------------------------------------
FUNCTION ORG_LEVEL_GIVEN(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_deal_id                IN       DEAL_DETAIL.DEAL_ID%TYPE,
                         I_deal_detail_id         IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                         O_org_level_given_flag   IN OUT   NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: MAKE_ORDLOC_BUILD
-- Purpose : create records on the ordloc_discount_build table for each
--           item/location on the order.
---------------------------------------------------------------------------------------------------------------
FUNCTION MAKE_ORDLOC_BUILD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_override_manual_ind   IN       DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE,
                           I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_ORG_HIER
-- Purpose : Check if any deals have some organization hierarchy info,
--           go to find the org hier info for each item/location.
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_ORG_HIER(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_COSTING_LOC
-- Purpose : get the costing location info for each item/location.
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_COSTING_LOC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_discount_build_tbl   IN OUT   DISCOUNT_BUILD_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_OTB_COST
-- Purpose : get original order costs.
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_OTB_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_override_manual_ind   IN       DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE,
                      I_import_country_id     IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                      I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL,
                      I_otb_flag              IN OUT   NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: FIND_ITEMS_FOR_DEAL
-- Purpose : For each of the deals found for the order, find all order items that
--           are affected, find the right threshold values, and insert into the
--           ordloc_discount table
---------------------------------------------------------------------------------------------------------------
FUNCTION FIND_ITEMS_FOR_DEAL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_deal_tbl              IN OUT   DEAL_TBL,
                             I_disc_item_tbl         IN OUT   DISC_ITEM_TBL,
                             O_disc_item_tbl         IN OUT   DISC_ITEM_TBL,--out
                             I_disc_item_tbl_cnt     IN OUT   NUMBER,
                             I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL,
                             I_txn_discount_exists   IN OUT   NUMBER,
                             I_seq_no                IN OUT  ORDLOC_DISCOUNT.SEQ_NO%TYPE)
RETURN BINARY_INTEGER;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_THRESHOLD_VALUE
-- Purpose : to find the discount value given the order cost
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_THRESHOLD_VALUE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_deal_thresh_val   IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                             I_deal_id           IN       DEAL_DETAIL.DEAL_ID%TYPE,
                             I_deal_detail_id    IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                             I_rebate_ind        IN       DEAL_HEAD.REBATE_IND%TYPE,
                             I_discount_value    IN OUT   DEAL_THRESHOLD.VALUE%TYPE)
RETURN BINARY_INTEGER;
---------------------------------------------------------------------------------------------------------------
-- Function: PROCESS_EXCLUSIVE_DEAL
-- Purpose : process exclusive deal
---------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_EXCLUSIVE_DEAL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                 IN       ORDLOC_DISCOUNT_BUILD.ITEM%TYPE,
                                I_location             IN       ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
                                I_discount_build_tbl   IN OUT   DISCOUNT_BUILD_TBL,
                                I_merch_level          IN OUT   DEAL_ITEMLOC.MERCH_LEVEL%TYPE,
                                I_org_level            IN OUT   DEAL_ITEMLOC.ORG_LEVEL%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_APPLICATION_ORDER
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_APPLICATION_ORDER(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item                IN       ORDLOC_DISCOUNT_BUILD.ITEM%TYPE,
                               I_location            IN       ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
                               I_application_order   IN OUT   ORDLOC_DISCOUNT.APPLICATION_ORDER%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_ITEM_INDEX
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_INDEX(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_discount_build_tbl   IN OUT   DISCOUNT_BUILD_TBL,
                        I_item                 IN       ORDLOC_DISCOUNT_BUILD.ITEM%TYPE,
                        I_location             IN       ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
                        I_item_index           IN OUT   NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: STORE_DISC_ITEM
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION STORE_DISC_ITEM(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item_deal_tbl         IN OUT   ITEM_DEAL_TBL,
                         I_item_deal_tbl_array   IN       NUMBER,
                         I_deal_tbl              IN OUT   DEAL_TBL,
                         I_deal_tbl_array        IN       NUMBER,
                         O_disc_item_tbl         IN OUT   DISC_ITEM_TBL,
                         I_disc_item_tbl_cnt     IN OUT   NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: CALCULATE_COST
-- Purpose : update the ordloc table for off_invoice deal
---------------------------------------------------------------------------------------------------------------
FUNCTION CALCULATE_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_disc_item_tbl         IN OUT   DISC_ITEM_TBL,
                        I_disc_item_tbl_cnt     IN OUT   NUMBER,
                        I_import_order_ind      IN       ORDHEAD.IMPORT_ORDER_IND%TYPE,
                        I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: CALC_DISCOUNT
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION CALC_DISCOUNT(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item                   IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                       I_pack_no                IN       ORDLOC_DISCOUNT.PACK_NO%TYPE,
                       I_location               IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                       I_discount_value         IN       ORDLOC_DISCOUNT.DISCOUNT_VALUE%TYPE,
                       I_discount_type          IN       ORDLOC_DISCOUNT.DISCOUNT_TYPE%TYPE,
                       I_deal_id                IN OUT   DEAL_DETAIL.DEAL_ID%TYPE,
                       I_deal_detail_id         IN OUT   DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                       I_paid_ind               IN OUT   VARCHAR2,
                       I_deal_class             IN       DEAL_DETAIL.DEAL_CLASS%TYPE,
                       I_unit_cost_init         IN       ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                       I_qty_ordered            IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                       I_total_qty_ordered      IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                       I_buy_item               IN       DEAL_DETAIL.QTY_THRESH_BUY_ITEM%TYPE,
                       I_buy_qty                IN       DEAL_DETAIL.QTY_THRESH_BUY_QTY%TYPE,
                       I_get_item               IN       DEAL_DETAIL.QTY_THRESH_GET_ITEM%TYPE,
                       I_get_qty                IN       DEAL_DETAIL.QTY_THRESH_GET_QTY%TYPE,
                       I_free_item_unit_cost    IN       DEAL_DETAIL.QTY_THRESH_FREE_ITEM_UNIT_COST%TYPE,
                       I_recur_ind              IN       DEAL_DETAIL.QTY_THRESH_RECUR_IND%TYPE,
                       I_disc_item_tbl          IN OUT   DISC_ITEM_TBL,
                       I_disc_item_tbl_cnt      IN OUT   NUMBER,
                       I_qty_thresh_get_type    IN       DEAL_DETAIL.QTY_THRESH_GET_TYPE%TYPE,
                       I_qty_thresh_get_value   IN       DEAL_DETAIL.QTY_THRESH_GET_VALUE%TYPE,
                       I_get_free_discount      IN       DEAL_DETAIL.GET_FREE_DISCOUNT%TYPE,
                       I_get_list_array_tbl     IN OUT   GET_LIST_ARRAY_TBL,
                       I_discount_build_tbl     IN OUT   DISCOUNT_BUILD_TBL,
                       I_cut_off_costs_tbl      IN OUT   CUT_OFF_COSTS_TBL,
                       I_no_deal_applied_ind    IN OUT   VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_PRORATED_CORE_INFO
-- Purpose : populate/get available qty and its cost with/for current item
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_PRORATED_CORE_INFO(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                   IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                I_location               IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                I_total_qty_ordered      IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                I_pror_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                I_invc_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                I_used_as_buy_item_ind   IN OUT   NUMBER,
                                I_prorated_deal_qty      IN OUT   ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                I_prorated_loc_qty       IN OUT   ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                I_deal_id                IN OUT   DEAL_DETAIL.DEAL_ID%TYPE,
                                I_deal_detail_id         IN OUT   DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                I_paid_ind               IN OUT   VARCHAR2,
                                I_disc_item_tbl          IN OUT   DISC_ITEM_TBL,
                                I_disc_item_tbl_cnt      IN OUT   NUMBER,
                                I_get_list_array_tbl     IN OUT   GET_LIST_ARRAY_TBL,
                                I_no_deal_applied_ind    IN OUT   VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_LIST_INDEX
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_LIST_INDEX(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_get_list_array_tbl        IN OUT   GET_LIST_ARRAY_TBL,
                        I_item                      IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                        I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                        I_which_list                IN OUT   NUMBER,
                        I_could_not_find_item_ind   IN OUT   VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: INSERT_NEW_PRORATED_INFO
-- Purpose : item we were were looking for has not yet been processed.  Add original order info.
---------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_NEW_PRORATED_INFO(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_item                      IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                  I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                  I_total_qty_ordered         IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                  I_deal_id                   IN OUT   DEAL_DETAIL.DEAL_ID%TYPE,
                                  I_deal_detail_id            IN OUT   DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                  I_paid_ind                  IN OUT   VARCHAR2,
                                  I_which_list                IN OUT   NUMBER,
                                  I_could_not_find_item_ind   IN OUT   VARCHAR2,
                                  I_disc_item_tbl             IN OUT   DISC_ITEM_TBL,
                                  I_disc_item_tbl_cnt         IN OUT   NUMBER,
                                  I_get_list_array_tbl        IN OUT   GET_LIST_ARRAY_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: STORE_NEW_DISCOUNT_DETAILS
-- Purpose : cumulate total qty_ordered including item and component item
---------------------------------------------------------------------------------------------------------------
FUNCTION STORE_NEW_DISCOUNT_DETAILS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item                   IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                    I_location               IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                    I_which_list             IN OUT   NUMBER,
                                    I_pror_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                    I_invc_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                    I_qty_ordered            IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                    I_total_qty_ordered      IN       ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                    I_deal_id                IN       DEAL_DETAIL.DEAL_ID%TYPE,
                                    I_deal_detail_id         IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                    I_paid_ind               IN       VARCHAR2,
                                    I_get_list_array_tbl     IN OUT   GET_LIST_ARRAY_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: INSERT_CUT_OFF_COST
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_CUT_OFF_COST(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cut_off_costs_tbl         IN OUT   CUT_OFF_COSTS_TBL,
                             I_buy_item                  IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                             I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                             I_total_cut_off_cost        IN       ORDLOC.UNIT_COST%TYPE,
                             I_could_not_find_item_ind   IN OUT   VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_CUT_OFF_COST_INDEX
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_CUT_OFF_COST_INDEX(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_cut_off_costs_tbl         IN OUT   CUT_OFF_COSTS_TBL,
                                I_buy_item                  IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                I_which_list                IN OUT   NUMBER,
                                I_could_not_find_item_ind   IN OUT   VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: CALCULATE_AVERAGE_UNIT_COST
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION CALCULATE_AVERAGE_UNIT_COST(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item                      IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                     I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                     I_get_list_array_tbl        IN OUT   GET_LIST_ARRAY_TBL,
                                     I_average_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: SET_PRORATED_CORE_INFO
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION SET_PRORATED_CORE_INFO(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                   IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                I_location               IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                I_pror_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                I_unit_discount          IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                I_invc_unit_cost         IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                I_prorated_deal_qty      IN OUT   ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                I_prorated_loc_qty       IN OUT   ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                                I_used_as_buy_item_ind   IN OUT   NUMBER,
                                I_paid_ind               IN OUT   VARCHAR2,
                                I_get_list_array_tbl     IN OUT   GET_LIST_ARRAY_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_ORDLOC_DISCOUNT
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC_DISCOUNT(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                    IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                I_pack_no                 IN       ORDLOC_DISCOUNT.PACK_NO%TYPE,
                                I_location                IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                I_deal_id                 IN       DEAL_DETAIL.DEAL_ID%TYPE,
                                I_deal_detail_id          IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                I_discount                IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                                I_discount_amt_per_unit   IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE)

RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: INS_BUY_GET_INTO_CUT_OFF_COST
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION INS_BUY_GET_INTO_CUT_OFF_COST(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_cut_off_costs_tbl         IN OUT   CUT_OFF_COSTS_TBL,
                                       I_buy_item                  IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                       I_get_item                  IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                       I_location                  IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                       I_deal_id                   IN       DEAL_DETAIL.DEAL_ID%TYPE,
                                       I_paid_ind                  IN       VARCHAR2,
                                       I_total_cut_off_cost        IN       ORDLOC.UNIT_COST%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_ITEM_UNIT_COST
-- Purpose : find get item's init unit cost
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_UNIT_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item                  IN       ORDLOC_DISCOUNT_BUILD.ITEM%TYPE,
                            I_location              IN       ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
                            I_unit_cost             IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                            I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL,
                            I_no_deal_applied_ind   IN OUT   VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_TOTAL_QTY
-- Purpose :  find total get qty for this deal (independent of loc), and this loc's qty
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_QTY(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item                    IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                       I_location                IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                       I_get_item_qty_ordered    IN OUT   ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                       I_total_get_qty_ordered   IN OUT   ORDLOC_DISCOUNT_BUILD.QTY_ORDERED%TYPE,
                       I_deal_id                 IN       DEAL_DETAIL.DEAL_ID%TYPE,
                       I_deal_detail_id          IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                       I_disc_item_tbl           IN OUT   DISC_ITEM_TBL,
                       I_disc_item_tbl_cnt       IN OUT   NUMBER,
                       I_no_deal_applied_ind     IN OUT   VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: APPLY_CUT_OFF_COSTS
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION APPLY_CUT_OFF_COSTS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_get_list_array_tbl     IN OUT   GET_LIST_ARRAY_TBL,
                             I_cut_off_costs_tbl      IN OUT   CUT_OFF_COSTS_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: ADD_TO_ORDLOC_DISCOUNT
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION ADD_TO_ORDLOC_DISCOUNT(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                    IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                I_pack_no                 IN       ORDLOC_DISCOUNT.PACK_NO%TYPE,
                                I_location                IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                I_deal_id                 IN       DEAL_DETAIL.DEAL_ID%TYPE,
                                I_deal_detail_id          IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                I_discount_amt_per_unit   IN       ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: CALCULATE_ORDLOC_UNIT_COST
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION CALCULATE_ORDLOC_UNIT_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item                 IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                                    I_location             IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                                    I_get_list_array_tbl   IN OUT   GET_LIST_ARRAY_TBL,
                                    I_unit_cost            IN OUT   ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_ORDLOC
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item            IN       ORDLOC_DISCOUNT_BUILD.ITEM%TYPE,
                       I_location        IN       ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
                       I_loc_type        IN       ORDLOC_DISCOUNT_BUILD.LOC_TYPE%TYPE,
                       I_unit_cost       IN       ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: APPLY_ELC
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION APPLY_ELC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_item                IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                   I_pack_no             IN       ORDLOC_DISCOUNT.PACK_NO%TYPE,
                   I_location            IN       ORDLOC_DISCOUNT.LOCATION%TYPE,
                   I_import_order_ind    IN       ORDHEAD.IMPORT_ORDER_IND%TYPE,
                   I_origin_country_id   IN       ORDLOC_DISCOUNT_BUILD.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: PROCESS_TXN_DISCOUNT
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TXN_DISCOUNT(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_deal_tbl              IN OUT   DEAL_TBL,
                              I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL,
                              I_seq_no                IN OUT   ORDLOC_DISCOUNT.SEQ_NO%TYPE)
RETURN BINARY_INTEGER;
---------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_OTB
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_OTB(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_discount_build_tbl    IN OUT   DISCOUNT_BUILD_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: INSERT_REV_ORDERS
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_REV_ORDERS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item_cost            IN OUT   ITEM_COST_TBL,
                           I_discount_build_tbl   IN OUT   DISCOUNT_BUILD_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: UPDATE_LC
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_LC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_item              IN       ORDLOC_DISCOUNT_BUILD.ITEM%TYPE,
                   I_location          IN       ORDLOC_DISCOUNT_BUILD.LOCATION%TYPE,
                   I_average_cost      IN       ORDLOC_DISCOUNT_BUILD.UNIT_COST%TYPE,
                   I_lc_cost_changed   IN       NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: INSERT_L10N_DOC_DETAILS_GTT
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_L10N_DOC_DETAILS_GTT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item_cost       IN OUT   ITEM_COST_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: DELETE_ORDLOC_DISC_BLD
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORDLOC_DISC_BLD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: DELETE_ORDER_DEAL_BUILD
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORDER_DEAL_BUILD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: CLEANUP_DISCOUNT_TABLES
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION CLEANUP_DISCOUNT_TABLES(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_order_rec_valid   IN OUT   ORDER_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
-- Function: DELETE_DCQ
-- Purpose :
---------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DCQ(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_order_rec_valid   IN OUT   ORDER_TBL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
END DEAL_ORD_LIB_SQL;
/