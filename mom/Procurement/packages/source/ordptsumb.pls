
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ORD_PARENT_SUM_SQL AS

LP_order_no ordloc.order_no%TYPE;
LP_supplier item_supplier.supplier%TYPE;
---------------------------------------------------------
FUNCTION PROCESS_ITEM(O_error_message     IN OUT VARCHAR2,
                      I_item              IN     ITEM_MASTER.ITEM%TYPE,
                      I_location          IN     ORDLOC.LOCATION%TYPE,
                      I_qty_ordered       IN     ORDLOC.QTY_ORDERED%TYPE,
                      I_qty_received      IN     ORDLOC.QTY_RECEIVED%TYPE,
                      I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                      I_unit_retail       IN     ORDLOC.UNIT_RETAIL%TYPE,
                      I_unit_cost         IN     ORDLOC.UNIT_COST%TYPE,
                      I_origin_country_id IN     ORDSKU.ORIGIN_COUNTRY_ID%TYPE)
                      RETURN BOOLEAN IS

   L_item_parent       ITEM_MASTER.ITEM_PARENT%TYPE;
   L_dummy             VARCHAR2(1);

   cursor C_CHECK_ORDITEM_SUM_TEMP is
      select 'x'
        from gtt_orditem_sum
       where item = I_item
         and order_no = LP_order_no
         and location = I_location;

BEGIN          
   open C_CHECK_ORDITEM_SUM_TEMP;
   fetch C_CHECK_ORDITEM_SUM_TEMP into L_dummy;
   if C_CHECK_ORDITEM_SUM_TEMP%FOUND then
      update gtt_orditem_sum
         set qty_ordered = qty_ordered + I_qty_ordered,
             qty_received = qty_received + I_qty_received
       where item = I_item
         and order_no = LP_order_no
         and location = I_location;
         return TRUE;
   end if;
   close C_CHECK_ORDITEM_SUM_TEMP;
   insert into gtt_orditem_sum (order_no,
                                item,
                                item_parent,
                                item_grandparent,
                                diff_1,
                                diff1_seq,
                                diff_2,
                                diff2_seq,
                                diff_3,
                                diff3_seq,
                                diff_4,
                                diff4_seq,
                                location,
                                loc_type,
                                qty_ordered,
                                unit_retail,
                                supplier_cost,
                                qty_received)
       select LP_order_no,
              I_item,
              im1.item_parent,
              im1.item_grandparent,
              im1.diff_1,
              d1.display_seq,
              im1.diff_2,
              d2.display_seq,
              im1.diff_3,
              d3.display_seq,
              im1.diff_4,
              d4.display_seq,
              I_location,
              I_loc_type,
              I_qty_ordered,
              I_unit_retail,
              I_unit_cost,
              I_qty_received
         from item_master im1,
              item_master im2,
              diff_group_detail d1,
              diff_group_detail d2,
              diff_group_detail d3,
              diff_group_detail d4
        where I_item              = im1.item
          and im1.item_parent     = im2.item
          and im2.diff_1          = d1.diff_group_id (+)
          and (im1.diff_1         = d1.diff_id or
               (im1.diff_1 is not NULL and d1.diff_id is NULL))
          and im2.diff_2          = d2.diff_group_id (+)
          and (im1.diff_2         = d2.diff_id or
               d2.diff_id is NULL)
          and im2.diff_3          = d3.diff_group_id (+)
          and (im1.diff_3         = d3.diff_id or
               d3.diff_id is NULL)
          and im2.diff_4          = d4.diff_group_id (+)
          and (im1.diff_4         = d4.diff_id or
               d4.diff_id is NULL);

   return TRUE; 
        
EXCEPTION
   when OTHERS then
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM, 
                                         'ORD_PARENT_SUM_SQL.PROCESS_ITEM', 
                                         to_char(SQLCODE));
   return FALSE;

END PROCESS_ITEM;
-----------------------------------------------------------------------
FUNCTION PROCESS_PACK(O_error_message     IN OUT VARCHAR2,
                      I_pack_no           IN     ORDLOC.ITEM%TYPE,
                      I_qty_ordered       IN     ORDLOC.QTY_ORDERED%TYPE,
                      I_qty_received      IN     ORDLOC.QTY_RECEIVED%TYPE,
                      I_location          IN     ORDLOC.LOCATION%TYPE,
                      I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                      I_origin_country_id IN     ORDSKU.ORIGIN_COUNTRY_ID%TYPE)
                      RETURN BOOLEAN IS
   
   cursor C_BREAK_PACK is
      select p.item,
             p.qty,
             il.unit_retail,
             ils.unit_cost
        from v_packsku_qty p, 
             item_loc il,
             item_loc_soh ils,
             item_master im
       where p.pack_no     = I_pack_no
         and p.item        = il.item
         and il.item       = ils.item
         and il.loc        = ils.loc
         and ils.item      = im.item
         and im.item_level > 1
         and il.loc        = I_location;

BEGIN
   FOR recs in C_BREAK_PACK LOOP      
      if PROCESS_ITEM(O_error_message,
                      recs.item,
                      I_location,
                      recs.qty*I_qty_ordered,
                      recs.qty*I_qty_received,
                      I_loc_type,
                      recs.unit_retail,
                      recs.unit_cost,
                      I_origin_country_id) = FALSE then
         return FALSE;      
      end if;

   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
	   	                            'ORD_PARENT_SUM_SQL.PROCESS_PACK', 
			                     to_char(SQLCODE));
      return FALSE;
END PROCESS_PACK;
------------------------------------------------------------------------

FUNCTION POP_GTT_ORDITEM_SUM(O_error_message IN OUT VARCHAR2,
                             I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
                             I_supplier      IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
                             RETURN BOOLEAN IS

   cursor C_GET_ORDER_INFO is 
      select o.item,
             o.location,
             o.loc_type,
             o.qty_ordered,
             nvl(o.qty_received, 0) qty_received,
             o.unit_retail,
             o.unit_cost,
             k.origin_country_id,
             im.pack_ind
        from ordloc o, ordsku k, item_master im
       where o.order_no = LP_order_no
         and o.order_no = k.order_no
         and o.item = k.item
         and o.item = im.item
         and (im.pack_ind = 'Y' or im.item_level > 1);
     
   BEGIN

   LP_order_no := I_order_no;
   LP_supplier := I_supplier;

   FOR rec in C_GET_ORDER_INFO LOOP
      
      if rec.pack_ind = 'Y' then 
         if PROCESS_PACK(O_error_message,
                         rec.item,
                         rec.qty_ordered,
                         rec.qty_received,
                         rec.location,
                         rec.loc_type,
                         rec.origin_country_id) = FALSE then
             return FALSE;
          end if;
      else
         if PROCESS_ITEM(O_error_message,
                         rec.item,
                         rec.location,
                         rec.qty_ordered,
                         rec.qty_received,
                         rec.loc_type,
                         rec.unit_retail,
                         rec.unit_cost,
                         rec.origin_country_id) = FALSE then
            return FALSE;
         end if;
      end if;
  END LOOP;
  return TRUE;
EXCEPTION
   when OTHERS then
   O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                          SQLERRM, 
                                         'ORDFASH_SQL.POP_GTT_ORDITEM_SUM', 
                                          to_char(SQLCODE));
   return FALSE;

END POP_GTT_ORDITEM_SUM;
---------------------------------------------------------------------

            
END ORD_PARENT_SUM_SQL;
/
