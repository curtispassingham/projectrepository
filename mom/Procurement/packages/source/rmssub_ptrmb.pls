
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_PAYTERM AS

------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE
------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2);
------------------------------------------------------------------------------------------------------
   -- PUBLIC PROCEDURE
------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code      OUT  VARCHAR2,
                  O_error_message    OUT  VARCHAR2,
                  I_message          IN   RIB_OBJECT,
                  I_message_type     IN	VARCHAR2)
IS

   PROGRAM_ERROR    EXCEPTION;

   L_message               "RIB_PayTermDesc_REC";
   L_message_type          VARCHAR2(15) := LOWER(I_message_type);
   L_program               VARCHAR2(50) := 'RMSSUB_PAYTERM.CONSUME';
   L_dml_rec               TERMS_SQL.PAYTERM_REC;

BEGIN

   O_STATUS_CODE := API_CODES.SUCCESS;

   -- perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   if L_message_type in  (RMSSUB_PAYTERM.HDR_ADD, RMSSUB_PAYTERM.HDR_UPD,
                          RMSSUB_PAYTERM.DTL_ADD, RMSSUB_PAYTERM.DTL_UPD) then

      L_message := treat(I_MESSAGE AS "RIB_PayTermDesc_REC");

      if L_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', 'No message', NULL, NULL);
         raise PROGRAM_ERROR;
         O_status_code := 'E';
      end if;


      -- Validate Message Contents
      if RMSSUB_PAYTERM_VALIDATE.CHECK_MESSAGE(O_error_message,
                                              L_dml_rec,
                                              L_message,
                                              L_message_type) = FALSE then
         raise PROGRAM_ERROR;
         O_status_code := 'E';
      end if;
      ---

      -- INSERT/UPDATE table
      if RMSSUB_PAYTERM_SQL.PERSIST(O_error_message,
                                    L_dml_rec,
                                    L_message_type)    = FALSE then
         raise PROGRAM_ERROR;
      end if;

   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE_TYPE', L_message_type, NULL, NULL);
      raise PROGRAM_ERROR;
      O_status_code := 'E';
   end if;

   O_status_code := 'S';

EXCEPTION
   when PROGRAM_ERROR then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);
   when OTHERS then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);

END CONSUME;
------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2) IS

   L_program VARCHAR2(50) := 'RMSSUB_PAYTERM.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             O_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
END HANDLE_ERRORS;
------------------------------------------------------------------------------------------------------
END RMSSUB_PAYTERM;
/
