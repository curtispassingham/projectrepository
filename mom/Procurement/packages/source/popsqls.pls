
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE POP_TERMS_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
--Function Name:  GET_NEXT_POP_DEF_SEQ_NO
--Purpose:        Gets the next available POP_DEF_SEQ_NO from the POP_DEF_SEQ_NO_SEQUENCE
-------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_POP_DEF_SEQ_NO(O_error_message          IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_pop_def_seq_no         IN OUT     POP_TERMS_DEF.POP_DEF_SEQ_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:  GET_NEXT_POP_FULFILL_SEQ_NO
--Purpose:        Gets the next available POP_DEF_SEQ_NO from the POP_DEF_SEQ_NO_SEQUENCE
-------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_POP_FULFILL_SEQ_NO(O_error_message      IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_pop_fulfill_seq_no IN OUT    POP_TERMS_FULFILLMENT.POP_FULFILL_SEQ_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:  CHECK_POP_FULFILL_EXIST
--Purpose:        Checks if fulfillments exist for a given term definition.
-------------------------------------------------------------------------------------------
FUNCTION CHECK_POP_FULFILL_EXIST    (O_error_message      IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_fulfills_exist     IN OUT    BOOLEAN,
                                     I_pop_def_seq_no     IN        POP_TERMS_DEF.POP_DEF_SEQ_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:  DELETE_FULFILLMENTS
--Purpose:        Deletes fulfillments for a given term definition.
-------------------------------------------------------------------------------------------
FUNCTION DELETE_FULFILLMENTS        (O_error_message      IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_pop_def_seq_no     IN        POP_TERMS_DEF.POP_DEF_SEQ_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
END POP_TERMS_SQL;
/
