CREATE OR REPLACE PACKAGE DEAL_FINANCE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------------------------------------------
-- Function Name:  VALIDATE_INVOICE_DATE  
-- Purpose      :  This function is called by the form DEALMAIN.FMB  
--                 It determines if the user has entered a valid invoice date 
-----------------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_INVOICE_DATE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_invoice_date    IN     DATE,
                               I_deal_id         IN     DEAL_HEAD.DEAL_ID%TYPE,
                               I_deal_start_date IN     DATE,
                               I_deal_end_date   IN     DATE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------------------------------
-- Function Name:  CALC_NEXT_INVOICE_DATE  
-- Purpose      :  This function is called by batch 
--                 It calculates the next invoice date using using the bill back period  
-----------------------------------------------------------------------------------------------------------------------------
FUNCTION CALC_NEXT_INVOICE_DATE(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_next_invoice_date    IN OUT DATE,
                                I_deal_start_date      IN     DATE,
                                I_deal_end_date        IN     DATE,
                                I_billing_period       IN     DEAL_HEAD.BILL_BACK_PERIOD%TYPE,
                                I_current_invoice_date IN     DATE,
                                I_last_rep_inv_date    IN     DATE DEFAULT NULL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------------------------------
-- Function Name:  CALC_INITIAL_INVOICE_DATE  
-- Purpose      :  This function is called by DEALMAIN.FMB  
--                 It calculates the initial invoice date when the deal is created 
-----------------------------------------------------------------------------------------------------------------------------   
FUNCTION CALC_INITIAL_INVOICE_DATE(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_invoice_date        IN OUT DATE,
                                   I_deal_start_date     IN     DATE,
                                   I_deal_end_date       IN     DATE,
                                   I_billing_period      IN     DEAL_HEAD.BILL_BACK_PERIOD%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------------------------------
-- Function Name:  CALC_NEXT_REPORT_DATE  
-- Purpose      :  This function is called by the deal_actual_forecast_sql.create_template 
--                 function and will calculate the next reporting period date using
--                 the billing period  
-----------------------------------------------------------------------------------------------------------------------------
FUNCTION CALC_NEXT_REPORT_DATE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_next_report_date  IN OUT DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE,
                               I_deal_end_date     IN     DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE,
                               I_billing_period    IN     DEAL_HEAD.BILL_BACK_PERIOD%TYPE,
                               I_last_report_date  IN     DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------------------------------
FUNCTION ADD_MONTHS_454(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_next_report_date   IN OUT   DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE,
                        I_no_of_months       IN       NUMBER,
                        I_last_report_date   IN       DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------------------------------
END DEAL_FINANCE_SQL;
/
