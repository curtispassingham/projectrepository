CREATE OR REPLACE PACKAGE BODY DEAL_INCOME_SQL AS
---------------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
---------------------------------------------------------------------------------------------------------------
-- Function: GET_DEAL_HEAD_ROW
-- Purpose : Fetch the deal header level records.
----------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_HEAD_ROW(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_deal_head            IN OUT   DEAL_HEAD_TBL,
                           I_deal_id              IN       DEAL_HEAD.DEAL_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(100) := 'DEAL_INCOME_SQL.GET_DEAL_HEAD_ROW';
   
   cursor C_DEAL_HEAD is
      select rowid,
             currency_code,
             billing_type,
             deal_appl_timing,
             threshold_limit_type,
             threshold_limit_uom,
             rebate_ind,
             rebate_calc_type,
             growth_rebate_ind,
             rebate_purch_sales_ind,
             bill_back_method,
             deal_income_calculation,
             stock_ledger_ind,
             include_vat_ind,
             status,
             NVL(growth_rate_to_date,0),
             NVL(turnover_to_date,0),
             NVL(actual_monies_earned_to_date,0)
        from deal_head
       where deal_id = I_deal_id;
BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_DEAL_HEAD;
   fetch C_DEAL_HEAD BULK COLLECT into O_deal_head;
   close C_DEAL_HEAD;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END GET_DEAL_HEAD_ROW;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_DEAL_DETAIL_ROW
-- Purpose : Fetch the deal detail level records.
----------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_DETAIL_ROW(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_deal_detail          IN OUT   DEAL_DETAIL_TBL,
                             I_deal_id              IN       DEAL_HEAD.DEAL_ID%TYPE,
                             I_deal_detail_id       IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(100) := 'DEAL_INCOME_SQL.GET_DEAL_DETAIL_ROW';

   cursor C_DEAL_DETAIL is
       select rowid,
             calc_to_zero_ind,
             threshold_value_type,
             NVL(total_forecast_units,0),
             NVL(total_forecast_revenue,0),
             NVL(total_budget_turnover,0),
             NVL(total_actual_forecast_turnover,0),
             NVL(total_baseline_growth_budget,0),
             NVL(total_baseline_growth_act_for,0),
             NVL(growth_rate_to_date,0)
        from deal_detail
       where deal_id        = I_deal_id
         and deal_detail_id = I_deal_detail_id;
BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_DEAL_DETAIL;
   fetch C_DEAL_DETAIL BULK COLLECT into O_deal_detail;
   close C_DEAL_DETAIL;
   
   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END GET_DEAL_DETAIL_ROW;
----------------------------------------------------------------------------------------------------------------
-- Function: GET_DEAL_DETAILS
-- Purpose : Fetch the deal details at header and detail level.
----------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_DETAILS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_deal_head            IN OUT   DEAL_HEAD_TBL,
                          O_deal_detail          IN OUT   DEAL_DETAIL_TBL,
                          I_deal_id              IN       DEAL_HEAD.DEAL_ID%TYPE,
                          I_deal_detail_id       IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(100) := 'DEAL_INCOME_SQL.GET_DEAL_DETAILS';

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if DEAL_INCOME_SQL.GET_DEAL_HEAD_ROW(O_error_message,
                                        O_deal_head,
                                        I_deal_id) = FALSE then
      return FALSE;
   end if;

   if DEAL_INCOME_SQL.GET_DEAL_DETAIL_ROW(O_error_message,
                                          O_deal_detail,
                                          I_deal_id,
                                          I_deal_detail_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
   return FALSE;
END GET_DEAL_DETAILS;
----------------------------------------------------------------------------------------------------------------
-- Function: GET_DEAL_ACTUAL_FORECAST_TOTAL
-- Purpose : This function will be used to retrieve totals from the DEAL_ACTUAL_FORECAST table,
--           depending on the parameters passed in it can be used to sum a combination of actuals,
--           forecasts or both for all periods or a specific period for a single deal component
--           or all deal components.
----------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_daf_totals           IN OUT   DAF_TOTALS_TBL,
                                        I_deal_id              IN       DEAL_HEAD.DEAL_ID%TYPE,
                                        I_deal_detail_id       IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                        I_forecast_ind         IN       DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_IND%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(100) := 'DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL';

   cursor c_daf_totals is
      select nvl(sum(baseline_turnover), 0),
             nvl(sum(budget_turnover), 0),
             nvl(sum(budget_income), 0),
             nvl(sum(actual_forecast_turnover), 0),
             nvl(sum(actual_forecast_income), 0),
             nvl(sum(actual_forecast_trend_turnover), 0),
             nvl(sum(actual_forecast_trend_income), 0),
             nvl(sum(actual_income),0)
        from deal_actuals_forecast daf
       where deal_id             = I_deal_id
         and deal_detail_id      = decode(I_deal_detail_id, -1, deal_detail_id, I_deal_detail_id)
         and actual_forecast_ind = nvl(I_forecast_ind, actual_forecast_ind);
BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open c_daf_totals;
   fetch c_daf_totals BULK COLLECT into O_daf_totals;
   close c_daf_totals;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
   return FALSE;
END GET_DEAL_ACTUAL_FORECAST_TOTAL;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_DEAL_ACT_FCT_TTL_RPT_DT
-- Purpose : This function will be used to retrieve totals from the DEAL_ACTUAL_FORECAST table,
--           for all periods less than the passed reporting date for a single deal component.
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_ACT_FCT_TTL_RPT_DT(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_daf_totals           IN OUT   DAF_TOTALS_TBL,
                                     I_deal_id              IN       DEAL_HEAD.DEAL_ID%TYPE,
                                     I_deal_detail_id       IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                     I_reporting_date       IN       DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(100) := 'DEAL_INCOME_SQL.GET_DEAL_ACT_FCT_TTL_RPT_DT';
   
   cursor C_DAF_TOTALS_RPT_DATE is
      select nvl(sum(baseline_turnover), 0),
             nvl(sum(budget_turnover), 0),
             nvl(sum(budget_income), 0),
             nvl(sum(actual_forecast_turnover), 0),
             nvl(sum(actual_forecast_income), 0),
             nvl(sum(actual_forecast_trend_turnover), 0),
             nvl(sum(actual_forecast_trend_income), 0),
             nvl(sum(actual_income),0)
        from deal_actuals_forecast daf
       where deal_id = I_deal_id
         and deal_detail_id = I_deal_detail_id
         and reporting_date < I_reporting_date;
BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_reporting_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_reporting_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_DAF_TOTALS_RPT_DATE;
   fetch C_DAF_TOTALS_RPT_DATE BULK COLLECT into O_daf_totals;
   close C_DAF_TOTALS_RPT_DATE;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
   return FALSE;
END GET_DEAL_ACT_FCT_TTL_RPT_DT;
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_REPORTING_PERIOD_COUNT(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_reporting_periods    IN OUT   DEAL_HEAD.DEAL_ID%TYPE,
                                    I_deal_id              IN       DEAL_HEAD.DEAL_ID%TYPE,
                                    I_deal_detail_id       IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(100) := 'DEAL_INCOME_SQL.GET_REPORTING_PERIOD_COUNT';

   cursor C_REPORTING_PERIOD_COUNT is
      SELECT COUNT(*)
        FROM deal_actuals_forecast
       WHERE deal_id         = I_deal_id
         AND deal_detail_id  = I_deal_detail_id
         AND actual_forecast_ind = 'F';
BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   open C_REPORTING_PERIOD_COUNT;
   fetch C_REPORTING_PERIOD_COUNT into O_reporting_periods;
   close C_REPORTING_PERIOD_COUNT;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
   return FALSE;
END GET_REPORTING_PERIOD_COUNT;
---------------------------------------------------------------------------------------------------------------
-- Function: GET_DEAL_ACTUAL_ITEM_LOC_TOTAL
-- Purpose : This function will be used to retrieve totals till date from the DEAL_ACTUAL_ITEM_LOC table,
--           for each deal/item/location.
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_ACTUAL_ITEM_LOC_TOTAL(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_actual_income_itemloc_td   IN OUT   DEAL_ACTUALS_ITEM_LOC.ACTUAL_INCOME%TYPE,
                                        I_deal_id                    IN       DEAL_ACTUALS_ITEM_LOC.DEAL_ID%TYPE,
                                        I_deal_detail_id             IN       DEAL_ACTUALS_ITEM_LOC.DEAL_DETAIL_ID%TYPE,
                                        I_item                       IN       DEAL_ACTUALS_ITEM_LOC.ITEM%TYPE,
                                        I_loc_type                   IN       DEAL_ACTUALS_ITEM_LOC.LOC_TYPE%TYPE,
                                        I_location                   IN       DEAL_ACTUALS_ITEM_LOC.LOCATION%TYPE,
                                        I_threshold_limit_type       IN       DEAL_HEAD.THRESHOLD_LIMIT_TYPE%TYPE,
                                        I_reporting_date             IN       DEAL_ACTUALS_ITEM_LOC.REPORTING_DATE%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(100) := 'DEAL_INCOME_SQL.GET_DEAL_ACTUAL_ITEM_LOC_TOTAL';

   cursor C_DAIL_TOTALS is
      SELECT NVL(SUM(CASE WHEN dh.currency_code <> vloc.currency_code THEN
                        CURRENCY_SQL.CONVERT_VALUE('C',
                                                   dh.currency_code,
                                                   vloc.currency_code,
                                                   dail.actual_income)
                     ELSE dail.actual_income
                     END
                    ), 0)
        FROM deal_actuals_item_loc dail,
             deal_head dh,
             (SELECT st.store loc,
                     st.currency_code,
                     'S' loc_type
                FROM store st
               UNION ALL
              SELECT wh.wh loc,
                     wh.currency_code,
                     'W' loc_type
                FROM wh
               WHERE stockholding_ind = 'Y'
                 AND finisher_ind = 'N') vloc
       WHERE dail.deal_id = I_deal_id
         AND dail.deal_id = dh.deal_id
         AND dail.location  = vloc.loc
         AND dail.loc_type  = vloc.loc_type
         AND dail.deal_detail_id      = DECODE(I_deal_detail_id, -1, dail.deal_detail_id, I_deal_detail_id)
         AND dail.item                = I_item
         AND dail.location            = I_location
         AND dail.loc_type            = I_loc_type
         AND dail.reporting_date      < TO_DATE(I_reporting_date,'YYYYMMDD');
BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_reporting_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_reporting_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_DAIL_TOTALS;
   fetch C_DAIL_TOTALS into O_actual_income_itemloc_td;
   close C_DAIL_TOTALS;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END GET_DEAL_ACTUAL_ITEM_LOC_TOTAL;
---------------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTIONS
---------------------------------------------------------------------------------------------------------------
FUNCTION EXTERNAL_APPLY_DEAL_PERF(O_error_message                     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_deal_id                           IN       VARCHAR2,
                                  I_deal_detail_id                    IN       VARCHAR2,
                                  IO_amt_per_unit                     IN OUT   VARCHAR2,
                                  I_old_period_turnover               IN       VARCHAR2,
                                  I_new_period_turnover               IN       VARCHAR2,
                                  I_convert_amt_per_unit              IN       VARCHAR2,
                                  I_update_forecast_unit_amt          IN       VARCHAR2,
                                  I_update_actual_fixed_totals        IN       VARCHAR2,
                                  I_upd_deal_detail_actual_total      IN       VARCHAR2,
                                  I_update_budget_fixed_totals        IN       VARCHAR2,
                                  I_upd_deal_detail_budget_total      IN       VARCHAR2,
                                  I_update_total_baseline             IN       VARCHAR2,
                                  I_update_turnover_trend             IN       VARCHAR2,
                                  I_forecast_income_calc              IN       VARCHAR2,
                                  I_deal_to_date_calcs                IN       VARCHAR2)
RETURN BOOLEAN IS

   L_amt_per_unit         DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE := to_number(IO_amt_per_unit,'99999999999999999999.9999');
   L_old_period_turnover  DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE := to_number(I_old_period_turnover,'99999999999999999999.9999');
   L_new_period_turnover  DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE := to_number(I_new_period_turnover,'99999999999999999999.9999');
   L_deal_id              DEAL_HEAD.DEAL_ID%TYPE := to_number(I_deal_id,'9999999999');
   L_deal_detail_id       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE;
   L_program              VARCHAR2(100) := 'DEAL_INCOME_SQL.EXTERNAL_APPLY_DEAL_PERF';

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if IO_amt_per_unit is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'IO_amt_per_unit',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_old_period_turnover is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_period_turnover',
                                            L_program,
                                            NULL);
        return FALSE; 
   end if;

   if I_new_period_turnover is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_period_turnover',
                                            L_program,
                                            NULL);
        return FALSE; 
   end if;

   select to_number(DECODE(I_deal_detail_id,' ','-1',I_deal_detail_id),'9999999999') into L_deal_detail_id from dual;

   if I_convert_amt_per_unit = 'Y' then
      if DEAL_INCOME_SQL.CONVERT_AMT_PER_UNIT(O_error_message,
                                              L_amt_per_unit,
                                              L_deal_id,
                                              L_deal_detail_id)= FALSE then
         return FALSE;
      end if;
   end if;
   IO_amt_per_unit := to_char(L_amt_per_unit);
   if I_update_forecast_unit_amt = 'Y' then
      if DEAL_INCOME_SQL.UPDATE_FORECAST_UNIT_AMT(O_error_message,
                                                  L_deal_id,
                                                  L_deal_detail_id,
                                                  L_amt_per_unit) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_update_actual_fixed_totals = 'Y' then
      if DEAL_INCOME_SQL.UPDATE_ACTUAL_FIXED_TOTALS(O_error_message,
                                                    L_deal_id,
                                                    L_deal_detail_id,
                                                    L_old_period_turnover,
                                                    L_new_period_turnover) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_upd_deal_detail_actual_total = 'Y' then
      if DEAL_INCOME_SQL.UPD_DEAL_DETAIL_ACTUAL_TOTALS(O_error_message,
                                                         L_deal_id,
                                                         L_deal_detail_id) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_update_budget_fixed_totals = 'Y' then
      if DEAL_INCOME_SQL.UPDATE_BUDGET_FIXED_TOTALS(O_error_message,
                                                    L_deal_id,
                                                    L_deal_detail_id,
                                                    L_old_period_turnover,
                                                    L_new_period_turnover) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_upd_deal_detail_budget_total = 'Y' then
      if DEAL_INCOME_SQL.UPD_DEAL_DETAIL_BUDGET_TOTALS(O_error_message,
                                                          L_deal_id,
                                                          L_deal_detail_id) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_update_total_baseline = 'Y' then
      if DEAL_INCOME_SQL.UPDATE_TOTAL_BASELINE(O_error_message,
                                               L_deal_id,
                                               L_deal_detail_id) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_update_turnover_trend = 'Y' then
      if DEAL_INCOME_SQL.UPDATE_TURNOVER_TREND(O_error_message,
                                               L_deal_id,
                                               L_deal_detail_id) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_forecast_income_calc = 'Y' then
      if DEAL_INCOME_SQL.FORECAST_INCOME_CALC(O_error_message,
                                              L_deal_id,
                                              L_deal_detail_id,
                                              L_amt_per_unit) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_deal_to_date_calcs = 'Y' then
      if DEAL_INCOME_SQL.DEAL_TO_DATE_CALCS(O_error_message,
                                            L_deal_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END EXTERNAL_APPLY_DEAL_PERF;
---------------------------------------------------------------------------------------------------------------
FUNCTION CONVERT_AMT_PER_UNIT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_amt_per_unit      IN OUT   DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE,
                              I_deal_id           IN       DEAL_DETAIL.DEAL_ID%TYPE,
                              I_deal_detail_id    IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(100) := 'DEAL_INCOME_SQL.CONVERT_AMT_PER_UNIT';
   L_deal_head_tbl      deal_head_tbl;
   L_deal_detail_tbl    deal_detail_tbl;

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if DEAL_INCOME_SQL.GET_DEAL_DETAILS(O_error_message,
                                       L_deal_head_tbl,
                                       L_deal_detail_tbl,
                                       I_deal_id,
                                       I_deal_detail_id) = FALSE then
      return FALSE;
   end if;
   if L_deal_head_tbl is NOT NULL and L_deal_head_tbl.count > 0 then
      if L_deal_head_tbl(1).threshold_limit_type = 'Q' then
         if L_deal_head_tbl(1).status = 'W' then
            if L_deal_detail_tbl(1).total_budget_turnover = 0.0 then
               O_amt_per_unit := 0.0;
            else
               O_amt_per_unit := L_deal_detail_tbl(1).total_forecast_revenue/L_deal_detail_tbl(1).total_budget_turnover;
            end if;
         else
            if L_deal_detail_tbl(1).total_actual_forecast_turnover = 0.0 then
               O_amt_per_unit := 0.0;
            else
               O_amt_per_unit := L_deal_detail_tbl(1).total_forecast_revenue/L_deal_detail_tbl(1).total_actual_forecast_turnover;
            end if;
         end if;
      elsif L_deal_head_tbl(1).threshold_limit_type = 'A' then
            if L_deal_detail_tbl(1).total_forecast_units = 0.0 then
               O_amt_per_unit := 0.0;
            else
               if L_deal_head_tbl(1).status = 'W' then
                  O_amt_per_unit := L_deal_detail_tbl(1).total_forecast_units/L_deal_detail_tbl(1).total_budget_turnover;
               else
                  O_amt_per_unit := L_deal_detail_tbl(1).total_forecast_units/L_deal_detail_tbl(1).total_actual_forecast_turnover;
               end if;
            end if;
      end if;
   end if;

   return TRUE;
EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
   return FALSE;
END CONVERT_AMT_PER_UNIT;
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_FORECAST_UNIT_AMT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_deal_id           IN       DEAL_DETAIL.DEAL_ID%TYPE,
                                  I_deal_detail_id    IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                  I_amt_per_unit      IN       DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(100) := 'DEAL_INCOME_SQL.UPDATE_FORECAST_UNIT_AMT';
   L_deal_head_tbl            deal_head_tbl;
   L_deal_detail_tbl          deal_detail_tbl;
   L_total_forecast_value     DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE := 0.00;

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_amt_per_unit is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if DEAL_INCOME_SQL.GET_DEAL_DETAILS(O_error_message,
                                       L_deal_head_tbl,
                                       L_deal_detail_tbl,
                                       I_deal_id,
                                       I_deal_detail_id) = FALSE then
      return FALSE;
   end if;

   if I_amt_per_unit = 0.0 then
      L_total_forecast_value := 0.0;
   else
      L_total_forecast_value := L_deal_detail_tbl(1).total_actual_forecast_turnover * I_amt_per_unit;
   end if;

   if L_deal_head_tbl(1).threshold_limit_type = 'Q' then
      update deal_detail
         set total_forecast_revenue = L_total_forecast_value
       where rowid = L_deal_detail_tbl(1).rowid;
   elsif L_deal_head_tbl(1).threshold_limit_type = 'A' then
         update deal_detail
            set total_forecast_units = L_total_forecast_value
          where rowid = L_deal_detail_tbl(1).rowid;
   end if;

   return TRUE;
EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END UPDATE_FORECAST_UNIT_AMT;
---------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ACTUAL_FIXED_TOTALS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                                    I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                    I_old_period_turnover      IN       DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE,
                                    I_new_period_turnover      IN       DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE)
RETURN BOOLEAN IS

   L_program                              VARCHAR2(100) := 'DEAL_INCOME_SQL.UPDATE_ACTUAL_FIXED_TOTALS';
   L_daf_totals_tbl                       daf_totals_tbl;
   L_deal_detail_tbl                      deal_detail_tbl;
   L_actuals_act_fct_turnover             DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE;
   L_total_af_turnover                    DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE;
   L_new_act_fct_turnover_totals          DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE;
   L_new_actuals_ratio                    DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE;
   L_actuals_total                        DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE;
   L_act_fct_turnover_adj_amt             DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE;

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_old_period_turnover is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_period_turnover',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_new_period_turnover is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_period_turnover',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if DEAL_INCOME_SQL.GET_DEAL_DETAIL_ROW(O_error_message,
                                          L_deal_detail_tbl,
                                          I_deal_id,
                                          I_deal_detail_id) = FALSE then
      return FALSE;
   end if;
   L_total_af_turnover := L_deal_detail_tbl(1).total_actual_forecast_turnover;
   --Get the actuals totals.
   if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message,
                                                     L_daf_totals_tbl,
                                                     I_deal_id,
                                                     I_deal_detail_id,
                                                     'A') = FALSE then  
      return FALSE;
   end if;
   L_actuals_act_fct_turnover := L_daf_totals_tbl(1).actual_forecast_turnover;

   -- If the actuals exceed the fixed total then all forecasts need to be set to zero
   -- and the fixed total value on DEAL_DETAIL updated with the actuals total
   -- regardless of what the fixed flag may be

   if L_actuals_act_fct_turnover > L_total_af_turnover then
      update deal_detail
                  set total_actual_forecast_turnover = L_actuals_act_fct_turnover
                where deal_id        = I_deal_id
                  and deal_detail_id = I_deal_detail_id;

      update deal_actuals_forecast
                     set actual_forecast_turnover = 0
                   where deal_id             = I_deal_id
                     and deal_detail_id      = I_deal_detail_id
                     and actual_forecast_ind = 'F';
   end if;

   -- Calculate actuals
   L_actuals_total := L_actuals_act_fct_turnover - I_new_period_turnover;

   -- Calculate ratio to apply back to each of the period values.
   L_new_actuals_ratio := (L_total_af_turnover - (I_new_period_turnover + L_actuals_total))/(L_total_af_turnover - (I_old_period_turnover + L_actuals_total));

   --Update all the forecast values using the even spread amount.
   update deal_actuals_forecast
               set actual_forecast_turnover = (actual_forecast_turnover * L_new_actuals_ratio)
             where deal_id             = I_deal_id
               and deal_detail_id      = I_deal_detail_id
               and actual_forecast_ind = 'F';

   -- Retrieve the summed total for all the periods (Actual and Forecast)
   if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message,
                                                     L_daf_totals_tbl,
                                                     I_deal_id,
                                                     I_deal_detail_id,
                                                     NULL) = FALSE then  
      return FALSE;
   end if;
   L_new_act_fct_turnover_totals := L_daf_totals_tbl(1).actual_forecast_turnover;

   -- If the fixed total does not equal the summed total then we need to
   -- adjust the last period to ensure they match (this will be caused by
   -- rounding errors during the period updates)
   if L_new_act_fct_turnover_totals <> L_total_af_turnover then
      L_act_fct_turnover_adj_amt := L_new_act_fct_turnover_totals - L_total_af_turnover;
      update deal_actuals_forecast daf
                  set actual_forecast_turnover = actual_forecast_turnover - L_act_fct_turnover_adj_amt
                where daf.deal_id             = I_deal_id
                  and daf.deal_detail_id      = I_deal_detail_id
                  and daf.actual_forecast_ind = 'F'
                  and daf.reporting_date = (select max(reporting_date)
                                              from deal_actuals_forecast daf2
                                             where daf2.deal_id             = daf.deal_id
                                               and daf2.actual_forecast_ind = daf.actual_forecast_ind
                                               and daf2.deal_detail_id      = daf.deal_detail_id);
   end if;

   return TRUE;
EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END UPDATE_ACTUAL_FIXED_TOTALS;
---------------------------------------------------------------------------------------------------------------
FUNCTION UPD_DEAL_DETAIL_ACTUAL_TOTALS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                                       I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN IS
   L_program                              VARCHAR2(100) := 'DEAL_INCOME_SQL.UPD_DEAL_DETAIL_ACTUAL_TOTALS';
   L_daf_totals_tbl                       daf_totals_tbl;
   L_total_af_turnover                    DEAL_DETAIL.TOTAL_ACTUAL_FORECAST_TURNOVER%TYPE;

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message,
                                                     L_daf_totals_tbl,
                                                     I_deal_id,
                                                     I_deal_detail_id,
                                                     NULL) = FALSE then
      return FALSE;
   end if;
   L_total_af_turnover := L_daf_totals_tbl(1).actual_forecast_turnover;

   update deal_detail
               set total_actual_forecast_turnover = L_total_af_turnover
             where deal_id        = I_deal_id
               and deal_detail_id = I_deal_detail_id;

   return TRUE;
EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END UPD_DEAL_DETAIL_ACTUAL_TOTALS;
----------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_BUDGET_FIXED_TOTALS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                                    I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                    I_old_period_turnover      IN       DEAL_DETAIL.TOTAL_BUDGET_TURNOVER%TYPE,
                                    I_new_period_turnover      IN       DEAL_DETAIL.TOTAL_BUDGET_TURNOVER%TYPE)
RETURN BOOLEAN IS

   L_program                              VARCHAR2(100) := 'DEAL_INCOME_SQL.UPDATE_BUDGET_FIXED_TOTALS';
   L_daf_totals_tbl                       daf_totals_tbl;
   L_deal_detail_tbl                      deal_detail_tbl;
   L_actuals_budget_turnover              DEAL_ACTUALS_FORECAST.BUDGET_TURNOVER%TYPE;
   L_budget_turnover_adj_amt              DEAL_ACTUALS_FORECAST.BUDGET_TURNOVER%TYPE;
   L_total_budget_turnover                DEAL_DETAIL.TOTAL_BUDGET_TURNOVER%TYPE;
   L_new_budget_turnover_totals           DEAL_ACTUALS_FORECAST.BUDGET_TURNOVER%TYPE;
   L_new_budget_ratio                     DEAL_ACTUALS_FORECAST.BUDGET_TURNOVER%TYPE;
   L_actuals_total                        DEAL_ACTUALS_FORECAST.BUDGET_TURNOVER%TYPE;

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_old_period_turnover is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_period_turnover',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_new_period_turnover is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_new_period_turnover',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;

   if DEAL_INCOME_SQL.GET_DEAL_DETAIL_ROW(O_error_message,
                                          L_deal_detail_tbl,
                                          I_deal_id,
                                          I_deal_detail_id) = FALSE then
      return FALSE;
   end if;
   L_total_budget_turnover := L_deal_detail_tbl(1).total_budget_turnover;

   -- Get the actuals totals
   if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message,
                                                     L_daf_totals_tbl,
                                                     I_deal_id,
                                                     I_deal_detail_id,
                                                     'A') = FALSE then
      return FALSE;
   end if;
   L_actuals_budget_turnover := L_daf_totals_tbl(1).budget_turnover;

   -- If the actuals exceed the fixed total then all forecasts need to be set to zero
   -- and the fixed total value on DEAL_DETAIL updated with the actuals total
   -- regardless of what the fixed flag may be
   if L_actuals_budget_turnover > L_total_budget_turnover then
      update deal_detail
         set total_budget_turnover = L_actuals_budget_turnover
       where deal_id        = I_deal_id
         and deal_detail_id = I_deal_detail_id;

      update deal_actuals_forecast
         set budget_turnover = 0
       where deal_id             = I_deal_id
         and deal_detail_id      = I_deal_detail_id
         and actual_forecast_ind = 'F';
   end if;

   L_actuals_total := L_actuals_budget_turnover - I_new_period_turnover;

   L_new_budget_ratio := (L_total_budget_turnover - (I_new_period_turnover + L_actuals_total))/(L_total_budget_turnover - (I_old_period_turnover + L_actuals_total));

   --Update all the forecast values based on the ratio.
   update deal_actuals_forecast
               set budget_turnover     = (budget_turnover * L_new_budget_ratio)
             where deal_id             = I_deal_id
               and deal_detail_id      = I_deal_detail_id
               and actual_forecast_ind = 'F';

   -- Retrieve the summed total for all the periods (Actual and Forecast).
   if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message,
                                                     L_daf_totals_tbl,
                                                     I_deal_id,
                                                     I_deal_detail_id,
                                                     NULL) = FALSE then
      return FALSE;
   end if;
   L_new_budget_turnover_totals := L_daf_totals_tbl(1).budget_turnover;

   -- If the fixed total does not equal the summed total then we need to
   -- adjust the last period to ensure they match (this will be caused by
   -- rounding errors during the period updates).
   if L_new_budget_turnover_totals <> L_total_budget_turnover then
    -- Calculate the adjustment required (Will work for -ve and +ve)
      L_budget_turnover_adj_amt := L_new_budget_turnover_totals - L_total_budget_turnover;
      update deal_actuals_forecast daf
                     set budget_turnover = budget_turnover - L_budget_turnover_adj_amt
                   where daf.deal_id             = I_deal_id
                     and daf.deal_detail_id      = I_deal_detail_id
                     and daf.actual_forecast_ind = 'F'
                     and daf.reporting_date      = (select max(reporting_date)
                                                      from deal_actuals_forecast daf2
                                                     where daf2.deal_id             = daf.deal_id
                                                       and daf2.actual_forecast_ind = daf.actual_forecast_ind
                                                       and daf2.deal_detail_id      = daf.deal_detail_id);
   end if;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END UPDATE_BUDGET_FIXED_TOTALS;
----------------------------------------------------------------------------------------------------------------
FUNCTION UPD_DEAL_DETAIL_BUDGET_TOTALS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                                       I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN IS

   L_program                              VARCHAR2(100) := 'DEAL_INCOME_SQL.UPD_DEAL_DETAIL_BUDGET_TOTALS';
   L_daf_totals_tbl                       daf_totals_tbl;
   L_total_budget_turnover                DEAL_DETAIL.TOTAL_BUDGET_TURNOVER%TYPE;

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message,
                                                     L_daf_totals_tbl,
                                                     I_deal_id,
                                                     I_deal_detail_id,
                                                     NULL) = FALSE then
      return FALSE;
   end if;
   L_total_budget_turnover := L_daf_totals_tbl(1).budget_turnover;

   UPDATE deal_detail
      SET total_budget_turnover = L_total_budget_turnover
    WHERE deal_id        = I_deal_id
      AND deal_detail_id = I_deal_detail_id;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
   return FALSE;
END UPD_DEAL_DETAIL_BUDGET_TOTALS;
----------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_TOTAL_BASELINE(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                               I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN IS

   L_program                              VARCHAR2(100) := 'DEAL_INCOME_SQL.UPDATE_TOTAL_BASELINE';
   L_daf_totals_tbl                       daf_totals_tbl;
   L_deal_head_tbl                        deal_head_tbl;
   L_deal_detail_tbl                      deal_detail_tbl;
   L_total_budget_turnover                DEAL_DETAIL.TOTAL_BUDGET_TURNOVER%TYPE;
   L_total_af_turnover                    DEAL_DETAIL.TOTAL_ACTUAL_FORECAST_TURNOVER%TYPE;
   L_total_baseline_growth_budget         DEAL_ACTUALS_FORECAST.BASELINE_TURNOVER%TYPE;
   L_total_baseline_growth_act_fr        DEAL_ACTUALS_FORECAST.BASELINE_TURNOVER%TYPE;

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if DEAL_INCOME_SQL.GET_DEAL_DETAILS(O_error_message,
                                       L_deal_head_tbl,
                                       L_deal_detail_tbl,
                                       I_deal_id,
                                       I_deal_detail_id) = FALSE then
      return FALSE;
   end if;

   L_total_budget_turnover := L_deal_detail_tbl(1).total_budget_turnover;
   L_total_af_turnover := L_deal_detail_tbl(1).total_actual_forecast_turnover;

   -- Retrieve the summed deal totals i.e. baseline totals.
   if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message,
                                                     L_daf_totals_tbl,
                                                     I_deal_id,
                                                     I_deal_detail_id,
                                                     NULL) = FALSE then
      return FALSE;
   end if;

   if L_daf_totals_tbl(1).baseline_turnover = 0.0 then
      L_total_baseline_growth_budget := 0.0;
      L_total_baseline_growth_act_fr := 0.0;
   else
      if L_deal_head_tbl(1).status = 'W' then
         L_total_baseline_growth_budget := (L_total_budget_turnover - L_daf_totals_tbl(1).baseline_turnover)/(L_daf_totals_tbl(1).baseline_turnover/100);
      else
         L_total_baseline_growth_act_fr := (L_total_af_turnover - L_daf_totals_tbl(1).baseline_turnover)/(L_daf_totals_tbl(1).baseline_turnover/100);
      end if;
   end if;

   if L_deal_head_tbl(1).status = 'W' then
      update deal_detail
                  set total_baseline_growth_budget = L_total_baseline_growth_budget
                where deal_id        = I_deal_id
                  and deal_detail_id = I_deal_detail_id;
   else
      update deal_detail
                  set total_baseline_growth_act_for = L_total_baseline_growth_act_fr
                where deal_id        = I_deal_id
                  and deal_detail_id = I_deal_detail_id;
   end if;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
   return FALSE;
END UPDATE_TOTAL_BASELINE;
----------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_TURNOVER_TREND(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                               I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN IS

   L_program                              VARCHAR2(100) := 'DEAL_INCOME_SQL.UPDATE_TURNOVER_TREND';
   L_deal_detail_tbl                      deal_detail_tbl;
   L_growth_rate_to_date                  DEAL_DETAIL.GROWTH_RATE_TO_DATE%TYPE;

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if DEAL_INCOME_SQL.GET_DEAL_DETAIL_ROW(O_error_message,
                                          L_deal_detail_tbl,
                                          I_deal_id,
                                          I_deal_detail_id) = FALSE then
      return FALSE;
   end if;

   L_growth_rate_to_date := L_deal_detail_tbl(1).growth_rate_to_date;

   update deal_actuals_forecast
      set actual_forecast_trend_turnover = decode(actual_forecast_ind,'F', (actual_forecast_turnover *((L_growth_rate_to_date / 100) + 1.0)),'A', actual_forecast_turnover),
                   actual_forecast_trend_income   = decode(actual_forecast_ind,'F', actual_forecast_trend_income,'A',actual_income)
             where deal_id        = I_deal_id
               and deal_detail_id = I_deal_detail_id;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END UPDATE_TURNOVER_TREND;
----------------------------------------------------------------------------------------------------------------
FUNCTION FORECAST_INCOME_CALC(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                              I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                              I_amt_per_unit             IN       DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE)
RETURN BOOLEAN IS
   L_program                              VARCHAR2(100) := 'DEAL_INCOME_SQL.FORECAST_INCOME_CALC';
   L_daf_totals_tbl                       daf_totals_tbl;
   L_deal_head_tbl                        deal_head_tbl;
   L_deal_detail_tbl                      deal_detail_tbl;

   L_reporting_date                       VARCHAR2(15);
   L_temp_double                          VARCHAR2(10);
   L_actual_forecast_ind                  DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_IND%TYPE;
   L_first_period                         NUMBER(4);
   L_temp                                 NUMBER(20,4) := 0.00;

   L_budget_turnover_total                DEAL_ACTUALS_FORECAST.BUDGET_TURNOVER%TYPE                 :=0.00;
   L_af_turnover_total                    DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        :=0.00;
   L_af_trend_turnover_total              DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TREND_TURNOVER%TYPE  :=0.00;

   L_budget_income_total                  DEAL_ACTUALS_FORECAST.BUDGET_INCOME%TYPE                   :=0.00;
   L_af_income_total                      DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE                   :=0.00;
   L_af_trend_income_total                DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TREND_INCOME%TYPE    :=0.00;
   
   L_budget_turnover                      DEAL_ACTUALS_FORECAST.BUDGET_TURNOVER%TYPE                 :=0.00;
   L_af_turnover                          DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        :=0.00;
   L_af_trend_turnover                    DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TREND_TURNOVER%TYPE  :=0.00;
   
   L_budget_income                        DEAL_ACTUALS_FORECAST.BUDGET_INCOME%TYPE                   :=0.00;
   L_af_income                            DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE                   :=0.00;
   L_af_order_income                      DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE                   :=0.00;
   L_af_trend_income                      DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TREND_INCOME%TYPE    :=0.00;
   
   L_ptd_budget_turnover                  DEAL_ACTUALS_FORECAST.BUDGET_TURNOVER%TYPE                 :=0.00;
   L_ptd_af_turnover                      DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        :=0.00;
   L_ptd_af_trend_turnover                DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TREND_TURNOVER%TYPE  :=0.00;
   
   L_budget_income_calc                   DEAL_ACTUALS_FORECAST.BUDGET_INCOME%TYPE                   :=0.00;
   L_af_income_calc                       DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE                   :=0.00;
   L_af_trend_income_calc                 DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TREND_INCOME%TYPE    :=0.00;
   
   L_prev_budget_income                   DEAL_ACTUALS_FORECAST.BUDGET_INCOME%TYPE                   :=0.00;
   L_prev_af_income                       DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE                   :=0.00;
   L_prev_af_trend_income                 DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TREND_INCOME%TYPE    :=0.00;
   
   L_actual_units                         DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_UNITS%TYPE;
   L_actual_revenue                       DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_REVENUE%TYPE;
   L_actuals_td                           DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_REVENUE%TYPE;
   L_af_turnover_td                       DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE;
   L_af_trend_turnover_td                 DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TREND_TURNOVER%TYPE;
   L_forecast_turnover_td                 DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE;
   L_forecast_turnover_td_trend           DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TREND_TURNOVER%TYPE;
   L_weighted_amt_per_unit                DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE;
   L_weighted_amt_per_unit_trend          DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE;
   L_actuals_order                        DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_REVENUE%TYPE;
   L_actuals_order_no                     DEAL_ACTUALS_ITEM_LOC.ORDER_NO%TYPE;
   
   TYPE all_periods_rec IS RECORD
       ( reporting_date        VARCHAR2(15),
         actual_forecast_ind   DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_IND%TYPE,
         actual_units          DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_UNITS%TYPE,
         actual_revenue        DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_REVENUE%TYPE,
         budget_turnover       DEAL_ACTUALS_FORECAST.BUDGET_TURNOVER%TYPE,
         af_turnover           DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE,
         af_trend_turnover     DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TREND_TURNOVER%type
        );   
   TYPE all_periods_tbl IS TABLE of all_periods_rec INDEX BY BINARY_INTEGER;

   all_period_tbl all_periods_tbl;

   cursor C_ALL_PERIODS is
      select to_char(daf.reporting_date, 'YYYYMMDD') reporting_date,
             daf.actual_forecast_ind,
             decode(daf.actual_forecast_ind,'F',0.0,sum(nvl(dail.actual_turnover_units,0))) actual_turnover_units,
             decode(daf.actual_forecast_ind,'F',0.0,sum(nvl(dail.actual_turnover_revenue,0))) actual_turnover_revenue,
             daf.budget_turnover,
             daf.actual_forecast_turnover,
             daf.actual_forecast_trend_turnover
        from deal_actuals_forecast daf,
             (select deal_id,
                     deal_detail_id,
                     max(reporting_date) last_reporting_date
                from deal_actuals_forecast
            group by deal_id,
                     deal_detail_id) vmd,
             deal_actuals_item_loc dail
       where daf.deal_id            = I_deal_id
         and daf.deal_detail_id     = I_deal_detail_id
         and vmd.deal_id            = I_deal_id
         and vmd.deal_detail_id     = I_deal_detail_id
         and dail.deal_id(+)        = I_deal_id
         and dail.deal_detail_id(+) = I_deal_detail_id
         and dail.reporting_date(+) = daf.reporting_date
    group by daf.reporting_date,
             daf.actual_forecast_ind,
             daf.budget_turnover,
             daf.actual_forecast_turnover,
             daf.actual_forecast_trend_turnover,
             vmd.last_reporting_date
    order by daf.reporting_date;

   cursor C_FORECAST_PERIODS is
      select to_char(daf.reporting_date, 'YYYYMMDD'),
             null actual_forecast_ind,
             null actual_units,
             null actual_revenue,
             daf.budget_turnover,
             daf.actual_forecast_turnover,
             daf.actual_forecast_trend_turnover
        from deal_actuals_forecast daf,
             (select deal_id,
                     deal_detail_id,
                     max(reporting_date) last_reporting_date
                from deal_actuals_forecast
             group by deal_id, deal_detail_id) vmd
       where daf.deal_id        = I_deal_id
         and daf.deal_detail_id = I_deal_detail_id
         and daf.actual_forecast_ind = 'F'
         and vmd.deal_id        = I_deal_id
         and vmd.deal_detail_id = I_deal_detail_id
    order by daf.reporting_date;

   cursor c_get_actuals_order is
         select nvl(order_no, -1) actuals_order_no,
                sum(nvl(decode(L_deal_head_tbl(1).threshold_limit_type,'Q',actual_turnover_units,'A', actual_turnover_revenue),0)) actuals_order
           from deal_actuals_item_loc
          where deal_id        = I_deal_id
            and deal_detail_id = I_deal_detail_id
            and reporting_date = to_date(L_reporting_date, 'YYYYMMDD')
          group by nvl(order_no, -1);

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_amt_per_unit is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_amt_per_unit',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if DEAL_INCOME_SQL.GET_DEAL_DETAILS(O_error_message,
                                       L_deal_head_tbl,
                                       L_deal_detail_tbl,
                                       I_deal_id,
                                       I_deal_detail_id) = FALSE then
      return FALSE;
   end if;

   if I_deal_id = -1 then
      if L_deal_head_tbl is not null and L_deal_head_tbl.count > 0 then
         if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                             L_budget_income,
                                             I_deal_id,
                                             I_deal_detail_id,
                                             L_deal_head_tbl(1).threshold_limit_type,
                                             L_deal_detail_tbl(1).threshold_value_type,
                                             L_deal_head_tbl(1).rebate_calc_type,
                                             L_deal_detail_tbl(1).calc_to_zero_ind,
                                             L_deal_head_tbl(1).deal_income_calculation,
                                             L_deal_head_tbl(1).rebate_ind,
                                             0,
                                             0,
                                             0,
                                             0) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   -- If the calculation type is pro-rated using forecast then get the forecast totals and use these values to determine the total income for the component.
   if L_deal_head_tbl is not null and L_deal_head_tbl.count > 0 then
      if L_deal_head_tbl(1).deal_income_calculation = 'P' then
         if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message,
                                                           L_daf_totals_tbl,
                                                           I_deal_id,
                                                           I_deal_detail_id,
                                                           NULL) = FALSE then
             return FALSE;
         end if;
         L_budget_turnover_total    := L_daf_totals_tbl(1).budget_turnover;
         L_af_turnover_total        := L_daf_totals_tbl(1).actual_forecast_turnover;
         L_af_trend_turnover_total  := L_daf_totals_tbl(1).actual_forecast_trend_turnover;
      
         L_actuals_td                 := 0;
         L_af_turnover_td             := 0;
         L_af_trend_turnover_td       := 0;
         L_forecast_turnover_td       := 0;
         L_forecast_turnover_td_trend := 0;
      
         for rec in C_ALL_PERIODS
          loop
             L_reporting_date      := rec.reporting_date;
             L_actual_forecast_ind := rec.actual_forecast_ind;
             L_actual_units        := rec.actual_turnover_units;
             L_actual_revenue      := rec.actual_turnover_revenue;
             L_budget_turnover     := rec.budget_turnover;
             L_af_turnover         := rec.actual_forecast_turnover;
             L_af_trend_turnover   := rec.actual_forecast_trend_turnover;
      
             if L_deal_head_tbl(1).status <> 'W' then
             -- Variables for weighted amount per unit calculation.
                if L_deal_head_tbl(1).threshold_limit_type = 'Q' then
                   L_actuals_td := L_actuals_td + L_actual_revenue;
                else
                   L_actuals_td := L_actuals_td + L_actual_units;
                end if;
                L_af_turnover_td       := L_af_turnover_td + L_af_turnover;
                L_af_trend_turnover_td := L_af_trend_turnover_td + L_af_trend_turnover;
      
                if L_actual_forecast_ind = 'F' then
                   L_forecast_turnover_td       := L_forecast_turnover_td + L_af_turnover;
                   L_forecast_turnover_td_trend := L_forecast_turnover_td_trend + L_af_trend_turnover;
                end if;
                if (L_deal_head_tbl(1).threshold_limit_type = 'Q' and L_deal_detail_tbl(1).threshold_value_type = 'P') or
                   (L_deal_head_tbl(1).threshold_limit_type = 'A' and L_deal_detail_tbl(1).threshold_value_type = 'A') then
                   if L_af_turnover_td = 0 then
                      L_weighted_amt_per_unit := 0;
                   else
                      L_weighted_amt_per_unit       := (L_actuals_td + (L_forecast_turnover_td * I_amt_per_unit)) / L_af_turnover_td;
                   end if;
                   
                   if L_af_trend_turnover_td = 0 then
                      L_weighted_amt_per_unit_trend := 0;
                   else
                      L_weighted_amt_per_unit_trend := (L_actuals_td + (L_forecast_turnover_td_trend * I_amt_per_unit)) / L_af_trend_turnover_td;
                   end if; 
                else
                    L_weighted_amt_per_unit       := I_amt_per_unit;
                    L_weighted_amt_per_unit_trend := I_amt_per_unit;
                end if;
            end if;
         -- For bill-back non-rebate deals need to pass in the period turnover and sum the income returned
         -- for each pass. This will then be fed into the prorated calc in the second reporting period loop below.
            if L_deal_head_tbl(1).rebate_ind = 'N' then
            -- Calculate the budget turnover income only if deal is in Worksheet status.
               if L_deal_head_tbl(1).status = 'W' then
                  L_budget_income := 0.0;
                  if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                      L_budget_income,
                                                      I_deal_id,
                                                      I_deal_detail_id,
                                                      L_deal_head_tbl(1).threshold_limit_type,
                                                      L_deal_detail_tbl(1).threshold_value_type,
                                                      L_deal_head_tbl(1).rebate_calc_type,
                                                      L_deal_detail_tbl(1).calc_to_zero_ind,
                                                      L_deal_head_tbl(1).deal_income_calculation,
                                                      L_deal_head_tbl(1).rebate_ind,
                                                      L_budget_turnover,
                                                      L_budget_turnover,
                                                      I_amt_per_unit,
                                                      I_amt_per_unit) = FALSE then
                     return FALSE;
                  end if;
                  L_budget_income_total := L_budget_income_total + L_budget_income;
               end if;
            -- Calculate the actual forecast/trend turnover income only if deal is Approved.
               if L_deal_head_tbl(1).status = 'A' then
                  L_af_income := 0.0;
                  if L_actual_forecast_ind = 'F' then
                     if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                         L_af_income,
                                                         I_deal_id,
                                                         I_deal_detail_id,
                                                         L_deal_head_tbl(1).threshold_limit_type,
                                                         L_deal_detail_tbl(1).threshold_value_type,
                                                         L_deal_head_tbl(1).rebate_calc_type,
                                                         L_deal_detail_tbl(1).calc_to_zero_ind,
                                                         L_deal_head_tbl(1).deal_income_calculation,
                                                         L_deal_head_tbl(1).rebate_ind,
                                                         L_af_turnover,
                                                         L_af_turnover,
                                                         I_amt_per_unit,
                                                         L_weighted_amt_per_unit) = FALSE then
                        return FALSE;
                     end if;
                  else
                     L_af_order_income := 0.0;
                     L_actuals_order   := 0.0;
                     for i in c_get_actuals_order
                     loop
                        L_actuals_order_no := i.actuals_order_no;
                        L_actuals_order    := i.actuals_order;
                        if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                            L_af_order_income,
                                                            I_deal_id,
                                                            I_deal_detail_id,
                                                            L_deal_head_tbl(1).threshold_limit_type,
                                                            L_deal_detail_tbl(1).threshold_value_type,
                                                            L_deal_head_tbl(1).rebate_calc_type,
                                                            L_deal_detail_tbl(1).calc_to_zero_ind,
                                                            L_deal_head_tbl(1).deal_income_calculation,
                                                            L_deal_head_tbl(1).rebate_ind,
                                                            L_actuals_order,
                                                            L_actuals_order,
                                                            I_amt_per_unit,
                                                            L_weighted_amt_per_unit) = FALSE then
                            return FALSE;
                        end if;
                        if L_af_order_income < 0 then
                           L_af_order_income := 0; 
                        end if;
                        L_af_income := L_af_income + L_af_order_income;
                     end loop;
                  end if;
                  L_af_income_total := L_af_income_total + L_af_income;
                  L_af_trend_income := 0.0;
                  if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                      L_af_trend_income,
                                                      I_deal_id,
                                                      I_deal_detail_id,
                                                      L_deal_head_tbl(1).threshold_limit_type,
                                                      L_deal_detail_tbl(1).threshold_value_type,
                                                      L_deal_head_tbl(1).rebate_calc_type,
                                                      L_deal_detail_tbl(1).calc_to_zero_ind,
                                                      L_deal_head_tbl(1).deal_income_calculation,
                                                      L_deal_head_tbl(1).rebate_ind,
                                                      L_af_trend_turnover,
                                                      L_af_trend_turnover,
                                                      I_amt_per_unit,
                                                      L_weighted_amt_per_unit) = FALSE then
                     return FALSE;
                  end if;
                  L_af_trend_income_total := L_af_trend_income_total + L_af_trend_income;
               end if;
            end if;
         end loop;
         if L_deal_head_tbl(1).rebate_ind = 'Y' then
         -- Calculate the total budget turnover income only if deal is in Worksheet status.
            if L_deal_head_tbl(1).status = 'W' then
               L_budget_income_total := 0.0;
               if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                   L_budget_income_total,
                                                   I_deal_id,
                                                   I_deal_detail_id,
                                                   L_deal_head_tbl(1).threshold_limit_type,
                                                   L_deal_detail_tbl(1).threshold_value_type,
                                                   L_deal_head_tbl(1).rebate_calc_type,
                                                   L_deal_detail_tbl(1).calc_to_zero_ind,
                                                   L_deal_head_tbl(1).deal_income_calculation,
                                                   L_deal_head_tbl(1).rebate_ind,
                                                   L_budget_turnover_total,
                                                   L_budget_turnover_total,
                                                   I_amt_per_unit,
                                                   I_amt_per_unit) = FALSE then
                   return FALSE;
               end if;
            end if;
         -- Calculate the total actual forecast/trend turnover income only if deal is Approved.
            if L_deal_head_tbl(1).status = 'A' then
               L_af_income_total := 0.00;
               if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                   L_af_income_total,
                                                   I_deal_id,
                                                   I_deal_detail_id,
                                                   L_deal_head_tbl(1).threshold_limit_type,
                                                   L_deal_detail_tbl(1).threshold_value_type,
                                                   L_deal_head_tbl(1).rebate_calc_type,
                                                   L_deal_detail_tbl(1).calc_to_zero_ind,
                                                   L_deal_head_tbl(1).deal_income_calculation,
                                                   L_deal_head_tbl(1).rebate_ind,
                                                   L_af_turnover_total,
                                                   L_af_turnover_total,
                                                   I_amt_per_unit,
                                                   L_weighted_amt_per_unit) = FALSE then
                   return FALSE;
               end if;
               L_af_trend_income_total := 0.00;
               if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                   L_af_trend_income_total,
                                                   I_deal_id,
                                                   I_deal_detail_id,
                                                   L_deal_head_tbl(1).threshold_limit_type,
                                                   L_deal_detail_tbl(1).threshold_value_type,
                                                   L_deal_head_tbl(1).rebate_calc_type,
                                                   L_deal_detail_tbl(1).calc_to_zero_ind,
                                                   L_deal_head_tbl(1).deal_income_calculation,
                                                   L_deal_head_tbl(1).rebate_ind,
                                                   L_af_trend_turnover_total,
                                                   L_af_trend_turnover_total,
                                                   I_amt_per_unit,
                                                   L_weighted_amt_per_unit_trend) = FALSE then
                   return FALSE;
               end if;
            end if;
         end if;
      end if;
   end if;
   if L_deal_head_tbl is not null and L_deal_head_tbl.count > 0 then
      if L_deal_head_tbl(1).deal_income_calculation = 'P' then
      -- Retrieve the totals for Actuals periods.
         if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message,
                                                           L_daf_totals_tbl,
                                                           I_deal_id,
                                                           I_deal_detail_id,
                                                           'A') = FALSE then
             return FALSE;
         end if;
         L_prev_af_income        := L_daf_totals_tbl(1).actual_forecast_income;
         L_prev_af_trend_income  := L_daf_totals_tbl(1).actual_forecast_trend_income;
         L_ptd_af_turnover       := L_daf_totals_tbl(1).actual_forecast_turnover;
         L_ptd_af_trend_turnover := L_daf_totals_tbl(1).actual_forecast_trend_turnover;
      
         open C_FORECAST_PERIODS;
         fetch C_FORECAST_PERIODS BULK COLLECT into all_period_tbl;
         close C_FORECAST_PERIODS;
      else
         open C_ALL_PERIODS;
         fetch C_ALL_PERIODS BULK COLLECT into all_period_tbl;
         close C_ALL_PERIODS;
      end if;
   end if;
   L_actuals_td                 := 0;
   L_af_turnover_td             := 0;
   L_af_trend_turnover_td       := 0;
   L_forecast_turnover_td       := 0;
   L_forecast_turnover_td_trend := 0;
   L_first_period := 1;

   if all_period_tbl is not null and all_period_tbl.count > 0 then
      for rec in all_period_tbl.first..all_period_tbl.last
      loop
      -- Calculate period to date values.
         if L_deal_head_tbl(1).rebate_ind = 'Y' or L_deal_head_tbl(1).deal_income_calculation = 'P' then
            L_ptd_budget_turnover   := L_ptd_budget_turnover + all_period_tbl(rec).budget_turnover;
            L_ptd_af_turnover       := L_ptd_af_turnover + all_period_tbl(rec).af_turnover;
            L_ptd_af_trend_turnover := L_ptd_af_trend_turnover + all_period_tbl(rec).af_trend_turnover;
         else
            L_ptd_budget_turnover   := all_period_tbl(rec).budget_turnover;
            L_ptd_af_turnover       := all_period_tbl(rec).af_turnover;
            L_ptd_af_trend_turnover := all_period_tbl(rec).af_trend_turnover;
         end if;
      -- If the calculation type is pro-rated then the period income is set by determining the proportion of the deal for this period 
      -- and then multiplying this by the total for the entire deal. For actuals earned each period is calculated seperately.
         if L_deal_head_tbl(1).deal_income_calculation = 'P' then
            if L_budget_turnover_total = 0.0 then
               L_budget_income := 0.0;
            else
               L_budget_income := (L_ptd_budget_turnover/L_budget_turnover_total) * L_budget_income_total;
               L_budget_income := ROUND(L_budget_income,4);
               L_budget_income := L_budget_income - L_prev_budget_income;
            end if;
            if L_af_turnover_total = 0.0 then
               L_af_income := 0.0;
            else
               L_af_income := (L_ptd_af_turnover/L_af_turnover_total) * L_af_income_total;
               L_af_income := ROUND(L_af_income,4);
               L_af_income := L_af_income - L_prev_af_income;
            end if;
            if L_af_trend_turnover_total = 0.0 then
               L_af_trend_income := 0.0;
            else
               L_af_trend_income := (L_ptd_af_trend_turnover/L_af_trend_turnover_total) * L_af_trend_income_total;
               L_af_trend_income := ROUND(L_af_trend_income,4);
               L_af_trend_income := L_af_trend_income - L_prev_af_trend_income;
            end if;
      -- Calculate the previous income :
            L_prev_budget_income   := L_prev_budget_income + L_budget_income;
            L_prev_af_income       := L_prev_af_income + L_af_income;
            L_prev_af_trend_income := L_prev_af_trend_income + L_af_trend_income;
         else 
      -- Variables for weighted amount per unit calculation.
            if L_deal_head_tbl(1).threshold_limit_type = 'Q' then
               L_actuals_td := L_actuals_td + all_period_tbl(rec).actual_revenue;
            else
               L_actuals_td := L_actuals_td + all_period_tbl(rec).actual_units;
            end if;
            L_af_turnover_td       := L_af_turnover_td + all_period_tbl(rec).af_turnover;
            L_af_trend_turnover_td := L_af_trend_turnover_td + all_period_tbl(rec).af_trend_turnover;
            if all_period_tbl(rec).actual_forecast_ind = 'F' then
               L_forecast_turnover_td := L_forecast_turnover_td + all_period_tbl(rec).af_turnover;
               L_forecast_turnover_td_trend := L_forecast_turnover_td_trend + all_period_tbl(rec).af_trend_turnover;
            end if;
            if ((L_deal_head_tbl(1).threshold_limit_type = 'Q' and L_deal_detail_tbl(1).threshold_value_type = 'P') or
               (L_deal_head_tbl(1).threshold_limit_type = 'A' and L_deal_detail_tbl(1).threshold_value_type = 'A')) then
               if L_af_turnover_td = 0 then
                  L_weighted_amt_per_unit := 0;
               else
                  L_weighted_amt_per_unit       := (L_actuals_td + (L_forecast_turnover_td * I_amt_per_unit))/L_af_turnover_td;
               end if;

               if L_af_trend_turnover_td = 0 then
                  L_weighted_amt_per_unit_trend := 0;
               else
                  L_weighted_amt_per_unit_trend := (L_actuals_td + (L_forecast_turnover_td_trend * I_amt_per_unit))/L_af_trend_turnover_td;
               end if; 
            else
               L_weighted_amt_per_unit       := I_amt_per_unit;
               L_weighted_amt_per_unit_trend := I_amt_per_unit;
            end if;
            if L_deal_head_tbl(1).status = 'W' then
      -- Calculate the budget income
      -- For bill-back rebate deals need to pass in the total period turnover to date, else
      -- the turnover for the period is used.
               L_budget_income_calc := 0.00;
               if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                   L_budget_income_calc,
                                                   I_deal_id,
                                                   I_deal_detail_id,
                                                   L_deal_head_tbl(1).threshold_limit_type,
                                                   L_deal_detail_tbl(1).threshold_value_type,
                                                   L_deal_head_tbl(1).rebate_calc_type,
                                                   L_deal_detail_tbl(1).calc_to_zero_ind,
                                                   L_deal_head_tbl(1).deal_income_calculation,
                                                   L_deal_head_tbl(1).rebate_ind,
                                                   all_period_tbl(rec).budget_turnover,
                                                   L_ptd_budget_turnover,
                                                   I_amt_per_unit,
                                                   I_amt_per_unit) = FALSE then
                  return FALSE;
               end if;
            end if;
            if L_deal_head_tbl(1).status = 'A' then
      -- Calculate the actual forecast turnover income
      -- For bill-back rebate deals need to pass in the total period turnover to date, else the turnover for the period is used.
               L_af_income_calc := 0.00;
               if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                   L_af_income_calc,
                                                   I_deal_id,
                                                   I_deal_detail_id,
                                                   L_deal_head_tbl(1).threshold_limit_type,
                                                   L_deal_detail_tbl(1).threshold_value_type,
                                                   L_deal_head_tbl(1).rebate_calc_type,
                                                   L_deal_detail_tbl(1).calc_to_zero_ind,
                                                   L_deal_head_tbl(1).deal_income_calculation,
                                                   L_deal_head_tbl(1).rebate_ind,
                                                   all_period_tbl(rec).af_turnover,
                                                   L_ptd_af_turnover,
                                                   I_amt_per_unit,
                                                   L_weighted_amt_per_unit) = FALSE then
                  return FALSE;
               end if;
      -- Calculate the actual forecast trend income
      -- For bill-back rebate deals need to pass in the total period turnover to date, else the turnover for the period is used.
               L_af_trend_income_calc := 0.00;
               if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                   L_af_trend_income_calc,
                                                   I_deal_id,
                                                   I_deal_detail_id,
                                                   L_deal_head_tbl(1).threshold_limit_type,
                                                   L_deal_detail_tbl(1).threshold_value_type,
                                                   L_deal_head_tbl(1).rebate_calc_type,
                                                   L_deal_detail_tbl(1).calc_to_zero_ind,
                                                   L_deal_head_tbl(1).deal_income_calculation,
                                                   L_deal_head_tbl(1).rebate_ind,
                                                   all_period_tbl(rec).af_trend_turnover,
                                                   L_ptd_af_trend_turnover,
                                                   I_amt_per_unit,
                                                   L_weighted_amt_per_unit_trend) = FALSE then
                  return FALSE;
               end if;
            end if;
      -- For bill-back rebate deals need to minus the discounts already applied, else the income is the calculated discount.
            if L_deal_head_tbl(1).status = 'W' then
               L_budget_income_calc := ROUND(L_budget_income_calc,4);
               if L_deal_head_tbl(1).rebate_ind = 'Y' then
                  L_budget_income      := L_budget_income_calc - L_prev_budget_income;
                  L_prev_budget_income := L_prev_budget_income + L_budget_income;
               else
                  L_budget_income :=  L_budget_income_calc;
               end if;
            else
                L_af_income_calc       := ROUND(L_af_income_calc,4);
                L_af_trend_income_calc := ROUND(L_af_trend_income_calc,4);
                if L_deal_head_tbl(1).rebate_ind = 'Y' then
                   L_af_income      := L_af_income_calc - L_prev_af_income;
                   L_prev_af_income := L_prev_af_income + L_af_income;
                   
                   L_af_trend_income      := L_af_trend_income_calc - L_prev_af_trend_income;
                   L_prev_af_trend_income := L_prev_af_trend_income + L_af_trend_income;
                else
                   L_af_income       := L_af_income_calc;
                   L_af_trend_income := L_af_trend_income_calc;
                end if;
            end if;
         end if;
         if L_deal_head_tbl(1).status = 'W' then
            update deal_actuals_forecast
               set budget_income = L_budget_income
             where deal_id             = I_deal_id
               and deal_detail_id      = I_deal_detail_id
               and actual_forecast_ind = 'F'
               and reporting_date      = to_date(all_period_tbl(rec).reporting_date, 'YYYYMMDD');
         else
            update deal_actuals_forecast
               set actual_forecast_income       = L_af_income,
                   actual_forecast_trend_income = L_af_trend_income
             where deal_id             = I_deal_id
               and deal_detail_id      = I_deal_detail_id
               and actual_forecast_ind = 'F'
               and reporting_date      = to_date(all_period_tbl(rec).reporting_date, 'YYYYMMDD');
         end if;
      end loop;
   end if;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END FORECAST_INCOME_CALC;
----------------------------------------------------------------------------------------------------------------
FUNCTION DEAL_TO_DATE_CALCS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(100) := 'DEAL_INCOME_SQL.DEAL_TO_DATE_CALCS';
   L_deal_detail_id            DEAL_DETAIL.DEAL_DETAIL_ID%TYPE;
   L_daf_totals_tbl            daf_totals_tbl;
   L_growth_rate_to_date       NUMBER(20,4);

   cursor C_GET_DEAL_COMPONENTS is
      select deal_detail_id
        from deal_detail
      where deal_id = I_deal_id;

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   -- Retrieve the summed actual totals for all deal components.

   if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message,
                                                     L_daf_totals_tbl,
                                                     I_deal_id,
                                                     -1,
                                                     'A') = FALSE then  
      return FALSE;
   end if;

   if L_daf_totals_tbl(1).budget_turnover = 0.0 then
      L_growth_rate_to_date := 0.0;
   else
      L_growth_rate_to_date := ((L_daf_totals_tbl(1).actual_forecast_turnover/L_daf_totals_tbl(1).budget_turnover) - 1) * 100;
   end if;
   update deal_head
      set growth_rate_to_date          = L_growth_rate_to_date,
          turnover_to_date             = L_daf_totals_tbl(1).actual_forecast_turnover,
          actual_monies_earned_to_date = L_daf_totals_tbl(1).actual_income
    where deal_id = I_deal_id;

   for rec in C_GET_DEAL_COMPONENTS
   loop
      L_deal_detail_id := rec.deal_detail_id;
      if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message,
                                                        L_daf_totals_tbl,
                                                        I_deal_id,
                                                        L_deal_detail_id,
                                                        'A') = FALSE then  
         return FALSE;
      end if;
      if L_daf_totals_tbl(1).budget_turnover = 0.0 then
         L_growth_rate_to_date := 0.0;
      else
         L_growth_rate_to_date := ((L_daf_totals_tbl(1).actual_forecast_turnover/L_daf_totals_tbl(1).budget_turnover) - 1) * 100;
      end if;
      update deal_detail
         set growth_rate_to_date          = L_growth_rate_to_date,
             turnover_to_date             = L_daf_totals_tbl(1).actual_forecast_turnover,
             actual_monies_earned_to_date = L_daf_totals_tbl(1).actual_income
       where deal_id = I_deal_id
         and deal_detail_id = L_deal_detail_id;
   end loop;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
   return FALSE;
END DEAL_TO_DATE_CALCS;
----------------------------------------------------------------------------------------------------------------
FUNCTION CALCULATE_INCOME(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_income                   IN OUT   DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE,
                          I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                          I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                          I_threshold_limit_type     IN       DEAL_HEAD.THRESHOLD_LIMIT_TYPE%TYPE,
                          I_threshold_value_type     IN       DEAL_DETAIL.THRESHOLD_VALUE_TYPE%TYPE,
                          I_rebate_calc_type         IN       DEAL_HEAD.REBATE_CALC_TYPE%TYPE,
                          I_calc_to_zero_ind         IN       DEAL_DETAIL.CALC_TO_ZERO_IND%TYPE,
                          I_deal_income_calculation  IN       DEAL_HEAD.DEAL_INCOME_CALCULATION%TYPE,
                          I_rebate_ind               IN       DEAL_HEAD.REBATE_IND%TYPE,
                          I_period_threshold_value   IN       DEAL_THRESHOLD.UPPER_LIMIT%TYPE,
                          I_threshold_value          IN       DEAL_THRESHOLD.UPPER_LIMIT%TYPE,
                          I_amt_per_unit             IN       DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE,
                          I_weighted_amt_per_unit    IN       DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE)


RETURN BOOLEAN IS
   L_program                              VARCHAR2(100) := 'DEAL_INCOME_SQL.CALCULATE_INCOME';
   L_threshold_count                      NUMBER(4)     := 0;
   L_thresholds_tbl                       deal_threshold_tbl;
   L_thresholds_rec                       deal_threshold_rec;
   L_prev_deal_id                         DEAL_HEAD.DEAL_ID%TYPE;
   L_prev_deal_detail_id                  DEAL_DETAIL.DEAL_DETAIL_ID%TYPE;
   L_income                               DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE := 0.00;
   L_threshold_portion                    DEAL_THRESHOLD.UPPER_LIMIT%TYPE;
   Ld_threshold_value                     DEAL_THRESHOLD.UPPER_LIMIT%TYPE;
   L_lower_limit                          DEAL_THRESHOLD.LOWER_LIMIT%TYPE         := 0.00;
   L_upper_limit                          DEAL_THRESHOLD.UPPER_LIMIT%TYPE         := 0.00;
   L_value                                DEAL_THRESHOLD.UPPER_LIMIT%TYPE         := 0.00;
   L_max_thresh_upper_limit               DEAL_THRESHOLD.UPPER_LIMIT%TYPE         := 0.00;
   L_total_ind                            DEAL_THRESHOLD.TOTAL_IND%TYPE;
   L_billing_type                         DEAL_HEAD.BILLING_TYPE%TYPE;
   L_amt_per_unit                         DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE := I_amt_per_unit;
   L_weighted_amt_per_unit                DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE := I_weighted_amt_per_unit;
   L_threshold_value                      DEAL_THRESHOLD.UPPER_LIMIT%TYPE         := I_threshold_value;
   L_period_threshold_value               DEAL_THRESHOLD.UPPER_LIMIT%TYPE         := I_period_threshold_value;

   cursor C_DEAL_BILL_TYPE is
      select billing_type
        from deal_head
       where deal_id = I_deal_id;

BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_amt_per_unit is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_amt_per_unit',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_threshold_limit_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_threshold_limit_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_threshold_value_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_threshold_value_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_DEAL_BILL_TYPE;
   fetch C_DEAL_BILL_TYPE into L_billing_type;
   close C_DEAL_BILL_TYPE;

   if I_deal_id = -1 then
      L_prev_deal_id := NULL;
      L_prev_deal_detail_id := NULL;
      if L_thresholds_tbl is not null then
         L_thresholds_tbl.delete();
         L_threshold_count := 0;
      end if;
      return TRUE;
   end if;

   if L_billing_type <> 'VFM' and L_billing_type <> 'VFP' then
      if I_rebate_calc_type <> 'L' and I_rebate_calc_type <> 'S' then
         O_error_message := SQL_LIB.CREATE_MSG('UNKNOWN_REBATE_CALC_TYPE',
                                               I_rebate_calc_type,
                                               I_deal_id,
                                               I_deal_detail_id);
         return FALSE;
      end if;
   end if;

   -- Only use the converted amount per unit in income calculation for deals that require a conversion,
   -- else always set the conversion factor to 1.
   if I_amt_per_unit = 0.0 or
      (I_threshold_limit_type = 'Q' and I_threshold_value_type = 'A') or
      (I_threshold_limit_type = 'A' and I_threshold_value_type = 'P') then
      L_amt_per_unit := 1;
      L_weighted_amt_per_unit := 1;
   end if;

   -- If the deal component has changed then get the deal thresholds.
   if I_deal_id <> nvl(L_prev_deal_id,-1) or I_deal_detail_id <> nvl(L_prev_deal_detail_id,-1) then
   -- Set the previous variables
      L_prev_deal_id := I_deal_id;
      L_prev_deal_detail_id := I_deal_detail_id;
   -- Free up the previous threshold array now weve finished with it
      if L_thresholds_tbl is not null then
         L_thresholds_tbl.delete();
         L_threshold_count := 0;
      end if;
   -- Retrieve and store the deal thresholds in a local array
      if DEAL_INCOME_SQL.GET_DEAL_THRESHOLDS(O_error_message,
                                             L_thresholds_tbl,
                                             L_threshold_count,
                                             I_deal_id,
                                             I_deal_detail_id) = FALSE then
         return FALSE;
      end if;
   end if;
   if I_rebate_calc_type = 'L' then
      if L_threshold_count > 0 then
         L_max_thresh_upper_limit := L_thresholds_tbl(L_threshold_count).upper_limit;
      end if;
   -- If total turnover > max threshold upper limit value then need to set the total turnover = max threshold
   -- upper limit value to ensure the maximum discount is applied.
      if I_threshold_value > L_max_thresh_upper_limit then
         L_threshold_value := L_max_thresh_upper_limit;
      end if;
      if I_period_threshold_value > L_max_thresh_upper_limit then
         L_period_threshold_value := L_max_thresh_upper_limit;
      end if;
   -- Loop through the threshold bands
      if L_thresholds_tbl is not null and L_thresholds_tbl.count > 0 then
         for rec in L_thresholds_tbl.first..L_thresholds_tbl.last 
           loop
               L_lower_limit := L_thresholds_tbl(rec).lower_limit;
               L_upper_limit := L_thresholds_tbl(rec).upper_limit;
               L_value       := L_thresholds_tbl(rec).value;
               L_total_ind   := L_thresholds_tbl(rec).total_ind;

        -- If the value falls into the threshold then use the discount value to calculate the income.
               if L_threshold_value >= L_lower_limit and L_threshold_value <= L_upper_limit then
        -- If the calc to zero ind is set to N then deduct the first thresholds lower limit from the threshold value before performing the calculation.
                  Ld_threshold_value := L_threshold_value;
                  if I_calc_to_zero_ind = 'N' then
                     Ld_threshold_value := Ld_threshold_value - L_thresholds_tbl(rec).lower_limit;
                     L_period_threshold_value := L_period_threshold_value - L_thresholds_tbl(rec).lower_limit;
                  end if;
                  if I_threshold_value_type = 'P' then
                     L_income := Ld_threshold_value * (L_value/100) * L_weighted_amt_per_unit;
                  else
        -- Calculate amount off unit as threshold value multiplied by turnover for the period multiplied by the converted amount off per unit.
                      if L_total_ind = 'N' then
        -- For non-rebate deals need to use the period to date turnover else need to use the period turnover. When this function is 
        -- called for the prorated total calculation then the Ld_threshold_value and L_period_threshold_value will be the same so the 
        -- calculation will still work OK.
                         if I_rebate_ind = 'Y' then
                            L_income := L_income + (L_value * Ld_threshold_value * L_weighted_amt_per_unit);
                         else
                            L_income := L_income + (L_value * L_period_threshold_value * L_amt_per_unit);
                         end if;
                      else
                            L_income := L_income + L_value;
                      end if;
                  end if;
               end if;
           end loop;
      end if;
   elsif I_rebate_calc_type = 'S' then
    --  Loop through the thresholds that apply and calculate the discount from each.
        if L_thresholds_tbl is not null and L_thresholds_tbl.count > 0 then
           for rec in L_thresholds_tbl.first..L_thresholds_tbl.last
              loop
                 if L_thresholds_tbl(rec).lower_limit <= I_threshold_value then
                    L_lower_limit := L_thresholds_tbl(rec).lower_limit;
                    L_upper_limit := L_thresholds_tbl(rec).upper_limit;
                    L_value       := L_thresholds_tbl(rec).value;
                    L_total_ind   := L_thresholds_tbl(rec).total_ind;
        -- If this is the first threshold and the calc to zero ind is set then adjust the lower limit to zero.
                    if I_calc_to_zero_ind = 'Y' and rec = L_thresholds_tbl.first then
                       L_lower_limit := 0;
                    end if;
        --  Calculate the amount of the threshold that applies.
                    if L_upper_limit >= I_threshold_value then
                       L_threshold_portion := I_threshold_value - L_lower_limit;
                    else
                       L_threshold_portion := L_upper_limit - L_lower_limit;
                    end if;
                    if I_threshold_value_type = 'P' then
                       L_income := L_income + (L_threshold_portion * (L_value/100) * L_weighted_amt_per_unit);
                -- Calculate amount off unit as threshold portion value multiplied by threshold value multiplied by the amount per unit.
                    else
                       if L_total_ind = 'N' then
                          L_income := L_income + (L_threshold_portion * L_value * L_weighted_amt_per_unit);
                       else
                          L_income := L_income + L_value;
                       end if;
                    end if;
                 end if;
           end loop;
        end if;
   end if;
   O_income := L_income;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
   return FALSE;
END CALCULATE_INCOME;
----------------------------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_THRESHOLDS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_thresholds           IN OUT   DEAL_THRESHOLD_TBL,
                             O_threshold_count      IN OUT   DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE,
                             I_deal_id              IN       DEAL_HEAD.DEAL_ID%TYPE,
                             I_deal_detail_id       IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN IS
   L_program                              VARCHAR2(100) := 'DEAL_INCOME_SQL.GET_DEAL_THRESHOLDS';

   cursor C_THRESH_COUNT is
      select count(*)
        from deal_threshold
       where deal_id         = i_deal_id
         and deal_detail_id  = I_deal_detail_id;

   cursor C_THRESHOLDS is
      SELECT lower_limit,
             upper_limit,
             NVL(total_ind,'N'),
             value
        FROM deal_threshold
       WHERE deal_id         = I_deal_id
         AND deal_detail_id  = I_deal_detail_id
    ORDER BY lower_limit;

BEGIN
   O_threshold_count := 0;
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_deal_detail_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_THRESH_COUNT;
   fetch C_THRESH_COUNT into O_threshold_count;
   close C_THRESH_COUNT;

   if O_threshold_count > 0 then
      open C_THRESHOLDS;
      fetch C_THRESHOLDS BULK COLLECT into O_thresholds;
      close C_THRESHOLDS;
   end if;

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
   return FALSE;
END GET_DEAL_THRESHOLDS;
----------------------------------------------------------------------------------------------------------------
-- Below code is commented because it is not tested and not been called by any of the screen though it is called via batch and
-- same logic is written in dealinclib.pc file.
/*FUNCTION ACTUAL_INCOME_CALC(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_actual_income            IN OUT   DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE,
                            I_deal_id                  IN       DEAL_HEAD.DEAL_ID%TYPE,
                            I_deal_detail_id           IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                            I_threshold_limit_type     IN       DEAL_HEAD.THRESHOLD_LIMIT_TYPE%TYPE,
                            I_threshold_value_type     IN       DEAL_DETAIL.THRESHOLD_VALUE_TYPE%TYPE,
                            I_rebate_calc_type         IN       DEAL_HEAD.REBATE_CALC_TYPE%TYPE,
                            I_calc_to_zero_ind         IN       DEAL_DETAIL.CALC_TO_ZERO_IND%TYPE,
                            I_total_actual_fixed_ind   IN       DEAL_DETAIL.TOTAL_ACTUAL_FIXED_IND%TYPE,
                            I_deal_income_calculation  IN       DEAL_HEAD.DEAL_INCOME_CALCULATION%TYPE,
                            I_actual_turnover_units    IN       DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_UNITS%TYPE,
                            I_actual_turnover_revenue  IN       DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_REVENUE%TYPE,
                            I_act_for_turnover_total   IN       GTT_DEALINC_DEALS.ACT_FOR_TURNOVER_TOTAL%TYPE,
                            I_rebate_ind               IN       DEAL_HEAD.REBATE_IND%TYPE,
                            I_final_period_ind         IN       VARCHAR2,
                            I_reporting_date           IN       DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE,
                            I_order_no                 IN       DEAL_ACTUALS_ITEM_LOC.ORDER_NO%TYPE,
                            I_item                     IN       DEAL_ACTUALS_ITEM_LOC.ITEM%TYPE,
                            I_loc_type                 IN       DEAL_ACTUALS_ITEM_LOC.LOC_TYPE%TYPE,
                            I_location                 IN       DEAL_ACTUALS_ITEM_LOC.LOCATION%TYPE,
                            I_curr_forecast_turnover   IN       DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE)
RETURN BOOLEAN IS

   L_program                              VARCHAR2(100) := 'DEAL_INCOME_SQL.ACTUAL_INCOME_CALC';
   L_ps_calc_neg_ind                      SYSTEM_OPTIONS.CALC_NEGATIVE_INCOME%TYPE;
   L_actual_value                         DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_UNITS%TYPE           := 0.00;
   L_threshold_value                      DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.00;
   L_new_threshold_value                  DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.00;
   L_actual_income                        DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE                   := 0.00;
   L_actual_income_temp                   DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE                   := 0.00;
   L_actual_income_actuals                DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE                   := 0.00;
   L_threshold_value_actuals              DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.00;
   L_component_actuals                    NUMBER(20,4)                                               := 0.00;
   L_component_actuals_prorate            DEAL_ACTUALS_ITEM_LOC.ACTUAL_TURNOVER_UNITS%TYPE           := 0.00;
   L_component_actuals_units              NUMBER(20,4)                                               := 0.00;
   L_component_actuals_revenue            NUMBER(20,4)                                               := 0.00;
   L_amt_per_unit                         DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE                    := 0.00;
   L_weighted_amt_per_unit                DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE                    := 0.00;
   L_total_actual_units_td                DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE                    := 0.00;
   L_total_actual_revenue_td              DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE                    := 0.00;
   L_forecast_ratio                       DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE                    := 0.00;
   L_actual_turnover_diff                 DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.0;
   L_old_forecast_turnover                DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.0;
   L_new_forecast_turnover                DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.0;
   L_new_forecast_turnover_total          DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.0;
   L_total_actual_turnover_il_td          DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.0;
   L_total_actual_turnover_il_cur         DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.0;
   L_total_actual_turnover_td             DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.0;
   L_actual_turnover_itemloc              DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.0;
   L_actual_income_td                     DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE                   := 0.0;
   L_actual_income_temp2                  DEAL_ACTUALS_FORECAST.ACTUAL_INCOME%TYPE                   := 0.00;
   L_actual_turnover_pos                  DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.00;
   L_actual_turnover_neg                  DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.00;
   L_neg_turnover                         DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.00;
   L_neg_turnover_actual                  DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.00;
   L_neg_turnover_rem                     DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.00;
   L_actual_turnover_prev                 DEAL_ACTUALS_FORECAST.ACTUAL_FORECAST_TURNOVER%TYPE        := 0.00;

   L_daf_totals_actuals_tbl               daf_totals_tbl;
   L_daf_totals_forecast_tbl              daf_totals_tbl;
   L_deal_detail_tbl                      deal_detail_tbl;
   L_deal_head_tbl                        deal_head_tbl;
   L_component_actuals_tbl                deal_component_actuals_tbl;
   L_turnover_itemloc_td_tbl              turnover_itemloc_td_tbl;
   L_all_periods_turnover_tbl             all_periods_turnover_tbl;
   L_daf_details_tbl                      daf_details_tbl;

   L_prev_deal_id                         DEAL_HEAD.DEAL_ID%TYPE                                     := NULL;
   L_component_actuals_count              NUMBER(20)                                                 := 0;
   L_turnover_itemloc_td_count            NUMBER(20)                                                 := 0;
   L_all_periods_turnover_count           NUMBER(20)                                                 := 0;
   L_daf_details_count                    NUMBER(20)                                                 := 0;
   L_threshold_count                      NUMBER(20)                                                 := 0;
   L_actual_income_itemloc_td             DEAL_ACTUALS_ITEM_LOC.ACTUAL_INCOME%TYPE;

   cursor C_SYS_OPT_CALC_NEG_IND is
      select so.calc_negative_income
        from system_options so;

   cursor C_GET_COMPONENT_ACTUALS_COUNT is
      select count(*)
        from (select deal_id,
                     deal_detail_id,
                     reporting_date,
                     sum(nvl(decode(I_threshold_limit_type,'Q',actual_turnover_units,'A', actual_turnover_revenue),0)),
                     sum(nvl(actual_turnover_units,0)),
                     sum(nvl(actual_turnover_revenue,0)),
                     nvl(order_no, -1)
                from deal_actuals_item_loc
               where deal_id = I_deal_id
               group by deal_id,
                     deal_detail_id,
                     reporting_date,
                     nvl(order_no, -1));

   cursor C_GET_COMPONENT_ACTUALS is
      select  to_char(t.deal_id) ,
              to_char(t.deal_detail_id) ,
              to_char(t.reporting_date, 'YYYYMMDD'),
              nvl(decode(I_threshold_limit_type,'Q',t.actual_turnover_units,'A',t.actual_turnover_revenue),0),
              t.actual_turnover_units,
              t.actual_turnover_revenue,
              nvl(t.order_no,-1)
        from
           (select dile.deal_id  deal_id ,
                   dile.deal_detail_id deal_detail_id ,
                   dile.reporting_date reporting_date,
                   sum(nvl(dile.actual_turnover_units,0)) actual_turnover_units,
                   sum(nvl(case when dh.currency_code <> vloc.currency_code then
                              currency_sql.convert_value('C',
                                                         dh.currency_code,
                                                         vloc.currency_code,
                                                         dile.actual_turnover_revenue)
                           else dile.actual_turnover_revenue
                           end,0)) actual_turnover_revenue,
                   nvl(dile.order_no,-1) order_no
              from deal_actuals_item_loc dile,
                   deal_head dh,
                   (select st.store loc,
                           st.currency_code,
                           'S' loc_type
                      from store st
                    union all
                    select wh.wh loc,
                           wh.currency_code,
                           'W' loc_type
                      from wh
                     where stockholding_ind = 'Y'
                       and finisher_ind = 'N') vloc
             where dile.deal_id = I_deal_id
               and dile.deal_id = dh.deal_id
               and dile.location  = vloc.loc
               and dile.loc_type  = vloc.loc_type
          group by dile.deal_id,
                   dile.deal_detail_id,
                   dile.reporting_date,
                   nvl(dile.order_no,-1)) t;

   cursor C_GET_TURNOVER_ITEMLOC_TD_CNT is
      select count(*)
        from (select deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location,
                     nvl(order_no,-1),
                     reporting_date,
                     nvl(sum(nvl(decode(I_threshold_limit_type,'Q',actual_turnover_units,'A', actual_turnover_revenue),0)),0)
                from deal_actuals_item_loc
               where deal_id = I_deal_id
               group by deal_id,
                     deal_detail_id,
                     item, loc_type,
                     location,
                     nvl(order_no,-1),
                     reporting_date);

   cursor C_GET_TURNOVER_ITEMLOC_TD is
      select to_char(dile.deal_id),
             to_char(dile.deal_detail_id),
             dile.item,
             dile.loc_type,
             to_char(dile.location),
             to_char(nvl(dile.order_no,-1)),
             to_char(dile.reporting_date, 'YYYYMMDD'),
             nvl(sum(nvl(decode(I_threshold_limit_type,
                                'Q',dile.actual_turnover_units,
                                'A',case when dh.currency_code <> vloc.currency_code then
                                        CURRENCY_SQL.CONVERT_VALUE('C',
                                                                   dh.currency_code,
                                                                   vloc.currency_code,
                                                                   dile.actual_turnover_revenue)
                                    else dile.actual_turnover_revenue
                                    end
                                ),0)),0)
        from deal_actuals_item_loc dile,
             deal_head dh,
             (select st.store loc,
                     st.currency_code,
                     'S' loc_type
                from store st
               union all
              select wh.wh loc,
                     wh.currency_code,
                     'W' loc_type
                from wh
               where stockholding_ind = 'Y'
                 and finisher_ind = 'N') vloc
       where dile.deal_id = I_deal_id
         and dile.deal_id = dh.deal_id
         and dile.location  = vloc.loc
         and dile.loc_type  = vloc.loc_type
       group by dile.deal_id,
                dile.deal_detail_id,
                dile.item,
                dile.loc_type,
                dile.location,
                nvl(dile.order_no, -1),
                to_char(dile.reporting_date, 'YYYYMMDD');

   cursor C_GET_PERIOD_CNT is
      select count(*)
        from (select actual_forecast_turnover,
                     actual_forecast_ind,
                     to_char(reporting_date, 'YYYYMMDD')
                from deal_actuals_forecast
               where deal_id        = I_deal_id
                 and deal_detail_id = I_deal_detail_id);

   cursor C_GET_PERIOD is
      select actual_forecast_turnover,
             actual_forecast_ind,
             to_char(reporting_date, 'YYYYMMDD')
        from deal_actuals_forecast
       where deal_id        = I_deal_id
         and deal_detail_id = I_deal_detail_id;

   cursor C_GET_ACTUAL_DIFF is
     select nvl(daf.actual_forecast_turnover,0)
            - sum(nvl(decode(I_threshold_limit_type,
                           'Q', dail.actual_turnover_units,
                           'A', case when dh.currency_code <> vloc.currency_code then
                                      CURRENCY_SQL.CONVERT_VALUE('C',
                                                                 dh.currency_code,
                                                                 vloc.currency_code,
                                                                 dail.actual_turnover_revenue)
                                 else dail.actual_turnover_revenue
                                 end
                           ),0))
       from deal_actuals_item_loc dail,
            deal_actuals_forecast daf,
            deal_head dh,
            (select st.store loc,
                    st.currency_code,
                    'S' loc_type
               from store st
             union all
             select wh.wh loc,
                    wh.currency_code,
                   'W' loc_type
               from wh
              where stockholding_ind = 'Y'
                and finisher_ind = 'N') vloc
      where dail.deal_id = I_deal_id
        and dail.deal_id = dh.deal_id
        and dail.location  = vloc.loc
        and dail.loc_type  = vloc.loc_type
        and dail.deal_detail_id = I_deal_detail_id
        and dail.reporting_date = I_reporting_date
        and daf.deal_id = dail.deal_id
        and daf.deal_detail_id = dail.deal_detail_id
        and daf.reporting_date = dail.reporting_date
      group by daf.actual_forecast_turnover;

   cursor C_GET_FUTURE_FORECASTS is
      select nvl(actual_forecast_turnover,0) actual_forecast_turnover
       from deal_actuals_forecast
      where deal_id = I_deal_id
        and deal_detail_id = I_deal_detail_id
        and reporting_date > I_reporting_date;

   cursor C_GET_TURNOVER_CUR is
      select sum(decode(I_threshold_limit_type,'Q',actual_turnover_units,'A', actual_turnover_revenue))
        from deal_actuals_item_loc
       where deal_id = I_deal_id
         and deal_detail_id = I_deal_detail_id
         and reporting_date = I_reporting_date;

   cursor C_GET_TOTAL_TURNOVER is
       select nvl(sum(decode((sign(actual_forecast_turnover)),1,actual_forecast_turnover,-1,0)),0) positive,
              nvl(sum(decode((sign(actual_forecast_turnover)),-1,abs(actual_forecast_turnover),1,0)),0) negative,
              nvl(sum(actual_income),0) income
         from deal_actuals_forecast
        where deal_id = I_deal_id
          and deal_detail_id = I_deal_detail_id
          and reporting_date < I_reporting_date;

   cursor C_GET_DAF_DETAILS_COUNT is
      select count(*)
         from (select to_char(deal_id),
                      to_char(deal_detail_id),
                      to_char(reporting_date, 'YYYYMMDD'),
                      nvl(actual_forecast_turnover,0),
                      nvl(actual_income,0)
                 from deal_actuals_forecast
                where deal_id = I_deal_id
                  and deal_detail_id = I_deal_detail_id
                  and reporting_date < I_reporting_date);

   cursor C_GET_DAF_DETAILS is
      select to_char(deal_id),
             to_char(deal_detail_id),
             to_char(reporting_date, 'YYYYMMDD'),
             nvl(actual_forecast_turnover,0),
             nvl(actual_income,0)
        from deal_actuals_forecast
       where deal_id = I_deal_id
         and deal_detail_id = I_deal_detail_id
         and reporting_date < I_reporting_date
    order by to_char(reporting_date, 'YYYYMMDD') desc;

   cursor C_GET_COMP_ACTUALS_PRORATE is
      select nvl(sum(nvl(decode(I_threshold_limit_type,'Q',actual_turnover_units,'A', actual_turnover_revenue),0)),0)
        from deal_actuals_item_loc
       where deal_id = I_deal_id
         and deal_detail_id = I_deal_detail_id
         and ((L_actual_value > 0 and (nvl(decode(I_threshold_limit_type,'Q',actual_turnover_units,'A', actual_turnover_revenue),0) > 0))
           or (L_actual_value < 0 and (nvl(decode(I_threshold_limit_type,'Q',actual_turnover_units,'A', actual_turnover_revenue),0) < 0)))
         and reporting_date = I_reporting_date;
   
BEGIN
   if I_deal_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_deal_id',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;
   if I_deal_detail_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_deal_detail_id',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;
   if I_reporting_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_reporting_date',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_item',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_loc_type',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_location',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;
   if I_threshold_limit_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_threshold_limit_type',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;
   if I_threshold_value_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_threshold_value_type',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;
   if I_rebate_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_rebate_ind',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;
   if I_final_period_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_final_period_ind',
                                                  L_program,
                                                  NULL);
      return FALSE;
   end if;
   -- Get the actual totals up to date, for use in subsequent calculations.
   if DEAL_INCOME_SQL.GET_DEAL_ACT_FCT_TTL_RPT_DT(O_error_message,
                                                  L_daf_totals_actuals_tbl,
                                                  I_deal_id,
                                                  I_deal_detail_id,
                                                  I_reporting_date) = FALSE then
      return FALSE;
   end if;
   open C_SYS_OPT_CALC_NEG_IND;
   fetch C_SYS_OPT_CALC_NEG_IND into L_ps_calc_neg_ind;
   close C_SYS_OPT_CALC_NEG_IND;

   if I_threshold_limit_type = 'Q' then
      L_actual_value := I_actual_turnover_units;
   else
      L_actual_value := I_actual_turnover_revenue;
   end if;

   if I_deal_id <> nvl(L_prev_deal_id,-1) then
   -- Set the previous variables
      L_prev_deal_id := I_deal_id;
   -- Free up the previous component_actuals.
      if L_component_actuals_tbl is not null then
         L_component_actuals_tbl.delete();
         L_component_actuals_count := 0;
      end if;
   -- Retrieve and store the component actuals in a local table type.
      open C_GET_COMPONENT_ACTUALS_COUNT;
      fetch C_GET_COMPONENT_ACTUALS_COUNT into L_component_actuals_count;
      close C_GET_COMPONENT_ACTUALS_COUNT;

      if L_component_actuals_count > 0 then
         open C_GET_COMPONENT_ACTUALS;
         fetch C_GET_COMPONENT_ACTUALS BULK COLLECT into L_component_actuals_tbl;
         close C_GET_COMPONENT_ACTUALS;
      end if;
   -- Get turnover_itemloc info.
      open C_GET_TURNOVER_ITEMLOC_TD_CNT;
      fetch C_GET_TURNOVER_ITEMLOC_TD_CNT into L_turnover_itemloc_td_count;
      close C_GET_TURNOVER_ITEMLOC_TD_CNT;

      if L_turnover_itemloc_td_count > 0 then
         open C_GET_TURNOVER_ITEMLOC_TD;
         fetch C_GET_TURNOVER_ITEMLOC_TD BULK COLLECT into L_turnover_itemloc_td_tbl;
         close C_GET_TURNOVER_ITEMLOC_TD;
      end if;
      -- Get all periods turnover info.
      if L_all_periods_turnover_tbl is not null then
         L_all_periods_turnover_tbl.delete();
         L_all_periods_turnover_count := 0;
      end if;

      open C_GET_PERIOD_CNT;
      fetch C_GET_PERIOD_CNT into L_all_periods_turnover_count;
      close C_GET_PERIOD_CNT;

      if L_all_periods_turnover_count > 0 then
         open C_GET_PERIOD;
         fetch C_GET_PERIOD BULK COLLECT into L_all_periods_turnover_tbl;
         close C_GET_PERIOD;
      end if;
   end if;

   L_total_actual_turnover_td := 0;
   L_total_actual_units_td    := 0;
   L_total_actual_revenue_td  := 0;

   if L_component_actuals_tbl is not null and L_component_actuals_tbl.count > 0 then
      for rec in L_component_actuals_tbl.first..L_component_actuals_tbl.last
      loop
         if (I_deal_id = L_component_actuals_tbl(rec).deal_id and
             I_deal_detail_id = L_component_actuals_tbl(rec).deal_detail_id and
             I_reporting_date = to_date(L_component_actuals_tbl(rec).reporting_date,'YYYYMMDD') and
             I_order_no = L_component_actuals_tbl(rec).order_no) then
             L_component_actuals         := L_component_actuals_tbl(rec).component_actuals;
             L_component_actuals_units   := L_component_actuals_tbl(rec).component_actuals_unit;
             L_component_actuals_revenue := L_component_actuals_tbl(rec).component_actuals_revenue;
             EXIT;
         end if;
      end loop;
   end if;
   if L_component_actuals_tbl is not null and L_component_actuals_tbl.count > 0 then
      for rec in L_component_actuals_tbl.first..L_component_actuals_tbl.last
      loop
         if (I_deal_id = L_component_actuals_tbl(rec).deal_id and
             I_deal_detail_id = L_component_actuals_tbl(rec).deal_detail_id) then
             if (I_reporting_date >= to_date(L_component_actuals_tbl(rec).reporting_date,'YYYYMMDD')) then
                 L_total_actual_turnover_td := L_total_actual_turnover_td + L_component_actuals_tbl(rec).component_actuals;
                 L_total_actual_units_td    := L_total_actual_units_td + L_component_actuals_tbl(rec).component_actuals_unit;
                 L_total_actual_revenue_td  := L_total_actual_revenue_td + L_component_actuals_tbl(rec).component_actuals_revenue;
             end if;
         end if;
      end loop;
   end if;

   L_total_actual_turnover_il_td  := 0;
   L_total_actual_turnover_il_cur := 0;

   if L_turnover_itemloc_td_tbl is not null and L_turnover_itemloc_td_tbl.count > 0 then
      for rec in L_turnover_itemloc_td_tbl.first..L_turnover_itemloc_td_tbl.last
      loop
         if (I_deal_id = L_turnover_itemloc_td_tbl(rec).deal_id and 
             I_deal_detail_id = L_turnover_itemloc_td_tbl(rec).deal_detail_id and
             I_item = L_turnover_itemloc_td_tbl(rec).item and
             I_loc_type = L_turnover_itemloc_td_tbl(rec).loc_type and
             I_location = L_turnover_itemloc_td_tbl(rec).location) then
             if I_reporting_date > to_date(L_turnover_itemloc_td_tbl(rec).reporting_date,'YYYYMMDD') then
                L_total_actual_turnover_il_td := L_total_actual_turnover_il_td + L_turnover_itemloc_td_tbl(rec).total_actual_turnover_itemloc;
             end if;
             if (I_reporting_date = to_date(L_turnover_itemloc_td_tbl(rec).reporting_date,'YYYYMMDD') and
                 I_order_no = L_turnover_itemloc_td_tbl(rec).order_no) then
                 L_total_actual_turnover_il_td := L_total_actual_turnover_il_td + L_turnover_itemloc_td_tbl(rec).total_actual_turnover_itemloc;
                 L_total_actual_turnover_il_cur := L_total_actual_turnover_il_cur + L_turnover_itemloc_td_tbl(rec).total_actual_turnover_itemloc;
             end if;
         end if;
      end loop;
   end if;
   -- Get item loc actual income for all previous reporting periods.
   if I_rebate_ind = 'Y' and I_deal_income_calculation = 'P' then
      if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_ITEM_LOC_TOTAL(O_error_message,
                                                        L_actual_income_itemloc_td,
                                                        I_deal_id,
                                                        I_deal_detail_id,
                                                        I_item,
                                                        I_loc_type,
                                                        I_location,
                                                        I_threshold_limit_type,
                                                        I_reporting_date) = FALSE then
          return FALSE;
      end if;
   end if;
   -- If the calculation type is pro-rated using forecast then get the forecast total for all Forecasts apart from the current one, 
   -- and use this value to determine the total deal income, for Actuals Earned then the value from the actuals row is used in the income calculation.

   if I_deal_income_calculation = 'P' then
      L_threshold_value := I_act_for_turnover_total;
      if I_total_actual_fixed_ind = 'N' then
         L_threshold_value := L_threshold_value + L_component_actuals;
      end if;
   -- Need to calculate a weighted amount per unit ratio to date.
      if ((I_threshold_limit_type = 'Q' and I_threshold_value_type = 'P') or
          (I_threshold_limit_type = 'A' and I_threshold_value_type = 'A')) then
        -- Get the forecast totals
          if DEAL_INCOME_SQL.GET_DEAL_ACTUAL_FORECAST_TOTAL(O_error_message,
                                                            L_daf_totals_forecast_tbl,
                                                            I_deal_id,
                                                            I_deal_detail_id,
                                                            'F') = FALSE then  
             return FALSE;
          end if;
          if DEAL_INCOME_SQL.GET_DEAL_DETAIL_ROW(O_error_message,
                                                 L_deal_detail_tbl,
                                                 I_deal_id,
                                                 I_deal_detail_id) = FALSE then
              return FALSE;
          end if;
        -- Workaround to enable the weighted amt per unit ratio calculation to work correctly
        -- by calculating the new forecast turnovers once this period's total actuals have
        -- been applied. This is done in dealfct.pc but this information is also needed here
        -- in order to calculate the correct weighted amt per unit ratio.
        -- Only need to perform the workaround for fixed deals as the future forecast turnovers get updated
        -- by dealfct to try and keep the total fixed. deals that are not fixed do not get their
        -- future turnover's updated.
          if I_total_actual_fixed_ind = 'Y' then
             open C_GET_ACTUAL_DIFF;
             fetch C_GET_ACTUAL_DIFF into L_actual_turnover_diff;
             close C_GET_ACTUAL_DIFF;

        -- Now loop through each future forecast and apply pro-rate out the difference to each.
        -- The reason we process each forecast period seperatley and not as a forecast total is because of the
        -- special case where a recalculated forecast period could result in the total turnover ( actuals
        -- and forecasts ) being greater than the forecasted total, in this case, the forecast period
        -- will be set to zero for that period. Using totalling logic wold not pick this up.
             for rec in C_GET_FUTURE_FORECASTS
             loop
                L_old_forecast_turnover := rec.actual_forecast_turnover;
        -- The new forecast is calculated by prorating the difference between this period's actual and forecast
        -- turnover over the current turnover and adding it to the current turnover.
                if (L_daf_totals_forecast_tbl(1).actual_forecast_turnover - I_curr_forecast_turnover) = 0 then
                    L_new_forecast_turnover := L_old_forecast_turnover;
                else
                    L_new_forecast_turnover := L_old_forecast_turnover + (L_actual_turnover_diff * (L_old_forecast_turnover/(L_daf_totals_forecast_tbl(1).actual_forecast_turnover - I_curr_forecast_turnover)));
                end if;
                if L_new_forecast_turnover < 0.0 then
                   L_new_forecast_turnover := 0.0;
                end if;
                L_new_forecast_turnover_total := L_new_forecast_turnover_total + L_new_forecast_turnover;
             end loop;
          else
              L_new_forecast_turnover_total := L_daf_totals_forecast_tbl(1).actual_forecast_turnover - I_curr_forecast_turnover;
          end if; -- End fixed_ind = 'Y'
                  --  End of workaround
          L_new_threshold_value := L_threshold_value;
        -- If the total actuals received so far are greater than the turnover total, then set the
        -- turnover total to the sum of the acuals, regardless if it is fixed or not.
          if ((L_new_forecast_turnover_total + L_component_actuals + L_daf_totals_actuals_tbl(1).actual_forecast_turnover) > L_new_threshold_value) then
             L_new_threshold_value := L_new_forecast_turnover_total + L_component_actuals + L_daf_totals_actuals_tbl(1).actual_forecast_turnover;
          end if;
          if I_threshold_limit_type = 'Q' and I_threshold_value_type = 'P' then
             if L_deal_detail_tbl(1).total_actual_forecast_turnover = 0 then
                L_forecast_ratio := 0;
             else
                L_forecast_ratio := L_deal_detail_tbl(1).total_forecast_revenue/L_deal_detail_tbl(1).total_actual_forecast_turnover;
             end if;
             if L_new_threshold_value = 0 then
                L_amt_per_unit := 0;
             else
                L_amt_per_unit := (L_total_actual_revenue_td +(L_new_forecast_turnover_total * L_forecast_ratio))/L_new_threshold_value;
             end if;
          else
             if L_deal_detail_tbl(1).total_actual_forecast_turnover = 0 then
                L_forecast_ratio := 0;
             else
                L_forecast_ratio := L_deal_detail_tbl(1).total_forecast_units/L_deal_detail_tbl(1).total_actual_forecast_turnover;
             end if;
             if L_new_threshold_value = 0 then
                L_amt_per_unit := 0;
             else
                L_amt_per_unit := (L_total_actual_units_td +(L_new_forecast_turnover_total * L_forecast_ratio))/L_new_threshold_value;
             end if;
          end if;
      else
         L_amt_per_unit := 0.0;
      end if;
   else
      if I_rebate_ind = 'Y' then
     -- Set the threshold value to the actual values to date.
         L_threshold_value := L_daf_totals_actuals_tbl(1).actual_forecast_turnover + L_component_actuals;
     -- Calculate weighted amount per unit.
         if ((I_threshold_limit_type = 'Q' and I_threshold_value_type = 'P') or
             (I_threshold_limit_type = 'A' and I_threshold_value_type = 'A')) then
             if I_threshold_limit_type = 'Q' and I_threshold_value_type = 'P' then
                if L_total_actual_units_td = 0 then
                   L_amt_per_unit := 0;
                else
                   L_amt_per_unit := L_total_actual_revenue_td/L_total_actual_units_td;
                end if;
             else
                if L_total_actual_revenue_td = 0 then
                   L_amt_per_unit := 0;
                else
                   L_amt_per_unit := L_total_actual_units_td/L_total_actual_revenue_td;
                end if;
             end if;
         else
            L_amt_per_unit := 0.0;
         end if;
      else
     -- Set the threshold value to zero, it will get set to the component actuals below.
         L_threshold_value := L_component_actuals;
     -- Calculate amount per unit.
         if I_threshold_limit_type = 'Q' and I_threshold_value_type = 'P' then
            if L_component_actuals_units = 0 then
               L_amt_per_unit := 0;
            else
               L_amt_per_unit := L_component_actuals_revenue/L_component_actuals_units;
            end if;
         elsif I_threshold_limit_type = 'A' and I_threshold_value_type = 'A' then
            if L_component_actuals_revenue = 0 then
               L_amt_per_unit := 0;
            else
               L_amt_per_unit := L_component_actuals_units/L_component_actuals_revenue;
            end if;
         else
            L_amt_per_unit := 0.0;
         end if;
      end if;
   end if;
   if I_deal_income_calculation = 'P' and I_rebate_ind = 'N' then
      if L_all_periods_turnover_tbl is not null and L_all_periods_turnover_tbl.count > 0 then
         for rec in L_all_periods_turnover_tbl.first..L_all_periods_turnover_tbl.last
         loop
            if L_all_periods_turnover_tbl(rec).actual_forecast_ind = 'F' and to_date(L_all_periods_turnover_tbl(rec).reporting_date,'YYYYMMDD') <> I_reporting_date then
               L_actual_turnover_itemloc := L_all_periods_turnover_tbl(rec).actual_forecast_turnover;
               L_actual_income_temp := 0;
               if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                   L_actual_income_temp,
                                                   I_deal_id,
                                                   I_deal_detail_id,
                                                   I_threshold_limit_type,
                                                   I_threshold_value_type,
                                                   I_rebate_calc_type,
                                                   I_calc_to_zero_ind,
                                                   I_deal_income_calculation,
                                                   I_rebate_ind,
                                                   L_actual_turnover_itemloc,
                                                   L_actual_turnover_itemloc,
                                                   L_amt_per_unit,
                                                   L_amt_per_unit) = FALSE then
                   return FALSE;
               end if;
               if L_actual_income_temp < 0 then
                  L_actual_income_temp := 0;
               end if;
               L_actual_income := L_actual_income + L_actual_income_temp;
            else
               if L_turnover_itemloc_td_tbl is not null and L_turnover_itemloc_td_tbl.count > 0 then
                  for i in L_turnover_itemloc_td_tbl.first..L_turnover_itemloc_td_tbl.last
                  loop
                      if (I_deal_id = L_turnover_itemloc_td_tbl(i).deal_id and
                          I_deal_detail_id = L_turnover_itemloc_td_tbl(i).deal_detail_id and
                          I_item = L_turnover_itemloc_td_tbl(i).item and
                          I_loc_type = L_turnover_itemloc_td_tbl(i).loc_type and
                          I_location = L_turnover_itemloc_td_tbl(i).location and
                          L_all_periods_turnover_tbl(rec).reporting_date = L_turnover_itemloc_td_tbl(i).reporting_date) then
                          L_actual_turnover_itemloc := L_turnover_itemloc_td_tbl(i).total_actual_turnover_itemloc;
                          L_actual_income_temp := 0;
                          if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                              L_actual_income_temp,
                                                              I_deal_id,
                                                              I_deal_detail_id,
                                                              I_threshold_limit_type,
                                                              I_threshold_value_type,
                                                              I_rebate_calc_type,
                                                              I_calc_to_zero_ind,
                                                              I_deal_income_calculation,
                                                              I_rebate_ind,
                                                              L_actual_turnover_itemloc,
                                                              L_actual_turnover_itemloc,
                                                              L_amt_per_unit,
                                                              L_amt_per_unit) = FALSE then
                              return FALSE;
                          end if;
                          if L_actual_income_temp < 0 then
                             L_actual_income_temp := 0;
                          end if;
                          L_actual_income := L_actual_income + L_actual_income_temp;
                      end if;
                  end loop;
               end if;
            end if;
         end loop;
      end if;
   else
   -- This function retrieves information from the deal_head table.
      if DEAL_INCOME_SQL.GET_DEAL_HEAD_ROW(O_error_message,
                                           L_deal_head_tbl,
                                           I_deal_id) = FALSE then
         return FALSE;
      end if;
   -- The cursor C_GET_TURNOVER_CUR gets the total turnover for the current reporting period.
      open C_GET_TURNOVER_CUR;
      fetch C_GET_TURNOVER_CUR into L_neg_turnover;
      close C_GET_TURNOVER_CUR;
   --Calculate negative income for Bill Back receipt based deals.
      if L_deal_head_tbl(1).billing_type = 'BB' and I_rebate_ind = 'N' and L_deal_head_tbl(1).deal_appl_timing = 'R' and L_ps_calc_neg_ind = 'Y' and L_neg_turnover < 0 then
      -- The cursor C_GET_TOTAL_TURNOVER gets the total turnover till date.
         open C_GET_TOTAL_TURNOVER;
         fetch C_GET_TOTAL_TURNOVER into L_actual_turnover_pos,
                                         L_actual_turnover_neg,
                                         L_actual_income_temp;
         close C_GET_TOTAL_TURNOVER;
         L_actual_turnover_neg := L_actual_turnover_neg + abs(L_neg_turnover);
         if L_actual_turnover_neg >= L_actual_turnover_pos then
            L_actual_income := L_actual_income_temp;
         else
            open C_GET_DAF_DETAILS_COUNT;
            fetch C_GET_DAF_DETAILS_COUNT into L_daf_details_count;
            close C_GET_DAF_DETAILS_COUNT;
            if L_daf_details_count > 0 then
               open C_GET_DAF_DETAILS;
               fetch C_GET_DAF_DETAILS BULK COLLECT into L_daf_details_tbl;
               close C_GET_DAF_DETAILS;
            end if;
            L_actual_income_temp  := 0.0;
            L_neg_turnover_actual := -L_neg_turnover;
            L_neg_turnover_rem    := L_neg_turnover_actual - L_daf_details_tbl(1).actual_turnover;
         -- This for loop contains the processing logic for negative income calculation.
            for j in L_daf_details_tbl.first..L_daf_details_tbl.last
            loop
               if (L_neg_turnover_rem = 0) or (L_neg_turnover_actual = L_daf_details_tbl(j).actual_turnover) then
                  L_actual_income_temp := L_actual_income_temp + L_daf_details_tbl(j).actual_income;
                  exit;
               elsif L_neg_turnover_rem > 0 and L_neg_turnover_actual > L_daf_details_tbl(j).actual_turnover then
                     L_actual_income_temp  := L_actual_income_temp + L_daf_details_tbl(j).actual_income;
                     L_neg_turnover_rem    := L_neg_turnover_actual - L_daf_details_tbl(j).actual_turnover;
                     L_neg_turnover_actual := L_neg_turnover_rem;
                     continue;
               else
                   L_actual_turnover_prev := L_daf_details_tbl(j).actual_turnover - L_neg_turnover_actual;
                   if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                                       L_actual_income_temp2,
                                                       I_deal_id,
                                                       I_deal_detail_id,
                                                       I_threshold_limit_type,
                                                       I_threshold_value_type,
                                                       I_rebate_calc_type,
                                                       I_calc_to_zero_ind,
                                                       I_deal_income_calculation,
                                                       I_rebate_ind,
                                                       L_actual_turnover_prev,
                                                       L_actual_turnover_prev,
                                                       L_amt_per_unit,
                                                       L_amt_per_unit) = FALSE then
                      return FALSE;
                   end if;
                   L_actual_income_temp := L_actual_income_temp + (L_daf_details_tbl(j).actual_income - L_actual_income_temp2);
                   exit;
               end if;
            end loop;
         end if;
         L_actual_income := -L_actual_income_temp;
      else
         if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                             L_actual_income,
                                             I_deal_id,
                                             I_deal_detail_id,
                                             I_threshold_limit_type,
                                             I_threshold_value_type,
                                             I_rebate_calc_type,
                                             I_calc_to_zero_ind,
                                             I_deal_income_calculation,
                                             I_rebate_ind,
                                             L_threshold_value,
                                             L_threshold_value,
                                             L_amt_per_unit,
                                             L_amt_per_unit) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   -- Set the output value.
   L_actual_income_itemloc_td := nvl(L_actual_income_itemloc_td,0);
   if I_deal_income_calculation = 'P' then
      if I_final_period_ind = 'N' then
    -- Multiply the total income for the component (ie. for all item / locations for this reporting period )
    -- by the ratio of this item/loc turnover to date to the total turnover to date.
    -- This is a prorated deal, therefore L_actual_income was calculated using all previous actuals,
    -- so subtract the previous income to get the correct income for this period.
         if L_threshold_value = 0 then
            O_actual_income := -L_actual_income_itemloc_td;
         else
            L_actual_income := (L_actual_income *(L_total_actual_turnover_il_td/L_threshold_value));
            L_actual_income := ROUND(L_actual_income,4);
            O_actual_income := L_actual_income - L_actual_income_itemloc_td;
         end if;
      else
    -- Need to calculate the income on the total turnover of the actuals, including this final period's actuals,
    -- For certain limit type, value type combinations ( Q/P and A/A ) We also need to construct a weighted
    -- amt_per_unit ratio to pass into the calculate_income() function
    -- to cater for different amount per unit ratios over the deal's periods.
         L_threshold_value_actuals := L_component_actuals + L_daf_totals_actuals_tbl(1).actual_forecast_turnover;
    -- Calculate weighted amount per unit
         if ((I_threshold_limit_type = 'Q' and I_threshold_value_type = 'P') or
             (I_threshold_limit_type = 'A' and I_threshold_value_type = 'A')) then
             if I_threshold_limit_type = 'Q' and I_threshold_value_type = 'P' then
                if L_total_actual_units_td = 0 then
                   L_weighted_amt_per_unit := 0;
                else
                   L_weighted_amt_per_unit := L_total_actual_revenue_td/L_total_actual_units_td;
                end if;
                if L_component_actuals_units = 0 then
                   L_amt_per_unit := 0;
                else
                   L_amt_per_unit := L_component_actuals_revenue/L_component_actuals_units;
                end if;
             else
                if L_total_actual_revenue_td = 0 then
                   L_weighted_amt_per_unit := 0;
                else
                   L_weighted_amt_per_unit := L_total_actual_units_td/L_total_actual_revenue_td;
                end if;
                if L_component_actuals_revenue = 0 then
                    L_amt_per_unit := 0;
                else
                     L_amt_per_unit := L_component_actuals_units/L_component_actuals_revenue;
                end if;
             end if;
         else
             L_amt_per_unit := 0.0;
             L_weighted_amt_per_unit := 0.0;
         end if;
         if DEAL_INCOME_SQL.CALCULATE_INCOME(O_error_message,
                                             L_actual_income_actuals,
                                             I_deal_id,
                                             I_deal_detail_id,
                                             I_threshold_limit_type,
                                             I_threshold_value_type,
                                             I_rebate_calc_type,
                                             I_calc_to_zero_ind,
                                             I_deal_income_calculation,
                                             I_rebate_ind,
                                             L_threshold_value_actuals,
                                             L_threshold_value_actuals,
                                             L_amt_per_unit,
                                             L_weighted_amt_per_unit) = FALSE then
            return FALSE;
         end if;
      -- Multiply the total income for the component (ie. for all item / locations for this reporting period )
      -- by the ratio of this item/loc turnover to date to the total turnover to date.
      -- This is a prorated deal, therefore L_actual_income was calculated using all previous actuals,
      -- so subtract the previous income to get the correct income for this period.
         if L_total_actual_turnover_td = 0 then
            O_actual_income := -L_actual_income_itemloc_td;
         else
            L_actual_income := (L_actual_income_actuals * (L_total_actual_turnover_il_td/L_total_actual_turnover_td));
            L_actual_income := ROUND(L_actual_income,4);
            O_actual_income := L_actual_income - L_actual_income_itemloc_td;
         end if;
      end if;
   else
      if I_rebate_ind = 'Y' then
     -- Multiply the total income for the component (ie. for all item / locations for this reporting period )
     -- by the ratio of this item/loc turnover to date to the total turnover to date.
     -- This is a rebate deal, therefore ld_actual_income was calculated using all previous actuals,
     -- so subtract the previous income to get the correct income for this period.
         if L_total_actual_turnover_td = 0 then
            O_actual_income := -L_actual_income_itemloc_td;
         else
            L_actual_income := (L_actual_income * (L_total_actual_turnover_il_td/L_total_actual_turnover_td));
            L_actual_income := ROUND(L_actual_income,4);
            O_actual_income := L_actual_income - L_actual_income_itemloc_td;
         end if;
      else
         /*
          * Multiply the total income for the component (ie. for all item / locations for this reporting period )
         * by the ratio of this item/loc turnover to the total turnover for the current period.
         * For non-rebate, actual income calculation, only current period turnover is considered.
         */

         /*
         * If the total turnover for the current reporting period is negative, then the income for the
         * positive components needs to be zero. The vice versa is also true.
         */
   /*      if ((L_component_actuals = 0) or (L_total_actual_turnover_il_cur < 0 and L_neg_turnover > 0) or (L_total_actual_turnover_il_cur > 0 and L_neg_turnover < 0)) then
             O_actual_income := 0;
         elsif (L_total_actual_turnover_td = 0 and L_neg_turnover >= 0) then
              O_actual_income := 0;
         else
            if L_deal_head_tbl(1).billing_type = 'BB' and I_rebate_ind = 'N' and L_deal_head_tbl(1).deal_appl_timing = 'R' then
         -- The cursor C_GET_COMP_ACTUALS_PRORATE prorates the total deal income between the individual components
              open C_GET_COMP_ACTUALS_PRORATE;
              fetch C_GET_COMP_ACTUALS_PRORATE into L_component_actuals_prorate;
              close C_GET_COMP_ACTUALS_PRORATE;
              if L_component_actuals_prorate <> 0 then
                 O_actual_income := (L_actual_income * (L_total_actual_turnover_il_cur/L_component_actuals_prorate));
              else
                 O_actual_income := 0;
              end if;
            else
               O_actual_income := (L_actual_income * (L_total_actual_turnover_il_cur/L_component_actuals));
            end if;
         end if;
      end if;
   end if;
   O_actual_income := ROUND(O_actual_income,4);

   return TRUE;

EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                L_program,
                                                to_char(SQLCODE));
   return FALSE;
END ACTUAL_INCOME_CALC; */
---------------------------------------------------------------------------------------------------------------
END DEAL_INCOME_SQL;
/