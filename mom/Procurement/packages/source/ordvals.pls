CREATE OR REPLACE PACKAGE ORDER_VALIDATE_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------
--Function Name: ORDLOC_WKSHT_FILTER_LIST
--Purpose      : Checks the security settings base on views,
--               and returns appropriately
--------------------------------------------------------------------
FUNCTION ORDLOC_WKSHT_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_diff          IN OUT VARCHAR2,
                                  I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function : ORDER_EXISTS
-- Purpose  : Checks if order exists on ORDHEAD table.
---------------------------------------------------------------------
FUNCTION ORDER_EXISTS(O_error_message  IN OUT VARCHAR2,
                      O_exists         IN OUT BOOLEAN,
                      I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: CHECK_DETAIL_RECS
-- Purpose      : This function indicates if detail records exist on the
--                ordcust, ordloc, ordsku, ord_matrix_wksht and
--                ordsku_wksht tables
------------------------------------------------------------------------
FUNCTION CHECK_DETAIL_RECS(O_error_message    IN OUT  VARCHAR2,
                           O_cust_details_ind IN OUT  VARCHAR2,
                           O_items_ind        IN OUT  VARCHAR2,
                           I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                           I_customer_ind     IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: CHECK_CUST_RECS
-- Purpose      : indicates if detail records exist on the ordcust table
--                for I_order_no
------------------------------------------------------------------------
FUNCTION CHECK_CUST_RECS(O_error_message    IN OUT VARCHAR2,
                         O_cust_details_ind IN OUT VARCHAR2,
                         I_order_no         IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: CHECK_ALLOC_QTY
-- Purpose      : checks if the total quantity allocated for an item is
--                either greater than the quantity ordered, and returns
--                'Y' for alloc_excess_ind, or less than the quantity
--                ordered, and returns 'L' for alloc_excess_ind.
------------------------------------------------------------------------
FUNCTION CHECK_ALLOC_QTY(O_error_message    IN OUT VARCHAR2,
                         O_alloc_excess_ind IN OUT VARCHAR2,
                         I_order_no         IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: COUNTRY_EXISTS
-- Purpose      : used to validate the existence of a country on a passed
--                in table for a passed in order
------------------------------------------------------------------------
FUNCTION COUNTRY_EXISTS(O_error_message    IN OUT VARCHAR2,
                        O_exists           IN OUT BOOLEAN,
                        I_country_id       IN     COUNTRY.COUNTRY_ID%TYPE,
                        I_table_name       IN     VARCHAR2,
                        I_order_no         IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: VALID_TO_TICKET_AT_LOC
-- Purpose      : Checks to see whether it is valid to ticket for the
--                entered purchase order at the entered location
-- Created      : FEB-99 Rebecca Dobosh
------------------------------------------------------------------------
FUNCTION VALID_TO_TICKET_AT_LOC(O_error_message    IN OUT VARCHAR2,
                                O_valid            IN OUT BOOLEAN,
                                I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                                I_location         IN     ORDLOC.LOCATION%TYPE,
                                I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: SPLIT_REF_ORDNO_EXISTS
-- Purpose      : Checks to see if the entered split reference order number exists.
------------------------------------------------------------------------
FUNCTION SPLIT_REF_ORDNO_EXISTS(O_error_message     IN OUT VARCHAR2,
                                O_exists            IN OUT BOOLEAN,
                                I_split_ref_ordno   IN     ORDHEAD.SPLIT_REF_ORDNO%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------
-- Function Name: VALID_TO_SPLIT_ORDER
-- Purpose      : Checks to see if the order is valid to split.  The following
--                orders are valid.
--                1. In worksheet status.
--                2. Not a vendor generated order.
--                3. Not associated with a contract.
--                4. Not associated with any allocations.
--                5. Does not contain any buyer packs.
--                6. Contains only one location.
--                7. Has truck splitting constraints set up for it.
------------------------------------------------------------------------
FUNCTION VALID_TO_SPLIT_ORDER(O_error_message   IN OUT VARCHAR2,
                              O_valid           IN OUT BOOLEAN,
                              I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------
-- Function Name: ALLOC_EXISTS
-- Purpose      : Checks to see if the order is associated with an allocation.
------------------------------------------------------------------------
FUNCTION ALLOC_EXISTS(O_error_message   IN OUT   VARCHAR2,
                      O_exists          IN OUT   BOOLEAN,
                      O_ASN_exists      IN OUT   BOOLEAN,
                      I_order_no        IN       ALLOC_HEADER.ORDER_NO%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------
-- Function Name: VALIDATE_SHIP_QTY
-- Purpose      : Verifies that the quantity ordered will not fall below
--                the sum of the quantity expected for all shipments
--                or appointments.
------------------------------------------------------------------------
FUNCTION VALIDATE_SHIP_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_cancellable_qty IN OUT   ORDLOC.QTY_ORDERED%TYPE,
                           I_order_no        IN       ORDLOC.ORDER_NO%TYPE,
                           I_item            IN       ORDLOC.ITEM%TYPE,
                           I_loc             IN       ORDLOC.LOCATION%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------
-- Function Name: ORDLOC_FILTER_LIST
-- Purpose  : checks to see if the # of records in V_ORDLOC is different then what's in ORDLOC
--            for a specific order.  If it is, it'll return Y, else it'll return N.
-- Calls    : <none>
-- Input Value/s: I_ord_no -- order number
-- Return Values: O_diff  -- Y if different, N if same
-- Created on   : 20-May-2003 by Joe Strano

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_ORDLOC
-- which only returns data that the user has permission to access.  
------------------------------------------------------------------------
   FUNCTION ORDLOC_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_diff         IN OUT VARCHAR2,
                               I_order_no        IN ORDHEAD.ORDER_NO%TYPE)
            RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: ALLOCDTL_FILTER_LIST
-- Purpose  : checks to see if the # of records in V_ORDLOC is different then what's in ORDLOC
--            for a specific order.  If it is, it'll return Y, else it'll return N.
-- Calls    : <none>
-- Input Value/s: I_ord_no -- order number
-- Return Values: O_diff  -- Y if different, N if same
-- Created on   : 20-May-2003 by Joe Strano

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_ALLOC_DETAIL
-- which only returns data that the user has permission to access.  
------------------------------------------------------------------------
   FUNCTION ALLOCDTL_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_diff         IN OUT VARCHAR2,
                                 I_order_no        IN ORDHEAD.ORDER_NO%TYPE)
            RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: ORDSKU_FILTER_LIST
-- Purpose  : checks to see if the # of records in V_ORDSKU is different then what's in ORDSKU
--            for a specific order.  If it is, it'll return Y, else it'll return N.
-- Calls    : <none>
-- Input Value/s: I_ord_no -- order number
-- Return Values: O_diff  -- Y if different, N if same
-- Created on   : 20-May-2003 by Joe Strano

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_ORDSKU
-- which only returns data that the user has permission to access.  
------------------------------------------------------------------------
   FUNCTION ORDSKU_FILTER_LIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_diff         IN OUT VARCHAR2,
                               I_order_no        IN ORDHEAD.ORDER_NO%TYPE)
            RETURN BOOLEAN;            
------------------------------------------------------------------------
-- Function Name: ITEM_ON_RECV_ORD
-- Purpose      : Verifies that a specified item can be reclassified
--------------------------------------------------------------------------------------------
FUNCTION ITEM_ON_RECV_ORD (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_order_flag      IN OUT   VARCHAR2,
                           I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: PARENT_DIFF_EXISTS
-- Purpose      : Verifies if the item parent has differentiators
--------------------------------------------------------------------------------------------
FUNCTION PARENT_DIFF_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist           IN OUT   BOOLEAN,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                            I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: CHECK_ORDHEAD_DATES
-- Purpose      : Check if the not before date and not after date are greater than the vdate.
--------------------------------------------------------------------------------------------
FUNCTION CHECK_ORDHEAD_DATES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function : RECEIVED_QTY_EXISTS
-- Purpose  : Checks if  received quantity exists.
--------------------------------------------------------------------------
FUNCTION RECEIVED_QTY_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_qty_received    IN OUT   VARCHAR2,
                             I_order_no        IN       ORDLOC.ORDER_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: ORDER_QTY_EXISTS
-- Purpose      : Verifies if the order has zero quantities.
--------------------------------------------------------------------------------------------
FUNCTION ORDER_QTY_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exist           IN OUT   VARCHAR2,
                          I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------

END ORDER_VALIDATE_SQL;
/