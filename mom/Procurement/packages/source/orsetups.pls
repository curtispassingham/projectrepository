CREATE OR REPLACE PACKAGE ORDER_SETUP_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
   TYPE ADD_ITEM_TO_PO_RECTYPE is RECORD(
      I_order_no               ORDHEAD.ORDER_NO%TYPE,
      I_elc_ind                SYSTEM_OPTIONS.ELC_IND%TYPE,
      I_latest_ship_days       SYSTEM_OPTIONS.LATEST_SHIP_DAYS%TYPE,
      I_ord_import_ind         ORDHEAD.IMPORT_ORDER_IND%TYPE,
      I_earliest_ship_date     ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
      I_latest_ship_date       ORDHEAD.LATEST_SHIP_DATE%TYPE,
      I_ord_currency           ORDHEAD.CURRENCY_CODE%TYPE,
      I_ord_exchg_rate         ORDHEAD.EXCHANGE_RATE%TYPE,
      I_import_country_id      ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
      I_contract_no            ORDHEAD.CONTRACT_NO%TYPE,
      I_contract_currency      CONTRACT_HEADER.CURRENCY_CODE%TYPE,
      I_pickup_loc             ORDHEAD.PICKUP_LOC%TYPE,
      I_pickup_no              ORDHEAD.PICKUP_NO%TYPE,
      I_item                   ORDSKU.ITEM%TYPE,
      I_ref_item               ORDSKU.REF_ITEM%TYPE,
      I_pack_ind               ITEM_MASTER.PACK_IND%TYPE,
      I_item_xform_ind         ITEM_MASTER.ITEM_XFORM_IND%TYPE,
      I_sellable_ind           ITEM_MASTER.SELLABLE_IND%TYPE,
      I_supplier               ORDHEAD.SUPPLIER%TYPE,
      I_supplier_currency      SUPS.CURRENCY_CODE%TYPE,
      I_origin_country_id      ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
      I_location               ORDLOC.LOCATION%TYPE,
      I_loc_type               ORDLOC.LOC_TYPE%TYPE,
      I_supp_pack_size         ORDSKU.SUPP_PACK_SIZE%TYPE,
      I_lead_time              ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
      I_act_qty                ORDLOC.QTY_ORDERED%TYPE,
      I_last_rounded_qty       ORDLOC.LAST_ROUNDED_QTY%TYPE,
      I_last_grp_rounded_qty   ORDLOC.LAST_GRP_ROUNDED_QTY%TYPE,
      I_unit_cost              ORDLOC.UNIT_COST%TYPE,
      I_supp_unit_cost         ORDLOC.UNIT_COST%TYPE,
      I_source_type            CODE_DETAIL.CODE%TYPE,
      I_non_scale_ind          ORDLOC.NON_SCALE_IND%TYPE,
      I_tsf_po_link_no         ORDLOC.TSF_PO_LINK_NO%TYPE,
      I_previous_item          ORDSKU.ITEM%TYPE,
      I_prev_pack_ind          ITEM_MASTER.PACK_IND%TYPE,
      I_calling_form           VARCHAR2(30));

   TYPE ADD_ITEM_TO_PO_TBLTYPE is TABLE of ADD_ITEM_TO_PO_RECTYPE index by PLS_INTEGER;
-------------------------------------------------------------------------------
-- Name:       LOCK_ORDHEAD
-- Purpose:    This function will be called from each of the ordering forms to
--             lock the ordhead record of the order being edited.
-------------------------------------------------------------------------------
FUNCTION LOCK_ORDHEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    APPLY_ITEM_LIST
-- Purpose: This function will add all the items in an item list belonging
--          to the order's department and supplied by the order's supplier/
--          origin country to the ordsku_wksht table.  If the SKU in the SKUlist
--          is a pack item, it will only be added if the orderable_ind field on
--          the packhead table is 'Y'.  I_dept is optional.
-------------------------------------------------------------------------------
FUNCTION APPLY_ITEM_LIST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_ordsku_exists    IN OUT   VARCHAR2,
                         O_wksht_exists     IN OUT   VARCHAR2,
                         I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                         I_skulist          IN       SKULIST_HEAD.SKULIST%TYPE,
                         I_supplier         IN       ORDHEAD.SUPPLIER%TYPE,
                         I_dept             IN       ORDHEAD.DEPT%TYPE,
                         I_wksht_qty        IN       ORDLOC_WKSHT.ACT_QTY%TYPE,
                         I_loc              IN       ORDLOC_WKSHT.LOCATION%TYPE,
                         I_loc_type         IN       ORDLOC_WKSHT.LOC_TYPE%TYPE,
                         I_origin_country   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                         I_uop_type         IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       ORDLOC_WKS_INSERT
-- Purpose:    This function centralizes the multiple inserts into ordloc_wksht
--             O_exists is set if an insert violates a unique constraint.
--
-------------------------------------------------------------------------------
FUNCTION ORDLOC_WKS_INSERT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists           IN OUT   VARCHAR2,
                           I_order            IN       ORDHEAD.ORDER_NO%TYPE,
                           I_item             IN       ORDLOC_WKSHT.ITEM%TYPE,
                           I_item_parent      IN       ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                           I_diff1            IN       ORDLOC_WKSHT.DIFF_1%TYPE,
                           I_diff2            IN       ORDLOC_WKSHT.DIFF_2%TYPE,
                           I_diff3            IN       ORDLOC_WKSHT.DIFF_3%TYPE,
                           I_diff4            IN       ORDLOC_WKSHT.DIFF_4%TYPE,
                           I_loc_type         IN       ORDLOC_WKSHT.LOC_TYPE%TYPE,
                           I_location         IN       ORDLOC_WKSHT.LOCATION%TYPE,
                           I_act_qty          IN       ORDLOC_WKSHT.ACT_QTY%TYPE,
                           I_orig_country     IN       ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE,
                           I_standard_uom     IN       ORDLOC_WKSHT.STANDARD_UOM%TYPE,
                           I_wksht_qty        IN       ORDLOC_WKSHT.WKSHT_QTY%TYPE,
                           I_uop              IN       ORDLOC_WKSHT.UOP%TYPE,
                           I_supp_pack_size   IN       ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       EXPAND_PARENT_PACK
-- Purpose:    This function will expand either a parent or a pack, deleting
--             the parent/pack record from ordsku_wksht and inserting records
--             to ordsku_wksht for all items in the parent/pack.
-------------------------------------------------------------------------------
FUNCTION EXPAND_PARENT_PACK(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                            I_item             IN       ITEM_MASTER.ITEM%TYPE,
                            I_act_qty          IN       ORDLOC_WKSHT.ACT_QTY%TYPE,
                            I_loc              IN       ITEM_LOC.LOC%TYPE,
                            I_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                            I_supplier         IN       ORDHEAD.SUPPLIER%TYPE,
                            I_origin_country   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                            I_supp_pack_size   IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                            I_uop              IN       ORDLOC_WKSHT.UOP%TYPE,
                            I_uop_type         IN       VARCHAR2,
                            I_suom             IN       ORDLOC_WKSHT.STANDARD_UOM%TYPE,
                            I_wksht_qty        IN       ORDLOC_WKSHT.WKSHT_QTY%TYPE,
                            I_override_ind     IN       VARCHAR2,
                            I_contract_no      IN       CONTRACT_HEADER.CONTRACT_NO%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       POP_ITEMS
-- Purpose:    This function will populate the ordloc and ordsku
--             tables with all fully distributed worksheet records and delete
--             them off the worksheet tables.
-------------------------------------------------------------------------------
FUNCTION POP_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                   I_supplier        IN       ORDHEAD.SUPPLIER%TYPE,
                   I_source          IN       VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       ADD_ITEM_TO_PO
-- Purpose:    This function is called by POP_ITEMS and BUYER_WORKSHEET_SQL.ADD_TO_PO.
---            It will populate the ordloc and ordsku tables with the passed in parameters.
-------------------------------------------------------------------------------
FUNCTION ADD_ITEM_TO_PO(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_recalc_hts             IN OUT   VARCHAR2,
                        I_order_no               IN       ORDHEAD.ORDER_NO%TYPE,
                        I_elc_ind                IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                        I_latest_ship_days       IN       SYSTEM_OPTIONS.LATEST_SHIP_DAYS%TYPE,
                        I_ord_import_ind         IN       ORDHEAD.IMPORT_ORDER_IND%TYPE,
                        I_earliest_ship_date     IN       ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
                        I_latest_ship_date       IN       ORDHEAD.LATEST_SHIP_DATE%TYPE,
                        I_ord_currency           IN       ORDHEAD.CURRENCY_CODE%TYPE,
                        I_ord_exchg_rate         IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                        I_import_country_id      IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                        I_contract_no            IN       ORDHEAD.CONTRACT_NO%TYPE,
                        I_contract_currency      IN       CONTRACT_HEADER.CURRENCY_CODE%TYPE,
                        I_pickup_loc             IN       ORDHEAD.PICKUP_LOC%TYPE,
                        I_pickup_no              IN       ORDHEAD.PICKUP_NO%TYPE,
                        I_item                   IN       ORDSKU.ITEM%TYPE,
                        I_ref_item               IN       ORDSKU.REF_ITEM%TYPE,
                        I_pack_ind               IN       ITEM_MASTER.PACK_IND%TYPE,
                        I_supplier               IN       ORDHEAD.SUPPLIER%TYPE,
                        I_supplier_currency      IN       SUPS.CURRENCY_CODE%TYPE,
                        I_origin_country_id      IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                        I_location               IN       ORDLOC.LOCATION%TYPE,
                        I_loc_type               IN       ORDLOC.LOC_TYPE%TYPE,
                        I_supp_pack_size         IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                        I_lead_time              IN       ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
                        I_act_qty                IN       ORDLOC.QTY_ORDERED%TYPE,
                        I_last_rounded_qty       IN       ORDLOC.LAST_ROUNDED_QTY%TYPE,
                        I_last_grp_rounded_qty   IN       ORDLOC.LAST_GRP_ROUNDED_QTY%TYPE,
                        I_unit_cost              IN       ORDLOC.UNIT_COST%TYPE,
                        I_supp_unit_cost         IN       ORDLOC.UNIT_COST%TYPE,
                        I_source_type            IN       CODE_DETAIL.CODE%TYPE,
                        I_non_scale_ind          IN       ORDLOC.NON_SCALE_IND%TYPE,
                        I_tsf_po_link_no         IN       ORDLOC.TSF_PO_LINK_NO%TYPE,
                        I_previous_item          IN       ORDSKU.ITEM%TYPE,
                        I_prev_pack_ind          IN       ITEM_MASTER.PACK_IND%TYPE,
                        I_calling_form           IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION ADD_ITEM_TO_PO(O_error_message        IN OUT   VARCHAR2,
                        O_recalc_hts           IN OUT   VARCHAR2,
                        I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                        I_elc_ind              IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                        I_latest_ship_days     IN       SYSTEM_OPTIONS.LATEST_SHIP_DAYS%TYPE,
                        I_ord_import_ind       IN       ORDHEAD.IMPORT_ORDER_IND%TYPE,
                        I_earliest_ship_date   IN       ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
                        I_latest_ship_date     IN       ORDHEAD.LATEST_SHIP_DATE%TYPE,
                        I_ord_currency         IN       ORDHEAD.CURRENCY_CODE%TYPE,
                        I_ord_exchg_rate       IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                        I_import_country_id    IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                        I_contract_no          IN       ORDHEAD.CONTRACT_NO%TYPE,
                        I_contract_currency    IN       CONTRACT_HEADER.CURRENCY_CODE%TYPE,
                        I_pickup_loc           IN       ORDHEAD.PICKUP_LOC%TYPE,
                        I_pickup_no            IN       ORDHEAD.PICKUP_NO%TYPE,
                        I_item                 IN       ORDSKU.ITEM%TYPE,
                        I_ref_item             IN       ORDSKU.REF_ITEM%TYPE,
                        I_pack_ind             IN       ITEM_MASTER.PACK_IND%TYPE,
                        I_item_xform_ind       IN       ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                        I_sellable_ind         IN       ITEM_MASTER.SELLABLE_IND%TYPE,
                        I_supplier             IN       ORDHEAD.SUPPLIER%TYPE,
                        I_supplier_currency    IN       SUPS.CURRENCY_CODE%TYPE,
                        I_origin_country_id    IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                        I_location             IN       ORDLOC.LOCATION%TYPE,
                        I_loc_type             IN       ORDLOC.LOC_TYPE%TYPE,
                        I_supp_pack_size       IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                        I_lead_time            IN       ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
                        I_act_qty              IN       ORDLOC.QTY_ORDERED%TYPE,
                        I_last_rounded_qty     IN       ORDLOC.LAST_ROUNDED_QTY%TYPE,
                        I_last_grp_rounded_qty IN       ORDLOC.LAST_GRP_ROUNDED_QTY%TYPE,
                        I_unit_cost            IN       ORDLOC.UNIT_COST%TYPE,
                        I_supp_unit_cost       IN       ORDLOC.UNIT_COST%TYPE,
                        I_source_type          IN       CODE_DETAIL.CODE%TYPE,
                        I_non_scale_ind        IN       ORDLOC.NON_SCALE_IND%TYPE,
                        I_tsf_po_link_no       IN       ORDLOC.TSF_PO_LINK_NO%TYPE,
                        I_previous_item        IN       ORDSKU.ITEM%TYPE,
                        I_prev_pack_ind        IN       ITEM_MASTER.PACK_IND%TYPE,
                        I_calling_form         IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION ADD_ITEM_TO_PO(O_error_message   IN OUT   VARCHAR2,
                        O_recalc_hts      IN OUT   VARCHAR2,
                        L_add_to_po_tbl   IN       ORDER_SETUP_SQL.ADD_ITEM_TO_PO_TBLTYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       LOCK_DETAIL
-- Purpose:    Locks records for the deletes performed by the
--             DELETE_ORDER function.
-------------------------------------------------------------------------------
FUNCTION LOCK_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                     I_item            IN       ITEM_MASTER.ITEM%TYPE,
                     I_location        IN       ORDLOC.LOCATION%TYPE,
                     I_calling_form    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       DELETE_ORDSKU
-- Purpose:    Deletes ordsku records if all ordloc records for a SKU have
--             been deleted.
-------------------------------------------------------------------------------
FUNCTION DELETE_ORDSKU(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       DELETE_ORDSKU_ITEM
-- Purpose:    Deletes ordsku records for a given order and item combination.
-------------------------------------------------------------------------------
FUNCTION DELETE_ORDSKU_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                            I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       DELETE_ORDER
-- Purpose:    if I_calling_form = 'ordloc' then
--               delete unused cost components off ordsku_exp.
--               if I_loc_type = 'W' then delete from alloc_header along with
--                 all child records on alloc_detail for the alloc_header
--                 order/SKU/location combination.
--               delete from repl_results for the order/item/locations combination
--             if I_calling_form = 'ordsku' then:
--               delete unused cost components off ordsku_hts and ordsku_hts_assess
--               tables.
--               delete from alloc_header along with
--                 all child records on alloc_detail for the alloc_header
--                 order/item/location combination.
--               delete ordloc_rev for the order/item combination.
--               delete ordloc for the order/item combination.
--               delete ordsku_discount for the order/item combination.
--               delete req_doc for the order/item combination.
--               delete timeline for the order/item combination.
--               delete repl_results for the order/item combination.
--             if I_calling_form = 'ordhead' OR 'split_po' then execute the following logic:
--               delete from ordsku_exp, ordsku_hts_assess, and ordsku_hts for the order.
--               delete from alloc_header and alloc_detail for the order.
--               delete from ordloc_wksht for the order.
--               delete from ordloc_rev for the order.
--               delete from ordloc for the order.
--               delete from ordsku_discount for the order.
--               delete from ordsku_wksht for the order.
--               delete from ordsku_rev for the order.
--               delete from ordsku for the order and order/item combinations.
--               delete from ordcust for the order.
--               delete from timeline for the order.
--               delete from ordhead_rev for the order.
--               delete from ord_xdock_temp for the order.
--               delete req_doc for the order and order/item combinations.
--               delete repl_results for the order.
--               delete ord_inv_mgmt for the order.
--             if I_calling_form = 'temp' then
--               delete from ord_xdock_temp for the order.
--             if I_calling_form = 'preissue' then
--               delete from ord_preissue for the order.
--             if I_calling_form = 'split_po' then
--               delete from ordhead for the order.
-------------------------------------------------------------------------------
FUNCTION DELETE_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                      I_item            IN       ITEM_MASTER.ITEM%TYPE,
                      I_location        IN       ORDLOC.LOCATION%TYPE,
                      I_loc_type        IN       ORDLOC.LOC_TYPE%TYPE,
                      I_calling_form    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       DELETE_TEMP_TABLES
-- Purpose:    This function will delete all of the temporary ordering tables.
-------------------------------------------------------------------------------
FUNCTION DELETE_TEMP_TABLES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       EXPLODE_GRADE
-- Purpose:    This function will divide the value in ordloc_wksht.act_qty
--             among all stores in the store grade, inserting new records
--             into ordloc_wksht for all valid stores in the store_grade.
-------------------------------------------------------------------------------
FUNCTION EXPLODE_GRADE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_validation_flag IN OUT   VARCHAR2,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                       I_where_clause    IN       FILTER_TEMP.WHERE_CLAUSE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       CREATE_FROM_EXISTING
-- Purpose:    This function will create a new order_no (O_order_no) copying all
--             data from the ordhead, ordsku, ordloc, ord_inv_mgmt, work orders
--             allocations, customer order info and order LC tables where order_no =
--             I_order_no.  If the system elc indicator is 'Y', ordsku_exp
--             records are then inserted.  If the order's import
--             indicator is 'Y', ordsku_hts and ordsku_hts_assess records are
--             inserted.
-------------------------------------------------------------------------------
FUNCTION CREATE_FROM_EXISTING(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_order_no        IN OUT   ORDHEAD.ORDER_NO%TYPE,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       GET_CUST_LOCS
-- Purpose:    This function will return the store or allocation wh/store
--             value(s) for a customer order.
-------------------------------------------------------------------------------
FUNCTION GET_CUST_LOCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_cust_store      IN OUT   ORDLOC.LOCATION%TYPE,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       CALC_ORDER_QTYS
-- Purpose:    This function will get the total calc_qty and total act_qty
--             off of the ordloc_wksht form and pass them back.
-------------------------------------------------------------------------------
FUNCTION CALC_ORDER_QTYS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_calc_qty        IN OUT   ORDLOC_WKSHT.CALC_QTY%TYPE,
                         O_act_qty         IN OUT   ORDLOC_WKSHT.ACT_QTY%TYPE,
                         O_wksht_qty       IN OUT   ORDLOC_WKSHT.WKSHT_QTY%TYPE,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                         I_where_clause    IN       FILTER_TEMP.WHERE_CLAUSE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       CHECK_FILTER_CRITERIA
-- Purpose:    This function will check to see if the item exists on the filtered
--             order.
-------------------------------------------------------------------------------
FUNCTION CHECK_FILTER_CRITERIA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_filter_ind      IN OUT   VARCHAR2,
                               I_order_no        IN       ORDLOC_WKSHT.ORDER_NO%TYPE,
                               I_item            IN       ORDLOC_WKSHT.ITEM%TYPE,
                               I_where_clause    IN       FILTER_TEMP.WHERE_CLAUSE%TYPE,
                               I_location        IN       ORDLOC_WKSHT.LOCATION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       UPDATE_SHIP_DATES
-- Purpose:    This function will update the earliest ship date and the latest
--             ship date at the PO header level based on the dates at the PO
--             item level.
-------------------------------------------------------------------------------
FUNCTION UPDATE_SHIP_DATES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       UPDATE_IMPORT_COUNTRY
-- Purpose:    This function will update the import country at the PO Header 
--             level based on the first store location which is chosen in the
--             Location distribution scree.
-------------------------------------------------------------------------------
FUNCTION UPDATE_IMPORT_COUNTRY(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                               I_import_country_id   IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:       APPLY_CHANGES
-- Purpose:    This function applies changes made to the country of origin or
--             supplier pack size for an item in the Order Distribution Worksheet form across
--             all the distribution records for an item.
-------------------------------------------------------------------------------
FUNCTION APPLY_CHANGES(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                       I_item_parent      IN       ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                       I_diff1            IN       ORDLOC_WKSHT.DIFF_1%TYPE,
                       I_diff2            IN       ORDLOC_WKSHT.DIFF_2%TYPE,
                       I_diff3            IN       ORDLOC_WKSHT.DIFF_3%TYPE,
                       I_diff4            IN       ORDLOC_WKSHT.DIFF_4%TYPE,
                       I_item             IN       ORDLOC_WKSHT.ITEM%TYPE,
                       I_origin_country   IN       ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE,
                       I_supplier         IN       ORDHEAD.SUPPLIER%TYPE DEFAULT NULL,
                       I_override         IN       VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Name:       POP_CHECK_CHILD
-- Purpose:    This function will be called after distributing by a diff.  It will check if the
--             combination of the item_parent and diff references an item.  If so, it will check if
--             that item exists for the given supp/country on the order.  If it's not an existing item
--             or the item doesn't exist for the supplier/country, a record will be inserted onto a
--             temp table
-------------------------------------------------------------------------------
FUNCTION POP_CHECK_CHILD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_rejected   IN OUT   BOOLEAN,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function: DELETE_COMPS
-- Purpose : This function will delete Expenses, HTS codes, and Assessments
--           from ORDSKU_EXP, ORDSKU_HTS, and ORDSKU_HTS_ASSESS. Depending on
--           the values passed in.  If just the order_no is passed in, and all
--           other input parameters are NULL, all records will be deleted from
--           the above mentioned tables for the Order no.  If 'E' is passed in
--           the I_comp_type parameter, records will only be deleted from the
--           Expense table.  If 'A' is passed in then HTS and Assessment records
--           will be deleted.  If more information is passed in, it will limit
--           which records are deleted.  If 'S' is passed in the I_origin
--           parameter, then only the Expense records with origin = 'S'
--           ('System Generated'), will be deleted.  If NULL, all will be
--           deleted.
-------------------------------------------------------------------------------
FUNCTION DELETE_COMPS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                      I_item            IN       ITEM_MASTER.ITEM%TYPE,
                      I_location        IN       COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                      I_loc_type        IN       COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                      I_comp_type       IN       ELC_COMP.COMP_TYPE%TYPE,
                      I_origin          IN       ORDLOC_EXP.ORIGIN%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function: DELETE_COMPS
-- Purpose : This function will delete Expenses, HTS codes, and Assessments
--           from ORDSKU_EXP, ORDSKU_HTS, and ORDSKU_HTS_ASSESS. Depending on
--           the values passed in.  If just the order_no is passed in, and all
--           other input parameters are NULL, all records will be deleted from
--           the above mentioned tables for the Order no.  If 'E' is passed in
--           the I_comp_type parameter, records will only be deleted from the
--           Expense table.  If 'A' is passed in then HTS and Assessment records
--           will be deleted.  If more information is passed in, it will limit
--           which records are deleted.  If 'S' is passed in the I_origin
--           parameter, then only the Expense records with origin = 'S'
--           ('System Generated'), will be deleted.  If NULL, all will be
--           deleted.  If a calling form is passed in and it is order redistribution
--           and the order is approved then ordsku_hts records will not be deleted.
-------------------------------------------------------------------------------
FUNCTION DELETE_COMPS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                      I_item             IN       ITEM_MASTER.ITEM%TYPE,
                      I_component_item   IN       ITEM_MASTER.ITEM%TYPE,
                      I_location         IN       COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                      I_loc_type         IN       COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                      I_comp_type        IN       ELC_COMP.COMP_TYPE%TYPE,
                      I_origin           IN       ORDLOC_EXP.ORIGIN%TYPE,
                      I_calling_form     IN       VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Function Name: EXPLODE_NON_SCALING
--Purpose:       This function will explode quantity out out to ordloc and alloc detail
--               given the sku and order number
--Calls:         ORDSKU.FMB
--Created:       26-OCT-99, by Matthew Brown
--------------------------------------------------------------------------------------------
FUNCTION EXPLODE_NON_SCALING(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item            IN       ORDLOC.ITEM%TYPE,
                             I_order_no        IN       ORDLOC.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Name:       UPDATE_ORDER_UNIT_COST
-- Purpose:    Updates the orders unit_cost for sku or pack_no passed in.
--             Convert suppliers unit_cost to order unit_cost.
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_UNIT_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_unit_cost       IN OUT   ORDLOC.UNIT_COST%TYPE,
                                I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                                I_supplier        IN       ORDHEAD.SUPPLIER%TYPE,
                                I_item_no         IN       ORDLOC.ITEM%TYPE,
                                I_location        IN       ORDLOC.LOCATION%TYPE,
                                I_cost_source     IN       ORDLOC.COST_SOURCE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Function Name: DEFAULT_ORDHEAD_DOCS
--Purpose:       This function will default documents to the Order Header level.
--------------------------------------------------------------------------------------------
FUNCTION DEFAULT_ORDHEAD_DOCS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Function Name: DEFAULT_ORDSKU_DOCS
--Purpose:       This function will default documents to the Order Item level.
--------------------------------------------------------------------------------------------
FUNCTION DEFAULT_ORDSKU_DOCS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no            IN       ORDSKU.ORDER_NO%TYPE,
                             I_item                IN       ORDSKU.ITEM%TYPE,
                             I_origin_country_id   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                             I_elc_ind             IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                             I_default_hts         IN       VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Name:       GET_LC_REF_ID
-- Purpose:    To retrieve the Letter of Credit Reference ID associated with
--             the order.  This value may be NULL.
--------------------------------------------------------------------------------------------
FUNCTION GET_LC_REF_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_lc_ref_id       IN OUT   ORDLC.LC_REF_ID%TYPE,
                       I_order_no        IN       ORDLC.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Function Name: DELETE_WO_WKSHT
--Purpose:       This function will delete worksheet records off the ordloc_wksht
--               table once a work order has been created for them.
--Created:       28-JUL-98
--------------------------------------------------------------------------------------------
FUNCTION DELETE_WO_WKSHT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN       ORDLOC_WKSHT.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Function Name: ADD_ITEM
--Purpose:       This function will check to see if the sku, order-no record exists in the
--               ordsku table, it not it will insert a new record into the ordsku table.
--Created:       07-JUL-99
--------------------------------------------------------------------------------------------
FUNCTION ADD_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                  I_item            IN       ORDSKU.ITEM%TYPE,
                  I_ref_item        IN       ORDSKU.REF_ITEM%TYPE,
                  I_supplier        IN       ORDHEAD.SUPPLIER%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: DEFAULT_ORDER_INV_MGMT_INFO
--Purpose:       This function is called to create default order inventory management information
--               for the passed in order.
--               When an order/supplier/location (and potentially dept) combination is passed in
--               this function will call the SUP_INV_MGMT_SQL.GET_INV_MGMT_DATA function to
--               fetch the appropriate supplier inventory management information and write this
--               data to the order inventory management table for the order.
--               I_currency_code and I_exchange_rate can be passed in as NULL.
--               I_order_type can be 'O' (purchase order), 'CB' (contract order batch), and 'CO'
--               (contract order online).  If the order is a contract order, scaling  and
--               and truck splitting information is not defaulted to the order since scaling
--               and truck splitting functionality is not available for contract orders.
--Created:       04-AUG-99
--Modified:      15-MAR-01
--------------------------------------------------------------------------------------------
FUNCTION DEFAULT_ORDER_INV_MGMT_INFO(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_order_no            IN       ORDSKU.ORDER_NO%TYPE,
                                     I_supplier            IN       SUPS.SUPPLIER%TYPE,
                                     I_dept                IN       DEPS.DEPT%TYPE,
                                     I_location            IN       WH.WH%TYPE,
                                     I_currency_code       IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                     I_exchange_rate       IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                                     I_order_type          IN       VARCHAR2,
                                     I_order_approve_ind   IN       ORD_INV_MGMT.ORD_APPROVE_IND%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: GET_CONTRACT_ORDER_INFO
--Purpose:       This new function will retrieve the following information from CONTRACT_HEADER
--               by passing in the contract number:  supplier, department, currency, origin country,
--               start date and end date.
--Created:       07-JUL-99
--------------------------------------------------------------------------------------------
FUNCTION GET_CONTRACT_ORDER_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_supplier        IN OUT   CONTRACT_HEADER.SUPPLIER%TYPE,
                                 O_dept            IN OUT   CONTRACT_HEADER.DEPT%TYPE,
                                 O_currency_code   IN OUT   CONTRACT_HEADER.CURRENCY_CODE%TYPE,
                                 O_country_id      IN OUT   CONTRACT_HEADER.COUNTRY_ID%TYPE,
                                 O_start_date      IN OUT   CONTRACT_HEADER.START_DATE%TYPE,
                                 O_end_date        IN OUT   CONTRACT_HEADER.END_DATE%TYPE,
                                 I_contract_no     IN       CONTRACT_HEADER.CONTRACT_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: DELETE_SINGLE_CONTAINER_ITEM
--Purpose      : This function is an overload of the DELETE_CONTAINER_ITEMS function and
--               deletes all the items for a given order, or a single item, item/loc order
--               record depending on the values passed in.  valid values for delete_ind are
--               'L' - ordloc, 'S' - ordsku, 'B' - both
-----------------------------------------------------------------------------------------------
FUNCTION DELETE_SINGLE_CONTAINER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_order           IN       ORDLOC.ORDER_NO%TYPE,
                                      I_contents_item   IN       ITEM_MASTER.ITEM%TYPE,
                                      I_location        IN       ORDLOC.LOCATION%TYPE,
                                      I_delete_ind      IN       VARCHAR2)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: POP_SINGLE_CONTAINER_ITEM
--Purpose      : This function is an overload of the POP_CONTAINER_ITEM.  the function will
--               insert/update container for a single contents item, or for all contents on the
--               order.
-----------------------------------------------------------------------------------------------
FUNCTION POP_SINGLE_CONTAINER_ITEM(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_order_no          IN       ORDHEAD.ORDER_NO%TYPE,
                                   I_supplier          IN       SUPS.SUPPLIER%TYPE,
                                   I_location          IN       ORDLOC.LOCATION%TYPE,
                                   I_contents_item     IN       ITEM_MASTER.ITEM%TYPE,
                                   I_origin_country_id IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: POP_CONTAINER_ITEM
--Purpose      : This function will insert/update all container items on a given
--               order by calling POP_SINGLE_CONTAINER_ITEM with the correct inputs
-----------------------------------------------------------------------------------------------
FUNCTION POP_CONTAINER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                            I_supplier        IN       SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: DELETE_CONTAINER_ITEMS
--Purpose      : This function will delete all container items on a given order by calling
--               DELETE_SINGLE_CONTAINER_ITEM with the correct inputs.
-----------------------------------------------------------------------------------------------
FUNCTION DELETE_CONTAINER_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order           IN       ORDLOC.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--Function Name: CANCEL_CONTAINER_ITEM
--Purpose      : This function will update the qty_cancelled, qty_ordered, cancel_code,
--               and cancel_id/date values to be consistent with their associated contents items
-----------------------------------------------------------------------------------------------
FUNCTION CANCEL_CONTAINER_ITEMS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no         IN       ORDLOC.ORDER_NO%TYPE,
                                I_container_item   IN       ORDLOC.ITEM%TYPE,
                                I_location         IN       ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:       COUNT_SKULIST_RECORDS
-- Purpose:    This function will be called from each of the ordering forms to
--             count all items in SKULIST_DETAIL table that does not exist in
--             ORDLOC_WKSHT table for a certain order number.
-----------------------------------------------------------------------------------------------
FUNCTION COUNT_SKULIST_RECORDS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_skulist_records   IN OUT   NUMBER,
                               I_skulist           IN       SKULIST_DETAIL.SKULIST%TYPE,
                               I_order_no          IN       ORDHEAD.ORDER_NO%TYPE,
                               I_location          IN       ORDHEAD.LOCATION%type)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:       UPDATE_LOCATION
-- Purpose:    Updates the Location and Location Type in the ORDLOC_WKSHT table.
-----------------------------------------------------------------------------------------------
FUNCTION UPDATE_LOCATION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN ;
-----------------------------------------------------------------------------------------------
-- Name:        DSDORD_DUP_CHECK
-- Purpose:     This function will check if passed item already exists in ORDAUTO_TEMP table 
--              for the given order.
-----------------------------------------------------------------------------------------------
FUNCTION DSDORD_DUP_CHECK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_item_exists     IN OUT   BOOLEAN,
                          I_order_no        IN       ORDAUTO_TEMP.ORDER_NO%TYPE,
                          I_item            IN       ORDAUTO_TEMP.ITEM%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:        ORDAUTO_TEMP_DELETE
-- Purpose:     This new function will delete records from ORDAUTO_TEMP based on order number.
-----------------------------------------------------------------------------------------------
FUNCTION ORDAUTO_TEMP_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN       ORDAUTO_TEMP.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:        INSERT_CONTAINER_ITEM
-- Purpose:     This new function will insert the container item of the content item into the
--              ORDAUTO_TEMP table.
-----------------------------------------------------------------------------------------------
FUNCTION INSERT_CONTAINER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no        IN       ORDAUTO_TEMP.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:        GET_SCALE_AIP_ORD_IND
-- Purpose:     This new function will get the scale_aip_ord_ind value from the SUPS table.
-----------------------------------------------------------------------------------------------
FUNCTION GET_SCALE_AIP_ORD_IND(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_scale_aip_ord_ind   IN OUT   SUPS.SCALE_AIP_ORDERS%TYPE,
                               I_supplier            IN       SUPS.SCALE_AIP_ORDERS%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:        FASHPACK_INS_ITEM_LOC
-- Purpose:     This new function will insert ITEM-LOC data for pack items created via fashpack.
-----------------------------------------------------------------------------------------------
FUNCTION FASHPACK_INS_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Name:       CHECK_RANGED_LOC
-- Purpose:    This function will check if the items are ranged to the locations added to
--             an order. This function will return an indicator which would determine if there
--             is at least one non-ranged or incidentally ranged location in the order.
------------------------------------------------------------------------------------------------
FUNCTION CHECK_RANGED_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_ranged_loc      IN OUT   VARCHAR2,
                          I_order_no        IN       ORDLOC_WKSHT.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Name:       UPDATE_RANGED_IND
-- Purpose:    This function will set ITEM_LOC.RANGED_IND = 'Y for the item/location
--             combination that is intentionally ranged.
------------------------------------------------------------------------------------------------
FUNCTION UPDATE_RANGED_IND(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no        IN       ORDLOC.ORDER_NO%TYPE,
                           I_item            IN       ITEM_LOC.ITEM%TYPE,
                           I_location        IN       ITEM_LOC.LOC%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Name:       POP_ORDCUST_DETAIL
-- Purpose:    This function will populate the ORDCUST_DETAIL table with the additional
--             details of the item in the Customer Order.
------------------------------------------------------------------------------------------------
FUNCTION POP_ORDCUST_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDLOC.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Name:       DELETE_ORD_INV_MGMT
-- Purpose:    This function will delete records in the ORD_INV_MGMT table
--             if the order has been changed from a Non-CO to a CO PO.
------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORD_INV_MGMT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN       ORDLOC.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Name:       CHECK_DIRECT_SHIP_ITEM
-- Purpose:    This function will check if all items in a Drop Ship CO PO supports direct delivery.
------------------------------------------------------------------------------------------------
FUNCTION CHECK_DIRECT_SHIP_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                                I_supplier        IN       ORDHEAD.SUPPLIER%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Name:       DELETE_ORDCUST
-- Purpose:    This function will delete ordcust records.
------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORDCUST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no        IN       ORDSKU.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Name:       UPD_ORDLOC_WKSHT_CTRY
-- Purpose:    This function will update origin_country_id in ORDLOC_WKSHT.
--             for the passed in item and order.
------------------------------------------------------------------------------------------------
FUNCTION UPD_ORDLOC_WKSHT_CTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no          IN ORDSKU.ORDER_NO%TYPE,
                               I_item              IN ORDSKU.ITEM%TYPE,
                               I_origin_country_id IN ordloc_wksht.origin_country_id%type)
  RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Name:       DEL_ORDLOC_DISCOUNT
-- Purpose:    This function will delete ordloc_discount records
--             for the passed in item and order.
------------------------------------------------------------------------------------------------
FUNCTION DEL_ORDLOC_DISCOUNT(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no          IN ORDSKU.ORDER_NO%TYPE,
                             I_item              IN ORDSKU.ITEM%TYPE)
  RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Name:       REDIST_CANCEL
-- Purpose:    This function will cancel ordloc records for previously approve orders
--             when redistributing an order
------------------------------------------------------------------------------------------------
FUNCTION REDIST_CANCEL(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no          IN     ORDLOC.ORDER_NO%TYPE)
  RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
END ORDER_SETUP_SQL;
/
