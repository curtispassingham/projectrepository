CREATE OR REPLACE PACKAGE BODY FUTURE_COST_ROLLFWD_SQL AS
----------------------------------------------------------------------------------------
-- Package Variables
----------------------------------------------------------------------------------------
   LP_system_optns     SYSTEM_OPTIONS%ROWTYPE;
   LP_vdate            DATE   := get_vdate;
----------------------------------------------------------------------------------------
$if $$UTPLSQL=FALSE $then
   --------------------------------------------------------------------------------------
   -- Function Name: HELP_ROLL_DEAL_1
   -- Purpose      : Step one in deal calculations.
   --                Populates to tables to assist with deal calculations.
   --                - future_cost_buyget_help_gtt is used to help with buy-get deals by getting
   --               the buy item and the get item into one row.
   --                - future_cost_working_gtt is used to get all necessary deal attributes
   --                in one spot for use by HELP_ROLL_DEAL_2 and HELP_ROLL_DEAL_3.
   --------------------------------------------------------------------------------------
   FUNCTION HELP_ROLL_DEAL_1(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: HELP_ROLL_DEAL_2
   -- Purpose      : Step two in deal calculations, add additional values to the
   --                future_cost_working_gtt table
   --------------------------------------------------------------------------------------
   FUNCTION HELP_ROLL_DEAL_2(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: HELP_ROLL_DEAL_3
   -- Purpose      : Step three in deal calculations, add additional values to the
   --                future_cost_working_gtt table
   --------------------------------------------------------------------------------------
   FUNCTION HELP_ROLL_DEAL_3(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: MERGE_DEAL
   -- Purpose      : Step four in deal calculations, add additional values to the
   --                future_cost_working_gtt table.  It also updates future_cost_gtt
   --                with the results of the calculations.
   --------------------------------------------------------------------------------------
   FUNCTION MERGE_DEAL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   ----------------------------------------------------------------------------------------
   -- Function Name: SETUP_WHOLESALE_FRANCHISE
   -- Purpose      : Get wholesale / franchise stores that depend on whs in the
   --                future_cost_thread table
   ----------------------------------------------------------------------------------------
   FUNCTION SETUP_WHOLESALE_FRANCHISE(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                      I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: PUSH_TEMPL_DATES
   -- Purpose      :
   --------------------------------------------------------------------------------------
   FUNCTION PUSH_TEMPL_DATES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: SYNC_TEMPL_ID
   -- Purpose      : Updates the templ_id in future_cost_gtt for relatoinship records
   --------------------------------------------------------------------------------------
   FUNCTION SYNC_TEMPL_ID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: SYNC_PASSTHRU_PCT
   -- Purpose      :
   --------------------------------------------------------------------------------------
   FUNCTION SYNC_PASSTHRU_PCT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: ROLL_ELC_TEXPZ
   -- Purpose      : Updates elc_amount and pricing_cost on future_cost_gtt for all
   --                TEXPZ type elc components that apply.
   --------------------------------------------------------------------------------------
   FUNCTION ROLL_ELC_TEXPZ(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: ROLL_ELC_TEXPC
   -- Purpose      : Updates elc_amount and pricing_cost on future_cost_gtt for all
   --                TEXPC type elc components that apply.
   --------------------------------------------------------------------------------------
   FUNCTION ROLL_ELC_TEXPC(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: ROLL_ELC_TDTY
   -- Purpose      : Updates elc_amount and pricing_cost on future_cost_gtt for all
   --                TDTY type elc components that apply.
   --------------------------------------------------------------------------------------
   FUNCTION ROLL_ELC_TDTY(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: ROLL_DEAL_PASSTHRU
   -- Purpose      : Applies the deal passthru for the wholesale and franchise
   --                stores.
   --------------------------------------------------------------------------------------
   FUNCTION ROLL_DEAL_PASSTHRU(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: ROLL_TMPL
   -- Purpose      : Applies the wf cost templates for the wholesale and franchise
   --                stores.
   --------------------------------------------------------------------------------------
   FUNCTION ROLL_TMPL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                      I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: HELP_ROLL_TMPL
   -- Purpose      : Helper function to apply the wf cost templated.
   --------------------------------------------------------------------------------------
   FUNCTION HELP_ROLL_TMPL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --------------------------------------------------------------------------------------
   -- Function Name: MERGE_TMPL
   -- Purpose      : Updates the pricing_cost for wf store with wf cost templates.
   --------------------------------------------------------------------------------------
   FUNCTION MERGE_TMPL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
$end
----------------------------------------------------------------------------------------

FUNCTION ROLL_DEAL_PASSTHRU(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                            I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

   L_return NUMBER(1);

BEGIN

   merge into future_cost_gtt fc
   using
    ( select inner.item,
             inner.location,
             inner.supplier,
             inner.origin_country_id,
             inner.active_date,
             inner.base_cost,
             (inner.base_cost - ((NVL(inner.base_cost,0) - NVL(inner.net_cost,0)) * NVL(inner.passthru_pct,0)/100)) net_cost
        from (select distinct gtt.item,
                     gtt.location,
                     gtt.supplier,
                     gtt.origin_country_id,
                     gtt.active_date,
                     gtt.base_cost,
                     gtt.net_cost,
                     gtt.passthru_pct
                from future_cost_gtt gtt
               where gtt.store_type ='F') inner) use_this
   on (    fc.item                 = use_this.item
       and fc.supplier             = use_this.supplier
       and fc.origin_country_id    = use_this.origin_country_id
       and fc.location             = use_this.location
       and fc.active_date          = use_this.active_date)
   when matched then
   update
      set fc.base_cost = NVL(use_this.base_cost,0),
          fc.net_cost = NVL(use_this.net_cost,0),
          fc.net_net_cost = NVL(use_this.net_cost,0),
          fc.dead_net_net_cost = NVL(use_this.net_cost,0),
          fc.acquisition_cost = NVL(use_this.net_cost,0),
          fc.pricing_cost = NVL(use_this.net_cost,0);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.ROLL_DEAL_PASSTHRU',
                                            to_char(SQLCODE));
      return 0;
END ROLL_DEAL_PASSTHRU;
----------------------------------------------------------------------------------------
FUNCTION SEED_WF_STORES(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id   IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id               IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER is
   
   cursor C_EVENT is
      select event_type
        from cost_event
       where cost_event_process_id = I_cost_event_process_id;

   L_event_type       COST_EVENT.EVENT_TYPE%TYPE;

BEGIN

   open C_EVENT;
   fetch C_EVENT into L_event_type;
   close C_EVENT;
 

   if L_event_type = 'SC' or L_event_type = 'DP' or L_event_type = 'T' or L_event_type = 'TR' or L_event_type = 'CC' or L_event_type = 'CL' or L_event_type = 'D' or L_event_type = 'ELC' or L_event_type = 'R' or L_event_type = 'OH'then 
      insert into future_cost_gtt  (item,
                                    supplier,
                                    origin_country_id,
                                    location,
                                    loc_type,
                                    active_date,
                                    base_cost,
                                    net_cost,
                                    net_net_cost,
                                    dead_net_net_cost,
                                    pricing_cost,
                                    calc_date,
                                    start_ind,
                                    acquisition_cost,
                                    elc_amt,
                                    primary_supp_country_ind,
                                    currency_code,
                                    division,
                                    group_no,
                                    dept,
                                    class,
                                    subclass,
                                    item_grandparent,
                                    item_parent,
                                    diff_1,
                                    diff_2,
                                    diff_3,
                                    diff_4,
                                    chain,
                                    area,
                                    region,
                                    district,
                                    supp_hier_lvl_1,
                                    supp_hier_lvl_2,
                                    supp_hier_lvl_3,
                                    reclass_no,
                                    cost_change,
                                    simple_pack_ind,
                                    primary_cost_pack,
                                    primary_cost_pack_qty,
                                    store_type,
                                    costing_loc,
                                    templ_id,
                                    passthru_pct,
                                    negotiated_item_cost,
                                    extended_base_cost,
                                    wac_tax,
                                    default_costing_type,
                                    processing_seq_no)
          select distinct fc.item,
                          fc.supplier,
                          fc.origin_country_id,
                          l.location,  /* franchise location */
                          iscl.loc_type,
                          fc.active_date,
                          fc.base_cost,
                          fc.net_cost,
                          fc.net_net_cost,
                          fc.dead_net_net_cost,
                          fc.pricing_cost,
                          fc.calc_date,
                          fc.start_ind,
                          DECODE(fc.default_costing_type, 'BC', fc.base_cost, 'NIC', fc.negotiated_item_cost, 'EBC', fc.extended_base_cost),
                          wf.elc_amt,
                          fc.primary_supp_country_ind,
                          fc.currency_code,
                          fc.division,
                          fc.group_no,
                          fc.dept,
                          fc.class,
                          fc.subclass,
                          fc.item_grandparent,
                          fc.item_parent,
                          fc.diff_1,
                          fc.diff_2,
                          fc.diff_3,
                          fc.diff_4,
                          sh.chain,
                          sh.area,
                          sh.region,
                          sh.district,
                          fc.supp_hier_lvl_1,
                          fc.supp_hier_lvl_2,
                          fc.supp_hier_lvl_3,
                          fc.reclass_no,
                          fc.cost_change,
                          fc.simple_pack_ind,
                          fc.primary_cost_pack,
                          fc.primary_cost_pack_qty,
                          s.store_type,
                          il.costing_loc as costing_loc,
                          NULL templ_id,
                          NULL passthru_pct,
                          fc.negotiated_item_cost,
                          fc.extended_base_cost,
                          fc.wac_tax,
                          fc.default_costing_type,
                          0 processing_seq_no
                     from item_loc il,
                          item_supp_country_loc iscl,
                          future_cost_gtt fc,
                          future_cost_gtt wf,
                          store s,
                          store_hierarchy sh,
                          (select store as location
                             from store
                            where NOT EXISTS (select 1 from future_cost_wf_helper_temp l
                                               where cost_event_process_id = I_cost_event_process_id)
                              and store_type  ='F'
                           UNION
                           select location
                             from future_cost_wf_helper_temp
                            where cost_event_process_id = I_cost_event_process_id) l
                     where fc.item                  = wf.item
                       and fc.item                  = iscl.item
                       and fc.item                  = il.item
                       and fc.supplier              = wf.supplier
                       and fc.supplier              = iscl.supplier
                       and fc.location              = il.costing_loc         /* this is checking if the cost event happening on costing location of a franchise location */
                       and fc.origin_country_id     = iscl.origin_country_id
                       and l.location               = il.loc                 
                       and l.location               = iscl.loc              /* l.location gives the franchise location */
                       and iscl.loc                 = s.store
                       and s.store                  = sh.store
                       and NOT EXISTS (select 'x'
                                         from future_cost_gtt f
                                        where f.item = fc.item
                                          and f.supplier = fc.supplier
                                          and f.origin_country_id = fc.origin_country_id
                                          and f.location = l.location
                                          and f.active_date = fc.active_date);
                    
   else
      merge into future_cost_gtt fct
         using (select /*+ ORDERED */ 
                      fc.item,
                      fc.supplier,
                      fc.origin_country_id,
                      wf.location,
                      wf.loc_type,
                      fc.active_date,
                      fc.base_cost,
                      fc.net_cost,
                      fc.net_net_cost,
                      fc.dead_net_net_cost,
                      fc.pricing_cost,
                      fc.calc_date,
                      fc.start_ind,
                      wf.acquisition_cost,
                      wf.elc_amt,
                      fc.primary_supp_country_ind,
                      fc.currency_code,
                      fc.division,
                      fc.group_no,
                      fc.dept,
                      fc.class,
                      fc.subclass,
                      fc.item_parent,
                      fc.item_grandparent,
                      fc.diff_1,
                      fc.diff_2,
                      fc.diff_3,
                      fc.diff_4,
                      wf.chain,
                      wf.area,
                      wf.region,
                      wf.district,
                      fc.supp_hier_lvl_1,
                      fc.supp_hier_lvl_2,
                      fc.supp_hier_lvl_3,
                      fc.reclass_no,
                      fc.cost_change,
                      fc.simple_pack_ind,
                      fc.primary_cost_pack,
                      fc.primary_cost_pack_qty,
                      wf.store_type,
                      wf.costing_loc as costing_loc,
                      NULL templ_id,
                      NULL passthru_pct,
                      fc.negotiated_item_cost,
                      fc.extended_base_cost,
                      fc.wac_tax,
                      fc.default_costing_type,
                      0 processing_seq_no
                 from future_cost_gtt wf,
                      future_cost fc,
                      (select store as location
                         from store
                        where NOT EXISTS (select 1 from future_cost_wf_helper_temp l
                                           where cost_event_process_id = I_cost_event_process_id)
                       UNION
                       select location
                         from future_cost_wf_helper_temp
                        where cost_event_process_id = I_cost_event_process_id) l
                where fc.loc_type              in ('S','W')            /* costing location can be both company store or wh */
                  and fc.item                  = wf.item
                  and fc.supplier              = wf.supplier
                  and fc.origin_country_id     = wf.origin_country_id
                  and wf.costing_loc           = fc.location           /* change at costing loc of a franchise store*/
                  and l.location               = wf.location
               UNION
               select distinct 
                      fc.item,
                      fc.supplier,
                      fc.origin_country_id,
                      wf.location,
                      wf.loc_type,
                      fc.active_date,
                      fc.base_cost,
                      fc.net_cost,
                      fc.net_net_cost,
                      fc.dead_net_net_cost,
                      fc.pricing_cost,
                      fc.calc_date,
                      fc.start_ind,
                      fc.acquisition_cost,
                      fc.elc_amt,
                      fc.primary_supp_country_ind,
                      fc.currency_code,
                      fc.division,
                      fc.group_no,
                      fc.dept,
                      fc.class,
                      fc.subclass,
                      fc.item_parent,
                      fc.item_grandparent,
                      fc.diff_1,
                      fc.diff_2,
                      fc.diff_3,
                      fc.diff_4,
                      wf.chain,
                      wf.area,
                      wf.region,
                      wf.district,
                      fc.supp_hier_lvl_1,
                      fc.supp_hier_lvl_2,
                      fc.supp_hier_lvl_3,
                      fc.reclass_no,
                      fc.cost_change,
                      fc.simple_pack_ind,
                      fc.primary_cost_pack,
                      fc.primary_cost_pack_qty,
                      wf.store_type,
                      wf.costing_loc as costing_loc,
                      NULL templ_id,
                      NULL passthru_pct,
                      fc.negotiated_item_cost,
                      fc.extended_base_cost,
                      fc.wac_tax,
                      fc.default_costing_type,
                      0 processing_seq_no
                 from future_cost_gtt fc,
                      future_cost_gtt wf,
                      (select store as location
                         from store
                        where NOT EXISTS (select 1 from future_cost_wf_helper_temp l
                                           where cost_event_process_id = I_cost_event_process_id)
                       UNION
                       select location
                         from future_cost_wf_helper_temp
                        where cost_event_process_id = I_cost_event_process_id) l
                where fc.loc_type              in ('S','W') /* costing location can be both company store or wh */
                  and fc.item                  = wf.item
                  and fc.supplier              = wf.supplier
                  and fc.origin_country_id     = wf.origin_country_id
                  and fc.location              = wf.costing_loc
                  and wf.active_date          != fc.active_date
                  and l.location               = wf.location) use_this
        on (    fct.item                  = use_this.item
            and fct.supplier              = use_this.supplier
            and fct.origin_country_id     = use_this.origin_country_id
            and fct.location              = use_this.location
            and fct.active_date           = use_this.active_date)
        when MATCHED then
           update
              set fct.base_cost             = use_this.base_cost,
                  fct.net_cost              = use_this.net_cost,
                  fct.net_net_cost          = use_this.net_net_cost,
                  fct.dead_net_net_cost     = use_this.dead_net_net_cost,
                  fct.pricing_cost          = use_this.pricing_cost,
                  fct.negotiated_item_cost  = use_this.negotiated_item_cost,
                  fct.extended_base_cost    = use_this.extended_base_cost,
                  fct.wac_tax               = use_this.wac_tax
        when NOT MATCHED then
           insert (item,
                   supplier,
                   origin_country_id,
                   location,
                   loc_type,
                   active_date,
                   base_cost,
                   net_cost,
                   net_net_cost,
                   dead_net_net_cost,
                   pricing_cost,
                   calc_date,
                   start_ind,
                   acquisition_cost,
                   elc_amt,
                   primary_supp_country_ind,
                   currency_code,
                   division,
                   group_no,
                   dept,
                   class,
                   subclass,
                   item_parent,
                   item_grandparent,
                   diff_1,
                   diff_2,
                   diff_3,
                   diff_4,
                   chain,
                   area,
                   region,
                   district,
                   supp_hier_lvl_1,
                   supp_hier_lvl_2,
                   supp_hier_lvl_3,
                   reclass_no,
                   cost_change,
                   simple_pack_ind,
                   primary_cost_pack,
                   primary_cost_pack_qty,
                   store_type,
                   costing_loc,
                   templ_id,
                   passthru_pct,
                   negotiated_item_cost,
                   extended_base_cost,
                   wac_tax,
                   default_costing_type,
                   processing_seq_no)
           values (use_this.item,
                   use_this.supplier,
                   use_this.origin_country_id,
                   use_this.location,
                   use_this.loc_type,
                   use_this.active_date,
                   use_this.base_cost,
                   use_this.net_cost,
                   use_this.net_net_cost,
                   use_this.dead_net_net_cost,
                   use_this.pricing_cost,
                   use_this.calc_date,
                   use_this.start_ind,
                   use_this.acquisition_cost,
                   use_this.elc_amt,
                   use_this.primary_supp_country_ind,
                   use_this.currency_code,
                   use_this.division,
                   use_this.group_no,
                   use_this.dept,
                   use_this.class,
                   use_this.subclass,
                   use_this.item_parent,
                   use_this.item_grandparent,
                   use_this.diff_1,
                   use_this.diff_2,
                   use_this.diff_3,
                   use_this.diff_4,
                   use_this.chain,
                   use_this.area,
                   use_this.region,
                   use_this.district,
                   use_this.supp_hier_lvl_1,
                   use_this.supp_hier_lvl_2,
                   use_this.supp_hier_lvl_3,
                   use_this.reclass_no,
                   use_this.cost_change,
                   use_this.simple_pack_ind,
                   use_this.primary_cost_pack,
                   use_this.primary_cost_pack_qty,
                   use_this.store_type,
                   use_this.costing_loc,
                   use_this.templ_id,
                   use_this.passthru_pct,
                   use_this.negotiated_item_cost,
                   use_this.extended_base_cost,
                   use_this.wac_tax,
                   use_this.default_costing_type,
                   use_this.processing_seq_no);
   end if;

   return 1;
   
 
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.SEED_WF_STORES',
                                            to_char(SQLCODE));
      return 0;
END SEED_WF_STORES;
----------------------------------------------------------------------------------------

FUNCTION PUSH_TEMPL_DATES(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

 

BEGIN

   
       merge into future_cost_gtt fct
       using (select * from  (select fc.item,
                          fc.supplier,
                          fc.origin_country_id,
                          fc.location,
                          fc.loc_type,
                          fc.active_date,
                          fc.base_cost,
                          fc.net_cost,
                          fc.net_net_cost,
                          fc.dead_net_net_cost,
                          fc.pricing_cost,
                          fc.calc_date,
                          fc.start_ind,
                          fc.acquisition_cost,
                          fc.elc_amt,
                          fc.primary_supp_country_ind,
                          fc.currency_code,
                          fc.division,
                          fc.group_no,
                          fc.dept,
                          fc.class,
                          fc.subclass,
                          fc.item_parent,
                          fc.item_grandparent,
                          fc.diff_1,
                          fc.diff_2,
                          fc.diff_3,
                          fc.diff_4,
                          fc.chain,
                          fc.area,
                          fc.region,
                          fc.district,
                          fc.supp_hier_lvl_1,
                          fc.supp_hier_lvl_2,
                          fc.supp_hier_lvl_3,
                          fc.reclass_no,
                          fc.cost_change,
                          fc.simple_pack_ind,
                          fc.primary_cost_pack,
                          fc.primary_cost_pack_qty,
                          fc.store_type,
                          fc.costing_loc,
                          fc.templ_id,
                          fc.passthru_pct,
                          wf.wf_date,
                          0 as processing_seq_no,
                          rank() over(partition by fc.item,
                                       fc.supplier,
                                       fc.origin_country_id,
                                       fc.location,
                                       wf.wf_date
                         order by fc.active_date desc) as rank_value
                     from future_cost_gtt fc,
                          (select dept,class,subclass,location,start_date as wf_date ,item ,templ_id from wf_cost_relationship)wf,
                          wf_cost_buildup_tmpl_head wc
                    where fc.store_type            = 'F'
                      and wc.first_applied         = 'C'
                      and wc.templ_id              = wf.templ_id
                      and wf.dept                  = fc.dept
                      and wf.class                 = fc.class
                      and wf.subclass              = fc.subclass
                      and NVL(wf.item,-1)          = fc.item
                      and wf.location              = fc.location
                      and fc.active_date          <= wf.wf_date) inner1
              where inner1.rank_value = 1) use_this
   on (    fct.item                  = use_this.item
       and fct.supplier              = use_this.supplier
       and fct.origin_country_id     = use_this.origin_country_id
       and fct.location              = use_this.location
       and fct.active_date           = use_this.wf_date)
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           acquisition_cost,
           elc_amt,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_parent,
           item_grandparent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           templ_id,
           passthru_pct,
           processing_seq_no)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.wf_date,
           use_this.base_cost,
           use_this.net_cost,
           use_this.net_net_cost,
           use_this.dead_net_net_cost,
           use_this.pricing_cost,
           use_this.calc_date,
           use_this.start_ind,
           use_this.acquisition_cost,
           use_this.elc_amt,
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_parent,
           use_this.item_grandparent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           use_this.reclass_no,
           use_this.cost_change,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           use_this.templ_id,
           use_this.passthru_pct,
           use_this.processing_seq_no);
  
 merge into future_cost_gtt fct
        using (select * from  (select fc.item,
                           fc.supplier,
                           fc.origin_country_id,
                           fc.location,
                           fc.loc_type,
                           fc.active_date,                           
                           fc.base_cost,
                           fc.net_cost,
                           fc.net_net_cost,
                           fc.dead_net_net_cost,
                           fc.pricing_cost,
                           fc.calc_date,
                           fc.start_ind,
                           fc.acquisition_cost,
                           fc.elc_amt,
                           fc.primary_supp_country_ind,
                           fc.currency_code,
                           fc.division,
                           fc.group_no,
                           fc.dept,
                           fc.class,
                           fc.subclass,
                           fc.item_parent,
                           fc.item_grandparent,
                           fc.diff_1,
                           fc.diff_2,
                           fc.diff_3,
                           fc.diff_4,
                           fc.chain,
                           fc.area,
                           fc.region,
                           fc.district,
                           fc.supp_hier_lvl_1,
                           fc.supp_hier_lvl_2,
                           fc.supp_hier_lvl_3,
                           fc.reclass_no,
                           fc.cost_change,
                           fc.simple_pack_ind,
                           fc.primary_cost_pack,
                           fc.primary_cost_pack_qty,
                           fc.store_type,
                           fc.costing_loc,
                           fc.templ_id,
                           fc.passthru_pct,
                           0 as processing_seq_no,
                           wf.wf_date wf_date,
                           rank() over(partition by fc.item,
                                        fc.supplier,
                                        fc.origin_country_id,
                                        fc.location,
                                        wf.wf_date
                          order by fc.active_date desc) as rank_value
                      from future_cost_gtt fc,
                           ( select dept,class,subclass,location,end_date + 1 as wf_date , item ,templ_id from wf_cost_relationship) wf,
                           wf_cost_buildup_tmpl_head wc
                     where fc.store_type            = 'F'
                       and wc.first_applied         = 'C'
                       and wc.templ_id              = wf.templ_id
                       and wf.dept                  = fc.dept
                       and wf.class                 = fc.class
                       and wf.subclass              = fc.subclass
                       and NVL(wf.item,-1)          = fc.item
                       and wf.location              = fc.location
                       and fc.active_date          <= wf.wf_date) inner1
               where inner1.rank_value = 1) use_this
    on (    fct.item                  = use_this.item
        and fct.supplier              = use_this.supplier
        and fct.origin_country_id     = use_this.origin_country_id
        and fct.location              = use_this.location
        and fct.active_date           = use_this.wf_date)
    when not matched then
    insert (item,
            supplier,
            origin_country_id,
            location,
            loc_type,
            active_date,
            base_cost,
            net_cost,
            net_net_cost,
            dead_net_net_cost,
            pricing_cost,
            calc_date,
            start_ind,
            acquisition_cost,
            elc_amt,
            primary_supp_country_ind,
            currency_code,
            division,
            group_no,
            dept,
            class,
            subclass,
            item_parent,
            item_grandparent,
            diff_1,
            diff_2,
            diff_3,
            diff_4,
            chain,
            area,
            region,
            district,
            supp_hier_lvl_1,
            supp_hier_lvl_2,
            supp_hier_lvl_3,
            reclass_no,
            cost_change,
            simple_pack_ind,
            primary_cost_pack,
            primary_cost_pack_qty,
            store_type,
            costing_loc,
            templ_id,
            passthru_pct,
            processing_seq_no)
    values (use_this.item,
            use_this.supplier,
            use_this.origin_country_id,
            use_this.location,
            use_this.loc_type,
            use_this.wf_date,
            use_this.base_cost,
            use_this.net_cost,
            use_this.net_net_cost,
            use_this.dead_net_net_cost,
            use_this.pricing_cost,
            use_this.calc_date,
            use_this.start_ind,
            use_this.acquisition_cost,
            use_this.elc_amt,
            use_this.primary_supp_country_ind,
            use_this.currency_code,
            use_this.division,
            use_this.group_no,
            use_this.dept,
            use_this.class,
            use_this.subclass,
            use_this.item_parent,
            use_this.item_grandparent,
            use_this.diff_1,
            use_this.diff_2,
            use_this.diff_3,
            use_this.diff_4,
            use_this.chain,
            use_this.area,
            use_this.region,
            use_this.district,
            use_this.supp_hier_lvl_1,
            use_this.supp_hier_lvl_2,
            use_this.supp_hier_lvl_3,
            use_this.reclass_no,
            use_this.cost_change,
            use_this.simple_pack_ind,
            use_this.primary_cost_pack,
            use_this.primary_cost_pack_qty,
            use_this.store_type,
            use_this.costing_loc,
            use_this.templ_id,
            use_this.passthru_pct,
            use_this.processing_seq_no);
  
  
   merge into future_cost_gtt fct
   using (select * from  (select fc.item,
                          fc.supplier,
                          fc.origin_country_id,
                          fc.location,
                          fc.loc_type,
                          fc.active_date,
                          fc.base_cost,
                          fc.net_cost,
                          fc.net_net_cost,
                          fc.dead_net_net_cost,
                          fc.pricing_cost,
                          fc.calc_date,
                          fc.start_ind,
                          fc.acquisition_cost,
                          fc.elc_amt,
                          fc.primary_supp_country_ind,
                          fc.currency_code,
                          fc.division,
                          fc.group_no,
                          fc.dept,
                          fc.class,
                          fc.subclass,
                          fc.item_parent,
                          fc.item_grandparent,
                          fc.diff_1,
                          fc.diff_2,
                          fc.diff_3,
                          fc.diff_4,
                          fc.chain,
                          fc.area,
                          fc.region,
                          fc.district,
                          fc.supp_hier_lvl_1,
                          fc.supp_hier_lvl_2,
                          fc.supp_hier_lvl_3,
                          fc.reclass_no,
                          fc.cost_change,
                          fc.simple_pack_ind,
                          fc.primary_cost_pack,
                          fc.primary_cost_pack_qty,
                          fc.store_type,
                          fc.costing_loc,
                          fc.templ_id,
                          fc.passthru_pct,
                          0 as processing_seq_no,
                          wf.wf_date as wf_date,
                          rank() over(partition by fc.item,
                                       fc.supplier,
                                       fc.origin_country_id,
                                       fc.location,
                                       wf.wf_date
                         order by fc.active_date desc) as rank_value
                     from future_cost_gtt fc,
                          (select dept,class,subclass,location,start_date as wf_date,end_date+1, templ_id from wf_cost_relationship)wf,
                          item_master im,
                          wf_cost_buildup_tmpl_head wfh
                    where fc.store_type            = 'F' 
                      and wfh.first_applied       != 'C'
                      and wfh.templ_id             = wf.templ_id
                      and im.dept                  = wf.dept
                      and im.class                 = wf.class
                      and im.subclass              = wf.subclass
                      and im.item                  = fc.item
                      and wf.location              = fc.location
                      and fc.active_date          <= wf.wf_date
                      and wf.class                != '-1'
                      and wf.subclass             != '-1'
                      and not exists
                          (  select 1 from wf_cost_relationship wfc ,wf_cost_buildup_tmpl_head wfhc
                             where wfhc.first_applied='C'
                             and wfhc.templ_id=wfc.templ_id
                             and NVL(wfc.item,-1) =fc.item 
                             and wfc.dept =fc.dept
                             and wfc.class =fc.class
                             and wfc.subclass=fc.subclass
                             and wfc.location = fc.location
                             and wf.wf_date between wfc.start_date and wfc.end_date
                          )) inner1
              where inner1.rank_value = 1) use_this
   on (    fct.item                  = use_this.item
       and fct.supplier              = use_this.supplier
       and fct.origin_country_id     = use_this.origin_country_id
       and fct.location              = use_this.location
       and fct.active_date           = use_this.wf_date)
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           acquisition_cost,
           elc_amt,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_parent,
           item_grandparent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           templ_id,
           passthru_pct,
           processing_seq_no)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.wf_date,
           use_this.base_cost,
           use_this.net_cost,
           use_this.net_net_cost,
           use_this.dead_net_net_cost,
           use_this.pricing_cost,
           use_this.calc_date,
           use_this.start_ind,
           use_this.acquisition_cost,
           use_this.elc_amt,
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_parent,
           use_this.item_grandparent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           use_this.reclass_no,
           use_this.cost_change,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           use_this.templ_id,
           use_this.passthru_pct,
           use_this.processing_seq_no);
           
           
           merge into future_cost_gtt fct
              using (select * from  (select fc.item,
                                     fc.supplier,
                                     fc.origin_country_id,
                                     fc.location,
                                     fc.loc_type,
                                     fc.active_date,
                                     fc.base_cost,
                                     fc.net_cost,
                                     fc.net_net_cost,
                                     fc.dead_net_net_cost,
                                     fc.pricing_cost,
                                     fc.calc_date,
                                     fc.start_ind,
                                     fc.acquisition_cost,
                                     fc.elc_amt,
                                     fc.primary_supp_country_ind,
                                     fc.currency_code,
                                     fc.division,
                                     fc.group_no,
                                     fc.dept,
                                     fc.class,
                                     fc.subclass,
                                     fc.item_parent,
                                     fc.item_grandparent,
                                     fc.diff_1,
                                     fc.diff_2,
                                     fc.diff_3,
                                     fc.diff_4,
                                     fc.chain,
                                     fc.area,
                                     fc.region,
                                     fc.district,
                                     fc.supp_hier_lvl_1,
                                     fc.supp_hier_lvl_2,
                                     fc.supp_hier_lvl_3,
                                     fc.reclass_no,
                                     fc.cost_change,
                                     fc.simple_pack_ind,
                                     fc.primary_cost_pack,
                                     fc.primary_cost_pack_qty,
                                     fc.store_type,
                                     fc.costing_loc,
                                     fc.templ_id,
                                     fc.passthru_pct,
                                     0 as processing_seq_no,
                                     wf.wf_date as wf_date,
                                     rank() over(partition by fc.item,
                                                  fc.supplier,
                                                  fc.origin_country_id,
                                                  fc.location,
                                                  wf.wf_date
                                    order by fc.active_date desc) as rank_value
                                from future_cost_gtt fc,
                                     (select dept,class,subclass,location,end_date + 1 as wf_date,start_date,templ_id from wf_cost_relationship) wf,
                                     item_master im,
                                     wf_cost_buildup_tmpl_head wfh
                               where fc.store_type            = 'F' 
                                 and wfh.first_applied       != 'C'
                                 and wfh.templ_id             = wf.templ_id
                                 and im.dept                  = wf.dept
                                 and im.class                 = wf.class
                                 and im.subclass              = wf.subclass
                                 and im.item                  = fc.item
                                 and wf.location              = fc.location
                                 and fc.active_date          <= wf.wf_date
                                 and wf.class                != '-1'
                                 and wf.subclass             != '-1'
                                 and not exists
                                     (  select 1 from wf_cost_relationship wfc ,wf_cost_buildup_tmpl_head wfhc
                                        where wfhc.first_applied='C'
                                        and wfhc.templ_id=wfc.templ_id
                                        and NVL(wfc.item,-1) =fc.item 
                                        and wfc.dept =fc.dept
                                        and wfc.class =fc.class
                                        and wfc.subclass=fc.subclass
                                        and wfc.location = fc.location
                                        and wf.wf_date between wfc.start_date and wfc.end_date
                                     )) inner1
                         where inner1.rank_value = 1) use_this
              on (    fct.item                  = use_this.item
                  and fct.supplier              = use_this.supplier
                  and fct.origin_country_id     = use_this.origin_country_id
                  and fct.location              = use_this.location
                  and fct.active_date           = use_this.wf_date)
              when not matched then
              insert (item,
                      supplier,
                      origin_country_id,
                      location,
                      loc_type,
                      active_date,
                      base_cost,
                      net_cost,
                      net_net_cost,
                      dead_net_net_cost,
                      pricing_cost,
                      calc_date,
                      start_ind,
                      acquisition_cost,
                      elc_amt,
                      primary_supp_country_ind,
                      currency_code,
                      division,
                      group_no,
                      dept,
                      class,
                      subclass,
                      item_parent,
                      item_grandparent,
                      diff_1,
                      diff_2,
                      diff_3,
                      diff_4,
                      chain,
                      area,
                      region,
                      district,
                      supp_hier_lvl_1,
                      supp_hier_lvl_2,
                      supp_hier_lvl_3,
                      reclass_no,
                      cost_change,
                      simple_pack_ind,
                      primary_cost_pack,
                      primary_cost_pack_qty,
                      store_type,
                      costing_loc,
                      templ_id,
                      passthru_pct,
                      processing_seq_no)
              values (use_this.item,
                      use_this.supplier,
                      use_this.origin_country_id,
                      use_this.location,
                      use_this.loc_type,
                      use_this.wf_date,
                      use_this.base_cost,
                      use_this.net_cost,
                      use_this.net_net_cost,
                      use_this.dead_net_net_cost,
                      use_this.pricing_cost,
                      use_this.calc_date,
                      use_this.start_ind,
                      use_this.acquisition_cost,
                      use_this.elc_amt,
                      use_this.primary_supp_country_ind,
                      use_this.currency_code,
                      use_this.division,
                      use_this.group_no,
                      use_this.dept,
                      use_this.class,
                      use_this.subclass,
                      use_this.item_parent,
                      use_this.item_grandparent,
                      use_this.diff_1,
                      use_this.diff_2,
                      use_this.diff_3,
                      use_this.diff_4,
                      use_this.chain,
                      use_this.area,
                      use_this.region,
                      use_this.district,
                      use_this.supp_hier_lvl_1,
                      use_this.supp_hier_lvl_2,
                      use_this.supp_hier_lvl_3,
                      use_this.reclass_no,
                      use_this.cost_change,
                      use_this.simple_pack_ind,
                      use_this.primary_cost_pack,
                      use_this.primary_cost_pack_qty,
                      use_this.store_type,
                      use_this.costing_loc,
                      use_this.templ_id,
                      use_this.passthru_pct,
                      use_this.processing_seq_no);
                      
                      
                      
           merge into future_cost_gtt fct
           using (select * from  (select fc.item,
                                         fc.supplier,
                                         fc.origin_country_id,
                                         fc.location,
                                         fc.loc_type,
                                         fc.active_date,
                                         fc.base_cost,
                                         fc.net_cost,
                                         fc.net_net_cost,
                                         fc.dead_net_net_cost,
                                         fc.pricing_cost,
                                         fc.calc_date,
                                         fc.start_ind,
                                         fc.acquisition_cost,
                                         fc.elc_amt,
                                         fc.primary_supp_country_ind,
                                         fc.currency_code,
                                         fc.division,
                                         fc.group_no,
                                         fc.dept,
                                         fc.class,
                                         fc.subclass,
                                         fc.item_parent,
                                         fc.item_grandparent,
                                         fc.diff_1,
                                         fc.diff_2,
                                         fc.diff_3,
                                         fc.diff_4,
                                         fc.chain,
                                         fc.area,
                                         fc.region,
                                         fc.district,
                                         fc.supp_hier_lvl_1,
                                         fc.supp_hier_lvl_2,
                                         fc.supp_hier_lvl_3,
                                         fc.reclass_no,
                                         fc.cost_change,
                                         fc.simple_pack_ind,
                                         fc.primary_cost_pack,
                                         fc.primary_cost_pack_qty,
                                         fc.store_type,
                                         fc.costing_loc,
                                         fc.templ_id,
                                         fc.passthru_pct,
                                         0 as processing_seq_no,
                                         wf.wf_date as wf_date,
                                         rank() over(partition by fc.item,
                                                                  fc.supplier,
                                                                  fc.origin_country_id,
                                                                  fc.location,
                                                                  wf.wf_date
                                                         order by fc.active_date desc) as rank_value
                                   from future_cost_gtt fc,
                                        (select dept,class,subclass,location,start_date as wf_date,end_date+1 end_date, templ_id from wf_cost_relationship)wf,
                                        item_master im,
                                        wf_cost_buildup_tmpl_head wfh
                                  where fc.store_type            = 'F' 
                                    and wfh.first_applied       != 'C'
                                    and wfh.templ_id             = wf.templ_id
                                    and im.dept                  = wf.dept
                                    and im.class                 = wf.class
                                    and im.subclass              = fc.subclass
                                    and im.item                  = fc.item
                                    and wf.location              = fc.location
                                    and fc.active_date          <= wf.wf_date
                                    and wf.class                != '-1'
                                    and wf.subclass              = '-1'
                                    and not exists
                                       (  select 1 from wf_cost_relationship wfc ,wf_cost_buildup_tmpl_head wfhc
                                           where (wfhc.first_applied='C' or ( wfhc.first_applied !='C' and wfc.class != '-1' and wfc.subclass != '-1'))
                                             and wfhc.templ_id=wfc.templ_id
                                             and decode(wfc.item,'-1',fc.item,wfc.item) =fc.item 
                                             and wfc.dept =fc.dept
                                             and wfc.class =fc.class
                                             and wfc.subclass=fc.subclass
                                             and wfc.location = fc.location
                                             and wf.wf_date between wfc.start_date and wfc.end_date 
                                       )) inner1
                         where inner1.rank_value = 1) use_this
              on (    fct.item                  = use_this.item
                  and fct.supplier              = use_this.supplier
                  and fct.origin_country_id     = use_this.origin_country_id
                  and fct.location              = use_this.location
                  and fct.active_date           = use_this.wf_date)
              when not matched then
              insert (item,
                      supplier,
                      origin_country_id,
                      location,
                      loc_type,
                      active_date,
                      base_cost,
                      net_cost,
                      net_net_cost,
                      dead_net_net_cost,
                      pricing_cost,
                      calc_date,
                      start_ind,
                      acquisition_cost,
                      elc_amt,
                      primary_supp_country_ind,
                      currency_code,
                      division,
                      group_no,
                      dept,
                      class,
                      subclass,
                      item_parent,
                      item_grandparent,
                      diff_1,
                      diff_2,
                      diff_3,
                      diff_4,
                      chain,
                      area,
                      region,
                      district,
                      supp_hier_lvl_1,
                      supp_hier_lvl_2,
                      supp_hier_lvl_3,
                      reclass_no,
                      cost_change,
                      simple_pack_ind,
                      primary_cost_pack,
                      primary_cost_pack_qty,
                      store_type,
                      costing_loc,
                      templ_id,
                      passthru_pct,
                      processing_seq_no)
              values (use_this.item,
                      use_this.supplier,
                      use_this.origin_country_id,
                      use_this.location,
                      use_this.loc_type,
                      use_this.wf_date,
                      use_this.base_cost,
                      use_this.net_cost,
                      use_this.net_net_cost,
                      use_this.dead_net_net_cost,
                      use_this.pricing_cost,
                      use_this.calc_date,
                      use_this.start_ind,
                      use_this.acquisition_cost,
                      use_this.elc_amt,
                      use_this.primary_supp_country_ind,
                      use_this.currency_code,
                      use_this.division,
                      use_this.group_no,
                      use_this.dept,
                      use_this.class,
                      use_this.subclass,
                      use_this.item_parent,
                      use_this.item_grandparent,
                      use_this.diff_1,
                      use_this.diff_2,
                      use_this.diff_3,
                      use_this.diff_4,
                      use_this.chain,
                      use_this.area,
                      use_this.region,
                      use_this.district,
                      use_this.supp_hier_lvl_1,
                      use_this.supp_hier_lvl_2,
                      use_this.supp_hier_lvl_3,
                      use_this.reclass_no,
                      use_this.cost_change,
                      use_this.simple_pack_ind,
                      use_this.primary_cost_pack,
                      use_this.primary_cost_pack_qty,
                      use_this.store_type,
                      use_this.costing_loc,
                      use_this.templ_id,
                      use_this.passthru_pct,
                      use_this.processing_seq_no);
                      
                      
           merge into future_cost_gtt fct
              using (select * from  (select fc.item,
                                            fc.supplier,
                                            fc.origin_country_id,
                                            fc.location,
                                            fc.loc_type,
                                            fc.active_date,
                                            fc.base_cost,
                                            fc.net_cost,
                                            fc.net_net_cost,
                                            fc.dead_net_net_cost,
                                            fc.pricing_cost,
                                            fc.calc_date,
                                            fc.start_ind,
                                            fc.acquisition_cost,
                                            fc.elc_amt,
                                            fc.primary_supp_country_ind,
                                            fc.currency_code,
                                            fc.division,
                                            fc.group_no,
                                            fc.dept,
                                            fc.class,
                                            fc.subclass,
                                            fc.item_parent,
                                            fc.item_grandparent,
                                            fc.diff_1,
                                            fc.diff_2,
                                            fc.diff_3,
                                            fc.diff_4,
                                            fc.chain,
                                            fc.area,
                                            fc.region,
                                            fc.district,
                                            fc.supp_hier_lvl_1,
                                            fc.supp_hier_lvl_2,
                                            fc.supp_hier_lvl_3,
                                            fc.reclass_no,
                                            fc.cost_change,
                                            fc.simple_pack_ind,
                                            fc.primary_cost_pack,
                                            fc.primary_cost_pack_qty,
                                            fc.store_type,
                                            fc.costing_loc,
                                            fc.templ_id,
                                            fc.passthru_pct,
                                            0 as processing_seq_no,
                                            wf.wf_date as wf_date,
                                            rank() over(partition by fc.item,
                                                                     fc.supplier,
                                                                     fc.origin_country_id,
                                                                     fc.location,
                                                                     wf.wf_date
                                                            order by fc.active_date desc) as rank_value
                                       from future_cost_gtt fc,
                                            (select dept,class,subclass,location,end_date + 1 as wf_date,start_date,templ_id from wf_cost_relationship) wf,
                                            item_master im,
                                            wf_cost_buildup_tmpl_head wfh
                                      where fc.store_type            = 'F' 
                                        and wfh.first_applied       != 'C'
                                        and wfh.templ_id             = wf.templ_id
                                        and im.dept                  = wf.dept
                                        and im.class                 = wf.class
                                        and im.subclass              = fc.subclass
                                        and im.item                  = fc.item
                                        and wf.location              = fc.location
                                        and fc.active_date          <= wf.wf_date
                                        and wf.class                != '-1'
                                        and wf.subclass              = '-1' 
                                        and not exists
                                       (  select 1 from wf_cost_relationship wfc , wf_cost_buildup_tmpl_head wfhc
                                           where (wfhc.first_applied='C' or (wfhc.first_applied !='C' and wfc.class != '-1' and wfc.subclass != '-1'))
                                             and wfhc.templ_id=wfc.templ_id
                                             and decode(wfc.item,'-1',fc.item,wfc.item) =fc.item 
                                             and wfc.dept =fc.dept
                                             and wfc.class =fc.class
                                             and wfc.subclass=fc.subclass
                                             and wfc.location = fc.location
                                             and wf.wf_date between wfc.start_date and wfc.end_date
                                       )) inner1
                         where inner1.rank_value = 1) use_this
              on (    fct.item                  = use_this.item
                  and fct.supplier              = use_this.supplier
                  and fct.origin_country_id     = use_this.origin_country_id
                  and fct.location              = use_this.location
                  and fct.active_date           = use_this.wf_date)
              when not matched then
              insert (item,
                      supplier,
                      origin_country_id,
                      location,
                      loc_type,
                      active_date,
                      base_cost,
                      net_cost,
                      net_net_cost,
                      dead_net_net_cost,
                      pricing_cost,
                      calc_date,
                      start_ind,
                      acquisition_cost,
                      elc_amt,
                      primary_supp_country_ind,
                      currency_code,
                      division,
                      group_no,
                      dept,
                      class,
                      subclass,
                      item_parent,
                      item_grandparent,
                      diff_1,
                      diff_2,
                      diff_3,
                      diff_4,
                      chain,
                      area,
                      region,
                      district,
                      supp_hier_lvl_1,
                      supp_hier_lvl_2,
                      supp_hier_lvl_3,
                      reclass_no,
                      cost_change,
                      simple_pack_ind,
                      primary_cost_pack,
                      primary_cost_pack_qty,
                      store_type,
                      costing_loc,
                      templ_id,
                      passthru_pct,
                      processing_seq_no)
              values (use_this.item,
                      use_this.supplier,
                      use_this.origin_country_id,
                      use_this.location,
                      use_this.loc_type,
                      use_this.wf_date,
                      use_this.base_cost,
                      use_this.net_cost,
                      use_this.net_net_cost,
                      use_this.dead_net_net_cost,
                      use_this.pricing_cost,
                      use_this.calc_date,
                      use_this.start_ind,
                      use_this.acquisition_cost,
                      use_this.elc_amt,
                      use_this.primary_supp_country_ind,
                      use_this.currency_code,
                      use_this.division,
                      use_this.group_no,
                      use_this.dept,
                      use_this.class,
                      use_this.subclass,
                      use_this.item_parent,
                      use_this.item_grandparent,
                      use_this.diff_1,
                      use_this.diff_2,
                      use_this.diff_3,
                      use_this.diff_4,
                      use_this.chain,
                      use_this.area,
                      use_this.region,
                      use_this.district,
                      use_this.supp_hier_lvl_1,
                      use_this.supp_hier_lvl_2,
                      use_this.supp_hier_lvl_3,
                      use_this.reclass_no,
                      use_this.cost_change,
                      use_this.simple_pack_ind,
                      use_this.primary_cost_pack,
                      use_this.primary_cost_pack_qty,
                      use_this.store_type,
                      use_this.costing_loc,
                      use_this.templ_id,
                      use_this.passthru_pct,
                      use_this.processing_seq_no);     
                      

                      
           merge into future_cost_gtt fct
              using (select * from  (select fc.item,
                                            fc.supplier,
                                            fc.origin_country_id,
                                            fc.location,
                                            fc.loc_type,
                                            fc.active_date,
                                            fc.base_cost,
                                            fc.net_cost,
                                            fc.net_net_cost,
                                            fc.dead_net_net_cost,
                                            fc.pricing_cost,
                                            fc.calc_date,
                                            fc.start_ind,
                                            fc.acquisition_cost,
                                            fc.elc_amt,
                                            fc.primary_supp_country_ind,
                                            fc.currency_code,
                                            fc.division,
                                            fc.group_no,
                                            fc.dept,
                                            fc.class,
                                            fc.subclass,
                                            fc.item_parent,
                                            fc.item_grandparent,
                                            fc.diff_1,
                                            fc.diff_2,
                                            fc.diff_3,
                                            fc.diff_4,
                                            fc.chain,
                                            fc.area,
                                            fc.region,
                                            fc.district,
                                            fc.supp_hier_lvl_1,
                                            fc.supp_hier_lvl_2,
                                            fc.supp_hier_lvl_3,
                                            fc.reclass_no,
                                            fc.cost_change,
                                            fc.simple_pack_ind,
                                            fc.primary_cost_pack,
                                            fc.primary_cost_pack_qty,
                                            fc.store_type,
                                            fc.costing_loc,
                                            fc.templ_id,
                                            fc.passthru_pct,
                                            0 as processing_seq_no,
                                            wf.wf_date as wf_date,
                                            rank() over(partition by fc.item,
                                                                     fc.supplier,
                                                                     fc.origin_country_id,
                                                                     fc.location,
                                                                     wf.wf_date
                                                            order by fc.active_date desc) as rank_value
                                      from future_cost_gtt fc,
                                           (select dept,class,subclass,location,start_date as wf_date,end_date+1 end_date, templ_id from wf_cost_relationship)wf,
                                           item_master im,
                                           wf_cost_buildup_tmpl_head wfh
                                     where fc.store_type            = 'F' 
                                       and wfh.first_applied       != 'C'
                                       and wfh.templ_id             = wf.templ_id
                                       and im.dept                  = wf.dept
                                       and im.class                 = fc.class
                                       and im.subclass              = fc.subclass
                                       and im.item                  = fc.item
                                       and wf.location              = fc.location
                                       and fc.active_date          <= wf.wf_date
                                       and wf.class                 = '-1'
                                       and wf.subclass              = '-1'
                                       and not exists
                                          (  select 1 from wf_cost_relationship wfc ,wf_cost_buildup_tmpl_head wfhc
                                              where (wfhc.first_applied='C' or (wfhc.first_applied !='C' and wfc.class != '-1' and wfc.subclass != '-1') 
                                                                            or (wfhc.first_applied !='C' and wfc.class != '-1' and wfc.subclass = '-1')
                                                    )
                                                and wfhc.templ_id=wfc.templ_id
                                                and decode(wfc.item,'-1',fc.item,wfc.item) =fc.item  
                                                and wfc.dept =fc.dept
                                                and wfc.class =fc.class
                                                and decode(wfc.subclass,'-1',fc.subclass,wfc.subclass) = fc.subclass 
                                                and wfc.location = fc.location
                                                and wf.wf_date between wfc.start_date and wfc.end_date 
                                          )) inner1
                         where inner1.rank_value = 1) use_this
              on (    fct.item                  = use_this.item
              and fct.supplier              = use_this.supplier
              and fct.origin_country_id     = use_this.origin_country_id
              and fct.location              = use_this.location
              and fct.active_date           = use_this.wf_date)
              when not matched then
              insert (item,
                      supplier,
                      origin_country_id,
                      location,
                      loc_type,
                      active_date,
                      base_cost,
                      net_cost,   
                      net_net_cost,
                      dead_net_net_cost,
                      pricing_cost,
                      calc_date,
                      start_ind,
                      acquisition_cost,
                      elc_amt,
                      primary_supp_country_ind,
                      currency_code,
                      division,
                      group_no,
                      dept,
                      class,
                      subclass,
                      item_parent,
                      item_grandparent,
                      diff_1,
                      diff_2,
                      diff_3,
                      diff_4,
                      chain,
                      area,
                      region,
                      district,
                      supp_hier_lvl_1,
                      supp_hier_lvl_2,
                      supp_hier_lvl_3,
                      reclass_no,
                      cost_change,
                      simple_pack_ind,
                      primary_cost_pack,
                      primary_cost_pack_qty,
                      store_type,
                      costing_loc,
                      templ_id,
                      passthru_pct,
                      processing_seq_no)
              values (use_this.item,
                      use_this.supplier,
                      use_this.origin_country_id,
                      use_this.location,
                      use_this.loc_type,
                      use_this.wf_date,
                      use_this.base_cost,
                      use_this.net_cost,
                      use_this.net_net_cost,
                      use_this.dead_net_net_cost,
                      use_this.pricing_cost,
                      use_this.calc_date,
                      use_this.start_ind,
                      use_this.acquisition_cost,
                      use_this.elc_amt,
                      use_this.primary_supp_country_ind,
                      use_this.currency_code,
                      use_this.division,
                      use_this.group_no,
                      use_this.dept,
                      use_this.class,
                      use_this.subclass,
                      use_this.item_parent,
                      use_this.item_grandparent,
                      use_this.diff_1,
                      use_this.diff_2,
                      use_this.diff_3,
                      use_this.diff_4,
                      use_this.chain,
                      use_this.area,
                      use_this.region,
                      use_this.district,
                      use_this.supp_hier_lvl_1,
                      use_this.supp_hier_lvl_2,
                      use_this.supp_hier_lvl_3,
                      use_this.reclass_no,
                      use_this.cost_change,
                      use_this.simple_pack_ind,
                      use_this.primary_cost_pack,
                      use_this.primary_cost_pack_qty,
                      use_this.store_type,
                      use_this.costing_loc,
                      use_this.templ_id,
                      use_this.passthru_pct,
                      use_this.processing_seq_no);   
                      
                      
              merge into future_cost_gtt fct
              using (select * from  (select fc.item,
                                            fc.supplier,
                                            fc.origin_country_id,
                                            fc.location,
                                            fc.loc_type,
                                            fc.active_date,
                                            fc.base_cost,
                                            fc.net_cost,
                                            fc.net_net_cost,
                                            fc.dead_net_net_cost,
                                            fc.pricing_cost,
                                            fc.calc_date,
                                            fc.start_ind,
                                            fc.acquisition_cost,
                                            fc.elc_amt,
                                            fc.primary_supp_country_ind,
                                            fc.currency_code,
                                            fc.division,
                                            fc.group_no,
                                            fc.dept,
                                            fc.class,
                                            fc.subclass,
                                            fc.item_parent,
                                            fc.item_grandparent,
                                            fc.diff_1,
                                            fc.diff_2,
                                            fc.diff_3,
                                            fc.diff_4,
                                            fc.chain,
                                            fc.area,
                                            fc.region,
                                            fc.district,
                                            fc.supp_hier_lvl_1,
                                            fc.supp_hier_lvl_2,
                                            fc.supp_hier_lvl_3,
                                            fc.reclass_no,
                                            fc.cost_change,
                                            fc.simple_pack_ind,
                                            fc.primary_cost_pack,
                                            fc.primary_cost_pack_qty,
                                            fc.store_type,
                                            fc.costing_loc,
                                            fc.templ_id,
                                            fc.passthru_pct,
                                            0 as processing_seq_no,
                                            wf.wf_date as wf_date,
                                            rank() over(partition by fc.item,
                                                                     fc.supplier,
                                                                     fc.origin_country_id,
                                                                     fc.location,
                                                                     wf.wf_date
                                                            order by fc.active_date desc) as rank_value
                                      from future_cost_gtt fc,
                                           (select dept,class,subclass,location,end_date + 1 as wf_date,start_date,templ_id from wf_cost_relationship) wf,
                                           item_master im,
                                           wf_cost_buildup_tmpl_head wfh
                                     where fc.store_type            = 'F' 
                                       and wfh.first_applied       != 'C'
                                       and wfh.templ_id             = wf.templ_id
                                       and im.dept                  = wf.dept
                                       and im.class                 = fc.class
                                       and im.subclass              = fc.subclass
                                       and im.item                  = fc.item
                                       and wf.location              = fc.location
                                       and fc.active_date          <= wf.wf_date
                                       and wf.class                 = '-1'
                                       and wf.subclass              = '-1' 
                                       and not exists
                                          (  select 1 from wf_cost_relationship wfc ,wf_cost_buildup_tmpl_head wfhc
                                              where (wfhc.first_applied='C' or (wfhc.first_applied!='C' and wfc.class != '-1' and wfc.subclass != '-1') 
                                                                            or (wfhc.first_applied!='C' and wfc.class != '-1' and wfc.subclass = '-1')
                                                    )
                                                and wfhc.templ_id=wfc.templ_id
                                                and decode(wfc.item,'-1',fc.item,wfc.item) =fc.item
                                                and wfc.dept =fc.dept
                                                and wfc.class =fc.class
                                                and decode(wfc.subclass,'-1',fc.subclass,wfc.subclass) = fc.subclass
                                                and wfc.location = fc.location
                                                and wf.wf_date between wfc.start_date and wfc.end_date
                                          )) inner1
                         where inner1.rank_value = 1) use_this
              on (    fct.item                  = use_this.item
                  and fct.supplier              = use_this.supplier
                  and fct.origin_country_id     = use_this.origin_country_id
                  and fct.location              = use_this.location
                  and fct.active_date           = use_this.wf_date)
              when not matched then
              insert (item,
                      supplier,
                      origin_country_id,
                      location,
                      loc_type,
                      active_date,
                      base_cost,
                      net_cost,
                      net_net_cost,
                      dead_net_net_cost,
                      pricing_cost,
                      calc_date,
                      start_ind,
                      acquisition_cost,
                      elc_amt,
                      primary_supp_country_ind,
                      currency_code,
                      division,
                      group_no,
                      dept,
                      class,
                      subclass,
                      item_parent,
                      item_grandparent,
                      diff_1,
                      diff_2,
                      diff_3,
                      diff_4,
                      chain,
                      area,
                      region,
                      district,
                      supp_hier_lvl_1,
                      supp_hier_lvl_2,
                      supp_hier_lvl_3,
                      reclass_no,
                      cost_change,
                      simple_pack_ind,
                      primary_cost_pack,
                      primary_cost_pack_qty,
                      store_type,
                      costing_loc,
                      templ_id,
                      passthru_pct,
                      processing_seq_no)
              values (use_this.item,
                      use_this.supplier,
                      use_this.origin_country_id,
                      use_this.location,
                      use_this.loc_type,
                      use_this.wf_date,
                      use_this.base_cost,
                      use_this.net_cost,
                      use_this.net_net_cost,
                      use_this.dead_net_net_cost,
                      use_this.pricing_cost,
                      use_this.calc_date,
                      use_this.start_ind,
                      use_this.acquisition_cost,
                      use_this.elc_amt,
                      use_this.primary_supp_country_ind,
                      use_this.currency_code,
                      use_this.division,
                      use_this.group_no,
                      use_this.dept,
                      use_this.class,
                      use_this.subclass,
                      use_this.item_parent,
                      use_this.item_grandparent,
                      use_this.diff_1,
                      use_this.diff_2,
                      use_this.diff_3,
                      use_this.diff_4,
                      use_this.chain,
                      use_this.area,
                      use_this.region,
                      use_this.district,
                      use_this.supp_hier_lvl_1,
                      use_this.supp_hier_lvl_2,
                      use_this.supp_hier_lvl_3,
                      use_this.reclass_no,
                      use_this.cost_change,
                      use_this.simple_pack_ind,
                      use_this.primary_cost_pack,
                      use_this.primary_cost_pack_qty,
                      use_this.store_type,
                      use_this.costing_loc,
                      use_this.templ_id,
                      use_this.passthru_pct,
                      use_this.processing_seq_no);                                                     
           
         return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.PUSH_TEMPL_DATES',
                                            to_char(SQLCODE));
      return 0;
END PUSH_TEMPL_DATES;

----------------------------------------------------------------------------------------


FUNCTION SYNC_TEMPL_ID(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is


BEGIN

   merge into future_cost_gtt fc
   using ( select fc.*,
                  wfc.templ_id templ_id_new
             from future_cost_gtt fc,
                  wf_cost_relationship wfc,
                  wf_cost_buildup_tmpl_head wch
            where wch.first_applied = 'C'
              and wch.templ_id     = wfc.templ_id
              and fc.dept          = wfc.dept
              and fc.class         = wfc.class
              and fc.subclass      = wfc.subclass
              and fc.location      = wfc.location
              and fc.item          = NVL(wfc.item,-1)          /* only the item which has applied cost based template will be selected*/
              and fc.store_type    = 'F'
              and fc.active_date  >= wfc.start_date
              and fc.active_date  <= wfc.end_date) use_this
  on (    fc.item              = use_this.item
      and fc.supplier              = use_this.supplier
      and fc.origin_country_id     = use_this.origin_country_id
      and fc.location              = use_this.location
      and fc.active_date           = use_this.active_date)
  when matched then
  update
     set fc.templ_id = use_this.templ_id_new;
     

   
   
   merge into future_cost_gtt fc
   using ( select fc.*,
                  wfc.templ_id templ_id_new
             from future_cost_gtt fc,
                  wf_cost_relationship wfc,
                  wf_cost_buildup_tmpl_head wch
            where wch.first_applied != 'C'
              and wch.templ_id     = wfc.templ_id
              and fc.dept          = wfc.dept
              and fc.class         = wfc.class
              and fc.subclass      = wfc.subclass
              and fc.location      = wfc.location
              and fc.store_type    ='F'
              and fc.active_date  >= wfc.start_date
              and fc.active_date  <= wfc.end_date
              and wfc.class       != '-1'
              and wfc.subclass    != '-1'
              and fc.templ_id is null
          ) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.active_date)
   when matched then
   update
      set fc.templ_id = use_this.templ_id_new;
      
      
      
    merge into future_cost_gtt fc
    using ( select fc.*,
                  wfc.templ_id templ_id_new
             from future_cost_gtt fc,
                  wf_cost_relationship wfc,
                  wf_cost_buildup_tmpl_head wch
            where wch.first_applied != 'C'
              and wch.templ_id     = wfc.templ_id
              and fc.dept          = wfc.dept
              and fc.class         = wfc.class
              and fc.location      = wfc.location
              and fc.store_type    ='F'
              and fc.active_date  >= wfc.start_date
              and fc.active_date  <= wfc.end_date
              and wfc.class       != '-1'
              and wfc.subclass     = '-1'
              and fc.templ_id is null
            ) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.active_date)
   when matched then
   update
      set fc.templ_id = use_this.templ_id_new;   
      

   merge into future_cost_gtt fc
   using ( select fc.*,
                  wfc.templ_id templ_id_new
             from future_cost_gtt fc,
                  wf_cost_relationship wfc,
                  wf_cost_buildup_tmpl_head wch
            where wch.first_applied != 'C'
              and wch.templ_id     = wfc.templ_id
              and fc.dept          = wfc.dept
              and fc.location      = wfc.location
              and fc.store_type    ='F'
              and fc.active_date  >= wfc.start_date
              and fc.active_date  <= wfc.end_date
              and wfc.class        = '-1'
              and wfc.subclass     = '-1'
              and fc.templ_id is null
            ) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.active_date)
   when matched then
   update
      set fc.templ_id = use_this.templ_id_new;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.SYNC_TEMPL_ID',
                                            to_char(SQLCODE));
      return 0;
END SYNC_TEMPL_ID;
----------------------------------------------------------------------------------------
FUNCTION SYNC_PASSTHRU_PCT(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

BEGIN

   merge into future_cost_gtt fc
   using ( select fc.*,
                  dp.passthru_pct passthru_pct_new
             from future_cost_gtt fc,
                  deal_passthru dp
            where fc.dept       = dp.dept
              and fc.supplier in (select supplier
                                    from sups
                                   where supplier_parent = dp.supplier
                                          or supplier = dp.supplier)
              and fc.location   = dp.location
              and fc.store_type  in ('W', 'F')
              and fc.costing_loc  = dp.costing_loc ) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.active_date)
   when matched then
   update
      set fc.passthru_pct = use_this.passthru_pct_new;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.SYNC_PASSTHRU_PCT',
                                            to_char(SQLCODE));
      return 0;
END SYNC_PASSTHRU_PCT;
----------------------------------------------------------------------------------------
FUNCTION SETUP_WHOLESALE_FRANCHISE(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                   I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

BEGIN

   --seed wh records to W F store
   if FUTURE_COST_ROLLFWD_SQL.SEED_WF_STORES(O_error_message,
                                             I_cost_event_process_id,
                                             I_thread_id) = 0 then
      return 0;
   end if;

   --push cost template dates to fct, need to reset processing_seq_no? -- doesn't seem like we need to worry about this.
   if FUTURE_COST_ROLLFWD_SQL.PUSH_TEMPL_DATES(O_error_message,
                                               I_cost_event_process_id,
                                               I_thread_id) = 0 then
      return 0;
   end if;

   --push template ids to fct
   if FUTURE_COST_ROLLFWD_SQL.SYNC_TEMPL_ID(O_error_message,
                                            I_cost_event_process_id,
                                            I_thread_id) = 0 then
      return 0;
   end if;

   --push passthru percent to fct
   if FUTURE_COST_ROLLFWD_SQL.SYNC_PASSTHRU_PCT(O_error_message,
                                                I_cost_event_process_id,
                                                I_thread_id) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.SETUP_WHOLESALE_FRANCHISE',
                                            to_char(SQLCODE));
      return 0;
END SETUP_WHOLESALE_FRANCHISE;
----------------------------------------------------------------------------------------
FUNCTION HELP_ROLL_TMPL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

BEGIN

--insert for calc_basis = S
insert into future_cost_comp_gtt
(select inner.item,
        inner.supplier,
        inner.origin_country_id,
        inner.location,
        inner.active_date,
        inner.templ_id,
        inner.cost_comp_id,
        inner.comp_rate,
        inner.per_count_uom,
        inner.per_count,
        CASE when inner.uom_class = 'MISC' then
             (inner.per_unit_value * (inner.comp_rate/NVL(inner.per_count,1)))
             when inner.uom_class = 'PACK' then
             (inner.per_unit_value * (inner.comp_rate/NVL(inner.per_count,1)))
             when inner.uom_class != 'MISC' then
             decode(uc.operator,'M',((inner.per_unit_value * uc.factor) * (inner.comp_rate/NVL(inner.per_count,1))),
                                    ((NVL(inner.per_unit_value,1)/NVL(uc.factor,1)) * (inner.comp_rate/NVL(inner.per_count,1))))
        end value,
        first_value(curr.exchange_rate) over
        (partition by inner.item, inner.location, inner.supplier, inner.origin_country_id, inner.active_date
         order by curr.effective_date desc) exchange_rate
  from uom_conversion uc,
       mv_currency_conversion_rates curr,
       (select fc.item,
               fc.supplier,
               fc.origin_country_id,
               fc.location,
               fc.active_date,
               fc.templ_id,
               fc.currency_code,
               tmpl.cost_comp_id,
               tmpl.calc_basis,
               tmpl.comp_rate,
               tmpl.per_count_uom,
               tmpl.per_count,
               tmpl.comp_currency,
               isc.supp_pack_size,
               im.standard_uom,
               im.uom_conv_factor,
               ucl.uom_class,
               dimen.weight,
               dimen.weight_uom,
               dimen.length,
               dimen.height,
               dimen.width,
               dimen.lwh_uom,
               dimen.liquid_volume,
               dimen.liquid_volume_uom,
               dimen.dim_object,
               CASE when ucl.uom_class = 'QTY' then
                       case when im.uom_conv_factor is NULL then
                          DECODE(im.standard_uom,'EA',1,0) end
                    when ucl.uom_class = 'MASS' then
                       DECODE(dimen.weight,NULL,0,dimen.weight/isc.supp_pack_size)
                    when ucl.uom_class= 'LVOL' then
                       DECODE(dimen.liquid_volume,NULL,0,dimen.liquid_volume/isc.supp_pack_size)
                    when ucl.uom_class= 'VOL' then
                       case when dimen.length is NULL or dimen.height is NULL or dimen.width is NULL then 0
                          else (dimen.length * dimen.height * dimen.width/isc.supp_pack_size) end
                    when ucl.uom_class = 'AREA' then
                       case when dimen.length is NULL or dimen.width is NULL then 0
                          else (dimen.length * dimen.width/isc.supp_pack_size) end
                    when ucl.uom_class = 'DIMEN' then
                       DECODE(dimen.length,NULL,0,dimen.length/isc.supp_pack_size)
                    when ucl.uom_class = 'PACK' then
                       1/isc.supp_pack_size
                    when ucl.uom_class = 'MISC' then
                       NVL(misc.value,0)
               end per_unit_value,
               decode(ucl.uom_class,'QTY','EA','MASS',dimen.weight_uom,'LVOL',dimen.liquid_volume_uom,
                                    'VOL',dimen.lwh_uom||'3','AREA',dimen.lwh_uom||'2','DIMEN',dimen.lwh_uom,NULL) from_uom
         from future_cost_gtt fc,
              wf_cost_buildup_tmpl_detail tmpl,
              item_supp_country isc,
              item_master im,
              uom_class ucl,
              item_supp_country_dim dimen,
              (select item,
                      supplier,
                      value
                 from item_supp_uom) misc
        where fc.templ_id = tmpl.templ_id
          and fc.item = isc.item
          and fc.supplier = isc.supplier
          and fc.origin_country_id = isc.origin_country_id
          and fc.store_type in ('F')
          and tmpl.calc_basis = 'S'
          and fc.item = im.item
          and tmpl.per_count_uom = ucl.uom
          and fc.item = dimen.item(+)
          and fc.supplier = dimen.supplier(+)
          and fc.origin_country_id = dimen.origin_country(+)
          and fc.item = misc.item(+)
          and fc.supplier = misc.supplier(+)) inner
where inner.from_uom = uc.from_uom(+)
  and inner.per_count_uom = uc.to_uom(+)
  and inner.comp_currency = curr.from_currency
  and inner.currency_code = curr.to_currency
  and curr.exchange_type  = 'C'
  and curr.effective_date <= inner.active_date);

--insert for calc_basis = V
insert into future_cost_comp_gtt
(select fc.item,
        fc.supplier,
        fc.origin_country_id,
        fc.location,
        fc.active_date,
        fc.templ_id,
        tmpl.cost_comp_id,
        tmpl.comp_rate,
        tmpl.per_count_uom,
        tmpl.per_count,
        NVL(fc.acquisition_cost * (tmpl.comp_rate/100),0),
        null exchange_rate
   from future_cost_gtt fc,
        wf_cost_buildup_tmpl_detail tmpl
  where fc.templ_id = tmpl.templ_id
    and fc.store_type in ('F')
    and tmpl.calc_basis = 'V');

return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.HELP_ROLL_TMPL',
                                            to_char(SQLCODE));
      return 0;
END HELP_ROLL_TMPL;
----------------------------------------------------------------------------------------
FUNCTION MERGE_TMPL(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                   I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

BEGIN

-- first merge templates without upcharges
/* changing here if cost is chosen , since no upcharges will be associated with it and the pricing cost will be same as defined at the template*/
merge into future_cost_gtt fc
   using
    ( select inner.item,
             inner.location,
             inner.supplier,
             inner.origin_country_id,
             inner.active_date,
             inner.pricing_cost
        from (select distinct fc.item,
                     fc.location,
                     fc.supplier,
                     fc.origin_country_id,
                     fc.active_date,
                     DECODE(wch.first_applied, 'M',(NVL(fc.acquisition_cost,0) + (NVL(fc.acquisition_cost,0) * NVL(wch.margin_pct,0)/100)),
                     --DECODE(wch.first_applied, 'C',decode(wch.final_cost_ind,'Y', wch.cost,( wch.cost *(1-(nvl(fc.passthru_pct,0)/100)))))) pricing_cost 
                     DECODE(wch.first_applied, 'C',decode(wch.final_cost_ind,'Y', wch.cost,decode(fc.base_cost,fc.net_net_cost,wch.cost,( wch.cost *(1-(nvl(fc.passthru_pct,0)/100))))))) pricing_cost  
                from future_cost_gtt fc,
                     wf_cost_buildup_tmpl_head wch
               where fc.templ_id = wch.templ_id
                 and wch.first_applied in ('M','C')
                 and fc.store_type = 'F'
                 and not exists (select 1
                                   from wf_cost_buildup_tmpl_detail det
                                  where wch.templ_id = det.templ_id
                                     and rownum < 2)) inner) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.active_date)
   when matched then
   update
      set fc.pricing_cost = NVL(use_this.pricing_cost,0);

--  merge templates for %retail 
 /* if retail  is chosen , the value will be unit_retail in item_loc*/
merge into future_cost_gtt fc
   using
    ( select inner.item,
             inner.location,
             inner.supplier,
             inner.origin_country_id,
             inner.active_date,
             inner.pricing_cost
        from (select distinct fc.item,
                     fc.location,
                     fc.supplier,
                     fc.origin_country_id,
                     fc.active_date,
                     DECODE(wch.first_applied,'R',(il.unit_retail *(1-NVL(wch.margin_pct,0)/100))* mc.exchange_rate) pricing_cost 
                from future_cost_gtt fc,
                     wf_cost_buildup_tmpl_head wch,
                     item_loc il,
                      store st,
                      sups s,
                      ( select from_currency,
                        to_currency,
                        effective_date,
                        exchange_rate,
                        rank() over
                            (PARTITION BY from_currency, to_currency, exchange_type
                                 ORDER BY effective_date DESC) date_rank
                   from mv_currency_conversion_rates 
                  where exchange_type = 'C'
                          and effective_date   <= LP_vdate  ) mc 
               where fc.templ_id = wch.templ_id
                 and wch.first_applied = 'R'
                 and fc.store_type = 'F'
                 and fc.item = il.item
                 and fc.location = il.loc
                 and fc.location = st.store
                 and fc.supplier = s.supplier
                 and mc.from_currency          = st.currency_code
                 and mc.to_currency            = s.currency_code
                 and mc.date_rank              = 1 
                 and not exists (select 1
                                   from wf_cost_buildup_tmpl_detail det
                                  where wch.templ_id = det.templ_id
                                     and rownum < 2)) inner) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.active_date)
   when matched then
   update
      set fc.pricing_cost = NVL(use_this.pricing_cost,0);

-- merge templates with upcharges
merge into future_cost_gtt fc
   using
    ( select inner.item,
             inner.location,
             inner.supplier,
             inner.origin_country_id,
             inner.active_date,
             inner.pricing_cost
        from (select distinct fc.item,
                     fc.location,
                     fc.supplier,
                     fc.origin_country_id,
                     fc.active_date,
                     DECODE(wch.first_applied, 'M',
                             (NVL(fc.acquisition_cost,0) + ((NVL(fc.acquisition_cost,0) * (NVL(wch.margin_pct,0)/100)) + tmpl.value)),--apply margin first and then add upcharge
                              (NVL(fc.acquisition_cost,0) + tmpl.value) + ((NVL(fc.acquisition_cost,0) + tmpl.value) * (NVL(wch.margin_pct,0)/100))) pricing_cost --first apply upcharges to the acquisition cost and then apply margin 
                from (select item,
                             supplier,
                             origin_country_id,
                             location,
                             active_date,
                             SUM(NVL(value,0) * NVL(exchange_rate,1)) value
                        from future_cost_comp_gtt
                    group by item,
                             supplier,
                             origin_country_id,
                             location,
                             active_date) tmpl,
                     future_cost_gtt fc,
                     wf_cost_buildup_tmpl_head wch
               where fc.templ_id = wch.templ_id
                 and fc.store_type = 'F'
                 and fc.item = tmpl.item
                 and fc.location = tmpl.location
                 and fc.supplier = tmpl.supplier
                 and fc.active_date = tmpl.active_date
                 and fc.origin_country_id = tmpl.origin_country_id
                 and exists (select 'x'
                               from wf_cost_buildup_tmpl_detail det
                              where wch.templ_id = det.templ_id
                                and rownum < 2)) inner) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.active_date)
   when matched then
   update
      set fc.pricing_cost = NVL(use_this.pricing_cost,0);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.MERGE_TMPL',
                                            to_char(SQLCODE));
      return 0;
END MERGE_TMPL;
----------------------------------------------------------------------------------------
FUNCTION ROLL_TMPL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                   I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

   L_return NUMBER;

BEGIN

   L_return := HELP_ROLL_TMPL(O_error_message,
                              I_cost_event_process_id,
                              I_thread_id);
   if L_return = 0 then
      return L_return;
   end if;
   --
   L_return := MERGE_TMPL(O_error_message,
                          I_cost_event_process_id,
                          I_thread_id);
   if L_return = 0 then
      return L_return;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.ROLL_TMPL',
                                            to_char(SQLCODE));
      return 0;
END ROLL_TMPL;
----------------------------------------------------------------------------------------
FUNCTION ROLL_FORWARD(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      L_action                IN     COST_EVENT.ACTION%TYPE,
                      L_event_type            IN     COST_EVENT.EVENT_TYPE%TYPE,
                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                      I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return BOOLEAN is

   L_error_message      rtk_errors.rtk_text%type := NULL;
   L_system_options_row system_options%rowtype;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   
   -- call roll forward functions in sequence
   -- add processing number to gtt rows
   if FUTURE_COST_ROLLFWD_SQL.ADD_PROCESSING_SEQ_NO(O_error_message,
                                                    I_cost_event_process_id,
                                                    I_thread_id) = 0 then
      return FALSE;
   end if;

   -- roll forward cost changes across the timeline
   if FUTURE_COST_ROLLFWD_SQL.ROLL_COST_CHANGE(O_error_message,
                                               I_cost_event_process_id,
                                               I_thread_id) = 0 then
      return FALSE;
   end if;

   -- roll forward reclassification across the timeline
   if FUTURE_COST_ROLLFWD_SQL.ROLL_RECLASS(O_error_message,
                                           I_cost_event_process_id,
                                           I_thread_id) = 0 then
      return FALSE;
   end if;

   -- roll forward deals across the timeline
 

   if FUTURE_COST_ROLLFWD_SQL.ROLL_DEAL(O_error_message,
                                        I_cost_event_process_id,
                                        I_thread_id) = 0 then
      return FALSE;
   end if;


   --seed wh records to w/f stores
   --NOTE: processing_seq_no number is not correct for w/f stores brought in by this logic
   --      this is OK for logic that follows, but if a change is made that needs the
   --      processing_seq_no correct after this point, this will have to be addressed.

      if FUTURE_COST_ROLLFWD_SQL.SETUP_WHOLESALE_FRANCHISE(O_error_message,
                                                           I_cost_event_process_id,
                                                           I_thread_id) = 0 then
         return FALSE;
      end if;

   -- roll forward deal passthru for the wholesale/franchise stores.
   if FUTURE_COST_ROLLFWD_SQL.ROLL_DEAL_PASSTHRU(O_error_message,
                                                 I_cost_event_process_id,
                                                 I_thread_id) = 0 then
      return FALSE;
   end if;

   -- roll forward ELC across the time line
   if L_system_options_row.elc_ind = 'Y' then
      if FUTURE_COST_ROLLFWD_SQL.ROLL_ELC(O_error_message,
                                          I_cost_event_process_id,
                                          I_thread_id) = 0 then
         return FALSE;
      end if;
   end if;

   -- roll forward cost templates

      if FUTURE_COST_ROLLFWD_SQL.ROLL_TMPL(O_error_message,
                                           I_cost_event_process_id,
                                           I_thread_id) = 0 then
         return FALSE;
      end if;


   --  Do not allow negative cost
   if CONVERT_NEGATIVE_COST(O_error_message,
                            I_cost_event_process_id,
                            I_thread_id) = 0 then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.ROLL_FORWARD',
                                            to_char(SQLCODE));
      return FALSE;
END ROLL_FORWARD;
----------------------------------------------------------------------------------------
FUNCTION ADD_PROCESSING_SEQ_NO(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

BEGIN

   merge into future_cost_gtt gtt
   using
    ( select g.item, g.supplier, g.origin_country_id, g.location, g.active_date,
             rank() over(partition by g.item, g.supplier, g.origin_country_id, g.location
                         order by g.active_date) as processing_seq_no
       from future_cost_gtt g) use_this
   on (    gtt.item                  = use_this.item
       and gtt.supplier              = use_this.supplier
       and gtt.origin_country_id     = use_this.origin_country_id
       and gtt.location              = use_this.location
       and gtt.active_date           = use_this.active_date)
   when matched then
   update
      set gtt.processing_seq_no  = use_this.processing_seq_no,
          gtt.elc_amt         = null;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.ADD_PROCESSING_SEQ_NO',
                                            to_char(SQLCODE));
      return 0;
END ADD_PROCESSING_SEQ_NO;
----------------------------------------------------------------------------------------
FUNCTION ROLL_COST_CHANGE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

BEGIN

   merge into future_cost_gtt gtt
   using ( SELECT calc_cost, nic, ebc, wt, dct, item, supplier, origin_country_id, location, processing_seq_no
             FROM future_cost_gtt
            WHERE 1 = 1
            AND NVL(store_type,'-999') <> 'F'
             MODEL
               PARTITION BY (item, supplier, origin_country_id, location)
               DIMENSION BY (processing_seq_no )
               MEASURES (base_cost calc_cost, cost_change cc, negotiated_item_cost nic, extended_base_cost ebc, wac_tax wt, default_costing_type dct)
               RULES SEQUENTIAL ORDER (
                              calc_cost[ANY] ORDER BY processing_seq_no ASC=
                                 decode(cv(processing_seq_no),
                                        1, calc_cost[cv(processing_seq_no)],
                                        decode(cc[cv(processing_seq_no)],
                                               null, calc_cost[cv(processing_seq_no)-1],
                                               calc_cost[cv(processing_seq_no)])),
                              --roll forward negotiated_item_cost
                              nic[ANY] ORDER BY processing_seq_no ASC=
                                 decode(cv(processing_seq_no),
                                        1, nic[cv(processing_seq_no)],
                                        decode(cc[cv(processing_seq_no)],
                                               null, nic[cv(processing_seq_no)-1],
                                               nic[cv(processing_seq_no)])),
                              --roll forward extended_base_cost
                              ebc[ANY] ORDER BY processing_seq_no ASC=
                                 decode(cv(processing_seq_no),
                                        1, ebc[cv(processing_seq_no)],
                                        decode(cc[cv(processing_seq_no)],
                                               null, ebc[cv(processing_seq_no)-1],
                                               ebc[cv(processing_seq_no)])),
                              --roll forward wac_tax
                              wt[ANY] ORDER BY processing_seq_no ASC=
                                 decode(cv(processing_seq_no),
                                        1, wt[cv(processing_seq_no)],
                                        decode(cc[cv(processing_seq_no)],
                                               null, wt[cv(processing_seq_no)-1],
                                               wt[cv(processing_seq_no)]))
               )) use_this
   on (    gtt.item               = use_this.item
       and gtt.supplier           = use_this.supplier
       and gtt.origin_country_id  = use_this.origin_country_id
       and gtt.location           = use_this.location
       and gtt.processing_seq_no  = use_this.processing_seq_no)
   when matched then
   update
      set gtt.base_cost            = use_this.calc_cost,
          gtt.negotiated_item_cost = use_this.nic,
          gtt.extended_base_cost   = use_this.ebc,
          gtt.wac_tax              = use_this.wt,
          --ok to overwrite these since promo will be recalculated each time
          gtt.net_cost          = DECODE(use_this.dct,
                                         'NIC', NVL(use_this.nic, 0),
                                         'BC', NVL(use_this.calc_cost, 0),
                                         NVL(use_this.calc_cost, 0)),
          gtt.net_net_cost      = DECODE(use_this.dct,
                                         'NIC', NVL(use_this.nic, 0),
                                         'BC', NVL(use_this.calc_cost, 0),
                                         NVL(use_this.calc_cost, 0)),
          gtt.dead_net_net_cost = DECODE(use_this.dct,
                                         'NIC', NVL(use_this.nic, 0),
                                         'BC', NVL(use_this.calc_cost, 0),
                                         NVL(use_this.calc_cost, 0)),
          gtt.pricing_cost      = DECODE(use_this.dct,
                                         'NIC', NVL(use_this.nic, 0),
                                         'BC', NVL(use_this.calc_cost, 0),
                                         NVL(use_this.calc_cost, 0)) + NVL(use_this.wt, 0);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.ROLL_COST_CHANGE',
                                            to_char(SQLCODE));
      return 0;
END ROLL_COST_CHANGE;
----------------------------------------------------------------------------------------
FUNCTION ROLL_RECLASS(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_cost_event_process_id   IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                      I_thread_id               IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

BEGIN

   merge into future_cost_gtt gtt
   using ( SELECT calc_dept,
                  calc_class,
                  calc_subclass,
                  calc_division,
                  calc_group_no,
                  item,
                  supplier,
                  origin_country_id,
                  location,
                  processing_seq_no
             FROM future_cost_gtt
             WHERE 1 = 1
             MODEL
               PARTITION BY (item, supplier, origin_country_id, location)
               DIMENSION BY (processing_seq_no )
               MEASURES ( dept calc_dept,
                          class calc_class,
                          subclass calc_subclass,
                          division calc_division,
                          group_no calc_group_no,
                          reclass_no calc_reclass)
               RULES SEQUENTIAL ORDER (
                              calc_dept[ANY] ORDER BY processing_seq_no ASC=
                                 DECODE(cv(processing_seq_no),
                                        1, calc_dept[cv(processing_seq_no)],
                                        DECODE(calc_reclass[cv(processing_seq_no)],
                                               NULL, calc_dept[cv(processing_seq_no)-1],
                                               calc_dept[cv(processing_seq_no)])),
                              calc_class[ANY] ORDER BY processing_seq_no ASC=
                                 DECODE(cv(processing_seq_no),
                                        1, calc_class[cv(processing_seq_no)],
                                        DECODE(calc_reclass[cv(processing_seq_no)],
                                               NULL, calc_class[cv(processing_seq_no)-1],
                                               calc_class[cv(processing_seq_no)])),
                              calc_subclass[ANY] ORDER BY processing_seq_no ASC=
                                 DECODE(cv(processing_seq_no),
                                        1, calc_subclass[cv(processing_seq_no)],
                                        DECODE(calc_reclass[cv(processing_seq_no)],
                                               NULL, calc_subclass[cv(processing_seq_no)-1],
                                               calc_subclass[cv(processing_seq_no)])),
                              calc_division[ANY] ORDER BY processing_seq_no ASC=
                                 DECODE(cv(processing_seq_no),
                                        1, calc_division[cv(processing_seq_no)],
                                        DECODE(calc_reclass[cv(processing_seq_no)],
                                               NULL, calc_division[cv(processing_seq_no)-1],
                                               calc_division[cv(processing_seq_no)])),
                              calc_group_no[ANY] ORDER BY processing_seq_no ASC=
                                 DECODE(cv(processing_seq_no),
                                        1, calc_group_no[cv(processing_seq_no)],
                                        DECODE(calc_reclass[cv(processing_seq_no)],
                                               NULL, calc_group_no[cv(processing_seq_no)-1],
                                               calc_group_no[cv(processing_seq_no)]))
               )) use_this
   on (    gtt.item                  = use_this.item
       and gtt.supplier              = use_this.supplier
       and gtt.origin_country_id     = use_this.origin_country_id
       and gtt.location              = use_this.location
       and gtt.processing_seq_no     = use_this.processing_seq_no)
   when matched then
   update
      set gtt.dept     = use_this.calc_dept,
          gtt.class    = use_this.calc_class,
          gtt.subclass = use_this.calc_subclass,
          gtt.division = use_this.calc_division,
          gtt.group_no = use_this.calc_group_no;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.ROLL_RECLASS',
                                            TO_CHAR(SQLCODE));
      return 0;
END ROLL_RECLASS;
----------------------------------------------------------------------------------------
FUNCTION HELP_ROLL_DEAL_1(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

BEGIN

   -- populate helper table for buy one get one deal components
   -- retrieve the unit cost of the get item if the item is the buy item
   -- retrieve the unit cost of the buy item if the item is the get item
   insert into future_cost_buyget_help_gtt (
      future_cost_gtt_rowid,
      deal_id,
      deal_detail_id,
      other_item_unit_cost
   )
   select distinct fc.rowid,
                   fcd.deal_id,
                   fcd.deal_detail_id,
                   --Retrieve the base_cost or negotiated_item_cost for the buy or get item
                   --depending on the default_costing_type.
                   --If default_costing_type is 'BC' or NULL then use the base_cost
                   --If default_costing_type is 'NIC' then use the negotiated_item_cost
                   first_value(decode(NVL(fc_other.default_costing_type, 'BC'),
                                      'BC', fc_other.base_cost,
                                      NVL(fc_other.negotiated_item_cost, 0))) over
                       (partition by fc_other.item,
                                     fc_other.supplier,
                                     fc_other.origin_country_id
                            order by fc_other.active_date desc)
     from deal_item_loc_explode_gtt fcd,
          future_cost_gtt fc,
          future_cost_gtt fc_other
    where fcd.threshold_value_type = 'Q'  -- quantity
      and fcd.qty_thresh_get_item != fcd.qty_thresh_buy_item
      and fcd.deal_head_type      != 'O'  -- Exclude PO specific deals
      --
      and fc.item                  = fcd.item
      and fc.supplier              = fcd.supplier
      and fc.origin_country_id     = fcd.origin_country_id
      and fc.location              = fcd.location
      and fc.active_date          >= fcd.active_date
      and fc.active_date          <= fcd.close_date
      and NVL(fc.store_type, 999) not in ('F')
      --
      and fc_other.item(+)                   = decode(fcd.item,
                                                 fcd.qty_thresh_get_item, fcd.qty_thresh_buy_item,
                                                 fcd.qty_thresh_buy_item, fcd.qty_thresh_get_item)
      and fc_other.supplier(+)               = fcd.supplier
      and fc_other.origin_country_id(+)      = fcd.origin_country_id
      and fc_other.location(+)               = fcd.location
      and fc_other.active_date(+)           <= LP_vdate
      and NVL(fc_other.store_type, 999) not in ('F');

   -- When the Buy or Get item is not in the current cost event then that item
   -- would not exists in the future_cost_gtt, therefore we need to extract the
   -- base_cost from the actual future_cost table.
   update future_cost_buyget_help_gtt fcbg
      set other_item_unit_cost = (select distinct
                                         --Retrieve the base_cost or negotiated_item_cost for the buy or get item
                                         --depending on the default_costing_type.
                                         --If default_costing_type is 'BC' or NULL then use the base_cost
                                         --If default_costing_type is 'NIC' then use the negotiated_item_cost
                                         first_value(decode(NVL(fc_other.default_costing_type, 'BC'),
                                                            'BC', fc_other.base_cost,
                                                            NVL(fc_other.negotiated_item_cost, 0)))
                                            over (partition by fc_other.item,
                                                               fc_other.supplier,
                                                               fc_other.origin_country_id
                                                      order by fc_other.active_date desc)
                                    from deal_item_loc_explode_gtt fcd,
                                         future_cost_gtt fc,
                                         future_cost fc_other
                                   where fc.rowid                 = fcbg.future_cost_gtt_rowid
                                     and fcd.threshold_value_type = 'Q'  -- quantity
                                     and fcd.qty_thresh_get_item != fcd.qty_thresh_buy_item
                                     and fcd.deal_head_type      != 'O'  -- Exclude PO specific deals
                                     --
                                     and fc.item                  = fcd.item
                                     and fc.supplier              = fcd.supplier
                                     and fc.origin_country_id     = fcd.origin_country_id
                                     and fc.location              = fcd.location
                                     and fc.active_date          >= fcd.active_date
                                     and fc.active_date          <= fcd.close_date
                                     --
                                     and fc_other.item = decode(fcd.item,
                                                                fcd.qty_thresh_get_item, fcd.qty_thresh_buy_item,
                                                                fcd.qty_thresh_buy_item, fcd.qty_thresh_get_item)
                                     and fc_other.supplier              = fc.supplier
                                     and fc_other.origin_country_id     = fc.origin_country_id
                                     and fc_other.location              = fc.location
                                     and fc_other.active_date          <= LP_vdate)
    where fcbg.other_item_unit_cost is NULL;

   ---
   insert into future_cost_working_gtt    (  item,
                                             supplier,
                                             origin_country_id,
                                             location,
                                             loc_type,
                                             active_date,
                                             base_cost,
                                             net_cost,
                                             net_net_cost,
                                             dead_net_net_cost,
                                             pricing_cost,
                                             calc_date,
                                             start_ind,
                                             primary_supp_country_ind,
                                             currency_code,
                                             elc_amt,
                                             negotiated_item_cost,
                                             extended_base_cost,
                                             wac_tax,
                                             default_costing_type,
                                             processing_seq_no,
                                             deal_id,
                                             deal_detail_id,
                                             deal_active_date,
                                             deal_close_date,
                                             cost_appl_ind,
                                             price_cost_appl_ind,
                                             deal_class,
                                             threshold_value_type,
                                             bg_buy_item,
                                             bg_get_type,
                                             bg_get_value,
                                             bg_buy_qty,
                                             bg_recur_ind,
                                             bg_buy_target,
                                             bg_buy_avg_loc,
                                             bg_get_item,
                                             bg_get_qty,
                                             bg_free_item_unit_cost,
                                             deal_head_type,
                                             partner_type,
                                             partner_id,
                                             create_datetime,
                                             deal_detail_application_order,
                                             threshold_value,
                                             helper_get_item_unit_cost,
                                             helper_buy_item_unit_cost,
                                             helper_get_item_unit_cost_init,
                                             helper_new_get_unit_cost_n,
                                             helper_new_buy_unit_cost_n,
                                             helper_new_get_unit_cost_nn,
                                             helper_new_buy_unit_cost_nn,
                                             helper_new_get_unit_cost_dnn,
                                             helper_new_buy_unit_cost_dnn,
                                             helper_get_qty,
                                             helper_buy_qty,
                                             helper_buy_target,
                                             helper_get_target,
                                             helper_thresh_get_qty,
                                             helper_total_discount,
                                             helper_get_value,
                                             helper_get_discount_share,
                                             helper_buy_discount_share,
                                             helper_buy_plus_get_qty,
                                             get_free_discount)
                select distinct item,
                       supplier,
                       origin_country_id,
                       location,
                       loc_type,
                       active_date,
                       base_cost,
                       net_cost,
                       net_net_cost,
                       dead_net_net_cost,
                       pricing_cost,
                       calc_date,
                       start_ind,
                       primary_supp_country_ind,
                       currency_code,
                       elc_amt,
                       negotiated_item_cost,
                       extended_base_cost,
                       wac_tax,
                       default_costing_type,
                       processing_seq_no,
                       deal_id,
                       deal_detail_id,
                       deal_active_date,
                       deal_close_date,
                       cost_appl_ind,
                       price_cost_appl_ind,
                       deal_class,
                       threshold_value_type,
                       bg_buy_item,
                       bg_get_type,
                       bg_get_value,
                       bg_buy_qty,
                       bg_recur_ind,
                       bg_buy_target,
                       bg_buy_avg_loc,
                       bg_get_item,
                       bg_get_qty,
                       bg_free_item_unit_cost,
                       deal_head_type,
                       partner_type,
                       partner_id,
                       create_datetime,
                       deal_detail_application_order,
                       threshold_value * exchange_rate,
                       case  -- helper_get_item_unit_cost
                          --case 1
                          when threshold_value_type = 'Q' and   -- quantity
                               bg_get_type = 'X' then   -- free
                                  bg_free_item_unit_cost * exchange_rate
                          --case 2
                          when threshold_value_type = 'Q' and   -- quantity
                               item = bg_buy_item and
                               bg_get_type != 'X' and   -- percent, amount or fixed amount
                               bg_get_item = bg_buy_item then
                                  --If default_costing_type is 'BC' or NULL then use the base_cost
                                  --If default_costing_type is 'NIC' then use the negotiated_item_cost
                                  decode(NVL(default_costing_type, 'BC'), 'BC', base_cost, NVL(negotiated_item_cost, 0))
                          --case 3
                          when threshold_value_type = 'Q' and
                               item = bg_buy_item and
                               bg_get_type != 'X' and
                               bg_get_item != bg_buy_item then
                                  nvl(other_item_unit_cost,-999)
                          --case 4
                          when threshold_value_type = 'Q' and
                               item = bg_get_item and
                               bg_get_type != 'X' then
                                  --If default_costing_type is 'BC' or NULL then use the base_cost
                                  --If default_costing_type is 'NIC' then use the negotiated_item_cost
                                  decode(NVL(default_costing_type, 'BC'), 'BC', base_cost, NVL(negotiated_item_cost, 0))
                          else
                             -999
                       end helper_get_item_unit_cost,
                       helper_buy_item_unit_cost,
                       helper_get_item_unit_cost_init,
                       helper_new_get_unit_cost_n,
                       helper_new_buy_unit_cost_n,
                       helper_new_get_unit_cost_nn,
                       helper_new_buy_unit_cost_nn,
                       helper_new_get_unit_cost_dnn,
                       helper_new_buy_unit_cost_dnn,
                       decode(sign(helper_get_qty), -1, -999, helper_get_qty * (bg_buy_avg_loc/bg_buy_target)),
                       helper_buy_qty,
                       helper_buy_target,
                       helper_get_target,
                       helper_thresh_get_qty,
                       helper_total_discount,
                       helper_get_value,
                       helper_get_discount_share,
                       helper_buy_discount_share,
                       helper_buy_plus_get_qty,
                       get_free_discount
                from (select /*+ LEADING(FCD) */ fc.item,
                             fc.supplier,
                             fc.origin_country_id,
                             fc.location,
                             fc.loc_type,
                             fc.active_date,
                             fc.base_cost,
                             fc.net_cost,
                             fc.net_net_cost,
                             fc.dead_net_net_cost,
                             fc.pricing_cost,
                             fc.calc_date,
                             fc.start_ind,
                             fc.primary_supp_country_ind,
                             fc.currency_code,
                             fc.elc_amt,
                             fc.negotiated_item_cost,
                             fc.extended_base_cost,
                             fc.wac_tax,
                             fc.default_costing_type,
                             fc.processing_seq_no,
                             --
                             fcd.deal_id,
                             fcd.deal_detail_id,
                             fcd.active_date deal_active_date,
                             fcd.close_date deal_close_date,
                             fcd.cost_appl_ind,
                             fcd.price_cost_appl_ind,
                             fcd.deal_class,
                             fcd.threshold_value_type,
                             fcd.qty_thresh_buy_item bg_buy_item,
                             fcd.qty_thresh_get_type bg_get_type,
                             fcd.qty_thresh_get_value bg_get_value,
                             fcd.qty_thresh_buy_qty bg_buy_qty,
                             fcd.qty_thresh_recur_ind bg_recur_ind,
                             fcd.qty_thresh_buy_target bg_buy_target,
                             fcd.qty_thresh_buy_avg_loc bg_buy_avg_loc,
                             fcd.qty_thresh_get_item bg_get_item,
                             fcd.qty_thresh_get_qty bg_get_qty,
                             fcd.qty_thresh_free_item_unit_cost bg_free_item_unit_cost,
                             fcd.deal_head_type,
                             fcd.partner_type,
                             fcd.partner_id,
                             fcd.create_datetime,
                             fcd.deal_detail_application_order,
                             --
                             dt.value threshold_value,
                             ---buy/get helpers
                             -- retrieve the unit cost for the get item
                             --This is done here for -999 check -- redone in outer query to make sure convert is done
                             case  -- helper_get_item_unit_cost
                                --case 1
                                when fcd.threshold_value_type = 'Q' and   -- quantity
                                     fcd.qty_thresh_get_type = 'X' then   -- free
                                        fcd.qty_thresh_free_item_unit_cost --convert in outer query
                                --case 2
                                when fcd.threshold_value_type = 'Q' and   -- quantity
                                     fcd.item = fcd.qty_thresh_buy_item and
                                     fcd.qty_thresh_get_type != 'X' and   -- percent, amount or fixed amount
                                     fcd.qty_thresh_get_item = fcd.qty_thresh_buy_item then
                                        --If default_costing_type is 'BC' or NULL then use the base_cost
                                        --If default_costing_type is 'NIC' then use the negotiated_item_cost
                                        decode(NVL(fc.default_costing_type, 'BC'), 'BC', fc.base_cost, NVL(fc.negotiated_item_cost, 0))
                                --case 3
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.item = fcd.qty_thresh_buy_item and
                                     fcd.qty_thresh_get_type != 'X' and
                                     fcd.qty_thresh_get_item != fcd.qty_thresh_buy_item then
                                        nvl(bg_help.other_item_unit_cost,-999)
                                --case 4
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.item = fcd.qty_thresh_get_item and
                                     fcd.qty_thresh_get_type != 'X' then
                                        --If default_costing_type is 'BC' or NULL then use the base_cost
                                        --If default_costing_type is 'NIC' then use the negotiated_item_cost
                                        decode(NVL(fc.default_costing_type, 'BC'), 'BC', fc.base_cost, NVL(fc.negotiated_item_cost, 0))
                                else
                                   -999
                             end helper_get_item_unit_cost,
                             -- retrieve the unit cost for the buy item
                             case  -- helper_buy_item_unit_cost
                                --case 1
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_buy_item = fcd.item then
                                        --If default_costing_type is 'BC' or NULL then use the base_cost
                                        --If default_costing_type is 'NIC' then use the negotiated_item_cost
                                        decode(NVL(fc.default_costing_type, 'BC'), 'BC', fc.base_cost, NVL(fc.negotiated_item_cost, 0))
                                --case 2
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_get_item = fcd.item and
                                     fcd.qty_thresh_get_item = fcd.qty_thresh_buy_item then
                                        --If default_costing_type is 'BC' or NULL then use the base_cost
                                        --If default_costing_type is 'NIC' then use the negotiated_item_cost
                                        decode(NVL(fc.default_costing_type, 'BC'), 'BC', fc.base_cost, NVL(fc.negotiated_item_cost, 0))
                                --case 3
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_get_item = fcd.item and
                                     fcd.qty_thresh_buy_item != fcd.qty_thresh_get_item then
                                        nvl(bg_help.other_item_unit_cost,-999)
                                else
                                   -999
                             end helper_buy_item_unit_cost,
                             -- retrieve the initial cost for the get item
                             --This is done here for -999 check -- redone in outer query to make sure convert is done
                             case  -- helper_get_item_unit_cost_init
                                --case 1
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_buy_item = fcd.qty_thresh_get_item then
                                        --If default_costing_type is 'BC' or NULL then use the base_cost
                                        --If default_costing_type is 'NIC' then use the negotiated_item_cost
                                        decode(NVL(fc.default_costing_type, 'BC'), 'BC', fc.base_cost, NVL(fc.negotiated_item_cost, 0))
                                --case 2
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_buy_item != fcd.qty_thresh_get_item and
                                     fcd.qty_thresh_get_item = fcd.item then
                                        --If default_costing_type is 'BC' or NULL then use the base_cost
                                        --If default_costing_type is 'NIC' then use the negotiated_item_cost
                                        decode(NVL(fc.default_costing_type, 'BC'), 'BC', fc.base_cost, NVL(fc.negotiated_item_cost, 0))
                                --case 3
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_buy_item != fcd.qty_thresh_get_item and
                                     fcd.qty_thresh_get_item != fcd.item then
                                        nvl(bg_help.other_item_unit_cost,-999)
                                else
                                   -999
                             end helper_get_item_unit_cost_init,
                             null helper_new_get_unit_cost_n,
                             null helper_new_buy_unit_cost_n,
                             null helper_new_get_unit_cost_nn,
                             null helper_new_buy_unit_cost_nn,
                             null helper_new_get_unit_cost_dnn,
                             null helper_new_buy_unit_cost_dnn,
                             -- compute for the quantity of the get item
                             case  -- helper_get_qty
                                --case 1
                                when fcd.threshold_value_type = 'Q' and   -- quantity
                                     fcd.qty_thresh_recur_ind = 'N' and
                                     fcd.qty_thresh_buy_item = fcd.qty_thresh_get_item and
                                     fcd.qty_thresh_buy_target > fcd.qty_thresh_buy_qty and
                                     fcd.qty_thresh_get_qty > (fcd.qty_thresh_buy_target - fcd.qty_thresh_buy_qty) then
                                        fcd.qty_thresh_buy_target - fcd.qty_thresh_buy_qty
                                --case 2
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_recur_ind = 'N' and
                                     fcd.qty_thresh_buy_item = fcd.qty_thresh_get_item and
                                     fcd.qty_thresh_buy_target <= fcd.qty_thresh_buy_qty and
                                     0 > (fcd.qty_thresh_buy_target - fcd.qty_thresh_buy_qty) then
                                        fcd.qty_thresh_buy_target - fcd.qty_thresh_buy_qty
                                --case 3
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_recur_ind = 'N' and
                                     fcd.qty_thresh_buy_item = fcd.qty_thresh_get_item and
                                     fcd.qty_thresh_buy_target > fcd.qty_thresh_buy_qty and
                                     fcd.qty_thresh_get_qty <= (fcd.qty_thresh_buy_target - fcd.qty_thresh_buy_qty) then
                                        fcd.qty_thresh_get_qty
                                ---------------------
                                --case 4
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_recur_ind = 'N' and
                                     fcd.qty_thresh_buy_item != fcd.qty_thresh_get_item and
                                     fcd.qty_thresh_buy_target > fcd.qty_thresh_buy_qty and
                                     fcd.qty_thresh_get_qty > fcd.qty_thresh_buy_target then
                                        fcd.qty_thresh_buy_target
                                --case 5
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_recur_ind = 'N' and
                                     fcd.qty_thresh_buy_item != fcd.qty_thresh_get_item and
                                     fcd.qty_thresh_buy_target <= fcd.qty_thresh_buy_qty and
                                     0 > fcd.qty_thresh_buy_target then
                                        fcd.qty_thresh_buy_target
                                --case 6
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_recur_ind = 'N' and
                                     fcd.qty_thresh_buy_item != fcd.qty_thresh_get_item and
                                     fcd.qty_thresh_buy_target > fcd.qty_thresh_buy_qty and
                                     fcd.qty_thresh_get_qty <= fcd.qty_thresh_buy_target then
                                        fcd.qty_thresh_get_qty
                                ---------------------
                                --case 7
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_recur_ind = 'Y' and
                                     fcd.qty_thresh_buy_item = fcd.qty_thresh_get_item then
                                        ((floor(fcd.qty_thresh_buy_target /
                                                (fcd.qty_thresh_buy_qty+fcd.qty_thresh_get_qty)) *
                                           fcd.qty_thresh_get_qty) +
                                         (greatest((fcd.qty_thresh_buy_target -
                                            ((fcd.qty_thresh_buy_qty+fcd.qty_thresh_get_qty) *
                                              (floor(fcd.qty_thresh_buy_target /
                                                     (fcd.qty_thresh_buy_qty+fcd.qty_thresh_get_qty)))))
                                            - fcd.qty_thresh_buy_qty, 0)))
                                --case 8
                                when fcd.threshold_value_type = 'Q' and
                                     fcd.qty_thresh_recur_ind = 'Y' and
                                     fcd.qty_thresh_buy_item != fcd.qty_thresh_get_item then
                                        (greatest(fcd.qty_thresh_buy_target,
                                                  floor(fcd.qty_thresh_buy_target / fcd.qty_thresh_buy_qty) *
                                                  fcd.qty_thresh_get_qty))
                                else
                                   -999
                             end helper_get_qty,
                             qty_thresh_buy_qty        helper_buy_qty,
                             qty_thresh_buy_target     helper_buy_target,
                             qty_thresh_buy_target     helper_get_target,
                             qty_thresh_get_qty        helper_thresh_get_qty,
                             null                      helper_total_discount,
                             qty_thresh_get_value      helper_get_value,
                             null                      helper_get_discount_share,
                             null                      helper_buy_discount_share,
                             qty_thresh_buy_qty + qty_thresh_get_qty         helper_buy_plus_get_qty,
                             --
                             rank() over(partition by fcd.item, fcd.supplier, fcd.origin_country_id,
                                                      fcd.location, decode(dh.billing_type, 'OI', fc.active_date, fcd.active_date)
                                order by
                                   decode(nvl(fcd.deal_class,'ZZ'), 'EX', 1, 2),
                                   decode(nvl(fcd.deal_class,'ZZ'), 'EX', fcd.setup_merch_level, 1),
                                   decode(nvl(fcd.deal_class,'ZZ'), 'EX', fcd.setup_org_level, 1),
                                   decode(nvl(fcd.deal_class,'ZZ'), 'EX', fcd.deal_id, 1)) exclusive_rank,
                             --
                             bg_help.other_item_unit_cost,
                             first_value(curr.exchange_rate) over
                                (partition by fc.item, fc.location, fc.supplier, fc.origin_country_id, fc.active_date
                                  order by curr.effective_date desc) exchange_rate,
                             fcd.get_free_discount
                        from future_cost_gtt fc,
                             deal_item_loc_explode_gtt fcd,
                             deal_head dh,
                             deal_threshold dt,
                             future_cost_buyget_help_gtt bg_help,
                             mv_currency_conversion_rates curr,
                             wh
                       where fc.item                        = fcd.item
                         and fc.supplier                    = fcd.supplier
                         and fc.origin_country_id           = fcd.origin_country_id
                         and fc.location                    = fcd.location
                         and fc.active_date                >= fcd.active_date
                         and fc.active_date                <= nvl(fcd.close_date, fc.active_date)
                         --item/loc hier still lines up with setup hier
                         and fc.division                    = nvl(fcd.setup_division, fc.division)
                         and fc.group_no                    = nvl(fcd.setup_group_no, fc.group_no)
                         and fc.dept                        = nvl(fcd.setup_dept, fc.dept)
                         and fc.class                       = nvl(fcd.setup_class, fc.class)
                         and fc.subclass                    = nvl(fcd.setup_subclass, fc.subclass)
                         and nvl(fc.item_parent,'-1')       = nvl(fcd.setup_item_parent, nvl(fc.item_parent,'-1'))
                         and nvl(fc.item_grandparent,'-1')  = nvl(fcd.setup_item_grandparent, nvl(fc.item_grandparent,'-1'))
                         and nvl(fc.diff_1,'-1')            = nvl(fcd.setup_diff_1, nvl(fc.diff_1,'-1'))
                         and nvl(fc.diff_2,'-1')            = nvl(fcd.setup_diff_2, nvl(fc.diff_2,'-1'))
                         and nvl(fc.diff_3,'-1')            = nvl(fcd.setup_diff_3, nvl(fc.diff_3,'-1'))
                         and nvl(fc.diff_4,'-1')            = nvl(fcd.setup_diff_4, nvl(fc.diff_4,'-1'))
                         and nvl(fc.chain,'-1')             = nvl(fcd.setup_chain, nvl(fc.chain,'-1'))
                         and nvl(fc.area,'-1')              = nvl(fcd.setup_area, nvl(fc.area,'-1'))
                         and nvl(fc.region,'-1')            = nvl(fcd.setup_region, nvl(fc.region,'-1'))
                         and nvl(fc.district,'-1')          = nvl(fcd.setup_district, nvl(fc.district,'-1'))
                         --
                         and nvl(fc.supp_hier_lvl_1,'-1')   = decode(fcd.partner_type,'S1',fcd.partner_id,nvl(fc.supp_hier_lvl_1,'-1'))
                         and nvl(fc.supp_hier_lvl_2,'-1')   = decode(fcd.partner_type,'S2',fcd.partner_id,nvl(fc.supp_hier_lvl_2,'-1'))
                         and nvl(fc.supp_hier_lvl_3,'-1')   = decode(fcd.partner_type,'S3',fcd.partner_id,nvl(fc.supp_hier_lvl_3,'-1'))
                         --
                         and fcd.setup_location             = wh.physical_wh(+)
                         and fc.location                    = decode(fcd.loc_type, 'W', wh.wh,
                                                                     nvl(fcd.setup_location, fc.location))
                         --
                         and fcd.deal_id                    = dh.deal_id
                         --
                         and fcd.deal_id                    = dt.deal_id(+)
                         and fcd.deal_detail_id             = dt.deal_detail_id(+)
                         --
                         and fcd.deal_head_type            != 'O'  -- Exclude PO specific deals
                         --
                         and dt.target_level_ind(+)         = 'Y'
                         --
                         and fc.rowid                       = bg_help.future_cost_gtt_rowid(+)
                         --
                         and dh.currency_code      = curr.from_currency
                         and fc.currency_code      = curr.to_currency
                         and curr.exchange_type     = 'C'
                         and curr.effective_date   <= fc.active_date
             )
       where exclusive_rank = 1
         and ((threshold_value_type != 'Q')
               or
              (    threshold_value_type = 'Q'
               and bg_buy_target >= bg_buy_qty
               and helper_get_item_unit_cost      != -999
               and helper_get_item_unit_cost_init != -999
               and helper_buy_item_unit_cost      != -999
               and helper_get_qty                 != -999
              ));

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.HELP_ROLL_DEAL_1',
                                            to_char(SQLCODE));
      return 0;
END HELP_ROLL_DEAL_1;
----------------------------------------------------------------------------------------
FUNCTION HELP_ROLL_DEAL_2(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

   L_return                NUMBER;
   L_so_rowtype           system_options%ROWTYPE;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_so_rowtype) = FALSE then
      return 0;
   end if;

   merge into future_cost_working_gtt gtt
   using (select distinct work.item,
                 work.supplier,
                 work.origin_country_id,
                 work.location,
                 work.active_date,
                 work.deal_id,
                 work.deal_detail_id,
                 work.helper_get_qty,
                 work.helper_get_item_unit_cost_init,
                 work.net_cost,
                 work.net_net_cost,
                 work.dead_net_net_cost,
                 case
                   when work.threshold_value_type = 'Q' and
                        work.bg_buy_item = work.bg_get_item then
                           work.helper_buy_target - work.helper_get_qty
                   when work.threshold_value_type = 'Q' and
                        work.bg_buy_item != work.bg_get_item and
                        work.bg_buy_item = work.item then
                           work.helper_buy_target
                 end new_buy_divisor,
                 case
                   -- case 1
                   when work.threshold_value_type = 'Q' and
                        work.bg_get_type = 'X' then
                           work.helper_get_item_unit_cost * work.helper_get_qty
                   -- case 2
                   when work.threshold_value_type = 'Q' and
                        work.bg_get_type = 'P' then
                           work.helper_get_item_unit_cost * (work.helper_get_value / 100) * work.helper_get_qty
                   -- case 3
                   when work.threshold_value_type = 'Q' and
                        work.bg_get_type = 'A' and
                        work.helper_get_value < helper_get_item_unit_cost then
                           work.helper_get_value * work.helper_get_qty
                   -- case 4
                   when work.threshold_value_type = 'Q' and
                        work.bg_get_type = 'A' and
                        work.helper_get_value >= helper_get_item_unit_cost then
                           work.helper_get_item_unit_cost * work.helper_get_qty
                   -- case 5
                   when work.threshold_value_type = 'Q' and
                        work.bg_get_type = 'F' and
                        work.helper_get_value < helper_get_item_unit_cost then
                           (work.helper_get_item_unit_cost - work.helper_get_value) * work.helper_get_qty
                   -- case 6
                   when work.threshold_value_type = 'Q' and
                        work.bg_get_type = 'F' and
                        work.helper_get_value >= helper_get_item_unit_cost then
                           0
                   else
                       -999
                 end helper_total_discount,
                 case
                   -- case 1
                   when work.threshold_value_type = 'Q' and
                        work.bg_buy_item = work.bg_get_item then
                           ((work.helper_get_item_unit_cost * work.helper_get_qty) /
                            ((work.helper_buy_item_unit_cost * (work.helper_buy_target - work.helper_get_qty)) +
                             (work.helper_get_qty * work.helper_get_item_unit_cost)))
                   -- case 2
                   when work.threshold_value_type = 'Q' and
                        work.bg_buy_item != work.bg_get_item and
                        work.bg_get_type  = 'X' then
                           work.get_free_discount/100
                   -- case 3
                   when work.threshold_value_type = 'Q' and
                        work.bg_buy_item != work.bg_get_item and
                        work.bg_get_type != 'X' then
                           ((work.helper_get_item_unit_cost * work.helper_get_qty) /
                            ((work.helper_buy_item_unit_cost * work.helper_buy_target) +
                             (work.helper_get_qty * work.helper_get_item_unit_cost)))
                   else
                        null
                 end helper_get_discount_share,
                 --
                 count(1)
                   over(partition by work.item, work.supplier, work.origin_country_id,
                                     work.location, work.active_date) as key_rank_count,
                 rank()
                   over(partition by work.item, work.supplier, work.origin_country_id,
                                     work.location, work.active_date
                        order by DECODE(work.cost_appl_ind,'N',1,'NN',2,'DNN',3,-999),
                                 DECODE(L_so_rowtype.deal_type_priority,
                                        work.deal_head_type, 1, 2),
                                 DECODE(L_so_rowtype.deal_age_priority,
                                        'O', to_number(to_char(work.create_datetime,'YYYYMMDD')),        --asc
                                        'N', to_number(to_char(work.create_datetime,'YYYYMMDD')) * -1),  --desc
                                 work.deal_id,
                                 work.deal_detail_application_order) as key_day_rank,
                 dense_rank()
                   over(partition by work.item, work.supplier, work.origin_country_id, work.location
                        order by work.active_date) as key_rank
           from future_cost_working_gtt work
   ) use_this
   on (    gtt.item                  = use_this.item
       and gtt.supplier              = use_this.supplier
       and gtt.origin_country_id     = use_this.origin_country_id
       and gtt.location              = use_this.location
       and gtt.active_date           = use_this.active_date
       and gtt.deal_id               = use_this.deal_id
       and gtt.deal_detail_id        = use_this.deal_detail_id)
   when matched then update
      set helper_total_discount     = use_this.helper_total_discount,
          helper_get_discount_share = use_this.helper_get_discount_share,
          helper_buy_discount_share = 1 - use_this.helper_get_discount_share,
          key_rank_count            = use_this.key_rank_count,
          key_day_rank              = use_this.key_day_rank,
          key_rank                  = use_this.key_rank,
          --
          gtt.helper_new_buy_unit_cost_n = use_this.net_cost -
            ((use_this.helper_total_discount * (1 - use_this.helper_get_discount_share)) / use_this.new_buy_divisor),
          gtt.helper_new_get_unit_cost_n = greatest(use_this.helper_get_item_unit_cost_init -
            ((use_this.helper_total_discount * use_this.helper_get_discount_share) / use_this.helper_get_qty), 0),
          --
          gtt.helper_new_buy_unit_cost_nn = use_this.net_net_cost -
            ((use_this.helper_total_discount * (1 - use_this.helper_get_discount_share)) / use_this.new_buy_divisor),
          gtt.helper_new_get_unit_cost_nn = greatest(use_this.helper_get_item_unit_cost_init -
            ((use_this.helper_total_discount * use_this.helper_get_discount_share) / use_this.helper_get_qty), 0),
          --
          gtt.helper_new_buy_unit_cost_dnn = use_this.dead_net_net_cost -
            ((use_this.helper_total_discount * (1 - use_this.helper_get_discount_share)) / use_this.new_buy_divisor),
          gtt.helper_new_get_unit_cost_dnn = greatest(use_this.helper_get_item_unit_cost_init -
            ((use_this.helper_total_discount * use_this.helper_get_discount_share) / use_this.helper_get_qty), 0);

   delete from future_cost_working_gtt
    where threshold_value_type = 'Q'
      and helper_total_discount = -999;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.HELP_ROLL_DEAL_2',
                                            to_char(SQLCODE));
      return 0;
END HELP_ROLL_DEAL_2;
----------------------------------------------------------------------------------------
FUNCTION HELP_ROLL_DEAL_3(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

   L_return NUMBER;
BEGIN

   merge into future_cost_working_gtt gtt
   using (select work.item,
                 work.supplier,
                 work.origin_country_id,
                 work.location,
                 work.active_date,
                 work.deal_id,
                 work.deal_detail_id,
                 helper_new_buy_unit_cost_n,
                 helper_new_buy_unit_cost_nn,
                 helper_new_buy_unit_cost_dnn,
                 --
                 case when work.bg_buy_item = work.bg_get_item and
                           work.helper_new_buy_unit_cost_n < 0 then
                        ((work.helper_new_get_unit_cost_n * work.helper_get_qty) +
                           work.helper_new_buy_unit_cost_n * (work.helper_buy_target - work.helper_get_qty)) /
                              work.helper_get_qty
                      when work.bg_get_item != work.bg_buy_item and
                           work.bg_buy_item = work.item and
                           work.helper_new_buy_unit_cost_n < 0 then
                        ((work.helper_new_get_unit_cost_n * work.helper_get_qty) +
                           work.helper_new_buy_unit_cost_n * work.helper_buy_target) /
                              work.helper_get_qty
                      else
                         helper_new_get_unit_cost_n
                 end as helper_new_get_unit_cost_n,
                 --
                 case when work.bg_buy_item = work.bg_get_item and
                           work.helper_new_buy_unit_cost_nn < 0 then
                        ((work.helper_new_get_unit_cost_nn * work.helper_get_qty) +
                           work.helper_new_buy_unit_cost_nn * (work.helper_buy_target - work.helper_get_qty)) /
                              work.helper_get_qty
                      when work.bg_get_item != work.bg_buy_item and
                           work.bg_buy_item = work.item and
                           work.helper_new_buy_unit_cost_nn < 0 then
                        ((work.helper_new_get_unit_cost_nn * work.helper_get_qty) +
                           work.helper_new_buy_unit_cost_nn * work.helper_buy_target) /
                              work.helper_get_qty
                      else
                         helper_new_get_unit_cost_nn
                 end as helper_new_get_unit_cost_nn,
                 --
                 case when work.bg_buy_item = work.bg_get_item and
                           work.helper_new_buy_unit_cost_dnn < 0 then
                        ((work.helper_new_get_unit_cost_dnn * work.helper_get_qty) +
                           work.helper_new_buy_unit_cost_dnn * (work.helper_buy_target - work.helper_get_qty)) /
                              work.helper_get_qty
                      when work.bg_get_item != work.bg_buy_item and
                           work.bg_buy_item = work.item and
                           work.helper_new_buy_unit_cost_dnn < 0 then
                        ((work.helper_new_get_unit_cost_dnn * work.helper_get_qty) +
                           work.helper_new_buy_unit_cost_dnn * work.helper_buy_target) /
                              work.helper_get_qty
                      else
                         helper_new_get_unit_cost_dnn
                 end as helper_new_get_unit_cost_dnn
           from future_cost_working_gtt work
   ) use_this
   on (    gtt.item                  = use_this.item
       and gtt.supplier              = use_this.supplier
       and gtt.origin_country_id     = use_this.origin_country_id
       and gtt.location              = use_this.location
       and gtt.active_date           = use_this.active_date
       and gtt.deal_id               = use_this.deal_id
       and gtt.deal_detail_id        = use_this.deal_detail_id)
   when matched then update
      set gtt.helper_new_buy_unit_cost_n   = greatest(gtt.helper_new_buy_unit_cost_n,        0),
          gtt.helper_new_get_unit_cost_n   = greatest(use_this.helper_new_get_unit_cost_n,   0),
          gtt.helper_new_buy_unit_cost_nn  = greatest(gtt.helper_new_buy_unit_cost_nn,       0),
          gtt.helper_new_get_unit_cost_nn  = greatest(use_this.helper_new_get_unit_cost_nn,  0),
          gtt.helper_new_buy_unit_cost_dnn = greatest(gtt.helper_new_buy_unit_cost_dnn,      0),
          gtt.helper_new_get_unit_cost_dnn = greatest(use_this.helper_new_get_unit_cost_dnn, 0);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.HELP_ROLL_DEAL_3',
                                            to_char(SQLCODE));
      return 0;
END HELP_ROLL_DEAL_3;
----------------------------------------------------------------------------------------
FUNCTION MERGE_DEAL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                    I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

   L_return NUMBER;

BEGIN

   merge into future_cost_gtt gtt
   using
   (select item,
           supplier,
           origin_country_id,
           location,
           active_date,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           key_day_rank,
           key_rank_count
      from future_cost_working_gtt gtt
     WHERE 1 = 1
     MODEL
     PARTITION BY (item, supplier, origin_country_id, location, active_date)
     DIMENSION BY (key_rank, key_day_rank )
     MEASURES ( deal_id,
                deal_detail_id,
                item line_item,
                --
                base_cost,
                net_cost,
                net_net_cost,
                dead_net_net_cost,
                pricing_cost,
                --
                cost_appl_ind,
                price_cost_appl_ind,
                deal_class,
                threshold_value_type,
                bg_buy_item,
                bg_get_type,
                bg_get_value,
                bg_buy_qty,
                bg_recur_ind,
                bg_buy_target,
                bg_buy_avg_loc,
                bg_get_item,
                bg_get_qty,
                bg_free_item_unit_cost,
                deal_head_type,
                partner_type,
                partner_id,
                create_datetime,
                deal_detail_application_order,
                threshold_value,
                --
                helper_get_item_unit_cost,
                helper_buy_item_unit_cost,
                helper_get_item_unit_cost_init,
                helper_new_get_unit_cost_n,
                helper_new_buy_unit_cost_n,
                helper_new_get_unit_cost_nn,
                helper_new_buy_unit_cost_nn,
                helper_new_get_unit_cost_dnn,
                helper_new_buy_unit_cost_dnn,
                helper_new_item_unit_cost,
                helper_get_qty,
                helper_buy_qty,
                helper_buy_target,
                helper_get_target,
                helper_thresh_get_qty,
                helper_total_discount,
                helper_get_value,
                helper_get_discount_share,
                helper_buy_discount_share,
                helper_buy_plus_get_qty,
                --
                negotiated_item_cost,
                extended_base_cost,
                wac_tax,
                default_costing_type,
                --
                key_rank_count
              )
     RULES SEQUENTIAL ORDER (
        ---
        net_cost[ANY,ANY] ORDER BY key_rank ASC, key_day_rank ASC=
           case when cost_appl_ind[CV(key_rank),CV(key_day_rank)] in('N') then
              case when deal_id[CV(key_rank),CV(key_day_rank)] is null then
                 decode(NVL(default_costing_type[CV(key_rank),CV(key_day_rank)], 'BC'), 'BC', base_cost[CV(key_rank),CV(key_day_rank)], negotiated_item_cost[CV(key_rank),CV(key_day_rank)])
              else
                 -- ACTUALLY DO SOMETHING
                 case when threshold_value_type[CV(key_rank),CV(key_day_rank)] = 'A' then
                    net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                        threshold_value[CV(key_rank),CV(key_day_rank)]
                 when threshold_value_type[CV(key_rank),CV(key_day_rank)] = 'P' then
                    case when deal_class[CV(key_rank),CV(key_day_rank)] = 'CU' then
                       net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                           (decode(NVL(default_costing_type[CV(key_rank),CV(key_day_rank)], 'BC'), 'BC', base_cost[CV(key_rank),CV(key_day_rank)], negotiated_item_cost[CV(key_rank),CV(key_day_rank)]) *
                            threshold_value[CV(key_rank),CV(key_day_rank)] / 100)
                    else
                       net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                           (net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] *
                            threshold_value[CV(key_rank),CV(key_day_rank)] / 100)
                    end
                 when threshold_value_type[CV(key_rank),CV(key_day_rank)] = 'F' then
                    LEAST(net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)],
                           threshold_value[CV(key_rank),CV(key_day_rank)])
                 when threshold_value_type[CV(key_rank),CV(key_day_rank)] = 'Q' then
                    case when bg_buy_item[CV(key_rank),CV(key_day_rank)] =
                              bg_get_item[CV(key_rank),CV(key_day_rank)] then
                         net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                         (net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                           (((helper_new_get_unit_cost_n[CV(key_rank),CV(key_day_rank)] *
                              helper_get_qty[CV(key_rank),CV(key_day_rank)]) +
                             (helper_new_buy_unit_cost_n[CV(key_rank),CV(key_day_rank)] *
                             (helper_buy_target[CV(key_rank),CV(key_day_rank)] - helper_get_qty[CV(key_rank),CV(key_day_rank)]))) /
                                helper_buy_target[CV(key_rank),CV(key_day_rank)])
                         )
                    when line_item[CV(key_rank),CV(key_day_rank)] =
                         bg_buy_item[CV(key_rank),CV(key_day_rank)] then
                        helper_new_buy_unit_cost_n[CV(key_rank),CV(key_day_rank)]
                    when line_item[CV(key_rank),CV(key_day_rank)] =
                         bg_get_item[CV(key_rank),CV(key_day_rank)] then
                         net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                         (net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                          ((net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] *
                            (helper_get_target[CV(key_rank),CV(key_day_rank)] - helper_get_qty[CV(key_rank),CV(key_day_rank)])) +
                            (helper_new_get_unit_cost_n[CV(key_rank),CV(key_day_rank)]*
                             helper_get_qty[CV(key_rank),CV(key_day_rank)])) /
                           helper_get_target[CV(key_rank),CV(key_day_rank)]
                         )
                    else
                       0
                    end
                 end
              end
           when CV(key_day_rank) > 1 then
                net_cost[CV(key_rank),CV(key_day_rank)-1]
           else
              decode(NVL(default_costing_type[CV(key_rank),CV(key_day_rank)], 'BC'), 'BC', base_cost[CV(key_rank),CV(key_day_rank)], negotiated_item_cost[CV(key_rank),CV(key_day_rank)])
           end,
        ---
        net_net_cost[ANY,ANY] ORDER BY key_rank ASC, key_day_rank ASC=
           case when cost_appl_ind[CV(key_rank),CV(key_day_rank)] in('NN','N') then
              case when deal_id[CV(key_rank),CV(key_day_rank)] is null then
                 decode(NVL(default_costing_type[CV(key_rank),CV(key_day_rank)], 'BC'), 'BC', base_cost[CV(key_rank),CV(key_day_rank)], negotiated_item_cost[CV(key_rank),CV(key_day_rank)])
              else
                 -- ACTUALLY DO SOMETHING
                 case when threshold_value_type[CV(key_rank),CV(key_day_rank)] = 'A' then
                    net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                        threshold_value[CV(key_rank),CV(key_day_rank)]
                 when threshold_value_type[CV(key_rank),CV(key_day_rank)] = 'P' then
                    case when deal_class[CV(key_rank),CV(key_day_rank)] = 'CU' then
                       net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                           (decode(NVL(default_costing_type[CV(key_rank),CV(key_day_rank)], 'BC'), 'BC', base_cost[CV(key_rank),CV(key_day_rank)], negotiated_item_cost[CV(key_rank),CV(key_day_rank)]) *
                            threshold_value[CV(key_rank),CV(key_day_rank)] / 100)
                    else
                       net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                           (net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] *
                            threshold_value[CV(key_rank),CV(key_day_rank)] / 100)
                    end
                 when threshold_value_type[CV(key_rank),CV(key_day_rank)] = 'F' then
                     LEAST(net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)],
                           threshold_value[CV(key_rank),CV(key_day_rank)])
                 when threshold_value_type[CV(key_rank),CV(key_day_rank)] = 'Q' then
                    case when bg_buy_item[CV(key_rank),CV(key_day_rank)] =
                              bg_get_item[CV(key_rank),CV(key_day_rank)] then
                         net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                         (net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                           (((helper_new_get_unit_cost_n[CV(key_rank),CV(key_day_rank)] *
                              helper_get_qty[CV(key_rank),CV(key_day_rank)]) +
                             (helper_new_buy_unit_cost_n[CV(key_rank),CV(key_day_rank)] *
                             (helper_buy_target[CV(key_rank),CV(key_day_rank)] - helper_get_qty[CV(key_rank),CV(key_day_rank)]))) /
                                helper_buy_target[CV(key_rank),CV(key_day_rank)])
                         )
                    when line_item[CV(key_rank),CV(key_day_rank)] =
                         bg_buy_item[CV(key_rank),CV(key_day_rank)] then
                        helper_new_buy_unit_cost_n[CV(key_rank),CV(key_day_rank)]
                    when line_item[CV(key_rank),CV(key_day_rank)] =
                         bg_get_item[CV(key_rank),CV(key_day_rank)] then
                         net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                         (net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                          ((net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] *
                            (helper_get_target[CV(key_rank),CV(key_day_rank)] - helper_get_qty[CV(key_rank),CV(key_day_rank)])) +
                            (helper_new_get_unit_cost_n[CV(key_rank),CV(key_day_rank)]*
                             helper_get_qty[CV(key_rank),CV(key_day_rank)])) /
                           helper_get_target[CV(key_rank),CV(key_day_rank)]
                         )
                    else
                       0
                    end
                 end
              end
           when CV(key_day_rank) > 1 then
              net_net_cost[CV(key_rank),CV(key_day_rank)-1]
           else
              decode(NVL(default_costing_type[CV(key_rank),CV(key_day_rank)], 'BC'), 'BC', base_cost[CV(key_rank),CV(key_day_rank)],
                                                                                           negotiated_item_cost[CV(key_rank),CV(key_day_rank)])
           end,
        ---
        dead_net_net_cost[ANY,ANY] ORDER BY key_rank ASC, key_day_rank ASC=
           case when cost_appl_ind[CV(key_rank),CV(key_day_rank)] in('DNN','NN','N') then
              case when deal_id[CV(key_rank),CV(key_day_rank)] is null then
                 decode(NVL(default_costing_type[CV(key_rank),CV(key_day_rank)], 'BC'), 'BC', base_cost[CV(key_rank),CV(key_day_rank)],
                                                                                              negotiated_item_cost[CV(key_rank),CV(key_day_rank)])
              else
                 -- ACTUALLY DO SOMETHING
                 -- compute for currency amount
                 case when threshold_value_type[CV(key_rank),CV(key_day_rank)] = 'A' then
                    dead_net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)]-
                        threshold_value[CV(key_rank),CV(key_day_rank)]
                 -- compute for percentage
                 when threshold_value_type[CV(key_rank),CV(key_day_rank)] = 'P' then   -- percentage
                    case when deal_class[CV(key_rank),CV(key_day_rank)] = 'CU' then
                       dead_net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                           (decode(NVL(default_costing_type[CV(key_rank),CV(key_day_rank)], 'BC'), 'BC', base_cost[CV(key_rank),CV(key_day_rank)],
                                                                                                         negotiated_item_cost[CV(key_rank),CV(key_day_rank)]) *
                            threshold_value[CV(key_rank),CV(key_day_rank)] / 100)
                    else
                       dead_net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                           (dead_net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] *
                            threshold_value[CV(key_rank),CV(key_day_rank)] / 100)
                    end
                 -- compute for fixed amount
                 when threshold_value_type[CV(key_rank),CV(key_day_rank)] = 'F' then
                      LEAST(dead_net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)],
                           threshold_value[CV(key_rank),CV(key_day_rank)])
                 -- compute for quantity
                 when threshold_value_type[CV(key_rank),CV(key_day_rank)] = 'Q' then
                    case when bg_buy_item[CV(key_rank),CV(key_day_rank)] =
                              bg_get_item[CV(key_rank),CV(key_day_rank)] then
                         dead_net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                         (dead_net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                           (((helper_new_get_unit_cost_n[CV(key_rank),CV(key_day_rank)] *
                              helper_get_qty[CV(key_rank),CV(key_day_rank)]) +
                             (helper_new_buy_unit_cost_n[CV(key_rank),CV(key_day_rank)] *
                             (helper_buy_target[CV(key_rank),CV(key_day_rank)] - helper_get_qty[CV(key_rank),CV(key_day_rank)]))) /
                                helper_buy_target[CV(key_rank),CV(key_day_rank)])
                         )
                    when line_item[CV(key_rank),CV(key_day_rank)] =
                         bg_buy_item[CV(key_rank),CV(key_day_rank)] then
                        helper_new_buy_unit_cost_n[CV(key_rank),CV(key_day_rank)]
                    when line_item[CV(key_rank),CV(key_day_rank)] =
                         bg_get_item[CV(key_rank),CV(key_day_rank)] then
                         dead_net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                         (dead_net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] -
                          ((dead_net_net_cost[CV(key_rank),decode(CV(key_day_rank),1,CV(key_day_rank),CV(key_day_rank)-1)] *
                            (helper_get_target[CV(key_rank),CV(key_day_rank)] - helper_get_qty[CV(key_rank),CV(key_day_rank)])) +
                            (helper_new_get_unit_cost_n[CV(key_rank),CV(key_day_rank)]*
                             helper_get_qty[CV(key_rank),CV(key_day_rank)])) /
                           helper_get_target[CV(key_rank),CV(key_day_rank)]
                         )
                    else
                       0
                    end
                 end
              end
           else
              decode(NVL(default_costing_type[CV(key_rank),CV(key_day_rank)], 'BC'), 'BC', base_cost[CV(key_rank),CV(key_day_rank)],
                                                                                           negotiated_item_cost[CV(key_rank),CV(key_day_rank)])
           end,
        ---------------------
        pricing_cost[ANY,ANY] ORDER BY key_rank ASC, key_day_rank ASC=
           case when price_cost_appl_ind[CV(key_rank),CV(key_day_rank)] = 'Y' then
                dead_net_net_cost[CV(key_rank),CV(key_day_rank)] + NVL(wac_tax[CV(key_rank),CV(key_day_rank)],0)
           when price_cost_appl_ind[CV(key_rank),CV(key_day_rank)] = 'N'
            and CV(key_day_rank) > 1 then
                pricing_cost[CV(key_rank),CV(key_day_rank)-1]
           else
                base_cost[CV(key_rank),CV(key_day_rank)] + NVL(wac_tax[CV(key_rank),CV(key_day_rank)],0)
           end
     )
   ) use_this
   on (    gtt.item                  = use_this.item
       and gtt.supplier              = use_this.supplier
       and gtt.origin_country_id     = use_this.origin_country_id
       and gtt.location              = use_this.location
       and gtt.active_date           = use_this.active_date
       and use_this.key_day_rank     = use_this.key_rank_count)
   when matched then
   update
      set gtt.net_cost          = use_this.net_cost,
          gtt.net_net_cost      = use_this.net_net_cost,
          gtt.dead_net_net_cost = use_this.dead_net_net_cost,
          gtt.pricing_cost      = use_this.pricing_cost;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.MERGE_DEAL',
                                            to_char(SQLCODE));
      return 0;
END MERGE_DEAL;
----------------------------------------------------------------------------------------
FUNCTION ROLL_DEAL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                   I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

   L_return NUMBER;

BEGIN

   L_return := HELP_ROLL_DEAL_1(O_error_message,
                                I_cost_event_process_id,
                                I_thread_id);
   if L_return = 0 then
      return L_return;
   end if;
   --
   L_return := HELP_ROLL_DEAL_2(O_error_message,
                                I_cost_event_process_id,
                                I_thread_id);
   if L_return = 0 then
      return L_return;
   end if;
   --
   L_return := HELP_ROLL_DEAL_3(O_error_message,
                                I_cost_event_process_id,
                                I_thread_id);
   if L_return = 0 then
      return L_return;
   end if;
   --
   L_return := MERGE_DEAL(O_error_message,
                                I_cost_event_process_id,
                                I_thread_id);
   if L_return = 0 then
      return L_return;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.ROLL_DEAL',
                                            to_char(SQLCODE));
      return 0;
END ROLL_DEAL;
----------------------------------------------------------------------------------------
FUNCTION ROLL_ELC_TEXPZ(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

BEGIN

--TEXPZ
   merge into future_cost_gtt fc
   using
    ( select inner.item,
             inner.location,
             inner.supplier,
             inner.origin_country_id,
             inner.active_date,
             inner.est_exp_value * inner.exchange_rate texpz_value
        from (select distinct gtt.item,
                     gtt.location,
                     gtt.supplier,
                     gtt.origin_country_id,
                     gtt.active_date,
                     case ged.pack_no
                        when '-999' then
                           first_value(NVL(ged.est_exp_value, 0)) over
                              (partition by gtt.item,
                                            gtt.location,
                                            gtt.supplier,
                                            gtt.origin_country_id,
                                            gtt.active_date
                                   order by ged.active_date desc) 
                        else
                           sum(NVL(ged.est_exp_value, 0) * vpq.qty) over
                              (partition by gtt.item,
                                            gtt.location,
                                            gtt.supplier,
                                            gtt.origin_country_id,
                                            gtt.active_date
                                   order by ged.active_date desc) 
                        end est_exp_value,
                     case when h.discharge_port = czgl.primary_discharge_port
                           and h.zone_id = czgl.zone_id then 1
                          else 2  -- base expense
                     end texpz_helper,
                     min(case when h.discharge_port = czgl.primary_discharge_port
                               and h.zone_id = czgl.zone_id then 1
                              else 2  -- base expense
                         end)
                        over (partition by gtt.item, gtt.location, gtt.supplier, gtt.origin_country_id, gtt.active_date)
                         as texpz_helper_min,
                     curr.exchange_rate exchange_rate,
                     rank ()
                     over (partition by  gtt.item,gtt.supplier, gtt.origin_country_id,gtt.location
                     order by   est_exp_value asc) rank_asc
                from gtt_fc_item_exp_detail ged,
                     item_exp_head h,
                     future_cost_gtt gtt,
                     cost_zone_group_loc czgl,
                     item_master im,
                     mv_currency_conversion_rates curr,
                     v_packsku_qty vpq
               where im.item               = ged.item
                 and im.cost_zone_group_id = czgl.zone_group_id
                 and gtt.location          = czgl.location
                 --
                 and h.supplier      = gtt.supplier
                 and h.supplier      = ged.supplier
                 --
                 and gtt.item        = DECODE(ged.pack_no, '-999', ged.item, ged.pack_no)
                 and h.item          = ged.item
                 and vpq.pack_no(+)  = ged.pack_no
                 and vpq.item(+)     = ged.item
                 --
                 and h.item_exp_type = ged.item_exp_type
                 and h.item_exp_seq  = ged.item_exp_seq
                 and h.zone_group_id = im.cost_zone_group_id
                 and h.item_exp_type = 'Z'
                 --
                 and ((h.discharge_port = czgl.primary_discharge_port )
                      or h.base_exp_ind = 'Y')
                 --
                 and ged.comp_id       = 'TEXPZ'
                 --
                 and ged.comp_currency      = curr.from_currency
                 and gtt.currency_code      = curr.to_currency
                 and curr.exchange_type     = 'C'
                 and curr.effective_date    = (select max(effective_date) 
                                                 from mv_currency_conversion_rates curri
                                                where curr.exchange_type        = 'C'
                                                  and curr.from_currency =curri.from_currency 
                                                  and curr.to_currency = curri.to_currency 
                                                  and curri.effective_date <=gtt.calc_date)
                 and gtt.active_date          >= LP_vdate
                 and gtt.active_date          >= ged.active_date
                 and ((gtt.location              = NVL(ged.location,gtt.location))
                       or (NVL(gtt.costing_loc,-1)= NVL(ged.location,gtt.location)))) inner
       -- will only match if texpz_helper is 1 or 2
       where inner.texpz_helper = texpz_helper_min
         and inner.rank_asc = 1) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.active_date)
   when matched then
   update
      set fc.pricing_cost = nvl(fc.pricing_cost, 0) + nvl(use_this.texpz_value, 0),
          fc.acquisition_cost = decode(fc.store_type,'F',nvl(fc.acquisition_cost, 0) + nvl(use_this.texpz_value, 0)),
          fc.elc_amt  = use_this.texpz_value;

   return 1;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.ROLL_ELC_TEXPZ',
                                            to_char(SQLCODE));
      return 0;
END ROLL_ELC_TEXPZ;
----------------------------------------------------------------------------------------
FUNCTION ROLL_ELC_TEXPC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

BEGIN

--TEXPC
   merge into future_cost_gtt fc
   using
    ( select inner.item,
             inner.location,
             inner.supplier,
             inner.origin_country_id,
             inner.active_date,
             inner.texpc_value * inner.exchange_rate texpc_value
        from (select distinct gtt.item,
                     gtt.location,
                     gtt.supplier,
                     gtt.origin_country_id,
                     gtt.active_date,
                     case ged.pack_no
                        when '-999' then
                           first_value(NVL(ged.est_exp_value, 0)) over
                              (partition by gtt.item,
                                            gtt.location,
                                            gtt.supplier,
                                            gtt.origin_country_id,
                                            gtt.active_date
                                   order by ged.active_date desc)
                        else 
                           sum(NVL(ged.est_exp_value, 0) * vpq.qty) over
                              (partition by gtt.item,
                                            gtt.location,
                                            gtt.supplier,
                                            gtt.origin_country_id,
                                            gtt.active_date
                                   order by ged.active_date desc) 
                     end texpc_value,
                     curr.exchange_rate exchange_rate
                from gtt_fc_item_exp_detail ged,
                     item_exp_head h,
                     future_cost_gtt gtt,
                     mv_currency_conversion_rates curr,
                     v_packsku_qty vpq
               where h.supplier          = gtt.supplier
                 and h.supplier          = ged.supplier
                 --
                 and gtt.item            = DECODE(ged.pack_no, '-999', ged.item, ged.pack_no)
                 and h.item              = ged.item
                 and vpq.pack_no(+)      = ged.pack_no
                 and vpq.item(+)         = ged.item
                 ---
                 and h.origin_country_id = gtt.origin_country_id
                 --
                 and h.item_exp_type = ged.item_exp_type
                 and h.item_exp_seq  = ged.item_exp_seq
                 --
                 and h.item_exp_type           = 'C'
                 and h.base_exp_ind            = 'Y'
                 and ged.comp_id               = 'TEXPC'
                 --
                 and ged.comp_currency         = curr.from_currency
                 and gtt.currency_code         = curr.to_currency
                 and curr.exchange_type        = 'C'
                 and curr.effective_date       = (select max(effective_date) 
                                                    from mv_currency_conversion_rates curri
                                                   where curr.exchange_type        = 'C'
                                                     and curr.from_currency =curri.from_currency 
                                                     and curr.to_currency = curri.to_currency 
                                                     and curri.effective_date <=gtt.calc_date)
                 and gtt.active_date          >= LP_vdate
                 and gtt.active_date          >= ged.active_date
                 and ((gtt.location              = NVL(ged.location,gtt.location)) 
                       or (NVL(gtt.costing_loc,-1)= NVL(ged.location,gtt.location))))inner) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.active_date)
   when matched then
   update
      set fc.pricing_cost = nvl(fc.pricing_cost, 0) + nvl(use_this.texpc_value, 0),
          fc.acquisition_cost = decode(fc.store_type,'F',nvl(fc.acquisition_cost, 0) + nvl(use_this.texpc_value, 0)),
          fc.elc_amt  = nvl(fc.elc_amt, 0) + nvl(use_this.texpc_value, 0);
          
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.ROLL_ELC_TEXPC',
                                            to_char(SQLCODE));
      return 0;
END ROLL_ELC_TEXPC;
----------------------------------------------------------------------------------------
FUNCTION ROLL_ELC_TDTY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

   L_tdty_currency        elc_comp.comp_currency%TYPE;

   cursor c_get_assess_curr IS
      select comp_currency
        from elc_comp
       where comp_id = 'TDTY'||LP_system_optns.base_country_id;

BEGIN

   open c_get_assess_curr;
   fetch c_get_assess_curr into L_tdty_currency;
   close c_get_assess_curr;

--TDTY
   if LP_system_optns.hts_tracking_level = 'M' then

      merge into future_cost_gtt fc
      using
       ( select inner.item,
                inner.location,
                inner.supplier,
                inner.origin_country_id,
                inner.active_date,
                inner.tdty_value * inner.exchange_rate tdty_value
           from (select distinct gtt.item,
                        gtt.location,
                        gtt.supplier,
                        gtt.origin_country_id,
                        gtt.active_date,
                        gtt.currency_code,
                        sum(NVL(a.est_assess_value, 0)) over
                        (partition by gtt.item,
                                      gtt.location,
                                      gtt.supplier,
                                      gtt.origin_country_id,
                                      gtt.active_date) tdty_value,
                        curr.exchange_rate exchange_rate
                   from gtt_fc_item_hts_assess a,
                        future_cost_gtt gtt,
                        mv_currency_conversion_rates curr,
                        item_supp_manu_country ismc
                  where a.item                 = gtt.item
                    and a.import_country_id    = LP_system_optns.base_country_id
                    and a.origin_country_id    = ismc.manu_country_id
                    and gtt.item               = ismc.item
                    and gtt.supplier           = ismc.supplier
                    and ismc.primary_manu_ctry_ind = 'Y'
                    and a.effect_from         <= gtt.active_date
                    and a.effect_to           >= gtt.active_date
                    and a.comp_id              = 'TDTY'||LP_system_optns.base_country_id
                    --
                    and L_tdty_currency        = curr.from_currency
                    and gtt.currency_code      = curr.to_currency
                    and curr.exchange_type     = 'C'
                    and curr.effective_date    = (select max(effective_date) 
                                                    from mv_currency_conversion_rates curri
                                                   where curr.exchange_type        = 'C'
                                                     and curr.from_currency =curri.from_currency 
                                                     and curr.to_currency = curri.to_currency 
                                                     and curri.effective_date <=gtt.calc_date)
                    and gtt.active_date          >= LP_vdate
                    and gtt.active_date          >= a.active_date) inner) use_this
      on (    fc.item                  = use_this.item
          and fc.supplier              = use_this.supplier
          and fc.origin_country_id     = use_this.origin_country_id
          and fc.location              = use_this.location
          and fc.active_date           = use_this.active_date)
      when matched then
      update
         set fc.pricing_cost = nvl(fc.pricing_cost, 0) + nvl(use_this.tdty_value, 0),
             fc.acquisition_cost = decode(fc.store_type,'F',nvl(fc.acquisition_cost, 0) + nvl(use_this.tdty_value, 0)),
             fc.elc_amt  = nvl(fc.elc_amt, 0) + nvl(use_this.tdty_value, 0);

   else

      merge into future_cost_gtt fc
      using
       ( select inner.item,
                inner.location,
                inner.supplier,
                inner.origin_country_id,
                inner.active_date,
                inner.tdty_value * inner.exchange_rate tdty_value
           from (select distinct gtt.item,
                        gtt.location,
                        gtt.supplier,
                        gtt.origin_country_id,
                        gtt.active_date,
                        gtt.currency_code,
                        sum(NVL(a.est_assess_value, 0)) over
                        (partition by gtt.item,
                                      gtt.location,
                                      gtt.supplier,
                                      gtt.origin_country_id,
                                      gtt.active_date) tdty_value,
                        curr.exchange_rate exchange_rate
                   from gtt_fc_item_hts_assess a,
                        future_cost_gtt gtt,
                        mv_currency_conversion_rates curr
                  where a.item                 = gtt.item
                    and a.import_country_id    = LP_system_optns.base_country_id
                    and a.origin_country_id    = gtt.origin_country_id
                    and a.effect_from         <= gtt.active_date
                    and a.effect_to           >= gtt.active_date
                    and a.comp_id              = 'TDTY'||LP_system_optns.base_country_id
                    --
                    and L_tdty_currency        = curr.from_currency
                    and gtt.currency_code      = curr.to_currency
                    and curr.exchange_type     = 'C'
                    and curr.effective_date    = (select max(effective_date) 
                                                    from mv_currency_conversion_rates curri
                                                   where curr.exchange_type        = 'C'
                                                     and curr.from_currency =curri.from_currency 
                                                     and curr.to_currency = curri.to_currency 
                                                     and curri.effective_date <=gtt.calc_date)
                    and gtt.active_date          >= LP_vdate) inner) use_this
      on (    fc.item              = use_this.item
          and fc.supplier          = use_this.supplier
          and fc.origin_country_id = use_this.origin_country_id
          and fc.location          = use_this.location
          and fc.active_date       = use_this.active_date)
      when matched then
      update
         set fc.pricing_cost = nvl(fc.pricing_cost, 0) + nvl(use_this.tdty_value, 0),
             fc.acquisition_cost = decode(fc.store_type,'F',nvl(fc.acquisition_cost, 0) + nvl(use_this.tdty_value, 0)),
             fc.elc_amt  = nvl(fc.elc_amt, 0) + nvl(use_this.tdty_value, 0);
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.ROLL_ELC_TDTY',
                                            to_char(SQLCODE));
      return 0;
END ROLL_ELC_TDTY;
----------------------------------------------------------------------------------------
FUNCTION ROLL_ELC(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                  I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

   L_return NUMBER;

BEGIN

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_optns) then
      return 0;
   end if;

   L_return := ROLL_ELC_TEXPZ(O_error_message,
                              I_cost_event_process_id,
                              I_thread_id);
   if L_return = 0 then
      return L_return;
   end if;
   --
   L_return := ROLL_ELC_TEXPC(O_error_message,
                              I_cost_event_process_id,
                              I_thread_id);
   if L_return = 0 then
      return L_return;
   end if;
   --
   L_return := ROLL_ELC_TDTY(O_error_message,
                             I_cost_event_process_id,
                             I_thread_id);
   if L_return = 0 then
      return L_return;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.ROLL_ELC',
                                            to_char(SQLCODE));
      return 0;
END ROLL_ELC;
----------------------------------------------------------------------------------------------------
FUNCTION CONVERT_NEGATIVE_COST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER is

   L_return NUMBER;

BEGIN

   update future_cost_gtt
      set base_cost         = decode(sign(base_cost),         -1, 0, base_cost),
          net_cost          = decode(sign(net_cost),          -1, 0, net_cost),
          net_net_cost      = decode(sign(net_net_cost),      -1, 0, net_net_cost),
          dead_net_net_cost = decode(sign(dead_net_net_cost), -1, 0, dead_net_net_cost),
          pricing_cost      = decode(sign(pricing_cost),      -1, 0, pricing_cost);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_ROLLFWD_SQL.CONVERT_NEGATIVE_COST',
                                            to_char(SQLCODE));
      return 0;
END CONVERT_NEGATIVE_COST;
----------------------------------------------------------------------------------------
END FUTURE_COST_ROLLFWD_SQL;
/