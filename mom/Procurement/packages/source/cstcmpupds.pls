CREATE OR REPLACE PACKAGE COST_COMP_UPD_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
-- Function Name: SAVE_UPDATES
-- Purpose: The function is called by the forms where the cost components modifications are done.
--          This function saves the changes on a staging table at defaulting level/comp id/defaulting entity.
--          Merge statements will ensure that only one row exists for defaulting level/comp id/defaulting entity (and 
--          expense profile key in case of expense profile)
---------------------------------------------------------------------------------------------
FUNCTION SAVE_UPDATES (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_seq_no                 IN      COST_COMP_UPD_STG.SEQ_NO%TYPE,
                       I_defaulting_level       IN      COST_COMP_UPD_STG.DEFAULTING_LEVEL%TYPE,
                       I_effective_date         IN      COST_COMP_UPD_STG.EFFECTIVE_DATE%TYPE, 
                       I_comp_id                IN      ELC_COMP.COMP_ID%TYPE,
                       I_comp_type              IN      ELC_COMP.COMP_TYPE%TYPE,
                       I_dept                   IN      DEPS.DEPT%TYPE,
                       I_supplier               IN      SUPS.SUPPLIER%TYPE,
                       I_item                   IN      ITEM_MASTER.ITEM%TYPE,
                       I_item_exp_type          IN      ITEM_EXP_DETAIL.ITEM_EXP_TYPE%TYPE,
                       I_item_exp_seq           IN      ITEM_EXP_DETAIL.ITEM_EXP_SEQ%TYPE,
                       I_hts                    IN      HTS.HTS%TYPE,
                       I_import_country_id      IN      HTS.IMPORT_COUNTRY_ID%TYPE,
                       I_origin_country_id      IN      HTS.IMPORT_COUNTRY_ID%TYPE,
                       I_effect_from            IN      DATE,
                       I_effect_to              IN      DATE,
                       I_exp_prof_key           IN      EXP_PROF_HEAD.EXP_PROF_KEY%TYPE,
                       I_from_loc               IN      ITEM_LOC.LOC%TYPE,
                       I_to_loc                 IN      ITEM_LOC.LOC%TYPE,
                       I_zone_group_id          IN      COST_ZONE.ZONE_GROUP_ID%TYPE,
                       I_zone_id                IN      COST_ZONE.ZONE_ID%TYPE,
                       I_old_comp_rate          IN      ELC_COMP.COMP_RATE%TYPE,
                       I_old_comp_currency      IN      ELC_COMP.COMP_CURRENCY%TYPE,     
                       I_old_per_count          IN      ELC_COMP.PER_COUNT%TYPE,
                       I_old_per_count_uom      IN      ELC_COMP.PER_COUNT_UOM%TYPE,
                       I_new_comp_rate          IN      ELC_COMP.COMP_RATE%TYPE,
                       I_new_comp_currency      IN      ELC_COMP.COMP_CURRENCY%TYPE,
                       I_new_per_count          IN      ELC_COMP.PER_COUNT%TYPE,
                       I_new_per_count_uom      IN      ELC_COMP.PER_COUNT_UOM %TYPE,
                       I_cntry_default_ind      IN      COST_COMP_UPD_STG.CNTRY_DEFAULT_IND%TYPE,
                       I_supp_default_ind       IN      COST_COMP_UPD_STG.SUPP_DEFAULT_IND%TYPE,
                       I_ptnr_default_ind       IN      COST_COMP_UPD_STG.PTNR_DEFAULT_IND%TYPE,
                       I_item_default_ind       IN      COST_COMP_UPD_STG.ITEM_DEFAULT_IND%TYPE,
                       I_order_default_ind      IN      COST_COMP_UPD_STG.ORDER_DEFAULT_IND%TYPE,
                       I_tsf_alloc_default_ind  IN      COST_COMP_UPD_STG.TSF_ALLOC_DEFAULT_IND%TYPE,
                       I_dept_default_ind       IN      COST_COMP_UPD_STG.DEPT_DEFAULT_IND%TYPE,
                       I_lading_port            IN      COST_COMP_UPD_STG.LADING_PORT%TYPE DEFAULT NULL,
                       I_discharge_port         IN      COST_COMP_UPD_STG.DISCHARGE_PORT%TYPE DEFAULT NULL
                       )
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_RATES_EFFECTIVE_DATE
-- The function is called by a batch/shell script which is scheduled to run at the end of the day.
-- This function updates values on the base form (elccomp, itassess and itemchrg).
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_RATES_EFFECTIVE_DATE(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PROCESS_ITEM_COST_COMP_UPDATES
-- Purpose: The function is called by a batch/shell script which is scheduled to run at the end of the day.
--          The function calls the functions to cascade the HTS, EXPENSES and UPCHARGES to the item.
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_COST_COMP_UPDATES (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_Thread_no              IN      RESTART_CONTROL.NUM_THREADS%TYPE,
                                         I_Threads                IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PROCESS_ORD_COST_COMP_UPDATES
-- Purpose: The function is called by a batch/shell script which is scheduled to run at the end of the day.
--          The function calls the functions to cascade the HTS, EXPENSES to the orders.
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_ORD_COST_COMP_UPDATES (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_Thread_no              IN      RESTART_CONTROL.NUM_THREADS%TYPE,
                                        I_Threads                IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN;                                   
---------------------------------------------------------------------------------------------
-- Function Name: GET_DEFAULT_IND
-- Purpose: This function retrieves the cascade indicator values from the staging table,
--          depending on the values passed to the function.
---------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_IND (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_defaulting_level       IN      COST_COMP_UPD_STG.DEFAULTING_LEVEL%TYPE,
                          I_dept                   IN      DEPS.DEPT%TYPE,
                          I_comp_id                IN      ELC_COMP.COMP_ID%TYPE,
                          I_comp_type              IN      ELC_COMP.COMP_TYPE%TYPE,
                          I_supplier               IN      SUPS.SUPPLIER%TYPE,
                          I_item                   IN      ITEM_MASTER.ITEM%TYPE,
                          I_item_exp_type          IN      ITEM_EXP_DETAIL.ITEM_EXP_TYPE%TYPE,
                          I_item_exp_seq           IN      ITEM_EXP_DETAIL.ITEM_EXP_SEQ%TYPE,
                          I_hts                    IN      HTS.HTS%TYPE,
                          I_import_country_id      IN      HTS.IMPORT_COUNTRY_ID%TYPE,
                          I_origin_country_id      IN      HTS.IMPORT_COUNTRY_ID%TYPE,
                          I_effect_from            IN      DATE,
                          I_effect_to              IN      DATE,
                          I_exp_prof_key           IN      EXP_PROF_HEAD.EXP_PROF_KEY%TYPE,
                          I_from_loc               IN      ITEM_LOC.LOC%TYPE,
                          I_to_loc                 IN      ITEM_LOC.LOC%TYPE,
                          O_cntry_default_ind      IN OUT  COST_COMP_UPD_STG.CNTRY_DEFAULT_IND%TYPE,
                          O_supp_default_ind       IN OUT  COST_COMP_UPD_STG.SUPP_DEFAULT_IND%TYPE,
                          O_ptnr_default_ind       IN OUT  COST_COMP_UPD_STG.PTNR_DEFAULT_IND%TYPE,
                          O_item_default_ind       IN OUT  COST_COMP_UPD_STG.ITEM_DEFAULT_IND%TYPE,
                          O_order_default_ind      IN OUT  COST_COMP_UPD_STG.ORDER_DEFAULT_IND%TYPE,
                          O_tsf_alloc_default_ind  IN OUT  COST_COMP_UPD_STG.TSF_ALLOC_DEFAULT_IND%TYPE,
                          O_dept_default_ind       IN OUT  COST_COMP_UPD_STG.DEPT_DEFAULT_IND%TYPE
                          )
RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_DEFAULT_IND
-- Purpose: For every item whose charges are modified, insert a record for the child in the 
--          cost_comp_upd_stg table.
---------------------------------------------------------------------------------------------

FUNCTION DEFAULT_PARENT_CHRGS (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_Item              IN      ITEM_MASTER.ITEM%TYPE,
                               I_comp_type  IN     COST_COMP_UPD_STG.COMP_TYPE%TYPE
                               )
RETURN BOOLEAN;                     


-----------------------------------------------------------------------------------------
-- Function Name: UPDATE_EXP_PROF_DETAIL
-- Purpose: -- The function is called by a batch/shell script which is scheduled to run at the end of the day.
-- The function calls the functions to cascade the Expenses to the 
-- Supplier, Partner and Country profiles
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_EXP_PROF_DETAIL(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_DEPT_CHRG_DETAIL
-- Purpose: -- The function is called by a batch/shell script which is scheduled to run at the end of the day.
-- The function calls the functions to cascade the Upcharges to the 
-- dept.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_DEPT_CHRG_DETAIL(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ALLOC_CHRG
-- The function is called by a batch/shell script which is scheduled to run at the end of the day.
-- The function calls the functions to cascade the Upcharges to the 
-- transfers and allocations
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_TSF_ALLOC_CHRG(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_Thread_no     IN      RESTART_CONTROL.NUM_THREADS%TYPE,  
                               I_Threads       IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_COST_COMP
-- The function is called by the cost component forms, whenever a component is deleted.
-- This function deletes the entry in table cost_comp_upd_stg for a perticular component at the 
-- level it was deleted.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_COST_COMP(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_defaulting_level  IN      COST_COMP_UPD_STG.DEFAULTING_LEVEL%TYPE,
                          I_comp_id           IN      ELC_COMP.COMP_ID%TYPE,
                          I_comp_type         IN      ELC_COMP.COMP_TYPE%TYPE,
                          I_dept              IN      DEPS.DEPT%TYPE,
                          I_supplier          IN      SUPS.SUPPLIER%TYPE,
                          I_item              IN      ITEM_MASTER.ITEM%TYPE,
                          I_item_list         IN      SKULIST_HEAD.SKULIST%TYPE,
                          I_item_exp_type     IN      ITEM_EXP_DETAIL.ITEM_EXP_TYPE%TYPE,
                          I_item_exp_seq      IN      ITEM_EXP_DETAIL.ITEM_EXP_SEQ%TYPE,
                          I_hts               IN      HTS.HTS%TYPE,
                          I_import_country_id IN      HTS.IMPORT_COUNTRY_ID%TYPE,
                          I_origin_country_id IN      HTS.IMPORT_COUNTRY_ID%TYPE,
                          I_effect_from       IN      COST_COMP_UPD_STG.EFFECT_FROM%TYPE,
                          I_effect_to         IN      COST_COMP_UPD_STG.EFFECT_TO%TYPE,
                          I_exp_prof_key      IN      EXP_PROF_HEAD.EXP_PROF_KEY%TYPE,
                          I_from_loc          IN      ITEM_LOC.LOC%TYPE,
                          I_to_loc            IN      ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN; 
------------------------------------------------------------------------------------------------------
-- Function Name: DELETE_COST_COMP_EXT
-- Purpose: This function will delete the entries in elc_comp_cfa_ext tables for a corresponding delete
--          in the elc_comp table  
-------------------------------------------------------------------------------------------------------

FUNCTION DELETE_COST_COMP_EXT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_comp_id         IN      ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
END;
/

