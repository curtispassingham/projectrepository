CREATE OR REPLACE PACKAGE CONTRACT_ORDER_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------
-- Name:    BUILD_CONTRACT_ORDERS
-- Purpose: This function consolidates contract_ordloc and contract_ordsku
--            records, assigns a new order number to them and creates a
--            header record on the contract_ordhead table.
-- Created By: Phil Tenney 02-SEP-96
-- Modified By: Steve Priewe 10-16-97
---------------------------------------------------------------------------
FUNCTION BUILD_CONTRACT_ORDERS(O_error_message        IN OUT VARCHAR2,
                               I_contract_ordhead_seq IN     CONTRACT_ORDSKU.CONTRACT_ORDHEAD_SEQ%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
---
-- Name:    CREATE_CONTRACT_ORDERS
-- Purpose: This procedure creates orders on the ordhead, ordsku and ordloc
--        tables for all orders attached to the sequence number passed
--        in.  The temporary tables contract_ordhead, contract_ordsku and
--        contract_ordloc are used to build the records.  All orders are
--        created in inout status.
-- Created By: Phil Tenney, 02-SEP-96.
-- Modified By: Steve Priewe, 20-OCT-97.
-------------------------------------------------------------------------------
FUNCTION CREATE_CONTRACT_ORDERS(O_error_message         IN OUT VARCHAR2,
                                I_contract_ordhead_seq  IN     CONTRACT_ORDSKU.CONTRACT_ORDHEAD_SEQ%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Name:    POP_CONTRACT_ORDSKU
-- Purpose: Populates contract SKU for ordering.
-- Created By: Duncan Angove, 20-OCT-96.
-- Modified By:  Darcie Miller 16-OCT-97.
---------------------------------------------------------------------------
FUNCTION POP_CONTRACT_ORDSKU (I_skulist       IN SKULIST_DETAIL.SKULIST%TYPE,
                              I_seq           IN CONTRACT_ORDHEAD.CONTRACT_ORDHEAD_SEQ%TYPE,
                              O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Name:    UPDATE_ORDITEM
-- Purpose: This function adjusts the order quantity on the contract_detail and
-- contract_cost tables for items that are added or deleted from orders, and for
-- orders that are approved, unapproved, or set to cancel status.  This function
-- is also used by RECEIVE_SQL to add to the order quantity if the order has been
-- over recieved.
--
-- The I_allow_over_order_ind is used to indicate whether to allow the ordered
-- qty to exceed the contracted qty, if necessary.  This is used in receiving where
-- the ordered qty may exceed the contracted qty if the order was over received.
--
-- O_adequate_sup_avail is a boolean value used when adding to a contract qty_ordered.
-- O_adequate_sup_avail will be TRUE if sup_avail.qty_avail is >= qty_ordered
-- (indicating that there is adequate supplier availability to update this record),
-- FALSE otherwise.
--
-- The I_ignore_sup_avail parameter is used to tell the program if it should
-- ignore the supplier available qty when adding to a contract qty_ordered.
--  ->If I_ignore_sup_avail is TRUE and sup_avail.qty_avail is not adequate to
--    update this order, then this will not be an error -- the sup_avail.qty_avail
--    will be reduced to zero rather than error out.
--  ->If I_ignore_sup_avail is FALSE (do not ignore supplier availability) and
--    sup_avail.qty_avail is not adequate to update this order then an error message
--    will be created, O_adequate_sup_avail will be FALSE, but the function will
--    return TRUE so the calling program can determine what actions to take.
--
-- Created By:  Steve Priewe, 20-OCT-97.
---------------------------------------------------------------------------
FUNCTION UPDATE_ORDITEM (O_error_message        IN OUT  VARCHAR2,
                         O_adequate_sup_avail   IN OUT  BOOLEAN,
                         I_contract_no          IN      ordhead.contract_no%TYPE,
                         I_order_no             IN      ordhead.order_no%TYPE,
                         I_item                 IN      CONTRACT_ORDSKU.ITEM%TYPE,
                         I_qty_ordered          IN      contract_ordloc.qty_ordered%TYPE,
                         I_contract_type        IN      contract_header.contract_type%TYPE,
                         I_add_delete_ind       IN      VARCHAR2,
                         I_allow_over_order_ind IN      VARCHAR2,
                         I_ignore_sup_avail     IN      BOOLEAN,
                         I_changing_status      IN      BOOLEAN DEFAULT FALSE)
RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Name:   UPDATE_ORDSTAT
-- Purpose: This function adjusts the supplier available quantity and the order
-- quantity on the contract tables when an order is approved, unapproved, or cancelled.
-- Created By: Duncan Angove, 20-OCT-96.
-- Modified By: Steve Priewe, 21-OCT_97.
---------------------------------------------------------------------------
FUNCTION UPDATE_ORDSTAT (I_contract_no    IN      ordhead.contract_no%TYPE,
                         I_order_no       IN      ordhead.order_no%TYPE,
                         I_old_status     IN      ordhead.status%TYPE,
                         I_new_status     IN      ordhead.status%TYPE,
                         O_error_message  IN OUT  VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    UPDATE_CONTRACT_ORD
-- Purpose: Updates the contract_ordsku table with the contract_ordhead_seq's
-- order_no and the items contract unit cost.  In addition, this function updates
-- the contract_ordloc table with the item/location's unit retail.
-- Created By: Duncan Angove, 20-OCT-96.
-- Modified By: Steve Priewe, 22-OCT-97.
--------------------------------------------------------------------------------
FUNCTION UPDATE_CONTRACT_ORD (I_item             IN     ITEM_MASTER.ITEM%TYPE,
                              I_seq              IN     CONTRACT_ORDLOC.CONTRACT_ORDHEAD_SEQ%TYPE,
                              I_contract_no      IN     CONTRACT_ORDLOC.CONTRACT_NO%TYPE,
                              O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    CONTRACT_ITEM_EXISTS
-- Purpose: This function will check to see if an itemlist contains item(s) that
--          are on a current,approved contract.
-- Created By: Michael Johnson.
--------------------------------------------------------------------------------
FUNCTION CONTRACT_ITEM_EXISTS (O_error_message    IN OUT VARCHAR2,
                               O_contract_ind     IN OUT BOOLEAN,
                               I_skulist          IN     SKULIST_DETAIL.SKULIST%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    VALIDATE_UPDATES
-- Purpose: This function will perform some of the validation that is contained in
--          the functions update_ordstat and update_orditem.  This is used to validate
--          an order when it is submitted.
-- Created By: Michael Johnson.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_UPDATES (O_error_message    IN OUT VARCHAR2,
                           I_contract_no      IN     ORDHEAD.CONTRACT_NO%TYPE,
                           I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                           I_old_status       IN     ORDHEAD.STATUS%TYPE,
                           I_new_status       IN     ORDHEAD.STATUS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    UPDATE_CONTRACT_HEADER_DATE
-- Purpose: This function will update the contract_header.last_ordered_date field
--          when creating an order from a contract in the ordhead form.  This functionality
--          is taken care of when creating a new order with a contract through the
--          orddtl form.
-- Created By: Tara Roberts.
--------------------------------------------------------------------------------
FUNCTION UPDATE_CONTRACT_HEADER_DATE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_contract_no        IN       ORDHEAD.CONTRACT_NO%TYPE,
                                     I_order_apprv_date   IN       ORDHEAD.ORIG_APPROVAL_DATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    CONTRACT_ORDLOC_EXISTS
-- Purpose: This function will check for existing records on CONTRACT_ORDLOC.
-- Created By: Michael Johnson.
--------------------------------------------------------------------------------
FUNCTION CONTRACT_ORDLOC_EXISTS( O_error_message         IN OUT  VARCHAR2,
                                 O_exist                 IN OUT  BOOLEAN,
                                 I_contract_ordhead_seq  IN      CONTRACT_ORDLOC.CONTRACT_ORDHEAD_SEQ%TYPE,
                                 I_contract_no           IN      ORDHEAD.CONTRACT_NO%TYPE,
                                 I_item                  IN      CONTRACT_ORDLOC.ITEM%TYPE,
                                 I_loc                   IN      CONTRACT_ORDLOC.LOCATION%TYPE,
                                 I_loc_type              IN      CONTRACT_ORDLOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:    POP_CONTRACT_ORDSKU
-- Purpose: This function will be called from ADF Orders for Contracted Items screen to populate
--          the CONTRACT_ORDSKU table by item list, item, item parent and diff, and by contract number.
-------------------------------------------------------------------------------------------------------
FUNCTION POP_CONTRACT_ORDSKU( O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_contract_ordhead_seq   IN       CONTRACT_ORDHEAD.CONTRACT_ORDHEAD_SEQ%TYPE,
                              I_contract_no            IN       CONTRACT_ORDSKU.CONTRACT_NO%TYPE,
                              I_item_value_type        IN       VARCHAR2,
                              I_item_value             IN       VARCHAR2,
                              I_diff1                  IN       ITEM_MASTER.DIFF_1%TYPE DEFAULT NULL,
                              I_diff2                  IN       ITEM_MASTER.DIFF_2%TYPE DEFAULT NULL,
                              I_diff3                  IN       ITEM_MASTER.DIFF_3%TYPE DEFAULT NULL,
                              I_diff4                  IN       ITEM_MASTER.DIFF_4%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Name:    POP_CONTRACT_ORDLOC
-- Purpose: This function will insert or update the CONTRACT_ORDLOC table for the specified
--          location type, location value, quantity ordered, contract ordhead sequence number,
--          contract number, and item.
-------------------------------------------------------------------------------------------------------
FUNCTION POP_CONTRACT_ORDLOC( O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_loc_type               IN       VARCHAR2,
                              I_loc                    IN       VARCHAR2,
                              I_qty_ordered            IN       CONTRACT_ORDLOC.QTY_ORDERED%TYPE,
                              I_contract_ordhead_seq   IN       CONTRACT_ORDLOC.CONTRACT_ORDHEAD_SEQ%TYPE,
                              I_contract_no            IN       CONTRACT_ORDLOC.CONTRACT_NO%TYPE,
                              I_item                   IN       CONTRACT_ORDSKU.ITEM%TYPE,
                              I_apply_to_all_item      IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- Name:    UPDATE_DELETE_CONTRACT
-- Purpose: This function will update and delete the data from the CONTRACT_ORDLOC,
--          CONTRACT_ORDSKU and CONTRACT_ORDHEAD tables based on the sequence number.
------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_DELETE_CONTRACT( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_seq             IN       CONTRACT_ORDHEAD.CONTRACT_ORDHEAD_SEQ%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
END;
/
