CREATE OR REPLACE PACKAGE ORDER_APPROVE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-- Function   : PACK_SKU
-- Purpose    : This function blows buyer packs out to individual item level if 
--              the pack's receive_ind on packhead indicates that it will be 
--              received as 'Eaches.'
-------------------------------------------------------------------------------
FUNCTION PACK_SKU(I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function   : APPROVAL_PRIVS
-- Purpose    : This function check if the user role has the privilege to approve 
--              orders.
-------------------------------------------------------------------------------
FUNCTION APPROVAL_PRIVS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_approval_privs   IN OUT   BOOLEAN)
   return BOOLEAN;
-------------------------------------------------------------------------------
-- Function   : GET_APPROVAL_AMT
-- Purpose    : This function retrieves the maximum order approval amount associated
--              with the user's role. It returns 0 if the user does not have any
--              role privilege.
-------------------------------------------------------------------------------
FUNCTION GET_APPROVAL_AMT(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_approval_amt   IN OUT   RTK_ROLE_PRIVS.ORD_APPR_AMT%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------
END;
/
