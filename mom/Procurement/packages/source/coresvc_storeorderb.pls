CREATE OR REPLACE PACKAGE BODY CORESVC_STOREORDER AS

LP_detail    BOOLEAN := FALSE;
 
----------------------------------------------------------------------------------
    --Private Functions
----------------------------------------------------------------------------------
FUNCTION BUILD_XORDERDESC(IO_error_message    IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                          O_xorderdesc_rec    OUT NOCOPY "RIB_XOrderDesc_REC",
                          I_locpotsfdesc_rec  IN         "RIB_LocPOTsfDesc_REC",
                          I_order_no          IN         ORDHEAD.ORDER_NO%TYPE,
                          I_action            IN         VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(60) := 'CORESVC_STOREORDER.BUILD_XORDERDESC';

   L_xorderdtl_rec      "RIB_XOrderDtl_REC";
   L_xorderdtl_tbl      "RIB_XOrderDtl_TBL" := NULL;

   L_ord_status         ORDHEAD.STATUS%TYPE;
   L_message_status     VARCHAR2(1)          := NULL;
   L_loc                ORDLOC.LOCATION%TYPE;
   L_loc_type           ORDLOC.LOC_TYPE%TYPE;
   L_dept               ITEM_MASTER.DEPT%TYPE :=NULL;
   L_prev_dept          ITEM_MASTER.DEPT%TYPE :=NULL;
   L_chk_dept           BOOLEAN := TRUE;

   cursor C_GET_ORD_STATUS is
      select status
        from ordhead
       where order_no = NVL(I_locpotsfdesc_rec.order_id, I_order_no);

   cursor C_GET_DEPT(L_item IN ITEM_MASTER.ITEM%TYPE) is
      select dept
        from item_master
       where item = L_item;

BEGIN

   open C_GET_ORD_STATUS;
   fetch C_GET_ORD_STATUS into L_ord_status;
   close C_GET_ORD_STATUS;

   if I_locpotsfdesc_rec.locpotsfdtl_tbl is NOT NULL and
      I_locpotsfdesc_rec.locpotsfdtl_tbl.COUNT > 0 and
      (I_action in (LOCPOTSF_CRE, LOCPOTSF_DTL_CRE, LOCPOTSF_DTL_MOD) or
       (I_action = LOCPOTSF_MOD and L_ord_status != 'A')) then
      ---
      L_xorderdtl_tbl := "RIB_XOrderDtl_TBL"();

      FOR i IN I_locpotsfdesc_rec.locpotsfdtl_tbl.FIRST..I_locpotsfdesc_rec.locpotsfdtl_tbl.LAST LOOP

         if I_locpotsfdesc_rec.locpotsfdtl_tbl(i).item is NULL then
            IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                   'item',
                                                   'NULL',
                                                   'NOT NULL');
            return FALSE;
         end if;

         if I_locpotsfdesc_rec.locpotsfdtl_tbl(i).pack_size is NULL and
            I_action = LOCPOTSF_DTL_MOD then
            ---
            IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                   'pack_size',
                                                   'NULL',
                                                   'NOT NULL');
            return FALSE;
         end if;

         if I_locpotsfdesc_rec.locpotsfdtl_tbl(i).ordered_quantity is NULL then
            IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                   'ordered_quantity',
                                                   'NULL',
                                                   'NOT NULL');
            return FALSE;
         end if;

         if LP_detail = FALSE then
            L_loc := I_locpotsfdesc_rec.loc;
            L_loc_type := I_locpotsfdesc_rec.loc_type;
         else
            L_loc := I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc;
            L_loc_type := I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc_type;
         end if;
         if L_chk_dept = TRUE then
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_DEPT',
                             'ITEM_MASTER',
                             'Item:'||I_locpotsfdesc_rec.locpotsfdtl_tbl(i).item);
            open C_GET_DEPT (I_locpotsfdesc_rec.locpotsfdtl_tbl(i).item);

            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_DEPT',
                             'ITEM_MASTER',
                             'Item:'||I_locpotsfdesc_rec.locpotsfdtl_tbl(i).item);
            fetch C_GET_DEPT into L_dept;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_DEPT',
                             'ITEM_MASTER',
                             'Item:'||I_locpotsfdesc_rec.locpotsfdtl_tbl(i).item);
            close C_GET_DEPT;

            if L_prev_dept is NULL then
               L_prev_dept := L_dept;
            elsif L_prev_dept <> L_dept then
               L_dept := NULL;
               L_chk_dept := FALSE;
            end if;
         end if;

         L_xorderdtl_rec := "RIB_XOrderDtl_REC"(0,
                                                I_locpotsfdesc_rec.locpotsfdtl_tbl(i).item,
                                                L_loc,
                                                I_locpotsfdesc_rec.locpotsfdtl_tbl(i).unit_cost,
                                                NULL, -- REF_ITEM
                                                I_locpotsfdesc_rec.origin_country_id, -- ORIGIN_COUNTRY_ID
                                                I_locpotsfdesc_rec.locpotsfdtl_tbl(i).pack_size,
                                                0, -- QTY_ORDERED
                                                L_loc_type, -- LOC_TYPE
                                                'N', -- CANCEL_IND
                                                'N'); -- REINSTATE_IND

         if I_action in (LOCPOTSF_CRE, LOCPOTSF_DTL_CRE, LOCPOTSF_DTL_MOD) then
            L_xorderdtl_rec.qty_ordered := I_locpotsfdesc_rec.locpotsfdtl_tbl(i).ordered_quantity;
         end if;

         L_xorderdtl_tbl.extend;
         L_xorderdtl_tbl(L_xorderdtl_tbl.COUNT) := L_xorderdtl_rec;

      END LOOP;

   end if;

   if I_action in (LOCPOTSF_CRE, LOCPOTSF_MOD) then
      if I_locpotsfdesc_rec.order_status = 'P' then
         L_message_status := 'W';
      else
         L_message_status := 'A';
      end if;
   end if;

   O_xorderdesc_rec := "RIB_XOrderDesc_REC"(0,
                                            NVL(I_locpotsfdesc_rec.order_id, I_order_no),
                                            I_locpotsfdesc_rec.source_id,
                                            NULL, -- CURRENCY CODE
                                            NULL, -- TERMS
                                            I_locpotsfdesc_rec.not_before_date,
                                            I_locpotsfdesc_rec.not_after_date,
                                            NULL, -- OTB_EOW_DATE
                                            L_dept, -- DEPT
                                            NVL(L_message_status, L_ord_status),
                                            NULL, -- EXCHANGE_RATE
                                            NULL, -- INCLUDE_ON_ORD_IND
                                            NULL, -- WRITTEN_DATE
                                            L_xorderdtl_tbl,
                                            I_locpotsfdesc_rec.orig_ind, -- ORIG_IND
                                            NULL, -- EDI_PO_IND
                                            NULL, -- PRE_MARK_IND
                                            I_locpotsfdesc_rec.user_id,
                                            I_locpotsfdesc_rec.comments);


   return TRUE;

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END BUILD_XORDERDESC;
------------------------------------------------------------------------------------------
FUNCTION BUILD_XTSFDESC(O_error_message    IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                        O_xtsfdesc_rec     OUT NOCOPY "RIB_XTsfDesc_REC",
                        I_locpotsfdesc_rec IN         "RIB_LocPOTsfDesc_REC",
                        I_tsf_no           IN          TSFHEAD.TSF_NO%TYPE,
                        I_action           IN          VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'CORESVC_STOREORDER.BUILD_XTSFDESC';

   L_wh                 WH.WH%TYPE;
   L_message_status     TSFHEAD.STATUS%TYPE;

   L_xtsfdtl_rec        "RIB_XTsfDtl_REC";
   L_xtsfdtl_tbl        "RIB_XTsfDtl_TBL"  := NULL;
   
   cursor C_GET_WH_FOR_ITEM is
      select wh
        from wh,
             item_loc il
       where il.item = I_locpotsfdesc_rec.LocPOTsfDtl_TBL(1).item
         and il.loc = I_locpotsfdesc_rec.loc
         and wh.wh = NVL(il.source_wh,-1)
         and wh.physical_wh = I_locpotsfdesc_rec.source_id;

   cursor C_GET_VWH_CHANNEL_ORG_ENTITY is
      select wh
       from (
       select wh,
       wh.protected_ind,
         (case when 
               (select NVL(pw.primary_vwh,0) 
                from wh pw 
                where pw.wh=wh.physical_wh 
                )=wh.wh 
          then 1 else 2 end)prm_ind
        from wh,
             store s
       where wh.physical_wh = I_locpotsfdesc_rec.source_id
         and wh.stockholding_ind = 'Y'
         and s.store = I_locpotsfdesc_rec.loc
         and s.channel_id = wh.channel_id
         and NVL(s.org_unit_id,0)= NVL(wh.org_unit_id,0)
         and NVL(s.tsf_entity_id,0) = NVL(wh.tsf_entity_id,0)
         and wh.finisher_ind = 'N'
         and exists (select 1 
                       from item_loc il 
                      where il.item = I_locpotsfdesc_rec.LocPOTsfDtl_TBL(1).item 
                        and il.loc=wh.wh)
         order by protected_ind,prm_ind );

   cursor C_GET_VWH_BY_CHANNEL_ENTITY is
      select wh
      from (select wh,
         wh.protected_ind,
         (case when 
               (select NVL(pw.primary_vwh,0) 
                from wh pw 
                where pw.wh=wh.physical_wh 
                )=wh.wh 
          then 1 else 2 end)prm_ind
        from wh,
             store s
       where wh.physical_wh = I_locpotsfdesc_rec.source_id
         and wh.stockholding_ind = 'Y'
         and s.store = I_locpotsfdesc_rec.loc
         and s.channel_id = wh.channel_id
         and NVL(s.tsf_entity_id,0) = NVL(wh.tsf_entity_id,0)
         and wh.finisher_ind = 'N'
         and exists (select 1 
                       from item_loc il 
                      where il.item = I_locpotsfdesc_rec.LocPOTsfDtl_TBL(1).item 
                        and il.loc=wh.wh)
          order by protected_ind,prm_ind );

   cursor C_GET_VWH_BY_CHANNEL_ORG is
      select wh
       from ( select wh,
         wh.protected_ind,
         (case when 
               (select NVL(pw.primary_vwh,0) 
                from wh pw 
                where pw.wh=wh.physical_wh 
                )=wh.wh 
          then 1 else 2 end)prm_ind
        from wh,
             store s
       where wh.physical_wh = I_locpotsfdesc_rec.source_id
         and wh.stockholding_ind = 'Y'
         and s.store = I_locpotsfdesc_rec.loc
         and s.channel_id = wh.channel_id
         and NVL(s.org_unit_id,0)= NVL(wh.org_unit_id,0)
         and wh.finisher_ind = 'N'
         and exists (select 1 
                       from item_loc il 
                      where il.item = I_locpotsfdesc_rec.LocPOTsfDtl_TBL(1).item 
                        and il.loc=wh.wh)
         order by protected_ind,prm_ind ); 

   cursor C_GET_VWH_BY_CHANNEL is
      select wh
       from (select wh,
         wh.protected_ind,
         (case when 
               (select NVL(pw.primary_vwh,0) 
                from wh pw 
                where pw.wh=wh.physical_wh 
                )=wh.wh 
          then 1 else 2 end)prm_ind
        from wh,
             store s
       where wh.physical_wh = I_locpotsfdesc_rec.source_id
         and wh.stockholding_ind = 'Y'
         and s.store = I_locpotsfdesc_rec.loc
         and s.channel_id = wh.channel_id
         and wh.finisher_ind = 'N'
         and exists (select 1 
                       from item_loc il 
                      where il.item = I_locpotsfdesc_rec.LocPOTsfDtl_TBL(1).item 
                        and il.loc=wh.wh)
      order by protected_ind,prm_ind );

   cursor C_GET_PRIMARY_VWH is
      select primary_vwh
        from wh
       where wh = I_locpotsfdesc_rec.source_id;

   cursor C_GET_EXISTING_WH is
      select from_loc
        from tsfhead
       where tsf_no = I_locpotsfdesc_rec.order_id;

BEGIN

   if I_action = LOCPOTSF_CRE then
      open C_GET_WH_FOR_ITEM;
      fetch C_GET_WH_FOR_ITEM into L_wh;

      if C_GET_WH_FOR_ITEM%NOTFOUND then

         open C_GET_VWH_CHANNEL_ORG_ENTITY;
         fetch C_GET_VWH_CHANNEL_ORG_ENTITY into L_wh;

         if C_GET_VWH_CHANNEL_ORG_ENTITY%NOTFOUND then

            open C_GET_VWH_BY_CHANNEL_ENTITY;
            fetch C_GET_VWH_BY_CHANNEL_ENTITY into L_wh;

            if C_GET_VWH_BY_CHANNEL_ENTITY%NOTFOUND then

               open C_GET_VWH_BY_CHANNEL_ORG;
               fetch C_GET_VWH_BY_CHANNEL_ORG into L_wh;

               if C_GET_VWH_BY_CHANNEL_ORG%NOTFOUND then

                  open C_GET_VWH_BY_CHANNEL;
                  fetch C_GET_VWH_BY_CHANNEL into L_wh;

                  if C_GET_VWH_BY_CHANNEL%NOTFOUND then

                     open C_GET_PRIMARY_VWH;
                     fetch C_GET_PRIMARY_VWH into L_wh;
                     close C_GET_PRIMARY_VWH;

                  end if;

                  close C_GET_VWH_BY_CHANNEL;

               end if;

               close C_GET_VWH_BY_CHANNEL_ORG;

            end if;

            close C_GET_VWH_BY_CHANNEL_ENTITY;

         end if;

         close C_GET_VWH_CHANNEL_ORG_ENTITY;

      end if;

      close C_GET_WH_FOR_ITEM;  
   else
      open C_GET_EXISTING_WH;
      fetch C_GET_EXISTING_WH into L_wh;
      close C_GET_EXISTING_WH;
   end if;

   if I_action in (LOCPOTSF_CRE, LOCPOTSF_DTL_CRE, LOCPOTSF_DTL_MOD) and
      I_locpotsfdesc_rec.LocPOTsfDtl_TBL is NOT NULL and
      I_locpotsfdesc_rec.LocPOTsfDtl_TBL.COUNT > 0 then

      L_xtsfdtl_tbl := "RIB_XTsfDtl_TBL"();

      FOR i IN I_locpotsfdesc_rec.LocPOTsfDtl_TBL.FIRST..I_locpotsfdesc_rec.LocPOTsfDtl_TBL.LAST LOOP

         if I_locpotsfdesc_rec.LocPOTsfDtl_TBL(i).item is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                  'item',
                                                  'NULL',
                                                  'NOT NULL');
            return FALSE;
         end if;

         if I_locpotsfdesc_rec.LocPOTsfDtl_TBL(i).pack_size is NULL and
            I_action = LOCPOTSF_DTL_MOD then
            ---
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                  'pack_size',
                                                  'NULL',
                                                  'NOT NULL');
            return FALSE;
         end if;

         if I_locpotsfdesc_rec.LocPOTsfDtl_TBL(i).ordered_quantity is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                  'quantity',
                                                  'NULL',
                                                  'NOT NULL');
            return FALSE;
         end if;

         L_xtsfdtl_rec := "RIB_XTsfDtl_REC"(0,
                                          I_locpotsfdesc_rec.LocPOTsfDtl_TBL(i).item,
                                          I_locpotsfdesc_rec.LocPOTsfDtl_TBL(i).ordered_quantity,
                                          I_locpotsfdesc_rec.LocPOTsfDtl_TBL(i).pack_size,
                                          NULL, -- INV_STATUS
                                          NULL); -- UNIT_COST

         L_xtsfdtl_tbl.extend;
         L_xtsfdtl_tbl(L_xtsfdtl_tbl.COUNT) := L_xtsfdtl_rec;

      END LOOP;

   end if;

   if I_action in (LOCPOTSF_CRE, LOCPOTSF_MOD, LOCPOTSF_DTL_CRE, LOCPOTSF_DTL_MOD) then
      if I_locpotsfdesc_rec.order_status = 'P' then
         L_message_status := 'I';
      else
         L_message_status := 'A';
      end if;
   end if;

   O_xtsfdesc_rec := "RIB_XTsfDesc_REC"(0,
                                      NVL(I_locpotsfdesc_rec.order_id, I_tsf_no),
                                      'W', -- FROM_LOC_TYPE
                                      NVL(L_wh, I_locpotsfdesc_rec.source_id), -- FROM_LOC
                                      I_locpotsfdesc_rec.loc_type, -- TO_LOC_TYPE
                                      I_locpotsfdesc_rec.loc, -- TO_LOC
                                      I_locpotsfdesc_rec.not_before_date, -- DELIVERY_DATE
                                      NULL, -- DEPT
                                      NULL, -- ROUTING_CODE
                                      NULL, -- FREIGHT_CODE
                                      'SIM', -- TSF_TYPE
                                      L_xtsfdtl_tbl,
                                      L_message_status,
                                      I_locpotsfdesc_rec.user_id,
                                      I_locpotsfdesc_rec.comments);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BUILD_XTSFDESC;
------------------------------------------------------------------------------------------
FUNCTION LOCK_LOC_PO(IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_order_id       IN     TSFHEAD.TSF_NO%TYPE,
                     I_source_type    IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(60)  := 'CORESVC_STOREORDER.LOCK_LOC_PO';

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(25);

   cursor C_LOCK_TSFHEAD is
      select 'x'
        from tsfhead
       where tsf_no = I_order_id
         for update nowait;

   cursor C_LOCK_TSFDETAIL is
      select 'x'
        from tsfdetail
       where tsf_no = I_order_id
         for update nowait;

BEGIN

   if I_source_type = 'S' then
      if ORDER_SETUP_SQL.LOCK_ORDHEAD(IO_error_message,
                                      I_order_id) = FALSE then
         return FALSE;
      end if;

      return TRUE;
   else

      L_table := 'TSFHEAD';

      open C_LOCK_TSFHEAD;
      close C_LOCK_TSFHEAD;

      L_table := 'TSFDETAIL';

      open C_LOCK_TSFDETAIL;
      close C_LOCK_TSFDETAIL;

   end if;

   return TRUE;

EXCEPTION
    when RECORD_LOCKED then
       IO_error_message := sql_lib.create_msg('RECORD_LOCKED',
                                              L_table,
                                              I_order_id,
                                              NULL);
       return FALSE;

   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END LOCK_LOC_PO;
---------------------------------------------------------------------------------------
FUNCTION BUILD_XORDERREF(IO_error_message    IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                         O_xorder_rec        OUT NOCOPY    RIB_OBJECT,
                         I_locpotsfdesc_rec  IN            "RIB_LocPOTsfDesc_REC",
                         I_order_approved    IN            VARCHAR2)
RETURN BOOLEAN IS

   L_program     VARCHAR2(60)  := 'CORESVC_STOREORDER.BUILD_XORDERREF';

   L_xorderdtlref_rec   "RIB_XOrderDtlRef_REC";
   L_xorderdtlref_tbl   "RIB_XOrderDtlRef_TBL"  := NULL;

   L_xorderdtl_rec      "RIB_XOrderDtl_REC";
   L_xorderdtl_tbl      "RIB_XOrderDtl_TBL";

   L_loc                ORDLOC.LOCATION%TYPE;
   L_loc_type           ORDLOC.LOC_TYPE%TYPE;

BEGIN

   if I_locpotsfdesc_rec.locpotsfdtl_tbl is NOT NULL and
      I_locpotsfdesc_rec.locpotsfdtl_tbl.COUNT >0 then
      ----
      -- if the delete message has details on it and the order is already in 'A'pproved
      -- status, we need to send a mod/cancel message into RMS rather than removing the
      -- record completely.
      ----
      if I_order_approved = 'Y' then

         L_xorderdtl_tbl := "RIB_XOrderDtl_TBL"();

         FOR i IN 1..I_locpotsfdesc_rec.locpotsfdtl_tbl.COUNT LOOP

            if LP_detail = FALSE then
               L_loc := I_locpotsfdesc_rec.loc;
               L_loc_type := I_locpotsfdesc_rec.loc_type;
            else
               L_loc := I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc;
               L_loc_type := I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc_type;
            end if;

            L_xorderdtl_rec := "RIB_XOrderDtl_REC"(0,
                                                 I_locpotsfdesc_rec.locpotsfdtl_tbl(i).item,
                                                 L_loc,
                                                 I_locpotsfdesc_rec.locpotsfdtl_tbl(i).unit_cost,
                                                 NULL, -- REF_ITEM
                                                 I_locpotsfdesc_rec.origin_country_id, -- ORIGIN_COUNTRY_ID
                                                 I_locpotsfdesc_rec.locpotsfdtl_tbl(i).pack_size,
                                                 0, -- QTY_ORDERED
                                                 L_loc_type, -- LOC_TYPE
                                                 'Y', -- CANCEL_IND
                                                 'N'); -- REINSTATE_IND

            L_xorderdtl_tbl.EXTEND();
            L_xorderdtl_tbl(L_xorderdtl_tbl.COUNT) := L_xorderdtl_rec;

         END LOOP;

         O_xorder_rec := "RIB_XOrderDesc_REC"(0,
                                            I_locpotsfdesc_rec.order_id,
                                            I_locpotsfdesc_rec.source_id,
                                            NULL, -- CURRENCY CODE
                                            NULL, -- TERMS
                                            I_locpotsfdesc_rec.not_before_date,
                                            I_locpotsfdesc_rec.not_after_date,
                                            NULL, -- OTB_EOW_DATE
                                            NULL, -- DEPT
                                            I_locpotsfdesc_rec.order_status,
                                            NULL, -- EXCHANGE_RATE
                                            NULL, -- INCLUDE_ON_ORD_IND
                                            NULL, -- WRITTEN_DATE
                                            L_xorderdtl_tbl,
                                            I_locpotsfdesc_rec.orig_ind, -- ORIG_IND
                                            NULL, -- EDI_PO_IND
                                            NULL, -- PRE_MARK_IND
                                            I_locpotsfdesc_rec.user_id,
                                            I_locpotsfdesc_rec.comments);

      else -- order status is not 'A'pproved so delete the record(s)
         L_xorderdtlref_tbl := "RIB_XOrderDtlRef_TBL"();

         FOR i IN I_locpotsfdesc_rec.locpotsfdtl_tbl.FIRST..I_locpotsfdesc_rec.locpotsfdtl_tbl.LAST LOOP

            if LP_detail = FALSE then
               L_loc := I_locpotsfdesc_rec.loc;
            else
               L_loc := I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc;
            end if;

            L_xorderdtlref_rec := "RIB_XOrderDtlRef_REC"(0,
                                                       I_locpotsfdesc_rec.locpotsfdtl_tbl(i).item,
                                                       L_loc,
                                                       NULL);

            L_xorderdtlref_tbl.extend;
            L_xorderdtlref_tbl(L_xorderdtlref_tbl.COUNT) := L_xorderdtlref_rec;

         END LOOP;

         O_xorder_rec := "RIB_XOrderRef_REC"(0,
                                           I_locpotsfdesc_rec.order_id,
                                           L_xorderdtlref_tbl);

      end if;

   else -- dtl_tbl is null, delete the entire order
      ---
      -- if we get a message in to delete an already approved order, this
      -- message should be changed over to status change message - changing
      -- the status from 'A'pproved to 'C'losed will tell the XOrder API to
      -- cancel all remaining unshipped qty and close out the order.
      -- Otherwise, treat the message as a normal header delete message.
      ---
      if I_order_approved = 'Y' then

         O_xorder_rec := "RIB_XOrderDesc_REC"(0,
                                            I_locpotsfdesc_rec.order_id,
                                            I_locpotsfdesc_rec.source_id,
                                            NULL, -- CURRENCY CODE
                                            NULL, -- TERMS
                                            NULL, -- NOT_BEFORE_DATE
                                            NULL, -- NOT_AFTER_DATE
                                            NULL, -- OTB_EOW_DATE
                                            NULL, -- DEPT
                                            'C',  -- ORDER_STATUS
                                            NULL, -- EXCHANGE_RATE
                                            NULL, -- INCLUDE_ON_ORD_IND
                                            NULL, -- WRITTEN_DATE
                                            NULL, -- DTL_TBL
                                            I_locpotsfdesc_rec.orig_ind, -- ORIG_IND
                                            NULL, -- EDI_PO_IND
                                            NULL, -- PRE_MARK_IND
                                            I_locpotsfdesc_rec.user_id,
                                            I_locpotsfdesc_rec.comments);

      else
         O_xorder_rec := "RIB_XOrderRef_REC"(0,
                                           I_locpotsfdesc_rec.order_id,
                                           L_xorderdtlref_tbl);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BUILD_XORDERREF;
------------------------------------------------------------------------------------------
FUNCTION BUILD_XTSFREF(IO_error_message    IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                       O_xtsfref_rec       OUT NOCOPY    "RIB_XTsfRef_REC",
                       O_xtsfdesc_rec      OUT NOCOPY    "RIB_XTsfDesc_REC",
                       I_locpotsfdesc_rec  IN            "RIB_LocPOTsfDesc_REC")
RETURN BOOLEAN IS

   L_program     VARCHAR2(60)  := 'CORESVC_STOREORDER.BUILD_XTSFREF';

   L_xtsfdtlref_rec   "RIB_XTsfDtlRef_REC";
   L_xtsfdtlref_tbl   "RIB_XTsfDtlRef_TBL"  := NULL;

   L_xtsfdtl_rec      "RIB_XTsfDtl_REC";
   L_xtsfdtl_tbl      "RIB_XTsfDtl_TBL"  := NULL;

   L_item               TSFDETAIL.ITEM%TYPE;
   L_shipped_qty        TSFDETAIL.SHIP_QTY%TYPE;
   L_tsf_qty            TSFDETAIL.TSF_QTY%TYPE;
   L_wh                 WH.WH%TYPE;

   cursor C_CHECK_SHIPPED_QTY is
      select tsf_qty,
             NVL(ship_qty, 0)
        from tsfdetail
       where tsf_no = I_locpotsfdesc_rec.order_id
         and item = L_item;

   cursor C_GET_EXISTING_WH is
      select from_loc
        from tsfhead
       where tsf_no = I_locpotsfdesc_rec.order_id;

   cursor C_GET_TSF_DETAILS is
      select item,
             NVL(ship_qty, 0) ship_qty
        from tsfdetail
       where tsf_no = I_locpotsfdesc_rec.order_id;

BEGIN

   if I_locpotsfdesc_rec.locpotsfdtl_tbl is NOT NULL and
      I_locpotsfdesc_rec.locpotsfdtl_tbl.COUNT >0 then

      L_xtsfdtlref_tbl := "RIB_XTsfDtlRef_TBL"();
      L_xtsfdtl_tbl := "RIB_XTsfDtl_TBL"();

      FOR i IN I_locpotsfdesc_rec.locpotsfdtl_tbl.FIRST..I_locpotsfdesc_rec.locpotsfdtl_tbl.LAST LOOP

         L_item := I_locpotsfdesc_rec.locpotsfdtl_tbl(i).item;

         open C_CHECK_SHIPPED_QTY;
         fetch C_CHECK_SHIPPED_QTY into L_tsf_qty,
                                        L_shipped_qty;
         close C_CHECK_SHIPPED_QTY;

         ---
         -- If this item has already been shipped or partially shipped, the record
         -- cannot be deleted.  To get around this, the remaining amount to be
         -- shipped should be cancelled so that no more qty is sent.
         ---
         if L_shipped_qty != 0 then
            if L_shipped_qty = L_tsf_qty then
               IO_error_message := SQL_LIB.CREATE_MSG('NO_DEL_ITEM_FULLY_SHIPPED', L_item, I_locpotsfdesc_rec.order_id, NULL);
               return FALSE;
            end if;

            L_xtsfdtl_rec := "RIB_XTsfDtl_REC"(0,
                                             I_locpotsfdesc_rec.locpotsfdtl_tbl(i).item,
                                             L_shipped_qty,
                                             NULL,
                                             NULL, -- INV_STATUS
                                             NULL); -- UNIT_COST

            L_xtsfdtl_tbl.EXTEND;
            L_xtsfdtl_tbl(L_xtsfdtl_tbl.COUNT) := L_xtsfdtl_rec;

         else
            -- the item has not been shipped nor partially shipped - delete it.
            L_xtsfdtlref_rec := "RIB_XTsfDtlRef_REC"(0,
                                                   I_locpotsfdesc_rec.locpotsfdtl_tbl(i).item);

            L_xtsfdtlref_tbl.extend;
            L_xtsfdtlref_tbl(L_xtsfdtlref_tbl.COUNT) := L_xtsfdtlref_rec;
         end if;

      END LOOP;

   elsif I_locpotsfdesc_rec.order_status = 'A' then
      L_xtsfdtl_tbl := "RIB_XTsfDtl_TBL"();

      for rec in C_get_tsf_details LOOP
         L_xtsfdtl_rec := "RIB_XTsfDtl_REC"(0,
                                          rec.item,
                                          rec.ship_qty,
                                          NULL,
                                          NULL, -- INV_STATUS
                                          NULL); -- UNIT_COST

         L_xtsfdtl_tbl.EXTEND;
         L_xtsfdtl_tbl(L_xtsfdtl_tbl.COUNT) := L_xtsfdtl_rec;

      END LOOP;

   end if;

   if L_xtsfdtl_tbl is NOT NULL and
      L_xtsfdtl_tbl.COUNT != 0 then
      ---
      open C_GET_EXISTING_WH;
      fetch C_GET_EXISTING_WH into L_wh;
      close C_GET_EXISTING_WH;

      O_xtsfdesc_rec := "RIB_XTsfDesc_REC"(0,
                                         I_locpotsfdesc_rec.order_id,
                                         'W', -- FROM_LOC_TYPE
                                         L_wh, -- FROM_LOC
                                         I_locpotsfdesc_rec.loc_type, -- TO_LOC_TYPE
                                         I_locpotsfdesc_rec.loc, -- TO_LOC
                                         NULL, -- DELIVERY_DATE
                                         NULL, -- DEPT
                                         NULL, -- ROUTING_CODE
                                         NULL, -- FREIGHT_CODE
                                         'SIM', -- TSF_TYPE
                                         L_xtsfdtl_tbl,
                                         'A',  -- STATUS
                                         I_locpotsfdesc_rec.user_id,  -- USER_ID
                                         NULL); -- COMMENT_DESC
   end if;

   ---
   -- always populate the ref object.  If this is a header delete, it will be used.  If
   -- it is a detail delete, it might not be used, but the calling function will check
   -- for any details in this object and react accordingly.
   ---
   O_xtsfref_rec := "RIB_XTsfRef_REC"(0,
                                    I_locpotsfdesc_rec.order_id,
                                    L_xtsfdtlref_tbl);

   return TRUE;

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BUILD_XTSFREF;

/******************************************************************************
  Function CREATE_MOD_LOCPO
  It will create a PO or Transfer to RMS
                   PO or Transfer Details to RMS
  It will modify a PO or Transfer to RMS
                   PO or Transfer Details to RMS
  based on the action parameter                   
 ******************************************************************************/
FUNCTION CREATE_MOD_LOCPO(IO_error_message  IN OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                          O_locpotsfref_rec OUT NOCOPY "RIB_LocPOTsfRef_REC",                          
                          I_locpodesc_rec   IN          "RIB_LocPOTsfDesc_REC",
                          I_action          IN          VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'CORESVC_STOREORDER.CREATE_MOD_LOCPO';
   L_return_code        VARCHAR2(10) := 'FALSE';
   L_order_no           ORDHEAD.ORDER_NO%TYPE := NULL;
   L_tsf_no             TSFHEAD.TSF_NO%TYPE   := NULL;

   L_min_qty            NUMBER;
   L_ord_qty            NUMBER;
   L_min_flag           VARCHAR2(10);
   L_xorderdesc_rec     "RIB_XOrderDesc_REC";
   L_xtsfdesc_rec       "RIB_XTsfDesc_REC";
   L_message_type       VARCHAR2(50);
   L_status_code        VARCHAR2(10) := 'S';

BEGIN

   if I_locpodesc_rec.source_type is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'source_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_locpodesc_rec.source_type not in ('S', 'W') then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'source_type',
                                            I_locpodesc_rec.source_type,
                                            'S or W');
      return FALSE;
   end if;

   if I_locpodesc_rec.loc is NULL then
      if I_locpodesc_rec.source_type = 'S' then
         if I_action != LOCPOTSF_MOD then
            if I_locpodesc_rec.locpotsfdtl_tbl is NOT NULL and
               I_locpodesc_rec.locpotsfdtl_tbl.COUNT >0 then
               FOR i IN I_locpodesc_rec.locpotsfdtl_tbl.FIRST..I_locpodesc_rec.locpotsfdtl_tbl.LAST LOOP
                  if I_locpodesc_rec.locpotsfdtl_tbl(i).loc is NULL then
                     IO_error_message := SQL_LIB.CREATE_MSG('RMSSVC_INVALID_ENTRY',
                                                           'loc',
                                                           'NULL',
                                                           NULL);
                     return FALSE;
                  elsif I_locpodesc_rec.locpotsfdtl_tbl(i).loc = -1 then
                     IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                           'loc',
                                                           'NULL',
                                                           'NOT NULL');
                     return FALSE;
                  end if;
               END LOOP;
               LP_detail := TRUE;
            else
               IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'loc',
                                                     'NULL',
                                                     'NOT NULL');
               return FALSE;
            end if;
         end if;
      else
         IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'loc',
                                               'NULL',
                                               'NULL');
         return FALSE;
      end if;
   else
      if I_locpodesc_rec.source_type = 'S' then
         if I_locpodesc_rec.locpotsfdtl_tbl is NOT NULL and
            I_locpodesc_rec.locpotsfdtl_tbl.COUNT >0 then
            FOR i IN I_locpodesc_rec.locpotsfdtl_tbl.FIRST..I_locpodesc_rec.locpotsfdtl_tbl.LAST LOOP
               if I_locpodesc_rec.locpotsfdtl_tbl(i).loc is NOT NULL and
                  I_locpodesc_rec.locpotsfdtl_tbl(i).loc != -1 then
                  IO_error_message := SQL_LIB.CREATE_MSG('RMSSVC_INVALID_ENTRY',
                                                        'loc',
                                                        'NOT NULL',
                                                        NULL);
                  return FALSE;
               end if;
            END LOOP;
         end if;
         LP_detail := FALSE;
      end if;
   end if;

   if I_locpodesc_rec.loc_type is NULL then
      if I_locpodesc_rec.source_type = 'S' then
         if I_action != LOCPOTSF_MOD then
            if I_locpodesc_rec.locpotsfdtl_tbl is NOT NULL and 
               I_locpodesc_rec.locpotsfdtl_tbl.COUNT >0  then
               FOR i IN I_locpodesc_rec.locpotsfdtl_tbl.FIRST..I_locpodesc_rec.locpotsfdtl_tbl.LAST LOOP
                  if I_locpodesc_rec.locpotsfdtl_tbl(i).loc_type is NULL then
                     IO_error_message := SQL_LIB.CREATE_MSG('RMSSVC_INVALID_ENTRY',
                                                           'loc_type',
                                                           'NULL',
                                                           NULL);
                     return FALSE;
                  elsif I_locpodesc_rec.locpotsfdtl_tbl(i).loc_type = 'X' then
                     IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                           'loc_type',
                                                           'NULL',
                                                           'NOT NULL');
                     return FALSE;
                  end if;
               END LOOP;
            else
               IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'loc_type',
                                                     'NULL',
                                                     'NOT NULL');
               return FALSE;
            end if;
         end if;
      else
         IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'loc_type',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
   else
      if I_locpodesc_rec.source_type = 'S' then
         if I_locpodesc_rec.locpotsfdtl_tbl is NOT NULL and
            I_locpodesc_rec.locpotsfdtl_tbl.COUNT>0 then
            FOR i IN I_locpodesc_rec.locpotsfdtl_tbl.FIRST..I_locpodesc_rec.locpotsfdtl_tbl.LAST LOOP
               if I_locpodesc_rec.locpotsfdtl_tbl(i).loc_type is NOT NULL and
                  I_locpodesc_rec.locpotsfdtl_tbl(i).loc_type != 'X' then
                  IO_error_message := SQL_LIB.CREATE_MSG('RMSSVC_INVALID_ENTRY',
                                                        'loc_type',
                                                        'NOT NULL',
                                                        NULL);
                  return FALSE;
               end if;
            END LOOP;
         end if;
      end if;
   end if;

   if I_locpodesc_rec.source_id is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'source_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_locpodesc_rec.orig_ind is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'orig_ind',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_locpodesc_rec.orig_ind not in (7, 8) then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'orig_ind',
                                            I_locpodesc_rec.orig_ind,
                                            '7 (SIM) or 8 (Allocations)');
      return FALSE;
   end if;

   if I_action in (LOCPOTSF_CRE, LOCPOTSF_MOD) then
      if I_locpodesc_rec.order_status is NULL then
         IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'status',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;

      if I_locpodesc_rec.order_status not in ('P', 'A') then
         IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'status',
                                               I_locpodesc_rec.order_status,
                                               'P or A');
         return FALSE;
      end if;
   end if;

   if I_locpodesc_rec.order_id IS NOT NULL and
      I_action = LOCPOTSF_CRE then
      ---
      IO_error_message := SQL_LIB.CREATE_MSG('ORDER_MUST_BE_NULL_CRE', null, null, null);
      return FALSE;
   end if;

   if I_action in (LOCPOTSF_MOD, LOCPOTSF_DTL_MOD, LOCPOTSF_DTL_CRE) then
      if LOCK_LOC_PO(IO_error_message,
                     I_locpodesc_rec.order_id,
                     I_locpodesc_rec.source_type) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_locpodesc_rec.source_type = 'S' then
      if I_action = LOCPOTSF_CRE then
         NEXT_ORDER_NUMBER(L_order_no,
                           L_return_code,
                           IO_error_message);

         if L_return_code = 'FALSE' then
            return FALSE;
         end if;
      end if;

      if BUILD_XORDERDESC(IO_error_message,
                          L_xorderdesc_rec,
                          I_locpodesc_rec,
                          L_order_no,
                          I_action) = FALSE then
         return FALSE;
      end if;

      if I_action = LOCPOTSF_CRE then
         L_message_type := RMSSUB_XORDER.LP_cre_type;
      elsif I_action = LOCPOTSF_MOD then
         L_message_type := RMSSUB_XORDER.LP_mod_type;
      elsif I_action = LOCPOTSF_DTL_CRE then
         L_message_type := RMSSUB_XORDER.LP_dtl_cre_type;
      elsif I_action = LOCPOTSF_DTL_MOD then
         L_message_type := RMSSUB_XORDER.LP_dtl_mod_type;
      end if;

      RMSSUB_XORDER.CONSUME(L_status_code,
                            IO_error_message,
                            L_xorderdesc_rec,
                            L_message_type);

      if L_status_code != 'S' then
         return FALSE;
      end if;

      CHECK_VEND_MIN(L_xorderdesc_rec.order_no,
                     -1, -- store
                     -1, -- wh
                     L_min_qty,
                     L_ord_qty,
                     L_min_flag,
                     L_return_code,
                     IO_error_message);

      if L_return_code != 'TRUE' then
         return FALSE;
      end if;

      if L_min_flag = 'TRUE' then
         return FALSE;
      end if;

      if I_action = LOCPOTSF_CRE then
         O_locpotsfref_rec := "RIB_LocPOTsfRef_REC"(0,L_order_no);
      end if;

   else
      if I_action = LOCPOTSF_CRE then
         NEXT_TRANSFER_NUMBER(L_tsf_no,
                              L_return_code,
                              IO_error_message);

         if L_return_code = 'FALSE' then
            return FALSE;
         end if;
      end if;

      if BUILD_XTSFDESC(IO_error_message,
                        L_xtsfdesc_rec,
                        I_locpodesc_rec,
                        L_tsf_no,
                        I_action) = FALSE then
         return FALSE;
      end if;

      if I_action = LOCPOTSF_CRE then
         L_message_type := RMSSUB_XTSF.LP_cre_type;
      elsif I_action = LOCPOTSF_MOD then
         L_message_type := RMSSUB_XTSF.LP_mod_type;
      elsif I_action = LOCPOTSF_DTL_CRE then
         L_message_type := RMSSUB_XTSF.LP_dtl_cre_type;
      elsif I_action = LOCPOTSF_DTL_MOD then
         L_message_type := RMSSUB_XTSF.LP_dtl_mod_type;
      end if;

      RMSSUB_XTSF.CONSUME(L_status_code,
                          IO_error_message,
                          L_xtsfdesc_rec,
                          L_message_type);

      if L_status_code != 'S' then
         return FALSE;
      end if;

      if I_action = LOCPOTSF_CRE then
         O_locpotsfref_rec := "RIB_LocPOTsfRef_REC"(0,L_tsf_no);
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_MOD_LOCPO;
/******************************************************************************
 * Function DEL_LOCPO
 * It will Delete a PO or Transfer in RMS.
 *                  PO or Transfer Details to RMS
 * based on the action parameter                   
 ******************************************************************************/
FUNCTION DEL_LOCPO(IO_error_message    IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                   I_locpotsfdesc_rec  IN            "RIB_LocPOTsfDesc_REC",
                   I_action            IN            VARCHAR2)
RETURN BOOLEAN IS

   L_program        VARCHAR2(60)       := 'CORESVC_STOREORDER.DEL_LOCPO';
   L_xorder_rec     RIB_OBJECT;
   L_xtsfref_rec    "RIB_XTsfRef_REC";
   L_xtsfdesc_rec   "RIB_XTsfDesc_REC";
   L_message_type   VARCHAR2(15);
   L_min_qty        NUMBER;
   L_ord_qty        NUMBER;
   L_min_flag       VARCHAR2(10);
   L_return_code    VARCHAR2(10);

   L_order_rec      ORDER_SQL.ORDER_REC;

   L_order_approved VARCHAR2(1) := 'N';
   L_status_code    VARCHAR2(10) :='S';

   cursor C_CHECK_APPROVED is
      select 'Y'
        from ordhead
       where order_no = I_locpotsfdesc_rec.order_id
         and (status = 'A'
              or orig_approval_date is NOT NULL);

BEGIN
   if I_locpotsfdesc_rec.order_id is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'order_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_locpotsfdesc_rec.source_id is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'source',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_locpotsfdesc_rec.source_type is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'source_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_locpotsfdesc_rec.source_type not in ('S', 'W') then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'source_type',
                                            I_locpotsfdesc_rec.source_type,
                                            'S or W');
      return FALSE;
   end if;

   if I_locpotsfdesc_rec.loc is NULL then
      if I_locpotsfdesc_rec.source_type = 'S' then
         if I_action != LOCPOTSF_DEL then
            if I_locpotsfdesc_rec.locpotsfdtl_tbl is NOT NULL and
               I_locpotsfdesc_rec.locpotsfdtl_tbl.COUNT >0  then
               FOR i IN I_locpotsfdesc_rec.locpotsfdtl_tbl.FIRST..I_locpotsfdesc_rec.locpotsfdtl_tbl.LAST LOOP
                  if I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc is NULL then
                     IO_error_message := SQL_LIB.CREATE_MSG('RMSSVC_INVALID_ENTRY',
                                                           'loc',
                                                           'NULL',
                                                           NULL);
                     return FALSE;
                  elsif I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc = -1 then
                     IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                           'loc',
                                                           'NULL',
                                                           'NOT NULL');
                     return FALSE;
                  end if;
               END LOOP;
               LP_detail := TRUE;
            else
               IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'loc',
                                                     'NULL',
                                                     'NOT NULL');
               return FALSE;
            end if;
         end if;
      else
         IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'loc',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
   else
      if I_locpotsfdesc_rec.source_type = 'S' then
         if I_locpotsfdesc_rec.locpotsfdtl_tbl is NOT NULL and
            I_locpotsfdesc_rec.locpotsfdtl_tbl.COUNT >0  then
            FOR i IN I_locpotsfdesc_rec.locpotsfdtl_tbl.FIRST..I_locpotsfdesc_rec.locpotsfdtl_tbl.LAST LOOP
               if I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc is NOT NULL and
                  I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc != -1 then
                  IO_error_message := SQL_LIB.CREATE_MSG('RMSSVC_INVALID_ENTRY',
                                                        'loc',
                                                        'NOT NULL',
                                                        NULL);
                  return FALSE;
               end if;
            END LOOP;
         end if;
         LP_detail := FALSE;
      end if;
   end if;

   if I_locpotsfdesc_rec.loc_type is NULL then
      if I_locpotsfdesc_rec.source_type = 'S' then
         if I_action != LOCPOTSF_DEL then
            if I_locpotsfdesc_rec.locpotsfdtl_tbl is NOT NULL and
               I_locpotsfdesc_rec.locpotsfdtl_tbl.COUNT >0 then
               FOR i IN I_locpotsfdesc_rec.locpotsfdtl_tbl.FIRST..I_locpotsfdesc_rec.locpotsfdtl_tbl.LAST LOOP
                  if I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc_type is NULL then
                     IO_error_message := SQL_LIB.CREATE_MSG('RMSSVC_INVALID_ENTRY',
                                                           'loc_type',
                                                           'NULL',
                                                           NULL);
                     return FALSE;
                  elsif I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc_type = 'X' then
                     IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                           'loc_type',
                                                           'NULL',
                                                           'NOT NULL');
                     return FALSE;
                  end if;
               END LOOP;
            else
               IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'loc_type',
                                                     'NULL',
                                                    'NOT NULL');
               return FALSE;
            end if;
         end if;
      else
         IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'loc_type',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
   else
      if I_locpotsfdesc_rec.source_type = 'S' then
         if I_locpotsfdesc_rec.locpotsfdtl_tbl is NOT NULL and
            I_locpotsfdesc_rec.locpotsfdtl_tbl.COUNT >0  then
            FOR i IN I_locpotsfdesc_rec.locpotsfdtl_tbl.FIRST..I_locpotsfdesc_rec.locpotsfdtl_tbl.LAST LOOP
               if I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc_type is NOT NULL and
                  I_locpotsfdesc_rec.locpotsfdtl_tbl(i).loc_type != 'X' then
                  IO_error_message := SQL_LIB.CREATE_MSG('RMSSVC_INVALID_ENTRY',
                                                        'loc_type',
                                                        'NOT NULL',
                                                        NULL);
                  return FALSE;
               end if;
            END LOOP;
         end if;
      end if;
   end if;

   if I_action = LOCPOTSF_DTL_DEL and
      I_locpotsfdesc_rec.order_status is NULL then
      --
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'order_status',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if LOCK_LOC_PO(IO_error_message,
                  I_locpotsfdesc_rec.order_id,
                  I_locpotsfdesc_rec.source_type) = FALSE then
      return FALSE;
   end if;

   open C_CHECK_APPROVED;
   fetch C_CHECK_APPROVED into L_order_approved;
   close C_CHECK_APPROVED;

   if I_locpotsfdesc_rec.source_type = 'S' then

      if BUILD_XORDERREF(IO_error_message,
                         L_xorder_rec,
                         I_locpotsfdesc_rec,
                         L_order_approved) = FALSE then
         return FALSE;
      end if;

      if L_order_approved = 'Y' then
         L_message_type := RMSSUB_XORDER.LP_mod_type;
      else
         if I_action = LOCPOTSF_DEL then
            L_message_type := RMSSUB_XORDER.LP_del_type;
         elsif I_action = LOCPOTSF_DTL_DEL then
            L_message_type := RMSSUB_XORDER.LP_dtl_del_type;
         end if;
      end if;

      RMSSUB_XORDER.CONSUME(L_status_code,
                            IO_error_message,
                            L_xorder_rec,
                            L_message_type);

      if L_status_code != 'S' then
         return FALSE;
      end if;

   else
      if BUILD_XTSFREF(IO_error_message,
                       L_xtsfref_rec,
                       L_xtsfdesc_rec,
                       I_locpotsfdesc_rec) = FALSE then
         return FALSE;
      end if;

      if I_action = LOCPOTSF_DEL then

         if I_locpotsfdesc_rec.order_status = 'A' then
            RMSSUB_XTSF.CONSUME(L_status_code,
                                IO_error_message,
                                L_xtsfdesc_rec,
                                RMSSUB_XTSF.LP_dtl_mod_type);

            if L_status_code != 'S' then
               return FALSE;
            end if;
         else
            RMSSUB_XTSF.CONSUME(L_status_code,
                                IO_error_message,
                                L_xtsfref_rec,
                                RMSSUB_XTSF.LP_del_type);

            if L_status_code != 'S' then
               return FALSE;
            end if;
         end if;

      elsif I_action = LOCPOTSF_DTL_DEL then
         ---
         -- if anything was populated in the desc message, call consume with dtl_mo
         -- as the message type - this will take care of any partially shipped items
         -- that need to have qty cancelled.
         ---
         if L_xtsfdesc_rec is NOT NULL then
            RMSSUB_XTSF.CONSUME(L_status_code,
                                IO_error_message,
                                L_xtsfdesc_rec,
                                RMSSUB_XTSF.LP_dtl_mod_type);

            if L_status_code != 'S' then
               return FALSE;
            end if;

         end if;

         ---
         -- if there are any details in the ref message, call consume with a dtl_del
         -- message type.
         ---
         if L_xtsfref_rec.xtsfdtlref_tbl is NOT NULL and
            L_xtsfref_rec.xtsfdtlref_tbl.COUNT > 0 then
            ---
            RMSSUB_XTSF.CONSUME(L_status_code,
                                IO_error_message,
                                L_xtsfref_rec,
                                RMSSUB_XTSF.LP_dtl_del_type);

            if L_status_code != 'S' then
               return FALSE;
            end if;
         end if;

      end if;

   end if;

   CHECK_VEND_MIN(I_locpotsfdesc_rec.order_id,
                  -1, -- store
                  -1, -- wh
                  L_min_qty,
                  L_ord_qty,
                  L_min_flag,
                  L_return_code,
                  IO_error_message);

   if L_return_code != 'TRUE' then
      return FALSE;
   end if;

   if L_min_flag = 'TRUE' then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEL_LOCPO;
/******************************************************************************
 * Function GET_DEALS
 * It will retrieve Deals Information from RMS     
 ******************************************************************************/
FUNCTION GET_DEALS(IO_error_message            IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                   O_locpotsfdealscoldesc_rec  OUT NOCOPY    "RIB_LocPOTsfDealsColDesc_REC",                  
                   I_locpotsfdealscrivo_rec    IN            "RIB_LocPOTsfDealsCriVo_REC")
RETURN BOOLEAN IS
   L_program                VARCHAR2(60)  := 'CORESVC_STOREORDER.GET_DEALS';
   L_locpotsfdealscrivo_rec "RIB_LocPOTsfDealsCriVo_REC";
   L_locpotsfdealsdesc_tbl  "RIB_LocPOTsfDealsDesc_TBL" := "RIB_LocPOTsfDealsDesc_TBL"();   
   L_locpotsfdealsdesc_rec  "RIB_LocPOTsfDealsDesc_REC";
   L_status_code            VARCHAR2(10) :='S';
   L_collection_size        NUMBER :=0;

   cursor C_GET_ORG_HIER is
      select a.chain,a.area,r.region,d.district,s.store
        from area a,
             region r,
             district d,
             store s
       where a.area = r.area
         and r.region = d.region
         and d.district = s.district
         and s.store = L_locpotsfdealscrivo_rec.loc;

   L_org_hier_rec    C_GET_ORG_HIER%ROWTYPE;

   cursor C_GET_MERCH_HIER is
      select g.division,
             g.group_no,
             d.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1,
             im.diff_2,
             im.diff_3,
             im.diff_4
        from groups g,
             deps d,
             item_master im
       where im.item = L_locpotsfdealscrivo_rec.item
         and im.dept = d.dept
         and d.group_no = g.group_no;

   L_merch_hier_rec    C_GET_MERCH_HIER%ROWTYPE;

   cursor C_GET_ITEMS_DEALS is
      select dh.deal_id,
             dh.type,
             dh.active_date,
             dh.close_date,
             dd.deal_class,
             dh.threshold_limit_type,
             dd.threshold_value_type,
             dt.lower_limit,
             dt.upper_limit,
             dt.value
        from deal_head dh,
             deal_detail dd,
             deal_threshold dt
       where dh.deal_id = dd.deal_id
         and dd.deal_id = dt.deal_id(+)
         and dd.deal_detail_id = dt.deal_detail_id(+)
         and dh.status = 'A'
         and dh.supplier = L_locpotsfdealscrivo_rec.source_id
         and dh.active_date <= L_locpotsfdealscrivo_rec.not_before_date
         and (dh.close_date is NULL OR dh.close_date >= L_locpotsfdealscrivo_rec.not_before_date)
         and dh.billing_type = 'OI'
         and exists (select 'x'
                       from deal_itemloc dil
                      where dil.deal_id = dh.deal_id
                        and dil.deal_detail_id = dd.deal_detail_id
                        and excl_ind = 'N'
                        and ((org_level = 5 and location = L_locpotsfdealscrivo_rec.loc)
                             or (org_level = 4 and district = L_org_hier_rec.district)
                             or (org_level = 3 and region = L_org_hier_rec.region)
                             or (org_level = 2 and area = L_org_hier_rec.area)
                             or (org_level = 1 and chain = L_org_hier_rec.chain))
                        and (merch_level = 1
                             or (merch_level = 2 and division = L_merch_hier_rec.division)
                             or (merch_level = 3 and group_no = L_merch_hier_rec.group_no)
                             or (merch_level = 4 and dept = L_merch_hier_rec.dept)
                             or (merch_level = 5 and class = L_merch_hier_rec.class)
                             or (merch_level = 6 and subclass = L_merch_hier_rec.subclass)
                             or (merch_level = 7 and item_parent = L_merch_hier_rec.item_parent)
                             or (merch_level = 8 and item_parent = L_merch_hier_rec.item_parent
                                                 and diff_1 = L_merch_hier_rec.diff_1)
                             or (merch_level = 9 and item_parent = L_merch_hier_rec.item_parent
                                                 and diff_2 = L_merch_hier_rec.diff_2)
                             or (merch_level = 10 and item_parent = L_merch_hier_rec.item_parent
                                                  and diff_3 = L_merch_hier_rec.diff_3)
                             or (merch_level = 11 and item_parent = L_merch_hier_rec.item_parent
                                                  and diff_4 = L_merch_hier_rec.diff_4)
                             or (merch_level = 12 and item = L_locpotsfdealscrivo_rec.item)));


   TYPE GET_ITEMS_DEALS_TBL is TABLE OF C_GET_ITEMS_DEALS%ROWTYPE
      INDEX BY BINARY_INTEGER;

   L_get_items_deals  GET_ITEMS_DEALS_TBL;

BEGIN

     L_locpotsfdealscrivo_rec := I_locpotsfdealscrivo_rec;

   if L_locpotsfdealscrivo_rec.source_id is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'source_id',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   if L_locpotsfdealscrivo_rec.loc is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'loc',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   if L_locpotsfdealscrivo_rec.loc_type is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'loc_type',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   if L_locpotsfdealscrivo_rec.item is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'item',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   if L_locpotsfdealscrivo_rec.not_before_date is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'not_before_date',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   open C_GET_ORG_HIER;
   fetch C_GET_ORG_HIER into L_org_hier_rec;
   close C_GET_ORG_HIER;

   open C_GET_MERCH_HIER;
   fetch C_GET_MERCH_HIER into L_merch_hier_rec;
   close C_GET_MERCH_HIER;

   open C_GET_ITEMS_DEALS;
   fetch C_GET_ITEMS_DEALS bulk collect into L_get_items_deals;
   close C_GET_ITEMS_DEALS;

   FOR i IN 1..L_get_items_deals.COUNT LOOP

      L_locpotsfdealsdesc_rec := "RIB_LocPOTsfDealsDesc_REC"(0,
                                                     L_get_items_deals(i).deal_id,
                                                     L_get_items_deals(i).type,
                                                     L_get_items_deals(i).active_date,
                                                     L_get_items_deals(i).close_date,
                                                     L_get_items_deals(i).deal_class,
                                                     L_get_items_deals(i).threshold_limit_type,
                                                     L_get_items_deals(i).threshold_value_type,
                                                     L_get_items_deals(i).lower_limit,
                                                     L_get_items_deals(i).upper_limit,
                                                     L_get_items_deals(i).value);

      L_locpotsfdealsdesc_tbl.EXTEND();
      L_locpotsfdealsdesc_tbl(L_locpotsfdealsdesc_tbl.COUNT) := L_locpotsfdealsdesc_rec;

   END LOOP;
   if L_locpotsfdealsdesc_tbl IS NOT NULL then
      L_collection_size := L_locpotsfdealsdesc_tbl.COUNT;
   end if;
   O_locpotsfdealscoldesc_rec := "RIB_LocPOTsfDealsColDesc_REC"(0,
                                                               L_collection_size,
                                                               L_locpotsfdealsdesc_tbl);

   return TRUE;

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEALS;
/******************************************************************************
 * Function GET_ITEMS_SALES
 * It will retrieve the Item Sales information from RMS.   
 ******************************************************************************/
FUNCTION GET_ITEMS_SALES(IO_error_message                 IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                         O_locpotsfitmslscoldesc_rec      OUT NOCOPY "RIB_LocPOTsfItmSlsColDesc_REC",                         
                         I_locpoitmslsreq_rec             IN         "RIB_LocPOTsfItmSlsCriVo_REC")
RETURN BOOLEAN IS

   L_program   VARCHAR2(60)  := 'CORESVC_STOREORDER.GET_ITEMS_SALES';
   L_locpoitmslsreqdtl_rec  "RIB_LocPOTsfItmSlsCriVo_REC";
   L_locpotsfitmslsdesc_rec "RIB_LocPOTsfItmSlsDesc_REC";
   L_locpotsfitmslsdesc_tbl  "RIB_LocPOTsfItmSlsDesc_TBL"  := "RIB_LocPOTsfItmSlsDesc_TBL"();
   L_status_code            VARCHAR2(10)             := 'S';


   cursor C_GET_ITEMS_SALES is
      select eow_date,
             sum(sales_issues) sales_issues,
             value,
             sales_type
        from item_loc_hist
       where item = L_locpoitmslsreqdtl_rec.item
         and loc = L_locpoitmslsreqdtl_rec.loc
         and loc_type = 'S'
         and sales_type != 'I'
       group by eow_date, value, sales_type;

   TYPE ITEM_SALES_DATA is TABLE OF C_GET_ITEMS_SALES%ROWTYPE
     INDEX BY BINARY_INTEGER;

   L_item_sales_data  ITEM_SALES_DATA;
   L_collection_size  NUMBER :=0;
BEGIN
   L_locpoitmslsreqdtl_rec := I_locpoitmslsreq_rec;

   if L_locpoitmslsreqdtl_rec.loc is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'loc',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   if L_locpoitmslsreqdtl_rec.loc_type is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'loc_type',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   if L_locpoitmslsreqdtl_rec.item is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'item',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   open C_GET_ITEMS_SALES;
   fetch C_GET_ITEMS_SALES bulk collect into L_item_sales_data;
   close C_GET_ITEMS_SALES;

   for i in 1..L_item_sales_data.COUNT LOOP

      L_locpotsfitmslsdesc_rec := "RIB_LocPOTsfItmSlsDesc_REC"(0,
                                                           L_item_sales_data(i).eow_date,
                                                           L_item_sales_data(i).sales_issues,
                                                           L_item_sales_data(i).value,
                                                           L_item_sales_data(i).sales_type);

      L_locpotsfitmslsdesc_tbl.EXTEND();
      L_locpotsfitmslsdesc_tbl(L_locpotsfitmslsdesc_tbl.COUNT) := L_locpotsfitmslsdesc_rec;

   END LOOP;
   if L_locpotsfitmslsdesc_tbl IS NOT NULL then
      L_collection_size := L_locpotsfitmslsdesc_tbl.COUNT;
   end if; 
   O_locpotsfitmslscoldesc_rec := "RIB_LocPOTsfItmSlsColDesc_REC"(0,
                                                                 L_collection_size,
                                                                 L_locpotsfitmslsdesc_tbl);

   return TRUE;

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMS_SALES;
/******************************************************************************
 * Function GET_ITEMS_SALES
 * It will Retrieve header level PO or transfer information from RMS.   
 ******************************************************************************/
FUNCTION GET_STORE_ORDERS(IO_error_message          IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                          O_locpotsfhdrcoldesc_rec  OUT NOCOPY    "RIB_LocPOTsfHdrColDesc_REC",                          
                          I_locpotsfhdrcrivo_rec    IN            "RIB_LocPOTsfHdrCriVo_REC")
RETURN BOOLEAN IS  

   L_program              VARCHAR2(60) := 'CORESVC_STOREORDER.GET_STORE_ORDERS';
   L_filter_data          "RIB_LocPOTsfHdrCriVo_REC";
   L_locpotsfhdrdesc_rec  "RIB_LocPOTsfHdrDesc_REC";
   L_locpotsfhdrdesc_tbl  "RIB_LocPOTsfHdrDesc_TBL" := "RIB_LocPOTsfHdrDesc_TBL"();
   L_status_code          VARCHAR2(10) := 'S';

   cursor C_GET_STORE_PO_DATA(L_po_status1 VARCHAR2,
                              L_po_status2 VARCHAR2,
                              L_tsf_status1 VARCHAR2,
                              L_tsf_status2 VARCHAR2,
                              L_tsf_status3 VARCHAR2) is
      select oh.order_no order_id,
             'S' source_type,
             oh.supplier source_id,
             oh.written_date create_date,
             DECODE(oh.status, 'W', 'P',
                               'S', 'P',
                               oh.status) order_status,
             oh.not_before_date not_before_date,
             oh.not_after_date not_after_date,
             oh.orig_approval_id user_id,
             sum(ol.qty_ordered) quantity
        from ordhead oh,
             ordloc ol
       where oh.order_no = NVL(L_filter_data.order_id, oh.order_no)
         and ol.item = NVL(L_filter_data.item, ol.item)
         and ol.order_no = oh.order_no
         and ((oh.location = L_filter_data.loc
               and ol.location is NULL)
              or (1 = (select count(distinct(location))
                         from ordloc
                        where order_no = oh.order_no)
                  and (ol.location = L_filter_data.loc)))
         and 'S' = NVL(L_filter_data.source_type, 'S')
         and (('S' = L_filter_data.source_type
         and oh.supplier = NVL(L_filter_data.source_id, oh.supplier))
             or L_filter_data.source_type is NULL)
         and (L_filter_data.order_status is NULL
             or oh.status in (L_po_status1, L_po_status2))
         and (oh.not_before_date BETWEEN NVL(L_filter_data.not_before_date, oh.not_before_date)
                                     AND NVL(L_filter_data.not_after_date, oh.not_before_date)
              or oh.not_after_date BETWEEN NVL(L_filter_data.not_before_date, oh.not_after_date)
                                       AND NVL(L_filter_data.not_after_date, oh.not_after_date)
              or (L_filter_data.not_before_date BETWEEN oh.not_before_date AND oh.not_after_date
                  and L_filter_data.not_before_date = L_filter_data.not_after_date))
     group by oh.order_no, oh.supplier, oh.written_date, oh.status, oh.not_before_date, oh.not_after_date, oh.orig_approval_id
      UNION ALL
      select th.tsf_no order_id,
             'W' source_type,
             wh.physical_wh source_id,
             th.create_date create_date,
             DECODE(th.status, 'I', 'P',
                               'B', 'P',
                               'S', 'C',
                               'P', 'A',
                               'L', 'A',
                               th.status) order_status,
             th.delivery_date not_before_date,
             th.delivery_date not_after_date,
             th.create_id user_id,
             sum(td.tsf_qty) quantity
        from tsfhead th,
             tsfdetail td,
             wh
       where th.tsf_no = NVL(L_filter_data.order_id, th.tsf_no)
         and td.item = NVL(L_filter_data.item, td.item)
         and td.tsf_no = th.tsf_no
         and th.to_loc = L_filter_data.loc
         and th.from_loc_type = 'W'
         and 'W' = NVL(L_filter_data.source_type, 'W')
         and (('W' = L_filter_data.source_type
              and (th.from_loc in (select wh
                                     from wh
                                    where physical_wh = L_filter_data.source_id) or
                   L_filter_data.source_id is NULL))
              or L_filter_data.source_type is NULL)
         and (L_filter_data.order_status is NULL
              or th.status in (L_tsf_status1, L_tsf_status2, L_tsf_status3))
         and th.delivery_date BETWEEN NVL(L_filter_data.not_before_date, th.delivery_date)
                                  AND NVL(L_filter_data.not_after_date, th.delivery_date)
         and th.status != 'D'
         and th.inventory_type = 'A'
         and wh.wh = th.from_loc
    group by th.tsf_no, wh.physical_wh, wh.wh, th.create_date, th.status, th.delivery_date, th.create_id;

   TYPE STORE_PO_DATA is TABLE OF C_GET_STORE_PO_DATA%ROWTYPE
      INDEX BY BINARY_INTEGER;

   L_store_po_data   STORE_PO_DATA;
   L_collection_size NUMBER :=0;
BEGIN

   L_filter_data := I_locpotsfhdrcrivo_rec;

   if L_filter_data.loc is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'loc',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   if L_filter_data.loc_type is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'loc_type',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   if L_filter_data.order_status is NOT NULL and
      L_filter_data.order_status not in ('A', 'P', 'C') then
      ---
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'order_status',
                                             L_filter_data.order_status,
                                             'A, P or C');
      return FALSE;
   end if;

   if L_filter_data.source_type is NOT NULL and
      L_filter_data.source_type not in ('S', 'W') then
      ---
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'source_type',
                                             L_filter_data.source_type,
                                             'S or W');
      return FALSE;
   end if;

   -- open the cursor passing in parameters based on the requested status

   if L_filter_data.order_status is NULL then
      open C_GET_STORE_PO_DATA(NULL, NULL, NULL, NULL, NULL);
   elsif L_filter_data.order_status = 'C' then
      open C_GET_STORE_PO_DATA('C', NULL, 'C', 'S', NULL);
   elsif L_filter_data.order_status = 'A' then
      open C_GET_STORE_PO_DATA('A', NULL, 'A', 'L', 'P');
   elsif L_filter_data.order_status = 'P' then
      open C_GET_STORE_PO_DATA('S', 'W', 'I', 'B', NULL);
   end if;

   fetch C_GET_STORE_PO_DATA bulk collect into L_store_po_data;

   close C_GET_STORE_PO_DATA;

   for i in 1..L_store_po_data.COUNT LOOP
      L_locpotsfhdrdesc_rec := "RIB_LocPOTsfHdrDesc_REC"(0,
                                                       L_store_po_data(i).order_id,
                                                       L_store_po_data(i).source_type,
                                                       L_store_po_data(i).source_id,
                                                       L_store_po_data(i).create_date,
                                                       L_store_po_data(i).order_status,
                                                       L_store_po_data(i).not_before_date,
                                                       L_store_po_data(i).not_after_date,
                                                       L_store_po_data(i).user_id,
                                                       L_store_po_data(i).quantity);

      L_locpotsfhdrdesc_tbl.EXTEND;
      L_locpotsfhdrdesc_tbl(L_locpotsfhdrdesc_tbl.COUNT) := L_locpotsfhdrdesc_rec;

   END LOOP;
   if L_locpotsfhdrdesc_tbl IS NOT NULL then
      L_collection_size := L_locpotsfhdrdesc_tbl.COUNT;
   end if;
   O_locpotsfhdrcoldesc_rec := "RIB_LocPOTsfHdrColDesc_REC"(0,
                                                           L_collection_size,
                                                           L_locpotsfhdrdesc_tbl);

   return TRUE;

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END GET_STORE_ORDERS;
/******************************************************************************
 * Function GET_ITEMS_SALES
 * It will retrieve the header/details of a PO or transfer from RMS.
 ******************************************************************************/
FUNCTION GET_STORE_ORDER_DETAILS(IO_error_message         IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_locpotsfdesc_rec       OUT NOCOPY "RIB_LocPOTsfDesc_REC",                                 
                                 I_locpotsfdtlscrivo_rec  IN         "RIB_LocPOTsfDtlsCriVo_REC")
   RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'CORESVC_STOREORDER.GET_STORE_ORDER_DETAILS';
   L_rib_locpotsfdtlscrivo_rec  "RIB_LocPOTsfDtlsCriVo_REC";
   L_locpotsfdesc_rec     "RIB_LocPOTsfDesc_REC" := "RIB_LocPOTsfDesc_REC"(0, null, null, null, null, null, null,
                                                                          null, null, null, null, null, null,null ,null);                                                                      
   L_locpotsfdtl_rec      "RIB_LocPOTsfDtl_REC";
   L_locpotsfdtl_tbl      "RIB_LocPOTsfDtl_TBL"  := "RIB_LocPOTsfDtl_TBL"();
   L_status_code          VARCHAR2(10) := 'S';
   
   cursor C_GET_ORDHEAD_INFO is
      select location,
             loc_type,
             orig_approval_id,
             comment_desc,
             status,
             not_before_date,
             not_after_date,
             written_date
        from ordhead
       where order_no = L_rib_locpotsfdtlscrivo_rec.order_id
         and supplier = L_rib_locpotsfdtlscrivo_rec.source_id;

   cursor C_GET_ORDLOC_INFO is
      select ol.item,
             ol.location,
             os.supp_pack_size,
             ol.qty_ordered,
             ol.unit_cost
        from ordloc ol,
             ordsku os
       where ol.order_no = L_rib_locpotsfdtlscrivo_rec.order_id
         and ol.order_no = os.order_no
         and ol.item = os.item;

   cursor C_GET_TSFHEAD_INFO is
      select to_loc,
             to_loc_type,
             create_id,
             comment_desc,
             status,
             delivery_date,
             delivery_date,
             create_date
        from tsfhead
       where tsf_no = L_rib_locpotsfdtlscrivo_rec.order_id
         and from_loc_type = 'W'
         and status != 'D';

   cursor C_GET_TSFDETAIL_INFO is
      select item,
             supp_pack_size,
             sum(tsf_qty) tsf_qty
        from tsfdetail
       where tsf_no = L_rib_locpotsfdtlscrivo_rec.order_id
    group by item, supp_pack_size;

   TYPE ORDER_DETAIL_DATA is TABLE OF C_GET_ORDLOC_INFO%ROWTYPE
      INDEX BY BINARY_INTEGER;

   TYPE TSF_DETAIL_DATA is TABLE OF C_GET_TSFDETAIL_INFO%ROWTYPE
      INDEX BY BINARY_INTEGER;

   L_order_detail_data   ORDER_DETAIL_DATA;
   L_tsf_detail_data     TSF_DETAIL_DATA;

BEGIN
   L_rib_locpotsfdtlscrivo_rec := I_locpotsfdtlscrivo_rec;
   if L_rib_locpotsfdtlscrivo_rec.order_id is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'order_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if L_rib_locpotsfdtlscrivo_rec.source_type is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'source_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if L_rib_locpotsfdtlscrivo_rec.source_type not in ('S', 'W') then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'source_type',
                                            L_rib_locpotsfdtlscrivo_rec.source_type,
                                            'S or W');
      return FALSE;
   end if;

   if L_rib_locpotsfdtlscrivo_rec.source_id is NULL then
      IO_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'source_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   L_locpotsfdesc_rec.order_id            := L_rib_locpotsfdtlscrivo_rec.order_id;
   L_locpotsfdesc_rec.source_type         := L_rib_locpotsfdtlscrivo_rec.source_type;
   L_locpotsfdesc_rec.source_id           := L_rib_locpotsfdtlscrivo_rec.source_id;
   L_locpotsfdesc_rec.orig_ind            := NULL;
   L_locpotsfdesc_rec.origin_country_id   := NULL;
   

   if L_rib_locpotsfdtlscrivo_rec.source_type = 'S' then

      OPEN C_GET_ORDHEAD_INFO;
      FETCH C_GET_ORDHEAD_INFO into L_locpotsfdesc_rec.loc,
                                    L_locpotsfdesc_rec.loc_type,
                                    L_locpotsfdesc_rec.user_id,
                                    L_locpotsfdesc_rec.comments,
                                    L_locpotsfdesc_rec.order_status,
                                    L_locpotsfdesc_rec.not_before_date,
                                    L_locpotsfdesc_rec.not_after_date,
                                    L_locpotsfdesc_rec.create_date;

      if C_GET_ORDHEAD_INFO%NOTFOUND then
         CLOSE C_GET_ORDHEAD_INFO;

         IO_error_message := SQL_LIB.CREATE_MSG('INV_ORD_DTL_REQ_DATA',
                                               L_rib_locpotsfdtlscrivo_rec.order_id,
                                               L_rib_locpotsfdtlscrivo_rec.source_type,
                                               L_rib_locpotsfdtlscrivo_rec.source_id);
         return FALSE;
      end if;

      CLOSE C_GET_ORDHEAD_INFO;

      OPEN C_GET_ORDLOC_INFO;
      FETCH C_GET_ORDLOC_INFO BULK COLLECT INTO L_order_detail_data;
      CLOSE C_GET_ORDLOC_INFO;

      for i in 1..L_ORDER_DETAIL_DATA.COUNT LOOP
         L_locpotsfdtl_rec := "RIB_LocPOTsfDtl_REC"(0,
                                                    L_order_detail_data(i).item,
                                                    L_order_detail_data(i).supp_pack_size,
                                                    L_order_detail_data(i).qty_ordered,
                                                    L_order_detail_data(i).unit_cost,
                                                    null, -- orig_qty 
                                                    null, -- loc can be location from ordloc
                                                    null  -- loc_type can be found from ordloc 
                                                    );

         L_locpotsfdtl_tbl.EXTEND;
         L_locpotsfdtl_tbl(L_locpotsfdtl_tbl.COUNT) := L_locpotsfdtl_rec;

      END LOOP;

      if L_locpotsfdesc_rec.loc is NULL and
         L_order_detail_data is not NULL and
         L_order_detail_data.COUNT >= 1 then
         ---
         L_locpotsfdesc_rec.loc := L_order_detail_data(1).location;
      end if;

      L_locpotsfdesc_rec.locpotsfdtl_tbl  := L_locpotsfdtl_tbl;
   else
      OPEN C_GET_TSFHEAD_INFO;
      FETCH C_GET_TSFHEAD_INFO into L_locpotsfdesc_rec.loc,
                                    L_locpotsfdesc_rec.loc_type,
                                    L_locpotsfdesc_rec.user_id,
                                    L_locpotsfdesc_rec.comments,
                                    L_locpotsfdesc_rec.order_status,
                                    L_locpotsfdesc_rec.not_before_date,
                                    L_locpotsfdesc_rec.not_after_date,
                                    L_locpotsfdesc_rec.create_date;

      if C_GET_TSFHEAD_INFO%NOTFOUND then
         CLOSE C_GET_TSFHEAD_INFO;

         IO_error_message := SQL_LIB.CREATE_MSG('INV_TSF_ID', L_rib_locpotsfdtlscrivo_rec.order_id, NULL, NULL);
         return FALSE;
      end if;

      CLOSE C_GET_TSFHEAD_INFO;

      OPEN C_GET_TSFDETAIL_INFO;
      FETCH C_GET_TSFDETAIL_INFO BULK COLLECT INTO L_tsf_detail_data;
      CLOSE C_GET_TSFDETAIL_INFO;

      for i in 1..L_TSF_DETAIL_DATA.COUNT LOOP
         L_locpotsfdtl_rec := "RIB_LocPOTsfDtl_REC"(0,
                                                    L_tsf_detail_data(i).item,
                                                    L_tsf_detail_data(i).supp_pack_size,
                                                    L_tsf_detail_data(i).tsf_qty,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL);
         L_locpotsfdtl_tbl.EXTEND;
         L_locpotsfdtl_tbl(L_locpotsfdtl_tbl.COUNT) := L_locpotsfdtl_rec;

      end loop;

      L_locpotsfdesc_rec.locpotsfdtl_tbl  := L_locpotsfdtl_tbl;

   end if;

   O_locpotsfdesc_rec := L_locpotsfdesc_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_STORE_ORDER_DETAILS;

END CORESVC_STOREORDER;
/