
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_PAYTERM_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------
FUNCTION PERSIST(O_error_message      OUT    RTK_ERRORS.RTK_TEXT%TYPE,  
                 I_message            IN     TERMS_SQL.PAYTERM_REC,
                 I_message_type       IN     VARCHAR2)
   RETURN BOOLEAN; 
---------------------------------------------------------------------
END;
/