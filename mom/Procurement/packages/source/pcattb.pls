CREATE OR REPLACE PACKAGE BODY PC_ATTRIB_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION GET_DESC(I_cost_change     IN       NUMBER,
                  I_type            IN       VARCHAR2,
                  O_description     IN OUT   COST_SUSP_SUP_HEAD.COST_CHANGE_DESC%TYPE,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(60) := 'PC_ATTRIB_SQL.GET_DESC'; 
   cursor C_COST_SUSP_SUP_HEAD is
      select cost_change_desc
        from cost_susp_sup_head
       where cost_change = I_cost_change;
BEGIN

   if I_type in ('SKU', 'SUP', 'C', 'ITEM') then
   -- Cost Change
      SQL_LIB.SET_MARK('OPEN', 
                       'C_COST_SUSP_SUP_HEAD', 
                       'COST_SUSP_SUP_HEAD', 
                       NULL);
      open C_COST_SUSP_SUP_HEAD;
      SQL_LIB.SET_MARK('FETCH', 
                       'C_COST_SUSP_SUP_HEAD', 
                       'COST_SUSP_SUP_HEAD', 
                       NULL);
      fetch C_COST_SUSP_SUP_HEAD into O_description;
      if C_COST_SUSP_SUP_HEAD%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_COST_CHANGE', 
                                               NULL, 
                                               NULL, 
                                               NULL);
         SQL_LIB.SET_MARK('CLOSE', 
                          'C_COST_SUSP_SUP_HEAD', 
                          'COST_SUSP_SUP_HEAD', 
                          NULL);
         close C_COST_SUSP_SUP_HEAD;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 
                       'C_COST_SUSP_SUP_HEAD', 
                       'COST_SUSP_SUP_HEAD', 
                       NULL);
      close C_COST_SUSP_SUP_HEAD;
   else
   -- Invalid Type
      O_error_message := SQL_LIB.CREATE_MSG('INV_COST_CHG_TYPE', 
                                            NULL, 
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_DESC;
---------------------------------------------------------------------------------------------


FUNCTION GET_HEADER_INFO(I_cost_change     IN       NUMBER,
                         I_type            IN       VARCHAR2,
                         O_description     IN OUT   COST_SUSP_SUP_HEAD.COST_CHANGE_DESC%TYPE,
                         O_reason          IN OUT   COST_SUSP_SUP_HEAD.REASON%TYPE,
                         O_status          IN OUT   COST_SUSP_SUP_HEAD.STATUS%TYPE,
                         O_active_date     IN OUT   COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE, 
                         O_approval_date   IN OUT   COST_SUSP_SUP_HEAD.APPROVAL_DATE%TYPE,
                         O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS
   L_program VARCHAR2(60) := 'PC_ATTRIB_SQL.GET_HEADER_INFO';
   cursor C_COST_SUSP_SUP_HEAD is
      select cost_change_desc,
             reason,
             status,
             active_date,
             approval_date
        from cost_susp_sup_head
       where cost_change = I_cost_change;
BEGIN

   if I_type in ('SKU', 'SUP', 'C', 'ITEM') then
   -- Cost Change
      SQL_LIB.SET_MARK('OPEN', 
                       'C_COST_SUSP_SUP_HEAD', 
                       'COST_SUSP_SUP_HEAD', 
                       NULL);
      open C_COST_SUSP_SUP_HEAD;
      SQL_LIB.SET_MARK('FETCH', 
                       'C_COST_SUSP_SUP_HEAD', 
                       'COST_SUSP_SUP_HEAD', 
                       NULL);
      fetch C_COST_SUSP_SUP_HEAD into O_description,
                                      O_reason,
                                      O_status,
                                      O_active_date,
                                      O_approval_date;
      if C_COST_SUSP_SUP_HEAD%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_COST_CHANGE', 
                                               NULL, 
                                               NULL, 
                                               NULL);
         SQL_LIB.SET_MARK('CLOSE', 
                          'C_COST_SUSP_SUP_HEAD', 
                          'COST_SUSP_SUP_HEAD', 
                          NULL);
         close C_COST_SUSP_SUP_HEAD;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 
                       'C_COST_SUSP_SUP_HEAD', 
                       'COST_SUSP_SUP_HEAD', 
                       NULL);
      close C_COST_SUSP_SUP_HEAD;
   else
   -- Invalid Type
      O_error_message := SQL_LIB.CREATE_MSG('INV_COST_CHG_TYPE', 
                                            NULL, 
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_HEADER_INFO;
---------------------------------------------------------------------------------------------
FUNCTION GET_REASON_DESC(I_reason          IN       COST_CHG_REASON.REASON%TYPE,
                         I_type            IN       VARCHAR2,
                         O_description     IN OUT   COST_CHG_REASON_TL.REASON_DESC%TYPE,
                         O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE) 
   RETURN BOOLEAN is

   L_program VARCHAR2(60) := 'PC_ATTRIB_SQL.GET_REASON_DESC';

   cursor C_COST_CHG_REASON is
      select reason_desc
        from v_cost_chg_reason_tl
       where reason = I_reason;

BEGIN
   if I_type in ('SKU', 'SUP', 'C', 'ITEM') then
   -- Cost Change
      SQL_LIB.SET_MARK('OPEN', 
                       'C_COST_CHG_REASON', 
                       'COST_CHG_REASON', 
                       NULL);
      open C_COST_CHG_REASON;
      SQL_LIB.SET_MARK('FETCH', 
                       'C_COST_CHG_REASON', 
                       'COST_CHG_REASON', 
                       NULL);
      fetch C_COST_CHG_REASON into O_description;
      if C_COST_CHG_REASON%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_REASON_EXIST', 
                                               NULL, 
                                               NULL, 
                                               NULL);
         SQL_LIB.SET_MARK('CLOSE', 
                          'C_COST_CHG_REASON', 
                          'COST_CHG_REASON', 
                          NULL);
         close C_COST_CHG_REASON;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 
                       'C_COST_CHG_REASON', 
                       'COST_CHG_REASON', 
                       NULL);
      close C_COST_CHG_REASON;
   else
   -- Invalid Type
      O_error_message := SQL_LIB.CREATE_MSG('INV_COST_CHG_TYPE', 
                                            NULL, 
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_REASON_DESC;
---------------------------------------------------------------------------------------------
   FUNCTION GET_COST_CHG_ORG(I_cost_change  IN  NUMBER,
                             O_cost_chg_org  IN OUT  VARCHAR2,
                             O_error_message  IN OUT  VARCHAR2)
   RETURN BOOLEAN is
      cursor C_COST_CHG_ORG is
         select cost_change_origin
           from cost_susp_sup_head
          where cost_change = I_cost_change;
   BEGIN
      open C_COST_CHG_ORG;
      fetch C_COST_CHG_ORG into O_cost_chg_org;
      if C_COST_CHG_ORG%NOTFOUND then
         close C_COST_CHG_ORG;
         O_error_message := sql_lib.create_msg('INV_COST_CHG',
                                               To_Char(I_cost_change),
                                               NULL,NULL);
         RETURN FALSE;
      else
         close C_COST_CHG_ORG;
      end if;
      RETURN TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                               SQLERRM,
                                               'PC_ATTRIB_SQL.GET_COST_CHG_ORG',
                                               To_Char(SQLCODE));
      RETURN FALSE;
   END GET_COST_CHG_ORG;
------------------------------------------------------------------------------------------------- 
FUNCTION MERGE_COST_CHG_REASON_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_reason          IN       COST_CHG_REASON_TL.REASON%TYPE,
                                  I_reason_desc     IN       COST_CHG_REASON_TL.REASON_DESC%TYPE,
                                  I_lang            IN       COST_CHG_REASON_TL.LANG%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(61) := 'PC_ATTRIB_SQL.MERGE_COST_CHG_REASON_TL';
   L_table         VARCHAR2(30) := 'COST_CHG_REASON_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   L_trans_exists    VARCHAR2(1)  := NULL;
   L_orig_lang_ind   VARCHAR2(1)  := 'N';
   L_reviewed_ind    VARCHAR2(1)  := 'N';

   cursor C_REASON_TRANS_EXIST is
      select 'x'
        from cost_chg_reason_tl
       where reason = I_reason
         and rownum = 1;

   cursor C_LOCK_COST_CHG_REASON_TL is
      select 'x'
        from cost_chg_reason_tl
       where reason = I_reason
         and lang   = I_lang
         for update nowait;

BEGIN

   -- Check first if the reason (regardless of language) already exists in the table.
   -- If it already exists, set orig_lang_ind = 'N'. Otherwise, set orig_lang_ind = 'Y'.
   -- Reviewed_ind is only set to 'N' for entries in the original language to indicate
   -- if the original description has changed and translation should be reviewed for accuracy.
   -- Both flags are only used for inserts, not updates.    

   open C_REASON_TRANS_EXIST;
   fetch C_REASON_TRANS_EXIST into L_trans_exists;
   close C_REASON_TRANS_EXIST;

   if L_trans_exists is NULL then
      L_orig_lang_ind := 'Y';
      L_reviewed_ind := 'N';
   else
      L_orig_lang_ind := 'N';
      L_reviewed_ind := 'Y';
   end if;

   open C_LOCK_COST_CHG_REASON_TL;
   close C_LOCK_COST_CHG_REASON_TL;

   merge into cost_chg_reason_tl ccrtl
      using (select I_reason reason,
                    I_lang lang,
                    I_reason_desc reason_desc,
                    L_orig_lang_ind orig_lang_ind, --used for inserts only
                    L_reviewed_ind reviewed_ind,   --used for inserts only
                    user create_id,
                    sysdate create_datetime,
                    user last_update_id,
                    sysdate last_update_datetime
               from dual) use_this
         on (ccrtl.reason = use_this.reason and
             ccrtl.lang   = use_this.lang)
   when matched then
      update
         set ccrtl.reason_desc = use_this.reason_desc,
             ccrtl.reviewed_ind = decode(ccrtl.orig_lang_ind, 'Y', 'N', reviewed_ind), --when description is changed for the original language, set the entry for translation review
             ccrtl.last_update_id = use_this.last_update_id,
             ccrtl.last_update_datetime = use_this.last_update_datetime
   when NOT matched then
      insert (reason,
              lang,
              reason_desc,
              orig_lang_ind,
              reviewed_ind,
              create_id,
              create_datetime,
              last_update_id,
              last_update_datetime)
      values (use_this.reason,
              use_this.lang,
              use_this.reason_desc,
              use_this.orig_lang_ind,
              use_this.reviewed_ind,
              use_this.create_id,
              use_this.create_datetime,
              use_this.last_update_id,
              use_this.last_update_datetime);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            to_char(SQLCODE));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_COST_CHG_REASON_TL;
-------------------------------------------------------------------------------------------------
FUNCTION DEL_COST_CHG_REASON_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_reason          IN       COST_CHG_REASON_TL.REASON%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(61) := 'PC_ATTRIB_SQL.DEL_COST_CHG_REASON_TL';
   L_table         VARCHAR2(30) := 'COST_CHG_REASON_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_COST_CHG_REASON_TL is
      select 'x'
        from cost_chg_reason_tl
       where reason = I_reason
         for update nowait;

BEGIN

   open C_LOCK_COST_CHG_REASON_TL;
   close C_LOCK_COST_CHG_REASON_TL;

   delete from cost_chg_reason_tl
    where reason = I_reason;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            to_char(SQLCODE));
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEL_COST_CHG_REASON_TL;
----------------------------------------------------------------------------------------------
END PC_ATTRIB_SQL;

/


