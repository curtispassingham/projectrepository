
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE LC_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------
-- Function Name: VALID_LC_GET_INFO
-- Purpose:       Validates the LC Ref ID or Bank LC ID is a valid value on the 
--                lc_head table.  If LC Ref ID is valid, then retrieves corresponding
--                details.
--------------------------------------------------------------------------------------
FUNCTION VALID_LC_GET_INFO(O_error_message  IN OUT VARCHAR2,
                           O_exist          IN OUT BOOLEAN,
                           O_status         IN OUT LC_HEAD.STATUS%TYPE,
                           O_lc_type        IN OUT LC_HEAD.LC_TYPE%TYPE,
                           O_form_type      IN OUT LC_HEAD.FORM_TYPE%TYPE,
                           O_country        IN OUT LC_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                           O_currency       IN OUT LC_HEAD.CURRENCY_CODE%TYPE,
                           O_applicant      IN OUT LC_HEAD.APPLICANT%TYPE,
                           O_beneficiary    IN OUT LC_HEAD.BENEFICIARY%TYPE,
                           O_issue_bank     IN OUT LC_HEAD.ISSUING_BANK%TYPE,
                           O_advising_bank  IN OUT LC_HEAD.ADVISING_BANK%TYPE,
                           IO_bank_lc_id    IN OUT LC_HEAD.BANK_LC_ID%TYPE,
                           I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: DETAILS_EXIST
-- Purpose:       Checks the lc_detail, lc_amendments, lc_activity, or req_doc table
--                for records that exist with the given LC Ref ID.
--                If the record type = 'D' (LC detail table), function will return
--                number of records that exist on the table for the given LC Ref ID.
--------------------------------------------------------------------------------------
FUNCTION DETAILS_EXIST(O_error_message  IN OUT VARCHAR2,
                       O_exist          IN OUT BOOLEAN,
                       O_num_recs       IN OUT NUMBER,
                       I_detail_type    IN     VARCHAR2,
                       I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: NEXT_LC_REF_ID
-- Purpose:       Generates an unique lc ref id number. 
--------------------------------------------------------------------------------------
FUNCTION NEXT_LC_REF_ID(O_error_message  IN OUT VARCHAR2,
                        O_lc_ref_id      IN OUT LC_HEAD.LC_REF_ID%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: LOCK_LC_DETAILS
-- Purpose:       Locks the records on the LC_detail, LC_ amendments, LC_activity,
--                and Req_Docs tables prior to a deletion
--------------------------------------------------------------------------------------
FUNCTION LOCK_LC_DETAILS(O_error_message  IN OUT VARCHAR2,
                         I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: DELETE_LC_DETAILS
-- Purpose:       Deletes records on the LC_detail, LC_amendments, LC_activity,
--                and Req_Docs tables after the locking logic is called.
--------------------------------------------------------------------------------------
FUNCTION DELETE_LC_DETAILS(O_error_message  IN OUT VARCHAR2,
                           I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: LOCK_LCHEAD
-- Purpose:       Locks the record on the LC head table that corresponds to the
--                passed in LC Ref ID.
--------------------------------------------------------------------------------------
FUNCTION LOCK_LCHEAD(O_error_message  IN OUT VARCHAR2,
                     I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: UPDATE_LCHEAD
-- Purpose:       Updates the LC_head table for the fields that may change when
--                orders are removed from the LC_detail table.
--------------------------------------------------------------------------------------
FUNCTION UPDATE_LCHEAD(O_error_message  IN OUT VARCHAR2,
                       I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: GET_LC_HEAD_INFO
-- Purpose:       Retrieves information corresponding to the passed in
--                letter of credit. 
--------------------------------------------------------------------------------------
FUNCTION GET_LC_HEAD_INFO(O_error_message      IN OUT VARCHAR2,
                          O_exist              IN OUT BOOLEAN,
                          O_amount             IN OUT LC_HEAD.AMOUNT%TYPE,
                          O_earliest_ship_date IN OUT LC_HEAD.EARLIEST_SHIP_DATE%TYPE,
                          O_latest_ship_date   IN OUT LC_HEAD.LATEST_SHIP_DATE%TYPE,
                          O_expiration_date    IN OUT LC_HEAD.EXPIRATION_DATE%TYPE,
                          O_transshipment_ind  IN OUT LC_HEAD.TRANSSHIPMENT_IND%TYPE,
                          O_part_shipment_ind  IN OUT LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE,
                          O_origin_country_id  IN OUT LC_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                          I_lc_ref_id          IN     LC_HEAD.LC_REF_ID%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: LOCK_PO_DETAILS
-- Purpose:       This function will lock the records on the LC detail and Required 
--                Documents tables for LC Ref ID and order no. prior to a deletion.
--------------------------------------------------------------------------------------
FUNCTION LOCK_PO_DETAILS(O_error_message  IN OUT VARCHAR2,
                         I_lc_ref_id      IN     LC_DETAIL.LC_REF_ID%TYPE,
                         I_order_no       IN     LC_DETAIL.ORDER_NO%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: DELETE_PO_DETAILS
-- Purpose:       This function will delete the records on the LC detail and Required
--                Documents tables for a given LC Ref ID and order no. after the
--                locking logic has been called.
--------------------------------------------------------------------------------------
FUNCTION DELETE_PO_DETAILS(O_error_message  IN OUT VARCHAR2,
                           I_lc_ref_id      IN     LC_DETAIL.LC_REF_ID%TYPE,
                           I_order          IN     LC_DETAIL.ORDER_NO%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: RECALC_COST
-- Purpose:       Updates the display item amount on the LC_detail header after
--                details have been added to or deleted from the LC.
--------------------------------------------------------------------------------------
FUNCTION RECALC_COST(O_error_message  IN OUT VARCHAR2,
                     O_new_amount     IN OUT LC_DETAIL.COST%TYPE,
                     I_lc_ref_id      IN     LC_DETAIL.LC_REF_ID%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function : CONVERT_AMOUNT
-- Purpose:   Converts a numeric amount into text.
--------------------------------------------------------------------------------------
FUNCTION CONVERT_AMOUNT(O_error_message     IN OUT  VARCHAR2,
                        O_amount_text       IN OUT  VARCHAR2,
                        I_capitalization    IN      VARCHAR2,
                        I_amount            IN      LC_HEAD.AMOUNT%TYPE,
                        I_currency_code     IN      LC_HEAD.CURRENCY_CODE%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function : GET_BANK_LC_ID
-- Purpose  : Retrieves the Bank LC ID for the letter of credit to which a PO is 
--            attached.
--------------------------------------------------------------------------------------
FUNCTION GET_BANK_LC_ID(O_error_message     IN OUT  VARCHAR2,
                        O_bank_lc_id        IN OUT  LC_HEAD.BANK_LC_ID%TYPE,
                        I_order_no          IN      ORDHEAD.ORDER_NO%TYPE )
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function : VALID_LC_ORDER
-- Purpose  : Validates that the passed in Letter of Credit is valid to be 
--            attached to a Purchase Order.  
--------------------------------------------------------------------------------------
FUNCTION VALID_LC_ORDER(O_error_message        IN OUT  VARCHAR2,
                        O_valid                IN OUT  BOOLEAN,
                        I_applicant            IN      LC_HEAD.APPLICANT%TYPE,
                        I_beneficiary          IN      LC_HEAD.BENEFICIARY%TYPE,
                        I_purchase_type        IN      LC_HEAD.PURCHASE_TYPE%TYPE,
                        I_fob_title_pass       IN      LC_HEAD.FOB_TITLE_PASS%TYPE,
                        I_fob_title_pass_desc  IN      LC_HEAD.FOB_TITLE_PASS_DESC%TYPE,
                        I_lc_ref_id            IN      LC_DETAIL.LC_REF_ID%TYPE,
                        I_order_no             IN      ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function : ATTACHED_LC
-- Purpose  : Determines whether a letter of credit has been entered for a 
--            Purchase Order.
--------------------------------------------------------------------------------------
FUNCTION ATTACHED_LC(O_error_message    IN OUT  VARCHAR2,
                     O_attached         IN OUT  BOOLEAN,
                     I_order_no         IN      ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function : ADD_PO
-- Purpose:   Attachs purchase orders to a letter of credit.  If the letter of credit
--            is in 'W'orksheet status, a record will be written to the lc_detail 
--            table.  If the letter of credit is not in 'W'orksheet or C'l'osed 
--            status, a record will be written to the lc_amendments table.  No 
--            purchase orders can be attached to a closed letter of credit.
--------------------------------------------------------------------------------------
FUNCTION ADD_PO(O_error_message     IN OUT  VARCHAR2,
                I_lc_ref_id         IN      LC_HEAD.LC_REF_ID%TYPE,
                I_order_no          IN      ORDHEAD.ORDER_NO%TYPE )
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
FUNCTION ADD_PO(O_error_message     IN OUT  VARCHAR2,
                I_lc_ref_id         IN      LC_HEAD.LC_REF_ID%TYPE,
                I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                I_key_value_2       IN      REQ_DOC.KEY_VALUE_2%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: ORDER_UPDATE_LC
-- Purpose:       Updates the letter of credit attached to the passed in order if the 
--                order has been updated itself.
--------------------------------------------------------------------------------------
FUNCTION ORDER_UPDATE_LC(O_error_message  IN OUT VARCHAR2,
                         I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: CHECK_AMEND_EXISTS
-- Purpose:       Checks if an amendment already exists for Removing an Order ('RO')
--                for a passed in letter of credit / order record
--------------------------------------------------------------------------------------
FUNCTION CHECK_AMEND_EXISTS(O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE,
                            I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Function Name: WRITE_LCORDAPP
-- Purpose:       Writes a record to the lc_ordapply table. 
--------------------------------------------------------------------------------------
FUNCTION WRITE_LCORDAPP(O_error_message  IN OUT VARCHAR2,
                        I_update_ordlc   IN     BOOLEAN,
                        I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
         RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: DELETE_LCORDAPP
-- Purpose:       Deletes a record from the lc_ordapply table.       
--------------------------------------------------------------------------------------
FUNCTION DELETE_LCORDAPP(O_error_message  IN OUT VARCHAR2,
                         I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
         RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: LC_DOWNLOAD
-- Purpose:       This function accepts a where clause from, and built by, the LC Find 
--                form which will be used to designate LCs to be downloaded by writing 
--                them to the LC Download staging table.
--------------------------------------------------------------------------------------
FUNCTION LC_DOWNLOAD(O_error_message  IN OUT VARCHAR2,
                     I_lc_type        IN     VARCHAR2,
                     I_where_clause   IN     VARCHAR2)
         RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: DETAILS_EXIST
-- Purpose:       Checks the lc_detail with an order number, lc_amendments, lc_activity, or req_doc table
--                for records that exist with the given LC Ref ID.
--                If the record type = 'D' (LC detail table), function will return
--                number of records that exist on the table for the given LC Ref ID and order_no.
--------------------------------------------------------------------------------------
FUNCTION DETAILS_EXIST(O_error_message  IN OUT VARCHAR2,
                       O_exist          IN OUT BOOLEAN,
                       O_num_recs       IN OUT NUMBER,
                       I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                       I_detail_type    IN     VARCHAR2,
                       I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE)
         RETURN BOOLEAN;

--------------------------------------------------------------------------------------
END LC_SQL;
/
