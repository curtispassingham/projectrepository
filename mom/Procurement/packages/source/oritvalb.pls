CREATE OR REPLACE PACKAGE BODY ORDER_ITEM_VALIDATE_SQL AS
---------------------------------------------------------------------------------------
FUNCTION EXIST_ORDLOC_WKSHT(O_error_message IN OUT  VARCHAR2,
                            O_exists        IN OUT  BOOLEAN,
                            I_order_no      IN      ORDLOC.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(64) := 'ORDER_ITEM_VALIDATE_SQL.EXIST_ORDLOC_WKSHT';
   L_exists         VARCHAR2(1);

   cursor C_ORDLOC_WKSHT is
      select 'Y'
        from ordloc_wksht
       where order_no = I_order_no;
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK ('OPEN', 'C_ORDLOC_WKSHT', 'ordloc_wksht','Order No: '||to_char(I_order_no));
   open C_ORDLOC_WKSHT;
   ---
   SQL_LIB.SET_MARK ('FETCH', 'C_ORDLOC_WKSHT', 'ordloc_wksht','Order No: '||to_char(I_order_no));
   fetch C_ORDLOC_WKSHT into L_exists;
   ---
   if C_ORDLOC_WKSHT%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK ('CLOSE', 'C_ORDLOC_WKSHT', 'ordloc_wksht', 'Order No: '||to_char(I_order_no));
   close C_ORDLOC_WKSHT;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END EXIST_ORDLOC_WKSHT;
-----------------------------------------------------------------------------
FUNCTION EXIST_ORDLOC(O_error_message  IN OUT  VARCHAR2,
                      O_exists         IN OUT  BOOLEAN,
                      I_order_no       IN      ORDLOC.ORDER_NO%TYPE,
                      I_item           IN      ITEM_MASTER.ITEM%TYPE,
                      I_location       IN      ORDLOC.LOCATION%TYPE,
                      I_loc_type       IN      ORDLOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'ORDER_ITEM_VALIDATE_SQL.EXIST_ORDLOC';
   L_exists       VARCHAR2(1);

   cursor C_ORDLOC_ITEM_LOC_TYPE is
      select 'Y' 
        from ordloc
       where order_no = I_order_no
         and loc_type = I_loc_type
         and item     = I_item
      union all
      select 'Y'
        from ordloc ol,
             item_master i
       where ol.order_no            = I_order_no
         and ol.loc_type            = I_loc_type
         and ol.item                = i.item
         and i.item_level           = i.tran_level
         and (i.item_parent         = I_item
              or i.item_grandparent = I_item);

   cursor C_ORDLOC_ITEM is
      select 'Y' 
        from ordloc
       where order_no = I_order_no
         and item     = I_item 
      union all
      select 'Y'
        from ordloc ol,
             item_master i
       where ol.order_no            = I_order_no
         and ol.item                = i.item
         and i.item_level           = i.tran_level
         and (i.item_parent         = I_item
              or i.item_grandparent = I_item);

   cursor C_ORDLOC_LOC_TYPE is
      select 'Y' 
        from ordloc
       where order_no = I_order_no
         and loc_type = I_loc_type;

   cursor C_ORDLOC_LOC_LOC_TYPE is
      select 'Y' 
        from ordloc
       where order_no = I_order_no
         and loc_type = I_loc_type
         and location = I_location;

   cursor C_ORDLOC_ALL is
      select 'Y'
        from ordloc
       where order_no = I_order_no
         and item     = I_item
         and loc_type = I_loc_type
         and location = I_location
       union all
      select 'Y'
        from ordloc ol,
             item_master i
       where ol.order_no            = I_order_no
         and ol.item                = i.item
         and i.item_level           = i.tran_level
         and (i.item_parent         = I_item
              or i.item_grandparent = I_item)
         and ol.loc_type            = I_loc_type
         and ol.location            = I_location;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   if I_item is NULL and I_location is NULL and I_loc_type is NOT NULL then
      SQL_LIB.SET_MARK ('OPEN', 'C_ORDLOC_LOC_TYPE', 'ordloc', 
                        'Order No: ' || to_char(I_order_no) ||
                        ', Loc Type: ' || I_loc_type);
      open C_ORDLOC_LOC_TYPE;
      SQL_LIB.SET_MARK ('FETCH', 'C_ORDLOC_LOC_TYPE', 'ordloc', 
                        'Order No: ' || to_char(I_order_no) ||
                        ', Loc Type: ' || I_loc_type);
      fetch C_ORDLOC_LOC_TYPE into L_exists;
      if C_ORDLOC_LOC_TYPE%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      SQL_LIB.SET_MARK ('CLOSE', 'C_ORDLOC_LOC_TYPE', 'ordloc', 
                        'Order No: ' || to_char(I_order_no) ||
                        ', Loc Type: ' || I_loc_type);
      close C_ORDLOC_LOC_TYPE;
      ---
   elsif I_item is NOT NULL and I_location is NULL and I_loc_type is NOT NULL then
      SQL_LIB.SET_MARK ('OPEN', 'C_ORDLOC_ITEM_LOC_TYPE', 'ordloc',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' || I_item ||
                        ', Loc Type: ' || I_loc_type);
      open C_ORDLOC_ITEM_LOC_TYPE;
      SQL_LIB.SET_MARK ('FETCH', 'C_ORDLOC_ITEM_LOC_TYPE', 'ordloc',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' || I_item ||
                        ', Loc Type: ' || I_loc_type);
      fetch C_ORDLOC_ITEM_LOC_TYPE into L_exists;
      if C_ORDLOC_ITEM_LOC_TYPE%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      SQL_LIB.SET_MARK ('CLOSE', 'C_ORDLOC_ITEM_LOC_TYPE', 'ordloc',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' || I_item ||
                        ', Loc Type: ' || I_loc_type);
      close C_ORDLOC_ITEM_LOC_TYPE;
      ---
   elsif I_item is NOT NULL and I_location is NULL and I_loc_type is NULL then
      SQL_LIB.SET_MARK ('OPEN', 'C_ORDLOC_ITEM', 'ordloc',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' || I_item);
      open C_ORDLOC_ITEM;
      SQL_LIB.SET_MARK ('FETCH', 'C_ORDLOC_ITEM', 'ordloc',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' || I_item);
      fetch C_ORDLOC_ITEM into L_exists;
      if C_ORDLOC_ITEM%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      SQL_LIB.SET_MARK ('CLOSE', 'C_ORDLOC_ITEM', 'ordloc',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' || I_item);
      close C_ORDLOC_ITEM;
   elsif I_item is NOT NULL and I_location is NOT NULL and
         I_loc_type is NOT NULL then
      SQL_LIB.SET_MARK ('OPEN','C_ORDLOC_ALL','ordloc',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' || I_item ||
                        ', Loc Type: ' || I_loc_type ||
                        ', Location: ' || to_char(I_location));
      open C_ORDLOC_ALL;
      SQL_LIB.SET_MARK ('FETCH','C_ORDLOC_ALL','ordloc',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' || I_item ||
                        ', Loc Type: ' || I_loc_type ||
                        ', Location: ' || to_char(I_location));
      fetch C_ORDLOC_ALL into L_exists;
      if C_ORDLOC_ALL%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      SQL_LIB.SET_MARK ('CLOSE','C_ORDLOC_ALL','ordloc',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' || I_item ||
                        ', Loc Type: ' || I_loc_type ||
                        ', Location: ' || to_char(I_location));
      close C_ORDLOC_ALL;
   elsif I_item is NULL and I_location is NOT NULL and 
         I_loc_type is NOT NULL then
      SQL_LIB.SET_MARK('OPEN','C_ORDLOC_LOC_LOC_TYPE', 'ordloc',
                       'Order No: ' || to_char(I_order_no) ||
                       ', Loc Type: ' || I_loc_type ||
                       ', Location: ' || to_char(I_location));
      open C_ORDLOC_LOC_LOC_TYPE;
      SQL_LIB.SET_MARK('FETCH','C_ORDLOC_LOC_LOC_TYPE', 'ordloc',
                       'Order No: ' || to_char(I_order_no) ||
                       ', Loc Type: ' || I_loc_type ||
                       ', Location: ' || to_char(I_location));
      fetch C_ORDLOC_LOC_LOC_TYPE into L_exists;
      if C_ORDLOC_LOC_LOC_TYPE%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_ORDLOC_LOC_LOC_TYPE', 'ordloc',
                       'Order No: ' || to_char(I_order_no) ||
                       ', Loc Type: ' || I_loc_type ||
                       ', Location: ' || to_char(I_location));
      close C_ORDLOC_LOC_LOC_TYPE;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END EXIST_ORDLOC;
-----------------------------------------------------------------------------
FUNCTION EXIST_ORDSKU(O_error_message   IN OUT  VARCHAR2,
                      O_exists          IN OUT  BOOLEAN,
                      I_order_no        IN      ORDLOC.ORDER_NO%TYPE,
                      I_item            IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(64) := 'ORDER_ITEM_VALIDATE_SQL.EXIST_ORDSKU';
   L_exists         VARCHAR2(1); 

   cursor C_ORDSKU is
      select 'Y'
        from ordsku
       where order_no = I_order_no;

   cursor C_ORDSKU_ITEM is 
      select 'Y'
        from ordsku
       where order_no = I_order_no
         and item     = I_item
      UNION ALL
      select 'Y'
        from ordsku os,
             item_master i
       where os.order_no            = I_order_no
         and os.item                = i.item
         and i.item_level           = i.tran_level
         and (i.item_parent         = I_item
              or i.item_grandparent = I_item);

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   O_exists := FALSE;
   ---
   if I_item is NULL then
      SQL_LIB.SET_MARK ('OPEN', 'C_ORDSKU', 'ordsku','Order No: ' || to_char(I_order_no));
      open C_ORDSKU;
      SQL_LIB.SET_MARK ('FETCH', 'C_ORDSKU', 'ordsku','Order No: ' || to_char(I_order_no));
      fetch C_ORDSKU into L_exists;
      ---
      if C_ORDSKU%FOUND then
         O_exists := TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK ('CLOSE', 'C_ORDSKU', 'ordsku','Order No: ' || to_char(I_order_no));
      close C_ORDSKU;
   else
      SQL_LIB.SET_MARK ('OPEN','C_ORDSKU_ITEM','ordsku',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' ||I_item);
      open C_ORDSKU_ITEM;
      SQL_LIB.SET_MARK ('FETCH','C_ORDSKU_ITEM','ordsku',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' ||I_item);
      fetch C_ORDSKU_ITEM into L_exists;
      if C_ORDSKU_ITEM%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      SQL_LIB.SET_MARK ('CLOSE','C_ORDSKU_ITEM','ordsku',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' ||I_item);
      close C_ORDSKU_ITEM;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END EXIST_ORDSKU;
----------------------------------------------------------------------------------
FUNCTION CHECK_SHIPPED  (O_error_message  IN OUT  VARCHAR2,
                         O_shipped        IN OUT  BOOLEAN,
                         I_order_no       IN      ORDLOC.ORDER_NO%TYPE,
                         I_item           IN      ITEM_MASTER.ITEM%TYPE,
                         I_location       IN      ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN is

   L_program       VARCHAR2(64) := 'ORDER_ITEM_VALIDATE_SQL.CHECK_SHIPPED';
   L_exists        VARCHAR2(1);

   cursor C_RECD_QTY is
      select 'Y'
        from shipment sm,
             shipsku  ss
       where sm.shipment = ss.shipment
         and sm.order_no = I_order_no
         and (ss.item = NVL(I_item, ss.item) 
              or ss.item in (select i.item
                               from item_master i
                              where item_level           = tran_level
                                and (item_parent         = I_item
                                     or item_grandparent = I_item)))
         and sm.to_loc = NVL(I_location, sm.to_loc)
         and ss.qty_received > 0;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
   else
      SQL_LIB.SET_MARK ('OPEN', 'C_RECD_QTY', 'shipment, shipsku', 
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' ||I_item||
                        ', Location: ' || to_char(I_location));
      open C_RECD_QTY;
      ---
      SQL_LIB.SET_MARK ('FETCH', 'C_RECD_QTY', 'shipment, shipsku',
                        'Order No: ' || to_char(I_order_no) || 
                        ', Item: ' ||I_item||
                        ', Location: ' || to_char(I_location));
      fetch C_RECD_QTY into L_exists;
      ---
      if C_RECD_QTY%FOUND then
         O_shipped := TRUE;
      else
         O_shipped := FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK ('CLOSE', 'C_RECD_QTY', 'shipment, shipsku',
                        'Order No: ' || to_char(I_order_no) ||
                        ', Item: ' ||I_item||
                        ', Location: ' || to_char(I_location));
      close C_RECD_QTY;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END CHECK_SHIPPED;
----------------------------------------------------------------------------------
FUNCTION TSF_PO_LINK_EXISTS(O_error_message   IN OUT   VARCHAR,
                            O_exists          IN OUT   BOOLEAN,
                            I_order_no        IN       ORDLOC.ORDER_NO%TYPE,
                            I_item            IN       ORDLOC.ITEM%TYPE,
                            I_location        IN       ORDLOC.LOCATION%TYPE,
                            I_tsf_po_link_no  IN       ORDLOC.TSF_PO_LINK_NO%TYPE)
   return BOOLEAN is

   L_dummy   VARCHAR2(1);

   cursor C_LINK_EXISTS is
      select 'x'
        from ordloc
       where order_no = NVL(I_order_no, order_no)
         and item = NVL(I_item, item)
         and location = NVL(I_location, location)
         and tsf_po_link_no = NVL(I_tsf_po_link_no, tsf_po_link_no)
         and tsf_po_link_no is NOT NULL
         and rownum = 1;

BEGIN
   SQL_LIB.SET_MARK ('OPEN', 'C_LINK_EXISTS', 'ORDLOC',
                     'Order No: '||to_char(I_order_no)||', Item: '||I_item||', Location: '||to_char(I_location));
   open C_LINK_EXISTS;
   SQL_LIB.SET_MARK ('FETCH', 'C_LINK_EXISTS', 'ORDLOC',
                     'Order No: '||to_char(I_order_no)||', Item: '||I_item||', Location: '||to_char(I_location));
   fetch C_LINK_EXISTS into L_dummy;
   ---
   O_exists := C_LINK_EXISTS%FOUND;
   ---
   SQL_LIB.SET_MARK ('CLOSE', 'C_LINK_EXISTS', 'ORDLOC',
                     'Order No: '||to_char(I_order_no)||', Item: '||I_item||', Location: '||to_char(I_location));
   close C_LINK_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              'ORDER_ITEM_VALIDATE_SQL.TSF_PO_LINK_EXISTS',
                                              to_char(SQLCODE));
      return FALSE;
END TSF_PO_LINK_EXISTS;
------------------------------------------------------------------------
FUNCTION CHECK_QTY_CHANGE(O_error_message     IN OUT VARCHAR2,
                          O_valid_qty_change  IN OUT BOOLEAN,
                          I_order_no          IN     ORDLOC.ORDER_NO%TYPE,
                          I_item              IN     ORDLOC.ITEM%TYPE,
                          I_location          IN     ORDLOC.LOCATION%TYPE,
                          I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                          I_new_qty_ordered   IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_qty_ordered       IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_qty_allocated     IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_qty_received      IN     ORDLOC.QTY_RECEIVED%TYPE,
                          I_qty_cancelled     IN     ORDLOC.QTY_CANCELLED%TYPE,
                          I_orig_repl_qty     IN     ORDLOC.ORIGINAL_REPL_QTY%TYPE,
                          I_min_repl_qty      IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_max_repl_qty      IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_supp_pack_size    IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                          I_ord_or_cancel_chg IN     VARCHAR2)
RETURN BOOLEAN IS

   L_function          VARCHAR2(50)                          := 'ORDER_ITEM_ATTRIB_SQL.CHECK_QTY_CHANGE';
   L_new_qty_ordered   ORDLOC.QTY_ORDERED%TYPE               := I_new_qty_ordered;
   L_qty_ordered       ORDLOC.QTY_ORDERED%TYPE               := 0;
   L_qty_received      ORDLOC.QTY_RECEIVED%TYPE              := 0;
   L_qty_shipped       SHIPSKU.QTY_EXPECTED%TYPE             := 0;
   L_qty_appointed     APPT_DETAIL.QTY_APPOINTED%TYPE        := 0;
   L_qty_ship_appt     APPT_DETAIL.QTY_APPOINTED%TYPE        := 0;
   L_location          ORDLOC.LOCATION%TYPE                  := I_location;
   L_supp_pack_size    ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE := NVL(I_supp_pack_size, 1);

   cursor C_GET_QTY_SHIPPED is
      select nvl(sum(nvl(s.qty_expected,0) - nvl(s.qty_received,0)), 0)
        from shipsku s,
             shipment h
       where s.shipment     = h.shipment
         and h.order_no     = I_order_no
         and h.to_loc       = L_location
         and s.item         = I_item
         and h.ship_origin  = 4
         and h.status_code <> 'C';

   cursor C_GET_QTY_APPOINTED is
      select nvl(sum(nvl(qty_appointed,0) - nvl(qty_received,0)), 0)
        from appt_head aph,
             appt_detail apd
       where aph.appt     = apd.appt
         and aph.status   <> 'AC'
         and apd.doc_type = 'P'
         and apd.doc      = I_order_no
         and apd.loc      = L_location
         and apd.item     = I_item;

   cursor C_GET_PHYS_QTY_ORD is
      select nvl(sum(qty_ordered), 0)
        from ordloc
       where order_no = I_order_no
         and item     = I_item
         and location in (select wh
                            from wh
                           where physical_wh      = L_location
                             and stockholding_ind = 'Y'
                             and wh              <> I_location);

   cursor C_GET_PHYS_QTY_REC is
      select sum(nvl(qty_received,0))
        from ordloc
       where order_no = I_order_no
         and item     = I_item
         and location in (select wh
                            from wh
                           where physical_wh      = L_location
                             and stockholding_ind = 'Y');

BEGIN
   ---
   O_valid_qty_change := TRUE;
   ---
   -- Evaluate the new qty ordered.
   ---
   -- The qty ordered cannot be below the qty allocated
   ---
   if I_new_qty_ordered < I_qty_allocated then
      ---
      -- If it's the qty ordered ('O') being changed, a different message will be
      -- displayed than if it's the qty ordered UOP ('OU') or the qty cancelled UOP ('CU') being changed.
      ---
      if I_ord_or_cancel_chg = 'O' then
         O_error_message := SQL_LIB.CREATE_MSG('QTY_ORD_NOT_LESS_ALLOC',
                                               to_char(I_qty_allocated),
                                               NULL,
                                               NULL);
      elsif I_ord_or_cancel_chg = 'OU' then
         O_error_message := SQL_LIB.CREATE_MSG('QTY_ORD_NOT_LESS_ALLOC',
                                               to_char(I_qty_allocated / L_supp_pack_size),
                                               NULL,
                                               NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('QTY_CANCEL_ORD_ALLOC',
                                               to_char((I_qty_ordered + I_qty_cancelled - I_qty_allocated) / L_supp_pack_size),
                                               NULL,
                                               NULL); 
      end if;
      ---
      O_valid_qty_change := FALSE;
      return TRUE;
   end if;
   ---
   -- If there is an original replenishment qty, the qty ordered cannot be changed outside of the 
   -- tolerance range.  These tolerances are defined on the Replenishment Item Location table (repl_item_loc).
   -- There are unit and percent tolerances, whichever is greater is used as the range.
   ---
   if I_loc_type = 'S' and I_orig_repl_qty is not NULL then
      if (I_new_qty_ordered < I_min_repl_qty) or (I_new_qty_ordered > I_max_repl_qty) then
         ---
         -- If it's the qty ordered ('O') being changed, a different message will be
         -- displayed than if it's the qty ordered UOP ('OU') or the qty cancelled UOP ('CU') being changed.
         ---
         if I_ord_or_cancel_chg = 'O' then         
            O_error_message := SQL_LIB.CREATE_MSG('QTY_RANGE',
                                                  to_char(I_min_repl_qty),
                                                  to_char(I_max_repl_qty),
                                                  NULL);
         elsif I_ord_or_cancel_chg = 'OU' then         
            O_error_message := SQL_LIB.CREATE_MSG('QTY_RANGE',
                                                  to_char(I_min_repl_qty / L_supp_pack_size),
                                                  to_char(I_max_repl_qty / L_supp_pack_size),
                                                  NULL);
         else
            O_error_message := SQL_LIB.CREATE_MSG('QTY_RANGE_CANCEL',
                                                  to_char(I_min_repl_qty / L_supp_pack_size),
                                                  to_char(I_max_repl_qty / L_supp_pack_size),
                                                  NULL);
         end if;
         ---
         O_valid_qty_change := FALSE;
         return TRUE;
      end if;
   end if;
   ---
   -- If the location's qty that's being changed is a store,
   -- then appointment and shipment qtys will be stored for the location on the order.  Otherwise
   -- the appointment and shipment information will be stored at the physical location level
   -- and must be retrieved for the physical loc and compared to the sum of all virtual locations
   -- within that physical.
   ---
   if I_loc_type = 'S' then
      ---
      -- The Qty Expected minus the Qty Received is retrieved
      -- because we will use the qty received from the order
      -- and do not want to double count the received qty.  Also
      -- it may be possible to 'Cancel' a shipment that has already
      -- been received against, in which case if we only looked at the 
      -- qty received on the shipment we would not count this qty because
      -- we only look at shipments that are not 'cancelled'.
      -- Therefore, to be safe we will always look only at the qty expected
      -- minus the qty received on the shipsku table.
      ---
      open C_GET_QTY_SHIPPED;
      fetch C_GET_QTY_SHIPPED into L_qty_shipped;
      close C_GET_QTY_SHIPPED;
      ---
      -- If the Qty Expected - Qty Received is less than zero, set L_qty_shipped equal to zero.
      ---
      if L_qty_shipped < 0 then
         L_qty_shipped := 0;
      end if;
      ---
      -- Since we are using the order's qty received in the calculations
      -- below we will only retrieve the qty appointed minus the qty received
      -- for the appointments.
      ---
      open C_GET_QTY_APPOINTED;
      fetch C_GET_QTY_APPOINTED into L_qty_appointed;
      close C_GET_QTY_APPOINTED;
      ---
      -- If the Qty Appointed - Qty Received is less than zero, set L_qty_appointed equal to zero.
      ---
      if L_qty_appointed < 0 then
         L_qty_appointed := 0;
      end if;
      ---
      if L_qty_shipped > L_qty_appointed then
         L_qty_ship_appt := L_qty_shipped;
      else
         L_qty_ship_appt := L_qty_appointed;
      end if;
      ---
      if I_new_qty_ordered < (L_qty_ship_appt + nvl(I_qty_received,0)) then
         ---
         -- If it's the qty ordered ('O') being changed, a different message will be
         -- displayed than if it's the qty ordered UOP ('OU') or the qty cancelled UOP ('CU') being changed.
         ---
         if I_ord_or_cancel_chg = 'O' then 
            O_error_message := SQL_LIB.CREATE_MSG('QTY_ORD_NOT_LESS_OUTSTAND',
                                                  to_char(L_qty_ship_appt + nvl(I_qty_received,0)),
                                                  NULL,
                                                  NULL);
         elsif I_ord_or_cancel_chg = 'OU' then 
            O_error_message := SQL_LIB.CREATE_MSG('QTY_ORD_NOT_LESS_OUTSTAND',
                                                  to_char((L_qty_ship_appt + nvl(I_qty_received,0)) / L_supp_pack_size),
                                                  NULL,
                                                  NULL);
         else
            O_error_message := SQL_LIB.CREATE_MSG('QTY_CANCEL_ORD_LESS_OUT',
                                                  to_char((I_qty_ordered + I_qty_cancelled 
                                                           - L_qty_ship_appt - nvl(I_qty_received,0)) / L_supp_pack_size),
                                                  NULL,
                                                  NULL);
         end if;
         ---
         O_valid_qty_change := FALSE;
         return TRUE;
      end if;
   else
      ---
      -- WH appointment and shipment information will 
      -- be stored at the physical location level and must be retrieved for the 
      -- physical loc and compared to the sum of all virtual locations
      -- within that physical.
      ---
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_location,
                                       I_location) = FALSE then
         return FALSE;
      end if;
      ---
      -- The Qty Expected minus the Qty Received is retrieved
      -- because we will use the qty received from the order
      -- and do not want to double count the received qty.  Also
      -- it may be possible to 'Cancel' a shipment that has already
      -- been received against, in which case if we only looked at the 
      -- qty received on the shipment we would not count this qty because
      -- we only look at shipments that are not 'cancelled'.
      -- Therefore, to be safe we will always look only at the qty expected
      -- minus the qty received on the shipsku table.
      ---
      open C_GET_QTY_SHIPPED;
      fetch C_GET_QTY_SHIPPED into L_qty_shipped;
      close C_GET_QTY_SHIPPED;
      ---
      -- If the Qty Expected - Qty Received is less than zero, set L_qty_shipped equal to zero.
      ---
      if L_qty_shipped < 0 then
         L_qty_shipped := 0;
      end if;
      ---
      -- Since we are using the order's qty received in the calculations
      -- below we will only retrieve the qty appointed minus the qty received
      -- for the appointments.
      ---
      open C_GET_QTY_APPOINTED;
      fetch C_GET_QTY_APPOINTED into L_qty_appointed;
      close C_GET_QTY_APPOINTED;
      ---
      -- If the Qty Appointed - Qty Received is less than zero, set L_qty_appointed equal to zero.
      ---
      if L_qty_appointed < 0 then
         L_qty_appointed := 0;
      end if;
      ---
      if L_qty_shipped > L_qty_appointed then
         L_qty_ship_appt := L_qty_shipped;
      else
         L_qty_ship_appt := L_qty_appointed;
      end if;
      ---
      -- Need to retrieve the sum of the qty ordered and received
      -- for all virtual locations within the physical.
      ---
      open C_GET_PHYS_QTY_ORD;
      fetch C_GET_PHYS_QTY_ORD into L_qty_ordered;
      close C_GET_PHYS_QTY_ORD;
      ---
      open C_GET_PHYS_QTY_REC;
      fetch C_GET_PHYS_QTY_REC into L_qty_received;
      close C_GET_PHYS_QTY_REC;
      ---
      -- This comparison is done as follows:
      -- Above we retrieved the sum of the old qty ordered for all virtuals within the physical
      -- We need to subtract the old qty ordered of the passed in virtual loc from that sum and
      -- then add the new qty ordered to the sum in order to get the total new qty ordered for all
      -- of the virtuals within the physical.
      -- This will then be compared to the sum of the qty appt or shipped plus total qty received.
      -- The new total qty ordered cannot fall below that value.
      ---
      if (I_new_qty_ordered + L_qty_ordered) < (L_qty_ship_appt + L_qty_received) then
         ---
         -- If it's the qty ordered ('O') being changed, a different message will be
         -- displayed than if it's the qty ordered UOP ('OU') or the qty cancelled UOP ('CU') being changed.
         ---
         if I_ord_or_cancel_chg = 'O' then 
            O_error_message := SQL_LIB.CREATE_MSG('QTY_ORD_LESS_PHY_OUT',
                                                  to_char(I_new_qty_ordered + L_qty_ordered),
                                                  to_char(L_qty_ship_appt + L_qty_received),
                                                  NULL);
         elsif I_ord_or_cancel_chg = 'OU' then -- Order Qty UOP was modified
            O_error_message := SQL_LIB.CREATE_MSG('QTY_ORD_LESS_PHY_OUT',
                                                  to_char((I_new_qty_ordered + L_qty_ordered) / L_supp_pack_size),
                                                  to_char((L_qty_ship_appt + L_qty_received) / L_supp_pack_size),
                                                  NULL);
         else
            O_error_message := SQL_LIB.CREATE_MSG('PHY_QTY_CANCEL_ORD_OUT',
                                                  to_char((I_new_qty_ordered + L_qty_ordered) / L_supp_pack_size),
                                                  to_char((L_qty_ship_appt + L_qty_received) / L_supp_pack_size),
                                                  NULL);
         end if;
         ---
         O_valid_qty_change := FALSE;
         return TRUE;
      end if;
      ---
      -- The new qty ordered cannot fall below the qty received.
      ---
      if I_new_qty_ordered < (nvl(I_qty_received,0)) then
         ---
         -- If it's the qty ordered being changed, a different message will be
         -- displayed than if it's the qty cancelled being changed.
         ---
         if I_ord_or_cancel_chg in ('O','OU') then 
            O_error_message := SQL_LIB.CREATE_MSG('NEW_ORD_QTY',
                                                  NULL,
                                                  NULL,
                                                  NULL);
         else
            O_error_message := SQL_LIB.CREATE_MSG('NEW_CANCEL_ORD_REC',
                                                  to_char((I_qty_ordered + I_qty_cancelled - nvl(I_qty_received,0)) / L_supp_pack_size),
                                                  NULL,
                                                  NULL);
         end if;
         ---
         O_valid_qty_change := FALSE;
         return TRUE;
      end if;
   end if;
   --- 
   return TRUE;

EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_QTY_CHANGE;
----------------------------------------------------------------------------------
FUNCTION GET_QTY_CHANGE(
    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_low_val          IN OUT ORDLOC.QTY_ORDERED%TYPE,
    O_high_val         IN OUT ORDLOC.QTY_ORDERED%TYPE,
    I_order_no          IN ORDLOC.ORDER_NO%TYPE,
    I_item              IN ORDLOC.ITEM%TYPE,
    I_location          IN ORDLOC.LOCATION%TYPE,
    I_loc_type          IN ORDLOC.LOC_TYPE%TYPE,
    I_qty_ordered       IN ORDLOC.QTY_ORDERED%TYPE,
    I_qty_allocated     IN ORDLOC.QTY_ORDERED%TYPE,
    I_qty_received      IN ORDLOC.QTY_RECEIVED%TYPE,
    I_qty_cancelled     IN ORDLOC.QTY_CANCELLED%TYPE,
    I_orig_repl_qty     IN ORDLOC.ORIGINAL_REPL_QTY%TYPE,
    I_min_repl_qty      IN ORDLOC.QTY_ORDERED%TYPE,
    I_max_repl_qty      IN ORDLOC.QTY_ORDERED%TYPE,
    I_supp_pack_size    IN ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
    I_ord_or_cancel_chg IN VARCHAR2)
  RETURN BOOLEAN
IS
  L_function VARCHAR2(50)                                := 'ORDER_ITEM_ATTRIB_SQL.GET_QTY_CHANGE';
  L_qty_ordered ORDLOC.QTY_ORDERED%TYPE                  := 0;
  L_qty_expected ORDLOC.QTY_ORDERED%TYPE                 := 0;
  L_qty_received ORDLOC.QTY_RECEIVED%TYPE                := 0;
  L_qty_shipped SHIPSKU.QTY_EXPECTED%TYPE                := 0;
  L_qty_appointed APPT_DETAIL.QTY_APPOINTED%TYPE         := 0;
  L_qty_ship_appt APPT_DETAIL.QTY_APPOINTED%TYPE         := 0;
  L_location ORDLOC.LOCATION%TYPE                        := I_location;
  L_supp_pack_size ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE := NVL(I_supp_pack_size, 1);
  --
  
  CURSOR C_GET_LOC_EXPECTED 
  IS 
  select qty_expected from
   (select NVL(SUM(NVL(shs.qty_expected, 0)), 0) qty_expected
     from shipsku shs,
      shipment shp
    where shs.shipment = shp.shipment
    and shp.order_no = I_order_no
    and ((shp.to_loc_type = 'W' and shp.to_loc = I_location)
     or (shp.to_loc_type = 'S' and shp.to_loc = I_location))
    and shp.status_code <> 'C'
    and shs.item = I_item
    union
   select NVL(SUM(NVL(shsl.qty_expected, 0)), 0) qty_expected
     from shipsku shs,
      shipment shp,
      shipsku_loc shsl
    where shs.shipment = shp.shipment
    and shs.shipment = shsl.shipment
    and shsl.seq_no = shs.seq_no
    and shsl.item = shs.item
    and shp.order_no = I_order_no
          and shsl.to_loc=I_location
    and (shp.to_loc_type = 'W' and shp.to_loc in (select physical_wh from wh where wh = I_location and physical_wh <> wh))
    and shp.status_code <> 'C'
    and shs.item = I_item)
   where qty_expected>0;
   
  CURSOR C_GET_QTY_SHIPPED
  IS
    SELECT NVL(SUM(NVL(s.qty_expected,0) - NVL(s.qty_received,0)), 0)
    FROM shipsku s,
      shipment h
    WHERE s.shipment   = h.shipment
    AND h.order_no     = I_order_no
    AND (h.to_loc_type = 'W' and h.to_loc in (select physical_wh from wh where wh = L_location)
         or (h.to_loc_type = 'S' and h.to_loc = L_location))
    AND s.item         = I_item
    AND h.status_code <> 'C';
  --
  CURSOR C_GET_QTY_APPOINTED
  IS
    SELECT NVL(SUM(NVL(qty_appointed,0) - NVL(qty_received,0)), 0)
    FROM appt_head aph,
      appt_detail apd
    WHERE aph.appt   = apd.appt
    AND aph.status  <> 'AC'
    AND apd.doc_type = 'P'
    AND apd.doc      = I_order_no
    and (apd.loc_type = 'W' and apd.loc in (select physical_wh from wh where wh = L_location)
             or (apd.loc_type = 'S' and apd.loc = L_location))
    AND apd.item     = I_item;
  --
  CURSOR C_GET_PHYS_QTY_ORD
  IS
    SELECT NVL(SUM(qty_ordered), 0)
    FROM ordloc
    WHERE order_no = I_order_no
    AND item       = I_item
    AND location  IN
      (SELECT wh
      FROM wh
      WHERE physical_wh    = L_location
      AND stockholding_ind = 'Y'
      AND wh              <> I_location
      );
  --
  CURSOR C_GET_PHYS_QTY_REC
  IS
    SELECT SUM(NVL(qty_received,0))
    FROM ordloc
    WHERE order_no = I_order_no
    AND item       = I_item
    AND location  IN
      (SELECT wh FROM wh WHERE physical_wh = L_location AND stockholding_ind = 'Y'
      );
  --
  PROCEDURE set_low_val(
      I_qty IN ORDLOC.QTY_ORDERED%TYPE)
  IS
  BEGIN
    IF I_qty     IS NOT NULL AND I_qty > NVL(O_low_val,0) THEN
      O_low_val := I_qty;
    END IF;
  END set_low_val;
--
  PROCEDURE set_high_val(
      I_qty IN ORDLOC.QTY_ORDERED%TYPE)
  IS
  BEGIN
    IF I_qty      IS NOT NULL AND I_qty > NVL(O_high_val,0) THEN
      O_high_val := I_qty;
    END IF;
  END set_high_val;
--
BEGIN
  O_low_val  := 0;
  O_high_val := NULL;
  --
  IF I_qty_allocated      IS NOT NULL THEN
    IF I_ord_or_cancel_chg = 'O' THEN
      set_low_val(I_qty_allocated);
    elsif I_ord_or_cancel_chg = 'OU' THEN
      set_low_val(I_qty_allocated / L_supp_pack_size);
    ELSE
      set_high_val((I_qty_ordered + I_qty_cancelled - I_qty_allocated) / L_supp_pack_size);
    END IF;
  END IF;
  ---
  -- If there is an original replenishment qty, the qty ordered cannot be changed outside of the
  -- tolerance range.  These tolerances are defined on the Replenishment Item Location table (repl_item_loc).
  -- There are unit and percent tolerances, whichever is greater is used as the range.
  ---
  IF I_loc_type              = 'S' AND I_orig_repl_qty IS NOT NULL THEN
    IF I_min_repl_qty       IS NOT NULL OR I_max_repl_qty IS NOT NULL THEN
      IF I_ord_or_cancel_chg = 'O' THEN
        set_low_val(I_min_repl_qty);
        set_high_val(I_max_repl_qty);
      elsif I_ord_or_cancel_chg = 'OU' THEN
        set_low_val(I_min_repl_qty  / L_supp_pack_size);
        set_high_val(I_max_repl_qty / L_supp_pack_size);
      ELSE
        set_low_val((I_qty_ordered  + I_qty_cancelled-I_max_repl_qty) / L_supp_pack_size);
        set_high_val((I_qty_ordered + I_qty_cancelled-I_min_repl_qty) / L_supp_pack_size);
      END IF;
    END IF;
  END IF;
  ---
  -- If the location's qty that's being changed is a store,
  -- then appointment and shipment qtys will be stored for the location on the order.  Otherwise
  -- the appointment and shipment information will be stored at the physical location level
  -- and must be retrieved for the physical loc and compared to the sum of all virtual locations
  -- within that physical.
  ---
  IF I_loc_type = 'S' THEN
    ---
    -- The Qty Expected minus the Qty Received is retrieved
    -- because we will use the qty received from the order
    -- and do not want to double count the received qty.  Also
    -- it may be possible to 'Cancel' a shipment that has already
    -- been received against, in which case if we only looked at the
    -- qty received on the shipment we would not count this qty because
    -- we only look at shipments that are not 'cancelled'.
    -- Therefore, to be safe we will always look only at the qty expected
    -- minus the qty received on the shipsku table.
    ---
    OPEN C_GET_QTY_SHIPPED;
    FETCH C_GET_QTY_SHIPPED INTO L_qty_shipped;
    CLOSE C_GET_QTY_SHIPPED;
    ---
    -- If the Qty Expected - Qty Received is less than zero, set L_qty_shipped equal to zero.
    ---
    IF L_qty_shipped < 0 THEN
      L_qty_shipped := 0;
    END IF;
    ---
    -- Since we are using the order's qty received in the calculations
    -- below we will only retrieve the qty appointed minus the qty received
    -- for the appointments.
    ---
    OPEN C_GET_QTY_APPOINTED;
    FETCH C_GET_QTY_APPOINTED INTO L_qty_appointed;
    CLOSE C_GET_QTY_APPOINTED;
    ---
    -- If the Qty Appointed - Qty Received is less than zero, set L_qty_appointed equal to zero.
    ---
    IF L_qty_appointed < 0 THEN
      L_qty_appointed := 0;
    END IF;
    ---
    IF L_qty_shipped   > L_qty_appointed THEN
      L_qty_ship_appt := L_qty_shipped;
    ELSE
      L_qty_ship_appt := L_qty_appointed;
    END IF;
    ---
    IF I_ord_or_cancel_chg = 'O' THEN
      set_low_val(L_qty_ship_appt + NVL(I_qty_received,0));
    elsif I_ord_or_cancel_chg = 'OU' THEN
      set_low_val((L_qty_ship_appt + NVL(I_qty_received,0)) / L_supp_pack_size);
    ELSE
      set_high_val((I_qty_ordered + I_qty_cancelled - L_qty_ship_appt - NVL(I_qty_received,0)) / L_supp_pack_size);
    END IF;
  ELSE
    ---
    -- WH appointment and shipment information will
    -- be stored at the physical location level and must be retrieved for the
    -- physical loc and compared to the sum of all virtual locations
    -- within that physical.
    ---
    IF WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message, L_location, I_location) = FALSE THEN
      RETURN FALSE;
    END IF;
    ---
    -- The Qty Expected minus the Qty Received is retrieved
    -- because we will use the qty received from the order
    -- and do not want to double count the received qty.  Also
    -- it may be possible to 'Cancel' a shipment that has already
    -- been received against, in which case if we only looked at the
    -- qty received on the shipment we would not count this qty because
    -- we only look at shipments that are not 'cancelled'.
    -- Therefore, to be safe we will always look only at the qty expected
    -- minus the qty received on the shipsku table.
    ---
    OPEN C_GET_QTY_SHIPPED;
    FETCH C_GET_QTY_SHIPPED INTO L_qty_shipped;
    CLOSE C_GET_QTY_SHIPPED;
    ---
    -- If the Qty Expected - Qty Received is less than zero, set L_qty_shipped equal to zero.
    ---
    IF L_qty_shipped < 0 THEN
      L_qty_shipped := 0;
    END IF;
    ---
    -- Since we are using the order's qty received in the calculations
    -- below we will only retrieve the qty appointed minus the qty received
    -- for the appointments.
    ---
    OPEN C_GET_QTY_APPOINTED;
    FETCH C_GET_QTY_APPOINTED INTO L_qty_appointed;
    CLOSE C_GET_QTY_APPOINTED;
    ---
    -- If the Qty Appointed - Qty Received is less than zero, set L_qty_appointed equal to zero.
    ---
    IF L_qty_appointed < 0 THEN
      L_qty_appointed := 0;
    END IF;
    ---
    IF L_qty_shipped   > L_qty_appointed THEN
      L_qty_ship_appt := L_qty_shipped;
    ELSE
      L_qty_ship_appt := L_qty_appointed;
    END IF;
    ---
    -- Need to retrieve the sum of the qty ordered and received
    -- for all virtual locations within the physical.
    ---
    OPEN C_GET_PHYS_QTY_ORD;
    FETCH C_GET_PHYS_QTY_ORD INTO L_qty_ordered;
    CLOSE C_GET_PHYS_QTY_ORD;
    ---
    OPEN C_GET_PHYS_QTY_REC;
    FETCH C_GET_PHYS_QTY_REC INTO L_qty_received;
    CLOSE C_GET_PHYS_QTY_REC;
    ---
    OPEN C_GET_LOC_EXPECTED;
    FETCH C_GET_LOC_EXPECTED INTO L_qty_expected;
    CLOSE C_GET_LOC_EXPECTED;  
    ---
    -- This comparison is done as follows:
    -- Above we retrieved the sum of the old qty ordered for all virtuals within the physical
    -- We need to subtract the old qty ordered of the passed in virtual loc from that sum and
    -- then add the new qty ordered to the sum in order to get the total new qty ordered for all
    -- of the virtuals within the physical.
    -- This will then be compared to the sum of the qty appt or shipped plus total qty received.
    -- The new total qty ordered cannot fall below that value.
    ---
    IF I_ord_or_cancel_chg = 'O' THEN
      set_low_val(L_qty_ship_appt + L_qty_received);
    elsif I_ord_or_cancel_chg = 'OU' THEN -- Order Qty UOP was modified
      set_low_val((L_qty_ship_appt + L_qty_received - L_qty_ordered) / L_supp_pack_size);
    ELSE
      set_high_val((I_qty_ordered + I_qty_cancelled - L_qty_ship_appt - L_qty_received) / L_supp_pack_size);
    END IF;
    -- The new qty ordered cannot fall below the qty received.
    ---
    IF I_ord_or_cancel_chg IN ('O') THEN
      set_low_val(NVL(I_qty_received,0) + NVL(L_qty_expected,0));
	ELSIF I_ord_or_cancel_chg IN ('OU') THEN
	  set_low_val((NVL(I_qty_received,0) + NVL(L_qty_expected,0)) / L_supp_pack_size);
    ELSE
      set_high_val((I_qty_ordered + I_qty_cancelled - NVL(I_qty_received,0) - NVL(L_qty_expected,0)) / L_supp_pack_size);
    END IF;
  END IF;
  ---
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_function, TO_CHAR(SQLCODE));
  RETURN FALSE;
END GET_QTY_CHANGE;
----------------------------------------------------------------------------------
END ORDER_ITEM_VALIDATE_SQL;
/
