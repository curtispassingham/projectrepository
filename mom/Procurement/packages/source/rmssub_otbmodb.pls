
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_OTBMOD AS


PROGRAM_ERROR    EXCEPTION;

-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS (O_status               IN OUT  VARCHAR2,
                         IO_error_message       IN OUT  VARCHAR2,
                         I_cause                IN      VARCHAR2,
                         I_program              IN      VARCHAR2);

-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code          IN OUT  VARCHAR2,
                  O_error_message        IN OUT  VARCHAR2,
                  I_message              IN      RIB_OBJECT,
                  I_message_type         IN      VARCHAR2)
IS

   otbCnt BINARY_INTEGER := 0;
   L_rib_otb_tbl          "RIB_OTB_TBL" := null;
   L_rib_otbdesc_rec      "RIB_OTBDesc_REC" := null;
 
BEGIN

   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   L_rib_otbdesc_rec := treat(I_MESSAGE AS "RIB_OTBDesc_REC");

   if L_rib_otbdesc_rec is NULL or L_rib_otbdesc_rec.otb_tbl is NULL or L_rib_otbdesc_rec.otb_tbl.count =0 then
      O_status_code := API_CODES.SUCCESS;
      return;
   end if;

   L_rib_otb_tbl := L_rib_otbdesc_rec.otb_tbl;

   if OTB_SQL.INIT_ORD_RECEIVE(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   FOR otbCnt IN L_rib_otb_tbl.FIRST..L_rib_otb_tbl.LAST LOOP

      if OTB_SQL.BUILD_ORD_RECEIVE(O_error_message,
                                   L_rib_otb_tbl(otbCnt).unit_retail,
                                   L_rib_otb_tbl(otbCnt).unit_cost,
                                   L_rib_otb_tbl(otbCnt).po_nbr,
                                   L_rib_otb_tbl(otbCnt).dept,
                                   L_rib_otb_tbl(otbCnt).class,
                                   L_rib_otb_tbl(otbCnt).subclass,
                                   L_rib_otb_tbl(otbCnt).receipt_qty,
                                   L_rib_otb_tbl(otbCnt).approved_qty) = FALSE then
         raise PROGRAM_ERROR;
      end if;

   END LOOP;

   if OTB_SQL.FLUSH_ORD_RECEIVE(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- If no error is raised, then the subscription has completed successfully.
   O_status_code := API_CODES.SUCCESS;

EXCEPTION
   when PROGRAM_ERROR then
     HANDLE_ERRORS(O_status_code,
                   O_error_message,
                   API_LIBRARY.FATAL_ERROR,
                   'RMSSUB_OTBMOD.CONSUME');
   when OTHERS then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     'RMSSUB_OTBMOD.CONSUME');

END CONSUME;
-----------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status               IN OUT  VARCHAR2,
                        IO_error_message       IN OUT  VARCHAR2,
                        I_cause                IN      VARCHAR2,
                        I_program              IN      VARCHAR2)
IS

BEGIN
   
   API_LIBRARY.HANDLE_ERRORS(O_status,
                             IO_error_message,
                             I_cause,
                             I_program);
   
EXCEPTION
   when OTHERS then
      IO_error_message := sql_lib.create_msg('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             'RMSSUB_OTBMOD.HANDLE_ERRORS',
                                             to_char(SQLCODE));
     
     API_LIBRARY.HANDLE_ERRORS(O_status,
                               IO_error_message,
                               API_LIBRARY.FATAL_ERROR,
                               'RMSSUB_OTBMOD.HANDLE_ERRORS');


END HANDLE_ERRORS;
-----------------------------------------------------------------------------------------
END RMSSUB_OTBMOD;
/

