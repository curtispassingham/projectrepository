CREATE OR REPLACE PACKAGE CONTRACT_DIST_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_DIFF
-- Purpose:       Validates the diffs before distributing the contract.
--
FUNCTION VALIDATE_DIFF(O_error_message  IN OUT  VARCHAR2,
                       O_qty_ind        IN OUT  VARCHAR2,
                       I_diff_no        IN      NUMBER,
                       I_diff_group     IN      V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
                       I_contract       IN      CONTRACT_HEADER.CONTRACT_NO%TYPE,
                       I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function_Name: VALIDATE_DATE
-- Purpose:       Validates the filter criteria for dates before distributing the contract.
--
FUNCTION VALIDATE_DATE(O_error_message  IN OUT  VARCHAR2,
                       I_contract       IN      CONTRACT_HEADER.CONTRACT_NO%TYPE,
                       I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_LOCATION
-- Purpose:       Validates the filter criteria for locations before distributing the contract.
--
FUNCTION VALIDATE_LOCATION(O_error_message  IN OUT  VARCHAR2,
                           O_qty_ind        IN OUT  VARCHAR2,
                           I_contract       IN      CONTRACT_HEADER.CONTRACT_NO%TYPE,
                           I_complete_ind   IN      VARCHAR2,
                           I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: POP_DETAIL_COST
-- Purpose:       Populate the contract detail and cost tables based on the contract matrix
--                temp table.
--
FUNCTION POP_DETAIL_COST(O_error_message       IN OUT  VARCHAR2,
                         O_qty_increased_ind   IN OUT  BOOLEAN,
                         O_diff_cost_ind       IN OUT  BOOLEAN,
                         I_contract_no         IN      CONTRACT_HEADER.CONTRACT_NO%TYPE,
                         I_contract_type       IN      CONTRACT_HEADER.CONTRACT_TYPE%TYPE,
                         I_mode          IN      VARCHAR2,
                         I_calling_form        IN      VARCHAR2,
                         I_supplier            IN      ITEM_SUPPLIER.SUPPLIER%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: CONTRACT_MATRIX_TEMP_EXISTS
-- Purpose:       Check for duplicate records on the contract matrix temp table.
--
FUNCTION CONTRACT_MATRIX_TEMP_EXISTS
         (O_error_message     IN OUT VARCHAR2,
          O_duplicate_found   IN OUT BOOLEAN,
          I_item_grandparent  IN     CONTRACT_MATRIX_TEMP.ITEM_GRANDPARENT%TYPE,
          I_item_parent       IN     CONTRACT_MATRIX_TEMP.ITEM_PARENT%TYPE,
          I_item              IN     CONTRACT_MATRIX_TEMP.ITEM%TYPE,
          I_diff_1            IN     CONTRACT_MATRIX_TEMP.DIFF_1%TYPE,
          I_diff_2            IN     CONTRACT_MATRIX_TEMP.DIFF_2%TYPE,
          I_diff_3            IN     CONTRACT_MATRIX_TEMP.DIFF_3%TYPE,
          I_diff_4            IN     CONTRACT_MATRIX_TEMP.DIFF_4%TYPE,
          I_contract_no       IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: ASSOCIATE_TO_CHILD
-- Purpose:       Checks all the records on the contract matrix temp table for the
--                specified contract and attempts to associate an existing child
--                item that match the parent/diffs relationship. If the association
--                cannot be made, then an error is written to the orddist item temp
--                table. Errors include if the child item does not exist, if the child
--                item is not approved, or if the child item does not have the
--                supplier/country relationship for the contract. If the association
--                already exists, then the update is skipped for that child item.
--
FUNCTION ASSOCIATE_TO_CHILD
         (O_error_message   IN OUT VARCHAR2,
          O_child_rejected  IN OUT BOOLEAN,
          I_contract_no     IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: EXPLODE_TO_TRAN_LEVEL
-- Purpose:       For a specified contract number and either item parent or item grandparent,
--                this function inserts into the contract_matrix_temp table for all compatible
--                child items that are at the transaction level. Compatible child items must be
--                approved, at the transaction level, not exist on the contract_matrix_temp table,
--                and have the contract's supplier/country relationship. If the compatible child
--                item has a diff. 1, diff. 2, diff. 3 or diff. 4 value that is an ID, then the
--                value will be inserted into the table; otherwise NULL will be inserted. If
--                specified, the location type, location, and ready date values will be inserted;
--                otherwise NULL will be inserted.
FUNCTION EXPLODE_TO_TRAN_LEVEL
         (O_error_message       IN OUT VARCHAR2,
          O_rec_inserted        IN OUT BOOLEAN,
          I_item_grandparent    IN     CONTRACT_MATRIX_TEMP.ITEM_GRANDPARENT%TYPE,
          I_item_parent         IN     CONTRACT_MATRIX_TEMP.ITEM_PARENT%TYPE,
          I_loc_type            IN     CONTRACT_MATRIX_TEMP.LOC_TYPE%TYPE,
          I_location            IN     CONTRACT_MATRIX_TEMP.LOCATION%TYPE,
          I_ready_date          IN     CONTRACT_MATRIX_TEMP.READY_DATE%TYPE,
          I_contract_supplier   IN     CONTRACT_HEADER.SUPPLIER%TYPE,
          I_contract_country_id IN     CONTRACT_HEADER.COUNTRY_ID%TYPE,
          I_contract_no         IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: GET_CURRENCY_CODES
-- Purpose:
--
FUNCTION GET_CURRENCY_CODES
         (O_error_message       IN OUT VARCHAR2,
          IO_currency_supplier  IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
          O_rate_supplier       IN OUT CURRENCY_RATES.EXCHANGE_RATE%TYPE,
          O_cost_fmt_supplier   IN OUT CURRENCIES.CURRENCY_COST_FMT%TYPE,
          O_cost_dec_supplier   IN OUT CURRENCIES.CURRENCY_COST_DEC%TYPE,
          IO_supplier           IN OUT CONTRACT_HEADER.SUPPLIER%TYPE,
          IO_currency_contract  IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
          O_rate_contract       IN OUT CURRENCY_RATES.EXCHANGE_RATE%TYPE,
          O_cost_fmt_contract   IN OUT CURRENCIES.CURRENCY_COST_FMT%TYPE,
          O_cost_dec_contract   IN OUT CURRENCIES.CURRENCY_COST_DEC%TYPE,
          I_contract_no         IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_PARENT_DIFF
-- Purpose:       This function looks for parent/multiple diffs for the passed in contract_no.
--                If there are any found the function returns true otherwise false.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PARENT_DIFF (O_error_message       IN OUT VARCHAR2,
                               O_exists              IN OUT BOOLEAN,
                               I_contract_no         IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DIST_BY_READY_DATE
-- Purpose:       This function updates quantities, delete original
--                items which were multi-selected, delete from table for specified
--                contract_no.
---------------------------------------------------------------------------------------------
FUNCTION DIST_BY_READY_DATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_contract_no     IN       CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
END CONTRACT_DIST_SQL;
/
