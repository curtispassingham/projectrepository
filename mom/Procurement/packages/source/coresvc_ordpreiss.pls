CREATE OR REPLACE PACKAGE CORESVC_ORD_PREISSUE_SQL AUTHID CURRENT_USER AS

FUNCTION GENERATE_ORDER_NOS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_businessObject   IN OUT   "RIB_OrdNumColDesc_REC",
                            I_SvcOpContext     IN       "RIB_ServiceOpContext_REC",
                            I_businessObject   IN       "RIB_OrdNumCriVo_REC")
RETURN BOOLEAN;

END CORESVC_ORD_PREISSUE_SQL;
/
