
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORDER_INSTOCK_DATE_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------
--Filename : ORDER_INSTOCK_DATE.pls
--Purpose  : This package contains functions regarding the order instock date.
--Author   : Shaheen Malik (Accenture - ERC)
--------------------------------------------------------
--------------------------------------------------------
--Function Name : GET_MAX_INSTOCK_DT
--Purpose       : This function will be used to get the maximum 
--                date from the ORDLOC table for a given PO.
--------------------------------------------------------
FUNCTION GET_MAX_INSTOCK_DT(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_max_est_instock_date      IN OUT  ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE,
                            I_order_no                  IN      ORDLOC.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------
--Function Name : upd_instock_dt
--Purpose       : This function will be used to update the 
--                estimated in stock date on the ORDLOC table.
--------------------------------------------------------
FUNCTION UPD_INSTOCK_DT(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no             IN       ORDLOC.ORDER_NO%TYPE,
                        I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                        I_location             IN       ORDLOC.LOCATION%TYPE,
                        I_supplier             IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------
--Function Name : get_instock_dt
--Purpose       : This function will be used to calculate the "estimated_instock_date" 
--                for a given order no / item / location / supplier combination.
--------------------------------------------------------
FUNCTION GET_INSTOCK_DT(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_est_instock_date     IN OUT   ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE,
                        I_order_no             IN       ORDLOC.ORDER_NO%TYPE,
                        I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                        I_location             IN       ORDLOC.LOCATION%TYPE,
                        I_supplier             IN       ORDHEAD.SUPPLIER%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------
--Function Name : mass_upd_instock_dt
--Purpose       : This function will be used to update the 
--                estimated instock date on the ordloc table.
--------------------------------------------------------
FUNCTION MASS_UPD_INSTOCK_DT(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no       IN       ORDLOC.ORDER_NO%TYPE,
                             I_supplier       IN       ORDHEAD.SUPPLIER%TYPE,
                             I_date           IN       DATE)
RETURN BOOLEAN;

END ORDER_INSTOCK_DATE_SQL;
/
