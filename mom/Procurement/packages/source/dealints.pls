
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DEAL_INT_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------
-- Filename: dealints.pls
-- Purpose: This package will perform the required processing
--          in cases where RPM attaches a deal to Vendor Funded
--          Promotions. 
-- Author: Kathleen B. Latorre (Accenture - ERC)
----------------------------------------------------------------

--------------------------------------------------------------------------------------------
-- Function Name: UPDATE_DEAL_PROM
-- Purpose: This function will update the DEAL_DETAIL.VFP_CONTRIB_PCT
--          field with the value passed in through the I_contribution_pct
--          parameter, provided the deal is a Vendor Funded Promotion.
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_DEAL_PROM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_deal_id            IN       DEAL_DETAIL.DEAL_ID%TYPE,
                          I_deal_detail_id     IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                          I_contribution_pct   IN       DEAL_DETAIL.VFP_DEFAULT_CONTRIB_PCT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END DEAL_INT_SQL;
/                          
