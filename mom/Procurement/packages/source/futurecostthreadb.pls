CREATE OR REPLACE PACKAGE BODY FUTURE_COST_THREAD_SQL AS
----------------------------------------------------------------------------------------
-- Package Variables
----------------------------------------------------------------------------------------
   LP_event_run_type COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE := NULL;
   LP_max_tran_size  COST_EVENT_RUN_TYPE_CONFIG.MAX_TRAN_SIZE%TYPE := NULL;
   LP_vdate          DATE := get_vdate;
----------------------------------------------------------------------------------------
-- Private Functions
----------------------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
-- compile private functions as public for unit testing
$if $$UTPLSQL=FALSE $then
   ----------------------------------------------------------------------------------------
   -- Package Variables
   ----------------------------------------------------------------------------------------
   LP_event_type     COST_EVENT.EVENT_TYPE%TYPE := NULL;
   ----------------------------------------------------------------------------------------
   ----------------------------------------------------------------------------------------
   -- Function Name: REFRESH_THREAD_TABLE
   -- Purpose      : Delete any existing thread records for the process id.
   --                Records may exist when restarting for asynchronous runs where records may have been committed.
   ----------------------------------------------------------------------------------------
   FUNCTION REFRESH_THREAD_TABLE(O_error_message           IN OUT rtk_errors.rtk_text%TYPE,
                                 I_cost_event_process_id   IN     cost_event.cost_event_process_id%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_NIL
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the new item/location cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_NIL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_COST_CHANGE
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the cost change cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_COST_CHANGE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                    I_action                IN     COST_EVENT.ACTION%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_RECLASS
   -- Purpose      :  Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the reclass cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                I_action                IN     COST_EVENT.ACTION%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_MERGE_MERCH_HIER
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the merch hier cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_MERGE_MERCH_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_MERGE_ORG_HIER
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the org hier cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_MERGE_ORG_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_MERGE_SUPP_HIER
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the supplier hier cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_MERGE_SUPP_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_ELC
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the ELC cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_ELC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_COST_ZONE
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the cost zone cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_COST_ZONE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_ITEM_CZG
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the cost zone group cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_ITEM_CZG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_DEAL
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the deal cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_action                IN     COST_EVENT.ACTION%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_PRIMARY_PACK_COST
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the primary packt cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_PRIMARY_PACK_COST(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_TEMPLATE
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the cost template cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_TEMPLATE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
    -- Function Name: GET_ITEMLOC_TEMPLATE_RELN
    -- Purpose      : Insert all item/location/supplier/origin country records into the
    --                cost_event_thread table, that are related to the cost relationship cost event.
    ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_TEMPLATE_RELN(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_DEAL_PASSTHRU
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the deal passthru cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_DEAL_PASSTHRU(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: PROCESS_SUPP_CNTR
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the supplier/country cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION PROCESS_SUPP_CNTR(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_action                IN     COST_EVENT.ACTION%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: THREAD_ITEM_LOC
   -- Purpose      : Divides records into different threads based on the max thread size from
   --                the cost_event_run_type_config table.
   ----------------------------------------------------------------------------------------
   FUNCTION THREAD_ITEM_LOC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: SCHEDULE_THREADS
   -- Purpose      : Process cost event by executing its threads.
   ----------------------------------------------------------------------------------------
   FUNCTION SCHEDULE_THREADS(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_override_event_run_type IN     VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;
   ---

   ----------------------------------------------------------------------------------------
   -- Function Name: GET_ITEMLOC_FC
   -- Purpose      : Insert all item/location/supplier/origin country records into the
   --                cost_event_thread table, that are related to the change of costing loc of a franchise store.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_ITEMLOC_FC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------


   -- Procedure Name: WRITE_ERROR
   -- Purpose       : Writes error messages to the cost_event_result table.
   ----------------------------------------------------------------------------------------
   PROCEDURE WRITE_ERROR(I_error_message         IN rtk_errors.rtk_text%TYPE,
                         I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE);
$else
   -- Set Testing Type
   LP_event_type     COST_EVENT.EVENT_TYPE%TYPE := 'TST';
$end
-------------------------------------------------------------------------------------------------------
FUNCTION REFRESH_THREAD_TABLE(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   delete from cost_event_thread
    where cost_event_process_id = I_cost_event_process_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.REFRESH_THREAD_TABLE',
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_THREAD_TABLE;
----------------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_NIL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_error_message      rtk_errors.rtk_text%type := NULL;
   L_system_options_row system_options%rowtype;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   insert all
   when loc_rank = 1 and nvl(store_type,'X') in('F') then
      into future_cost_wf_helper_temp (cost_event_process_id,
                                       location)
                               values (I_cost_event_process_id,
                                       location)
   when 1 = 1 then
      into cost_event_thread (cost_event_process_id,
                              item,
                              supplier,
                              origin_country_id,
                              location,
                              thread_id,
                              store_type)
                      values (I_cost_event_process_id,
                              item,
                              supplier,
                              origin_country_id,
                              location,
                              0,
                              store_type)
      select I_cost_event_process_id,
             cen.item,
             iscl.supplier,
             iscl.origin_country_id,
             cen.location,
             cen.store_type,
             rank() over(partition by iscl.loc order by iscl.item,iscl.supplier,iscl.origin_country_id) loc_rank
        from cost_event_nil cen,
             item_supp_country_loc iscl
       where cen.cost_event_process_id = I_cost_event_process_id
         and cen.item                  = iscl.item
         and cen.location              = iscl.loc;



      --bring all costing locations into thread table (if they are not already there)
      merge into cost_event_thread cet
      using (select distinct I_cost_event_process_id as cost_event_process_id,
                    iscl.item,
                    iscl.supplier,
                    iscl.origin_country_id,
                    iscl.loc as location,
                    0 as thread_id,
                    null as store_type
               from cost_event_nil cen,
                    item_loc il,
                    item_supp_country_loc iscl
              where cen.cost_event_process_id = I_cost_event_process_id
                and cen.store_type            = 'F'
                and cen.item                  = il.item
                and cen.location              = il.loc
                and il.item                   = iscl.item
                and il.costing_loc            = iscl.loc) use_this
      on (    cet.cost_event_process_id  = use_this.cost_event_process_id
          and cet.item                   = use_this.item
          and cet.supplier               = use_this.supplier
          and cet.origin_country_id      = use_this.origin_country_id
          and cet.location               = use_this.location)
      when not matched then
      insert (cost_event_process_id,
              item,
              supplier,
              origin_country_id,
              location,
              thread_id,
              store_type)
      values (use_this.cost_event_process_id,
              use_this.item,
              use_this.supplier,
              use_this.origin_country_id,
              use_this.location,
              use_this.thread_id,
              use_this.store_type);



   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_NIL',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_NIL;
----------------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_RTC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_error_message      rtk_errors.rtk_text%type := NULL;

BEGIN

   insert into cost_event_thread (cost_event_process_id,
                              item,
                              supplier,
                              origin_country_id,
                              location,
                              store_type,
                              thread_id)
      select distinct I_cost_event_process_id,
             crc.item,
             iscl.supplier,
             iscl.origin_country_id,
             crc.loc,
             'F',
             rank() over(partition by iscl.loc order by iscl.item,iscl.supplier,iscl.origin_country_id)
        from cost_event_retail_change crc,
             item_supp_country_loc iscl
       where crc.cost_event_process_id = I_cost_event_process_id
         and crc.item                  = iscl.item
         and crc.loc                   = iscl.loc;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_RTC',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_RTC;
----------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_COST_CHANGE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                 I_action                IN     COST_EVENT.ACTION%TYPE)
RETURN BOOLEAN IS

BEGIN
   if I_action = FUTURE_COST_EVENT_SQL.ADD_EVENT then
      insert into cost_event_thread (cost_event_process_id,
                                     item,
                                     supplier,
                                     origin_country_id,
                                     location,
                                     thread_id,
                                     store_type)
         select distinct I_cost_event_process_id,
                im.item,
                cd.supplier,
                cd.origin_country_id,
                iscl.loc,
                0,
                s.store_type
           from cost_event_cost_chg ce,
                cost_susp_sup_detail cd,
                item_master im,
                item_supp_country_loc iscl,
                store s
          where ce.cost_event_process_id = I_cost_event_process_id
            and ce.src_tmp_ind           = 'N'
            and ce.cost_change           = cd.cost_change
            and (im.item              = cd.item or
                 im.item_parent       = cd.item or
                 im.item_grandparent  = cd.item)
            and im.item_level            = im.tran_level
            and iscl.item                = im.item
            and iscl.supplier            = cd.supplier
            and iscl.origin_country_id   = cd.origin_country_id
            and iscl.loc                 = s.store(+)
            and nvl(s.store_type,'X') not in ('F')
         union
         select distinct I_cost_event_process_id,
                im.item,
                cd.supplier,
                cd.origin_country_id,
                cd.loc,
                0,
                s.store_type
           from cost_event_cost_chg ce,
                cost_susp_sup_detail_loc cd,
                item_master im,
                store s
          where ce.cost_event_process_id = I_cost_event_process_id
            and ce.src_tmp_ind           = 'N'
            and ce.cost_change           = cd.cost_change
            and (im.item              = cd.item or
                 im.item_parent       = cd.item or
                 im.item_grandparent  = cd.item)
            and im.item_level            = im.tran_level
            and cd.loc                   = s.store(+)
            and nvl(s.store_type,'X') not in ('F')
         union
         select distinct I_cost_event_process_id,
                im.item,
                cct.supplier,
                cct.origin_country_id,
                iscl.loc,
                0,
                s.store_type
           from cost_event_cost_chg ce,
                cost_change_temp cct,
                item_master im,
                item_supp_country_loc iscl,
                store s
          where ce.cost_event_process_id = I_cost_event_process_id
            and ce.src_tmp_ind           = 'Y'
            and ce.cost_change           = cct.cost_change
            and cct.unit_cost_new        IS NOT NULL
            and (im.item              = cct.item or
                 im.item_parent       = cct.item or
                 im.item_grandparent  = cct.item)
            and im.item_level            = im.tran_level
            and iscl.item                = im.item
            and iscl.supplier            = cct.supplier
            and iscl.origin_country_id   = cct.origin_country_id
            and iscl.loc                 = s.store(+)
            and nvl(s.store_type,'X') not in ('F')
         union --For stores
         select distinct I_cost_event_process_id,
                im.item,
                cclt.supplier,
                cclt.origin_country_id,
                cclt.location,
                0,
                s.store_type
           from cost_event_cost_chg ce,
                cost_change_loc_temp cclt,
                item_master im,
                store s
          where ce.cost_event_process_id = I_cost_event_process_id
            and ce.src_tmp_ind           = 'Y'
            and ce.cost_change           = cclt.cost_change
            and cclt.unit_cost_new       IS NOT NULL
            and cclt.loc_type            = 'S'
            and (im.item              = cclt.item or
                 im.item_parent       = cclt.item or
                 im.item_grandparent  = cclt.item)
            and im.item_level            = im.tran_level
            and cclt.location            = s.store
            and s.store_type             not in ('F')
         union --For warehouses
         select distinct I_cost_event_process_id,
                im.item,
                cclt.supplier,
                cclt.origin_country_id,
                iscl.loc,
                0,
                null
           from cost_event_cost_chg ce,
                cost_change_loc_temp cclt,
                item_master im,
                item_supp_country_loc iscl,
                wh
          where ce.cost_event_process_id = I_cost_event_process_id
            and ce.src_tmp_ind           = 'Y'
            and ce.cost_change           = cclt.cost_change
            and cclt.unit_cost_new       IS NOT NULL
            and cclt.loc_type            = 'W'
            and cclt.item                = iscl.item
            and cclt.supplier            = iscl.supplier
            and cclt.origin_country_id   = iscl.origin_country_id
            and cclt.location            = wh.physical_wh
            and iscl.loc                 = wh.wh
            and (im.item              = cclt.item or
                 im.item_parent       = cclt.item or
                 im.item_grandparent  = cclt.item)
            and im.item_level            = im.tran_level;
   else
      insert into cost_event_thread (cost_event_process_id,
                                     item,
                                     supplier,
                                     origin_country_id,
                                     location,
                                     thread_id,
                                     store_type)
         select distinct I_cost_event_process_id,
                fc.item,
                fc.supplier,
                fc.origin_country_id,
                fc.location,
                0,
                fc.store_type
           from cost_event_cost_chg ce,
                future_cost fc
          where ce.cost_event_process_id = I_cost_event_process_id
            and ce.cost_change           = fc.cost_change
            and nvl(fc.store_type,'X') not in ('F');
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_COST_CHANGE',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_COST_CHANGE;
----------------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_RECLASS(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id   IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_action                  IN       COST_EVENT.ACTION%TYPE)
RETURN BOOLEAN IS

BEGIN

      insert into cost_event_thread(cost_event_process_id,
                                    item,
                                    supplier,
                                    origin_country_id,
                                    location,
                                    thread_id,
                                    store_type)
         select distinct I_cost_event_process_id,
                iscl.item,
                iscl.supplier,
                iscl.origin_country_id,
                iscl.loc,
                0,
                s.store_type
           from cost_event_reclass ce,
                item_master im,
                item_supp_country_loc iscl,
                store s
          where ce.cost_event_process_id = I_cost_event_process_id
            and (im.item = ce.item or
                 im.item_parent = ce.item or
                 im.item_grandparent = ce.item)
            and im.item_level = im.tran_level
            and im.item = iscl.item
            and iscl.loc = s.store(+)
            and NVL(s.store_type,'X') NOT in ('F');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_RECLASS',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_RECLASS;
----------------------------------------------------------------------------------------

FUNCTION GET_ITEMLOC_MERGE_MERCH_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
   select distinct I_cost_event_process_id,
          fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          0,
          fc.store_type
     from cost_event_merch_hier mh,
          future_cost fc
    where mh.cost_event_process_id = I_cost_event_process_id
      and fc.division              = nvl(mh.old_division, fc.division)
      and fc.group_no              = mh.old_group_no
      and fc.dept                  = nvl(mh.dept, fc.dept)
      and nvl(fc.store_type,'X')   not in ('F');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_MERGE_MERCH_HIER',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_MERGE_MERCH_HIER;
----------------------------------------------------------------------------------------

FUNCTION GET_ITEMLOC_MERGE_ORG_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
   select distinct I_cost_event_process_id,
          fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          0,
          fc.store_type
     from cost_event_org_hier oh,
          future_cost fc,
          wh
    where oh.cost_event_process_id = I_cost_event_process_id
      and nvl(fc.chain,-999)       = nvl(oh.old_chain, nvl(fc.chain,-999))
      and nvl(fc.area,-999)        = nvl(oh.old_area, nvl(fc.area,-999))
      and nvl(fc.region,-999)      = nvl(oh.old_region, nvl(fc.region,-999))
      and nvl(fc.district,-999)    = nvl(oh.old_district, nvl(fc.district,-999))
      and ((fc.loc_type = 'W' and wh.wh = fc.location and wh.physical_wh = oh.location) or (fc.location = nvl(oh.location, fc.location)))
      and nvl(fc.store_type,'X')   not in ('F');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_MERGE_ORG_HIER',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_MERGE_ORG_HIER;
----------------------------------------------------------------------------------------

FUNCTION GET_ITEMLOC_MERGE_SUPP_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
   select distinct I_cost_event_process_id,
          iscl.item,
          iscl.supplier,
          iscl.origin_country_id,
          iscl.loc,
          0,
          s.store_type
     from cost_event_supp_hier sh,
          item_supp_country_loc iscl,
          store s
    where sh.cost_event_process_id    = I_cost_event_process_id
      and iscl.item                   = sh.item
      and iscl.supplier               = sh.supplier
      and iscl.origin_country_id      = sh.origin_country_id
      and iscl.loc                    = sh.location
      and iscl.loc                    = s.store(+)
      and nvl(s.store_type,'X')       not in ('F');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_MERGE_SUPP_HIER',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_MERGE_SUPP_HIER;
----------------------------------------------------------------------------------------

FUNCTION GET_ITEMLOC_ELC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
   select distinct elc.cost_event_process_id,
          iscl.item,
          iscl.supplier,
          iscl.origin_country_id,
          iscl.loc,
          0,
          s.store_type
     from item_supp_country_loc iscl,
          store s,
          cost_event_elc elc,
          cost_zone_group_loc czgl
    where iscl.item = elc.item
      and iscl.supplier = elc.supplier
      and iscl.origin_country_id = NVL(elc.origin_country_id, iscl.origin_country_id)
      and elc.cost_event_process_id = I_cost_event_process_id
      and czgl.zone_id = NVL(elc.cost_zone, czgl.zone_id)
      and czgl.zone_group_id = NVL(elc.cost_zone_group, czgl.zone_group_id)
      and czgl.location = iscl.loc
      and iscl.loc = s.store(+)
      and NVL(s.store_type,'X') NOT in ('F');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_ELC',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_ELC;
----------------------------------------------------------------------------------------

FUNCTION GET_ITEMLOC_COST_ZONE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
   select distinct I_cost_event_process_id,
          iscl.item,
          iscl.supplier,
          iscl.origin_country_id,
          iscl.loc,
          0,
          s.store_type
     from item_supp_country_loc iscl,
          cost_event_cost_zone ce,
          store s
    where ce.cost_event_process_id    = I_cost_event_process_id
      and iscl.loc                    = ce.location
      and iscl.loc                    = s.store(+)
      and nvl(s.store_type,'X')       not in ('F');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_COST_ZONE',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_COST_ZONE;
----------------------------------------------------------------------------------------

FUNCTION GET_ITEMLOC_ITEM_CZG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
   select distinct I_cost_event_process_id,
          fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          0,
          fc.store_type
     from cost_event_item_cost_zone cz,
          item_master im,
          future_cost fc
    where cz.cost_event_process_id = I_cost_event_process_id
      and (im.item             = cz.item or
           im.item_parent      = cz.item or
           im.item_grandparent = cz.item)
      and im.item = fc.item
      and nvl(fc.store_type,'W') not in ('F');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_ITEM_CZG',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_ITEM_CZG;
----------------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_DEAL(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                         I_action                IN     COST_EVENT.ACTION%TYPE)
RETURN BOOLEAN IS

BEGIN

   if I_action = FUTURE_COST_EVENT_SQL.ADD_EVENT then

      insert all
         when rank_value = 1 then -- one record per item/supplier/country/loc
         into cost_event_thread (cost_event_process_id,
                                 item,
                                 deal_buy_item,
                                 supplier,
                                 origin_country_id,
                                 location,
                                 thread_id,
                                 store_type)
                          values(I_cost_event_process_id,
                                 item,
                                 deal_buy_item,
                                 supplier,
                                 origin_country_id,
                                 location,
                                 thread_id,
                                 store_type)
         when thread_id != -999 then --not records marked for exclusion
         into deal_item_loc_explode (item,
                                     supplier,
                                     origin_country_id,
                                     location,
                                     loc_type,
                                     deal_id,
                                     deal_detail_id,
                                     active_date,
                                     close_date,
                                     cost_appl_ind,
                                     price_cost_appl_ind,
                                     deal_class,
                                     threshold_value_type,
                                     qty_thresh_buy_item,
                                     qty_thresh_get_type,
                                     qty_thresh_get_value,
                                     qty_thresh_buy_qty,
                                     qty_thresh_recur_ind,
                                     qty_thresh_buy_target,
                                     qty_thresh_buy_avg_loc,
                                     qty_thresh_get_item,
                                     qty_thresh_get_qty,
                                     qty_thresh_free_item_unit_cost,
                                     setup_merch_level,
                                     setup_division,
                                     setup_group_no,
                                     setup_dept,
                                     setup_class,
                                     setup_subclass,
                                     setup_item_parent,
                                     setup_item_grandparent,
                                     setup_diff_1,
                                     setup_diff_2,
                                     setup_diff_3,
                                     setup_diff_4,
                                     setup_org_level,
                                     setup_chain,
                                     setup_area,
                                     setup_region,
                                     setup_district,
                                     setup_location,
                                     deal_head_type,
                                     partner_type,
                                     partner_id,
                                     create_datetime,
                                     deal_detail_application_order,
                                     get_free_discount)
                             values (item,
                                     supplier,
                                     origin_country_id,
                                     location,
                                     loc_type,
                                     deal_id,
                                     deal_detail_id,
                                     active_date,
                                     close_date,
                                     cost_appl_ind,
                                     price_cost_appl_ind,
                                     deal_class,
                                     threshold_value_type,
                                     qty_thresh_buy_item,
                                     qty_thresh_get_type,
                                     qty_thresh_get_value,
                                     qty_thresh_buy_qty,
                                     qty_thresh_recur_ind,
                                     qty_thresh_buy_target,
                                     qty_thresh_buy_avg_loc,
                                     qty_thresh_get_item,
                                     qty_thresh_get_qty,
                                     qty_thresh_free_item_unit_cost,
                                     setup_merch_level,
                                     setup_division,
                                     setup_group_no,
                                     setup_dept,
                                     setup_class,
                                     setup_subclass,
                                     setup_item_parent,
                                     setup_item_grandparent,
                                     setup_diff_1,
                                     setup_diff_2,
                                     setup_diff_3,
                                     setup_diff_4,
                                     setup_org_level,
                                     setup_chain,
                                     setup_area,
                                     setup_region,
                                     setup_district,
                                     setup_location,
                                     deal_head_type,
                                     partner_type,
                                     partner_id,
                                     create_datetime,
                                     deal_detail_application_order,
                                     get_free_discount)
         select all_rec.*,
                rank()
                  over (partition by item,
                                     supplier,
                                     origin_country_id,
                                     location,
                                     thread_id
                            order by deal_id,
                                     deal_detail_id) rank_value
           from (select item,
                        supplier,
                        origin_country_id,
                        location,
                        loc_type,
                        deal_id,
                        deal_detail_id,
                        active_date,
                        close_date,
                        cost_appl_ind,
                        price_cost_appl_ind,
                        deal_class,
                        threshold_value_type,
                        qty_thresh_buy_item,
                        qty_thresh_get_type,
                        qty_thresh_get_value,
                        qty_thresh_buy_qty,
                        qty_thresh_recur_ind,
                        qty_thresh_buy_target,
                        qty_thresh_buy_avg_loc,
                        qty_thresh_get_item,
                        qty_thresh_get_qty,
                        qty_thresh_free_item_unit_cost,
                        setup_merch_level,
                        setup_division,
                        setup_group_no,
                        setup_dept,
                        setup_class,
                        setup_subclass,
                        setup_item_parent,
                        setup_item_grandparent,
                        setup_diff_1,
                        setup_diff_2,
                        setup_diff_3,
                        setup_diff_4,
                        setup_org_level,
                        setup_chain,
                        setup_area,
                        setup_region,
                        setup_district,
                        setup_location,
                        deal_head_type,
                        partner_type,
                        partner_id,
                        create_datetime,
                        deal_detail_application_order,
                        get_free_discount,
                        deal_buy_item,
                        thread_id, -- mark exlusion records with -999 for deletion purposes
                        store_type
                   from
                       (select distinct fc.item,
                        fc.supplier,
                        fc.origin_country_id,
                        fc.location,
                        fc.loc_type,
                        dd.deal_id,
                        dd.deal_detail_id,
                        inner.deal_active_date active_date,
                        inner.deal_close_date close_date,
                        dd.cost_appl_ind,
                        dd.price_cost_appl_ind,
                        dd.deal_class,
                        dd.threshold_value_type,
                        dd.qty_thresh_buy_item,
                        dd.qty_thresh_get_type,
                        dd.qty_thresh_get_value,
                        dd.qty_thresh_buy_qty,
                        dd.qty_thresh_recur_ind,
                        dd.qty_thresh_buy_target,
                        dd.qty_thresh_buy_avg_loc,
                        dd.qty_thresh_get_item,
                        dd.qty_thresh_get_qty,
                        dd.qty_thresh_free_item_unit_cost,
                        dil.merch_level setup_merch_level,
                        DECODE(dil.merch_level, 2, dil.division, NULL) setup_division,
                        DECODE(dil.merch_level, 3, dil.group_no, NULL) setup_group_no,
                        NULL setup_dept,
                        NULL setup_class,
                        NULL setup_subclass,
                        NULL setup_item_parent,
                        NULL setup_item_grandparent,
                        NULL setup_diff_1,
                        NULL setup_diff_2,
                        NULL setup_diff_3,
                        NULL setup_diff_4,
                        dil.org_level setup_org_level,
                        DECODE(dil.org_level, 1, dil.chain,    NULL) setup_chain,
                        DECODE(dil.org_level, 2, dil.area,     NULL) setup_area,
                        DECODE(dil.org_level, 3, dil.region,   NULL) setup_region,
                        DECODE(dil.org_level, 4, dil.district, NULL) setup_district,
                        DECODE(fc.loc_type,'W',wh.physical_wh,DECODE(dil.org_level, 5, dil.location, NULL)) setup_location,
                        inner.type deal_head_type,
                        inner.partner_type,
                        inner.partner_id,
                        LP_vdate create_datetime,
                        dd.application_order deal_detail_application_order,
                        dd.get_free_discount,
                        DECODE(fc.item, dd.qty_thresh_buy_item, fc.item, dd.qty_thresh_buy_item) deal_buy_item,
                        DECODE(dil.excl_ind, 'Y', -999, 0) thread_id, -- mark exlusion records with -999 for deletion purposes
                        fc.store_type store_type,
                        rank() over (partition by fc.item,
                                                  fc.supplier,
                                                  fc.origin_country_id,
                                                  fc.location,
                                                  dd.deal_id,
                                                  dd.deal_detail_id,
                                                  inner.deal_active_date
                                         order by dil.division ,
                                                  dil.group_no) inner_rank_value
                   from deal_detail dd,
                        deal_itemloc_div_grp dil,
                        future_cost fc,
                        wh,
                        (select dh.deal_id,
                                dh.supplier,
                                dh.partner_type,
                                dh.partner_id,
                                dh.type,
                                dh.active_date deal_active_date,
                                dh.close_date  deal_close_date
                           from deal_head dh,
                                cost_event_deal cd
                          where cd.cost_event_process_id = I_cost_event_process_id
                            and cd.deal_id               = dh.deal_id) inner
                  where inner.deal_id     = dd.deal_id
                    and dd.deal_id        = dil.deal_id
                    and dd.deal_detail_id = dil.deal_detail_id
                    and fc.origin_country_id = NVL(dil.origin_country_id, fc.origin_country_id)
                    -- Partner Type level matches
                    and (   (inner.partner_type = 'S'  and fc.supplier in ( select supplier
                                                                              from sups
                                                                             where supplier_parent = inner.supplier
                                                                                or supplier = inner.supplier))
                         or (inner.partner_type = 'S1' and fc.supp_hier_lvl_1 = inner.partner_id)
                         or (inner.partner_type = 'S2' and fc.supp_hier_lvl_2 = inner.partner_id)
                         or (inner.partner_type = 'S3' and fc.supp_hier_lvl_3 = inner.partner_id))
                    -- Merch level matches
                    and (
                         (dil.merch_level = 1)
                         or
                         (    dil.merch_level  in (2,3)
                          and fc.division     = NVL(dil.division, fc.division)
                          and fc.group_no     = NVL(dil.group_no, fc.group_no)))
                    --Org level matches
                    and fc.location = wh.wh(+)
                    and ((NVL(dil.org_level, 0) = 0)
                          or
                         (    NVL(dil.org_level, 0)             in (1,2,3,4,5)
                          and NVL(fc.chain,-999)                         = NVL(dil.chain, nvl(fc.chain,-999))
                          and NVL(fc.area,-999)                          = NVL(dil.area, nvl(fc.area,-999))
                          and NVL(fc.region,-999)                        = NVL(dil.region, nvl(fc.region,-999))
                          and NVL(fc.district,-999)                      = NVL(dil.district, nvl(fc.district,-999))
                          and NVL(wh.physical_wh, fc.location) = NVL(dil.location, NVL(wh.physical_wh, fc.location))))
                          and NVL(fc.store_type,'X') NOT IN ('F')) inner2
                 where inner2.inner_rank_value = 1
                 union
                 select item,
                        supplier,
                        origin_country_id,
                        location,
                        loc_type,
                        deal_id,
                        deal_detail_id,
                        active_date,
                        close_date,
                        cost_appl_ind,
                        price_cost_appl_ind,
                        deal_class,
                        threshold_value_type,
                        qty_thresh_buy_item,
                        qty_thresh_get_type,
                        qty_thresh_get_value,
                        qty_thresh_buy_qty,
                        qty_thresh_recur_ind,
                        qty_thresh_buy_target,
                        qty_thresh_buy_avg_loc,
                        qty_thresh_get_item,
                        qty_thresh_get_qty,
                        qty_thresh_free_item_unit_cost,
                        setup_merch_level,
                        setup_division,
                        setup_group_no,
                        setup_dept,
                        setup_class,
                        setup_subclass,
                        setup_item_parent,
                        setup_item_grandparent,
                        setup_diff_1,
                        setup_diff_2,
                        setup_diff_3,
                        setup_diff_4,
                        setup_org_level,
                        setup_chain,
                        setup_area,
                        setup_region,
                        setup_district,
                        setup_location,
                        deal_head_type,
                        partner_type,
                        partner_id,
                        create_datetime,
                        deal_detail_application_order,
                        get_free_discount,
                        deal_buy_item,
                        thread_id, -- mark exlusion records with -999 for deletion purposes
                        store_type
                   from
                       (select distinct fc.item,
                        fc.supplier,
                        fc.origin_country_id,
                        fc.location,
                        fc.loc_type,
                        dd.deal_id,
                        dd.deal_detail_id,
                        inner.deal_active_date active_date,
                        inner.deal_close_date close_date,
                        dd.cost_appl_ind,
                        dd.price_cost_appl_ind,
                        dd.deal_class,
                        dd.threshold_value_type,
                        dd.qty_thresh_buy_item,
                        dd.qty_thresh_get_type,
                        dd.qty_thresh_get_value,
                        dd.qty_thresh_buy_qty,
                        dd.qty_thresh_recur_ind,
                        dd.qty_thresh_buy_target,
                        dd.qty_thresh_buy_avg_loc,
                        dd.qty_thresh_get_item,
                        dd.qty_thresh_get_qty,
                        dd.qty_thresh_free_item_unit_cost,
                        dil.merch_level setup_merch_level,
                        NULL setup_division,
                        NULL setup_group_no,
                        dil.dept setup_dept,
                        dil.class setup_class,
                        dil.subclass setup_subclass,
                        NULL setup_item_parent,
                        NULL setup_item_grandparent,
                        NULL setup_diff_1,
                        NULL setup_diff_2,
                        NULL setup_diff_3,
                        NULL setup_diff_4,
                        dil.org_level setup_org_level,
                        DECODE(dil.org_level, 1, dil.chain,    NULL) setup_chain,
                        DECODE(dil.org_level, 2, dil.area,     NULL) setup_area,
                        DECODE(dil.org_level, 3, dil.region,   NULL) setup_region,
                        DECODE(dil.org_level, 4, dil.district, NULL) setup_district,
                        DECODE(fc.loc_type,'W',wh.physical_wh,DECODE(dil.org_level, 5, dil.location, NULL)) setup_location,
                        inner.type deal_head_type,
                        inner.partner_type,
                        inner.partner_id,
                        LP_vdate create_datetime,
                        dd.application_order deal_detail_application_order,
                        dd.get_free_discount,
                        DECODE(fc.item, dd.qty_thresh_buy_item, fc.item, dd.qty_thresh_buy_item) deal_buy_item,
                        DECODE(dil.excl_ind, 'Y', -999, 0) thread_id, -- mark exlusion records with -999 for deletion purposes
                        fc.store_type,
                        rank() over (partition by fc.item,
                                                  fc.supplier,
                                                  fc.origin_country_id,
                                                  fc.location,
                                                  dd.deal_id,
                                                  dd.deal_detail_id,
                                                  inner.deal_active_date
                                         order by dil.dept,
                                                  dil.class,
                                                  dil.subclass) inner_rank_value
                   from deal_detail dd,
                        deal_itemloc_dcs dil,
                        future_cost fc,
                        wh,
                        (select dh.deal_id,
                                dh.supplier,
                                dh.partner_type,
                                dh.partner_id,
                                dh.type,
                                dh.active_date deal_active_date,
                                dh.close_date  deal_close_date
                           from deal_head dh,
                                cost_event_deal cd
                          where cd.cost_event_process_id = I_cost_event_process_id
                            and cd.deal_id               = dh.deal_id) inner
                  where inner.deal_id     = dd.deal_id
                    and dd.deal_id        = dil.deal_id
                    and dd.deal_detail_id = dil.deal_detail_id
                    and fc.origin_country_id = NVL(dil.origin_country_id, fc.origin_country_id)
                    -- Partner Type level matches
                    and (   (inner.partner_type = 'S'  and fc.supplier in ( select supplier
                                                                              from sups
                                                                             where supplier_parent = inner.supplier
                                                                                or supplier = inner.supplier))
                         or (inner.partner_type = 'S1' and fc.supp_hier_lvl_1 = inner.partner_id)
                         or (inner.partner_type = 'S2' and fc.supp_hier_lvl_2 = inner.partner_id)
                         or (inner.partner_type = 'S3' and fc.supp_hier_lvl_3 = inner.partner_id))
                    -- Merch level matches
                    and dil.merch_level in (4,5,6)
                    and fc.dept     = dil.dept
                    and fc.class    = NVL(dil.class, fc.class)
                    and fc.subclass = NVL(dil.subclass, fc.subclass)
                    --Org level matches
                    and fc.location = wh.wh(+)
                    and ((NVL(dil.org_level, 0) = 0)
                          or
                         (    NVL(dil.org_level, 0)             in (1,2,3,4,5)
                          and NVL(fc.chain,-999)                         = NVL(dil.chain, NVL(fc.chain,-999))
                          and NVL(fc.area,-999)                          = NVL(dil.area, NVL(fc.area,-999))
                          and NVL(fc.region,-999)                        = NVL(dil.region, NVL(fc.region,-999))
                          and NVL(fc.district,-999)                      = NVL(dil.district, NVL(fc.district,-999))
                          and NVL(wh.physical_wh, fc.location) = NVL(dil.location, NVL(wh.physical_wh, fc.location))))
                          and NVL(fc.store_type,'X') NOT IN ('F')) inner2
                 where inner2.inner_rank_value = 1
                 union
                 select distinct fc.item,
                        fc.supplier,
                        fc.origin_country_id,
                        fc.location,
                        fc.loc_type,
                        dd.deal_id,
                        dd.deal_detail_id,
                        inner.deal_active_date active_date,
                        inner.deal_close_date close_date,
                        dd.cost_appl_ind,
                        dd.price_cost_appl_ind,
                        dd.deal_class,
                        dd.threshold_value_type,
                        dd.qty_thresh_buy_item,
                        dd.qty_thresh_get_type,
                        dd.qty_thresh_get_value,
                        dd.qty_thresh_buy_qty,
                        dd.qty_thresh_recur_ind,
                        dd.qty_thresh_buy_target,
                        dd.qty_thresh_buy_avg_loc,
                        dd.qty_thresh_get_item,
                        dd.qty_thresh_get_qty,
                        dd.qty_thresh_free_item_unit_cost,
                        dil.merch_level setup_merch_level,
                        NULL setup_division,
                        NULL setup_group_no,
                        NULL setup_dept,
                        NULL setup_class,
                        NULL setup_subclass,
                        dil.item_parent setup_item_parent,
                        dil.item_grandparent setup_item_grandparent,
                        dil.diff_1 setup_diff_1,
                        dil.diff_2 setup_diff_2,
                        dil.diff_3 setup_diff_3,
                        dil.diff_4 setup_diff_4,
                        dil.org_level setup_org_level,
                        decode(dil.org_level, 1, dil.chain,    NULL) setup_chain,
                        decode(dil.org_level, 2, dil.area,     NULL) setup_area,
                        decode(dil.org_level, 3, dil.region,   NULL) setup_region,
                        decode(dil.org_level, 4, dil.district, NULL) setup_district,
                        decode(fc.loc_type,'W',wh.physical_wh,decode(dil.org_level, 5, dil.location, NULL)) setup_location,
                        inner.type deal_head_type,
                        inner.partner_type,
                        inner.partner_id,
                        LP_vdate create_datetime,
                        dd.application_order deal_detail_application_order,
                        dd.get_free_discount,
                        decode(fc.item, dd.qty_thresh_buy_item, fc.item, dd.qty_thresh_buy_item) deal_buy_item,
                        decode(dil.excl_ind, 'Y', -999, 0) thread_id, -- mark exlusion records with -999 for deletion purposes
                        fc.store_type
                   from deal_detail dd,
                        deal_itemloc_parent_diff dil,
                        future_cost fc,
                        wh,
                        (select dh.deal_id,
                                dh.supplier,
                                dh.partner_type,
                                dh.partner_id,
                                dh.type,
                                dh.active_date deal_active_date,
                                dh.close_date  deal_close_date
                           from deal_head dh,
                                cost_event_deal cd
                          where cd.cost_event_process_id = I_cost_event_process_id
                            and cd.deal_id               = dh.deal_id
                            and rownum                   > 0) inner
                  where inner.deal_id     = dd.deal_id
                    and dd.deal_id        = dil.deal_id
                    and dd.deal_detail_id = dil.deal_detail_id
                    and fc.origin_country_id = NVL(dil.origin_country_id, fc.origin_country_id)
                    -- Partner Type level matches
                    and (   (inner.partner_type = 'S'  and fc.supplier in ( select supplier
                                                                              from sups
                                                                             where supplier_parent = inner.supplier
                                                                                or supplier = inner.supplier))
                         or (inner.partner_type = 'S1' and fc.supp_hier_lvl_1 = inner.partner_id)
                         or (inner.partner_type = 'S2' and fc.supp_hier_lvl_2 = inner.partner_id)
                         or (inner.partner_type = 'S3' and fc.supp_hier_lvl_3 = inner.partner_id))
                    -- Merch level matches
                    and dil.merch_level in(7,8,9,10,11)
                    and dil.item_parent = fc.item_parent
                    and 1 = case when dil.merch_level = 7 and dil.item_parent = fc.item_parent then 1
                                 when dil.merch_level != 7 then 1
                                 else 0 end
                    and 1 = case when dil.merch_level = 8 and dil.item_parent = fc.item_parent and dil.diff_1 = fc.diff_1 then 1
                                 when dil.merch_level != 8 then 1
                                 else 0 end
                    and 1 = case when dil.merch_level = 9 and dil.item_parent = fc.item_parent and dil.diff_2 = fc.diff_2 then 1
                                 when dil.merch_level != 9 then 1
                                 else 0 end
                    and 1 = case when dil.merch_level = 10 and dil.item_parent = fc.item_parent and dil.diff_3 = fc.diff_3 then 1
                                 when dil.merch_level != 10 then 1
                                 else 0 end
                    and 1 = case when dil.merch_level = 11 and dil.item_parent = fc.item_parent and dil.diff_4 = fc.diff_4 then 1
                                 when dil.merch_level != 11 then 1
                                 else 0 end
                    --Org level matches
                    and fc.location = wh.wh(+)
                    and ((NVL(dil.org_level, 0) = 0)
                          or
                         (    nvl(dil.org_level, 0)             in (1,2,3,4,5)
                          and nvl(fc.chain,-999)                         = nvl(dil.chain, nvl(fc.chain,-999))
                          and nvl(fc.area,-999)                          = nvl(dil.area, nvl(fc.area,-999))
                          and nvl(fc.region,-999)                        = nvl(dil.region, nvl(fc.region,-999))
                          and nvl(fc.district,-999)                      = nvl(dil.district, nvl(fc.district,-999))
                          and nvl(wh.physical_wh, fc.location) = nvl(dil.location, nvl(wh.physical_wh, fc.location))))
                 union
                 select distinct fc.item,
                        fc.supplier,
                        fc.origin_country_id,
                        fc.location,
                        fc.loc_type,
                        dd.deal_id,
                        dd.deal_detail_id,
                        inner.deal_active_date active_date,
                        inner.deal_close_date close_date,
                        dd.cost_appl_ind,
                        dd.price_cost_appl_ind,
                        dd.deal_class,
                        dd.threshold_value_type,
                        dd.qty_thresh_buy_item,
                        dd.qty_thresh_get_type,
                        dd.qty_thresh_get_value,
                        dd.qty_thresh_buy_qty,
                        dd.qty_thresh_recur_ind,
                        dd.qty_thresh_buy_target,
                        dd.qty_thresh_buy_avg_loc,
                        dd.qty_thresh_get_item,
                        dd.qty_thresh_get_qty,
                        dd.qty_thresh_free_item_unit_cost,
                        dil.merch_level setup_merch_level,
                        NULL setup_division,
                        NULL setup_group_no,
                        NULL setup_dept,
                        NULL setup_class,
                        NULL setup_subclass,
                        NULL setup_item_parent,
                        NULL setup_item_grandparent,
                        NULL setup_diff_1,
                        NULL setup_diff_2,
                        NULL setup_diff_3,
                        NULL setup_diff_4,
                        dil.org_level setup_org_level,
                        decode(dil.org_level, 1, dil.chain,    NULL) setup_chain,
                        decode(dil.org_level, 2, dil.area,     NULL) setup_area,
                        decode(dil.org_level, 3, dil.region,   NULL) setup_region,
                        decode(dil.org_level, 4, dil.district, NULL) setup_district,
                        decode(fc.loc_type,'W',wh.physical_wh,decode(dil.org_level, 5, dil.location, NULL)) setup_location,
                        inner.type deal_head_type,
                        inner.partner_type,
                        inner.partner_id,
                        LP_vdate create_datetime,
                        dd.application_order deal_detail_application_order,
                        dd.get_free_discount,
                        decode(fc.item, dd.qty_thresh_buy_item, fc.item, dd.qty_thresh_buy_item) deal_buy_item,
                        decode(dil.excl_ind, 'Y', -999, 0) thread_id, -- mark exlusion records with -999 for deletion purposes
                        fc.store_type
                   from deal_detail dd,
                        deal_itemloc_item dil,
                        future_cost fc,
                        wh,
                        (select dh.deal_id,
                                dh.supplier,
                                dh.partner_type,
                                dh.partner_id,
                                dh.type,
                                dh.active_date deal_active_date,
                                dh.close_date  deal_close_date
                           from deal_head dh,
                                cost_event_deal cd
                          where cd.cost_event_process_id = I_cost_event_process_id
                            and cd.deal_id               = dh.deal_id) inner
                  where inner.deal_id     = dd.deal_id
                    and dd.deal_id        = dil.deal_id
                    and fc.origin_country_id = NVL(dil.origin_country_id, fc.origin_country_id)
                    and dd.deal_detail_id = dil.deal_detail_id
                    -- Partner Type level matches
                    and (   (inner.partner_type = 'S'  and fc.supplier in ( select supplier
                                                                              from sups
                                                                             where supplier_parent = inner.supplier
                                                                                or supplier = inner.supplier))
                         or (inner.partner_type = 'S1' and fc.supp_hier_lvl_1 = inner.partner_id)
                         or (inner.partner_type = 'S2' and fc.supp_hier_lvl_2 = inner.partner_id)
                         or (inner.partner_type = 'S3' and fc.supp_hier_lvl_3 = inner.partner_id))
                    -- Merch level matches
                    and fc.item = dil.item
                    --Org level matches
                    and fc.location = wh.wh(+)
                    and (NVL(dil.org_level, 0) = 0
                         or (dil.org_level = 1 and fc.chain = dil.chain)
                         or (dil.org_level = 2 and fc.area = dil.area)
                         or (dil.org_level = 3 and fc.region = dil.region)
                         or (dil.org_level = 4 and fc.district = dil.district)
                         or (dil.org_level = 5 and nvl(wh.physical_wh, fc.location) = dil.location))
                    and nvl(fc.store_type,'X') not in ('F')
               ) all_rec;

      -- use exclusion records to delete from set of non-exlusion records
      delete from deal_item_loc_explode d
      where rowid in(select dile.rowid
                       from cost_event_thread ct,
                            cost_event_deal cd,
                            deal_itemloc di,
                            deal_item_loc_explode dile
                      where ct.cost_event_process_id  = I_cost_event_process_id
                        and ct.cost_event_process_id  = cd.cost_event_process_id
                        and cd.deal_id                = di.deal_id
                        and di.excl_ind               = 'Y'
                        and ct.item                   = dile.item
                        and nvl(ct.deal_buy_item, -1) = nvl((decode(dile.item,
                                                                    dile.qty_thresh_buy_item,
                                                                    dile.item,
                                                                    dile.qty_thresh_buy_item)),
                                                             -1)
                        and ct.supplier               = dile.supplier
                        and ct.origin_country_id      = dile.origin_country_id
                        and ct.location               = dile.location
                        and ct.thread_id              = -999
                        and cd.deal_id                = dile.deal_id
                        and di.deal_detail_id         = dile.deal_detail_id);

      delete from cost_event_thread cet
       where cet.cost_event_process_id  = I_cost_event_process_id
         and cet.thread_id              = -999;

   else

      insert into cost_event_thread (cost_event_process_id,
                                     item,
                                     deal_buy_item,
                                     supplier,
                                     origin_country_id,
                                     location,
                                     thread_id,
                                     store_type)
      select distinct I_cost_event_process_id,
             fd.item,
             fd.qty_thresh_buy_item,
             fd.supplier,
             fd.origin_country_id,
             fd.location,
             0,
             s.store_type
        from deal_item_loc_explode fd,
             cost_event_deal d,
             store s
       where d.cost_event_process_id = I_cost_event_process_id
         and d.deal_id               = fd.deal_id
         and fd.location             = s.store(+)
         and nvl(s.store_type,'X') NOT IN ('F');

      if I_action = FUTURE_COST_EVENT_SQL.REMOVE_EVENT then
        delete from deal_item_loc_explode
              where deal_id in (select deal_id
                                  from cost_event_deal
                                 where cost_event_process_id = I_cost_event_process_id);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_DEAL',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_DEAL;
----------------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_PRIMARY_PACK_COST(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
   select distinct I_cost_event_process_id,
          iscl.item,
          iscl.supplier,
          iscl.origin_country_id,
          iscl.loc,
          0,
          s.store_type
     from item_supp_country_loc iscl,
          cost_event_prim_pack cpp,
          store s
    where cpp.cost_event_process_id = I_cost_event_process_id
      and cpp.item                  = iscl.item
      and cpp.location              = iscl.loc
      and iscl.loc                  = s.store(+)
      and nvl(s.store_type,'X') NOT IN ('F');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_PRIMARY_PACK_COST',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_PRIMARY_PACK_COST;
----------------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_TEMPLATE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS


cursor C_GET_FIRST_APPLIED IS
select first_applied
from wf_cost_buildup_tmpl_head wch,
     cost_event_cost_tmpl r
where r.cost_event_process_id=I_cost_event_process_id
  and wch.templ_id =r.templ_id;

 L_first_applied     wf_cost_buildup_tmpl_head.first_applied%TYPE;

BEGIN

   insert into future_cost_wf_helper_temp (cost_event_process_id,
                                           location)
   select I_cost_event_process_id,
          r.location
     from cost_event_cost_tmpl t,
          wf_cost_relationship r
    where t.cost_event_process_id = I_cost_event_process_id
      and t.templ_id              = r.templ_id;


    SQL_LIB.SET_MARK('OPEN',
                       'C_GET_FIRST_APPLIED',
                       'wf_cost_buildup_tmpl_head, cost_event_cost_relationship',
                       NULL);
      open C_GET_FIRST_APPLIED;

    SQL_LIB.SET_MARK ('FETCH',
                       'C_GET_FIRST_APPLIED',
                       'wf_cost_buildup_tmpl_head, cost_event_cost_relationship',
                       NULL);
      fetch C_GET_FIRST_APPLIED into L_first_applied;

    SQL_LIB.SET_MARK ('CLOSE',
                       'C_GET_FIRST_APPLIED',
                       'wf_cost_buildup_tmpl_head, cost_event_cost_relationship',
                       NULL);
      close C_GET_FIRST_APPLIED;

   if L_first_applied = 'C' THEN
      insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
        select distinct I_cost_event_process_id,
          fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.costing_loc,
          0,
          null
       from cost_event_cost_tmpl t,
          wf_cost_relationship r,
          future_cost fc
       where t.cost_event_process_id = I_cost_event_process_id
         and t.templ_id              = r.templ_id
         and r.dept                  = fc.dept
         and r.class                 = fc.class
         and r.subclass              = fc.subclass
         and r.location              = fc.location
         and r.item                  = fc.item;

   end if;


   if L_first_applied <> 'C' THEN
      insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
      select distinct I_cost_event_process_id,
             fc.item,
             fc.supplier,
             fc.origin_country_id,
             fc.costing_loc,
             0,
             null
      from cost_event_cost_tmpl t,
           wf_cost_relationship r,
           future_cost fc
      where t.cost_event_process_id = I_cost_event_process_id
        and t.templ_id              = r.templ_id
        and r.dept                  = fc.dept
        and decode(r.class,'-1',fc.class,r.class) = fc.class
        and decode(r.subclass,'-1',fc.subclass,r.subclass) = fc.subclass
        and r.location              = fc.location;

  end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_TEMPLATE',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_TEMPLATE;
----------------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_TEMPLATE_RELN(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   insert into future_cost_wf_helper_temp (cost_event_process_id,
                                           location)
   select I_cost_event_process_id,
          r.location
     from wf_cost_buildup_tmpl_head t,
          cost_event_cost_relationship r
    where r.cost_event_process_id = I_cost_event_process_id
      and t.templ_id              = r.templ_id;

    
   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
         select distinct I_cost_event_process_id,
                fc.item,
                fc.supplier,
                fc.origin_country_id,
                fc.costing_loc,
                0,
               null
         from cost_event_cost_relationship t,
              future_cost fc,
              wf_cost_buildup_tmpl_head wch
         where t.cost_event_process_id = I_cost_event_process_id
         and t.dept                  = fc.dept
         and t.class                 = fc.class
         and t.subclass              = fc.subclass
         and t.location              = fc.location
         and NVL(t.item,-1)          = fc.item
         and wch.templ_id            = t.templ_id
         and wch.first_applied       = 'C';   /* cost based template ,will always be applied at item level */

/* for any other template, it will be at dept ,class,subclass level */
   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
   select distinct I_cost_event_process_id,
          fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.costing_loc,
          0,
          null
     from cost_event_cost_relationship t,
          future_cost fc,
          wf_cost_buildup_tmpl_head wch
    where t.cost_event_process_id                        = I_cost_event_process_id
      and t.dept                                         = fc.dept
      and decode(t.class, -1, fc.class,t.class)          = fc.class
      and decode(t.subclass, -1, fc.subclass,t.subclass) = fc.subclass
      and t.location                                     = fc.location
      and wch.templ_id                                   = t.templ_id
      and wch.first_applied                             != 'C'
      and not exists
      (  select 1 from wf_cost_relationship wfc ,wf_cost_buildup_tmpl_head wf
          where wf.first_applied='C'
            and wf.templ_id=wfc.templ_id
            and NVL(wfc.item,-1) =fc.item
            and wfc.dept =fc.dept
            and wfc.class =fc.class
            and wfc.subclass=fc.subclass
            and wfc.location = fc.location
            and (wfc.start_date <= t.new_start_date and wfc.end_date >= t.new_end_date)
         UNION ALL
         select 1 from cost_event_thread
          where cost_event_process_id = I_cost_event_process_id
            and item = fc.item
            and location = fc.costing_loc
            and supplier = fc.supplier
            and origin_country_id = fc.origin_country_id
            and thread_id = 0   
      );
      
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_TEMPLATE_RELN',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_TEMPLATE_RELN;
----------------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_DEAL_PASSTHRU(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   insert into future_cost_wf_helper_temp (cost_event_process_id,
                                           location)
   select I_cost_event_process_id,
          e.location
     from cost_event_deal_passthru e
    where e.cost_event_process_id = I_cost_event_process_id;

   insert into cost_event_thread (cost_event_process_id,
                           item,
                           supplier,
                           origin_country_id,
                           location,
                           thread_id,
                           store_type)
   select distinct I_cost_event_process_id,
          iscl.item,
          iscl.supplier,
          iscl.origin_country_id,
          iscl.loc,
          0,
          null
     from cost_event_deal_passthru e,
          item_master im,
          item_supp_country_loc iscl
    where e.dept = im.dept
      and e.cost_event_process_id = I_cost_event_process_id
      and im.item_level = im.tran_level
      and iscl.item     = im.item
      and iscl.supplier in (select s.supplier
                              from sups s
                             where s.supplier_parent = e.supplier
                                or s.supplier        = e.supplier)
      and iscl.loc      = e.costing_loc;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_DEAL_PASSTHRU',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_DEAL_PASSTHRU;
----------------------------------------------------------------------------------------
FUNCTION PROCESS_SUPP_CNTR(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_action                IN     COST_EVENT.ACTION%TYPE)
RETURN BOOLEAN IS
L_error_message      rtk_errors.rtk_text%type := NULL;
L_system_options_row system_options%rowtype;

BEGIN

   if I_action = FUTURE_COST_EVENT_SQL.REMOVE_EVENT then
      insert into cost_event_thread (cost_event_process_id,
                                     item,
                                     supplier,
                                     origin_country_id,
                                     location,
                                     thread_id,
                                     store_type)
                              select distinct I_cost_event_process_id,
                                     cesp.item,
                                     cesp.supplier,
                                     cesp.origin_country_id,
                                     cesp.location,
                                     0,
                                     s.store_type
                                from cost_event_supp_country cesp,
                                     store s
                               where cesp.cost_event_process_id = I_cost_event_process_id
                                 and cesp.location              = s.store(+);
                                 --and NVL(s.store_type,'X') NOT IN ('F');

      update deal_item_loc_explode dile1
         set close_date = LP_vdate
       where exists(select 'x'
                      from deal_item_loc_explode dile,
                           deal_head dh,
                           cost_event_supp_country cesp
                     where cesp.cost_event_process_id                 = I_cost_event_process_id
                       and cesp.item                                  = dile.item
                       and cesp.supplier                              = dile.supplier
                       and cesp.origin_country_id                     = dile.origin_country_id
                       and cesp.location                              = dile.location
                       and dile1.deal_id                              = dile.deal_id
                       and dile1.item                                 = dile.item
                       and dile1.supplier                             = dile.supplier
                       and dile1.origin_country_id                    = dile.origin_country_id
                       and dile1.location                             = dile.location
                       and dh.deal_id                                 = dile.deal_id
                       and dh.status                                  = 'A'
                       and NVL(dile.close_date,LP_vdate+1)            > LP_vdate);
   else
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                               L_system_options_row) = FALSE then
         return FALSE;
      end if;
      insert all
      when loc_rank = 1 and nvl(store_type,'X') in('F') then
         into future_cost_wf_helper_temp (cost_event_process_id,
                                          location)
                                  values (I_cost_event_process_id,
                                          loc)
      when 1 = 1 then
         into cost_event_thread (cost_event_process_id,
                                 item,
                                 supplier,
                                 origin_country_id,
                                 location,
                                 thread_id,
                                 store_type)
                         values (I_cost_event_process_id,
                                 item,
                                 supplier,
                                 origin_country_id,
                                 loc,
                                 0,
                                 store_type)
         select distinct I_cost_event_process_id,
                         iscl.item,
                         iscl.supplier,
                         iscl.origin_country_id,
                         iscl.loc,
                         s.store_type,
                         rank() over(partition by iscl.loc order by iscl.item,iscl.supplier,iscl.origin_country_id) loc_rank
                    from item_supp_country_loc iscl,
                         cost_event_supp_country cesp,
                         store s
                   where cesp.cost_event_process_id = I_cost_event_process_id
                     and cesp.item                  = iscl.item
                     and cesp.supplier              = iscl.supplier
                     and cesp.origin_country_id     = iscl.origin_country_id
                     and cesp.location              = iscl.loc
                     and iscl.loc                   = s.store(+);
         --bring all costing locations into thread table (if they are not already there)
         merge into cost_event_thread cet
         using (select distinct I_cost_event_process_id as cost_event_process_id,
                       iscl.item,
                       iscl.supplier,
                       iscl.origin_country_id,
                       iscl.loc as location,
                       0 as thread_id,
                       null as store_type
                  from cost_event_supp_country cesp,
                       item_loc il,
                       item_supp_country_loc iscl,
                       store s
                 where cesp.cost_event_process_id = I_cost_event_process_id
                   and cesp.supplier              = iscl.supplier
                   and s.store_type               = 'F'
                   and cesp.location              = s.store
                   and cesp.item                  = il.item
                   and cesp.location              = il.loc
                   and il.item                    = iscl.item
                   and il.source_wh               = iscl.loc) use_this
         on (    cet.cost_event_process_id  = use_this.cost_event_process_id
                 and cet.item                   = use_this.item
                 and cet.supplier               = use_this.supplier
                 and cet.origin_country_id      = use_this.origin_country_id
                 and cet.location               = use_this.location)
         when not matched then
         insert (cost_event_process_id,
                 item,
                 supplier,
                 origin_country_id,
                 location,
                 thread_id,
                 store_type)
         values (use_this.cost_event_process_id,
                 use_this.item,
                 use_this.supplier,
                 use_this.origin_country_id,
                 use_this.location,
                 use_this.thread_id,
                 use_this.store_type);


   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.PROCESS_SUPP_CNTR',
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_SUPP_CNTR;
----------------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_FC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

 BEGIN
   insert into future_cost_wf_helper_temp (cost_event_process_id,
                                           location)
   select I_cost_event_process_id,
          r.franchise_loc
     from cost_event_cl r;

   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
   select distinct I_cost_event_process_id,
          isl.item,
          isl.supplier,
          isl.origin_country_id,
          isl.loc,
          0,
          'F'
     from cost_event_cl t,
          item_supp_country_loc isl
    where t.cost_event_process_id = I_cost_event_process_id
      and t.item                  = isl.item
      and t.franchise_loc         = isl.loc ;

/* get the costing location records for seeding */

   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
   select distinct I_cost_event_process_id,
          isl.item,
          isl.supplier,
          isl.origin_country_id,
          isl.loc,
          0,
          null
     from cost_event_cl t,
          item_supp_country_loc isl
    where t.cost_event_process_id = I_cost_event_process_id
      and t.item                  = isl.item
      and t.costing_loc           = isl.loc ;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.GET_ITEMLOC_FC',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_FC;
----------------------------------------------------------------------------------------
FUNCTION THREAD_ITEM_LOC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_RUN_CONFIG is
      select event_run_type,
             max_tran_size
        from cost_event_run_type_config
       where event_type = LP_event_type;

BEGIN

   -- determine run configuration
   open C_GET_RUN_CONFIG;
   fetch C_GET_RUN_CONFIG into LP_event_run_type,
                               LP_max_tran_size;
   if C_GET_RUN_CONFIG%NOTFOUND then
      close C_GET_RUN_CONFIG;
      O_error_message := SQL_LIB.CREATE_MSG('NO_FC_RUN_CONFIG', I_cost_event_process_id);
      return FALSE;
   end if;
   close C_GET_RUN_CONFIG;

   -- determine thread id per item, supplier, country, location combination and insert as new rows
   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  deal_buy_item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
   select inner.cost_event_process_id,
          inner.item,
          inner.deal_buy_item,
          inner.supplier,
          inner.origin_country_id,
          inner.location,
          ceil(inner.rank/LP_max_tran_size) thread_num,
          inner.store_type
     from (select cet.cost_event_process_id,
                  cet.item,
                  cet.deal_buy_item,
                  cet.supplier,
                  cet.origin_country_id,
                  cet.location,
                  -- nvl on deal_buy_item ensures that the buy and get items for deals are on the same thread
                  dense_rank() over(order by nvl(il.primary_cost_pack,il.item),
                                    cet.supplier,
                                    cet.origin_country_id,
                                    decode(cet.store_type,'F',il.costing_loc,cet.location)) rank,
                  cet.store_type
             from cost_event_thread cet,
                  item_loc          il
            where cet.cost_event_process_id = I_cost_event_process_id
              and il.loc = cet.location
              and il.item = nvl(cet.deal_buy_item, cet.item)) inner;

   -- delete initial thread records
   delete from cost_event_thread
    where cost_event_process_id = I_cost_event_process_id
      and thread_id = 0 ;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.THREAD_ITEM_LOC',
                                            to_char(SQLCODE));
      return FALSE;
END THREAD_ITEM_LOC;
----------------------------------------------------------------------------------------
FUNCTION SCHEDULE_THREADS(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_override_event_run_type IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   L_thread_count       NUMBER := 0;
   L_return_code        NUMBER := FUTURE_COST_EVENT_SQL.SUCCESS;
   L_invalid_run_type   EXCEPTION;
   PRAGMA               EXCEPTION_INIT(L_invalid_run_type,-08004);
   L_src_tmp_ind        COST_EVENT_COST_CHG.SRC_TMP_IND%TYPE := NULL;

   cursor C_THREAD_COUNT is
      select max(thread_id)
        from cost_event_thread
       where cost_event_process_id = I_cost_event_process_id;

   cursor C_CHECK_SRC_TMP is
      select src_tmp_ind
        from cost_event_cost_chg
       where cost_event_process_id = I_cost_event_process_id
         and src_tmp_ind           = 'Y'
         and rownum                = 1;


BEGIN

   -- determine thread count
   open C_THREAD_COUNT;
   fetch C_THREAD_COUNT into L_thread_count;
   if C_THREAD_COUNT%NOTFOUND then
      close C_THREAD_COUNT;
      O_error_message := SQL_LIB.CREATE_MSG('NO_THREADS_FOUND', I_cost_event_process_id);
      return FALSE;
   end if;
   close C_THREAD_COUNT;

   -- Override the run type if the input override parameter is defined as Y
   if I_override_event_run_type = 'Y' then
      LP_event_run_type := FUTURE_COST_EVENT_SQL.OVRID_COST_EVENT_RUN_TYPE;
   end if;

   -- If the cost event is of type Cost Change and src_tmp_ind is set to 'Y'
   -- then set run type to Override.
   if LP_event_type = FUTURE_COST_EVENT_SQL.COST_CHANGE_COST_EVENT_TYPE then
      open C_CHECK_SRC_TMP;
      fetch C_CHECK_SRC_TMP into L_src_tmp_ind;
      close C_CHECK_SRC_TMP;

      if L_src_tmp_ind = 'Y' then
         LP_event_run_type := FUTURE_COST_EVENT_SQL.OVRID_COST_EVENT_RUN_TYPE;
      end if;
   end if;

   -- loop and spawn threads depending on run type
   FOR i in 1..L_thread_count LOOP
      ---
      -- write a row with a status of NEW to the cost_event_result table
      insert into cost_event_result(cost_event_process_id,
                                    thread_id,
                                    attempt_num,
                                    status,
                                    error_message,
                                    create_datetime)
                             values(I_cost_event_process_id,
                                    i,   -- thread id
                                    0,   -- attempt number
                                    FUTURE_COST_EVENT_SQL.NEW_COST_EVENT,
                                    NULL,
                                    SYSDATE);

      -- synchronous
      if LP_event_run_type = FUTURE_COST_EVENT_SQL.SYNC_COST_EVENT_RUN_TYPE then
         FUTURE_COST_SQL.PROCESS_COST_EVENTS(I_cost_event_process_id,
                                             i,   -- thread number
                                             L_return_code,
                                             O_error_message);
         if L_return_code = FUTURE_COST_EVENT_SQL.FAILURE then
            return FALSE;
         end if;
      elsif
      -- asynchronous
         LP_event_run_type = FUTURE_COST_EVENT_SQL.ASYNC_COST_EVENT_RUN_TYPE then
         if ENQUEUE_COST_EVENT_THREAD(O_error_message,
                                      I_cost_event_process_id,
                                      i) = FALSE then
            return FALSE;
         end if;
      elsif
         --Over ride event run type
         LP_event_run_type = FUTURE_COST_EVENT_SQL.OVRID_COST_EVENT_RUN_TYPE then
         FUTURE_COST_SQL.PROCESS_COST_EVENTS(I_cost_event_process_id,
                                             i,   -- thread number
                                             L_return_code,
                                             O_error_message);
         if L_return_code = FUTURE_COST_EVENT_SQL.FAILURE then
            return FALSE;
         end if;
      else
         raise L_invalid_run_type;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   WHEN L_invalid_run_type THEN
      O_error_message := SQL_LIB.CREATE_MSG('INV_COST_EVENT_RUN_TYPE');
      return FALSE;
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.SCHEDULE_THREADS',
                                            to_char(SQLCODE));
      return FALSE;
END SCHEDULE_THREADS;
----------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_error_message         IN RTK_ERRORS.RTK_TEXT%TYPE,
                      I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE) AS
BEGIN

   -- Error prior to threading will have a thread_id of 0
   insert into cost_event_result(cost_event_process_id,
                                 thread_id,
                                 attempt_num,
                                 status,
                                 retry_user_id,
                                 error_message,
                                 create_datetime)
                          values(I_cost_event_process_id,
                                 0,   -- thread_id
                                 0,   -- attempt_num
                                 FUTURE_COST_EVENT_SQL.ERROR_COST_EVENT,
                                 USER,
                                 I_error_message,
                                 SYSDATE);

END;
----------------------------------------------------------------------------------------
PROCEDURE PROCESS_COST_EVENTS(I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              O_return_code             IN OUT NUMBER,
                              O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_override_event_run_type IN     VARCHAR2 DEFAULT 'N') IS

   L_action            COST_EVENT.ACTION%TYPE;
   L_return_code       BOOLEAN;
   L_rec_found         VARCHAR2(1) := 'N';
   L_warring_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_event_run_type    COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE := NULL;
   L_src_tmp_ind       COST_EVENT_COST_CHG.SRC_TMP_IND%TYPE := NULL;
   L_override_run_type VARCHAR2(5);

   cursor c_event is
      select action,
             event_type,
             override_run_type
        from cost_event
       where cost_event_process_id =  I_cost_event_process_id;

   cursor C_REC_FOUND is
      select 'Y'
        from cost_event_thread
       where cost_event_process_id = I_cost_event_process_id
         and rownum = 1;

   cursor C_GET_RUN_TYPE is
      select NVL(L_override_run_type,event_run_type)
        from cost_event_run_type_config
       where event_type = LP_event_type;

   cursor C_CHECK_SRC_TMP is
      select src_tmp_ind
        from cost_event_cost_chg
       where cost_event_process_id = I_cost_event_process_id
         and src_tmp_ind           = 'Y'
         and rownum                = 1;



BEGIN

   -- clear out cost_event_thread table in case of restart prior to threading
   if REFRESH_THREAD_TABLE(O_error_message,
                           I_cost_event_process_id) = FALSE then
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      WRITE_ERROR(O_error_message, I_cost_event_process_id);
      return;
   end if;

   -- determine action and event type
   open c_event;
   fetch c_event into L_action,
                      LP_event_type,
                      L_override_run_type;
   close c_event;

   case LP_event_type

   when FUTURE_COST_EVENT_SQL.NEW_ITEM_LOC_COST_EVENT_TYPE then
      -- new item loc for approved items (i)
      L_return_code := GET_ITEMLOC_NIL(O_error_message, I_cost_event_process_id);

   when FUTURE_COST_EVENT_SQL.COST_CHANGE_COST_EVENT_TYPE then
      -- cost change approval and change from approved status (s)
      L_return_code := GET_ITEMLOC_COST_CHANGE(O_error_message, I_cost_event_process_id, L_action);

   when FUTURE_COST_EVENT_SQL.RECLASS_COST_EVENT_TYPE then
      -- reclassification (i)
      L_return_code := GET_ITEMLOC_RECLASS(O_error_message, I_cost_event_process_id, L_action);

   when FUTURE_COST_EVENT_SQL.MERCH_HIER_COST_EVENT_TYPE then
      -- merch hierarchy change (i)
      L_return_code := GET_ITEMLOC_MERGE_MERCH_HIER(O_error_message, I_cost_event_process_id);

   when FUTURE_COST_EVENT_SQL.ORG_HIER_COST_EVENT_TYPE then
      -- org hierarchy change (i)
      L_return_code := GET_ITEMLOC_MERGE_ORG_HIER(O_error_message, I_cost_event_process_id);

   when FUTURE_COST_EVENT_SQL.SUPP_HIER_COST_EVENT_TYPE then
      -- supplier hierarchy change (i)
      L_return_code := GET_ITEMLOC_MERGE_SUPP_HIER(O_error_message, I_cost_event_process_id);

   when FUTURE_COST_EVENT_SQL.ELC_COST_EVENT_TYPE then
      -- item expense change (i)
      L_return_code := GET_ITEMLOC_ELC(O_error_message, I_cost_event_process_id);

   when FUTURE_COST_EVENT_SQL.COST_ZONE_COST_EVENT_TYPE then
      -- location cost zone change (i)
      L_return_code := GET_ITEMLOC_COST_ZONE(O_error_message, I_cost_event_process_id);

   when FUTURE_COST_EVENT_SQL.ITEM_COST_ZONE_GRP_EVENT_TYPE then
      -- item cost zone group change (i)
      L_return_code := GET_ITEMLOC_ITEM_CZG(O_error_message, I_cost_event_process_id);

   when FUTURE_COST_EVENT_SQL.DEAL_COST_EVENT_TYPE then
      -- deals (i)
      L_return_code := GET_ITEMLOC_DEAL(O_error_message, I_cost_event_process_id, L_action);

   when FUTURE_COST_EVENT_SQL.PRIMARY_PACK_COST_EVENT_TYPE then
      -- primary cost pack
      L_return_code := GET_ITEMLOC_PRIMARY_PACK_COST(O_error_message, I_cost_event_process_id);

   when FUTURE_COST_EVENT_SQL.SUPP_COUNTRY_COST_EVENT_TYPE then
      -- Add/remove supplier/country
      L_return_code := PROCESS_SUPP_CNTR(O_error_message, I_cost_event_process_id, L_action);
   when FUTURE_COST_EVENT_SQL.TEMPLATE_COST_EVENT_TYPE then
      -- Modify Template
      L_return_code := GET_ITEMLOC_TEMPLATE (O_error_message, I_cost_event_process_id);

   when FUTURE_COST_EVENT_SQL.TEMPLATE_RELN_COST_EVENT_TYPE then
      -- Add/remove/modify WF Cost Relationship
      L_return_code := GET_ITEMLOC_TEMPLATE_RELN (O_error_message,I_cost_event_process_id);

   when FUTURE_COST_EVENT_SQL.DEAL_PASSTHRU_COST_EVENT_TYPE then
      -- Add/remove/modify Deal Passthru
      L_return_code :=  GET_ITEMLOC_DEAL_PASSTHRU (O_error_message, I_cost_event_process_id);

   when FUTURE_COST_EVENT_SQL.FRAN_COST_LOC_CHG_EVENT_TYPE then
     -- Cost location change of a franchise store
        L_return_code :=  GET_ITEMLOC_FC (O_error_message, I_cost_event_process_id);

 when FUTURE_COST_EVENT_SQL.RETAIL_CHANGE_COST_EVENT_TYPE then
     -- retail change for the franchise location.
        L_return_code :=  GET_ITEMLOC_RTC (O_error_message, I_cost_event_process_id);

   else
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_COST_EVENT_TYPE');
      WRITE_ERROR(O_error_message, I_cost_event_process_id);
      return;
   end case;

   -- check for execution status of initial threading inserts
   if not L_return_code then
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      WRITE_ERROR(O_error_message, I_cost_event_process_id);
      return;
   end if;

   --Is there any records to process
   --if no record found then, stop and write warring to cost_event_result

   open C_GET_RUN_TYPE;
   fetch C_GET_RUN_TYPE into L_event_run_type;
   close C_GET_RUN_TYPE;

   -- Override the run type from the table if the input override parameter is defined as Y
   if I_override_event_run_type = 'Y' then
      L_event_run_type := FUTURE_COST_EVENT_SQL.OVRID_COST_EVENT_RUN_TYPE;
   end if;

   -- If the cost event is of type Cost Change and src_tmp_ind is set to 'Y'
   -- then set run type to Override.
   if LP_event_type = FUTURE_COST_EVENT_SQL.COST_CHANGE_COST_EVENT_TYPE then
      open C_CHECK_SRC_TMP;
      fetch C_CHECK_SRC_TMP into L_src_tmp_ind;
      close C_CHECK_SRC_TMP;
      if L_src_tmp_ind = 'Y' then
         L_event_run_type := FUTURE_COST_EVENT_SQL.OVRID_COST_EVENT_RUN_TYPE;
      end if;
   end if;

   case L_event_run_type
   -- batch
   when FUTURE_COST_EVENT_SQL.BATCH_COST_EVENT_RUN_TYPE then
      open C_REC_FOUND;
      fetch C_REC_FOUND into L_rec_found;
      if C_REC_FOUND%NOTFOUND then
         close C_REC_FOUND;
         L_warring_message := SQL_LIB.CREATE_MSG('NO_FC_RECORDS');
         insert into cost_event_result(cost_event_process_id,
                                       thread_id,
                                       attempt_num,
                                       status,
                                       retry_user_id,
                                       error_message,
                                       create_datetime)
                                values(I_cost_event_process_id,
                                       0,   -- thread_id
                                       0,   -- attempt_num
                                       FUTURE_COST_EVENT_SQL.NEW_COST_EVENT,
                                       NULL,
                                       L_warring_message,
                                       SYSDATE);
         return;
      end if;
      close C_REC_FOUND;
   -- synchronous or asynchronous or override
   else
      open C_REC_FOUND;
      fetch C_REC_FOUND into L_rec_found;
      if C_REC_FOUND%NOTFOUND then
         close C_REC_FOUND;
         L_warring_message := SQL_LIB.CREATE_MSG('NO_FC_RECORDS');
         insert into cost_event_result(cost_event_process_id,
                                       thread_id,
                                       attempt_num,
                                       status,
                                       retry_user_id,
                                       error_message,
                                       create_datetime)
                                values(I_cost_event_process_id,
                                       0,   -- thread_id
                                       0,   -- attempt_num
                                       FUTURE_COST_EVENT_SQL.COMPLETE_COST_EVENT,
                                       NULL,
                                       L_warring_message,
                                       SYSDATE);
         return;
      end if;
      close C_REC_FOUND;

      -- thread cost event
      if THREAD_ITEM_LOC(O_error_message,
                         I_cost_event_process_id) = FALSE then
         O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
         WRITE_ERROR(O_error_message, I_cost_event_process_id);
         return;
      end if;

      -- spawn processes for each thread
      if SCHEDULE_THREADS(O_error_message,
                          I_cost_event_process_id,
                          I_override_event_run_type) = FALSE then
         O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
         -- Write error only if asynchronous run to handle failure prior to thread scheduling.
         -- Errors from synch will be returned as error message and failed status to controlling process.
         if LP_event_run_type = FUTURE_COST_EVENT_SQL.ASYNC_COST_EVENT_RUN_TYPE then
            WRITE_ERROR(O_error_message, I_cost_event_process_id);
         end if;
         return;
      end if;
   end case;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.PROCESS_COST_EVENTS',
                                            to_char(SQLCODE));
      WRITE_ERROR(O_error_message, I_cost_event_process_id);
      O_return_code :=  FUTURE_COST_EVENT_SQL.FAILURE;

END PROCESS_COST_EVENTS;
----------------------------------------------------------------------------------------
FUNCTION INSERT_BATCH_THREADS(I_thread_num            IN     NUMBER,
                              O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(61) := 'FUTURE_COST_THREAD_SQL.INSERT_BATCH_THREADS';

BEGIN

   -- thread the records on cost_event_thread based on number of threads set for the batch
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'cost_event_thread',
                    'thread_num: '||I_thread_num);
   insert into cost_event_thread (cost_event_process_id,
                                  item,
                                  deal_buy_item,
                                  supplier,
                                  origin_country_id,
                                  location,
                                  thread_id,
                                  store_type)
   select inner.cost_event_process_id,
          inner.item,
          inner.deal_buy_item,
          inner.supplier,
          inner.origin_country_id,
          inner.location,
          (mod(inner.ranking, I_thread_num) + 1) as thread_num,
          s.store_type
     from (select cet.cost_event_process_id,
                  cet.item,
                  cet.deal_buy_item,
                  cet.supplier,
                  cet.origin_country_id,
                  cet.location,
                  dense_rank() over(order by nvl(il.primary_cost_pack,il.item),
                                                 cet.supplier,
                                                 cet.origin_country_id,
                                                 decode(cet.store_type,'F',il.costing_loc,cet.location)) as ranking
             from cost_event_thread          cet,
                  cost_event_run_type_config cec,
                  cost_event                 ce,
                  item_loc                   il
            where ce.cost_event_process_id = cet.cost_event_process_id
              and cet.thread_id = 0
              and NVL(ce.override_run_type,cec.event_run_type) = 'BATCH'
              and cec.event_type = ce.event_type
              and il.loc = cet.location
              and il.item = nvl(cet.deal_buy_item, cet.item)
              and not exists (select 'x'
                                from cost_event_result cer
                               where cer.cost_event_process_id = cet.cost_event_process_id
                                 and cer.thread_id             = cet.thread_id)
          ) inner,
          store s
    where inner.location = s.store(+);


   -- insert into cost_event_result table to track status
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'cost_event_result',
                    'thread_num: '||I_thread_num);
   insert into cost_event_result (cost_event_process_id,
                                  thread_id,
                                  attempt_num,
                                  status,
                                  error_message,
                                  create_datetime)
   select distinct cet.cost_event_process_id,
          cet.thread_id,
          0,   -- attempt_number
          FUTURE_COST_EVENT_SQL.NEW_COST_EVENT,
          NULL,
          SYSDATE
     from cost_event_thread          cet,
          cost_event_run_type_config cec,
          cost_event                 ce
    where ce.cost_event_process_id = cet.cost_event_process_id
      and cet.thread_id != 0
      and NVL(ce.override_run_type,cec.event_run_type) = 'BATCH'
      and cec.event_type = ce.event_type
      -- retrieve only new records which is not yet in the cost_event_result table
      and not exists (select 'x'
                        from cost_event_result cer
                       where cer.cost_event_process_id = cet.cost_event_process_id
                         and cer.thread_id             = cet.thread_id);


   /* delete initial thread records */
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'cost_event_thread',
                    'thread_num: '||I_thread_num);
   delete from cost_event_thread cet
    where cet.thread_id = 0
      and exists (select 'x'
                    from  cost_event_thread          cetc,
                          cost_event_run_type_config cec,
                          cost_event                 ce
                    where cetc.cost_event_process_id = cet.cost_event_process_id
                      and cetc.item = cet.item
                      and nvl(cetc.deal_buy_item, -1) = nvl(cet.deal_buy_item, -1)
                      and cetc.supplier = cet.supplier
                      and cetc.origin_country_id = cet.origin_country_id
                      and cetc.location = cet.location
                      and ce.cost_event_process_id = cetc.cost_event_process_id
                      and cetc.thread_id <> 0
                      and NVL(ce.override_run_type,cec.event_run_type) = 'BATCH'
                      and cec.event_type = ce.event_type);


   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_BATCH_THREADS;
----------------------------------------------------------------------------------------
PROCEDURE PROCESS_COST_EVENTS(I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE) IS

   L_return_code      NUMBER := FUTURE_COST_EVENT_SQL.SUCCESS;
   L_error_message    RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   FUTURE_COST_THREAD_SQL.PROCESS_COST_EVENTS(I_cost_event_process_id,
                                              L_return_code,
                                              L_error_message);

   if L_return_code = FUTURE_COST_EVENT_SQL.FAILURE then
      WRITE_ERROR(L_error_message, I_cost_event_process_id);
   end if;

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_THREAD_SQL.PROCESS_COST_EVENTS',
                                            to_char(SQLCODE));
      WRITE_ERROR(L_error_message, I_cost_event_process_id);

END PROCESS_COST_EVENTS;
----------------------------------------------------------------------------------------
FUNCTION ENQUEUE_COST_EVENT_THREAD(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_cost_event_process_id  IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                   I_thread_id              IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS
   L_program             VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ENQUEUE_COST_EVENT_THREAD';
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_queue_name          VARCHAR2(200);
   L_message_handle      RAW(16);
   L_message             cost_event_thread_msg := cost_event_thread_msg(I_cost_event_process_id, I_thread_id);
   L_owner               VARCHAR2(30);

   cursor C_GET_OWNER is
      select table_owner
        from system_options;

BEGIN

   open  C_GET_OWNER;
   fetch C_GET_OWNER into L_owner;
   close C_GET_OWNER;

   L_queue_name := L_owner || '.cost_event_thread_queue';
   L_enqueue_options.visibility := DBMS_AQ.IMMEDIATE;

   DBMS_AQ.ENQUEUE(
      queue_name          => L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
END ENQUEUE_COST_EVENT_THREAD;
----------------------------------------------------------------------------------------
END FUTURE_COST_THREAD_SQL;
/