CREATE OR REPLACE PACKAGE BODY COST_COMP_UPD_SQL AS

-- Package plsql table definitions.
TYPE ordsku_assess_TBL        is table of   COST_COMP_EXC_LOG%ROWTYPE   INDEX BY BINARY_INTEGER;
TYPE orders_TBL               is table of   COST_COMP_EXC_LOG%ROWTYPE            INDEX BY BINARY_INTEGER;
TYPE rev_ord_TBL              is table of   REV_ORDERS%ROWTYPE                   INDEX BY BINARY_INTEGER;
TYPE mod_order_item_hts_TBL   is table of   MOD_ORDER_ITEM_HTS%ROWTYPE           INDEX BY BINARY_INTEGER;

L_system_options              SYSTEM_OPTIONS%ROWTYPE;
L_vdate                       PERIOD.VDATE%TYPE := GET_VDATE();
L_next_vdate                  PERIOD.VDATE%TYPE := L_vdate +1;

---------------------------------------------------------------------------------------------
-- Function Name: SAVE_UPDATES
-- Purpose: The function is called by the forms where the cost components modifications are done.
--          This function saves the changes on a staging table at defaulting level/comp id/defaulting entity.
--          Merge statements will ensure that only one row exists for defaulting level/comp id/defaulting entity
--          (and values specific to the component type).
---------------------------------------------------------------------------------------------
FUNCTION SAVE_UPDATES (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_seq_no                 IN      COST_COMP_UPD_STG.SEQ_NO%TYPE,
                       I_defaulting_level       IN      COST_COMP_UPD_STG.DEFAULTING_LEVEL%TYPE,
                       I_effective_date         IN      COST_COMP_UPD_STG.EFFECTIVE_DATE%TYPE,                       
                       I_comp_id                IN      ELC_COMP.COMP_ID%TYPE,
                       I_comp_type              IN      ELC_COMP.COMP_TYPE%TYPE,
                       I_dept                   IN      DEPS.DEPT%TYPE,
                       I_supplier               IN      SUPS.SUPPLIER%TYPE,
                       I_item                   IN      ITEM_MASTER.ITEM%TYPE,
                       I_item_exp_type          IN      ITEM_EXP_DETAIL.ITEM_EXP_TYPE%TYPE,
                       I_item_exp_seq           IN      ITEM_EXP_DETAIL.ITEM_EXP_SEQ%TYPE,
                       I_hts                    IN      HTS.HTS%TYPE,
                       I_import_country_id      IN      HTS.IMPORT_COUNTRY_ID%TYPE,
                       I_origin_country_id      IN      HTS.IMPORT_COUNTRY_ID%TYPE,
                       I_effect_from            IN      DATE,
                       I_effect_to              IN      DATE,
                       I_exp_prof_key           IN      EXP_PROF_HEAD.EXP_PROF_KEY%TYPE,
                       I_from_loc               IN      ITEM_LOC.LOC%TYPE,
                       I_to_loc                 IN      ITEM_LOC.LOC%TYPE,
                       I_zone_group_id          IN      COST_ZONE.ZONE_GROUP_ID%TYPE,
                       I_zone_id                IN      COST_ZONE.ZONE_ID%TYPE,
                       I_old_comp_rate          IN      ELC_COMP.COMP_RATE%TYPE,
                       I_old_comp_currency      IN      ELC_COMP.COMP_CURRENCY%TYPE,
                       I_old_per_count          IN      ELC_COMP.PER_COUNT%TYPE,
                       I_old_per_count_uom      IN      ELC_COMP.PER_COUNT_UOM%TYPE,
                       I_new_comp_rate          IN      ELC_COMP.COMP_RATE%TYPE,
                       I_new_comp_currency      IN      ELC_COMP.COMP_CURRENCY%TYPE,
                       I_new_per_count          IN      ELC_COMP.PER_COUNT%TYPE,
                       I_new_per_count_uom      IN      ELC_COMP.PER_COUNT_UOM%TYPE,
                       I_cntry_default_ind      IN      COST_COMP_UPD_STG.CNTRY_DEFAULT_IND%TYPE,
                       I_supp_default_ind       IN      COST_COMP_UPD_STG.SUPP_DEFAULT_IND%TYPE,
                       I_ptnr_default_ind       IN      COST_COMP_UPD_STG.PTNR_DEFAULT_IND%TYPE,
                       I_item_default_ind       IN      COST_COMP_UPD_STG.ITEM_DEFAULT_IND%TYPE,
                       I_order_default_ind      IN      COST_COMP_UPD_STG.ORDER_DEFAULT_IND%TYPE,
                       I_tsf_alloc_default_ind  IN      COST_COMP_UPD_STG.TSF_ALLOC_DEFAULT_IND%TYPE,
                       I_dept_default_ind       IN      COST_COMP_UPD_STG.DEPT_DEFAULT_IND%TYPE,
                       I_lading_port            IN      COST_COMP_UPD_STG.LADING_PORT%TYPE DEFAULT NULL,
                       I_discharge_port         IN      COST_COMP_UPD_STG.DISCHARGE_PORT%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

BEGIN

   -- Check for correctness of the input values
   if I_defaulting_level  is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_defaulting_level',
                                            'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                             NULL);
      return FALSE;
   end if;
   
   if I_comp_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_comp_id',
                                            'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                             NULL);
      return FALSE;
   end if;
   
   if I_comp_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_comp_type',
                                            'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                             NULL);
      return FALSE;
   end if;
   
   if I_old_comp_rate is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_comp_rate',
                                            'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                             NULL);
      return FALSE;
   end if;
   
   if I_new_comp_rate is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_comp_rate',
                                            'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                             NULL);
      return FALSE;
   end if;

   if I_comp_type NOT IN ('E', 'A', 'U') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- The following condition is satified when the function is called from ELC Comp form.
   if I_defaulting_level = 'E' then
      
      -- merge statement ensures that there are no duplicate rows for the unique key combination.
      merge into cost_comp_upd_stg ccus
      using (select 'x' from dual) 
      on   (ccus.seq_no           = I_seq_no)
      when matched then
      update
            set ccus.new_comp_rate         = I_new_comp_rate,
                ccus.new_comp_currency     = I_new_comp_currency, 
                ccus.new_per_count         = I_new_per_count, 
                ccus.new_per_count_uom     = I_new_per_count_uom,
                ccus.cntry_default_ind     = I_cntry_default_ind,
                ccus.supp_default_ind      = I_supp_default_ind,
                ccus.ptnr_default_ind      = I_ptnr_default_ind,
                ccus.item_default_ind      = I_item_default_ind,
                ccus.order_default_ind     = I_order_default_ind,
                ccus.tsf_alloc_default_ind = I_tsf_alloc_default_ind,
                ccus.dept_default_ind      = I_dept_default_ind,
                ccus.effective_date        = I_effective_date
      when not matched then
        insert (seq_no,
                defaulting_level,
                effective_date,
                dept,
                comp_id,
                comp_type,
                supplier,
                item,
                item_exp_type,
                item_exp_seq,   
                hts,
                import_country_id,      
                origin_country_id,      
                effect_from,
                effect_to,     
                exp_prof_key,   
                from_loc,       
                to_loc, 
                zone_group_id,
                zone_id,
                old_comp_rate,
                old_comp_currency,
                old_per_count,
                old_per_count_uom,
                new_comp_rate,
                new_comp_currency,
                new_per_count,
                new_per_count_uom,
                cntry_default_ind,
                supp_default_ind,
                ptnr_default_ind,
                item_default_ind,
                order_default_ind,
                tsf_alloc_default_ind,
                dept_default_ind) 
        values (cost_comp_upd_stg_sequence.nextval,
                I_defaulting_level,
                I_effective_date,
                NULL,
                I_comp_id,
                I_comp_type,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                I_import_country_id,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                I_old_comp_rate,
                I_old_comp_currency,
                I_old_per_count,
                I_old_per_count_uom,
                I_new_comp_rate,
                I_new_comp_currency,
                I_new_per_count,
                I_new_per_count_uom,
                NVL(I_cntry_default_ind, 'N'),
                NVL(I_supp_default_ind,  'N'),
                NVL(I_ptnr_default_ind,  'N'),
                NVL(I_item_default_ind,  'N'),
                NVL(I_order_default_ind, 'N'),
                NVL(I_tsf_alloc_default_ind, 'N'),
                NVL(I_dept_default_ind,  'N'));      
      --
      return TRUE;            
      --
   end if;
   
   -- The following condition is satisfied when the function is called from Expense profile form.
   if I_defaulting_level in ('S', 'P', 'C') then
      
      -- Validate the component type
      if I_comp_type != 'E' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                               'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_exp_prof_key is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_exp_prof_key',
                                               'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                NULL);
         return FALSE;
      end if;
   
      -- merge statement ensures that there are no duplicate rows for the unique key combination.
      merge into cost_comp_upd_stg ccus
      using (select 'x' from dual)
            on (ccus.seq_no         = I_seq_no)
      when matched then
      update
            set ccus.new_comp_rate         = I_new_comp_rate,
                ccus.new_comp_currency     = I_new_comp_currency,
                ccus.new_per_count         = I_new_per_count, 
                ccus.new_per_count_uom     = I_new_per_count_uom,
                ccus.item_default_ind      = I_item_default_ind,
                ccus.order_default_ind     = I_order_default_ind,
                ccus.effective_date        = I_effective_date
      when not matched then
        insert (seq_no,
                defaulting_level,
                effective_date,
                dept,
                comp_id,
                comp_type,
                supplier,
                item,
                item_exp_type,
                item_exp_seq,
                hts,
                import_country_id,      
                origin_country_id,      
                effect_from,
                effect_to,     
                exp_prof_key,   
                from_loc,       
                to_loc,
                zone_group_id,
                zone_id,
                old_comp_rate,
                old_comp_currency,
                old_per_count,
                old_per_count_uom,
                new_comp_rate,
                new_comp_currency,
                new_per_count,
                new_per_count_uom,
                cntry_default_ind,
                supp_default_ind,
                ptnr_default_ind,
                item_default_ind,
                order_default_ind,
                tsf_alloc_default_ind,
                dept_default_ind,
                lading_port,
                discharge_port) 
        values (cost_comp_upd_stg_sequence.nextval,
                I_defaulting_level,
                I_effective_date,
                NULL,
                I_comp_id,
                I_comp_type,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                I_origin_country_id,
                NULL,
                NULL,
                I_exp_prof_key,
                NULL,
                NULL,
                I_zone_group_id,
                I_zone_id,
                I_old_comp_rate,
                I_old_comp_currency,
                I_old_per_count,
                I_old_per_count_uom,
                I_new_comp_rate,
                I_new_comp_currency,
                I_new_per_count,
                I_new_per_count_uom,
                NVL(I_cntry_default_ind, 'N'),
                NVL(I_supp_default_ind,  'N'),
                NVL(I_ptnr_default_ind,  'N'),
                NVL(I_item_default_ind,  'N'),
                NVL(I_order_default_ind, 'N'),
                NVL(I_tsf_alloc_default_ind, 'N'),
                NVL(I_dept_default_ind,  'N'),
                I_lading_port,
                I_discharge_port);
      
      return TRUE;            
      
   end if;

   -- The following condition is satified when the function is called from Department Charge form.
   if I_defaulting_level = 'D' then
      
      -- Validate the component type
      if I_comp_type != 'U' then
         O_error_message := 'INV_PARAMS';
         return FALSE;
      end if;

      if I_dept is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_dept',
                                               'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                NULL);
         return FALSE;
      end if;
      
      if I_from_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_from_loc',
                                               'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                NULL);
         return FALSE;
      end if;
      
      if I_to_loc   is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_to_loc',
                                               'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                NULL);
         return FALSE;
      end if;
   
      merge into cost_comp_upd_stg ccus
      using  (select 'x' from dual) 
           on  (ccus.seq_no = I_seq_no)
      when matched then
      update
            set ccus.new_comp_rate         = I_new_comp_rate,
                ccus.new_comp_currency     = I_new_comp_currency, 
                ccus.new_per_count         = I_new_per_count, 
                ccus.new_per_count_uom     = I_new_per_count_uom,
                ccus.item_default_ind      = I_item_default_ind,
                ccus.tsf_alloc_default_ind = I_tsf_alloc_default_ind,
                ccus.effective_date        = I_effective_date
      when not matched then
        insert (seq_no,
                defaulting_level,
                effective_date,
                dept,
                comp_id,
                comp_type,
                supplier,
                item,
                item_exp_type,
                item_exp_seq,   
                hts,
                import_country_id,
                origin_country_id,      
                effect_from,
                effect_to,     
                exp_prof_key,   
                from_loc,       
                to_loc, 
                zone_group_id,
                zone_id,
                old_comp_rate,
                old_comp_currency,
                old_per_count,
                old_per_count_uom,
                new_comp_rate,
                new_comp_currency,
                new_per_count,
                new_per_count_uom,
                cntry_default_ind,
                supp_default_ind,
                ptnr_default_ind,
                item_default_ind,
                order_default_ind,
                tsf_alloc_default_ind,
                dept_default_ind) 
        values (cost_comp_upd_stg_sequence.nextval,
                I_defaulting_level,
                I_effective_date,
                I_dept,
                I_comp_id,
                I_comp_type,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                I_from_loc,
                I_to_loc,
                NULL,
                NULL,
                I_old_comp_rate,
                I_old_comp_currency,
                I_old_per_count,
                I_old_per_count_uom,
                I_new_comp_rate,
                I_new_comp_currency,
                I_new_per_count,
                I_new_per_count_uom,
                NVL(I_cntry_default_ind, 'N'),
                NVL(I_supp_default_ind,  'N'),
                NVL(I_ptnr_default_ind,  'N'),
                NVL(I_item_default_ind,  'N'),
                NVL(I_order_default_ind, 'N'),
                NVL(I_tsf_alloc_default_ind, 'N'),
                NVL(I_dept_default_ind,  'N'));      
      
      return TRUE;            
   end if;   
   
   -- The following condition is satisfied when the function is called from the forms Item expense, Item Charges or Item HTS Accessment.
   if I_defaulting_level in ('I') then
     if I_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_item',
                                               'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                NULL);
         return FALSE;
      end if;
      
      if I_comp_type = 'E' then
      -- this scenario is when the function is called from the Item expense form, which updates the Item expenses.

         if I_supplier      is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_supplier',
                                                  'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                   NULL);
         end if;
         
         if I_item_exp_type is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_item_exp_type',
                                                  'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                   NULL);
         end if;
         
         if I_item_exp_seq  is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_item_exp_seq',
                                                  'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                   NULL);
            return FALSE;
         end if;

         -- merge statement ensures that there are no duplicate rows for the unique key combination.
         merge into cost_comp_upd_stg ccus
            using (select 'x' from dual)
               on (ccus.comp_id          = I_comp_id
               and ccus.comp_type        = 'E'
               and ccus.item             = I_item
               and ccus.defaulting_level = 'I'
               and ccus.supplier         = I_supplier
               and ccus.item_exp_type    = I_item_exp_type
               and ccus.item_exp_seq     = I_item_exp_seq)
         when matched then
         update
               set ccus.new_comp_rate         = I_new_comp_rate,
                   ccus.new_comp_currency     = I_new_comp_currency, 
                   ccus.new_per_count         = I_new_per_count, 
                   ccus.new_per_count_uom     = I_new_per_count_uom,
                   ccus.order_default_ind     = I_order_default_ind,
                   ccus.effective_date        = I_effective_date
         when not matched then
           insert (seq_no,
                   defaulting_level,
                   effective_date,
                   dept,
                   comp_id,
                   comp_type,
                   supplier,
                   item,
                   item_exp_type,
                   item_exp_seq,   
                   hts,
                   import_country_id,      
                   origin_country_id,      
                   effect_from,
                   effect_to,     
                   exp_prof_key,   
                   from_loc,       
                   to_loc, 
                   zone_group_id,
                   zone_id,
                   old_comp_rate,
                   old_comp_currency,
                   old_per_count,
                   old_per_count_uom,
                   new_comp_rate,
                   new_comp_currency,
                   new_per_count,
                   new_per_count_uom,
                   cntry_default_ind,
                   supp_default_ind,
                   ptnr_default_ind,
                   item_default_ind,
                   order_default_ind,
                   tsf_alloc_default_ind,
                   dept_default_ind,
                   lading_port,
                   discharge_port) 
           values (cost_comp_upd_stg_sequence.nextval,
                   I_defaulting_level,
                   I_effective_date,
                   NULL,
                   I_comp_id,
                   I_comp_type,
                   I_supplier,
                   I_item,
                   I_item_exp_type,
                   I_item_exp_seq,
                   NULL,
                   NULL,
                   I_origin_country_id,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   I_zone_group_id,
                   I_zone_id,
                   I_old_comp_rate,
                   I_old_comp_currency,
                   I_old_per_count,
                   I_old_per_count_uom,
                   I_new_comp_rate,
                   I_new_comp_currency,
                   I_new_per_count,
                   I_new_per_count_uom,
                   NVL(I_cntry_default_ind, 'N'),
                   NVL(I_supp_default_ind,  'N'),
                   NVL(I_ptnr_default_ind,  'N'),
                   NVL(I_item_default_ind,  'N'),
                   NVL(I_order_default_ind, 'N'),
                   NVL(I_tsf_alloc_default_ind, 'N'),
                   NVL(I_dept_default_ind,  'N'),
                   I_lading_port,
                   I_discharge_port);
                   --
         return TRUE; 
      end if;  -- end component type check. 'E'
      
      if I_comp_type = 'U' then
      -- this scenario is when the function is called from the Item charge form, which updates the Items' charges.

         if I_from_loc      is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_from_loc',
                                                  'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                   NULL);
            return FALSE;
         end if;
         
         if I_to_loc        is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_to_loc',
                                                  'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                   NULL);
            return FALSE;
         end if;

         -- merge statement ensures that there are no duplicate rows for the unique key combination.
         merge into cost_comp_upd_stg ccus
         using (select 'x' from dual)
               on (ccus.comp_id          = I_comp_id
               and ccus.comp_type        = 'U'
               and ccus.item             = I_item
               and ccus.defaulting_level = 'I'
               and ccus.from_loc         = I_from_loc
               and ccus.to_loc           = I_to_loc )
         when matched then
         update
               set ccus.new_comp_rate         = I_new_comp_rate,
                   ccus.new_comp_currency     = I_new_comp_currency, 
                   ccus.new_per_count         = I_new_per_count, 
                   ccus.new_per_count_uom     = I_new_per_count_uom,
                   ccus.tsf_alloc_default_ind = I_tsf_alloc_default_ind,
                   ccus.effective_date        = I_effective_date
         when not matched then
           insert (seq_no,
                   defaulting_level,
                   effective_date,
                   dept,
                   comp_id,
                   comp_type,
                   supplier,
                   item,
                   item_exp_type,
                   item_exp_seq,   
                   hts,
                   import_country_id,      
                   origin_country_id,      
                   effect_from,
                   effect_to,     
                   exp_prof_key,   
                   from_loc,       
                   to_loc, 
                   zone_group_id,
                   zone_id,
                   old_comp_rate,
                   old_comp_currency,
                   old_per_count,
                   old_per_count_uom,
                   new_comp_rate,
                   new_comp_currency,
                   new_per_count,
                   new_per_count_uom,
                   cntry_default_ind,
                   supp_default_ind,
                   ptnr_default_ind,
                   item_default_ind,
                   order_default_ind,
                   tsf_alloc_default_ind,
                   dept_default_ind) 
           values (cost_comp_upd_stg_sequence.nextval,
                   I_defaulting_level,
                   I_effective_date,
                   NULL,
                   I_comp_id,
                   I_comp_type,
                   NULL,
                   I_item,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   I_from_loc,
                   I_to_loc,
                   NULL,
                   NULL,
                   I_old_comp_rate,
                   I_old_comp_currency,
                   I_old_per_count,
                   I_old_per_count_uom,
                   I_new_comp_rate,
                   I_new_comp_currency,
                   I_new_per_count,
                   I_new_per_count_uom,
                   NVL(I_cntry_default_ind, 'N'),
                   NVL(I_supp_default_ind,  'N'),
                   NVL(I_ptnr_default_ind,  'N'),
                   NVL(I_item_default_ind,  'N'),
                   NVL(I_order_default_ind, 'N'),
                   NVL(I_tsf_alloc_default_ind, 'N'),
                   NVL(I_dept_default_ind,  'N'));      

         return TRUE; 
      end if;  -- end component type check. 'U'

      if I_comp_type = 'A' then
      -- this scenario is when the function is called from the Item hts assessment form, which updates Assessments for an Item.
         -- Validate parameter values
         if I_hts is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_hts',
                                                  'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                   NULL);
            return FALSE;
         end if;
         
         if I_import_country_id is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_import_country_id',
                                                  'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                   NULL);
            return FALSE;
         end if;
         
         if I_origin_country_id is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_origin_country_id',
                                                  'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                   NULL);
            return FALSE;
         end if;
         
         if I_effect_from is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_effect_from',
                                                  'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                   NULL);
            return FALSE;
         end if;
         
         if I_effect_to is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_effect_to',
                                                  'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                                   NULL);
            return FALSE;
         end if;

         merge into cost_comp_upd_stg ccus
         using (select 'x' from dual)  
               on (ccus.comp_id          = I_comp_id
               and ccus.comp_type        = 'A'
               and ccus.item             = I_item
               and ccus.defaulting_level = 'I'
               and ccus.hts              = I_hts
               and ccus.import_country_id= I_import_country_id
               and ccus.origin_country_id= I_origin_country_id
               and ccus.effect_from      = I_effect_from
               and ccus.effect_to        = I_effect_to )
         when matched then
         update
               set ccus.new_comp_rate         = I_new_comp_rate,
                   ccus.new_per_count         = I_new_per_count, 
                   ccus.new_per_count_uom     = I_new_per_count_uom,
                   ccus.order_default_ind     = I_order_default_ind,
                   ccus.effective_date        = I_effective_date
         when not matched then
           insert (seq_no,
                   defaulting_level,
                   effective_date,
                   dept,
                   comp_id,
                   comp_type,
                   supplier,
                   item,
                   item_exp_type,
                   item_exp_seq,   
                   hts,
                   import_country_id,      
                   origin_country_id,      
                   effect_from,
                   effect_to,     
                   exp_prof_key,   
                   from_loc,       
                   to_loc, 
                   zone_group_id,
                   zone_id,
                   old_comp_rate,
                   old_comp_currency,
                   old_per_count,
                   old_per_count_uom,
                   new_comp_rate,
                   new_comp_currency,
                   new_per_count,
                   new_per_count_uom,
                   cntry_default_ind,
                   supp_default_ind,
                   ptnr_default_ind,
                   item_default_ind,
                   order_default_ind,
                   tsf_alloc_default_ind,
                   dept_default_ind) 
           values (cost_comp_upd_stg_sequence.nextval,
                   I_defaulting_level,
                   I_effective_date,
                   NULL,
                   I_comp_id,
                   I_comp_type,
                   NULL,
                   I_item,
                   NULL,
                   NULL,
                   I_hts,
                   I_import_country_id,
                   I_origin_country_id,
                   I_effect_from,
                   I_effect_to,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   I_old_comp_rate,
                   I_old_comp_currency,
                   I_old_per_count,
                   I_old_per_count_uom,
                   I_new_comp_rate,
                   I_new_comp_currency,
                   I_new_per_count,
                   I_new_per_count_uom,
                   NVL(I_cntry_default_ind, 'N'),
                   NVL(I_supp_default_ind,  'N'),
                   NVL(I_ptnr_default_ind,  'N'),
                   NVL(I_item_default_ind,  'N'),
                   NVL(I_order_default_ind, 'N'),
                   NVL(I_tsf_alloc_default_ind, 'N'),
                   NVL(I_dept_default_ind,  'N'));      

         return TRUE;
      end if;  -- end component type check. 'A'
   end if;  -- end defaulting level = 'I'
   --
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.SAVE_UPDATES',
                                            to_char(SQLCODE));
   RETURN FALSE;
   --
END SAVE_UPDATES;
--
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_RATES_EFFECTIVE_DATE
-- The function is called by a batch/shell script which is scheduled to run at the end of the day.
-- This function updates values on the base form (elccomp, itassess and itemchrg).
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_RATES_EFFECTIVE_DATE(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN
IS

BEGIN
   L_vdate := get_vdate(); 
   -- 
   -- Update elccomp records with the values entered in comp rate screen opened from elc. The following stmt updates the 
   -- component rates to the values updated from comp rate screen effective from the next day.
   merge into elc_comp elc
   using (select ccu.comp_id,
                 ccu.new_comp_rate,
                 ccu.new_comp_currency,
                 ccu.new_per_count,
                 ccu.new_per_count_uom
            from cost_comp_upd_stg ccu
           where ccu.defaulting_level = 'E'
             and ccu.effective_date   = L_next_vdate) ccus  
      on (elc.comp_id = ccus.comp_id)
   when matched then
   update
         set elc.comp_rate         = ccus.new_comp_rate,
             elc.comp_currency     = ccus.new_comp_currency,
             elc.per_count         = ccus.new_per_count,
             elc.per_count_uom     = ccus.new_per_count_uom;


   -- Update dept chrg detail records with the values entered in comp rate screen opened from dept chrg. The following stmt updates the 
   -- component rates to the values updated from comp rate screen effective from the next day.
   merge into dept_chrg_detail dcd
   using (select ccu.comp_id,
                 ccu.new_comp_rate,
                 ccu.new_comp_currency,
                 ccu.new_per_count,
                 ccu.new_per_count_uom,
                 ccu.dept,
                 ccu.from_loc,
                 ccu.to_loc
            from cost_comp_upd_stg ccu
           where ccu.defaulting_level = 'D'
             and ccu.effective_date   = L_next_vdate) ccus  
      on (dcd.comp_id  = ccus.comp_id
      and dcd.dept     = ccus.dept
      and dcd.from_loc = ccus.from_loc
      and dcd.to_loc   = ccus.to_loc)
   when matched then
   update
         set dcd.comp_rate         = ccus.new_comp_rate,
             dcd.comp_currency     = ccus.new_comp_currency,
             dcd.per_count         = ccus.new_per_count,
             dcd.per_count_uom     = ccus.new_per_count_uom;


   -- Update exp profile detail records with the values entered in comp rate screen opened from expprof screen. The following stmt updates 
   -- the component rates to the values updated from comp rate screen effective from the next day.
   merge into exp_prof_detail epd
   using (select ccu.comp_id,
                 ccu.new_comp_rate,
                 ccu.new_comp_currency,
                 ccu.new_per_count,
                 ccu.new_per_count_uom,
                 ccu.exp_prof_key
            from cost_comp_upd_stg ccu
           where ccu.defaulting_level in ('S', 'P', 'C')
             and ccu.effective_date   = L_next_vdate) ccus
      on (epd.comp_id      = ccus.comp_id
      and epd.exp_prof_key = ccus.exp_prof_key)
   when matched then
   update
         set epd.comp_rate         = ccus.new_comp_rate,
             epd.comp_currency     = ccus.new_comp_currency,
             epd.per_count         = ccus.new_per_count,
             epd.per_count_uom     = ccus.new_per_count_uom;
return TRUE;

EXCEPTION
   when OTHERS then
  
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.UPDATE_RATES_EFFECTIVE_DATE',
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_RATES_EFFECTIVE_DATE;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_EXP_PROF_DETAIL
-- Purpose: The function is called by the function PROCESS_COST_COMP_UPDATES. This function 
--          selects rows from the cost_comp_upd_stg which have the defaulting level Elc and 
--          component type as Expense. It updates the expense profile detail table with the 
--          new component rate, currency and per count, UOM values.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_EXP_PROF_DETAIL(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN
IS

BEGIN

  -- Bulk insert into the global temporary table, select records from the staging
  insert into gtt_cost_comp_upd
  (       row_id,
          exp_prof_key,
          comp_id,
          curr_comp_rate,
          curr_comp_currency,
          curr_per_count,     
          curr_per_count_uom,
          old_comp_rate,      
          old_comp_currency,
          old_per_count,
          old_per_count_uom,
          new_comp_rate,
          new_comp_currency,
          new_per_count,
          new_per_count_uom)
  (select epd.rowid,
          epd.exp_prof_key,
          epd.comp_id,
          epd.comp_rate     curr_comp_rate,
          epd.comp_currency curr_comp_currency,
          epd.per_count     curr_per_count,
          epd.per_count_uom curr_per_count_uom,
          ccu.old_comp_rate,
          ccu.old_comp_currency,
          ccu.old_per_count,
          ccu.old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_comp_currency,
          ccu.new_per_count,
          ccu.new_per_count_uom
     from exp_prof_detail   epd,
          cost_comp_upd_stg ccu,
          exp_prof_head     eph
    where epd.comp_id          =  ccu.comp_id
      and eph.exp_prof_key     =  epd.exp_prof_key
      and ccu.defaulting_level = 'E'
      and ccu.comp_type        = 'E'
      and ((ccu.supp_default_ind   = 'Y' and eph.module = 'SUPP')
        or (ccu.ptnr_default_ind   = 'Y' and eph.module = 'PTNR')
        or (ccu.cntry_default_ind  = 'Y' and eph.module = 'CTRY'))
      and ccu.effective_date = L_next_vdate);
  
   insert into cost_comp_exc_log
   (        exception_type,
            exp_prof_key,
            comp_id,
            old_comp_rate,
            old_comp_currency,
            old_per_count,
            old_per_count_uom,
            new_comp_rate,    
            new_comp_currency,
            new_per_count,    
            new_per_count_uom,
            create_id,
            create_datetime)
   ( select 'EP',
            exp_prof_key,
            comp_id,
            curr_comp_rate,
            curr_comp_currency,
            curr_per_count,
            curr_per_count_uom,
            new_comp_rate,    
            new_comp_currency,
            new_per_count,    
            new_per_count_uom,
            user,       
            sysdate
     from   gtt_cost_comp_upd gtt
     where  gtt.old_comp_rate               != gtt.curr_comp_rate
       or   gtt.old_comp_currency           != gtt.curr_comp_currency
       or   NVL(gtt.old_per_count, -999)    != NVL(gtt.curr_per_count, -999)
       or   NVL(gtt.old_per_count_uom, -999)!= NVL(gtt.curr_per_count_uom, -999));

   -- Updating the expense profile detail table, with the new component rate, currency and per count, UOM values.
   merge into exp_prof_detail epd
   using (select row_id,
                 new_comp_rate,
                 new_comp_currency,
                 new_per_count,
                 new_per_count_uom
            from gtt_cost_comp_upd ) gtt  
      ON (epd.rowid = gtt.row_id)
   when matched then
   update
         set epd.comp_rate         = gtt.new_comp_rate,
             epd.comp_currency     = gtt.new_comp_currency,
             epd.per_count         = gtt.new_per_count,
             epd.per_count_uom     = gtt.new_per_count_uom;

   -- delete the contents of the global temporary table.
   delete gtt_cost_comp_upd;   
   return TRUE;          

EXCEPTION
   when OTHERS then
      -- delete the contents of the global temporary table.
      delete gtt_cost_comp_upd;   
   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.UPDATE_EXP_PROF_DETAIL',
                                            to_char(SQLCODE));
      return FALSE;
        
END UPDATE_EXP_PROF_DETAIL;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_EXP_RECAL_ASSMT
-- Purpose: The function is for internal use and is called by the function UPDATE_ORDLOC_EXP. This function 
--          updates the ordloc exp component values. It also calls ELC_CALC_SQL.CALC_COMP to 
--          recalculate the order expenses depending on the upd_assess_ind it updates the associated
--          assessments as well, 
--    Note: This is a private function.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_EXP_RECAL_ASSMT (O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_ordloc_exp_log_tbl       IN OUT orders_TBL,
                                 O_log_tbl_cnt              IN OUT NUMBER,
                                 O_mod_order_item_hts_tbl   IN OUT mod_order_item_hts_TBL,
                                 O_mod_order_tbl_cnt        IN OUT NUMBER,
                                 O_rev_orders_tbl           IN OUT rev_ord_TBL,
                                 O_rev_ord_tbl_cnt          IN OUT NUMBER,
                                 I_row_id                   IN ROWID,
                                 I_item                     IN ITEM_MASTER.ITEM%TYPE,
                                 I_supplier                 IN SUPS.SUPPLIER%TYPE,
                                 I_order_no                 IN ORDHEAD.ORDER_NO%TYPE,
                                 I_ord_stat                 IN ORDHEAD.STATUS%TYPE,
                                 I_pack_item                IN ORDLOC_EXP.PACK_ITEM%TYPE,
                                 I_location                 IN ORDLOC_EXP.LOCATION%TYPE,
                                 I_comp_id                  IN ELC_COMP.COMP_ID%TYPE,
                                 I_orig_country_id          IN ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                 I_curr_comp_rate           IN ORDLOC_EXP.COMP_RATE%TYPE,
                                 I_curr_comp_currency       IN ORDLOC_EXP.COMP_CURRENCY%TYPE,
                                 I_curr_per_count           IN ORDLOC_EXP.PER_COUNT%TYPE,
                                 I_curr_per_count_uom       IN ORDLOC_EXP.PER_COUNT_UOM%TYPE,
                                 I_old_comp_rate            IN ORDLOC_EXP.COMP_RATE%TYPE,
                                 I_old_comp_currency        IN ORDLOC_EXP.COMP_CURRENCY%TYPE,
                                 I_old_per_count            IN ORDLOC_EXP.PER_COUNT%TYPE,
                                 I_old_per_count_uom        IN ORDLOC_EXP.PER_COUNT_UOM%TYPE,
                                 I_exchange_rate            IN ORDLOC_EXP.EXCHANGE_RATE%TYPE,
                                 I_new_comp_rate            IN ORDLOC_EXP.COMP_RATE%TYPE,
                                 I_new_comp_currency        IN ORDLOC_EXP.COMP_CURRENCY%TYPE,
                                 I_new_per_count            IN ORDLOC_EXP.PER_COUNT%TYPE,
                                 I_new_per_count_uom        IN ORDLOC_EXP.PER_COUNT_UOM%TYPE)
RETURN BOOLEAN
IS
   cursor C_ord_assessments(I_order_no ORDHEAD.ORDER_NO%TYPE, I_item ITEM_MASTER.ITEM%TYPE, I_comp_id ELC_COMP.COMP_ID%TYPE)
   is
   select distinct
          oh.order_no,
          oh.item,
          oha.nom_flag_2,
          oha.cvb_code,
          oh.hts,
          oh.import_country_id,
          oh.effect_from,
          oh.effect_to
     from ordsku_hts        oh,
          ordsku_hts_assess oha,
          cvb_detail        cd
    where oh.order_no  = I_order_no
      and oh.item      = I_item
      and oh.effect_from <= L_vdate
      and oh.effect_to   >= L_vdate
      and oha.order_no = oh.order_no
      and oha.seq_no   = oh.seq_no
      and cd.cvb_code  = oha.cvb_code
      and cd.comp_id   = I_comp_id
    order by order_no, item; 
    
    L_ord_in_rev_tbl  VARCHAR2(1);

BEGIN
   --
   if I_curr_comp_rate     != I_old_comp_rate or
      I_curr_comp_currency != I_old_comp_currency or
      NVL(I_curr_per_count, -999)     != NVL(I_old_per_count, -999) or
      NVL(I_curr_per_count_uom, -999) != NVL(I_old_per_count_uom, -999) then

      O_ordloc_exp_log_tbl(O_log_tbl_cnt).order_no          := I_order_no;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).item              := I_item;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).pack_item         := I_pack_item;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).comp_id           := I_comp_id;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).location          := I_location;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).old_comp_rate     := I_curr_comp_rate;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).old_comp_currency := I_curr_comp_currency;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).old_per_count     := I_curr_per_count;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).old_per_count_uom := I_curr_per_count_uom;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).new_comp_rate     := I_new_comp_rate;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).new_comp_currency := I_new_comp_currency;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).new_per_count     := I_new_per_count;     
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).new_per_count_uom := I_new_per_count_uom; 
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).reason_code       := 'ORDU';
      O_log_tbl_cnt := O_log_tbl_cnt +1;

   end if;
   --
   -- Update the perticular Ordloc expense.
   update ordloc_exp oe
      set oe.comp_rate         = I_new_comp_rate,
          oe.comp_currency     = I_new_comp_currency,
          oe.per_count         = I_new_per_count,
          oe.per_count_uom     = I_new_per_count_uom,
          oe.exchange_rate     = decode(oe.comp_currency, I_new_comp_currency, oe.exchange_rate, I_exchange_rate)
    where oe.rowid = I_row_id;
   --
   
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PE',
                             I_item, -- item
                             I_supplier,
                             NULL, -- item_exp_type
                             NULL, -- item_exp_seq
                             I_order_no,
                             NULL, -- ord_seq_no
                             I_pack_item, -- pack_item
                             NULL, -- zone_id
                             I_location,
                             NULL, -- hts
                             NULL, -- import_country_id
                             I_orig_country_id,
                             NULL, -- effect_from
                             NULL) = FALSE then -- effect_to
      return FALSE;
   end if;
   --
   -- updating all the related assessments
   for assmnt in C_ord_assessments(I_order_no, I_item, I_comp_id) loop
      if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                'PA',
                                I_item, -- item
                                NULL,
                                NULL, -- item_exp_type
                                NULL, -- item_exp_seq
                                I_order_no,
                                NULL, -- ord_seq_no
                                I_pack_item, -- pack_item
                                NULL, -- zone_id
                                NULL,
                                assmnt.hts, -- hts
                                assmnt.import_country_id, -- import_country_id
                                I_orig_country_id,
                                assmnt.effect_from, -- effect_from
                                assmnt.effect_to) = FALSE then -- effect_to
         return FALSE;
      end if;

      O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).order_no            := I_order_no;
      O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).item                := I_item;
      O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).import_country_id   := assmnt.import_country_id;
      O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).hts                 := assmnt.hts;
      O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).effect_from         := assmnt.effect_from;
      O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).effect_to           := assmnt.effect_to;
      O_mod_order_tbl_cnt := O_mod_order_tbl_cnt+1;
      --
      
   end loop;
   --
   -- Make an entry into the rev table, if it is not present in db table of the plsql table.
   L_ord_in_rev_tbl := 'N';
   for cnt in 1..O_rev_ord_tbl_cnt -1 loop
      if O_rev_orders_tbl(cnt).order_no = I_order_no then
         L_ord_in_rev_tbl := 'Y';
         EXIT;
      end if;
   end loop;
   --
   if L_ord_in_rev_tbl = 'N' then
      O_rev_orders_tbl(O_rev_ord_tbl_cnt).order_no := I_order_no;
      O_rev_ord_tbl_cnt := O_rev_ord_tbl_cnt + 1;
   end if;
   --
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.UPDATE_EXP_RECAL_ASSMT',
                                            to_char(SQLCODE));
      return FALSE;       
END UPDATE_EXP_RECAL_ASSMT;


---------------------------------------------------------------------------------------------
-- Function Name: FLUSH_CHANGES_EXPENSES
-- Purpose: The function is called by the function UPDATE_ORDLOC_EXP. This function 
--          copies all the data from the plsql tables sent as parameters to the respective tables.
--          The orders which were updated manually are inserted into the table ordloc_exp_exc_log
--          When an approved order is updated or modified are stored into the table rev_orders.
---------------------------------------------------------------------------------------------
FUNCTION FLUSH_CHANGES_EXPENSES(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,                
                                I_ordloc_exp_log_tbl     IN      orders_TBL,
                                I_rev_orders_tbl         IN      rev_ord_TBL,
                                I_mod_order_item_hts_tbl IN      mod_order_item_hts_TBL)
RETURN BOOLEAN
IS
BEGIN
   for rec in 1..I_ordloc_exp_log_tbl.COUNT loop
      insert into cost_comp_exc_log
        (exception_type,
         order_no,               
         item,                   
         pack_item,
         location,
         comp_id,                
         old_comp_rate,          
         old_comp_currency,      
         old_per_count,          
         old_per_count_uom,      
         new_comp_rate,          
         new_comp_currency,      
         new_per_count,          
         new_per_count_uom,      
         create_id,              
         create_datetime,        
         reason_code)
        values
        ('OE',
         I_ordloc_exp_log_tbl(rec).order_no,          
         I_ordloc_exp_log_tbl(rec).item,              
         I_ordloc_exp_log_tbl(rec).pack_item,
         I_ordloc_exp_log_tbl(rec).location,
         I_ordloc_exp_log_tbl(rec).comp_id,           
         I_ordloc_exp_log_tbl(rec).old_comp_rate,     
         I_ordloc_exp_log_tbl(rec).old_comp_currency, 
         I_ordloc_exp_log_tbl(rec).old_per_count,     
         I_ordloc_exp_log_tbl(rec).old_per_count_uom, 
         I_ordloc_exp_log_tbl(rec).new_comp_rate,     
         I_ordloc_exp_log_tbl(rec).new_comp_currency, 
         I_ordloc_exp_log_tbl(rec).new_per_count,     
         I_ordloc_exp_log_tbl(rec).new_per_count_uom, 
         user,
         sysdate,
         I_ordloc_exp_log_tbl(rec).reason_code);
   end loop;
   --
   --
   for rec in 1..I_rev_orders_tbl.COUNT loop
      merge into rev_orders ro
      using (select 'x' from dual) 
      on    (ro.order_no = I_rev_orders_tbl(rec).order_no) 
      when not matched then
         insert (order_no)
         values (I_rev_orders_tbl(rec).order_no); 
   end loop;
   --
   --
   for rec in 1..I_mod_order_item_hts_tbl.COUNT loop
      
      -- insert the record if the records do not exist
      merge into mod_order_item_hts moi
      using (select 'x' from dual)
         on (moi.order_no          = I_mod_order_item_hts_tbl(rec).order_no
         and moi.item              = I_mod_order_item_hts_tbl(rec).item
         and moi.import_country_id = I_mod_order_item_hts_tbl(rec).import_country_id
         and moi.hts               = I_mod_order_item_hts_tbl(rec).hts
         and moi.effect_from       = I_mod_order_item_hts_tbl(rec).effect_from
         and moi.effect_to         = I_mod_order_item_hts_tbl(rec).effect_to
         and moi.unapprove_ind     = 'N'
         and moi.pgm_name          = 'batch_ordcostcompupd'
         and to_char(moi.updated_datetime,'DD-MON-YY')  = to_char(L_vdate,'DD-MON-YY'))
      when not matched then
       insert (order_no,               
               item,                   
               import_country_id,               
               hts,          
               effect_from,
               effect_to,      
               unapprove_ind,
               pgm_name,
               updated_datetime)
              values
              (I_mod_order_item_hts_tbl(rec).order_no,          
               I_mod_order_item_hts_tbl(rec).item,              
               I_mod_order_item_hts_tbl(rec).import_country_id,
               I_mod_order_item_hts_tbl(rec).hts,     
               I_mod_order_item_hts_tbl(rec).effect_from,     
               I_mod_order_item_hts_tbl(rec).effect_to, 
               'N',
               'batch_ordcostcompupd',
               L_vdate);

   end loop;
   
   return TRUE;
   --
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.FLUSH_CHANGES_EXPENSES',
                                            to_char(SQLCODE));
      return FALSE;
END FLUSH_CHANGES_EXPENSES;


---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ORDLOC_EXP
-- Purpose: The function is called by the function PROCESS_COST_COMP_UPDATES. This function 
--          selects rows from the cost_comp_upd_stg which have the defaulting levels as 'I'tem,
--          'E'lc, 'S'upplier, 'C'ountry or 'P'artner. It updates the Order expenses and associated
--          assessments.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC_EXP(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_Thread_no     IN      RESTART_CONTROL.NUM_THREADS%TYPE,
                           I_Threads       IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN
IS
   
   L_ordloc_exp_log_tbl                   orders_TBL;  -- to hold the orders which are shipped or updated
   
   L_rev_orders_tbl                       rev_ord_TBL; -- holds the orders that need to be inserted into the REV Table.   
   L_mod_order_item_hts_tbl               mod_order_item_hts_TBL; -- holds the assessments which were recalculated.
   
   -- Cursor used to loop through the records of the GTT. 
   cursor C_ord_items
   is 
   select gtt.row_id,
          gtt.order_no,
          gtt.item,
          gtt.supplier,
          gtt.origin_country_id,
          gtt.pack_item,
          gtt.location,
          gtt.comp_id,
          gtt.curr_comp_rate,
          gtt.curr_comp_currency,
          gtt.curr_per_count,
          gtt.curr_per_count_uom,
          gtt.old_comp_rate,
          gtt.old_comp_currency,
          gtt.old_per_count,
          gtt.old_per_count_uom,
          gtt.new_comp_rate,
          gtt.new_comp_currency,
          gtt.new_per_count,
          gtt.new_per_count_uom,
          gtt.shipped_ind,
          gtt.order_status,
          gtt.nom_flag_2,
          gtt.cvb_code
     from gtt_cost_comp_upd gtt
     order by order_no, item;

   -- cursor used to fetch all the assessments which are needed to be updated due to the change in the expense.
   cursor C_ord_assessments(I_order_no ORDHEAD.ORDER_NO%TYPE, I_item ITEM_MASTER.ITEM%TYPE, I_comp_id ELC_COMP.COMP_ID%TYPE)
   is
   select oh.order_no,
          oh.item,
          oha.nom_flag_2,
          oha.cvb_code,
          oh.hts,
          oh.import_country_id,
          oh.effect_from,
          oh.effect_to
     from ordsku_hts        oh,
          ordsku_hts_assess oha,
          cvb_detail        cd
    where oh.order_no  = I_order_no
      and oh.item      = I_item
      and oh.effect_from <= L_vdate
      and oh.effect_to   >= L_vdate
      and oha.order_no = oh.order_no
      and oha.seq_no   = oh.seq_no
      and cd.cvb_code  = oha.cvb_code
      and cd.comp_id   = I_comp_id
    order by order_no, item, oha.seq_no;
   L_ord_assessments  C_ord_assessments%ROWTYPE;
   
   -- cursor to fetch all the hts codes associated to the order.
   cursor C_ord_hts(I_order_no ORDHEAD.ORDER_NO%TYPE, I_item ITEM_MASTER.ITEM%TYPE)
   is
   select oh.hts,
          oh.import_country_id,
          oh.effect_from,
          oh.effect_to
     from ordsku_hts oh
    where oh.order_no     = I_order_no
      and oh.item         = I_item
      and oh.effect_from <= L_vdate
      and oh.effect_to   >= L_vdate;
     

   L_prev_ord           ORDHEAD.ORDER_NO%TYPE := NULL;
   L_prevord_status     VARCHAR2(1)           := NULL;
   L_log_tbl_cnt        NUMBER := 1;
   L_rev_ord_tbl_cnt    NUMBER := 1;
   L_mod_order_tbl_cnt  NUMBER := 1;

   -- Cursor to fetch the custom entries.
   cursor C_custom_entry(I_ord_no ORDHEAD.ORDER_NO%TYPE, I_item ITEM_MASTER.ITEM%TYPE)
   is
   select distinct ceh.ce_id,
          coi.vessel_id,
          coi.voyage_flt_id,
          coi.estimated_depart_date,
          NVL(coi.manifest_item_qty, 0) manifest_item_qty,
          NVL(coi.manifest_item_qty_uom, 0) manifest_item_qty_uom,
          ceh.status,
          oh.supplier
     from ce_ord_item coi, 
          ce_head ceh, 
          ordhead oh
    where oh.order_no  = I_ord_no
      and coi.order_no = oh.order_no
      and coi.item     = I_item
      and ceh.ce_id    = coi.ce_id
    order by ceh.status desc;

   L_custom_entry C_custom_entry%ROWTYPE;
   
   -- Cursor to get the total ordered quantity
   cursor C_ord_qty(I_order ORDLOC.ORDER_NO%TYPE, I_item ORDLOC.ITEM%TYPE)
   is 
   select SUM(ol.qty_ordered)
     from ordloc ol
    where ol.order_no = I_order
      and ol.item     = I_item;

   L_error_message         RTK_ERRORS.RTK_KEY%TYPE;
   L_standard_uom          UOM_CLASS.UOM%TYPE;
   L_standard_class        UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor           ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_total_qty             NUMBER;
   L_std_manifest_item_qty NUMBER;
   L_ordered_qty           NUMBER;
   L_ord_in_rev_tbl        VARCHAR2(1);
   L_exchange_rate         ORDLOC_EXP.EXCHANGE_RATE%TYPE;
   L_written_date          DATE;
   --
BEGIN
   
   -- Populate the global temporary table with the selected order item records.
   insert into gtt_cost_comp_upd
   (      row_id,
          order_no,
          item,
          supplier,
          origin_country_id,
          pack_item,
          location,
          comp_id,
          curr_comp_rate,
          curr_comp_currency,
          curr_per_count,
          curr_per_count_uom,
          old_comp_rate,
          old_comp_currency,
          old_per_count,
          old_per_count_uom,
          new_comp_rate,
          new_comp_currency,
          new_per_count,
          new_per_count_uom,
          order_status,
          nom_flag_2,
          cvb_code,
          shipped_ind)
   with ordloc_exp1 as
   -- This select query gets the data defaulted from Item expense form and defaulted to the order expense.
     (select oe.rowid row_id,
             oe.order_no,
             oe.item,
             oh.supplier,
             os.origin_country_id,
             oe.pack_item,
             oe.location,
             oe.comp_id,
             oe.comp_rate            curr_comp_rate,
             oe.comp_currency        curr_comp_currency,
             oe.per_count            curr_per_count,
             oe.per_count_uom        curr_per_count_uom,
             ccu.old_comp_rate,
             ccu.old_comp_currency,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_comp_currency,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oe.nom_flag_2,
             oe.cvb_code
        from cost_comp_upd_stg ccu,
             ordhead           oh, 
             ordsku            os, 
             ordloc_exp        oe
       where ccu.defaulting_level  = 'I'
         and ccu.comp_type         = 'E'
         and ccu.order_default_ind = 'Y'
         and ccu.comp_id  = oe.comp_id
         and ccu.supplier = oh.supplier
         and ccu.item     = oe.item
         and oh.order_no  = oe.order_no
         and oh.status in ('W', 'S', 'A')
         and os.order_no  = oe.order_no
         and os.item      = oe.item
         and ((os.origin_country_id = ccu.origin_country_id 
              and NVL(oh.lading_port,'-999') = NVL(ccu.lading_port,'-999')
              and NVL(oh.discharge_port,'-999') = NVL(ccu.discharge_port,'-999'))
              or exists (select 'x' 
                           from cost_zone_group_loc cz 
                          where cz.zone_group_id = ccu.zone_group_id
                            and cz.zone_id       = ccu.zone_id
                            and cz.location      = oe.location)) 
         and ccu.effective_date   = L_next_vdate
         and MOD(oe.order_no, I_Threads)+1 = I_Thread_no),
    
   ordloc_exp1a as -- queries only the shipped order from the data set ordloc_exp1
     (select oe.*,
               'Y'  -- shipped indicator.
          from ordloc_exp1 oe
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.order_no    = oe.order_no
                  and ss.item        = oe.item
                  and rownum =1)),

   ordloc_exp2 as
     -- the following query gets the data defaulted from Expense profile form when the updated is done at the Supplier level.
     (select oe.rowid row_id,
             oe.order_no,
             oe.item,
             oh.supplier,
             os.origin_country_id,
             oe.pack_item,
             oe.location,
             oe.comp_id,
             oe.comp_rate            curr_comp_rate,
             oe.comp_currency        curr_comp_currency,
             oe.per_count            curr_per_count,
             oe.per_count_uom        curr_per_count_uom,
             ccu.old_comp_rate,
             ccu.old_comp_currency,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_comp_currency,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oe.nom_flag_2,
             oe.cvb_code
        from cost_comp_upd_stg ccu, 
             exp_prof_head     eph,
             ordloc_exp oe, 
             ordhead    oh, 
             ordsku     os
       where ccu.defaulting_level  = 'S'
         and ccu.comp_type         = 'E'
         and ccu.order_default_ind = 'Y'
         and ccu.exp_prof_key      = eph.exp_prof_key
         and ccu.comp_id = oe.comp_id
         and oh.order_no = oe.order_no
         and oh.status in ('W', 'S', 'A')
         and oe.defaulted_from = 'S'
         and oe.key_value_1 = eph.key_value_1
         and ((os.origin_country_id = ccu.origin_country_id 
              and NVL(oh.lading_port,'-999') = NVL(ccu.lading_port,'-999')
              and NVL(oh.discharge_port,'-999') = NVL(ccu.discharge_port,'-999'))
              or exists (select 'x' 
                           from cost_zone_group_loc cz 
                          where cz.zone_group_id = ccu.zone_group_id
                            and cz.zone_id       = ccu.zone_id
                            and cz.location      = oe.location)) 
         and os.order_no = oe.order_no
         and os.item     = oe.item
         and eph.module     = 'SUPP'
         and ccu.effective_date   = L_next_vdate
         and not exists (select 'x'    -- check that the same order exp may not be updated from an update done at Item exp and defaulted to Order.
                           from cost_comp_upd_stg ccu2
                          where ccu2.defaulting_level  = 'I'
                            and ccu2.comp_type         = 'E'
                            and ccu2.order_default_ind = 'Y'
                            and ccu2.comp_id  = ccu.comp_id
                            and ccu2.supplier = oh.supplier
                            and ccu2.item     = os.item
                            and ccu2.effective_date   = L_next_vdate
                            and ((os.origin_country_id = ccu2.origin_country_id 
                                 and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                 and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999'))
                               or exists (select 'x' 
                                            from cost_zone_group_loc cz 
                                           where cz.zone_group_id = ccu2.zone_group_id
                                             and cz.zone_id       = ccu2.zone_id
                                             and cz.location      = oe.location)))
         and MOD(oe.order_no, I_Threads)+1 = I_Thread_no),

   ordloc_exp2a as -- queries order expense records with shipped orders.
     (select oe.*,
               'Y' -- shipped indicator
          from ordloc_exp2 oe
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.order_no    = oe.order_no
                  and ss.item        = oe.item
                  and rownum =1)),
     
   ordloc_exp3 as
     -- the following query gets the data defaulted from Expense profile form when the updated is done at the Partner level.   
     (select oe.rowid row_id,
             oe.order_no,
             oe.item,
             oh.supplier,
             os.origin_country_id,
             oe.pack_item,
             oe.location,
             oe.comp_id,
             oe.comp_rate            curr_comp_rate,
             oe.comp_currency        curr_comp_currency,
             oe.per_count            curr_per_count,
             oe.per_count_uom        curr_per_count_uom,
             ccu.old_comp_rate,
             ccu.old_comp_currency,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_comp_currency,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oe.nom_flag_2,
             oe.cvb_code        
        from exp_prof_head     eph, 
             cost_comp_upd_stg ccu, 
             ordloc_exp oe,
             ordhead    oh,
             ordsku     os
       where ccu.defaulting_level  = 'P'
         and ccu.comp_type         = 'E'
         and ccu.order_default_ind = 'Y'
         and ccu.exp_prof_key      = eph.exp_prof_key
         and ccu.comp_id           = oe.comp_id
         and oh.order_no    = oe.order_no
         and oh.status in ('W','S','A')
         and oe.defaulted_from = 'P'
         and oe.key_value_1 = eph.key_value_1
         and oe.key_value_2 = eph.key_value_2
         and ((os.origin_country_id = ccu.origin_country_id 
              and NVL(oh.lading_port,'-999') = NVL(ccu.lading_port,'-999')
              and NVL(oh.discharge_port,'-999') = NVL(ccu.discharge_port,'-999'))
              or exists (select 'x' 
                           from cost_zone_group_loc cz 
                          where cz.zone_group_id = ccu.zone_group_id
                            and cz.zone_id       = ccu.zone_id
                            and cz.location      = oe.location)) 
         and os.order_no = oe.order_no
         and os.item     = oe.item
         and eph.module  = 'PTNR'
         and not exists (select 'x'     -- check that the same order exp could not be updated from an update done at Item exp and defaulted to Order.
                           from cost_comp_upd_stg ccu2
                          where ccu2.defaulting_level  = 'I'
                            and ccu2.comp_type         = 'E'
                            and ccu2.order_default_ind = 'Y'
                            and ccu2.comp_id  = ccu.comp_id
                            and ccu2.supplier = oh.supplier
                            and ccu2.item     = os.item
                            and ccu2.effective_date   = L_next_vdate
                            and ((os.origin_country_id = ccu2.origin_country_id
                                 and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                 and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999'))
                                 or exists (select 'x' 
                                              from cost_zone_group_loc cz 
                                             where cz.zone_group_id = ccu2.zone_group_id
                                               and cz.zone_id       = ccu2.zone_id
                                               and cz.location      = oe.location)))
         and ccu.effective_date   = L_next_vdate                                      
         and mod(oe.order_no, I_Threads)+1 = I_Thread_no),

   ordloc_exp3a as
     (select oe.*,
               'Y'
          from ordloc_exp3 oe
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.order_no    = oe.order_no
                  and ss.item        = oe.item
                  and rownum =1)),
   
   ordloc_exp4 as
   -- the following query gets the data which was defaulted at the expense profile country level.
     (select oe.rowid row_id,
             oe.order_no,
             oe.item,
             oh.supplier,
             os.origin_country_id,
             oe.pack_item,
             oe.location,
             oe.comp_id,
             oe.comp_rate           curr_comp_rate,
             oe.comp_currency       curr_comp_currency,
             oe.per_count           curr_per_count,
             oe.per_count_uom       curr_per_count_uom,
             ccu.old_comp_rate,
             ccu.old_comp_currency,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_comp_currency,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oe.nom_flag_2,
             oe.cvb_code
        from cost_comp_upd_stg ccu, 
             exp_prof_head     eph, 
             ordloc_exp oe, 
             ordhead oh, 
             ordsku  os
       where ccu.defaulting_level  = 'C'
         and ccu.comp_type         = 'E'
         and ccu.order_default_ind = 'Y'
         and ccu.comp_id           = oe.comp_id
         and ccu.exp_prof_key      = eph.exp_prof_key      
         and oe.key_value_1        = ccu.origin_country_id
         and NVL(oh.lading_port,'-999') = NVL(ccu.lading_port,'-999')
         and NVL(oh.discharge_port,'-999') = NVL(ccu.discharge_port,'-999')
         and oh.order_no = oe.order_no
         and oh.status in ('W','S','A')
         and oe.defaulted_from = 'C'
         and os.order_no = oe.order_no
         and os.item     = oe.item
         and eph.module  = 'CTRY'
         and not exists (select 'x'     -- check that the same order exp could not be updated from an update done at Item exp and defaulted to Order.
                           from cost_comp_upd_stg ccu2
                          where ccu2.defaulting_level  = 'I'
                            and ccu2.comp_type         = 'E'
                            and ccu2.order_default_ind = 'Y'
                            and ccu2.comp_id  = ccu.comp_id
                            and ccu2.supplier = oh.supplier
                            and ccu2.item     = os.item
                            and ccu2.effective_date   = L_next_vdate
                            and ((os.origin_country_id = ccu2.origin_country_id
                                 and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                 and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999'))
                                 or exists (select 'x' 
                                              from cost_zone_group_loc cz 
                                             where cz.zone_group_id = ccu2.zone_group_id
                                               and cz.zone_id       = ccu2.zone_id
                                               and cz.location      = oe.location)))
         and ccu.effective_date   = L_next_vdate                                      
         and mod(oe.order_no, I_Threads)+1 = I_Thread_no),
      
   ordloc_exp4a as
     (select oe.*,
               'Y'
          from ordloc_exp4 oe
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.order_no    = oe.order_no
                  and ss.item        = oe.item
                  and rownum =1)),

   ordloc_exp5 as
     -- the following query gets the data from Expense profile form when the updated is done at the ELC level.      
     (select oe.rowid row_id,
             oe.order_no,
             oe.item,
             oh.supplier,
             os.origin_country_id,
             oe.pack_item,
             oe.location,
             oe.comp_id,
             oe.comp_rate           curr_comp_rate,
             oe.comp_currency       curr_comp_currency,
             oe.per_count           curr_per_count,
             oe.per_count_uom       curr_per_count_uom,
             ccu.old_comp_rate,
             ccu.old_comp_currency,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_comp_currency,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oe.nom_flag_2,
             oe.cvb_code
        from cost_comp_upd_stg ccu, 
             ordhead  oh, 
             ordsku   os, 
             ordloc_exp oe
       where ccu.defaulting_level  = 'E'
         and ccu.comp_type         = 'E'
         and ccu.order_default_ind = 'Y'
         and ccu.comp_id = oe.comp_id
         and oh.order_no = oe.order_no
         and oh.status in ('W', 'S', 'A')
         and os.order_no = oe.order_no
         and os.item     = oe.item
         and not exists ( select 'x'     -- check that the same order exp could not be updated from an update done at Item exp and defaulted to Order.
                            from cost_comp_upd_stg ccu2
                           where ccu2.defaulting_level  = 'I'
                             and ccu2.comp_type         = 'E'
                             and ccu2.order_default_ind = 'Y'
                             and ccu2.comp_id  = ccu.comp_id
                             and ccu2.supplier = oh.supplier
                             and ccu2.item     = os.item
                             and ccu2.effective_date   = L_next_vdate
                             and ((os.origin_country_id = ccu2.origin_country_id
                                  and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                  and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999'))
                                 or exists (select 'x' 
                                              from cost_zone_group_loc cz 
                                             where cz.zone_group_id = ccu2.zone_group_id
                                               and cz.zone_id       = ccu2.zone_id
                                               and cz.location      = oe.location))
                             and rownum =1
                           UNION ALL
                          select 'x'
                            from cost_comp_upd_stg  ccu2,
                                 exp_prof_head      eph
                           where ccu2.defaulting_level  = 'S'
                             and ccu2.comp_type         = 'E'
                             and ccu2.order_default_ind = 'Y'
                             and ccu2.exp_prof_key      = eph.exp_prof_key
                             and ccu2.comp_id           = oe.comp_id
                             and ccu2.effective_date    = L_next_vdate
                             and ((os.origin_country_id  = ccu2.origin_country_id
                                   and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                   and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999'))
                                 or exists (select 'x' 
                                              from cost_zone_group_loc cz 
                                             where cz.zone_group_id = ccu2.zone_group_id
                                               and cz.zone_id       = ccu2.zone_id
                                               and cz.location      = oe.location))
                             and oe.key_value_1         = eph.key_value_1
                             and eph.module             = 'SUPP'
                             and rownum =1
                           UNION ALL
                          select 'x'
                            from cost_comp_upd_stg  ccu2,
                                 exp_prof_head      eph
                           where ccu2.defaulting_level  = 'P'
                             and ccu2.comp_type         = 'E'
                             and ccu2.order_default_ind = 'Y'
                             and ccu2.exp_prof_key      = eph.exp_prof_key
                             and ccu2.comp_id           = oe.comp_id
                             and ccu2.effective_date    = L_next_vdate
                             and ((os.origin_country_id = ccu2.origin_country_id
                                   and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                   and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999'))
                                 or exists (select 'x' 
                                              from cost_zone_group_loc cz 
                                             where cz.zone_group_id = ccu2.zone_group_id
                                               and cz.zone_id       = ccu2.zone_id
                                               and cz.location      = oe.location))
                             and oe.key_value_1         = eph.key_value_1
                             and oe.key_value_2         = eph.key_value_2   
                             and eph.module             = 'PTNR'
                             and rownum =1
                           UNION ALL
                          select 'x'
                            from cost_comp_upd_stg  ccu2,
                                 exp_prof_head      eph
                           where ccu2.defaulting_level  = 'C'
                             and ccu2.comp_type         = 'E'
                             and ccu2.order_default_ind = 'Y'
                             and ccu2.exp_prof_key      = eph.exp_prof_key
                             and ccu2.comp_id           = oe.comp_id
                             and oe.key_value_1         = eph.origin_country_id
                             and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                             and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999')
                             and eph.module             = 'CTRY'
                             and ccu2.effective_date    = L_next_vdate
                             and rownum =1 )
         and ccu.effective_date   = L_next_vdate                    
         and mod(oe.order_no, I_Threads)+1 = I_Thread_no),
      
   ordloc_exp5a as
     (select oe.*,
               'Y'
          from ordloc_exp5 oe
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.order_no    = oe.order_no
                  and ss.item        = oe.item
                  and rownum =1))
               
   select ordloc_exp1a.*
     from ordloc_exp1a  
   union all
   select oe.*,
          'N'
     from ordloc_exp1 oe
    where oe.row_id not in (select oea.row_id
                              from ordloc_exp1a oea)                              
   union all
   select ordloc_exp2a.*
     from ordloc_exp2a  
   union all
   select oe.*,
          'N'
     from ordloc_exp2 oe
    where oe.row_id not in (select oea.row_id
                              from ordloc_exp2a oea)                              
   union all
   select ordloc_exp3a.*
     from ordloc_exp3a  
   union all
   select oe.*,
          'N'
     from ordloc_exp3 oe
    where oe.row_id not in (select oea.row_id
                              from ordloc_exp3a oea) 
   union all                                 
   select ordloc_exp4a.*
     from ordloc_exp4a  
   union all
   select oe.*,
          'N'
     from ordloc_exp4 oe
    where oe.row_id not in (select oea.row_id
                              from ordloc_exp4a oea)                              
   union all                                 
   select ordloc_exp5a.*
     from ordloc_exp5a  
   union all
   select oe.*,
          'N'
     from ordloc_exp5 oe
    where oe.row_id not in (select oea.row_id
                              from ordloc_exp5a oea);
   
   -- In all the above queries we have selected the orders rowids along with the other required values into the Global temporary table.
   --
   -- inserting into gtt_cost_comp_upd those records which were defaulted into ordloc_exp when lading/discharge ports were not specified
   insert into gtt_cost_comp_upd
        ( row_id,
          order_no,
          item,
          supplier,
          origin_country_id,
          pack_item,
          location,
          comp_id,
          curr_comp_rate,
          curr_comp_currency,
          curr_per_count,
          curr_per_count_uom,
          old_comp_rate,
          old_comp_currency,
          old_per_count,
          old_per_count_uom,
          new_comp_rate,
          new_comp_currency,
          new_per_count,
          new_per_count_uom,
          order_status,
          nom_flag_2,
          cvb_code,
          shipped_ind)
    with ordloc_exp6 AS
        (SELECT oe.ROWID  row_id,
                oe.order_no,
                oe.item,
                oh.supplier,
                os.origin_country_id,
                oe.pack_item,
                oe.location,    
                oe.comp_id,
                oe.comp_rate            curr_comp_rate,
                oe.comp_currency        curr_comp_currency,
                oe.per_count            curr_per_count,
                oe.per_count_uom        curr_per_count_uom,
                ccu.old_comp_rate,
                ccu.old_comp_currency,
                ccu.old_per_count,
                ccu.old_per_count_uom,
                ccu.new_comp_rate,
                ccu.new_comp_currency,
                ccu.new_per_count,
                ccu.new_per_count_uom,
                oh.status,
                oe.nom_flag_2,
                oe.cvb_code
           FROM ordloc_exp oe,
                ordhead oh,
                ordsku os,
                item_exp_head ieh,
                cost_comp_upd_stg ccu
          WHERE ccu.defaulting_level  = 'I'
            and ccu.comp_type         = 'E'
            and ccu.order_default_ind = 'Y'
            and ccu.item_exp_seq = ieh.item_exp_seq
            and ccu.item= ieh.item
            and ccu.supplier = ieh.supplier
            and ieh.base_exp_ind   = 'Y'
            and ccu.item = oe.item
            and ccu.comp_id  = oe.comp_id
            and ccu.supplier = oh.supplier
            and oe.order_no= oh.order_no
            and oh.status in ('W', 'S', 'A')
            and os.order_no  = oe.order_no
            and os.item      = NVL(oe.pack_item,oe.item)
            and (os.origin_country_id = ccu.origin_country_id
                or exists (select 'x' 
                             from cost_zone_group_loc cz 
                            where cz.zone_group_id = ccu.zone_group_id
                              and cz.zone_id       = ccu.zone_id
                              and cz.location      = oe.location))
            and ccu.effective_date   = L_next_vdate                    
            and mod(oe.order_no, I_Threads)+1 = I_Thread_no
      UNION ALL
         SELECT oe.ROWID  row_id,
                oe.order_no,
                oe.item,
                oh.supplier,
                os.origin_country_id,
                oe.pack_item,
                oe.location,
                oe.comp_id,
                oe.comp_rate            curr_comp_rate,
                oe.comp_currency        curr_comp_currency,
                oe.per_count            curr_per_count,
                oe.per_count_uom        curr_per_count_uom,
                ccu.old_comp_rate,
                ccu.old_comp_currency,
                ccu.old_per_count,
                ccu.old_per_count_uom,
                ccu.new_comp_rate,
                ccu.new_comp_currency,
                ccu.new_per_count,
                ccu.new_per_count_uom,
                oh.status,
                oe.nom_flag_2,
                oe.cvb_code
           FROM cost_comp_upd_stg ccu, 
                exp_prof_head     eph,
                ordloc_exp oe, 
                ordhead    oh, 
                ordsku     os
          WHERE ccu.defaulting_level  = 'S'
            and ccu.comp_type         = 'E'
            and ccu.order_default_ind = 'Y'
            and ccu.exp_prof_key      = eph.exp_prof_key
            and eph.base_prof_ind = 'Y'
            and ccu.comp_id = oe.comp_id
            and oe.order_no = oh.order_no
            and oh.status in ('W', 'S', 'A')
            and oe.defaulted_from = 'S'
            and oe.key_value_1 = eph.key_value_1
            and os.order_no = oe.order_no
            and (os.origin_country_id = ccu.origin_country_id
                or exists (select 'x' 
                             from cost_zone_group_loc cz 
                            where cz.zone_group_id = ccu.zone_group_id
                              and cz.zone_id       = ccu.zone_id
                              and cz.location      = oe.location))         
            and os.item     = NVL(oe.pack_item,oe.item)
            and eph.module     = 'SUPP'
            and ccu.effective_date   = L_next_vdate
            and MOD(oe.order_no, I_Threads)+1 = I_Thread_no 
      UNION ALL
         SELECT oe.ROWID  row_id,
                oe.order_no,
                oe.item,
                oh.supplier,
                os.origin_country_id,
                oe.pack_item,
                oe.location,
                oe.comp_id,
                oe.comp_rate            curr_comp_rate,
                oe.comp_currency        curr_comp_currency,
                oe.per_count            curr_per_count,
                oe.per_count_uom        curr_per_count_uom,
                ccu.old_comp_rate,
                ccu.old_comp_currency,
                ccu.old_per_count,
                ccu.old_per_count_uom,
                ccu.new_comp_rate,
                ccu.new_comp_currency,
                ccu.new_per_count,
                ccu.new_per_count_uom,
                oh.status,
                oe.nom_flag_2,
                oe.cvb_code
           FROM cost_comp_upd_stg ccu, 
                exp_prof_head     eph,
                ordloc_exp oe, 
                ordhead    oh, 
                ordsku     os 
          WHERE ccu.defaulting_level  = 'P'
            and ccu.comp_type         = 'E'
            and ccu.order_default_ind = 'Y'
            and ccu.exp_prof_key      = eph.exp_prof_key
            and eph.base_prof_ind = 'Y'
            and ccu.comp_id = oe.comp_id
            and oe.order_no = oh.order_no
            and oh.status in ('W','S','A')
            and oe.defaulted_from = 'P'
            and oe.key_value_1 = eph.key_value_1
            and oe.key_value_2 = eph.key_value_2
            and (os.origin_country_id = ccu.origin_country_id
                or exists (select 'x' 
                             from cost_zone_group_loc cz 
                            where cz.zone_group_id = ccu.zone_group_id
                              and cz.zone_id       = ccu.zone_id
                              and cz.location      = oe.location))
            and os.order_no = oe.order_no
            and os.item     = NVL(oe.pack_item,oe.item)
            and eph.module  = 'PTNR'
            and ccu.effective_date   = L_next_vdate
            and MOD(oe.order_no, I_Threads)+1 = I_Thread_no 
      UNION ALL
         SELECT oe.ROWID  row_id,
                oe.order_no,
                oe.item,
                oh.supplier,
                os.origin_country_id,
                oe.pack_item,
                oe.location,
                oe.comp_id,
                oe.comp_rate            curr_comp_rate,
                oe.comp_currency        curr_comp_currency,
                oe.per_count            curr_per_count,
                oe.per_count_uom        curr_per_count_uom,
                ccu.old_comp_rate,
                ccu.old_comp_currency,
                ccu.old_per_count,
                ccu.old_per_count_uom,
                ccu.new_comp_rate,
                ccu.new_comp_currency,
                ccu.new_per_count,
                ccu.new_per_count_uom,
                oh.status,
                oe.nom_flag_2,
                oe.cvb_code
           FROM cost_comp_upd_stg ccu, 
                exp_prof_head     eph,
                ordloc_exp oe, 
                ordhead    oh, 
                ordsku     os 
          WHERE ccu.defaulting_level  = 'C'
            and ccu.comp_type         = 'E'
            and ccu.order_default_ind = 'Y'
            and ccu.exp_prof_key      = eph.exp_prof_key  
            and eph.base_prof_ind     = 'Y'
            and ccu.comp_id           = oe.comp_id          
            and oe.key_value_1        = ccu.origin_country_id
            and oe.order_no = oh.order_no
            and oh.status in ('W','S','A')
            and oe.defaulted_from = 'C'
            and os.order_no = oe.order_no
            and os.item     = NVL(oe.pack_item,oe.item)
            and eph.module  = 'CTRY'
            and ccu.effective_date   = L_next_vdate
            and MOD(oe.order_no, I_Threads)+1 = I_Thread_no),
    ordloc_exp6a as
     (select oe.*,
             'Y'
        from ordloc_exp6 oe
       where exists (select 'x'
                       from shipment sh,
                            shipsku  ss
                       where sh.shipment    = ss.shipment
                         and sh.order_no    = oe.order_no
                         and ss.item        = oe.item
                         and rownum =1)
         and oe.row_id not in (select row_id
                               from gtt_cost_comp_upd))
     select ordloc_exp6a.*
       from ordloc_exp6a
    union all
     select oe.*,
            'N'
       from ordloc_exp6 oe
      where oe.row_id not in (select oea.row_id
                                from ordloc_exp6a oea)                                                       
        and oe.row_id not in (select row_id
                                from gtt_cost_comp_upd);
   if L_system_options.import_ind = 'N' then
      --
      -- Updating the orders.
      -- 1. Unapporve an order.
      -- 2. Update the ordloc expense.
      -- 3. Call ELC_CALC_SQL.CALC_COMP to recalculate the est_exp_value.
      -- 4. Approve the orders.
      --
      L_prev_ord       := NULL;
      L_prevord_status := NULL;
      --
      --      
      for ord_item in C_ord_items loop
         if L_prev_ord is not NULL then
            if L_prev_ord != ord_item.order_no then
               -- New order being considered, hence approve the previous order
               if L_prevord_status = 'A' then
                  if OTB_SQL.ORD_APPROVE_CASCADE(L_prev_ord,
                                                 O_error_message) = FALSE then
                     return FALSE;
                  end if;  
               end if;
               --
               -- Unapprove the new order.
               if ord_item.order_status = 'A' then
                  if OTB_SQL.ORD_UNAPPROVE_CASCADE(ord_item.order_no,
                                                   O_error_message) = FALSE then
                     return FALSE;
                  end if;
               end if;   
               L_prev_ord       := ord_item.order_no;
               L_prevord_status := ord_item.order_status;
            end if;
         else
            if ord_item.order_status = 'A' then
               if OTB_SQL.ORD_UNAPPROVE_CASCADE(ord_item.order_no,
                                        O_error_message) = FALSE then
                  return FALSE;
               end if;   
            end if;   
            L_prev_ord       := ord_item.order_no;
            L_prevord_status := ord_item.order_status;
         end if;
         --
         --
         if ORDER_ATTRIB_SQL.GET_WRITTEN_DATE(O_error_message,
                                              L_written_date,
                                              ord_item.order_no) = FALSE then
            return FALSE;         
         end if;
         --
         if CURRENCY_SQL.GET_RATE(O_error_message,
                                  L_exchange_rate,
                                  ord_item.new_comp_currency,
                                  'E',
                                  L_written_date) = false then
            return FALSE;
         end if;
         ---
         if ord_item.shipped_ind = 'Y' then
            -- Since the orders' item has been shipped, we put the record into the 
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).item              := ord_item.item;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).pack_item         := ord_item.pack_item;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).location          := ord_item.location;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_currency := ord_item.curr_comp_currency;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_currency := ord_item.new_comp_currency;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).reason_code       := 'ORDS';
            L_log_tbl_cnt := L_log_tbl_cnt +1;
         else
            -- order/item is not shipped.
            if ord_item.curr_comp_rate       != ord_item.old_comp_rate or
               ord_item.curr_comp_currency   != ord_item.old_comp_currency or
               NVL(ord_item.curr_per_count, -999)       != NVL(ord_item.old_per_count, -999) or
               NVL(ord_item.curr_per_count_uom, -999)   != NVL(ord_item.old_per_count_uom, -999) then

               L_ordloc_exp_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).item              := ord_item.item;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).pack_item         := ord_item.pack_item;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).location          := ord_item.location;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_currency := ord_item.curr_comp_currency;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_currency := ord_item.new_comp_currency;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;     
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom; 
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).reason_code       := 'ORDU';
               L_log_tbl_cnt := L_log_tbl_cnt +1;

            end if;
            --
            -- Update the perticular Ordloc expense.
            update ordloc_exp oe
               set oe.comp_rate         = ord_item.new_comp_rate,
                   oe.comp_currency     = ord_item.new_comp_currency,
                   oe.per_count         = ord_item.new_per_count,
                   oe.per_count_uom     = ord_item.new_per_count_uom,
                   oe.exchange_rate     = decode(oe.comp_currency, ord_item.new_comp_currency, oe.exchange_rate, L_exchange_rate)
             where oe.rowid = ord_item.row_id;
            -- recalculate
            if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                      'PE',
                                      ord_item.item, -- item
                                      ord_item.supplier,
                                      NULL, -- item_exp_type
                                      NULL, -- item_exp_seq
                                      ord_item.order_no,
                                      NULL, -- ord_seq_no
                                      ord_item.pack_item, -- pack_item
                                      NULL, -- zone_id
                                      ord_item.location,
                                      NULL, -- hts
                                      NULL, -- import_country_id
                                      ord_item.origin_country_id,
                                      NULL, -- effect_from
                                      NULL) = FALSE then -- effect_to
               return FALSE;
            end if; 
            --
            -- Make an entry into the rev table, if it is not present in db table of the plsql table.
            L_ord_in_rev_tbl := 'N';
            for cnt in 1..L_rev_ord_tbl_cnt-1 loop
               if L_rev_orders_tbl(cnt).order_no = ord_item.order_no then
                  L_ord_in_rev_tbl := 'Y';
                  EXIT;
               end if;
            end loop;
            --
            if L_ord_in_rev_tbl = 'N' then
               L_rev_orders_tbl(L_rev_ord_tbl_cnt).order_no := ord_item.order_no;
               L_rev_ord_tbl_cnt := L_rev_ord_tbl_cnt + 1;
            end if;            --
         end if; -- if order shipped.
      end loop;
      --
      if L_prev_ord is not NULL and L_prevord_status = 'A' then
         if OTB_SQL.ORD_APPROVE_CASCADE(L_prev_ord,
                                O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;
      --
   else -- import ind = 'Y'
      --
      L_log_tbl_cnt     := 1;
      L_prev_ord        := NULL;
      L_prevord_status  := NULL;
      --
      -- Start looping through each of the order items captured in the global temporary table.
      for ord_item in C_ord_items loop
         --
         -- Approve the previous order if this is the second different order.
         if L_prev_ord is not NULL then
            if L_prev_ord != ord_item.order_no then
               -- New order being considered, hence approve the previous order
               if L_prevord_status = 'A' then

                  if OTB_SQL.ORD_APPROVE_CASCADE(L_prev_ord,
                                         O_error_message) = FALSE then
                     return FALSE;
                  end if;  
               end if;   
               --
               -- Unapprove the new order.
               if ord_item.order_status = 'A' then

                  if OTB_SQL.ORD_UNAPPROVE_CASCADE(ord_item.order_no,
                                           O_error_message) = FALSE then
                     return FALSE;
                  end if;
               end if;   
               L_prev_ord       := ord_item.order_no;
               L_prevord_status := ord_item.order_status;
            end if;
         else
            if ord_item.order_status = 'A' then
               if OTB_SQL.ORD_UNAPPROVE_CASCADE(ord_item.order_no,
                                        O_error_message) = FALSE then
                  return FALSE;
               end if;   
            end if;
            L_prev_ord        := ord_item.order_no;
            L_prevord_status  := ord_item.order_status;
         end if;
         --
         --
         if ORDER_ATTRIB_SQL.GET_WRITTEN_DATE(O_error_message,
                                              L_written_date,
                                              ord_item.order_no) = FALSE then
            return FALSE;         
         end if;
         --
         if CURRENCY_SQL.GET_RATE(O_error_message,
                                  L_exchange_rate,
                                  ord_item.new_comp_currency,
                                  'E',
                                  L_written_date) = false then
            return FALSE;
         end if;
         --
         
         if ord_item.shipped_ind = 'Y' then
            -- Since the orders' item has been shipped, we put the record into the 
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).item              := ord_item.item;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).pack_item         := ord_item.pack_item;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).location          := ord_item.location;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_currency := ord_item.curr_comp_currency;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_currency := ord_item.new_comp_currency;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).reason_code       := 'ORDS';
            L_log_tbl_cnt := L_log_tbl_cnt +1;
            --
            --
         else  -- The order is not yet shipped, The assessments need to be checked.
            --
            -- Check if there are expense related assessments.
            open  C_ord_assessments(ord_item.order_no, ord_item.item, ord_item.comp_id);
            fetch C_ord_assessments into L_ord_assessments;
            if C_ord_assessments%FOUND then
               close C_ord_assessments;
               
               -- Verify that at least one of the custom entries is in worksheet status.
               open  C_custom_entry(ord_item.order_no, ord_item.item);
               fetch C_custom_entry into  L_custom_entry;
               if C_custom_entry%FOUND then
                  close C_custom_entry;
                  --
                  if L_custom_entry.status = 'W' then
                     -- Update the expense and the assessments that would get affected.
                     if ord_item.curr_comp_rate     != ord_item.old_comp_rate or
                        ord_item.curr_comp_currency != ord_item.old_comp_currency or
                        NVL(ord_item.curr_per_count, -999)     != NVL(ord_item.old_per_count, -999) or
                        NVL(ord_item.curr_per_count_uom, -999) != NVL(ord_item.old_per_count_uom, -999) then
                        --
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).item              := ord_item.item;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).pack_item         := ord_item.pack_item;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).location          := ord_item.location;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_currency := ord_item.curr_comp_currency;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_currency := ord_item.new_comp_currency;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).reason_code       := 'ORDU';
                        L_log_tbl_cnt := L_log_tbl_cnt +1;
                        --
                     end if;
                     --
                     -- Update the perticular Ordloc expense.
                     update ordloc_exp oe
                        set oe.comp_rate         = ord_item.new_comp_rate,
                            oe.comp_currency     = ord_item.new_comp_currency,
                            oe.per_count         = ord_item.new_per_count,
                            oe.per_count_uom     = ord_item.new_per_count_uom,
                            oe.exchange_rate     = decode(oe.comp_currency, ord_item.new_comp_currency, oe.exchange_rate, L_exchange_rate)
                      where oe.rowid = ord_item.row_id;
                     --
                     if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                               'PE',
                                               ord_item.item, -- item
                                               ord_item.supplier,
                                               NULL, -- item_exp_type
                                               NULL, -- item_exp_seq
                                               ord_item.order_no,
                                               NULL, -- ord_seq_no
                                               ord_item.pack_item, -- pack_item
                                               NULL, -- zone_id
                                               ord_item.location,
                                               NULL, -- hts
                                               NULL, -- import_country_id
                                               ord_item.origin_country_id,
                                               NULL, -- effect_from
                                               NULL) = FALSE then -- effect_to
                        return FALSE;
                     end if;

                     -- Make an entry into the rev plsql table, if it's not present.
                     L_ord_in_rev_tbl := 'N';
                     for cnt in 1..L_rev_ord_tbl_cnt-1 loop
                        if L_rev_orders_tbl(cnt).order_no = ord_item.order_no then
                           L_ord_in_rev_tbl := 'Y';
                           EXIT;
                        end if;
                     end loop;
                     --
                     if L_ord_in_rev_tbl = 'N' then
                        L_rev_orders_tbl(L_rev_ord_tbl_cnt).order_no := ord_item.order_no;
                        L_rev_ord_tbl_cnt := L_rev_ord_tbl_cnt + 1;
                     end if;            --
                     
                     -- Loop through all assessments that would need recalculation due to expense update.
                     for assmnt in C_ord_assessments(ord_item.order_no, ord_item.item, ord_item.comp_id) loop
                        
                        -- Recalculate the assessment
                        if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                                  'PA',
                                                  ord_item.item, -- item
                                                  NULL,
                                                  NULL, -- item_exp_type
                                                  NULL, -- item_exp_seq
                                                  ord_item.order_no,
                                                  NULL, -- ord_seq_no
                                                  ord_item.pack_item, -- pack_item
                                                  NULL, -- zone_id
                                                  NULL,
                                                  assmnt.hts, -- hts
                                                  assmnt.import_country_id, -- import_country_id
                                                  ord_item.origin_country_id,
                                                  assmnt.effect_from, -- effect_from
                                                  assmnt.effect_to) = FALSE then -- effect_to
                           return FALSE;
                        end if;                      
                        --
                        -- log the assessment update into local table variable
                        L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).order_no            := ord_item.order_no;
                        L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).item                := ord_item.item;
                        L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).import_country_id   := assmnt.import_country_id;
                        L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).hts                 := assmnt.hts;
                        L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).effect_from         := assmnt.effect_from;
                        L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).effect_to           := assmnt.effect_to;
                        L_mod_order_tbl_cnt := L_mod_order_tbl_cnt+1;
                        --
                        --

                        -- / Delete and redefault the custom entry charges
                        -- loop through all the custom entries and for all those having status as 'W'orksheet, delete and redefault the custom entry.
                        for cust_ent in C_custom_entry(ord_item.order_no, ord_item.item) loop
                           if cust_ent.status = 'W' then
                              -- Delete the custom entry, and call CE_CHARGES_SQL.INSERT_COMPS to redefault the custom entry.
                              delete from ce_charges
                                    where ce_id       = cust_ent.ce_id
                                      and order_no    = ord_item.order_no
                                      and item        = ord_item.item
                                      and hts         = assmnt.hts 
                                      and effect_from = assmnt.effect_from  
                                      and effect_to   = assmnt.effect_to    
                                      and (ord_item.pack_item IS NULL
                                        or (ord_item.pack_item IS NOT NULL
                                            AND pack_item = ord_item.pack_item));
                              --
                              -- redefault the custom entry.
                              if CE_CHARGES_SQL.INSERT_COMPS(L_error_message,
                                                             cust_ent.ce_id, 
                                                             cust_ent.vessel_id,
                                                             cust_ent.voyage_flt_id,
                                                             TO_DATE(cust_ent.estimated_depart_date, 'DD-MON-RR'),
                                                             ord_item.order_no,
                                                             ord_item.item,
                                                             ord_item.pack_item,
                                                             assmnt.hts,
                                                             assmnt.import_country_id,
                                                             TO_DATE(assmnt.effect_from, 'DD-MON-RR'),
                                                             TO_DATE(assmnt.effect_to,   'DD-MON-RR')) = FALSE then
                                 return FALSE;
                              end if;
                              --
                           end if;
                        end loop; -- for each custom entry.
                     end loop; -- for each assessment
                  else -- none of the custom entries are in Worksheet status
                     --
                     if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(L_error_message,
                                                         L_standard_uom,
                                                         L_standard_class,    
                                                         L_conv_factor,
                                                         ord_item.item,
                                                         'N') = FALSE then
                        return FALSE;
                     end if;
                     --
                     -- Calculate the total of all the manifest quantities of the custom entries.
                     L_total_qty := 0;
                     for cust_ent in C_custom_entry(ord_item.order_no, ord_item.item) loop
                        --
                        if L_standard_uom != cust_ent.manifest_item_qty_uom then
                           if UOM_SQL.CONVERT(L_error_message, 
                                              L_std_manifest_item_qty,
                                              L_standard_uom,
                                              cust_ent.manifest_item_qty,
                                              cust_ent.manifest_item_qty_uom,
                                              ord_item.item,
                                              ord_item.supplier,
                                              ord_item.origin_country_id) = FALSE then
                              return FALSE;
                           end if;
                        else
                           L_std_manifest_item_qty := cust_ent.manifest_item_qty;
                        end if;   
                        --
                        L_total_qty := L_total_qty + L_std_manifest_item_qty;
                        --
                     end loop;
                     --
                     -- Get the order items total quenatiy.
                     open C_ord_qty(ord_item.order_no, ord_item.item);
                     fetch C_ord_qty into L_ordered_qty;
                     close C_ord_qty;
                     --
                     -- Check if the total manifest quantity is lesser than the ordered quantity.
                     if L_total_qty < L_ordered_qty then
                        -- Update the expense and the assessments that would get affected.
                        if UPDATE_EXP_RECAL_ASSMT (O_error_message,      
                                                   L_ordloc_exp_log_tbl,
                                                   L_log_tbl_cnt,
                                                   L_mod_order_item_hts_tbl,
                                                   L_mod_order_tbl_cnt,
                                                   L_rev_orders_tbl,
                                                   L_rev_ord_tbl_cnt,
                                                   ord_item.row_id,             
                                                   ord_item.item,
                                                   ord_item.supplier,
                                                   ord_item.order_no,
                                                   ord_item.order_status,
                                                   ord_item.pack_item,          
                                                   ord_item.location,
                                                   ord_item.comp_id,
                                                   ord_item.origin_country_id,
                                                   ord_item.curr_comp_rate,     
                                                   ord_item.curr_comp_currency,
                                                   ord_item.curr_per_count,
                                                   ord_item.curr_per_count_uom,
                                                   ord_item.old_comp_rate,
                                                   ord_item.old_comp_currency,
                                                   ord_item.old_per_count,
                                                   ord_item.old_per_count_uom,
                                                   L_exchange_rate,                                                   
                                                   ord_item.new_comp_rate,
                                                   ord_item.new_comp_currency,
                                                   ord_item.new_per_count,
                                                   ord_item.new_per_count_uom) = FALSE then
                           return FALSE;
                        end if;
                     else 
                        -- the ordered quantity is not less than the manifest.
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).item              := ord_item.item;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).pack_item         := ord_item.pack_item;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).location          := ord_item.location;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_currency := ord_item.curr_comp_currency;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_currency := ord_item.new_comp_currency;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).reason_code       := 'CEVF';
                        L_log_tbl_cnt := L_log_tbl_cnt +1;
                        --
                     end if;  -- end total manifest qty < ordered qty
                     --          
                  end if; -- custom entry in worksheet status.
               else
                  close C_custom_entry;
                  -- no custom entries present, update the expense and recalculate all the related assessments.

                  -- Update the expense and the assessments that would get affected.
                  if UPDATE_EXP_RECAL_ASSMT (O_error_message,      
                                             L_ordloc_exp_log_tbl,
                                             L_log_tbl_cnt,      
                                             L_mod_order_item_hts_tbl,
                                             L_mod_order_tbl_cnt,
                                             L_rev_orders_tbl,
                                             L_rev_ord_tbl_cnt,
                                             ord_item.row_id,             
                                             ord_item.item,
                                             ord_item.supplier,
                                             ord_item.order_no,
                                             ord_item.order_status,
                                             ord_item.pack_item,          
                                             ord_item.location,
                                             ord_item.comp_id,
                                             ord_item.origin_country_id,
                                             ord_item.curr_comp_rate,     
                                             ord_item.curr_comp_currency, 
                                             ord_item.curr_per_count,     
                                             ord_item.curr_per_count_uom, 
                                             ord_item.old_comp_rate, 
                                             ord_item.old_comp_currency, 
                                             ord_item.old_per_count, 
                                             ord_item.old_per_count_uom,
                                             L_exchange_rate,                                             
                                             ord_item.new_comp_rate, 
                                             ord_item.new_comp_currency, 
                                             ord_item.new_per_count, 
                                             ord_item.new_per_count_uom) = FALSE then
                     return FALSE;
                  end if;
               end if;               
            else -- assessments are not affected by the expense update
               close C_ord_assessments;
               --
               if ord_item.curr_comp_rate     != ord_item.old_comp_rate or
                  ord_item.curr_comp_currency != ord_item.old_comp_currency or
                  NVL(ord_item.curr_per_count, -999)     != NVL(ord_item.old_per_count, -999) or
                  NVL(ord_item.curr_per_count_uom, -999) != NVL(ord_item.old_per_count_uom, -999) then

                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).item              := ord_item.item;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).pack_item         := ord_item.pack_item;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).location          := ord_item.location;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_currency := ord_item.curr_comp_currency;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_currency := ord_item.new_comp_currency;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).reason_code       := 'ORDU';
                  L_log_tbl_cnt := L_log_tbl_cnt +1;

               end if;
               --
               -- Update the perticular Ordloc expense.
               update ordloc_exp oe
                  set oe.comp_rate         = ord_item.new_comp_rate,
                      oe.comp_currency     = ord_item.new_comp_currency,
                      oe.per_count         = ord_item.new_per_count,
                      oe.per_count_uom     = ord_item.new_per_count_uom,
                      oe.exchange_rate     = decode(oe.comp_currency, ord_item.new_comp_currency, oe.exchange_rate, L_exchange_rate)
                where oe.rowid = ord_item.row_id;
               --
               if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                         'PE',
                                         ord_item.item, -- item
                                         ord_item.supplier,
                                         NULL, -- item_exp_type
                                         NULL, -- item_exp_seq
                                         ord_item.order_no,
                                         NULL, -- ord_seq_no
                                         ord_item.pack_item, -- pack_item
                                         NULL, -- zone_id
                                         ord_item.location,
                                         NULL, -- hts
                                         NULL, -- import_country_id
                                         ord_item.origin_country_id,
                                         NULL, -- effect_from
                                         NULL) = FALSE then -- effect_to
                  return FALSE;
               end if;
               
               -- Make an entry into the rev table, if it is not present in db table of the plsql table.
               L_ord_in_rev_tbl := 'N';
               for cnt in 1..L_rev_ord_tbl_cnt-1 loop
                  if L_rev_orders_tbl(cnt).order_no = ord_item.order_no then
                     L_ord_in_rev_tbl := 'Y';
                     EXIT;
                  end if;
               end loop;
               --
               if L_ord_in_rev_tbl = 'N' then
                  L_rev_orders_tbl(L_rev_ord_tbl_cnt).order_no := ord_item.order_no;
                  L_rev_ord_tbl_cnt := L_rev_ord_tbl_cnt + 1;
               end if;            --
                
            end if; -- end if expense related assessments found
            
            -- calling calc comp to recompute the consolidated/dependent component values.
            if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                      'PA',
                                      ord_item.item,
                                      NULL,
                                      NULL,
                                      NULL,
                                      ord_item.order_no,
                                      NULL,
                                      ord_item.pack_item,
                                      NULL,
                                      NULL,
                                      NULL,
                                      ord_item.origin_country_id,
                                      NULL,
                                      NULL) = FALSE then
               return FALSE;
            end if;
            
            -- whenever the updated expense component has the inDuty flag +/- all the hts are updated and hence lot the hts values.
            if ord_item.nom_flag_2 IN ('+', '-') then
               for l_hts in C_ord_hts(ord_item.order_no, ord_item.item) loop

                  L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).order_no            := ord_item.order_no;
                  L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).item                := ord_item.item;
                  L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).import_country_id   := l_hts.import_country_id;
                  L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).hts                 := l_hts.hts;
                  L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).effect_from         := l_hts.effect_from;
                  L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).effect_to           := l_hts.effect_to;
                  L_mod_order_tbl_cnt := L_mod_order_tbl_cnt+1;
                  --
               end loop;
            end if;
            
         end if; -- order shipped.   
      end loop; -- end loop for each selected record, for an item order.   
      
      -- Approve the last order if it was unapproved.
      if L_prev_ord is not NULL and L_prevord_status = 'A' then
         if OTB_SQL.ORD_APPROVE_CASCADE(L_prev_ord,
                                O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;
   end if; -- end if import ind
   --
   -- call the flush expenses to copy the contents of the plsql table.
   if FLUSH_CHANGES_EXPENSES(O_error_message,       
                             L_ordloc_exp_log_tbl,
                             L_rev_orders_tbl,
                             L_mod_order_item_hts_tbl) = FALSE then
      return FALSE;
   end if;                             
   --                             
   delete gtt_cost_comp_upd;  
   return TRUE;
   --
EXCEPTION
   when OTHERS then
      -- delete the contents of the global temporary table.
      delete gtt_cost_comp_upd;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.UPDATE_ORDLOC_EXP',
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ORDLOC_EXP;


---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ASSESS_RECAL_ASSESS
-- Purpose: The function is for internal use and is called by the function UPDATE_ORDSKU_HTS_ASSESS. 
-- This function updates the ordloc exp component values. It also calls ELC_CALC_SQL.CALC_COMP to 
-- recalculate the order expenses depending on the upd_assess_ind it updates the associated
-- assessments as well,
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ASSESS_RECAL_ASSESS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_ordsku_assess_log_tbl  IN OUT ordsku_assess_TBL,
                                    O_log_tbl_cnt            IN OUT NUMBER,
                                    O_mod_order_item_hts_tbl IN OUT mod_order_item_hts_TBL,
                                    O_mod_order_tbl_cnt      IN OUT NUMBER,
                                    O_rev_orders_tbl         IN OUT rev_ord_TBL, 
                                    O_rev_ord_tbl_cnt        IN OUT NUMBER,
                                    I_row_id                 IN ROWID,
                                    I_item                   IN ITEM_MASTER.ITEM%TYPE,
                                    I_order_no               IN ORDHEAD.ORDER_NO%TYPE,
                                    I_ord_stat               IN ORDHEAD.STATUS%TYPE,
                                    I_seq_no                 IN ORDSKU_HTS.SEQ_NO%TYPE,
                                    I_comp_id                IN ELC_COMP.COMP_ID%TYPE,
                                    I_pack_item              IN ORDLOC_EXP.PACK_ITEM%TYPE,
                                    I_hts                    IN ORDSKU_HTS.HTS%TYPE,
                                    I_origin_country_id      IN ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                    I_import_country_id      IN ORDSKU_HTS.IMPORT_COUNTRY_ID%TYPE,
                                    I_effect_from            IN ORDSKU_HTS.EFFECT_FROM%TYPE,
                                    I_effect_to              IN ORDSKU_HTS.EFFECT_TO%TYPE,                           
                                    I_curr_comp_rate         IN ORDLOC_EXP.COMP_RATE%TYPE,
                                    I_curr_per_count         IN ORDLOC_EXP.PER_COUNT%TYPE,
                                    I_curr_per_count_uom     IN ORDLOC_EXP.PER_COUNT_UOM%TYPE,
                                    I_old_comp_rate          IN ORDLOC_EXP.COMP_RATE%TYPE,
                                    I_old_per_count          IN ORDLOC_EXP.PER_COUNT%TYPE,
                                    I_old_per_count_uom      IN ORDLOC_EXP.PER_COUNT_UOM%TYPE,
                                    I_new_comp_rate          IN ORDLOC_EXP.COMP_RATE%TYPE,
                                    I_new_per_count          IN ORDLOC_EXP.PER_COUNT%TYPE,
                                    I_new_per_count_uom      IN ORDLOC_EXP.PER_COUNT_UOM%TYPE)
RETURN BOOLEAN
IS
   L_ord_in_rev_tbl   VARCHAR2(1);
BEGIN
   --
   -- / Updating the Order Expense and making a log entry.
   if I_curr_comp_rate     != I_old_comp_rate or
      I_curr_per_count     != I_old_per_count or
      NVL(I_curr_per_count_uom, -999) != NVL(I_old_per_count_uom, -999) then
      
      -- Since the orders' item has been shipped, we put the record into the 
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).order_no          := I_order_no;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).comp_id           := I_comp_id;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).old_comp_rate     := I_curr_comp_rate;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).old_per_count     := I_curr_per_count;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).old_per_count_uom := I_curr_per_count_uom;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).new_comp_rate     := I_new_comp_rate;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).new_per_count     := I_new_per_count;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).new_per_count_uom := I_new_per_count_uom;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).reason_code       := 'ORDU';
      O_log_tbl_cnt  := O_log_tbl_cnt + 1;
      --
   end if;
   --
   -- Update the perticular Ordloc expense.
   update ordsku_hts_assess oha
      set oha.comp_rate         = I_new_comp_rate,
          oha.per_count         = I_new_per_count,
          oha.per_count_uom     = I_new_per_count_uom
    where oha.rowid = I_row_id;
    
   -- Recalculate the assessment which has been updated.
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PA',
                             I_item, -- item
                             NULL,
                             NULL,   -- item_exp_type
                             NULL,   -- item_exp_seq
                             I_order_no,
                             NULL,   -- ord_seq_no
                             I_pack_item, -- pack_item
                             NULL,   -- zone_id
                             NULL,
                             I_hts,  -- hts
                             I_import_country_id,      -- import_country_id
                             I_origin_country_id,
                             I_effect_from,            -- effect_from
                             I_effect_to) = FALSE then -- effect_to
      return FALSE;
   end if;
   --
   O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).order_no            := I_order_no;
   O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).item                := I_item;
   O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).import_country_id   := I_import_country_id;
   O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).hts                 := I_hts;
   O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).effect_from         := I_effect_from;
   O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).effect_to           := I_effect_to;
   O_mod_order_tbl_cnt := O_mod_order_tbl_cnt+1;
   --
   -- storing the updated order into rev_order table.
   -- Make an entry into the rev table, if it is not present in db table of the plsql table.
   L_ord_in_rev_tbl := 'N';
   for cnt in 1..O_rev_ord_tbl_cnt -1 loop
      if O_rev_orders_tbl(cnt).order_no = I_order_no then
         L_ord_in_rev_tbl := 'Y';
         EXIT;
      end if;
   end loop;
   --
   if L_ord_in_rev_tbl = 'N' then
      O_rev_orders_tbl(O_rev_ord_tbl_cnt).order_no := I_order_no;
      O_rev_ord_tbl_cnt := O_rev_ord_tbl_cnt + 1;
   end if;
   --
   return TRUE;
   --
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.UPDATE_ASSESS_RECAL_ASSESS',
                                            to_char(SQLCODE));
      return FALSE;   
      
END UPDATE_ASSESS_RECAL_ASSESS;    


---------------------------------------------------------------------------------------------
-- Function Name: FLUSH_CHANGES_ASSESSMENTS
-- Purpose: The function is called by the function UPDATE_ORDSKU_HTS_ASSESS. This function 
--          copies all the data from the plsql tables sent as parameters to the respective tables.
--          The orders which were updated manually are inserted into the table ordsku_hts_assess_exc_log
--          When an approved order is updated or modified the record is entered into the table rev_orders.
---------------------------------------------------------------------------------------------
FUNCTION FLUSH_CHANGES_ASSESSMENTS(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,                
                                   I_ordsku_assess_log_tbl IN      ordsku_assess_TBL,
                                   I_ordsku_mod_order_tbl  IN      mod_order_item_hts_TBL,
                                   I_rev_orders_tbl        IN      rev_ord_TBL)
RETURN BOOLEAN
IS
BEGIN

   for rec in 1..I_ordsku_assess_log_tbl.COUNT loop
      insert into cost_comp_exc_log 
        (exception_type,
         order_no,               
         comp_id,                
         old_comp_rate,          
         old_per_count,          
         old_per_count_uom,      
         new_comp_rate,          
         new_per_count,          
         new_per_count_uom,      
         create_id,              
         create_datetime,        
         reason_code)
        values
        ('OA',
         I_ordsku_assess_log_tbl(rec).order_no,
         I_ordsku_assess_log_tbl(rec).comp_id,         
         I_ordsku_assess_log_tbl(rec).old_comp_rate,     
         I_ordsku_assess_log_tbl(rec).old_per_count,     
         I_ordsku_assess_log_tbl(rec).old_per_count_uom, 
         I_ordsku_assess_log_tbl(rec).new_comp_rate,     
         I_ordsku_assess_log_tbl(rec).new_per_count,     
         I_ordsku_assess_log_tbl(rec).new_per_count_uom, 
         user,
         sysdate,
         I_ordsku_assess_log_tbl(rec).reason_code);
   end loop;   
   --   
   for rec in 1..I_ordsku_mod_order_tbl.COUNT loop
         
      -- insert the record if the records do not exist
      merge into mod_order_item_hts moi
      using (select 'x' from dual)
         on (moi.order_no          = I_ordsku_mod_order_tbl(rec).order_no
         and moi.item              = I_ordsku_mod_order_tbl(rec).item
         and moi.import_country_id = I_ordsku_mod_order_tbl(rec).import_country_id
         and moi.hts               = I_ordsku_mod_order_tbl(rec).hts
         and moi.effect_from       = I_ordsku_mod_order_tbl(rec).effect_from
         and moi.effect_to         = I_ordsku_mod_order_tbl(rec).effect_to
         and moi.unapprove_ind     = 'N'
         and moi.pgm_name          = 'batch_ordcostcompupd'
         and to_char(moi.updated_datetime,'DD-MON-YY') = to_char(L_vdate,'DD-MON-YY'))
      when not matched then
       insert (order_no,               
               item,                   
               import_country_id,               
               hts,          
               effect_from,
               effect_to,      
               unapprove_ind,
               pgm_name,
               updated_datetime)
              values
              (I_ordsku_mod_order_tbl(rec).order_no,          
               I_ordsku_mod_order_tbl(rec).item,              
               I_ordsku_mod_order_tbl(rec).import_country_id,
               I_ordsku_mod_order_tbl(rec).hts,     
               I_ordsku_mod_order_tbl(rec).effect_from,     
               I_ordsku_mod_order_tbl(rec).effect_to, 
               'N',
               'batch_ordcostcompupd',
               L_vdate);

   end loop;
   --
   --
   for rec in 1..I_rev_orders_tbl.COUNT loop   
      merge into rev_orders ro
      using (select 'x' from dual) 
      on    (ro.order_no = I_rev_orders_tbl(rec).order_no) 
      when not matched then
        insert (order_no)
        values (I_rev_orders_tbl(rec).order_no);
   end loop;
   --   
   return TRUE;
   --
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.FLUSH_CHANGES_ASSESSMENTS',
                                            to_char(SQLCODE));
      return FALSE;       
END FLUSH_CHANGES_ASSESSMENTS;


---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ORDSKU_HTS_ASSESS
-- Purpose: The function is called by the function PROCESS_COST_COMP_UPDATES. This function 
--          selects rows from the cost_comp_upd_stg which have the defaulting levels as 'I'tem,
--          and 'E'lc. It updates the Order item hts assessments.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDSKU_HTS_ASSESS(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_Thread_no     IN      RESTART_CONTROL.NUM_THREADS%TYPE,
                                  I_Threads       IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN
IS
   
   L_ordsku_assess_log_tbl                ordsku_assess_TBL;      -- to hold the orders which are shipped.
   L_rev_orders_tbl                       rev_ord_TBL;            -- holds the orders that need to be inserted into the REV Table.
   L_mod_order_item_hts_tbl               mod_order_item_hts_TBL; -- holds the assessments which were recalculated.
   
   -- Cursor used to loop through the records of the GTT.
   cursor C_ord_items
   is 
   select gtt.row_id,
          gtt.order_no,
          gtt.item,
          gtt.supplier,
          gtt.origin_country_id,
          gtt.import_country_id,
          gtt.pack_item,
          gtt.hts,
          gtt.seq_no,
          gtt.effect_from,
          gtt.effect_to,
          gtt.comp_id,
          gtt.curr_comp_rate,
          gtt.curr_comp_currency,
          gtt.curr_per_count,
          gtt.curr_per_count_uom,
          gtt.old_comp_rate,
          gtt.old_comp_currency,
          gtt.old_per_count,
          gtt.old_per_count_uom,
          gtt.new_comp_rate,
          gtt.new_comp_currency,
          gtt.new_per_count,
          gtt.new_per_count_uom,
          gtt.shipped_ind,
          gtt.order_status,
          gtt.nom_flag_2,
          gtt.cvb_code
     from gtt_cost_comp_upd gtt
     order by order_no, item;

   L_prev_ord        ORDHEAD.ORDER_NO%TYPE := NULL;
   L_prevord_status     VARCHAR2(1)        := NULL;
   L_log_tbl_cnt        NUMBER := 1;
   L_rev_ord_tbl_cnt    NUMBER := 1;
   L_mod_order_tbl_cnt  NUMBER := 1;

   -- Cursor to fetch the custom entries.
   cursor C_custom_entry(I_ord_no ORDHEAD.ORDER_NO%TYPE, I_item ITEM_MASTER.ITEM%TYPE)
   is
   select distinct
          ceh.ce_id,
          coi.vessel_id, 
          coi.voyage_flt_id,
          coi.estimated_depart_date,
          NVL(coi.manifest_item_qty, 0) manifest_item_qty,
          NVL(coi.manifest_item_qty_uom, 0) manifest_item_qty_uom,
          ceh.status,
          oh.supplier
     from ce_ord_item coi, 
          ce_head ceh, 
          ordhead oh
    where oh.order_no  = I_ord_no
      and coi.order_no = oh.order_no
      and coi.item     = I_item
      and ceh.ce_id    = coi.ce_id
    order by ceh.status desc;
    
   L_custom_entry C_custom_entry%ROWTYPE;
   
   -- Cursor to get the total ordered quantity
   cursor C_ord_qty(I_order ORDLOC.ORDER_NO%TYPE, I_item ORDLOC.ITEM%TYPE)
   is 
   select SUM(ol.qty_ordered)
     from ordloc ol
    where ol.order_no = I_order
      and ol.item     = I_item;
   --   
   --
   L_error_message         RTK_ERRORS.RTK_KEY%TYPE;
   L_standard_uom          UOM_CLASS.UOM%TYPE;
   L_standard_class        UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor           ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_total_qty             NUMBER;
   L_std_manifest_item_qty NUMBER;
   L_ordered_qty           NUMBER;
   L_ord_in_rev_tbl        VARCHAR2(1);
   --
BEGIN
   --
   -- Populate the global temporary table with the selected order item records.
   insert into gtt_cost_comp_upd
   (      row_id,
          order_no,
          item,
          supplier,
          origin_country_id,
          import_country_id,
          pack_item,
          hts,
          seq_no,
          effect_from,
          effect_to,
          comp_id,
          curr_comp_rate,
          curr_per_count,
          curr_per_count_uom,
          old_comp_rate,
          old_per_count,
          old_per_count_uom,
          new_comp_rate,
          new_per_count,
          new_per_count_uom,
          order_status,
          nom_flag_2,
          cvb_code,
          shipped_ind)
   with ordsku_hts1 as  
   -- This select query gets the data defaulted from Elc comp form.
     (select oha.rowid row_id,
             oha.order_no,
             oht.item,
             oh.supplier,
             oht.origin_country_id,
             oht.import_country_id,
             oht.pack_item,
             oht.hts,
             oha.seq_no,
             oht.effect_from,
             oht.effect_to,
             oha.comp_id,
             oha.comp_rate       curr_comp_rate,
             oha.per_count       curr_per_count,
             oha.per_count_uom   curr_per_count_uom,
             ccu.old_comp_rate,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oha.nom_flag_2,
             oha.cvb_code
        from cost_comp_upd_stg ccu, 
             ordsku_hts_assess oha, 
             ordsku_hts oht, 
             ordhead oh,
             ordsku os
       where ccu.defaulting_level   = 'E'
         and ccu.comp_type          = 'A'
         and ccu.order_default_ind  = 'Y'
         and oha.comp_id  = ccu.comp_id
         and oha.seq_no   = oht.seq_no
         and oha.order_no = oht.order_no
         and nvl(oht.pack_item,oht.item)    = os.item
         and oht.order_no = os.order_no
         and oh.order_no  = oha.order_no
         and oh.import_country_id = ccu.import_country_id
         and oh.status    IN ('S', 'W', 'A')
         and ccu.effective_date = L_next_vdate
         and not exists ( select 'x'
                            from cost_comp_upd_stg ccu2
                           where ccu2.defaulting_level = 'I'
                             and ccu2.comp_type        = 'A'
                             and ccu2.order_default_ind= 'Y'
                             and oha.comp_id           = ccu2.comp_id
                             and oht.item              = ccu2.item  
                             and oht.hts               = ccu2.hts
                             and oht.effect_from       = ccu2.effect_from
                             and oht.effect_to         = ccu2.effect_to
                             and oht.import_country_id = ccu2.import_country_id 
                             and oht.origin_country_id = ccu2.origin_country_id
                             and ccu2.effective_date   = L_next_vdate)
         and mod(oha.order_no, I_Threads)+1 = I_Thread_no),
   --
   ordsku_hts1a as  -- queries all the shipped orders
      (select oh.*,
                'Y' -- shipped indicator
           from ordsku_hts1 oh
          where exists (select 'x'
                  from shipment sh,
                       shipsku  ss
                 where sh.shipment    = ss.shipment
                   and sh.order_no    = oh.order_no
                   and ss.item        = oh.item
                   and rownum =1)),

   ordsku_hts2 as  
   -- This select query gets the data defaulted from Item assessment form.
     (select oha.rowid row_id,
             oha.order_no,
             oht.item,
             oh.supplier,
             oht.origin_country_id,  
             oht.import_country_id,
             oht.pack_item,
             oht.hts,
             oha.seq_no,
             oht.effect_from,
             oht.effect_to,
             oha.comp_id,
             oha.comp_rate,
             oha.per_count,
             oha.per_count_uom,
             ccu.old_comp_rate,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oha.nom_flag_2,
             oha.cvb_code
        from cost_comp_upd_stg ccu, 
             ordsku_hts_assess oha, 
             ordsku_hts oht, 
             ordhead oh,
             ordsku  os
       where ccu.defaulting_level  = 'I'
         and ccu.comp_type         = 'A'
         and ccu.order_default_ind = 'Y'
         and oha.comp_id  = ccu.comp_id
         and oha.order_no = oht.order_no
         and oha.seq_no   = oht.seq_no
         and oht.item     = ccu.item
         and nvl(oht.pack_item,oht.item) = os.item
         and oht.hts               = ccu.hts
         and oht.effect_from       = ccu.effect_from
         and oht.effect_to         = ccu.effect_to
         and oht.import_country_id = ccu.import_country_id
         and os.order_no           = oha.order_no
         and oht.origin_country_id = ccu.origin_country_id
         and oh.order_no           = oht.order_no
         and oh.status in ('S', 'W', 'A')
         and ccu.effective_date    = L_next_vdate
         and mod(oha.order_no, I_Threads)+1 = I_Thread_no),
   --
   ordsku_hts2a as -- queries all the shipped orders
      (select oh.*,
                'Y' -- shipped indicator.
         from ordsku_hts2 oh
        where exists (select 'x'
                from shipment sh,
                     shipsku  ss
               where sh.shipment    = ss.shipment
                 and sh.order_no    = oh.order_no
                 and ss.item        = oh.item
                 and rownum =1))
   -- query all the orders taking union of the above data sets.
   select ordsku_hts1a.*
     from ordsku_hts1a  
   union all
   select oh.*,
          'N'     -- shipped indicator
     from ordsku_hts1 oh
    where oh.row_id not in (select oha.row_id
                              from ordsku_hts1a oha)                              
   union all
   select ordsku_hts2a.*
     from ordsku_hts2a  
   union all
   select oh.*,
          'N'
     from ordsku_hts2 oh
    where oh.row_id not in (select oha.row_id
                              from ordsku_hts2a oha);
                                 
   
   -- In all the above queries we have selected the orders rowids along with the other required values into the Global temporary table.
   --
   L_log_tbl_cnt     := 1;
   L_prev_ord        := NULL;
   L_prevord_status  := NULL;
   --
   -- Start looping through each of the order items captured in the global temporary table.
   for ord_item in C_ord_items loop
      --
      -- Approve the previous order if this is the second different order.
      if L_prev_ord is not NULL then
         if L_prev_ord != ord_item.order_no then
            -- New order being considered, hence approve the previous order
            if L_prevord_status = 'A' then
               if OTB_SQL.ORD_APPROVE_CASCADE(L_prev_ord,
                                      O_error_message) = FALSE then
                  return FALSE;
               end if;  
            end if;   
            --
            -- Unapprove the new order.
            if ord_item.order_status = 'A' then
               if OTB_SQL.ORD_UNAPPROVE_CASCADE(ord_item.order_no,
                                        O_error_message) = FALSE then
                  return FALSE;
               end if;
            end if;   
            L_prev_ord       := ord_item.order_no;
            L_prevord_status := ord_item.order_status;
         end if;
      else
         if ord_item.order_status = 'A' then
            if OTB_SQL.ORD_UNAPPROVE_CASCADE(ord_item.order_no,
                                     O_error_message) = FALSE then
               return FALSE;
            end if;
         end if;
         L_prev_ord        := ord_item.order_no;
         L_prevord_status  := ord_item.order_status;
      end if;
      --  
      if ord_item.shipped_ind = 'Y' then
         -- Since the orders' item has been shipped, we put the record into the 
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).reason_code       := 'ORDS';
         L_log_tbl_cnt := L_log_tbl_cnt +1;
         --
      else  -- The order is not yet shipped, the assessments need to be checked.
         --
         -- Verify that at least one of the custom entries is in worksheet status.
         open  C_custom_entry(ord_item.order_no, ord_item.item);
         fetch C_custom_entry into  L_custom_entry;
         if C_custom_entry%FOUND then
            close C_custom_entry;
            --
            if L_custom_entry.status = 'W' then
               -- update this assessment and also the other assessments which get affected through CVB.
               if UPDATE_ASSESS_RECAL_ASSESS(L_error_message,   
                                             L_ordsku_assess_log_tbl, 
                                             L_log_tbl_cnt,   
                                             L_mod_order_item_hts_tbl,
                                             L_mod_order_tbl_cnt,
                                             L_rev_orders_tbl,
                                             L_rev_ord_tbl_cnt,                                             
                                             ord_item.row_id,        
                                             ord_item.item,                  
                                             ord_item.order_no,              
                                             ord_item.order_status,
                                             ord_item.seq_no,
                                             ord_item.comp_id,
                                             ord_item.pack_item,             
                                             ord_item.hts,                   
                                             ord_item.origin_country_id,     
                                             ord_item.import_country_id,     
                                             ord_item.effect_from,           
                                             ord_item.effect_to,                               
                                             ord_item.curr_comp_rate,        
                                             ord_item.curr_per_count,        
                                             ord_item.curr_per_count_uom,    
                                             ord_item.old_comp_rate,         
                                             ord_item.old_per_count,         
                                             ord_item.old_per_count_uom,     
                                             ord_item.new_comp_rate,         
                                             ord_item.new_per_count,         
                                             ord_item.new_per_count_uom) = FALSE then               
                  return FALSE;
               end if;
               --
               for cust_ent in C_custom_entry(ord_item.order_no, ord_item.item) loop
                  if cust_ent.status = 'W' then
                     -- Delete the custom entry, and call CE_CHARGES_SQL.INSERT_COMPS to redefault the custom entry.
                     delete from ce_charges
                           where ce_id       = cust_ent.ce_id
                             and order_no    = ord_item.order_no
                             and item        = ord_item.item
                             and hts         = ord_item.hts 
                             and effect_from = ord_item.effect_from  
                             and effect_to   = ord_item.effect_to    
                             and (ord_item.pack_item IS NULL
                               or (ord_item.pack_item IS NOT NULL
                                   AND pack_item = ord_item.pack_item));
                     --
                     -- redefault the custom entry.
                     if CE_CHARGES_SQL.INSERT_COMPS(L_error_message,
                                                    cust_ent.ce_id,
                                                    cust_ent.vessel_id,
                                                    cust_ent.voyage_flt_id,
                                                    TO_DATE(cust_ent.estimated_depart_date, 'DD-MON-RR'),
                                                    ord_item.order_no,
                                                    ord_item.item,
                                                    ord_item.pack_item,
                                                    ord_item.hts,
                                                    ord_item.import_country_id,
                                                    TO_DATE(ord_item.effect_from, 'DD-MON-RR'),
                                                    TO_DATE(ord_item.effect_to,   'DD-MON-RR')) = FALSE then
                        return FALSE;
                     end if;
                     --
                  end if;
               end loop;
               --
            else -- None of the custom entries in worksheet status.
               if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(L_error_message,
                                                   L_standard_uom,
                                                   L_standard_class,    
                                                   L_conv_factor,
                                                   ord_item.item,
                                                   'N') = FALSE then
                  return FALSE;
               end if;
               --
               -- Calculate the total of all the manifest quantities of the custom entries.
               for cust_ent in C_custom_entry(ord_item.order_no, ord_item.item) loop

                  if L_standard_uom != cust_ent.manifest_item_qty_uom then
                     if UOM_SQL.CONVERT(L_error_message, 
                                        L_std_manifest_item_qty,
                                        L_standard_uom,
                                        cust_ent.manifest_item_qty,
                                        cust_ent.manifest_item_qty_uom,
                                        ord_item.item,
                                        ord_item.supplier,
                                        ord_item.origin_country_id) = FALSE then
                        return FALSE;
                     end if;
                  else
                     L_std_manifest_item_qty := cust_ent.manifest_item_qty;
                  end if;
                  --
                  L_total_qty := L_total_qty + L_std_manifest_item_qty;
                  --
               end loop;
               --
               -- Get the order items total quenatiy.
               open C_ord_qty(ord_item.order_no, ord_item.item);
               fetch C_ord_qty into L_ordered_qty;
               close C_ord_qty;

               -- Check if the total manifest quantity is lesser than the ordered quantity.
               if L_total_qty < L_ordered_qty then

                  if UPDATE_ASSESS_RECAL_ASSESS(L_error_message,   
                                                L_ordsku_assess_log_tbl, 
                                                L_log_tbl_cnt,           
                                                L_mod_order_item_hts_tbl,
                                                L_mod_order_tbl_cnt,
                                                L_rev_orders_tbl,
                                                L_rev_ord_tbl_cnt,                                             
                                                ord_item.row_id,                
                                                ord_item.item,                  
                                                ord_item.order_no,              
                                                ord_item.order_status,
                                                ord_item.seq_no,
                                                ord_item.comp_id,
                                                ord_item.pack_item,             
                                                ord_item.hts,                   
                                                ord_item.origin_country_id,     
                                                ord_item.import_country_id,     
                                                ord_item.effect_from,           
                                                ord_item.effect_to,                               
                                                ord_item.curr_comp_rate,        
                                                ord_item.curr_per_count,        
                                                ord_item.curr_per_count_uom,    
                                                ord_item.old_comp_rate,         
                                                ord_item.old_per_count,         
                                                ord_item.old_per_count_uom,     
                                                ord_item.new_comp_rate,         
                                                ord_item.new_per_count,         
                                                ord_item.new_per_count_uom) = FALSE then
                     return FALSE;
                  end if;
                  
               else -- the ordered quantity is not less than the manifest.
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).reason_code       := 'CEVF';
                  L_log_tbl_cnt := L_log_tbl_cnt +1;
               end if;  -- end total manifest qty < ordered qty
            end if;
         else -- no custom entries present.
            close C_custom_entry;
            -- update this assessment and also the other assessments which get affected through CVB.

            if UPDATE_ASSESS_RECAL_ASSESS(L_error_message,   
                                          L_ordsku_assess_log_tbl, 
                                          L_log_tbl_cnt,           
                                          L_mod_order_item_hts_tbl,
                                          L_mod_order_tbl_cnt,
                                          L_rev_orders_tbl,
                                          L_rev_ord_tbl_cnt,                                             
                                          ord_item.row_id,                
                                          ord_item.item,                  
                                          ord_item.order_no,              
                                          ord_item.order_status,
                                          ord_item.seq_no,
                                          ord_item.comp_id,
                                          ord_item.pack_item,             
                                          ord_item.hts,                   
                                          ord_item.origin_country_id,     
                                          ord_item.import_country_id,     
                                          ord_item.effect_from,           
                                          ord_item.effect_to,                               
                                          ord_item.curr_comp_rate,        
                                          ord_item.curr_per_count,        
                                          ord_item.curr_per_count_uom,    
                                          ord_item.old_comp_rate,         
                                          ord_item.old_per_count,         
                                          ord_item.old_per_count_uom,     
                                          ord_item.new_comp_rate,         
                                          ord_item.new_per_count,         
                                          ord_item.new_per_count_uom) = FALSE then               
               return FALSE;
            end if;
         end if;
      end if; -- end if Order shipped.
   end loop; -- end loop for each selected record, for an item order.   
   --
   if L_prev_ord is not NULL and L_prevord_status = 'A' then
      if OTB_SQL.ORD_APPROVE_CASCADE(L_prev_ord,
                             O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   --
   -- Call the flush assessments function to copy the contents of the plsql tables to the respective database tables.
   if FLUSH_CHANGES_ASSESSMENTS(O_error_message,         
                                L_ordsku_assess_log_tbl,
                                L_mod_order_item_hts_tbl,
                                L_rev_orders_tbl) = FALSE then
      return FALSE;
   end if;                                
   --                                   
   delete gtt_cost_comp_upd;  -- commented for testing.
   return TRUE;
   --
EXCEPTION
   when OTHERS then
      -- delete the contents of the global temporary table.
      delete gtt_cost_comp_upd;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.UPDATE_ORDSKU_HTS_ASSESS',
                                            to_char(SQLCODE));
      return FALSE;   
END UPDATE_ORDSKU_HTS_ASSESS;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_DEPT_CHRG_DETAIL
-- Purpose: The function is called by the function PROCESS_COST_COMP_UPDATES. This function 
--          selects rows from the cost_comp_upd_stg which have the defaulting level Elc and 
--          component type as UpCharges for departments. It updates the Department upcharges.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_DEPT_CHRG_DETAIL(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN
IS

BEGIN
  
  -- Bulk insert into the global temporary table, select records from the staging
  insert into gtt_cost_comp_upd
  (       row_id,
          dept,
          from_loc,
          to_loc,
          comp_id,
          curr_comp_rate,
          curr_comp_currency,
          curr_per_count,     
          curr_per_count_uom,
          old_comp_rate,      
          old_comp_currency,
          old_per_count,
          old_per_count_uom,
          new_comp_rate,
          new_comp_currency,
          new_per_count,
          new_per_count_uom)
  (select dcd.rowid,
          dcd.dept,
          dcd.from_loc,
          dcd.to_loc,
          dcd.comp_id,
          dcd.comp_rate     curr_comp_rate,
          dcd.comp_currency curr_comp_currency,
          dcd.per_count     curr_per_count,
          dcd.per_count_uom curr_per_count_uom,
          ccu.old_comp_rate,
          ccu.old_comp_currency,
          ccu.old_per_count,
          ccu.old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_comp_currency,
          ccu.new_per_count,
          ccu.new_per_count_uom
     from dept_chrg_detail   dcd,
          cost_comp_upd_stg  ccu
    where dcd.comp_id          = ccu.comp_id
      and ccu.defaulting_level = 'E'
      and ccu.comp_type        = 'U'
      and ccu.dept_default_ind = 'Y'
      and ccu.effective_date   = L_next_vdate);
  
   insert into cost_comp_exc_log
   (        exception_type,
            dept,
            from_loc,
            to_loc,
            comp_id,
            old_comp_rate,
            old_comp_currency,
            old_per_count,    
            old_per_count_uom,
            new_comp_rate,    
            new_comp_currency,
            new_per_count,    
            new_per_count_uom,
            create_id,       
            create_datetime)            
   ( select 'D',
            dept,
            from_loc,
            to_loc,
            comp_id,          
            curr_comp_rate,
            curr_comp_currency,
            curr_per_count,    
            curr_per_count_uom,
            new_comp_rate,    
            new_comp_currency,
            new_per_count,    
            new_per_count_uom,
            user,       
            sysdate
     from   gtt_cost_comp_upd gtt
     where  gtt.old_comp_rate               != gtt.curr_comp_rate
       or   gtt.old_comp_currency           != gtt.curr_comp_currency
       or   NVL(gtt.old_per_count, -999)    != NVL(gtt.curr_per_count, -999)
       or   NVL(gtt.old_per_count_uom, -999)!= NVL(gtt.curr_per_count_uom, -999));

   -- Updating the department charge detail table, with the new component rate, currency and per count, UOM values.
   merge into dept_chrg_detail dcd
   using (select row_id,
                 new_comp_rate,
                 new_comp_currency,
                 new_per_count,
                 new_per_count_uom
            from gtt_cost_comp_upd) gtt  
      on (dcd.rowid = gtt.row_id)
   when matched then
   update
         set dcd.comp_rate         = gtt.new_comp_rate,
             dcd.comp_currency     = gtt.new_comp_currency,
             dcd.per_count         = gtt.new_per_count,
             dcd.per_count_uom     = gtt.new_per_count_uom;

   -- delete the contents of the global temporary table.
   delete gtt_cost_comp_upd;   
   return TRUE;     
   --
EXCEPTION
   when OTHERS then
      -- delete the contents of the global temporary table.
      delete gtt_cost_comp_upd;   

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.UPDATE_DEPT_CHRG_DETAIL',
                                            to_char(SQLCODE));
      return FALSE;
        
END UPDATE_DEPT_CHRG_DETAIL;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ITEM_CHRG_DETAIL
-- Purpose: The function is called by the function PROCESS_COST_COMP_UPDATES. This function 
--          selects rows from the cost_comp_upd_stg which have the defaulting level Elc and Dept and 
--          component type as UpCharges for items. It updates the Items' Department upcharges.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_CHRG_DETAIL(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_Thread_no     IN      RESTART_CONTROL.NUM_THREADS%TYPE,  
                                 I_Threads       IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN                               
IS

BEGIN
  
  -- Bulk insert into the global temporary table, select records from the staging
  insert into gtt_cost_comp_upd
  (       row_id,
          item,
          from_loc,
          to_loc,
          comp_id,
          curr_comp_rate,
          curr_comp_currency,
          curr_per_count,     
          curr_per_count_uom,
          old_comp_rate,      
          old_comp_currency,
          old_per_count,
          old_per_count_uom,
          new_comp_rate,
          new_comp_currency,
          new_per_count,
          new_per_count_uom)
  (select icd.rowid,
          icd.item,
          icd.from_loc,
          icd.to_loc,
          icd.comp_id,
          icd.comp_rate     curr_comp_rate,
          icd.comp_currency curr_comp_currency,
          icd.per_count     curr_per_count,
          icd.per_count_uom curr_per_count_uom,
          ccu.old_comp_rate,
          ccu.old_comp_currency,
          ccu.old_per_count,
          ccu.old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_comp_currency,
          ccu.new_per_count,
          ccu.new_per_count_uom
     from item_chrg_detail   icd,
          cost_comp_upd_stg  ccu
    where icd.comp_id          = ccu.comp_id
      and ccu.defaulting_level = 'E'                    -- ELC
      and ccu.item_default_ind = 'Y'                    -- defaulting to Items
      and ccu.comp_type        = 'U'                    -- Upcharges
      and ccu.effective_date   = L_next_vdate
      and mod(icd.from_loc, I_Threads)+1 = I_Thread_no
      and not exists (select 'x' 
                        from cost_comp_upd_stg ccu2,
                             item_master       im
                       where ccu2.defaulting_level = 'D'
                         and ccu2.item_default_ind = 'Y'
                         and ccu2.comp_type        = 'U'
                         and ccu2.comp_id          = ccu.comp_id
                         and ccu2.from_loc         = icd.from_loc
                         and ccu2.to_loc           = icd.to_loc
                         and ccu2.effective_date   = L_next_vdate
                         and im.item               = icd.item
                         and im.dept               = ccu2.dept)
   UNION ALL
   select icd.rowid,
          icd.item,
          icd.from_loc,
          icd.to_loc,
          icd.comp_id,
          icd.comp_rate     curr_comp_rate,
          icd.comp_currency curr_comp_currency,
          icd.per_count     curr_per_count,
          icd.per_count_uom curr_per_count_uom,
          ccu.old_comp_rate,
          ccu.old_comp_currency,
          ccu.old_per_count,
          ccu.old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_comp_currency,
          ccu.new_per_count,
          ccu.new_per_count_uom
     from item_chrg_detail   icd,
          item_master        im,
          cost_comp_upd_stg  ccu
    where icd.comp_id          = ccu.comp_id
      and icd.from_loc         = ccu.from_loc
      and icd.to_loc           = ccu.to_loc
      and ccu.defaulting_level = 'D'
      and ccu.item_default_ind = 'Y'
      and ccu.comp_type        = 'U'
      and im.item              = icd.item
      and im.dept              = ccu.dept
      and ccu.effective_date   = L_next_vdate
      and mod(icd.from_loc, I_Threads)+1 = I_Thread_no);

   insert into cost_comp_exc_log
   (        exception_type,
            item,
            from_loc,
            to_loc,
            comp_id,
            old_comp_rate,
            old_comp_currency,
            old_per_count,
            old_per_count_uom,
            new_comp_rate,
            new_comp_currency,
            new_per_count,
            new_per_count_uom,
            create_id,
            create_datetime)
   ( select 'IU',
            item,
            from_loc,
            to_loc,
            comp_id,
            curr_comp_rate,
            curr_comp_currency,
            curr_per_count,
            curr_per_count_uom,
            new_comp_rate,
            new_comp_currency,
            new_per_count,
            new_per_count_uom,
            user,
            sysdate
     from   gtt_cost_comp_upd gtt
     where  gtt.old_comp_rate               != gtt.curr_comp_rate                                 
       or   gtt.old_comp_currency           != gtt.curr_comp_currency                                   
       or   NVL(gtt.old_per_count, -999)    != NVL(gtt.curr_per_count, -999)                                    
       or   NVL(gtt.old_per_count_uom, -999)!= NVL(gtt.curr_per_count_uom, -999));

   -- Updating the department charge detail table, with the new component rate, currency and per count, UOM values.
   merge into item_chrg_detail icd
   using (select row_id,
                 new_comp_rate,
                 new_comp_currency,
                 new_per_count,
                 new_per_count_uom
            from gtt_cost_comp_upd) gtt  
      ON (icd.rowid = gtt.row_id)
   when matched then
   update
         set icd.comp_rate         = gtt.new_comp_rate,
             icd.comp_currency     = gtt.new_comp_currency,
             icd.per_count         = gtt.new_per_count,
             icd.per_count_uom     = gtt.new_per_count_uom;
             
   -- delete the contents of the global temporary table.
   delete gtt_cost_comp_upd;   
   return TRUE;

EXCEPTION
   when OTHERS then
      -- delete the contents of the global temporary table.
      delete gtt_cost_comp_upd;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.UPDATE_ITEM_CHRG_DETAIL',
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ITEM_CHRG_DETAIL;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ITEM_HTS_DETAIL
-- Purpose: The function is called by the function PROCESS_COST_COMP_UPDATES. This function 
--          selects rows from the cost_comp_upd_stg which have the defaulting level Elc and 
--          component type as Assesses. It updates the Item assessments.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_HTS_DETAIL(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN                               
IS
   cursor C_gl_temp_records
   is
   select item,
          hts,
          import_country_id,
          origin_country_id,
          effect_from,
          effect_to
   from   gtt_cost_comp_upd
   union all
   select distinct a.item,
          a.hts,
          a.import_country_id,
          a.origin_country_id,
          a.effect_from,
          a.effect_to
     from item_hts_assess a, 
          cvb_detail cd , 
          elc_comp e,
          cost_comp_upd_stg ccu
    where cd.comp_id = ccu.comp_id
      and cd.cvb_code = e.cvb_code
      and e.comp_type = 'A'
      and a.comp_id = e.comp_id
      and ccu.defaulting_level  = 'E'
      and ccu.comp_type         = 'E'
      and ccu.item_default_ind  = 'Y'
      and ccu.effective_date    = L_next_vdate;
   
   L_error_message RTK_ERRORS.RTK_TEXT%TYPE;
   L_valid BOOLEAN;
     
BEGIN
  
  -- Bulk insert into the global temporary table, select records from the staging
  insert into gtt_cost_comp_upd
  (       row_id,
          hts,
          item,
          import_country_id,
          origin_country_id,
          effect_from,
          effect_to,
          comp_id,
          curr_comp_rate,
          curr_per_count,     
          curr_per_count_uom,
          old_comp_rate,      
          old_per_count,
          old_per_count_uom,
          new_comp_rate,
          new_per_count,
          new_per_count_uom)
  (select iha.rowid,
          iha.hts,
          iha.item,
          iha.import_country_id,
          iha.origin_country_id,
          iha.effect_from,
          iha.effect_to,
          iha.comp_id,
          iha.comp_rate     curr_comp_rate,
          iha.per_count     curr_per_count,
          iha.per_count_uom curr_per_count_uom,
          ccu.old_comp_rate,
          ccu.old_per_count,
          ccu.old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_per_count,
          ccu.new_per_count_uom
     from item_hts_assess    iha,
          cost_comp_upd_stg  ccu
    where iha.comp_id           = ccu.comp_id
      and iha.import_country_id = ccu.import_country_id
      and ccu.defaulting_level  = 'E'
      and ccu.comp_type         = 'A'
      and ccu.item_default_ind  = 'Y'
      and ccu.effective_date    = L_next_vdate);
  
   insert into cost_comp_exc_log
   (        exception_type,
            item,
            hts,
            import_country_id,
            origin_country_id,
            effect_from,
            effect_to,
            comp_id,
            old_comp_rate,
            old_per_count,    
            old_per_count_uom,
            new_comp_rate,    
            new_per_count,    
            new_per_count_uom,
            create_id,       
            create_datetime)   
   ( select 'IA',
            item,
            hts,
            import_country_id,
            origin_country_id,
            effect_from,
            effect_to,
            comp_id,
            curr_comp_rate,
            curr_per_count,    
            curr_per_count_uom,
            new_comp_rate,    
            new_per_count,    
            new_per_count_uom,
            user,       
            sysdate
     from   gtt_cost_comp_upd gtt
     where  gtt.old_comp_rate                != gtt.curr_comp_rate
       or   NVL(gtt.old_per_count, -999)     != NVL(gtt.curr_per_count, -999)
       or   NVL(gtt.old_per_count_uom, -999) != NVL(gtt.curr_per_count_uom, -999));

   -- Updating the department charge detail table, with the new component rate, currency and per count, UOM values.
   merge into item_hts_assess iha
   using (select row_id,
                 new_comp_rate,
                 new_per_count,
                 new_per_count_uom
            from gtt_cost_comp_upd) gtt  
      on (iha.rowid = gtt.row_id)
   when matched then
   update
         set iha.comp_rate         = gtt.new_comp_rate,
             iha.per_count         = gtt.new_per_count,
             iha.per_count_uom     = gtt.new_per_count_uom;

   -- for each of the records in the global temporary table, Call ELC_CALC_SQL.CALC_COMP to recalculate the est_exp_value.
   for gtt_rec in C_gl_temp_records loop
      if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                'IA',
                                gtt_rec.item,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                gtt_rec.hts,
                                gtt_rec.import_country_id,
                                gtt_rec.origin_country_id, 
                                gtt_rec.effect_from,
                                gtt_rec.effect_to) = FALSE then
         return FALSE;
      end if;   
   end loop;
   --
   -- delete the contents of the global temporary table.
   delete gtt_cost_comp_upd;
   return TRUE;
   --   
EXCEPTION
   when OTHERS then
      -- delete the contents of the global temporary table.
      delete gtt_cost_comp_upd;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.UPDATE_ITEM_HTS_DETAIL',
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ITEM_HTS_DETAIL;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_TSFDETAIL_CHRG
-- The function is called by the function PROCESS_COST_COMP_UPDATES. This function 
-- selects rows from the cost_chg_upd_stg which have the defaulting level Elc, Dept and item and
--  component type as UpCharges for items. It updates the Department upcharges.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_TSFDETAIL_CHRG(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_Thread_no     IN      RESTART_CONTROL.NUM_THREADS%TYPE,
                               I_Threads       IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN                               
IS

BEGIN

   -- Bulk insert into the global temporary table, select records from the staging
  insert into gtt_cost_comp_upd
  (       row_id,
          tsf_no,
          tsf_seq_no,
          from_loc,
          to_loc,
          item,
          comp_id,
          curr_comp_rate,     
          curr_comp_currency,
          curr_per_count,
          curr_per_count_uom,
          old_comp_rate,
          old_comp_currency,
          old_per_count,
          old_per_count_uom,
          new_comp_rate,
          new_comp_currency,
          new_per_count,
          new_per_count_uom)
  (select tdc.rowid,
          tdc.tsf_no,
          tdc.tsf_seq_no,
          tdc.from_loc,
          tdc.to_loc,
          tdc.item,
          tdc.comp_id,
          tdc.comp_rate,
          tdc.comp_currency,
          tdc.per_count,
          tdc.per_count_uom,
          ccu.old_comp_rate,
          ccu.old_comp_currency,
          ccu.old_per_count,
          ccu.old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_comp_currency,
          ccu.new_per_count,
          ccu.new_per_count_uom
     from cost_comp_upd_stg ccu, 
          tsfdetail_chrg    tdc, 
          tsfhead           th 
    where ccu.comp_id = tdc.comp_id
      and ccu.defaulting_level = 'E'
      and ccu.tsf_alloc_default_ind = 'Y'
      and ccu.comp_type = 'U'
      and th.tsf_no = tdc.tsf_no
      and th.status not in ('C','D','X','S')
      and th.wf_order_no is NULL 
      and th.rma_no is NULL
      and ccu.effective_date = L_next_vdate
      and not exists (select 'x'
                        from cost_comp_upd_stg ccu2,
                             item_master       im 
                       where ccu2.defaulting_level = 'D'
                         and ccu2.comp_type        = 'U'
                         and ccu2.tsf_alloc_default_ind = 'Y'
                         and ccu2.comp_id  = ccu.comp_id
                         and ccu2.from_loc = tdc.from_loc
                         and ccu2.to_loc   = tdc.to_loc
                         and im.item       = tdc.item
                         and im.dept       = ccu2.dept
                         and ccu2.effective_date = L_next_vdate
                        UNION ALL
                      select 'x'
                        from cost_comp_upd_stg ccu2
                       where ccu2.defaulting_level = 'I'
                         and ccu2.comp_type  = 'U'
                         and ccu2.tsf_alloc_default_ind = 'Y'
                         and ccu2.comp_id = ccu.comp_id
                         and tdc.item     = ccu2.item
                         and ccu2.effective_date = L_next_vdate)
      and mod(tdc.tsf_no,I_Threads)+1 = I_Thread_no
   UNION ALL
   select tdc.rowid,
          tdc.tsf_no,
          tdc.tsf_seq_no,
          tdc.from_loc,
          tdc.to_loc,
          tdc.item,
          tdc.comp_id,
          tdc.comp_rate,
          tdc.comp_currency,
          tdc.per_count,
          tdc.per_count_uom,
          ccu.old_comp_rate,
          ccu.old_comp_currency,
          ccu.old_per_count,
          ccu.old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_comp_currency,
          ccu.new_per_count,
          ccu.new_per_count_uom
     from cost_comp_upd_stg ccu, 
          tsfdetail_chrg tdc, 
          tsfhead        th,
          item_master    im
    where ccu.comp_id = tdc.comp_id
      and ccu.defaulting_level ='D'
      and ccu.tsf_alloc_default_ind = 'Y'
      and ccu.comp_type = 'U'
      and tdc.to_loc    = ccu.to_loc 
      and tdc.from_loc  = ccu.from_loc
      and im.item       = tdc.item
      and im.dept       = ccu.dept
      and th.tsf_no     = tdc.tsf_no
      and th.status not in ('C','D','X','S')
      and th.wf_order_no is NULL
      and th.rma_no is NULL
      and ccu.effective_date = L_next_vdate
      and not exists (select 'x'
                        from cost_comp_upd_stg ccu2
                       where ccu2.defaulting_level = 'I'
                         and ccu2.comp_type        = 'U'
                         and ccu2.tsf_alloc_default_ind = 'Y'
                         and ccu2.comp_id = ccu.comp_id
                         and ccu2.item    = tdc.item
                         and ccu2.effective_date = L_next_vdate)
      and mod(tdc.tsf_no,I_Threads)+1 = I_Thread_no
   UNION ALL
   select tdc.rowid,
          tdc.tsf_no,
          tdc.tsf_seq_no,
          tdc.from_loc,
          tdc.to_loc,
          tdc.item,
          tdc.comp_id,
          tdc.comp_rate,
          tdc.comp_currency,
          tdc.per_count,
          tdc.per_count_uom,
          ccu.old_comp_rate,
          ccu.old_comp_currency,
          ccu.old_per_count,
          ccu.old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_comp_currency,
          ccu.new_per_count,
          ccu.new_per_count_uom
     from cost_comp_upd_stg ccu, 
          tsfdetail_chrg    tdc, 
          tsfhead           th 
    where tdc.comp_id  = ccu.comp_id
      and tdc.to_loc   = ccu.to_loc
      and tdc.item     = ccu.item
      and tdc.from_loc = ccu.from_loc
      and th.tsf_no = tdc.tsf_no
      and th.status not in ('C','D','X','S')
      and ccu.defaulting_level      = 'I'
      and ccu.tsf_alloc_default_ind = 'Y'
      and ccu.comp_type = 'U'
      and th.wf_order_no is NULL 
      and th.rma_no is NULL
      and ccu.effective_date = L_next_vdate
      and mod(tdc.tsf_no,I_Threads)+1 = I_Thread_no);
   

   insert into cost_comp_exc_log
          (exception_type,
           tsf_no,
           tsf_seq_no,
           from_loc,
           to_loc,
           item,
           comp_id,
           old_comp_rate,
           old_comp_currency,
           old_per_count,
           old_per_count_uom,
           new_comp_rate,
           new_comp_currency,
           new_per_count,
           new_per_count_uom,
           create_id,
           create_datetime)
   (select 'T',
           tsf_no,
           tsf_seq_no,
           from_loc,
           to_loc,
           item,
           comp_id,
           curr_comp_rate,     
           curr_comp_currency,
           curr_per_count,
           curr_per_count_uom,
           new_comp_rate,
           new_comp_currency,
           new_per_count,
           new_per_count_uom,
           user,
           sysdate 
      from gtt_cost_comp_upd gtt
     where gtt.old_comp_rate               != gtt.curr_comp_rate                                 
       or  gtt.old_comp_currency           != gtt.curr_comp_currency                                   
       or  NVL(gtt.old_per_count, -999)    != NVL(gtt.curr_per_count, -999)                                    
       or  NVL(gtt.old_per_count_uom, -999)!= NVL(gtt.curr_per_count_uom, -999));

   -- Updating the department charge detail table, with the new component rate, currency and per count, UOM values.
   merge into tsfdetail_chrg tdc
   using (select row_id,
                 new_comp_rate,
                 new_comp_currency,
                 new_per_count,
                 new_per_count_uom
            from gtt_cost_comp_upd
         ) gtt  
      ON (tdc.rowid = gtt.row_id)
   when matched then
   update
         set tdc.comp_rate         = gtt.new_comp_rate,
             tdc.comp_currency     = gtt.new_comp_currency,
             tdc.per_count         = gtt.new_per_count,
             tdc.per_count_uom     = gtt.new_per_count_uom;

   -- delete the contents of the global temporary table.
   delete gtt_cost_comp_upd;
   return TRUE;

EXCEPTION
   when OTHERS then
      -- delete the contents of the global temporary table.
      delete gtt_cost_comp_upd;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.UPDATE_TSFDETAIL_CHRG',
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_TSFDETAIL_CHRG;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ALLOC_CHRG
-- The function is called by the function PROCESS_COST_COMP_UPDATES. This function 
-- selects rows from the cost_chg_upd_stg which have the defaulting level Elc, Dept and item and
--  component type as UpCharges for items. It updates the Department upcharges.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ALLOC_CHRG(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_Thread_no     IN      RESTART_CONTROL.NUM_THREADS%TYPE,  
                           I_Threads       IN      RESTART_CONTROL.NUM_THREADS%TYPE  
                           )
RETURN BOOLEAN                               
IS

BEGIN

   -- Bulk insert into the global temporary table, select records from the staging
  insert into gtt_cost_comp_upd
  (       row_id,
          alloc_no,
          to_loc,
          item,
          comp_id,
          curr_comp_rate,     
          curr_comp_currency,
          curr_per_count,
          curr_per_count_uom,
          old_comp_rate,
          old_comp_currency,
          old_per_count,
          old_per_count_uom,
          new_comp_rate,
          new_comp_currency,
          new_per_count,
          new_per_count_uom,
          shipped_ind)
          
   with  alloc_chrg1 as
     -- select stmt queries, allocation chrg updates done at ELC comp level. 
     (select ac.rowid row_id,
             ac.alloc_no,
             ac.to_loc,
             ac.item,
             ac.comp_id,
             ac.comp_rate,
             ac.comp_currency,
             ac.per_count,
             ac.per_count_uom,
             ccu.old_comp_rate,
             ccu.old_comp_currency,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_comp_currency,
             ccu.new_per_count,
             ccu.new_per_count_uom
        from cost_comp_upd_stg ccu,
             alloc_chrg ac,
             alloc_header ah
       where ccu.comp_id = ac.comp_id
         and ccu.defaulting_level ='E'
         and ccu.tsf_alloc_default_ind = 'Y'
         and ccu.comp_type = 'U'
         and ah.alloc_no = ac.alloc_no
         and ah.status not in ('R','C','X')
         and ccu.effective_date = L_next_vdate
         and not exists (select 'x'
                           from cost_comp_upd_stg ccu2,
                                item_master       im
                          where ccu2.defaulting_level = 'D'
                            and ccu2.comp_type        = 'U'
                            and ccu2.tsf_alloc_default_ind = 'Y'
                            and ccu2.comp_id  = ccu.comp_id
                            and im.item       = ac.item
                            and im.dept       = ccu2.dept
                            and ac.to_loc     = ccu2.to_loc
                            and ah.wh         = ccu2.from_loc
                            and ccu2.effective_date = L_next_vdate
                           UNION ALL
                         select 'x'
                           from cost_comp_upd_stg ccu2
                          where ccu2.defaulting_level = 'I'
                            and ccu2.comp_type  = 'U'
                            and ccu2.tsf_alloc_default_ind = 'Y'
                            and ccu2.comp_id  = ccu.comp_id
                            and ccu2.item     = ac.item
                            and ccu2.effective_date = L_next_vdate)
         and mod(ac.alloc_no,I_Threads)+1 = I_Thread_no),
         
   alloc_chrg1a as -- data set for selected allocation which are shipped.
      (select ac.*,
               'Y'
          from alloc_chrg1 ac
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.to_loc      = ac.to_loc
                  and ss.distro_no   = ac.alloc_no
                  and ss.distro_type = 'A'
                  and ss.item = ac.item
                  and rownum =1)),
                  
   alloc_chrg2 as
      -- data set for selecting allocation charge records which get updated from update at Item charge level.
      (select ac.rowid row_id,
              ac.alloc_no,
              ac.to_loc,
              ac.item,
              ac.comp_id,
              ac.comp_rate,
              ac.comp_currency,
              ac.per_count,
              ac.per_count_uom,
              ccu.old_comp_rate,
              ccu.old_comp_currency,
              ccu.old_per_count,
              ccu.old_per_count_uom,
              ccu.new_comp_rate,
              ccu.new_comp_currency,
              ccu.new_per_count,
              ccu.new_per_count_uom
         from cost_comp_upd_stg ccu,
              alloc_chrg ac,
              alloc_header ah
        where ac.comp_id  = ccu.comp_id
          and ac.to_loc   = ccu.to_loc
          and ac.item     = ccu.item
          and ah.wh       = ccu.from_loc
          and ah.alloc_no = ac.alloc_no
          and ah.status not in ('R','C','X')
          and ccu.defaulting_level = 'I'
          and ccu.tsf_alloc_default_ind = 'Y'
          and ccu.comp_type = 'U'
          and ccu.effective_date = L_next_vdate
          and mod(ac.alloc_no, I_Threads)+1 = I_Thread_no),
          
   alloc_chrg2a as 
      (select ac.*,
               'Y'
          from alloc_chrg2 ac
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.to_loc      = ac.to_loc
                  and ss.distro_no   = ac.alloc_no
                  and ss.distro_type = 'A'
                  and ss.item = ac.item
                  and rownum =1)),
                  
   alloc_chrg3 as
      -- data set for selecting allocation charge records which get updated from update at department charge level.
      (select ac.rowid row_id,
              ac.alloc_no,
              ac.to_loc,
              ac.item,
              ac.comp_id,
              ac.comp_rate,
              ac.comp_currency,
              ac.per_count,
              ac.per_count_uom,
              ccu.old_comp_rate,
              ccu.old_comp_currency,
              ccu.old_per_count,
              ccu.old_per_count_uom,
              ccu.new_comp_rate,
              ccu.new_comp_currency,
              ccu.new_per_count,
              ccu.new_per_count_uom
         from cost_comp_upd_stg ccu,
              alloc_chrg ac,
              alloc_header ah,
              item_master  im
        where ccu.comp_id = ac.comp_id
          and ccu.defaulting_level ='D'
          and ccu.tsf_alloc_default_ind = 'Y'
          and ccu.comp_type = 'U'
          and im.item       = ac.item
          and im.dept       = ccu.dept
          and ac.to_loc = ccu.to_loc
          and ah.wh = ccu.from_loc
          and ah.alloc_no = ac.alloc_no
          and ah.status not in ('R','C','X')
          and ccu.effective_date = L_next_vdate
          and not exists (select 'x'
                            from cost_comp_upd_stg ccu2
                           where ccu2.defaulting_level = 'I'
                             and ccu2.comp_type = 'U'
                             and ccu2.item      = ac.item
                             and ccu2.tsf_alloc_default_ind = 'Y'
                             and ccu2.comp_id   = ccu.comp_id
                             and ccu2.effective_date = L_next_vdate)
          and mod(ac.alloc_no,I_Threads)+1 = I_Thread_no),
          
   alloc_chrg3a as 
      (select ac.*,
               'Y'
          from alloc_chrg3 ac
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.to_loc      = ac.to_loc
                  and ss.distro_no   = ac.alloc_no
                  and ss.distro_type = 'A'
                  and ss.item = ac.item
                  and rownum =1))
                  
   -- Obtain the complete set of allocation charge records by taking an union of all the above data sets.                  
   select alloc_chrg1a.*
     from alloc_chrg1a  
   union all
   select ac.*,
             'N'
        from alloc_chrg1 ac
       where ac.row_id not in (select aca.row_id
                                 from alloc_chrg1a aca)                              
   union all
   select alloc_chrg2a.*
     from alloc_chrg2a  
   union all
   select ac.*,
             'N'
        from alloc_chrg2 ac
       where ac.row_id not in (select aca.row_id
                                 from alloc_chrg2a aca)
   union all
   select alloc_chrg3a.*
     from alloc_chrg3a  
   union all
   select ac.*,
             'N'
        from alloc_chrg3 ac
       where ac.row_id not in (select aca.row_id
                                       from alloc_chrg3a aca);
                
   insert into cost_comp_exc_log 
          (exception_type,
           alloc_no,
           to_loc,
           item,
           comp_id,
           old_comp_rate,
           old_comp_currency,
           old_per_count,
           old_per_count_uom,
           new_comp_rate,
           new_comp_currency,
           new_per_count,
           new_per_count_uom,
           create_id,
           create_datetime,
           reason_code)
   (select 'A',
           alloc_no,
           to_loc,
           item,
           comp_id,
           curr_comp_rate,     
           curr_comp_currency,
           curr_per_count,
           curr_per_count_uom,
           new_comp_rate,
           new_comp_currency,
           new_per_count,
           new_per_count_uom,    
           user,
           sysdate,
           decode(shipped_ind, 'Y', 'ALOCS', 'ALOCU')
      from gtt_cost_comp_upd gtt
     where gtt.old_comp_rate               != gtt.curr_comp_rate                                 
       or  gtt.old_comp_currency           != gtt.curr_comp_currency                                   
       or  NVL(gtt.old_per_count, -999)    != NVL(gtt.curr_per_count, -999)
       or  NVL(gtt.old_per_count_uom, -999)!= NVL(gtt.curr_per_count_uom, -999)
       or  shipped_ind = 'Y');


   -- Updating the department charge detail table, with the new component rate, currency and per count, UOM values.
   merge into alloc_chrg ac
   using (select row_id,
                 new_comp_rate,
                 new_comp_currency,
                 new_per_count,
                 new_per_count_uom,
                 shipped_ind
            from gtt_cost_comp_upd) gtt
      ON (ac.rowid = gtt.row_id 
          and gtt.shipped_ind = 'N')
   when matched then
   update
         set ac.comp_rate         = gtt.new_comp_rate,
             ac.comp_currency     = gtt.new_comp_currency,
             ac.per_count         = gtt.new_per_count,
             ac.per_count_uom     = gtt.new_per_count_uom;
             
   -- delete the contents of the global temporary table.
   delete gtt_cost_comp_upd;   
   return TRUE;

EXCEPTION
   when OTHERS then
      -- delete the contents of the global temporary table.
      delete gtt_cost_comp_upd;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'COST_COMP_UPD_SQL.UPDATE_ALLOC_CHRG',
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ALLOC_CHRG;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ITEM_EXP_DETAIL
-- The function is called by the function PROCESS_COST_COMP_UPDATES. This function 
-- selects rows from the cost_chg_upd_stg and updates will happen when the elc component or the 
-- expense profile is changed and defaulted to the item level. The change at the expense profile 
-- holds higher precedence over the change in the elc component.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_EXP_DETAIL(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_Thread_no     IN      RESTART_CONTROL.NUM_THREADS%TYPE,  
                                I_Threads       IN      RESTART_CONTROL.NUM_THREADS%TYPE  
                                )
RETURN BOOLEAN                               
IS
   
   cursor C_gl_temp_records is
      select item,
             supplier,
             item_exp_type,
             item_exp_seq,
             DECODE(item_exp_type,'C',origin_country_id,null) origin_country_id
        from gtt_cost_comp_upd;
        
   L_cost_event_id      COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_elc_changes        OBJ_ELC_COST_EVENT_TBL   :=   OBJ_ELC_COST_EVENT_TBL();
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

  insert into gtt_cost_comp_upd
  (       row_id,
          item,
          supplier,
          origin_country_id,
          item_exp_type,
          item_exp_seq,
          comp_id,
          curr_comp_rate,     
          curr_comp_currency,
          curr_per_count,
          curr_per_count_uom,
          old_comp_rate,
          old_comp_currency,
          old_per_count,
          old_per_count_uom,
          new_comp_rate,
          new_comp_currency,
          new_per_count,
          new_per_count_uom,
          zone_id,
          zone_group_id)
  (select ied.rowid,
          ied.item,
          ied.supplier,
          ieh.origin_country_id,
          ied.item_exp_type, 
          ied.item_exp_seq,
          ied.comp_id,
          ied.comp_rate,
          ied.comp_currency,
          ied.per_count,
          ied.per_count_uom,
          ccu.old_comp_rate,
          ccu.old_comp_currency,
          ccu.old_per_count,
          ccu.old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_comp_currency,
          ccu.new_per_count,
          ccu.new_per_count_uom,
          ccu.zone_id,
          ccu.zone_group_id
     from exp_prof_head eph, 
          item_exp_detail ied, 
          item_exp_head ieh,
          cost_comp_upd_stg ccu
    where ccu.exp_prof_key   = eph.exp_prof_key
      and ied.comp_id        = ccu.comp_id
      and ied.defaulted_from = 'S'
      and ied.key_value_1    = eph.key_value_1      
      and ied.item           = ieh.item    
      and ied.supplier       = ieh.supplier
      and ied.item_exp_type  = ieh.item_exp_type
      and ied.item_exp_seq   = ieh.item_exp_seq
      and ((ieh.origin_country_id  = ccu.origin_country_id
           and NVL(ieh.lading_port,'-999') = NVL(ccu.lading_port,'-999')
           and ieh.discharge_port = NVL(ccu.discharge_port,'-999'))
           or (ieh.zone_id = ccu.zone_id and ieh.zone_group_id = ccu.zone_group_id))
      and eph.module             = 'SUPP'
      and eph.exp_prof_type      = ied.item_exp_type
      and ccu.defaulting_level   = 'S'
      and ccu.comp_type          = 'E'
      and ccu.item_default_ind   = 'Y'
      and ccu.effective_date     = L_next_vdate  
      and mod(ied.supplier,I_Threads)+1 = I_Thread_no
   UNION ALL
   select ied.rowid,
          ied.item,
          ied.supplier,
          ieh.origin_country_id,
          ied.item_exp_type, 
          ied.item_exp_seq,
          ied.comp_id,
          ied.comp_rate,
          ied.comp_currency,
          ied.per_count,
          ied.per_count_uom,
          ccu.old_comp_rate,
          ccu.old_comp_currency,
          ccu.old_per_count,
          ccu.old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_comp_currency,
          ccu.new_per_count,
          ccu.new_per_count_uom,
          ccu.zone_id,
          ccu.zone_group_id          
     from exp_prof_head eph, 
          item_exp_detail ied, 
          item_exp_head ieh,
          cost_comp_upd_stg ccu
    where ccu.exp_prof_key = eph.exp_prof_key
      and ied.comp_id = ccu.comp_id
      and ied.defaulted_from = 'P'
      and ied.key_value_1 = eph.key_value_1
      and ied.key_value_2 = eph.key_value_2
      and ied.item            = ieh.item
      and ied.supplier        = ieh.supplier
      and ied.item_exp_type   = ieh.item_exp_type
      and ied.item_exp_seq    = ieh.item_exp_seq
      and ((ieh.origin_country_id  = ccu.origin_country_id
           and NVL(ieh.lading_port,'-999') = NVL(ccu.lading_port,'-999')
           and ieh.discharge_port = NVL(ccu.discharge_port,'-999'))
           or (ieh.zone_id = ccu.zone_id and ieh.zone_group_id = ccu.zone_group_id))
      and eph.module          = 'PTNR'
      and eph.exp_prof_type   = ied.item_exp_type
      and ccu.defaulting_level = 'P'
      and ccu.comp_type        = 'E'
      and item_default_ind     = 'Y'
      and ccu.effective_date   = L_next_vdate
    and mod(ied.supplier,I_Threads)+1 = I_Thread_no
   UNION ALL
   select ied.rowid,
          ied.item,
          ied.supplier,
          ieh.origin_country_id,
          ied.item_exp_type, 
          ied.item_exp_seq,
          ied.comp_id,
          ied.comp_rate,
          ied.comp_currency,
          ied.per_count,
          ied.per_count_uom,
          ccu.old_comp_rate,
          ccu.old_comp_currency,
          ccu.old_per_count,
          ccu.old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_comp_currency,
          ccu.new_per_count,
          ccu.new_per_count_uom,
          ccu.zone_id,
          ccu.zone_group_id          
     from exp_prof_head   eph, 
          item_exp_detail ied, 
          item_exp_head ieh, 
          cost_comp_upd_stg ccu
    where ccu.exp_prof_key = eph.exp_prof_key
      and ied.comp_id      = ccu.comp_id
      and ied.defaulted_from  = 'C'
      and ied.key_value_1     = eph.origin_country_id
      and ied.item            = ieh.item
      and ied.supplier        = ieh.supplier
      and ied.item_exp_type   = 'C'
      and ied.item_exp_seq    = ieh.item_exp_seq 
      and eph.module          = 'CTRY'
      and eph.exp_prof_type     = ied.item_exp_type
      and eph.origin_country_id = ieh.origin_country_id
      and eph.lading_port = ieh.lading_port
      and eph.discharge_port = ieh.discharge_port
      and ccu.defaulting_level  = 'C'
      and ccu.comp_type         = 'E'
      and item_default_ind      = 'Y'
      and ccu.effective_date    = L_next_vdate
      and mod(ied.supplier,I_Threads)+1 = I_Thread_no
   UNION ALL
   select ied.rowid,
          ied.item,
          ied.supplier,
          ieh.origin_country_id,
          ied.item_exp_type, 
          ied.item_exp_seq,
          ied.comp_id,
          ied.comp_rate,
          ied.comp_currency,
          ied.per_count,
          ied.per_count_uom,
          ccu.old_comp_rate,
          ccu.old_comp_currency,
          ccu.old_per_count,
          ccu.old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_comp_currency,
          ccu.new_per_count,
          ccu.new_per_count_uom,
          ccu.zone_id,
          ccu.zone_group_id          
     from item_exp_detail   ied,
          item_exp_head     ieh,
          cost_comp_upd_stg ccu
    where ccu.comp_id  = ied.comp_id
      and ied.item     = ieh.item
      and ied.supplier = ieh.supplier
      and ied.item_exp_type    = ieh.item_exp_type
      and ied.item_exp_seq     = ieh.item_exp_seq
      and ccu.defaulting_level = 'E'
      and ccu.comp_type        = 'E'
      and item_default_ind     = 'Y'
      and ccu.effective_date   = L_next_vdate
      and not exists (select 'x'              
                        from cost_comp_upd_stg ccu2,               
                             exp_prof_head     eph                       
                       where ccu2.defaulting_level in ('S','P','C')
                         and ccu2.comp_type          = 'E'
                         and ccu2.item_default_ind   = 'Y'
                         and ccu2.comp_id            = ccu.comp_id
                         and ccu2.exp_prof_key       = eph.exp_prof_key
                         and ((ied.defaulted_from    = 'S'
                              and eph.key_value_1    = ied.key_value_1
                              and ((ieh.origin_country_id = ccu2.origin_country_id
                                   and NVL(ieh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                   and ieh.discharge_port = NVL(ccu2.discharge_port,'-999'))
                                  or (ieh.zone_id = ccu2.zone_id and ieh.zone_group_id = ccu2.zone_group_id)))                          
                          or
                            (ied.defaulted_from    = 'P'
                              and eph.key_value_1  = ied.key_value_1
                              and eph.key_value_2  = ied.key_value_2
                              and ((ieh.origin_country_id = ccu2.origin_country_id
                                   and NVL(ieh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                   and ieh.discharge_port = NVL(ccu2.discharge_port,'-999'))                              
                                  or (ieh.zone_id = ccu2.zone_id and ieh.zone_group_id = ccu2.zone_group_id)))
                          or (ied.defaulted_from   = 'C'
                              and eph.key_value_1  = ied.key_value_1))
                         and ccu2.effective_date   = L_next_vdate)
      and mod(ied.supplier,I_Threads)+1 = I_Thread_no);


   insert into cost_comp_exc_log 
          (exception_type,
           item,
           supplier,
           item_exp_type,
           item_exp_seq,
           comp_id,           
           old_comp_rate,
           old_comp_currency,
           old_per_count,
           old_per_count_uom,
           new_comp_rate,
           new_comp_currency,
           new_per_count,
           new_per_count_uom,
           create_id,
           create_datetime)
   (select 'IE',
           item,
           supplier,
           item_exp_type,
           item_exp_seq,
           comp_id,
           curr_comp_rate,     
           curr_comp_currency,
           curr_per_count,
           curr_per_count_uom,
           new_comp_rate,
           new_comp_currency,
           new_per_count,
           new_per_count_uom,
           user,
           sysdate
      from gtt_cost_comp_upd gtt
     where gtt.old_comp_rate               != gtt.curr_comp_rate                                 
       or  gtt.old_comp_currency           != gtt.curr_comp_currency                                   
       or  NVL(gtt.old_per_count, -999)    != NVL(gtt.curr_per_count, -999)                                    
       or  NVL(gtt.old_per_count_uom, -999)!= NVL(gtt.curr_per_count_uom, -999));
   

   merge into item_exp_detail ied
   using (select row_id,
                 new_comp_rate,
                 new_comp_currency,
                 new_per_count,
                 new_per_count_uom
            from gtt_cost_comp_upd) gtt  
      ON (ied.rowid = gtt.row_id)
   when matched then
   update
         set ied.comp_rate            = gtt.new_comp_rate,
             ied.comp_currency        = gtt.new_comp_currency,
             ied.per_count            = gtt.new_per_count,
             ied.per_count_uom        = gtt.new_per_count_uom;

   -- for each of the records in the global temporary table, Call ELC_CALC_SQL.CALC_COMP to recalculate the est_exp_value.
   for gtt_rec in C_gl_temp_records loop
      if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                'IE',
                                gtt_rec.item,
                                gtt_rec.supplier,
                                gtt_rec.item_exp_type,
                                gtt_rec.item_exp_seq,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                gtt_rec.origin_country_id,
                                NULL,
                                NULL) = FALSE then
         return FALSE;
      end if;
   end loop;
   
   if L_system_options.elc_ind = 'Y' and 
      (L_system_options.elc_inclusive_ind_comp_store = 'Y' or L_system_options.elc_inclusive_ind_wf_store = 'Y') then

      select OBJ_ELC_COST_EVENT_REC(item,
                                    supplier,
                                    origin_country_id,
                                    zone_group_id,
                                    zone_id)
                  bulk collect into L_elc_changes                                                                     
                                    from (select distinct ccu.item,
                                                 ccu.supplier,
                                                 CASE 
                                                    WHEN ccu.item_exp_type = 'C' then ccu.origin_country_id
                                                    ELSE NULL
                                                 END origin_country_id,
                                                 CASE
                                                    WHEN ccu.item_exp_type =  'Z' then ccu.zone_group_id
                                                    ELSE NULL
                                                 END zone_group_id,
                                                 CASE
                                                    WHEN ccu.item_exp_type = 'Z' then ccu.zone_id
                                                    ELSE NULL
                                                 END zone_id
                                            from gtt_cost_comp_upd ccu);
                                         

      if L_elc_changes.COUNT > 0 then
          if FUTURE_COST_EVENT_SQL.ADD_ELC_EVENTS(L_error_message,
                                                  L_cost_event_id,
                                                  L_elc_changes,
                                                  USER,
                                                  FUTURE_COST_EVENT_SQL.BATCH_COST_EVENT_RUN_TYPE) = FALSE then
             return FALSE;
          end if;
      end if;
   end if;
   ---                                            
   
   -- delete the contents of the global temporary table.
   delete gtt_cost_comp_upd;  
   return TRUE;

EXCEPTION
   when OTHERS then
      -- delete the contents of the global temporary table.
      delete gtt_cost_comp_upd;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.UPDATE_ITEM_EXP_DETAIL',
                                            to_char(SQLCODE));
      return FALSE;
      
END UPDATE_ITEM_EXP_DETAIL;
---------------------------------------------------------------------------------------------
-- Function Name: PROCESS_ITEM_COST_COMP_UPDATES
-- Purpose: The function is called by a batch/shell script which is scheduled to run at the end of the day.
--          The function calls the functions to cascade the HTS, EXPENSES and UPCHARGES to the item.
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_COST_COMP_UPDATES (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_Thread_no              IN      RESTART_CONTROL.NUM_THREADS%TYPE,
                                         I_Threads                IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN
IS
   
   cursor C_sys_opts
   is 
   select *
     from system_options;

BEGIN
   
   L_vdate := get_vdate();

   open  C_sys_opts;
   fetch C_sys_opts into L_system_options;
   close C_sys_opts;
   
   -- Update Item expense tables which is cascaded from ELC and Expense profile form.
   if  UPDATE_ITEM_EXP_DETAIL(O_error_message,   
                              I_Thread_no,       
                              I_Threads) = FALSE then
      return FALSE;
   end if;   

   if L_system_options.import_ind = 'Y' then
      -- Update Item assessment tables which is cascaded from ELC comp form.
      if  UPDATE_ITEM_HTS_DETAIL(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   -- Update Item upcharge which is cascaded from ELC comp form and from Department charge form.
   if  UPDATE_ITEM_CHRG_DETAIL(O_error_message,  
                               I_Thread_no,      
                               I_Threads) = FALSE then
      return FALSE;
   end if;   

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.PROCESS_ITEM_COST_COMP_UPDATES',
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_ITEM_COST_COMP_UPDATES;
---------------------------------------------------------------------------------------------
-- Function Name: PROCESS_ORD_COST_COMP_UPDATES
-- Purpose: The function is called by a batch/shell script which is scheduled to run at the end of the day.
--          The function calls the functions to cascade the HTS, EXPENSES to the orders.
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_ORD_COST_COMP_UPDATES (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_Thread_no              IN      RESTART_CONTROL.NUM_THREADS%TYPE,
                                        I_Threads                IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN
IS
   cursor C_sys_opts
   is 
   select *
     from system_options;
   
BEGIN
   
   open  C_sys_opts;
   fetch C_sys_opts into L_system_options;
   close C_sys_opts;
   
   L_vdate := get_vdate();
   
   -- Update Order expenses which is casacaded from ELC comp, Expense profile and Item expense forms.
   if  UPDATE_ORDLOC_EXP(O_error_message,        
                         I_Thread_no,            
                         I_Threads) = FALSE then
      return FALSE;
   end if;   
 
   if L_system_options.import_ind = 'Y' then
      -- Update Order Assessments which is casacaded from ELC comp, and Item HTS assessments forms.
      if  UPDATE_ORDSKU_HTS_ASSESS(O_error_message, 
                                   I_Thread_no,     
                                   I_Threads) = FALSE then
         return FALSE;
      end if;   
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.PROCESS_ORD_COST_COMP_UPDATES',
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_ORD_COST_COMP_UPDATES;
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_TSF_ALLOC_CHRG
-- Purpose: The function is called by a batch/shell script which is scheduled to run at the end of the day.
--          The function calls the functions to cascade the Upcharges chnages to tsfs and allocs.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_TSF_ALLOC_CHRG (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_Thread_no              IN      RESTART_CONTROL.NUM_THREADS%TYPE,
                                I_Threads                IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN
IS
   
BEGIN
   
   L_vdate := get_vdate();
   
   -- Update Item expense tables which is cascaded from ELC and Expense profile form.
   if  UPDATE_ALLOC_CHRG(O_error_message,   
                         I_Thread_no,       
                         I_Threads) = FALSE then
      return FALSE;
   end if;   

   -- Update Item assessment tables which is cascaded from ELC comp form.
   if  UPDATE_TSFDETAIL_CHRG(O_error_message,   
                             I_Thread_no,       
                             I_Threads) = FALSE then
      return FALSE;
   end if;   

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.PROCESS_ITEM_COST_COMP_UPDATES',
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_TSF_ALLOC_CHRG;
---------------------------------------------------------------------------------------------
-- Function Name: GET_DEFAULT_IND
-- Purpose: This function retrieves the cascade indicator values from the staging table,
--          depending on the values passed to the function.
---------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_IND (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_defaulting_level       IN      COST_COMP_UPD_STG.DEFAULTING_LEVEL%TYPE,
                          I_dept                   IN      DEPS.DEPT%TYPE,
                          I_comp_id                IN      ELC_COMP.COMP_ID%TYPE,
                          I_comp_type              IN      ELC_COMP.COMP_TYPE%TYPE,
                          I_supplier               IN      SUPS.SUPPLIER%TYPE,
                          I_item                   IN      ITEM_MASTER.ITEM%TYPE,
                          I_item_exp_type          IN      ITEM_EXP_DETAIL.ITEM_EXP_TYPE%TYPE,
                          I_item_exp_seq           IN      ITEM_EXP_DETAIL.ITEM_EXP_SEQ%TYPE,
                          I_hts                    IN      HTS.HTS%TYPE,
                          I_import_country_id      IN      HTS.IMPORT_COUNTRY_ID%TYPE,
                          I_origin_country_id      IN      HTS.IMPORT_COUNTRY_ID%TYPE,
                          I_effect_from            IN      DATE,
                          I_effect_to              IN      DATE,
                          I_exp_prof_key           IN      EXP_PROF_HEAD.EXP_PROF_KEY%TYPE,
                          I_from_loc               IN      ITEM_LOC.LOC%TYPE,
                          I_to_loc                 IN      ITEM_LOC.LOC%TYPE,
                          O_cntry_default_ind      IN OUT  COST_COMP_UPD_STG.CNTRY_DEFAULT_IND%TYPE,
                          O_supp_default_ind       IN OUT  COST_COMP_UPD_STG.SUPP_DEFAULT_IND%TYPE,
                          O_ptnr_default_ind       IN OUT  COST_COMP_UPD_STG.PTNR_DEFAULT_IND%TYPE,
                          O_item_default_ind       IN OUT  COST_COMP_UPD_STG.ITEM_DEFAULT_IND%TYPE,
                          O_order_default_ind      IN OUT  COST_COMP_UPD_STG.ORDER_DEFAULT_IND%TYPE,
                          O_tsf_alloc_default_ind  IN OUT  COST_COMP_UPD_STG.TSF_ALLOC_DEFAULT_IND%TYPE,
                          O_dept_default_ind       IN OUT  COST_COMP_UPD_STG.DEPT_DEFAULT_IND%TYPE)
RETURN BOOLEAN
IS 
   -- Definition of the cursor to fetch all the cascading indicator values based on the parameter values.
   cursor C_default_indicators
   is
   select cntry_default_ind,
          supp_default_ind,
          ptnr_default_ind,
          item_default_ind,
          order_default_ind,
          tsf_alloc_default_ind,
          dept_default_ind
    from  cost_comp_upd_stg
   where  comp_id          = I_comp_id
     and  comp_type        = I_comp_type
     and  defaulting_level = I_defaulting_level
     and (dept             = I_dept     or I_dept       is NULL)
     and (supplier         = I_supplier or I_supplier   is NULL)
     and (item             = I_item     or I_item       is NULL) 
     and (item_exp_type    = I_item_exp_type or I_item_exp_type is NULL)
     and (item_exp_seq     = I_item_exp_seq  or I_item_exp_seq  is NULL)
     and (hts              = I_hts           or I_hts is NULL)
     and (import_country_id= I_import_country_id or I_import_country_id is NULL)
     and (origin_country_id= I_origin_country_id or I_origin_country_id is NULL)
     and (effect_from      = I_effect_from       or I_effect_from       is NULL)
     and (effect_to        = I_effect_to         or I_effect_to         is NULL)
     and (exp_prof_key     = I_exp_prof_key      or I_exp_prof_key      is NULL)
     and (from_loc         = I_from_loc          or I_from_loc          is NULL)
     and (to_loc           = I_to_loc            or I_to_loc            is NULL);   

BEGIN
   
   -- Validate the input parameter values.
   if I_defaulting_level  is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_defaulting_level',
                                            'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                            NULL);
      return FALSE;
   end if;

   if I_comp_id  is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_comp_id',
                                            'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                            NULL);
      return FALSE;
   end if;
   
   if I_comp_type  is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_comp_type',
                                            'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                            NULL);
      return FALSE;
   end if;
   
   -- check the defaulting level parameter values.
   if I_defaulting_level not in ('S', 'P', 'I', 'C', 'E', 'D') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   if I_comp_type not in ('U', 'A', 'E') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   -- The expense profile key is required when this function is called from the Expense profile form.
   if I_defaulting_level in ('S', 'P', 'C') and 
     (I_exp_prof_key is NULL or I_comp_type    != 'E') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   -- Check for the required values when this function is called from the Department Up charge form.
   if I_defaulting_level = 'D' then
      if I_comp_type != 'U' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                               'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      --
      if I_dept     is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_dept',
                                               'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                               NULL);
         return FALSE;
      end if;
      
      if I_from_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_from_loc',
                                               'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                               NULL);
         return FALSE;
      end if;
      
      if I_to_loc   is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_to_loc',
                                               'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                               NULL);
         return FALSE;
      end if;
   end if;  -- I_defaulting_level = 'D'
   
   -- Check for the required values when this function is called from the forms Item expense, Item Upcharge or Item HTS assessment.
   if I_defaulting_level = 'I' then
      --
      if I_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_item',
                                               'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                                NULL);
         return FALSE;
      end if;
      --
      -- required values when called from Item Expense
      if I_comp_type = 'E' then 
         if I_item_exp_type is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_item_exp_type',
                                                  'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                                  NULL);
            return FALSE;
         end if;
         
         if I_item_exp_seq is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_item_exp_seq',
                                                  'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                                  NULL);
            return FALSE;
         end if;
         
         if I_supplier is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_supplier',
                                                  'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                                  NULL);
            return FALSE;
         end if;
      end if;
      --
      -- required values when called from Item charge
      if I_comp_type = 'U' then 
         if I_from_loc is NULL  then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_from_loc',
                                                  'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                                  NULL);
            return FALSE;
         end if;

         if I_to_loc   is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_to_loc',
                                                  'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                                  NULL);
            return FALSE;
         end if;

      end if;
      --
      -- required values when called from Item HTS assessment.
      if I_comp_type = 'A' then 
         if I_hts               is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_comp_type',
                                                  'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                                  NULL);
            return FALSE;
         end if;
         if I_import_country_id is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_import_country_id',
                                                  'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                                  NULL);
            return FALSE;
         end if;
         if I_origin_country_id is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_origin_country_id',
                                                  'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                                  NULL);
            return FALSE;
         end if;
         if I_effect_from       is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_effect_from',
                                                  'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                                  NULL);
            return FALSE;
         end if;
         if I_effect_to         is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_effect_to',
                                                  'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                                  NULL);
            return FALSE;
         end if;
      end if;
   end if;  -- if I_defaulting_level = 'I'
   
   -- After having validated the paramters, open cursor to fetch the indicator values. -- Fetch directly into the OUT parameters.
   open C_default_indicators;
   fetch C_default_indicators into O_cntry_default_ind, 
                                   O_supp_default_ind,
                                   O_ptnr_default_ind,
                                   O_item_default_ind,
                                   O_order_default_ind,
                                   O_tsf_alloc_default_ind,
                                   O_dept_default_ind;
   
   if SQL%ROWCOUNT > 1 then
      close C_default_indicators;
      O_error_message := 'INV_PARAMS';
      return FALSE;
   end if;
   close C_default_indicators;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.GET_DEFAULT_IND',
                                            to_char(SQLCODE));
   return FALSE;

END GET_DEFAULT_IND;

---------------------------------------------------------------------------------------------
-- Function Name: DEFAULT_PARENT_CHRGS
-- Purpose: For every item whose charges are modified, insert a record for the child in the 
--          cost_comp_upd_stg table.
---------------------------------------------------------------------------------------------
FUNCTION DEFAULT_PARENT_CHRGS (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_Item              IN      ITEM_MASTER.ITEM%TYPE,
                               I_comp_type         IN      COST_COMP_UPD_STG.COMP_TYPE%TYPE
                               )
RETURN BOOLEAN 
IS
   -- Cursor to fetch the chid items which already have upcharges associated with them.
   cursor C_children_upchrgs
   is
   select im.item,
          ccu.from_loc,
          ccu.to_loc,
          ccu.comp_id,
          icd.comp_rate old_comp_rate,
          icd.comp_currency old_comp_currency,
          icd.per_count old_per_count,
          icd.per_count_uom old_per_count_uom,
          ccu.new_comp_rate,
          ccu.new_comp_currency,
          ccu.new_per_count,
          ccu.new_per_count_uom,
          ccu.tsf_alloc_default_ind
     from item_chrg_detail icd,
          cost_comp_upd_stg ccu,
          item_master im
    where (im.item_parent = I_Item
           or im.item_grandparent = I_Item)
       and ccu.item = I_item
       and ccu.comp_type = I_comp_type
       and ccu.defaulting_level = 'I'
       and icd.item = im.item
       and icd.from_loc = ccu.from_loc
       and icd.to_loc = ccu.to_loc
       and icd.comp_id = ccu.comp_id
       order by im.item;
   
   --Cursor to fetch the chid items which already have expenses associated with them.
   cursor C_children_exps
   is
   select ied.item,
           ied.supplier,
           ied.item_exp_type,
           ied.item_exp_seq,
           ied.comp_id,
           ccu.origin_country_id,
           ccu.zone_id,
           ccu.zone_group_id,
           ied.comp_rate old_comp_rate,
           ied.comp_currency old_comp_currency,
           ied.per_count old_per_count,
           ied.per_count_uom old_per_count_uom,
           ccu.new_comp_rate,
           ccu.new_comp_currency,
           ccu.new_per_count,
           ccu.new_per_count_uom,
           ccu.order_default_ind,
           ccu.lading_port,
           ccu.discharge_port
      from item_exp_detail ied,
           item_exp_head ieh,
           cost_comp_upd_stg ccu,
           item_master im
    where (im.item_parent = I_Item
           or im.item_grandparent = I_Item)
       and ccu.item = I_Item
       and ccu.comp_type = I_comp_type
       and ccu.defaulting_level  = 'I'   
       and ied.item              = im.item 
       and ieh.item              = ied.item
       and ied.supplier          = ccu.supplier
       and ied.item_exp_type     = ccu.item_exp_type
       and ied.item_exp_seq      = ccu.item_exp_seq
       and ied.comp_id           = ccu.comp_id 
       and ied.item_exp_type     = ieh.item_exp_type
       and ied.item_exp_seq      = ieh.item_exp_seq
       and ied.supplier          = ieh.supplier 
       and (ccu.origin_country_id is NULL or
           (ieh.origin_country_id = ccu.origin_country_id
            and NVL(ieh.lading_port,'-999') = NVL(ccu.lading_port,'-999')
            and ieh.discharge_port = NVL(ccu.discharge_port,'-999')))
       order by im.item;  
       
   --Cursor to fetch the chid items which already have Assessments associated with them    
   cursor C_children_assessments
       is
       select iha.item,
              iha.comp_id,
              iha.hts,
              ccu.import_country_id,
              ccu.origin_country_id,
              ccu.effect_from,
              ccu.effect_to,
              iha.comp_rate old_comp_rate,
              iha.per_count old_per_count,
              iha.per_count_uom old_per_count_uom,
              ccu.new_comp_rate,
              ccu.new_per_count,
              ccu.new_per_count_uom,
              ccu.order_default_ind
         from item_hts_assess iha,
              cost_comp_upd_stg ccu,
              item_master im
       where (im.item_parent = I_Item
              or im.item_grandparent = I_Item)
          and ccu.item =  I_Item
          and ccu.comp_type = I_comp_type
          and ccu.defaulting_level  = 'I'    
          and ccu.hts = iha.hts
          and iha.item = im.item   
          and ccu.comp_id = iha.comp_id
          and ccu.origin_country_id = iha.origin_country_id
          and ccu.import_country_id = iha.import_country_id
          and ccu.effect_from = iha.effect_from
          and ccu.effect_to = iha.effect_to 
       order by im.item; 
      
BEGIN 
   if I_comp_type = 'U' then 
      For child_rec in C_children_upchrgs loop 
          if COST_COMP_UPD_SQL.SAVE_UPDATES(O_error_message,
                                            NULL,
                                            'I',
                                            L_next_vdate,
                                            child_rec.comp_id,
                                            'U',
                                            NULL,
                                            NULL,
                                            child_rec.item,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            child_rec.from_loc,
                                            child_rec.to_loc,
                                            NULL,
                                            NULL,
                                            child_rec.old_comp_rate,
                                            child_rec.old_comp_currency,
                                            child_rec.old_per_count,
                                            child_rec.old_per_count_uom,
                                            child_rec.new_comp_rate,
                                            child_rec.new_comp_currency,
                                            child_rec.new_per_count,
                                            child_rec.new_per_count_uom,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            child_rec.tsf_alloc_default_ind,
                                            NULL) = FALSE then
             return FALSE;
          end if;
      end loop;
      return TRUE;
      
   elsif I_comp_type = 'E' then 
   
      For child_rec in C_children_exps loop   
          if COST_COMP_UPD_SQL.SAVE_UPDATES(O_error_message,
                                            NULL,
                                            'I',
                                            L_next_vdate,
                                            child_rec.comp_id,
                                            'E',
                                            NULL,
                                            child_rec.supplier,
                                            child_rec.item,
                                            child_rec.item_exp_type,
                                            child_rec.item_exp_seq,
                                            NULL,
                                            NULL,
                                            child_rec.origin_country_id,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            child_rec.zone_group_id,
                                            child_rec.zone_id,
                                            child_rec.old_comp_rate,
                                            child_rec.old_comp_currency,
                                            child_rec.old_per_count,
                                            child_rec.old_per_count_uom,
                                            child_rec.new_comp_rate,
                                            child_rec.new_comp_currency,
                                            child_rec.new_per_count,
                                            child_rec.new_per_count_uom,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            child_rec.order_default_ind,
                                            NULL,
                                            NULL,
                                            child_rec.lading_port,
                                            child_rec.discharge_port) = FALSE then
              return FALSE;
          end if;
      end loop;
      return TRUE;
      
   elsif I_comp_type = 'A' then
   
      For child_rec in C_children_assessments loop   
          if COST_COMP_UPD_SQL.SAVE_UPDATES(O_error_message,
                                            NULL,
                                            'I',
                                            L_next_vdate,
                                            child_rec.comp_id,
                                            'A',
                                            NULL,
                                            NULL,
                                            child_rec.item,
                                            NULL,
                                            NULL,
                                            child_rec.hts,
                                            child_rec.import_country_id,
                                            child_rec.origin_country_id,
                                            child_rec.effect_from,
                                            child_rec.effect_to,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            child_rec.old_comp_rate,
                                            NULL,
                                            child_rec.old_per_count,
                                            child_rec.old_per_count_uom,
                                            child_rec.new_comp_rate,
                                            NULL,
                                            child_rec.new_per_count,
                                            child_rec.new_per_count_uom,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            child_rec.order_default_ind,
                                            NULL,
                                            NULL) = FALSE then
                    return FALSE;
                end if;
            end loop;
      return TRUE;
   end if;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.DEFAULT_PARENT_CHRGS',
                                            to_char(SQLCODE));
      return FALSE;       
END DEFAULT_PARENT_CHRGS;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_COST_COMP
-- The function is called by the cost component forms, whenever a component is deleted.
-- This function deletes the entry in table cost_comp_upd_stg for a particular component at the 
-- level it was deleted.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_COST_COMP(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_defaulting_level  IN      COST_COMP_UPD_STG.DEFAULTING_LEVEL%TYPE,
                          I_comp_id           IN      ELC_COMP.COMP_ID%TYPE,
                          I_comp_type         IN      ELC_COMP.COMP_TYPE%TYPE,
                          I_dept              IN      DEPS.DEPT%TYPE,
                          I_supplier          IN      SUPS.SUPPLIER%TYPE,
                          I_item              IN      ITEM_MASTER.ITEM%TYPE,
                          I_item_list         IN      SKULIST_HEAD.SKULIST%TYPE,
                          I_item_exp_type     IN      ITEM_EXP_DETAIL.ITEM_EXP_TYPE%TYPE,
                          I_item_exp_seq      IN      ITEM_EXP_DETAIL.ITEM_EXP_SEQ%TYPE,
                          I_hts               IN      HTS.HTS%TYPE,
                          I_import_country_id IN      HTS.IMPORT_COUNTRY_ID%TYPE,
                          I_origin_country_id IN      HTS.IMPORT_COUNTRY_ID%TYPE,
                          I_effect_from       IN      COST_COMP_UPD_STG.EFFECT_FROM%TYPE,
                          I_effect_to         IN      COST_COMP_UPD_STG.EFFECT_TO%TYPE,
                          I_exp_prof_key      IN      EXP_PROF_HEAD.EXP_PROF_KEY%TYPE,
                          I_from_loc          IN      ITEM_LOC.LOC%TYPE,
                          I_to_loc            IN      ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN
IS
   L_program   VARCHAR2(250)  := 'COST_COMP_UPD_SQL.DELETE_COST_COMP';
   
   cursor C_LIST_ITEMS is
      select item
      from   skulist_detail
      where  skulist = I_item_list;

BEGIN

   -- check the defaulting level parameter values.
   if I_defaulting_level not in ('S', 'P', 'I', 'C', 'E', 'D') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   if I_comp_type not in ('U', 'A', 'E') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   if I_defaulting_level = 'E' then
      delete from cost_comp_upd_stg
      where comp_id = I_comp_id
        and defaulting_level = 'E';
   end if;     
   
   if I_defaulting_level in ('S', 'P', 'C') then
      delete from cost_comp_upd_stg
      where comp_id          = I_comp_id
        and defaulting_level = I_defaulting_level
        and exp_prof_key     = I_exp_prof_key;
   end if;
   
   -- Check for the required values when this function is called from the Department Up charge form.
   if I_defaulting_level = 'D' then
      if I_comp_type != 'U' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                               L_program,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      --

      delete from cost_comp_upd_stg
      where comp_id = NVL(I_comp_id, comp_id)
        and defaulting_level = 'D'
        and dept             = I_dept
        and from_loc         = I_from_loc
        and to_loc           = I_to_loc;
     
   end if;  -- I_defaulting_level = 'D'
   
   -- Check for the required values when this function is called from the forms Item expense, Item Upcharge or Item HTS assessment.
   if I_defaulting_level = 'I' then
      --
      -- required values when called from Item Expense
      if I_comp_type = 'E' then 
         
         delete from cost_comp_upd_stg
         where comp_id          = NVL(I_comp_id, comp_id) -- when a port level is deleted, all the components under it will be deleted. In this
           and defaulting_level = 'I'                     -- case the in param I_comp_id will be NULL.
           and item             = I_item
           and item_exp_type    = I_item_exp_type
           and item_exp_seq     = I_item_exp_seq
           and supplier         = I_supplier;
         
      end if;
      --
      -- required values when called from Item charge
      if I_comp_type = 'U' then 
         if I_item is not NULL then
            delete from cost_comp_upd_stg
            where comp_id          = NVL(I_comp_id, comp_id) -- when a location level is deleted, all the components under it will be deleted. In this
              and defaulting_level = 'I'                     -- case the in param I_comp_id will be NULL.
              and from_loc         = I_from_loc
              and to_loc           = I_to_loc
              and item             = I_item;
         elsif I_item_list is not NULL then
            FOR items in C_LIST_ITEMS LOOP
               delete from cost_comp_upd_stg
               where comp_id          = NVL(I_comp_id, comp_id) -- deleting the records from the staging table for items of the item list
                 and defaulting_level = 'I'                     
                 and from_loc         = I_from_loc
                 and to_loc           = I_to_loc
                 and item             = items.item;
            END LOOP;     
         end if;        
           
      end if;
      --
      -- required values when called from Item HTS assessment.
      if I_comp_type = 'A' then 
         
         delete from cost_comp_upd_stg
         where comp_id          = NVL(I_comp_id, comp_id)
           and defaulting_level = 'I'                    
           and hts              = I_hts
           and import_country_id= I_import_country_id
           and origin_country_id= I_origin_country_id
           and effect_from      = I_effect_from
           and effect_to        = I_effect_to
           and item             = I_item;
         
      end if;
   end if;  -- if I_defaulting_level = 'I'
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'COST_COMP_UPD_SQL.DELETE_COST_COMP',
                                            to_char(SQLCODE));
      return FALSE;       

END DELETE_COST_COMP;   

-----------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_COST_COMP_EXT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_comp_id         IN      ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN IS
   L_table           VARCHAR2(30);
   L_program         VARCHAR2(60) := 'COST_COMP_UPD_SQL.DELETE_COST_COMP_EXT';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);
   
   cursor C_LOCK_ELC_COMP_CFA_EXT is
     select 'x' 
       from elc_comp_cfa_ext
      where comp_id = I_comp_id
   for update nowait;     
BEGIN   
   
   L_table := 'ELC_COMP_CFA_EXT';
   
   open C_LOCK_ELC_COMP_CFA_EXT;
   close C_LOCK_ELC_COMP_CFA_EXT;
   
   -- delete from elc_comp CFA extension table
   delete from elc_comp_cfa_ext
      where comp_id = I_comp_id;
   
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED','ELC_COMP',
                                             I_comp_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_COST_COMP_EXT;
------------------------------------------------------------------------------------------------------
END COST_COMP_UPD_SQL;
/ 
