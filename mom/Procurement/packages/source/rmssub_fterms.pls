
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_FTERM AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
FUNCTION CONSUME (O_status_code        IN OUT  VARCHAR2,
                  O_error_message      IN OUT  VARCHAR2,
                  I_message_clob       IN      CLOB)
RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_FTERM;
/


