CREATE OR REPLACE PACKAGE SPLIT_WRAPPER_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-- Function Name: SPLIT_WRAPPER
-- Purpose      : This function is a wrapper that fetches driving cursor data for
--                Truck Splitting process. Called from Order Find Screen.
-------------------------------------------------------------------------------
FUNCTION SPLIT_WRAPPER(I_order_no        IN      ORDHEAD.ORDER_NO%TYPE,
                       O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BINARY_INTEGER;
-------------------------------------------------------------------------------
END SPLIT_WRAPPER_SQL;
/