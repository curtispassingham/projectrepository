CREATE OR REPLACE PACKAGE DEAL_SQL AUTHID CURRENT_USER AS
-- Record Type

TYPE DEAL_CRITERIA_REC is RECORD (deal_id                  DEAL_HEAD.DEAL_ID%TYPE,
                                  type                     DEAL_HEAD.TYPE%TYPE,
                                  status                   DEAL_HEAD.STATUS%TYPE,
                                  ext_ref_no               DEAL_HEAD.EXT_REF_NO%TYPE,
                                  order_no                 DEAL_HEAD.ORDER_NO%TYPE,
                                  currency                 DEAL_HEAD.CURRENCY_CODE%TYPE,
                                  active_date_min          DEAL_HEAD.ACTIVE_DATE%TYPE,
                                  active_date_max          DEAL_HEAD.ACTIVE_DATE%TYPE,
                                  close_date_min           DEAL_HEAD.CLOSE_DATE%TYPE,
                                  close_date_max           DEAL_HEAD.CLOSE_DATE%TYPE,
                                  partner_type             DEAL_HEAD.PARTNER_TYPE%TYPE,
                                  supplier                 DEAL_HEAD.PARTNER_ID%TYPE,
                                  sup_name                 SUPS.SUP_NAME%TYPE,
                                  division                 DEAL_ITEMLOC.DIVISION%TYPE,
                                  group_no                 DEAL_ITEMLOC.GROUP_NO%TYPE,
                                  dept                     DEAL_ITEMLOC.DEPT%TYPE,
                                  class                    DEAL_ITEMLOC.CLASS%TYPE,
                                  subclass                 DEAL_ITEMLOC.SUBCLASS%TYPE,
                                  item                     DEAL_ITEMLOC.ITEM%TYPE,
                                  item_desc                ITEM_MASTER.ITEM_DESC%TYPE,
                                  chain                    DEAL_ITEMLOC.CHAIN%TYPE,
                                  area                     DEAL_ITEMLOC.AREA%TYPE,
                                  region                   DEAL_ITEMLOC.REGION%TYPE,
                                  district                 DEAL_ITEMLOC.DISTRICT%TYPE,
                                  loc_type                 DEAL_ITEMLOC.LOC_TYPE%TYPE,
                                  location                 DEAL_ITEMLOC.LOCATION%TYPE,
                                  deal_comp_type           DEAL_DETAIL.DEAL_COMP_TYPE%TYPE,
                                  billing_type             DEAL_HEAD.BILLING_TYPE%TYPE,
                                  rebate_ind               DEAL_HEAD.REBATE_IND%TYPE,
                                  growth_rebate_ind        DEAL_HEAD.GROWTH_REBATE_IND%TYPE,
                                  rebate_purch_sales_ind   DEAL_HEAD.REBATE_PURCH_SALES_IND%TYPE);
-------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_ITEMLOC(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_deal_criteria          IN       DEAL_CRITERIA_REC,
                          I_called_from_ordering   IN       VARCHAR2,
                          I_order_no               IN       DEAL_HEAD.ORDER_NO%TYPE,
                          I_origin_country_id      IN       VARCHAR2,
                          I_promo                  IN       DEAL_PROM.PROMOTION%TYPE,
                          I_newe                   IN       BOOLEAN := FALSE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:  INSERT_DEAL_QUEUE
--Purpose:        Insert a new deal_queue record.
-------------------------------------------------------------------------------------------
FUNCTION INSERT_DEAL_QUEUE(O_error_message   IN OUT   VARCHAR2,
                           I_deal_id         IN       DEAL_HEAD.DEAL_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:  CLOSE_CONFLICT_ANNUAL_DEAL
--Purpose:        Close a conflicting annual deal.
-------------------------------------------------------------------------------------------
FUNCTION CLOSE_CONFLICT_ANNUAL_DEAL(O_error_message     IN OUT   VARCHAR2,
                                    I_deal_id           IN       DEAL_HEAD.DEAL_ID%TYPE,
                                    I_user_id           IN       DEAL_HEAD.CLOSE_ID%TYPE,
                                    I_close_date        IN       DEAL_HEAD.CLOSE_DATE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:  SWITCH_ORDER
--Purpose:        Update deal components' application order for a given deal ID.
-------------------------------------------------------------------------------------------
FUNCTION SWITCH_ORDER(O_error_message         IN OUT   VARCHAR2,
                      I_deal_id               IN       DEAL_HEAD.DEAL_ID%TYPE,
                      I_deal_detail_id_1      IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                      I_application_order_1   IN       DEAL_DETAIL.APPLICATION_ORDER%TYPE,
                      I_deal_detail_id_2      IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                      I_application_order_2   IN       DEAL_DETAIL.APPLICATION_ORDER%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:  UPDATE_APPL_ORD
--Purpose:        Update deal components' application order for a given deal ID.
-------------------------------------------------------------------------------------------
FUNCTION UPDATE_APPL_ORD(O_error_message       IN OUT   VARCHAR2,
                         I_deal_id             IN       DEAL_HEAD.DEAL_ID%TYPE,
                         I_application_order   IN       DEAL_DETAIL.APPLICATION_ORDER%TYPE,
                         I_action_ind          IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:  CHECK_SUBMIT
--Purpose:        Check the existence of required records in the deal dialogue before a deal can be submitted.
-------------------------------------------------------------------------------------------
FUNCTION CHECK_SUBMIT(O_error_message    IN OUT   VARCHAR2,
                      O_exists           IN OUT   BOOLEAN,
                      I_deal_id          IN       DEAL_HEAD.DEAL_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function Name:  APPLY_ITEM_LIST
--Purpose:        Inserts a DEAL_ITEMLOC record for each valid item based on the passed in itemlist.
-------------------------------------------------------------------------------------------
FUNCTION APPLY_ITEM_LIST(O_error_message       IN OUT   VARCHAR2,
                         O_conflict_exists     IN OUT   BOOLEAN,
                         O_item_sup_ind        IN OUT   VARCHAR2,
                         I_item_list           IN       SKULIST_HEAD.SKULIST%TYPE,
                         I_deal_id             IN       DEAL_HEAD.DEAL_ID%TYPE,
                         I_deal_detail_id      IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                         I_partner_type        IN       PARTNER.PARTNER_TYPE%TYPE,
                         I_partner_id          IN       PARTNER.PARTNER_ID%TYPE,
                         I_supplier            IN       SUPS.SUPPLIER%TYPE,
                         I_origin_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                         I_org_level           IN       DEAL_ITEMLOC.ORG_LEVEL%TYPE,
                         I_loc_type            IN       DEAL_ITEMLOC.LOC_TYPE%TYPE,
                         I_org_value           IN       NUMBER,
                         I_excl_ind            IN       DEAL_ITEMLOC.EXCL_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Function Name:  INSERT_DEAL_CALC_QUEUE
--Purpose:        This function will insert a new record into the DEAL_CALC_QUEUE table
--------------------------------------------------------------------------------------------
FUNCTION INSERT_DEAL_CALC_QUEUE(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no              IN       DEAL_CALC_QUEUE.ORDER_NO%TYPE,
                                I_recalc_all_ind        IN       DEAL_CALC_QUEUE.RECALC_ALL_IND%TYPE,
                                I_override_manual_ind   IN       DEAL_CALC_QUEUE.OVERRIDE_MANUAL_IND%TYPE,
                                I_order_appr_ind        IN       DEAL_CALC_QUEUE.ORDER_APPR_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Function:  UPDATE_THRESH_TARGET_IND
--Purpose:   This package function will set the target_level_ind on the deal_threshold table
--           to 'N' for all records for a given deal ID and deal detail ID.  It will then set the
--           indicator to 'Y' for the threshold that is passed into the function.
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_THRESH_TARGET_IND(O_error_message    IN OUT   VARCHAR2,
                                  I_deal_id          IN OUT   DEAL_HEAD.DEAL_ID%TYPE,
                                  I_deal_detail_id   IN OUT   DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                  I_lower            IN       DEAL_THRESHOLD.LOWER_LIMIT%TYPE,
                                  I_upper            IN       DEAL_THRESHOLD.UPPER_LIMIT%TYPE,
                                  I_value            IN       DEAL_THRESHOLD.VALUE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Function:  INSERT_PO_DEAL
--Purpose:   This function will insert a header record for a new PO-specific deal.
--------------------------------------------------------------------------------------------
FUNCTION INSERT_PO_DEAL(O_error_message                  IN OUT   VARCHAR2,
                        IO_deal_id                       IN OUT   deal_head.deal_id%TYPE,
                        I_order_no                       IN       deal_head.order_no%TYPE,
                        I_supplier                       IN       deal_head.supplier%TYPE,
                        I_currency_code                  IN       deal_head.currency_code%TYPE,
                        I_active_date                    IN       deal_head.active_date%TYPE,
                        I_create_date                    IN       deal_head.create_datetime%TYPE,
                        I_approval_date                  IN       deal_head.approval_date%TYPE,
                        I_user_id                        IN       deal_head.create_id%TYPE,
                        I_ext_ref_no                     IN       deal_head.ext_ref_no%TYPE,
                        I_comments                       IN       deal_head.comments%TYPE,
                        I_billing_type                   IN       DEAL_HEAD.BILLING_TYPE%TYPE,
                        I_bill_back_period               IN       DEAL_HEAD.BILL_BACK_PERIOD%TYPE,
                        I_deal_appl_timing               IN       DEAL_HEAD.DEAL_APPL_TIMING%TYPE,
                        I_threshold_limit_type           IN       DEAL_HEAD.THRESHOLD_LIMIT_TYPE%TYPE,
                        I_threshold_limit_uom            IN       DEAL_HEAD.THRESHOLD_LIMIT_UOM%TYPE,
                        I_rebate_ind                     IN       DEAL_HEAD.REBATE_IND%TYPE,
                        I_rebate_calc_type               IN       DEAL_HEAD.REBATE_CALC_TYPE%TYPE,
                        I_growth_rebate_ind              IN       DEAL_HEAD.GROWTH_REBATE_IND%TYPE,
                        I_historical_comp_start_date     IN       DEAL_HEAD.HISTORICAL_COMP_START_DATE%TYPE,
                        I_historical_comp_end_date       IN       DEAL_HEAD.HISTORICAL_COMP_END_DATE%TYPE,
                        I_rebate_purch_sales_ind         IN       DEAL_HEAD.REBATE_PURCH_SALES_IND%TYPE,
                        I_deal_reporting_level           IN       DEAL_HEAD.DEAL_REPORTING_LEVEL%TYPE,
                        I_bill_back_method               IN       DEAL_HEAD.BILL_BACK_METHOD%TYPE,
                        I_deal_income_calculation        IN       DEAL_HEAD.DEAL_INCOME_CALCULATION%TYPE,
                        I_invoice_processing_logic       IN       DEAL_HEAD.INVOICE_PROCESSING_LOGIC%TYPE,
                        I_stock_ledger_ind               IN       DEAL_HEAD.STOCK_LEDGER_IND%TYPE,
                        I_include_vat_ind                IN       DEAL_HEAD.INCLUDE_VAT_IND%TYPE,
                        I_billing_partner_type           IN       DEAL_HEAD.BILLING_PARTNER_TYPE%TYPE,
                        I_billing_partner_id             IN       DEAL_HEAD.BILLING_PARTNER_ID%TYPE,
                        I_billing_supplier_id            IN       DEAL_HEAD.BILLING_SUPPLIER_ID%TYPE,
                        I_growth_rate_to_date            IN       DEAL_HEAD.GROWTH_RATE_TO_DATE%TYPE,
                        I_turnover_to_date               IN       DEAL_HEAD.TURNOVER_TO_DATE%TYPE,
                        I_actual_monies_earned_to_date   IN       DEAL_HEAD.ACTUAL_MONIES_EARNED_TO_DATE%TYPE,
                        I_security_ind                   IN       DEAL_HEAD.SECURITY_IND%TYPE,
                        I_est_next_invoice_date          IN       DEAL_HEAD.EST_NEXT_INVOICE_DATE%TYPE,
                        I_last_invoice_date              IN       DEAL_HEAD.LAST_INVOICE_DATE%TYPE)

RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Function:  APPLY_ITEM_LOC_LIST
--Purpose:   This function inserts DEAL_ITEMLOC records based on the passed location list and item list.
--------------------------------------------------------------------------------------------
FUNCTION APPLY_ITEM_LOC_LIST(O_error_message       IN OUT   VARCHAR2,
                             O_conflict_exists     IN OUT   BOOLEAN,
                             O_item_sup_ind        IN OUT   VARCHAR2,
                             I_loc_list            IN       LOC_LIST_DETAIL.LOC_LIST%TYPE,
                             I_item_list           IN       SKULIST_HEAD.SKULIST%TYPE,
                             I_deal_id             IN       DEAL_HEAD.DEAL_ID%TYPE,
                             I_deal_detail_id      IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                             I_partner_type        IN       DEAL_HEAD.PARTNER_TYPE%TYPE,
                             I_partner_id          IN       DEAL_HEAD.PARTNER_ID%TYPE,
                             I_supplier            IN       DEAL_HEAD.SUPPLIER%TYPE,
                             I_origin_country_id   IN       DEAL_ITEMLOC.ORIGIN_COUNTRY_ID%TYPE,
                             I_excl_ind            IN       DEAL_ITEMLOC.EXCL_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Function:  APPLY_LOC_LIST
--Purpose:   This function inserts a DEAL_ITEMLOC record for each location based on the passed in location list.
--------------------------------------------------------------------------------------------
FUNCTION APPLY_LOC_LIST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_conflict_exists     IN OUT   BOOLEAN,
                        I_loc_list            IN       LOC_LIST_DETAIL.LOC_LIST%TYPE,
                        I_deal_id             IN       DEAL_HEAD.DEAL_ID%TYPE,
                        I_deal_detail_id      IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                        I_merch_level         IN       DEAL_ITEMLOC.MERCH_LEVEL%TYPE,
                        I_merch_value         IN       VARCHAR2,
                        I_merch_value_2       IN       VARCHAR2,
                        I_merch_value_3       IN       VARCHAR2,
                        I_origin_country_id   IN       DEAL_ITEMLOC.ORIGIN_COUNTRY_ID%TYPE,
                        I_excl_ind            IN       DEAL_ITEMLOC.EXCL_IND%TYPE,
                        I_supplier            IN       DEAL_HEAD.SUPPLIER%TYPE,
                        I_partner_type        IN       DEAL_HEAD.PARTNER_TYPE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Function:  INSERT_QTY_THRESH_ITEMS
--Purpose:   This function will insert deal_itemloc records for the buy item and get item
--           of a given quantity threshold value type record.
--------------------------------------------------------------------------------------------
FUNCTION INSERT_QTY_THRESH_ITEMS(O_error_message    IN OUT   VARCHAR2,
                                 I_deal_id          IN       DEAL_HEAD.DEAL_ID%TYPE,
                                 I_deal_detail_id   IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                 I_buy_item         IN       DEAL_DETAIL.QTY_THRESH_BUY_ITEM%TYPE,
                                 I_get_item         IN       DEAL_DETAIL.QTY_THRESH_GET_ITEM%TYPE,
                                 I_org_level        IN       DEAL_ITEMLOC.ORG_LEVEL%TYPE,
                                 I_chain            IN       DEAL_ITEMLOC.CHAIN%TYPE,
                                 I_area             IN       DEAL_ITEMLOC.AREA%TYPE,
                                 I_region           IN       DEAL_ITEMLOC.REGION%TYPE,
                                 I_district         IN       DEAL_ITEMLOC.DISTRICT%TYPE,
                                 I_location         IN       DEAL_ITEMLOC.LOCATION%TYPE,
                                 I_loc_type         IN       DEAL_ITEMLOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Function:  APPLY_ON_RCPT_DEALS
--Purpose:   This function will apply discounts to items at time of order receipt.
--------------------------------------------------------------------------------------------
FUNCTION APPLY_ON_RCPT_DEALS(O_error_message   IN OUT   VARCHAR2,
                             I_order_no        IN       ordloc_discount.order_no%TYPE,
                             I_item            IN       ordloc_discount.item%TYPE,
                             I_location        IN       ordloc_discount.location%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Function:  DELETE_DEALS
--Purpose:   This function will remove a deal that is associated with an order that is being
--           approved while the deal is in worksheet or rejected status.
--------------------------------------------------------------------------------------------
FUNCTION DELETE_DEALS(O_error_message   IN OUT   VARCHAR2,
                      I_deal_id         IN       deal_head.deal_id%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Function:  CHECK_BUY_GET_DEALS_FOR_ITEM_SUPP
--Purpose:   This function check if the item exists on an active buy/get type deal
--           for the supplier.  If the item is on a buy/get deal, the function will
--           pass out the buy qty/amount, the buy type (units, dollars, etc.), the
--           get item, get type and deal ID.
--------------------------------------------------------------------------------------------
FUNCTION CHECK_IS_BUY_ITEM (O_error_message      IN OUT   VARCHAR2,
                            O_item_is_buy_item   IN OUT   BOOLEAN,
                            O_qty_locs_on_deal   IN OUT   ORDLOC.QTY_ORDERED%TYPE,
                            O_deal_id            IN OUT   DEAL_HEAD.DEAL_ID%TYPE,
                            O_buy_qty            IN OUT   DEAL_DETAIL.QTY_THRESH_BUY_QTY%TYPE,
                            O_get_type           IN OUT   DEAL_DETAIL.QTY_THRESH_GET_TYPE%TYPE,
                            O_get_value          IN OUT   DEAL_DETAIL.QTY_THRESH_GET_VALUE%TYPE,
                            O_get_qty            IN OUT   DEAL_DETAIL.QTY_THRESH_GET_QTY%TYPE,
                            O_get_item           IN OUT   DEAL_DETAIL.QTY_THRESH_GET_ITEM%TYPE,
                            I_item               IN       ITEM_MASTER.ITEM%TYPE,
                            I_loc                IN       STORE.STORE%TYPE,
                            I_loc_type           IN       ORDLOC.LOC_TYPE%TYPE,
                            I_order_no           IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Function:  DELETE_ITEMLOC_THRESH
--Purpose:   This function will remove a deal itemloc and or deal threshold records that are
--           associated with the deal component that is being deleted.
--------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEMLOC_THRESH(O_error_message    IN OUT   VARCHAR2,
                               I_itemloc_exist             BOOLEAN,
                               I_thresh_exist              BOOLEAN,
                               I_deal_id          IN       deal_itemloc.deal_id%TYPE,
                               I_deal_detail_id   IN       deal_itemloc.deal_detail_id%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
-- Function:  CREATE_FROM_EXIST
-- Purpose:   This function will create a completely new deal based on an existing deal.
--------------------------------------------------------------------------------------------
FUNCTION CREATE_FROM_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_new_deal_id     IN OUT   DEAL_ACTUALS_FORECAST.DEAL_ID%TYPE,
                           I_deal_id         IN       DEAL_ACTUALS_FORECAST.DEAL_ID%TYPE,
                           I_start_date      IN       DEAL_HEAD.ACTIVE_DATE%TYPE,
                           I_end_date        IN       DEAL_HEAD.CLOSE_DATE%TYPE,
                           I_supplier        IN       DEAL_HEAD.SUPPLIER%TYPE,
                           I_partner_type    IN       DEAL_HEAD.PARTNER_TYPE%TYPE,
                           I_partner_id      IN       DEAL_HEAD.PARTNER_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
-- Function:  PO_CASCADE 
-- Purpose:   This function is called by dealordlib.pc, it checks if any of the components 
--            associated within the PO specific deal apply the discount at transaction level 
--------------------------------------------------------------------------------------------
FUNCTION PO_CASCADE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_po_cascade      IN OUT   VARCHAR2,
                    I_deal_id         IN       DEAL_DETAIL.DEAL_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Function:  CHECK_STATUS
--Purpose:   This function will check if the status is valid to be edited.
--------------------------------------------------------------------------------------------
FUNCTION CHECK_STATUS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_status         IN OUT  FIXED_DEAL.STATUS%TYPE,
                      I_end_date       IN      FIXED_DEAL.COLLECT_END_DATE%TYPE,
                      I_deal_no        IN      FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Function:  CHECK_TYPE
--Purpose:   This function will check if type is valid to be edited.
--------------------------------------------------------------------------------------------
FUNCTION CHECK_TYPE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    O_type           IN OUT  FIXED_DEAL.TYPE%TYPE,
                    I_start_date     IN      FIXED_DEAL.COLLECT_START_DATE%TYPE,
                    I_deal_no        IN      FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Function:  CHECK_INVOICE
--Purpose:   This function will check if invoice processing logic is valid to be edited.
--------------------------------------------------------------------------------------------
FUNCTION CHECK_INVOICE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_invoice        IN OUT  FIXED_DEAL.INVOICE_PROCESSING_LOGIC%TYPE,
                       I_start_date     IN      FIXED_DEAL.COLLECT_START_DATE%TYPE,
                       I_deal_no        IN      FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Function:  CHECK_DEBIT_CREDIT
--Purpose:   This function will check if the debit credit indicator is valid to be edited.
--------------------------------------------------------------------------------------------
FUNCTION CHECK_DEBIT_CREDIT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_deb_cred       IN OUT  FIXED_DEAL.DEB_CRED_IND%TYPE,
                            I_start_date     IN      FIXED_DEAL.COLLECT_START_DATE%TYPE,
                            I_deal_no        IN      FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Function:  CHECK_MERCH_IND
--Purpose:   This function will check if the merchandise indicator is valid to be edited.
--------------------------------------------------------------------------------------------
FUNCTION CHECK_MERCH_IND(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_merch_ind      IN OUT  FIXED_DEAL.MERCH_IND%TYPE,
                         I_start_date     IN      FIXED_DEAL.COLLECT_START_DATE%TYPE,
                         I_deal_no        IN      FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Function:  CHECK_VAT_IND
--Purpose:   This function will check if the VAT indicator is valid to be edited.
--------------------------------------------------------------------------------------------
FUNCTION CHECK_VAT_IND(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_vat_ind        IN OUT  FIXED_DEAL.VAT_IND%TYPE,
                       I_start_date     IN      FIXED_DEAL.COLLECT_START_DATE%TYPE,
                       I_deal_no        IN      FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Function:  DELETE_DEAL_CFA_EXT
--Purpose:   This function will delete the deal records from deal_head_cfa_ext
--------------------------------------------------------------------------------------------
FUNCTION DELETE_DEAL_CFA_EXT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_deal_id        IN      FIXED_DEAL.DEAL_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
-- Function : GET_DEAL_ITEMLOC_WRP
-- Purpose  : This is a wrapper function for GET_DEAL_ITEMLOC that passes a database 
--            type object instead of a PL/SQL type as an input parameter to be called from Java wrappers.
--------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_ITEMLOC_WRP(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_deal_criteria          IN       WRP_DEAL_CRITERIA_REC,
                              I_called_from_ordering   IN       VARCHAR2,
                              I_order_no               IN       DEAL_HEAD.ORDER_NO%TYPE,
                              I_origin_country_id      IN       VARCHAR2,
                              I_promo                  IN       DEAL_PROM.PROMOTION%TYPE,
                              I_newe                   IN       VARCHAR2)
   RETURN NUMBER;
   
END DEAL_SQL;
/