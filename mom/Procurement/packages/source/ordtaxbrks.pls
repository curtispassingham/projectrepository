CREATE OR REPLACE PACKAGE ORDTAXBRK_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------------------
TYPE po_tax_breakup_entity_rec IS RECORD(from_entity_type         CODE_DETAIL.CODE_DESC%TYPE,
                                         from_entity_id           ORDHEAD.SUPPLIER%TYPE,
                                         from_entity_description  SUPS.SUP_NAME%TYPE,
                                         to_entity_type           CODE_DETAIL.CODE_DESC%TYPE,
                                         to_entity_id             ORDLOC.LOCATION%TYPE,
                                         to_entity_description    STORE.STORE_NAME%TYPE,
                                         loc_type                 ORDLOC.LOC_TYPE%TYPE,
                                         error_message            RTK_ERRORS.RTK_TEXT%TYPE,
                                         return_code              VARCHAR2(5));

TYPE po_tax_breakup_entity_rec_tbl IS TABLE OF po_tax_breakup_entity_rec INDEX BY BINARY_INTEGER;


TYPE po_tax_breakup_item_rec IS RECORD(item                 ITEM_MASTER.ITEM%TYPE,
                                       item_desc            ITEM_MASTER.ITEM_DESC%TYPE,
                                       currency             CURRENCIES.CURRENCY_CODE%TYPE,
                                       unit_cost_excl_tax   ORDLOC.UNIT_COST%TYPE,
                                       unit_cost_incl_tax   ORDLOC.UNIT_COST%TYPE,
                                       unit_tax_amt        ORD_TAX_BREAKUP.TOTAL_TAX_AMT%TYPE,
                                       quantity             ORDLOC.QTY_ORDERED%TYPE,
                                       total_tax_amt        ORD_TAX_BREAKUP.TOTAL_TAX_AMT%TYPE,
                                       error_message        RTK_ERRORS.RTK_TEXT%TYPE,
                                       return_code          VARCHAR2(5));

TYPE po_tax_breakup_item_rec_tbl IS TABLE OF po_tax_breakup_item_rec INDEX BY BINARY_INTEGER;

TYPE po_tax_breakup_rec IS RECORD(tax_code                ORD_TAX_BREAKUP.TAX_CODE%TYPE,
                                  tax_rate                ORD_TAX_BREAKUP.TAX_RATE%TYPE,
                                  calculation_basis       ORD_TAX_BREAKUP.CALCULATION_BASIS%TYPE,
                                  per_count               ORD_TAX_BREAKUP.PER_COUNT%TYPE,
                                  per_count_uom           ORD_TAX_BREAKUP.PER_COUNT_UOM%TYPE,
                                  unit_tax_amt            ORD_TAX_BREAKUP.TOTAL_TAX_AMT%TYPE,
                                  taxable_base            ORD_TAX_BREAKUP.TAXABLE_BASE%TYPE,
                                  modified_taxable_base   ORD_TAX_BREAKUP.MODIFIED_TAXABLE_BASE%TYPE,
                                  recoverable_amount      ORD_TAX_BREAKUP.RECOVERABLE_AMOUNT%TYPE,
                                  total_tax_amt           ORD_TAX_BREAKUP.TOTAL_TAX_AMT%TYPE,
                                  error_message           RTK_ERRORS.RTK_TEXT%TYPE,
                                  return_code             VARCHAR2(5));

TYPE po_tax_breakup_rec_tbl IS TABLE OF po_tax_breakup_rec INDEX BY BINARY_INTEGER;


------------------------------------------------------------------------------------------------------------
--- Procedure Name: QUERY_PROCEDURE_ENTITY
--- Purpose:        This procedure will query ORDHEAD and ORDLOC tables and populate the above declared 
---                 table of records that will be used as the 'base table' for the Entity Details section
---                 in the Order Tax breakup form.
------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE_ENTITY(IO_po_tax_breakup_entity_tbl   IN OUT   PO_TAX_BREAKUP_ENTITY_REC_TBL,
                                 I_order_no                     IN       ORDHEAD.ORDER_NO%TYPE);
                                 
------------------------------------------------------------------------------------------------------------
--- Procedure Name: QUERY_PROCEDURE_ITEM
--- Purpose:        This procedure will query ORDLOC and ORD_TAX_BREAKUP tables and populate the above declared 
---                 table of records that will be used as the 'base table' for the Item Details section
---                 in the Order Tax breakup form.
------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE_ITEM (IO_po_tax_breakup_item_tbl   IN OUT   PO_TAX_BREAKUP_ITEM_REC_TBL,
                                I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
                                I_location              IN       ORDLOC.LOCATION%TYPE,
                                I_loc_type              IN       ORDLOC.LOC_TYPE%TYPE);
------------------------------------------------------------------------------------------------------------
--- Procedure Name: QUERY_PROCEDURE_TAX
--- Purpose:        This procedure will query ORD_TAX_BREAKUP table and populate the above declared 
---                 table of records that will be used as the 'base table' for the Tax Breakup section
---                 in the Order Tax breakup form.
------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE_TAX (IO_po_tax_breakup_tbl   IN OUT   PO_TAX_BREAKUP_REC_TBL,
                               I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
                               I_item                  IN       ORDLOC.ITEM%TYPE,
                               I_location              IN       ORDLOC.LOCATION%TYPE,
                               I_loc_type              IN       ORDLOC.LOC_TYPE%TYPE);
------------------------------------------------------------------------------------------------------------
--- Procedure Name: DELETE_ORDTAXBRK
--- Purpose:        This function deletes any ORD_TAX_BREAKUP records populated by external tax system
---                 if the user clicks the Cancel button.
------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORDTAXBRK(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no       IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
END ORDTAXBRK_SQL;
/