CREATE OR REPLACE PACKAGE BODY SPLIT_ORDER_SQL IS

/* Global Variables */
FULL_TRUCK          NUMBER(1) := 1;
LTL_TRUCK           NUMBER(1) := 2;
UNKNOWN_TRUCK       NUMBER(1) := 3;
NUM_SPLIT_CNST      NUMBER(1) := 2;
L_CASE              NUMBER(1) := 1;
L_ALL               NUMBER(1) := 2;
NULL_OPERATOR       NUMBER(1) := 2;

/* before changing or adding additional truck status #defines please
   note that the values of these are used as they relate to each other */
BELOW               NUMBER(1) := 1;
OVER                NUMBER(1) := 2;

EMPTY_ORDER         NUMBER(1) := 0;
ADD_CASE            NUMBER(1) := 1;
FT_STOP             NUMBER(1) := 2;
RFT_STOP            NUMBER(1) := 4;
LTL_STOP            NUMBER(1) := 3;

/* Private Functions */
-----------------------------------------------------------------------------------
FUNCTION LOAD_CONST(I_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN NUMBER;
-----------------------------------------------------------------------------------
FUNCTION SPLIT_BALANCE(I_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                       O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION SPLIT_ITEMSEQ(I_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                       O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION ITEM_QTY_IN_TRUCK(I_ordhead_tbl       IN     ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                           I_ordsku_tbl        IN     ADD_LINE_ITEM_SQL.ORDSKU_TBL,
                           O_qty_in_truck      IN OUT NUMBER,
                           O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION SPLIT_CLEANUP(I_ordhead_tbl   IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                       O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION NUM_TRUCKS(I_ordhead_tbl       IN     ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                    O_num_trucks        IN OUT NUMBER,
                    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION CREATE_MODEL_TRUCK(I_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                            I_num_trucks        IN     NUMBER,
                            O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION MODEL_TO_SOLUTION(I_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                           O_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                           O_stock_out         IN OUT NUMBER,
                           O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION TRUCK_STATUS(I_ordhead_tbl       IN     ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                      I_increment         IN     NUMBER,
                      I_map               IN     ADD_LINE_ITEM_SQL.CNST_QTY_TBL,
                      O_truck_status      IN OUT NUMBER,
                      O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION MOVE_ORDER(I_ordhead_tbl       IN     ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                    O_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION MOVE_CASE_ALL(I_ordhead_rec       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_REC,
                       I_ordsku_rec        IN     ADD_LINE_ITEM_SQL.ORDSKU_REC,
                       I_ordloc_tbl        IN OUT ADD_LINE_ITEM_SQL.ORDLOC_TBL,
                       O_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                       I_case_or_all_ind   IN     NUMBER,
                       I_move_qty          IN     NUMBER,
                       O_stock_exist_ind   IN OUT NUMBER,
                       O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION MOVE_STOCK(I_ordhead_rec   IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_REC,
                    I_ordsku_rec    IN     ADD_LINE_ITEM_SQL.ORDSKU_REC,
                    I_ordloc_rec    IN OUT ADD_LINE_ITEM_SQL.ORDLOC_REC,
                    O_ordhead_tbl   IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                    I_need_qty      IN     NUMBER,
                    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION ADD_ORD_TO_START(I_to_head_tbl    IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                          I_from_head_tbl  IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                          O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION OUTPUT_TO_SOLUTION(I_ordhead_tbl        IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                            I_sol_head_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                            O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION UOM_CONVERSION(I_from_uom      IN     VARCHAR2,
                        I_to_uom        IN     VARCHAR2,
                        I_from_value    IN     NUMBER,
                        O_to_value      IN OUT NUMBER,
                        O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION SPLIT_ROUND_TO_PACKSIZE(I_qty_to_round     IN     NUMBER,
                                 I_packsize         IN     NUMBER,
                                 I_round_pct        IN     NUMBER,
                                 O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN NUMBER;
------------------------------------------------------------------------------
FUNCTION SPLIT_PO(I_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                  O_split_ind         IN OUT NUMBER,
                  O_error_message     IN OUT VARCHAR2)
RETURN NUMBER IS
   L_program            VARCHAR2(30):= 'SPLIT_PO_SQL.SPLIT_PO';
   L_split_message      VARCHAR2(2000);
   L_return             NUMBER(1) := 0;
   L_num_trucks         NUMBER;
   O_key                VARCHAR2(255);

BEGIN
   /* This module only processes one order at a time.  Even though
      is accepts a pointer to a list of orders, it will only process
      the order the passed in list is currently pointing at, it will
      not advance the order pointer and process the next order
      in the list */
   if I_ordhead_tbl.COUNT = 0 then
      O_split_ind := 0;
      return 0;
   end if;

   I_ordhead_tbl(1).split_ref_ord_no := I_ordhead_tbl(1).order_no;
   /* load up the truck splitting constraints from the item_supplier dialogue */
   L_return := LOAD_CONST(I_ordhead_tbl,
                          O_error_message);
   if L_return < 0 then
      return L_return;
   elsif L_return > 0 then
      L_split_message := O_error_message;
      
      if SQL_LIB.PARSE_MSG(L_split_message,
                           O_key)= FALSE then
         return -1;
      end if;
      if CREATE_PO_LIB_SQL.UPDATE_OIM(I_ordhead_tbl(1).order_no,
                                      'N',
                                      L_split_message,
                                      O_error_message) = FALSE then
         return -1;
      end if;
      return L_return;
   end if;
   /* check if current order is full */
   if NUM_TRUCKS(I_ordhead_tbl,
                 L_num_trucks,
                 O_error_message) = FALSE then
      return -1;
   end if;
   /* if the input order is smaller that one truck, no splitting is required */
   if L_num_trucks <= 1 then

      L_split_message := SQL_LIB.CREATE_MSG('NO_SPLIT',
                                            I_ordhead_tbl(1).order_no,
                                            NULL,
                                            NULL);
                                            
      if SQL_LIB.PARSE_MSG(L_split_message,
                           O_key)= FALSE then
         return -1;
      end if;

      if L_num_trucks = 1 then
         if CREATE_PO_LIB_SQL.UPDATE_OIM(I_ordhead_tbl(1).order_no,
                                         'N',
                                         L_split_message,
                                         O_error_message) = FALSE then
            return -1;
         end if;
      else
         if CREATE_PO_LIB_SQL.UPDATE_OIM(I_ordhead_tbl(1).order_no,
                                         'Y',
                                         L_split_message,
                                         O_error_message) = FALSE then
            return -1;
         end if;
      end if;

     /* 2 = no need to split, 1 = non-fatal error */
      O_split_ind := 0;

      O_error_message := L_split_message;

      return 2;
   end if;

   O_split_ind := 1;
   if I_ordhead_tbl(1).sup_info.truck_method = 'B' then
      if SPLIT_BALANCE(I_ordhead_tbl,
                       O_error_message) = FALSE then
         return -1;
      end if;
   else
      if SPLIT_ITEMSEQ(I_ordhead_tbl,
                       O_error_message) = FALSE then
         return -1;
      end if;
   end if;
   return 0;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return -1;
END SPLIT_PO; /* end split_po */

-------------------------------------------------------------------------------------------------------
/*
 * This function accepts an OLL (ordering linked list) and loads up information needed to split the
 *  current order in the OLL.  This information includes:
 *    - item's constraint qty -- The total quantity of each line item in the
 *                               OLL in the UOM of the constraint (for both constraints).
 *    - item's constraint map -- The quantity of 1 unit of each line item in the
 *                               OLL in the UOM of the constraint (for both constraints).
 *    - item's increment and round pct.
 *    - orders total constraint qty -- the total quantity across the entire order
 *                                     converted to the UOM of the constraint (for both constriants).
 *    - reset cnst -- the constraint values increated to take advantage of threshold
 *                    to avoid LTL trucks when possible (for both constraints).
 */
-------------------------------------------------------------------------------------------------------
FUNCTION LOAD_CONST(I_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(30)   := 'SPLIT_ORDER_SQL.LOAD_CONST';

   L_curr_sku_tbl        ADD_LINE_ITEM_SQL.ORDSKU_TBL;
   L_curr_loc_tbl        ADD_LINE_ITEM_SQL.ORDLOC_TBL;

   L_item                ORDSKU.ITEM%TYPE := NULL;
   L_supp                ORDHEAD.SUPPLIER%TYPE := NULL;
   L_ctry                ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_loc                 ORDHEAD.LOCATION%TYPE;

   L_supp_pack_size      NUMBER;
   L_pallet_size         NUMBER;
   L_length              NUMBER;
   L_width               NUMBER;
   L_height              NUMBER;
   L_lwh_uom             VARCHAR2(10);
   L_weight              NUMBER;
   L_weight_uom          VARCHAR2(10);
   L_stat_cube           NUMBER;

   L_num_trucks          NUMBER;
   L_num_full_trucks     NUMBER;

   cursor C_DIM is
      select isc.supp_pack_size,
             isc.ti * isc.hi * isc.supp_pack_size,          /* pallet */
             dim.length,
             dim.width,
             dim.height,
             dim.lwh_uom,
             dim.weight,
             dim.weight_uom,
             dim.stat_cube,
             /* fetch the smallest order increment
                depending on the rounding level */
             DECODE(iscl.round_lvl,
                    'P',  isc.ti * isc.hi * isc.supp_pack_size,
                    'L',  isc.ti * isc.supp_pack_size,
                    'LP', isc.ti * isc.supp_pack_size,
                          isc.supp_pack_size),
             /* fetch the rounding percent for the smallest order
                increment depending on the rounding level */
             DECODE(iscl.round_lvl,
                    'P',  iscl.round_to_pallet_pct,
                    'L',  iscl.round_to_layer_pct,
                    'LP', iscl.round_to_layer_pct,
                          iscl.round_to_case_pct)
        from item_supp_country isc,
             item_supp_country_dim dim,
             item_supp_country_loc iscl
       where isc.item              = L_item
         and isc.supplier          = L_supp
         and isc.origin_country_id = L_ctry
         and isc.item              = iscl.item
         and isc.supplier          = iscl.supplier
         and isc.origin_country_id = iscl.origin_country_id
         /* if loc is a wh, get the info at its rounding seq wh if inv seq
            rounding is being used, otherwise get the info at the wh level.
            if loc is a store, get the info at the store level. */
         and iscl.loc              = L_loc
         and isc.item              = dim.item(+)
         and isc.supplier          = dim.supplier(+)
         and isc.origin_country_id = dim.origin_country(+)
         and dim.dim_object(+)     = 'CA';

BEGIN

   for L_ctr IN 1..I_ordhead_tbl.COUNT
   LOOP
      L_supp := I_ordhead_tbl(L_ctr).supplier;

      L_curr_sku_tbl := I_ordhead_tbl(L_ctr).ordsku_tbl;

      for L_sku_ctr in 1..L_curr_sku_tbl.COUNT
      LOOP
         L_curr_loc_tbl := L_curr_sku_tbl(L_sku_ctr).ordloc_tbl;
         /* set the bind the variables for the dimension cursor */
         L_item       := L_curr_sku_tbl(L_sku_ctr).item;
         L_ctry       := L_curr_sku_tbl(L_sku_ctr).ctry;
         L_loc        := L_curr_loc_tbl(1).repl_wh_link;
         open C_DIM;
         fetch C_DIM INTO L_supp_pack_size,
                          L_pallet_size,
                          L_length,
                          L_width,
                          L_height,
                          L_lwh_uom,
                          L_weight,
                          L_weight_uom,
                          L_stat_cube,
                          L_curr_sku_tbl(L_sku_ctr).splitting_increment,
                          L_curr_sku_tbl(L_sku_ctr).round_pct;
         close C_DIM;
         for L_loc_ctr IN 1..L_curr_loc_tbl.COUNT
         LOOP
            /* given the dimensions - populate the qty of 1 unit of the item
               in each constraint UOM */
            for i in 1 .. NUM_SPLIT_CNST
            LOOP
               if I_ordhead_tbl(L_ctr).sup_info.cnst_type(i) = 'M' then

                  if L_weight IS NULL then
                     O_error_message := SQL_LIB.CREATE_MSG('NO_WT_INFO',
                                                            L_item,
                                                            L_supp,
                                                            L_ctry);
                     /* 2 = no need to split, 1 = non-fatal error */
                     return 1;
                  end if;

                  /* convert UOMs */
                  if UOM_CONVERSION(L_weight_uom,
                                    I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i),
                                    L_weight,
                                    L_weight,
                                    O_error_message) = FALSE then
                     return -1;
                  end if;
                  L_curr_loc_tbl(L_loc_ctr).cnst_qty(i) := L_curr_loc_tbl(L_loc_ctr).qty * (L_weight / L_supp_pack_size);
                  L_curr_loc_tbl(L_loc_ctr).cnst_map(i) := L_weight / L_supp_pack_size;
                  I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(i) := I_ordhead_tbl(l_ctr).sup_info.total_cnst_qty(i) + L_curr_loc_tbl(L_loc_ctr).cnst_qty(i);
               end if;
               if I_ordhead_tbl(L_ctr).sup_info.cnst_type(i) = 'V' then
                  /* convert splitting constraint's volume uom to dimension uom
                     Retek convention: volume uom = dimension uom + 3
                     e.g.: volume uom 'IN3', corresponding dimension uom is 'IN'.
                     There are a few exceptions that need to be hard coded:
                     1) CC (cubic centimeter) => 'CM'
                     2) CFT (cubic feet) => 'FT'
                     3) CBM (cubic meter) => 'M'
                     if the last character is 3 then it is a volume dimension and so
                     truncate the last character, else do not do anything*/

                  if I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i) = 'CC' then
                     I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i) := 'CM';
                  end if;
                  if I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i) = 'CFT' then
                     I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i) := 'FT';
                  end if;

                  if I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i) = 'CBM' then
                     I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i) := 'M';
                  elsif SUBSTR(I_ordhead_tbl(l_ctr).sup_info.cnst_uom(i),-1,1) = '3' then
                     I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i) := SUBSTR(I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i), 1, (LENGTH(I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i)) -1));
                  else
                     I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i) := I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i);
                  end if;

                  if L_length IS NULL or L_width IS NULL or L_height IS NULL then
                     O_error_message := SQL_LIB.CREATE_MSG('NO_DIMEN_INFO',
                                                            L_item,
                                                            L_supp,
                                                            L_ctry);
                     /* 2 = no need to split, 1 = non-fatal error */
                     return 1;
                  end if;
                  /* convert UOM */
                  if UOM_CONVERSION(L_lwh_uom,
                                    I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i),
                                    L_length,
                                    L_length,
                                    O_error_message) = FALSE then
                     return -1;
                  end if;
                  if UOM_CONVERSION(L_lwh_uom,
                                    I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i),
                                    L_width,
                                    L_width,
                                    O_error_message) = FALSE then
                     return -1;
                  end if;
                  if UOM_CONVERSION(L_lwh_uom,
                                    I_ordhead_tbl(L_ctr).sup_info.cnst_uom(i),
                                    L_height,
                                    L_height,
                                    O_error_message) = FALSE then
                     return -1;
                  end if;

                  L_curr_loc_tbl(L_loc_ctr).cnst_qty(i) := L_curr_loc_tbl(L_loc_ctr).qty * ((L_length*L_width*L_height)/L_supp_pack_size);
                  L_curr_loc_tbl(L_loc_ctr).cnst_map(i) := (L_length * L_width * L_height) / L_supp_pack_size;
                  I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(i) := I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(i) + L_curr_loc_tbl(L_loc_ctr).cnst_qty(i);
               end if;

               if I_ordhead_tbl(L_ctr).sup_info.cnst_type(i) = 'P'
               then
                  L_curr_loc_tbl(L_loc_ctr).cnst_qty(i) := L_curr_loc_tbl(L_loc_ctr).qty / L_pallet_size;
                  L_curr_loc_tbl(L_loc_ctr).cnst_map(i) := 1 / L_pallet_size;
                  I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(i) := I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(i) + L_curr_loc_tbl(L_loc_ctr).cnst_qty(i);
               end if;

               if I_ordhead_tbl(L_ctr).sup_info.cnst_type(i) = 'C'
               then
                  L_curr_loc_tbl(L_loc_ctr).cnst_qty(i) := L_curr_loc_tbl(L_loc_ctr).qty / L_supp_pack_size;
                  L_curr_loc_tbl(L_loc_ctr).cnst_map(i) := 1 / L_supp_pack_size;
                  I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(i) := I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(i) + L_curr_loc_tbl(L_loc_ctr).cnst_qty(i);
               end if;

               if I_ordhead_tbl(L_ctr).sup_info.cnst_type(i) = 'E'
               then
                  L_curr_loc_tbl(L_loc_ctr).cnst_qty(i) := L_curr_loc_tbl(L_loc_ctr).qty;
                  L_curr_loc_tbl(L_loc_ctr).cnst_map(i) := 1;
                  I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(i) := I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(i) + L_curr_loc_tbl(L_loc_ctr).cnst_qty(i);
               end if;

               if I_ordhead_tbl(l_ctr).sup_info.cnst_type(i) = 'S'
               then
                  if L_stat_cube IS NULL then
                     O_error_message := SQL_LIB.CREATE_MSG('NO_DIMEN_INFO',
                                                            L_item,
                                                            L_supp,
                                                            L_ctry);
                     /* 2 = no need to split, 1 = non-fatal error */
                     return 1;
                  end if;
                  L_curr_loc_tbl(L_loc_ctr).cnst_qty(i) := (L_curr_loc_tbl(L_loc_ctr).qty / L_supp_pack_size) * L_stat_cube;
                  L_curr_loc_tbl(L_loc_ctr).cnst_map(i) := L_stat_cube / L_supp_pack_size;
                  I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(i) :=  I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(i) + L_curr_loc_tbl(L_loc_ctr).cnst_qty(i);
               end if;
            END LOOP; /* end for loop */

         END LOOP; /* end of for loop for ordloc*/
         L_curr_sku_tbl(L_sku_ctr).ordloc_tbl := L_curr_loc_tbl;
      END LOOP; /* end of for loop for ordsku*/

      /* reset the max constraint.  this is an attempt to reduce number of
         LTL orders by utilizing the threshold. */
      for ll_ctr in 1..NUM_SPLIT_CNST
      LOOP
         if I_ordhead_tbl(L_ctr).sup_info.cnst_type(ll_ctr) IS NOT NULL then
            L_num_trucks := I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(ll_ctr) /
                            I_ordhead_tbl(L_ctr).sup_info.cnst_value(ll_ctr);
            L_num_full_trucks := floor(L_num_trucks);

            if (L_num_trucks - L_num_full_trucks) <= (L_num_full_trucks * I_ordhead_tbl(L_ctr).sup_info.cnst_threshold(ll_ctr)) then
               I_ordhead_tbl(L_ctr).sup_info.cnst_value_reset(ll_ctr) := I_ordhead_tbl(L_ctr).sup_info.total_cnst_qty(ll_ctr) / L_num_full_trucks;
            else
               I_ordhead_tbl(L_ctr).sup_info.cnst_value_reset(ll_ctr) := I_ordhead_tbl(L_ctr).sup_info.cnst_value(ll_ctr);
            end if;
         end if;
      END LOOP;
      I_ordhead_tbl(L_ctr).ordsku_tbl := L_curr_sku_tbl;
   END LOOP; /* end of for loop ordhead */
   return 0;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return -1;
END LOAD_CONST; /* end load_const */
---------------------------------------------------------------------------------------------------------------------
/*
 * This function will try to split the input order using the
 *  balanced approach.
 *
 *  Count the number of trucks that remain on the SOURCE order
 *   (prime the loop).
 *
 *  Loop while more than one truck remains on the SOURCE order.
 *
 *     Create a model truck based on the current configuration
 *      of the SOURCE order.
 *
 *     Loop while there is stock left on the SOURCE order to
 *      build a model truck (the current model).
 *
 *        Move a model truck from the SOURCE order to the
 *         WORKING order.
 *        Set a flag when the SOURCE order is no longer able to
 *         fill a model truck.
 *        Move the WORKING order into the SOLUTION list.
 *
 *     End Loop.
 *
 *     Count the number of trucks that remain on the SOURCE order.
 *
 *  End Loop.
 *
 *  Move remaining stock from the SOURCE order to the WORKING order.
 *  Move the WORKING order into the SOLUTION list.
 *  Replace the SOURCE order with the SOLUTION list.
 *  Give back memory origionally used to hold the SOURCE order.
 *
 */
----------------------------------------------------------------------------------------------
FUNCTION SPLIT_BALANCE(I_ordhead_tbl    IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                       O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_in_ord_tbl  ADD_LINE_ITEM_SQL.ORDHEAD_TBL;
   L_sol_ord_tbl ADD_LINE_ITEM_SQL.ORDHEAD_TBL;

   L_num_trucks  NUMBER;
   L_stock_flag  NUMBER := 1;
   L_program     VARCHAR2(30):= 'SPLIT_ORDER_SQL.SPLIT_BALANCE';

BEGIN
   /* prime the num trucks loop */
   if NUM_TRUCKS(I_ordhead_tbl,
                 L_num_trucks,
                 O_error_message) = FALSE then
      return FALSE;
   end if;

   while L_num_trucks >= 1
   LOOP
      /* prorate items on input order across truck loads */
      if CREATE_MODEL_TRUCK(I_ordhead_tbl,
                            L_num_trucks,
                            O_error_message) = FALSE then
         return FALSE;
      end if;
      L_stock_flag := 1;
      /* Move a model order from orig order to solution order */
      if MODEL_TO_SOLUTION(I_ordhead_tbl,
                           L_in_ord_tbl,
                           L_stock_flag,
                           O_error_message) = FALSE then
         return FALSE;
      end if;
      if L_in_ord_tbl IS NOT NULL and L_in_ord_tbl.count > 0 then
         if ADD_ORD_TO_START(L_sol_ord_tbl,
                             L_in_ord_tbl,
                             O_error_message) = FALSE then
            return FALSE;
         end if;
      else
         EXIT;
      end if;
      L_in_ord_tbl.DELETE();
      if NUM_TRUCKS(I_ordhead_tbl,
                    L_num_trucks,
                    O_error_message) = FALSE then
         return FALSE;
      end if;
   END LOOP; /* end while num trucks */
   /* Move what's left of the input order to the solution order.
      This is a two step process because we need to get the LTL order
      into newly allocated memory as we will free the inputed order's
      memory at the end of the splitting processing. */
   if MOVE_ORDER(I_ordhead_tbl,
                 L_in_ord_tbl,
                 O_error_message) = FALSE then
      return FALSE;
   end if;
   if L_in_ord_tbl IS NOT NULL then
      /* this is a LTL order */
      L_in_ord_tbl(1).sup_info.ltl_flag := LTL_TRUCK;
      if ADD_ORD_TO_START(L_sol_ord_tbl,
                          L_in_ord_tbl,
                          O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   /* attach the next order in the input order list to the end
      of the solution list */
   if OUTPUT_TO_SOLUTION(I_ordhead_tbl,
                         L_sol_ord_tbl,
                         O_error_message) = FALSE then
      return FALSE;
   end if;

   I_ordhead_tbl := L_sol_ord_tbl;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END SPLIT_BALANCE; /* end split_balance */
---------------------------------------------------------------------------------------------------------------
/*
 * Based on the most constraining constraint, count
 * how many truck loads are currently on the input order.
 */
---------------------------------------------------------------------------------
FUNCTION NUM_TRUCKS(I_ordhead_tbl   IN     ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                    O_num_trucks    IN OUT NUMBER,
                    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(30) := 'SPLIT_ORDER_SQL.NUM_TRUCKS';
   L_temp           NUMBER := 0;
   L_num_trucks     NUMBER := 0;

BEGIN
      for L_ctr IN 1..NUM_SPLIT_CNST
      LOOP
         if I_ordhead_tbl(1).sup_info.cnst_type(L_ctr) IS NOT NULL then

            L_temp := I_ordhead_tbl(1).sup_info.total_cnst_qty(L_ctr) /
                      I_ordhead_tbl(1).sup_info.cnst_value_reset(L_ctr);
            if L_temp > L_num_trucks then
               L_num_trucks := L_temp;
            end if;
         end if;
      END LOOP;
   O_num_trucks := L_num_trucks;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END NUM_TRUCKS; /* end num_trucks */
----------------------------------------------------------------------------------
/*
 * Given the input order, find the qty of each item that
 * should be placed on truck loads such that each truck has
 * a propotional amount of each item.  Based on the most
 * constraining constraint.
 */
----------------------------------------------------------------------------------
FUNCTION CREATE_MODEL_TRUCK(I_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                            I_num_trucks        IN     NUMBER,
                            O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(40):= 'SPLIT_ORDER_SQL.CREATE_MODEL_TRUCK';

   L_curr_sku_tbl       ADD_LINE_ITEM_SQL.ORDSKU_TBL;
   L_curr_loc_tbl       ADD_LINE_ITEM_SQL.ORDLOC_TBL;
   L_curr_loc_tbl_check ADD_LINE_ITEM_SQL.ORDLOC_TBL;
   L_curr_sku_tbl_check ADD_LINE_ITEM_SQL.ORDSKU_TBL;
   L_curr_sku_tbl_large ADD_LINE_ITEM_SQL.ORDSKU_TBL;

   L_loc_qty            NUMBER := 0;
   L_model_qty          NUMBER := 0;
   L_large_fraction     NUMBER := 0;
   L_total_qty          NUMBER := 0;
   L_ctr                NUMBER;
   L_prev_qty           NUMBER := 0;
   L_total_constraint   NUMBER := 0;
   L_idx                BINARY_INTEGER := NULL;

BEGIN

   /* determine which constraint is the most constraining */
   for L_ctr IN 1..NUM_SPLIT_CNST
   LOOP
      if I_ordhead_tbl(1).sup_info.cnst_type(L_ctr) IS NOT NULL then
         if (I_ordhead_tbl(1).sup_info.total_cnst_qty(L_ctr) /
              I_ordhead_tbl(1).sup_info.cnst_value_reset(L_ctr)) > L_prev_qty then
            I_ordhead_tbl(1).sup_info.cnst_index := L_ctr;
            L_prev_qty := I_ordhead_tbl(1).sup_info.total_cnst_qty(L_ctr) /
                          I_ordhead_tbl(1).sup_info.cnst_value_reset(L_ctr);
         end if;
      end if;
   END LOOP;

   /* create model truck based on most constraining constraint */
   L_curr_sku_tbl := I_ordhead_tbl(1).ordsku_tbl;
   L_loc_qty := 0;

   for L_sku_ctr IN 1..L_curr_sku_tbl.COUNT
   LOOP
      /* get the total qty for the item across all locs */
      L_curr_loc_tbl := L_curr_sku_tbl(L_sku_ctr).ordloc_tbl;
      L_loc_qty := 0;
      for L_loc_ctr IN 1..L_curr_loc_tbl.COUNT
      LOOP
         L_loc_qty := L_loc_qty + L_curr_loc_tbl(L_loc_ctr).qty;
      END LOOP;

         /* L_loc_qty = total number of qty of the item across all locs*/

      /* if there is not an increment of the item, it is not put on the model */
      if L_loc_qty < L_curr_sku_tbl(L_sku_ctr).splitting_increment then
         L_curr_sku_tbl(L_sku_ctr).model_qty := 0;
      /* there is an increment, put item on model */
      elsif L_loc_qty >= L_curr_sku_tbl(L_sku_ctr).splitting_increment then
         L_model_qty := L_loc_qty / I_num_trucks;
         L_curr_sku_tbl(L_sku_ctr).prorate_qty := L_model_qty;
         L_model_qty := SPLIT_ROUND_TO_PACKSIZE(L_model_qty,
                                                L_curr_sku_tbl(L_sku_ctr).splitting_increment,
                                                L_curr_sku_tbl(L_sku_ctr).round_pct,
                                                O_error_message);
         L_curr_sku_tbl(L_sku_ctr).model_qty := L_model_qty;
         /* since the map will not vary across locations, we can use the
            map from the first loc */
         L_total_qty := L_total_qty + (L_model_qty *
                        L_curr_sku_tbl(l_sku_ctr).ordloc_tbl(1).cnst_map(I_ordhead_tbl(1).sup_info.cnst_index));
      end if;

   END LOOP; /* End of for L_curr_sku_tbl */
   I_ordhead_tbl(1).ordsku_tbl := L_curr_sku_tbl;

   /* if model over the physical limit of the truck due to rounding,
      remove case(s) from model */
   while L_total_qty > (I_ordhead_tbl(1).sup_info.cnst_value(I_ordhead_tbl(1).sup_info.cnst_index) +
                              I_ordhead_tbl(1).sup_info.cnst_value(I_ordhead_tbl(1).sup_info.cnst_index) *
                              I_ordhead_tbl(1).sup_info.cnst_threshold(I_ordhead_tbl(1).sup_info.cnst_index))

   LOOP
      /* use greater than so model is not brought down to zero */
      L_curr_sku_tbl_check := I_ordhead_tbl(1).ordsku_tbl;
      L_large_fraction := 0;
      L_curr_sku_tbl_large.delete();

      for L_ctr IN 1..L_curr_sku_tbl_check.COUNT
      LOOP
         if L_curr_sku_tbl_check(L_ctr).model_qty > 0 then
          /*Calculate rounded up fraction is calculated as (model - prorate) / increment */
             L_curr_sku_tbl_check(L_ctr).fraction := (L_curr_sku_tbl_check(L_ctr).model_qty - L_curr_sku_tbl_check(L_ctr).prorate_qty) / L_curr_sku_tbl_check(1).splitting_increment;

             if L_curr_sku_tbl_large IS NULL then
                L_large_fraction := L_curr_sku_tbl_check(L_ctr).fraction;
                L_curr_sku_tbl_large := L_curr_sku_tbl_check;
             else
                if L_large_fraction < L_curr_sku_tbl_check(L_ctr).fraction then
                   L_large_fraction := L_curr_sku_tbl_check(L_ctr).fraction;
                   L_curr_sku_tbl_large := L_curr_sku_tbl_check;
                end if;
             end if;
         end if;  /*end of if curr sku rec check model qty > 0 */

      END LOOP; /*end of while curr sku ptr check is not null */

      L_curr_sku_tbl_large(1).model_qty := L_curr_sku_tbl_large(1).model_qty - L_curr_sku_tbl_large(1).splitting_increment;

      /* since the map will not vary across locations, we can use the map from the first loc */
      L_total_qty := L_total_qty - L_curr_sku_tbl_large(1).splitting_increment *
                      L_curr_sku_tbl_large(1).ordloc_tbl(1).cnst_map(I_ordhead_tbl(1).sup_info.cnst_index);
   END LOOP; /*end of while */

   /* if model under reset constraint due to rounding, add case(s) to model */
   L_curr_sku_tbl := I_ordhead_tbl(1).ordsku_tbl;
   L_curr_loc_tbl := L_curr_sku_tbl(1).ordloc_tbl;
   if L_curr_sku_tbl_large.COUNT > 0 then
      I_ordhead_tbl(1).ordsku_tbl := L_curr_sku_tbl_large;
   end if;

   while L_total_qty < I_ordhead_tbl(1).sup_info.cnst_value_reset(I_ordhead_tbl(1).sup_info.cnst_index)
   LOOP
      /* get the total qty for the item across all locs */
      L_curr_sku_tbl_check := L_curr_sku_tbl;
      L_curr_loc_tbl_check := L_curr_loc_tbl;
      L_large_fraction := 0;
      L_curr_sku_tbl_large.delete();

      for j in 1..L_curr_sku_tbl_check.COUNT
      LOOP
         L_loc_qty := 0;
         for L_loc_ctr in 1..L_curr_loc_tbl_check.COUNT
         LOOP
            L_loc_qty := L_loc_qty + L_curr_loc_tbl_check(L_loc_ctr).qty;
         END LOOP;
         /*Calculate rounded down fraction is calculated as (prorate - model) / increment */
         L_curr_sku_tbl_check(j).fraction := (L_curr_sku_tbl_check(j).prorate_qty - L_curr_sku_tbl_check(j).model_qty) / L_curr_sku_tbl_check(j).splitting_increment;

         if (L_curr_sku_tbl_check(j).fraction > 0 AND
             L_loc_qty >= L_curr_sku_tbl_check(j).model_qty + L_curr_sku_tbl_check(j).splitting_increment AND
             L_total_qty + (L_curr_sku_tbl_check(j).splitting_increment * L_curr_sku_tbl_check(j).ordloc_tbl(1).cnst_map(I_ordhead_tbl(1).sup_info.cnst_index))
             <= (I_ordhead_tbl(1).sup_info.cnst_value(I_ordhead_tbl(1).sup_info.cnst_index) +
                 I_ordhead_tbl(1).sup_info.cnst_value(I_ordhead_tbl(1).sup_info.cnst_index) *
                 I_ordhead_tbl(1).sup_info.cnst_threshold(I_ordhead_tbl(1).sup_info.cnst_index))) then
            if L_large_fraction < L_curr_sku_tbl_check(j).fraction then
                L_large_fraction := L_curr_sku_tbl_check(j).fraction;
                L_curr_sku_tbl_large := L_curr_sku_tbl_check;
            end if;

         end if;
         L_idx := L_curr_sku_tbl_check.NEXT(j);
         if L_idx IS NOT NULL then
            L_curr_loc_tbl_check := L_curr_sku_tbl_check(j).ordloc_tbl;
         end if;
      END LOOP;

      /* don't add more to model than is avaible on the order */
      if L_curr_sku_tbl_large IS NOT NULL AND L_curr_sku_tbl_large.count > 0 then
         L_curr_sku_tbl_large(1).model_qty := L_curr_sku_tbl_large(1).model_qty + L_curr_sku_tbl_large(1).splitting_increment;
         /* since the map will not vary across locations, we can use the
            map from the first loc */
         L_total_qty := L_total_qty + L_curr_sku_tbl_large(1).splitting_increment *
                         L_curr_sku_tbl_large(1).ordloc_tbl(1).cnst_map(I_ordhead_tbl(1).sup_info.cnst_index);
      else
         EXIT;
      end if;
      if L_curr_sku_tbl_large.COUNT > 0 then
         I_ordhead_tbl(1).ordsku_tbl := L_curr_sku_tbl_large;
      end if;
   END LOOP;

   /* if model under inital constraint, set ltl flag */
   if L_total_qty < I_ordhead_tbl(1).sup_info.cnst_value(I_ordhead_tbl(1).sup_info.cnst_index) then
      I_ordhead_tbl(1).sup_info.ltl_flag := LTL_TRUCK;
   else
      I_ordhead_tbl(1).sup_info.ltl_flag := FULL_TRUCK;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_MODEL_TRUCK; /* end create_model_truck */
---------------------------------------------------------------------------------------------------------------------
/*
 * This function will try to split the input order using the
 *  item sequence approach -- create as many full trucks
 *  that consist of a single item as possible.
 *
 * Loop for all items on the order
 *
 *    Get the qty of the current item such that adding an additional
 *     increment of it to the item would push it over the reset
 *     constraint (TRUCK_QTY).
 *
 *    Loop until all stock of item is removed from SOURCE order
 *
 *       Get status of WORKING order (see truck_status())
 *
 *       if status is EMPTY_ORDER
 *          move TRUCK_QTY of item from SOURCE to WORKING
 *
 *       if status is ADD_CASE
 *          move an increment of item from SOURCE to WORKING
 *
 *       if status is FT_STOP
 *          move an increment of item from SOURCE to WORKING
 *          move WORKING to SOLUTION
 *
 *       if status is RFT_STOP
 *          move WORKING to SOLUTION
 *
 *       if status is LTL_STOP
 *          set WORKING's ltl_flag
 *          move WORKING to SOLUTION
 *
 *    End stock Loop
 *
 *    Move left overs from WORKING to HOLDING
 *
 * End item Loop
 *
 * Split HOLDING by calling split_cleanup()
 * Move HOLDING to SOLUTION
 * Replace SOURCE with SOLUTION list in the inputted OLL
 * Free the memory used by SOURCE
 *
 */
----------------------------------------------------------------------------------------------
FUNCTION SPLIT_ITEMSEQ(I_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                       O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_curr_sku_tbl      ADD_LINE_ITEM_SQL.ORDSKU_TBL;
   L_curr_loc_tbl      ADD_LINE_ITEM_SQL.ORDLOC_TBL;
   L_program           VARCHAR2(30) := 'SPLIT_ORDER_SQL.SPLIT_ITEMSEQ';
   L_in_ord_tbl        ADD_LINE_ITEM_SQL.ORDHEAD_TBL;
   L_sol_ord_tbl       ADD_LINE_ITEM_SQL.ORDHEAD_TBL;
   L_hold_ord_tbl      ADD_LINE_ITEM_SQL.ORDHEAD_TBL;

   L_truck_qty         NUMBER(10):= 0;
   L_stock_exist_ind   NUMBER(1) := 1;
   L_truck_status      NUMBER(1) := EMPTY_ORDER;
   L_idx               BINARY_INTEGER := NULL;

BEGIN
   /* walk through sku nodes */
   L_curr_sku_tbl := I_ordhead_tbl(1).ordsku_tbl;
   for L_sku_ctr in 1..L_curr_sku_tbl.COUNT
   LOOP
      L_curr_loc_tbl := L_curr_sku_tbl(L_sku_ctr).ordloc_tbl;
      /* get number of units that will take order to just below
         the truck constraints */
      if ITEM_QTY_IN_TRUCK(I_ordhead_tbl,
                           L_curr_sku_tbl,
                           L_truck_qty,
                           O_error_message) = FALSE then
         return FALSE;
      end if;
      L_stock_exist_ind := 1;
      while L_stock_exist_ind = 1
      LOOP
         if TRUCK_STATUS(L_in_ord_tbl,
                         L_curr_sku_tbl(L_sku_ctr).splitting_increment,
                         L_curr_loc_tbl(1).cnst_map,
                         L_truck_status,
                         O_error_message) = FALSE then
            return FALSE;
         end if;
         if L_truck_status = EMPTY_ORDER then
            /* Move one truck qty from input order to working order.
               This will take us to position where next case added will
               exceed the reset constraint */
            if MOVE_CASE_ALL(I_ordhead_tbl(1),
                             L_curr_sku_tbl(L_sku_ctr),
                             L_curr_loc_tbl,
                             L_in_ord_tbl,
                             L_CASE,
                             L_truck_qty,
                             L_stock_exist_ind,
                             O_error_message) = FALSE then
               return FALSE;
            end if;
         end if;
         if L_truck_status = ADD_CASE OR L_truck_status = FT_STOP then
            /* move increment from input order to working order */
            if MOVE_CASE_ALL(I_ordhead_tbl(1),
                             L_curr_sku_tbl(L_sku_ctr),
                             L_curr_loc_tbl,
                             L_in_ord_tbl,
                             L_CASE,
                             L_curr_sku_tbl(L_sku_ctr).splitting_increment,
                             L_stock_exist_ind,
                             O_error_message) = FALSE then
               return FALSE;
            end if;
         end if;
         /* if the working order is full move it to the
            solution list */
         if (L_truck_status = FT_STOP) OR
            (L_truck_status = RFT_STOP) OR
            (L_truck_status = LTL_STOP) then
            if L_truck_status = LTL_STOP then
               L_in_ord_tbl(1).sup_info.ltl_flag := LTL_TRUCK;
            else
               L_in_ord_tbl(1).sup_info.ltl_flag := FULL_TRUCK;
            end if;
            /* add working order to begining of the solution order list */
            if ADD_ORD_TO_START(L_sol_ord_tbl,
                                L_in_ord_tbl,
                                O_error_message) = FALSE then
               return FALSE;
            end if;
         end if;
      END LOOP; /* while stock exist */

      /* add any leftover to the holding struct */
      if L_in_ord_tbl IS NOT NULL AND L_in_ord_tbl.COUNT > 0 then
         /* add working order to begining of the holding order list */
         if MOVE_CASE_ALL(L_in_ord_tbl(1),
                          L_in_ord_tbl(1).ordsku_tbl(1),
                          L_in_ord_tbl(1).ordsku_tbl(1).ordloc_tbl,
                          L_hold_ord_tbl,
                          L_ALL,
                          1,
                          L_stock_exist_ind,
                          O_error_message) = FALSE then
            return FALSE;
         end if;
         L_in_ord_tbl.DELETE();
      end if;
      L_idx := L_curr_sku_tbl.FIRST;
      L_idx := L_curr_sku_tbl.NEXT(L_idx);
      if L_idx IS NULL then
         L_curr_loc_tbl.DELETE();
      else
         L_curr_loc_tbl := L_curr_sku_tbl(L_sku_ctr).ordloc_tbl;
      end if;
   END LOOP; /* loop sku nodes */
   /* process left over qtys and move results to solution */
   if L_hold_ord_tbl IS NOT NULL AND L_hold_ord_tbl.COUNT > 0 then
      if SPLIT_CLEANUP(L_hold_ord_tbl,
                       O_error_message) = FALSE then
         return FALSE;
      end if;
      if ADD_ORD_TO_START(L_sol_ord_tbl,
                          L_hold_ord_tbl,
                          O_error_message) = FALSE  then
         return FALSE;
      end if;
   end if;
   /* attach the next order in the input order list to the end
      of the solution list */
   if OUTPUT_TO_SOLUTION(I_ordhead_tbl,
                         L_sol_ord_tbl,
                         O_error_message) = FALSE then
      return FALSE;
   end if;
   I_ordhead_tbl := L_sol_ord_tbl;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END SPLIT_ITEMSEQ; /* end split_itemseq */
---------------------------------------------------------------------------------------------------------------
/*
 * Get the qty of the current item such that adding an additional
 *  increment of it to the item would push it over the reset
 */
---------------------------------------------------------------------------------------------------------------
FUNCTION ITEM_QTY_IN_TRUCK(I_ordhead_tbl       IN     ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                           I_ordsku_tbl        IN     ADD_LINE_ITEM_SQL.ORDSKU_TBL,
                           O_qty_in_truck      IN OUT NUMBER,
                           O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_temp_qty           NUMBER(10) := 0;
   L_temp_qty_in_truck  NUMBER(10) := 0;
   L_truck_qty          NUMBER(10) := -1;
   L_program            VARCHAR2(40) := 'SPLIT_ORDER_SQL.ITEM_QTY_IN_TRUCK';

BEGIN
   /* get number qty of item that can fit on one truck
      take the lowest qty (ie the most constraining constraint */

   for i IN 1..NUM_SPLIT_CNST
   LOOP
      if I_ordhead_tbl(1).sup_info.cnst_type(i) IS NOT NULL then
         /* we can use the map from the first node as the map will not vary
            by location */
         L_temp_qty := I_ordhead_tbl(1).sup_info.cnst_value_reset(i) /
                       I_ordsku_tbl(1).ordloc_tbl(1).cnst_map(i);
        /* rounding - always round down (send rnd pct 100),
           will add last increment--handle boundry condition
           elsewhere (truck_status) */
         L_temp_qty_in_truck := SPLIT_ROUND_TO_PACKSIZE(L_temp_qty,
                                                        I_ordsku_tbl(1).splitting_increment,
                                                        100.0,
                                                        O_error_message);
         if L_temp_qty_in_truck * I_ordsku_tbl(1).ordloc_tbl(1).cnst_map(i) =
             I_ordhead_tbl(1).sup_info.cnst_value_reset(i) then
            L_temp_qty := (L_temp_qty_in_truck - I_ordsku_tbl(1).splitting_increment);
         else
            L_temp_qty := L_temp_qty_in_truck;
         end if;
         if ((L_temp_qty < L_truck_qty) OR (L_truck_qty = -1)) then
            L_truck_qty := L_temp_qty;
         end if;
      end if;
   END LOOP;
   O_qty_in_truck := L_truck_qty;

   return TRUE;
EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
   return FALSE;
END ITEM_QTY_IN_TRUCK; /* item_qty_in_truck */
---------------------------------------------------------------------------------------------------------------
/*
 * This function will try to split the input order by moving increments
 *  of each item from the SOURCE order unit the new order is a full truck.
 *
 * If the SOURCE order is less than a full truck
 *    Move SOURCE to WORKING
 *
 * else if the SOURCE order is more than a full truck
 *
 *    Loop on items
 *
 *       Loop until all stock of item is removed from SOURCE order
 *
 *          Get status of WORKING order (see truck_status())
 *
 *          if status is EMPTY_ORDER, ADD_CASE, FT_STOP
 *             move ncrement of item from SOURCE to WORKING
 *
 *          if status is FT_STOP, RFT_STOP, LTL_STOP
 *             if status is LTL_STOP
 *                set WORKING's ltl_flag
 *             move WORKING to SOLUTION
 *
 *       End stock Loop
 *
 *       Move left overs from WORKING to HOLDING
 *
 *    End item Loop
 *
 * End if
 *
 * move WORKING to SOLUTION
 * Replace SOURCE with SOLUTION list in the inputted OLL
 * Free the memory used by SOURCE
 */
---------------------------------------------------------------------------------------------------------------
FUNCTION SPLIT_CLEANUP(I_ordhead_tbl   IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                       O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(30):= 'SPLIT_ORDER_SQL.SPLIT_CLEANUP';
   L_curr_ord_tbl     ADD_LINE_ITEM_SQL.ORDHEAD_TBL := I_ordhead_tbl;
   L_curr_sku_tbl     ADD_LINE_ITEM_SQL.ORDSKU_TBL;
   L_curr_loc_tbl     ADD_LINE_ITEM_SQL.ORDLOC_TBL;

   L_in_ord_tbl       ADD_LINE_ITEM_SQL.ORDHEAD_TBL;
   L_sol_ord_tbl      ADD_LINE_ITEM_SQL.ORDHEAD_TBL;

   L_num_trucks       NUMBER;
   l_stock_exist_ind  NUMBER := 1;
   l_truck_status     NUMBER := ADD_CASE;
   L_idx              BINARY_INTEGER := NULL;

BEGIN

   if NUM_TRUCKS(L_curr_ord_tbl,
                 L_num_trucks,
                 O_error_message) = FALSE then
      return FALSE;
   end if;

   if L_num_trucks >= 1 then
      /* walk through sku nodes */
      L_curr_sku_tbl := L_curr_ord_tbl(1).ordsku_tbl;
      for L_sku_ctr in 1..L_curr_sku_tbl.COUNT
      LOOP
         L_curr_loc_tbl := L_curr_sku_tbl(L_sku_ctr).ordloc_tbl;
         L_stock_exist_ind := 1;
         while L_stock_exist_ind = 1
         LOOP
            if TRUCK_STATUS(L_in_ord_tbl,
                            L_curr_sku_tbl(L_sku_ctr).splitting_increment,
                            L_curr_loc_tbl(1).cnst_map,
                            L_truck_status,
                            O_error_message) = FALSE then
               return FALSE;
            end if;

            if ((L_truck_status = ADD_CASE) OR
                (L_truck_status = EMPTY_ORDER) OR
                (L_truck_status = FT_STOP)) then
               /* move increment from input order to working order */
               if MOVE_CASE_ALL(L_curr_ord_tbl(1),
                                L_curr_sku_tbl(L_sku_ctr),
                                L_curr_loc_tbl,
                                L_in_ord_tbl,
                                L_CASE,
                                L_curr_sku_tbl(L_sku_ctr).splitting_increment,
                                L_stock_exist_ind,
                                O_error_message) = FALSE then
                  return FALSE;
               end if;
            end if;

            /* if the working order is full move it to the
               solution list */
            if ((L_truck_status = FT_STOP) OR
                (L_truck_status = RFT_STOP) OR
                (L_truck_status = LTL_STOP)) then
               if (L_truck_status = LTL_STOP) then
                  L_in_ord_tbl(1).sup_info.ltl_flag := LTL_TRUCK;
               else
                  L_in_ord_tbl(1).sup_info.ltl_flag := FULL_TRUCK;
               end if;
               /* add working order to begining of the solution order list */
               if ADD_ORD_TO_START(L_sol_ord_tbl,
                                   L_in_ord_tbl,
                                   O_error_message) = FALSE then
                  return FALSE;
               end if;
            end if;
         END LOOP; /* while stock exist */

         L_idx := L_curr_sku_tbl.NEXT(L_sku_ctr);
         if L_idx IS NULL then
            L_curr_loc_tbl.DELETE();
         else
            L_curr_loc_tbl := L_curr_sku_tbl(L_sku_ctr).ordloc_tbl;
         end if;

      END LOOP; /* loop sku nodes */
   else /* num_trucks <= 1 */
      if MOVE_ORDER(L_curr_ord_tbl,
                    L_in_ord_tbl,
                    O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   /* move possible LTL order to solution list */
   if L_in_ord_tbl IS NOT NULL AND L_in_ord_tbl.COUNT > 0 then
      /* add working order to begining of the solution order list */
      L_in_ord_tbl(1).sup_info.ltl_flag := LTL_TRUCK;
      if ADD_ORD_TO_START(L_sol_ord_tbl,
                          L_in_ord_tbl,
                          O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   I_ordhead_tbl := L_sol_ord_tbl;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END SPLIT_CLEANUP; /* end split_cleanup */
---------------------------------------------------------------------------------------------------------------
/*
 * Determine the status of the passed in order in relation it's truck constrints.
 * This entire function is based on the most constrainting constraint.
 *
 *  A) If the passed in truck is empty
 *     output status is EMPTY_ORDER
 *
 *  B) If the passed in truck is not empty
 *
 *      1) Determine the starting status of the order
 *
 *          i) BELOW if the total truck is below the truck constaints
 *         ii) OVER if the total truck is over the truck constrints
 *
 *      2) Determine the status of the order after adding one more
 *         order increment of the current item to it.
 *
 *          i) RTF_STOP if addition of incrememt takes order over
 *             physical limit and starting status was OVER
 *         ii) LTL_STOP if addition of incrememt takes order over
 *             physical limit and starting status was BELOW
 *        iii) FT_STOP if addition of increment takes order over
 *             reset constraint but not over physical limit
 *         iv) ADD_CASE if addition of increment does not take order
 *             over the truck constraints
 *
 ************************************************************************
 *                                                                      *
 * # denotes the setting of the starting status                         *
 * + denotes the setting of the output status (status after the         *
 *   addition of an order increment)                                    *
 *                                                                      *
 *----------------------------------------------------------------------*
 *                                                                      *
 *                 # OVER                                               *
 *                 if starting status = BELOW                           *
 *                    + RFT_STOP                                        *
 *                 else starting status = OVER                          *
 *                    + LTL_STOP                                        *
 *                                                                      *
 * 44,000 lbs --------------------------------------- physical limit    *
 *                                                                      *
 *                 # OVER                                               *
 *                 + FT_STOP                                            *
 *                                                                      *
 * 42,000 lbs --------------------------------------- reset constraint  *
 *                                                                      *
 *                 # OVER                                               *
 *                 + ADD_CASE                                           *
 *                                                                      *
 * 40,000 lbs --------------------------------------- constraint        *
 *                                                                      *
 *                 # BELOW                                              *
 *                 + ADD_CASE                                           *
 *                                                                      *
 ************************************************************************
 */
---------------------------------------------------------------------------------------------------------------
FUNCTION TRUCK_STATUS(I_ordhead_tbl       IN     ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                      I_increment         IN     NUMBER,
                      I_map               IN     ADD_LINE_ITEM_SQL.CNST_QTY_TBL,
                      O_truck_status      IN OUT NUMBER,
                      O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(30):= 'SPLIT_ORDER_SQL.TRUCK_STATUS';
   L_starting_status    NUMBER := BELOW;
   L_ending_status      NUMBER := 0;

   /* initialize the output status to the lowest possible level */
   L_output_status      NUMBER := ADD_CASE;
   L_ctr                NUMBER := 0;

BEGIN

   /* empty order can always be added to */
   if I_ordhead_tbl.COUNT = 0 then
      O_truck_status := EMPTY_ORDER;
      return TRUE;
   end if;

   /* get starting status */
   for i IN 1..NUM_SPLIT_CNST
   LOOP
      if I_ordhead_tbl(1).sup_info.cnst_type(i) IS NOT NULL then
         if (I_ordhead_tbl(1).sup_info.total_cnst_qty(i) >
             I_ordhead_tbl(1).sup_info.cnst_value_reset(i)) then
            L_starting_status := OVER;
         end if;
      end if;
   END LOOP;

   /* determine status after adding increment */
   for j in 1..NUM_SPLIT_CNST
   LOOP
      if I_ordhead_tbl(1).sup_info.cnst_type(j) IS NOT NULL then
         /* adding increment pushes truck over physical limit */
         if ((I_ordhead_tbl(1).sup_info.total_cnst_qty(j) + I_increment * I_map(j)) >
             (I_ordhead_tbl(1).sup_info.cnst_value(j) +
              I_ordhead_tbl(1).sup_info.cnst_value(j) *
              I_ordhead_tbl(1).sup_info.cnst_threshold(j))) then
            if (L_starting_status = OVER) then
               L_ending_status := RFT_STOP;
            else
               L_ending_status := LTL_STOP;
            end if;
         /* adding increment pushes truck over reset constraint but not over physical limit */
         elsif ((I_ordhead_tbl(1).sup_info.total_cnst_qty(j) + I_increment * I_map(j)) >=
               I_ordhead_tbl(1).sup_info.cnst_value_reset(j)) then
            L_ending_status := FT_STOP;
         else
            L_ending_status := ADD_CASE;
         end if;
         /* we want to use the greatest or most limiting truck status */
         if (L_ending_status > L_output_status) then
            L_output_status := L_ending_status;
         end if;
      end if;
   END LOOP;

   O_truck_status := L_output_status;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END TRUCK_STATUS; /* end truck_status */
---------------------------------------------------------------------------------------------------------------
/*
 Based on the model qtys on the input order, this function
 will create an order pointed to by the output order.  The
 stock flag will be triggered when the the qty remaining on
 the input order AFTER a model truck has been pulled off
 is not enough to build a second model truck.
 */
---------------------------------------------------------------------------------------------------------------
FUNCTION MODEL_TO_SOLUTION(I_ordhead_tbl       IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                           O_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                           O_stock_out         IN OUT NUMBER,
                           O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(40) := 'SPLIT_ORDER_SQL.MODEL_TO_SOLUTION';
   L_model_qty         NUMBER := 0;
   L_move_qty          NUMBER := 0;
   L_loc_qty           NUMBER := 0;

BEGIN
   for L_sku_ctr IN 1..I_ordhead_tbl(1).ordsku_tbl.COUNT
   LOOP
      if I_ordhead_tbl(1).ordsku_tbl(L_sku_ctr).model_qty > 0 then
         L_model_qty := I_ordhead_tbl(1).ordsku_tbl(L_sku_ctr).model_qty;
         L_loc_qty := 0;
         for L_loc_ctr in 1..I_ordhead_tbl(1).ordsku_tbl(L_sku_ctr).ordloc_tbl.COUNT
         LOOP
            if L_model_qty > 0 then
               if I_ordhead_tbl(1).ordsku_tbl(L_sku_ctr).ordloc_tbl(L_loc_ctr).qty >= L_model_qty then
                  L_move_qty := l_model_qty;
                  L_model_qty := 0;
               else
                  L_move_qty := I_ordhead_tbl(1).ordsku_tbl(L_sku_ctr).ordloc_tbl(L_loc_ctr).qty;
                  L_model_qty := L_model_qty - L_move_qty;
               end if;

               if L_move_qty > 0 then
                  if MOVE_STOCK(I_ordhead_tbl(1),
                                I_ordhead_tbl(1).ordsku_tbl(L_sku_ctr),
                                I_ordhead_tbl(1).ordsku_tbl(L_sku_ctr).ordloc_tbl(L_loc_ctr),
                                O_ordhead_tbl,
                                L_move_qty,
                                O_error_message) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if;

            L_loc_qty := L_loc_qty + I_ordhead_tbl(1).ordsku_tbl(L_sku_ctr).ordloc_tbl(L_loc_ctr).qty;

         END LOOP;
         if L_loc_qty < I_ordhead_tbl(1).ordsku_tbl(L_sku_ctr).model_qty then
            O_stock_out := 0;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END MODEL_TO_SOLUTION; /* end model_to_solution */
---------------------------------------------------------------------------------------------------------------
FUNCTION ADD_ORD_TO_START(I_to_head_tbl    IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                          I_from_head_tbl  IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                          O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(40) := 'SPLIT_ORDER_SQL.ADD_ORD_TO_START';
   L_local_to            ADD_LINE_ITEM_SQL.ORDHEAD_TBL;

BEGIN
   if I_from_head_tbl is NULL or I_from_head_tbl.count = 0 then
      return TRUE;
   end if;
   if I_to_head_tbl is NULL or I_to_head_tbl.count = 0 then
      I_to_head_tbl := I_from_head_tbl;
   else
      L_local_to := I_to_head_tbl;
      I_to_head_tbl := I_from_head_tbl;
      for i in 1..L_local_to.count LOOP
         I_to_head_tbl(I_to_head_tbl.count + 1) := L_local_to(i);
      END LOOP;
   end if;
   I_from_head_tbl.DELETE();
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END ADD_ORD_TO_START; /* end add_ord_to_start */
---------------------------------------------------------------------------------------------------------------
FUNCTION MOVE_ORDER(I_ordhead_tbl       IN     ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                    O_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(30) := 'SPLIT_ORDER_SQL.MOVE_ORDER';
   L_curr_ord_tbl   ADD_LINE_ITEM_SQL.ORDHEAD_TBL := I_ordhead_tbl;
   L_curr_sku_tbl   ADD_LINE_ITEM_SQL.ORDSKU_TBL;
   L_dummy          NUMBER := 0;
   
BEGIN

   L_curr_sku_tbl := L_curr_ord_tbl(1).ordsku_tbl;
   for L_sku_ctr IN 1..L_curr_sku_tbl.COUNT
   LOOP
      if MOVE_CASE_ALL(L_curr_ord_tbl(1),
                       L_curr_sku_tbl(L_sku_ctr),
                       L_curr_sku_tbl(L_sku_ctr).ordloc_tbl,
                       O_ordhead_tbl,
                       L_ALL,
                       1,
                       L_dummy,
                       O_error_message) = FALSE then
         return FALSE;
      end if;
   END LOOP; /* loop sku nodes */

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END MOVE_ORDER; /* end move_order */
---------------------------------------------------------------------------------------------------------------
FUNCTION MOVE_CASE_ALL(I_ordhead_rec       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_REC,
                       I_ordsku_rec        IN     ADD_LINE_ITEM_SQL.ORDSKU_REC,
                       I_ordloc_tbl        IN OUT ADD_LINE_ITEM_SQL.ORDLOC_TBL,
                       O_ordhead_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                       I_case_or_all_ind   IN     NUMBER,
                       I_move_qty          IN     NUMBER,
                       O_stock_exist_ind   IN OUT NUMBER,
                       O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(30) := 'SPLIT_ORDER_SQL.MOVE_CASE_ALL';
   L_tmploc_tbl   ADD_LINE_ITEM_SQL.ORDLOC_TBL;
   L_qty          NUMBER := 0;
   L_need_qty     NUMBER := I_move_qty;
   L_loc_ctr      BINARY_INTEGER := NULL;

BEGIN
   L_tmploc_tbl := I_ordloc_tbl;
   if L_tmploc_tbl is NOT NULL and L_tmploc_tbl.COUNT > 0 then
      L_loc_ctr := L_tmploc_tbl.FIRST;
   end if;

   while L_need_qty > 0 and L_loc_ctr is NOT NULL
   LOOP
       if I_case_or_all_ind = L_ALL then
          L_qty := L_tmploc_tbl(L_loc_ctr).qty;
       else
          /* take a full increment */
          if L_tmploc_tbl(L_loc_ctr).qty >= L_need_qty then
             L_qty := L_need_qty;
             L_need_qty := 0;
          else /* not a full increment take all there */
             L_qty := L_tmploc_tbl(L_loc_ctr).qty;
             L_need_qty := L_need_qty - L_tmploc_tbl(L_loc_ctr).qty;
          end if;
       end if;
       /* move the stock */
       if L_qty > 0 then
          if MOVE_STOCK(I_ordhead_rec,
                        I_ordsku_rec,
                        L_tmploc_tbl(L_loc_ctr),
                        O_ordhead_tbl,
                        L_qty,
                        O_error_message) = FALSE then
             return FALSE;
          end if;
       end if;

       if L_tmploc_tbl(L_loc_ctr).qty = 0 then
          L_loc_ctr := L_tmploc_tbl.NEXT(L_loc_ctr);
          if L_loc_ctr IS NULL then
             O_stock_exist_ind := 0;
          end if;
       end if;
   END LOOP;
   I_ordloc_tbl := L_tmploc_tbl;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END MOVE_CASE_ALL; /* end move_case_all */
---------------------------------------------------------------------------------------------------------------
FUNCTION MOVE_STOCK(I_ordhead_rec   IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_REC,
                    I_ordsku_rec    IN     ADD_LINE_ITEM_SQL.ORDSKU_REC,
                    I_ordloc_rec    IN OUT ADD_LINE_ITEM_SQL.ORDLOC_REC,
                    O_ordhead_tbl   IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                    I_need_qty      IN     NUMBER,
                    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(30) := 'SPLIT_ORDER_SQL.MOVE_STOCK';
   L_line_info    ADD_LINE_ITEM_SQL.DRIV_CUR_INFO_PROCESS_REC;
   L_tmploc_rec   ADD_LINE_ITEM_SQL.ORDLOC_REC;

BEGIN

   L_tmploc_rec := I_ordloc_rec;


   L_line_info.source_type := L_tmploc_rec.source_type;
   L_line_info.supplier := I_ordhead_rec.supplier;
   L_line_info.dept := I_ordhead_rec.dept;
   L_line_info.order_status := I_ordhead_rec.order_status;
   L_line_info.item := I_ordsku_rec.item;

   L_line_info.contract := I_ordhead_rec.contract;
   L_line_info.contract_type := I_ordhead_rec.contract_type;
   L_line_info.loc_country := I_ordhead_rec.import_country_id;

   L_line_info.ctry := I_ordsku_rec.ctry;
   L_line_info.pack_size := I_ordsku_rec.pack_size;

   L_line_info.supp_lead_time := 0;
   L_line_info.pickup_lead_time := 0;
   L_line_info.eso := 0;
   L_line_info.aso := 0;

   L_line_info.contract_terms := I_ordhead_rec.contract_terms;
   L_line_info.contract_approval_ind := I_ordhead_rec.contract_approval_ind;
   L_line_info.contract_header_rowid := I_ordhead_rec.contract_header_rowid;

   L_line_info.pack_ind := L_tmploc_rec.pack_ind;
   L_line_info.loc := L_tmploc_rec.loc;
   L_line_info.phy_loc := L_tmploc_rec.phy_loc;
   L_line_info.repl_wh_link := L_tmploc_rec.repl_wh_link;
   L_line_info.loc_type := L_tmploc_rec.loc_type;
   L_line_info.due_ind := L_tmploc_rec.due_ind;
   L_line_info.xdock_ind := L_tmploc_rec.xdock_ind;
   L_line_info.xdock_store := L_tmploc_rec.xdock_store;
   L_line_info.unit_cost := L_tmploc_rec.unit_cost;
   L_line_info.unit_cost_init := L_tmploc_rec.unit_cost_init;
   L_line_info.non_scale_ind := L_tmploc_rec.non_scale_ind;
   L_line_info.tsf_po_link := L_tmploc_rec.tsf_po_link;
   L_line_info.pool_supp := I_ordhead_rec.pool_supp;
   L_line_info.file_id := I_ordhead_rec.file_id;


   L_line_info.rr_rowid := L_tmploc_rec.rr_rowid;
   L_line_info.ir_rowid := L_tmploc_rec.ir_rowid;
   L_line_info.bw_rowid := L_tmploc_rec.bw_rowid;

   /* scaling specific */
   L_line_info.split_ref_ord_no := I_ordhead_rec.split_ref_ord_no;
   L_line_info.splitting_increment := I_ordsku_rec.splitting_increment;
   L_line_info.round_pct := I_ordsku_rec.round_pct;
   L_line_info.model_qty := I_ordsku_rec.model_qty;

   for i in 1..NUM_SPLIT_CNST
   LOOP
      if I_ordhead_rec.sup_info.cnst_type(i) IS NOT NULL then
         L_line_info.cnst_map(i) := L_tmploc_rec.cnst_map(i);
         L_line_info.cnst_qty(i) :=
                   I_need_qty * L_tmploc_rec.cnst_map(i);
         L_tmploc_rec.cnst_qty(i) := L_tmploc_rec.cnst_qty(i) - I_need_qty * L_tmploc_rec.cnst_map(i);
      else
         L_line_info.cnst_map(i) := 0;
         L_line_info.cnst_qty(i) := 0;
      end if;
   END LOOP;

   L_line_info.qty := I_need_qty;
   L_tmploc_rec.qty := L_tmploc_rec.qty - I_need_qty;
   L_line_info.last_rounded_qty := I_need_qty;
   L_tmploc_rec.last_rounded_qty := L_tmploc_rec.last_rounded_qty - I_need_qty;
   L_line_info.last_grp_rounded_qty := L_tmploc_rec.last_grp_rounded_qty;

   /* Add the item/location to the order */
   if ADD_LINE_ITEM_SQL.ADD_TO_ORDER(O_ordhead_tbl,
                                     L_line_info,
                                     I_ordhead_rec.sup_info,
                                     NULL,
                                     O_error_message) = FALSE then
      return FALSE;
   end if;

   /* decrment the from order's total cnst qty and add to the new orders */
   for j in 1..NUM_SPLIT_CNST
   LOOP
      if I_ordhead_rec.sup_info.cnst_type(j) IS NOT NULL then
         I_ordhead_rec.sup_info.total_cnst_qty(j) := I_ordhead_rec.sup_info.total_cnst_qty(j) - L_line_info.cnst_map(j) * L_line_info.qty;
         O_ordhead_tbl(1).sup_info.total_cnst_qty(j) := O_ordhead_tbl(1).sup_info.total_cnst_qty(j) + L_line_info.cnst_map(j) * L_line_info.qty;
      end if;
   END LOOP;

   I_ordloc_rec := L_tmploc_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END MOVE_STOCK; /* end move_stock */
---------------------------------------------------------------------------------------------------------------
FUNCTION OUTPUT_TO_SOLUTION(I_ordhead_tbl        IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                            I_sol_head_tbl       IN OUT ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                            O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_curr_ord_tbl      ADD_LINE_ITEM_SQL.ORDHEAD_TBL := I_sol_head_tbl;
   L_ord_ctr           BINARY_INTEGER;
   L_program           VARCHAR2(35) := 'SPLIT_ORDER_SQL.OUTPUT_TO_SOLUTION';
   L_idx               BINARY_INTEGER := NULL;

BEGIN
   if I_sol_head_tbl.COUNT > 0 AND I_ordhead_tbl.COUNT > 1 then
      for i in 2..I_ordhead_tbl.COUNT 
      LOOP
         I_sol_head_tbl(I_sol_head_tbl.COUNT + i) := I_ordhead_tbl(i);
         I_ordhead_tbl.delete(i);
      END LOOP;
   end if; 

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END OUTPUT_TO_SOLUTION; /* end output_to_solution */
---------------------------------------------------------------------------------------------------------------
FUNCTION UOM_CONVERSION(I_from_uom      IN     VARCHAR2,
                        I_to_uom        IN     VARCHAR2,
                        I_from_value    IN     NUMBER,
                        O_to_value      IN OUT NUMBER,
                        O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(40) := 'SPLIT_ORDER_SQL.UOM_CONVERSION';
   L_factor      NUMBER(20,10);
   L_operator    VARCHAR2(1);

   cursor C_UOM IS
     select factor,
            operator
       from uom_conversion
      where from_uom = I_from_uom
        and to_uom = I_to_uom;

BEGIN
   if I_from_uom = I_to_uom then
     O_to_value := I_from_uom;
     return TRUE;
   end if;

   open C_UOM;
   fetch C_UOM INTO L_factor,
                    L_operator;
   if L_operator = 'M' then
      O_to_value := I_from_value * L_factor;
   else
      O_to_value := I_from_value / L_factor;
   end if;
   close C_UOM;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END UOM_CONVERSION; /* end uom_conversion */
---------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORDER(I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(30) := 'SPLIT_ORDER_SQL.DELETE_ORDER';
   /* std plsql vars */
   L_failed            NUMBER := 0;
   L_exception         NUMBER := 0;
   L_err_message       VARCHAR2(2000) := NULL;
   L_franchise_po_ind  VARCHAR2(2);
   L_sqlcode           NUMBER := 0;
   L_return_code       BOOLEAN;
   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_table             VARCHAR2(30) := 'ORDHEAD';

   cursor C_FRANCHISE_PO_IND IS
      select 'Y'
        from ordhead
       where order_no = TO_NUMBER(I_order_no)
         and wf_order_no IS NOT NULL;

BEGIN
   open C_FRANCHISE_PO_IND;
   fetch C_FRANCHISE_PO_IND INTO L_franchise_po_ind;
   close C_FRANCHISE_PO_IND;

   /* If a Franchise PO, associated Franchise Order will also be deleted */
   if L_franchise_po_ind <> 'Y' then
      BEGIN
         L_return_code := WF_PO_SQL.DELETE_F_ORDER(L_error_message,
                                                   I_order_no);
         if L_return_code = TRUE then
            L_failed  := 0;
         else
            L_failed  := 1;
            L_err_message := L_error_message;
         end if;
      EXCEPTION
         when OTHERS then
            L_exception := 1;
            L_failed := 0;
            L_err_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                 SQLERRM,
                                                 'WF_PO_SQL.DELETE_F_ORDER',
                                                 SQLCODE);
      END;

      if L_failed = 1 or L_exception = 1 then
         O_error_message := L_err_message;
         return FALSE;
      end if;
   end if;

   if ADD_LINE_ITEM_SQL.LP_LOCKED_IND <> 'Y' then
      BEGIN
         L_error_message := NULL;
         L_return_code := ORDER_SETUP_SQL.LOCK_DETAIL(L_error_message,
                                                      I_order_no,
                                                      NULL,
                                                      NULL,
                                                      'split_po');

         if L_return_code = TRUE then
            L_failed  := 0;
         else
            L_failed  := 1;
            L_err_message := L_error_message;
         end if;
      EXCEPTION
        when OTHERS then
          L_exception := 1;
          L_failed := 0;
          L_err_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                'ORDER_SETUP_SQL.LOCK_DETAIL',
                                                SQLCODE);
      END;

      if L_failed = 1 or L_exception = 1 then
        O_error_message := L_err_message;
        return FALSE;
      end if;
   end if;

   BEGIN
      L_error_message := NULL;
      L_return_code := ORDER_SETUP_SQL.DELETE_ORDER(L_error_message,
                                                    I_order_no,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    'split_po');
      if L_return_code = TRUE then
         l_failed  := 0;
      else
         l_failed  := 1;
         l_err_message := L_error_message;
      end if;
   EXCEPTION
      when OTHERS then
         L_exception := 1;
         L_failed := 0;
         L_err_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              'ORDER_SETUP_SQL.DELETE_ORDER',
                                              SQLCODE);
   END;

   if L_failed = 1 or L_exception = 1 then
      O_error_message := L_err_message;
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
   return FALSE;
END DELETE_ORDER; /* end delete_order */
---------------------------------------------------------------------------------------------------------------
FUNCTION SPLIT_ROUND_TO_PACKSIZE(I_qty_to_round     IN     NUMBER,
                                 I_packsize         IN     NUMBER,
                                 I_round_pct        IN     NUMBER,
                                 O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN NUMBER IS
   L_program      VARCHAR2(40) := 'SPLIT_ORDER_SQL.SPLIT_ROUND_TO_PACKSIZE';
   L_dividend     NUMBER;
BEGIN
   if I_packsize <= 0 then
      O_error_message := 'Invalid pack size '||I_packsize;
      return -1.0;
   end if;

   if I_qty_to_round < 0 then
      O_error_message := 'Invalid rounding quantity '|| I_qty_to_round;
      return -1.0;
   end if;

   if I_qty_to_round = 0 then
      return 0.0;
   end if;

   /* if the qty to round can be fully divided. not do rounding */
   if MOD(I_qty_to_round, I_packsize) = 0 then
      return (I_qty_to_round);
   end if;

   L_dividend := FLOOR(I_qty_to_round / I_packsize);

   if I_qty_to_round - (L_dividend * I_packsize) >=  (I_packsize * (I_round_pct / 100)) OR
      (L_dividend = 0) then
      /* round up */
      return (L_dividend + 1) * I_packsize;
   else
      /* round down */
      return L_dividend * I_packsize;
   end if;

   return 0;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return -1;
END SPLIT_ROUND_TO_PACKSIZE; /* end split_round_to_packsize */
---------------------------------------------------------------------------------------------------------------
END SPLIT_ORDER_SQL;
/