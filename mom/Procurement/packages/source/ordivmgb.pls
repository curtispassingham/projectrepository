
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ORD_INV_MGMT_SQL AS
---------------------------------------------------------------------------------------
FUNCTION POP_ORDIVMGT(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_qty_prescaled1      IN OUT NUMBER,
                      O_qty_ordered1        IN OUT NUMBER,
                      O_qty_prescaled2      IN OUT NUMBER,
                      O_qty_ordered2        IN OUT NUMBER,
                      O_qty_prescaled3      IN OUT NUMBER,
                      O_qty_ordered3        IN OUT NUMBER,
                      O_qty_prescaled4      IN OUT NUMBER,
                      O_qty_ordered4        IN OUT NUMBER,
                      O_qty_prescaled5      IN OUT NUMBER,
                      O_qty_ordered5        IN OUT NUMBER,
                      O_qty_prescaled6      IN OUT NUMBER,
                      O_qty_ordered6        IN OUT NUMBER,
                      O_supp_prescale_cost  IN OUT NUMBER,
                      O_supp_order_cost     IN OUT NUMBER,
                      O_prescaled_cost      IN OUT NUMBER,
                      O_cost                IN OUT NUMBER,
                      O_eso                 IN OUT NUMBER,
                      O_aso                 IN OUT NUMBER,
                      O_item_locs_total     IN OUT NUMBER,
                      O_item_locs_due       IN OUT NUMBER,
                      I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                      I_supplier            IN     ORDHEAD.SUPPLIER%TYPE,
                      I_item                IN     ITEM_MASTER.ITEM%TYPE,
                      I_location            IN     ORDLOC.LOCATION%TYPE,
                      I_due_ord_process_ind IN     ORD_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE,
                      I_cnstr_type1         IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                      I_cnstr_type2         IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                      I_cnstr_type3         IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                      I_cnstr_type4         IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                      I_cnstr_type5         IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                      I_cnstr_type6         IN     ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                      I_cnstr_uom1          IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                      I_cnstr_uom2          IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                      I_cnstr_uom3          IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                      I_cnstr_uom4          IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                      I_cnstr_uom5          IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                      I_cnstr_uom6          IN     ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE)
   return BOOLEAN IS

   L_outstand_cost_ord  ORDLOC.UNIT_COST%TYPE;
   L_cancel_cost_ord    ORDLOC.UNIT_COST%TYPE;
   ---
   L_supp_prescale_cost ORDLOC.UNIT_COST%TYPE;
   L_supp_order_cost    ORDLOC.UNIT_COST%TYPE;
   L_qty_prescaled      ORDLOC.QTY_ORDERED%TYPE;
   L_qty_ordered        ORDLOC.QTY_ORDERED%TYPE;
   L_supp_sku_cost      ORDLOC.UNIT_COST%TYPE;
   L_ord_sku_cost       ORDLOC.UNIT_COST%TYPE;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   -- Below, constraints three and four are compared with constraints one and two to see if they are
   -- of the same type.  If they are the same type, then we use the qty_prescaled and qty_ordered values
   -- from one or two to populate three or four.  For instance, if the constraint type three is the 
   -- same as constraint type one, then O_qty_prescaled3 and O_qty_ordered3 are set equal to O_qty_prescaled1
   -- and O_qty_ordered1.  However, since constraints one and two cannot be of the same type, there is no
   -- reason to check for their equality here.  

   if I_cnstr_type1 is NOT NULL then
      if CNSTR_ORD_QTYS(O_error_message,
                        O_qty_prescaled1,
                        O_qty_ordered1, 
                        I_order_no,
                        I_supplier,
                        I_item,
                        I_location,
                        I_cnstr_type1,
                        I_cnstr_uom1) = FALSE then
         return FALSE;
      end if;
   end if;
   
   if I_cnstr_type2 is NOT NULL then
      if CNSTR_ORD_QTYS(O_error_message,
                        O_qty_prescaled2,
                        O_qty_ordered2,
                        I_order_no,
                        I_supplier,
                        I_item,
                        I_location,
                        I_cnstr_type2,
                        I_cnstr_uom2) = FALSE then
         return FALSE;
      end if;
   end if;
   
   -- If I_cnstr_type3 is equal to either I_cnstr_type1 or 2, we can use the values of the O_qty_prescaled and 
   -- O_qty_ordered variables for this constraint.  If the constraint types are 'M' or 'V', then the UOM's also
   -- need to be equal.  

   if I_cnstr_type3 is NOT NULL then
      if I_cnstr_type3 = I_cnstr_type1
      and (I_cnstr_type3 in ('A','P','C','E','S') or (I_cnstr_type3 in ('M','V') and I_cnstr_uom3 = I_cnstr_uom1)) then
         O_qty_prescaled3 := O_qty_prescaled1;
         O_qty_ordered3   := O_qty_ordered1;
      elsif I_cnstr_type3 = I_cnstr_type2
      and (I_cnstr_type3 in ('A','P','C','E','S') or (I_cnstr_type3 in ('M','V') and I_cnstr_uom3 = I_cnstr_uom2)) then
         O_qty_prescaled3 := O_qty_prescaled2;
         O_qty_ordered3   := O_qty_ordered2;
      else
         if CNSTR_ORD_QTYS(O_error_message,
                           O_qty_prescaled3,
                           O_qty_ordered3,
                           I_order_no,
                           I_supplier,
                           I_item,
                           I_location,
                           I_cnstr_type3,
                           I_cnstr_uom3) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   -- If I_cnstr_type4 is equal to either I_cnstr_type1 or 2, we can use the values of the O_qty_prescaled and 
   -- O_qty_ordered variables for this constraint.  If the constraint types are 'M' or 'V', then the UOM's also
   -- need to be equal.  Since I_cnstr_type3 and I_cnstr_type4 can't be equal, there's no use in comparing
   -- the two.

   if I_cnstr_type4 is NOT NULL then
      if I_cnstr_type4 = I_cnstr_type1
      and (I_cnstr_type4 in ('A','P','C','E','S') or (I_cnstr_type4 in ('M','V') and I_cnstr_uom4 = I_cnstr_uom1)) then
         O_qty_prescaled4 := O_qty_prescaled1;
         O_qty_ordered4   := O_qty_ordered1;
      elsif I_cnstr_type4 = I_cnstr_type2
      and (I_cnstr_type4 in ('A','P','C','E','S') or (I_cnstr_type4 in ('M','V') and I_cnstr_uom4 = I_cnstr_uom2)) then
         O_qty_prescaled4 := O_qty_prescaled2;
         O_qty_ordered4   := O_qty_ordered2;
      else
         if CNSTR_ORD_QTYS(O_error_message,
                           O_qty_prescaled4,
                           O_qty_ordered4,
                           I_order_no,
                           I_supplier,
                           I_item,
                           I_location,
                           I_cnstr_type4,
                           I_cnstr_uom4) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   
   -- If I_cnstr_type5 is equal to I_cnstr_type1 or 2 or 3 or 4, we can use the values of the O_qty_prescaled and 
   -- O_qty_ordered variables for this constraint.  If the constraint types are 'M' or 'V', then the UOM's also
   -- need to be equal.  

   if I_cnstr_type5 is NOT NULL then
      if I_cnstr_type5 = I_cnstr_type1
      and (I_cnstr_type5 in ('A','P','C','E','S') or (I_cnstr_type5 in ('M','V') and I_cnstr_uom5 = I_cnstr_uom1)) then
         O_qty_prescaled5 := O_qty_prescaled1;
         O_qty_ordered5   := O_qty_ordered1;
      elsif I_cnstr_type5 = I_cnstr_type2
      and (I_cnstr_type5 in ('A','P','C','E','S') or (I_cnstr_type5 in ('M','V') and I_cnstr_uom5 = I_cnstr_uom2)) then
         O_qty_prescaled5 := O_qty_prescaled2;
         O_qty_ordered5   := O_qty_ordered2;
      elsif I_cnstr_type5 = I_cnstr_type3
      and (I_cnstr_type5 in ('A','P','C','E','S') or (I_cnstr_type5 in ('M','V') and I_cnstr_uom5 = I_cnstr_uom3)) then
         O_qty_prescaled5 := O_qty_prescaled3;
         O_qty_ordered5   := O_qty_ordered3;
      elsif I_cnstr_type5 = I_cnstr_type4
      and (I_cnstr_type5 in ('A','P','C','E','S') or (I_cnstr_type5 in ('M','V') and I_cnstr_uom5 = I_cnstr_uom4)) then
         O_qty_prescaled5 := O_qty_prescaled4;
         O_qty_ordered5   := O_qty_ordered4;
      else
         if CNSTR_ORD_QTYS(O_error_message,
                           O_qty_prescaled5,
                           O_qty_ordered5,
                           I_order_no,
                           I_supplier,
                           I_item,
                           I_location,
                           I_cnstr_type5,
                           I_cnstr_uom5) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   -- If I_cnstr_type6 is equal to I_cnstr_type1 or 2 or 3 or 4, we can use the values of the O_qty_prescaled and 
   -- O_qty_ordered variables for this constraint.  If the constraint types are 'M' or 'V', then the UOM's also
   -- need to be equal.  Since I_cnstr_type5 and I_cnstr_type6 can't be equal, there's no use in comparing
   -- the two.

   if I_cnstr_type6 is NOT NULL then
      if I_cnstr_type6 = I_cnstr_type1
      and (I_cnstr_type6 in ('A','P','C','E','S') or (I_cnstr_type6 in ('M','V') and I_cnstr_uom6 = I_cnstr_uom1)) then
         O_qty_prescaled6 := O_qty_prescaled1;
         O_qty_ordered6   := O_qty_ordered1;
      elsif I_cnstr_type6 = I_cnstr_type2
      and (I_cnstr_type6 in ('A','P','C','E','S') or (I_cnstr_type6 in ('M','V') and I_cnstr_uom6 = I_cnstr_uom2)) then
         O_qty_prescaled6 := O_qty_prescaled2;
         O_qty_ordered6   := O_qty_ordered2;
      elsif I_cnstr_type6 = I_cnstr_type3
      and (I_cnstr_type6 in ('A','P','C','E','S') or (I_cnstr_type6 in ('M','V') and I_cnstr_uom6 = I_cnstr_uom3)) then
         O_qty_prescaled6 := O_qty_prescaled3;
         O_qty_ordered6   := O_qty_ordered3;
      elsif I_cnstr_type6 = I_cnstr_type4
      and (I_cnstr_type6 in ('A','P','C','E','S') or (I_cnstr_type6 in ('M','V') and I_cnstr_uom6 = I_cnstr_uom4)) then
         O_qty_prescaled6 := O_qty_prescaled4;
         O_qty_ordered6   := O_qty_ordered4;
      else
         if CNSTR_ORD_QTYS(O_error_message,
                           O_qty_prescaled6,
                           O_qty_ordered6,
                           I_order_no,
                           I_supplier,
                           I_item,
                           I_location,
                           I_cnstr_type6,
                           I_cnstr_uom6) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   if ORDER_CALC_SQL.SUPPLIER_COST_UNITS(O_error_message,
                                         O_supp_prescale_cost,
                                         O_supp_order_cost,
                                         O_prescaled_cost,
                                         O_cost,
                                         L_qty_prescaled,
                                         L_qty_ordered,
                                         L_supp_sku_cost,
                                         L_ord_sku_cost,
                                         I_order_no,
                                         I_item,
                                         I_location,
                                         NULL, --I_alloc_no,
                                         I_supplier,
                                         NULL, --I_order_status
                                         NULL, --I_currency_code,
                                         NULL /* I_exchange_rate */ ) = FALSE then
      return FALSE;
   end if;

   if I_due_ord_process_ind = 'Y' then
      if ORDER_DUE_SQL.TOTAL_ESO_ASO(O_error_message,
                                     O_eso,
                                     O_aso,
                                     I_order_no,
                                     I_item,
                                     I_location) = FALSE then
         return FALSE;
      end if;

      if ORDER_DUE_SQL.COUNT_ITEM_LOCS(O_error_message,
                                       O_item_locs_total,
                                       O_item_locs_due,
                                       I_order_no,
                                       I_item,
                                       I_location) = FALSE then
         return FALSE;
      end if;
   else
      O_eso := NULL;
      O_aso := NULL;
      O_item_locs_total := NULL;
      O_item_locs_due := NULL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM, 
                                            'ORD_INV_MGMT_SQL.POP_ORDIVMGT', 
                                             to_char(SQLCODE));
      return FALSE;
END POP_ORDIVMGT;
---------------------------------------------------------------------------------------
FUNCTION CNSTR_ORD_QTYS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_qty_prescaled  IN OUT  NUMBER,
                        O_qty_ordered    IN OUT  NUMBER,
                        I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                        I_supplier       IN      ORDHEAD.SUPPLIER%TYPE,
                        I_item           IN      ITEM_MASTER.ITEM%TYPE,
                        I_location       IN      ORDLOC.LOCATION%TYPE,
                        I_cnstr_type     IN      ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                        I_cnstr_uom      IN      ORD_INV_MGMT.SCALE_CNSTR_UOM1%TYPE)
   return BOOLEAN IS

   L_outstand_cost_ord  ORDLOC.UNIT_COST%TYPE;
   L_cancel_cost_ord    ORDLOC.UNIT_COST%TYPE;
   L_cnstr_type         UOM_CLASS.UOM_CLASS%TYPE;
   L_supplier           ORDHEAD.SUPPLIER%TYPE;
   ---
   L_prescale_cost      ORDLOC.UNIT_COST%TYPE;    
   L_order_cost         ORDLOC.UNIT_COST%TYPE; 
   L_supp_prescale_cost ORDLOC.UNIT_COST%TYPE;
   L_supp_order_cost    ORDLOC.UNIT_COST%TYPE;
   L_supp_sku_cost      ORDLOC.UNIT_COST%TYPE;
   L_ord_sku_cost       ORDLOC.UNIT_COST%TYPE;
   L_exists             BOOLEAN;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   if I_supplier is NULL then
      if ORDER_ATTRIB_SQL.GET_SUPPLIER(O_error_message,
                                       L_exists,
                                       L_supplier,
                                       I_order_no) = FALSE then
         return FALSE;
      end if;
   else
      L_supplier := I_supplier;
   end if;

   if I_cnstr_type = 'A' then
      if ORDER_CALC_SQL.SUPPLIER_COST_UNITS(O_error_message,
                                            L_supp_prescale_cost,
                                            L_supp_order_cost,
                                            L_prescale_cost,
                                            L_order_cost,
                                            O_qty_prescaled,
                                            O_qty_ordered,
                                            L_supp_sku_cost,
                                            L_ord_sku_cost,
                                            I_order_no,
                                            I_item,
                                            I_location,
                                            NULL, --I_alloc_no,
                                            I_supplier,
                                            NULL, --I_order_status
                                            NULL, --I_ord_currency_rate
                                            NULL) = FALSE then  -- I_order_exchange_rate
         return FALSE;
      end if;
   end if;

   if I_cnstr_type in ('V','M') then
      if ORDER_ATTRIB_SQL.GET_TOTAL_UOM(I_order_no,
                                        I_item,
                                        I_location,
                                        I_cnstr_uom,
                                        L_supplier,
                                        'B', -- order_qty_type
                                        O_qty_ordered,
                                        O_qty_prescaled,
                                        O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_cnstr_type = 'C' then
      if ORDER_ATTRIB_SQL.GET_TOTAL_CASES(O_error_message,
                                          O_qty_ordered,
                                          O_qty_prescaled,
                                          I_order_no,
                                          I_item,
                                          I_location) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_cnstr_type = 'P' then
      if ORDER_ATTRIB_SQL.GET_TOTAL_PALLETS(O_error_message,
                                            O_qty_ordered,
                                            O_qty_prescaled,
                                            I_order_no,
                                            L_supplier,
                                            I_item,
                                            I_location) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_cnstr_type = 'E' then
      if ORDER_ATTRIB_SQL.GET_TOTAL_UOM(I_order_no,
                                        I_item,
                                        I_location,
                                        'EA',
                                        L_supplier,
                                        'B', -- order_qty_type
                                        O_qty_ordered,
                                        O_qty_prescaled,
                                        O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   
   if I_cnstr_type = 'S' then
      if ORDER_ATTRIB_SQL.GET_TOTAL_STATCASE(O_error_message,
                                             O_qty_ordered,
                                             O_qty_prescaled,
                                             I_order_no,
                                             L_supplier,
                                             I_item,
                                             I_location) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM, 
                                            'ORD_INV_MGMT_SQL.CNSTR_ORD_QTYS', 
                                             to_char(SQLCODE));
      return FALSE;
END CNSTR_ORD_QTYS;
-----------------------------------------------------------------------------------------------
FUNCTION GET_SINGLE_LOC_IND(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_single_loc_ind   IN OUT   ORD_INV_MGMT.SINGLE_LOC_IND%TYPE,              
                            I_order_no         IN       ORD_INV_MGMT.ORDER_NO%TYPE)
   return BOOLEAN is

   cursor C_GET_IND is
      select single_loc_ind
        from ord_inv_mgmt
       where order_no = I_order_no;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_IND','ORD_INV_MGMT','Order No: '||to_char(I_order_no));
   open C_GET_IND;
   SQL_LIB.SET_MARK('FETCH','C_GET_IND','ORD_INV_MGMT','Order No: '||to_char(I_order_no));
   fetch C_GET_IND into O_single_loc_ind;
   SQL_LIB.SET_MARK('CLOSE','C_GET_IND','ORD_INV_MGMT','Order No: '||to_char(I_order_no));
   close C_GET_IND;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                            'ORD_INV_MGMT_SQL.GET_SINGLE_LOC_IND', 
                                             to_char(SQLCODE));
      return FALSE;
END GET_SINGLE_LOC_IND;
---------------------------------------------------------------------------------------
FUNCTION FILE_ID_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists           IN OUT   BOOLEAN,
                        I_file_id          IN       ORD_INV_MGMT.FILE_ID%TYPE)
   return BOOLEAN is

   L_dummy VARCHAR2(1);

   cursor C_EXISTS is
      select 'x'
        from ord_inv_mgmt
       where file_id = I_file_id;

BEGIN
   if I_file_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_file_id',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   O_exists := TRUE;

   SQL_LIB.SET_MARK('OPEN','C_EXISTS','ORD_INV_MGMT','FILE ID: '||I_file_id);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_EXISTS','ORD_INV_MGMT','FILE ID: '||I_file_id);
   fetch C_EXISTS into L_dummy;
   ---
   if C_EXISTS%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','ORD_INV_MGMT','FILE ID: '||I_file_id);
   close C_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                            'ORD_INV_MGMT_SQL.FILE_ID_EXISTS', 
                                             to_char(SQLCODE));
      return FALSE;
END FILE_ID_EXISTS;
---------------------------------------------------------------------------------------
FUNCTION GET_POOL_SUPP_FILE_ID(O_error_message   IN OUT   VARCHAR2,
                               O_pool_supplier   IN OUT   ORD_INV_MGMT.POOL_SUPPLIER%TYPE,
                               O_file_id         IN OUT   ORD_INV_MGMT.FILE_ID%TYPE,
                               I_order_no        IN       ORD_INV_MGMT.ORDER_NO%TYPE)
   return BOOLEAN is

   cursor C_GET_INFO is
      select pool_supplier,
             file_id
        from ord_inv_mgmt
       where order_no = I_order_no;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_INFO','ORD_INV_MGMT','ORDER NO: '||to_char(I_order_no));
   open C_GET_INFO;

   SQL_LIB.SET_MARK('FETCH','C_GET_INFO','ORD_INV_MGMT','ORDER NO: '||to_char(I_order_no));
   fetch C_GET_INFO into O_pool_supplier,
                         O_file_id;

   SQL_LIB.SET_MARK('CLOSE','C_GET_INFO','ORD_INV_MGMT','ORDER NO: '||to_char(I_order_no));
   close C_GET_INFO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                            'ORD_INV_MGMT_SQL.GET_POOL_SUPP_FILE_ID', 
                                             to_char(SQLCODE));
      return FALSE;
END GET_POOL_SUPP_FILE_ID;
---------------------------------------------------------------------------------------
FUNCTION GET_TRUCK_SPLIT_IND(O_error_message     IN OUT   VARCHAR2,
                             O_truck_split_ind   IN OUT   ORD_INV_MGMT.TRUCK_SPLIT_IND%TYPE,
                             I_order_no          IN       ORD_INV_MGMT.ORDER_NO%TYPE)
   return BOOLEAN is
  
   cursor C_GET_IND is
      select truck_split_ind
        from ord_inv_mgmt
       where order_no = I_order_no;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_IND','ORD_INV_MGMT','ORDER NO: '||to_char(I_order_no));
   open C_GET_IND;

   SQL_LIB.SET_MARK('FETCH','C_GET_IND','ORD_INV_MGMT','ORDER NO: '||to_char(I_order_no));
   fetch C_GET_IND into O_truck_split_ind;

   SQL_LIB.SET_MARK('CLOSE','C_GET_IND','ORD_INV_MGMT','ORDER NO: '||to_char(I_order_no));
   close C_GET_IND;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                            'ORD_INV_MGMT_SQL.GET_TRUCK_SPLIT_IND', 
                                             to_char(SQLCODE));
      return FALSE;
END GET_TRUCK_SPLIT_IND;
---------------------------------------------------------------------------------------
FUNCTION AMOUNT_CONSTRAINT_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists         IN OUT VARCHAR2,
                                  I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   CURSOR C_INV_MGMT_REC IS
      SELECT 'Y'
        FROM ord_inv_mgmt
       WHERE order_no = I_order_no
         AND ( (scale_cnstr_ind = 'Y'  
		        AND ( scale_cnstr_type1= 'A'
                      OR scale_cnstr_type2 = 'A'))
               OR min_cnstr_type1 = 'A'
               OR min_cnstr_type2 = 'A') 
		AND rownum = 1;
               
   L_program_name  VARCHAR2(255) := 'ORD_INV_MGMT_SQL.AMOUNT_CONSTRAINT_EXISTS';               
BEGIN
    O_exists := 'N';
	if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_INV_MGMT_REC','ord_inv_mgmt',NULL);
   open C_INV_MGMT_REC;
   SQL_LIB.SET_MARK('FETCH','C_INV_MGMT_REC','ord_inv_mgmt',NULL);
   fetch C_INV_MGMT_REC into O_exists;
   SQL_LIB.SET_MARK('CLOSE','C_INV_MGMT_REC','ord_inv_mgmt',NULL);
   close C_INV_MGMT_REC;	  
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                            L_program_name, 
                                             to_char(SQLCODE));
      return FALSE;
   
END AMOUNT_CONSTRAINT_EXISTS ; 
FUNCTION DELETE_ORD_INV_MGMT(O_error_message    IN OUT      VARCHAR2,
                             I_order_no         IN          ORD_INV_MGMT.order_no%TYPE)

RETURN BOOLEAN is

   L_table            VARCHAR2(30);
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);



   cursor C_ORD_INV_MGMT is
      select 'x'
        from ord_inv_mgmt
       where order_no = I_order_no
         for update nowait;

BEGIN
 
   --lock table ord_inv_mgmt and delete the entry for a given order no.
   L_table := 'ord_inv_mgmt';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ORD_INV_MGMT',
                    'ORD_INV_MGMT',
                    'ORDER NO : '||to_char(I_order_no));

   open C_ORD_INV_MGMT;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORD_INV_MGMT',
                    'ORD_INV_MGMT',
                    'ORDER NO: '||to_char(I_order_no));

   close C_ORD_INV_MGMT;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL,'ORD_INV_MGMT', 'ORDER NO '||TO_CHAR(I_order_no));

   delete from ord_inv_mgmt
    where order_no = I_order_no;
   ---
   return TRUE;


EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
         return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DEAL_SQL.DELETE_DEALS',
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_ORD_INV_MGMT;
---------------------------------------------------------------------------------------------------------------------------  
END ORD_INV_MGMT_SQL;
/
