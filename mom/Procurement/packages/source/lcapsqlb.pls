CREATE OR REPLACE PACKAGE BODY LC_APPLY_SQL AS
--------------------------------------------------------------------------------------
FUNCTION VALID_LCORDAPP_ID(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%type,
                           O_exists          IN OUT BOOLEAN,
                           I_lc_ref_id       IN     LC_HEAD.LC_REF_ID%TYPE,
                           I_applicant       IN     LC_HEAD.APPLICANT%TYPE,
                           I_beneficiary     IN     LC_HEAD.BENEFICIARY%TYPE,
                           I_issuing_bank    IN     LC_HEAD.ISSUING_BANK%TYPE,
                           I_purchase_type   IN     LC_HEAD.PURCHASE_TYPE%TYPE,
                           I_fob_title_pass  IN     LC_HEAD.FOB_TITLE_PASS%TYPE,
                           I_fob_title_desc  IN     LC_HEAD.FOB_TITLE_PASS_DESC%TYPE)
RETURN BOOLEAN IS
   L_program    VARCHAR2(50) := 'LC_APPLY_SQL.VALID_LCORDAPP_ID';
   L_exists     VARCHAR2(1);
   L_found      VARCHAR2(1);

   cursor C_CHECK_LC_HEAD is
      select 'x'
        from lc_head
       where lc_ref_id                  = I_lc_ref_id
         and lc_type                   != 'O'
         and status                    != 'L'
         and applicant                  = I_applicant
         and beneficiary                = I_beneficiary
         and issuing_bank               = I_issuing_bank
         and purchase_type              = I_purchase_type
         and (fob_title_pass             = I_fob_title_pass
             or (fob_title_pass is NULL and I_fob_title_pass is NULL))
         and (upper(fob_title_pass_desc) = upper(I_fob_title_desc)
             or (fob_title_pass_desc is NULL and I_fob_title_desc is NULL));

   cursor C_CHECK_V_LC_ORDAPPLY is
      select 'x'
        from v_lc_ordapply
       where lc_ref_id                  = I_lc_ref_id
         and lc_type               not in ('O','N')
         and applicant                  = I_applicant
         and beneficiary                = I_beneficiary
         and purchase_type              = I_purchase_type
         and (fob_title_pass             = I_fob_title_pass
             or (fob_title_pass is NULL and I_fob_title_pass is NULL))
         and (upper(fob_title_pass_desc) = upper(I_fob_title_desc)
             or (fob_title_pass_desc is NULL and I_fob_title_desc is NULL));


   cursor C_LC_PO_MATCH is
      select 'x'
        from lc_head 
       where lc_ref_id = I_lc_ref_id
     union
      select 'x'
        from v_lc_ordapply
       where lc_ref_id = I_lc_ref_id;

BEGIN
   --- check the required information has been passed in
   if I_lc_ref_id is NULL     or
      I_applicant is NULL     or
      I_beneficiary is NULL   or
      I_purchase_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;

   O_exists := TRUE;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_LC_HEAD','LC_HEAD',NULL);
   open C_CHECK_LC_HEAD;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_LC_HEAD','LC_HEAD',NULL);
   fetch C_CHECK_LC_HEAD into L_exists;

   if C_CHECK_LC_HEAD%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_LC_HEAD','LC_HEAD',NULL);
      close C_CHECK_LC_HEAD;
      
      SQL_LIB.SET_MARK('OPEN','C_CHECK_V_LC_ORDAPPLY','V_LC_ORDAPPLY',NULL);
      open C_CHECK_V_LC_ORDAPPLY;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_V_LC_ORDAPPLY','V_LC_ORDAPPLY',NULL);
      fetch C_CHECK_V_LC_ORDAPPLY into L_exists;

      if C_CHECK_V_LC_ORDAPPLY%NOTFOUND then
         ---
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_V_LC_ORDAPPLY','V_LC_ORDAPPLY',NULL);
         close C_CHECK_V_LC_ORDAPPLY;
         ---         
         SQL_LIB.SET_MARK('OPEN','C_LC_PO_MATCH','LC_HEAD/V_LC_ORDAPPLY',to_char(I_lc_ref_id));
         open  C_LC_PO_MATCH;
         SQL_LIB.SET_MARK('FETCH','C_LC_PO_MATCH','LC_HEAD/V_LC_ORDAPPLY',to_char(I_lc_ref_id));
         fetch C_LC_PO_MATCH into L_found;
         if C_LC_PO_MATCH%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_LC_REF_ID',NULL,NULL,NULL);
            O_exists := FALSE;
         else
            O_error_message := SQL_LIB.CREATE_MSG('NOT_PO_LC_MATCH',NULL,NULL,NULL);
            O_exists := FALSE;
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_LC_PO_MATCH','LC_HEAD/V_LC_ORDAPPLY',to_char(I_lc_ref_id));
         close C_LC_PO_MATCH;
         ---
      else
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_V_LC_ORDAPPLY','V_LC_ORDAPPLY',NULL);
         close C_CHECK_V_LC_ORDAPPLY;
      end if;   
    else
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_LC_HEAD','LC_HEAD',NULL);
      close C_CHECK_LC_HEAD;
    end if;
 
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALID_LCORDAPP_ID;

--------------------------------------------------------------------------------------
FUNCTION WRITE_LCHEAD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%type,
                      I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                      I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE,
                      I_form_type      IN     LC_HEAD.FORM_TYPE%TYPE,
                      I_lc_type        IN     LC_HEAD.LC_TYPE%TYPE,
                      I_issuing_bank   IN     LC_HEAD.ISSUING_BANK%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(50)      := 'LC_APPLY_SQL.WRITE_LCHEAD';
   L_vdate              PERIOD.VDATE%TYPE := GET_VDATE;
   L_lc_exp_days        SYSTEM_OPTIONS.LC_EXP_DAYS%TYPE;
   L_origin_country_id  COUNTRY.COUNTRY_ID%TYPE;
   L_exchange_rate      CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_currency_code      LC_HEAD.CURRENCY_CODE%TYPE;
   L_dummy_exch_rate    CURRENCY_RATES.EXCHANGE_RATE%TYPE;

BEGIN
   --- check the required information has been passed in
   if I_order_no is NULL      or
      I_lc_ref_id is NULL     or
      I_form_type is NULL     or
      I_lc_type is NULL       or
      I_issuing_bank is NULL  then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_LC_EXP_DAYS(O_error_message,
                                         L_lc_exp_days) = FALSE then
      return FALSE;
   end if;
      
   if ORDER_ATTRIB_SQL.GET_COUNTRY_OF_ORIGIN(O_error_message,
                                             L_origin_country_id,
                                             I_order_no) = FALSE then
      return FALSE;
   end if;

   if ORDER_ATTRIB_SQL.GET_CURRENCY_RATE (O_error_message,
                                          L_currency_code,
                                          L_dummy_exch_rate,
                                          I_order_no) = FALSE then
      return FALSE;
   end if;

   if CURRENCY_SQL.GET_RATE(O_error_message,
                            L_exchange_rate, 
                            L_currency_code,
                            'L',
                            NULL) = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('INSERT',NULL,'LC_ORDAPPLY',NULL);
   insert into lc_head(lc_ref_id,
                       bank_lc_id,
                       status,
                       form_type,
                       lc_type,
                       currency_code,
                       exchange_rate,
                       origin_country_id,
                       applicant,
                       beneficiary,
                       application_date,
                       confirmed_date,
                       expiration_date,
                       earliest_ship_date,
                       latest_ship_date,
                       credit_avail_with,
                       issuing_bank,
                       advising_bank,
                       confirming_bank,
                       transferring_bank,
                       negotiating_bank,
                       paying_bank,
                       variance_pct,
                       specification,
                       amount_type,
                       amount,
                       presentation_terms,
                       purchase_type,
                       place_of_expiry,
                       advice_method,
                       issuance,
                       drafts_at,
                       fob_title_pass,
                       fob_title_pass_desc,
                       with_recourse_ind,
                       transferable_ind,
                       transshipment_ind,
                       partial_shipment_ind,
                       lc_neg_days,
                       transport_to,
                       comments,
                       lading_port,
                       discharge_port)
               (select I_lc_ref_id,
                       NULL,
                       'W',
                       I_form_type,
                       I_lc_type,
                       L_currency_code,
                       L_exchange_rate,
                       L_origin_country_id,
                       ol.applicant,
                       ol.beneficiary,
                       L_vdate,
                       NULL,
                       oh.latest_ship_date + L_lc_exp_days,
                       oh.earliest_ship_date,
                       oh.latest_ship_date,
                       NULL,
                       I_issuing_bank,
                       s.advising_bank,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       s.variance_pct,
                       (DECODE(s.variance_pct,0,'M',NULL)),
                       (DECODE(s.variance_pct,0,'E','A')),
                       0,
                       s.presentation_terms,
                       oh.purchase_type,
                       s.place_of_expiry,
                       NULL,
                       NULL,
                       s.drafts_at,
                       oh.fob_title_pass,
                       oh.fob_title_pass_desc,
                       s.with_recourse_ind,
                       'N',
                       ol.transshipment_ind,
                       ol.partial_shipment_ind,
                       s.lc_neg_days,                     
                       NULL,
                       NULL,
                       (DECODE(I_lc_type,'M',oh.lading_port,'N',oh.lading_port,'O',NULL,'R',NULL)),
                       (DECODE(I_lc_type,'M',oh.discharge_port,'N',oh.discharge_port,'O',NULL,'R',NULL))
                  from ordlc   ol,
                       ordhead oh,
                       sup_import_attr s
                 where ol.order_no    = I_order_no
                   and oh.order_no    = ol.order_no
                   and ol.beneficiary = s.supplier);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END WRITE_LCHEAD;

--------------------------------------------------------------------------------------
FUNCTION PROCESS_LCORDAPP(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%type,
                          O_error_flag    IN OUT  BOOLEAN,
                          I_issuing_bank  IN      LC_HEAD.ISSUING_BANK%TYPE,
                          I_currency_code IN      LC_HEAD.CURRENCY_CODE%TYPE) 
   RETURN BOOLEAN IS   

   L_program             VARCHAR2(50)   :=  'LC_APPLY_SQL.PROCESS_LCORDAPP';
   L_failed_item_level   BOOLEAN;
   L_error_flag          BOOLEAN;
   L_exists              BOOLEAN;
   L_bank_lc_id          LC_HEAD.BANK_LC_ID%TYPE;
   L_status		 LC_HEAD.STATUS%TYPE;
   L_lc_type	         LC_HEAD.LC_TYPE%TYPE;
   L_form_type	         LC_HEAD.FORM_TYPE%TYPE;
   L_country             LC_HEAD.ORIGIN_COUNTRY_ID%TYPE;
   L_currency	         LC_HEAD.CURRENCY_CODE%TYPE;
   L_applicant           LC_HEAD.APPLICANT%TYPE;
   L_beneficiary	 LC_HEAD.BENEFICIARY%TYPE;
   L_issue_bank          LC_HEAD.ISSUING_BANK%TYPE;
   L_advise_bank         LC_HEAD.ADVISING_BANK%TYPE;
   L_applicant_date      LC_HEAD.APPLICATION_DATE%TYPE;
   L_confirmed_date      LC_HEAD.CONFIRMED_DATE%TYPE;
   L_earliest_ship_date  LC_HEAD.EARLIEST_SHIP_DATE%TYPE;
   L_latest_ship_date    LC_HEAD.LATEST_SHIP_DATE%TYPE;
   L_expiration_date	 LC_HEAD.EXPIRATION_DATE%TYPE;
   L_num_recs            NUMBER;
   ---
   cursor C_LC_ORDAPPLY is
      select order_no,
             lc_ref_id,
             form_type,
             lc_type
        from lc_ordapply
       where lc_ref_id is not NULL
             order by form_type;

BEGIN

   O_error_flag := FALSE;
   FOR L_rec in C_LC_ORDAPPLY LOOP
      ---
      if LC_SQL.VALID_LC_GET_INFO(O_error_message,
                                  L_exists,
                                  L_status,
                                  L_lc_type,
                                  L_form_type,
                                  L_country,
                                  L_currency,
                                  L_applicant,
                                  L_beneficiary,
                                  L_issue_bank,
                                  L_advise_bank,
                                  L_bank_lc_id,
                                  L_rec.lc_ref_id) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists = FALSE then
         ---
         if L_rec.form_type is NULL then
            --- 
            if SYSTEM_OPTIONS_SQL.GET_LC_DEFAULTS(O_error_message,
                                                  L_form_type,
                                                  L_lc_type) = FALSE then
               return FALSE;
            end if;
            ---
         else
            L_form_type  := L_rec.form_type;
            L_lc_type    := L_rec.lc_type;
         end if;
         ---
         --Create a new Letter of Credit.
         ---
         if LC_APPLY_SQL.WRITE_LCHEAD(O_error_message,
                                      L_rec.order_no,
                                      L_rec.lc_ref_id,
                                      L_form_type,
                                      L_lc_type,
                                      I_issuing_bank) = FALSE then
            return FALSE;
         end if;
         --Attach the Purchase Order to the new Letter of Credit.
         ---
         if LC_SQL.ADD_PO(O_error_message,
                          L_rec.lc_ref_id,
                          L_rec.order_no,
                          I_issuing_bank) = FALSE then
            return FALSE;
         end if;
         ---
      else  -- L_exists is TRUE --
         ---
         --Validate that Purchase Order can be attached to the specified
         --Letter of Credit.
         ---
         if L_lc_type = 'N' then
            ---
            if LC_SQL.DETAILS_EXIST(O_error_message,
                                    L_exists,
                                    L_num_recs,
                                    L_rec.order_no,
                                    'D',
                                    L_rec.lc_ref_id) = FALSE then
               return FALSE;
            end if;
            ---
            if L_exists = TRUE then
               O_error_flag := TRUE;
               ---
               update lc_ordapply
                  set error_code = 'ORD_ADD_LC_NORM'
                where order_no   = L_rec.order_no;
            else
               O_error_flag := FALSE;
               ---
               --Attach the Purchase Order to the new Letter of Credit.
               ---  
               if LC_SQL.ADD_PO(O_error_message,
                                L_rec.lc_ref_id,
                                L_rec.order_no,
                                I_issuing_bank) = FALSE then
                  return FALSE;
               end if; 
            end if;
            ---
         else
            ---
            --Attach the Purchase Order to the new Letter of Credit.
            ---  
            if LC_SQL.ADD_PO(O_error_message,
                             L_rec.lc_ref_id,
                             L_rec.order_no,
                             I_issuing_bank) = FALSE then
               return FALSE;
            end if; 
         end if;
         ---
      end if;
      ---    
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM, 
                                            L_program, to_char(SQLCODE));      
      return FALSE;

END PROCESS_LCORDAPP;

--------------------------------------------------------------------------------------
FUNCTION RECALC_LCORDAPP_PROJ(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%type,
                              O_line_of_credit        IN OUT  PARTNER.LINE_OF_CREDIT%TYPE,
                              O_outstand_credit       IN OUT  PARTNER.OUTSTAND_CREDIT%TYPE,
                              O_open_credit           IN OUT  PARTNER.OPEN_CREDIT%TYPE,
                              O_line_of_credit_prim   IN OUT  PARTNER.LINE_OF_CREDIT%TYPE,
                              O_outstand_credit_prim  IN OUT  PARTNER.OUTSTAND_CREDIT%TYPE,
                              O_open_credit_prim      IN OUT  PARTNER.OPEN_CREDIT%TYPE,
			      I_issuing_bank          IN      PARTNER.PARTNER_ID%TYPE)
   RETURN BOOLEAN IS   

   L_program               VARCHAR2(50) := 'LC_APPLY_SQL.RECALC_LCORDAPP_PROJ';
   L_line_of_credit        PARTNER.LINE_OF_CREDIT%TYPE;
   L_outstand_credit       PARTNER.OUTSTAND_CREDIT%TYPE;
   L_open_credit           PARTNER.OPEN_CREDIT%TYPE;
   L_line_of_credit_prim   PARTNER.LINE_OF_CREDIT%TYPE;
   L_outstand_credit_prim  PARTNER.OUTSTAND_CREDIT%TYPE;
   L_open_credit_prim      PARTNER.OPEN_CREDIT%TYPE;
   L_total_cost_bank       PARTNER.LINE_OF_CREDIT%TYPE;
   L_total_cost_ord        PARTNER.LINE_OF_CREDIT%TYPE;
   L_prescale_cost_ord	   ORDLOC.UNIT_COST%TYPE;
   L_outstand_cost_ord     PARTNER.LINE_OF_CREDIT%TYPE;     
   L_cancel_cost_ord       PARTNER.LINE_OF_CREDIT%TYPE;
   L_grand_total_cost_bank PARTNER.LINE_OF_CREDIT%TYPE   := 0;
   
   cursor C_LC_ORDAPPLY is
      select order_no
        from lc_ordapply
       where lc_ref_id is not NULL;
   
BEGIN
   if PARTNER_SQL.GET_BANK_CREDIT(O_error_message,
                                  L_line_of_credit,
                                  L_outstand_credit,
                                  L_open_credit,
                                  L_line_of_credit_prim,
                                  L_outstand_credit_prim,
                                  L_open_credit_prim,
                                  I_issuing_bank,
                                  TRUE) = FALSE then
      return FALSE;
   end if;
   ---
   FOR L_rec in C_LC_ORDAPPLY LOOP
      ---
      --Determine the total cost of the order.
      ---
      if ORDER_CALC_SQL.TOTAL_COSTS(O_error_message,
                                    L_total_cost_ord,
				    L_prescale_cost_ord,				
                                    L_outstand_cost_ord,
                                    L_cancel_cost_ord,
                                    L_rec.order_no,
				    NULL,
				    NULL) = FALSE then
         return FALSE;
      end if;
      ---
      --Convert total cost into the issuing bank's currency.
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          to_char(L_rec.order_no),
                                          'O',
                                          NULL,
                                          I_issuing_bank,
                                          'BK',
                                          NULL,
                                          L_total_cost_ord,
                                          L_total_cost_bank,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if; 
      ---
      L_grand_total_cost_bank := L_grand_total_cost_bank + L_total_cost_bank;
      ---
   END LOOP;
   ---
   --Adjust line_of_credit, outstanding_credit, and open_credit.
   ---
   O_line_of_credit   :=  L_line_of_credit;
   O_outstand_credit  :=  L_outstand_credit + L_grand_total_cost_bank;
   O_open_credit      :=  L_open_credit     - L_grand_total_cost_bank;
   ---
   --Convert line_of_credit, outstand_credit, and open_credit to system's primary currency. 
   ---
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       I_issuing_bank,
                                       'BK',
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       O_line_of_credit,
                                       O_line_of_credit_prim,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
      return FALSE;
   end if; 
   ---
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       I_issuing_bank,
                                       'BK',
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       O_outstand_credit,
                                       O_outstand_credit_prim,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
      return FALSE;
   end if; 
   ---
   if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                       I_issuing_bank,
                                       'BK',
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       O_open_credit,
                                       O_open_credit_prim,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
      return FALSE;
   end if; 
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM, 
                                            L_program, to_char(SQLCODE));      
      return FALSE;

END RECALC_LCORDAPP_PROJ;
--------------------------------------------------------------------------------------
FUNCTION CHECK_LADING_PORT(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%type,
                           O_warning_flg           IN OUT  VARCHAR2,
                           I_lc_ref_id             IN      LC_DETAIL.LC_REF_ID%TYPE,
                           I_lading_port           IN      ORDHEAD.LADING_PORT%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(50) := 'LC_APPLY_SQL.CHECK_LADING_PORT';
   L_order_no              ORDHEAD.ORDER_NO%TYPE;
   L_lading_port           ORDHEAD.LADING_PORT%TYPE;

   CURSOR C_GET_LCDETAIL IS
   SELECT ORDER_NO
     FROM LC_DETAIL
    WHERE LC_REF_ID = I_lc_ref_id;

   CURSOR C_ORDHEAD IS
   SELECT LADING_PORT
     FROM ORDHEAD
    WHERE ORDER_NO = L_order_no;

BEGIN
   O_warning_flg := 'N';

   if I_lc_ref_id IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('VALIDATION_ERROR',
                                            'LC REF ID IS NULL',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_lading_port IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('VALIDATION_ERROR',
                                            'Lading Port IS NULL',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   OPEN C_GET_LCDETAIL;
   FETCH C_GET_LCDETAIL INTO L_order_no;
   if C_GET_LCDETAIL%ROWCOUNT = 0 then
      CLOSE C_GET_LCDETAIL;
      return TRUE;
   else
      OPEN C_ORDHEAD;
      FETCH C_ORDHEAD INTO L_lading_port;
      if C_ORDHEAD%ROWCOUNT = 0 THEN
         RAISE NO_DATA_FOUND;
      end if;
      CLOSE C_GET_LCDETAIL;
      CLOSE C_ORDHEAD;
   end if;

   if L_lading_port <> I_lading_port then
      O_warning_flg := 'Y';
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;
END CHECK_LADING_PORT;
--------------------------------------------------------------------------------------
FUNCTION CHECK_DISCHARGE_PORT(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%type,
                              O_warning_flg           IN OUT  VARCHAR2,
                              I_lc_ref_id             IN      LC_DETAIL.LC_REF_ID%TYPE,
                              I_discharge_port        IN      ORDHEAD.DISCHARGE_PORT%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(50) := 'LC_APPLY_SQL.CHECK_DISCHARGE_PORT';
   L_order_no              ORDHEAD.ORDER_NO%TYPE;
   L_discharge_port        ORDHEAD.DISCHARGE_PORT%TYPE;

   CURSOR C_GET_LCDETAIL IS
   SELECT ORDER_NO
     FROM LC_DETAIL
    WHERE LC_REF_ID = I_lc_ref_id;

   CURSOR C_ORDHEAD IS
   SELECT DISCHARGE_PORT
     FROM ORDHEAD
    WHERE ORDER_NO = L_order_no;

BEGIN
   O_warning_flg := 'N';

   if I_lc_ref_id IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('VALIDATION_ERROR',
                                            'LC REF ID IS NULL',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_discharge_port IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('VALIDATION_ERROR',
                                            'Discharge Port IS NULL',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   OPEN C_GET_LCDETAIL;
   FETCH C_GET_LCDETAIL INTO L_order_no;
   if C_GET_LCDETAIL%ROWCOUNT = 0 then
      CLOSE C_GET_LCDETAIL;
      return TRUE;
   else
      OPEN C_ORDHEAD;
      FETCH C_ORDHEAD INTO L_discharge_port;
      if C_ORDHEAD%ROWCOUNT = 0 THEN
         RAISE NO_DATA_FOUND;
      end if;
      CLOSE C_GET_LCDETAIL;
      CLOSE C_ORDHEAD;
   end if;

   if L_discharge_port <> I_discharge_port then
      O_warning_flg := 'Y';
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;
END CHECK_DISCHARGE_PORT;
--------------------------------------------------------------------------------------
END LC_APPLY_SQL;
/
