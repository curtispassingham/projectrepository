
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE LC_ACTIVE_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------
-- Name:     UPDATE_ACTIVITY
-- Purpose:  This function will calculate and update the header block fields (Net Amount, Open Amount, 
--           Amendments, Drawdowns and Charges) on the LC Activity form.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ACTIVITY(O_error_message  IN OUT VARCHAR2,
                         O_net_amount     IN OUT lc_activity.amount%TYPE,
                         O_open_amount    IN OUT lc_activity.amount%TYPE,
                         O_amendments     IN OUT lc_activity.amount%TYPE,
                         O_drawdowns      IN OUT lc_activity.amount%TYPE,
                         O_charges        IN OUT lc_activity.amount%TYPE,
                         I_amount         IN     lc_activity.amount%TYPE,
                         I_lc_ref_id      IN     lc_activity.lc_ref_id%TYPE)	
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Name:     ORDER_EXIST
-- Purpose:  This function will check the LC detail table for records that exist with Order that is --           --           passed into the function.  
---------------------------------------------------------------------------------------------
FUNCTION ORDER_EXIST(O_error_message  IN OUT VARCHAR2,
                     O_exist          IN OUT BOOLEAN,
                     I_lc_ref_id      IN     lc_detail.lc_ref_id%TYPE,
                     I_order          IN     lc_detail.order_no%TYPE)	
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: ACTIVITY
-- Purpose:       Writes a record to the lc_activity table when the 
--                letter of credit is confirmed or when an amendment is generated.
--------------------------------------------------------------------------------------
FUNCTION ACTIVITY(O_error_message  IN OUT VARCHAR2,
                  I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE,
                  I_amend_amount   IN     LC_ACTIVITY.AMOUNT%TYPE,
                  I_amend_no       IN     LC_ACTIVITY.TRANS_NO%TYPE)
         RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END;
/


