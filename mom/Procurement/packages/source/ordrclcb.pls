CREATE OR REPLACE PACKAGE BODY ORDER_RECALC_SQL AS
-------------------------------------------------------------------
--DATA TYPE DECLARATION
-------------------------------------------------------------------

TYPE ordhead_info_rectype IS RECORD
   (supplier             ORDHEAD.SUPPLIER%TYPE,
    import_order_ind     ORDHEAD.IMPORT_ORDER_IND%TYPE,
    earliest_ship_date   ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
    latest_ship_date     ORDHEAD.LATEST_SHIP_DATE%TYPE,
    currency_code        ORDHEAD.CURRENCY_CODE%TYPE,
    exchange_rate        ORDHEAD.EXCHANGE_RATE%TYPE,
    import_country_id    ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
    pickup_loc           ORDHEAD.PICKUP_LOC%TYPE,
    pickup_no            ORDHEAD.PICKUP_NO%TYPE);

TYPE repl_results_rectype IS RECORD
   (recalc_type                     REPL_RESULTS.RECALC_TYPE%TYPE,
    recalc_qty                      REPL_RESULTS.RECALC_QTY%TYPE,
    alloc_no                        REPL_RESULTS.ALLOC_NO%TYPE,
    orig_roq                        REPL_RESULTS.ORDER_ROQ%TYPE,
    item                            REPL_RESULTS.ITEM%TYPE,
    item_type                       REPL_RESULTS.ITEM_TYPE%TYPE,
    origin_country_id               REPL_RESULTS.ORIGIN_COUNTRY_ID%TYPE,
    location                        REPL_RESULTS.LOCATION%TYPE,
    loc_type                        REPL_RESULTS.LOC_TYPE%TYPE,
    stock_cat                       REPL_RESULTS.STOCK_CAT%TYPE,
    source_wh                       REPL_RESULTS.SOURCE_WH%TYPE,
    repl_method                     REPL_RESULTS.REPL_METHOD%TYPE,
    pres_stock                      REPL_RESULTS.PRES_STOCK%TYPE,
    demo_stock                      REPL_RESULTS.DEMO_STOCK%TYPE,
    min_stock                       REPL_RESULTS.MIN_STOCK%TYPE,
    max_stock                       REPL_RESULTS.MAX_STOCK%TYPE,
    incr_pct                        REPL_RESULTS.INCR_PCT%TYPE,
    min_supply_days                 REPL_RESULTS.MIN_SUPPLY_DAYS%TYPE,
    max_supply_days                 REPL_RESULTS.MAX_SUPPLY_DAYS%TYPE,
    time_supply_horizon             REPL_RESULTS.TIME_SUPPLY_HORIZON%TYPE,
    inv_selling_days                REPL_RESULTS.INV_SELLING_DAYS%TYPE,
    service_level                   REPL_RESULTS.SERVICE_LEVEL%TYPE,
    lost_sales_factor               REPL_RESULTS.LOST_SALES_FACTOR%TYPE,
    supp_lead_time                  REPL_RESULTS.SUPP_LEAD_TIME%TYPE,
    pickup_lead_time                REPL_RESULTS.PICKUP_LEAD_TIME%TYPE,
    wh_lead_time                    REPL_RESULTS.WH_LEAD_TIME%TYPE,
    next_order_lead_time            REPL_RESULTS.NEXT_ORDER_LEAD_TIME%TYPE,
    review_time                     REPL_RESULTS.REVIEW_TIME%TYPE,
    terminal_stock_qty              REPL_RESULTS.TERMINAL_STOCK_QTY%TYPE,
    season_id                       REPL_RESULTS.SEASON_ID%TYPE,
    phase_id                        REPL_RESULTS.PHASE_ID%TYPE,
    non_scaling_ind                 REPL_RESULTS.NON_SCALING_IND%TYPE,
    store_ord_mult                  REPL_RESULTS.STORE_ORD_MULT%TYPE,
    repl_date                       REPL_RESULTS.REPL_DATE%TYPE);

TYPE new_repls_rectype IS RECORD
   (new_raw_roq                     REPL_RESULTS.ORDER_ROQ%TYPE,
    order_point                     REPL_RESULTS.ORDER_POINT%TYPE,
    order_up_to_point               REPL_RESULTS.ORDER_UP_TO_POINT%TYPE,
    net_inventory                   REPL_RESULTS.NET_INVENTORY%TYPE,
    stock_on_hand                   REPL_RESULTS.STOCK_ON_HAND%TYPE,
    pack_comp_soh                   REPL_RESULTS.PACK_COMP_SOH%TYPE,
    on_order                        REPL_RESULTS.ON_ORDER%TYPE,
    in_transit_qty                  REPL_RESULTS.IN_TRANSIT_QTY%TYPE,
    pack_comp_intran                REPL_RESULTS.PACK_COMP_INTRAN%TYPE,
    tsf_resv_qty                    REPL_RESULTS.TSF_RESV_QTY%TYPE,
    pack_comp_resv                  REPL_RESULTS.PACK_COMP_RESV%TYPE,
    tsf_expected_qty                REPL_RESULTS.TSF_EXPECTED_QTY%TYPE,
    pack_comp_exp                   REPL_RESULTS.PACK_COMP_EXP%TYPE,
    rtv_qty                         REPL_RESULTS.RTV_QTY%TYPE,
    alloc_in_qty                    REPL_RESULTS.ALLOC_IN_QTY%TYPE,
    alloc_out_qty                   REPL_RESULTS.ALLOC_OUT_QTY%TYPE,
    non_sellable_qty                REPL_RESULTS.NON_SELLABLE_QTY%TYPE,
    safety_stock                    REPL_RESULTS.SAFETY_STOCK%TYPE,
    lost_sales                      REPL_RESULTS.LOST_SALES%TYPE,
    due_ind                         REPL_RESULTS.DUE_IND%TYPE,
    aso                             REPL_RESULTS.ACCEPTED_STOCK_OUT%TYPE,
    eso                             REPL_RESULTS.ESTIMATED_STOCK_OUT%TYPE,
    min_supply_days_forecast        REPL_RESULTS.MIN_SUPPLY_DAYS_FORECAST%TYPE,
    max_supply_days_forecast        REPL_RESULTS.MIN_SUPPLY_DAYS_FORECAST%TYPE,
    time_supply_horizon_forecast    REPL_RESULTS.TIME_SUPPLY_HORIZON_FORECAST%TYPE,
    order_lead_time_forecast        REPL_RESULTS.ORDER_LEAD_TIME_FORECAST%TYPE,
    next_lead_time_forecast         REPL_RESULTS.NEXT_LEAD_TIME_FORECAST%TYPE,
    review_time_forecast            REPL_RESULTS.REVIEW_TIME_FORECAST%TYPE,
    inv_sell_days_forecast          REPL_RESULTS.INV_SELL_DAYS_FORECAST%TYPE);

TYPE orig_cntry_tabletype IS TABLE OF ORDSKU.ORIGIN_COUNTRY_ID%TYPE NOT NULL
   INDEX BY BINARY_INTEGER;

-------------------------------------------------------------------
FUNCTION LOCK_REPL_RESULTS(O_error_message   IN OUT VARCHAR2,
                           I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_REPL_RESULTS is
      select 'x'
        from repl_results
       where order_no = I_order_no
         and recalc_type != 'N'
         for update nowait;
BEGIN
   open C_LOCK_REPL_RESULTS;
   close C_LOCK_REPL_RESULTS;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('REPL_RESULTS_LOCKED2',
                                            to_char(I_order_no),
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.LOCK_REPL_RESULTS',
                                             to_char(SQLCODE));
      return FALSE;
END LOCK_REPL_RESULTS;
-------------------------------------------------------------------
FUNCTION INITIALIZE_NEW_REPLS_RECS(O_error_message   IN OUT VARCHAR2,
                                   IO_new_repls_rec   IN OUT NEW_REPLS_RECTYPE,
                                   IO_wh_new_repls_rec   IN OUT NEW_REPLS_RECTYPE)
   RETURN BOOLEAN IS

   L_new_repls_rec   NEW_REPLS_RECTYPE;

BEGIN
   L_new_repls_rec.new_raw_roq                     := NULL;
   L_new_repls_rec.order_point                     := NULL;
   L_new_repls_rec.order_up_to_point               := NULL;
   L_new_repls_rec.net_inventory                   := NULL;
   L_new_repls_rec.stock_on_hand                   := NULL;
   L_new_repls_rec.pack_comp_soh                   := NULL;
   L_new_repls_rec.on_order                        := NULL;
   L_new_repls_rec.in_transit_qty                  := NULL;
   L_new_repls_rec.pack_comp_intran                := NULL;
   L_new_repls_rec.tsf_resv_qty                    := NULL;
   L_new_repls_rec.pack_comp_resv                  := NULL;
   L_new_repls_rec.tsf_expected_qty                := NULL;
   L_new_repls_rec.pack_comp_exp                   := NULL;
   L_new_repls_rec.rtv_qty                         := NULL;
   L_new_repls_rec.alloc_in_qty                    := NULL;
   L_new_repls_rec.alloc_out_qty                   := NULL;
   L_new_repls_rec.non_sellable_qty                := NULL;
   L_new_repls_rec.safety_stock                    := NULL;
   L_new_repls_rec.lost_sales                      := NULL;
   L_new_repls_rec.due_ind                         := 'N'; -- Defualted to not due.
   L_new_repls_rec.aso                             := 0;
   L_new_repls_rec.eso                             := 0;
   L_new_repls_rec.min_supply_days_forecast        := NULL;
   L_new_repls_rec.max_supply_days_forecast        := NULL;
   L_new_repls_rec.time_supply_horizon_forecast    := NULL;
   L_new_repls_rec.order_lead_time_forecast        := NULL;
   L_new_repls_rec.next_lead_time_forecast         := NULL;
   L_new_repls_rec.review_time_forecast            := NULL;
   L_new_repls_rec.inv_sell_days_forecast          := NULL;
   ---
   IO_new_repls_rec    := L_new_repls_rec;
   IO_wh_new_repls_rec := L_new_repls_rec;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.INITIALIZE_NEW_REPLS_RECS',
                                             to_char(SQLCODE));
      return FALSE;
END INITIALIZE_NEW_REPLS_RECS;
-------------------------------------------------------------------
FUNCTION GET_SYSTEM_OPTIONS_INFO(O_error_message     IN OUT   VARCHAR2,
                                 O_elc_ind           IN OUT   SYSTEM_OPTIONS.ELC_IND%TYPE,
                                 O_alloc_method      IN OUT   SYSTEM_OPTIONS.ALLOC_METHOD%TYPE,
                                 O_base_country_id   IN OUT   SYSTEM_OPTIONS.BASE_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_SYS_INFO is
      select elc_ind,
             alloc_method,
             base_country_id
        from system_options;

BEGIN
   open C_GET_SYS_INFO;
   fetch C_GET_SYS_INFO into O_elc_ind,
                             O_alloc_method,
                             O_base_country_id;
   close C_GET_SYS_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_RECALC_SQL.GET_SYSTEM_OPTIONS_INFO',
                                            to_char(SQLCODE));
      return FALSE;
END GET_SYSTEM_OPTIONS_INFO;
-------------------------------------------------------------------
FUNCTION GET_ORDHEAD_INFO(O_error_message      IN OUT VARCHAR2,
                          O_ordhead_info_rec   IN OUT ORDHEAD_INFO_RECTYPE,
                          I_order_no           IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_ORDHEAD_INFO is
      select supplier,
             import_order_ind,
             earliest_ship_date,
             latest_ship_date,
             currency_code,
             exchange_rate,
             import_country_id,
             pickup_loc,
             pickup_no
        from ordhead
       where order_no = I_order_no;

BEGIN
   open C_GET_ORDHEAD_INFO;
   fetch C_GET_ORDHEAD_INFO into O_ordhead_info_rec;
   close C_GET_ORDHEAD_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.GET_ORDHEAD_INFO',
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORDHEAD_INFO;
-------------------------------------------------------------------
FUNCTION GET_ORD_INV_MGMT_INFO(O_error_message        IN OUT VARCHAR2,
                               O_due_ord_process_ind  IN OUT ORD_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE,
                               O_due_ord_ind          IN OUT ORD_INV_MGMT.DUE_ORD_IND%TYPE,
                               O_due_ord_serv_basis   IN OUT ORD_INV_MGMT.DUE_ORD_SERV_BASIS%TYPE,
                               I_order_no             IN     ORD_INV_MGMT.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_DUE_IND is
      select due_ord_process_ind,
             due_ord_ind,
             due_ord_serv_basis
        from ord_inv_mgmt
       where order_no = I_order_no;

BEGIN
   open C_GET_DUE_IND;
   fetch C_GET_DUE_IND into O_due_ord_process_ind,
                            O_due_ord_ind,
                            O_due_ord_serv_basis;
   close C_GET_DUE_IND;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.GET_ORD_INV_MGMT_INFO',
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORD_INV_MGMT_INFO;
-------------------------------------------------------------------
FUNCTION GET_COST_AND_PACKSIZE(O_error_message      IN OUT VARCHAR2,
                               O_unit_cost          IN OUT ORDLOC.UNIT_COST%TYPE,
                               O_ord_pack_size      IN OUT ORDSKU.SUPP_PACK_SIZE%TYPE,
                               O_ordsku_rec_exists  IN OUT VARCHAR2,
                               I_order_no           IN     ORDSKU.ORDER_NO%TYPE,
                               I_item               IN     ORDSKU.ITEM%TYPE,
                               I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                               I_orig_cntry         IN     ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                               I_location           IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_ordsku_pack_size   ORDSKU.SUPP_PACK_SIZE%TYPE      := NULL;
   L_itspcnt_pack_size  ORDSKU.SUPP_PACK_SIZE%TYPE      := NULL;

   cursor C_GET_PACK_SIZE is
      select supp_pack_size
        from ordsku
       where order_no = I_order_no
         and item     = I_item;

   cursor C_GET_ITSPCNT is
      select iscl.unit_cost,
             isc.supp_pack_size
        from item_supp_country isc,
             item_supp_country_loc iscl
       where isc.item              = I_item
         and isc.supplier          = I_supplier
         and isc.origin_country_id = I_orig_cntry
         and isc.item              = iscl.item
         and isc.supplier          = iscl.supplier
         and isc.origin_country_id = iscl.origin_country_id
         and iscl.loc              = I_location;

BEGIN
   open C_GET_PACK_SIZE;
   fetch C_GET_PACK_SIZE into L_ordsku_pack_size;
   ---
   if C_GET_PACK_SIZE%NOTFOUND then
      O_ordsku_rec_exists := 'N';
   else
      O_ordsku_rec_exists := 'Y';
   end if;
   ---
   close C_GET_PACK_SIZE;
   ---
   open C_GET_ITSPCNT;
   fetch C_GET_ITSPCNT into O_unit_cost,
                            L_itspcnt_pack_size;
   close C_GET_ITSPCNT;
   ---
   if L_ordsku_pack_size is not NULL then
      O_ord_pack_size := L_ordsku_pack_size;
   elsif L_itspcnt_pack_size is not NULL then
      O_ord_pack_size := L_itspcnt_pack_size;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.GET_COST_AND_PACKSIZE',
                                             to_char(SQLCODE));
      return FALSE;
END GET_COST_AND_PACKSIZE;
-------------------------------------------------------------------
FUNCTION GET_UNIT_RETAIL(O_error_message   IN OUT VARCHAR2,
                         O_unit_retail     IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                         I_item            IN     ITEM_LOC.ITEM%TYPE,
          I_location        IN     ORDLOC.LOCATION%TYPE,
                         I_loc_type        IN     ORDLOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_av_cost              ITEM_LOC_SOH.AV_COST%TYPE;
   L_unit_cost            ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_selling_unit_retail  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom          ITEM_LOC.SELLING_UOM%TYPE;

BEGIN
   if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                               I_item,
                                               I_location,
                                               I_loc_type,
                                               L_av_cost,
                                               L_unit_cost,
                                               O_unit_retail,
                                               L_selling_unit_retail,
                                               L_selling_uom) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.GET_UNIT_RETAIL',
                                             to_char(SQLCODE));
      return FALSE;
END GET_UNIT_RETAIL;
-------------------------------------------------------------------
FUNCTION GET_NEW_RAW_ROQ(O_error_message         IN OUT   VARCHAR2,
                         O_new_repls_rec         IN OUT   NEW_REPLS_RECTYPE,
                         I_results_rec           IN       REPL_RESULTS_RECTYPE,
                         I_store_need            IN       NUMBER,
                         I_due_ord_process_ind   IN       ORD_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE,
                         I_due_ord_serv_basis    IN       ORD_INV_MGMT.DUE_ORD_SERV_BASIS%TYPE,
                         I_unit_cost             IN       ORDLOC.UNIT_COST%TYPE,
                         I_unit_retail           IN       ORDLOC.UNIT_RETAIL%TYPE,
                         I_domain_id             IN       ITEM_FORECAST.DOMAIN_ID%TYPE)
   RETURN BOOLEAN IS

   L_order_lead_time   NUMBER;
   L_new_raw_roq       NUMBER;
   L_eso               NUMBER;
   L_aso               NUMBER;

BEGIN
   -- The order lead time is the sum of the three
   -- leadtime components: supplier, pick up to loc,
   -- and wh to loc.
   ---
   L_order_lead_time := (NVL(I_results_rec.supp_lead_time, 0) +
                         NVL(I_results_rec.pickup_lead_time, 0) +
                         NVL(I_results_rec.wh_lead_time, 0));
   ---
   if GET_REPL_ORDER_QTY_SQL.REPL_METHOD(O_error_message,
                                         O_new_repls_rec.new_raw_roq,
                                         O_new_repls_rec.order_point,
                                         O_new_repls_rec.order_up_to_point,
                                         O_new_repls_rec.net_inventory,
                                         O_new_repls_rec.stock_on_hand,
                                         O_new_repls_rec.pack_comp_soh,
                                         O_new_repls_rec.on_order,
                                         O_new_repls_rec.in_transit_qty,
                                         O_new_repls_rec.pack_comp_intran,
                                         O_new_repls_rec.tsf_resv_qty,
                                         O_new_repls_rec.pack_comp_resv,
                                         O_new_repls_rec.tsf_expected_qty,
                                         O_new_repls_rec.pack_comp_exp,
                                         O_new_repls_rec.rtv_qty,
                                         O_new_repls_rec.alloc_in_qty,
                                         O_new_repls_rec.alloc_out_qty,
                                         O_new_repls_rec.non_sellable_qty,
                                         O_new_repls_rec.safety_stock,
                                         O_new_repls_rec.lost_sales,
                                         O_new_repls_rec.due_ind,
                                         O_new_repls_rec.aso,
                                         O_new_repls_rec.eso,
                                         O_new_repls_rec.min_supply_days_forecast,
                                         O_new_repls_rec.max_supply_days_forecast,
                                         O_new_repls_rec.time_supply_horizon_forecast,
                                         O_new_repls_rec.order_lead_time_forecast,
                                         O_new_repls_rec.next_lead_time_forecast,
                                         O_new_repls_rec.review_time_forecast,
                                         O_new_repls_rec.inv_sell_days_forecast,
                                         I_results_rec.item,
                                         I_results_rec.loc_type,
                                         I_results_rec.location,
                                         NULL,
                                         I_store_need,
                                         I_results_rec.pres_stock,
                                         I_results_rec.demo_stock,
                                         I_results_rec.repl_method,
                                         I_results_rec.min_stock,
                                         I_results_rec.max_stock,
                                         I_results_rec.incr_pct,
                                         I_results_rec.min_supply_days,
                                         I_results_rec.max_supply_days,
                                         I_results_rec.time_supply_horizon,
                                         I_results_rec.inv_selling_days,
                                         I_results_rec.service_level,
                                         I_results_rec.lost_sales_factor,
                                         L_order_lead_time,
                                         I_results_rec.next_order_lead_time,
                                         I_results_rec.review_time,
                                         I_results_rec.terminal_stock_qty,
                                         I_due_ord_serv_basis,
                                         I_unit_cost,
                                         I_unit_retail,
                                         I_due_ord_process_ind,
                                         NULL,
                                         I_results_rec.season_id,
                                         I_results_rec.phase_id,
                                         I_domain_id,
                                         NULL,
                                         I_results_rec.repl_date) = FALSE then
      return FALSE;
   end if;
   ---
   L_new_raw_roq := O_new_repls_rec.new_raw_roq;
   L_aso := O_new_repls_rec.aso;
   L_eso := O_new_repls_rec.eso;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_RECALC_SQL.GET_NEW_RAW_ROQ',
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEW_RAW_ROQ;
-------------------------------------------------------------------
FUNCTION GET_REF_ITEM(O_error_message   IN OUT VARCHAR2,
                      O_ref_item        IN OUT ORDSKU.REF_ITEM%TYPE,
                      I_item            IN     ORDSKU.ITEM%TYPE,
                      I_supplier        IN     SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_item_number_type   ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE;

   cursor C_PRIM_REF_ITEM is
      select item
        from item_master
       where item_parent = I_item
         and primary_ref_item_ind = 'Y';
BEGIN
   open C_PRIM_REF_ITEM;
   fetch C_PRIM_REF_ITEM into O_ref_item;
   close C_PRIM_REF_ITEM;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.GET_REF_ITEM',
                                             to_char(SQLCODE));
      return FALSE;
END GET_REF_ITEM;
-------------------------------------------------------------------
FUNCTION INSERT_ORDSKU(O_error_message     IN OUT VARCHAR2,
                       I_order_no          IN     ORDSKU.ORDER_NO%TYPE,
                       I_item              IN     ORDSKU.ITEM%TYPE,
                       I_ref_item          IN     ORDSKU.REF_ITEM%TYPE,
                       I_orig_cntry        IN     ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                       I_ordhead_info_rec  IN     ORDHEAD_INFO_RECTYPE,
                       I_ord_pack_size     IN     ORDSKU.SUPP_PACK_SIZE%TYPE)
   RETURN BOOLEAN IS

BEGIN
   insert into ordsku(order_no,
                      item,
                      ref_item,
                      origin_country_id,
                      earliest_ship_date,
                      latest_ship_date,
                      supp_pack_size,
                      non_scale_ind,
                      pickup_loc,
                      pickup_no)
               values(I_order_no,
                      I_item,
                      I_ref_item,
                      I_orig_cntry,
                      I_ordhead_info_rec.earliest_ship_date,
                      I_ordhead_info_rec.latest_ship_date,
                      I_ord_pack_size,
                      'N',
                      I_ordhead_info_rec.pickup_loc,
                      I_ordhead_info_rec.pickup_no);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.INSERT_ORDSKU',
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_ORDSKU;
-------------------------------------------------------------------
FUNCTION DEFAULT_DOCS(O_error_message   IN OUT VARCHAR2,
                      I_order_no        IN     ORDSKU.ORDER_NO%TYPE,
                      I_item            IN     ORDSKU.ITEM%TYPE,
                      I_orig_cntry      IN     ORDSKU.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

BEGIN
   -- Attach item level required documents to the order/item.
   if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                 'IT', -- Item level
                                 'POIT',
                                 I_item,
                                 I_order_no,
                                 NULL,
                                 I_item) = FALSE then
      return FALSE;
   end if;
   ---
   -- Attach country level required documents to the order/item.
   if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                 'CTRY', -- Country level
                                 'POIT',
                                 I_orig_cntry,
                                 I_order_no,
                                 NULL,
                                 I_item) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.DEFAULT_DOCS',
                                             to_char(SQLCODE));
      return FALSE;
END DEFAULT_DOCS;
-------------------------------------------------------------------
FUNCTION DEFAULT_HTS(O_error_message   IN OUT VARCHAR2,
                     I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                     I_import_cntry    IN     ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                     I_item            IN     ORDSKU.ITEM%TYPE)
   RETURN BOOLEAN IS

   -- Retrieve any chapters associated with the hts codes
   cursor C_GET_CHAPTERS is
      select distinct h.chapter
        from hts h,
             ordsku_hts osh
       where osh.order_no          = I_order_no
         and osh.hts               = h.hts
         and osh.import_country_id = I_import_cntry
         and osh.item              = I_item
         and osh.pack_item        is NULL;

BEGIN
   -- Attach the appropriate hts codes to the order/item.
   if ORDER_HTS_SQL.DEFAULT_HTS(O_error_message,
                                I_order_no,
                                I_item,
                 NULL) = FALSE then
      return FALSE;
   end if;
   ---
   -- Attach any required documents associated with the
   -- hts chapters to the order/item.
   ---
   for chapt_rec in C_GET_CHAPTERS loop
      if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                    'HTSC',
                                    'POIT',
                                    chapt_rec.chapter,
                                    I_order_no,
                                    I_import_cntry,
                                    I_item) = FALSE then
         return FALSE;
      end if;
   end loop;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.DEFAULT_HTS',
                                             to_char(SQLCODE));
      return FALSE;
END DEFAULT_HTS;
-------------------------------------------------------------------
FUNCTION UPDATE_IMPORT_STATUS(O_error_message       IN OUT VARCHAR2,
                              I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                              I_new_import_status   IN     ORDHEAD.IMPORT_ORDER_IND%TYPE)
   RETURN BOOLEAN IS

BEGIN
   update ordhead
      set import_order_ind     = I_new_import_status,
          last_update_id       = get_user,
          last_update_datetime = sysdate
    where order_no             = I_order_no;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.UPDATE_IMPORT_STATUS',
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_IMPORT_STATUS;
-------------------------------------------------------------------
FUNCTION ROUND_EACHES(O_error_message   IN OUT VARCHAR2,
                      IO_eaches         IN OUT NUMBER)
   RETURN BOOLEAN IS

BEGIN
   -- This funtion will round up to the nearest integer.
   if ((trunc(IO_eaches) + 1) - IO_eaches) < 1 then
      IO_eaches := (trunc(IO_eaches) + 1);
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.ROUND_EACHES',
                                             to_char(SQLCODE));
      return FALSE;
END ROUND_EACHES;
-------------------------------------------------------------------
FUNCTION CREATE_ORDSKU(O_error_message     IN OUT VARCHAR2,
                       I_order_no          IN     ORDSKU.ORDER_NO%TYPE,
                       I_item              IN     ORDSKU.ITEM%TYPE,
                       I_ordhead_info_rec  IN     ORDHEAD_INFO_RECTYPE,
                       I_orig_cntry        IN     ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                       I_base_cntry_id     IN     SYSTEM_OPTIONS.BASE_COUNTRY_ID%TYPE,
                       I_ord_pack_size     IN     ORDSKU.SUPP_PACK_SIZE%TYPE,
                       I_elc_ind           IN     SYSTEM_OPTIONS.ELC_IND%TYPE)
   RETURN BOOLEAN IS

   L_ref_item            ORDSKU.REF_ITEM%TYPE;
   L_new_import_status   ORDHEAD.IMPORT_ORDER_IND%TYPE;

BEGIN
   if GET_REF_ITEM(O_error_message,
                   L_ref_item,
                   I_item,
                   I_ordhead_info_rec.supplier) = FALSE then
      return FALSE;
   end if;
   ---
   if INSERT_ORDSKU(O_error_message,
                    I_order_no,
                    I_item,
                    L_ref_item,
                    I_orig_cntry,
                    I_ordhead_info_rec,
                    I_ord_pack_size) = FALSE then
      return FALSE;
   end if;
   ---
   if DEFAULT_DOCS(O_error_message,
                   I_order_no,
                   I_item,
                   I_orig_cntry) = FALSE then
      return FALSE;
   end if;
   ---
   -- If this item is being imported into the country
   ---
   if I_orig_cntry != nvl(I_ordhead_info_rec.import_country_id, I_base_cntry_id) then
      if I_elc_ind = 'Y' then
         if DEFAULT_HTS(O_error_message,
                        I_order_no,
                        nvl(I_ordhead_info_rec.import_country_id, I_base_cntry_id),
                        I_item) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- If this was originally not an import order but became one through the
      -- addition of an import item, ordhead's import order ind should be updated.
      ---
      if I_ordhead_info_rec.import_order_ind = 'N' then
         L_new_import_status := 'Y';
         ---
         if UPDATE_IMPORT_STATUS(O_error_message,
                                 I_order_no,
                                 L_new_import_status) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.CREATE_ORDSKU',
                                             to_char(SQLCODE));
      return FALSE;
END CREATE_ORDSKU;
-------------------------------------------------------------------
FUNCTION FETCH_EXISTING_ORDLOC(O_error_message        IN OUT VARCHAR2,
                               O_existing_ordloc_qty  IN OUT ORDLOC.QTY_ORDERED%TYPE,
                               I_order_no             IN     ORDLOC.ORDER_NO%TYPE,
                               I_item                 IN     ORDLOC.ITEM%TYPE,
                               I_location             IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_QTY is
      select qty_ordered
        from ordloc
       where order_no = I_order_no
         and item     = I_item
         and location = I_location;

BEGIN
   open C_GET_QTY;
   fetch C_GET_QTY into O_existing_ordloc_qty;
   close C_GET_QTY;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.FETCH_EXISTING_ORDLOC',
                                             to_char(SQLCODE));
      return FALSE;
END FETCH_EXISTING_ORDLOC;
-------------------------------------------------------------------
FUNCTION INSERT_ORDLOC(O_error_message     IN OUT VARCHAR2,
                       I_order_no          IN     ORDLOC.ORDER_NO%TYPE,
                       I_item              IN     ORDLOC.ITEM%TYPE,
                       I_location          IN     ORDLOC.LOCATION%TYPE,
                       I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                       I_unit_retail       IN     ORDLOC.UNIT_RETAIL%TYPE,
                       I_order_qty         IN     ORDLOC.QTY_ORDERED%TYPE,
                       I_unit_cost         IN     ORDLOC.UNIT_COST%TYPE,
                       I_order_currency    IN     ORDHEAD.CURRENCY_CODE%TYPE,
                       I_supp_currency     IN     SUPS.CURRENCY_CODE%TYPE,
                       I_ord_exchange_rate IN     ORDHEAD.EXCHANGE_RATE%TYPE,
                       I_non_scaling_ind   IN     ORDLOC.NON_SCALE_IND%TYPE)
   RETURN BOOLEAN IS

   L_unit_cost   ORDLOC.UNIT_COST%TYPE  := I_unit_cost;

BEGIN
   if I_order_currency != I_supp_currency then
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_unit_cost,
                              I_supp_currency,
                              I_order_currency,
                              L_unit_cost,
                              'C', -- C for cost conversion
                              NULL,
                              'P', -- purchase order exhange type
                              NULL,
                              I_ord_exchange_rate) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   insert into ordloc(order_no,
                      item,
                      location,
                      loc_type,
                      unit_retail,
                      qty_ordered,
                      qty_prescaled,
                      original_repl_qty,
                      unit_cost,
                      cost_source,
                      non_scale_ind)
               values(I_order_no,
                      I_item,
                      I_location,
                      I_loc_type,
                      I_unit_retail,
                      I_order_qty,
                      I_order_qty,
                      I_order_qty,
                      L_unit_cost,
                      'NORM',
                      I_non_scaling_ind);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.INSERT_ORDLOC',
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_ORDLOC;
-------------------------------------------------------------------
FUNCTION CALC_ALLOC_ORDLOC_UPDATE(O_error_message          IN OUT VARCHAR2,
                                  O_update_qty             IN OUT ORDLOC.QTY_ORDERED%TYPE,
                                  I_existing_ordloc_qty    IN     ORDLOC.QTY_ORDERED%TYPE,
                                  I_orig_total_alloc_qty   IN     NUMBER,
                                  I_new_total_alloc_qty    IN     NUMBER)
   RETURN BOOLEAN IS

BEGIN
   O_update_qty := nvl(I_existing_ordloc_qty, 0) + (nvl(I_new_total_alloc_qty,0) - nvl(I_orig_total_alloc_qty,0));
   ---
   if O_update_qty < I_new_total_alloc_qty then
      O_update_qty := I_new_total_alloc_qty;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.CALC_ALLOC_ORDLOC_UPDATE',
                                             to_char(SQLCODE));
      return FALSE;
END CALC_ALLOC_ORDLOC_UPDATE;
-------------------------------------------------------------------
FUNCTION GET_TOTAL_ALLOC_QTY(O_error_message   IN OUT VARCHAR2,
                             O_total_alloc_qty IN OUT NUMBER,
                             I_alloc_no        IN     ALLOC_HEADER.ALLOC_NO%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_ALLOC_QTY is
      select sum(qty_allocated)
        from alloc_detail
       where alloc_no = I_alloc_no;
BEGIN
   open C_GET_ALLOC_QTY;
   fetch C_GET_ALLOC_QTY into O_total_alloc_qty;
   close C_GET_ALLOC_QTY;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.GET_TOTAL_ALLOC_QTY',
                                             to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_ALLOC_QTY;
-------------------------------------------------------------------
FUNCTION ALLOCS_EXIST(O_error_message   IN OUT VARCHAR2,
                      O_allocs_exist    IN OUT VARCHAR2,
                      O_alloc_no        IN OUT ALLOC_HEADER.ALLOC_NO%TYPE,
                      I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                      I_item            IN     ORDSKU.ITEM%TYPE,
                      I_location        IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   cursor C_ALLOCS_EXIST is
      select alloc_no
        from alloc_header
       where order_no = I_order_no
         and item     = I_item
         and wh       = I_location;

BEGIN
   open C_ALLOCS_EXIST;
   fetch C_ALLOCS_EXIST into O_alloc_no;
   ---
   if C_ALLOCS_EXIST%NOTFOUND then
      O_allocs_exist := 'N';
   else
      O_allocs_exist := 'Y';
   end if;
   ---
   close C_ALLOCS_EXIST;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.ALLOCS_EXIST',
                                             to_char(SQLCODE));
      return FALSE;
END ALLOCS_EXIST;
-------------------------------------------------------------------
FUNCTION CALC_NONALLOC_ORDLOC_UPDATE(O_error_message       IN OUT VARCHAR2,
                                     O_update_qty          IN OUT NUMBER,
                                     I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                                     I_item                IN     ORDLOC.ITEM%TYPE,
                                     I_location            IN     ORDLOC.LOCATION%TYPE,
                                     I_new_roq             IN     NUMBER,
                                     I_ordloc_rec_exists   IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_allocs_exist      VARCHAR2(1);
   L_total_alloc_qty   NUMBER;
   L_alloc_no          ALLOC_HEADER.ALLOC_NO%TYPE;
   L_new_roq           NUMBER := I_new_roq;

BEGIN
   if nvl(L_new_roq, -1) < 0 then
      L_new_roq := 0;
   end if;
   ---
   -- If there is no ordloc rec, no allocs can exist.
   ---
   if I_ordloc_rec_exists != 'N' then
      if ALLOCS_EXIST(O_error_message,
                      L_allocs_exist,
                      L_alloc_no,
                      I_order_no,
                      I_item,
                      I_location) = FALSE then
         return FALSE;
      end if;
   elsif I_ordloc_rec_exists = 'N' then
      L_allocs_exist := 'N';
   end if;
   ---
   if L_allocs_exist = 'Y' then
      if GET_TOTAL_ALLOC_QTY(O_error_message,
                             L_total_alloc_qty,
                             L_alloc_no) = FALSE then
         return FALSE;
      end if;
      ---
      O_update_qty := nvl(L_total_alloc_qty, 0) + L_new_roq;
   else
      O_update_qty := L_new_roq;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.CALC_NONALLOC_ORDLOC_UPDATE',
                                             to_char(SQLCODE));
      return FALSE;
END CALC_NONALLOC_ORDLOC_UPDATE;
-------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC(O_error_message  IN OUT VARCHAR2,
                       I_update_qty     IN     ORDLOC.QTY_ORDERED%TYPE,
                       I_order_no       IN     ORDLOC.ORDER_NO%TYPE,
                       I_item           IN     ORDLOC.ITEM%TYPE,
                       I_location       IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_REC is
      select 'x'
        from ordloc
       where order_no = I_order_no
         and item     = I_item
         and location = I_location
         for update nowait;

BEGIN
   open C_LOCK_REC;
   close C_LOCK_REC;
   ---
   update ordloc
      set qty_ordered       = I_update_qty,
          qty_prescaled     = I_update_qty,
          original_repl_qty = I_update_qty
    where order_no = I_order_no
      and item     = I_item
      and location = I_location;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ORDLOC_REC_LOCK_3',
                                            to_char(I_order_no),
                                            I_item,
                                            to_char(I_location));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.UPDATE_ORDLOC',
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_ORDLOC;
-------------------------------------------------------------------
FUNCTION ORDLOC_EXISTS(O_error_message       IN OUT VARCHAR2,
                       O_ordloc_rec_exists   IN OUT VARCHAR2,
                       I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                       I_item                IN     ORDSKU.ITEM%TYPE,
                       I_location            IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_fetch      VARCHAR2(1) := NULL;

   cursor C_ORDLOC_EXISTS is
      select 'x'
        from ordloc
       where order_no = I_order_no
         and item     = I_item
         and location = I_location;

BEGIN
   open C_ORDLOC_EXISTS;
   fetch C_ORDLOC_EXISTS into L_fetch;
   close C_ORDLOC_EXISTS;
   ---
   if L_fetch is NULL then
      O_ordloc_rec_exists := 'N';
   elsif L_fetch = 'x' then
      O_ordloc_rec_exists := 'Y';
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.ORDLOC_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END ORDLOC_EXISTS;
-------------------------------------------------------------------
FUNCTION DELETE_ORDLOC(O_error_message   IN OUT VARCHAR2,
                       I_order_no        IN     ORDLOC.ORDER_NO%TYPE,
                       I_item            IN     ORDLOC.ITEM%TYPE,
                       I_location        IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_REC is
      select 'x'
        from ordloc
       where order_no = I_order_no
         and item     = I_item
         and location = I_location
         for update nowait;

BEGIN
   open C_LOCK_REC;
   close C_LOCK_REC;
   ---
   delete from ordloc
      where order_no = I_order_no
        and item = I_item
        and location = I_location;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ORDLOC_REC_LOCK_3',
                                            to_char(I_order_no),
                                            I_item,
                                            to_char(I_location));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.DELETE_ORDLOC',
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_ORDLOC;
-------------------------------------------------------------------
FUNCTION NONXDOCK_ORDLOC_MAINTENANCE(O_error_message     IN OUT VARCHAR2,
                                     IO_recalc_expenses  IN OUT VARCHAR2,
                                     I_ordloc_rec_exists IN     VARCHAR2,
                                     I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                     I_item              IN     ORDLOC.ITEM%TYPE,
                                     I_location          IN     ORDLOC.LOCATION%TYPE,
                                     I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                                     I_new_roq           IN     NUMBER,
                                     I_new_raw_roq       IN     NUMBER,
                                     I_unit_retail       IN     ORDLOC.UNIT_RETAIL%TYPE,
                                     I_unit_cost         IN     ORDLOC.UNIT_COST%TYPE,
                                     I_supplier          IN     ORDHEAD.SUPPLIER%TYPE,
                                     I_supp_currency     IN     SUPS.CURRENCY_CODE%TYPE,
                                     I_order_currency    IN     ORDHEAD.CURRENCY_CODE%TYPE,
                                     I_ord_exchange_rate IN     ORDHEAD.EXCHANGE_RATE%TYPE,
                                     I_orig_cntry        IN     ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                     I_elc_ind           IN     SYSTEM_OPTIONS.ELC_IND%TYPE,
                                     I_non_scaling_ind   IN     REPL_RESULTS.NON_SCALING_IND%TYPE)
   RETURN BOOLEAN IS

   L_recalc_expenses       VARCHAR2(1)             := IO_recalc_expenses;
   L_unit_retail           ORDLOC.UNIT_RETAIL%TYPE := I_unit_retail;
   L_unit_cost             ORDLOC.UNIT_COST%TYPE   := I_unit_cost;
   L_ordloc_rec_exists     VARCHAR2(1)             := I_ordloc_rec_exists;
   L_update_qty            ORDLOC.QTY_ORDERED%TYPE;
   L_ord_pack_size         ORDSKU.SUPP_PACK_SIZE%TYPE;
   L_ordsku_rec_exists     VARCHAR2(1);

BEGIN
   if L_ordloc_rec_exists is NULL then
      if ORDLOC_EXISTS(O_error_message,
                       L_ordloc_rec_exists,
                       I_order_no,
                       I_item,
                       I_location) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   -- Calculate the qty to be used to update ordloc.
   ---
   if CALC_NONALLOC_ORDLOC_UPDATE(O_error_message,
                                  L_update_qty,
                                  I_order_no,
                                  I_item,
                                  I_location,
                                  I_new_roq,
                                  L_ordloc_rec_exists) = FALSE then
      return FALSE;
   end if;
   ---
   if L_update_qty > 0 then
      ---
      -- If there is no ordloc record, then one will need to be inserted.
      ---
      if L_ordloc_rec_exists = 'N' then
         if L_unit_retail is NULL then
            if GET_UNIT_RETAIL(O_error_message,
                               L_unit_retail,
                               I_item,
                               I_location,
                               I_loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_unit_cost is NULL then
            if GET_COST_AND_PACKSIZE(O_error_message,
                                     L_unit_cost,
                                     L_ord_pack_size,
                                     L_ordsku_rec_exists,
                                     I_order_no,
                                     I_item,
                                     I_supplier,
                                     I_orig_cntry,
                                     I_location) = FALSE then
                return FALSE;
             end if;
         end if;
         ---
         if INSERT_ORDLOC(O_error_message,
                          I_order_no,
                          I_item,
                          I_location,
                          I_loc_type,
                          L_unit_retail,
                          I_new_roq,
                          L_unit_cost,
                          I_order_currency,
                          I_supp_currency,
                          I_ord_exchange_rate,
                          I_non_scaling_ind) = FALSE then
            return FALSE;
         end if;
         ---
         -- If the elc ind is Y, then the landed costs associated with
         -- this order/item/location will need to be defaulted/calculated.
         ---
         if I_elc_ind = 'Y' then
            if ORDER_EXPENSE_SQL.INSERT_COST_COMP(O_error_message,
                                                  I_order_no,
                                                  I_item,
                    NULL,
                                                  I_location,
                                                  I_loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
      ---
      -- If there is an ordloc record, update ordloc with the positive update qty
      elsif L_ordloc_rec_exists = 'Y' then
         if UPDATE_ORDLOC(O_error_message,
                          L_update_qty,
                          I_order_no,
                          I_item,
                          I_location) = FALSE then
            return FALSE;
         end if;
         ---
         if I_elc_ind = 'Y' then
            L_recalc_expenses := 'Y';
         end if;
      end if;
   elsif (L_update_qty <= 0) and (L_ordloc_rec_exists = 'Y') then
      ---
      -- This function will delete all records associated with the
      -- the ordloc record
      if ORDER_SETUP_SQL.DELETE_ORDER (O_error_message,
                                       I_order_no,
                                       I_item,
                                       I_location,
                                       I_loc_type,
                                       'ordloc') = FALSE then
         return FALSE;
      end if;
      ---
      -- this function will actually delete the ordloc record.
      if DELETE_ORDLOC(O_error_message,
                       I_order_no,
                       I_item,
                       I_location) = FALSE then
         return FALSE;
      end if;
      ---
      if I_elc_ind = 'Y' then
         L_recalc_expenses := 'Y';
      end if;
   end if;
   ---
   IO_recalc_expenses := L_recalc_expenses;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.NONXDOCK_ORDLOC_MAINTENANCE',
                                             to_char(SQLCODE));
      return FALSE;
END NONXDOCK_ORDLOC_MAINTENANCE;
-------------------------------------------------------------------
FUNCTION NONXDOCK_ROQ_ORD_MAINTENANCE(O_error_message      IN OUT VARCHAR2,
                                      IO_recalc_expenses   IN OUT VARCHAR2,
                                      IO_ordsku_rec_exists IN OUT VARCHAR2,
                                      I_order_no           IN     ORDHEAD.ORDER_NO%TYPE,
                                      I_ordhead_info_rec   IN     ORDHEAD_INFO_RECTYPE,
                                      I_item               IN     ORDSKU.ITEM%TYPE,
                                      I_supp_currency      IN     CURRENCIES.CURRENCY_CODE%TYPE,
                                      I_orig_roq           IN     REPL_RESULTS.ORDER_ROQ%TYPE,
                                      I_new_roq            IN     NUMBER,
                                      I_new_raw_roq        IN     NUMBER,
                                      I_location           IN     ORDLOC.LOCATION%TYPE,
                                      I_loc_type           IN     ORDLOC.LOC_TYPE%TYPE,
                                      I_elc_ind            IN     SYSTEM_OPTIONS.ELC_IND%TYPE,
                                      I_orig_cntry         IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                      I_base_cntry_id      IN     SYSTEM_OPTIONS.BASE_COUNTRY_ID%TYPE,
                                      I_unit_cost          IN     ORDLOC.UNIT_COST%TYPE,
                                      I_unit_retail        IN     ORDLOC.UNIT_RETAIL%TYPE,
                                      I_ord_pack_size      IN     ORDSKU.SUPP_PACK_SIZE%TYPE,
                                      I_non_scaling_ind    IN     REPL_RESULTS.NON_SCALING_IND%TYPE)
   RETURN BOOLEAN IS

   L_recalc_expenses   VARCHAR2(1)                              := IO_recalc_expenses;
   L_ordsku_rec_exists VARCHAR2(1)                              := IO_ordsku_rec_exists;
   L_ordloc_rec_exists VARCHAR2(1);

BEGIN
   --************************************************************************
   -- If the new roq is positive, then the order will need to be updated    *
   -- to reflect the new roq. There may or may not already be an ordsku     *
   -- an ordsku record. If not, one will be created. There may or may not   *
   -- be an ordloc record. If not, one will be created. If so, the existing *
   -- ordloc record will be updated to reflect the new roq                  *
   --************************************************************************
   if I_new_roq > 0 then
      ---
      -- If there is no ordsku record, one needs to be created prior to
      -- the creation of an ordloc record.
      ---
      if L_ordsku_rec_exists = 'N' then
         if CREATE_ORDSKU(O_error_message,
                          I_order_no,
                          I_item,
                          I_ordhead_info_rec,
                          I_orig_cntry,
                          I_base_cntry_id,
                          I_ord_pack_size,
                          I_elc_ind) = FALSE then
            return FALSE;
         end if;
         ---
         L_ordsku_rec_exists := 'Y';
         L_ordloc_rec_exists := 'N';
      end if;
   end if;
   ---
   -- Create, update or delete the ordloc record
   ---
   if NONXDOCK_ORDLOC_MAINTENANCE(O_error_message,
                                  L_recalc_expenses,
                                  L_ordloc_rec_exists,
                                  I_order_no,
                                  I_item,
                                  I_location,
                                  I_loc_type,
                                  I_new_roq,
                                  I_new_raw_roq,
                                  I_unit_retail,
                                  I_unit_cost,
                                  I_ordhead_info_rec.supplier,
                                  I_supp_currency,
                                  I_ordhead_info_rec.currency_code,
                                  I_ordhead_info_rec.exchange_rate,
                                  I_orig_cntry,
                                  I_elc_ind,
                                  I_non_scaling_ind) = FALSE then
      return FALSE;
   end if;
   ---
   IO_recalc_expenses   := L_recalc_expenses;
   IO_ordsku_rec_exists := L_ordsku_rec_exists;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.NONXDOCK_ROQ_ORD_MAINTENANCE',
                                             to_char(SQLCODE));
      return FALSE;
END NONXDOCK_ROQ_ORD_MAINTENANCE;
-------------------------------------------------------------------
FUNCTION UPDATE_REPL_RESULTS(O_error_message   IN OUT VARCHAR2,
                             I_new_repls_rec   IN     NEW_REPLS_RECTYPE,
                             I_new_roq         IN     REPL_RESULTS.ORDER_ROQ%TYPE,
                             I_order_no        IN     REPL_RESULTS.ORDER_NO%TYPE,
                             I_alloc_no        IN     REPL_RESULTS.ALLOC_NO%TYPE,
                             I_item            IN     REPL_RESULTS.ITEM%TYPE,
                             I_location        IN     REPL_RESULTS.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_table             VARCHAR2(30);
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_REPL_RESULTS is
      select 'x'
        from repl_results
       where order_no = I_order_no
         and item     = I_item
         and location = I_location
         for update nowait;
BEGIN
   -- lock table records before updating
   ---
   L_table := 'repl_results';
   open C_LOCK_REPL_RESULTS;
   close C_LOCK_REPL_RESULTS;
   ---
   update repl_results
      set alloc_no                     = I_alloc_no,
          raw_roq                      = I_new_repls_rec.new_raw_roq,
          prescale_roq                 = I_new_roq,
          order_roq                    = I_new_roq,
          order_point                  = I_new_repls_rec.order_point,
          order_up_to_point            = I_new_repls_rec.order_up_to_point,
          net_inventory                = I_new_repls_rec.net_inventory,
          stock_on_hand                = I_new_repls_rec.stock_on_hand,
          pack_comp_soh                = I_new_repls_rec.pack_comp_soh,
          on_order                     = I_new_repls_rec.on_order,
          in_transit_qty               = I_new_repls_rec.in_transit_qty,
          pack_comp_intran             = I_new_repls_rec.pack_comp_intran,
          tsf_resv_qty                 = I_new_repls_rec.tsf_resv_qty,
          pack_comp_resv               = I_new_repls_rec.pack_comp_resv,
          tsf_expected_qty             = I_new_repls_rec.tsf_expected_qty,
          pack_comp_exp                = I_new_repls_rec.pack_comp_exp,
          rtv_qty                      = I_new_repls_rec.rtv_qty,
          alloc_in_qty                 = I_new_repls_rec.alloc_in_qty,
          alloc_out_qty                = I_new_repls_rec.alloc_out_qty,
          non_sellable_qty             = I_new_repls_rec.non_sellable_qty,
          safety_stock                 = I_new_repls_rec.safety_stock,
          lost_sales                   = I_new_repls_rec.lost_sales,
          due_ind                      = (nvl(I_new_repls_rec.due_ind, 'N')),
          accepted_stock_out           = (nvl(I_new_repls_rec.aso, 0)),
          estimated_stock_out          = (nvl(I_new_repls_rec.eso, 0)),
          min_supply_days_forecast     = I_new_repls_rec.min_supply_days_forecast,
          max_supply_days_forecast     = I_new_repls_rec.max_supply_days_forecast,
          time_supply_horizon_forecast = I_new_repls_rec.time_supply_horizon_forecast,
          order_lead_time_forecast     = I_new_repls_rec.order_lead_time_forecast,
          next_lead_time_forecast      = I_new_repls_rec.next_lead_time_forecast,
          review_time_forecast         = I_new_repls_rec.review_time_forecast,
          inv_sell_days_forecast       = I_new_repls_rec.inv_sell_days_forecast,
          recalc_qty                   = NULL, -- Set any recalc qty to NULL since recalc has occured
          recalc_type                  = 'N' -- Set recalc type to 'N'one since recalc has occured
    where order_no = I_order_no
      and item     = I_item
      and location = I_location;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             to_char(I_order_no),
                                             I_item);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ORDER_RECALC_SQL.UPDATE_REPL_RESULTS',
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_REPL_RESULTS;
-------------------------------------------------------------------
FUNCTION GET_INNER_CASE(O_error_message   IN OUT VARCHAR2,
                        O_inner_case      IN OUT ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                        I_item            IN     ORDSKU.ITEM%TYPE,
                        I_supplier        IN     ORDHEAD.SUPPLIER%TYPE,
                        I_orig_cntry      IN     ORDSKU.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_item            ORDSKU.ITEM%TYPE               := I_item;
   L_supplier        ORDHEAD.SUPPLIER%TYPE         := I_supplier;
   L_orig_cntry      ORDSKU.ORIGIN_COUNTRY_ID%TYPE := I_orig_cntry;

   cursor C_GET_INNER_CASE is
      select inner_pack_size
        from item_supp_country
       where item              = L_item
         and supplier          = L_supplier
         and origin_country_id = L_orig_cntry;

BEGIN
   open C_GET_INNER_CASE;
   fetch C_GET_INNER_CASE into O_inner_case;
   close C_GET_INNER_CASE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.GET_INNER_CASE',
                                             to_char(SQLCODE));
      return FALSE;
END GET_INNER_CASE;
-------------------------------------------------------------------
FUNCTION GET_BREAK_PACK_IND(O_error_message   IN OUT VARCHAR2,
                            O_break_pack_ind  IN OUT WH.BREAK_PACK_IND%TYPE,
                            I_wh              IN     WH.WH%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_BREAK_PACK_IND is
      select break_pack_ind
        from wh
       where wh = I_wh;

BEGIN
   open C_GET_BREAK_PACK_IND;
   fetch C_GET_BREAK_PACK_IND into O_break_pack_ind;
   close C_GET_BREAK_PACK_IND;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.GET_BREAK_PACK_IND',
                                             to_char(SQLCODE));
      return FALSE;
END GET_BREAK_PACK_IND;
-------------------------------------------------------------------
FUNCTION GET_STORE_NEED(O_error_message         IN OUT   VARCHAR2,
                        O_total_store_need      IN OUT   NUMBER,
                        O_total_aso             IN OUT   REPL_RESULTS.ACCEPTED_STOCK_OUT%TYPE,
                        O_total_eso             IN OUT   REPL_RESULTS.ESTIMATED_STOCK_OUT%TYPE,
                        O_due_ind               IN OUT   REPL_RESULTS.DUE_IND%TYPE,
                        I_results_rec           IN       REPL_RESULTS_RECTYPE,
                        I_supplier              IN       ORDHEAD.SUPPLIER%TYPE,
                        I_ord_pack_size         IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                        I_inner_case            IN       ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                        I_due_ord_process_ind   IN       ORD_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE,
                        I_due_ord_serv_basis    IN       ORD_INV_MGMT.DUE_ORD_SERV_BASIS%TYPE,
                        I_unit_cost             IN       ORDLOC.UNIT_COST%TYPE,
                        I_unit_retail           IN       ORDLOC.UNIT_RETAIL%TYPE,
                        I_domain_id             IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_new_sourced_repls_rec   NEW_REPLS_RECTYPE;
   L_vdate                   PERIOD.VDATE%TYPE;
   L_break_pack_ind          WH.BREAK_PACK_IND%TYPE;
   L_new_store_roq           NUMBER;
   L_store_need              NUMBER;
   L_loc_type                REPL_RESULTS.LOC_TYPE%TYPE := 'S';
   L_standard_uom            UOM_CLASS.UOM%TYPE;
   L_standard_class          UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor             ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_sourced_results_rec     REPL_RESULTS_RECTYPE;

   -- Some parameters will come from the store level
   -- while others will be defaulted from the source wh.
   cursor C_SOURCED_STORES is
      select I_results_rec.recalc_type,
             I_results_rec.recalc_qty,
             I_results_rec.alloc_no,
             I_results_rec.orig_roq,
             I_results_rec.item,
             I_results_rec.item_type,
             I_results_rec.origin_country_id,
             ril.location,
             L_loc_type, -- loc type: always a store
             I_results_rec.stock_cat,
             I_results_rec.source_wh,
             I_results_rec.repl_method,
             ril.pres_stock,
             ril.demo_stock,
             ril.min_stock,
             ril.max_stock,
             ril.incr_pct,
             I_results_rec.min_supply_days,
             I_results_rec.max_supply_days,
             I_results_rec.time_supply_horizon,
             I_results_rec.inv_selling_days,
             I_results_rec.service_level,
             I_results_rec.lost_sales_factor,
             I_results_rec.supp_lead_time,
             I_results_rec.pickup_lead_time,
             I_results_rec.wh_lead_time,
             I_results_rec.next_order_lead_time,
             I_results_rec.review_time,
             ril.terminal_stock_qty,
             ril.season_id,
             ril.phase_id,
             I_results_rec.non_scaling_ind,
             ril.store_ord_mult,
             I_results_rec.repl_date
        from repl_item_loc ril,
             store s
       where ril.item = I_results_rec.item
         and ril.stock_cat = 'W'
         and ril.source_wh = I_results_rec.location
---
-- Ensure stock will arrive at an open store
---
         and s.store = ril.location
         and (L_vdate <=
                (NVL(s.store_close_date, L_vdate) -
                 NVL(s.stop_order_days, 0))
                     OR s.store_close_date IS NULL)
         and ((s.store_open_date - s.start_order_days) <= L_vdate)
---
-- Make sure the item is on active replenishment
---
         and (ril.activate_date <= L_vdate
         and NVL(ril.deactivate_date, (L_vdate + 1)) > L_vdate);

BEGIN
   if GET_BREAK_PACK_IND(O_error_message,
                         L_break_pack_ind,
                         I_results_rec.location) = FALSE  then
      return FALSE;
   end if;
   ---
   L_vdate := GET_VDATE;
   ---
   open C_SOURCED_STORES;
   ---
   LOOP
      fetch C_SOURCED_STORES into L_sourced_results_rec;
      ---
      EXIT WHEN C_SOURCED_STORES%NOTFOUND;
      ---
      if GET_NEW_RAW_ROQ(O_error_message,
                         L_new_sourced_repls_rec,
                         L_sourced_results_rec,
                         L_store_need,
                         I_due_ord_process_ind,
                         I_due_ord_serv_basis,
                         I_unit_cost,
                         I_unit_retail,
                         I_domain_id) = FALSE then
         return FALSE;
      end if;
      ---
      -- If due order processing is OFF and the location is NOT due
      -- zero out the order quantity so it is not included in wh total
      ---
      if I_due_ord_process_ind ='N' then
         if L_new_sourced_repls_rec.due_ind = 'N' then
            L_new_sourced_repls_rec.new_raw_roq := 0;
         end if;
      end if;
      ---
      -- If the roq is greater than 0, it should be rounded to the store ord mult.
      ---
      if (L_new_sourced_repls_rec.new_raw_roq > 0) then
         ---
         --*********************************************************************************
         -- If the break pack ind is no, the roq will be rounded to the order case size.   *
         -- If the break pack ind is yes and the store order multiple is not eaches,       *
         -- the roq will be rounded to inner case or the order case size depending on the  *
         -- value of the store order multiple.                                             *
         --*********************************************************************************
         if (L_break_pack_ind = 'N') or
            (L_sourced_results_rec.store_ord_mult != 'E' and L_break_pack_ind = 'Y') then
            ---
            if ROUNDING_SQL.TO_INNER_CASE(O_error_message,
                                          L_new_store_roq,
                                          I_results_rec.item,
                                          I_supplier,
                                          I_results_rec.origin_country_id,
                                          L_sourced_results_rec.location,
                                          L_new_sourced_repls_rec.new_raw_roq,
                                          I_ord_pack_size,
                                          I_inner_case,
                                          L_sourced_results_rec.store_ord_mult,
                                          L_break_pack_ind) = FALSE then
               return FALSE;
            end if;
         ---
         -- If the store order multiple is eaches and the break pack ind is Y,
         -- the roq will be rounded up to the nearest integer.
         elsif (L_sourced_results_rec.store_ord_mult = 'E' and L_break_pack_ind = 'Y') then
            if ROUND_EACHES(O_error_message,
                            L_new_sourced_repls_rec.new_raw_roq) = FALSE then
               return FALSE;
            end if;
            ---
            L_new_store_roq := L_new_sourced_repls_rec.new_raw_roq;
         end if;
      elsif (L_new_sourced_repls_rec.new_raw_roq <= 0) or (L_new_sourced_repls_rec.new_raw_roq is NULL) then
         L_new_store_roq := 0;
      end if;
      ---
      -- If due order processing in ON and the method is NOT dynamic, warehouse will be
      -- due if one of the stores is due.  If the method IS dynamic, the warehouse due
      -- flag will be set by evaluating the summed up aso/eso.  If due ordering is NOT on
      -- the warehouse will be due if any of the locations are due.
      ---
      if I_due_ord_process_ind ='Y' then
         if I_results_rec.repl_method != 'D' then
            if O_due_ind != 'Y' then
               if L_new_sourced_repls_rec.due_ind = 'Y' then
                  O_due_ind := 'Y';
               end if;
            end if;
         end if;
      else
         if O_due_ind != 'Y' then
            if L_new_sourced_repls_rec.due_ind = 'Y' then
               O_due_ind := 'Y';
            end if;
         end if;
      end if;
      ---
      -- Running totals.
      ---
      O_total_store_need := NVL(O_total_store_need, 0) + L_new_store_roq;
      O_total_aso        := NVL(O_total_aso, 0) + NVL(L_new_sourced_repls_rec.aso, 0);
      O_total_eso        := NVL(O_total_eso, 0) + NVL(L_new_sourced_repls_rec.eso, 0);
   END LOOP;
   ---
   close C_SOURCED_STORES;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_RECALC_SQL.GET_STORE_NEED',
                                            to_char(SQLCODE));
      return FALSE;
END GET_STORE_NEED;
-------------------------------------------------------------------
FUNCTION GET_WH_NEW_RAW_ROQ(O_error_message         IN OUT   VARCHAR2,
                            O_wh_new_repls_rec      IN OUT   NEW_REPLS_RECTYPE,
                            I_results_rec           IN       REPL_RESULTS_RECTYPE,
                            I_supplier              IN       ORDHEAD.SUPPLIER%TYPE,
                            I_ord_pack_size         IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                            I_inner_case            IN       ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                            I_due_ord_process_ind   IN       ORD_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE,
                            I_due_ord_serv_basis    IN       ORD_INV_MGMT.DUE_ORD_SERV_BASIS%TYPE,
                            I_unit_cost             IN       ORDLOC.UNIT_COST%TYPE,
                            I_unit_retail           IN       ORDLOC.UNIT_RETAIL%TYPE,
                            I_domain_id             IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_total_store_need   NUMBER;
   L_total_aso          REPL_RESULTS.ACCEPTED_STOCK_OUT%TYPE;
   L_total_eso          REPL_RESULTS.ESTIMATED_STOCK_OUT%TYPE;
   L_single_due_ind     REPL_RESULTS.DUE_IND%TYPE  := 'N';

BEGIN
   if GET_STORE_NEED(O_error_message,
                     L_total_store_need,
                     L_total_aso,
                     L_total_eso,
                     L_single_due_ind,
                     I_results_rec,
                     I_supplier,
                     I_ord_pack_size,
                     I_inner_case,
                     I_due_ord_process_ind,
                     I_due_ord_serv_basis,
                     I_unit_cost,
                     I_unit_retail,
                     I_domain_id) = FALSE then
      return FALSE;
   end if;
   ---
   if GET_NEW_RAW_ROQ(O_error_message,
                      O_wh_new_repls_rec,
                      I_results_rec,
                      L_total_store_need,
                      I_due_ord_process_ind,
                      I_due_ord_serv_basis,
                      I_unit_cost,
                      I_unit_retail,
                      I_domain_id) = FALSE then
      return FALSE;
   end if;
   ---
   -- If due order processing is ON, and the method is dynamic, summed up aso/eso
   -- determine the due flag.  If method is NOT dynamic, if one store is due, the
   -- warehouse will be due.  With due order processing OFF, if one location is due
   -- the warehouse will be due.
   ---
   if I_due_ord_process_ind = 'Y' then
      if I_results_rec.repl_method in ('D', 'DI') then
         if L_total_eso > L_total_aso then
            O_wh_new_repls_rec.due_ind := 'Y';
         else
            O_wh_new_repls_rec.due_ind := 'N';
         end if;
      else
         O_wh_new_repls_rec.due_ind := L_single_due_ind;
      end if;
   else
      if L_single_due_ind = 'Y' then
         O_wh_new_repls_rec.due_ind := 'Y';
      else
         O_wh_new_repls_rec.due_ind := 'N';
      end if;
   end if;
   ---
   O_wh_new_repls_rec.aso := L_total_aso;
   O_wh_new_repls_rec.eso := L_total_eso;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_RECALC_SQL.GET_WH_NEW_RAW_ROQ',
                                            to_char(SQLCODE));
      return FALSE;
END GET_WH_NEW_RAW_ROQ;
-------------------------------------------------------------------
FUNCTION TOTAL_ALLOC_QTYS(O_error_message             IN OUT VARCHAR2,
                          O_alloc_detail_rec_exists   IN OUT VARCHAR2,
                          O_orig_alloc_qty            IN OUT NUMBER,
                          IO_deleted_alloc_total      IN OUT NUMBER,
                          IO_inserted_alloc_total     IN OUT NUMBER,
                          I_new_roq                   IN     NUMBER,
                          I_alloc_no                  IN     ALLOC_DETAIL.ALLOC_NO%TYPE,
                          I_store                     IN     ALLOC_DETAIL.TO_LOC%TYPE)
   RETURN BOOLEAN IS

   L_new_roq   NUMBER := I_new_roq;

   cursor C_GET_ORIG_ALLOC is
      select qty_allocated
        from alloc_detail
       where alloc_no = I_alloc_no
         and to_loc   = I_store;

BEGIN
   -- This may not always be populated, therefore it should be initialized.
   O_orig_alloc_qty := NULL;
   ---
   --*******************************************************************************************
   -- This function keeps a running total of the alloc detail qty's originally on alloc detail *
   -- and the new alloc detail qtys.                                                           *
   --*******************************************************************************************
   ---
   open C_GET_ORIG_ALLOC;
   fetch C_GET_ORIG_ALLOC into O_orig_alloc_qty;
   ---
   if C_GET_ORIG_ALLOC%NOTFOUND then
      O_alloc_detail_rec_exists := 'N';
   else
      O_alloc_detail_rec_exists := 'Y';
   end if;
   ---
   close C_GET_ORIG_ALLOC;
   ---
   if (O_orig_alloc_qty < 0) or (O_orig_alloc_qty is NULL) then
      O_orig_alloc_qty := 0;
   end if;
   ---
   if L_new_roq < 0 then
      L_new_roq := 0;
   end if;
   ---
   IO_deleted_alloc_total   := nvl(IO_deleted_alloc_total, 0)  + O_orig_alloc_qty;
   IO_inserted_alloc_total  := nvl(IO_inserted_alloc_total, 0) + L_new_roq;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.TOTAL_ALLOC_QTYS',
                                             to_char(SQLCODE));
      return FALSE;
END TOTAL_ALLOC_QTYS;
-------------------------------------------------------------------
FUNCTION ALLOC_HEADER_EXISTS(O_error_message             IN OUT VARCHAR2,
                             O_alloc_header_rec_exists   IN OUT VARCHAR2,
                             O_alloc_no                  IN OUT ALLOC_HEADER.ALLOC_NO%TYPE,
                             I_order_no                  IN     ORDHEAD.ORDER_NO%TYPE,
                             I_source_wh                 IN     ALLOC_HEADER.WH%TYPE,
                             I_item                      IN     ORDSKU.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_order_no        ORDHEAD.ORDER_NO%TYPE := I_order_no;
   L_source_wh       ALLOC_HEADER.WH%TYPE  := I_source_wh;

   cursor C_GET_ALLOC is
      select alloc_no
        from alloc_header
       where order_no = L_order_no
         and wh       = L_source_wh
         and item     = I_item
         and status  in ('w','A');

BEGIN
   ---
   -- Need to find the existence of any allocations that
   -- are not in 'Closed' status.
   ---
   open C_GET_ALLOC;
   fetch C_GET_ALLOC into O_alloc_no;
   ---
   if C_GET_ALLOC%NOTFOUND then
      O_alloc_header_rec_exists := 'N';
   else
      O_alloc_header_rec_exists := 'Y';
   end if;
   ---
   close C_GET_ALLOC;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.ALLOC_HEADER_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END ALLOC_HEADER_EXISTS;
-------------------------------------------------------------------
FUNCTION INSERT_ALLOC_HEADER(O_error_message   IN OUT VARCHAR2,
                             O_alloc_no        IN OUT ALLOC_HEADER.ALLOC_NO%TYPE,
                             I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                             I_wh              IN     ALLOC_HEADER.WH%TYPE,
                             I_item            IN     ALLOC_HEADER.ITEM%TYPE,
                             I_alloc_method    IN     SYSTEM_OPTIONS.ALLOC_METHOD%TYPE)
   RETURN BOOLEAN IS

   L_return_code VARCHAR2(5);

BEGIN
   NEXT_ALLOC_NO(O_alloc_no,
                 L_return_code,
                 O_error_message);
   ---
   if L_return_code = 'FALSE' then
      return FALSE;
   end if;
   ---
   insert into alloc_header(alloc_no,
                            order_no,
                            wh,
                            item,
                            status,
                            alloc_desc,
                            po_type,
                            alloc_method,
                            order_type,
                            origin_ind)
                     values(O_alloc_no,
                            I_order_no,
                            I_wh,
                            I_item,
                            'A',
                            'PO #'||to_char(I_order_no),
                            NULL,
                            I_alloc_method,
                            'PREDIST',
                            'RMS');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.INSERT_ALLOC_HEADER',
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_ALLOC_HEADER;
-------------------------------------------------------------------
FUNCTION INSERT_ALLOC_DETAIL(O_error_message   IN OUT VARCHAR2,
                             I_alloc_no        IN     ALLOC_DETAIL.ALLOC_NO%TYPE,
                             I_store           IN     ALLOC_DETAIL.TO_LOC%TYPE,
                             I_qty_alloced     IN     ALLOC_DETAIL.QTY_ALLOCATED%TYPE,
                             I_non_scaling_ind IN     REPL_RESULTS.NON_SCALING_IND%TYPE)
   RETURN BOOLEAN IS

BEGIN
   insert into alloc_detail(alloc_no,
                            to_loc,
                            to_loc_type,
                            qty_transferred,
                            qty_allocated,
                            qty_prescaled,
                            qty_distro,
                            qty_selected,
                            qty_cancelled,
                            qty_received,
                            po_rcvd_qty,
                            non_scale_ind)
                     values(I_alloc_no,
                            I_store,
                            'S',      -- replenishment never uses wh's for crossdock
                            NULL,
                            I_qty_alloced,
                            I_qty_alloced,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            I_non_scaling_ind);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.INSERT_ALLOC_DETAIL',
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_ALLOC_DETAIL;
-------------------------------------------------------------------
FUNCTION UPDATE_ALLOC_DETAIL(O_error_message     IN OUT VARCHAR2,
                             I_alloc_no          IN     ALLOC_DETAIL.ALLOC_NO%TYPE,
                             I_store             IN     ALLOC_DETAIL.TO_LOC%TYPE,
                             I_qty_alloced       IN     ALLOC_DETAIL.QTY_ALLOCATED%TYPE,
                             I_non_scaling_ind   IN     REPL_RESULTS.NON_SCALING_IND%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_REC is
      select 'x'
        from alloc_detail
       where alloc_no = I_alloc_no
         and to_loc   = I_store
         for update nowait;

BEGIN
   open C_LOCK_REC;
   close C_LOCK_REC;
   ---
   update alloc_detail
      set qty_allocated = I_qty_alloced,
          qty_prescaled = I_qty_alloced,
          non_scale_ind = I_non_scaling_ind
    where alloc_no = I_alloc_no
      and to_loc   = I_store;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ALLOC_DETAIL_REC_LOCK',
                                            to_char(I_alloc_no),
                                            to_char(I_store),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.UPDATE_ALLOC_DETAIL',
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_ALLOC_DETAIL;
-------------------------------------------------------------------
FUNCTION DELETE_ALLOC_DETAIL(O_error_message   IN OUT VARCHAR2,
                             I_alloc_no        IN     ALLOC_DETAIL.ALLOC_NO%TYPE,
                             I_store           IN     ALLOC_DETAIL.TO_LOC%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_table         VARCHAR2(40);

   cursor C_LOCK_CHRG is
      select 'x'
        from alloc_chrg
       where alloc_no = I_alloc_no
         and to_loc   = I_store
      for update nowait;

   cursor C_LOCK_REC is
      select 'x'
        from alloc_detail
       where alloc_no = I_alloc_no
         and to_loc   = I_store
      for update nowait;

BEGIN
   L_table := 'ALLOC_CHRG';
   ---
   open C_LOCK_CHRG;
   close C_LOCK_CHRG;
   ---
   delete from alloc_chrg
      where alloc_no = I_alloc_no
        and to_loc   = I_store;
   ---
   L_table := 'ALLOC_DETAIL';
   ---
   open C_LOCK_REC;
   close C_LOCK_REC;
   ---
   delete from alloc_detail
      where alloc_no = I_alloc_no
        and to_loc   = I_store;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_alloc_no),
                                            to_char(I_store));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.DELETE_ALLOC_DETAIL',
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_ALLOC_DETAIL;
-------------------------------------------------------------------
FUNCTION XDOCK_ROQ_ORD_MAINTENANCE(O_error_message             IN OUT VARCHAR2,
                                   IO_ordsku_rec_exists        IN OUT VARCHAR2,
                                   IO_ordloc_rec_exists        IN OUT VARCHAR2,
                                   IO_alloc_header_rec_exists  IN OUT VARCHAR2,
                                   IO_alloc_no                 IN OUT ALLOC_HEADER.ALLOC_NO%TYPE,
                                   IO_alloc_detail_rec_exists  IN OUT VARCHAR2,
                                   I_prev_alloc_no             IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                                   I_order_no                  IN     ORDHEAD.ORDER_NO%TYPE,
                                   I_item                      IN     ORDSKU.ITEM%TYPE,
                                   I_store                     IN     ALLOC_DETAIL.TO_LOC%TYPE,
                                   I_new_roq                   IN     NUMBER,
                                   I_orig_alloc_qty            IN     NUMBER,
                                   I_source_wh                 IN     ALLOC_HEADER.WH%TYPE,
                                   I_supp_currency             IN     CURRENCIES.CURRENCY_CODE%TYPE,
                                   I_unit_cost                 IN     ORDLOC.UNIT_COST%TYPE,
                                   I_unit_retail               IN     ORDLOC.UNIT_RETAIL%TYPE,
                                   I_orig_cntry                IN     ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                   I_base_cntry_id             IN     SYSTEM_OPTIONS.BASE_COUNTRY_ID%TYPE,
                                   I_ord_pack_size             IN     ORDSKU.SUPP_PACK_SIZE%TYPE,
                                   I_elc_ind                   IN     SYSTEM_OPTIONS.ELC_IND%TYPE,
                                   I_non_scaling_ind           IN     REPL_RESULTS.NON_SCALING_IND%TYPE,
                                   I_alloc_method              IN     SYSTEM_OPTIONS.ALLOC_METHOD%TYPE,
                                   I_ordhead_info_rec          IN     ORDHEAD_INFO_RECTYPE)
   RETURN BOOLEAN IS

   L_ordloc_rec_exists         VARCHAR2(1)                := IO_ordloc_rec_exists;
   L_ordsku_rec_exists         VARCHAR2(1)                := IO_ordsku_rec_exists;
   L_alloc_header_rec_exists   VARCHAR2(1)                := IO_alloc_header_rec_exists;
   L_alloc_detail_rec_exists   VARCHAR2(1)                := IO_alloc_detail_rec_exists;
   L_alloc_no                  ALLOC_HEADER.ALLOC_NO%TYPE := IO_alloc_no;
   L_unit_retail               ORDLOC.UNIT_RETAIL%TYPE    := I_unit_retail;
   L_ordloc_insert_qty         NUMBER;
   L_loc_type                  ORDLOC.LOC_TYPE%TYPE;
   L_alloc_detail_updated      VARCHAR2(1);

BEGIN
   if I_new_roq > 0 then
      if L_ordloc_rec_exists is NULL then
         ---
         -- If there is no ordsku record, then we know there is
         -- no ordloc record and the existence of an ordloc record
         -- does not need to be determined.
         ---
         if L_ordsku_rec_exists != 'N' then
            if ORDLOC_EXISTS(O_error_message,
                             L_ordloc_rec_exists,
                             I_order_no,
                             I_item,
                             I_source_wh) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      if L_ordsku_rec_exists = 'N' then
         if CREATE_ORDSKU(O_error_message,
                          I_order_no,
                          I_item,
                          I_ordhead_info_rec,
                          I_orig_cntry,
                          I_base_cntry_id,
                          I_ord_pack_size,
                          I_elc_ind) = FALSE then
            return FALSE;
         end if;
         ---
         L_ordsku_rec_exists := 'Y';
         L_ordloc_rec_exists := 'N';
      end if;
      ---
      if L_ordloc_rec_exists = 'N' then
         L_loc_type := 'W';
         ---
         if L_unit_retail is NULL then
            if GET_UNIT_RETAIL(O_error_message,
                               L_unit_retail,
                               I_item,
                               I_source_wh,
                               L_loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         L_ordloc_insert_qty := 0;
         ---
         if INSERT_ORDLOC(O_error_message,
                          I_order_no,
                          I_item,
                          I_source_wh,
                          L_loc_type,
                          L_unit_retail,
                          L_ordloc_insert_qty,
                          I_unit_cost,
                          I_ordhead_info_rec.currency_code,
                          I_supp_currency,
                          I_ordhead_info_rec.exchange_rate,
                          I_non_scaling_ind) = FALSE then
            return FALSE;
         end if;
         ---
         L_ordloc_rec_exists := 'Y';
         ---
         if I_elc_ind = 'Y' then
            if ORDER_EXPENSE_SQL.INSERT_COST_COMP(O_error_message,
                                                  I_order_no,
                                                  I_item,
                    NULL,
                                                  I_source_wh,
                                                  L_loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      if L_alloc_no is NULL then
         if L_alloc_header_rec_exists = 'Y' then
            L_alloc_no := I_prev_alloc_no;
         elsif L_alloc_header_rec_exists is NULL then
            if ALLOC_HEADER_EXISTS(O_error_message,
                                   L_alloc_header_rec_exists,
                                   L_alloc_no,
                                   I_order_no,
                                   I_source_wh,
                                   I_item) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_alloc_header_rec_exists = 'N' then
            if INSERT_ALLOC_HEADER(O_error_message,
                                   L_alloc_no,
                                   I_order_no,
                                   I_source_wh,
                                   I_item,
                                   I_alloc_method) = FALSE then
               return FALSE;
            end if;
            ---
            L_alloc_header_rec_exists := 'Y';
         end if;
      end if;
      ---
      if L_alloc_detail_rec_exists = 'N' then
         if INSERT_ALLOC_DETAIL(O_error_message,
                                L_alloc_no,
                                I_store,
                                I_new_roq,
                                I_non_scaling_ind) = FALSE then
            return FALSE;
         end if;
         ---
         if ALLOC_CHARGE_SQL.DEFAULT_CHRGS(O_error_message,
                                           L_alloc_no,
                                           I_source_wh,
                                           I_store,
                                           'S',
                                           I_item) = FALSE then
            return FALSE;
         end if;
         ---
         L_alloc_detail_rec_exists := 'Y';
      elsif L_alloc_detail_rec_exists = 'Y' then
         if UPDATE_ALLOC_DETAIL(O_error_message,
                                L_alloc_no,
                                I_store,
                                I_new_roq,
                                I_non_scaling_ind) = FALSE then
            return FALSE;
         end if;
      end if;
   elsif (I_new_roq <= 0) and (L_alloc_detail_rec_exists = 'Y') then
      if DELETE_ALLOC_DETAIL(O_error_message,
                             L_alloc_no,
                             I_store) = FALSE then
         return FALSE;
      end if;
      ---
      L_alloc_detail_rec_exists := 'N';
   end if;
   ---
   IO_alloc_no                := L_alloc_no;
   IO_ordsku_rec_exists       := L_ordsku_rec_exists;
   IO_ordloc_rec_exists       := L_ordloc_rec_exists;
   IO_alloc_header_rec_exists := L_alloc_header_rec_exists;
   IO_alloc_detail_rec_exists := L_alloc_detail_rec_exists;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.XDOCK_ROQ_ORD_MAINTENANCE',
                                             to_char(SQLCODE));
      return FALSE;
END XDOCK_ROQ_ORD_MAINTENANCE;
-------------------------------------------------------------------
FUNCTION GET_ORIG_AND_NEW_ALLOC_TOTALS(O_error_message          IN OUT VARCHAR2,
                                       O_total_orig_alloc_qty   IN OUT NUMBER,
                                       O_total_new_alloc_qty    IN OUT NUMBER,
                                       I_order_no               IN     ORDHEAD.ORDER_NO%TYPE,
                                       I_alloc_no               IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                                       I_item                   IN     ORDSKU.ITEM%TYPE,
                                       I_supplier               IN     ORDHEAD.SUPPLIER%TYPE,
                                       I_orig_cntry             IN     ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                       I_ord_pack_size          IN     ORDSKU.SUPP_PACK_SIZE%TYPE,
                                       I_deleted_alloc_total    IN     NUMBER,
                                       I_inserted_alloc_total   IN     NUMBER)
   RETURN BOOLEAN IS

   L_total_new_alloc_qty   NUMBER := 0;
   L_total_orig_alloc_qty  NUMBER;

   cursor C_GET_NEW_TOTAL is
      select sum(qty_allocated)
        from alloc_detail
       where alloc_no = I_alloc_no;

BEGIN
   open C_GET_NEW_TOTAL;
   fetch C_GET_NEW_TOTAL into L_total_new_alloc_qty;
   close C_GET_NEW_TOTAL;
   ---
   L_total_orig_alloc_qty := (nvl(L_total_new_alloc_qty, 0) - nvl(I_inserted_alloc_total,0)) + nvl(I_deleted_alloc_total, 0);
   ---
   O_total_orig_alloc_qty := L_total_orig_alloc_qty;
   O_total_new_alloc_qty  := L_total_new_alloc_qty;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.GET_ORIG_AND_NEW_ALLOC_TOTALS',
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORIG_AND_NEW_ALLOC_TOTALS;
-------------------------------------------------------------------
FUNCTION DELETE_ALLOC_HEADER(O_error_message   IN OUT VARCHAR2,
                             I_alloc_no        IN     ALLOC_HEADER.ALLOC_NO%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_REC is
      select 'x'
        from alloc_header
       where alloc_no = I_alloc_no;

BEGIN
   open C_LOCK_REC;
   close C_LOCK_REC;
   ---
   delete from alloc_header
     where alloc_no = I_alloc_no;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ALLOC_HEADER_REC_LOCK_3',
                                            to_char(I_alloc_no),
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.DELETE_ALLOC_HEADER',
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_ALLOC_HEADER;
-------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC_FOR_ALLOCS(O_error_message         IN OUT VARCHAR2,
                                  IO_recalc_expenses      IN OUT VARCHAR2,
                                  I_total_orig_alloc_qty  IN OUT NUMBER,
                                  I_total_new_alloc_qty   IN OUT NUMBER,
                                  I_order_no              IN     ORDLOC.ORDER_NO%TYPE,
                                  I_alloc_no              IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                                  I_item                  IN     ORDLOC.ITEM%TYPE,
                                  I_source_wh             IN     ORDLOC.LOCATION%TYPE,
                                  I_supplier              IN     ORDHEAD.SUPPLIER%TYPE,
                                  I_ord_pack_size         IN     ORDSKU.SUPP_PACK_SIZE%TYPE,
                                  I_orig_cntry            IN     ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                  I_elc_ind               IN     SYSTEM_OPTIONS.ELC_IND%TYPE)
   RETURN BOOLEAN IS

   L_recalc_expenses      VARCHAR2(1) := IO_recalc_expenses;
   L_existing_ordloc_qty  NUMBER;
   L_update_qty           NUMBER;

BEGIN
   if FETCH_EXISTING_ORDLOC(O_error_message,
                            L_existing_ordloc_qty,
                            I_order_no,
                            I_item,
                            I_source_wh) = FALSE then
      return FALSE;
   end if;
   ---
   if CALC_ALLOC_ORDLOC_UPDATE(O_error_message,
                               L_update_qty,
                               nvl(L_existing_ordloc_qty, 0),
                               I_total_orig_alloc_qty,
                               I_total_new_alloc_qty) = FALSE then
      return FALSE;
   end if;
   ---
   if L_update_qty > 0 then
      if UPDATE_ORDLOC(O_error_message,
                       L_update_qty,
                       I_order_no,
                       I_item,
                       I_source_wh) = FALSE then
         return FALSE;
      end if;
   elsif L_update_qty <= 0 then
      ---
      -- This function will delete all records associated with the
      -- the ordloc record. We know there's an ordloc record because we always insert one
      -- in xdock_roq_order_maint.
      ---
      if ORDER_SETUP_SQL.DELETE_ORDER (O_error_message,
                                       I_order_no,
                                       I_item,
                                       I_source_wh,
                                       'W',
                                       'ordloc') = FALSE then
         return FALSE;
      end if;
      ---
      -- this function will actually delete the ordloc record.
      ---
      if DELETE_ORDLOC(O_error_message,
                       I_order_no,
                       I_item,
                       I_source_wh) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   -- if the ordloc record is still required but there are no more allocations,
   -- delete the allocation header record.
   ---
   if (L_update_qty > 0) and (nvl(I_total_new_alloc_qty, 0) <= 0) then
      if DELETE_ALLOC_HEADER(O_error_message,
                             I_alloc_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_elc_ind = 'Y' then
      L_recalc_expenses := 'Y';
   end if;
   ---
   IO_recalc_expenses  := L_recalc_expenses;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.UPDATE_ORDLOC_FOR_ALLOCS',
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_ORDLOC_FOR_ALLOCS;
-------------------------------------------------------------------
FUNCTION RECALC_ELC(O_error_message   IN OUT VARCHAR2,
                    I_item            IN     ORDSKU.ITEM%TYPE,
                    I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

BEGIN
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PA',
                             I_item,
                             NULL,
                             NULL,
                             NULL,
                             I_order_no,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL) = FALSE then
      return FALSE;
   end if;
   ---
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PE',
                             I_item,
                             NULL,
                             NULL,
                             NULL,
                             I_order_no,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.RECALC_ELC',
                                             to_char(SQLCODE));
      return FALSE;
END RECALC_ELC;
-------------------------------------------------------------------
FUNCTION ORDLOC_QTYS_EXIST(O_error_message       IN OUT VARCHAR2,
                           O_ordloc_qtys_exist   IN OUT VARCHAR2,
                           I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                           I_item                IN     ORDSKU.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_dummy           VARCHAR2(1);

   cursor C_ORDLOC_QTYS_EXIST is
      select 'x'
        from ordloc
       where order_no = I_order_no
         and item = I_item;

BEGIN
   open C_ORDLOC_QTYS_EXIST;
   fetch C_ORDLOC_QTYS_EXIST into L_dummy;
   ---
   if C_ORDLOC_QTYS_EXIST%NOTFOUND then
      O_ordloc_qtys_exist := 'N';
   else
      O_ordloc_qtys_exist := 'Y';
   end if;
   ---
   close C_ORDLOC_QTYS_EXIST;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.ORDLOC_QTYS_EXIST',
                                             to_char(SQLCODE));
      return FALSE;
END ORDLOC_QTYS_EXIST;
-------------------------------------------------------------------
FUNCTION DELETE_ORDSKU(O_error_message   IN OUT VARCHAR2,
                       I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                       I_item            IN     ORDSKU.ITEM%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_REC is
      select 'x'
        from ordsku
       where order_no = I_order_no
         and item     = I_item
         for update nowait;

BEGIN
   open C_LOCK_REC;
   close C_LOCK_REC;
   ---
   delete from ordsku
         where order_no = I_order_no
           and item     = I_item;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ORDSKU_REC_LOC',
                                            to_char(I_order_no),
                                            I_item,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.DELETE_ORDSKU',
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_ORDSKU;
-------------------------------------------------------------------
FUNCTION DELETE_ORDSKU_REC(O_error_message   IN OUT VARCHAR2,
                           I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                           I_item            IN     ORDSKU.ITEM%TYPE)
   RETURN BOOLEAN IS

BEGIN
   if ORDER_SETUP_SQL.DELETE_ORDER(O_error_message,
                                   I_order_no,
                                   I_item,
                                   NULL,
                                   NULL,
                                   'ordsku') = FALSE then
      return FALSE;
   end if;
   ---
   if DELETE_ORDSKU(O_error_message,
                    I_order_no,
                    I_item) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.DELETE_ORDSKU_REC',
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_ORDSKU_REC;
-------------------------------------------------------------------
FUNCTION CHECK_IMPORT_STATUS(O_error_message      IN OUT VARCHAR2,
                             I_orig_cntry_table   IN     ORIG_CNTRY_TABLETYPE,
                             I_import_cntry_id    IN     ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                             I_order_no           IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_import_ind       VARCHAR2(1);
   L_current_item     ITEM_MASTER.ITEM%TYPE;
   L_keep_looping     BOOLEAN := TRUE;

BEGIN
   --************************************************************************
   -- This function assumes that the current order is an import order.      *
   -- It is trying to determine if the order is no longer an import order.  *
   -- The function can be easily modified simply update the status when the *
   -- import status has changed. However, since that functionality is not   *
   -- neccesary at this time, the function will only update if the order    *
   -- is no longer an import order.                                         *
   --************************************************************************
   ---
   L_current_item := I_orig_cntry_table.FIRST;
   ---
   while L_keep_looping loop
      if I_orig_cntry_table(L_current_item) != I_import_cntry_id then
         L_import_ind := 'Y';
         L_keep_looping := FALSE;
      else
         L_import_ind := 'N';
      end if;
      ---
      L_current_item := I_orig_cntry_table.NEXT(L_current_item);
      ---
      if L_current_item is NULL then
         L_keep_looping := FALSE;
      end if;
   end loop;
   ---
   if L_import_ind = 'N' then
      if UPDATE_IMPORT_STATUS(O_error_message,
                              I_order_no,
                              L_import_ind) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.CHECK_IMPORT_STATUS',
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_IMPORT_STATUS;
-------------------------------------------------------------------
FUNCTION UPDATE_DUE_STATUS(O_error_message   IN OUT VARCHAR2,
                           I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                           I_due_ord_ind     IN     ORD_INV_MGMT.DUE_ORD_IND%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_REC is
      select 'x'
        from ord_inv_mgmt
       where order_no = I_order_no;

BEGIN
   open C_LOCK_REC;
   close C_LOCK_REC;
   ---
   update ord_inv_mgmt
      set due_ord_ind = I_due_ord_ind
    where order_no    = I_order_no;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ORD_INV_MGMT_REC_LOCK',
                                            I_order_no,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.UPDATE_DUE_STATUS',
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_DUE_STATUS;
-------------------------------------------------------------------
FUNCTION RECALC_REPLENISHMENT(O_error_message   IN OUT   VARCHAR2,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_ordhead_info_retrieved     VARCHAR2(1) := 'N';
   L_unit_cost                  ORDLOC.UNIT_COST%TYPE;
   L_ord_pack_size              ORDSKU.SUPP_PACK_SIZE%TYPE;
   L_unit_retail                ITEM_LOC.UNIT_RETAIL%TYPE;
   L_store_need                 NUMBER;
   L_ordsku_rec_exists          VARCHAR2(1) := NULL;
   L_new_raw_roq                NUMBER;
   L_new_order_roq              NUMBER;
   L_repl_results_roq           NUMBER;
   L_elc_ind                    SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_base_cntry_id              SYSTEM_OPTIONS.BASE_COUNTRY_ID%TYPE;
   L_supp_currency              CURRENCIES.CURRENCY_CODE%TYPE;
   L_recalc_expenses            VARCHAR2(1) := 'N';
   L_wh_new_order_roq           NUMBER;
   L_inner_case                 ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE;
   L_break_pack_ind             WH.BREAK_PACK_IND%TYPE;
   L_due_ord_process_ind        ORD_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE;
   L_orig_due_status            ORD_INV_MGMT.DUE_ORD_IND%TYPE;
   L_new_due_status             ORD_INV_MGMT.DUE_ORD_IND%TYPE;
   L_boolean_due_status         BOOLEAN;
   L_due_ord_serv_basis         ORD_INV_MGMT.DUE_ORD_SERV_BASIS%TYPE;
   L_dept                       ITEM_MASTER.DEPT%TYPE;
   L_class                      ITEM_MASTER.CLASS%TYPE;
   L_subclass                   ITEM_MASTER.SUBCLASS%TYPE;
   L_domain_id                  ITEM_FORECAST.DOMAIN_ID%TYPE;
   L_deleted_alloc_total        NUMBER;
   L_inserted_alloc_total       NUMBER;
   L_total_orig_alloc_qty       NUMBER;
   L_total_new_alloc_qty        NUMBER;
   L_prev_item                  ORDSKU.ITEM%TYPE := -1;
   L_prev_location              ORDLOC.LOCATION%TYPE := -1;
   L_prev_loc_type              ORDLOC.LOC_TYPE%TYPE := 'Z';
   L_prev_source_wh             REPL_RESULTS.SOURCE_WH%TYPE := -1;
   L_prev_stock_cat             REPL_RESULTS.STOCK_CAT%TYPE := 'Z';
   L_prev_ord_pack_size         ORDSKU.SUPP_PACK_SIZE%TYPE := -1;
   L_prev_orig_cntry            ORDSKU.ORIGIN_COUNTRY_ID%TYPE := 'Z';
   L_prev_ordsku_rec_exists     VARCHAR2(1) := 'Z';
   L_ordloc_rec_exists          VARCHAR2(1);
   L_alloc_header_rec_exists    VARCHAR2(1) := NULL;
   L_alloc_detail_rec_exists    VARCHAR2(1);
   L_orig_alloc_qty             NUMBER;
   L_alloc_method               SYSTEM_OPTIONS.ALLOC_METHOD%TYPE;
   L_update_ordloc_for_allocs   VARCHAR2(1);
   L_ordloc_qtys_exist          VARCHAR2(1);
   L_prev_alloc_no              ALLOC_HEADER.ALLOC_NO%TYPE;
   L_results_rec_alloc          ALLOC_HEADER.ALLOC_NO%TYPE;
   L_round_this_number          NUMBER;
   ---
   L_ordhead_info_rec           ORDHEAD_INFO_RECTYPE;
   L_results_rec                REPL_RESULTS_RECTYPE;
   L_new_repls_rec              NEW_REPLS_RECTYPE;
   L_wh_new_repls_rec           NEW_REPLS_RECTYPE;
   ---
   L_orig_cntry_table           ORIG_CNTRY_TABLETYPE;

   -- driving cursor
   cursor C_RESULTS is
      select recalc_type,
             recalc_qty,
             alloc_no,
             order_roq orig_roq,
             item,
             item_type,
             origin_country_id,
             location,
             loc_type,
             stock_cat,
             NVL(source_wh, -1), -- erase any lingering value.
             repl_method,
             NVL(pres_stock, NULL), -- erase any lingering value.
             NVL(demo_stock, NULL),
             NVL(min_stock, NULL),
             NVL(max_stock, NULL),
             NVL(incr_pct, NULL),
             NVL(min_supply_days, NULL),
             NVL(max_supply_days, NULL),
             NVL(time_supply_horizon, NULL),
             NVL(inv_selling_days, NULL),
             NVL(service_level, NULL),
             NVL(lost_sales_factor, NULL),
             NVL(supp_lead_time, NULL),
             NVL(pickup_lead_time, NULL),
             NVL(wh_lead_time, NULL),
             NVL(next_order_lead_time, NULL),
             NVL(review_time, NULL),
             NVL(terminal_stock_qty, NULL),
             NVL(season_id, NULL),
             NVL(phase_id, NULL),
             non_scaling_ind,
             store_ord_mult,
             repl_date
        from repl_results
       where order_no     = I_order_no
         and recalc_type != 'N'
       order by item,
                stock_cat,
                source_wh,
                repl_method;

BEGIN
   -- Lock the repl_results record prior to any processing.
   if LOCK_REPL_RESULTS(O_error_message,
                        I_order_no) = FALSE then
      return FALSE;
   end if;
   ---
   open C_RESULTS;
   ---
   LOOP
      fetch C_RESULTS into L_results_rec;
      ---
      EXIT WHEN C_RESULTS%NOTFOUND;
      ---
      -- Logic for updating ordloc for any recalced crossdock stores
      -- from the previous item/source wh.
      ---
      if L_update_ordloc_for_allocs = 'Y' then
         -- Since we know the prev stock cat was C, if changed, then update needed.
         if (L_results_rec.stock_cat != 'C') or
            (L_prev_item != L_results_rec.item) or
            (L_prev_source_wh != L_results_rec.source_wh) then

            if GET_ORIG_AND_NEW_ALLOC_TOTALS(O_error_message,
                                             L_total_orig_alloc_qty,
                                             L_total_new_alloc_qty,
                                             I_order_no,
                                             L_prev_alloc_no,
                                             L_prev_item,
                                             L_ordhead_info_rec.supplier,
                                             L_prev_orig_cntry,
                                             L_prev_ord_pack_size,
                                             L_deleted_alloc_total,
                                             L_inserted_alloc_total) = FALSE then
               return FALSE;
            end if;
            ---
            if UPDATE_ORDLOC_FOR_ALLOCS(O_error_message,
                                        L_recalc_expenses,
                                        L_total_orig_alloc_qty,
                                        L_total_new_alloc_qty,
                                        I_order_no,
                                        L_prev_alloc_no,
                                        L_prev_item,
                                        L_prev_source_wh,
                                        L_ordhead_info_rec.supplier,
                                        L_prev_ord_pack_size,
                                        L_prev_orig_cntry,
                                        L_elc_ind) = FALSE then
               return FALSE;
            end if;
            ---
            -- If the prev alloc is the same as the current alloc,
            -- no new alloc was fetched. Therefore, the current alloc
            -- should be set to NULL so the prev alloc is not erroneously used.
            ---
            if L_prev_alloc_no = L_results_rec.alloc_no then
               L_results_rec.alloc_no := NULL;
            end if;
            ---
            L_update_ordloc_for_allocs := 'N';
            L_ordloc_rec_exists        := NULL;
            L_deleted_alloc_total      := NULL;
            L_inserted_alloc_total     := NULL;
            L_alloc_header_rec_exists  := NULL;
         end if;
      end if;
      ---
      -- Perform item level processing for the previous sku
      ---
      if L_prev_item != L_results_rec.item then
         ---
         -- Logic for recalcing elc expenses for all locations on the order
         -- for a sku.
         ---
         if (L_recalc_expenses = 'Y') and (L_elc_ind = 'Y') then
            if RECALC_ELC(O_error_message,
                          L_prev_item,
                          I_order_no) = FALSE then
               return FALSE;
            end if;
            ---
            L_recalc_expenses := 'N';
         end if;
         ---
         -- Determine if an existing ordsku record is no longer needed.
         -- If not, delete it.
         ---
         if L_prev_ordsku_rec_exists = 'Y' then
            if ORDLOC_QTYS_EXIST(O_error_message,
                                 L_ordloc_qtys_exist,
                                 I_order_no,
                                 L_prev_item) = FALSE then
               return FALSE;
            end if;
            ---
            if L_ordloc_qtys_exist = 'N' then
               if DELETE_ORDSKU_REC(O_error_message,
                                    I_order_no,
                                    L_prev_item) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
         ---
         if L_ordhead_info_rec.import_country_id = 'Y' then
           --*********************************************************************
           -- Populate the origin country id table with the origin country of    *
           -- the last sku. This table will be indexed by the item number.       *
           -- All origin countries are needed to determine whether an import     *
           -- order will remain an import order once replenishment recalculation *
           -- has taken place.                                                   *
           --*********************************************************************
            L_orig_cntry_table(L_prev_item) := L_prev_orig_cntry;
         end if;
         ---
         -- Null out values stored at the item level
         ---
         L_domain_id  := NULL;
         L_inner_case := NULL;
      end if; -- end previous item processing if the item has changed.
      ---
      if L_prev_source_wh != L_results_rec.source_wh then
         L_break_pack_ind := NULL;
      end if;

      --*************************************************************
      -- These replenishment calculated results will not be         *
      -- recalculated if the recalc type is 'Q'uantity because      *
      -- they will not be a factor in the recalculation. Therefore, *
      -- they are initialized to NULL each time through the loop to *
      -- ensure that NULL is put on the repl_results table if the   *
      -- recalc type is 'Q'.                                        *
      --*************************************************************
      if INITIALIZE_NEW_REPLS_RECS(O_error_message,
                                   L_new_repls_rec,
                                   L_wh_new_repls_rec) = FALSE then
         return FALSE;
      end if;
      ---
      -- Get System Level Information
      ---
      if L_elc_ind is NULL then
         if GET_SYSTEM_OPTIONS_INFO(O_error_message,
                                    L_elc_ind,
                                    L_alloc_method,
                                    L_base_cntry_id) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- Get Order Level Information
      ---
      if L_ordhead_info_retrieved != 'Y' then
         -- Get needed data from ordhead.
         if GET_ORDHEAD_INFO(O_error_message,
                             L_ordhead_info_rec,
                             I_order_no) = FALSE then
            return FALSE;
         end if;
         ---
         L_ordhead_info_retrieved := 'Y';
      end if;
      ---
      if L_supp_currency is NULL then
         if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                              L_supp_currency,
                                              L_ordhead_info_rec.supplier) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if L_due_ord_process_ind is NULL then
         if GET_ORD_INV_MGMT_INFO(O_error_message,
                                  L_due_ord_process_ind,
                                  L_orig_due_status,
                                  L_due_ord_serv_basis,
                                  I_order_no) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- Get Item Level Information
      ---
      if L_prev_item != L_results_rec.item then
         -- Retrieve information needed for order maintenance.
         -- Also, this function will determine if an ordsku record exists for
         -- the current item.
         if GET_COST_AND_PACKSIZE(O_error_message,
                                  L_unit_cost,
                                  L_ord_pack_size,
                                  L_ordsku_rec_exists,
                                  I_order_no,
                                  L_results_rec.item,
                                  L_ordhead_info_rec.supplier,
                                  L_results_rec.origin_country_id,
                                  L_results_rec.location) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- Get the domain id if needed and if not already retrieved
      ---
      if L_domain_id is NULL then
         -- Repl methods of Time Supply, Dynamic, Time Supply - Issues,
         -- and Dynamic - Issues require a domain id to generate an roq.
         -- If the recalc type is qty, then an roq does not need to be generated.
         if (L_results_rec.repl_method = 'TI' or
             L_results_rec.repl_method = 'DI' or
             L_results_rec.repl_method = 'T' or
             L_results_rec.repl_method = 'D') and (L_results_rec.recalc_type != 'Q') then

            if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                              L_results_rec.item,
                                              L_dept,
                                              L_class,
                                              L_subclass) = FALSE then
               return FALSE;
            end if;
            ---
            if FORECASTS_SQL.GET_DOMAIN(O_error_message,
                                        L_dept,
                                        L_class,
                                        L_subclass,
                                        L_domain_id) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      -- Get Item/Loc Level info
      ---
      if (L_prev_item != L_results_rec.item) or (L_prev_location != L_results_rec.location) then
         L_unit_retail := NULL;
         ---
         -- The retail is only needed to generate a new roq when the service basis
         -- is 'P'rofit.
         ---
         if L_due_ord_serv_basis = 'P' then
            if GET_UNIT_RETAIL(O_error_message,
                               L_unit_retail,
                               L_results_rec.item,
                               L_results_rec.location,
                               L_results_rec.loc_type) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      -- Recalc Logic
      ---

      --**************************************************************
      -- If this record is a store on replenishment with a stock     *
      -- category of Direct to Store or if it is a warehouse on      *
      -- replenishment with a replenishment method of Min/Max,       *
      -- Floating Point, Constant, Time Supply - Issues, or          *
      -- Dynamic - Issues then the following recalc logic should be  *
      -- performed                                                   *
      --**************************************************************
      if (L_results_rec.loc_type = 'S' and L_results_rec.stock_cat = 'D') or
         (L_results_rec.loc_type = 'W' and (L_results_rec.repl_method = 'M' or
                                            L_results_rec.repl_method = 'F' or
                                            L_results_rec.repl_method = 'C' or
                                            L_results_rec.repl_method = 'TI' or
                                            L_results_rec.repl_method = 'DI')) then

         --**********************************************************
         -- If the recalc type is A, the user wants to recalculate  *
         -- based on user specified replenishment attributes stored *
         -- on repl_results for the order/item/loc.                 *
         --**********************************************************
         if L_results_rec.recalc_type = 'A' then
            L_store_need := NULL;
            ---
            if GET_NEW_RAW_ROQ(O_error_message,
                               L_new_repls_rec,
                               L_results_rec,
                               L_store_need,
                               L_due_ord_process_ind,
                               L_due_ord_serv_basis,
                               L_unit_cost,
                               L_unit_retail,
                               L_domain_id) = FALSE then
               return FALSE;
            end if;
            ---
            --***********************************************************
            -- The user may specify an roq. In this case, no roq needs  *
            -- to be recalculated. The user specified roq will simply   *
            -- be rounded and used for scaling if scaling is indicated. *
            --***********************************************************
         elsif L_results_rec.recalc_type = 'Q' then
            L_new_repls_rec.new_raw_roq := nvl(L_results_rec.recalc_qty, 0);
         end if;
         ---
         L_new_order_roq := L_new_repls_rec.new_raw_roq;
         ---
         --************************************************************************
         -- If due order processing is off, then non due roqs should              *
         -- not be added to the order even if a positive roq was generated.       *
         -- However, The repl results table will still contain the roq generated. *
         --************************************************************************
         L_repl_results_roq := L_new_order_roq;
         ---
         -- if the user has specified an roq, the due indicator is ignored.
         ---
         if L_results_rec.recalc_type != 'Q' then
            if (L_due_ord_process_ind = 'N') and
               (L_new_repls_rec.due_ind = 'N') and
               (L_new_order_roq > 0) then
                  L_new_order_roq := 0;
            end if;
         end if;
         ---
         if NONXDOCK_ROQ_ORD_MAINTENANCE(O_error_message,
                                         L_recalc_expenses,
                                         L_ordsku_rec_exists,
                                         I_order_no,
                                         L_ordhead_info_rec,
                                         L_results_rec.item,
                                         L_supp_currency,
                                         L_results_rec.orig_roq,
                                         L_new_order_roq,
                                         L_new_repls_rec.new_raw_roq,
                                         L_results_rec.location,
                                         L_results_rec.loc_type,
                                         L_elc_ind,
                                         L_results_rec.origin_country_id,
                                         L_base_cntry_id,
                                         L_unit_cost,
                                         L_unit_retail,
                                         L_ord_pack_size,
                                         L_results_rec.non_scaling_ind) = FALSE then
            return FALSE;
         end if;
         ---
         if UPDATE_REPL_RESULTS(O_error_message,
                                L_new_repls_rec,
                                L_repl_results_roq,
                                I_order_no,
                                L_results_rec.alloc_no,
                                L_results_rec.item,
                                L_results_rec.location) = FALSE then
            return FALSE;
         end if;
         ---
         --***********************************************************
         -- If this record is a warehouse with a stock category of   *
         -- warehouse replenishment and a replenishment method of    *
         -- Time Supply or Dynamic, then the following recalculation *
         -- logic should be performed.                               *
         --***********************************************************
      elsif L_results_rec.loc_type = 'W' and
            (L_results_rec.repl_method = 'T' or L_results_rec.repl_method = 'D') then

         if L_inner_case is NULL then
            if GET_INNER_CASE(O_error_message,
                              L_inner_case,
                              L_results_rec.item,
                              L_ordhead_info_rec.supplier,
                              L_results_rec.origin_country_id) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         --**********************************************************
         -- If the recalc type is A, the user wants to recalculate  *
         -- based on user specified replenishment attributes stored *
         -- on repl_results for the order/item/loc.                 *
         --**********************************************************
         if L_results_rec.recalc_type = 'A' then

            if GET_WH_NEW_RAW_ROQ(O_error_message,
                                  L_wh_new_repls_rec,
                                  L_results_rec,
                                  L_ordhead_info_rec.supplier,
                                  L_ord_pack_size,
                                  L_inner_case,
                                  L_due_ord_process_ind,
                                  L_due_ord_serv_basis,
                                  L_unit_cost,
                                  L_unit_retail,
                                  L_domain_id) = FALSE then
               return FALSE;
            end if;
            ---
            --***********************************************************
            -- The user may specify an roq. In this case, no roq needs  *
            -- to be recalculated. The user specified roq will simply   *
            -- be rounded and used for scaling if scaling is indicated. *
            --***********************************************************
         elsif L_results_rec.recalc_type = 'Q' then
            L_wh_new_repls_rec.new_raw_roq := nvl(L_results_rec.recalc_qty, 0);
         end if;
         ---
         L_wh_new_order_roq := L_wh_new_repls_rec.new_raw_roq;
         ---
         --************************************************************************
         -- If due order processing is off, then non due roqs should              *
         -- not be added to the order even if a positive roq was generated.       *
         -- However, The repl results table will still contain the roq generated. *
         --************************************************************************
         L_repl_results_roq := L_wh_new_order_roq;
         ---
         -- if the user has specified an roq, the due indicator is ignored.
         ---
         if L_results_rec.recalc_type != 'Q' then
            if (L_due_ord_process_ind = 'N') and
               (L_wh_new_repls_rec.due_ind = 'N') and
               (L_wh_new_order_roq > 0) then
                  L_wh_new_order_roq := 0;
            end if;
         end if;
         ---
         if NONXDOCK_ROQ_ORD_MAINTENANCE(O_error_message,
                                         L_recalc_expenses,
                                         L_ordsku_rec_exists,
                                         I_order_no,
                                         L_ordhead_info_rec,
                                         L_results_rec.item,
                                         L_supp_currency,
                                         L_results_rec.orig_roq,
                                         L_wh_new_order_roq,
                                         L_wh_new_repls_rec.new_raw_roq,
                                         L_results_rec.location,
                                         L_results_rec.loc_type,
                                         L_elc_ind,
                                         L_results_rec.origin_country_id,
                                         L_base_cntry_id,
                                         L_unit_cost,
                                         L_unit_retail,
                                         L_ord_pack_size,
                                         L_results_rec.non_scaling_ind) = FALSE then
            return FALSE;
         end if;
         ---
         if UPDATE_REPL_RESULTS(O_error_message,
                                L_wh_new_repls_rec,
                                L_repl_results_roq,
                                I_order_no,
                                L_results_rec.alloc_no,
                                L_results_rec.item,
                                L_results_rec.location) = FALSE then
            return FALSE;
         end if;
         ---
         --***********************************************************
         -- If this is a store and its stock category is cross dock, *
         -- perform the following recalculation logic.               *
         --***********************************************************
      elsif L_results_rec.loc_type = 'S' and L_results_rec.stock_cat = 'C' then
         L_update_ordloc_for_allocs := 'Y';
         ---
         --**********************************************************
         -- If the recalc type is A, the user wants to recalculate  *
         -- based on user specified replenishment attributes stored *
         -- on repl_results for the order/item/loc.                 *
         --**********************************************************
         if L_results_rec.recalc_type = 'A' then
            ---
            L_store_need := NULL;
            ---
            if GET_NEW_RAW_ROQ(O_error_message,
                               L_new_repls_rec,
                               L_results_rec,
                               L_store_need,
                               L_due_ord_process_ind,
                               L_due_ord_serv_basis,
                               L_unit_cost,
                               L_unit_retail,
                               L_domain_id) = FALSE then
               return FALSE;
            end if;
            ---
            --***********************************************************
            -- The user may specify an roq. In this case, no roq needs  *
            -- to be recalculated. The user specified roq will simply   *
            -- be rounded and used for scaling if scaling is indicated. *
            --***********************************************************
         elsif L_results_rec.recalc_type = 'Q' then
            L_new_repls_rec.new_raw_roq := nvl(L_results_rec.recalc_qty, 0);
         end if;
         ---
         -- If the raw roq is greater than 0, round it to the store ord mult.
         ---
         if (L_new_repls_rec.new_raw_roq > 0)  and (L_results_rec.recalc_type != 'Q') then
            ---
            -- if the break pack ind is NULL then the source wh
            -- has changed. The break pack ind is NULLED out every
            -- time the source wh changes.
            ---
            if L_break_pack_ind is NULL then
               if GET_BREAK_PACK_IND(O_error_message,
                                     L_break_pack_ind,
                                     L_results_rec.source_wh) = FALSE  then
                  return FALSE;
               end if;
            end if;
            ---
            if (L_results_rec.store_ord_mult = 'E' and L_break_pack_ind = 'Y') then
               if ROUND_EACHES(O_error_message,
                               L_new_repls_rec.new_raw_roq) = FALSE then
                  return FALSE;
               end if;
               ---
               L_new_order_roq := L_new_repls_rec.new_raw_roq;
            end if;
         elsif (L_new_repls_rec.new_raw_roq <= 0) or (L_results_rec.recalc_type = 'Q') then
            L_new_order_roq := L_new_repls_rec.new_raw_roq;
         end if;
         ---
         --************************************************************************
         -- If due order processing is off, then non due roqs should              *
         -- not be added to the order even if a positive roq was generated.       *
         -- However, The repl results table will still contain the roq generated. *
         --************************************************************************
         L_repl_results_roq := L_new_order_roq;
         ---
         -- if the user has specified an roq, the due indicator is ignored.
         ---
         if L_results_rec.recalc_type != 'Q' then
            if (L_due_ord_process_ind = 'N') and
               (L_new_repls_rec.due_ind = 'N') and
               (L_new_order_roq > 0) then
                  L_new_order_roq := 0;
            end if;
         end if;
         ---
         if TOTAL_ALLOC_QTYS(O_error_message,
                             L_alloc_detail_rec_exists,
                             L_orig_alloc_qty,
                             L_deleted_alloc_total,
                             L_inserted_alloc_total,
                             L_new_order_roq,
                             L_results_rec.alloc_no,
                             L_results_rec.location) = FALSE then
            return FALSE;
         end if;
         ---
         if XDOCK_ROQ_ORD_MAINTENANCE(O_error_message,
                                      L_ordsku_rec_exists,
                                      L_ordloc_rec_exists,
                                      L_alloc_header_rec_exists,
                                      L_results_rec.alloc_no,
                                      L_alloc_detail_rec_exists,
                                      L_prev_alloc_no,
                                      I_order_no,
                                      L_results_rec.item,
                                      L_results_rec.location,
                                      L_new_order_roq,
                                      L_orig_alloc_qty,
                                      L_results_rec.source_wh,
                                      L_supp_currency,
                                      L_unit_cost,
                                      L_unit_retail,
                                      L_results_rec.origin_country_id,
                                      L_base_cntry_id,
                                      L_ord_pack_size,
                                      L_elc_ind,
                                      L_results_rec.non_scaling_ind,
                                      L_alloc_method,
                                      L_ordhead_info_rec) = FALSE then
            return FALSE;
         end if;
         ---
         -- If the allocation detail rec does not exist,
         -- the repl results allocation column for this
         -- record should be NULL.
         ---
         if L_alloc_detail_rec_exists = 'N' then
            L_results_rec_alloc := NULL;
         else
            L_results_rec_alloc := L_results_rec.alloc_no;
         end if;
         ---
         if UPDATE_REPL_RESULTS(O_error_message,
                                L_new_repls_rec,
                                L_repl_results_roq,
                                I_order_no,
                                L_results_rec_alloc,
                                L_results_rec.item,
                                L_results_rec.location) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- Populate previous record's needed values.
      ---
      L_prev_item              := L_results_rec.item;
      L_prev_location          := L_results_rec.location;
      L_prev_loc_type          := L_results_rec.loc_type;
      L_prev_source_wh         := L_results_rec.source_wh;
      L_prev_stock_cat         := L_results_rec.stock_cat;
      L_prev_ord_pack_size     := L_ord_pack_size;
      L_prev_orig_cntry        := L_results_rec.origin_country_id;
      L_prev_ordsku_rec_exists := L_ordsku_rec_exists;
      L_prev_alloc_no          := L_results_rec.alloc_no;
      L_prev_ord_pack_size     := L_ord_pack_size;
   END LOOP;
   ---
   close C_RESULTS;
   ---
   -- Logic for updating ordloc for any recalced crossdock stores.
   ---
   if L_update_ordloc_for_allocs = 'Y' then
      if GET_ORIG_AND_NEW_ALLOC_TOTALS(O_error_message,
                                       L_total_orig_alloc_qty,
                                       L_total_new_alloc_qty,
                                       I_order_no,
                                       L_prev_alloc_no,
                                       L_prev_item,
                                       L_ordhead_info_rec.supplier,
                                       L_prev_orig_cntry,
                                       L_prev_ord_pack_size,
                                       L_deleted_alloc_total,
                                       L_inserted_alloc_total) = FALSE then
         return FALSE;
      end if;
      ---
      if UPDATE_ORDLOC_FOR_ALLOCS(O_error_message,
                                  L_recalc_expenses,
                                  L_total_orig_alloc_qty,
                                  L_total_new_alloc_qty,
                                  I_order_no,
                                  L_prev_alloc_no,
                                  L_prev_item,
                                  L_prev_source_wh,
                                  L_ordhead_info_rec.supplier,
                                  L_prev_ord_pack_size,
                                  L_prev_orig_cntry,
                                  L_elc_ind) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   -- Logic for recalcing elc expenses for all locations on the order
   -- for a sku.
   ---
   if (L_recalc_expenses = 'Y') and (L_elc_ind = 'Y') then
      if RECALC_ELC(O_error_message,
                    L_prev_item,
                    I_order_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   -- Determine if an existing ordsku record is no longer needed.
   -- If not, delete it.
   ---
   if L_prev_ordsku_rec_exists = 'Y' then
      if ORDLOC_QTYS_EXIST(O_error_message,
                           L_ordloc_qtys_exist,
                           I_order_no,
                           L_prev_item) = FALSE then
         return FALSE;
      end if;
      ---
      if L_ordloc_qtys_exist = 'N' then
         if DELETE_ORDSKU_REC(O_error_message,
                              I_order_no,
                              L_prev_item) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   if L_ordhead_info_rec.import_order_ind = 'Y' then
      ---
      -- Create a row the for last item's origin country in the origin country table.
      ---
      L_orig_cntry_table(L_prev_item) := L_prev_orig_cntry;
      ---
      -- If this was originally an import order, determine if it is still
      -- an import order. If not, update the import status of the order.
      ---
      if CHECK_IMPORT_STATUS(O_error_message,
                             L_orig_cntry_table,
                             nvl(L_ordhead_info_rec.import_country_id, L_base_cntry_id),
                             I_order_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   -- Determine the due status of the order
   if ORDER_DUE_SQL.ORDER_DUE(O_error_message,
                              L_boolean_due_status,
                              I_order_no) = FALSE then
      return FALSE;
   end if;
   ---
   -- This code is needed because the above function returns an indicator
   -- type different from the variable type stored on the ord_inv_mgmt table.
   ---
   if L_boolean_due_status = TRUE then
      L_new_due_status := 'Y';
   elsif L_boolean_due_status = FALSE then
      L_new_due_status := 'N';
   end if;
   ---
   -- if the due satus of the order has changed, update the due status
   ---
   if L_new_due_status != L_orig_due_status then
      if UPDATE_DUE_STATUS(O_error_message,
                           I_order_no,
                           L_new_due_status) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if ROUNDING_SQL.ORDER_INTERFACE(O_error_message,
                                   I_order_no,
                                   L_ordhead_info_rec.supplier) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_RECALC_SQL.RECALC_REPLENISHMENT',
                                            to_char(SQLCODE));
      return FALSE;
END RECALC_REPLENISHMENT;
-------------------------------------------------------------------
FUNCTION RECALC_CONSTRAINTS(O_error_message   IN OUT VARCHAR2,
                            O_scaled_ind      IN OUT BOOLEAN,
                            I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_order_no   ORDHEAD.ORDER_NO%TYPE;
   L_ret_val   BINARY_INTEGER;
   L_error_message VARCHAR2(2000) := ' ';

BEGIN
   logger.log_information('Start processing RECALC_CONSTRAINTS call DELETE_TEMP_TABLES for order: '||I_order_no);
   if ORDER_SETUP_SQL.DELETE_TEMP_TABLES(O_error_message,
                                         I_order_no) = FALSE then
      return FALSE;
   end if;
   ---
   -- The item type must always be passed as NULL
   -- otherwise the correct insert-select into ordsku_hts_assess_temp\
   -- will not fire within the called function.
   ---
   logger.log_information('Start processing RECALC_CONSTRAINTS call ORDER_DIST_SQL.COPY_ORDLOC for order: '||I_order_no);
   if ORDER_DIST_SQL.COPY_ORDLOC(O_error_message,
                                 I_order_no,
                                 NULL,
                                 'N') = FALSE then
      return FALSE;
   end if;
   logger.log_information('Processing RECALC_CONSTRAINTS call SCALE_CONSTRAINTS_SQL for order: '||I_order_no);
   ---
   L_ret_val := SCALE_CONSTRAINTS_SQL(L_error_message,
                                      I_order_no);
   ---
   logger.log_information('Processing RECALC_CONSTRAINTS after call to SCALE_CONSTRAINTS_SQL for order: '||I_order_no);
   If L_ret_val < 0 then
      logger.log_information('Processing RECALC_CONSTRAINTS - L_ret_val = 0');
      O_scaled_ind := FALSE;
      O_error_message := L_error_message;
      return FALSE;
   elsif L_ret_val > 0 then
      logger.log_information('Processing RECALC_CONSTRAINTS - L_ret_val > 0');
      O_scaled_ind := FALSE;
      ---
      if L_error_message is not NULL then
         update ord_inv_mgmt
            set scale_issues = L_error_message,
                no_vehicles = NULL
          where order_no = I_order_no;
      end if;
   else  -- scaling successful
      O_scaled_ind := TRUE;
      ---
      update ord_inv_mgmt
         set scale_cnstr_chg_ind = 'N',
             scale_issues = NULL
       where order_no = I_order_no;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.RECALC_CONSTRAINTS',
                                             to_char(SQLCODE));
      return FALSE;
END RECALC_CONSTRAINTS;
-------------------------------------------------------------------
FUNCTION RECALC_ORDER(O_error_message  IN OUT VARCHAR2,
                      O_scaled_ind     IN OUT BOOLEAN,
                      I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                      I_recalc_type    IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_order_no          ORDHEAD.ORDER_NO%TYPE := I_order_no;
   L_recalc_type       VARCHAR2(1)           := I_recalc_type;
   L_supplier          ORDHEAD.SUPPLIER%TYPE;
   L_item              ORDLOC.ITEM%TYPE;
   L_loc               ORDLOC.LOCATION%TYPE;
   
   CURSOR C_GET_DEPOSIT_ITEM is
      select oh.supplier,
             ol.item,
             ol.location
        from ordhead oh, 
             ordloc ol, 
             item_master im
       where oh.order_no = I_order_no
         and oh.order_no = ol.order_no
         and im.item = ol.item
         and im.orderable_ind = 'Y'
         and deposit_item_type = 'E';

BEGIN
   -- Initialize the output scaling parameter.
   ---
   O_scaled_ind := FALSE;
   ---
   -- if the recalc type is R for recalc replenishment
   ---
   if L_recalc_type = 'R' then
      if RECALC_REPLENISHMENT(O_error_message,
                              L_order_no) = FALSE then
         return FALSE;
      end if;
   ---
   -- else if the recalc type is C for rescale the order to constraints
   elsif L_recalc_type = 'C' then
      if RECALC_CONSTRAINTS(O_error_message,
                            O_scaled_ind,
                            L_order_no) = FALSE then
         return FALSE;
      end if;
   ---
   -- else if the recalc type is 'B' for recalc both replenishment and
   -- rescale the order
   elsif L_recalc_type = 'B' then
      if RECALC_REPLENISHMENT(O_error_message,
                              L_order_no) = FALSE then
         return FALSE;
      end if;
      ---
      if RECALC_CONSTRAINTS(O_error_message,
                            O_scaled_ind,
                            L_order_no) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   open C_GET_DEPOSIT_ITEM;
   LOOP
      fetch C_GET_DEPOSIT_ITEM into L_supplier,
                                    L_item,
                                    L_loc;
      EXIT WHEN C_GET_DEPOSIT_ITEM%NOTFOUND;
         if ORDER_SETUP_SQL.POP_SINGLE_CONTAINER_ITEM(O_error_message,
                                                      L_order_no,
                                                      L_supplier,
                                                      L_loc,
                                                      L_item) = false then
            return FALSE;
         end if;
   END LOOP;
   close C_GET_DEPOSIT_ITEM;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.RECALC_ORDER',
                                             to_char(SQLCODE));
      return FALSE;
END RECALC_ORDER;
-------------------------------------------------------------------
FUNCTION UNDO_SCALING(O_error_message   IN OUT VARCHAR2,
                      I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_order_no   ORDHEAD.ORDER_NO%TYPE := I_order_no;

BEGIN
   -- Delete the existing post scaled order from the ordering tables.
   ---
   if ORDER_SETUP_SQL.DELETE_ORDER(O_error_message,
                                   L_order_no,
                                   NULL,
                                   NULL,
                                   NULL,
                                   'undo') = FALSE then
      return FALSE;
   end if;
   ---
   -- Rebuild the order from the temp tables.
   ---
   if ORDER_DIST_SQL.REBUILD_ORDER(O_error_message,
                                   L_order_no) = FALSE then
      return FALSE;
   end if;
   ---
   -- Delete the temp tables since they have been used to rebuild the order.
   ---
   if ORDER_SETUP_SQL.DELETE_TEMP_TABLES(O_error_message,
                                         L_order_no) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_RECALC_SQL.UNDO_SCALING',
                                             to_char(SQLCODE));
      return FALSE;
END UNDO_SCALING;
-------------------------------------------------------------------
END ORDER_RECALC_SQL;
/
