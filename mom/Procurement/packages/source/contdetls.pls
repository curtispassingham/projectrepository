CREATE OR REPLACE PACKAGE CONTRACT_DETAIL_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
-- Function Name: CONTRACT_HEADER_EXISTS
-- Purpose:       Determine if a record exists on the contract_header table
--                for a specified contract number.
--
FUNCTION CONTRACT_HEADER_EXISTS
         (O_error_message  IN OUT VARCHAR2,
          O_exists         IN OUT BOOLEAN,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CONTRACT_COST_EXISTS
-- Purpose:       Determine if a record exists on the contract_cost table
--                for a specified contract number.
--
FUNCTION CONTRACT_COST_EXISTS
         (O_error_message  IN OUT VARCHAR2,
          O_exists         IN OUT BOOLEAN,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CONTRACT_DETAIL_EXISTS
-- Purpose:       Determine if a record exists on the contract_detail table
--                for a specified contract number. An optional item value may
--                be passed into the function. Valid items will be:
--                   An actual item number that must be the item on the contract.
--                   A NULL value not basing the search on any item.
--                   'NULL' requiring that the item on the contract is NULL.
--
FUNCTION CONTRACT_DETAIL_EXISTS
         (O_error_message  IN OUT VARCHAR2,
          O_exists         IN OUT BOOLEAN,
          I_item           IN     CONTRACT_DETAIL.ITEM%TYPE,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CONTRACT_DETAIL_PARENT_EXISTS
-- Purpose:       Determine if a record exists on the contract_detail table
--                for a specified contract number and item parent or item
--                grandparent.
--
FUNCTION CONTRACT_DETAIL_PARENT_EXISTS
         (O_error_message    IN OUT VARCHAR2,
          O_exists           IN OUT BOOLEAN,
          I_item_parent      IN     CONTRACT_DETAIL.ITEM_PARENT%TYPE,
          I_item_grandparent IN     CONTRACT_DETAIL.ITEM_GRANDPARENT%TYPE,
          I_contract_no      IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CONTRACT_EXISTS_ON_ORDHEAD
-- Purpose:       Determine if a record exists on the ordhead table
--                for a specified contract number. An optional order status
--                or group of order status values may be passed into the
--                function. Valid status groups will be:
--                   'W'   Worksheet
--                   'S'   Submitted
--                   'A'   Approved
--                   'C'   Completed
--                   'WSA' Worksheet or Submitted or Approved
--
FUNCTION CONTRACT_EXISTS_ON_ORDHEAD
         (O_error_message  IN OUT VARCHAR2,
          O_exists         IN OUT BOOLEAN,
          I_status_code    IN     VARCHAR2,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_DETAIL_REF_ITEM
-- Purpose:       Gets the reference item and description from the
--                contract_detail table for a specified contract number
--                and item.
FUNCTION GET_DETAIL_REF_ITEM
         (O_error_message  IN OUT VARCHAR2,
          O_exists         IN OUT BOOLEAN,
          O_ref_item       IN OUT CONTRACT_DETAIL.REF_ITEM%TYPE,
          O_ref_item_desc  IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
          I_item           IN     CONTRACT_DETAIL.ITEM%TYPE,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_HEADER_DATES
-- Purpose:       Gets the start and end dates from the contract_header
--                table for a specified contract number.
--
FUNCTION GET_HEADER_DATES
         (O_error_message  IN OUT VARCHAR2,
          O_exists         IN OUT BOOLEAN,
          O_start_date     IN OUT CONTRACT_HEADER.START_DATE%TYPE,
          O_end_date       IN OUT CONTRACT_HEADER.END_DATE%TYPE,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_READY_DATE
-- Purpose:       Gets the ready date from the contract_detail
--                table for a specified contract number.
--
FUNCTION GET_READY_DATE
         (O_error_message  IN OUT VARCHAR2,
          O_exists         IN OUT BOOLEAN,
          O_ready_date     IN OUT CONTRACT_DETAIL.READY_DATE%TYPE,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_DETAIL_QUANTITIES
-- Purpose:       Gets the contracted, ordered, and received quantities from
--                the contract_detail table for a specified contract number and
--                contract item.
--
FUNCTION GET_DETAIL_QUANTITIES
         (O_error_message    IN OUT VARCHAR2,
          O_exists           IN OUT BOOLEAN,
          O_qty_contracted   IN OUT CONTRACT_DETAIL.QTY_CONTRACTED%TYPE,
          O_qty_ordered      IN OUT CONTRACT_DETAIL.QTY_ORDERED%TYPE,
          O_qty_received     IN OUT CONTRACT_DETAIL.QTY_RECEIVED%TYPE,
          I_item_grandparent IN     CONTRACT_DETAIL.ITEM_GRANDPARENT%TYPE,
          I_item_parent      IN     CONTRACT_DETAIL.ITEM_PARENT%TYPE,
          I_item             IN     CONTRACT_DETAIL.ITEM%TYPE,
          I_contract_no      IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_READY_DATE_IN_RANGE
-- Purpose:       Verify that the specified contract's ready date exists on the
--                contract_detail table within the specified start and end dates.
--
FUNCTION CHECK_READY_DATE_IN_RANGE
         (O_error_message  IN OUT VARCHAR2,
          O_within_range   IN OUT BOOLEAN,
          I_start_date     IN     CONTRACT_HEADER.START_DATE%TYPE,
          I_end_date       IN     CONTRACT_HEADER.END_DATE%TYPE,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_FILTER_WHERE
-- Purpose:       Get the where clause from the filter temp and verify that a
--                record exists for the specified form name and unique key.
--
FUNCTION GET_FILTER_WHERE
         (O_error_message  IN OUT VARCHAR2,
          O_filter_exists  IN OUT BOOLEAN,
          O_where_clause   IN OUT FILTER_TEMP.WHERE_CLAUSE%TYPE,
          I_form_name      IN     FILTER_TEMP.FORM_NAME%TYPE,
          I_unique_key     IN     FILTER_TEMP.UNIQUE_KEY%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: LOCK_CONTRACT_COST
-- Purpose:       Lock the contract cost table for a specified contract number.
--
FUNCTION LOCK_CONTRACT_COST
         (O_error_message  IN OUT VARCHAR2,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: LOCK_CONTRACT_DETAIL
-- Purpose:       Lock the contract detail table for a specified contract number.
--
FUNCTION LOCK_CONTRACT_DETAIL
         (O_error_message  IN OUT VARCHAR2,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: LOCK_CONTRACT_MATRIX_TEMP
-- Purpose:       Lock the contract matrix temp table for a specified contract
--                number.
--
FUNCTION LOCK_CONTRACT_MATRIX_TEMP
         (O_error_message  IN OUT VARCHAR2,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: LOCK_FILTER_TEMP
-- Purpose:       Lock the filter temp table for a specified form and key.
--
FUNCTION LOCK_FILTER_TEMP
         (O_error_message  IN OUT VARCHAR2,
          I_form_name      IN     FILTER_TEMP.FORM_NAME%TYPE,
          I_unique_key     IN     FILTER_TEMP.UNIQUE_KEY%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: DELETE_FILTER_TEMP
-- Purpose:       Locks and deletes from the filter temp table
--                for a specified form and key.
--
FUNCTION DELETE_FILTER_TEMP
         (O_error_message  IN OUT VARCHAR2,
          I_form_name      IN     FILTER_TEMP.FORM_NAME%TYPE,
          I_unique_key     IN     FILTER_TEMP.UNIQUE_KEY%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: INSERT_UPDATE_FILTER_TEMP
-- Purpose:       Locks and either inserts or updates the where clause from
--                the filter temp table for a specified form and key.
--
FUNCTION INSERT_UPDATE_FILTER_TEMP
         (O_error_message  IN OUT VARCHAR2,
          I_where_clause   IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
          I_form_name      IN     FILTER_TEMP.FORM_NAME%TYPE,
          I_unique_key     IN     FILTER_TEMP.UNIQUE_KEY%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: NEXT_PREV_CONTRACT_ORDSKU
-- Purpose:       Determines if there is either a next or previous item in
--                respect to a specified current item for a specified contract
--                order sequence number.
--
FUNCTION NEXT_PREV_CONTRACT_ORDSKU
         (O_error_message  IN OUT VARCHAR2,
          O_next_prev_exists  IN OUT BOOLEAN,
          I_contract_ordhead_seq  IN   CONTRACT_ORDSKU.CONTRACT_ORDHEAD_SEQ%TYPE,
          I_current_item          IN   CONTRACT_ORDSKU.ITEM%TYPE,
          I_next_prev_ind         IN   VARCHAR2)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_VALID_AVAILABILITY
-- Purpose:       Gets the quantity available and supplier for a specified
--                contract and item.
--
FUNCTION GET_VALID_AVAILABILITY
         (O_error_message  IN OUT VARCHAR2,
          O_exists         IN OUT BOOLEAN,
          O_qty_avail      IN OUT SUP_AVAIL.QTY_AVAIL%TYPE,
          O_supplier       IN OUT SUP_AVAIL.SUPPLIER%TYPE,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
          I_item           IN     CONTRACT_ORDSKU.ITEM%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: VALIDATE_CONTRACT_NO
-- Purpose:       Determines if the specified contract/item relationship
--                is valid for the specified date.
--
FUNCTION VALIDATE_CONTRACT_NO
         (O_error_message  IN OUT VARCHAR2,
          O_valid          IN OUT BOOLEAN,
          IO_vdate         IN OUT PERIOD.VDATE%TYPE,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
          I_item           IN     CONTRACT_ORDSKU.ITEM%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Function Name: VALIDATE_CONTRACT_NO
-- Purpose:       Overloaded function to determines if the specified contract/item relationship
--                is orderable or not.
--
FUNCTION VALIDATE_CONTRACT_NO
         (O_error_message  IN OUT VARCHAR2,
          O_valid          IN OUT BOOLEAN,
          O_orderable_ind  IN OUT CONTRACT_HEADER.ORDERABLE_IND%TYPE,
          IO_vdate         IN OUT PERIOD.VDATE%TYPE,
          I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
          I_item           IN     CONTRACT_ORDSKU.ITEM%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: INSERT_UPDATE_CONTRACT_ORDLOC
-- Purpose:       Inserts or updates the contract_ordloc table for the specified
--                location group, location group value, quantity ordered, contract
--                ordhead sequence number, contract number, and item.
--
FUNCTION INSERT_UPDATE_CONTRACT_ORDLOC
         (O_error_message         IN OUT VARCHAR2,
          I_loc_group             IN     VARCHAR2,
          I_loc_group_value       IN     VARCHAR2,
          I_qty_ordered           IN     CONTRACT_ORDLOC.QTY_ORDERED%TYPE,
          I_contract_ordhead_seq  IN     CONTRACT_ORDLOC.CONTRACT_ORDHEAD_SEQ%TYPE,
          I_contract_no           IN     CONTRACT_ORDLOC.CONTRACT_NO%TYPE,
          I_item                  IN     CONTRACT_ORDSKU.ITEM%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CREATE_FROM_EXISTING
-- Purpose:       Create a new contract based on a specified existing contract. This
--                includes inserts into contract_header, contract_detail, and
--                contract_cost.
--
FUNCTION CREATE_FROM_EXISTING
         (O_error_message           IN OUT VARCHAR2,
          O_dates_auto_adjust       IN OUT BOOLEAN,
          IO_contract_approval_ind  IN OUT VARCHAR2,
          I_new_contract_no         IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
          I_existing_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: VALID_CNTRACT_MATRIX_TEMP_LIST
-- Purpose:       Checks if at least one item within the specified item list
--                exists for the specified department, supplier, and origin
--                country that is not already associated with the specified
--                contract number.
--
FUNCTION VALID_CNTRACT_MATRIX_TEMP_LIST
         (O_error_message     IN OUT VARCHAR2,
          O_item_exists       IN OUT BOOLEAN,
          O_list_desc         IN OUT SKULIST_HEAD.SKULIST_DESC%TYPE,
          I_dept              IN     ITEM_MASTER.DEPT%TYPE,
          I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
          I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
          I_item_list         IN     SKULIST_DETAIL.SKULIST%TYPE,
          I_contract_no       IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: INS_CNTRACT_MATRIX_TEMP_LIST
-- Purpose:       Insert into the contract_matrix_temp table for a specified
--                contract all valid items for the specified department,
--                supplier, origin country, and item list.
--
FUNCTION INS_CNTRACT_MATRIX_TEMP_LIST
         (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
          I_dept                IN       ITEM_MASTER.DEPT%TYPE,
          I_supplier            IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
          I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
          I_item_list           IN       SKULIST_DETAIL.SKULIST%TYPE,
          I_qty                 IN       CONTRACT_MATRIX_TEMP.QTY%TYPE,
          I_contract_no         IN       CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: DELETE_CONTRACT_MATRIX_TEMP
-- Purpose:       Delete all records from the contract matrix temp table for a
--                specified contract number.
--
FUNCTION DELETE_CONTRACT_MATRIX_TEMP
         (O_error_message  IN OUT VARCHAR2,
          I_contract_no    IN     CONTRACT_MATRIX_TEMP.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_CONTRACT_MATRIX_TEMP_QTY
-- Purpose:       Gets the total qty from the contract matrix temp table for a
--                specified contract number. It then rounds up the quantities
--                or increments the quantities untill the sum of all distriubuted
--                quanties equal the total quantity specified.
--
FUNCTION GET_CONTRACT_MATRIX_TEMP_QTY
         (O_error_message     IN OUT VARCHAR2,
          I_contract_no       IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: POPULATE_CONTRACT_MATRIX_TEMP
-- Purpose:       This function populates contract_matrix_temp table by selecting
--                the contract detail record from contract_detail table and It is used for Contract Distribution logic.
---------------------------------------------------------------------------------------------
FUNCTION POPULATE_CONTRACT_MATRIX_TEMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_contract_no     IN       CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------------
END CONTRACT_DETAIL_SQL;
/
