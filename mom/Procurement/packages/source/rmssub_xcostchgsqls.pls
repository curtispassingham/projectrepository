
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XCOSTCHG_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_costchg_rec     IN       RMSSUB_XCOSTCHG.COST_CHANGE_RECTYPE,
                 I_message         IN       "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XCOSTCHG_SQL;
/

