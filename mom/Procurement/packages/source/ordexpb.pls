CREATE OR REPLACE PACKAGE BODY ORDER_EXPENSE_SQL AS
-------------------------------------------------------------------------------
-- Global Variables used in INSERT_COST_COMP, DEFAULT_EXPENSE, INSERT_COMPS,
-- INSERT_BASE_COMPS, and DEFAULT_PTNR functions.
---
   LP_buyer_pack         VARCHAR2(1)  := 'N';
   LP_lading_port        OUTLOC.OUTLOC_ID%TYPE;
   LP_discharge_port     OUTLOC.OUTLOC_ID%TYPE;
   LP_purchase_type      ORDHEAD.PURCHASE_TYPE%TYPE;
   LP_backhaul_type      ORDHEAD.BACKHAUL_TYPE%TYPE;
   LP_currency_code      CURRENCIES.CURRENCY_CODE%TYPE;
   LP_exchange_rate      CURRENCY_RATES.EXCHANGE_RATE%TYPE := -1;
   LP_written_date       ORDHEAD.WRITTEN_DATE%TYPE;
   LP_origin_country_id  COUNTRY.COUNTRY_ID%TYPE;
   LP_agent              PARTNER.PARTNER_ID%TYPE;
   LP_factory            PARTNER.PARTNER_ID%TYPE;
   LP_partner1           ORDHEAD.partner1%TYPE;
   LP_partner2           ORDHEAD.partner2%TYPE;
   LP_partner3           ORDHEAD.partner3%TYPE;
   LP_partner_type_1     ORDHEAD.partner_type_1%TYPE;
   LP_partner_type_2     ORDHEAD.partner_type_2%TYPE;
   LP_partner_type_3     ORDHEAD.partner_type_3%TYPE;
--------------------------------------------------------------------------------
TYPE locs_table_type IS
   TABLE OF ORDLOC_EXP.LOCATION%TYPE INDEX BY BINARY_INTEGER;
--------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ(O_error_message IN OUT VARCHAR2,
                      O_seq_no        IN OUT ORDLOC_EXP.SEQ_NO%TYPE,
                      I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(50) := 'ORDER_EXPENSE_SQL.GET_NEXT_SEQ';

   cursor C_MAX_SEQ_NO is
      select nvl(MAX(seq_no),0) + 1
        from ordloc_exp
       where order_no = I_order_no;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_MAX_SEQ_NO','ORDLOC_EXP',
                    'Order number: ' || to_char(I_order_no));
   open C_MAX_SEQ_NO;
   ---
   SQL_LIB.SET_MARK('FETCH','C_MAX_SEQ_NO','ORDLOC_EXP',
                    'Order number: ' || to_char(I_order_no));
   fetch C_MAX_SEQ_NO into O_seq_no;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_MAX_SEQ_NO','ORDLOC_EXP',
                    'Order number: ' || to_char(I_order_no));
   close C_MAX_SEQ_NO;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_SEQ;
-------------------------------------------------------------------------------
FUNCTION EXP_EXIST(O_error_message IN OUT VARCHAR2,
                   O_exists        IN OUT BOOLEAN,
                   I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                   I_item          IN     ITEM_MASTER.ITEM%TYPE,
                   I_location      IN     ORDLOC.LOCATION%TYPE,
                   I_comp_id       IN     ELC_COMP.COMP_ID%TYPE,
                   I_pack_item     IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)          := 'ORDER_EXPENSE_SQL.EXP_EXIST';
   L_exists         VARCHAR2(1)           := 'N';
   L_buyer_pack     VARCHAR2(1)           := 'N';
   L_item           ITEM_MASTER.ITEM%TYPE;
   L_pack_item      ITEM_MASTER.ITEM%TYPE;

   cursor C_BUYER_PACK is
      select 'Y'
        from item_master
       where item      = I_item
         and pack_ind  = 'Y'
         and pack_type = 'B';

   cursor C_PACK_ITEMS is 
      select item
        from v_packsku_qty
       where pack_no = L_pack_item;

   cursor C_CHECK_EXISTS is
      select 'Y'
        from ordloc_exp
       where order_no    = I_order_no
         and item        = L_item
         and location    = I_location
         and comp_id     = NVL(I_comp_id, comp_id)
         and (pack_item  = L_pack_item 
          or (pack_item  is NULL and L_pack_item is NULL));

BEGIN
   O_exists := TRUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_BUYER_PACK','PACKHEAD','Pack No = '||I_item||' Pack Type = B');
   open C_BUYER_PACK;
   SQL_LIB.SET_MARK('FETCH','C_BUYER_PACK','PACKHEAD','Pack No = '||I_item||' Pack Type = B');
   fetch C_BUYER_PACK into L_buyer_pack;
   SQL_LIB.SET_MARK('CLOSE','C_BUYER_PACK','PACKHEAD','Pack No = '||I_item||' Pack Type = B');
   close C_BUYER_PACK;
   ---
   if L_buyer_pack = 'N' then
      L_item      := I_item;
      L_pack_item := I_pack_item;
      ---
      SQL_LIB.SET_MARK('OPEN','C_CHECK_EXISTS','ORDLOC_EXP',NULL);
      open C_CHECK_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_EXISTS','ORDLOC_EXP',NULL);
      fetch C_CHECK_EXISTS into L_exists;
      ---
      if C_CHECK_EXISTS%NOTFOUND then
         O_exists := FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXISTS','ORDLOC_EXP',NULL);
      close C_CHECK_EXISTS;
   else -- L_buyer_pack = 'Y'
      ---
      -- If I_item is a buyer pack than need to check ordloc_exp where the pack_item
      -- column equals the value in I_item.  Loop through the component items of the
      -- pack in order to get the zone from the location passed in.  If at least one
      -- record is found on ordloc_exp, exit the loop and pass out TRUE in O_exists.
      ---
      L_pack_item := I_item;
      ---
      FOR C_rec in C_PACK_ITEMS LOOP
         L_item := C_rec.item;
         ---
         SQL_LIB.SET_MARK('OPEN','C_CHECK_EXISTS','ORDLOC_EXP',NULL);
         open C_CHECK_EXISTS;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_EXISTS','ORDLOC_EXP',NULL);
         fetch C_CHECK_EXISTS into L_exists;
         ---
         if C_CHECK_EXISTS%NOTFOUND then
            O_exists := FALSE;
         else
            O_exists := TRUE;
            exit;
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXISTS','ORDLOC_EXP',NULL);
         close C_CHECK_EXISTS;
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXP_EXIST;
-------------------------------------------------------------------------------
FUNCTION DEL_EXP(O_error_message  IN OUT VARCHAR2,
                 I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                 I_seq_no         IN     ORDLOC_EXP.SEQ_NO%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'ORDER_EXPENSE_SQL.DEL_EXP';
   L_table       VARCHAR2(30) := 'ORDLOC_EXP';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ORDLOC_EXP is
      select 'Y'
        from ordloc_exp
       where order_no  = I_order_no
         and seq_no    = I_seq_no
         for update nowait; 

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC_EXP','ORDLOC_EXP',
                    'Order number: ' || to_char(I_order_no) ||  
                    ', Sequence: ' || to_char(I_seq_no));
   open C_LOCK_ORDLOC_EXP;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC_EXP','ORDLOC_EXP',
                    'Order number: ' || to_char(I_order_no) ||  
                    ', Sequence: ' || to_char(I_seq_no));
   close C_LOCK_ORDLOC_EXP;

   SQL_LIB.SET_MARK('DELETE',NULL,'ORDLOC_EXP','Order number: ' || 
                    to_char(I_order_no) ||', Sequence: ' || to_char(I_seq_no));
   delete from ordloc_exp
         where order_no = I_order_no
           and seq_no   = I_seq_no;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             to_char(I_order_no),
                                             to_char(I_seq_no));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEL_EXP;
-----------------------------------------------------------------------------------------
FUNCTION INSERT_COST_COMP(O_error_message  IN OUT VARCHAR2,
                          I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                          I_item           IN     ITEM_MASTER.ITEM%TYPE,
                          I_component_item IN     ITEM_MASTER.ITEM%TYPE,
                          I_location       IN     ORDLOC.LOCATION%TYPE,
                          I_loc_type       IN     ORDLOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ORDER_EXPENSE_SQL.INSERT_COST_COMP';

BEGIN
   if INSERT_COST_COMP(O_error_message,
                       I_order_no,
                       I_item,
                       I_component_item,
                       I_location,
                       I_loc_type,
                       'N') = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_COST_COMP;
---------------------------------------------------------------------------------
FUNCTION INSERT_COST_COMP(O_error_message     IN OUT VARCHAR2,
                          I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_component_item    IN     ITEM_MASTER.ITEM%TYPE,
                          I_location          IN     ORDLOC.LOCATION%TYPE,
                          I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                          I_backhaul_only_ind IN     VARCHAR2) 
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ORDER_EXPENSE_SQL.INSERT_COST_COMP';

BEGIN
   if INSERT_COST_COMP(O_error_message,
                       I_order_no,
                       I_item,
                       NULL, -- origin country id
                       I_component_item,
                       I_location,
                       I_loc_type,
                       'N',
                       NULL) = FALSE then  -- backhaul only ind
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_COST_COMP;
------------------------------------------------------------------------
FUNCTION INSERT_COST_COMP(O_error_message     IN OUT VARCHAR2,
                          I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                          I_component_item    IN     ITEM_MASTER.ITEM%TYPE,
                          I_location          IN     ORDLOC.LOCATION%TYPE,
                          I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                          I_backhaul_only_ind IN     VARCHAR2,
                          I_import_ord_ind    IN     ORDHEAD.IMPORT_ORDER_IND%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ORDER_EXPENSE_SQL.INSERT_COST_COMP';
   L_supplier           SUPS.SUPPLIER%TYPE;
   L_consolidation_ind  SYSTEM_OPTIONS.CONSOLIDATION_IND%TYPE;
   L_exchange_type      CURRENCY_RATES.EXCHANGE_TYPE%TYPE;
   L_zone_group_id      COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   L_zone_id            COST_ZONE.ZONE_ID%TYPE;
   L_exists             BOOLEAN;
   L_item               ITEM_MASTER.ITEM%TYPE;
   ---
   L_table              VARCHAR2(30);
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   cursor C_BUYER_PACK is
      select 'Y' 
        from item_master
       where item      = L_item
         and pack_type = 'B';

    cursor C_GET_ORDER_INFO is
       select supplier,
              lading_port,
              discharge_port,
              NVL(purchase_type, 'FOB'),
              backhaul_type,
              currency_code,
              exchange_rate,
              written_date,
              agent,
              factory,
              partner1,
              partner2,
              partner3,
              partner_type_1,
              partner_type_2,
              partner_type_3
        from ordhead
       where order_no = I_order_no;

   cursor C_LOC is
      select location,
             loc_type
        from ordloc
       where order_no = I_order_no
         and item     = L_item;

   cursor C_GET_ORD_ITEMS is
      select item
        from ordsku
       where order_no = I_order_no;

   cursor C_LOCK_ORDLOC_EXP_BUYPACK is
      select 'x'
        from ordloc_exp
       where order_no      = I_order_no
         and origin        = 'S'
         and ((item        = I_item
               and pack_item is NULL)
          or (pack_item    = I_item))
         and comp_currency = LP_currency_code
         for update nowait;

   cursor C_LOCK_ORDLOC_EXP_ITEM(LI_item ITEM_MASTER.ITEM%TYPE) is
      select 'x'
        from ordloc_exp
       where order_no      = I_order_no
         and origin        = 'S'
         and item          = LI_item
         and location      = I_location
         and loc_type      = I_loc_type
         and comp_currency = LP_currency_code
         for update nowait;

   cursor C_LOCK_ORDLOC_EXP_BUYPACK_LOC(LI_location ORDLOC.LOCATION%TYPE,
                                        LI_loc_type ORDLOC.LOC_TYPE%TYPE) is
      select 'x'
        from ordloc_exp
       where order_no      = I_order_no
         and origin        = 'S'
         and ((item        = L_item
               and pack_item is NULL)
          or (pack_item    = L_item))
         and location      = LI_location
         and loc_type      = LI_loc_type
         and comp_currency = LP_currency_code
         for update nowait;

BEGIN

   if SYSTEM_OPTIONS_SQL.CONSOLIDATION_IND(O_error_message,
                                           L_consolidation_ind) = FALSE then
      return FALSE;
   end if;
   ---
   if L_consolidation_ind = 'Y' then
      L_exchange_type := 'C';
   else
      L_exchange_type := 'O';
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_ORDER_INFO','ORDHEAD','Order no: '||to_char(I_order_no));
   open C_GET_ORDER_INFO;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_ORDER_INFO','ORDHEAD','Order no: '||to_char(I_order_no));
   fetch C_GET_ORDER_INFO into L_supplier,
                               LP_lading_port,
                               LP_discharge_port,
                               LP_purchase_type,
                               LP_backhaul_type,
                               LP_currency_code,
                               LP_exchange_rate,
                               LP_written_date,
                               LP_agent,
                               LP_factory,
                               LP_partner1,
                               LP_partner2,
                               LP_partner3,
                               LP_partner_type_1,
                               LP_partner_type_2,
                               LP_partner_type_3;
   if C_GET_ORDER_INFO%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO', NULL, NULL, NULL);
      SQL_LIB.SET_MARK('CLOSE','C_GET_ORDER_INFO','ORDHEAD','Order no: '||to_char(I_order_no));
      close C_GET_ORDER_INFO;

      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_GET_ORDER_INFO','ORDHEAD','Order no: '||to_char(I_order_no));
   close C_GET_ORDER_INFO;
   ---
   if I_item is not NULL then
      L_item := I_item;
      ---
      if ORDER_ITEM_ATTRIB_SQL.GET_ORIGIN_COUNTRY(O_error_message,
                                                  L_exists,
                                                  LP_origin_country_id,
                                                  I_order_no,
                                                  I_item) = FALSE then
         return FALSE;
      end if;
      ---
      -- Need to reset the global variable to 'N' because non-pack items
      -- will not return a record from this cursor, so if the previous item
      -- checked was a Buyer Pack, the global variable will remain 'Y' even
      -- though the item is not a pack.
      ---
      LP_buyer_pack := 'N';
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_BUYER_PACK', 'PACKHEAD','Pack No: '||I_item);
      open C_BUYER_PACK;
      SQL_LIB.SET_MARK('FETCH', 'C_BUYER_PACK', 'PACKHEAD','Pack No: '||I_item);
      fetch C_BUYER_PACK into LP_buyer_pack;
      SQL_LIB.SET_MARK('CLOSE', 'C_BUYER_PACK', 'PACKHEAD','Pack No: '||I_item);
      close C_BUYER_PACK;
      ---
      if LP_buyer_pack = 'N' then
         if ITEM_ATTRIB_SQL.GET_COST_ZONE_GROUP(O_error_message,
                                                L_zone_group_id,
                                                I_item) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      -- Create landed cost components for all locations from ordloc table
      if I_location is NULL then
         FOR loc_rec in C_LOC LOOP
            if not DEFAULT_EXPENSES(O_error_message,
                                    I_order_no,
                                    L_supplier,
                                    I_item,
                                    I_component_item,
                                    loc_rec.location,
                                    loc_rec.loc_type,
                                    L_exchange_type,
                                    L_zone_group_id) then
               return FALSE;
               end if;
         END LOOP; -- for loc_rec in C_LOC loop
         ---
         L_table := 'ORDLOC_EXP';
         SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC_EXP_BUYPACK','ORDLOC_EXP','Order_no: '||to_char(I_order_no));
         open C_LOCK_ORDLOC_EXP_BUYPACK;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC_EXP_BUYPACK','ORDLOC_EXP','Order_no: '||to_char(I_order_no));
         close C_LOCK_ORDLOC_EXP_BUYPACK;
         -- Need to update the exchange rate to match the exchange rate for the order
         -- if the expense's currency is the same and the order's currency.

         SQL_LIB.SET_MARK('UPDATE',NULL,'ORDLOC_EXP','Order_no: '||to_char(I_order_no));
         update ordloc_exp 
            set exchange_rate = LP_exchange_rate
          where order_no      = I_order_no
            and origin        = 'S'
            and ((item        = I_item
                  and pack_item is NULL)
             or (pack_item    = I_item))
            and comp_currency = LP_currency_code;
         ---
      else -- I_location is not NULL
         if not DEFAULT_EXPENSES(O_error_message,
                                 I_order_no,
                                 L_supplier,
                                 I_item,
                                 I_component_item,
                                 I_location,
                                 I_loc_type,
                                 L_exchange_type,
                                 L_zone_group_id) then
            return FALSE;
         end if;
         ---
         -- Need to update the exchange rate to match the exchange rate for the order
         -- if the expense's currency is the same and the order's currency.
         ---
         if LP_buyer_pack = 'N' then
            L_table := 'ORDLOC_EXP';
            SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC_EXP_ITEM','ORDLOC_EXP','Order_no: '||to_char(I_order_no));
            open C_LOCK_ORDLOC_EXP_ITEM(I_item);
            SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC_EXP_ITEM','ORDLOC_EXP','Order_no: '||to_char(I_order_no));
            close C_LOCK_ORDLOC_EXP_ITEM;
            ---
            SQL_LIB.SET_MARK('UPDATE',NULL,'ORDLOC_EXP','Order_no: '||to_char(I_order_no));
            update ordloc_exp 
               set exchange_rate = LP_exchange_rate
             where order_no      = I_order_no
               and origin        = 'S'
               and item          = I_item
               and location      = I_location
               and loc_type      = I_loc_type
               and comp_currency = LP_currency_code;
         end if;
      end if;  -- I_location is not NULL/NULL
      ---
      -- Need to calculate all of the expenses.
      if LP_buyer_pack = 'Y' then
         -- Pass the Pack number (I_item) in the input parameter I_pack_item.
         -- Leave the input parameter I_item NULL so that the expenses of ALL the
         -- component items of the pack get calculated.
         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PE',
                                   NULL, -- item
                                   L_supplier,
                                   NULL, -- item_exp_type
                                   NULL, -- item_exp_seq
                                   I_order_no,
                                   NULL, -- ord_seq_no
                                   I_item, -- pack_item
                                   NULL, -- zone_id
                                   I_location,
                                   NULL, -- hts
                                   NULL, -- import_country_id
                                   LP_origin_country_id,
                                   NULL, -- effect_from
                                   NULL,-- effect_to
                                   I_import_ord_ind) = FALSE then 
            return FALSE;
         end if;
      else  -- LP_buyer_pack = 'N'
         -- Pass the item number (I_item) in the input parameter I_item.
         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PE',
                                   I_item, -- item
                                   L_supplier,
                                   NULL, -- item_exp_type
                                   NULL, -- item_exp_seq
                                   I_order_no,
                                   NULL, -- ord_seq_no
                                   NULL, -- pack_item
                                   NULL, -- zone_id
                                   I_location,
                                   NULL, -- hts
                                   NULL, -- import_country_id
                                   LP_origin_country_id,
                                   NULL, -- effect_from
                                   NULL,-- effect_to
                                   I_import_ord_ind) = FALSE then 
            return FALSE;
         end if;
      end if;
   else  -- I_item is NULL
      -- If backhaul only, delete all records on ordloc_exp for the item where the comp_id
      -- has an expense_category of 'B' on the elc_comp table
      if I_backhaul_only_ind = 'Y' then
         if DELETE_BACKHAUL_EXP(O_error_message,
                                I_order_no) = FALSE then
            return FALSE;
         end if;
      end if;
      FOR C_rec in C_GET_ORD_ITEMS LOOP
         L_item := C_rec.item;
         ---
         if ORDER_ITEM_ATTRIB_SQL.GET_ORIGIN_COUNTRY(O_error_message,
                                                     L_exists,
                                                     LP_origin_country_id,
                                                     I_order_no,
                                                     L_item) = FALSE then
            return FALSE;
         end if;
         ---
         -- Need to reset the global variable to 'N' because non-pack items
         -- will not return a record from this cursor, so if the previous item
         -- checked was a Buyer Pack, the global variable will remain 'Y' even
         -- though the item is not a pack.
         ---
         LP_buyer_pack := 'N';
         ---
         SQL_LIB.SET_MARK('OPEN', 'C_BUYER_PACK', 'PACKHEAD','Pack No: '||L_item);
         open C_BUYER_PACK;
         SQL_LIB.SET_MARK('FETCH', 'C_BUYER_PACK', 'PACKHEAD','Pack No: '||L_item);
         fetch C_BUYER_PACK into LP_buyer_pack;
         SQL_LIB.SET_MARK('CLOSE', 'C_BUYER_PACK', 'PACKHEAD','Pack No: '||L_item);
         close C_BUYER_PACK;
         ---
         if LP_buyer_pack = 'N' then
            if ITEM_ATTRIB_SQL.GET_COST_ZONE_GROUP(O_error_message,
                                                   L_zone_group_id,
                                                   L_item) = FALSE then
               return FALSE;
            end if;
         end if;
         ---      
         -- Create landed cost components for all locations from ordloc table
         if I_location is NULL then
            FOR loc_rec in C_LOC LOOP
               if I_backhaul_only_ind = 'N' then
                  if DEFAULT_EXPENSES(O_error_message,
                                      I_order_no,
                                      L_supplier,
                                      L_item,
                                      I_component_item,
                                      loc_rec.location,
                                      loc_rec.loc_type,
                                      L_exchange_type,
                                      L_zone_group_id)  = FALSE then 
                     return FALSE;
                  end if;
               else
                  if DEFAULT_BACK_EXPENSES(O_error_message,
                                           I_order_no,
                                           L_supplier,
                                           L_item,
                                           I_component_item,
                                           loc_rec.location,
                                           loc_rec.loc_type,
                                           L_exchange_type,
                                           L_zone_group_id)  = FALSE then
                     return FALSE;
                  end if;
               end if;
               ---
               L_table := 'ORDLOC_EXP';
               SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC_EXP_BUYPACK_LOC','ORDLOC_EXP','Order_no: '||to_char(I_order_no));
               open C_LOCK_ORDLOC_EXP_BUYPACK_LOC(loc_rec.location, loc_rec.loc_type);
               SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC_EXP_BUYPACK_LOC','ORDLOC_EXP','Order_no: '||to_char(I_order_no));
               close C_LOCK_ORDLOC_EXP_BUYPACK_LOC;
               -- Need to update the exchange rate to match the exchange rate for the order
               -- if the expense's currency is the same and the order's currency.
               SQL_LIB.SET_MARK('UPDATE',NULL,'ORDLOC_EXP','Order_no: '||to_char(I_order_no));
               update ordloc_exp 
                  set exchange_rate = LP_exchange_rate
                where order_no      = I_order_no
                  and origin        = 'S'
                  and ((item        = L_item
                        and pack_item is NULL)
                   or (pack_item    = L_item))
                  and location      = loc_rec.location
                  and loc_type      = loc_rec.loc_type
                  and comp_currency = LP_currency_code;

            END LOOP; -- for loc_rec in C_LOC loop
         else -- I_location is not NULL
            if not DEFAULT_EXPENSES(O_error_message,
                                    I_order_no,
                                    L_supplier,
                                    L_item,
                                    I_component_item,
                                    I_location,
                                    I_loc_type,
                                    L_exchange_type,
                                    L_zone_group_id) then 
               return FALSE;
            end if;
            ---
            if LP_buyer_pack = 'N' then
               L_table := 'ORDLOC_EXP';
               SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC_EXP_ITEM','ORDLOC_EXP','Order_no: '||to_char(I_order_no));
               open C_LOCK_ORDLOC_EXP_ITEM(L_item);
               SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC_EXP_ITEM','ORDLOC_EXP','Order_no: '||to_char(I_order_no));
               close C_LOCK_ORDLOC_EXP_ITEM;
               -- Need to update the exchange rate to match the exchange rate for the order
               -- if the expense's currency is the same and the order's currency.

              SQL_LIB.SET_MARK('UPDATE',NULL,'ORDLOC_EXP','Order_no: '||to_char(I_order_no));
               update ordloc_exp 
                  set exchange_rate = LP_exchange_rate
                where order_no      = I_order_no
                  and origin        = 'S'
                  and item          = L_item
                  and location      = I_location
                  and loc_type      = I_loc_type
                  and comp_currency = LP_currency_code;
            end if;
         end if;
         ---
         -- Need to calculate all of the expenses.
         if LP_buyer_pack = 'Y' then
            -- Pass the Pack number (I_item) in the input parameter I_pack_item.
            -- Leave the input parameter I_item NULL so that the expenses of ALL the
            -- component items of the pack get calculated.
            if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                      'PE',
                                      NULL, -- item
                                      L_supplier,
                                      NULL, -- item_exp_type
                                      NULL, -- item_exp_seq
                                      I_order_no,
                                      NULL, -- ord_seq_no
                                      L_item, -- pack_item
                                      NULL, -- zone_id
                                      I_location,
                                      NULL, -- hts
                                      NULL, -- import_country_id
                                      LP_origin_country_id,
                                      NULL, -- effect_from
                                      NULL, -- effect_to
                                      I_import_ord_ind) = FALSE then
               return FALSE;
            end if;
         else
            -- Pass the item number (L_item) in the input parameter I_item.
            if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                      'PE',
                                      L_item,
                                      L_supplier,
                                      NULL, -- item_exp_type
                                      NULL, -- item_exp_seq
                                      I_order_no,
                                      NULL, -- ord_seq_no
                                      NULL, -- pack_item
                                      NULL, -- zone_id
                                      I_location,
                                      NULL, -- hts
                                      NULL, -- import_country_id
                                      LP_origin_country_id,
                                      NULL, -- effect_from
                                      NULL, -- effect_to
                                      I_import_ord_ind) = FALSE then
               return FALSE;
            end if;
         end if;  -- L_buyer_pack 
      END LOOP;  -- C_GET_ORD_ITEMS LOOP
   end if;  -- I_item is NOT NULL
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_COST_COMP;
----------------------------------------------------------------------------------------
FUNCTION DEFAULT_EXPENSES(O_error_message     IN OUT VARCHAR2,
                          I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                          I_supplier          IN     SUPS.SUPPLIER%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_component_item    IN     ITEM_MASTER.ITEM%TYPE,
                          I_location          IN     ORDLOC.LOCATION%TYPE,
                          I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                          I_exchange_type     IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                          I_zone_group_id     IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE) 
   RETURN BOOLEAN IS

-- Internal function, called only by insert_cost_comp of this package

   L_program              VARCHAR2(64)          := 'ORDER_EXPENSE_SQL.DEFAULT_EXPENSES';
   L_exists               VARCHAR2(1)           := 'N';
   L_first_time           VARCHAR2(1)           := 'Y';
   L_get_base_exp         VARCHAR2(1)           := 'Y';
   L_records_inserted     VARCHAR2(1)           := 'Y';
   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_seq_no               ORDLOC_EXP.SEQ_NO%TYPE;
   L_zone_group_id        COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   L_zone_id              COST_ZONE.ZONE_ID%TYPE;
   ---
   L_table                VARCHAR2(30);
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);

   cursor C_ORDLOC_EXP_PACK is
      select 'Y'
        from ordloc_exp exp
       where exp.order_no    = I_order_no
         and exp.item      = L_item 
         and exp.pack_item = I_item
         and exp.location        = I_location
         and exp.loc_type        = I_loc_type
         and nvl(exp.origin, 'S') = 'S';

   cursor C_ORDLOC_EXP_NONPACK is
      select 'Y'
        from ordloc_exp exp
       where exp.order_no    = I_order_no
         and exp.item  = I_item 
         and exp.pack_item is NULL
         and exp.location        = I_location
         and exp.loc_type        = I_loc_type
         and nvl(exp.origin, 'S') = 'S';

   cursor C_SEQ_NO is
      select NVL(MAX(seq_no), 0) + 1
        from ordloc_exp
       where order_no = I_order_no;

   cursor C_PACKITEM is
      select item
        from v_packsku_qty
       where pack_no = I_item;
BEGIN
   if LP_buyer_pack = 'N' then
      if ITEM_ATTRIB_SQL.GET_COST_ZONE(O_error_message,
                                       L_zone_id,
                                       I_item,
                                       I_zone_group_id,
                                       I_location) = FALSE then
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',  'C_ORDLOC_EXP_NONPACK','ORDLOC_EXP', NULL);
      open  C_ORDLOC_EXP_NONPACK;
      SQL_LIB.SET_MARK('FETCH', 'C_ORDLOC_EXP_NONPACK','ORDLOC_EXP', NULL);
      fetch C_ORDLOC_EXP_NONPACK into L_exists;   
      SQL_LIB.SET_MARK('CLOSE', 'C_ORDLOC_EXP_NONPACK','ORDLOC_EXP', NULL);
      close C_ORDLOC_EXP_NONPACK;
      ---
      -- Expenses are defaulted into Orders when Items are added to the order.
      -- Because the same Item could get added to the order at different
      -- times, a check needs to be done to make sure the defaults have
      -- not already been done.  This existence check will prevent defaulting 
      -- for the same Item twice.

      -- Also, for the exchange rate of each component, we want the rate
      -- where the exchange type is 'P' (Purchase Order).  However, the user
      -- is not required to have rates set up for exchange type 'P'.  If there
      -- are no Purchase Order rates, we need to get the Operational or
      -- Consolidation rates depending on the consolidation indicator on the
      -- system options table.  This is why there are two insert select statements.

      if L_exists = 'N' then
         SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
         open C_SEQ_NO;
         SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
         fetch C_SEQ_NO into L_seq_no;
         SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
         close C_SEQ_NO;
         ---
         -- Both the Zone and Country functions get called twice, the first time they will 
         -- insert all components where the components currency exists on the currency_rates 
         -- table with an exchange_type of 'P' (Purchase Order).  The second call to the functions 
         -- will insert the components that didn't have an exchange_rate with exchange_type
         -- of 'P', instead we will use 'C'(Consolidation) or 'O'(Operational) depending
         -- on the system options consolidation indicator.  We have separated these inserts
         -- for performance reasons.
         ---
         -- INSERT ZONE LEVEL EXPENSES
         ---
         if INSERT_ZONE_COMPS(O_error_message,
                              L_records_inserted,
                              I_order_no,
                              L_seq_no,
                              I_item,
                              NULL,
                              I_supplier,
                              I_location,
                              I_loc_type,
                              I_zone_group_id,
                              L_zone_id,
                              I_exchange_type) = FALSE then
            return FALSE;
         end if;
         ---
         -- If there were no records inserted from either of these function
         -- calls, then we need to insert 'Base' Expenses.
         ---
         if (L_records_inserted = 'N') then
            L_records_inserted := 'Y';
            if INSERT_BASE_ZONE_COMPS(O_error_message,
                                      L_records_inserted,
                                      I_order_no,
                                      L_seq_no,
                                      I_item,
                                      NULL,
                                      I_supplier,
                                      I_location,
                                      I_loc_type,
                                      I_zone_group_id,
                                      L_zone_id,
                                      I_exchange_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if (L_records_inserted = 'Y') then
            SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            open C_SEQ_NO;
            SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            fetch C_SEQ_NO into L_seq_no;
            SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            close C_SEQ_NO;
         end if;
         ---
         -- INSERT COUNTRY LEVEL EXPENSES
         ---
         L_records_inserted := 'Y';
         if INSERT_CTRY_COMPS(O_error_message,
                              L_records_inserted,
                              I_order_no,
                              L_seq_no,
                              I_item,
                              NULL,
                              I_supplier,
                              I_location,
                              I_loc_type,
                              I_zone_group_id,
                              L_zone_id,
                              I_exchange_type) = FALSE then
            return FALSE;
         end if;
         ---
         -- If there were no records inserted from either of these function
         -- calls, then we need to insert 'Base' Expenses.
         ---
         if (L_records_inserted = 'N') then
            L_records_inserted := 'Y';
            if INSERT_BASE_CTRY_COMPS(O_error_message,
                                      L_records_inserted,
                                      I_order_no,
                                      L_seq_no,
                                      I_item,
                                      NULL,
                                      I_supplier,
                                      I_location,
                                      I_loc_type,
                                      I_zone_group_id,
                                      L_zone_id,
                                      I_exchange_type) = FALSE then 
               return FALSE;
            end if;
         end if;
         ---
         -- INSERT FACTORY PROFILES
         ---
         if (L_records_inserted = 'Y') then
            SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            open C_SEQ_NO;
            SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            fetch C_SEQ_NO into L_seq_no;
            SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            close C_SEQ_NO;
         end if;
         ---
         if LP_factory is not NULL then
            L_records_inserted := 'Y';
            ---
            if INSERT_PTNR_COMPS(O_error_message,
                                 L_records_inserted,
                                 I_order_no,
                                 L_seq_no,
                                 I_item,
                                 NULL,
                                 I_supplier,
                                 I_location,
                                 I_loc_type,
                                 I_zone_group_id,
                                 L_zone_id,
                                 I_exchange_type,
                                 'FA',
                                 LP_factory) = FALSE then
               return FALSE;
            end if;
            ---
            -- If there were no records inserted from either of these function
            -- calls, then we need to insert 'Base' Expense Profiles.
            ---
            if (L_records_inserted = 'N') then
               L_records_inserted := 'Y';
               ---
               if INSERT_BASE_PTNR_COMPS(O_error_message,
                                         L_records_inserted,
                                         I_order_no,
                                         L_seq_no,
                                         I_item,
                                         NULL,
                                         I_supplier,
                                         I_location,
                                         I_loc_type,
                                         I_zone_group_id,
                                         L_zone_id,
                                         I_exchange_type,
                                         'FA',
                                         LP_factory) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
         ---
         -- INSERT AGENT PROFILES
         ---
         if (L_records_inserted = 'Y') then
            SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            open C_SEQ_NO;
            SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            fetch C_SEQ_NO into L_seq_no;
            SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            close C_SEQ_NO;
         end if;
         ---
         if LP_agent is not NULL then
            L_records_inserted := 'Y';
            ---
            if INSERT_PTNR_COMPS(O_error_message,
                                 L_records_inserted,
                                 I_order_no,
                                 L_seq_no,
                                 I_item,
                                 NULL,
                                 I_supplier,
                                 I_location, 
                                 I_loc_type,
                                 I_zone_group_id,
                                 L_zone_id,
                                 I_exchange_type,
                                 'AG',
                                 LP_agent) = FALSE then
               return FALSE;
            end if;
            ---
            -- If there were no records inserted from either of these function
            -- calls, then we need to insert 'Base' Expense Profiles.
            ---
            if (L_records_inserted = 'N') then
               L_records_inserted := 'Y';
               ---
               if INSERT_BASE_PTNR_COMPS(O_error_message,
                                         L_records_inserted,
                                         I_order_no,
                                         L_seq_no,
                                         I_item,
                                         NULL,
                                         I_supplier,
                                         I_location,
                                         I_loc_type,
                                         I_zone_group_id,
                                         L_zone_id,
                                         I_exchange_type,
                                         'AG',
                                         LP_agent) = FALSE then
                  return FALSE;
               end if;
            end if;  -- if no matches were found then insert base
         end if;  -- LP_agent is not NULL
         ---
         if (L_records_inserted = 'Y') then
            SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            open C_SEQ_NO;
            SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            fetch C_SEQ_NO into L_seq_no;
            SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            close C_SEQ_NO;
         end if;
         ---
         if LP_partner1 is not NULL then
            L_records_inserted := 'Y';
            ---
            if INSERT_PTNR_COMPS(O_error_message,
                                 L_records_inserted,
                                 I_order_no,
                                 L_seq_no,
                                 I_item,
                                 NULL,
                                 I_supplier,
                                 I_location, 
                                 I_loc_type,
                                 I_zone_group_id,
                                 L_zone_id,
                                 I_exchange_type,
                                 LP_partner_type_1,
                                 LP_partner1) = FALSE then
               return FALSE;
            end if;
            ---
            -- If there were no records inserted from either of these function
            -- calls, then we need to insert 'Base' Expense Profiles.
            ---
            if (L_records_inserted = 'N') then
               L_records_inserted := 'Y';
               ---
               if INSERT_BASE_PTNR_COMPS(O_error_message,
                                         L_records_inserted,
                                         I_order_no,
                                         L_seq_no,
                                         I_item,
                                         NULL,
                                         I_supplier,
                                         I_location,
                                         I_loc_type,
                                         I_zone_group_id,
                                         L_zone_id,
                                         I_exchange_type,
                                         LP_partner_type_1,
                                         LP_partner1) = FALSE then
                  return FALSE;
               end if;
            end if;  -- if no matches were found then insert base
         end if;  -- LP_agent is not NULL
         --
         if (L_records_inserted = 'Y') then
            SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            open C_SEQ_NO;
            SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            fetch C_SEQ_NO into L_seq_no;
            SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            close C_SEQ_NO;
         end if;
         ---
    if LP_partner2 is not NULL then
            L_records_inserted := 'Y';
            ---
            if INSERT_PTNR_COMPS(O_error_message,
                                 L_records_inserted,
                                 I_order_no,
                                 L_seq_no,
                                 I_item,
                                 NULL,
                                 I_supplier,
                                 I_location, 
                                 I_loc_type,
                                 I_zone_group_id,
                                 L_zone_id,
                                 I_exchange_type,
                                 LP_partner_type_2,
                                 LP_partner2) = FALSE then
               return FALSE;
            end if;
            ---
            -- If there were no records inserted from either of these function
            -- calls, then we need to insert 'Base' Expense Profiles.
            ---
            if (L_records_inserted = 'N') then
               L_records_inserted := 'Y';
               ---
               if INSERT_BASE_PTNR_COMPS(O_error_message,
                                         L_records_inserted,
                                         I_order_no,
                                         L_seq_no,
                                         I_item,
                                         NULL,
                                         I_supplier,
                                         I_location,
                                         I_loc_type,
                                         I_zone_group_id,
                                         L_zone_id,
                                         I_exchange_type,
                                         LP_partner_type_2,
                                         LP_partner2) = FALSE then
                  return FALSE;
               end if;
            end if;  -- if no matches were found then insert base
         end if;  -- LP_agent is not NULL
         --
         if (L_records_inserted = 'Y') then
            SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            open C_SEQ_NO;
            SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            fetch C_SEQ_NO into L_seq_no;
            SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            close C_SEQ_NO;
         end if;
         ---
    if LP_partner3 is not NULL then
            L_records_inserted := 'Y';
            ---
            if INSERT_PTNR_COMPS(O_error_message,
                                 L_records_inserted,
                                 I_order_no,
                                 L_seq_no,
                                 I_item,
                                 NULL,
                                 I_supplier,
                                 I_location, 
                                 I_loc_type,
                                 I_zone_group_id,
                                 L_zone_id,
                                 I_exchange_type,
                                 LP_partner_type_3,
                                 LP_partner3) = FALSE then
               return FALSE;
            end if;
            ---
            -- If there were no records inserted from either of these function
            -- calls, then we need to insert 'Base' Expense Profiles.
            ---
            if (L_records_inserted = 'N') then
               L_records_inserted := 'Y';
               ---
               if INSERT_BASE_PTNR_COMPS(O_error_message,
                                         L_records_inserted,
                                         I_order_no,
                                         L_seq_no,
                                         I_item,
                                         NULL,
                                         I_supplier,
                                         I_location,
                                         I_loc_type,
                                         I_zone_group_id,
                                         L_zone_id,
                                         I_exchange_type,
                                         LP_partner_type_3,
                                         LP_partner3) = FALSE then
                  return FALSE;
               end if;
            end if;  -- if no matches were found then insert base
         end if;  -- LP_agent is not NULL
         --
         -- INSERT THE TEXP COMPONENT
         ---
         if (L_records_inserted = 'Y') then
            SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            open C_SEQ_NO;
            SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            fetch C_SEQ_NO into L_seq_no;
            SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            close C_SEQ_NO;
         end if;
         ---
         L_records_inserted := 'Y';
         ---
         if INSERT_TEXP(O_error_message,
                        L_records_inserted,
                        I_order_no,
                        L_seq_no,
                        I_item,
                        NULL,
                        I_supplier,
                        I_location,
                        I_loc_type,
                        I_zone_group_id,
                        L_zone_id,
                        I_exchange_type) = FALSE then
            return FALSE;
         end if;
      end if; -- if L_exists = 'N'
   else  -- buyer pack
      if I_component_item is NULL then
         FOR P1 in C_PACKITEM LOOP
            L_item := P1.item;
            ---
            -- When looping through pack component items, if the first one exists
            -- on ORDLOC_EXP for the given pack_no, then the entire pack must also
            -- exist on the table, therefore only need to check for existence one
            -- time.
            ---
            if L_first_time = 'Y' then
               L_first_time := 'N';
               ---
               SQL_LIB.SET_MARK('OPEN',  'C_ORDLOC_EXP_PACK','ORDLOC_EXP', NULL);
               open  C_ORDLOC_EXP_PACK;
               SQL_LIB.SET_MARK('FETCH', 'C_ORDLOC_EXP_PACK','ORDLOC_EXP', NULL);
               fetch C_ORDLOC_EXP_PACK into L_exists;   
               SQL_LIB.SET_MARK('CLOSE', 'C_ORDLOC_EXP_PACK','ORDLOC_EXP', NULL);
               close C_ORDLOC_EXP_PACK;
               ---
               if L_exists = 'Y' then
                  exit;
               end if;  
            end if;
            ---           
            SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            open C_SEQ_NO;
            SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            fetch C_SEQ_NO into L_seq_no;
            SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            close C_SEQ_NO;
            ---
            if PACKITEM_EXPENSES(O_error_message,
                 I_order_no,
                 I_supplier,
                 I_item,
                 L_item,
                 L_seq_no,
                 I_location,
                 I_loc_type,
                 I_exchange_type) = FALSE then 
               return FALSE;
            end if;
         END LOOP;
      else  -- I_component_item is not NULL and only one record
            -- should be inserted, instead of all records on pack.

         SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
         open C_SEQ_NO;
         SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
         fetch C_SEQ_NO into L_seq_no;
         SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
         close C_SEQ_NO;
      ---         
         if PACKITEM_EXPENSES(O_error_message,
                 I_order_no,
                 I_supplier,
                 I_item,
                 I_component_item,
                 L_seq_no,
                 I_location,
                 I_loc_type,
                 I_exchange_type) = FALSE then 
            return FALSE;
         end if;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_EXPENSES;
----------------------------------------------------------------------------------------
FUNCTION PACKITEM_EXPENSES(O_error_message     IN OUT VARCHAR2,
                           I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                           I_supplier          IN     SUPS.SUPPLIER%TYPE,
                           I_item              IN     ITEM_MASTER.ITEM%TYPE,
                           I_component_item    IN     ITEM_MASTER.ITEM%TYPE,
                           I_seq_no            IN     ORDLOC_EXP.SEQ_NO%TYPE,
                           I_location          IN     ORDLOC.LOCATION%TYPE,
                           I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                           I_exchange_type     IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE) 
   RETURN BOOLEAN IS

-- Internal function, called only by default_expenses of this package

   L_program              VARCHAR2(64)           := 'ORDER_EXPENSE_SQL.PACKITEM_EXPENSES';
   L_records_inserted     VARCHAR2(1)            := 'Y';
   L_item                 ITEM_MASTER.ITEM%TYPE  := I_component_item;
   L_seq_no               ORDLOC_EXP.SEQ_NO%TYPE := I_seq_no;
   L_zone_group_id        COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   L_zone_id              COST_ZONE.ZONE_ID%TYPE;
   ---
   L_table              VARCHAR2(30);
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ORDLOC_EXP is
      select 'x'
        from ordloc_exp
       where order_no      = I_order_no
         and origin        = 'S'
         and item          = L_item
         and pack_item     = I_item
         and comp_currency = LP_currency_code
         for update nowait;

   cursor C_SEQ_NO is
      select NVL(MAX(seq_no), 0) + 1
        from ordloc_exp
       where order_no = I_order_no;
BEGIN 
   if ITEM_ATTRIB_SQL.GET_COST_ZONE_GROUP(O_error_message,
                                          L_zone_group_id,
                                          L_item) = FALSE then
      return FALSE;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_COST_ZONE(O_error_message,
                                    L_zone_id,
                                    L_item,
                                    L_zone_group_id,
                                    I_location) = FALSE then
      return FALSE;
   end if;
   ---
   if INSERT_ZONE_COMPS(O_error_message,
                        L_records_inserted,
                        I_order_no,
                        L_seq_no,
                        L_item,
                        I_item,
                        I_supplier,
                        I_location,
                        I_loc_type,
                        L_zone_group_id,
                        L_zone_id,
                        I_exchange_type) = FALSE then
      return FALSE;
   end if;
   -- If there were no records inserted from either of these function
   -- calls, then we need to insert 'Base' Expenses.
   ---
   if (L_records_inserted = 'N') then
      L_records_inserted := 'Y';
      ---
      if INSERT_BASE_ZONE_COMPS(O_error_message,
                                L_records_inserted,
                                I_order_no,
                                L_seq_no,
                                L_item,
                                I_item,
                                I_supplier,
                                I_location,
                                I_loc_type,
                                L_zone_group_id,
                                L_zone_id,
                                I_exchange_type) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   -- INSERT COUNTRY LEVEL EXPENSES
   ---
   if (L_records_inserted = 'Y') then
      SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      open C_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      fetch C_SEQ_NO into L_seq_no;
      SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      close C_SEQ_NO;
   end if;
   ---
   L_records_inserted := 'Y';
   ---
   if INSERT_CTRY_COMPS(O_error_message,
                        L_records_inserted,
                        I_order_no,
                        L_seq_no,
                        L_item,
                        I_item,
                        I_supplier,
                        I_location,
                        I_loc_type,
                        L_zone_group_id,
                        L_zone_id,
                        I_exchange_type) = FALSE then
      return FALSE;
   end if;
   -- If there were no records inserted from either of these function
   -- calls, then we need to insert 'Base' Expenses.
   ---
   if (L_records_inserted = 'N') then
      L_records_inserted := 'Y';
      ---
      if INSERT_BASE_CTRY_COMPS(O_error_message,
                                L_records_inserted,
                                I_order_no,
                                L_seq_no,
                                L_item,
                                I_item,
                                I_supplier,
                                I_location,
                                I_loc_type,
                                L_zone_group_id,
                                L_zone_id,
                                I_exchange_type) = FALSE then 
         return FALSE;
      end if;
   end if;
   ---
   -- INSERT FACTORY PROFILES
   ---
   if (L_records_inserted = 'Y') then
      SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      open C_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      fetch C_SEQ_NO into L_seq_no;
      SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      close C_SEQ_NO;
   end if;
   ---
   if LP_factory is not NULL then
      L_records_inserted := 'Y';
      ---
      if INSERT_PTNR_COMPS(O_error_message,
                           L_records_inserted,
                           I_order_no,
                           L_seq_no,
                           L_item,
                           I_item,
                           I_supplier,
                           I_location,
                           I_loc_type,
                           L_zone_group_id,
                           L_zone_id,
                           I_exchange_type,
                           'FA',
                           LP_factory) = FALSE then
         return FALSE;
      end if;
      ---
      -- If there were no records inserted from either of these function
      -- calls, then we need to insert 'Base' Expense Profiles.
      ---
      if (L_records_inserted = 'N') then
         L_records_inserted := 'Y';
         ---
         if INSERT_BASE_PTNR_COMPS(O_error_message,
                                   L_records_inserted,
                                   I_order_no,
                                   L_seq_no,
                                   L_item,
                                   I_item,
                                   I_supplier,
                                   I_location,
                                   I_loc_type,
                                   L_zone_group_id,
                                   L_zone_id,
                                   I_exchange_type,
                                   'FA',
                                   LP_factory) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;  -- LP_factory is not NULL
   ---
   -- INSERT AGENT PROFILES
   ---
   if (L_records_inserted = 'Y') then
      SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      open C_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      fetch C_SEQ_NO into L_seq_no;
      SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      close C_SEQ_NO;
   end if;
   ---
   if LP_agent is not NULL then
      L_records_inserted := 'Y';
      ---
      if INSERT_PTNR_COMPS(O_error_message,
                           L_records_inserted,
                           I_order_no,
                           L_seq_no,
                           L_item,
                           I_item,
                           I_supplier,
                           I_location,
                           I_loc_type,
                           L_zone_group_id,
                           L_zone_id,
                           I_exchange_type,
                           'AG',
                           LP_agent) = FALSE then
         return FALSE;
      end if;
      ---
      -- If there were no records inserted from either of these function
      -- calls, then we need to insert 'Base' Expense Profiles.
      ---
      if (L_records_inserted = 'N') then
         L_records_inserted := 'Y';
         ---
         if INSERT_BASE_PTNR_COMPS(O_error_message,
                                   L_records_inserted,
                                   I_order_no,
                                   L_seq_no,
                                   L_item,
                                   I_item,
                                   I_supplier,
                                   I_location,
                                   I_loc_type,
                                   L_zone_group_id,
                                   L_zone_id,
                                   I_exchange_type,
                                   'AG',
                                   LP_agent) = FALSE then
            return FALSE;
         end if;
      end if;  -- if no matches were found then insert base
   end if;   -- LP_agent is not NULL
   
   ---
   if (L_records_inserted = 'Y') then
      SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      open C_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      fetch C_SEQ_NO into L_seq_no;
      SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      close C_SEQ_NO;
   end if;
   ---
   
   if LP_partner1 is not NULL then
      L_records_inserted := 'Y';
            ---
      if INSERT_PTNR_COMPS(O_error_message,
                           L_records_inserted,
                           I_order_no,
                           L_seq_no,
                           L_item,
                           I_item,
                           I_supplier,
                           I_location, 
                           I_loc_type,
                           L_zone_group_id,
                           L_zone_id,
                           I_exchange_type,
                           LP_partner_type_1,
                           LP_partner1) = FALSE then
         return FALSE;
      end if;
      ---
      -- If there were no records inserted from either of these function
      -- calls, then we need to insert 'Base' Expense Profiles.
      ---
      if (L_records_inserted = 'N') then
          L_records_inserted := 'Y';
          ---
          if INSERT_BASE_PTNR_COMPS(O_error_message,
                                    L_records_inserted,
                                    I_order_no,
                                    L_seq_no,
                                    L_item,
                                    I_item,
                                    I_supplier,
                                    I_location,
                                    I_loc_type,
                                    L_zone_group_id,
                                    L_zone_id,
                                    I_exchange_type,
                                    LP_partner_type_1,
                                    LP_partner1) = FALSE then
             return FALSE;
          end if;
      end if;  -- if no matches were found then insert base
   end if;  -- LP_partner1 is not NULL
   --
   if (L_records_inserted = 'Y') then
      SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      open C_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      fetch C_SEQ_NO into L_seq_no;
      SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      close C_SEQ_NO;
   end if;
   ---
   if LP_partner2 is not NULL then
      L_records_inserted := 'Y';
      ---
      if INSERT_PTNR_COMPS(O_error_message,
                           L_records_inserted,
                           I_order_no,
                           L_seq_no,
                           L_item,
                           I_item,
                           I_supplier,
                           I_location, 
                           I_loc_type,
                           L_zone_group_id,
                           L_zone_id,
                           I_exchange_type,
                           LP_partner_type_2,
                           LP_partner2) = FALSE then
         return FALSE;
      end if;
      ---
      -- If there were no records inserted from either of these function
      -- calls, then we need to insert 'Base' Expense Profiles.
      ---
      if (L_records_inserted = 'N') then
          L_records_inserted := 'Y';
      ---
         if INSERT_BASE_PTNR_COMPS(O_error_message,
                                   L_records_inserted,
                                   I_order_no,
                                   L_seq_no,
                                   L_item,
                                   I_item,
                                   I_supplier,
                                   I_location,
                                   I_loc_type,
                                   L_zone_group_id,
                                   L_zone_id,
                                   I_exchange_type,
                                   LP_partner_type_2,
                                   LP_partner2) = FALSE then
            return FALSE;
         end if;
      end if;  -- if no matches were found then insert base
   end if;  -- LP_partner2 is not NULL
   --
   if (L_records_inserted = 'Y') then
      SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      open C_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      fetch C_SEQ_NO into L_seq_no;
      SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      close C_SEQ_NO;
   end if;
   ---
   if LP_partner3 is not NULL then
      L_records_inserted := 'Y';
      ---
      if INSERT_PTNR_COMPS(O_error_message,
                           L_records_inserted,
                           I_order_no,
                           L_seq_no,
                           L_item,
                           I_item,
                           I_supplier,
                           I_location, 
                           I_loc_type,
                           L_zone_group_id,
                           L_zone_id,
                           I_exchange_type,
                           LP_partner_type_3,
                           LP_partner3) = FALSE then
         return FALSE;
      end if;
      ---
      -- If there were no records inserted from either of these function
      -- calls, then we need to insert 'Base' Expense Profiles.
      ---
      if (L_records_inserted = 'N') then
          L_records_inserted := 'Y';
          ---
          if INSERT_BASE_PTNR_COMPS(O_error_message,
                                    L_records_inserted,
                                    I_order_no,
                                    L_seq_no,
                                    L_item,
                                    I_item,
                                    I_supplier,
                                    I_location,
                                    I_loc_type,
                                    L_zone_group_id,
                                    L_zone_id,
                                    I_exchange_type,
                                    LP_partner_type_3,
                                    LP_partner3) = FALSE then
                  return FALSE;
          end if;
      end if;  -- if no matches were found then insert base
   end if;  -- LP_partner3 is not NULL
   --
   ---
   -- INSERT THE TEXP COMPONENT
   ---
   if (L_records_inserted = 'Y') then
      SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      open C_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      fetch C_SEQ_NO into L_seq_no;
      SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      close C_SEQ_NO;
   end if;
   ---
   L_records_inserted := 'Y';
   ---
   if INSERT_TEXP(O_error_message,
                  L_records_inserted,
                  I_order_no,
                  L_seq_no,
                  L_item,
                  I_item,
                  I_supplier,
                  I_location,
                  I_loc_type,
                  L_zone_group_id,
                  L_zone_id,
                  I_exchange_type) = FALSE then
      return FALSE;
   end if;
   ---
   -- Need to update the exchange rate to match the exchange rate for the order
   -- if the expense's currency is the same and the order's currency.
   --
   L_table := 'ORDLOC_EXP';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC_EXP','ORDLOC_EXP','Order_no: '||to_char(I_order_no));
   open C_LOCK_ORDLOC_EXP;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC_EXP','ORDLOC_EXP','Order_no: '||to_char(I_order_no));
   close C_LOCK_ORDLOC_EXP;
   ---
   SQL_LIB.SET_MARK('UPDATE',NULL,'ORDLOC_EXP','Order_no: '||to_char(I_order_no));
   update ordloc_exp 
      set exchange_rate = LP_exchange_rate
    where order_no      = I_order_no
      and origin        = 'S'
      and item          = L_item
      and pack_item     = I_item
      and comp_currency = LP_currency_code;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PACKITEM_EXPENSES;
----------------------------------------------------------------------------------------
FUNCTION DEFAULT_BACK_EXPENSES(O_error_message     IN OUT VARCHAR2,
                               I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                               I_supplier          IN     SUPS.SUPPLIER%TYPE,
                               I_item              IN     ITEM_MASTER.ITEM%TYPE,
                               I_component_item    IN     ITEM_MASTER.ITEM%TYPE,
                               I_location          IN     ORDLOC.LOCATION%TYPE,
                               I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                               I_exchange_type     IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                               I_zone_group_id     IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE)
   RETURN BOOLEAN IS

-- Internal function, called only by insert_cost_comp of this package

   L_program              VARCHAR2(64)          := 'ORDER_EXPENSE_SQL.DEFAULT_BACK_EXPENSES';
   L_exists               VARCHAR2(1)           := 'N';
   L_first_time           VARCHAR2(1)           := 'Y';
   L_get_base_exp         VARCHAR2(1)           := 'Y';
   L_records_inserted     VARCHAR2(1)           := 'Y';
   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_seq_no               ORDLOC_EXP.SEQ_NO%TYPE;
   L_zone_group_id        COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   L_zone_id              COST_ZONE.ZONE_ID%TYPE;
   ---
   L_table                VARCHAR2(30);
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ORDLOC_EXP is
      select 'x'
        from ordloc_exp
       where order_no      = I_order_no
         and origin        = 'S'
         and item          = L_item
         and pack_item     = I_item
         and comp_currency = LP_currency_code
         for update nowait;

   cursor C_SEQ_NO is
      select NVL(MAX(seq_no), 0) + 1
        from ordloc_exp
       where order_no = I_order_no;

   cursor C_PACKITEM is
      select item
        from v_packsku_qty
       where pack_no = I_item;
BEGIN
   if LP_buyer_pack = 'N' then
      if ITEM_ATTRIB_SQL.GET_COST_ZONE(O_error_message,
                                       L_zone_id,
                                       I_item,
                                       I_zone_group_id,
                                       I_location) = FALSE then
         return FALSE;
      end if;
      ---
      -- Also, for the exchange rate of each component, we want the rate
      -- where the exchange type is 'P' (Purchase Order).  However, the user
      -- is not required to have rates set up for exchange type 'P'.  If there
      -- are no Purchase Order rates, we need to get the Operational or
      -- Consolidation rates depending on the consolidation indicator on the
      -- system options table.  This is why there are two insert select statements.
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      open C_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      fetch C_SEQ_NO into L_seq_no;
      SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      close C_SEQ_NO;
      ---
      -- The Zone functions get called twice, the first time they will insert all components 
      -- where the components currency exists on the currency_rates table with an 
      -- exchange_type of 'P' (Purchase Order).  The second call to the functions 
      -- will insert the components that didn't have an exchange_rate with exchange_type
      -- of 'P', instead we will use 'C'(Consolidation) or 'O'(Operational) depending
      -- on the system options consolidation indicator.  We have separated these inserts
      -- for performance reasons.
      ---
      -- INSERT BACKHAUL EXPENSES
      ---
      if INSERT_ZONE_COMPS(O_error_message,
                           L_records_inserted,
                           I_order_no,
                           L_seq_no,
                           I_item,
                           NULL,
                           I_supplier,
                           I_location,
                           I_loc_type,
                           I_zone_group_id,
                           L_zone_id,
                           I_exchange_type) = FALSE then
         return FALSE;
      end if;
      ---
      -- If there were no records inserted from either of these function
      -- calls, then we need to insert 'Base' Expenses.
      ---
      if (L_records_inserted = 'N') then
          L_records_inserted := 'Y';
         if INSERT_BASE_ZONE_COMPS(O_error_message,
                                   L_records_inserted,
                                   I_order_no,
                                   L_seq_no,
                                   I_item,
                                   NULL,
                                   I_supplier,
                                   I_location,
                                   I_loc_type,
                                   I_zone_group_id,
                                   L_zone_id,
                                   I_exchange_type) = FALSE then
            return FALSE;
         end if;
      end if;
   else  -- buyer pack
      if I_component_item is NULL then
         FOR P1 in C_PACKITEM LOOP
            L_item := P1.item;
            ---           
            SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            open C_SEQ_NO;
            SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            fetch C_SEQ_NO into L_seq_no;
            SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
            close C_SEQ_NO;
            ---
            if PACKITEM_BACK_EXPENSES(O_error_message,
                                      I_order_no,
                                      I_supplier,
                                      I_item,
                                      L_item,
                                      L_seq_no,
                                      I_location,
                                      I_loc_type,
                                      I_exchange_type) = FALSE then
               return FALSE;
            end if;
         END LOOP;
      else  -- I_component_item is not NULL and only one record
            -- should be inserted, instead of all records on pack.

         SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
         open C_SEQ_NO;
         SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
         fetch C_SEQ_NO into L_seq_no;
         SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
         close C_SEQ_NO;
      ---
         if PACKITEM_BACK_EXPENSES(O_error_message,
                                   I_order_no,
                                   I_supplier,
                                   I_item,
                                   I_component_item,
                                   L_seq_no,
                                   I_location,
                                   I_loc_type,
                                   I_exchange_type) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_BACK_EXPENSES;
----------------------------------------------------------------------------------------
FUNCTION PACKITEM_BACK_EXPENSES(O_error_message     IN OUT VARCHAR2,
                                I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                I_supplier          IN     SUPS.SUPPLIER%TYPE,
                                I_item              IN     ITEM_MASTER.ITEM%TYPE,
                                I_component_item    IN     ITEM_MASTER.ITEM%TYPE,
                                I_seq_no            IN     ORDLOC_EXP.SEQ_NO%TYPE,
                                I_location          IN     ORDLOC.LOCATION%TYPE,
                                I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                                I_exchange_type     IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN IS

-- Internal function, called only by default_back_expenses of this package

   L_program              VARCHAR2(64)           := 'ORDER_EXPENSE_SQL.PACKITEM_BACK_EXPENSES';
   L_records_inserted     VARCHAR2(1)            := 'Y';
   L_item                 ITEM_MASTER.ITEM%TYPE  := I_component_item;
   L_seq_no               ORDLOC_EXP.SEQ_NO%TYPE := I_seq_no;
   L_zone_group_id        COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   L_zone_id              COST_ZONE.ZONE_ID%TYPE;
   ---
   L_table              VARCHAR2(30);
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ORDLOC_EXP is
      select 'x'
        from ordloc_exp
       where order_no      = I_order_no
         and origin        = 'S'
         and item          = L_item
         and pack_item     = I_item
         and comp_currency = LP_currency_code
         for update nowait;

   cursor C_SEQ_NO is
      select NVL(MAX(seq_no), 0) + 1
        from ordloc_exp
       where order_no = I_order_no;
BEGIN 
   if ITEM_ATTRIB_SQL.GET_COST_ZONE_GROUP(O_error_message,
                                          L_zone_group_id,
                                          L_item) = FALSE then
      return FALSE;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_COST_ZONE(O_error_message,
                                    L_zone_id,
                                    L_item,
                                    L_zone_group_id,
                                    I_location) = FALSE then
      return FALSE;
   end if;
   ---
   if INSERT_ZONE_COMPS(O_error_message,
                        L_records_inserted,
                        I_order_no,
                        L_seq_no,
                        L_item,
                        I_item,
                        I_supplier,
                        I_location,
                        I_loc_type,
                        L_zone_group_id,
                        L_zone_id,
                        I_exchange_type) = FALSE then
      return FALSE;
   end if;
   -- If there were no records inserted from either of these function
   -- calls, then we need to insert 'Base' Expenses.
   ---
   if (L_records_inserted = 'N') then
      L_records_inserted := 'Y';
      ---
      if INSERT_BASE_ZONE_COMPS(O_error_message,
                                L_records_inserted,
                                I_order_no,
                                L_seq_no,
                                L_item,
                                I_item,
                                I_supplier,
                                I_location,
                                I_loc_type,
                                L_zone_group_id,
                                L_zone_id,
                                I_exchange_type) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   -- Need to update the exchange rate to match the exchange rate for the order
   -- if the expense's currency is the same and the order's currency.
   --
   L_table := 'ORDLOC_EXP';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC_EXP','ORDLOC_EXP','Order_no: '||to_char(I_order_no));
   open C_LOCK_ORDLOC_EXP;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC_EXP','ORDLOC_EXP','Order_no: '||to_char(I_order_no));
   close C_LOCK_ORDLOC_EXP;
   ---
   SQL_LIB.SET_MARK('UPDATE',NULL,'ORDLOC_EXP','Order_no: '||to_char(I_order_no));
   update ordloc_exp 
      set exchange_rate = LP_exchange_rate
    where order_no      = I_order_no
      and origin        = 'S'
      and item          = L_item
      and pack_item     = I_item
      and comp_currency = LP_currency_code;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PACKITEM_BACK_EXPENSES;
----------------------------------------------------------------------------------------
FUNCTION INSERT_ZONE_COMPS(O_error_message    IN OUT VARCHAR2,
                           O_records_inserted IN OUT VARCHAR2,
                           I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                           I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                           I_item             IN     ITEM_MASTER.ITEM%TYPE,
                           I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                           I_supplier         IN     SUPS.SUPPLIER%TYPE,
                           I_location         IN     ORDLOC.LOCATION%TYPE,
                           I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                           I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                           I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                           I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'ORDER_EXPENSE_SQL.INSERT_ZONE_COMPS';
   L_dummy     NUMBER := NULL;
   L_exchange_type       CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;
   cursor C_CHECK_FOR_PO_EXCHANGE_TYPE is
      select 1
        from item_exp_head ieh,
             item_exp_detail exp,
             currency_rates cur,
             elc_comp elc
       where ieh.item           = I_item
         and ieh.supplier       = I_supplier
         and exp.comp_id        <> 'TEXPZ'
         and ieh.item_exp_type  = 'Z'
         and ieh.zone_group_id  = I_zone_group_id
         and ieh.zone_id        = I_zone_id
         and ieh.discharge_port = LP_discharge_port 
         and ieh.item           = exp.item
         and ieh.supplier       = exp.supplier
         and ieh.item_exp_seq   = exp.item_exp_seq
         and ieh.item_exp_type  = exp.item_exp_type
         and exp.comp_id        = elc.comp_id
         and ((LP_purchase_type = 'BACK'
         and LP_backhaul_type = 'C')
          or (((LP_purchase_type = 'BACK'
         and LP_backhaul_type = 'F')
          or LP_purchase_type <> 'BACK')
         and elc.exp_category <> 'B'))
         and elc.comp_currency  = cur.currency_code
         and cur.exchange_type  = 'P'
         and cur.effective_date <= LP_written_date
         and rownum = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   open C_CHECK_FOR_PO_EXCHANGE_TYPE;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   fetch C_CHECK_FOR_PO_EXCHANGE_TYPE into L_dummy;
   if C_CHECK_FOR_PO_EXCHANGE_TYPE%FOUND then
      L_exchange_type := 'P';
   else
      L_exchange_type := I_exchange_type;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   close C_CHECK_FOR_PO_EXCHANGE_TYPE;
   O_records_inserted := 'Y';
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDLOC_EXP', NULL);
   insert into ordloc_exp(order_no,
                          seq_no,
                          item,
                          pack_item,
                          location,
                          loc_type,
                          comp_id,
                          cvb_code,
                          cost_basis,
                          comp_rate,
                          comp_currency,
                          exchange_rate,
                          per_count,
                          per_count_uom,
                          est_exp_value,
                          nom_flag_1,
                          nom_flag_2,
                          nom_flag_3,
                          nom_flag_4,
                          nom_flag_5,
                          origin,
                          display_order,
                          defaulted_from,
                          key_value_1,
                          key_value_2)
                  select  I_order_no,
                          I_seq_no + rownum,
                          I_item,
                          I_pack_item,
                          I_location,
                          I_loc_type,
                          exp.comp_id,
                          exp.cvb_code,
                          decode(exp.cvb_code, NULL, NVL(elc.cost_basis, 'S')),
                          exp.comp_rate,
                          exp.comp_currency,
                          cur.exchange_rate,
                          exp.per_count,
                          exp.per_count_uom,
                          exp.est_exp_value,
                          exp.nom_flag_1,
                          exp.nom_flag_2,
                          exp.nom_flag_3,
                          exp.nom_flag_4,
                          exp.nom_flag_5,
                          'S',
                          exp.display_order,
                          exp.defaulted_from,
                          exp.key_value_1,
                          exp.key_value_2
                     from item_exp_head ieh,
                          item_exp_detail exp,
                          currency_rates cur,
                          elc_comp elc
                    where ieh.item           = I_item
                      and ieh.supplier       = I_supplier
                      and exp.comp_id        <> 'TEXPZ'
                      and ieh.item_exp_type  = 'Z'
                      and ieh.zone_group_id  = I_zone_group_id
                      and ieh.zone_id        = I_zone_id
                      and ieh.discharge_port = LP_discharge_port 
                      and ieh.item           = exp.item
                      and ieh.supplier       = exp.supplier
                      and ieh.item_exp_seq   = exp.item_exp_seq
                      and ieh.item_exp_type  = exp.item_exp_type
                      and exp.comp_id        = elc.comp_id
                      and ((LP_purchase_type = 'BACK'
                            and LP_backhaul_type = 'C')
                          or (((LP_purchase_type = 'BACK'
                                and LP_backhaul_type = 'F')
                                or LP_purchase_type <> 'BACK')
                             and elc.exp_category <> 'B'))
                      and exp.comp_currency  = cur.currency_code
                      and cur.exchange_type  = L_exchange_type  -- ('P','C','O')
                      and cur.effective_date = (select max(curr.effective_date)
                                                  from currency_rates curr
                                                 where exp.comp_currency    = curr.currency_code
                                                   and curr.effective_date <= LP_written_date
                                                   and curr.exchange_type    = L_exchange_type)
                      and not exists (select 'x'
                                        from ordloc_exp ord
                                       where ord.order_no      =  I_order_no
                                         and ord.item          =  I_item
                                         and ((ord.pack_item is NULL and I_pack_item is NULL)
                                             or (ord.pack_item = I_pack_item))
                                         and ord.location      = I_location
                                         and ord.comp_id       =  exp.comp_id
                                         and rownum = 1);
   if SQL%NOTFOUND then
      O_records_inserted := 'N';
   end if;
   --- 
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_ZONE_COMPS;
----------------------------------------------------------------------------------------
FUNCTION INSERT_CTRY_COMPS(O_error_message    IN OUT VARCHAR2,
                           O_records_inserted IN OUT VARCHAR2,
                           I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                           I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                           I_item             IN     ITEM_MASTER.ITEM%TYPE,
                           I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                           I_supplier         IN     SUPS.SUPPLIER%TYPE,
                           I_location         IN     ORDLOC.LOCATION%TYPE,
                           I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                           I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                           I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                           I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'ORDER_EXPENSE_SQL.INSERT_CTRY_COMPS';

BEGIN
   O_records_inserted := 'Y';
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDLOC_EXP', NULL);
   insert into ordloc_exp(order_no,
                          seq_no,
                          item,
                          pack_item,
                          location,
                          loc_type,
                          comp_id,
                          cvb_code,
                          cost_basis,
                          comp_rate,
                          comp_currency,
                          exchange_rate,
                          per_count,
                          per_count_uom,
                          est_exp_value,
                          nom_flag_1,
                          nom_flag_2,
                          nom_flag_3,
                          nom_flag_4,
                          nom_flag_5,
                          origin,
                          display_order,
                          defaulted_from,
                          key_value_1,
                          key_value_2)
                  select  I_order_no,
                          I_seq_no + rownum,
                          I_item,
                          I_pack_item,
                          I_location,
                          I_loc_type,
                          exp.comp_id,
                          exp.cvb_code,
                          decode(exp.cvb_code, NULL, NVL(elc.cost_basis, 'S')),
                          exp.comp_rate,
                          exp.comp_currency,
                          cur.exchange_rate,
                          exp.per_count,
                          exp.per_count_uom,
                          exp.est_exp_value,
                          exp.nom_flag_1,
                          exp.nom_flag_2,
                          exp.nom_flag_3,
                          exp.nom_flag_4,
                          exp.nom_flag_5,
                          'S',
                          exp.display_order,
                          exp.defaulted_from,
                          exp.key_value_1,
                          exp.key_value_2
                     from item_exp_head ieh,
                          item_exp_detail exp,
                          currency_rates cur,
                          elc_comp elc
                    where ieh.item              = I_item
                      and ieh.supplier          = I_supplier
                      and exp.comp_id           <> 'TEXPC'
                      and ieh.item_exp_type     = 'C'
                      and ieh.origin_country_id = LP_origin_country_id
                      and ieh.lading_port       = LP_lading_port
                      and ieh.discharge_port    = LP_discharge_port 
                      and ieh.item              = exp.item
                      and ieh.supplier          = exp.supplier
                      and ieh.item_exp_seq      = exp.item_exp_seq
                      and ieh.item_exp_type     = exp.item_exp_type
                      and exp.comp_id           = elc.comp_id
                      and exp.comp_currency     = cur.currency_code
                      and cur.exchange_type     = I_exchange_type  -- ('P','C','O')
                      and cur.effective_date    = (select max(curr.effective_date)
                                                     from currency_rates curr
                                                    where exp.comp_currency    = curr.currency_code
                                                      and curr.effective_date <= LP_written_date
                                                      and curr.exchange_type    = I_exchange_type)
                      and not exists (select 'x'
                                        from ordloc_exp ord
                                       where ord.order_no      =  I_order_no
                                         and ord.item          =  I_item
                                         and ((ord.pack_item is NULL and I_pack_item is NULL)
                                             or (ord.pack_item = I_pack_item))
                                         and ord.location      = I_location
                                         and ord.comp_id       =  exp.comp_id
                                         and rownum = 1);
   ---
   if SQL%NOTFOUND then -- Insert the 'Base' expenses for the Item/Supplier/Origin Country.
      O_records_inserted := 'N';
   end if;
   --- 
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_CTRY_COMPS;
----------------------------------------------------------------------------------------
FUNCTION INSERT_BASE_ZONE_COMPS(O_error_message    IN OUT VARCHAR2,
                                O_records_inserted IN OUT VARCHAR2,
                                I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                                I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                                I_item             IN     ITEM_MASTER.ITEM%TYPE,
                                I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                                I_supplier         IN     SUPS.SUPPLIER%TYPE,
                                I_location         IN     ORDLOC.LOCATION%TYPE,
                                I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                                I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                                I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                                I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'ORDER_EXPENSE_SQL.INSERT_BASE_ZONE_COMPS';
   L_dummy     NUMBER := NULL;
   L_exchange_type       CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;

   cursor C_CHECK_FOR_PO_EXCHANGE_TYPE is
      select 1
        from item_exp_head ieh,
             item_exp_detail exp,
             currency_rates cur,
             elc_comp elc
       where ieh.item           = I_item
         and ieh.supplier       = I_supplier 
         and ieh.base_exp_ind   = 'Y'
         and ieh.item_exp_type  = 'Z'
         and ieh.item           = exp.item
         and ieh.supplier       = exp.supplier
         and ieh.item_exp_seq   = exp.item_exp_seq
         and ieh.item_exp_type  = exp.item_exp_type
         and exp.comp_id        <> 'TEXPZ'
         and exp.comp_id        = elc.comp_id
         and ((LP_purchase_type = 'BACK'
               and LP_backhaul_type = 'C')
             or (((LP_purchase_type = 'BACK'
                   and LP_backhaul_type = 'F')
                  or LP_purchase_type <> 'BACK')
               and elc.exp_category <> 'B'))
         and exp.comp_currency  = cur.currency_code
         and cur.exchange_type  = 'P'
         and cur.effective_date <= LP_written_date
         and rownum = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   open C_CHECK_FOR_PO_EXCHANGE_TYPE;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   fetch C_CHECK_FOR_PO_EXCHANGE_TYPE into L_dummy;
   if C_CHECK_FOR_PO_EXCHANGE_TYPE%FOUND then
      L_exchange_type := 'P';
   else
      L_exchange_type := I_exchange_type;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   close C_CHECK_FOR_PO_EXCHANGE_TYPE;
   O_records_inserted := 'Y';
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDLOC_EXP', NULL);
   insert into ordloc_exp(order_no,
                          seq_no,
                          item,
                          pack_item,
                          location,
                          loc_type,
                          comp_id,
                          cvb_code,
                          cost_basis,
                          comp_rate,
                          comp_currency,
                          exchange_rate,
                          per_count,
                          per_count_uom,
                          est_exp_value,
                          nom_flag_1,
                          nom_flag_2,
                          nom_flag_3,
                          nom_flag_4,
                          nom_flag_5,
                          origin,
                          display_order,
                          defaulted_from,
                          key_value_1,
                          key_value_2)
                  select  I_order_no,
                          NVL(I_seq_no,1) + rownum,
                          I_item,
                          I_pack_item,
                          I_location,
                          I_loc_type,
                          exp.comp_id,
                          exp.cvb_code,
                          decode(exp.cvb_code, NULL, NVL(elc.cost_basis, 'S')),
                          exp.comp_rate,
                          exp.comp_currency,
                          cur.exchange_rate,
                          exp.per_count,
                          exp.per_count_uom,
                          exp.est_exp_value,
                          exp.nom_flag_1,
                          exp.nom_flag_2,
                          exp.nom_flag_3,
                          exp.nom_flag_4,
                          exp.nom_flag_5,
                          'S',
                          exp.display_order,
                          exp.defaulted_from,
                          exp.key_value_1,
                          exp.key_value_2
                     from item_exp_head ieh,
                          item_exp_detail exp,
                          currency_rates cur,
                          elc_comp elc
                    where ieh.item           = I_item
                      and ieh.supplier       = I_supplier 
                      and ieh.base_exp_ind   = 'Y'
                      and ieh.item_exp_type  = 'Z'
                      and ieh.item           = exp.item
                      and ieh.supplier       = exp.supplier
                      and ieh.item_exp_seq   = exp.item_exp_seq
                      and ieh.item_exp_type  = exp.item_exp_type
                      and exp.comp_id        <> 'TEXPZ'
                      and exp.comp_id        = elc.comp_id
                      and ((LP_purchase_type = 'BACK'
                            and LP_backhaul_type = 'C')
                          or (((LP_purchase_type = 'BACK'
                                and LP_backhaul_type = 'F')
                                or LP_purchase_type <> 'BACK')
                             and elc.exp_category <> 'B'))
                      and exp.comp_currency  = cur.currency_code
                      and cur.exchange_type  = L_exchange_type  -- ('P','C','O')
                      and cur.effective_date = (select max(curr.effective_date)
                                                  from currency_rates curr
                                                 where exp.comp_currency    = curr.currency_code
                                                   and curr.effective_date <= LP_written_date
                                                   and curr.exchange_type    = L_exchange_type)
                      and not exists (select 'x'
                                        from ordloc_exp ord
                                       where ord.order_no      =  I_order_no
                                         and ord.item          =  I_item
                                         and ((ord.pack_item is NULL and I_pack_item is NULL)
                                             or (ord.pack_item = I_pack_item))
                                         and ord.location      = I_location
                                         and ord.comp_id       =  exp.comp_id
                                         and rownum = 1);
   ---
   if SQL%NOTFOUND then
      O_records_inserted := 'N';
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_BASE_ZONE_COMPS;
----------------------------------------------------------------------------------------
FUNCTION INSERT_BASE_CTRY_COMPS(O_error_message    IN OUT VARCHAR2,
                                O_records_inserted IN OUT VARCHAR2,
                                I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                                I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                                I_item             IN     ITEM_MASTER.ITEM%TYPE,
                                I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                                I_supplier         IN     SUPS.SUPPLIER%TYPE,
                                I_location         IN     ORDLOC.LOCATION%TYPE,
                                I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                                I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                                I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                                I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE) 
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'ORDER_EXPENSE_SQL.INSERT_BASE_CTRY_COMPS';
   L_dummy     NUMBER := NULL;
   L_exchange_type       CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;

   cursor C_CHECK_FOR_PO_EXCHANGE_TYPE is
      select 1
        from item_exp_head ieh,
             item_exp_detail exp,
             currency_rates cur
       where ieh.item              = I_item
         and ieh.supplier          = I_supplier 
         and ieh.base_exp_ind      = 'Y'
         and ieh.origin_country_id = LP_origin_country_id
         and ieh.item_exp_type     = 'C'
         and ieh.item              = exp.item
         and ieh.supplier          = exp.supplier
         and ieh.item_exp_seq      = exp.item_exp_seq
         and ieh.item_exp_type     = exp.item_exp_type
         and exp.comp_id           <> 'TEXPC'
         and exp.comp_currency     = cur.currency_code
         and cur.exchange_type  = 'P'
         and cur.effective_date <= LP_written_date
         and rownum = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   open C_CHECK_FOR_PO_EXCHANGE_TYPE;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   fetch C_CHECK_FOR_PO_EXCHANGE_TYPE into L_dummy;
   if C_CHECK_FOR_PO_EXCHANGE_TYPE%FOUND then
      L_exchange_type := 'P';
   else
      L_exchange_type := I_exchange_type;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   close C_CHECK_FOR_PO_EXCHANGE_TYPE;

   O_records_inserted := 'Y';
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDLOC_EXP', NULL);
   insert into ordloc_exp(order_no,
                          seq_no,
                          item,
                          pack_item,
                          location,
                          loc_type,
                          comp_id,
                          cvb_code,
                          cost_basis,
                          comp_rate,
                          comp_currency,
                          exchange_rate,
                          per_count,
                          per_count_uom,
                          est_exp_value,
                          nom_flag_1,
                          nom_flag_2,
                          nom_flag_3,
                          nom_flag_4,
                          nom_flag_5,
                          origin,
                          display_order,
                          defaulted_from,
                          key_value_1,
                          key_value_2)
                  select  I_order_no,
                          I_seq_no + rownum,
                          I_item,
                          I_pack_item,
                          I_location,
                          I_loc_type,
                          exp.comp_id,
                          exp.cvb_code,
                          decode(exp.cvb_code, NULL, NVL(elc.cost_basis, 'S')),
                          exp.comp_rate,
                          exp.comp_currency,
                          cur.exchange_rate,
                          exp.per_count,
                          exp.per_count_uom,
                          exp.est_exp_value,
                          exp.nom_flag_1,
                          exp.nom_flag_2,
                          exp.nom_flag_3,
                          exp.nom_flag_4,
                          exp.nom_flag_5,
                          'S',
                          exp.display_order,
                          exp.defaulted_from,
                          exp.key_value_1,
                          exp.key_value_2
                     from item_exp_head ieh,
                          item_exp_detail exp,
                          currency_rates cur,
                          elc_comp elc
                    where ieh.item              = I_item
                      and ieh.supplier          = I_supplier 
                      and ieh.base_exp_ind      = 'Y'
                      and ieh.origin_country_id = LP_origin_country_id
                      and ieh.item_exp_type     = 'C'
                      and ieh.item              = exp.item
                      and ieh.supplier          = exp.supplier
                      and ieh.item_exp_seq      = exp.item_exp_seq
                      and ieh.item_exp_type     = exp.item_exp_type
                      and exp.comp_id           <> 'TEXPC'
                      and exp.comp_id           = elc.comp_id
                      and exp.comp_currency     = cur.currency_code
                      and cur.exchange_type     = L_exchange_type  -- ('P','C','O')
                      and cur.effective_date    = (select max(curr.effective_date)
                                                     from currency_rates curr
                                                    where exp.comp_currency    = curr.currency_code
                                                      and curr.effective_date <= LP_written_date
                                                      and curr.exchange_type    = L_exchange_type)
                      and not exists (select 'x'
                                        from ordloc_exp ord
                                       where ord.order_no      =  I_order_no
                                         and ord.item          =  I_item
                                         and ((ord.pack_item is NULL and I_pack_item is NULL)
                                             or (ord.pack_item = I_pack_item))
                                         and ord.location      = I_location
                                         and ord.comp_id       =  exp.comp_id
                                         and rownum = 1);
   ---
   if SQL%NOTFOUND then
      O_records_inserted := 'N';
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_BASE_CTRY_COMPS;
----------------------------------------------------------------------------------------
FUNCTION INSERT_PTNR_COMPS(O_error_message    IN OUT VARCHAR2,
                           O_records_inserted IN OUT VARCHAR2,
                           I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                           I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                           I_item             IN     ITEM_MASTER.ITEM%TYPE,
                           I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                           I_supplier         IN     SUPS.SUPPLIER%TYPE,
                           I_location         IN     ORDLOC.LOCATION%TYPE,
                           I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                           I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                           I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                           I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                           I_partner_type     IN     PARTNER.PARTNER_TYPE%TYPE,
                           I_partner          IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'ORDER_EXPENSE_SQL.INSERT_PTNR_COMPS';
   L_dummy     NUMBER := NULL;
   L_exchange_type       CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;

   cursor C_CHECK_FOR_PO_EXCHANGE_TYPE is
      select 1
        from exp_prof_detail exp,
             exp_prof_head eph,
             currency_rates cur
       where eph.module            = 'PTNR'
         and eph.key_value_1       = I_partner_type
         and eph.key_value_2       = I_partner
         and eph.discharge_port    = LP_discharge_port
         and ((eph.exp_prof_type   = 'Z'
         and eph.zone_group_id     = I_zone_group_id
         and eph.zone_id           = I_zone_id)
          or (eph.exp_prof_type    = 'C'
              and eph.origin_country_id = LP_origin_country_id
              and eph.lading_port       = LP_lading_port))
         and eph.exp_prof_key      = exp.exp_prof_key
         and exp.comp_id           not in ('TEXPZ','TEXPC')
         and exp.comp_currency     = cur.currency_code
         and cur.exchange_type  = 'P'
         and cur.effective_date <= LP_written_date;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   open C_CHECK_FOR_PO_EXCHANGE_TYPE;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   fetch C_CHECK_FOR_PO_EXCHANGE_TYPE into L_dummy;
   if C_CHECK_FOR_PO_EXCHANGE_TYPE%FOUND then
      L_exchange_type := 'P';
   else
      L_exchange_type := I_exchange_type;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   close C_CHECK_FOR_PO_EXCHANGE_TYPE;
   O_records_inserted := 'Y';
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDLOC_EXP', NULL);
   insert into ordloc_exp(order_no,
                          seq_no,
                          item,
                          pack_item,
                          location,
                          loc_type,
                          comp_id,
                          cvb_code,
                          cost_basis,
                          comp_rate,
                          comp_currency,
                          exchange_rate,
                          per_count,
                          per_count_uom,
                          est_exp_value,
                          nom_flag_1,
                          nom_flag_2,
                          nom_flag_3,
                          nom_flag_4,
                          nom_flag_5,
                          origin,
                          display_order,
                          defaulted_from,
                          key_value_1,
                          key_value_2)
                  select  I_order_no,
                          I_seq_no + rownum,
                          I_item,
                          I_pack_item,
                          I_location,
                          I_loc_type,
                          exp.comp_id,
                          exp.cvb_code,
                          decode(exp.cvb_code, NULL, NVL(elc.cost_basis, 'S')),
                          exp.comp_rate,
                          exp.comp_currency,
                          cur.exchange_rate,
                          exp.per_count,
                          exp.per_count_uom,
                          0,
                          exp.nom_flag_1,
                          exp.nom_flag_2,
                          exp.nom_flag_3,
                          exp.nom_flag_4,
                          exp.nom_flag_5,
                          'S',
                          elc.display_order,
                          'P',
                          I_partner_type,
                          I_partner
                     from exp_prof_detail exp,
                          exp_prof_head eph,
                          currency_rates cur,
                          elc_comp elc
                    where eph.module            = 'PTNR'
                      and eph.key_value_1       = I_partner_type
                      and eph.key_value_2       = I_partner
                      and eph.discharge_port    = LP_discharge_port
                      and ((eph.exp_prof_type   = 'Z'
                      and eph.zone_group_id     = I_zone_group_id
                      and eph.zone_id           = I_zone_id)
                       or (eph.exp_prof_type    = 'C'
                      and eph.origin_country_id = LP_origin_country_id
                      and eph.lading_port       = LP_lading_port))
                      and eph.exp_prof_key      = exp.exp_prof_key
                      and exp.comp_id           = elc.comp_id
                      and exp.comp_id           not in ('TEXPZ','TEXPC')
                      and exp.comp_currency     = cur.currency_code
                      and cur.exchange_type     = L_exchange_type   -- ('P','C','O')
                      and cur.effective_date    = (select max(curr.effective_date)
                                                     from currency_rates curr
                                                    where exp.comp_currency    = curr.currency_code
                                                      and curr.effective_date <= LP_written_date
                                                      and curr.exchange_type    = L_exchange_type)
                      and not exists (select 'x'
                                        from ordloc_exp ord
                                       where ord.order_no       = I_order_no
                                         and ord.item           = I_item
                                         and ((ord.pack_item    = I_pack_item)
                                              or (ord.pack_item is NULL and I_pack_item is NULL))
                                         and ord.location       = I_location
                                         and ord.comp_id        = exp.comp_id
                                         and rownum = 1);

    ---
    if SQL%NOTFOUND then
       O_records_inserted := 'N';
    end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_PTNR_COMPS;
----------------------------------------------------------------------------------------
FUNCTION INSERT_BASE_PTNR_COMPS(O_error_message    IN OUT VARCHAR2,
                                O_records_inserted IN OUT VARCHAR2,
                                I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                                I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                                I_item             IN     ITEM_MASTER.ITEM%TYPE,
                                I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                                I_supplier         IN     SUPS.SUPPLIER%TYPE,
                                I_location         IN     ORDLOC.LOCATION%TYPE,
                                I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                                I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                                I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                                I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                                I_partner_type     IN     PARTNER.PARTNER_TYPE%TYPE,
                                I_partner          IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'ORDER_EXPENSE_SQL.INSERT_BASE_PTNR_COMPS';
   L_dummy     NUMBER := NULL;
   L_exchange_type       CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;

   cursor C_CHECK_FOR_PO_EXCHANGE_TYPE is
      select 1
        from exp_prof_detail exp,
             exp_prof_head eph,
             currency_rates cur
       where eph.module            = 'PTNR'
         and eph.key_value_1       = I_partner_type
         and eph.key_value_2       = I_partner
         and eph.base_prof_ind     = 'Y'
         and eph.exp_prof_key      = exp.exp_prof_key
         and exp.comp_id           not in ('TEXPZ','TEXPC')
         and exp.comp_currency     = cur.currency_code
         and cur.exchange_type  = 'P'
         and cur.effective_date <= LP_written_date;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   open C_CHECK_FOR_PO_EXCHANGE_TYPE;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   fetch C_CHECK_FOR_PO_EXCHANGE_TYPE into L_dummy;
   if C_CHECK_FOR_PO_EXCHANGE_TYPE%FOUND then
      L_exchange_type := 'P';
   else
      L_exchange_type := I_exchange_type;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   close C_CHECK_FOR_PO_EXCHANGE_TYPE;
   O_records_inserted := 'Y';
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDLOC_EXP', NULL);
   insert into ordloc_exp(order_no,
                          seq_no,
                          item,
                          pack_item,
                          location,
                          loc_type,
                          comp_id,
                          cvb_code,
                          cost_basis,
                          comp_rate,
                          comp_currency,
                          exchange_rate,
                          per_count,
                          per_count_uom,
                          est_exp_value,
                          nom_flag_1,
                          nom_flag_2,
                          nom_flag_3,
                          nom_flag_4,
                          nom_flag_5,
                          origin,
                          display_order,
                          defaulted_from,
                          key_value_1,
                          key_value_2)
                  select  I_order_no,
                          I_seq_no + rownum,
                          I_item,
                          I_pack_item,
                          I_location,
                          I_loc_type,
                          exp.comp_id,
                          exp.cvb_code,
                          decode(exp.cvb_code, NULL, NVL(elc.cost_basis, 'S')),
                          exp.comp_rate,
                          exp.comp_currency,
                          cur.exchange_rate,
                          exp.per_count,
                          exp.per_count_uom,
                          0,
                          exp.nom_flag_1,
                          exp.nom_flag_2,
                          exp.nom_flag_3,
                          exp.nom_flag_4,
                          exp.nom_flag_5,
                          'S',
                          elc.display_order,
                          'P',
                          I_partner_type,
                          I_partner
                     from exp_prof_detail exp,
                          exp_prof_head eph,
                          currency_rates cur,
                          elc_comp elc
                    where eph.module            = 'PTNR'
                      and eph.key_value_1       = I_partner_type
                      and eph.key_value_2       = I_partner
                      and eph.base_prof_ind     = 'Y'
                      and eph.exp_prof_key      = exp.exp_prof_key
                      and exp.comp_id           = elc.comp_id
                      and exp.comp_id           not in ('TEXPZ','TEXPC')
                      and exp.comp_currency     = cur.currency_code
                      and cur.exchange_type     = L_exchange_type   -- ('P','C','O')
                      and cur.effective_date    = (select max(curr.effective_date)
                                                     from currency_rates curr
                                                    where exp.comp_currency    = curr.currency_code
                                                      and curr.effective_date <= LP_written_date
                                                      and curr.exchange_type    = L_exchange_type)
                      and not exists (select 'x'
                                        from ordloc_exp ord
                                       where ord.order_no       = I_order_no
                                         and ord.item           = I_item
                                         and ((ord.pack_item    = I_pack_item)
                                              or (ord.pack_item is NULL and I_pack_item is NULL))
                                         and ord.location       = I_location
                                         and ord.comp_id        = exp.comp_id
                                         and rownum = 1);

    ---
    if SQL%NOTFOUND then
       O_records_inserted := 'N';
    end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_BASE_PTNR_COMPS;
----------------------------------------------------------------------------------------
FUNCTION INSERT_TEXP(O_error_message    IN OUT VARCHAR2,
                     O_records_inserted IN OUT VARCHAR2,
                     I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                     I_seq_no           IN     ORDLOC_EXP.SEQ_NO%TYPE,
                     I_item             IN     ITEM_MASTER.ITEM%TYPE,
                     I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                     I_supplier         IN     SUPS.SUPPLIER%TYPE,
                     I_location         IN     ORDLOC.LOCATION%TYPE,
                     I_loc_type         IN     ORDLOC.LOC_TYPE%TYPE,
                     I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                     I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                     I_exchange_type    IN     CURRENCY_RATES.EXCHANGE_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'ORDER_EXPENSE_SQL.INSERT_TEXP';
   L_dummy     NUMBER := NULL;
   L_exchange_type       CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;

   cursor C_CHECK_FOR_PO_EXCHANGE_TYPE is
      select 1
        from currency_rates cur,
             elc_comp elc
       where elc.comp_id        = 'TEXP'
         and elc.comp_currency  = cur.currency_code
         and cur.exchange_type  = 'P'
         and cur.effective_date <= LP_written_date;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   open C_CHECK_FOR_PO_EXCHANGE_TYPE;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   fetch C_CHECK_FOR_PO_EXCHANGE_TYPE into L_dummy;

   if C_CHECK_FOR_PO_EXCHANGE_TYPE%FOUND then
      L_exchange_type := 'P';
   else
      L_exchange_type := I_exchange_type;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_FOR_PO_EXCHANGE_TYPE','CURRENCY_RATES, ELC_COMP', 'Effective date: '||to_char(LP_written_date, 'YYYYMMDDHH24MISS'));
   close C_CHECK_FOR_PO_EXCHANGE_TYPE;

   O_records_inserted := 'Y';
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'ORDLOC_EXP', NULL);
   insert into ordloc_exp(order_no,
                          seq_no,
                          item,
                          pack_item,
                          location,
                          loc_type,
                          comp_id,
                          cvb_code,
                          cost_basis,
                          comp_rate,
                          comp_currency,
                          exchange_rate,
                          per_count,
                          per_count_uom,
                          est_exp_value,
                          nom_flag_1,
                          nom_flag_2,
                          nom_flag_3,
                          nom_flag_4,
                          nom_flag_5,
                          origin,
                          display_order,
                          defaulted_from)
                  select  I_order_no,
                          I_seq_no,
                          I_item,
                          I_pack_item,
                          I_location,
                          I_loc_type,
                          elc.comp_id,
                          elc.cvb_code,
                          elc.cost_basis,
                          elc.comp_rate,
                          elc.comp_currency,
                          cur.exchange_rate,
                          elc.per_count,
                          elc.per_count_uom,
                          0,
                          elc.nom_flag_1,
                          elc.nom_flag_2,
                          elc.nom_flag_3,
                          elc.nom_flag_4,
                          elc.nom_flag_5,
                          'S',
                          elc.display_order,
                          'E'
                     from currency_rates cur,
                          elc_comp elc
                    where elc.comp_id        = 'TEXP'
                      and elc.comp_currency  = cur.currency_code
                      and cur.exchange_type  = L_exchange_type  -- ('P','C','O')
                      and cur.effective_date = (select max(curr.effective_date)
                                                  from currency_rates curr
                                                 where elc.comp_currency    = curr.currency_code
                                                   and curr.effective_date <= LP_written_date
                                                   and curr.exchange_type    = L_exchange_type)
                      and not exists (select 'x'
                                        from ordloc_exp ord
                                       where ord.order_no       = I_order_no
                                         and ord.item           = I_item
                                         and ((ord.pack_item    = I_pack_item)
                                              or (ord.pack_item is NULL and I_pack_item is NULL))
                                         and ord.location       = I_location
                                         and ord.comp_id        = elc.comp_id
                                         and rownum = 1);

   if SQL%NOTFOUND then
      O_records_inserted := 'N';
   end if;
   --- 
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_TEXP;
-------------------------------------------------------------------------------
FUNCTION DELETE_BACKHAUL_EXP(O_error_message  IN OUT VARCHAR2,
                             I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'ORDER_EXPENSE_SQL.DELETE_BACKHAUL_EXP';
   L_table       VARCHAR2(30) := 'ORDLOC_EXP';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ORDLOC_EXP is
      select /*+ INDEX(ordloc_exp, pk_ordloc_exp) */ 'x'
        from ordloc_exp
       where order_no = I_order_no
         and origin   = 'S'
         and comp_id in (select comp_id
                           from elc_comp
                          where exp_category = 'B')
         for update nowait; 

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC_EXP','ORDLOC_EXP',NULL);
   open C_LOCK_ORDLOC_EXP;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC_EXP','ORDLOC_EXP',NULL);
   close C_LOCK_ORDLOC_EXP;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'ORDLOC_EXP',NULL);
        delete /*+ INDEX(ordloc_exp, pk_ordloc_exp) */ from ordloc_exp
         where order_no = I_order_no
           and origin   = 'S'
           and comp_id in (select comp_id
                             from elc_comp
                            where exp_category = 'B');
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             to_char(I_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_BACKHAUL_EXP;
--------------------------------------------------------------------------------------
FUNCTION GET_UOM_RATE_VALUE(O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            O_per_count_uom  IN OUT UOM_CLASS.UOM%TYPE,
                            O_comp_rate      IN OUT ELC_COMP.COMP_RATE%TYPE,        
                            O_est_exp_value  IN OUT ORDLOC_EXP.EST_EXP_VALUE%TYPE,
                            I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE,
                            I_pack_item      IN     ITEM_MASTER.ITEM%TYPE,
                            I_comp_id        IN     ELC_COMP.COMP_ID%TYPE,
                            I_currency_code  IN     CURRENCIES.CURRENCY_CODE%TYPE,
                            I_exchange_rate  IN     CURRENCY_RATES.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(50)             := 'ORDER_EXPENSE_SQL.GET_UOM_RATE_VALUE';
   L_currency_code CURRENCIES.CURRENCY_CODE%TYPE    := NULL;

   cursor C_GET_UOM_RATE_VALUE is
      select per_count_uom,
             comp_rate / NVL(per_count,1),
             est_exp_value
        from ordloc_exp
       where order_no   = I_order_no
         and item       = I_item
         and (pack_item = I_pack_item
          or (pack_item is NULL and I_pack_item is NULL))
         and comp_id    = I_comp_id;

   cursor C_GET_CURRENCY is
      select comp_currency
        from elc_comp
       where comp_id = I_comp_id;

BEGIN
   O_exists := TRUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_UOM_RATE_VALUE','ORDLOC_EXP',NULL);
   open C_GET_UOM_RATE_VALUE;

   SQL_LIB.SET_MARK('FETCH','C_GET_UOM_RATE_VALUE','ORDLOC_EXP',NULL);
   fetch C_GET_UOM_RATE_VALUE into O_per_count_uom,
                                   O_comp_rate,
                                   O_est_exp_value;

   if C_GET_UOM_RATE_VALUE%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_GET_UOM_RATE_VALUE','ORDLOC_EXP',NULL);
      close C_GET_UOM_RATE_VALUE;
      O_exists := FALSE;
      return TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_GET_UOM_RATE_VALUE','ORDLOC_EXP',NULL);
   close C_GET_UOM_RATE_VALUE;

   if I_currency_code is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_CURRENCY','ELC_COMP','Comp ID: ' || I_comp_id);
      open C_GET_CURRENCY;

      SQL_LIB.SET_MARK('FETCH','C_GET_CURRENCY','ELC_COMP','Comp ID: ' || I_comp_id);
      fetch C_GET_CURRENCY into L_currency_code;
      if C_GET_CURRENCY%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_CURRENCY','ELC_COMP','Comp ID: ' || I_comp_id);
         close C_GET_CURRENCY;
         O_error_message := SQL_LIB.CREATE_MSG('COMP_NOT_EXIST',NULL,NULL,NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_GET_CURRENCY','ELC_COMP','Comp ID: ' || I_comp_id);
      close C_GET_CURRENCY;

      if CURRENCY_SQL.CONVERT(O_error_message,
                              O_comp_rate,
                              L_currency_code,
                              I_currency_code,
                              O_comp_rate,
                              'C',
                              NULL,
                              NULL,
                              NULL,
                              I_exchange_rate) = FALSE then
         return FALSE;
      end if;

      if CURRENCY_SQL.CONVERT(O_error_message,
                              O_est_exp_value,
                              L_currency_code,
                              I_currency_code,
                              O_est_exp_value,
                              'C',
                              NULL,
                              NULL,
                              NULL,
                              I_exchange_rate) = FALSE then
         return FALSE;
      end if;               
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_UOM_RATE_VALUE;
-------------------------------------------------------------------------------
FUNCTION DELETE_EXP(O_error_message  IN OUT VARCHAR2,
                    I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                    I_item           IN     ITEM_MASTER.ITEM%TYPE,
                    I_location       IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'ORDER_EXPENSE_SQL.DELETE_EXP';
   L_table       VARCHAR2(30) := 'ORDLOC_EXP';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ORDLOC_EXP_ITEM is
      select 'Y'
        from ordloc_exp
       where order_no  = I_order_no
         and item      = I_item
         for update nowait;

   cursor C_LOCK_ORDLOC_EXP_LOC is
      select 'Y'
        from ordloc_exp
       where order_no  = I_order_no
         and item      = I_item
         and location  = I_location
         for update nowait;

BEGIN
   if I_location is NULL then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC_EXP_ITEM','ORDLOC_EXP',NULL);
      open C_LOCK_ORDLOC_EXP_ITEM;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC_EXP_ITEM','ORDLOC_EXP',NULL);
      close C_LOCK_ORDLOC_EXP_ITEM;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'ORDLOC_EXP',NULL);
      delete from ordloc_exp
            where order_no = I_order_no
              and item     = I_item;
   else
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC_EXP_LOC','ORDLOC_EXP',NULL);
      open C_LOCK_ORDLOC_EXP_LOC;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC_EXP_LOC','ORDLOC_EXP',NULL);
      close C_LOCK_ORDLOC_EXP_LOC;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'ORDLOC_EXP',NULL);
      delete from ordloc_exp
            where order_no = I_order_no
              and item     = I_item
              and location = I_location;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             to_char(I_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_EXP;
---------------------------------------------------------------------------------------------
FUNCTION DEFAULT_LOC_EXP(O_error_message    IN OUT VARCHAR2,
                         I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item             IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item        IN     ITEM_MASTER.ITEM%TYPE,
                         I_zone_group_id    IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                         I_zone_id          IN     COST_ZONE.ZONE_ID%TYPE,
                         I_location         IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'ORDER_EXPENSE_SQL.DEFAULT_LOC_EXP';
   L_seq_no      ORDLOC_EXP.SEQ_NO%TYPE;
   L_table       VARCHAR2(30) := 'ORDLOC_EXP';
   L_locs        LOCS_TABLE_TYPE;
   L_loop_index  NUMBER := 0;
   L_loc_type    ITEM_LOC.LOC_TYPE%TYPE;
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ORDLOC_EXP_LOC is
      select 'Y'
        from ordloc_exp o
       where o.order_no        = I_order_no
         and o.item            = I_item
         and nvl(o.pack_item, -1) = nvl(I_pack_item, -1)
         and o.location       != I_location
         and o.location in (select c.location
                              from cost_zone_group_loc c
                             where c.zone_group_id = I_zone_group_id
                               and c.zone_id       = I_zone_id)
         for update nowait;

   cursor C_SEQ_NO is
      select NVL(MAX(seq_no), 0) + 1
        from ordloc_exp
       where order_no = I_order_no;

   cursor C_GET_LOCS is
      select distinct o.location
        from ordloc_exp o
       where o.order_no        = I_order_no
         and o.item            = I_item
         and nvl(o.pack_item, -1) = nvl(I_pack_item, -1)
         and o.location       != I_location
         and o.location in (select c.location
                              from cost_zone_group_loc c
                             where c.zone_group_id = I_zone_group_id
                               and c.zone_id       = I_zone_id);

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_LOCS','ORDLOC_EXP',NULL);
   open C_GET_LOCS;
   LOOP
      L_loop_index := L_loop_index + 1;

      SQL_LIB.SET_MARK('FETCH','C_GET_LOCS','ORDLOC_EXP',NULL);
      fetch C_GET_LOCS into L_locs(L_loop_index);

      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   SQL_LIB.SET_MARK('CLOSE','C_GET_LOCS','ORDLOC_EXP',NULL);
   close C_GET_LOCS;

   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC_EXP_LOC','ORDLOC_EXP',NULL);
   open C_LOCK_ORDLOC_EXP_LOC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC_EXP_LOC','ORDLOC_EXP',NULL);
   close C_LOCK_ORDLOC_EXP_LOC;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'ORDLOC_EXP',NULL);
   delete from ordloc_exp o
         where o.order_no        = I_order_no
           and o.item            = I_item
           and nvl(o.pack_item, -1) = nvl(I_pack_item, -1)
           and o.location       != I_location
           and o.location in (select c.location
                                from cost_zone_group_loc c
                               where c.zone_group_id = I_zone_group_id
                                 and c.zone_id       = I_zone_id);
   ---
   FOR loop_index IN 1..L_locs.COUNT LOOP

      SQL_LIB.SET_MARK('OPEN', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      open C_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      fetch C_SEQ_NO into L_seq_no;
      SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_NO','ORDLOC_EXP', 'Order no: '||to_char(I_order_no));
      close C_SEQ_NO;

      if LOCATION_ATTRIB_SQL.GET_TYPE (O_error_message,
                                       L_loc_type,
                                       L_locs(loop_index)) = FALSE then
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('INSERT', NULL, 'ORDLOC_EXP', NULL);
      insert into ordloc_exp(order_no,
                             seq_no,
                             item,
                             pack_item,
                             location,
                             loc_type,
                             comp_id,
                             cvb_code,
                             cost_basis,
                             comp_rate,
                             comp_currency,
                             exchange_rate,
                             per_count,
                             per_count_uom,
                             est_exp_value,
                             nom_flag_1,
                             nom_flag_2,
                             nom_flag_3,
                             nom_flag_4,
                             nom_flag_5,
                             origin,
                             display_order,
                             defaulted_from,
                             key_value_1,
                             key_value_2)
                     select  I_order_no,
                             L_seq_no + rownum,
                             I_item,
                             I_pack_item,
                             L_locs(loop_index),
                             L_loc_type,
                             o.comp_id,
                             o.cvb_code,
                             o.cost_basis,
                             o.comp_rate,
                             o.comp_currency,
                             o.exchange_rate,
                             o.per_count,
                             o.per_count_uom,
                             0,
                             o.nom_flag_1,
                             o.nom_flag_2,
                             o.nom_flag_3,
                             o.nom_flag_4,
                             o.nom_flag_5,
                             o.origin,
                             o.display_order,
                             o.defaulted_from,
                             o.key_value_1,
                             o.key_value_2
                        from ordloc_exp o
                       where o.order_no   = I_order_no
                         and o.item       = I_item
                         and nvl(o.pack_item, -1) = nvl(I_pack_item, -1)
                         and o.location   = I_location;
   END LOOP;
   ---
   -- Calculate the newly inserted expenses need to calculate all expenses for the order/item/zone
   ---
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PE',
                             I_item,
                             NULL,
                             NULL,
                             NULL,
                             I_order_no,
                             NULL,
                             I_pack_item,
                             I_zone_id,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL
                             ) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             to_char(I_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_LOC_EXP;
----------------------------------------------------------------------------------------------
END ORDER_EXPENSE_SQL;
/
