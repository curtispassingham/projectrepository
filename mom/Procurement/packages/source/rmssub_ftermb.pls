
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_FTERM AS

-------------------------------------------------------------------------------------------------------
FUNCTION PARSE_FTERM (O_error_message     OUT  VARCHAR2,
                      O_fterm_record      OUT  FTERM_SQL.fterm_record,
                      I_fterm_root        IN   xmldom.DOMElement)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_FTERM (O_error_message    IN OUT  VARCHAR2,
                        I_fterm_record     IN      FTERM_SQL.fterm_record)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------


-- PUBLIC FUNCTION -- 
-------------------------------------------------------------------------------------------------------
FUNCTION CONSUME (O_status_code        IN OUT  VARCHAR2,
                  O_error_message      IN OUT  VARCHAR2,
                  I_message_clob       IN      CLOB)
RETURN BOOLEAN IS

   fterm_record       xmldom.DOMElement;
   L_fterm_record     FTERM_SQL.fterm_record;

BEGIN

   -- parse the document and return the root element.
   fterm_record := API_LIBRARY.readRoot(I_message_clob, 'FrtTermDesc');
 
   if PARSE_FTERM(O_error_message,  
                    L_fterm_record,
                    fterm_record) = FALSE then
      return FALSE;
   end if;

   if PROCESS_FTERM(O_error_message,
                    L_fterm_record) = FALSE then
      return FALSE;
   end if;         

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_FTERM.CONSUME',
                                            to_char(SQLCODE));   
      return FALSE;
                     
END CONSUME;
------------------------------------------------------------------------------------------------------
FUNCTION PARSE_FTERM (O_error_message     OUT  VARCHAR2,
                      O_fterm_record      OUT  FTERM_SQL.fterm_record,
                      I_fterm_root        IN   xmldom.DOMElement)
return BOOLEAN IS
BEGIN      

   O_fterm_record.terms             := rib_xml.getChildText(I_fterm_root, 'freight_terms');
   O_fterm_record.description       := rib_xml.getChildText(I_fterm_root, 'term_desc');
   O_fterm_record.enabled_flag      := rib_xml.getChildText(I_fterm_root, 'enabled_flag');
   O_fterm_record.start_date_active := rib_xml.getChildDate(I_fterm_root, 'start_date_active');
   O_fterm_record.end_date_active   := rib_xml.getChildDate(I_fterm_root, 'end_date_active');

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_FTERM.PARSE_FTERM',
                                            to_char(SQLCODE));   
      return FALSE;

END PARSE_FTERM;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_FTERM (O_error_message    IN OUT  VARCHAR2,
                        I_fterm_record     IN      FTERM_SQL.fterm_record)
return BOOLEAN IS
  
    
BEGIN
   
   if FTERM_SQL.PROCESS_TERMS(O_error_message,
                              I_fterm_record) = FALSE then
      return FALSE;
   end if;
  
   return TRUE;   
        
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'RMSSUB_FTERM.PROCESS_FTERM',
                                          to_char(SQLCODE));   
      return FALSE;

END PROCESS_FTERM;
-----------------------------------------------------------------------------------------
END RMSSUB_FTERM;
/

