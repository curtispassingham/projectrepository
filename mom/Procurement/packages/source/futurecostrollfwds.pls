CREATE OR REPLACE PACKAGE FUTURE_COST_ROLLFWD_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
-- Function Name: ROLL_FORWARD
-- Purpose      : Calls the future_cost_rollfwd_control functions in the correct order.
----------------------------------------------------------------------------------------
FUNCTION ROLL_FORWARD(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      L_action                IN     COST_EVENT.ACTION%TYPE,
                      L_event_type            IN     COST_EVENT.EVENT_TYPE%TYPE,
                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                      I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: ADD_PROCESSING_SEQ_NO
-- Purpose      : Adds processing sequence to future_cost_gtt to assist with
--                further processing.
----------------------------------------------------------------------------------------
FUNCTION ADD_PROCESSING_SEQ_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER;
----------------------------------------------------------------------------------------
-- Function Name: ROLL_COST_CHANGE
-- Purpose      : Recalculates all future_cost_gtt records based on all cost changes
--                that are on the future cost table.
----------------------------------------------------------------------------------------
FUNCTION ROLL_COST_CHANGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER;
----------------------------------------------------------------------------------------
-- Function Name: ROLL_RECLASS
-- Purpose      : Applies dept/class/subclass reclassifications into future events -
--                maintains the dept/class/subclass values on future_cost_gtt.
----------------------------------------------------------------------------------------
FUNCTION ROLL_RECLASS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                      I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER;
----------------------------------------------------------------------------------------
-- Function Name: ROLL_DEAL
-- Purpose      : Recalculates all future_cost_gtt records based on all deals
--                that are on deal_item_loc_explode_gtt.
----------------------------------------------------------------------------------------
FUNCTION ROLL_DEAL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                   I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER;
----------------------------------------------------------------------------------------
-- Function Name: ROLL_ELC
-- Purpose      : Applies ELC into future_cost_gtt rows.
----------------------------------------------------------------------------------------
FUNCTION ROLL_ELC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                  I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER;
--------------------------------------------------------------------------------------
-- Function Name: CONVERT_NEGATIVE_COST
-- Purpose      : Negative cost is not allowed; this function change a negative
--                cost to zero.
--------------------------------------------------------------------------------------
FUNCTION CONVERT_NEGATIVE_COST(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
return NUMBER;
--

$if $$UTPLSQL=TRUE $then
   FUNCTION HELP_ROLL_DEAL_1(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION HELP_ROLL_DEAL_2(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION HELP_ROLL_DEAL_3(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION MERGE_DEAL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION SETUP_WHOLESALE_FRANCHISE(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                      I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION SEED_WF_STORES(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION PUSH_TEMPL_DATES(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION SYNC_TEMPL_ID(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION SYNC_PASSTHRU_PCT(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION ROLL_ELC_TEXPZ(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION ROLL_ELC_TEXPC(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION ROLL_ELC_TDTY(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION ROLL_DEAL_PASSTHRU(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION ROLL_TMPL(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION HELP_ROLL_TMPL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
   --
   FUNCTION MERGE_TMPL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   return NUMBER;
$end
--------------------------------------------------------------------------------------
END FUTURE_COST_ROLLFWD_SQL;
/

