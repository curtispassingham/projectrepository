CREATE OR REPLACE PACKAGE CORESVC_STOREORDER AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------
    --Global variables 
----------------------------------------------------------------------------------
LOCPOTSF_CRE            VARCHAR2(11) := 'locpotsfcre';
LOCPOTSF_DTL_CRE        VARCHAR2(14) := 'locpotsfdtlcre';
LOCPOTSF_MOD            VARCHAR2(11) := 'locpotsfmod';
LOCPOTSF_DTL_MOD        VARCHAR2(14) := 'locpotsfdtlmod';
LOCPOTSF_DEL            VARCHAR2(11) := 'locpotsfdel';
LOCPOTSF_DTL_DEL        VARCHAR2(14) := 'locpotsfdtldel';
/******************************************************************************
 * Function CREATE_MOD_LOCPO
 * It will create a PO or Transfer to RMS
 *                  PO or Transfer Details to RMS
 * It will modify a PO or Transfer to RMS
 *                  PO or Transfer Details to RMS
 * based on the action parameter                   
 ******************************************************************************/

FUNCTION CREATE_MOD_LOCPO(IO_error_message                IN  OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                          O_locpotsfref_rec               OUT NOCOPY "RIB_LocPOTsfRef_REC",                         
                          I_locpodesc_rec                 IN         "RIB_LocPOTsfDesc_REC",
                          I_action                        IN         VARCHAR2)
RETURN BOOLEAN;

/******************************************************************************
 * Function DEL_LOCPO
 * It will Delete a PO or Transfer in RMS.
 *                  PO or Transfer Details to RMS
 * based on the action parameter                   
 ******************************************************************************/   
FUNCTION DEL_LOCPO(IO_error_message                       IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                   I_locpotsfdesc_rec                     IN         "RIB_LocPOTsfDesc_REC",
                   I_action                               IN         VARCHAR2)
RETURN BOOLEAN;

/******************************************************************************

 * Function GET_DEALS
 * It will retrieve Deals Information from RMS     
 ******************************************************************************/  
FUNCTION GET_DEALS(IO_error_message                       IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                   O_locpotsfdealscoldesc_rec             OUT NOCOPY "RIB_LocPOTsfDealsColDesc_REC",                  
                   I_locpotsfdealscrivo_rec               IN         "RIB_LocPOTsfDealsCriVo_REC")
RETURN BOOLEAN;

/******************************************************************************
 * Function GET_ITEMS_SALES
 * It will retrieve the Item Sales information from RMS.   
 ******************************************************************************/ 
FUNCTION GET_ITEMS_SALES(IO_error_message                 IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                         O_locpotsfitmslscoldesc_rec      OUT NOCOPY "RIB_LocPOTsfItmSlsColDesc_REC",                         
                         I_locpoitmslsreq_rec             IN         "RIB_LocPOTsfItmSlsCriVo_REC")
RETURN BOOLEAN;
  
/******************************************************************************
 * Function GET_STORE_ORDERS
 * It will Retrieve header level PO or transfer information from RMS.   
 ******************************************************************************/ 
FUNCTION GET_STORE_ORDERS(IO_error_message                IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                          O_locpotsfhdrcoldesc_rec        OUT NOCOPY "RIB_LocPOTsfHdrColDesc_REC",                          
                          I_locpotsfhdrcrivo_rec          IN         "RIB_LocPOTsfHdrCriVo_REC")
RETURN BOOLEAN;
   
/******************************************************************************
 * Function GET_STORE_ORDER_DETAILS
 * It will retrieve the header/details of a PO or transfer from RMS.
 ******************************************************************************/
FUNCTION GET_STORE_ORDER_DETAILS(IO_error_message         IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_locpotsfdesc_rec       OUT NOCOPY "RIB_LocPOTsfDesc_REC",                                 
                                 I_locpotsfdtlscrivo_rec  IN         "RIB_LocPOTsfDtlsCriVo_REC")
   RETURN BOOLEAN;   
END CORESVC_STOREORDER;
/