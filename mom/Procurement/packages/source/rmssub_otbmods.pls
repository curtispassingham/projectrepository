
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_OTBMOD AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code          IN OUT  VARCHAR2,
                  O_error_message        IN OUT  VARCHAR2,
                  I_message              IN      RIB_OBJECT,
                  I_message_type         IN      VARCHAR2);
----------------------------------------------------------------------------
END RMSSUB_OTBMOD;
/


