/******************************************************************************
* Service Name     : StoreOrderService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/StoreOrderService/v1
* Description      : StoreOrder web service 
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE STOREORDERSERVICEPROVIDERIMPL AUTHID CURRENT_USER AS


/******************************************************************************
 *
 * Operation       : createLocPOTsfDesc
 * Description     : 
				Create a PO or Transfer in RMS based on request information from SIM.
			 
 * 
 * Input           : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
					This object contains the PO/Transfer information.
				
 * 
 * Output          : "RIB_LocPOTsfRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfRef/v1
 * Description     : 
					The output object consists of either the order number or the transfer number created in RMS.
				
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
					message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
					create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
					"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createLocPOTsfDesc(
                          I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_LocPOTsfDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_LocPOTsfRef_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : deleteLocPOTsfDesc
 * Description     : 
				Delete a PO or Transfer in RMS.
			 
 * 
 * Input           : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
					This object contains the PO/Transfer to be deleted.
				
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     : 
					InvocationSuccess object contains the success or failure status of processing the 
                              confirmation requests.
				
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
					message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
					create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
					"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE deleteLocPOTsfDesc(
                          I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_LocPOTsfDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_InvocationSuccess_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : modifyLocPOTsfDesc
 * Description     : 
				Modify a PO or Transfer in RMS.
			 
 * 
 * Input           : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
					This object contains the PO/Transfer information.
				
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     : 
					InvocationSuccess object contains the success or failure status of processing the 
                              confirmation requests.
				
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
					message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
					create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
					"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE modifyLocPOTsfDesc(
                          I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_LocPOTsfDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_InvocationSuccess_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : createDetailLocPOTsfDesc
 * Description     : 
				Create PO/Transfer Details in RMS.
			 
 * 
 * Input           : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
					This object contains the PO/Transfer information.
				
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     : 
					InvocationSuccess object contains the success or failure status of processing the 
                              confirmation requests.
				
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
					message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
					create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
					"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createDetailLocPOTsfDesc(
                          I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_LocPOTsfDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_InvocationSuccess_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : deleteDetailLocPOTsfDesc
 * Description     : 
				Delete PO/Transfer Details in RMS.
			 
 * 
 * Input           : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
					This object contains the PO/Transfer information.
				
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     : 
					InvocationSuccess object contains the success or failure status of processing the 
                              confirmation requests.
				
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
					message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
					create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
					"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE deleteDetailLocPOTsfDesc(
                          I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_LocPOTsfDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_InvocationSuccess_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : modifyDetailLocPOTsfDesc
 * Description     : 
				Modify PO/Transfer Details in RMS.
			 
 * 
 * Input           : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
					This object contains the PO/Transfer information.
				
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     : 
					InvocationSuccess object contains the success or failure status of processing the 
                              confirmation requests.
				
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
					message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
					create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
					"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE modifyDetailLocPOTsfDesc(
                          I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_LocPOTsfDesc_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_InvocationSuccess_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : queryDeal
 * Description     : 
				Retrieve Deals Information from RMS
			 
 * 
 * Input           : "RIB_LocPOTsfDealsCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDealsCriVo/v1
 * Description     : 
					This object consists of the store, item for which the Deals information need to be retrieved.
				
 * 
 * Output          : "RIB_LocPOTsfDealsColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDealsColDesc/v1
 * Description     : 
					This object consists of the deals for the input store,item.
				
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
					message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
					create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
					"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE queryDeal(
                          I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_LocPOTsfDealsCriVo_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_LocPOTsfDealsColDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : queryItemSales
 * Description     : 
				Retrieve the Item Sales information from RMS.
			 
 * 
 * Input           : "RIB_LocPOTsfItmSlsCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfItmSlsCriVo/v1
 * Description     : 
					This object consists of the input Store,Item for which sales information need to be retrieved.
				
 * 
 * Output          : "RIB_LocPOTsfItmSlsColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfItmSlsColDesc/v1
 * Description     : 
					This object consists of the item sales information for the input store,item.
				
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
					message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
					create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
					"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE queryItemSales(
                          I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_LocPOTsfItmSlsCriVo_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_LocPOTsfItmSlsColDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : queryStoreOrder
 * Description     : 
				Retrieve header level PO or transfer information from RMS.
			 
 * 
 * Input           : "RIB_LocPOTsfHdrCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfHdrCriVo/v1
 * Description     : 
					This object contains the search criteria for PO or transfer in RMS.
				
 * 
 * Output          : "RIB_LocPOTsfHdrColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfHdrColDesc/v1
 * Description     : 
					This object contains the header level information for POs and transfers retrieved from RMS based on the input.
				
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
					message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
					create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
					"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE queryStoreOrder(
                          I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_LocPOTsfHdrCriVo_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_LocPOTsfHdrColDesc_REC"
                          );
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : queryStoreOrderDetail
 * Description     : 
				Retrieve the header/details of a PO or transfer from RMS.
			 
 * 
 * Input           : "RIB_LocPOTsfDtlsCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDtlsCriVo/v1
 * Description     : 
					This object contains the search criteria for a PO or transfer in RMS, including the PO/Transfer number.
				
 * 
 * Output          : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
					This object contains the store order details related to a PO or transfer based on the input.
				
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
					message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
					create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
					"soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE queryStoreOrderDetail(
                          I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_LocPOTsfDtlsCriVo_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_LocPOTsfDesc_REC"
                          );
/******************************************************************************/



END StoreOrderServiceProviderImpl;
/