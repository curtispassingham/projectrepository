CREATE OR REPLACE PACKAGE BODY PO_INDUCT_ERRVAL AS
------------------------------------------------------------------------------------------
FUNCTION CHECK_PO_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                         I_order_no        IN       V_POIND_ERRORS_LIST.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'PO_INDUCT_ERRVAL.CHECK_PO_EXISTS';
   L_exists    VARCHAR2(1)  := NULL;

   cursor C_CHECK_ORDER is
     select 'x'
       from v_poind_errors_list
      where process_id = I_process_id
        and order_no   = I_order_no;

BEGIN
   open C_CHECK_ORDER;
   fetch C_CHECK_ORDER into L_exists;
   if C_CHECK_ORDER%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_CHECK_ORDER;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_ORDER%ISOPEN then
         close C_CHECK_ORDER;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_PO_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_SUPPLIER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_sup_name        IN OUT   VARCHAR2,
                               O_exists          IN OUT   BOOLEAN,
                               I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                               I_supplier        IN       V_POIND_ERRORS_LIST.SUPPLIER%TYPE)
RETURN BOOLEAN IS
   L_program    VARCHAR2(50) := 'PO_INDUCT_ERRVAL.CHECK_SUPPLIER_EXISTS';

   cursor C_GET_SUPPLIER is
      select DISTINCT sup_name_trans
        from v_poind_errors_list
       where process_id = I_process_id
         and supplier   = I_supplier;

BEGIN
   open C_GET_SUPPLIER;
   fetch C_GET_SUPPLIER into O_sup_name;
   if C_GET_SUPPLIER%NOTFOUND then
      O_sup_name := NULL;
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_GET_SUPPLIER;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_SUPPLIER%ISOPEN then
         close C_GET_SUPPLIER;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_SUPPLIER_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_LOC_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_location_name   IN OUT   STORE.STORE_NAME%TYPE,
                          O_exists          IN OUT   BOOLEAN,
                          I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                          I_location        IN       V_POIND_ERRORS_LIST.LOCATION%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(50) := 'PO_INDUCT_ERRVAL.CHECK_LOC_EXISTS';

   cursor C_GET_LOCATION is
      select DISTINCT loc_name_trans
        from v_poind_errors_list
       where process_id = I_process_id
         and location   = I_location;

BEGIN
   open C_GET_LOCATION;
   fetch C_GET_LOCATION into O_location_name;
   if C_GET_LOCATION%NOTFOUND then
      O_location_name := NULL;
      O_exists        := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_GET_LOCATION;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_LOCATION%ISOPEN then
         close C_GET_LOCATION;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_LOC_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_item_desc       IN OUT   VARCHAR2,
                           O_exists          IN OUT   BOOLEAN,
                           I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                           I_item            IN       V_POIND_ERRORS_LIST.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'PO_INDUCT_ERRVAL.CHECK_ITEM_EXISTS';

   cursor C_GET_ITEM is
     select DISTINCT item_desc_trans
       from v_poind_errors_list
      where process_id = I_process_id
        and item       = I_item;

BEGIN
   open C_GET_ITEM;
   fetch C_GET_ITEM into O_item_desc;
   if C_GET_ITEM%NOTFOUND then
      O_item_desc := NULL;
      O_exists    := FALSE;
   else
      O_exists    := TRUE;
   end if;
   close C_GET_ITEM;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_ITEM%ISOPEN then
         close C_GET_ITEM;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_ITEM_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_TABLE_DESC_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists          IN OUT   BOOLEAN,
                                 I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                                 I_wksht           IN       V_POIND_ERRORS_LIST.TABLE_DESC%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'PO_INDUCT_ERRVAL.CHECK_TABLE_DESC_EXISTS';
   L_exists    VARCHAR2(1)  := NULL;

   cursor C_CHECK_TABLE_DESC is
      select 'x'
        from v_poind_errors_list
       where process_id = I_process_id
         and upper(table_desc) = upper(I_wksht);

BEGIN
   open C_CHECK_TABLE_DESC;
   fetch C_CHECK_TABLE_DESC into L_exists;
   if C_CHECK_TABLE_DESC%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_CHECK_TABLE_DESC;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_TABLE_DESC%ISOPEN then
         close C_CHECK_TABLE_DESC;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_TABLE_DESC_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ROW_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT   BOOLEAN,
                          I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                          I_row             IN       V_POIND_ERRORS_LIST.ROW_SEQ%TYPE,
                          I_wksht           IN       V_POIND_ERRORS_LIST.TABLE_DESC%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'PO_INDUCT_ERRVAL.CHECK_ROW_EXISTS';
   L_exists    VARCHAR2(1)  := NULL;

   cursor C_CHECK_ROW is
      select 'x'
        from v_poind_errors_list
       where upper(table_desc) = upper(I_wksht)
         and process_id        = I_process_id
         and row_seq           = I_row;

BEGIN
   open C_CHECK_ROW;
   fetch C_CHECK_ROW into L_exists;
   if C_CHECK_ROW%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_CHECK_ROW;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_ROW%ISOPEN then
         close C_CHECK_ROW;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_ROW_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_COLUMN_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT   BOOLEAN,
                             I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                             I_column          IN       V_POIND_ERRORS_LIST.COLUMN_DESC%TYPE,
                             I_wksht           IN       V_POIND_ERRORS_LIST.TABLE_DESC%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'PO_INDUCT_ERRVAL.CHECK_COLUMN_EXISTS';
   L_exists    VARCHAR2(1)  := NULL;

   cursor C_CHECK_COLUMN is
      select 'x'
        from v_poind_errors_list
       where upper(table_desc) = I_wksht
         and process_id        = I_process_id
         and upper(column_desc) = upper(I_column);

BEGIN
   open C_CHECK_COLUMN;
   fetch C_CHECK_COLUMN into L_exists;
   if C_CHECK_COLUMN%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_CHECK_COLUMN;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_COLUMN%ISOPEN then
         close C_CHECK_COLUMN;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_COLUMN_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ERR_MSG_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_process_id      IN       V_POIND_ERRORS_LIST.PROCESS_ID%TYPE,
                              I_error           IN       V_POIND_ERRORS_LIST.ERROR_MSG%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'PO_INDUCT_ERRVAL.CHECK_ERR_MSG_EXISTS';
   L_exists    VARCHAR2(1)  := NULL;

   cursor C_CHECK_ERROR_MSG is
      select 'x'
        from v_poind_errors_list
       where process_id = I_process_id
         and error_msg  = I_error;

BEGIN
   open C_CHECK_ERROR_MSG;
   fetch C_CHECK_ERROR_MSG into L_exists;
   if C_CHECK_ERROR_MSG%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_CHECK_ERROR_MSG;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_ERROR_MSG%ISOPEN then
         close C_CHECK_ERROR_MSG;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_ERR_MSG_EXISTS;
------------------------------------------------------------------------------------------
END PO_INDUCT_ERRVAL;
/
