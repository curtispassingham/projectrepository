



CREATE OR REPLACE PACKAGE BODY TRANS_FINALIZE_SQL AS
--------------------------------------------------------------------------------------
FUNCTION FINALIZE(O_error_message     IN OUT   VARCHAR2,
                  I_vessel_id         IN       TRANSPORTATION.VESSEL_ID%TYPE,
                  I_voyage_flt_id     IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                  I_est_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                  I_entry_no          IN       CE_HEAD.ENTRY_NO%TYPE,
                  I_import_country    IN       CE_HEAD.IMPORT_COUNTRY_ID%TYPE,
                  I_currency_code     IN       CE_HEAD.CURRENCY_CODE%TYPE,
                  I_broker_id         IN       CE_HEAD.BROKER_ID%TYPE)
RETURN BOOLEAN IS

   L_program                VARCHAR2(64)      := 'TRANS_FINALIZE_SQL.FINALIZE';
   L_finalize_vve           VARCHAR2(1)       := NULL;
   L_ce_lic_visa_exists     VARCHAR2(1)       := 'N';
   L_exists                 VARCHAR2(1)       := 'N';
   L_first_time             VARCHAR2(1)       := 'Y';
   L_new_vve                VARCHAR2(1)       := 'Y';
   L_vdate                  PERIOD.VDATE%TYPE := GET_VDATE;
   L_entry_no_exists        BOOLEAN;
   L_transport_exists       BOOLEAN;
   L_transportation_id      TRANSPORTATION.TRANSPORTATION_ID%TYPE;
   L_vessel_id              TRANSPORTATION.VESSEL_ID%TYPE;
   L_voyage_flt_id          TRANSPORTATION.VOYAGE_FLT_ID%TYPE;
   L_est_depart_date        TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE;
   L_act_depart_date        TRANSPORTATION.ACTUAL_DEPART_DATE%TYPE;
   L_vessel_scac_code       TRANSPORTATION.VESSEL_SCAC_CODE%TYPE;
   L_hts                    CE_CHARGES.HTS%TYPE;
   L_effect_from            CE_CHARGES.EFFECT_FROM%TYPE;
   L_effect_to              CE_CHARGES.EFFECT_TO%TYPE;
   L_import_country_id      CE_HEAD.IMPORT_COUNTRY_ID%TYPE;
   L_lading_port            TRANSPORTATION.LADING_PORT%TYPE;
   L_discharge_port         TRANSPORTATION.DISCHARGE_PORT%TYPE;
   L_tran_mode_id           TRANSPORTATION.TRAN_MODE_ID%TYPE;
   L_arrival_date           TRANSPORTATION.ACTUAL_ARRIVAL_DATE%TYPE;
   L_export_country_id      TRANSPORTATION.EXPORT_COUNTRY_ID%TYPE;
   L_shipment_no            TRANSPORTATION.SHIPMENT_NO%TYPE;
   L_prev_vessel_id         TRANSPORTATION.VESSEL_ID%TYPE;
   L_prev_voyage_flt_id     TRANSPORTATION.VOYAGE_FLT_ID%TYPE;
   L_prev_est_depart_date   TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE;
   L_next_ce_id             CE_HEAD.CE_ID%TYPE;
   L_order_no               ORDHEAD.ORDER_NO%TYPE;
   L_prev_order_no          ORDHEAD.ORDER_NO%TYPE;
   L_prev_item              ITEM_MASTER.ITEM%TYPE;
   L_bl_awb_id              TRANSPORTATION.BL_AWB_ID%TYPE;
   L_container_id           TRANSPORTATION.CONTAINER_ID%TYPE;
   L_prev_bl_awb_id         TRANSPORTATION.BL_AWB_ID%TYPE;
   L_invoice_id             TRANSPORTATION.INVOICE_ID%TYPE;
   L_prev_invoice_id        TRANSPORTATION.INVOICE_ID%TYPE;
   L_invoice_date           TRANSPORTATION.INVOICE_DATE%TYPE;
   L_prev_invoice_date      TRANSPORTATION.INVOICE_DATE%TYPE;
   L_in_transit_no          TRANSPORTATION.IN_TRANSIT_NO%TYPE;
   L_prev_in_transit_no     TRANSPORTATION.IN_TRANSIT_NO%TYPE;
   L_in_transit_date        TRANSPORTATION.IN_TRANSIT_DATE%TYPE;
   L_prev_in_transit_date   TRANSPORTATION.IN_TRANSIT_DATE%TYPE;
   L_invoice_amt            TRANSPORTATION.INVOICE_AMT%TYPE;
   L_insert_invoice_amt     TRANSPORTATION.INVOICE_AMT%TYPE;
   L_currency_code          TRANSPORTATION.CURRENCY_CODE%TYPE;
   L_ord_currency_code      TRANSPORTATION.CURRENCY_CODE%TYPE;
   L_exchange_rate          TRANSPORTATION.EXCHANGE_RATE%TYPE;
   L_ord_exchange_rate      TRANSPORTATION.EXCHANGE_RATE%TYPE;
   L_carton_qty             TRANSPORTATION.CARTON_QTY%TYPE;
   L_insert_carton_qty      TRANSPORTATION.CARTON_QTY%TYPE;
   L_carton_uom             TRANSPORTATION.CARTON_UOM%TYPE;
   L_prev_carton_uom        TRANSPORTATION.CARTON_UOM%TYPE;
   L_gross_wt               TRANSPORTATION.GROSS_WT%TYPE;
   L_insert_gross_wt        TRANSPORTATION.GROSS_WT%TYPE;
   L_gross_wt_uom           TRANSPORTATION.GROSS_WT_UOM%TYPE;
   L_prev_gross_wt_uom      TRANSPORTATION.GROSS_WT_UOM%TYPE;
   L_net_wt                 TRANSPORTATION.NET_WT%TYPE;
   L_insert_net_wt          TRANSPORTATION.NET_WT%TYPE;
   L_net_wt_uom             TRANSPORTATION.NET_WT_UOM%TYPE;
   L_cubic                  TRANSPORTATION.CUBIC%TYPE;
   L_insert_cubic           TRANSPORTATION.CUBIC%TYPE;
   L_prev_net_wt_uom        TRANSPORTATION.NET_WT_UOM%TYPE;
   L_cubic_uom              TRANSPORTATION.CUBIC_UOM%TYPE;
   L_prev_cubic_uom         TRANSPORTATION.CUBIC_UOM%TYPE;
   L_rush_ind               TRANSPORTATION.RUSH_IND%TYPE;
   L_prev_rush_ind          TRANSPORTATION.RUSH_IND%TYPE;
   L_tariff_treatment       CE_ORD_ITEM.TARIFF_TREATMENT%TYPE;
   L_alc_status             CE_ORD_ITEM.ALC_STATUS%TYPE := 'P';
   L_quantity               TRANS_SKU.QUANTITY%TYPE;
   L_insert_quantity        TRANS_SKU.QUANTITY%TYPE;
   L_quantity_uom           TRANS_SKU.QUANTITY_UOM%TYPE;
   L_order_item_qty         ORDLOC.QTY_ORDERED%TYPE;
   L_order_total_qty        ORDLOC.QTY_ORDERED%TYPE;
   L_unit_cost              ORDLOC.UNIT_COST%TYPE;
   L_qty_1                  TRANSPORTATION.ITEM_QTY%TYPE;
   L_qty_2                  TRANSPORTATION.ITEM_QTY%TYPE;
   L_qty_3                  TRANSPORTATION.ITEM_QTY%TYPE;
   L_units_1                HTS.UNITS_1%TYPE;
   L_units_2                HTS.UNITS_2%TYPE;
   L_units_3                HTS.UNITS_3%TYPE;
   L_specific_rate          HTS_TARIFF_TREATMENT.SPECIFIC_RATE%TYPE;
   L_av_rate                HTS_TARIFF_TREATMENT.AV_RATE%TYPE;
   L_other_rate             HTS_TARIFF_TREATMENT.OTHER_RATE%TYPE;
   L_cvd_case_no            HTS_CVD.CASE_NO%TYPE;
   L_ad_case_no             HTS_AD.CASE_NO%TYPE;
   L_duty_comp_code         HTS.DUTY_COMP_CODE%TYPE;
   L_origin_country_id      COUNTRY.COUNTRY_ID%TYPE;
   L_supplier               SUPS.SUPPLIER%TYPE;
   L_payee                  PARTNER.PARTNER_ID%TYPE     := NULL;
   L_payee_type             PARTNER.PARTNER_TYPE%TYPE   := NULL;
   L_total_ce_comp          CE_CHARGES.COMP_VALUE%TYPE;
   L_desc                   ITEM_MASTER.ITEM_DESC%TYPE;
   L_status                 ITEM_MASTER.STATUS%TYPE;
   L_item_parent            ITEM_MASTER.ITEM_PARENT%TYPE;
   L_item_grandparent       ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
   L_item                   ITEM_MASTER.ITEM%TYPE;
   L_entry_no               CE_HEAD.ENTRY_NO%TYPE;
   L_header                 VARCHAR2(1)                 := 'Y';
   L_next_ce_id_temp        CE_HEAD.CE_ID%TYPE;
   L_table                  VARCHAR2(30);
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT (RECORD_LOCKED, -54);
   L_level                  VARCHAR2(30);
   L_transportation_item    TRANSPORTATION.ITEM%TYPE;
   L_zero_qty               VARCHAR2(1)                 := 'Y';
   L_prev_import_country_id ORDHEAD.IMPORT_COUNTRY_ID%TYPE := NULL;
   L_country_exists         VARCHAR2(1)       := 'N';   
   --

   /* ce_head type */
   TYPE ce_head_type is table of ce_head.ce_id%TYPE index by binary_integer;
   LP_ce_head               ce_head_type;
   item_count               NUMBER := 0;
   header_count             NUMBER := 0;

   cursor C_CHECK_FINALIZE_VVE is
      select 'X'
        from transportation
       where vessel_id             = I_vessel_id
         and voyage_flt_id         = I_voyage_flt_id
         and estimated_depart_date = I_est_depart_date
         and candidate_ind         = 'Y'
         and (status              != 'F'
          or status               is NULL)
         and rownum                = 1;

   cursor C_CHECK_IMPORT_COUNTRY is
      select 'Y'
        from transportation t,
             ordhead        o
       where t.vessel_id             = I_vessel_id
         and t.voyage_flt_id         = I_voyage_flt_id
         and t.estimated_depart_date = I_est_depart_date
         and t.order_no              = o.order_no
         and o.import_country_id     = I_import_country;

   cursor C_FINALIZE_VVE is
      select distinct t.transportation_id,
             t.vessel_id,
             t.voyage_flt_id,
             t.estimated_depart_date,
             o.import_country_id,
             t.order_no,
             t.item,
             t.invoice_id,
             t.vessel_scac_code,
             t.lading_port,
             t.discharge_port,
             t.tran_mode_id,
             t.actual_arrival_date,
             t.export_country_id,
             t.shipment_no,
             t.actual_depart_date,
             'x' lvlflag
        from transportation t,
             ordhead o,
             item_master im,
             ordloc ol
       where t.order_no              = o.order_no
         and ol.order_no             = o.order_no
         and ol.item                 = t.item
         and ol.qty_ordered          > 0
         and o.import_order_ind      = 'Y'
         and t.vessel_id             = NVL(I_vessel_id,t.vessel_id)
         and t.voyage_flt_id         = NVL(I_voyage_flt_id,t.voyage_flt_id)
         and t.estimated_depart_date = NVL(I_est_depart_date,t.estimated_depart_date)
         and t.candidate_ind         = 'Y'
         and (t.status              != 'F'
              or t.status           is NULL)
         and t.item                  = im.item
         and im.item_level           = im.tran_level
       UNION ALL
      select distinct t.transportation_id,
             t.vessel_id,
             t.voyage_flt_id,
             t.estimated_depart_date,
             o.import_country_id,
             t.order_no,
             ts.item,
             t.invoice_id,
             t.vessel_scac_code,
             t.lading_port,
             t.discharge_port,
             t.tran_mode_id,
             t.actual_arrival_date,
             t.export_country_id,
             t.shipment_no,
             t.actual_depart_date,
             'y' lvlflag
        from transportation t,
             ordhead o,
             item_master im,
             trans_sku ts,
             ordloc ol
       where t.order_no              = o.order_no
         and ol.order_no             = o.order_no
         and ol.item                 = ts.item
         and ol.qty_ordered          > 0
         and o.import_order_ind      = 'Y'
         and t.vessel_id             = NVL(I_vessel_id,t.vessel_id)
         and t.voyage_flt_id         = NVL(I_voyage_flt_id,t.voyage_flt_id)
         and t.estimated_depart_date = NVL(I_est_depart_date,t.estimated_depart_date)
         and t.candidate_ind         = 'Y'
         and (t.status              != 'F'
              or t.status           is NULL)
         and t.item                  = im.item
         and im.item_level           < im.tran_level
         and t.transportation_id     = ts.transportation_id
       UNION ALL
      select distinct t.transportation_id,
             t.vessel_id,
             t.voyage_flt_id,
             t.estimated_depart_date,
             o.import_country_id,
             t.order_no,
             os.item,
             t.invoice_id,
             t.vessel_scac_code,
             t.lading_port,
             t.discharge_port,
             t.tran_mode_id,
             t.actual_arrival_date,
             t.export_country_id,
             t.shipment_no,
             t.actual_depart_date,
             'z' lvlflag
        from transportation t,
             ordhead o,
             item_master im,
             ordsku os,
             ordloc ol
       where t.order_no              = o.order_no
         and ol.order_no             = o.order_no
         and ol.qty_ordered          > 0
         and o.import_order_ind      = 'Y'
         and t.vessel_id             = NVL(I_vessel_id,t.vessel_id)
         and t.voyage_flt_id         = NVL(I_voyage_flt_id,t.voyage_flt_id)
         and t.estimated_depart_date = NVL(I_est_depart_date,t.estimated_depart_date)
         and t.candidate_ind         = 'Y'
         and (t.status              != 'F'
              or t.status           is NULL)
         and t.item                  = im.item
         and im.item_level           < im.tran_level
         and o.order_no              = os.order_no
         and not exists (select 'x'
                           from trans_sku ts
                          where t.transportation_id = ts.transportation_id
                            and rownum              = 1)
         and exists (select 'x'
                       from item_master im2
                      where (im2.item_parent        = im.item
                            or im2.item_grandparent = im.item)
                        and im2.item                = os.item
                        and rownum                  = 1)
    ORDER BY 2,
             3,
             4,
             5,
             6,
             7,
             8;

   cursor C_ORDER_ITEM is
      select t.bl_awb_id,
             t.container_id,
             t.invoice_date,
             t.item_qty_uom,
             t.carton_uom,
             t.gross_wt_uom,
             t.net_wt_uom,
             t.cubic_uom,
             t.in_transit_no,
             t.in_transit_date,
             t.invoice_amt,
             t.rush_ind
        from transportation  t,
             ordhead         o
       where t.vessel_id             = L_vessel_id
         and t.voyage_flt_id         = L_voyage_flt_id
         and t.estimated_depart_date = L_est_depart_date
         and t.order_no              = L_order_no
         and t.item                  = L_item 
         and nvl(t.invoice_id,'-999')= nvl(L_invoice_id,'-999')
         and t.candidate_ind         = 'Y'
         and t.order_no              = o.order_no
         and o.import_order_ind      = 'Y'
         and t.transportation_id     = L_transportation_id
       UNION ALL
      select t.bl_awb_id,
             t.container_id,
             t.invoice_date,
             ts.quantity_uom,
             t.carton_uom,
             t.gross_wt_uom,
             t.net_wt_uom,
             t.cubic_uom,
             t.in_transit_no,
             t.in_transit_date,
             t.invoice_amt,
             t.rush_ind
        from transportation  t,
             ordhead         o,
             trans_sku      ts
       where t.vessel_id             = L_vessel_id
         and t.voyage_flt_id         = L_voyage_flt_id
         and t.estimated_depart_date = L_est_depart_date
         and t.order_no              = L_order_no
         and ts.item                  = L_item 
         and nvl(t.invoice_id,'-999')= nvl(L_invoice_id,'-999')
         and t.candidate_ind         = 'Y'
         and t.order_no              = o.order_no
         and o.import_order_ind      = 'Y'
         and ts.transportation_id    = t.transportation_id
       UNION ALL
      select t.bl_awb_id,
             t.container_id,
             t.invoice_date,
             t.item_qty_uom,
             t.carton_uom,
             t.gross_wt_uom,
             t.net_wt_uom,
             t.cubic_uom,
             t.in_transit_no,
             t.in_transit_date,
             t.invoice_amt,
             t.rush_ind
        from transportation  t,
             ordsku os
       where t.vessel_id             = L_vessel_id
         and t.voyage_flt_id         = L_voyage_flt_id
         and t.estimated_depart_date = L_est_depart_date
         and t.order_no              = L_order_no
         and os.item                  =  L_item 
         and nvl(t.invoice_id,'-999')= nvl(L_invoice_id,'-999')
         and t.candidate_ind         = 'Y'
         and os.order_no             = t.order_no
         and t.transportation_id     = L_transportation_id
         and (os.item                = t.item
             or os.item in (select im.item
                              from item_master im
                             where im.item_parent = t.item
                                or im.item_grandparent = t.item));

   cursor C_SUM_TRANS_SKU is
      select NVL(SUM(quantity),0)
        from trans_sku
       where transportation_id = L_transportation_id;

   cursor C_GET_SUPPLIER is
      select s.supplier
        from sup_import_attr s,
             ordhead         o
       where o.order_no = L_order_no
         and o.supplier = s.supplier;

   cursor C_TRANS_SKU is
      select t.quantity,
             t.quantity_uom,
             ol.unit_cost,
             oh.currency_code,
             oh.exchange_rate
        from trans_sku  t,
             ordloc    ol,
             ordhead   oh
       where t.transportation_id = L_transportation_id
         and oh.order_no         = ol.order_no
         and ol.order_no         = L_order_no
         and ol.item             = t.item
         and t.item              = L_item;

   cursor C_SUM_ORDLOC is
      select NVL(SUM(qty_ordered),0)
        from ordloc ol,
             item_master im
       where ol.order_no             = L_order_no
         and (im.item                = L_item
              or im.item_parent      = L_item
              or im.item_grandparent = L_item)
         and im.item_level           = im.tran_level;

   cursor C_ORDLOC is
      select NVL(SUM(ol.qty_ordered),0) qty,
             im.standard_uom,
             ol.unit_cost,
             oh.currency_code,
             oh.exchange_rate
        from ordloc    ol,
             ordhead   oh,
             item_master im
       where oh.order_no         = L_order_no
         and ol.order_no         = oh.order_no
         and ol.item             = im.item
         and im.item             = L_item
         and im.item_level       = im.tran_level
    group by ol.item,
             im.standard_uom,
             ol.unit_cost,
             oh.currency_code,
             oh.exchange_rate;

   cursor C_ORDSKU_HTS is
      select distinct oh.hts,
             oh.origin_country_id,
             oh.effect_from,
             oh.effect_to
        from ordsku_hts oh
       where oh.order_no   = L_order_no
         and (oh.item      = L_item
          or (oh.pack_item = L_item));

   cursor C_CE_LIC_VISA_EXIST(C_item item_master.item%type) is
      select 'Y'
        from ce_lic_visa c,
             trans_lic_visa t
       where c.ce_id                 = L_next_ce_id
         and c.vessel_id             = L_vessel_id
         and c.voyage_flt_id         = L_voyage_flt_id
         and c.estimated_depart_date = L_est_depart_date
         and c.order_no              = L_order_no
         and c.item                  = C_item
         and c.license_visa_type     = t.license_visa_type
         and c.license_visa_id       = t.license_visa_id
         and t.transportation_id     = L_transportation_id;

   cursor C_CHECK_CE_ORD_ITEM(C_item item_master.item%type) is
      select 'Y'
        from ce_ord_item
       where ce_id                 = L_next_ce_id
         and vessel_id             = L_vessel_id
         and voyage_flt_id         = L_voyage_flt_id
         and estimated_depart_date = L_est_depart_date
         and order_no              = L_order_no
         and item                  = C_item;

   cursor C_LOCK_CE_ORD_ITEM(C_item item_master.item%type) is
      select 'x'
        from ce_ord_item
       where ce_id                 = L_next_ce_id
         and vessel_id             = L_vessel_id
         and voyage_flt_id         = L_voyage_flt_id
         and estimated_depart_date = L_est_depart_date
         and order_no              = L_order_no
         and item                  = C_item
         for update nowait;

   cursor C_LOCK_TRANSPORTATION is
      select 'x'
        from transportation
       where transportation_id = L_transportation_id
         for update nowait;

   cursor C_LOCK_CE_TEMP is
      select 'x'
        from ce_temp
         for update nowait;

BEGIN
   --- check if specific V/V/E is ready to be finalized (Candidate Ind is 'Y'es)
   if ((I_vessel_id       is NOT NULL)  and
       (I_voyage_flt_id   is NOT NULL)  and
       (I_est_depart_date is NOT NULL)) then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_FINALIZE_VVE',
                       'TRANSPORTATION',
                       NULL);
      open C_CHECK_FINALIZE_VVE;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_FINALIZE_VVE',
                       'TRANSPORTATION',
                       NULL);
      fetch C_CHECK_FINALIZE_VVE into L_finalize_vve;
      if L_finalize_vve is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_FINALIZE',
                                               NULL,
                                               NULL,
                                               NULL);
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_FINALIZE_VVE',
                          'TRANSPORTATION',
                          NULL);
         close C_CHECK_FINALIZE_VVE;
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_FINALIZE_VVE',
                       'TRANSPORTATION',
                       NULL);
      close C_CHECK_FINALIZE_VVE;
   end if;
   ---
   --- cannot have duplicate entry numbers
   if I_entry_no is NOT NULL then
      if CE_SQL.VALIDATE_ENTRY_NO(O_error_message,
                                  L_entry_no_exists,
                                  I_entry_no) = FALSE then
         return FALSE;
      end if;
      ---
      if L_entry_no_exists then
         O_error_message := SQL_LIB.CREATE_MSG('DUP_ENTRY_NO',NULL,NULL,NULL);
         return FALSE;
      end if;
   end if;
   ---
   --- if no specific VVE was passed in, then process all V/V/E's that have
   --- at least one instance of Candidate Indicator = 'Y'es. Or process the
   --- specific V/V/E passed in
   SQL_LIB.SET_MARK('OPEN','C_FINALIZE_VVE','TRANSPORTATION',NULL);
   for C_FINALIZE_VVE_REC in C_FINALIZE_VVE loop
      if I_import_country is NOT NULL and L_prev_import_country_id is NULL then
         open C_CHECK_IMPORT_COUNTRY;
         fetch C_CHECK_IMPORT_COUNTRY into L_country_exists; 
         --
         if L_country_exists = 'N' then
            O_error_message := SQL_LIB.CREATE_MSG('SAME_ORD_IMP_CTRY_CE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHECK_IMPORT_COUNTRY',
                             'TRANSPORTATION, ORDHEAD',
                              NULL);
            close C_CHECK_IMPORT_COUNTRY;
            return FALSE;
         end if;
         --
         close C_CHECK_IMPORT_COUNTRY;
      end if;    
      L_transportation_id  := C_FINALIZE_VVE_REC.transportation_id;
      L_vessel_id          := C_FINALIZE_VVE_REC.vessel_id;
      L_voyage_flt_id      := C_FINALIZE_VVE_REC.voyage_flt_id;
      L_est_depart_date    := C_FINALIZE_VVE_REC.estimated_depart_date;
      L_order_no           := C_FINALIZE_VVE_REC.order_no;
      L_item               := C_FINALIZE_VVE_REC.item;
      L_invoice_id         := C_FINALIZE_VVE_REC.invoice_id;
      L_vessel_scac_code   := C_FINALIZE_VVE_REC.vessel_scac_code;
      L_lading_port        := C_FINALIZE_VVE_REC.lading_port;
      L_discharge_port     := C_FINALIZE_VVE_REC.discharge_port;
      L_tran_mode_id       := C_FINALIZE_VVE_REC.tran_mode_id;
      L_arrival_date       := C_FINALIZE_VVE_REC.actual_arrival_date;
      L_export_country_id  := C_FINALIZE_VVE_REC.export_country_id;
      L_shipment_no        := C_FINALIZE_VVE_REC.shipment_no;
      L_act_depart_date    := C_FINALIZE_VVE_REC.actual_depart_date;
      L_level              := C_FINALIZE_VVE_REC.lvlflag;
      L_zero_qty           := 'N';
      L_import_country_id  := C_FINALIZE_VVE_REC.import_country_id;

      ---
      -- if First Timed through loop, automatically get next CE_ID by calling header (set to Y).
      -- Get next ce_id if the same item under the same PO will contain another Invoice or
      -- a new PO exists for the same vessel/voyage/estimated_depart_date by calling header (set to Y).
      -- if there multiple items under same vessle/voyage/esitmated_depart_date/PO combination, L_header := 'N' meaning no header needed, just details.

      if ((L_prev_vessel_id       != L_vessel_id)        or
          (L_prev_voyage_flt_id   != L_voyage_flt_id)    or
          (L_prev_est_depart_date != L_est_depart_date)  or
          (L_first_time = 'Y'))                          then

         ---
         --- populate 'previous' previous with new V/V/E

         L_new_vve              := 'Y';
         L_prev_vessel_id       := L_vessel_id;
         L_prev_voyage_flt_id   := L_voyage_flt_id;
         L_prev_est_depart_date := L_est_depart_date;

         item_count             := 1;
         ---
         if CE_SQL.GET_NEXT_CE_ID(O_error_message,
                                  L_next_ce_id) = FALSE then
            return FALSE;
         end if;
         ---
         ---put new ce_id on ce_temp
         insert into ce_temp(ce_id,
                             transportation_id)
                      values(L_next_ce_id,
                             L_transportation_id);
         ---
         LP_ce_head(item_count)     := L_next_ce_id;
         header_count               := 1;
         L_header                   := 'Y';

      else

         if (L_prev_order_no = L_order_no) then
            if (L_prev_item   = L_item) then
               if (L_prev_invoice_id != L_invoice_id) then
                  item_count := item_count+1;
                  if (item_count > header_count) then
                     if CE_SQL.GET_NEXT_CE_ID(O_error_message,
                                             L_next_ce_id) = FALSE then
                        return FALSE;
                     end if;
                     ---
                     -- put new ce_id on ce_temp
                     insert into ce_temp(ce_id,
                                         transportation_id)
                                  values(L_next_ce_id,
                                         L_transportation_id);

                     LP_ce_head(item_count)     := L_next_ce_id;
                     header_count               := header_count+1;
                     L_header := 'Y';
                  else
                     L_next_ce_id               := LP_ce_head(item_count);
                     -- put previous ce_id on ce_temp
                     insert into ce_temp(ce_id,
                                         transportation_id)
                                  values(L_next_ce_id,
                                         L_transportation_id);
                     L_header := 'N';
                  end if;
               else
                  L_next_ce_id               := LP_ce_head(item_count);
                  -- put previous ce_id on ce_temp
                  insert into ce_temp(ce_id,
                                      transportation_id)
                               values(L_next_ce_id,
                                      L_transportation_id);
                  L_header := 'N';
               end if;
            elsif L_prev_item != L_item then
               item_count   := 1;
               L_prev_invoice_id  := NULL;
               L_next_ce_id := LP_ce_head(item_count);
               -- put previous ce_id on ce_temp
               insert into ce_temp(ce_id,
                                   transportation_id)
                            values(L_next_ce_id,
                                   L_transportation_id);
               L_header     := 'N';
            end if; -- loop for L_prev_item and L_item
         elsif (L_prev_order_no != L_order_no)  then
         --Different PO for the same vessel/voyage/estimated_depart_date
           if L_prev_import_country_id = L_import_country_id then
           --Same Invoices for the same vessel/voyage/estimated_depart_date
             L_next_ce_id := LP_ce_head(item_count);
             -- put previous ce_id on ce_temp
             insert into ce_temp(ce_id,
                                 transportation_id)
                          values(L_next_ce_id,
                                 L_transportation_id);
             L_header := 'N';
           else
           --Different Invoices for the same vessel/voyage/estimated_depart_date
            item_count   := 1;
            ---
            if CE_SQL.GET_NEXT_CE_ID(O_error_message,
                                     L_next_ce_id) = FALSE then
               return FALSE;
            end if;
            -- put next ce_id on ce_temp
            insert into ce_temp(ce_id,
                                transportation_id)
                         values(L_next_ce_id,
                                L_transportation_id);
            LP_ce_head(item_count) := L_next_ce_id;
            header_count           := header_count+1;
            L_header := 'Y';
           end if; -- for Invoices
         end if;  -- Loop for L_prev_order_no and L_order_no
      end if; -- for L_first_time
      ---
      if I_currency_code is not NULL then
         if CURRENCY_SQL.GET_RATE(O_error_message,
                                  L_exchange_rate,
                                  I_currency_code,
                                  'U',
                                  L_vdate) = FALSE then
            return FALSE;
         end if;
         ---
         --- don't want to overwrite passed in values
         L_currency_code := I_currency_code;
      else
         if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                          L_currency_code) = FALSE then
            return FALSE;
         end if;
         --
         if CURRENCY_SQL.GET_RATE(O_error_message,
                                  L_exchange_rate,
                                  L_currency_code,
                                  NULL,
                                  NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if PARTNER_SQL.GET_PRIM_IA(O_error_message,
                                 L_payee_type,
                                 L_payee,
                                 L_import_country_id) = FALSE then
         return FALSE;
      end if;
      ---
      if (L_first_time = 'Y') then          
         L_first_time := 'N';
      end if;   --- first time     
         L_entry_no := I_entry_no;

      ---
      -- building of ce_head
      ---

      if L_header = 'Y' then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'CE_HEAD',
                          NULL);
         insert into ce_head(ce_id,
                             status,
                             entry_no,
                             broker_id,
                             import_country_id,
                             currency_code,
                             exchange_rate,
                             live_ind,
                             payee,
                             payee_type)
                      values(L_next_ce_id,
                             'W',                   --- 'W'orksheet
                             L_entry_no,
                             I_broker_id,
                             L_import_country_id,
                             L_currency_code,
                             L_exchange_rate,
                             'N',                   --- 'N'o
                             L_payee,
                             L_payee_type);

          --- building of ce_shipment
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'CE_SHIPMENT',
                          NULL);
         insert into ce_shipment(ce_id,
                                 vessel_id,
                                 voyage_flt_id,
                                 estimated_depart_date,
                                 vessel_scac_code,
                                 lading_port,
                                 discharge_port,
                                 tran_mode_id,
                                 arrival_date,
                                 export_country_id,
                                 shipment_no,
                                 export_date)
                          values(L_next_ce_id,
                                 L_vessel_id,
                                 L_voyage_flt_id,
                                 L_est_depart_date,
                                 L_vessel_scac_code,
                                 L_lading_port,
                                 L_discharge_port,
                                 L_tran_mode_id,
                                 L_arrival_date,
                                 L_export_country_id,
                                 L_shipment_no,
                                 L_act_depart_date);
      end if; --L_header = 'Y'
      ---
      -- Can retrieve for each new order no
      ---
      if L_prev_order_no <> L_order_no then
         --- retrieve supplier from sup_import_attr table
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_SUPPLIER',
                          'SUP_IMPORT_ATTR',
                          NULL);
         open C_GET_SUPPLIER;
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_SUPPLIER',
                          'SUP_IMPORT_ATTR',
                          NULL);
         fetch C_GET_SUPPLIER into L_supplier;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_SUPPLIER',
                          'SUP_IMPORT_ATTR',
                          NULL);
         close C_GET_SUPPLIER;
      end if;
      ---
      if nvl(L_prev_item, ' ') <> L_item or
         L_first_time = 'Y' then
         if ITEM_ATTRIB_SQL.GET_PARENT_INFO(O_error_message,
                                            L_item_parent,
                                            L_desc,
                                            L_item_grandparent,
                                            L_desc,
                                            L_item) = FALSE then
             return FALSE;
          end if;
      end if;
      ---
      -- building of ce_ord_item
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_ORDER_ITEM',
                       'TRANSPORTATION',
                       NULL);
      open C_ORDER_ITEM;
      SQL_LIB.SET_MARK('FETCH',
                       'C_ORDER_ITEM',
                       'TRANSPORTATION',
                       NULL);
      fetch C_ORDER_ITEM into L_bl_awb_id,
                              L_container_id,
                              L_invoice_date,
                              L_quantity_uom,
                              L_carton_uom,
                              L_gross_wt_uom,
                              L_net_wt_uom,
                              L_cubic_uom,
                              L_in_transit_no,
                              L_in_transit_date,
                              L_invoice_amt,
                              L_rush_ind;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ORDER_ITEM',
                       'TRANSPORTATION',
                       NULL);
      close C_ORDER_ITEM;
      ---
      --- L_level is a flag set in C_FINALIZE_VVE_REC.
      --- It is selected into the alias 'lvlflg' and set, in the
      --- cursor, to L_level.  'y' indicates information
      --- is being pulled from the trans_sku table and 'z' indicates
      --- that information is being pulled from ordloc.
      if L_level = 'y' then  --- trans_sku
         --- build ce_ord_item using transku details
         --- calculate total quantity
         ---
         SQL_LIB.SET_MARK('OPEN',
                          'C_SUM_TRANS_SKU',
                          'TRANS_SKU',
                          NULL);
         open C_SUM_TRANS_SKU;
         SQL_LIB.SET_MARK('FETCH',
                          'C_SUM_TRANS_SKU',
                          'TRANS_SKU',
                          NULL);
         fetch C_SUM_TRANS_SKU into L_order_total_qty;
         SQL_LIB.SET_MARK('CL0SE',
                          'C_SUM_TRANS_SKU',
                          'TRANS_SKU',
                          NULL);
         close C_SUM_TRANS_SKU;
         ---
         SQL_LIB.SET_MARK('OPEN',
                          'C_TRANS_SKU',
                          'TRANS_SKU',
                          NULL);
         open C_TRANS_SKU;
         SQL_LIB.SET_MARK('FETCH',
                          'C_TRANS_SKU',
                          'TRANS_SKU',
                          NULL);
         fetch C_TRANS_SKU into L_order_item_qty,
                                L_quantity_uom,
                                L_unit_cost,
                                L_ord_currency_code,
                                L_ord_exchange_rate;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_TRANS_SKU',
                          'TRANS_SKU',
                          NULL);
         close C_TRANS_SKU;
         ---
      elsif L_level = 'z' then  ---ordloc table
         --- build ce_ord_item using ordering details
         --- caluculate total quantity
         ---
         SQL_LIB.SET_MARK('OPEN',
                          'C_SUM_ORDLOC',
                          'ORDLOC',
                          NULL);
         open C_SUM_ORDLOC;
         SQL_LIB.SET_MARK('FETCH',
                          'C_SUM_ORDLOC',
                          'ORDLOC',
                          NULL);
         fetch C_SUM_ORDLOC into L_order_total_qty;
         SQL_LIB.SET_MARK('CL0SE',
                          'C_SUM_ORDLOC',
                          'ORDLOC',
                          NULL);
         close C_SUM_ORDLOC;
         ---
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORDLOC',
                          'ORD_SKU',
                          NULL);
         open C_ORDLOC;
         SQL_LIB.SET_MARK('FETCH',
                          'C_ORDLOC',
                          'ORD_SKU',
                          NULL);
         fetch C_ORDLOC into L_order_item_qty,
                             L_quantity_uom,
                             L_unit_cost,
                             L_ord_currency_code,
                             L_ord_exchange_rate;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORDLOC',
                          'ORD_SKU',
                          NULL);
         close C_ORDLOC;
         ---
      end if;      --transportation table, end L_level check
      ---
      --- use hts to retrieve the tariff treatment
      SQL_LIB.SET_MARK('OPEN',
                       'C_ORDSKU_HTS',
                       'ORDSKU_HTS',
                       NULL);
      open C_ORDSKU_HTS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_ORDSKU_HTS',
                       'ORDSKU_HTS',
                       NULL);
      fetch C_ORDSKU_HTS into L_hts,
                              L_origin_country_id,
                              L_effect_from,
                              L_effect_to;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ORDSKU_HTS',
                       'ORDSKU_HTS',
                       NULL);
      close C_ORDSKU_HTS;
      ---
      if ORDER_HTS_SQL.GET_HTS_DETAILS(O_error_message,
                                       L_tariff_treatment,
                                       L_qty_1,
                                       L_qty_2,
                                       L_qty_3,
                                       L_units_1,
                                       L_units_2,
                                       L_units_3,
                                       L_specific_rate,
                                       L_av_rate,
                                       L_other_rate,
                                       L_cvd_case_no,
                                       L_ad_case_no,
                                       L_duty_comp_code,
                                       L_item,
                                       L_order_no,
                                       L_hts,
                                       L_import_country_id,
                                       L_origin_country_id,
                                       L_effect_from,
                                       L_effect_to) = FALSE then
         return FALSE;
      end if;
      ---
      --- tariff treatment might be NULL for Buyer Packs ordered as Pack; 
      --- The user will have ability to add the Tariff_treatment from CEORDIT screen
      ---
      --- if item was pulled from trans_sku y, or ordloc z, convert.
      if L_level in ('y','z') then
         --- convert from order currency into customs entry currency
         if (L_currency_code != L_ord_currency_code) or
            (L_exchange_rate != L_ord_exchange_rate) then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_unit_cost,
                                    L_ord_currency_code,
                                    L_currency_code,
                                    L_unit_cost,
                                    'C',
                                    NULL,
                                    NULL,
                                    L_ord_exchange_rate,
                                    L_exchange_rate) = FALSE then
               return FALSE;
            end if;
         end if;
         --- get parent level item to use in TRANSPORTATION_SQL.GET_QTYS
         if L_item_parent is not NULL then
            if TRANSPORTATION_SQL.RECORD_EXISTS (O_error_message,
                                                 L_transport_exists,
                                                 L_vessel_id,
                                                 L_voyage_flt_id,
                                                 L_est_depart_date,
                                                 L_order_no,
                                                 L_item_parent,
                                                 L_container_id,
                                                 L_bl_awb_id,
                                                 L_invoice_id,
                                                 NULL) = FALSE then
               return FALSE;
            end if;
            if L_transport_exists = TRUE then
               L_transportation_item := L_item_parent;
            else
               if L_item_grandparent is not NULL then
                  if TRANSPORTATION_SQL.RECORD_EXISTS (O_error_message,
                                                       L_transport_exists,
                                                       L_vessel_id,
                                                       L_voyage_flt_id,
                                                       L_est_depart_date,
                                                       L_order_no,
                                                       L_item_grandparent,
                                                       L_container_id,
                                                       L_bl_awb_id,
                                                       L_invoice_id,
                                                       NULL) = FALSE then
                     return FALSE;
                  end if;
                  if L_transport_exists = TRUE then
                     L_transportation_item := L_item_grandparent;
                  end if;
               end if;
            end if;
         end if;
      else
         --- if item not on trans_sku y, or ordloc z, use current item in TRANSPORTATION_SQL.GET_QTYS.
         L_transportation_item := L_item;
      end if;
      ---
      --- retrieve item's quantity and uom from transportation
      if TRANSPORTATION_SQL.GET_QTYS(O_error_message,
                                     L_carton_qty,
                                     L_carton_uom,
                                     L_quantity,
                                     L_quantity_uom,
                                     L_gross_wt,
                                     L_gross_wt_uom,
                                     L_net_wt,
                                     L_net_wt_uom,
                                     L_cubic,
                                     L_cubic_uom,
                                     L_invoice_amt,
                                     L_supplier,
                                     L_origin_country_id,
                                     L_vessel_id,
                                     L_voyage_flt_id,
                                     L_est_depart_date,
                                     L_order_no,
                                     L_transportation_item,
                                     L_container_id,
                                     L_bl_awb_id,
                                     L_invoice_id,
                                     L_currency_code,
                                     L_exchange_rate) = FALSE then
         return FALSE;
      end if;
      ---
      ---insert ordloc calculation convertions
      if L_level = ('z') then
         ---
         if L_order_item_qty = 0 then
            L_order_item_qty := 1;
         end if;
         --- find ratios of item quantities on trans_sku/ordloc to total
         --- quantity on transportation and insert as manifest_item_qty
         L_insert_quantity     := L_quantity   * (L_order_item_qty / L_order_total_qty);
         L_insert_carton_qty   := L_carton_qty * (L_order_item_qty / L_order_total_qty);
         L_insert_gross_wt     := L_gross_wt   * (L_order_item_qty / L_order_total_qty);
         L_insert_net_wt       := L_net_wt     * (L_order_item_qty / L_order_total_qty);
         L_insert_cubic        := L_cubic      * (L_order_item_qty / L_order_total_qty);
         ---
         L_insert_invoice_amt  := L_invoice_amt * (L_order_item_qty * L_unit_cost)/ (L_order_total_qty * L_unit_cost);
         ---
      else
         -- data is from trans_sku or transportation.
         -- detail-level qtys from trans_sku should override
         -- header-level item_qty from transportation
         if L_level = 'x' then
            L_insert_quantity    := L_quantity;
            L_insert_invoice_amt := L_invoice_amt;
         elsif L_level = 'y' then
            L_insert_quantity := L_order_item_qty;
            L_insert_invoice_amt := (L_invoice_amt * (L_order_item_qty * L_unit_cost))/ (L_order_total_qty * L_unit_cost);
         end if;
         ---
         L_insert_carton_qty  := L_carton_qty;
         L_insert_gross_wt    := L_gross_wt;
         L_insert_net_wt      := L_net_wt;
         L_insert_cubic       := L_cubic;
      end if;
      ---
      --- calculate amount for insert into ce_ord_item;
      ---
      --- reset quantity back to NULL
      if L_insert_carton_qty = 0 then
         L_insert_carton_qty := NULL;
         L_carton_uom := NULL;
      end if;
      ---
      if L_insert_gross_wt = 0 then
         L_insert_gross_wt     := NULL;
         L_gross_wt_uom := NULL;
      end if;
      ---
      if L_insert_net_wt = 0 then
         L_insert_net_wt     := NULL;
         L_net_wt_uom := NULL;
      end if;
      ---
      if L_insert_cubic = 0 then
         L_insert_cubic     := NULL;
         L_cubic_uom := NULL;
      end if;
      ---
      --- check ce_ord_item for a duplicate record
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_CE_ORD_ITEM',
                       'CE_ORD_ITEM',
                       NULL);
      open C_CHECK_CE_ORD_ITEM(L_item);
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_CE_ORD_ITEM',
                       'CE_ORD_ITEM',
                       NULL);
      fetch C_CHECK_CE_ORD_ITEM into L_exists;
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_CE_ORD_ITEM',
                       'CE_ORD_ITEM',
                       NULL);
      close C_CHECK_CE_ORD_ITEM;
      ---
      if (L_exists         = 'N')        or
         (L_new_vve        = 'Y')        then
         SQL_LIB.SET_MARK('INSERT',NULL,'CE_ORD_ITEM',NULL);
         insert into ce_ord_item(ce_id,
                                 vessel_id,
                                 voyage_flt_id,
                                 estimated_depart_date,
                                 order_no,
                                 item,
                                 bl_awb_id,
                                 invoice_id,
                                 invoice_date,
                                 invoice_amt,
                                 currency_code,
                                 exchange_rate,
                                 manifest_item_qty,
                                 manifest_item_qty_uom,
                                 carton_qty,
                                 carton_qty_uom,
                                 gross_wt,
                                 gross_wt_uom,
                                 net_wt,
                                 net_wt_uom,
                                 cubic,
                                 cubic_uom,
                                 in_transit_no,
                                 in_transit_date,
                                 rush_ind,
                                 tariff_treatment,
                                 alc_status)
                          values(L_next_ce_id,
                                 L_vessel_id,
                                 L_voyage_flt_id,
                                 L_est_depart_date,
                                 L_order_no,
                                 L_item,
                                 L_bl_awb_id,
                                 L_invoice_id,
                                 L_invoice_date,
                                 L_insert_invoice_amt,
                                 L_currency_code,
                                 L_exchange_rate,
                                 L_insert_quantity,
                                 L_quantity_uom,
                                 L_insert_carton_qty,
                                 L_carton_uom,
                                 L_insert_gross_wt,
                                 L_gross_wt_uom,
                                 L_insert_net_wt,
                                 L_net_wt_uom,
                                 L_insert_cubic,
                                 L_cubic_uom,
                                 L_in_transit_no,
                                 L_in_transit_date,
                                 L_rush_ind,
                                 L_tariff_treatment,
                                 L_alc_status);
         ---
         --- build ce_charges
         if CE_CHARGES_SQL.DEFAULT_CHARGES(O_error_message,
                                           L_next_ce_id,
                                           L_vessel_id,
                                           L_voyage_flt_id,
                                           L_est_depart_date,
                                           L_order_no,
                                           L_item,
                                           L_import_country_id) = FALSE then
            return FALSE;
         end if;
      else
         --- update ce_ord_item to reflect multiple transportation
         --- records with same V/V/E and order/item combination
         ---
         --- if one value is NULL, then populate with the other value
         --- if values are different then update to NULL or
         if (L_prev_bl_awb_id != L_bl_awb_id) then
            --- if multiple bl_awb_id then set to 'Multi'ple
            L_bl_awb_id := 'MULTI';
         elsif (L_bl_awb_id is NULL) then
            L_bl_awb_id := L_prev_bl_awb_id;
         end if;
         ---
         if (L_prev_in_transit_no != L_in_transit_no) then
            L_in_transit_no := NULL;
         elsif (L_in_transit_no is NULL) then
            L_in_transit_no := L_prev_in_transit_no;
         end if;
         ---
         if (L_prev_in_transit_date != L_in_transit_date) then
            L_in_transit_date := NULL;
         elsif (L_in_transit_date is NULL) then
            L_in_transit_date := L_prev_in_transit_date;
         end if;
         ---
         if (L_prev_rush_ind = 'Y') or
            (L_rush_ind      = 'Y') then
            L_rush_ind     := 'Y';
         end if;
         ---
         --- if the uom is already populated, don't want to overwrite
         if L_prev_carton_uom is not NULL then
            L_carton_uom := L_prev_carton_uom;
         end if;
         ---
         if L_prev_gross_wt_uom is not NULL then
            L_gross_wt_uom := L_prev_gross_wt_uom;
         end if;
         ---
         if L_prev_net_wt_uom is not NULL then
            L_net_wt_uom := L_prev_net_wt_uom;
         end if;
         ---
         if L_prev_cubic_uom is not NULL then
            L_cubic_uom := L_prev_cubic_uom;
         end if;
         ---
         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_CE_ORD_ITEM',
                          'CE_ORD_ITEM',
                          NULL);
         open C_LOCK_CE_ORD_ITEM(L_item);
         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_CE_ORD_ITEM',
                          'CE_ORD_ITEM',
                          NULL);
         close C_LOCK_CE_ORD_ITEM;
         ---
         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'CE_ORD_ITEM',
                          NULL);
         update ce_ord_item
            set bl_awb_id             = L_bl_awb_id,
                invoice_amt           = invoice_amt + L_insert_invoice_amt,
                manifest_item_qty     = manifest_item_qty + L_insert_quantity,
                manifest_item_qty_uom = L_quantity_uom,
                carton_qty            = carton_qty + L_insert_carton_qty,
                carton_qty_uom        = L_carton_uom,
                gross_wt              = gross_wt + L_insert_gross_wt,
                gross_wt_uom          = L_gross_wt_uom,
                net_wt                = net_wt + L_insert_net_wt,
                net_wt_uom            = L_net_wt_uom,
                cubic                 = cubic + L_insert_cubic,
                cubic_uom             = L_cubic_uom,
                in_transit_no         = L_in_transit_no,
                in_transit_date       = L_in_transit_date,
                rush_ind              = L_rush_ind
          where ce_id                 = L_next_ce_id
            and vessel_id             = L_vessel_id
            and voyage_flt_id         = L_voyage_flt_id
            and estimated_depart_date = L_est_depart_date
            and order_no              = L_order_no
            and item                  = L_item;
         end if;  --- order/item exists
         ---
         --- populate previous variables
         L_exists               := 'N';
         L_new_vve              := 'N';
         L_prev_order_no        := L_order_no;
         L_prev_item            := L_item;
         L_prev_bl_awb_id       := L_bl_awb_id;
         L_prev_invoice_id      := L_invoice_id;
         L_prev_invoice_date    := L_invoice_date;
         L_prev_in_transit_no   := L_in_transit_no;
         L_prev_in_transit_date := L_in_transit_date;
         L_prev_rush_ind        := L_rush_ind;
         L_prev_import_country_id := L_import_country_id;
         ---
         --- build ce_lic_visa
         SQL_LIB.SET_MARK('OPEN',
                          'C_CE_LIC_VISA_EXIST',
                          'CE_LIC_VISA',
                          NULL);
         open C_CE_LIC_VISA_EXIST(L_item);
         SQL_LIB.SET_MARK('FETCH',
                          'C_CE_LIC_VISA_EXIST',
                          'CE_LIC_VISA',
                          NULL);
         fetch C_CE_LIC_VISA_EXIST into L_ce_lic_visa_exists;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CE_LIC_VISA_EXIST',
                          'CE_LIC_VISA',
                          NULL);
         close C_CE_LIC_VISA_EXIST;
         ---
         if L_ce_lic_visa_exists = 'N' then
            SQL_LIB.SET_MARK('INSERT',NULL,'CE_LIC_VISA',NULL);
            insert into ce_lic_visa(ce_id,
                                    vessel_id,
                                    voyage_flt_id,
                                    estimated_depart_date,
                                    order_no,
                                    item,
                                    license_visa_type,
                                    license_visa_id,
                                    license_visa_qty,
                                    license_visa_qty_uom,
                                    quota_cat,
                                    net_weight,
                                    net_weight_uom,
                                    holder_id)
                             select L_next_ce_id,
                                    L_vessel_id,
                                    L_voyage_flt_id,
                                    L_est_depart_date,
                                    L_order_no,
                                    L_item,
                                    license_visa_type,
                                    license_visa_id,
                                    license_visa_qty,
                                    license_visa_qty_uom,
                                    quota_cat,
                                    net_weight,
                                    net_weight_uom,
                                    holder_id
                               from trans_lic_visa
                              where transportation_id = L_transportation_id;
         end if;  --- ce_lic_visa record exists
      ---
      --- reset previous uom variables
      L_prev_carton_uom   := NULL;
      L_prev_gross_wt_uom := NULL;
      L_prev_net_wt_uom   := NULL;
      L_prev_cubic_uom    := NULL;
      ---
      ---
      -- Calculate CE_COMP for all VVE/Order/Item/HTS combinations
      -- on the Entry.
      ---
      if CE_CHARGES_SQL.GET_CALC_CE_COMP(O_error_message,
                                         L_total_ce_comp,
                                         L_next_ce_id) = FALSE then
        return FALSE;
      end if;
      ---
      --- reset variables
      L_transportation_id  := NULL;
      L_vessel_id          := NULL;
      L_voyage_flt_id      := NULL;
      L_est_depart_date    := NULL;
      L_vessel_scac_code   := NULL;
      L_lading_port        := NULL;
      L_discharge_port     := NULL;
      L_tran_mode_id       := NULL;
      L_arrival_date       := NULL;
      L_export_country_id  := NULL;
      L_shipment_no        := NULL;
      L_ce_lic_visa_exists := 'N';
      ---
      L_prev_order_no      := L_order_no;
      L_prev_item          := L_item;
      L_prev_invoice_id    := L_invoice_id;
      L_next_ce_id         := NULL;
      ---
   end loop;    --- selecting V/V/E records

   if L_zero_qty = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('FINALIZE_QTY_ZERO',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   --- once a record has been finalized, update the transportation record status
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_TRANSPORTATION',
                    'TRANSPORTATION',
                    'Transportation ID: ' || to_char(L_transportation_id));
   open C_LOCK_TRANSPORTATION;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_TRANSPORTATION',
                    'TRANSPORTATION',
                    'Transportation ID: ' || to_char(L_transportation_id));
   close C_LOCK_TRANSPORTATION;
   ---


   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'TRANSPORTATION',
                    NULL);
   update transportation
      set status = 'F'
    where transportation_id in (select distinct(transportation_id)
                                 from ce_temp);

   ---
   --Delete records from ce_temp after locking it
   ---
   L_table := 'CE_TEMP';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_CE_TEMP',
                    'CE_TEMP',
                    NULL);
   open C_LOCK_CE_TEMP;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_CE_TEMP',
                    'CE_TEMP',
                    NULL);
   close C_LOCK_CE_TEMP;
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'CE_TEMP',
                    NULL);
   Delete from ce_temp;
   ---
   return TRUE;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END FINALIZE;
--------------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_vessel_id         IN       TRANSPORTATION.VESSEL_ID%TYPE,
                    I_voyage_flt_id     IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                    I_est_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                    I_function_key      IN       VARCHAR2,
                    I_seq_no            IN       NUMBER)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(75)   := 'TRANS_FINALIZE_SQL.CUSTOM_VAL';
   L_custom_obj_rec      CUSTOM_OBJ_REC :=  CUSTOM_OBJ_REC();

BEGIN

   L_custom_obj_rec.function_key:= I_function_key;
   L_custom_obj_rec.call_seq_no:= I_seq_no;
   L_custom_obj_rec.vessel_id:= I_vessel_id;
   L_custom_obj_rec.voyage_flt_id:= I_voyage_flt_id;
   L_custom_obj_rec.estimated_depart_date:= I_est_depart_date;
  

  
   if CALL_CUSTOM_SQL.EXEC_FUNCTION(O_error_message,
                                    L_custom_obj_rec) = FALSE then
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END CUSTOM_VAL;
--------------------------------------------------------------------------------------
END TRANS_FINALIZE_SQL;
/
