CREATE OR REPLACE PACKAGE BODY ITEM_COST_SQL AS

---------------------------------------------------------------------------------------------
-- Function Name : CHECK_DUP_ITEM_COST
-- Purpose       : This function checks if any record exists in ITEM_COST_HEAD for the input item,
--                 supplier,country and delivery country.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DUP_ITEM_COST(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists                IN OUT BOOLEAN,
                             I_item                  IN     ITEM_COST_HEAD.ITEM%TYPE,
                             I_supplier              IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                             I_origin_country_id     IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                             I_delivery_country_id   IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_COST_SQL.CHECK_DUP_ITEM_COST';
   L_dummy     VARCHAR2(1)  := NULL;

   cursor C_CHECK_DUP_ITEM_COST is
      select 'x'
       from item_cost_head
      where item = I_item
        and supplier = I_supplier
        and origin_country_id = I_origin_country_id
        and delivery_country_id = I_delivery_country_id
        and rownum = 1;

BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                            L_program,
                                            NULL);
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_supplier',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_origin_country_id',
                                            L_program,
                                            NULL);
     return FALSE;
   end if;
   ---
   if I_delivery_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_delivery_country_id',
                                            L_program,
                                            NULL);
     return FALSE;
   end if;

   O_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_DUP_ITEM_COST',
                    'ITEM_COST_HEAD',
                    NULL);

   open C_CHECK_DUP_ITEM_COST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_DUP_ITEM_COST',
                    'ITEM_COST_HEAD',
                    NULL);

   fetch C_CHECK_DUP_ITEM_COST into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_DUP_ITEM_COST',
                    'ITEM_COST_HEAD',
                    NULL);

   close C_CHECK_DUP_ITEM_COST;

   if L_dummy is NOT NULL then
     O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
END CHECK_DUP_ITEM_COST;
---------------------------------------------------------------------------------------------
-- Function Name : PRIMARY_DLVY_CTRY_EXISTS
-- Purpose       : This function will return the primary delivery country for  the input item,
--                 supplier and origin country.If primary delivery country does not exist, it will return
--                 NULL.
---------------------------------------------------------------------------------------------
FUNCTION PRIMARY_DLVY_CTRY_EXISTS(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_prim_delivery_country_id   IN OUT ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE,
                                  I_item                       IN     ITEM_COST_HEAD.ITEM%TYPE,
                                  I_supplier                   IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                                  I_origin_country_id          IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)  := 'ITEM_COST_SQL.PRIMARY_DLVY_CTRY_EXISTS';

   cursor C_GET_PRIMARY_DLVY_CTRY is
     select delivery_country_id
      from item_cost_head
      where item = I_item
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and prim_dlvy_ctry_ind = 'Y';
BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_supplier',
                                            L_program,
                                            NULL);
     return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_origin_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;

   O_prim_delivery_country_id := NULL;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_PRIMARY_DLVY_CTRY',
                    'ITEM_COST_HEAD',
                    NULL);

   open C_GET_PRIMARY_DLVY_CTRY;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_PRIMARY_DLVY_CTRY',
                    'ITEM_COST_HEAD',
                    NULL);

   fetch C_GET_PRIMARY_DLVY_CTRY into O_prim_delivery_country_id;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_PRIMARY_DLVY_CTRY',
                    'ITEM_COST_HEAD',
                    NULL);

   close C_GET_PRIMARY_DLVY_CTRY;

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
END PRIMARY_DLVY_CTRY_EXISTS;
---------------------------------------------------------------------------------------------
-- Function Name : CHECK_DLVY_CTRY_EXISTS
-- Purpose       : This function checks if any record exists in ITEM_COST_HEAD for the input
--                 delivery country
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DLVY_CTRY_EXISTS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists                IN OUT BOOLEAN,
                                I_delivery_country_id   IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_COST_SQL.CHECK_DLVY_CTRY_EXISTS';
   L_dummy     VARCHAR2(1)  := NULL;

   cursor C_CHECK_DLVY_CTRY_EXISTS is
     select 'x'
      from item_cost_head
      where delivery_country_id = I_delivery_country_id
      and rownum = 1;

BEGIN

   if I_delivery_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_delivery_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;

   O_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_DLVY_CTRY_EXISTS',
                    'ITEM_COST_HEAD',
                    NULL);

   open C_CHECK_DLVY_CTRY_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_DLVY_CTRY_EXISTS',
                    'ITEM_COST_HEAD',
                    NULL);

   fetch C_CHECK_DLVY_CTRY_EXISTS into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_DLVY_CTRY_EXISTS',
                    'ITEM_COST_HEAD',
                    NULL);

   close C_CHECK_DLVY_CTRY_EXISTS;

   if L_dummy is NOT NULL then
     O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
END CHECK_DLVY_CTRY_EXISTS;
---------------------------------------------------------------------------------------------
-- Function Name : CHANGE_PRIM_DLVY_CTRY
-- Purpose       : This function changes the primary delivery country indicator in ITEM_COST_HEAD
--                 if primary delivery country exists for input item,supplier and origin country. If primary
--                 delivery country does not exist, function does nothing.
---------------------------------------------------------------------------------------------
FUNCTION CHANGE_PRIM_DLVY_CTRY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item                IN     ITEM_COST_HEAD.ITEM%TYPE,
                               I_supplier            IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                               I_origin_country_id   IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'ITEM_COST_SQL.CHANGE_PRIM_DLVY_CTRY';
   L_table            VARCHAR2(20) := 'ITEM_COST_HEAD';
   L_prim_dlvy_ctry   ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE;
   L_dummy            VARCHAR2(1) := NULL;
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ITEM_COST_HEAD is
     select 'x'
      from item_cost_head
      where item = I_item
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and delivery_country_id = L_prim_dlvy_ctry
       for update nowait;

BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_item',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_supplier',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_origin_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if ITEM_COST_SQL.PRIMARY_DLVY_CTRY_EXISTS( O_error_message,
                                              L_prim_dlvy_ctry,
                                              I_item,
                                              I_supplier,
                                              I_origin_country_id) = FALSE then
     return FALSE;
   end if;

   if L_prim_dlvy_ctry is NOT NULL then
     SQL_LIB.SET_MARK('OPEN',
                      'C_LOCK_ITEM_COST_HEAD',
                      'ITEM_COST_HEAD',
                      NULL);

     open C_LOCK_ITEM_COST_HEAD;

     SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ITEM_COST_HEAD',
                       'ITEM_COST_HEAD',
                       NULL);

     close C_LOCK_ITEM_COST_HEAD;

     update item_cost_head
       set prim_dlvy_ctry_ind = 'N'
      where item = I_item
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and delivery_country_id = L_prim_dlvy_ctry;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
     O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                           L_table,
                                           NULL,
                                           TO_CHAR(SQLCODE));
     return FALSE;
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
END CHANGE_PRIM_DLVY_CTRY;
---------------------------------------------------------------------------------------------
-- Function Name : DEL_ITEM_COST
-- Purpose       : This function deletes from ITEM_COST_HEAD for input item,supplier,origin country and delivery country
---------------------------------------------------------------------------------------------
FUNCTION DEL_ITEM_COST( O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item                  IN     ITEM_COST_HEAD.ITEM%TYPE,
                        I_supplier              IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                        I_origin_country_id     IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                        I_delivery_country_id   IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'ITEM_COST_SQL.DEL_ITEM_COST';
   L_prim_dlvy_ctry   ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE;
   L_item_record      ITEM_MASTER%ROWTYPE;
   L_dummy            VARCHAR2(1) := NULL;
   L_table            VARCHAR2(25) := NULL;
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ITEM_COST_HEAD is
     select 'x'
      from item_cost_head
      where item = I_item
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and delivery_country_id = I_delivery_country_id
       for update nowait;

   cursor C_LOCK_ITEM_COST_DETAIL is
     select 'x'
      from item_cost_detail
      where item = I_item
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and delivery_country_id = I_delivery_country_id
       for update nowait;

   cursor C_LOCK_ITEM_SUPP_COUNTRY_LOC is
     select 'x'
      from item_supp_country_loc iscl
      where iscl.item = I_item
       and iscl.supplier = I_supplier
       and iscl.origin_country_id = I_origin_country_id
       and exists (select 'x'
                     from addr
                    where module in (LP_TAX_STORE_TYPE, LP_TAX_WH_TYPE)
                     and primary_addr_ind = 'Y'
                     and country_id = I_delivery_country_id
                     and iscl.loc = key_value_1)
       for update nowait;

BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                            L_program,
                                            NULL);
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_supplier',
                                            L_program,
                                            NULL);
     return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_origin_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_delivery_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_delivery_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_record,
                                      I_item) = FALSE then
     return FALSE;
   end if;

   if ITEM_COST_SQL.PRIMARY_DLVY_CTRY_EXISTS( O_error_message,
                                              L_prim_dlvy_ctry,
                                              I_item,
                                              I_supplier,
                                              I_origin_country_id) = FALSE then
     return FALSE;
   end if;
   ---
   if L_item_record.status in ('W','S') and
    (L_prim_dlvy_ctry <> I_delivery_country_id or
     L_prim_dlvy_ctry is NULL) then

     L_table := 'ITEM_COST_DETAIL';

     SQL_LIB.SET_MARK('OPEN',
                      'C_LOCK_ITEM_COST_DETAIL',
                      'ITEM_COST_DETAIL',
                      NULL);

     open C_LOCK_ITEM_COST_DETAIL;

     SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ITEM_COST_DETAIL',
                     'ITEM_COST_DETAIL',
                      NULL);

     close C_LOCK_ITEM_COST_DETAIL;

     delete item_cost_detail
      where item = I_item
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and delivery_country_id = I_delivery_country_id;

     L_table := 'ITEM_COST_HEAD';

     SQL_LIB.SET_MARK('OPEN',
                      'C_LOCK_ITEM_COST_HEAD',
                      'ITEM_COST_HEAD',
                      NULL);

     open C_LOCK_ITEM_COST_HEAD;

     SQL_LIB.SET_MARK('CLOSE',
                      'C_LOCK_ITEM_COST_HEAD',
                      'ITEM_COST_HEAD',
                       NULL);

     close C_LOCK_ITEM_COST_HEAD;

     delete item_cost_head
      where item = I_item
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and delivery_country_id = I_delivery_country_id;

     L_table := 'ITEM_SUPP_COUNTRY_LOC';

     SQL_LIB.SET_MARK('OPEN',
                      'C_LOCK_ITEM_SUPP_COUNTRY_LOC',
                      L_table,
                      NULL);

     open C_LOCK_ITEM_SUPP_COUNTRY_LOC;

     SQL_LIB.SET_MARK('CLOSE',
                      'C_LOCK_ITEM_SUPP_COUNTRY_LOC',
                      L_table,
                      NULL);

     close C_LOCK_ITEM_SUPP_COUNTRY_LOC;

     delete item_supp_country_loc iscl
      where iscl.item = I_item
       and iscl.supplier = I_supplier
       and iscl.origin_country_id = I_origin_country_id
       and exists (select 'x'
                     from addr
                    where module in (LP_TAX_STORE_TYPE, LP_TAX_WH_TYPE)
                     and primary_addr_ind = 'Y'
                     and country_id = I_delivery_country_id
                     and iscl.loc = key_value_1);
   else
     if L_prim_dlvy_ctry = I_delivery_country_id then
       O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DELETE_PRIM_CTRY',
                                             NULL,
                                             NULL,
                                             NULL);
       return FALSE;
     end if;
     ---
     if L_item_record.status NOT IN ('W','S') then
       O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DELETE_ITEM_COST',
                                             NULL,
                                             NULL,
                                             NULL);
       return FALSE;
     end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
     O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             NULL,
                                             TO_CHAR(SQLCODE));
       return FALSE;
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
     return FALSE;
END DEL_ITEM_COST;
---------------------------------------------------------------------------------------------
-- Function Name : GET_INCL_NIC_IND
-- Purpose       : This function retrieves the INCL_NIC_IND from the VAT_CODES table for a given VAT_CODE.
---------------------------------------------------------------------------------------------
FUNCTION GET_INCL_NIC_IND(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_incl_nic_ind    IN OUT VAT_CODES.INCL_NIC_IND%TYPE,
                          I_vat_code        IN     VAT_CODES.VAT_CODE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_COST_SQL.GET_INCL_NIC_IND';

   cursor C_GET_INCL_NIC_IND is
     select incl_nic_ind
      from vat_codes
      where vat_code = I_vat_code;

BEGIN

   if I_vat_code is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_vat_code',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                     'C_GET_INCL_NIC_IND',
                     'VAT_CODES',
                     NULL);

   open C_GET_INCL_NIC_IND;

   SQL_LIB.SET_MARK('FETCH',
                     'C_GET_INCL_NIC_IND',
                     'VAT_CODES',
                     NULL);

   fetch C_GET_INCL_NIC_IND into O_incl_nic_ind;

   SQL_LIB.SET_MARK('CLOSE',
                     'C_GET_INCL_NIC_IND',
                     'VAT_CODES',
                     NULL);

   close C_GET_INCL_NIC_IND;

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
END GET_INCL_NIC_IND;
---------------------------------------------------------------------------------------------
-- Function Name : GET_DLVY_CTRY_INFO
-- Purpose       : This function fetches default location,location type  and item cost indicator
--                 values(inclusive/exclusive tax) of the entered delivery country from the country_attrib table.
---------------------------------------------------------------------------------------------
FUNCTION GET_DLVY_CTRY_INFO (O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_default_location         IN OUT COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                             O_loc_type                 IN OUT COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE,
                             O_item_cost_tax_incl_ind   IN OUT COUNTRY_ATTRIB.ITEM_COST_TAX_INCL_IND%TYPE,
                             I_delivery_country_id      IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_COST_SQL.GET_DLVY_CTRY_INFO';

   cursor C_GET_DLVY_CTRY_INFO is
     select default_loc,
            default_loc_type,
            item_cost_tax_incl_ind
      from country_attrib
      where country_id = I_delivery_country_id;

BEGIN

   if I_delivery_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_delivery_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                     'C_GET_DLVY_CTRY_INFO',
                     'COUNTRY_ATRIBUTES',
                     NULL);

   open C_GET_DLVY_CTRY_INFO;

   SQL_LIB.SET_MARK('FETCH',
                     'C_GET_DLVY_CTRY_INFO',
                     'COUNTRY_ATRIBUTES',
                     NULL);

   fetch C_GET_DLVY_CTRY_INFO into  O_default_location,
                                    O_loc_type,
                                    O_item_cost_tax_incl_ind;

   SQL_LIB.SET_MARK('CLOSE',
                     'C_GET_DLVY_CTRY_INFO',
                     'COUNTRY_ATRIBUTES',
                     NULL);

   close C_GET_DLVY_CTRY_INFO;

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
     return FALSE;
END GET_DLVY_CTRY_INFO;
---------------------------------------------------------------------------------------------
-- Function Name : GET_NIC_STATIC_IND
-- Purpose       : This function fetches nic_static_ind from item_cost_head for input item,supplier,
--                 origin country and delivery countrye.
---------------------------------------------------------------------------------------------
FUNCTION GET_NIC_STATIC_IND (  O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_nic_static_ind        IN OUT ITEM_COST_HEAD.NIC_STATIC_IND%TYPE,
                               I_item                  IN     ITEM_COST_HEAD.ITEM%TYPE,
                               I_supplier              IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                               I_origin_country_id     IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                               I_delivery_country_id   IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_COST_SQL.GET_NIC_STATIC_IND';

   cursor C_GET_NIC_STATIC_IND is
     select nvl(nic_static_ind,'N')
      from item_cost_head
      where item = I_item
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and delivery_country_id = I_delivery_country_id;
BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_item',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_supplier',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_origin_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_delivery_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_delivery_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                     'C_GET_NIC_STATIC_IND',
                     'VAT_CODES',
                     NULL);

   open C_GET_NIC_STATIC_IND;

   SQL_LIB.SET_MARK('FETCH',
                     'C_GET_NIC_STATIC_IND',
                     'ITEM_COST_HEAD',
                     NULL);

   fetch C_GET_NIC_STATIC_IND into O_nic_static_ind;

   SQL_LIB.SET_MARK('CLOSE',
                     'C_GET_NIC_STATIC_IND',
                     'ITEM_COST_HEAD',
                     NULL);

   close C_GET_NIC_STATIC_IND;

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
     return FALSE;
END GET_NIC_STATIC_IND;
---------------------------------------------------------------------------------------------
-- Function Name : COMPUTE_ITEM_COST
-- Purpose       : This function will calculate item cost based on tax details
---------------------------------------------------------------------------------------------
FUNCTION COMPUTE_ITEM_COST (  O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_base_cost                   IN OUT ITEM_COST_HEAD.BASE_COST%TYPE,
                              O_extended_base_cost          IN OUT ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE,
                              O_inclusive_cost              IN OUT ITEM_COST_HEAD.INCLUSIVE_COST%TYPE,
                              O_negotiated_item_cost        IN OUT ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE,
                              I_item                        IN     ITEM_COST_HEAD.ITEM%TYPE,
                              I_nic_static_ind              IN     ITEM_COST_HEAD.NIC_STATIC_IND%TYPE,
                              I_supplier                    IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                              I_location                    IN     COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                              I_loc_type                    IN     COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE,
                              I_calling_form                IN     VARCHAR2 DEFAULT LP_ISC,
                              I_origin_country_id           IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE DEFAULT NULL,
                              I_delivery_country_id         IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE DEFAULT NULL,
                              I_prim_dlvy_ctry_ind          IN     ITEM_COST_HEAD.PRIM_DLVY_CTRY_IND%TYPE DEFAULT NULL,
                              I_update_itemcost_ind         IN     VARCHAR2 DEFAULT 'N',
                              I_update_itemcost_child_ind   IN     VARCHAR2 DEFAULT 'N',
                              I_insert_item_supplier_ind    IN     VARCHAR2 DEFAULT 'N',
                              I_item_add_rec                IN     ITEM_ADD_REC DEFAULT NULL,
                              I_item_cost_tax_incl_ind      IN     COUNTRY_ATTRIB.ITEM_COST_TAX_INCL_IND%TYPE DEFAULT 'Y')
RETURN BOOLEAN IS

   L_program                      VARCHAR2(64)     := 'ITEM_COST_SQL.COMPUTE_ITEM_COST';

   L_tax_calc_rec                 OBJ_TAX_CALC_REC := OBJ_TAX_CALC_REC();
   L_tax_calc_tbl                 OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();

   L_loc_type                     VARCHAR2(2) := NULL;
   L_system_options_rec           SYSTEM_OPTIONS%ROWTYPE := NULL;
   L_country_attrib_row           COUNTRY_ATTRIB%ROWTYPE := NULL;
   L_item_record                  ITEM_MASTER%ROWTYPE := NULL;
   L_country_id                   COUNTRY_ATTRIB.COUNTRY_ID%TYPE := NULL;
   L_vdate                        PERIOD.VDATE%TYPE := GET_VDATE;
   L_total_tax_amt_nic_incl       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := 0;
   L_total_tax_amount             ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := 0;
   L_total_recover_amount         ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := 0;
   L_primary_supp_currency_code   SUPS.CURRENCY_CODE%TYPE := NULL;
   L_tax_rate                     ITEM_COST_DETAIL.COMP_RATE%TYPE := 0;
   L_item_cost_tax_incl_ind       COUNTRY_ATTRIB.ITEM_COST_TAX_INCL_IND%TYPE := I_item_cost_tax_incl_ind;
   L_table                        VARCHAR2(20);
   L_exists                       VARCHAR2(1) := NULL;
   RECORD_LOCKED                  EXCEPTION;
   PRAGMA                         EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_GET_TOTAL_TAX is
     select sum(td.estimated_tax_value)
      from TABLE(CAST(L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td;

   cursor C_GET_TOTAL_TAX_NIC_INCL is
     select sum(td.estimated_tax_value)
      from TABLE(CAST(L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td,
          vat_codes vc
      where td.tax_code = vc.vat_code
       and vc.incl_nic_ind = 'Y';

   cursor C_LOCK_ITEM_COST_HEAD is
     select 'x'
      from item_cost_head
      where item = I_item
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and delivery_country_id = I_delivery_country_id
       for update nowait;

   cursor C_LOCK_ITEM_COST_DETAIL is
     select 'x'
      from item_cost_detail
      where item = I_item
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and delivery_country_id = I_delivery_country_id
       for update nowait;

   cursor C_LOCK_ITEM_COST_HEAD_CHILD is
     select 'x'
      from item_cost_head ich,
          item_master im
     where (im.item_parent = I_item or
          im.item_grandparent = I_item)
      and im.item_level <= im.tran_level
      and ich.item = im.item
      and ich.supplier = I_supplier
      and ich.origin_country_id = I_origin_country_id
      and ich.delivery_country_id = I_delivery_country_id
      for update of ich.negotiated_item_cost nowait;

   cursor C_LOCK_ITEM_COST_DETAIL_CHILD is
     select 'x'
      from item_cost_detail icd,
          item_master im
     where (im.item_parent = I_item or
          im.item_grandparent = I_item)
      and im.item_level <= im.tran_level
      and icd.item = im.item
      and icd.supplier = I_supplier
      and icd.origin_country_id = I_origin_country_id
      and icd.delivery_country_id = I_delivery_country_id
      for update of icd.comp_rate nowait;

   cursor C_CHECK_ISC_EXIST is
      select 'x'
        from item_supp_country
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id
         and rownum = 1;

   cursor C_GET_PRIMARY_CTRY is
      select country_id
        from mv_loc_prim_addr mva
       where mva.loc = I_location;

BEGIN

   -- Check for required paramaters
   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_item',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if UPPER(I_calling_form) = LP_ISC and
     I_nic_static_ind is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_nic_static_ind',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_supplier',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
       return FALSE;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_record,
                                      I_item) = FALSE then
     return FALSE;
   end if;
   ---
   if L_system_options_rec.default_tax_type = LP_GTAX then
     ---
     if I_location is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_location',
                                             L_program,
                                             NULL);
       return FALSE;
     end if;
     ---
     if I_loc_type is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_loc_type',
                                             L_program,
                                             NULL);
       return FALSE;
     end if;
   end if;
   --
   if I_loc_type = 'S' then
     L_loc_type := LP_TAX_STORE_TYPE;
   elsif I_loc_type = 'W' then
     L_loc_type := LP_TAX_WH_TYPE;
   elsif I_loc_type = 'E' then
     L_loc_type := LP_TAX_EXTFIN_TYPE;
   end if;
   ---

   -- Retrieve primary address details for locations
   if I_location is NOT NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_PRIMARY_CTRY',
                       'MV_LOC_PRIM_CTRY',
                       'Loc: '||TO_CHAR(I_location));
      open C_GET_PRIMARY_CTRY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_PRIMARY_CTRY',
                       'MV_LOC_PRIM_CTRY',
                       'Loc: '||TO_CHAR(I_location));
      fetch C_GET_PRIMARY_CTRY into L_country_id;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PRIMARY_CTRY',
                       'MV_LOC_PRIM_CTRY',
                       'Loc: '||TO_CHAR(I_location));
      close C_GET_PRIMARY_CTRY;
      ---
      -- Retrieve country information
      if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                 L_country_attrib_row,
                                                 NULL,
                                                 L_country_id) = FALSE then
         return FALSE;
      end if;

      if UPPER(I_calling_form) = LP_ISC then
         L_item_cost_tax_incl_ind := L_country_attrib_row.item_cost_tax_incl_ind;
      end if;
   end if;
   --

   -- Retrieve supplier currency
   if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE( O_error_message,
                                          L_primary_supp_currency_code,
                                          I_supplier) = FALSE then
     return FALSE;
   end if;
   --




   If L_system_options_rec.default_tax_type = LP_GTAX then
     if L_item_record.pack_type is NULL or
       L_item_record.pack_type = 'V' or L_item_record.pack_type = 'B' then
       --- Load L_tax_calc_tbl for tax engine input
       L_tax_calc_tbl.EXTEND;
       L_tax_calc_rec.I_item                := I_item;
       L_tax_calc_rec.I_pack_ind            := L_item_record.pack_ind;
       L_tax_calc_rec.I_from_entity         := I_supplier;
       L_tax_calc_rec.I_from_entity_type    := LP_TAX_SUPPLIER_TYPE;
       L_tax_calc_rec.I_to_entity           := I_location;
       L_tax_calc_rec.I_to_entity_type      := L_loc_type;
       L_tax_calc_rec.I_amount              := NVL(O_negotiated_item_cost,O_base_cost);
       L_tax_calc_rec.I_amount_curr         := L_primary_supp_currency_code;
       L_tax_calc_rec.I_amount_tax_incl_ind := L_item_cost_tax_incl_ind;
       L_tax_calc_rec.I_origin_country_id   := I_origin_country_id;
       L_tax_calc_rec.I_effective_from_date := L_vdate;
       L_tax_calc_tbl(L_tax_calc_tbl.COUNT) := L_tax_calc_rec;

       if TAX_SQL.CALC_COST_TAX(O_error_message,
                                L_tax_calc_tbl) = FALSE then
         return FALSE;
       end if;

       -- Retrieve total tax for NIC inclusive tax codes
       open C_GET_TOTAL_TAX_NIC_INCL;
       fetch C_GET_TOTAL_TAX_NIC_INCL into L_total_tax_amt_nic_incl;
       close C_GET_TOTAL_TAX_NIC_INCL;

       L_total_tax_amount := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_total_tax_amount;
       L_total_recover_amount   := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_total_recover_amount;

       if UPPER(I_calling_form) = LP_ISC then
         if L_country_attrib_row.item_cost_tax_incl_ind = 'N' then
            -- If the country has cost exclusive of tax then add total tax to base cost to determine negotiated item cost
            O_negotiated_item_cost := NVL(O_base_cost, 0) + NVL(L_total_tax_amt_nic_incl, 0);
         else
            -- If the country has cost inclusive of tax then strip-off total tax to determine base cost
            O_base_cost := NVL(O_negotiated_item_cost, 0) - NVL(L_total_tax_amt_nic_incl, 0);
         end if;
       elsif UPPER(I_calling_form) = LP_ISCL then
         if I_nic_static_ind is NULL then
            O_base_cost := NVL(O_negotiated_item_cost, 0) - NVL(L_total_tax_amt_nic_incl,0);
         else
            if I_nic_static_ind = 'N' then
              O_negotiated_item_cost := NVL(O_base_cost, 0) + NVL(L_total_tax_amt_nic_incl,0);
            else
              O_base_cost := NVL(O_negotiated_item_cost, 0) - NVL(L_total_tax_amt_nic_incl,0);
            end if;
         end if;
       end if;
       ---
       -- Determine extended base cost
       O_extended_base_cost := NVL(O_base_cost,0) + NVL(L_total_tax_amount, 0) - NVL(L_total_recover_amount, 0);
       -- Determine inclusive cost by adding total tax to base cost
       O_inclusive_cost     := NVL(O_base_cost,0) + NVL(L_total_tax_amount, 0);
     end if;
     ---

     -- Zero fill any NULL values
     O_base_cost            := NVL(O_base_cost, 0);
     O_extended_base_cost   := NVL(O_extended_base_cost, 0);
     O_inclusive_cost       := NVL(O_inclusive_cost, 0);
     O_negotiated_item_cost := NVL(O_negotiated_item_cost, 0);
   end if;

   --This currently only happens for a Quick Item Entry scenario
   --and when creating the pack in the Simple Pack Setup form
   if I_insert_item_supplier_ind = 'Y' then
      -- Check if an item_supp_country record exists
      L_exists := NULL;
      open C_CHECK_ISC_EXIST;
      fetch C_CHECK_ISC_EXIST into L_exists;
      close C_CHECK_ISC_EXIST;

      if L_exists is NOT NULL then
         -- Update the existing item_supp_country record
         update item_supp_country
            set base_cost = DECODE(L_system_options_rec.default_tax_type, 'GTAX', O_base_cost, NULL),
                extended_base_cost = DECODE(L_system_options_rec.default_tax_type, 'GTAX', O_extended_base_cost, NULL),
                inclusive_cost = DECODE(L_system_options_rec.default_tax_type, 'GTAX', O_inclusive_cost, NULL),
                negotiated_item_cost = DECODE(L_system_options_rec.default_tax_type, 'GTAX', O_negotiated_item_cost, NULL)
         where item = I_item
           and supplier = I_supplier
           and origin_country_id = I_origin_country_id;
      else
         -- Insert a new item_supp_country record
         if ITEM_ADD_SQL.INSERT_ITEM_SUPP_CTRY(O_error_message,
                                              I_item_add_rec.item,
                                              I_item_add_rec.supplier,
                                              I_item_add_rec.origin_country,
                                              I_item_add_rec.vpn,
                                              I_item_add_rec.consignment_rate,
                                              I_item_add_rec.pallet_name,
                                              I_item_add_rec.case_name,
                                              I_item_add_rec.inner_name,
                                              I_item_add_rec.unit_cost,
                                              I_item_add_rec.lead_time,
                                              I_item_add_rec.supp_pack_size,
                                              I_item_add_rec.inner_pack_size,
                                              I_item_add_rec.round_lvl,
                                              I_item_add_rec.round_to_inner_pct,
                                              I_item_add_rec.round_to_case_pct,
                                              I_item_add_rec.round_to_layer_pct,
                                              I_item_add_rec.round_to_pallet_pct,
                                              I_item_add_rec.packing_method,
                                              I_item_add_rec.default_uop,
                                              I_item_add_rec.ti,
                                              I_item_add_rec.hi,
                                              I_item_add_rec.length,
                                              I_item_add_rec.width,
                                              I_item_add_rec.height,
                                              I_item_add_rec.lwh_uom,
                                              I_item_add_rec.weight,
                                              I_item_add_rec.weight_uom,
                                              I_item_add_rec.primary_case_size,
                                              O_negotiated_item_cost,
                                              O_extended_base_cost,
                                              O_inclusive_cost,
                                              O_base_cost,
                                              I_item_add_rec.tolerance_type,
                                              I_item_add_rec.max_tolerance,
                                              I_item_add_rec.min_tolerance,
                                              I_item_add_rec.cost_uom) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   -- Update ITEM_COST_HEAD and ITEM_COST_DETAIL of the item if necessary
   if I_update_itemcost_ind = 'Y' and L_system_options_rec.default_tax_type = LP_GTAX then
   ---
     if I_origin_country_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_origin_country_id',
                                             L_program,
                                             NULL);
       return FALSE;
     end if;
     ---
     if I_delivery_country_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_delivery_country_id',
                                             L_program,
                                             NULL);
       return FALSE;
     end if;
     ---
     if I_prim_dlvy_ctry_ind is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_prim_dlvy_ctry_ind',
                                             L_program,
                                             NULL);
          return FALSE;
     end if;
     ---
     if I_nic_static_ind is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_nic_static_ind',
                                             L_program,
                                             NULL);
       return FALSE;
     end if;
     ---
     L_table := 'ITEM_COST_HEAD';
     SQL_LIB.SET_MARK('OPEN',
                     'C_LOCK_ITEM_COST_HEAD',
                     'ITEM_COST_HEAD',
                     NULL);

     open C_LOCK_ITEM_COST_HEAD;
     SQL_LIB.SET_MARK('CLOSE',
                     'C_LOCK_ITEM_COST_HEAD',
                     'ITEM_COST_HEAD',
                     NULL);

     close C_LOCK_ITEM_COST_HEAD;

     merge into item_cost_head ich
     using (select I_item item,
               I_supplier supplier,
               I_origin_country_id origin_country_id,
               I_delivery_country_id delivery_country_id,
               O_base_cost base_cost,
               O_extended_base_cost extended_base_cost,
               O_negotiated_item_cost negotiated_item_cost,
               O_inclusive_cost inclusive_cost
            from sups s
           where s.supplier = I_supplier) ich1
       on (ich.item = ich1.item
      and  ich.supplier = ich1.supplier
      and  ich.origin_country_id = ich1.origin_country_id
      and  ich.delivery_country_id = ich1.delivery_country_id)
      when matched then
        update set prim_dlvy_ctry_ind   = I_prim_dlvy_ctry_ind,
                nic_static_ind       = I_nic_static_ind,
                base_cost            = ich1.base_cost,
                extended_base_cost   = ich1.extended_base_cost,
                negotiated_item_cost = ich1.negotiated_item_cost,
                inclusive_cost       = ich1.inclusive_cost
      when NOT matched  then


        insert(item,
             supplier,
             origin_country_id,
             delivery_country_id,
             prim_dlvy_ctry_ind,
             nic_static_ind,
             base_cost,
             extended_base_cost,
             negotiated_item_cost,
             inclusive_cost)
        values(I_item,
             I_supplier,
             I_origin_country_id,
             I_delivery_country_id,
             I_prim_dlvy_ctry_ind,
             I_nic_static_ind,
             ich1.base_cost,
             ich1.extended_base_cost,
             ich1.negotiated_item_cost,
             ich1.inclusive_cost);
     ---
     L_table := 'ITEM_COST_DETAIL';
     SQL_LIB.SET_MARK('OPEN',
                     'C_LOCK_ITEM_COST_DETAIL',
                     'ITEM_COST_DETAIL',
                     NULL);

     open C_LOCK_ITEM_COST_DETAIL;
     SQL_LIB.SET_MARK('CLOSE',
                     'C_LOCK_ITEM_COST_DETAIL',
                     'ITEM_COST_DETAIL',
                     NULL);

     close C_LOCK_ITEM_COST_DETAIL;
     ---
  --   if L_system_options_rec.default_tax_type = LP_GTAX then
        -- Deleting existing records from ITEM_COST_DETAIL table before inserting the tax components.
         delete item_cost_detail
          where item = I_item
            and supplier = I_supplier
            and origin_country_id = I_origin_country_id
            and delivery_country_id = I_delivery_country_id;

         insert into item_cost_detail (item,
                                       supplier,
                                       origin_country_id,
                                       delivery_country_id,
                                       cond_type,
                                       cond_value,
                                       applied_on,
                                      modified_taxable_base,
                                       comp_rate,
                                       calculation_basis,
                                       recoverable_amount)
                                       (select I_item item,
                                               I_supplier supplier,
                                               I_origin_country_id origin_country_id,
                                               I_delivery_country_id delivery_country_id,
                                               td.tax_code cond_type,
                                               td.estimated_tax_value cond_value,
                                               (case when L_item_record.pack_ind = 'Y' then
                                                         (case when L_item_record.simple_pack_ind = 'Y' then
                                                                    td.taxable_base
                                                               when L_item_record.simple_pack_ind = 'N' then
                                                                    NULL
                                                          end)
                                                    when L_item_record.pack_ind = 'N' then
                                                             td.taxable_base
                                              end) applied_on,
                                              (case when L_item_record.pack_ind = 'Y' then
                                                         (case when L_item_record.simple_pack_ind = 'Y' then
                                                                    td.modified_taxable_base
                                                               when L_item_record.simple_pack_ind = 'N' then
                                                                    NULL
                                                          end)
                                                    when L_item_record.pack_ind = 'N' then
                                                             td.modified_taxable_base
                                              end) modified_taxable_base,
                                              (case when L_item_record.pack_ind = 'Y' then
                                                         (case when L_item_record.simple_pack_ind = 'Y' then
                                                                    td.tax_rate
                                                               when L_item_record.simple_pack_ind = 'N' then
                                                                    NULL
                                                         end)
                                                    when L_item_record.pack_ind = 'N' then
                                                             td.tax_rate
                                              end) comp_rate,
                                              (case when L_item_record.pack_ind = 'Y' then
                                                        (case when L_item_record.simple_pack_ind = 'Y' then
                                                                   td.calculation_basis
                                                              when L_item_record.simple_pack_ind = 'N' then
                                                                   NULL
                                                        end)
                                                    when L_item_record.pack_ind = 'N' then
                                                         td.calculation_basis
                                              end) calculation_basis,
                                               td.recoverable_amount recoverable_amount
                                          from TABLE(CAST(L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td
                                         where td.tax_type in ('C','B'));

    -- end if;
   end if;
   ---
   -- Update ITEM_COST_HEAD and ITEM_COST_DETAIL of the child items if necessary
   if I_update_itemcost_child_ind = 'Y' and L_system_options_rec.default_tax_type = LP_GTAX then
     ---
     L_table := 'ITEM_COST_HEAD';
     SQL_LIB.SET_MARK('OPEN',
                     'C_LOCK_ITEM_COST_HEAD_CHILD',
                     'ITEM_COST_HEAD',
                     NULL);

     open C_LOCK_ITEM_COST_HEAD_CHILD;
     SQL_LIB.SET_MARK('CLOSE',
                     'C_LOCK_ITEM_COST_HEAD_CHILD',
                     'ITEM_COST_HEAD',
                     NULL);

     close C_LOCK_ITEM_COST_HEAD_CHILD;

     merge into item_cost_head ich
     using (select im.item,
                   I_supplier supplier,
                   I_origin_country_id origin_country_id,
                   I_delivery_country_id delivery_country_id,
                   O_base_cost base_cost,
                   O_extended_base_cost extended_base_cost,
                   O_negotiated_item_cost negotiated_item_cost,
                   O_inclusive_cost inclusive_cost
              from item_master im,
                   sups s,
                   item_supp_country isc
             where (im.item_parent = I_item or im.item_grandparent = I_item)
               and im.item_level <= im.tran_level
               and isc.item = im.item
               and isc.supplier = I_supplier
               and isc.origin_country_id = I_origin_country_id			   
               and s.supplier = I_supplier) ich2
       on (ich.item = ich2.item
      and  ich.supplier = ich2.supplier
      and  ich.origin_country_id = ich2.origin_country_id
      and  ich.delivery_country_id = ich2.delivery_country_id)
      when matched then
        update set prim_dlvy_ctry_ind   = I_prim_dlvy_ctry_ind,
                   nic_static_ind       = I_nic_static_ind,
                   base_cost            = ich2.base_cost,
                   extended_base_cost   = ich2.extended_base_cost,
                   negotiated_item_cost = ich2.negotiated_item_cost,
                   inclusive_cost       = ich2.inclusive_cost
      when NOT matched  then
        insert(item,
               supplier,
               origin_country_id,
               delivery_country_id,
               prim_dlvy_ctry_ind,
               nic_static_ind,
               base_cost,
               extended_base_cost,
               negotiated_item_cost,
               inclusive_cost)
        values(ich2.item,
               I_supplier,
               I_origin_country_id,
               I_delivery_country_id,
               I_prim_dlvy_ctry_ind,
               I_nic_static_ind,
               ich2.base_cost,
               ich2.extended_base_cost,
               ich2.negotiated_item_cost,
               ich2.inclusive_cost);

     L_table := 'ITEM_COST_DETAIL';
     SQL_LIB.SET_MARK('OPEN',
                     'C_LOCK_ITEM_COST_DETAIL_CHILD',
                     'ITEM_COST_DETAIL',
                     NULL);

     open C_LOCK_ITEM_COST_DETAIL_CHILD;
     SQL_LIB.SET_MARK('CLOSE',
                     'C_LOCK_ITEM_COST_DETAIL_CHILD',
                     'ITEM_COST_DETAIL',
                     NULL);
     close C_LOCK_ITEM_COST_DETAIL_CHILD;

     -- if L_system_options_rec.default_tax_type = LP_GTAX then

     delete from item_cost_detail
     where item in (select item from item_master im
                     where (im.item_parent = I_item
                            or im.item_grandparent = I_item)
                       and im.item_level <= im.tran_level)
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and delivery_country_id = I_delivery_country_id;

     insert into item_cost_detail(item,
                                  supplier,
                                  origin_country_id,
                                   delivery_country_id,
                                   cond_type,
                                   cond_value,
                                   applied_on,
                                  modified_taxable_base,
                                   comp_rate,
                                   calculation_basis,
                                   recoverable_amount)
         (select im.item,
                 I_supplier supplier,
                 I_origin_country_id origin_country_id,
                 I_delivery_country_id delivery_country_id,
                 td.tax_code cond_type,
                 td.estimated_tax_value cond_value,
                 (case when L_item_record.pack_ind = 'Y' then
                           (case when L_item_record.simple_pack_ind = 'Y' then
                                      td.taxable_base
                                 when L_item_record.simple_pack_ind = 'N' then
                                      NULL
                           end)
                       when L_item_record.pack_ind = 'N' then
                            td.taxable_base
                 end) applied_on,
                 (case when L_item_record.pack_ind = 'Y' then
                           (case when L_item_record.simple_pack_ind = 'Y' then
                                      td.modified_taxable_base
                                 when L_item_record.simple_pack_ind = 'N' then
                                      NULL
                           end)
                       when L_item_record.pack_ind = 'N' then
                             td.modified_taxable_base
                 end) modified_taxable_base,
                 (case when L_item_record.pack_ind = 'Y' then
                            (case when L_item_record.simple_pack_ind = 'Y' then
                                       td.tax_rate
                                  when L_item_record.simple_pack_ind = 'N' then
                                       NULL
                            end)
                       when L_item_record.pack_ind = 'N' then
                            td.tax_rate
                 end) comp_rate,
                 (case when L_item_record.pack_ind = 'Y' then
                            (case when L_item_record.simple_pack_ind = 'Y' then
                                       td.calculation_basis
                                  when L_item_record.simple_pack_ind = 'N' then
                                       NULL
                            end)
                       when L_item_record.pack_ind = 'N' then
                             td.calculation_basis
                 end) calculation_basis,
                 td.recoverable_amount recoverable_amount
            from TABLE(CAST(L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td,
                 item_master im,
                 item_supp_country isc
           where td.tax_type in ('C','B')
             and (im.item_parent = I_item or im.item_grandparent = I_item)
             and isc.item = im.item
             and isc.supplier = I_supplier
             and isc.origin_country_id = I_origin_country_id			 
             and im.item_level <= im.tran_level );
    -- end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
     O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                           L_table,
                                           NULL,
                                           TO_CHAR(SQLCODE));
       return FALSE;
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
     return FALSE;
END COMPUTE_ITEM_COST;
---------------------------------------------------------------------------------------------
-- Function Name : COMPUTE_ITEM_COST_BULK
-- Purpose       : This function will calculate item cost based on tax details
---------------------------------------------------------------------------------------------
FUNCTION COMPUTE_ITEM_COST_BULK(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_comp_item_cost_tbl         IN OUT OBJ_COMP_ITEM_COST_TBL)

RETURN BOOLEAN IS

   L_program                      VARCHAR2(64)     := 'ITEM_COST_SQL.COMPUTE_ITEM_COST_BULK';
   L_tax_calc_tbl                 OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_comp_item_cost_tbl_in        OBJ_COMP_ITEM_COST_TBL := OBJ_COMP_ITEM_COST_TBL();
   L_comp_item_cost_tbl_out       OBJ_COMP_ITEM_COST_TBL := OBJ_COMP_ITEM_COST_TBL();
   L_tax_calc_flat_rec            OBJ_TAX_CALC_FLAT_REC := OBJ_TAX_CALC_FLAT_REC();
   L_tax_calc_flat_tbl            OBJ_TAX_CALC_FLAT_TBL := OBJ_TAX_CALC_FLAT_TBL();
   L_system_options_rec           SYSTEM_OPTIONS%ROWTYPE := NULL;
   L_user_id                      GTAX_ITEM_ROLLUP.LAST_UPDATE_ID%TYPE;
   L_sysdate                      GTAX_ITEM_ROLLUP.CREATE_DATETIME%TYPE;
   L_rec_count_in                 NUMBER(10) := 0;
   L_rec_count_out                NUMBER(10) := 0;
   L_table                        VARCHAR2(20);
   RECORD_LOCKED                  EXCEPTION;
   PRAGMA                         EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_vdate                        PERIOD.VDATE%TYPE := GET_VDATE;

   cursor C_GET_SYSTEM_INFO is
     select get_user,
          sysdate
      from dual;

   cursor C_LOCK_ITEM_COST_HEAD (L_comp_item_cost_tbl IN OBJ_COMP_ITEM_COST_TBL) is
     select 'x'
      from item_cost_head ich,
        TABLE(OBJ_COMP_ITEM_COST_RETURN(L_comp_item_cost_tbl)) comp_table_in
      where ich.item = comp_table_in.I_item
       and ich.supplier = comp_table_in.I_supplier
       and ich.origin_country_id = comp_table_in.I_origin_country_id
       and ich.delivery_country_id = comp_table_in.I_delivery_country_id
       and comp_table_in.I_update_itemcost_ind = 'Y'
       and L_system_options_rec.default_tax_type = 'GTAX'
      for update of ich.negotiated_item_cost nowait;

   cursor C_LOCK_ITEM_COST_DETAIL (L_comp_item_cost_tbl IN OBJ_COMP_ITEM_COST_TBL) is
     select 'x'
      from item_cost_detail icd,
        TABLE(OBJ_COMP_ITEM_COST_RETURN(L_comp_item_cost_tbl)) comp_table_in
      where icd.item = comp_table_in.I_item
       and icd.supplier = comp_table_in.I_supplier
       and icd.origin_country_id = comp_table_in.I_origin_country_id
       and icd.delivery_country_id = comp_table_in.I_delivery_country_id
       and comp_table_in.I_update_itemcost_ind = 'Y'
       and L_system_options_rec.default_tax_type = 'GTAX'
      for update of icd.cond_value nowait;

BEGIN

   -------------------------------------------------------------------------------------------------
   -- VALIDATION
   -------------------------------------------------------------------------------------------------
   -- Check for NULL input.  Return if no input.
   if IO_comp_item_cost_tbl is NULL or IO_comp_item_cost_tbl.COUNT = 0 then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'IO_comp_item_cost_tbl',
                                             L_program,
                                             NULL);
     return FALSE;
   end if;

   open C_GET_SYSTEM_INFO;
   fetch C_GET_SYSTEM_INFO into L_user_id,
                                L_sysdate;
   close C_GET_SYSTEM_INFO;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
       return FALSE;
   end if;

   -- Check for required parameters in IO_comp_item_cost
   for i IN IO_comp_item_cost_tbl.FIRST..IO_comp_item_cost_tbl.LAST LOOP
     if IO_comp_item_cost_tbl(i).I_item is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_item',
                                             L_program,
                                             NULL);
       return FALSE;
     end if;

     if UPPER(IO_comp_item_cost_tbl(i).I_calling_form) = LP_ISC and
       IO_comp_item_cost_tbl(i).I_nic_static_ind is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_nic_static_ind',
                                             L_program,
                                             NULL);
       return FALSE;
     end if;

     if IO_comp_item_cost_tbl(i).I_supplier is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_supplier',
                                             L_program,
                                             NULL);
       return FALSE;
     end if;

     if L_system_options_rec.default_tax_type = LP_GTAX and
       IO_comp_item_cost_tbl(i).I_location is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_location',
                                             L_program,
                                             NULL);
       return FALSE;
     end if;

     if L_system_options_rec.default_tax_type = LP_GTAX and
       IO_comp_item_cost_tbl(i).I_location is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_location',
                                             L_program,
                                             NULL);
       return FALSE;
     end if;

     -- Check inputs for rows that need the ITEM_COST* tables updated
     if IO_comp_item_cost_tbl(i).I_update_itemcost_ind = 'Y' then
       if IO_comp_item_cost_tbl(i).I_origin_country_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_origin_country_id',
                                                L_program,
                                                NULL);
         return FALSE;
       end if;
       ---
       if IO_comp_item_cost_tbl(i).I_delivery_country_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_delivery_country_id',
                                                L_program,
                                                NULL);
         return FALSE;
       end if;
       ---
       if IO_comp_item_cost_tbl(i).I_prim_dlvy_ctry_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_prim_dlvy_ctry_ind',
                                                L_program,
                                                NULL);
         return FALSE;
       end if;
       ---
       if IO_comp_item_cost_tbl(i).I_nic_static_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_nic_static_ind',
                                                L_program,
                                                NULL);
         return FALSE;
       end if;
     end if;
   end LOOP;

   -------------------------------------------------------------------------------------------------
   -- TRANSFORM OBJ_COMP_ITEM_COST_TBL TO OBJ_TAX_CALC_TBL
   -- CALL TAX_SQL
   -- TRANSFORM TAX_SQL OUTPUT AND COMPUTE COST VALUES
   -------------------------------------------------------------------------------------------------
   -- Store collection count for validation
   L_rec_count_in := IO_comp_item_cost_tbl.COUNT;
   -- Retrieve additional information and move IO_comp_item_cost_tbl into local collection L_comp_item_cost_tbl
   if L_system_options_rec.default_tax_type = LP_GTAX then
      select OBJ_COMP_ITEM_COST_REC(comp_tbl_in.I_item,
                                     comp_tbl_in.I_nic_static_ind,
                                     comp_tbl_in.I_supplier,
                                     comp_tbl_in.I_location,
                                     comp_tbl_in.I_loc_type,
                                     comp_tbl_in.I_effective_date,
                                     comp_tbl_in.I_calling_form,
                                     comp_tbl_in.I_origin_country_id,
                                     comp_tbl_in.I_delivery_country_id,
                                     comp_tbl_in.I_prim_dlvy_ctry_ind,
                                     comp_tbl_in.I_update_itemcost_ind,
                                     comp_tbl_in.I_update_itemcost_child_ind,
                                     comp_tbl_in.I_item_cost_tax_incl_ind,
                                     comp_tbl_in.O_base_cost,
                                     comp_tbl_in.O_extended_base_cost,
                                     comp_tbl_in.O_inclusive_cost,
                                     comp_tbl_in.O_negotiated_item_cost,
                                     comp_tbl_in.svat_tax_rate,
                                     decode(I_loc_type, 'S', LP_TAX_STORE_TYPE,
                                                   'W', LP_TAX_WH_TYPE,
                                                   'E', LP_TAX_EXTFIN_TYPE), --tax_loc_type
                                     im.pack_ind,
                                     im.pack_type,
                                     im.dept,
                                     sup.currency_code,           --prim_supp_currency_code
                                     mle.country_id,              --loc_prim_country
                                     -- In case of ISCL (item ranging), nic_static_ind of ITEM_COST_HEAD for the
                                     -- location's country should be used to determine if the cost should be tax
                                     -- inclusive or not. That value is passed in through comp_tbl_in.I_nic_static_ind.
                                     -- Otherwise, use item_cost_tax_incl_ind based on location's country.
                                     decode(comp_tbl_in.I_calling_form, LP_ISCL, comp_tbl_in.I_nic_static_ind,
                                            ca.item_cost_tax_incl_ind),  --loc_prim_country_tax_incl_ind
                                     NULL,                        --gtax_total_tax_amount
                                     NULL,                        --gtax_total_tax_amount_nic
                                     NULL)                        --gtax_total_recover_amount
       BULK COLLECT INTO L_comp_item_cost_tbl_in
       from TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) comp_tbl_in,
            item_master im,
            mv_l10n_entity mle,
            country_attrib ca,
            sups sup
       where comp_tbl_in.I_item = im.item
         and comp_tbl_in.I_supplier = sup.supplier
         and (mle.entity = 'LOC' or
             (mle.entity = 'PTNR' and mle.entity_type = 'E'))
         and mle.entity_id = TO_CHAR(comp_tbl_in.I_location)
         and mle.country_id = ca.country_id;

     L_rec_count_out := L_comp_item_cost_tbl_in.COUNT;

   -- Check collection count after collection copy
     if L_rec_count_in != L_rec_count_out then  
       O_error_message := SQL_LIB.CREATE_MSG('COL_COUNT_ADDR_CTRY_ERR',
                                               L_program,
                                               'IO_comp_item_cost_tbl',
                                               'L_comp_item_cost_tbl_in');
       return FALSE;
     end if;
   end if;
   -- Reset collection counters
   L_rec_count_in := 0;
   L_rec_count_out := 0;

   -- Set input counter to count of transformed table
   -- This will be checked after tax calculations have been performed
   L_rec_count_in := NVL(L_comp_item_cost_tbl_in.COUNT, 0);

   If L_system_options_rec.default_tax_type = LP_GTAX then
     -- Load L_tax_calc_tbl for tax engine call
     -- Only non-packs and vendor packs are picked-up
     select OBJ_TAX_CALC_REC(inner_table.I_item,
                             inner_table.pack_ind,
                             inner_table.I_supplier,
                             LP_TAX_SUPPLIER_TYPE,
                             inner_table.I_location,
                             inner_table.tax_loc_type,
                             inner_table.I_effective_date,
                             nvl(inner_table.amount,0),
                             inner_table.prim_supp_currency_code,
                             inner_table.I_item_cost_tax_incl_ind,
                             inner_table.I_origin_country_id,
                             NULL, --O_cum_tax_pct
                             NULL, --O_cum_tax_value
                             NULL, --O_total_tax_amount
                             NULL, --O_total_tax_amount_curr
                             NULL, --O_total_recover_amount
                             NULL, --O_total_recover_amount_curr
                             NULL, --O_tax_detail_tbl
                             'ITEMCOST',    --I_tran_type
                             GET_VDATE(),   --I_tran_date
                             NULL,          --I_tran_id
                             'C',           --I_cost_retail_ind
                             NULL,          --I_source_entity
                             NULL,          --I_source_entity_type
                             NULL)          --O_tax_exempt_ind
      BULK COLLECT INTO L_tax_calc_tbl
      -- Distinct is used as delivery_country_id input is not required at this point
      from (select distinct
                   comp_tbl_in.I_item,
                   comp_tbl_in.pack_ind,
                   comp_tbl_in.I_supplier,               --I_from_entity
                   comp_tbl_in.I_location,               --I_to_entity
                   comp_tbl_in.tax_loc_type,             --I_to_entity_type
                   comp_tbl_in.I_effective_date,         --I_effective_from_date
                   --NVL2(string1, value_if_NOT_null, value_if_null)
                   NVL2(comp_tbl_in.O_negotiated_item_cost, comp_tbl_in.O_negotiated_item_cost, comp_tbl_in.O_base_cost) amount,
                   comp_tbl_in.prim_supp_currency_code,  --I_amount_curr
                   comp_tbl_in.I_item_cost_tax_incl_ind, --I_AMOUNT_TAX_INCL_IND
                   comp_tbl_in.I_origin_country_id       --I_origin_country_id
              from TABLE(OBJ_COMP_ITEM_COST_RETURN(L_comp_item_cost_tbl_in)) comp_tbl_in,
                   country_attrib ca
             where comp_tbl_in.loc_prim_country = ca.country_id
               and (comp_tbl_in.pack_type is NULL or comp_tbl_in.pack_type = 'V')) inner_table;  -- non-packs and vendor packs only

      if L_tax_calc_tbl.COUNT > 0 then
         if TAX_SQL.CALC_COST_TAX(O_error_message,
                                  L_tax_calc_tbl) = FALSE then
            return FALSE;
         end if;

         -- Move tax engine output to local L_comp_item_cost_tbl_out collection
         select OBJ_COMP_ITEM_COST_REC(comp_tbl_in.I_item,
                                    comp_tbl_in.I_nic_static_ind,
                                    comp_tbl_in.I_supplier,
                                    comp_tbl_in.I_location,
                                    comp_tbl_in.I_loc_type,
                                    comp_tbl_in.I_effective_date,
                                    comp_tbl_in.I_calling_form,
                                    comp_tbl_in.I_origin_country_id,
                                    comp_tbl_in.I_delivery_country_id,
                                    comp_tbl_in.I_prim_dlvy_ctry_ind,
                                    comp_tbl_in.I_update_itemcost_ind,
                                    comp_tbl_in.I_update_itemcost_child_ind,
                                    comp_tbl_in.I_item_cost_tax_incl_ind,
                                    comp_tbl_in.O_base_cost,
                                    comp_tbl_in.O_extended_base_cost,
                                    comp_tbl_in.O_inclusive_cost,
                                    comp_tbl_in.O_negotiated_item_cost,
                                    comp_tbl_in.svat_tax_rate,
                                    comp_tbl_in.tax_loc_type,
                                    comp_tbl_in.pack_ind,
                                    comp_tbl_in.pack_type,
                                    comp_tbl_in.dept,
                                    comp_tbl_in.prim_supp_currency_code,
                                    comp_tbl_in.loc_prim_country,
                                    comp_tbl_in.loc_prim_country_tax_incl_ind,
                                    NVL(tax_engine_out.O_total_tax_amount, 0),
                                    NVL((select sum(NVL(td.estimated_tax_value, 0))
                                         from TABLE(CAST(tax_engine_out.O_tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td,
                                             vat_codes vc
                                        where td.tax_code = vc.vat_code
                                          and vc.incl_nic_ind = 'Y'), 0),  --gtax_total_tax_amount_nic
                                    NVL(tax_engine_out.O_total_recover_amount, 0))
             BULK COLLECT INTO L_comp_item_cost_tbl_out
       from TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax_engine_out,
            TABLE(OBJ_COMP_ITEM_COST_RETURN(L_comp_item_cost_tbl_in)) comp_tbl_in
      where comp_tbl_in.I_item = tax_engine_out.I_item (+)
        and comp_tbl_in.I_supplier = tax_engine_out.I_from_entity (+)
        and comp_tbl_in.I_location = tax_engine_out.I_to_entity (+)
        and comp_tbl_in.I_effective_date = tax_engine_out.I_effective_from_date (+)
        and comp_tbl_in.I_origin_country_id = tax_engine_out.I_origin_country_id (+)
        and (comp_tbl_in.pack_type is NULL or
             comp_tbl_in.pack_type = 'V' or
             comp_tbl_in.pack_type = 'B');
       -- Loop through collection and perform computation
       if L_comp_item_cost_tbl_out.COUNT > 0 then
         for i in L_comp_item_cost_tbl_out.FIRST..L_comp_item_cost_tbl_out.LAST LOOP
            if UPPER(L_comp_item_cost_tbl_out(i).I_calling_form) = LP_ISC then
              if L_comp_item_cost_tbl_out(i).loc_prim_country_tax_incl_ind = 'N' then
                 -- If the country has cost exclusive of tax then add total tax to base cost to determine negotiated item cost
                L_comp_item_cost_tbl_out(i).O_negotiated_item_cost := NVL(L_comp_item_cost_tbl_out(i).O_base_cost, 0) + NVL(L_comp_item_cost_tbl_out(i).gtax_total_tax_amount_nic, 0);
              else
                -- If the country has cost inclusive of tax then strip-off total tax to determine base cost
                L_comp_item_cost_tbl_out(i).O_base_cost := NVL(L_comp_item_cost_tbl_out(i).O_negotiated_item_cost, 0) - NVL(L_comp_item_cost_tbl_out(i).gtax_total_tax_amount_nic, 0);
              end if;
            elsif UPPER(L_comp_item_cost_tbl_out(i).I_calling_form) = LP_ISCL then
              if L_comp_item_cost_tbl_out(i).I_nic_static_ind is NULL then
                L_comp_item_cost_tbl_out(i).O_base_cost := NVL(L_comp_item_cost_tbl_out(i).O_negotiated_item_cost, 0) - NVL(L_comp_item_cost_tbl_out(i).gtax_total_tax_amount_nic,0);
              else
                if L_comp_item_cost_tbl_out(i).I_nic_static_ind = 'N' then
                  L_comp_item_cost_tbl_out(i).O_negotiated_item_cost := NVL(L_comp_item_cost_tbl_out(i).O_base_cost, 0) + NVL(L_comp_item_cost_tbl_out(i).gtax_total_tax_amount_nic,0);
                else
                  L_comp_item_cost_tbl_out(i).O_base_cost := NVL(L_comp_item_cost_tbl_out(i).O_negotiated_item_cost, 0) - NVL(L_comp_item_cost_tbl_out(i).gtax_total_tax_amount_nic,0);
                end if;
              end if;
            end if;
            -- Determine extended base cost
            L_comp_item_cost_tbl_out(i).O_extended_base_cost := NVL(L_comp_item_cost_tbl_out(i).O_base_cost,0) + NVL(L_comp_item_cost_tbl_out(i).gtax_total_tax_amount, 0) -
                                                   NVL(L_comp_item_cost_tbl_out(i).gtax_total_recover_amount, 0);
            -- Determine inclusive cost by adding total tax to base cost
            L_comp_item_cost_tbl_out(i).O_inclusive_cost := NVL(L_comp_item_cost_tbl_out(i).O_base_cost,0) + NVL(L_comp_item_cost_tbl_out(i).gtax_total_tax_amount, 0);
         end LOOP;
       end if;
     end if;

     -- Transfer the contents of the collection from the tax engine
     IO_comp_item_cost_tbl := L_comp_item_cost_tbl_out;
     -- Zero fill any NULL values
     if IO_comp_item_cost_tbl.COUNT > 0 then
       for i in IO_comp_item_cost_tbl.FIRST..IO_comp_item_cost_tbl.LAST LOOP
         IO_comp_item_cost_tbl(i).O_base_cost            := NVL(IO_comp_item_cost_tbl(i).O_base_cost, 0);
         IO_comp_item_cost_tbl(i).O_extended_base_cost   := NVL(IO_comp_item_cost_tbl(i).O_extended_base_cost, 0);
         IO_comp_item_cost_tbl(i).O_inclusive_cost       := NVL(IO_comp_item_cost_tbl(i).O_inclusive_cost, 0);
         IO_comp_item_cost_tbl(i).O_negotiated_item_cost := NVL(IO_comp_item_cost_tbl(i).O_negotiated_item_cost, 0);
       end LOOP;
     end if;

   -- Check if the input and output collections counts are the same
     L_rec_count_out := NVL(IO_comp_item_cost_tbl.COUNT, 0);
  
     if L_rec_count_in != L_rec_count_out then
       O_error_message := SQL_LIB.CREATE_MSG('COL_COUNT_ITEM_COMP_ERR',
                                   L_program,
                                   'L_comp_item_cost_tbl_in',
                                   'IO_comp_item_cost_tbl');
       return FALSE;
     end if;
   end if;   

   -------------------------------------------------------------------------------------------------
   -- MERGE INTO ITEM_COST* TABLES
   -------------------------------------------------------------------------------------------------
   if IO_comp_item_cost_tbl.COUNT > 0 and L_system_options_rec.default_tax_type = LP_GTAX then
     -- Lock item_cost_head records to be merged
     L_table := 'ITEM_COST_HEAD';
     SQL_LIB.SET_MARK('OPEN',
                      'C_LOCK_ITEM_COST_HEAD',
                      'ITEM_COST_HEAD',
                       NULL);
     open C_LOCK_ITEM_COST_HEAD(IO_comp_item_cost_tbl);
     SQL_LIB.SET_MARK('CLOSE',
                      'C_LOCK_ITEM_COST_HEAD',
                      'ITEM_COST_HEAD',
                      NULL);
     close C_LOCK_ITEM_COST_HEAD;

     -- Merge into ITEM_COST_HEAD for records that have I_update_itemcost_ind = 'Y'
     merge into item_cost_head ich
     using (select distinct result_tbl.I_item,
                            result_tbl.I_supplier,
                            result_tbl.I_origin_country_id,
                            result_tbl.I_delivery_country_id,
                            result_tbl.I_prim_dlvy_ctry_ind,
                            result_tbl.I_nic_static_ind,
                            result_tbl.O_base_cost,
                            result_tbl.O_extended_base_cost,
                            result_tbl.O_negotiated_item_cost,
                            result_tbl.O_inclusive_cost
             from TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) result_tbl,
                  sups s
            where result_tbl.I_update_itemcost_ind = 'Y'
              and s.supplier = result_tbl.I_supplier) ich1
       on (ich.item = ich1.I_item
      and  ich.supplier = ich1.I_supplier
      and  ich.origin_country_id = ich1.I_origin_country_id
      and  NVL(ich1.I_delivery_country_id,-2) = NVL(ich.delivery_country_id,-1)
      and  ich.prim_dlvy_ctry_ind = ich1.I_prim_dlvy_ctry_ind)
      when matched then
        update set nic_static_ind       = ich1.I_nic_static_ind,
                   base_cost            = ich1.O_base_cost,
                   extended_base_cost   = ich1.O_extended_base_cost,
                   negotiated_item_cost = ich1.O_negotiated_item_cost,
                   inclusive_cost       = ich1.O_inclusive_cost
             where L_system_options_rec.default_tax_type = 'GTAX'
      when NOT matched  then
        insert(item,
              supplier,
              origin_country_id,
              delivery_country_id,
              prim_dlvy_ctry_ind,
              nic_static_ind,
              base_cost,
              extended_base_cost,
              negotiated_item_cost,
              inclusive_cost)
        values(ich1.I_item,
              ich1.I_supplier,
              ich1.I_origin_country_id,
              ich1.I_delivery_country_id,
              ich1.I_prim_dlvy_ctry_ind,
              ich1.I_nic_static_ind,
              ich1.O_base_cost,
              ich1.O_extended_base_cost,
              ich1.O_negotiated_item_cost,
              ich1.O_inclusive_cost);

     -- Flatten L_tax_calc_tbl to create a set for merging into ITEM_COST DETAIL
     if L_tax_calc_tbl.COUNT > 0 then
       for i in L_tax_calc_tbl.FIRST..L_tax_calc_tbl.LAST LOOP
         if L_tax_calc_tbl(i).O_tax_detail_tbl is not NULL then
            if L_tax_calc_tbl(i).O_tax_detail_tbl.COUNT > 0 then
              for j in L_tax_calc_tbl(i).O_tax_detail_tbl.FIRST..L_tax_calc_tbl(i).O_tax_detail_tbl.LAST LOOP
                L_tax_calc_flat_rec.I_item                      := L_tax_calc_tbl(i).I_item;
                L_tax_calc_flat_rec.I_pack_ind                  := L_tax_calc_tbl(i).I_pack_ind;
                L_tax_calc_flat_rec.I_from_entity               := L_tax_calc_tbl(i).I_from_entity;
                L_tax_calc_flat_rec.I_from_entity_type          := L_tax_calc_tbl(i).I_from_entity_type;
                L_tax_calc_flat_rec.I_to_entity                 := L_tax_calc_tbl(i).I_to_entity;
                L_tax_calc_flat_rec.I_to_entity_type            := L_tax_calc_tbl(i).I_to_entity_type;
                L_tax_calc_flat_rec.I_effective_from_date       := L_tax_calc_tbl(i).I_effective_from_date;
                L_tax_calc_flat_rec.I_amount                    := L_tax_calc_tbl(i).I_amount;
                L_tax_calc_flat_rec.I_amount_curr               := L_tax_calc_tbl(i).I_amount_curr;
                L_tax_calc_flat_rec.O_cum_tax_pct               := L_tax_calc_tbl(i).O_cum_tax_pct;
                L_tax_calc_flat_rec.O_cum_tax_value             := L_tax_calc_tbl(i).O_cum_tax_value;
                L_tax_calc_flat_rec.O_total_tax_amount          := L_tax_calc_tbl(i).O_total_tax_amount;
                L_tax_calc_flat_rec.O_total_tax_amount_curr     := L_tax_calc_tbl(i).O_total_tax_amount_curr;
                L_tax_calc_flat_rec.O_total_recover_amount      := L_tax_calc_tbl(i).O_total_recover_amount;
                L_tax_calc_flat_rec.O_total_recover_amount_curr := L_tax_calc_tbl(i).O_total_recover_amount_curr;
                L_tax_calc_flat_rec.tax_code                    := L_tax_calc_tbl(i).O_tax_detail_tbl(j).tax_code;
                L_tax_calc_flat_rec.tax_type                    := L_tax_calc_tbl(i).O_tax_detail_tbl(j).tax_type;
                L_tax_calc_flat_rec.calculation_basis           := L_tax_calc_tbl(i).O_tax_detail_tbl(j).calculation_basis;
                L_tax_calc_flat_rec.taxable_base                := L_tax_calc_tbl(i).O_tax_detail_tbl(j).taxable_base;
                L_tax_calc_flat_rec.modified_taxable_base       := L_tax_calc_tbl(i).O_tax_detail_tbl(j).modified_taxable_base;
                L_tax_calc_flat_rec.tax_rate                    := L_tax_calc_tbl(i).O_tax_detail_tbl(j).tax_rate;
                L_tax_calc_flat_rec.estimated_tax_value         := L_tax_calc_tbl(i).O_tax_detail_tbl(j).estimated_tax_value;
                L_tax_calc_flat_rec.recoverable_amount          := L_tax_calc_tbl(i).O_tax_detail_tbl(j).recoverable_amount;
                L_tax_calc_flat_rec.currency_code               := L_tax_calc_tbl(i).O_tax_detail_tbl(j).currency_code;
                --
                L_tax_calc_flat_tbl.extend();
                L_tax_calc_flat_tbl(L_tax_calc_flat_tbl.COUNT)  := L_tax_calc_flat_rec;
              end LOOP;
            end if;
         end if;
       end LOOP;
     end if;

     -- Lock item_cost_head records to be merged
     L_table := 'ITEM_COST_DETAIL';
     SQL_LIB.SET_MARK('OPEN',
                     'C_LOCK_ITEM_COST_DETAIL',
                     'ITEM_COST_DETAIL',
                     NULL);
     open C_LOCK_ITEM_COST_DETAIL(IO_comp_item_cost_tbl);
     SQL_LIB.SET_MARK('CLOSE',
                     'C_LOCK_ITEM_COST_DETAIL',
                     'ITEM_COST_DETAIL',
                     NULL);
     close C_LOCK_ITEM_COST_DETAIL;

    -- if L_system_options_rec.default_tax_type = LP_GTAX then

        SQL_LIB.SET_MARK('DELETE',
                         'DELETE ITEM_COST_DETAIL',
                         'ITEM_COST_DETAIL',
                         NULL);
      delete from item_cost_detail
              where rowid in (select icd.rowid
                                from item_cost_detail icd,
                                     TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) result_tbl
                                where result_tbl.I_item = icd.item
                                  and result_tbl.I_supplier = icd.supplier
                                  and result_tbl.I_origin_country_id = icd.origin_country_id
                                  and result_tbl.I_delivery_country_id = icd.delivery_country_id
                                  and result_tbl.I_update_itemcost_ind = 'Y');

        SQL_LIB.SET_MARK('INSERT',
                         'INSERT ITEM_COST_DETAIL',
                         'ITEM_COST_DETAIL',
                         NULL);
       insert into item_cost_detail(item,
                                    supplier,
                                    origin_country_id,
                                    delivery_country_id,
                                    cond_type,
                                    cond_value,
                                    applied_on,
                                    modified_taxable_base,
                                    comp_rate,
                                    calculation_basis,
                                    recoverable_amount )
                            (select result_tbl.I_item,
                                    result_tbl.I_supplier,
                                    result_tbl.I_origin_country_id,
                                    result_tbl.I_delivery_country_id,
                                    tax_engine_out.tax_code cond_type,
                                    tax_engine_out.estimated_tax_value cond_value,
                                    --
                                    (case when im.pack_ind = 'Y' then
                                               (case when im.simple_pack_ind = 'Y' then
                                                          tax_engine_out.taxable_base
                                                     when im.simple_pack_ind = 'N' then
                                                          NULL
                                                end)
                                          when im.pack_ind = 'N' then
                                               tax_engine_out.taxable_base
                                    end) applied_on,
                                    --
                                    (case when im.pack_ind = 'Y' then
                                               (case when im.simple_pack_ind = 'Y' then
                                                          tax_engine_out.modified_taxable_base
                                                     when im.simple_pack_ind = 'N' then
                                                          NULL
                                                end)
                                          when im.pack_ind = 'N' then
                                               tax_engine_out.modified_taxable_base
                                    end) modified_taxable_base,
                                    --
                                    (case when im.pack_ind = 'Y' then
                                               (case when im.simple_pack_ind = 'Y' then
                                                          tax_engine_out.tax_rate
                                                     when im.simple_pack_ind = 'N' then
                                                          NULL
                                                end)
                                          when im.pack_ind = 'N' then
                                               tax_engine_out.tax_rate
                                    end) comp_rate,
                                    --
                                    (case when im.pack_ind = 'Y' then
                                               (case when im.simple_pack_ind = 'Y' then
                                                          tax_engine_out.calculation_basis
                                                     when im.simple_pack_ind = 'N' then
                                                          NULL
                                                end)
                                          when im.pack_ind = 'N' then
                                               tax_engine_out.calculation_basis
                                    end) calculation_basis,
                                    --
                                    tax_engine_out.recoverable_amount
                               from TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) result_tbl,
                                    TABLE(CAST(L_tax_calc_flat_tbl as OBJ_TAX_CALC_FLAT_TBL)) tax_engine_out,
                                    item_master im
                               where tax_engine_out.tax_type in ('C','B')
                                 and result_tbl.I_item = tax_engine_out.I_item (+)
                                 and result_tbl.I_supplier = tax_engine_out.I_from_entity (+)
                                 and result_tbl.I_location = tax_engine_out.I_to_entity (+)
                                 and result_tbl.I_update_itemcost_ind = 'Y'
                                 and im.item = result_tbl.I_item);
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
     O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                           L_table,
                                           NULL,
                                           TO_CHAR(SQLCODE));
       return FALSE;
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
     return FALSE;
END COMPUTE_ITEM_COST_BULK;
---------------------------------------------------------------------------------------------
-- Function Name : GET_COST_INFO
-- Purpose       : This function will retrieve the cost information (NIC, BC, EBC, IC, NIC is static indicator),
--                 default cost indicators for the primary delivery country of a given item/supplier/origin country
--                 combination, or for the primary delivery country of the primary supplier/origin country of the item if
--                 the supplier and origin country is not passed in.
---------------------------------------------------------------------------------------------
FUNCTION GET_COST_INFO (O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_item_cost_info_rec   IN OUT ITEM_COST_INFO_REC,
                        I_item                 IN     ITEM_COST_HEAD.ITEM%TYPE,
                        I_supplier             IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                        I_origin_country_id    IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'ITEM_COST_SQL.GET_COST_INFO';
   L_supplier            ITEM_COST_HEAD.SUPPLIER%TYPE := NULL;
   L_origin_country_id   ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE := NULL;

   cursor C_GET_PRIM_SUPP_COUNTRY is
     select NVL(I_supplier,is1.supplier),
          NVL(I_origin_country_id,isc.origin_country_id)
      from item_supplier is1,
          item_supp_country isc
      where is1.item = I_item
       and is1.item = isc.item
       and is1.supplier = isc.supplier
       and ((I_supplier is NULL and
           is1.primary_supp_ind = 'Y') or
          I_supplier is NOT NULL and
          is1.supplier = I_supplier
          )
       and ((I_origin_country_id is NULL and
            isc.primary_country_ind = 'Y') or
           I_origin_country_id is NOT NULL and
           isc.origin_country_id = I_origin_country_id);

   cursor C_GET_COST_INFO IS
     select I_item,
             L_supplier,
             L_origin_country_id,
             ich.delivery_country_id,
             ich.nic_static_ind,
             ich.base_cost,
             ich.negotiated_item_cost,
             ich.extended_base_cost,
             ich.inclusive_cost,
             ca.default_po_cost,
             ca.default_deal_cost,
             ca.default_cost_comp_cost
      from item_cost_head ich,
          country_attrib ca
      where ich.item = I_item
       and ich.supplier = L_supplier
       and ich.origin_country_id = L_origin_country_id
       and ich.prim_dlvy_ctry_ind = 'Y'
       and ich.delivery_country_id =  ca.country_id;

BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_item',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_supplier is NOT NULL then
     L_supplier := I_supplier;
   end if;

   if I_origin_country_id is NOT NULL then
     L_origin_country_id := I_origin_country_id;
   end if;
   ---
   if I_supplier is NULL or I_origin_country_id is NULL then

     SQL_LIB.SET_MARK('OPEN',
                     'C_GET_PRIM_SUPP_COUNTRY',
                     'ITEM_SUPP_COUNTRY',
                     NULL);

     open C_GET_PRIM_SUPP_COUNTRY;

     SQL_LIB.SET_MARK('FETCH',
                     'C_GET_PRIM_SUPP_COUNTRY',
                     'ITEM_SUPP_COUNTRY',
                      NULL);

     fetch C_GET_PRIM_SUPP_COUNTRY into L_supplier,
                               L_origin_country_id;

     SQL_LIB.SET_MARK('CLOSE',
                     'C_GET_PRIM_SUPP_COUNTRY',
                     'ITEM_SUPP_COUNTRY',
                      NULL);

     close C_GET_PRIM_SUPP_COUNTRY;

   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                     'C_GET_COST_INFO',
                     'ITEM_COST_HEAD,COUNTRY_ATTRIB',
                      NULL);

   open C_GET_COST_INFO;

   SQL_LIB.SET_MARK('FETCH',
                     'C_GET_COST_INFO',
                     'ITEM_COST_HEAD,COUNTRY_ATTRIB',
                     NULL);

   fetch C_GET_COST_INFO into O_item_cost_info_rec;

   SQL_LIB.SET_MARK('CLOSE',
                     'C_GET_COST_INFO',
                     'ITEM_COST_HEAD,COUNTRY_ATTRIB',
                     NULL);

   close C_GET_COST_INFO;

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
     return FALSE;
END GET_COST_INFO;
---------------------------------------------------------------------------------------------
-- Function Name : UPD_ITEM_SUPP_COUNTRY_COST
-- Purpose       : This function will update the costing information of the primary delivery country
--                 in ITEM_SUPP_COUNTRY table.
---------------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_SUPP_COUNTRY_COST (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item                   IN       ITEM_COST_HEAD.ITEM%TYPE,
                                     I_supplier               IN       ITEM_COST_HEAD.SUPPLIER%TYPE,
                                     I_origin_country_id      IN       ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                                     I_location               IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                                     I_base_cost              IN       ITEM_COST_HEAD.BASE_COST%TYPE,
                                     I_extended_base_cost     IN       ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE,
                                     I_negotiated_item_cost   IN       ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE,
                                     I_inclusive_cost         IN       ITEM_COST_HEAD.INCLUSIVE_COST%TYPE,
                                     I_callingform            IN       VARCHAR2,
									 I_default_down_ind       IN       VARCHAR2 DEFAULT 'Y')

RETURN BOOLEAN IS

   L_program                VARCHAR2(64) := 'ITEM_COST_SQL.UPD_ITEM_SUPP_COUNTRY_COST';
   L_base_cost              ITEM_COST_HEAD.BASE_COST%TYPE := 0;
   L_negotiated_item_cost   ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE := 0;
   L_extended_base_cost     ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE := 0;
   L_inclusive_cost         ITEM_COST_HEAD.INCLUSIVE_COST%TYPE := 0;
   L_table                  VARCHAR2(30) := NULL;
   L_delivery_country_id    ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE;
   L_default_po_cost        COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE;
   L_child_item             ITEM_MASTER.ITEM%TYPE;
   L_pack_ind               ITEM_MASTER.PACK_IND%TYPE;
   L_iscl_item              ITEM_MASTER.ITEM%TYPE;
   L_null_cost_exist        VARCHAR2(1) := NULL;
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_exists                 VARCHAR2(1) := 'N';
   L_item_record            ITEM_MASTER%ROWTYPE;

   cursor C_LOCK_ITEM_SUPP_COUNTRY is
     select 'x'
       from item_supp_country
      where item = I_item
        and supplier = I_supplier
        and origin_country_id = I_origin_country_id
        for update nowait;

   cursor C_LOCK_ITEM_SUPP_COUNTRY_LOC is
     select 'x'
       from item_supp_country_loc
      where item = I_item
        and supplier = I_supplier
        and origin_country_id = I_origin_country_id
        and loc = I_location
        for update nowait;

   cursor C_PRIM_LOC is
     select 'Y'
       from item_loc_soh
      where item = I_item
        and primary_supp = I_supplier
        and primary_cntry = I_origin_country_id
        and loc  = I_location;

   cursor C_GET_PRIM_DLVY_CTRY_DETAILS is
     select base_cost,
            negotiated_item_cost,
            extended_base_cost,
            inclusive_cost,
            delivery_country_id
       from item_cost_head
      where item = I_item
        and supplier = I_supplier
        and origin_country_id = I_origin_country_id
        and prim_dlvy_ctry_ind = 'Y';

   cursor C_GET_CHILD_ITEMS is
     select item
       from item_master
      where (item_parent = I_item or
            item_grandparent = I_item)
        and item_level <= tran_level;


   cursor C_CHECK_ISCL_NULL_COST is
     select 'x'
       from item_supp_country_loc
      where item = L_iscl_item
        and supplier = I_supplier
        and origin_country_id = I_origin_country_id
        and base_cost is NULL
        and negotiated_item_cost is NULL
        and extended_base_cost is NULL
        and inclusive_cost is NULL
        and rownum = 1;

BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_supplier',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_origin_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_negotiated_item_cost is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_negotiated_item_cost',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_base_cost is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_base_cost',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_extended_base_cost is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_extended_base_cost',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---

   if I_inclusive_cost is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_inclusive_cost',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                         L_item_record,
                                         I_item) = FALSE then
       return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_PRIM_DLVY_CTRY_DETAILS',
                    'ITEM_COST_HEAD',
                    NULL);

   open C_GET_PRIM_DLVY_CTRY_DETAILS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_PRIM_DLVY_CTRY_DETAILS',
                    'ITEM_COST_HEAD',
                    NULL);

   fetch C_GET_PRIM_DLVY_CTRY_DETAILS into L_base_cost,
                                           L_negotiated_item_cost,
                                           L_extended_base_cost,
                                           L_inclusive_cost,
                                           L_delivery_country_id;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_PRIM_DLVY_CTRY_DETAILS',
                    'ITEM_COST_HEAD',
                    NULL);

   close C_GET_PRIM_DLVY_CTRY_DETAILS;

   if ITEM_COST_SQL.GET_DEFAULT_PO_COST(O_error_message,
                                        L_default_po_cost,
                                        L_delivery_country_id,
                                        NULL) = FALSE then
     return FALSE;
   end if;

   if UPPER(I_callingform) = LP_ISC then

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ITEM_SUPP_COUNTRY',
                       'ITEM_SUPP_COUNTRY',
                       NULL);

      open C_LOCK_ITEM_SUPP_COUNTRY;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ITEM_SUPP_COUNTRY',
                       'ITEM_SUPP_COUNTRY',
                       NULL);

      close C_LOCK_ITEM_SUPP_COUNTRY;

      L_table := 'ITEM_SUPP_COUNTRY';

      update item_supp_country
         set unit_cost = DECODE(L_default_po_cost,
                                'NIC',NVL(L_negotiated_item_cost,unit_cost),
                                'BC',NVL(L_base_cost,unit_cost)),
             negotiated_item_cost = L_negotiated_item_cost,
             extended_base_cost = L_extended_base_cost,
             inclusive_cost = L_inclusive_cost,
             base_cost = L_base_cost
       where item  = I_item
         and supplier =I_supplier
         and origin_country_id = I_origin_country_id;

      L_iscl_item := I_item;

      open C_CHECK_ISCL_NULL_COST;
      fetch C_CHECK_ISCL_NULL_COST into L_null_cost_exist;
      close C_CHECK_ISCL_NULL_COST;

      if L_null_cost_exist is NOT NULL then

         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_ITEM_SUPP_COUNTRY_LOC',
                          'ITEM_SUPP_COUNTRY_LOC',
                          NULL);

         open C_LOCK_ITEM_SUPP_COUNTRY_LOC;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_ITEM_SUPP_COUNTRY_LOC',
                          'ITEM_SUPP_COUNTRY_LOC',
                          NULL);

         close C_LOCK_ITEM_SUPP_COUNTRY_LOC;

         L_table := 'ITEM_SUPP_COUNTRY_LOC';

         update item_supp_country_loc
            set unit_cost = DECODE(L_default_po_cost,
                                   'NIC',NVL(L_negotiated_item_cost,unit_cost),
                                   'BC',NVL(L_base_cost,unit_cost)),
                negotiated_item_cost = L_negotiated_item_cost,
                extended_base_cost = L_extended_base_cost,
                inclusive_cost = L_inclusive_cost,
                base_cost = L_base_cost
          where item  = I_item
            and supplier =I_supplier
            and origin_country_id = I_origin_country_id;

      end if;

      L_table := 'ITEM_SUPP_COUNTRY';

    if I_default_down_ind = 'Y' then 
      for c_rec in C_GET_CHILD_ITEMS LOOP
        L_child_item := c_rec.item;

        update item_supp_country
           set unit_cost = DECODE(L_default_po_cost,
                                  'NIC',NVL(L_negotiated_item_cost,unit_cost),
                                  'BC',NVL(L_base_cost,unit_cost)),
               negotiated_item_cost = L_negotiated_item_cost,
               extended_base_cost = L_extended_base_cost,
               inclusive_cost = L_inclusive_cost,
               base_cost = L_base_cost
         where item  = L_child_item
           and supplier =I_supplier
           and origin_country_id = I_origin_country_id;

         L_iscl_item := L_child_item;
         L_null_cost_exist := NULL;

         open C_CHECK_ISCL_NULL_COST;
         fetch C_CHECK_ISCL_NULL_COST into L_null_cost_exist;
         close C_CHECK_ISCL_NULL_COST;

         -- update item_supp_country_loc records for child items
         if L_null_cost_exist is NOT NULL then

            L_table := 'ITEM_SUPP_COUNTRY_LOC';

            update item_supp_country_loc
               set unit_cost = DECODE(L_default_po_cost,
                                      'NIC',NVL(L_negotiated_item_cost,unit_cost),
                                      'BC',NVL(L_base_cost,unit_cost)),
                   negotiated_item_cost = L_negotiated_item_cost,
                   extended_base_cost = L_extended_base_cost,
                   inclusive_cost = L_inclusive_cost,
                   base_cost = L_base_cost
             where item  = L_child_item
               and supplier =I_supplier
               and origin_country_id = I_origin_country_id;

         end if;
      end LOOP;
	 end if;

   elsif UPPER(I_callingform) = LP_ISCL then

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ITEM_SUPP_COUNTRY_LOC',
                       'ITEM_SUPP_COUNTRY_LOC',
                       NULL);

      open C_LOCK_ITEM_SUPP_COUNTRY_LOC;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ITEM_SUPP_COUNTRY_LOC',
                       'ITEM_SUPP_COUNTRY_LOC',
                       NULL);

      close C_LOCK_ITEM_SUPP_COUNTRY_LOC;

      L_table := 'ITEM_SUPP_COUNTRY_LOC';

      update item_supp_country_loc
         set unit_cost = DECODE(L_default_po_cost,
                                'NIC',NVL(I_negotiated_item_cost,unit_cost),
                                'BC',NVL(I_base_cost,unit_cost)),
             negotiated_item_cost = I_negotiated_item_cost,
             extended_base_cost = I_extended_base_cost,
             inclusive_cost = I_inclusive_cost,
             base_cost = I_base_cost
       where item  = I_item
         and supplier =I_supplier
         and origin_country_id = I_origin_country_id
         and loc = I_location;

      for c_rec in C_GET_CHILD_ITEMS LOOP
        L_child_item := c_rec.item;

        update item_supp_country_loc
           set unit_cost = DECODE(L_default_po_cost,
                                  'NIC',NVL(I_negotiated_item_cost,unit_cost),
                                  'BC',NVL(I_base_cost,unit_cost)),
               negotiated_item_cost = I_negotiated_item_cost,
               extended_base_cost = I_extended_base_cost,
               inclusive_cost = I_inclusive_cost,
               base_cost = I_base_cost
         where item  = L_child_item
           and supplier =I_supplier
           and origin_country_id = I_origin_country_id
           and loc = I_location;

      end LOOP;

      SQL_LIB.SET_MARK('OPEN',
                       'C_PRIM_LOC',
                       'ITEM_LOC_SOH',
                       NULL);

      open C_PRIM_LOC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_PRIM_LOC',
                       'ITEM_LOC_SOH',
                       NULL);
      fetch C_PRIM_LOC INTO L_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_PRIM_LOC',
                       'ITEM_LOC_SOH',
                       NULL);

      close C_PRIM_LOC;

      L_table := 'ITEM_LOC_SOH';

      if L_exists = 'Y' and L_item_record.status != 'A' then
         if not UPDATE_BASE_COST.CHG_ITEMLOC_PRIM_SUPP_CNTRY(O_error_message,
                                                             I_item,
                                                             I_location,
                                                             I_supplier,
                                                             I_origin_country_id,
                                                             'Y',
                                                             NULL) then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
     O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                           L_table,
                                           NULL,
                                           TO_CHAR(SQLCODE));
       return FALSE;
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                 SQLERRM,
                                 L_program,
                                 to_char(SQLCODE));
     return FALSE;
END UPD_ITEM_SUPP_COUNTRY_COST;
--------------------------------------------------------------------------------------------
FUNCTION UPD_ITEM_SUPP_COUNTRY_COST (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item                   IN       ITEM_COST_HEAD.ITEM%TYPE,
                                     I_supplier               IN       ITEM_COST_HEAD.SUPPLIER%TYPE,
                                     I_origin_country_id      IN       ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                                     I_location               IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                                     I_unit_cost              IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                     I_callingform            IN       VARCHAR2)
RETURN BOOLEAN is 
   L_program                VARCHAR2(64) := 'ITEM_COST_SQL.UPD_ITEM_SUPP_COUNTRY_COST';
   L_base_cost              ITEM_COST_HEAD.BASE_COST%TYPE := 0;
   L_negotiated_item_cost   ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE := 0;
   L_extended_base_cost     ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE := 0;
   L_inclusive_cost         ITEM_COST_HEAD.INCLUSIVE_COST%TYPE := 0;
   L_table                  VARCHAR2(30) := NULL;
   L_delivery_country_id    ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE;
   L_default_po_cost        COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE;
   L_child_item             ITEM_MASTER.ITEM%TYPE;
   L_pack_ind               ITEM_MASTER.PACK_IND%TYPE;
   L_iscl_item              ITEM_MASTER.ITEM%TYPE;
   L_null_cost_exist        VARCHAR2(1) := NULL;
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_exists                 VARCHAR2(1) := 'N';
   L_item_record            ITEM_MASTER%ROWTYPE;

  cursor C_LOCK_ITEM_SUPP_COUNTRY is
     select 'x'
       from item_supp_country
      where item = I_item
        and supplier = I_supplier
        and origin_country_id = I_origin_country_id
        for update nowait;

   cursor C_LOCK_ITEM_SUPP_COUNTRY_LOC is
     select 'x'
       from item_supp_country_loc
      where item = I_item
        and supplier = I_supplier
        and origin_country_id = I_origin_country_id
        and loc = I_location
        for update nowait;

   cursor C_GET_CHILD_ITEMS is
     select item
       from item_master
      where (item_parent = I_item or
            item_grandparent = I_item)
        and item_level <= tran_level;

   cursor C_CHECK_ISCL_NULL_COST is
     select 'x'
       from item_supp_country_loc
      where item = L_iscl_item
        and supplier = I_supplier
        and origin_country_id = I_origin_country_id
        and nvl(unit_cost,0) = 0
        and rownum = 1;

BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_supplier',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_origin_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_unit_cost is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_unit_cost',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_record,
                                      I_item) = FALSE then
       return FALSE;
   end if;

   if UPPER(I_callingform) = LP_ISC then

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ITEM_SUPP_COUNTRY',
                       'ITEM_SUPP_COUNTRY',
                       NULL);

      open C_LOCK_ITEM_SUPP_COUNTRY;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ITEM_SUPP_COUNTRY',
                       'ITEM_SUPP_COUNTRY',
                       NULL);

      close C_LOCK_ITEM_SUPP_COUNTRY;

      L_table := 'ITEM_SUPP_COUNTRY';

      update item_supp_country
         set unit_cost = I_unit_cost
       where item  = I_item
         and supplier =I_supplier
         and origin_country_id = I_origin_country_id;

      L_iscl_item := I_item;

      open C_CHECK_ISCL_NULL_COST;
      fetch C_CHECK_ISCL_NULL_COST into L_null_cost_exist;
      close C_CHECK_ISCL_NULL_COST;

      if L_null_cost_exist is NOT NULL then

         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_ITEM_SUPP_COUNTRY_LOC',
                          'ITEM_SUPP_COUNTRY_LOC',
                          NULL);

         open C_LOCK_ITEM_SUPP_COUNTRY_LOC;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_ITEM_SUPP_COUNTRY_LOC',
                          'ITEM_SUPP_COUNTRY_LOC',
                          NULL);

         close C_LOCK_ITEM_SUPP_COUNTRY_LOC;

         L_table := 'ITEM_SUPP_COUNTRY_LOC';

         update item_supp_country_loc
            set unit_cost = I_unit_cost
          where item  = I_item
            and supplier =I_supplier
            and origin_country_id = I_origin_country_id;
      end if;

      L_table := 'ITEM_SUPP_COUNTRY';

      for c_rec in C_GET_CHILD_ITEMS LOOP
         L_child_item := c_rec.item;

         update item_supp_country
            set unit_cost = I_unit_cost
          where item  = L_child_item
            and supplier =I_supplier
            and origin_country_id = I_origin_country_id;

         L_iscl_item := L_child_item;
         L_null_cost_exist := NULL;

         open C_CHECK_ISCL_NULL_COST;
         fetch C_CHECK_ISCL_NULL_COST into L_null_cost_exist;
         close C_CHECK_ISCL_NULL_COST;

         -- update item_supp_country_loc records for child items
         if L_null_cost_exist is NOT NULL then

            L_table := 'ITEM_SUPP_COUNTRY_LOC';

            update item_supp_country_loc
               set unit_cost = I_unit_cost
             where item  = L_child_item
               and supplier =I_supplier
               and origin_country_id = I_origin_country_id;

         end if;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
     O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                           L_table,
                                           NULL,
                                           TO_CHAR(SQLCODE));
       return FALSE;
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                 SQLERRM,
                                 L_program,
                                 to_char(SQLCODE));
     return FALSE;
END UPD_ITEM_SUPP_COUNTRY_COST;
---------------------------------------------------------------------------------------------
-- Function Name : QUERY_PROCEDURE
-- Purpose       : This function will query from item_cost_detail table based on values passed for
--                 item,supplier,origin country id and delivery country id
---------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (O_item_cost_detail_tbl     IN OUT ITEM_COST_DETAIL_TBL,
                           I_item                     IN     ITEM_COST_HEAD.ITEM%TYPE,
                           I_supplier                 IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                           I_origin_country_id        IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                           I_delivery_country_id      IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE,
                           I_loc                      IN     COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                           I_loc_type                 IN     COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE,
                           IO_base_cost               IN OUT ITEM_COST_HEAD.BASE_COST%TYPE,
                           IO_negotiated_item_cost    IN OUT ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE,
                           IO_extended_base_cost      IN OUT ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE,
                           IO_inclusive_cost          IN OUT ITEM_COST_HEAD.INCLUSIVE_COST%TYPE,
                           I_input_cost               IN     ITEM_COST_HEAD.BASE_COST%TYPE,
                           I_calling_form             IN     VARCHAR2,
                           I_item_cost_tax_incl_ind   IN     COUNTRY_ATTRIB.ITEM_COST_TAX_INCL_IND%TYPE)
IS

   L_program                      VARCHAR2(64) := 'ITEM_COST_SQL.QUERY_PROCEDURE';
   L_system_options_rec           SYSTEM_OPTIONS%ROWTYPE := NULL;
   L_error_message                RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_item_record                  ITEM_MASTER%ROWTYPE := NULL;
   L_loc_type                     VARCHAR2(2) := NULL;
   L_country_attrib_row           COUNTRY_ATTRIB%ROWTYPE := NULL;
   L_primary_supp_currency_code   SUPS.CURRENCY_CODE%TYPE := NULL;
   -- record and table for calling tax engine
   L_tax_calc_rec                 OBJ_TAX_CALC_REC := OBJ_TAX_CALC_REC();
   L_tax_calc_tbl                 OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   -- record and table for flattening results of the tax engine
   L_tax_info_rec                 OBJ_TAX_INFO_REC := OBJ_TAX_INFO_REC();
   L_tax_info_tbl                 OBJ_TAX_INFO_TBL := OBJ_TAX_INFO_TBL();
   L_total_tax_amount             ITEM_COST_HEAD.BASE_COST%TYPE := 0;
   L_total_recover_amount         ITEM_COST_HEAD.BASE_COST%TYPE := 0;
   L_tax_rate                     ITEM_COST_DETAIL.COMP_RATE%TYPE := 0;
   L_vdate                        PERIOD.VDATE%TYPE := GET_VDATE;

   cursor C_GET_ITEM_COST_DETAIL is
     select cond_type,
            cond_value,
            applied_on,
            modified_taxable_base,
            comp_rate,
            calc_basis,
            recoverable_amount,
            seq_num,
            error_message,
            return_code
       from (select 'BC' cond_type,
                    base_cost cond_value,
                    NULL applied_on,
                    NULL modified_taxable_base,
                    NULL comp_rate,
                    NULL calc_basis,
                    NULL recoverable_amount,
                    1 seq_num,
                    NULL error_message,
                    'TRUE' return_code
               from item_cost_head
              where item = I_item
                and supplier = I_supplier
                and origin_country_id =  I_origin_country_id
                and delivery_country_id = I_delivery_country_id
              union all
             select icd.cond_type cond_type,
                    icd.cond_value base_cost,
                    icd.applied_on applied_on,
                    icd.modified_taxable_base modified_taxable_base,
                    icd.comp_rate comp_rate,
                    NVL(icd.calculation_basis,'V') calc_basis,
                    icd.recoverable_amount recoverable_amount,
                    2 seq_num,
                    NULL error_message,
                    'TRUE' return_code
               from item_cost_detail icd,
                    vat_codes vc
              where icd.item = I_item
                and icd.supplier = I_supplier
                and icd.origin_country_id =  I_origin_country_id
                and icd.delivery_country_id = I_delivery_country_id
                and icd.cond_type = vc.vat_code
                and vc.incl_nic_ind = 'Y'
                and L_system_options_rec.default_tax_type = LP_GTAX
              union all
             select 'NIC' cond_type,
                    negotiated_item_cost cond_value,
                    NULL applied_on,
                    NULL modified_taxable_base,
                    NULL comp_rate,
                    NULL calc_basis,
                    NULL recoverable_amount,
                    3 seq_num,
                    NULL error_message,
                    'TRUE' return_code
               from item_cost_head
              where item = I_item
                and supplier = I_supplier
                and origin_country_id =  I_origin_country_id
                and delivery_country_id = I_delivery_country_id
              union all
             select icd.cond_type cond_type,
                    icd.cond_value base_cost,
                    icd.applied_on applied_on,
                    icd.modified_taxable_base modified_taxable_base,
                    icd.comp_rate comp_rate,
                    NVL(icd.calculation_basis,'V') calc_basis,
                    icd.recoverable_amount recoverable_amount,
                    4 seq_num,
                    NULL error_message,
                    'TRUE' return_code
               from item_cost_detail icd,
                    vat_codes vc
              where icd.item = I_item
                and icd.supplier = I_supplier
                and icd.origin_country_id =  I_origin_country_id
                and icd.delivery_country_id = I_delivery_country_id
                and icd.cond_type = vc.vat_code
                and vc.incl_nic_ind = 'N'
                and L_system_options_rec.default_tax_type = LP_GTAX
              union all
             select 'EBC' cond_type,
                    extended_base_cost cond_value,
                    NULL applied_on,
                    NULL modified_taxable_base,
                    NULL comp_rate,
                    NULL calc_basis,
                    NULL recoverable_amount,
                    5 seq_num,
                    NULL error_message,
                    'TRUE' return_code
               from item_cost_head
              where item = I_item
                and supplier = I_supplier
                and origin_country_id =  I_origin_country_id
                and delivery_country_id = I_delivery_country_id
              union all
             select 'IC' cond_type,
                    inclusive_cost cond_value,
                    NULL applied_on,
                    NULL modified_taxable_base,
                    NULL comp_rate,
                    NULL calc_basis,
                    NULL recoverable_amount,
                    6 seq_num,
                    NULL error_message,
                    'TRUE' return_code
               from item_cost_head
              where item = I_item
                and supplier = I_supplier
                and origin_country_id =  I_origin_country_id
                and delivery_country_id = I_delivery_country_id)
      order by seq_num;

   cursor C_GET_ITEM_COST_LOC_SVAT is
      select cond_type,
             cond_value,
             applied_on,
             modified_taxable_base,
             comp_rate,
             calc_basis,
             recoverable_amount,
             seq_num,
             error_message,
             return_code
        from (select 'BC' cond_type,
                      nvl(IO_base_cost,unit_cost) cond_value,
                      NULL applied_on,
                      NULL modified_taxable_base,
                      NULL comp_rate,
                      NULL calc_basis,
                      NULL recoverable_amount,
                      1 seq_num,
                      NULL error_message,
                      'TRUE' return_code
                 from item_supp_country_loc
                where item = I_item
                  and supplier = I_supplier
                  and origin_country_id =  I_origin_country_id
                  and loc = I_loc
                  and loc_type = I_loc_type
                union all
               select 'NIC' cond_type,
                      nvl(IO_negotiated_item_cost, negotiated_item_cost) cond_value,
                      NULL applied_on,
                      NULL modified_taxable_base,
                      NULL comp_rate,
                      NULL calc_basis,
                      NULL recoverable_amount,
                      3 seq_num,
                      NULL error_message,
                      'TRUE' return_code
                 from item_supp_country_loc
                where item = I_item
                  and supplier = I_supplier
                  and origin_country_id =  I_origin_country_id
                  and loc = I_loc
                  and loc_type = I_loc_type
                union all
               select 'EBC' cond_type,
                      nvl(IO_extended_base_cost,extended_base_cost) cond_value,
                      NULL applied_on,
                      NULL modified_taxable_base,
                      NULL comp_rate,
                      NULL calc_basis,
                      NULL recoverable_amount,
                      5 seq_num,
                      NULL error_message,
                      'TRUE' return_code
                 from item_supp_country_loc
                where item = I_item
                  and supplier = I_supplier
                  and origin_country_id =  I_origin_country_id
                  and loc = I_loc
                  and loc_type = I_loc_type
                union all
               select 'IC' cond_type,
                      nvl(IO_inclusive_cost,inclusive_cost) cond_value,
                      NULL applied_on,
                      NULL modified_taxable_base,
                      NULL comp_rate,
                      NULL calc_basis,
                      NULL recoverable_amount,
                      6 seq_num,
                      NULL error_message,
                      'TRUE' return_code
                 from item_supp_country_loc
                where item = I_item
                  and supplier = I_supplier
                  and origin_country_id =  I_origin_country_id
                  and loc = I_loc
                  and loc_type = I_loc_type)
                  order by seq_num;

   cursor C_GET_ITEM_COST_LOC_GTAX is
     select cond_type,
            cond_value,
            applied_on,
            modified_taxable_base,
            comp_rate,
            calc_basis,
            recoverable_amount,
            seq_num,
            error_message,
            return_code
      from (-- compute base cost
            select 'BC' cond_type,
                 -- if I_item_cost_tax_incl_ind = 'N' then I_input_cost is the BC (base cost)
                 -- if I_item_cost_tax_incl_ind = 'Y' then BC is computed as NIC less the tax amount with vat codes that are incl_nic_ind = 'Y'
                      (CASE I_item_cost_tax_incl_ind
                       WHEN 'N' then
                          NVL(I_input_cost, 0)
                       ELSE
                          NVL(I_input_cost, 0) - NVL(sum(NVL(ti.tax_amount, 0)), 0)
                       END) cond_value,
                 NULL applied_on,
                 NULL modified_taxable_base,
                 NULL comp_rate,
                 NULL calc_basis,
                 NULL recoverable_amount,
                 1 seq_num,
                 NULL error_message,
                 'TRUE' return_code
             from vat_codes vc,
                 TABLE(CAST(L_tax_info_tbl as OBJ_TAX_INFO_TBL)) ti
            where ti.item = I_item
              and ti.from_entity = I_supplier
              and ti.from_entity_type = LP_TAX_SUPPLIER_TYPE
              and ti.to_entity = I_loc
              and ti.to_entity_type = DECODE(I_loc_type,'S',LP_TAX_STORE_TYPE,'W',LP_TAX_WH_TYPE)
              and ti.tax_code = vc.vat_code
              and vc.incl_nic_ind = 'Y'
              and vc.vat_code = ti.tax_code(+)
            union all
            -- retrieve tax details with vat code that are incl_nic_ind = 'Y'
            select  ti.tax_code cond_type,
                    ti.tax_amount base_cost,
                    ti.taxable_base applied_on,
                    ti.modified_taxable_base modified_taxable_base,
                    ti.tax_rate comp_rate,
                    NVL(ti.calculation_basis,'V') calc_basis,
                    ti.recoverable_amount recoverable_amount,
                    2 seq_num,
                    NULL error_message,
                    'TRUE' return_code
             from vat_codes vc,
                 TABLE(CAST(L_tax_info_tbl as OBJ_TAX_INFO_TBL)) ti
            where ti.item = I_item
              and ti.from_entity = I_supplier
              and ti.from_entity_type = LP_TAX_SUPPLIER_TYPE
              and ti.to_entity = I_loc
              and ti.to_entity_type = DECODE(I_loc_type,'S',LP_TAX_STORE_TYPE,'W',LP_TAX_WH_TYPE)
              and ti.tax_code = vc.vat_code
              and vc.incl_nic_ind = 'Y'
              and L_system_options_rec.default_tax_type = LP_GTAX
              and vc.vat_code = ti.tax_code(+)
            union all
               -- compute base cost
               select 'NIC' cond_type,
                 -- if I_item_cost_tax_incl_ind = 'N' then NIC is BC(base cost) + tax amount whose vat codes are incl_nic_ind = 'Y'
                 -- if I_item_cost_tax_incl_ind = 'Y' then NIC is the input cost
                      (CASE I_item_cost_tax_incl_ind
                       WHEN 'N' then
                          NVL(I_input_cost, 0) + NVL(sum(NVL(ti.tax_amount, 0)), 0)
                       ELSE
                          NVL(I_input_cost, 0)
                       END) cond_value,
                 NULL applied_on,
                 NULL modified_taxable_base,
                 NULL comp_rate,
                 NULL calc_basis,
                 NULL recoverable_amount,
                 3 seq_num,
                 NULL error_message,
                 'TRUE' return_code
             from vat_codes vc,
                 TABLE(CAST(L_tax_info_tbl as OBJ_TAX_INFO_TBL)) ti
            where ti.item = I_item
              and ti.from_entity = I_supplier
              and ti.from_entity_type = LP_TAX_SUPPLIER_TYPE
              and ti.to_entity = I_loc
              and ti.to_entity_type = DECODE(I_loc_type,'S',LP_TAX_STORE_TYPE,'W',LP_TAX_WH_TYPE)
              and ti.tax_code = vc.vat_code
              and vc.incl_nic_ind = 'Y'
              and vc.vat_code = ti.tax_code(+)
            union all
            select gi.tax_code cond_type,
                 gi.tax_amount base_cost,
                 gi.taxable_base applied_on,
                 gi.modified_taxable_base modified_taxable_base,
                 gi.tax_rate comp_rate,
                 NVL(gi.calculation_basis,'V') calc_basis,
                 gi.recoverable_amount recoverable_amount,
                 4 seq_num,
                 NULL error_message,
                 'TRUE' return_code
             from vat_codes vc,
                 TABLE(CAST(L_tax_info_tbl as OBJ_TAX_INFO_TBL)) gi
            where gi.item = I_item
              and gi.from_entity = I_supplier
              and gi.from_entity_type = LP_TAX_SUPPLIER_TYPE
              and gi.to_entity = I_loc
              and gi.to_entity_type = DECODE(I_loc_type,'S',LP_TAX_STORE_TYPE,'W',LP_TAX_WH_TYPE)
              and gi.tax_code = vc.vat_code
              and vc.incl_nic_ind = 'N'
              and L_system_options_rec.default_tax_type = LP_GTAX
              and vc.vat_code = gi.tax_code(+)
                union all
               -- compute extended base cost
               select 'EBC' cond_type,
                 --- extended base cost is computed as base cost + the total tax amount less recoverable amount
                      (CASE I_item_cost_tax_incl_ind
                       WHEN 'N' then
                          NVL(I_input_cost, 0) + L_total_tax_amount - L_total_recover_amount
                       ELSE
                          NVL(I_input_cost, 0) - NVL(sum(NVL(ti.tax_amount, 0)), 0) + L_total_tax_amount - L_total_recover_amount
                       END) cond_value,
                 NULL applied_on,
                 NULL modified_taxable_base,
                 NULL comp_rate,
                 NULL calc_basis,
                 NULL recoverable_amount,
                 5 seq_num,
                 NULL error_message,
                 'TRUE' return_code
             from vat_codes vc,
                 TABLE(CAST(L_tax_info_tbl as OBJ_TAX_INFO_TBL)) ti
            where ti.item = I_item
              and ti.from_entity = I_supplier
              and ti.from_entity_type = LP_TAX_SUPPLIER_TYPE
              and ti.to_entity = I_loc
              and ti.to_entity_type = DECODE(I_loc_type,'S',LP_TAX_STORE_TYPE,'W',LP_TAX_WH_TYPE)
              and ti.tax_code = vc.vat_code
              and vc.incl_nic_ind = 'Y'
              and vc.vat_code = ti.tax_code(+)
                union all
               -- compute inclusive cost
               select 'IC' cond_type,
                 -- inclusive cost is computed as base cost + the total tax amount
                      (CASE I_item_cost_tax_incl_ind
                       WHEN 'N' then
                          NVL(I_input_cost, 0) + L_total_tax_amount
                       ELSE
                          NVL(I_input_cost, 0) - NVL(sum(NVL(ti.tax_amount, 0)), 0) + L_total_tax_amount
                       END) cond_value,
                 NULL applied_on,
                 NULL modified_taxable_base,
                 NULL comp_rate,
                 NULL calc_basis,
                 NULL recoverable_amount,
                 6 seq_num,
                 NULL error_message,
                 'TRUE' return_code
             from vat_codes vc,
                 TABLE(CAST(L_tax_info_tbl as OBJ_TAX_INFO_TBL)) ti
            where ti.item = I_item
              and ti.from_entity = I_supplier
              and ti.from_entity_type = LP_TAX_SUPPLIER_TYPE
              and ti.to_entity = I_loc
              and ti.to_entity_type = DECODE(I_loc_type,'S',LP_TAX_STORE_TYPE,'W',LP_TAX_WH_TYPE)
              and ti.tax_code = vc.vat_code
              and vc.incl_nic_ind = 'Y'
              and vc.vat_code = ti.tax_code(+))
         order by seq_num;

BEGIN

   if I_item is NULL then
     O_item_cost_detail_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                  'I_item',
                                                                   L_program,
                                                                   NULL);
     O_item_cost_detail_tbl(1).return_code := 'FALSE';
     return;
   end if;
   ---
   if I_supplier is NULL then
     O_item_cost_detail_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                  'I_supplier',
                                                                   L_program,
                                                                   NULL);
     O_item_cost_detail_tbl(1).return_code := 'FALSE';
     return;
   end if;
   ---
   if I_origin_country_id is NULL then
     O_item_cost_detail_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                  'I_origin_country_id',
                                                                   L_program,
                                                                   NULL);
     O_item_cost_detail_tbl(1).return_code := 'FALSE';
     return;
   end if;
   ---
   if I_delivery_country_id is NULL then
     O_item_cost_detail_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                  'I_delivery_country_id',
                                                                   L_program,
                                                                   NULL);
     O_item_cost_detail_tbl(1).return_code := 'FALSE';
     return;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            L_system_options_rec) = FALSE then
     O_item_cost_detail_tbl(1).error_message := L_error_message;
     O_item_cost_detail_tbl(1).return_code := 'FALSE';
     return;
   end if;

   if UPPER(I_calling_form) = LP_ISC then
     open C_GET_ITEM_COST_DETAIL;
     fetch C_GET_ITEM_COST_DETAIL BULK COLLECT INTO O_item_cost_detail_tbl;
     close C_GET_ITEM_COST_DETAIL;
     ---
   elsif UPPER(I_calling_form) = LP_ISCL and L_system_options_rec.default_tax_type = LP_SVAT then
      L_tax_calc_tbl.EXTEND;
      L_tax_calc_rec.I_item               := I_item;
      L_tax_calc_rec.I_from_entity        := I_supplier;
      L_tax_calc_rec.I_from_entity_type   := LP_TAX_SUPPLIER_TYPE;
      L_tax_calc_tbl(L_tax_calc_tbl.COUNT):= L_tax_calc_rec;

      if TAX_SQL.CALC_COST_TAX(L_error_message,
                               L_tax_calc_tbl) = FALSE then
         return;
      end if;

      L_tax_rate := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_cum_tax_pct;

      if L_country_attrib_row.item_cost_tax_incl_ind = 'N' or IO_base_cost is NOT NULL then
         IO_negotiated_item_cost := IO_base_cost;
         IO_extended_base_cost   := IO_base_cost;
         IO_inclusive_cost       := IO_base_cost;
      else
         IO_base_cost            := IO_negotiated_item_cost;
         IO_extended_base_cost   := IO_negotiated_item_cost;
         IO_inclusive_cost       := IO_negotiated_item_cost;
      end if;

      if IO_inclusive_cost is NULL then
         IO_inclusive_cost := IO_negotiated_item_cost;
      else
         IO_inclusive_cost := NVL(IO_inclusive_cost,0) + (NVL(IO_inclusive_cost,0) * L_tax_rate)/100;
      end if;
      ---
      open C_GET_ITEM_COST_LOC_SVAT;
      fetch C_GET_ITEM_COST_LOC_SVAT BULK COLLECT INTO O_item_cost_detail_tbl;
      close C_GET_ITEM_COST_LOC_SVAT;
      ---
   elsif UPPER(I_calling_form) = LP_ISCL and L_system_options_rec.default_tax_type = LP_GTAX then
     -- If the calling form is ITEMSUPPCTRYLOC, then the tax engine will be called
     -- to determine the location-specific tax detail values
     if I_loc_type = 'S' then
       L_loc_type := LP_TAX_STORE_TYPE;
     elsif I_loc_type = 'W' then
       L_loc_type := LP_TAX_WH_TYPE;
     elsif I_loc_type = 'E' then
       L_loc_type := LP_TAX_EXTFIN_TYPE;
     end if;
     ---
     -- Retrieve supplier currency
     if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(L_error_message,
                                          L_primary_supp_currency_code,
                                          I_supplier) = FALSE then
       O_item_cost_detail_tbl(1).error_message := L_error_message;
       O_item_cost_detail_tbl(1).return_code := 'FALSE';
       return;
     end if;

     -- Retrieve item_master row
     if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_error_message,
                                        L_item_record,
                                        I_item) = FALSE then
       O_item_cost_detail_tbl(1).error_message := L_error_message;
       O_item_cost_detail_tbl(1).return_code := 'FALSE';
       return;
     end if;

     -- Load L_tax_calc_tbl for tax engine call
     L_tax_calc_tbl.EXTEND;
     L_tax_calc_rec.I_item                := I_item;
     L_tax_calc_rec.I_pack_ind            := L_item_record.pack_ind;
     L_tax_calc_rec.I_from_entity         := I_supplier;
     L_tax_calc_rec.I_from_entity_type    := LP_TAX_SUPPLIER_TYPE;
     L_tax_calc_rec.I_to_entity           := I_loc;
     L_tax_calc_rec.I_to_entity_type      := L_loc_type;
     L_tax_calc_rec.I_amount              := I_input_cost;
     L_tax_calc_rec.I_amount_curr         := L_primary_supp_currency_code;
     L_tax_calc_rec.I_amount_tax_incl_ind := I_item_cost_tax_incl_ind;
     L_tax_calc_rec.I_origin_country_id   := I_origin_country_id;
     L_tax_calc_rec.I_effective_from_date := L_vdate;
     L_tax_calc_tbl(L_tax_calc_tbl.COUNT) := L_tax_calc_rec;

     -- Call the tax engine
     if TAX_SQL.CALC_COST_TAX(L_error_message,
                        L_tax_calc_tbl) = FALSE then
       O_item_cost_detail_tbl(1).error_message := L_error_message;
       O_item_cost_detail_tbl(1).return_code := 'FALSE';
       return;
     end if;

     -- Flatten the results of tax engine into L_tax_calc_tbl
     if L_tax_calc_tbl.COUNT > 0 then
       if L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl.COUNT > 0 then
         for i IN L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl.FIRST..L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl.LAST LOOP
            L_tax_info_rec.item                      := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).I_item;
            L_tax_info_rec.from_entity               := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).I_from_entity;
            L_tax_info_rec.from_entity_type          := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).I_from_entity_type;
            L_tax_info_rec.to_entity                 := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).I_to_entity;
            L_tax_info_rec.to_entity_type            := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).I_to_entity_type;
            ---
            L_tax_info_rec.amount                    := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl(i).taxable_base;
            L_tax_info_rec.tax_amount                := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl(i).estimated_tax_value;
            if  L_item_record.pack_ind != 'Y' or (L_item_record.pack_ind = 'Y' and L_item_record.simple_pack_ind = 'Y') then
               L_tax_info_rec.taxable_base           := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl(i).taxable_base;
               L_tax_info_rec.tax_rate               := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl(i).tax_rate;
               L_tax_info_rec.modified_taxable_base  := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl(i).modified_taxable_base;
            end if;

            L_tax_info_rec.tax_code                  := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl(i).tax_code;
            L_tax_info_rec.calculation_basis         := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl(i).calculation_basis;
            L_tax_info_rec.recoverable_amount        := L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl(i).recoverable_amount;
            --Sum values
            L_total_tax_amount                       := L_total_tax_amount + L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl(i).estimated_tax_value;
            L_total_recover_amount                   := L_total_recover_amount + L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_tax_detail_tbl(i).recoverable_amount;
               ---
            L_tax_info_tbl.extend();
            L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
         end LOOP;
       end if;
     end if;
     ---
     open C_GET_ITEM_COST_LOC_GTAX;
     fetch C_GET_ITEM_COST_LOC_GTAX BULK COLLECT INTO O_item_cost_detail_tbl;
     close C_GET_ITEM_COST_LOC_GTAX;
     ---
   end if;

   -- Update input/output paramaters with fetch values
   if O_item_cost_detail_tbl.COUNT > 0 then
     for i in O_item_cost_detail_tbl.FIRST..O_item_cost_detail_tbl.LAST LOOP
        if O_item_cost_detail_tbl(i).cond_type = 'BC' then
           IO_base_cost := O_item_cost_detail_tbl(i).cond_value;
        end if;
        ---
        if O_item_cost_detail_tbl(i).cond_type = 'NIC' then
           IO_negotiated_item_cost := O_item_cost_detail_tbl(i).cond_value;
        end if;
        ---
        if O_item_cost_detail_tbl(i).cond_type = 'EBC' then
           IO_extended_base_cost := O_item_cost_detail_tbl(i).cond_value;
        end if;
        ---
        if O_item_cost_detail_tbl(i).cond_type = 'IC' then
           IO_inclusive_cost := O_item_cost_detail_tbl(i).cond_value;
        end if;
     end LOOP;
   end if;

EXCEPTION
   when OTHERS then
     O_item_cost_detail_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                   SQLERRM,
                                                                   L_program,
                                                                   to_char(SQLCODE));
     O_item_cost_detail_tbl(1).return_code := 'FALSE';
     return;
END QUERY_PROCEDURE;
---------------------------------------------------------------------------------------------
-- Function Name : GET_ITEM_COST_HEAD
-- Purpose       : This function will return ITEM_COST_HEAD row for passed in item,supplier,
--                 origin country id and delivery country id
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_COST_HEAD (  O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_item_cost_head_row     IN OUT ITEM_COST_HEAD%ROWTYPE,
                               I_item                   IN     ITEM_COST_HEAD.ITEM%TYPE,
                               I_supplier               IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                               I_origin_country_id      IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                               I_delivery_country_id    IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'ITEM_COST_SQL.GET_ITEM_COST_HEAD';

   cursor C_GET_ITEM_COST_HEAD is
     select *
      from item_cost_head
      where item = I_item
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and delivery_country_id = I_delivery_country_id;
BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_item',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_supplier',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_origin_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_delivery_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_delivery_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                     'C_GET_ITEM_COST_HEAD',
                     'ITEM_COST_HEAD',
                     NULL);

   open C_GET_ITEM_COST_HEAD;

   SQL_LIB.SET_MARK('FETCH',
                     'C_GET_ITEM_COST_HEAD',
                     'ITEM_COST_HEAD',
                     NULL);

   fetch C_GET_ITEM_COST_HEAD INTO O_item_cost_head_row;

   SQL_LIB.SET_MARK('CLOSE',
                     'C_GET_ITEM_COST_HEAD',
                     'ITEM_COST_HEAD',
                     NULL);

   close C_GET_ITEM_COST_HEAD;

   return TRUE;
EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
END GET_ITEM_COST_HEAD;
---------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_PO_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_default_po_cost      IN OUT   COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE,
                             I_country_id           IN       COUNTRY_ATTRIB.COUNTRY_ID%TYPE,
                             I_loc                  IN       COUNTRY_ATTRIB.DEFAULT_LOC%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ITEM_COST_SQL.GET_DEFAULT_PO_COST';
   L_country_id         COUNTRY_ATTRIB.COUNTRY_ID%TYPE;

   cursor C_DEFAULT_PO_COST is
     select NVL(default_po_cost, 'BC') default_po_cost
      from country_attrib
      where country_id = L_country_id;

   cursor C_GET_PRIMARY_CTRY is
      select country_id
        from mv_loc_prim_addr mva
       where mva.loc = I_loc;
BEGIN

   if I_country_id is NULL then
     ---
     SQL_LIB.SET_MARK('OPEN','C_GET_PRIMARY_CTRY','MV_LOC_PRIM_ADDR','Loc: '||TO_CHAR(I_loc));
     open C_GET_PRIMARY_CTRY;
     SQL_LIB.SET_MARK('FETCH','C_GET_PRIMARY_CTRY','MV_LOC_PRIM_ADDR','Loc: '||TO_CHAR(I_loc));
     fetch C_GET_PRIMARY_CTRY into L_country_id;
     SQL_LIB.SET_MARK('OPEN','C_GET_PRIMARY_CTRY','MV_LOC_PRIM_ADDR','Loc: '||TO_CHAR(I_loc));
     close C_GET_PRIMARY_CTRY;
     ---
   else
     L_country_id := I_country_id;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                     'C_DEFAULT_PO_COST',
                     'country_attrib',
                     'Country id: '||L_country_id);
   open C_DEFAULT_PO_COST;
   SQL_LIB.SET_MARK('FETCH',
                     'C_DEFAULT_PO_COST',
                     'country_attrib',
                     'Country id: '||L_country_id);
   fetch C_DEFAULT_PO_COST into O_default_po_cost;
   SQL_LIB.SET_MARK('CLOSE',
                     'C_DEFAULT_PO_COST',
                     'country_attrib',
                     'Country id: '||L_country_id);

   close C_DEFAULT_PO_COST;

   if O_default_po_cost is NULL then
     O_default_po_cost := 'BC';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
     return FALSE;
END GET_DEFAULT_PO_COST;
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_COST (O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item                 IN     ITEM_COST_HEAD.ITEM%TYPE,
                           I_delivery_country_id  IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                           I_default_location     IN     COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                           I_default_loc_type     IN     COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64) := 'ITEM_COST_SQL.UPDATE_ITEM_COST';
   L_base_cost                ITEM_COST_HEAD.BASE_COST%TYPE;
   L_extended_base_cost       ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE;
   L_inclusive_cost           ITEM_COST_HEAD.INCLUSIVE_COST%TYPE;
   L_negotiated_item_cost     ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE;
   L_prev_origin_country_id   ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE;
   L_prev_supplier            ITEM_COST_HEAD.SUPPLIER%TYPE;
   L_prev_delivery_country_id ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE;
   L_pos_count                NUMBER := 0;
   L_exists                   BOOLEAN := FALSE;
   IO_obj_l10n_tax_rec        L10N_TAX_REC :=L10N_TAX_REC();
   --
   L_l10n_obj      L10N_OBJ := L10N_OBJ();

   cursor C_GET_ITEM_COST IS
     select LP_RECLASS_DOC_TYPE doc_type,
             ich.item,
             ich.supplier,
             ich.origin_country_id,
             I_default_location loc,
             I_default_loc_type loc_type,
             I_delivery_country_id delivery_country_id,
             ich.prim_dlvy_ctry_ind,
             (CASE ca.item_cost_tax_incl_ind
              WHEN 'Y' then ich.negotiated_item_cost
              ELSE  ich.base_cost
              END) unit_cost,
             ich.nic_static_ind,
             ich.base_cost,
             ich.negotiated_item_cost,
             ich.extended_base_cost,
             ich.inclusive_cost
      from item_cost_head ich,
          country_attrib ca
      where ich.item                = I_item
       and ich.delivery_country_id = ca.country_id
       and ca.country_id           = I_delivery_country_id;

   cursor C_GET_ITEM_SUPP_COUNTRY_LOC IS
      select LP_RECLASS_DOC_TYPE doc_type,
             isc.item,
             isc.supplier,
             isc.origin_country_id,
             isc.loc,
             isc.loc_type,
             im.dept,
             im.class,
             im.subclass,
             cl.class_vat_ind,
             isc.primary_loc_ind,
             ich.delivery_country_id,
             ich.nic_static_ind,
             (CASE ich.nic_static_ind
              WHEN 'Y' then ich.negotiated_item_cost
              ELSE  ich.base_cost
              END) unit_cost,
             ich.base_cost,
             ich.negotiated_item_cost,
             ich.extended_base_cost,
             ich.inclusive_cost
        from item_cost_head ich,
             item_supp_country_loc isc,
             item_master im,
             class cl,
             mv_l10n_entity ml
       where ich.item                = I_item
         and ich.delivery_country_id = I_delivery_country_id
         and isc.item                = ich.item
         and isc.supplier            = ich.supplier
         and isc.origin_country_id   = ich.origin_country_id
         and ml.entity_id            = TO_CHAR(isc.loc)
         and ml.entity               = 'LOC'
         and ml.country_id           = I_delivery_country_id
         and im.item                 = ich.item
         and cl.dept                 = im.dept
         and cl.class                = im.class
       order by 2,3;

   TYPE item_cost_type IS TABLE OF C_GET_ITEM_COST%ROWTYPE INDEX BY BINARY_INTEGER;
   item_cost_tbl item_cost_type;

   TYPE item_supp_loc_type IS TABLE OF C_GET_ITEM_SUPP_COUNTRY_LOC%ROWTYPE INDEX BY BINARY_INTEGER;
   item_supp_loc_tbl item_supp_loc_type;

BEGIN
   --
   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_item',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   --
   if I_delivery_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_delivery_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                     'C_GET_ITEM_COST',
                     'ITEM_COST_HEAD',
                      NULL);

   open C_GET_ITEM_COST;
   --
   SQL_LIB.SET_MARK('FETCH',
                     'C_GET_ITEM_COST',
                     'ITEM_COST_HEAD',
                     NULL);

   fetch C_GET_ITEM_COST bulk collect into item_cost_tbl;
   --
   SQL_LIB.SET_MARK('CLOSE',
                     'C_GET_ITEM_COST',
                     'ITEM_COST_HEAD',
                     NULL);

   close C_GET_ITEM_COST;
   --

   forall i in 1..item_cost_tbl.COUNT
   insert into l10n_doc_details_gtt( doc_type,
                                     item,
                                     supplier,
                                     location,
                                     loc_type,
                                     country_id,
                                     unit_cost)
                               values( item_cost_tbl(i).doc_type,
                                     item_cost_tbl(i).item,
                                     item_cost_tbl(i).supplier,
                                     item_cost_tbl(i).loc,
                                     item_cost_tbl(i).loc_type,
                                     item_cost_tbl(i).delivery_country_id,
                                     item_cost_tbl(i).unit_cost);

   FOR i in 1..item_cost_tbl.count LOOP
   --
     L_base_cost            := item_cost_tbl(i).base_cost;
     L_extended_base_cost   := item_cost_tbl(i).extended_base_cost;
     L_inclusive_cost       := item_cost_tbl(i).inclusive_cost;
     L_negotiated_item_cost := item_cost_tbl(i).negotiated_item_cost;

     if ITEM_COST_SQL.COMPUTE_ITEM_COST (O_error_message,
                                         L_base_cost,
                                         L_extended_base_cost,
                                         L_inclusive_cost,
                                         L_negotiated_item_cost,
                                         item_cost_tbl(i).item,
                                         item_cost_tbl(i).nic_static_ind,
                                         item_cost_tbl(i).supplier,
                                         item_cost_tbl(i).loc,
                                         item_cost_tbl(i).loc_type,
                                         LP_ISC,
                                         item_cost_tbl(i).origin_country_id,
                                         item_cost_tbl(i).delivery_country_id,
                                         item_cost_tbl(i).prim_dlvy_ctry_ind,
                                         'Y') = FALSE then
       return FALSE;
     end if;

   END LOOP;
   -- Clean Up the GTT
   delete from l10n_doc_details_gtt;
   --
   SQL_LIB.SET_MARK('OPEN',
                     'C_GET_ITEM_COST',
                     'ITEM_COST_HEAD',
                      NULL);
   --
   open C_GET_ITEM_SUPP_COUNTRY_LOC;
   --
   SQL_LIB.SET_MARK('FETCH',
                     'C_GET_ITEM_SUPP_COUNTRY_LOC',
                     'ITEM_COST_HEAD,COUNTRY_ATTRIB',
                     NULL);
   --
   fetch C_GET_ITEM_SUPP_COUNTRY_LOC bulk collect into item_supp_loc_tbl;
   --
   SQL_LIB.SET_MARK('CLOSE',
                     'C_GET_ITEM_SUPP_COUNTRY_LOC',
                     'ITEM_COST_HEAD,COUNTRY_ATTRIB',
                     NULL);

   close C_GET_ITEM_SUPP_COUNTRY_LOC;
   --

   forall i in 1..item_supp_loc_tbl.COUNT
   insert into l10n_doc_details_gtt( doc_type,
                                     item,
                                     supplier,
                                     location,
                                     loc_type,
                                     country_id,
                                     unit_cost)
                               values( item_supp_loc_tbl(i).doc_type,
                                     item_supp_loc_tbl(i).item,
                                     item_supp_loc_tbl(i).supplier,
                                     item_supp_loc_tbl(i).loc,
                                     item_supp_loc_tbl(i).loc_type,
                                     item_supp_loc_tbl(i).delivery_country_id,
                                     item_supp_loc_tbl(i).unit_cost);
   --
   if item_supp_loc_tbl.count > 0 then
     FOR i in item_supp_loc_tbl.first..item_supp_loc_tbl.last LOOP
       --
       L_base_cost            := item_supp_loc_tbl(i).base_cost;
       L_extended_base_cost   := item_supp_loc_tbl(i).extended_base_cost;
       L_inclusive_cost       := item_supp_loc_tbl(i).inclusive_cost;
       L_negotiated_item_cost := item_supp_loc_tbl(i).negotiated_item_cost;
       --
       if ITEM_COST_SQL.COMPUTE_ITEM_COST (O_error_message,
                                           L_base_cost,
                                           L_extended_base_cost,
                                           L_inclusive_cost,
                                           L_negotiated_item_cost,
                                           item_supp_loc_tbl(i).item,
                                           item_supp_loc_tbl(i).nic_static_ind,
                                           item_supp_loc_tbl(i).supplier,
                                           item_supp_loc_tbl(i).loc,
                                           item_supp_loc_tbl(i).loc_type,
                                           LP_ISCL,
                                           NULL,                                  -- origin_country_id
                                           NULL,                                  -- delivery_country_id
                                           NULL,                                  -- prim_dlvy_ctry_ind
                                           'N',                                   -- update_itemcost_ind
                                           'N',                                   -- update_itemcost_child_ind
                                           'N',                                   -- insert_item_supplier_ind
                                           NULL,                                  -- item_add_rec
                                           item_supp_loc_tbl(i).nic_static_ind    -- item_cost_tax_incl_ind
                                           ) = FALSE then
         return FALSE;
       end if;
       --
       if item_supp_loc_tbl(i).primary_loc_ind = 'Y' then
         --
         if ITEM_COST_SQL.UPD_ITEM_SUPP_COUNTRY_COST(O_error_message,
                                                      item_supp_loc_tbl(i).item,
                                                      item_supp_loc_tbl(i).supplier,
                                                      item_supp_loc_tbl(i).origin_country_id,
                                                      item_supp_loc_tbl(i).loc,
                                                      L_base_cost,
                                                      L_extended_base_cost,
                                                                      L_negotiated_item_cost,
                                                                      L_inclusive_cost,
                                                      LP_ISC) = FALSE then
            return FALSE;
         end if;
         --
       end if;
       --
       if ITEM_COST_SQL.UPD_ITEM_SUPP_COUNTRY_COST(O_error_message,
                                                    item_supp_loc_tbl(i).item,
                                                    item_supp_loc_tbl(i).supplier,
                                                    item_supp_loc_tbl(i).origin_country_id,
                                                    item_supp_loc_tbl(i).loc,
                                                    L_base_cost,
                                                    L_extended_base_cost,
                                                                   L_negotiated_item_cost,
                                                                   L_inclusive_cost,
                                                    LP_ISCL) = FALSE then
         return FALSE;
       end if;
       --
     END LOOP;
   else
     -- Update ITEM_SUPP_COUNTRY record when there is ITEM_SUPP_COUNTRY_LOC record
     FOR i in 1..item_cost_tbl.count LOOP
       --
       L_base_cost            := item_cost_tbl(i).base_cost;
       L_extended_base_cost   := item_cost_tbl(i).extended_base_cost;
       L_inclusive_cost       := item_cost_tbl(i).inclusive_cost;
       L_negotiated_item_cost := item_cost_tbl(i).negotiated_item_cost;
       --
       if item_cost_tbl(i).prim_dlvy_ctry_ind = 'Y' then
         --
         if ITEM_COST_SQL.UPD_ITEM_SUPP_COUNTRY_COST(O_error_message,
                                                      item_cost_tbl(i).item,
                                                      item_cost_tbl(i).supplier,
                                                      item_cost_tbl(i).origin_country_id,
                                                      item_cost_tbl(i).loc,
                                                      L_base_cost,
                                                      L_extended_base_cost,
                                                                      L_negotiated_item_cost,
                                                                      L_inclusive_cost,
                                                      LP_ISC) = FALSE then
            return FALSE;
         end if;
         --
       end if;
     END LOOP;
     --
   end if;
   -- Clean Up the GTT to load data for location retail tax calculation
   delete from l10n_doc_details_gtt;
   --
   SQL_LIB.SET_MARK('INSERT',
               'Before',
               'l10n_doc_details_gtt',
               NULL);

   insert into l10n_doc_details_gtt
        select NULL,                      --doc_id
               LP_RECLASS_DOC_TYPE,
               I_delivery_country_id,     --country_id
               NULL,                      --tsf_type
               il.item,
               NULL,                      --supplier
               il.loc,
               il.loc_type,
               NULL,                      --unit_cost
               NULL                       --Origin_country_id
          from item_loc il,
               mv_l10n_entity mle
         where il.item        = I_item
           and mle.entity     = 'LOC'
           and mle.entity_id  = il.loc
           and mle.country_id = I_delivery_country_id;

   -- Call LOAD_RECLASS to calculate retail tax for locations only if it is ranged to any location.
   if sql%rowcount > 0 then
      IO_obj_l10n_tax_rec.PROCEDURE_KEY      := 'LOAD_RECLASS';
      IO_obj_l10n_tax_rec.country_id         := I_delivery_country_id;
      IO_obj_l10n_tax_rec.doc_type           := LP_RECLASS_DOC_TYPE;
      IO_obj_l10n_tax_rec.item               := I_item;

      if L10N_SQL.EXEC_FUNCTION (O_error_message,
                          IO_obj_l10n_tax_rec)= FALSE then
        return FALSE;
      end if;
   end if;

   -- Clean Up the GTT
   delete from l10n_doc_details_gtt;
   --

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                 SQLERRM,
                                 L_program,
                                 to_char(SQLCODE));
     return FALSE;
END UPDATE_ITEM_COST;
---------------------------------------------------------------------------------------------
-- Function Name : ITEM_COST_EXIST
-- Purpose       : This function checks if any record exists in ITEM_COST_HEAD for the input item
--                and supplier.
---------------------------------------------------------------------------------------------
FUNCTION ITEM_COST_EXIST(O_error_message       IN OUT   VARCHAR2,
                         O_exists              IN OUT   BOOLEAN,
                         I_item                IN       ITEM_COST_HEAD.ITEM%TYPE,
                         I_supplier            IN       ITEM_COST_HEAD.SUPPLIER%TYPE)
 return BOOLEAN IS

   L_program          VARCHAR2(64) := 'ITEM_COST_SQL.ITEM_COST_EXIST';
   L_item_cost        VARCHAR2(1);

   cursor C_ITEM_COST is
    select 'x'
      from item_cost_head ich
     where ich.supplier = I_supplier
      and ich.item     = I_item
      and rownum = 1;

BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                          'I_item',
                                          'NULL',
                                          'NOT NULL');
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                          'I_supplier',
                                          'NULL',
                                          'NOT NULL');
     return FALSE;
   end if;

   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ITEM_COST','ITEM_COST_HEAD','Item: '||I_item||
               ' Supplier: '||to_char(I_supplier));
   open C_ITEM_COST;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_COST','ITEM_COST_HEAD','Item: '||I_item||
               ' Supplier: '||to_char(I_supplier));
   fetch C_ITEM_COST into L_item_cost;
     if C_ITEM_COST%FOUND then
       O_exists := TRUE;
     end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_COST','ITEM_COST_HEAD','Item: '||I_item||
               ' Supplier: '||to_char(I_supplier));
   close C_ITEM_COST;

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
     return FALSE;
END ITEM_COST_EXIST;
---------------------------------------------------------------------------------------------------
-- Function Name : BUYER_PACK_COST
-- Purpose       : This function checks if any record exists in ITEM_COST_HEAD for the input
--                 buyer pack item and supplier,if not creates one.
---------------------------------------------------------------------------------------------------
FUNCTION BUYER_PACK_COST(O_error_message       IN OUT   VARCHAR2,
                         I_item                IN       ITEM_COST_HEAD.ITEM%TYPE,
                         I_supplier            IN       ITEM_COST_HEAD.SUPPLIER%TYPE,
                         I_origin_country      IN       ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
 return BOOLEAN IS

   L_program                VARCHAR2(64) := 'ITEM_COST_SQL.BUYER_PACK_COST';
   L_exists                 VARCHAR2(1)  := NULL;
   L_country_attrib_row     COUNTRY_ATTRIB%ROWTYPE;
   L_system_options_row     SYSTEM_OPTIONS%ROWTYPE;


   CURSOR C_CHECK_ICH_ICD IS
       select 'x'
         from item_cost_head ich
        where ich.item = I_item
          and ich.supplier = I_supplier
          and ich.origin_country_id = I_origin_country
          and ich.delivery_country_id = L_system_options_row.base_country_id;

BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_supplier',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;

   if I_origin_country is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_origin_country',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
     return FALSE;
   end if;

   if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                              L_country_attrib_row,
                                              NULL,
                                              L_system_options_row.base_country_id) = FALSE then
     return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_ICH_ICD','ITEM_COST_HEAD','Item: '||I_item||
               ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country);
   open C_CHECK_ICH_ICD;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_ICH_ICD','ITEM_COST_HEAD','Item: '||I_item||
               ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country);
   fetch C_CHECK_ICH_ICD into L_exists;
     if C_CHECK_ICH_ICD%NOTFOUND then
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COST_HEAD','Item: '||I_item||
               ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country);
        insert into item_cost_head(item,
                                   supplier,
                                   origin_country_id,
                                   delivery_country_id,
                                   prim_dlvy_ctry_ind,
                                   nic_static_ind,
                                   base_cost,
                                   extended_base_cost,
                                   negotiated_item_cost,
                                   inclusive_cost)
                            values(I_item,
                                   I_supplier,
                                   I_origin_country,
                                   L_system_options_row.base_country_id,
                                   'Y',
                                   L_country_attrib_row.item_cost_tax_incl_ind,
                                   0,
                                   0,
                                   0,
                                   0);
     end if;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_ICH_ICD','ITEM_COST_HEAD','Item: '||I_item||
               ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country);
   close C_CHECK_ICH_ICD;

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
     return FALSE;
END BUYER_PACK_COST;
---------------------------------------------------------------------------------------------------
-- Function Name : DEF_ITEM_COST
-- Purpose       : This function checks if any record exists in ITEM_COST_HEAD for the input
--                 item and sync up ISC and corresponding ICH/ICD.
---------------------------------------------------------------------------------------------------
FUNCTION DEF_ITEM_COST(O_error_message               IN OUT   VARCHAR2,
                       I_item                        IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                       I_supplier                    IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                       I_unit_cost                   IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                       I_origin_country              IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       I_update_itemcost_child_ind   IN       VARCHAR2 DEFAULT 'N')
 RETURN BOOLEAN IS

   L_program                VARCHAR2(64) := 'ITEM_COST_SQL.DEF_ITEM_COST';
   L_base_cost              ITEM_SUPP_COUNTRY.BASE_COST%TYPE := NULL;
   L_extended_base_cost     ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST%TYPE := NULL;
   L_inclusive_cost         ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE := NULL;
   L_negotiated_item_cost   ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST%TYPE := NULL;
   L_country_attrib_row     COUNTRY_ATTRIB%ROWTYPE;
   L_system_options_row     SYSTEM_OPTIONS%ROWTYPE;


   CURSOR C_CHECK_ICH IS
       select delivery_country_id,
              prim_dlvy_ctry_ind
         from item_cost_head ich
        where ich.item = I_item
          and ich.supplier = I_supplier
          and ich.origin_country_id = I_origin_country
          and ich.delivery_country_id != L_system_options_row.base_country_id;

BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                          'I_item',
                                          'NULL',
                                          'NOT NULL');
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                          'I_supplier',
                                          'NULL',
                                          'NOT NULL');
     return FALSE;
   end if;

   if I_origin_country is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                          'I_origin_country',
                                          'NULL',
                                          'NOT NULL');
     return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
     return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_ICH','ITEM_COST_HEAD','Item: '||I_item||
               ' Supplier: '||to_char(I_supplier));
   FOR rec in C_CHECK_ICH LOOP
         if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                    L_country_attrib_row,
                                                    NULL,
                                                    rec.delivery_country_id) = FALSE then
            return FALSE;
         end if;

         if L_country_attrib_row.item_cost_tax_incl_ind = 'N' then
            L_base_cost := I_unit_cost;
         else
            L_negotiated_item_cost := I_unit_cost;
         end if;
         if ITEM_COST_SQL.COMPUTE_ITEM_COST(O_error_message,
                                            L_base_cost,
                                            L_extended_base_cost,
                                            L_inclusive_cost,
                                            L_negotiated_item_cost,
                                            I_item,
                                            L_country_attrib_row.item_cost_tax_incl_ind,
                                            I_supplier,
                                            L_country_attrib_row.default_loc,
                                            L_country_attrib_row.default_loc_type,
                                            'ITEMSUPPCTRY',    -- I_calling_form
                                            I_origin_country,
                                            rec.delivery_country_id,
                                            rec.prim_dlvy_ctry_ind,   --I_prim_dlvy_ctry_ind
                                            'Y',                      --I_update_itemcost_ind
                                            I_update_itemcost_child_ind) = FALSE then
           return FALSE;
           end if;
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
     return FALSE;
END DEF_ITEM_COST;
---------------------------------------------------------------------------------------------
-- Function Name : GET_COST_INFO_WRP
-- Purpose  : This is a wrapper function that returns a database
--            type object instead of a PLSQL type to be called from Java wrappers.
-------------------------------------------------------------------------------
FUNCTION GET_COST_INFO_WRP(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_item_cost_info_rec   IN OUT WRP_ITEM_COST_INFO_REC,
                           I_item                 IN     ITEM_COST_HEAD.ITEM%TYPE,
                           I_supplier             IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                           I_origin_country_id    IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'ITEM_COST_SQL.GET_COST_INFO_WRP';

   L_item_cost_info_rec  ITEM_COST_INFO_REC := NULL;

BEGIN

   if GET_COST_INFO(O_error_message,
                    L_item_cost_info_rec,
                    I_item,
                    I_supplier,
                    I_origin_country_id) = FALSE then
      return FALSE;
   end if;

   O_item_cost_info_rec := WRP_ITEM_COST_INFO_REC(L_item_cost_info_rec.item,
                                                  L_item_cost_info_rec.supplier,
                                                  L_item_cost_info_rec.origin_country_id,
                                                  L_item_cost_info_rec.delivery_country_id,
                                                  L_item_cost_info_rec.nic_static_ind,
                                                  L_item_cost_info_rec.base_cost,
                                                  L_item_cost_info_rec.negotiated_item_cost,
                                                  L_item_cost_info_rec.extended_base_cost,
                                                  L_item_cost_info_rec.inclusive_cost,
                                                  L_item_cost_info_rec.default_po_cost,
                                                  L_item_cost_info_rec.default_deal_cost,
                                                  L_item_cost_info_rec.default_cost_comp_cost);

   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
END GET_COST_INFO_WRP;
---------------------------------------------------------------------------------------------------
FUNCTION GET_COST_INFO_BOOL_WRP(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_item_cost_info_rec   IN OUT WRP_ITEM_COST_INFO_REC,
                                I_item                 IN     ITEM_COST_HEAD.ITEM%TYPE,
                                I_supplier             IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                                I_origin_country_id    IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(64) := 'ITEM_COST_SQL.GET_COST_INFO_BOOL_WRP';

   L_item_cost_info_rec  ITEM_COST_INFO_REC := NULL;

BEGIN
   
   if GET_COST_INFO(O_error_message,
                    L_item_cost_info_rec,
                    I_item,
                    I_supplier,
                    I_origin_country_id) = FALSE then
      return 0;
   end if;
   return 1;
EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return 0;
END GET_COST_INFO_BOOL_WRP;
---------------------------------------------------------------------------------------------------
FUNCTION COMPUTE_ITEM_COST_WRP(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_base_cost                   IN OUT ITEM_COST_HEAD.BASE_COST%TYPE,
                               O_extended_base_cost          IN OUT ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE,
                               O_inclusive_cost              IN OUT ITEM_COST_HEAD.INCLUSIVE_COST%TYPE,
                               O_negotiated_item_cost        IN OUT ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE,
                               I_item                        IN     ITEM_COST_HEAD.ITEM%TYPE,
                               I_nic_static_ind              IN     ITEM_COST_HEAD.NIC_STATIC_IND%TYPE,
                               I_supplier                    IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                               I_location                    IN     COUNTRY_ATTRIB.DEFAULT_LOC%TYPE,
                               I_loc_type                    IN     COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE,
                               I_calling_form                IN     VARCHAR2,
                               I_origin_country_id           IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                               I_delivery_country_id         IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE,
                               I_prim_dlvy_ctry_ind          IN     ITEM_COST_HEAD.PRIM_DLVY_CTRY_IND%TYPE,
                               I_update_itemcost_ind         IN     VARCHAR2,
                               I_update_itemcost_child_ind   IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)     := 'ITEM_COST_SQL.COMPUTE_ITEM_COST_WRP';
BEGIN
   if ITEM_COST_SQL.COMPUTE_ITEM_COST (O_error_message,            
                                       O_base_cost,                
                                       O_extended_base_cost,       
                                       O_inclusive_cost,           
                                       O_negotiated_item_cost,     
                                       I_item,                     
                                       I_nic_static_ind,
                                       I_supplier,                 
                                       I_location,                 
                                       I_loc_type,                 
                                       I_calling_form,             
                                       I_origin_country_id,        
                                       I_delivery_country_id,
                                       I_prim_dlvy_ctry_ind,
                                       I_update_itemcost_ind,
              		                   I_update_itemcost_child_ind,
                 		               'N',
				                       NULL,             
   			                           'Y') = FALSE then
    return FALSE;   			               
   end if;	  			               
   return true; 			               
EXCEPTION   			               
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
	 
END COMPUTE_ITEM_COST_WRP;
---------------------------------------------------------------------------------------------
END ITEM_COST_SQL;
/