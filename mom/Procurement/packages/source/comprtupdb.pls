CREATE OR REPLACE PACKAGE BODY COMP_RT_UPD_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION IS_UNIQUE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists            IN OUT BOOLEAN ,
                   I_defaulting_level  IN     COST_COMP_UPD_STG.DEFAULTING_LEVEL%TYPE,
                   I_effective_date    IN     COST_COMP_UPD_STG.EFFECTIVE_DATE%TYPE,
                   I_comp_id           IN     COST_COMP_UPD_STG.COMP_ID%TYPE,
                   I_dept              IN     COST_COMP_UPD_STG.DEPT%TYPE,
                   I_from_loc          IN     COST_COMP_UPD_STG.FROM_LOC%TYPE,
                   I_to_loc            IN     COST_COMP_UPD_STG.TO_LOC%TYPE,
                   I_exp_prof_key      IN     COST_COMP_UPD_STG.EXP_PROF_KEY%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64)  := 'COMP_RT_UPD_SQL.IS_UNIQUE';
   L_dup_exists   VARCHAR2(1)   := NULL;

   cursor C_CHECK_DUP is
      select 'x'
        from cost_comp_upd_stg
       where comp_id        = I_comp_id
         and effective_date = I_effective_date
         and defaulting_level = I_defaulting_level
         and (dept          = I_dept         or I_dept         is null)
         and (exp_prof_key  = I_exp_prof_key or I_exp_prof_key is null)
         and (from_loc      = I_from_loc     or I_from_loc     is null)
         and (to_loc        = I_to_loc       or I_to_loc       is null);
BEGIN
   O_exists := FALSE;
   ---
   if I_defaulting_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_defaulting_level',
                                            'NULL',
                                            'NOT NULL');
   end if;
   ---
   if I_comp_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_comp_id',
                                            'NULL',
                                            'NOT NULL');
   end if;
   ---
   if I_effective_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_effective_date',
                                            'NULL',
                                            'NOT NULL');
   end if;
   ---
   if I_defaulting_level = 'D' then
      if I_dept is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_dept',
                                               'NULL',
                                               'NOT NULL');
      end if;
      ---
      if I_from_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_from_loc',
                                               'NULL',
                                               'NOT NULL');
      end if;
      ---
      if I_to_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_to_loc',
                                               'NULL',
                                               'NOT NULL');
      end if;
   elsif I_defaulting_level in ('S','P','C') then
      if I_exp_prof_key is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_exp_prof_key',
                                               'NULL',
                                               'NOT NULL');
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_DUP',
                    'COST_COMP_UPD_STG',
                    'Comp ID: ' || to_char(I_comp_id)||' , Eff Date - '|| I_effective_date);
   open C_CHECK_DUP;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_DUP',
                    'COST_COMP_UPD_STG',
                    'Comp ID: ' || to_char(I_comp_id)||' , Eff Date - '|| I_effective_date);
   fetch C_CHECK_DUP into L_dup_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_DUP',
                    'COST_COMP_UPD_STG',
                    'Comp ID: ' || to_char(I_comp_id)||' , Eff Date - '|| I_effective_date);
   close C_CHECK_DUP;
   ---
   if L_dup_exists = 'x' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END IS_UNIQUE;
---------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQUENCE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_seq_no            IN OUT COST_COMP_UPD_STG.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)  := 'COMP_RT_UPD_SQL.GET_NEXT_SEQUENCE';
   L_exists               VARCHAR2(1) := NULL;
   L_first_time           BOOLEAN := TRUE;
   L_wrap_sequence_number COST_COMP_UPD_STG.SEQ_NO%TYPE := NULL;
   ---
   cursor C_NEXTVAL is
      select cost_comp_upd_stg_sequence.NEXTVAL
        from dual;
   ---
   cursor C_SEQ_EXISTS is
      select 'x'
        from cost_comp_upd_stg
       where seq_no = O_seq_no;
   ---
BEGIN
   ---
   LOOP
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_NEXTVAL',
                       'cost_comp_upd_stg_sequence',
                       NULL);
      open C_NEXTVAL;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_NEXTVAL',
                       'cost_comp_upd_stg_sequence',
                       NULL);
      fetch C_NEXTVAL into O_seq_no;

      --- handling if sequence runs out of numbers, notify user to contact DBA
      if L_first_time then
         L_wrap_sequence_number := O_seq_no;
         L_first_time           := FALSE;
      elsif (O_seq_no = L_wrap_sequence_number) THEN
         O_error_message := SQL_LIB.CREATE_MSG('INV_MGMT_SEQ_MAXED',
                                               NULL,
                                               NULL,
                                               NULL);
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_NEXTVAL',
                          'cost_comp_upd_stg_sequence',
                          NULL);

         close C_NEXTVAL;
         ---
         return FALSE;
      end if;
      ---
      /* does newly fetched sequence number exist on the COST_COMP_UPD_STG table? */
      SQL_LIB.SET_MARK('OPEN',
                       'C_SEQ_EXISTS',
                       'COST_COMP_UPD_STG',
                       NULL);
      open C_SEQ_EXISTS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_SEQ_EXISTS',
                       'COST_COMP_UPD_STG',
                       NULL);
      fetch C_SEQ_EXISTS into L_exists;
      ---
      if (C_SEQ_EXISTS%NOTFOUND) then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_SEQ_EXISTS',
                          'COST_COMP_UPD_STG',
                          NULL);
         close C_SEQ_EXISTS;
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_NEXTVAL',
                          'cost_comp_upd_stg_sequence',
                          NULL);
         close C_NEXTVAL;
         ---
         EXIT;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SEQ_EXISTS',
                       'COST_COMP_UPD_STG',
                       NULL);
      close C_SEQ_EXISTS;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_NEXTVAL',
                       'cost_comp_upd_stg_sequence',
                       NULL);
      close C_NEXTVAL;
      ---
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_SEQUENCE;
---------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_VALUES(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists            IN OUT BOOLEAN,
                            O_comp_rate         IN OUT COST_COMP_UPD_STG.NEW_COMP_RATE%TYPE,
                            O_comp_currency     IN OUT ELC_COMP.COMP_CURRENCY%TYPE,
                            O_per_count         IN OUT ELC_COMP.PER_COUNT%TYPE,
                            O_per_count_uom     IN OUT ELC_COMP.PER_COUNT_UOM%TYPE,
                            I_defaulting_level  IN     COST_COMP_UPD_STG.DEFAULTING_LEVEL%TYPE,
                            I_comp_id           IN     COST_COMP_UPD_STG.COMP_ID%TYPE,
                            I_dept              IN     COST_COMP_UPD_STG.DEPT%TYPE,
                            I_from_loc          IN     COST_COMP_UPD_STG.FROM_LOC%TYPE,
                            I_to_loc            IN     COST_COMP_UPD_STG.TO_LOC%TYPE,
                            I_exp_prof_key      IN     COST_COMP_UPD_STG.EXP_PROF_KEY%TYPE,
                            I_effective_date    IN     COST_COMP_UPD_STG.EFFECTIVE_DATE%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)  := 'COMP_RT_UPD_SQL.GET_DEFAULT_VALUES';

   cursor C_GET_DEFAULT is
      select new_comp_rate,
             new_comp_currency,
             new_per_count,
             new_per_count_uom
        from cost_comp_upd_stg
       where comp_id          =  I_comp_id
         and defaulting_level = I_defaulting_level
         and effective_date   < I_effective_date
         and (dept            = I_dept         or I_dept         is null)
         and (exp_prof_key    = I_exp_prof_key or I_exp_prof_key is null)
         and (from_loc        = I_from_loc     or I_from_loc     is null)
         and (to_loc          = I_to_loc       or I_to_loc       is null)
       order by effective_date DESC;

BEGIN
   O_exists := FALSE;
   ---
   if I_defaulting_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_defaulting_level',
                                            'NULL',
                                            'NOT NULL');
   end if;
   ---
   if I_comp_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_comp_id',
                                            'NULL',
                                            'NOT NULL');
   end if;
   ---
   if I_effective_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_effective_date',
                                            'NULL',
                                            'NOT NULL');
   end if;
   ---
   if I_defaulting_level = 'D' then
      if I_dept is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_dept',
                                               'NULL',
                                               'NOT NULL');
      end if;
      ---
      if I_from_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_from_loc',
                                               'NULL',
                                               'NOT NULL');
      end if;
      ---
      if I_to_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_to_loc',
                                               'NULL',
                                               'NOT NULL');
      end if;
   elsif I_defaulting_level in ('S','P','C') then
      if I_exp_prof_key is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_exp_prof_key',
                                               'NULL',
                                               'NOT NULL');
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_DEFAULT',
                    'COST_COMP_UPD_STG',
                    'Comp ID: ' || to_char(I_comp_id)||' , Eff Date - '|| I_effective_date);
   open C_GET_DEFAULT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_DEFAULT',
                    'COST_COMP_UPD_STG',
                    'Comp ID: ' || to_char(I_comp_id)||' , Eff Date - '|| I_effective_date);
   fetch C_GET_DEFAULT into O_comp_rate,
                            O_comp_currency,
                            O_per_count,
                            O_per_count_uom;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_DEFAULT',
                    'COST_COMP_UPD_STG',
                    'Comp ID: ' || to_char(I_comp_id)||' , Eff Date - '|| I_effective_date);
   close C_GET_DEFAULT;
   ---
   if O_comp_currency is NOT NULL then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEFAULT_VALUES;
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_OLD_VALUES(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_comp_rate         IN     COST_COMP_UPD_STG.NEW_COMP_RATE%TYPE,
                           I_comp_currency     IN     ELC_COMP.COMP_CURRENCY%TYPE,
                           I_per_count         IN     ELC_COMP.PER_COUNT%TYPE,
                           I_per_count_uom     IN     ELC_COMP.PER_COUNT_UOM%TYPE,
                           I_defaulting_level  IN     COST_COMP_UPD_STG.DEFAULTING_LEVEL%TYPE,
                           I_comp_id           IN     COST_COMP_UPD_STG.COMP_ID%TYPE,
                           I_dept              IN     COST_COMP_UPD_STG.DEPT%TYPE,
                           I_from_loc          IN     COST_COMP_UPD_STG.FROM_LOC%TYPE,
                           I_to_loc            IN     COST_COMP_UPD_STG.TO_LOC%TYPE,
                           I_exp_prof_key      IN     COST_COMP_UPD_STG.EXP_PROF_KEY%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64)  := 'COMP_RT_UPD_SQL.UPDATE_OLD_VALUES';
   ---
   L_comp_rate         COST_COMP_UPD_STG.NEW_COMP_RATE%TYPE := I_comp_rate;
   L_comp_currency     ELC_COMP.COMP_CURRENCY%TYPE          := I_comp_currency;
   L_per_count         ELC_COMP.PER_COUNT%TYPE              := I_per_count;
   L_per_count_uom     ELC_COMP.PER_COUNT_UOM%TYPE          := I_per_count_uom;
   L_rowid             ROWID;

   cursor C_GET_COMP_DETAILS is
      select new_comp_rate,
             new_comp_currency,
             new_per_count,
             new_per_count_uom,
             rowid row_id
        from cost_comp_upd_stg
       where comp_id          = I_comp_id
         and defaulting_level = I_defaulting_level
         and (dept            = I_dept         or I_dept         is null)
         and (exp_prof_key    = I_exp_prof_key or I_exp_prof_key is null)
         and (from_loc        = I_from_loc     or I_from_loc     is null)
         and (to_loc          = I_to_loc       or I_to_loc       is null)
       order by effective_date;

BEGIN
   if I_defaulting_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_defaulting_level',
                                            'NULL',
                                            'NOT NULL');
   end if;
   ---
   if I_comp_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_comp_id',
                                            'NULL',
                                            'NOT NULL');
   end if;
   ---
   if I_defaulting_level = 'D' then
      if I_dept is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_dept',
                                               'NULL',
                                               'NOT NULL');
      end if;
      ---
      if I_from_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_from_loc',
                                               'NULL',
                                               'NOT NULL');
      end if;
      ---
      if I_to_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_to_loc',
                                               'NULL',
                                               'NOT NULL');
      end if;
   elsif I_defaulting_level in ('S','P','C') then
      if I_exp_prof_key is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_exp_prof_key',
                                               'NULL',
                                               'NOT NULL');
      end if;
   end if;
   ---
   for rec in C_GET_COMP_DETAILS
   loop
      ---
      update cost_comp_upd_stg
         set old_comp_rate     = L_comp_rate,
             old_comp_currency = L_comp_currency,
             old_per_count     = L_per_count,
             old_per_count_uom = L_per_count_uom
       where rowid = rec.row_id;
      ---
      L_comp_rate     := rec.new_comp_rate;
      L_comp_currency := rec.new_comp_currency;
      L_per_count     := rec.new_per_count;
      L_per_count_uom := rec.new_per_count_uom;
   end loop;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_OLD_VALUES;
---------------------------------------------------------------------------------------------
END;
/
