CREATE OR REPLACE PACKAGE CONTRACT_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------
-- Name:       SUBMIT_CONTRACT
-- Purpose:    This function will execute the appropriate checks necessary
--             before submitting a contract
-- CreateD By: Dave Dederichs, 23-OCT-97
--------------------------------------------------------------------------
FUNCTION SUBMIT_CONTRACT(O_error_message  IN OUT VARCHAR2,
                         I_contract_type  IN     CONTRACT_HEADER.CONTRACT_TYPE%TYPE,
                         I_contract_no    IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Name:    CALC_CONTRACT_TOTALS
-- Purpose: This function consolidates the sku/date level commitment on
--            the contract_prod_plan table to the contract_sku table.
--            It then consolidates the sku level commitment on the
--            contract_sku table to the contract_header table for total_cost
--            and total_retail.
--            If the function is called passing in rollup mode = 'H' then
--            only the rollup to contract_header is done.
-- Created By: Phil Tenney, 19-AUG-96.
---------------------------------------------------------------------------
FUNCTION CALC_CONTRACT_TOTALS (O_error_message    IN OUT   VARCHAR2,
                               I_contract_no      IN       CONTRACT_HEADER.CONTRACT_NO%TYPE,
                               O_outstand_cost    IN OUT   CONTRACT_HEADER.OUTSTAND_COST%TYPE,
                               O_total_cost       IN OUT   CONTRACT_HEADER.TOTAL_COST%TYPE,
                               O_total_retail     IN OUT   ORDLOC.UNIT_RETAIL%TYPE)
return BOOLEAN;

---------------------------------------------------------------------------
-- Name:       GET_UNIT_COST
-- Purpose:    This function will fetch the unit cost for a SKU, style, or
--             option(style/color or style,size).  It will be called from
--             replenishment and  the online dialogue for creating manual
--             orders from contracts.
-- Created By: Aaron Morhman, 15-OCT-96
-------------------------------------------------------------------------------

FUNCTION GET_UNIT_COST(O_ERROR_MESSAGE IN OUT VARCHAR2,
                       I_ITEM          IN     CONTRACT_COST.ITEM%TYPE,
                       I_CONTRACT_NO   IN     CONTRACT_COST.CONTRACT_NO%TYPE,
                       O_COST          IN OUT CONTRACT_COST.UNIT_COST%TYPE)
return BOOLEAN;

FUNCTION GET_UNIT_COST(O_ERROR_MESSAGE    IN OUT VARCHAR2,
                       I_ITEM             IN     CONTRACT_COST.ITEM%TYPE,
                       I_CONTRACT_NO      IN     CONTRACT_COST.CONTRACT_NO%TYPE,
                       O_COST             IN OUT CONTRACT_COST.UNIT_COST%TYPE,
                       I_DIFF_1           IN     CONTRACT_COST.DIFF_1%TYPE,
                       I_DIFF_2           IN     CONTRACT_COST.DIFF_2%TYPE,
                       I_DIFF_3           IN     CONTRACT_COST.DIFF_3%TYPE,
                       I_DIFF_4           IN     CONTRACT_COST.DIFF_4%TYPE,
                       I_ITEM_PARENT      IN     CONTRACT_COST.ITEM_PARENT%TYPE,
                       I_ITEM_GRANDPARENT IN     CONTRACT_COST.ITEM_GRANDPARENT%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       LOCK_CONTRACT
-- Purpose:    This function will lock the contract_header record of the
--             contract being modified.  It will be called from all contracting
--             forms.
-- Created By: Aaron Morhman, 15-OCT-96
-------------------------------------------------------------------------------

FUNCTION LOCK_CONTRACT(O_error_message IN OUT VARCHAR2,
                       I_contract_no   IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------
-- Name:       GET_RETAIL_EXCL_VAT
-- Purpose:    This function will calculate the total retail excluding VAT for an
--             A- or B-type contract. This function calculates the total_retail_excl_vat
--             using the default vat on the system options table, not per location.
-- Created BY: Natasha Gilloth, 20-OCT-97
----------------------------------------------------------------------------------------
FUNCTION GET_RETAIL_EXCL_VAT(O_error_message            IN OUT  VARCHAR2,
                             O_total_retail_excl_vat    IN OUT  ORDLOC.UNIT_RETAIL%TYPE,
                             I_contract_no              IN      CONTRACT_HEADER.CONTRACT_NO%TYPE)
return BOOLEAN;

-----------------------------------------------------------------------------------------
-- Name:       POP_COST_HIST
-- Purpose:    This function will write records to the contract_cost_hist table when
--             orders raised from contracts are approved.
-- Created By: Natasha Gilloth, 20-OCT-97
-----------------------------------------------------------------------------------------
FUNCTION POP_COST_HIST(O_error_message  IN OUT  VARCHAR2,
                       I_contract_no    IN      CONTRACT_HEADER.CONTRACT_NO%TYPE,
                       I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
return BOOLEAN;
------------------------------------------------------------------------------------
FUNCTION GET_CONTRACT_TYPE (O_error_message   IN OUT VARCHAR2,
                            O_contract_type   IN OUT CONTRACT_HEADER.CONTRACT_TYPE%TYPE,
                            I_contract_no     IN CONTRACT_HEADER.CONTRACT_NO%TYPE)
return BOOLEAN;
------------------------------------------------------------------------------------
-- Name:       ITEM_ON_CONTRACT
-- Created:    10-JAN-00
-- Purpose:    Checks to see if an entered sku exists on the entered contract. It also
--             checks to see if the entered sku is part of a soft contract.
-- Created by: Andrew Cariveau
------------------------------------------------------------------------------------
FUNCTION ITEM_ON_CONTRACT (O_error_message   IN OUT VARCHAR2,
                           O_exists          IN OUT BOOLEAN,
                           O_pack_item_ind   IN OUT VARCHAR2,
                           I_contract_no     IN     CONTRACT_DETAIL.CONTRACT_NO%TYPE,
                           I_item            IN     CONTRACT_DETAIL.ITEM%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------
FUNCTION GET_CURRENCY_CODE (O_error_message   IN OUT VARCHAR2,
                            O_currency_code   IN OUT CONTRACT_HEADER.CURRENCY_CODE%TYPE,
                            I_contract_no     IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_supplier        IN OUT CONTRACT_HEADER.SUPPLIER%TYPE,
                      I_contract_no     IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_valid          IN OUT  BOOLEAN,
                  O_division       IN OUT  DIVISION.DIVISION%TYPE,
                  O_div_name       IN OUT  DIVISION.DIV_NAME%TYPE,
                  O_group_no       IN OUT  GROUPS.GROUP_NO%TYPE,
                  O_group_name     IN OUT  GROUPS.GROUP_NAME%TYPE,
                  O_dept           IN OUT  DEPS.DEPT%TYPE,
                  O_dept_name      IN OUT  DEPS.DEPT_NAME%TYPE,
                  O_supplier       IN OUT  SUPS.SUPPLIER%TYPE,
                  O_sup_name       IN OUT  SUPS.SUP_NAME%TYPE,
                  O_status         IN OUT  CONTRACT_HEADER.STATUS%TYPE,
                  I_contract_no    IN      CONTRACT_HEADER.CONTRACT_NO%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION GET_CONTRACT_HEADER_ROW(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_contract_header_row     IN OUT CONTRACT_HEADER%ROWTYPE,
                                 I_contract_no             IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_UNIT_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_contract_no     IN       CONTRACT_HEADER.CONTRACT_NO%TYPE,
                          I_item_parent     IN       CONTRACT_COST.ITEM_PARENT%TYPE,
                          I_diff            IN       CONTRACT_COST.DIFF_1%TYPE,
                          I_vpn             IN       ITEM_SUPPLIER.VPN%TYPE,
                          I_unit_cost       IN       CONTRACT_COST.UNIT_COST%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------
END;
/
