CREATE OR REPLACE PACKAGE BODY ORDREV_SQL AS
--------------------------------------------------------------------------
FUNCTION GET_OLD_NEW_DATES(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no         IN       ORDHEAD.ORDER_NO%TYPE,
                           I_rev_no           IN       ORDHEAD_REV.REV_NO%TYPE,
                           I_rev_type         IN       ORDLOC_REV.ORIGIN_TYPE%TYPE,
                           O_old_not_before   IN OUT   ORDHEAD.NOT_BEFORE_DATE%TYPE,
                           O_old_not_after    IN OUT   ORDHEAD.NOT_AFTER_DATE%TYPE,
                           O_old_pickup       IN OUT   ORDHEAD.PICKUP_DATE%TYPE,
                           O_new_not_before   IN OUT   ORDHEAD.NOT_BEFORE_DATE%TYPE,
                           O_new_not_after    IN OUT   ORDHEAD.NOT_AFTER_DATE%TYPE,
                           O_new_pickup       IN OUT   ORDHEAD.PICKUP_DATE%TYPE)
   RETURN BOOLEAN IS

   cursor C_DATES_NEW is
   select not_before_date,
          not_after_date,
          pickup_date
     from ordhead
    where order_no = I_order_no;

   cursor C_DATES_OLD is
   select not_before_date,
          not_after_date,
          pickup_date
     from ordhead_rev
    where order_no = I_order_no
      and rev_no = I_rev_no
      and origin_type = I_rev_type;

BEGIN
   O_new_not_before := NULL;
   O_new_not_after  := NULL;
   O_new_pickup     := NULL;

   O_old_not_before := NULL;
   O_old_not_after  := NULL;
   O_old_pickup     := NULL;

   SQL_LIB.SET_MARK('OPEN',
                    'C_DATES_NEW',
                    'ordhead',
                    'order no: '||TO_CHAR(I_order_no));
   open C_DATES_NEW;

   SQL_LIB.SET_MARK('FETCH',
                    'C_DATES_NEW',
                    'ordhead',
                    'order no: '||TO_CHAR(I_order_no));
   fetch C_DATES_NEW into O_new_not_before,
                          O_new_not_after,
                          O_new_pickup;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_DATES_NEW',
                    'ordhead',
                    'order no: '||TO_CHAR(I_order_no));
   close C_DATES_NEW;

   SQL_LIB.SET_MARK('OPEN',
                    'C_DATES_OLD',
                    'ordhead_rev',
                    'order no: '||TO_CHAR(I_order_no));
   open C_DATES_OLD;

   SQL_LIB.SET_MARK('FETCH',
                    'C_DATES_OLD',
                    'ordhead_rev',
                    'order no: '||TO_CHAR(I_order_no));
   fetch C_DATES_OLD into O_old_not_before,
                          O_old_not_after,
                          O_old_pickup;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_DATES_OLD',
                    'ordhead_rev',
                    'order no: '||TO_CHAR(I_order_no));
   close C_DATES_OLD;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDREV_SQL.GET_OLD_NEW_DATES',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_OLD_NEW_DATES;
--------------------------------------------------------------------------
FUNCTION QTY_CHANGE(O_error_message IN OUT VARCHAR2,
                    I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
                    I_rev_no        IN     ORDLOC_REV.REV_NO%TYPE,
                    I_rev_type      IN     ORDLOC_REV.ORIGIN_TYPE%TYPE,
                    I_item          IN     ORDLOC.ITEM%TYPE,
                    I_location      IN     ORDLOC.LOCATION%TYPE,
                    I_loc_type      IN     ORDLOC.LOC_TYPE%TYPE,
                    I_old_qty       IN     ORDLOC.QTY_RECEIVED%TYPE,
                    I_new_qty       IN     ORDLOC.QTY_RECEIVED%TYPE,
                    I_cancel_id     IN     ORDLOC.CANCEL_ID%TYPE)
                    RETURN BOOLEAN IS

   L_qty                ORDLOC.QTY_ORDERED%TYPE             := 0;
   L_alloc_no           ALLOC_HEADER.ALLOC_NO%TYPE          := NULL;
   L_received           SHIPSKU.QTY_RECEIVED%TYPE           := 0;
   L_location           ORDLOC.LOCATION%TYPE                := NULL;
   L_loc_type           ORDLOC.LOC_TYPE%TYPE                := NULL;
   L_ordsku_exists      VARCHAR2(1)                         := 'N';
   L_ordloc_exists      VARCHAR2(1)                         := 'N';
   L_approval_ind       VARCHAR2(1)                         := 'N';
   L_cancel_qty         ORDLOC.QTY_CANCELLED%TYPE           := NULL;
   L_reinstate_qty      ORDLOC.QTY_CANCELLED%TYPE           := NULL;
   L_cancel_date        ORDLOC.CANCEL_DATE%TYPE             := NULL;
   L_cancel_code        ORDLOC.CANCEL_CODE%TYPE             := NULL;
   L_cancel_id          ORDLOC.CANCEL_ID%TYPE               := NULL;
   L_cancel_qty_old     ORDLOC.QTY_CANCELLED%TYPE           := NULL;
   L_order_qty_old      ORDLOC.QTY_ORDERED%TYPE             := NULL;
   L_receive_qty_old    ORDLOC.QTY_RECEIVED%TYPE            := NULL;
   L_order_curr         CURRENCIES.CURRENCY_CODE%TYPE       := NULL;
   L_sup_curr           Currencies.Currency_code%TYPE       := NULL;
   L_unit_cost_sup      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE    := NULL;
   L_unit_cost_ord      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE    := NULL;
   L_unit_cost          ITEM_SUPP_COUNTRY.UNIT_COST%TYPE    := NULL;
   L_no_cancel_ind      VARCHAR2(1)                         := NULL;
   L_ordloc_locked      VARCHAR2(1)                         := NULL;
   L_table              VARCHAR2(50)                        := NULL;
   L_exchange_rate      ORDHEAD.EXCHANGE_RATE%TYPE          := NULL;
   L_container_item     ITEM_MASTER.CONTAINER_ITEM%TYPE     := NULL;
   L_tot_qty_ordered    ORDLOC.QTY_ORDERED%TYPE             := 0;
   L_otb_ind            SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE  := NULL;

   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_CANCEL_QTY is
      select qty_cancelled,
             qty_ordered,
             NVL(qty_received, 0)
        from ordloc
       where order_no = I_order_no
         and item = I_item
         and location = I_location;

   cursor C_ORDSKU_REC is
      select 'Y'
        from ordsku
       where order_no = I_order_no
         and item = I_item;

   cursor C_GET_CURR_COST is
      select ord.currency_code,
             sup.currency_code,
             iscl.unit_cost,
             ord.exchange_rate
        from sups sup,
             ordhead ord,
             item_supp_country_loc iscl,
             ordsku os
       where ord.order_no = I_order_no
         and ord.order_no = os.order_no
         and iscl.item = I_item
         and iscl.item = os.item
         and sup.supplier = ord.supplier
         and sup.supplier = iscl.supplier
         and iscl.origin_country_id = os.origin_country_id
         and iscl.loc = I_location;

   cursor C_LOCK_ORDLOC is
      select 'x'
        from ordloc
       where order_no = I_order_no
         and item = I_item
         and location = I_location
         for update nowait;

   cursor C_GET_CONTAINER_ITEM is
      select distinct(i.container_item) container_item,
             sum(o.qty_ordered) qty_ordered
        from ordloc o,
             item_master i
       where o.order_no = I_order_no
         and o.location = I_location
         and o.item = i.item
         and i.deposit_item_type = 'E'
       group by container_item;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_CANCEL_QTY','ordloc',
                    'order no: '||TO_CHAR(I_order_no)||
                    ', Item: '||I_item||
                    ', location: '||TO_CHAR(I_location));
   open C_GET_CANCEL_QTY;
   SQL_LIB.SET_MARK('FETCH','C_GET_CANCEL_QTY','ordloc',
                    'order no: '||TO_CHAR(I_order_no)||
                    ', Item: '||I_item||
                    ', location: '||TO_CHAR(I_location));
   fetch C_GET_CANCEL_QTY into L_cancel_qty_old,
                               L_order_qty_old,
                               L_receive_qty_old;
   SQL_LIB.SET_MARK('CLOSE','C_GET_CANCEL_QTY','ordloc',
                    'order no: '||TO_CHAR(I_order_no)||
                    ', Item: '||I_item||
                    ', location: '||TO_CHAR(I_location));
   close C_GET_CANCEL_QTY;

   L_cancel_qty := NVL(L_cancel_qty_old, 0) + (I_old_qty - NVL(I_new_qty, 0));
   if L_cancel_qty > 0 then
      L_cancel_date := GET_VDATE;
      L_cancel_code := 'V';  /* vendor cancelled */
      L_cancel_id := I_cancel_id;
   elsif L_cancel_qty_old = 0 or L_cancel_qty_old is NULL then
      L_no_cancel_ind := 'Y';
   elsif NVL(L_cancel_qty, 0) <= 0 then
      L_cancel_qty := NULL;
      L_cancel_date := NULL;
      L_cancel_code := NULL;
      L_cancel_id := NULL;
   end if;

   if NVL(I_new_qty, 0) > 0 then
      SQL_LIB.SET_MARK('OPEN','C_ORDSKU_REC','ordsku',
                       'order no: '||TO_CHAR(I_order_no)||
                       ', Item: '||I_item);
      open C_ORDSKU_REC;

      SQL_LIB.SET_MARK('FETCH','C_ORDSKU_REC','ordsku',
                       'order no: '||TO_CHAR(I_order_no)||
                       ', Item: '||I_item);
      fetch C_ORDSKU_REC into L_ordsku_exists;

      SQL_LIB.SET_MARK('CLOSE','C_ORDSKU_REC','ordsku',
                       'order no: '||TO_CHAR(I_order_no)||
                       ', Item: '||I_item);
      close C_ORDSKU_REC;

      if L_ordsku_exists = 'N' then


         SQL_LIB.SET_MARK('INSERT',NULL,'ordsku',
                          'order no: '||TO_CHAR(I_order_no)||
                          ', item: '||I_item);
         insert into ordsku(order_no,
                            item,
                            ref_item,
                            origin_country_id,
                            earliest_ship_date,
                            latest_ship_date,
                            supp_pack_size,
                            non_scale_ind)
                     select I_order_no,
                            I_item,
                            ref_item,
                            origin_country_id,
                            earliest_ship_date,
                            latest_ship_date,
                            supp_pack_size,
                            non_scale_ind
                       from ordsku_rev
                      where rev_no = I_rev_no
                        and origin_type = I_rev_type
                        and order_no = I_order_no
                        and item = I_item;
      end if;

      L_table := 'ordloc';
      open C_LOCK_ORDLOC;
      close C_LOCK_ORDLOC;
      L_ordloc_locked := 'Y';

      SQL_LIB.SET_MARK('UPDATE',NULL,'ordloc',
                       'order no: '||TO_CHAR(I_order_no)||
                       ', item: '||I_item||
                       ', location: '||TO_CHAR(I_location));
      if L_no_cancel_ind = 'Y' then
         update ordloc
            set qty_ordered = I_new_qty
          where order_no = I_order_no
            and item = I_item
            and location = I_location;

         /* if item is a deposit item, update the quantity of the container items as well */

         open C_GET_CONTAINER_ITEM;
         fetch C_GET_CONTAINER_ITEM into L_container_item,
                                         L_tot_qty_ordered;
         close C_GET_CONTAINER_ITEM;

         if L_container_item is NOT NULL then
            update ordloc
               set qty_ordered = L_tot_qty_ordered
             where order_no = I_order_no
               and item = L_container_item
               and location = I_location;
         end if;

      else
         update ordloc
            set qty_ordered = I_new_qty,
                qty_cancelled = L_cancel_qty,
                cancel_date = L_cancel_date,
                cancel_code = L_cancel_code,
                cancel_id = L_cancel_id
          where order_no = I_order_no
            and item = I_item
            and location = I_location;

         /* if item is a deposit item, cancel the container items as well*/

         open C_GET_CONTAINER_ITEM;
         fetch C_GET_CONTAINER_ITEM into L_container_item,
                                         L_tot_qty_ordered;
         close C_GET_CONTAINER_ITEM;

         if L_container_item is NOT NULL then
            update ordloc
               set qty_ordered = L_tot_qty_ordered
             where order_no = I_order_no
               and item = L_container_item
               and location = I_location;
         end if;
      end if;

      if SQL%NOTFOUND then
         SQL_LIB.SET_MARK('OPEN','C_GET_CURR_COST','SUPS, ORDHEAD',
                          'order no: '||TO_CHAR(I_order_no)||
                          ', Item: '||I_item);
         open C_GET_CURR_COST;
         SQL_LIB.SET_MARK('FETCH','C_GET_CURR_COST','SUPS, ORDHEAD',
                          'order no: '||TO_CHAR(I_order_no)||
                          ', Item: '||I_item);
         fetch C_GET_CURR_COST into L_order_curr,
                                    L_sup_curr,
                                    L_unit_cost_sup,
                                    L_exchange_rate;
         SQL_LIB.SET_MARK('CLOSE','C_GET_CURR_COST','SUPS, ORDHEAD',
                          'order no: '||TO_CHAR(I_order_no)||
                          ', Item: '||I_item);
         close C_GET_CURR_COST;
         ---
         if L_order_curr != L_sup_curr then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_unit_cost_sup,
                                    L_sup_curr,
                                    L_order_curr,
                                    L_unit_cost_ord,
                                    'C',
                                    NULL,
                                    NULL,
                                    NULL,
                                    L_exchange_rate) = FALSE then
               return FALSE;
            end if;
            L_unit_cost := L_unit_cost_ord;
         else
            L_unit_cost := L_unit_cost_sup;
         end if;

         SQL_LIB.SET_MARK('INSERT',NULL,'ordloc',
                          'order no: '||TO_CHAR(I_order_no)||
                          ', Item: '||I_item||
                          ', location: '||TO_CHAR(I_location));
         insert into ordloc(order_no,
                            item,
                            location,
                            loc_type,
                            unit_retail,
                            unit_cost,
                            cost_source,
                            qty_ordered,
                            qty_received,
                            qty_prescaled,
                            non_scale_ind)
                     select order_no,
                            item,
                            location,
                            loc_type,
                            unit_retail,
                            L_unit_cost,
                            'NORM',
                            qty_ordered,
                            qty_received,
                            qty_prescaled,
                            non_scale_ind
                       from ordloc_rev
                      where rev_no = I_rev_no
                        and origin_type = I_rev_type
                        and order_no = I_order_no
                        and item = I_item
                        and location = I_location;
      end if;
   else  /* I_new_qty = 0 and L_approved = 'Y' so record should be cancelled */
      L_cancel_qty := NVL(L_cancel_qty_old, 0) + (L_order_qty_old - L_receive_qty_old);
      if L_cancel_qty > 0 then
         if L_ordloc_locked != 'Y' then
            L_table := 'ordloc';
            open C_LOCK_ORDLOC;
            close C_LOCK_ORDLOC;
         end if;
         SQL_LIB.SET_MARK('UPDATE',NULL,'ordloc',
                          'order no: '||TO_CHAR(I_order_no)||
                          ', item: '||I_item||
                          ', location: '||TO_CHAR(I_location));
         update ordloc
            set qty_cancelled = L_cancel_qty,
                qty_ordered = L_receive_qty_old,
                cancel_code = L_cancel_code,
                cancel_id = L_cancel_id,
                cancel_date = L_cancel_date
          where order_no = I_order_no
            and item = I_item
            and location = I_location;

         /* if item is a deposit item, cancel the container items as well*/

         open C_GET_CONTAINER_ITEM;
         fetch C_GET_CONTAINER_ITEM into L_container_item,
                                         L_tot_qty_ordered;
         close C_GET_CONTAINER_ITEM;

         if L_container_item is NOT NULL then
            update ordloc
               set qty_ordered = I_new_qty
             where order_no = I_order_no
               and item = L_container_item
               and location = I_location;
         end if;
      end if;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_OTB_SYSTEM_IND(O_error_message,
                                            L_otb_ind) = FALSE then
      return FALSE;
   end if;
   
   if NVL(L_cancel_qty_old, 0) != NVL(L_cancel_qty, 0) and
         (NVL(L_cancel_qty, 0) > 0 or NVL(L_cancel_qty_old, 0) > 0) and L_otb_ind = 'Y' then
      if NVL(L_cancel_qty, 0) > NVL(L_cancel_qty_old, 0) then
         L_cancel_qty := NVL(L_cancel_qty, 0) - NVL(L_cancel_qty_old, 0);
         if OTB_SQL.ORD_CANCEL_REINSTATE(O_error_message,
                                         I_order_no,
                                         I_item,
                                         I_location,
                                         I_loc_type,
                                         L_cancel_qty,
                                         'C') = FALSE then
            return FALSE;
         end if;
      else
         L_reinstate_qty := NVL(L_cancel_qty_old, 0) - NVL(L_cancel_qty, 0);
         if OTB_SQL.ORD_CANCEL_REINSTATE(O_error_message,
                                         I_order_no,
                                         I_item,
                                         I_location,
                                         I_loc_type,
                                         L_reinstate_qty,
                                         'R') = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',L_table,
                                             TO_CHAR(I_order_no),
                                             I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'ORDREV_SQL.QTY_CHANGE',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END QTY_CHANGE;
--------------------------------------------------------------------------
FUNCTION COST_CHANGE(O_error_message IN OUT VARCHAR2,
                     I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
                     I_rev_no        IN     ORDLOC_REV.REV_NO%TYPE,
                     I_rev_type      IN     ORDLOC_REV.ORIGIN_TYPE%TYPE,
                     I_item          IN     ORDLOC.ITEM%TYPE,
                     I_location      IN     ORDLOC.LOCATION%TYPE,
                     I_loc_type      IN     ORDLOC.LOC_TYPE%TYPE,
                     I_cost_source   IN     ORDLOC.COST_SOURCE%TYPE,
                     I_new_cost      IN     ORDLOC.UNIT_COST%TYPE)
                     RETURN BOOLEAN IS

RECORD_LOCKED        EXCEPTION;
PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
L_ordsku_exists      VARCHAR2(1)                      := 'N';

cursor C_LOCK_ORDLOC is
   select 'x'
     from ordloc
    where order_no = I_order_no
      and item = I_item
      and location = I_location
      for update nowait;

cursor C_ORDSKU_REC is
   select 'Y'
     from ordsku
    where order_no = I_order_no
      and item = I_item;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_ORDSKU_REC','ordsku',
                    'order no: '||TO_CHAR(I_order_no)||
                    ', Item: '||I_item);
   open C_ORDSKU_REC;

   SQL_LIB.SET_MARK('FETCH','C_ORDSKU_REC','ordsku',
                    'order no: '||TO_CHAR(I_order_no)||
                    ', Item: '||I_item);
   fetch C_ORDSKU_REC into L_ordsku_exists;

   SQL_LIB.SET_MARK('CLOSE','C_ORDSKU_REC','ordsku',
                    'order no: '||TO_CHAR(I_order_no)||
                    ', Item: '||I_item);
   close C_ORDSKU_REC;

   if L_ordsku_exists = 'N' then
      SQL_LIB.SET_MARK('INSERT',NULL,'ordsku',
                       'order no: '||TO_CHAR(I_order_no)||
                       ', item: '||I_item);
      insert into ordsku(order_no,
                         item,
                         ref_item,
                         origin_country_id,
                         earliest_ship_date,
                         latest_ship_date,
                         supp_pack_size,
                         non_scale_ind)
                  select I_order_no,
                         I_item,
                         ref_item,
                         origin_country_id,
                         earliest_ship_date,
                         latest_ship_date,
                         supp_pack_size,
                         non_scale_ind
                    from ordsku_rev
                   where rev_no = I_rev_no
                     and origin_type = I_rev_type
                     and order_no = I_order_no
                     and item = I_item;
   end if;
   ---
   if OTB_SQL.ORD_UNAPPROVE(I_order_no,
                            O_error_message) = FALSE then
      return FALSE;
   end if;

   open C_LOCK_ORDLOC;
   close C_LOCK_ORDLOC;

   SQL_LIB.SET_MARK('UPDATE',NULL,'ordloc',
                    'order_no: '||TO_CHAR(I_order_no)||
                    'item: '|| I_item ||
                    'loc: '||to_char(I_location));
   update ordloc
      set cost_source = I_cost_source,
          unit_cost = I_new_cost
    where order_no = I_order_no
      and item = I_item
      and location = I_location;

   if SQL%NOTFOUND then
      SQL_LIB.SET_MARK('INSERT',NULL,'ordsku',
                       'order_no: '||TO_CHAR(I_order_no)||
                       'item: '|| I_item ||
                       'loc: '||to_char(I_location));
      insert into ordloc(order_no,
                         item,
                         location,
                         loc_type,
                         unit_retail,
                         qty_ordered,
                         qty_prescaled,
                         qty_received,
                         last_received,
                         qty_cancelled,
                         cancel_code,
                         cancel_date,
                         cancel_id,
                         original_repl_qty,
                         unit_cost,
                         unit_cost_init,
                         cost_source,
                         non_scale_ind)
                  select order_no,
                         item,
                         location,
                         loc_type,
                         unit_retail,
                         qty_ordered,
                         qty_prescaled,
                         qty_received,
                         NULL,  -- Last received
                         qty_cancelled,
                         cancel_code,
                         cancel_date,
                         cancel_id,
                         original_repl_qty,
                         I_new_cost,
                         NULL, -- unit_cost_init
                         I_cost_source,
                         non_scale_ind
                    from ordloc_rev
                   where rev_no = I_rev_no
                     and origin_type = I_rev_type
                     and order_no = I_order_no
                     and item = I_item;
   end if;
   
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PE',
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             I_order_no,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.ORD_APPROVE (I_order_no,
                           O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ORDLOC',
                                             TO_CHAR(I_order_no),
                                             I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'ORDREV_SQL.COST_CHANGE',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END COST_CHANGE;
--------------------------------------------------------------------------
FUNCTION DATE_CHANGE(O_error_message  IN OUT VARCHAR2,
                     I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                     I_old_not_before IN     ORDHEAD.NOT_BEFORE_DATE%TYPE,
                     I_old_not_after  IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                     I_old_pickup     IN     ORDHEAD.PICKUP_DATE%TYPE,
                     I_not_before     IN     ORDHEAD.NOT_BEFORE_DATE%TYPE,
                     I_not_after      IN     ORDHEAD.NOT_AFTER_DATE%TYPE,
                     I_pickup         IN     ORDHEAD.PICKUP_DATE%TYPE)
                     RETURN BOOLEAN IS

L_vdate              PERIOD.VDATE%TYPE   := GET_VDATE;
RECORD_LOCKED        EXCEPTION;
PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

cursor C_LOCK_ORDHEAD is
   select 'x'
     from ordhead
    where order_no = I_order_no
      for update nowait;

BEGIN
   if I_not_before is not NULL or
      I_not_after is not NULL or
      I_pickup is not NULL then
      open C_LOCK_ORDHEAD;
      close C_LOCK_ORDHEAD;

      if I_not_before is not NULL then
         if I_not_before < L_vdate then
            O_error_message :=
                   SQL_LIB.CREATE_MSG('DATE_NBD_NOT_BEFORE_TODAY',
                                       NULL, NULL, NULL);
            return FALSE;
         end if;
      end if;

      if I_pickup is not NULL then
         if I_pickup < L_vdate then
            O_error_message :=
                   SQL_LIB.CREATE_MSG('PICKUP_NOT_BEFORE_TODAY',
                                       NULL, NULL, NULL);
            return FALSE;
         end if;
      end if;

      if NVL(I_not_before, I_old_not_before) > NVL(I_not_after, I_old_not_after) then
         O_error_message :=
               SQL_LIB.CREATE_MSG('DATE_NAD_LATER_THAN_NBD',
                                    NULL, NULL, NULL);
         return FALSE;
      end if;

      if NVL(I_pickup, I_old_pickup) > NVL(I_not_after, I_old_not_after) then
         O_error_message :=
               SQL_LIB.CREATE_MSG('NAD_LATER_THAN_PICKD',
                                    NULL, NULL, NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('UPDATE',NULL,'ordhead',
                       'order no: '||TO_CHAR(I_order_no));
      update ordhead
         set not_before_date = NVL(I_not_before, not_before_date),
             not_after_date = NVL(I_not_after, not_after_date),
             pickup_date = NVL(I_pickup, pickup_date),
             last_update_id = get_user,
             last_update_datetime = sysdate
       where order_no = I_order_no;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ORDHEAD',
                                             TO_CHAR(I_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'ORDREV_SQL.DATE_CHANGE',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DATE_CHANGE;
---------------------------------------------------------------------------
FUNCTION CHK_ORD_CLOSE_METHOD (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_edi_to_send_flag  OUT    BOOLEAN,
                               I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_last_sent_rev_no  ORDHEAD.LAST_SENT_REV_NO%TYPE;
   L_found             VARCHAR2(1);

   cursor C_LAST_SENT_REV_NO IS
      SELECT last_sent_rev_no
        FROM ordhead
       WHERE order_no =  I_order_no;

   cursor C_ORDLOC_QTY IS
      SELECT 'x'
        FROM ordloc ol
       WHERE ol.order_no =  I_order_no
         AND exists ( select 'x'
                        from ordloc_rev olr
                       where ol.order_no = olr.order_no
                         and ol.item = olr.item
                         and ol.location = olr.location
                         and olr.rev_no = L_last_sent_rev_no
                         and nvl(ol.qty_cancelled,0) != nvl(olr.qty_cancelled,0))
         AND rownum = 1;

   cursor C_ORDLOC_REV_QTY IS
      SELECT 'x'
        FROM ordloc_rev
       WHERE order_no =  I_order_no
         AND origin_type = 'V'
         AND rev_no = L_last_sent_rev_no
         AND qty_ordered != 0
         AND rownum = 1;

BEGIN
   -- True if:
   --- 1. a revision has been sent
   --- 2. current revision has been zeroed out
   --- 3. last sent revision was not zeroed out
   O_edi_to_send_flag := FALSE;

   L_last_sent_rev_no := NULL;
   SQL_LIB.SET_MARK('OPEN','C_LAST_SENT_REV_NO','ORDHEAD',NULL);
   open C_LAST_SENT_REV_NO;
   SQL_LIB.SET_MARK('FETCH','C_LAST_SENT_REV_NO','ORDHEAD',NULL);
   fetch C_LAST_SENT_REV_NO into L_last_sent_rev_no;
   SQL_LIB.SET_MARK('CLOSE','C_LAST_SENT_REV_NO','ORDHEAD',NULL);
   close C_LAST_SENT_REV_NO;

   if L_last_sent_rev_no is null then
      return TRUE;  -- PO hasn't been sent, no need to update
   end if;

   L_found := NULL;
   SQL_LIB.SET_MARK('OPEN','C_ORDLOC_QTY','ORDLOC',NULL);
   open C_ORDLOC_QTY;
   SQL_LIB.SET_MARK('FETCH','C_ORDLOC_QTY','ORDLOC',NULL);
   fetch C_ORDLOC_QTY into L_found;
   SQL_LIB.SET_MARK('CLOSE','C_ORDLOC_QTY','ORDLOC',NULL);
   close C_ORDLOC_QTY;

   if L_found is null then
      return TRUE;
   end if;

   L_found := NULL;
   SQL_LIB.SET_MARK('OPEN','C_ORDLOC_REV_QTY','ORDLOC_REV',NULL);
   open C_ORDLOC_REV_QTY;
   SQL_LIB.SET_MARK('FETCH','C_ORDLOC_REV_QTY','ORDLOC_REV',NULL);
   fetch C_ORDLOC_REV_QTY into L_found;
   SQL_LIB.SET_MARK('CLOSE','C_ORDLOC_REV_QTY','ORDLOC_REV',NULL);
   close C_ORDLOC_REV_QTY;

   if L_found is null then
      -- Last sent revision was already zeroed out, no need to send
      return TRUE;
   end if;

   -- all conditions satisfied
   O_edi_to_send_flag := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'ORDREV_SQL.CHK_ORD_CLOSE_METHOD',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHK_ORD_CLOSE_METHOD;
-----------------------------------------------------------------------------------
END ORDREV_SQL;
/
