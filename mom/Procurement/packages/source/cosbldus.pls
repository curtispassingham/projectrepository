CREATE OR REPLACE PACKAGE COST_BUILDUP_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
   -- Define errors so procedures can raise_application_error
   ERRNUM_PACKAGE_CALL   NUMBER(6) := -20020;
   INVALID_ERROR         EXCEPTION;
   PARAM_ERROR           EXCEPTION;
-------------------------------------------------------------------------------------------

TYPE WF_CSTREL_REC is RECORD(dept            WF_COST_RELATIONSHIP.DEPT%TYPE,
                             class           WF_COST_RELATIONSHIP.CLASS%TYPE,
                             subclass        WF_COST_RELATIONSHIP.SUBCLASS%TYPE,
                             item            WF_COST_RELATIONSHIP.ITEM%TYPE,
                             location        WF_COST_RELATIONSHIP.LOCATION%TYPE,
                             start_date      WF_COST_RELATIONSHIP.START_DATE%TYPE,
                             end_date        WF_COST_RELATIONSHIP.END_DATE%TYPE,
                             templ_id        WF_COST_RELATIONSHIP.TEMPL_ID%TYPE,
                             templ_desc      WF_COST_BUILDUP_TMPL_HEAD.TEMPL_DESC%TYPE,
                             select_ind      VARCHAR2(1),
                             row_id          VARCHAR2(100),
                             error_message   RTK_ERRORS.RTK_TEXT%TYPE,
                             return_code     VARCHAR2(5));

TYPE WF_CSTREL_TBL is TABLE of WF_CSTREL_REC
   INDEX BY BINARY_INTEGER;
-------------------------------------------------------------------------------------------------------
-- Function Name  : GET_NEXT_TEMPLATE
-- Purpose        : This new function will be called from the costbdutpl.fmb form to fetch the
--                  next available template id.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_TEMPLATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_templ_id        IN OUT   WF_COST_BUILDUP_TMPL_HEAD.TEMPL_ID%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name  : COST_RELATION_OVERLAP_CHECK
-- Purpose        : This function will take the given dept, class(optional), subclass(optional),
--                  organizational group type and group value, start and end dates and check to see if
--                  any records exist that overlap.  If an overlap is found, O_overlap_exists
--                  should be returned TRUE.
-------------------------------------------------------------------------------------------------------
FUNCTION COST_RELATION_OVERLAP_CHECK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_overlap_exists  IN OUT   BOOLEAN,
                                     I_dept            IN       WF_COST_RELATIONSHIP.DEPT%TYPE,
                                     I_class           IN       WF_COST_RELATIONSHIP.CLASS%TYPE,
                                     I_subclass        IN       WF_COST_RELATIONSHIP.SUBCLASS%TYPE,
                                     I_group_type      IN       CODE_DETAIL.CODE%TYPE,
                                     I_group_value     IN       VARCHAR2,
                                     I_start_date      IN       WF_COST_RELATIONSHIP.START_DATE%TYPE,
                                     I_end_date        IN       WF_COST_RELATIONSHIP.END_DATE%TYPE,
                                     I_rowid           IN       VARCHAR2,
                                     I_item            IN       WF_COST_RELATIONSHIP.ITEM%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name  : COST_RELATION_EXPLODE
-- Purpose        : This function will take the given dept, class(optional), subclass(optional),
--                  organizational group type and group values and overwrite indicator and explode that
--                  information down to the subclass/wholesale location relationship level.
--                  The overwrite indicator will indicate whether to overwrite any overlaps or to explode
--                  all the values but don't overwrite the overlaps (if the overwrite indicator is NULL
--                  then no overlaps were found).
-------------------------------------------------------------------------------------------------------
FUNCTION COST_RELATION_EXPLODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_overwrite_ind   IN       VARCHAR2,
                               I_dept            IN       WF_COST_RELATIONSHIP.DEPT%TYPE,
                               I_class           IN       WF_COST_RELATIONSHIP.CLASS%TYPE,
                               I_subclass        IN       WF_COST_RELATIONSHIP.SUBCLASS%TYPE,
                               I_group_type      IN       CODE_DETAIL.CODE%TYPE,
                               I_group_value     IN       VARCHAR2,
                               I_start_date      IN       WF_COST_RELATIONSHIP.START_DATE%TYPE,
                               I_end_date        IN       WF_COST_RELATIONSHIP.END_DATE%TYPE,
                               I_templ_id        IN       WF_COST_RELATIONSHIP.TEMPL_ID%TYPE,
                               I_rowid           IN       VARCHAR2,
                               I_item            IN       WF_COST_RELATIONSHIP.ITEM%TYPE)
                               
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name  : TEMPL_RELATION_EXISTS
-- Purpose        : This function will check if the cost buildup template ID being passed in already
--                  exists in the WF_COST_RELATIONSHIP table.
-------------------------------------------------------------------------------------------------------
FUNCTION TEMPL_RELATION_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_templ_exists    IN OUT   BOOLEAN,
                               I_cbldup_tmpl_id  IN       WF_COST_RELATIONSHIP.TEMPL_ID%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name  : DEL_COST_TEMPLATE
-- Purpose        : This function will delete a cost template and any associated cost relationships.
-------------------------------------------------------------------------------------------------------
FUNCTION DEL_COST_TEMPLATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_templ_id        IN       WF_COST_BUILDUP_TMPL_HEAD.TEMPL_ID%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name  : VALIDATE_TEMPL_ID
-- Purpose        : This function will check if a record already exists in the WF_COST_BUILDUP_TMPL_HEAD
--                  table given a tmpl_id.
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TEMPL_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_templ_desc      IN OUT   WF_COST_BUILDUP_TMPL_HEAD.TEMPL_DESC%TYPE,
                           I_templ_id        IN       WF_COST_BUILDUP_TMPL_HEAD.TEMPL_ID%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name  : COST_COMP_EXISTS
-- Purpose        : This function will validate if the cost comp id already exists in the
--                  WF_COST_BUILDUP_TMPL_DETAIL table given the templ id.
-------------------------------------------------------------------------------------------------------
FUNCTION COST_COMP_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT   BOOLEAN,
                          I_cost_comp_id    IN       WF_COST_BUILDUP_TMPL_DETAIL.COST_COMP_ID%TYPE,
                          I_templ_id        IN       WF_COST_BUILDUP_TMPL_DETAIL.TEMPL_ID%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name  : INSERT_COMP_DETAIL
-- Purpose        : This function will be used to insert into the WF_COST_BUILDUP_TMPL_DETAIL table
--                  used in the costbdutpld.fmb form.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_COMP_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_comp_id    IN       WF_COST_BUILDUP_TMPL_DETAIL.COST_COMP_ID%TYPE,
                            I_templ_id        IN       WF_COST_BUILDUP_TMPL_DETAIL.TEMPL_ID%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name  : GET_CST_RELATION
-- Purpose        : This new procedure will be used to return records from the Cost Relationship
--                  table given the criteria in the search block of the new Franchise Cost
--                  Relationship form (costbdurel.fmb).
-------------------------------------------------------------------------------------------------------
PROCEDURE GET_CST_RELATION(O_wf_costrel_tbl   IN OUT   COST_BUILDUP_SQL.WF_CSTREL_TBL,
                           I_dept             IN       WF_COST_RELATIONSHIP.DEPT%TYPE,
                           I_class            IN       WF_COST_RELATIONSHIP.CLASS%TYPE,
                           I_subclass         IN       WF_COST_RELATIONSHIP.SUBCLASS%TYPE,
                           I_location         IN       WF_COST_RELATIONSHIP.LOCATION%TYPE,
                           I_start_date       IN       WF_COST_RELATIONSHIP.START_DATE%TYPE,
                           I_end_date         IN       WF_COST_RELATIONSHIP.END_DATE%TYPE,
                           I_templ_id         IN       WF_COST_RELATIONSHIP.TEMPL_ID%TYPE,
                           I_order_by         IN       VARCHAR2,
                           I_item             IN       WF_COST_RELATIONSHIP.ITEM%TYPE);
-------------------------------------------------------------------------------------------------------
-- Function Name  : APPLY_COST_TEMPLATE
-- Purpose        : This new function will be called from the wfcostcalc.pc and elccostcalc.pc batch programs
--                  to calculate the total applicable cost template value.
-------------------------------------------------------------------------------------------------------
FUNCTION APPLY_COST_TEMPLATE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_est_cost_temp      IN OUT   NUMBER,
                             I_item               IN       ITEM_MASTER.ITEM%TYPE,
                             I_supplier           IN       SUPS.SUPPLIER%TYPE,
                             I_origin_country_id  IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_location           IN       ITEM_LOC.LOC%TYPE,
                             I_acquisition_cost   IN       FUTURE_COST.ACQUISITION_COST%TYPE,
                             I_date               IN       WF_COST_RELATIONSHIP.START_DATE%TYPE,
                             I_supp_currency_code IN       SUPS.CURRENCY_CODE%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Procedure Name : DEL_COST_REL
-- Purpose        : This function will delete from the WF_COST_RELATIONSHIP table.
-------------------------------------------------------------------------------------------------------
FUNCTION DEL_COST_REL (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_wf_costrel_tbl   IN      COST_BUILDUP_SQL.WF_CSTREL_TBL)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name : DUP_RELN_EXISTS
-- Purpose       : This function will check if duplicate record already exists in WF_COST_RELATIONSHIP
-- for the passed in department,classs,subclass,location,start_date and end_date.
-------------------------------------------------------------------------------------------------------
FUNCTION DUP_RELN_EXISTS (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_dup_reln_exists IN OUT  BOOLEAN,
                          I_dept            IN      WF_COST_RELATIONSHIP.DEPT%TYPE,
                          I_class           IN      WF_COST_RELATIONSHIP.CLASS%TYPE,
                          I_subclass        IN      WF_COST_RELATIONSHIP.SUBCLASS%TYPE,
                          I_location        IN      WF_COST_RELATIONSHIP.LOCATION%TYPE,
                          I_start_date      IN      WF_COST_RELATIONSHIP.START_DATE%TYPE,
                          I_end_date        IN      WF_COST_RELATIONSHIP.END_DATE%TYPE,
                          I_item            IN      WF_COST_RELATIONSHIP.ITEM%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name : GET_FIRSTAPPLIED_VALUE
-- Purpose       : This function will check whether the cost buildup template is based on Margin/Upcharge/Retail
--                 Cost.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_FIRSTAPPLIED_VALUE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_first_applied   IN OUT  WF_COST_BUILDUP_TMPL_HEAD.FIRST_APPLIED%TYPE,
                                I_templ_id        IN      WF_COST_RELATIONSHIP.TEMPL_ID%TYPE)
 RETURN BOOLEAN;    
----------------------------------------------------------------------------------------------------
-- Function Name : CREATE_FC_FOR_COST_TMPL_RELSHP
-- Purpose       : This function will create future cost event for WF cost template relationship.
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_FC_FOR_COST_TMPL_RELSHP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------- 
-- Function Name : DEL_COST_REL_WRP
-- Purpose       : This is a wrapper function for DEL_COST_REL that passes a database 
--                 type object instead of a PL/SQL type as an input parameter to be called from Java wrappers.
---------------------------------------------------------------------------------------------------- 
FUNCTION DEL_COST_REL_WRP(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_wf_costrel_tbl   IN       WRP_WF_COST_RELSHP_TBL)
   RETURN INTEGER;
----------------------------------------------------------------------------------------------------
-- Function Name : DEL_COST_TEMP_COMP
-- Purpose       : This function will delete cost_comp_id of a template 
--                 and the corresponding TL data
---------------------------------------------------------------------------------------------------- 
FUNCTION DEL_COST_TEMP_COMP(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_templ_id         IN       WF_COST_BUILDUP_TMPL_DETAIL.TEMPL_ID%TYPE,
                            I_cost_comp_id     IN       WF_COST_BUILDUP_TMPL_DETAIL.COST_COMP_ID%TYPE)
 RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------- 
END;
/