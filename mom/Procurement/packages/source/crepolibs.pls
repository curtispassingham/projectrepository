CREATE OR REPLACE PACKAGE CREATE_PO_LIB_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------------------
-- Function: DO_INSERTS
-- Purpose : This function will take all the info in the order table types and create orders.
---------------------------------------------------------------------------------------------------------
FUNCTION DO_INSERTS(I_ordhead_tbl       IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                    O_orders_processed  IN OUT  NUMBER,
                    O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION APPROVE_SUBMIT_PO(I_ordhead_tbl    IN      ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                           O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION ROUND_PO(I_ordhead_tbl    IN      ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                  O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)   
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_OIM(I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                    I_ltl_flag       IN      ORD_INV_MGMT.LTL_IND%TYPE,
                    I_split_message  IN      ORD_INV_MGMT.TRUCK_SPLIT_ISSUES%TYPE,
                    O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_GROUP_ROUNDED_QTY(I_ordhead_tbl    IN      ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                                  O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
END CREATE_PO_LIB_SQL;
/