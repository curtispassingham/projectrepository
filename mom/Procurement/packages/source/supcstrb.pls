CREATE OR REPLACE PACKAGE BODY SUP_CONSTRAINTS_SQL AS

--------------------------------------------------------------------------------
--                           GLOBAL VARIABLES                                 --
--------------------------------------------------------------------------------


-- Hardcoded values used in Pro*C
MAX_SCALE_ITERATION            BINARY_INTEGER := 1000;
MAX_NUM_RANK                   BINARY_INTEGER := 12;        /* for max of 2 scaling constraints */ 
MAX_VALUE                      BINARY_INTEGER := 99999999; /* default scale_cnstr_max_val */ 
MAX_NUM_EVAL                   BINARY_INTEGER := 16;        /* for max of 2 scaling constraints */ 
MAX_SCALE_CNSTR                BINARY_INTEGER := 2;         /* max num of scaling constraints */
MAX_NUM_ORDLOC                 BINARY_INTEGER := 1000;      /* max num of locations the order goes to */
MAX_FETCH_SIZE                 BINARY_INTEGER := 1000;

LP_most_constraining           BINARY_INTEGER;
LP_NON_FATAL                   BOOLEAN := FALSE;    /* Value to indicate NON FATAL return */

-- System Options
LP_bracket_costing_ind         SYSTEM_OPTIONS.BRACKET_COSTING_IND%TYPE;
LP_elc_ind                     SYSTEM_OPTIONS.ELC_IND%TYPE;
LP_alloc_method                SYSTEM_OPTIONS.ALLOC_METHOD%TYPE;
LP_max_scaling_iterations      SYSTEM_OPTIONS.MAX_SCALING_ITERATIONS%TYPE;

LP_order_no                    ORDHEAD.ORDER_NO%TYPE;
LP_total_scale_cnstr           BINARY_INTEGER := 0;
LP_total_truck_load            BINARY_INTEGER := 0;
LP_total_ordloc                BINARY_INTEGER := 0;
LP_total_vloc                  BINARY_INTEGER := 0;

-- Table Types Used for Stuct Arrays
LP_rank_priority_tbl           SUP_CONSTRAINTS_SQL.RANK_PRIORITY_TBL;
LP_eval_system_tbl             SUP_CONSTRAINTS_SQL.EVAL_SYSTEM_TBL;
LP_ord_inv_mgmt_rec            SUP_CONSTRAINTS_SQL.ORD_INV_MGMT_REC;
LP_scale_cnstr_tbl             SUP_CONSTRAINTS_SQL.SCALE_CNSTR_TBL;
/* for day scaling */
LP_scale_item_loc_tbl          SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL;
LP_scale_loc_tbl               SUP_CONSTRAINTS_SQL.SCALE_LOC_TBL;
/* for case/pallet rounding and scaling */
LP_scale_item_loc_case_tbl     SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL;
LP_scale_loc_case_tbl          SUP_CONSTRAINTS_SQL.SCALE_LOC_TBL;
/* Driving Cursor Tbl */
LP_scaling_tbl                 SUP_CONSTRAINTS_SQL.SCALING_TBL;

LP_forecast_item_loc           SUP_CONSTRAINTS_SQL.FORECAST_ITEM_LOC_TBL;
LP_scale_cnstr_super_tbl       SUP_CONSTRAINTS_SQL.SCALE_CNSTR_SUPER_TBL2;
/* for crossdock */
LP_xdock_from_loc_tbl          SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_TBL;

/* for elc update, ordsku delete, alloc_header delete */
LP_elc_update_tbl              SUP_CONSTRAINTS_SQL.ITEM_LIST_TBL;
LP_item_delete_tbl             SUP_CONSTRAINTS_SQL.ITEM_LIST_TBL;
LP_alloc_delete_tbl            SUP_CONSTRAINTS_SQL.ALLOC_LIST_TBL;
--------------------------------------------------------------------------------
--                      PRIVATE FUNCTION PROTOTYPES                           --
--------------------------------------------------------------------------------

FUNCTION INIT_SCALING(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION GET_SCALE_CNSTR(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN    ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--
FUNCTION FILL_SCALE_CNSTR_TBL(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION PROCESS_SCALING(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION INIT_SCALE_ITEM_LOC_TBL(O_error_message   IN OUT  rtk_errors.rtk_text%TYPE)
RETURN BOOLEAN;
--
FUNCTION LOAD_FORECAST_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION GET_ORDER_DATE(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_upto_point_date  IN      REPL_RESULTS.REPL_DATE%TYPE,
                        I_day                    IN      NUMBER,
                        O_order_date             OUT     REPL_RESULTS.REPL_DATE%TYPE)
RETURN BOOLEAN;
--
FUNCTION FETCH_FORECAST_DATA(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cur_index              IN      NUMBER,
                             I_master_item            IN      REPL_RESULTS.MASTER_ITEM%TYPE,
                             I_loc_type               IN      REPL_RESULTS.LOC_TYPE%TYPE,
                             I_location               IN      REPL_RESULTS.LOCATION%TYPE,
                             I_sub_item_loc           IN      REPL_RESULTS.LOCATION%TYPE,
                             I_repl_method            IN      REPL_RESULTS.REPL_METHOD%TYPE,
                             I_stock_cat              IN      REPL_RESULTS.STOCK_CAT%TYPE,
                             I_start_date             IN      REPL_RESULTS.REPL_DATE%TYPE,
                             I_end_date               IN      REPL_RESULTS.REPL_DATE%TYPE)
RETURN BOOLEAN;
--
FUNCTION FETCH_DAILY_FORECAST_DATA(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_cur_index              IN      NUMBER,
                                   I_master_item            IN      REPL_RESULTS.MASTER_ITEM%TYPE,
                                   I_loc_type               IN      REPL_RESULTS.LOC_TYPE%TYPE,
                                   I_location               IN      REPL_RESULTS.LOCATION%TYPE,
                                   I_sub_item_loc           IN      REPL_RESULTS.LOCATION%TYPE,
                                   I_repl_method            IN      REPL_RESULTS.REPL_METHOD%TYPE,
                                   I_stock_cat              IN      REPL_RESULTS.STOCK_CAT%TYPE,
                                   I_start_date             IN      REPL_RESULTS.REPL_DATE%TYPE,
                                   I_end_date               IN      REPL_RESULTS.REPL_DATE%TYPE)

RETURN BOOLEAN;
--
FUNCTION LOAD_CONVERS_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION CALC_BUYER_PACK_DIM_WGT(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_pack_no           IN      ITEM_MASTER.ITEM%TYPE,
                                 I_supplier          IN      ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                                 I_origin_country_id IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                 I_supp_pack_size    IN      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                 I_to_dimension_uom  IN      ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
                                 I_to_weight_uom     IN      ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                                 O_total_volume      OUT     ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE,
                                 O_total_weight      OUT     ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE,
                                 O_stat_cube         OUT     ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE)
RETURN BOOLEAN;
--
FUNCTION UOM_CONVERSION(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_from_uom          IN      VARCHAR2,
                        I_to_uom            IN      VARCHAR2,
                        I_from_value        IN      NUMBER,
                        O_to_value          OUT     NUMBER)
RETURN BOOLEAN;
--
FUNCTION SETUP_XDOCK(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION ADD_XDOCK_TO_LOC(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_xdock_from_loc_rec IN OUT  SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_REC,
                          I_cur_index          IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION ADD_XDOCK_FROM_LOC(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_xdock_from_loc_tbl  IN OUT  SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_TBL,
                            I_cur_index           IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION CALC_DIR_TO_WH_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION CALC_XDOCK_RESIDUAL_QTY(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_in_som              IN      BOOLEAN,
                                 I_xdock_from_loc      IN      SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_REC,
                                 I_from_scale_item_loc IN      SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                                 I_from_index          IN      NUMBER,
                                 O_to_scale_item_loc   IN OUT  SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                                 I_to_index            IN      NUMBER,
                                 O_total_qty_rnd_som   OUT     ORDLOC.QTY_ORDERED%TYPE,
                                 O_total_qty_rnd_case  OUT     ORDLOC.QTY_ORDERED%TYPE,
                                 O_residual_qty        OUT     ORDLOC.QTY_ORDERED%TYPE)
RETURN BOOLEAN;
--
FUNCTION ROUND_TO_STORE_ORD_MULT(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_store_ord_mult     IN      ITEM_LOC.STORE_ORD_MULT%TYPE,
                                 I_inner_pack_size    IN      ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                                 I_supp_pack_size     IN      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                 I_round_to_inner_pct IN      ITEM_SUPP_COUNTRY.ROUND_TO_INNER_PCT%TYPE,
                                 I_round_to_case_pct  IN      ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT%TYPE,
                                 I_qty                IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                 O_qty_rnd            OUT     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN;
--
FUNCTION ROUND_TO_CASE_LAYER_PALLET(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_round_lvl           IN      ITEM_SUPP_COUNTRY.ROUND_LVL%TYPE,
                                    I_round_to_case_pct   IN      ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT%TYPE,
                                    I_round_to_layer_pct  IN      ITEM_SUPP_COUNTRY.ROUND_TO_LAYER_PCT%TYPE,
                                    I_round_to_pallet_pct IN      ITEM_SUPP_COUNTRY.ROUND_TO_PALLET_PCT%TYPE,
                                    I_supp_pack_size      IN      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                    I_tier                IN      ITEM_SUPP_COUNTRY.TI%TYPE,
                                    I_pallet_size         IN      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                    I_qty                 IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                    O_qty_rnd             OUT     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN;
--
FUNCTION ROUNDING(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_num               IN OUT  NUMBER,
                  I_round_to_pct      IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION EVAL_LAYER_PALLET(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_qty_suom          IN      NUMBER,
                           I_lp_size           IN      NUMBER,
                           I_round_to_lp_pct   IN      NUMBER,
                           O_qty_eval          OUT     NUMBER,
                           O_rounded           OUT     BOOLEAN)
RETURN BOOLEAN;
--
FUNCTION ADJUST_ITEM_MIN_MAX(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION CALC_TOTAL_ITEM_RESIDUAL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_item              IN      ITEM_LOC.ITEM%TYPE,
                                  O_total_residual    IN OUT  ORDLOC.QTY_ORDERED%TYPE)
RETURN BOOLEAN;
--
FUNCTION ADJUST_CONSTRAINT(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION SETUP_SCALE_CNSTR_SUPER(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION INIT_SCALE_LOC_ARRAY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION CALC_TOTAL_CNSTR_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_scale_loc         IN OUT  SUP_CONSTRAINTS_SQL.SCALE_LOC_TBL,
                              I_scale_item_loc    IN      SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                              I_loc_index         IN      NUMBER,
                              I_sol_index         IN      NUMBER,
                              I_want_residual     IN      BOOLEAN)
RETURN BOOLEAN;
--
FUNCTION CALC_CNSTR_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_scale_cnstr_type  IN      ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                        I_cur_index         IN      NUMBER,
                        I_qty               IN      NUMBER,
                        O_qty               OUT     NUMBER)
RETURN BOOLEAN;
--
FUNCTION CALC_TOTAL_RESIDUAL_CNSTR_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_scale_cnstr_type  IN      ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                                       I_location          IN      ITEM_LOC.LOC%TYPE,
                                       O_total_cnstr_qty   OUT     NUMBER)
RETURN BOOLEAN;
--
FUNCTION CALC_SUPER_MIN_MAX(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION SCALE_PROCESS(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION RANK_SOLUTION(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_scale_loc         IN OUT  SUP_CONSTRAINTS_SQL.SCALE_LOC_TBL,
                       I_loc_index         IN      NUMBER,
                       I_sol_index         IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION EVAL_RANK(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   I_qty               IN      NUMBER,
                   I_min               IN      NUMBER,
                   I_min_tol           IN      NUMBER,
                   I_max               IN      NUMBER,
                   I_max_tol           IN      NUMBER,
                   O_rank              OUT     VARCHAR)
RETURN BOOLEAN;
--
FUNCTION GET_RANK_ORDER(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_rank_code         IN      VARCHAR2,
                        O_rank_order        IN OUT  NUMBER,
                        O_accept_ind        OUT     VARCHAR2)
RETURN BOOLEAN;
--
FUNCTION CHECK_DIRECTION(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rank_1            IN      VARCHAR2,
                         I_rank_2            IN      VARCHAR2,
                         I_changeable        IN      VARCHAR2,
                         I_scale_dir         IN      NUMBER,
                         O_eval_result       OUT     NUMBER)
RETURN BOOLEAN;
--
FUNCTION GET_EVAL_SYSTEM(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rank_1            IN      VARCHAR2,
                         I_rank_2            IN      VARCHAR2,
                         O_direction         OUT     NUMBER)
RETURN BOOLEAN;
--
FUNCTION UPDATE_ORD_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_scale_loc         IN OUT  SUP_CONSTRAINTS_SQL.SCALE_LOC_TBL,
                        I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                        I_item              IN      ITEM_LOC.ITEM%TYPE,
                        I_location          IN      ITEM_LOC.LOC%TYPE,
                        I_loc_index         IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION SCALE(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
               I_loc_index         IN      NUMBER,
               O_sol_day           OUT     NUMBER,
               O_sol_case          OUT     NUMBER,
               O_cnt               IN OUT  NUMBER)
RETURN BOOLEAN;
--
FUNCTION COPY_FROM_PREV_ITER(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_scale_loc_tbl      IN OUT  SUP_CONSTRAINTS_SQL.SCALE_LOC_TBL,
                             O_scale_item_loc_tbl IN OUT  SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                             I_loc_index          IN      NUMBER,
                             I_sol_index          IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION GEN_ONE_DAY_SUPPLY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_loc_index         IN      NUMBER,
                            I_day               IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION GET_FORECAST_DATA(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cur_index         IN      NUMBER,
                           I_order_date        IN      ORDHEAD.WRITTEN_DATE%TYPE,
                           O_sales_qty         OUT     ORDLOC.QTY_ORDERED%TYPE)
RETURN BOOLEAN;
--
FUNCTION GET_DAILY_FORECAST_DATA(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cur_index         IN      NUMBER,
                                 I_order_date        IN      ORDHEAD.WRITTEN_DATE%TYPE,
                                 O_sales_qty         OUT     ORDLOC.QTY_ORDERED%TYPE,
                                 O_daily_data_found  OUT     BOOLEAN)
RETURN BOOLEAN;
--
FUNCTION EVAL_NON_SCALE_CNSTR(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_direction         IN      NUMBER,
                              I_day_case          IN      NUMBER,
                              O_scale_item_loc    IN OUT  SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                              I_cur_index         IN      NUMBER,
                              I_sol_index         IN      NUMBER,
                              I_proposed_qty      IN OUT  NUMBER,
                              O_scalable          OUT     BOOLEAN)
RETURN BOOLEAN;
--
FUNCTION EVAL_SOLUTION(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_scale_loc         IN OUT  SUP_CONSTRAINTS_SQL.SCALE_LOC_TBL,
                       I_loc_index         IN      NUMBER,
                       I_last_day          IN      NUMBER,
                       O_sol_day           IN OUT  NUMBER)
RETURN BOOLEAN;
--
FUNCTION CASE_PALLET_ROUND_ALL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_loc_index         IN      NUMBER,
                               I_sol_day           IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION CALC_XDOCK_QTY(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_in_som              IN      BOOLEAN,
                        I_xdock_from_loc_rec  IN      SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_REC,
                        I_from_scale_item_loc IN      SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                        I_from_index          IN      NUMBER,
                        O_to_scale_item_loc   IN OUT  SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                        I_to_index            IN      NUMBER,
                        O_total_qty_rnd_case  IN OUT  NUMBER,
                        I_item                IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
--
FUNCTION TRUNCATE_SOLUTION_DAY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_sol_day           IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION GEN_ONE_CASE_SUPPLY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_loc_index         IN      NUMBER,
                             I_sol_day           IN      NUMBER,
                             I_cur_index         IN      NUMBER,
                             I_sol_index         IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION WRITE_SCALED_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_loc_index         IN      NUMBER,
                          I_sol_case          IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION CALC_FINAL_SOURCE_WH_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION ADD_SOURCE_WH(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item              IN      REPL_ITEM_LOC.ITEM%TYPE,
                       I_source_wh         IN      REPL_ITEM_LOC.SOURCE_WH%TYPE,
                       I_wh_cur_index      IN      NUMBER,
                       I_xdock_cur_index   IN      NUMBER,
                       I_total_qty         IN      NUMBER) 
RETURN BOOLEAN;
--
FUNCTION OUTPUT_SCALING(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION CREATE_ORDSKU(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                       I_cur_index         IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION GET_HTS_CHAPTER_DOC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                             I_item              IN      ITEM_MASTER.ITEM%TYPE,
                             I_import_country_id IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
--
FUNCTION CREATE_ORDLOC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no          IN      ORDLOC.ORDER_NO%TYPE,
                       I_cur_index         IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION UPDATE_ORDLOC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no          IN      ORDLOC.ORDER_NO%TYPE,
                       I_item              IN      ORDLOC.ITEM%TYPE,
                       I_location          IN      ORDLOC.LOCATION%TYPE,
                       I_loc_type          IN      ORDLOC.LOC_TYPE%TYPE,
                       I_update_qty        IN      ORDLOC.QTY_ORDERED%TYPE)
RETURN BOOLEAN;
--
FUNCTION DELETE_ORDLOC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                       I_item              IN      ORDLOC.ITEM%TYPE,
                       I_location          IN      ORDLOC.LOCATION%TYPE,
                       I_loc_type          IN      ORDLOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
--
FUNCTION PROCESS_CROSSDOCK(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cur_index         IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION OUTPUT_ONE_XDOCK(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cur_index         IN      NUMBER,
                          O_alloc_no          IN OUT  ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
--
FUNCTION CREATE_ALLOC_HEADER(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                             I_item              IN      ITEM_MASTER.ITEM%TYPE,
                             I_source_wh         IN      REPL_ITEM_LOC.SOURCE_WH%TYPE,
                             O_alloc_no          OUT     ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
--
FUNCTION CREATE_ALLOC_DETAIL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_alloc_no          IN      ALLOC_DETAIL.ALLOC_NO%TYPE,
                             I_from_loc          IN      ALLOC_DETAIL.TO_LOC%TYPE,
                             I_to_loc            IN      ALLOC_DETAIL.TO_LOC%TYPE,
                             I_to_loc_type       IN      ALLOC_DETAIL.TO_LOC_TYPE%TYPE,
                             I_item              IN      ITEM_MASTER.ITEM%TYPE,
                             I_qty_prescaled     IN      ALLOC_DETAIL.QTY_PRESCALED%TYPE,
                             I_qty_scaled        IN      ALLOC_DETAIL.QTY_PRESCALED%TYPE)
RETURN BOOLEAN;
--
FUNCTION UPDATE_ALLOC_DETAIL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_alloc_no          IN      ALLOC_DETAIL.ALLOC_NO%TYPE,
                             I_loc_type          IN      ALLOC_DETAIL.TO_LOC_TYPE%TYPE,
                             I_location          IN      ALLOC_DETAIL.TO_LOC%TYPE,
                             I_qty_scaled        IN      ALLOC_DETAIL.QTY_PRESCALED%TYPE)
RETURN BOOLEAN;
--
FUNCTION DELETE_ALLOC_DETAIL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_alloc_no          IN      ALLOC_DETAIL.ALLOC_NO%TYPE,
                             I_loc_type          IN      ALLOC_DETAIL.TO_LOC_TYPE%TYPE,
                             I_location          IN      ALLOC_DETAIL.TO_LOC%TYPE)
RETURN BOOLEAN;
--
FUNCTION UPDATE_REPL_RESULTS(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                             I_cur_index         IN      NUMBER)
RETURN BOOLEAN;
--
FUNCTION UPDATE_ELC_ALL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION DELETE_ORDSKU_ALL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION DELETE_ALLOC_HEADER_ALL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--
FUNCTION GET_BRACKET_INFO(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_bracket_val1      OUT     NUMBER,
                          O_bracket_type1     OUT     VARCHAR2,
                          O_bracket_uom1      OUT     VARCHAR2,
                          O_next_bracket      OUT     BOOLEAN)
RETURN BOOLEAN;
--
FUNCTION UPDATE_GP_ROUND_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no          IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--
FUNCTION UPDATE_TRUCKLOAD(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                          I_num_truck_load    IN      NUMBER)
RETURN BOOLEAN;
--

-------------------------------------------------------------------------------
FUNCTION SCALING_CNSTR(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no        IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BINARY_INTEGER IS
   L_function          VARCHAR2(40) := 'SUP_CONSTRAINTS_SQL.SCALING_CNSTR';
BEGIN
  
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            TO_CHAR(I_order_no),
                                            L_function,
                                            NULL);
      return -1;
   else
      LP_order_no := I_order_no;
   end if;
   if INIT_SCALING(O_error_message) = FALSE then
      if LP_NON_FATAL then
         return 1;
      else
         return -1;
      end if;
   end if;
   
   if PROCESS_SCALING(O_error_message) = FALSE then
      if LP_NON_FATAL then
         return 1;
      else
         return -1;
      end if;
   end if;
   
   return 0;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return -1;
END SCALING_CNSTR;
-------------------------------------------------------------------------------
FUNCTION INIT_SCALING(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function          VARCHAR2(40) := 'SUP_CONSTRAINTS_SQL.INIT_SCALING';
   
   cursor C_SYSTEM_OPTIONS is
     select bracket_costing_ind,
            elc_ind,
            alloc_method,
            max_scaling_iterations
       from system_options;
   
BEGIN  
   SQL_LIB.SET_MARK('OPEN','C_SYSTEM_OPTIONS','SYSTEM_OPTIONS',NULL);  
   open C_SYSTEM_OPTIONS;
   SQL_LIB.SET_MARK('FETCH','C_SYSTEM_OPTIONS','SYSTEM_OPTIONS',NULL);  
   fetch C_SYSTEM_OPTIONS into LP_bracket_costing_ind,
                               LP_elc_ind,
                               LP_alloc_method,
                               LP_max_scaling_iterations;
   SQL_LIB.SET_MARK('CLOSE','C_SYSTEM_OPTIONS','SYSTEM_OPTIONS',NULL);  
   close C_SYSTEM_OPTIONS;
   
  /* Initialize rank priority table and eval system table */
  /* s_rank_codes -- i_rank_order -- s_accept_ind 
     "GG",           0,              "Y",
     "GY",           1,              "Y",
     "YG",           2,              "Y",
     "YY",           3,              "Y",
     "GR",           4,              "N",
     "YR",           5,              "N",
     "RG",           6,              "N",
     "RY",           7,              "N",
     "RR",           8,              "N",
     "G",            0,              "Y",
     "Y",            1,              "Y",
     "R",            2,              "N"         */
   LP_rank_priority_tbl(1).rank_code  := 'GG';
   LP_rank_priority_tbl(1).rank_order := 0;
   LP_rank_priority_tbl(1).accept_ind := 'Y';
   LP_rank_priority_tbl(2).rank_code  := 'GY';
   LP_rank_priority_tbl(2).rank_order := 1;
   LP_rank_priority_tbl(2).accept_ind := 'Y';   
   LP_rank_priority_tbl(3).rank_code  := 'YG';
   LP_rank_priority_tbl(3).rank_order := 2;
   LP_rank_priority_tbl(3).accept_ind := 'Y';
   LP_rank_priority_tbl(4).rank_code  := 'YY';
   LP_rank_priority_tbl(4).rank_order := 3;
   LP_rank_priority_tbl(4).accept_ind := 'Y';
   LP_rank_priority_tbl(5).rank_code  := 'GR';
   LP_rank_priority_tbl(5).rank_order := 4;
   LP_rank_priority_tbl(5).accept_ind := 'N';
   LP_rank_priority_tbl(6).rank_code  := 'YR';
   LP_rank_priority_tbl(6).rank_order := 5;
   LP_rank_priority_tbl(6).accept_ind := 'N';   
   LP_rank_priority_tbl(7).rank_code  := 'RG';
   LP_rank_priority_tbl(7).rank_order := 6;
   LP_rank_priority_tbl(7).accept_ind := 'N';
   LP_rank_priority_tbl(8).rank_code  := 'RY';
   LP_rank_priority_tbl(8).rank_order := 7;
   LP_rank_priority_tbl(8).accept_ind := 'N';
   LP_rank_priority_tbl(9).rank_code  := 'RR';
   LP_rank_priority_tbl(9).rank_order := 8;
   LP_rank_priority_tbl(9).accept_ind := 'N';   
   LP_rank_priority_tbl(10).rank_code  := 'G';
   LP_rank_priority_tbl(10).rank_order := 0;
   LP_rank_priority_tbl(10).accept_ind := 'Y';
   LP_rank_priority_tbl(11).rank_code  := 'Y';
   LP_rank_priority_tbl(11).rank_order := 1;
   LP_rank_priority_tbl(11).accept_ind := 'Y';
   LP_rank_priority_tbl(12).rank_code  := 'R';
   LP_rank_priority_tbl(12).rank_order := 2;
   LP_rank_priority_tbl(12).accept_ind := 'N';

/* s_rank_1 -- s_rank_2 -- i_direction */
     /*RL primary rank section 
     "RL",       "RL",       UP,
     "RL",       "YL",       UP,
     "RL",       "RH",       NO,
     "RL",       "YH",       UP, */
     
   LP_eval_system_tbl(1).rank_1    := 'RL';
   LP_eval_system_tbl(1).rank_2    := 'RL';
   LP_eval_system_tbl(1).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP;
   LP_eval_system_tbl(2).rank_1    := 'RL';
   LP_eval_system_tbl(2).rank_2    := 'YL';
   LP_eval_system_tbl(2).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP;
   LP_eval_system_tbl(3).rank_1    := 'RL';
   LP_eval_system_tbl(3).rank_2    := 'RH';
   LP_eval_system_tbl(3).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_NO;
   LP_eval_system_tbl(4).rank_1    := 'RL';
   LP_eval_system_tbl(4).rank_2    := 'YH';
   LP_eval_system_tbl(4).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP; 
     /*YL primary rank section 
     "YL",       "RL",       UP,
     "YL",       "YL",       UP,
     "YL",       "RH",       DOWN,
     "YL",       "YH",       UP, */
   LP_eval_system_tbl(5).rank_1    := 'YL';
   LP_eval_system_tbl(5).rank_2    := 'RL';
   LP_eval_system_tbl(5).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP;
   LP_eval_system_tbl(6).rank_1    := 'YL';
   LP_eval_system_tbl(6).rank_2    := 'YL';
   LP_eval_system_tbl(6).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP;
   LP_eval_system_tbl(7).rank_1    := 'YL';
   LP_eval_system_tbl(7).rank_2    := 'RH';
   LP_eval_system_tbl(7).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN;
   LP_eval_system_tbl(8).rank_1    := 'YL';
   LP_eval_system_tbl(8).rank_2    := 'YH';
   LP_eval_system_tbl(8).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP; 
     /*RH primary rank section 
     "RH",       "RL",       NO,
     "RH",       "YL",       DOWN,
     "RH",       "RH",       DOWN,
     "RH",       "YH",       DOWN, */
   LP_eval_system_tbl(9).rank_1    := 'RH';
   LP_eval_system_tbl(9).rank_2    := 'RL';
   LP_eval_system_tbl(9).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_NO;
   LP_eval_system_tbl(10).rank_1    := 'RH';
   LP_eval_system_tbl(10).rank_2    := 'YL';
   LP_eval_system_tbl(10).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN;
   LP_eval_system_tbl(11).rank_1    := 'RH';
   LP_eval_system_tbl(11).rank_2    := 'RH';
   LP_eval_system_tbl(11).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN;
   LP_eval_system_tbl(12).rank_1    := 'RH';
   LP_eval_system_tbl(12).rank_2    := 'YH';
   LP_eval_system_tbl(12).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN;  
     /*YH primary rank section 
     "YH",       "RL",       UP,
     "YH",       "YL",       DOWN,
     "YH",       "RH",       DOWN,
     "YH",       "YH",       DOWN */   
   LP_eval_system_tbl(13).rank_1    := 'YH';
   LP_eval_system_tbl(13).rank_2    := 'RL';
   LP_eval_system_tbl(13).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP;
   LP_eval_system_tbl(14).rank_1    := 'YH';
   LP_eval_system_tbl(14).rank_2    := 'YL';
   LP_eval_system_tbl(14).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN;
   LP_eval_system_tbl(15).rank_1    := 'YH';
   LP_eval_system_tbl(15).rank_2    := 'RH';
   LP_eval_system_tbl(15).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN;
   LP_eval_system_tbl(16).rank_1    := 'YH';
   LP_eval_system_tbl(16).rank_2    := 'YH';
   LP_eval_system_tbl(16).direction := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN;   
   
   if GET_SCALE_CNSTR(O_error_message,
                      LP_order_no) = FALSE then
      return FALSE;
   end if;
   
   if FILL_SCALE_CNSTR_TBL(O_error_message) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INIT_SCALING;
-------------------------------------------------------------------------------
FUNCTION GET_SCALE_CNSTR(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN    ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_function          VARCHAR2(40) := 'SUP_CONSTRAINTS_SQL.GET_SCALE_CNSTR';

   cursor C_ORD_INV_MGMT is 
      select scale_cnstr_ind,
             scale_cnstr_lvl,
             scale_cnstr_obj,
             scale_cnstr_type1,
             scale_cnstr_uom1,
             scale_cnstr_curr1,
             NVL(scale_cnstr_min_val1, 0),
             NVL(scale_cnstr_max_val1, 0),
             NVL(scale_cnstr_min_tol1, 0),
             NVL(scale_cnstr_max_tol1, 0),
             scale_cnstr_type2,
             scale_cnstr_uom2,
             scale_cnstr_curr2,
             NVL(scale_cnstr_min_val2, 0),
             NVL(scale_cnstr_max_val2, 0),
             NVL(scale_cnstr_min_tol2, 0),
             NVL(scale_cnstr_max_tol2, 0),
             max_scaling_iterations,
             due_ord_process_ind,
             mult_vehicle_ind
        from ord_inv_mgmt
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ORD_INV_MGMT','ORD_INV_MGMT','Order_no:' || I_order_no);  
   open C_ORD_INV_MGMT;
   SQL_LIB.SET_MARK('FETCH','C_ORD_INV_MGMT','ORD_INV_MGMT','Order_no:' || I_order_no);  
   fetch C_ORD_INV_MGMT into LP_ord_inv_mgmt_rec;
  
   if C_ORD_INV_MGMT%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_NO_REC_ORD','ORD_INV_MGMT',TO_CHAR(I_order_no),NULL);
      SQL_LIB.SET_MARK('CLOSE','C_ORD_INV_MGMT','ORD_INV_MGMT','Order_no:' || I_order_no);  
      close C_ORD_INV_MGMT;
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_ORD_INV_MGMT','ORD_INV_MGMT','Order_no:' || I_order_no);  
   close C_ORD_INV_MGMT;

   if (LP_ord_inv_mgmt_rec.scale_cnstr_ind = 'N' or
       LP_ord_inv_mgmt_rec.scale_cnstr_type1 is NULL or
       LP_ord_inv_mgmt_rec.scale_cnstr_min_val1 = 0 and
       LP_ord_inv_mgmt_rec.scale_cnstr_max_val1 = 0) then
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_ORD_NOT_DEF',
                                             TO_CHAR(LP_order_no),
                                             NULL,
                                             NULL);
      LP_NON_FATAL := TRUE;
      return FALSE;
   end if;
   
   if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl not in ('O','L')) then
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_SCL_CNSTR_DEF',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_NON_FATAL := TRUE;
      return FALSE;      
   end if;
   
   if (LP_ord_inv_mgmt_rec.scale_cnstr_obj not in ('M','X')) then
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_SCL_OBJ_DEF',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_NON_FATAL := TRUE;
      return FALSE;      
   end if;
   
   /* determine the max scaling iterations: min of max_scaling_iterations on */
   /* system_options and ord_inv_mgmt if defined;otherwise MAX_SCALE_ITERATION */
   if LP_ord_inv_mgmt_rec.max_scaling_iterations is NOT NULL then
      if (LP_max_scaling_iterations is NOT NULL and
          LP_max_scaling_iterations < LP_ord_inv_mgmt_rec.max_scaling_iterations) then
         LP_ord_inv_mgmt_rec.max_scaling_iterations := LP_max_scaling_iterations;
      end if;
   else
      LP_ord_inv_mgmt_rec.max_scaling_iterations := NVL(LP_max_scaling_iterations,MAX_SCALE_ITERATION);
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_SCALE_CNSTR;
-------------------------------------------------------------------------------
FUNCTION FILL_SCALE_CNSTR_TBL(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function          VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.FILL_SCALE_CNSTR_TBL';

BEGIN
   /*
    * Function copies scaling constraints to a Global collection. This is for
    * easy maintenance if Oracle Retail decides to support more than 2 scaling cnstrs.
    * Validation is done for each constraint type, including:
    * 1) if a "P"allet cnstr is defined, set program scaling round lvl to "P";
    * 2) if a "M"ass or "V"olume cnstr is defined, scaling uom must be defined;
    * 3) if an "A"mount cnstr is defined, scaling currency must be defined;
    * 4) scaling max value must be >0 unless scaling objective is Min and single
    *    truck load;
    * 5) scaling min value must be >0 if scaling objective is Min
    */
   LP_total_scale_cnstr := 0;
   LP_scale_cnstr_tbl(1).scale_cnstr_type := LP_ord_inv_mgmt_rec.scale_cnstr_type1;
   if LP_ord_inv_mgmt_rec.scale_cnstr_type1 in ('M','V') then 
      if LP_ord_inv_mgmt_rec.scale_cnstr_uom1 is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_UOM_DEF',
                                                NULL,
                                                NULL,
                                                NULL);
         LP_NON_FATAL := TRUE;
         return FALSE;
      else 
         LP_scale_cnstr_tbl(1).scale_cnstr_uom := LP_ord_inv_mgmt_rec.scale_cnstr_uom1;
      end if;
   end if;
   if LP_ord_inv_mgmt_rec.scale_cnstr_type1 = 'A' then
      if LP_ord_inv_mgmt_rec.scale_cnstr_curr1 is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_CURR_DEF',NULL,NULL,NULL);
         LP_NON_FATAL := TRUE;
         return FALSE;
      else
         LP_scale_cnstr_tbl(1).scale_cnstr_curr := LP_ord_inv_mgmt_rec.scale_cnstr_curr1;
      end if;
   end if;
   if LP_ord_inv_mgmt_rec.scale_cnstr_max_val1 = 0 then
      if LP_ord_inv_mgmt_rec.mult_vehicle_ind = 'N' and 
         LP_ord_inv_mgmt_rec.scale_cnstr_obj = 'M' then
         LP_ord_inv_mgmt_rec.scale_cnstr_max_val1 := MAX_VALUE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_MAX_VAL_DEF',NULL,NULL,NULL);
         LP_NON_FATAL := TRUE;
         return FALSE;
      end if;
   end if;
   if (LP_ord_inv_mgmt_rec.scale_cnstr_obj = 'M' and 
       LP_ord_inv_mgmt_rec.scale_cnstr_min_val1 = 0) then
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_MIN_VAL_DEF',NULL,NULL,NULL);
      LP_NON_FATAL := TRUE;
      return FALSE;  
   end if;
   LP_scale_cnstr_tbl(1).scale_cnstr_min_val := LP_ord_inv_mgmt_rec.scale_cnstr_min_val1;
   LP_scale_cnstr_tbl(1).scale_cnstr_max_val := LP_ord_inv_mgmt_rec.scale_cnstr_max_val1;
   LP_scale_cnstr_tbl(1).scale_cnstr_min_tol := LP_ord_inv_mgmt_rec.scale_cnstr_min_tol1;
   LP_scale_cnstr_tbl(1).scale_cnstr_max_tol := LP_ord_inv_mgmt_rec.scale_cnstr_max_tol1;
   
   LP_total_scale_cnstr := LP_total_scale_cnstr + 1;
   
   if (LP_ord_inv_mgmt_rec.scale_cnstr_type2 is NULL or
       (LP_ord_inv_mgmt_rec.scale_cnstr_min_val2 = 0 and
        LP_ord_inv_mgmt_rec.scale_cnstr_max_val2 = 0)) then
      return TRUE;
   end if;

   LP_scale_cnstr_tbl(2).scale_cnstr_type := LP_ord_inv_mgmt_rec.scale_cnstr_type2;
   if LP_ord_inv_mgmt_rec.scale_cnstr_type2 in ('M','V') then 
      if LP_ord_inv_mgmt_rec.scale_cnstr_uom2 is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_UOM_DEF',NULL,NULL,NULL);
         LP_NON_FATAL := TRUE;
         return FALSE;
      else 
         LP_scale_cnstr_tbl(2).scale_cnstr_uom := LP_ord_inv_mgmt_rec.scale_cnstr_uom2;
      end if;
   end if;
   if LP_ord_inv_mgmt_rec.scale_cnstr_type2 = 'A' then
      if LP_ord_inv_mgmt_rec.scale_cnstr_curr2 is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_CURR_DEF',NULL,NULL,NULL);
         LP_NON_FATAL := TRUE;
         return FALSE;
      else
         LP_scale_cnstr_tbl(2).scale_cnstr_curr := LP_ord_inv_mgmt_rec.scale_cnstr_curr2;
      end if;
   end if;
   if LP_ord_inv_mgmt_rec.scale_cnstr_max_val2 = 0 then
      if LP_ord_inv_mgmt_rec.mult_vehicle_ind = 'N' and 
         LP_ord_inv_mgmt_rec.scale_cnstr_obj = 'M' then
         LP_ord_inv_mgmt_rec.scale_cnstr_max_val2 := MAX_VALUE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_MAX_VAL_DEF',NULL,NULL,NULL);
         LP_NON_FATAL := TRUE;
         return FALSE;
      end if;
   end if;
   if (LP_ord_inv_mgmt_rec.scale_cnstr_obj = 'M' and 
       LP_ord_inv_mgmt_rec.scale_cnstr_min_val2 = 0) then
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_MIN_VAL_DEF',NULL,NULL,NULL);
      LP_NON_FATAL := TRUE;
      return FALSE;  
   end if;   
   
   LP_scale_cnstr_tbl(2).scale_cnstr_min_val := LP_ord_inv_mgmt_rec.scale_cnstr_min_val2;
   LP_scale_cnstr_tbl(2).scale_cnstr_max_val := LP_ord_inv_mgmt_rec.scale_cnstr_max_val2;
   LP_scale_cnstr_tbl(2).scale_cnstr_min_tol := LP_ord_inv_mgmt_rec.scale_cnstr_min_tol2;
   LP_scale_cnstr_tbl(2).scale_cnstr_max_tol := LP_ord_inv_mgmt_rec.scale_cnstr_max_tol2;
   
   LP_total_scale_cnstr := LP_total_scale_cnstr + 1;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FILL_SCALE_CNSTR_TBL;
-------------------------------------------------------------------------------
FUNCTION PROCESS_SCALING(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(40) := 'SUP_CONSTRAINTS_SQL.PROCESS_SCALING';
   
   L_bracket_val                  NUMBER(12,4) := 0;
   L_bracket_type                 VARCHAR2(6);
   L_bracket_uom                  VARCHAR2(6);
   L_bracket_exists               BOOLEAN;
   
   /*
    * Cursor for due order processing consists of three parts:
    * 1) non-crossdock manual and replenished orders that have order_roq > 0
    *    (They will have ordloc, but may or may not have repl_results.)
    * 2) crossdock manual and replenished orders that have order_roq > 0
    *    (They will have alloc_detail, but may or may not have repl_results.)
    * 3) replenished orders that have order_roq <= 0
    *    (They only have repl_results and a dummy ordhead for written_date.)
    */
    CURSOR C_DUE_ORD_PROC IS
     SELECT 'Y',               /* with ordloc */
            ol.item,
            NVL(w.physical_wh, -1),
            -1,                /* x-docked from location */
            ol.loc_type,
            ol.location,
            NVL(w.rounding_seq, ol.location),
            ol.qty_prescaled,
            rr.orig_raw_roq_pack,
            os.non_scale_ind,
            ol.non_scale_ind,
            im.pack_ind,
            im.dept,
            /* get supplier unit cost and supplier currency code */
            iscl.unit_cost,
            s.currency_code,
            /* get order currency code and exchange rate */
            oh.currency_code,
            oh.exchange_rate,
            /* get item level order constraint and dimension data from isc */
            isc.min_order_qty,
            isc.max_order_qty,
            iscd.length,
            iscd.height,
            iscd.width,
            iscd.lwh_uom,
            iscd.weight,
            iscd.weight_uom,
            iscd.stat_cube, /* stat case */
            nvl(os.supp_pack_size,isc.supp_pack_size),
            ((isc.ti * isc.hi)*nvl(os.supp_pack_size,isc.supp_pack_size)),
            (isc.ti*nvl(os.supp_pack_size,isc.supp_pack_size)),
            isc.inner_pack_size,
            /* get replenish data from repl_results */
            rr.master_item,    /* master item of the simple packs */
            rr.item_type,
            rr.stock_cat,
            rr.repl_method,
            rr.due_ind,
            /* the order point holds the minimum replenishment value.  Subtracting */
            /* the net inventory results in the quantity needed to reach the       */
            /* minimum.                                                            */
            rr.order_point - rr.net_inventory,

            /* for calculating the forecasting date range to be loaded into memory*/
            /* forecast start date = order written date */
            oh.written_date,

            /*
             * calculate order lead time date (OLTD):
             * OLTD = written date+lead time(OLT)
             * Depending on the stock_cat, calculate OLT as follows:
             * For crossdock (stock_cat = 'C'):
             *    OLT = supp_lead_time+pickup_lead_time+wh_lead_time
             * For others:
             *    OLT = supp_lead_time+pickup_lead_time
             * (loc_type='S', stock_cat='W' won't occur here)
             */
            oh.written_date+
                    DECODE(rr.stock_cat,
                           'C', (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0)+
                                 NVL(rr.wh_lead_time,0)),
                                (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0))),

            /*
             * calculate order upto point date (OUPD):
             * For repl_method of 'T' (Time Supply, Time Supply Seasonal) and
             * 'TI' (Time Supply Issue):
             * OUPD = written date + lead time(OLT) +
             *        MAX(review time(RT), max supply days(MSD))
             * For 'D' (Dynamic and Dynamic Seasonal),
             * 'DI' (Dynamic Issues) repl_methods:
             * OUPD = written date + MAX(curr order lead time (COLT) + inventory selling days(ISD),
             *                                                  next order lead time (NOLT) + review time (RT))
             * For 'F'(Floating Point) repl_methods:
             * OUPD = written date + lead time(NOLT) +
             *        MAX(review time(RT), inventory selling days(ISD))
             * For other repl_methods:
             * OUPD = written date + lead time(OLT) +
             *        MAX(review time(RT), inventory selling days(ISD))
             */
            oh.written_date+
                    DECODE(rr.repl_method,
                           'D',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                                      -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                          (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                          (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                                  -1, NVL(rr.review_time,0),
                                                      NVL(rr.inv_selling_days,0))),
                           'DI',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                                      -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                          (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                          (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                                  -1, NVL(rr.review_time,0),
                                                      NVL(rr.inv_selling_days,0))),
                           'F',rr.next_order_lead_time,
                    DECODE(rr.stock_cat,
                           'C', (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0)+
                                 NVL(rr.wh_lead_time,0)),
                                (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0))))+
                    DECODE(rr.repl_method,
                           'T',  DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.max_supply_days,0)),
                           'TI', DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.max_supply_days,0)),
                                 DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.inv_selling_days,0))),

            /*
             * To prevent order scaling before order written date, calculate:
             * For repl_method of 'T' and 'TI':
             * max num of days the order/item/loc can be scaled down =
             *   order upto point date(OUPD) - order written date =
             *   lead time(OLT) + MAX(review time(RT), max supply days(MSD))
             * For repl_method of 'D', 'DI', and 'F;:
             * max num of days the order/itme/loc can be scaled down =
             *   order upto point date(OUPD) - order written date =
             *   lead time(NOLT) + MAX(review time(RT), inventory selling days(ISD))
             * For other repl_methods:
             * max num of days the order/item/loc can be scaled down =
             *   order upto point date(OUPD) - order written date =
             *   lead time(OLT) + MAX(review time(RT), inventory selling days(ISD))
             */
            DECODE(rr.repl_method,
                   'D',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                              -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                  (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                  (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                          -1, NVL(rr.review_time,0),
                                              NVL(rr.inv_selling_days,0))),
                   'DI',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                              -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                  (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                  (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                          -1, NVL(rr.review_time,0),
                                              NVL(rr.inv_selling_days,0))),
                   'F',rr.next_order_lead_time,
            DECODE(rr.stock_cat,
                   'C', (NVL(rr.supp_lead_time,0)+
                         NVL(rr.pickup_lead_time,0)+
                         NVL(rr.wh_lead_time,0)),
                        (NVL(rr.supp_lead_time,0)+
                         NVL(rr.pickup_lead_time,0))))+
            DECODE(rr.repl_method,
                   'T',  DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.max_supply_days,0)),
                   'TI', DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.max_supply_days,0)),
                         DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.inv_selling_days,0))),

            rr.season_id,
            rr.phase_id,
            NVL(rr.max_scale_value,0),
            /* get fields for order writeout */
            iscl.supplier,
            iscl.origin_country_id,
            oh.import_order_ind,
            oh.import_country_id,
            oh.earliest_ship_date,
            oh.latest_ship_date,
            -1,                    /* alloc_no */
            ol.qty_ordered,
            /* since only approved items can be on orders, we are trusting that the rounding */
            /* information has been all pushed down to the item/supp/country/loc level.  NVL */
            /* used for the sole purpose of handling bad data.                               */
            NVL(iscl.round_lvl, 'BAD'),
            NVL(iscl.round_to_inner_pct, -1)/100,
            NVL(iscl.round_to_case_pct, -1)/100,
            NVL(iscl.round_to_layer_pct, -1)/100,
            NVL(iscl.round_to_pallet_pct, -1)/100,
            NULL as non_scale_ind,
            NULL as store_close_date,
            NULL as store_ord_mult,
            NULL as buyer_pack_ind,
            NULL as uom_conv_factor,
            NULL as min_scale_value,
            NULL as volume,
            NULL as ord_unit_cost,
            NULL as pack_item_qty,
            NULL as break_pack_ind,
            NULL as scaled_ind,
            NULL as qty_scled_sol
       FROM ordhead oh,
            ordsku os,
            ordloc ol,
            item_master im,
            item_supp_country isc,
            item_supp_country_dim iscd,
            sups s,
            repl_results rr,
            wh w,
            item_supp_country_loc iscl
      WHERE oh.order_no = LP_order_no
        AND os.order_no = oh.order_no
        AND ol.order_no = os.order_no
        AND ol.item = os.item
        AND im.item = ol.item
        AND im.orderable_ind = 'Y'
        AND isc.item = os.item
        AND isc.item = iscd.item(+)
        AND isc.item = iscl.item
        AND isc.supplier = oh.supplier
        AND isc.supplier = iscd.supplier(+)
        AND isc.supplier = iscl.supplier
        AND isc.origin_country_id = os.origin_country_id
        AND isc.origin_country_id = iscd.origin_country(+)
        AND isc.origin_country_id = iscl.origin_country_id
        AND s.supplier = isc.supplier
        AND rr.order_no(+) = ol.order_no
        AND rr.item(+) = ol.item
        AND rr.location(+) = ol.location
        AND rr.loc_type(+) = ol.loc_type
        AND w.wh(+) = ol.location
        AND ol.location = iscl.loc
        AND iscd.dim_object(+) = 'CA' /* cases only */
      UNION
     SELECT 'Y',                                   /* with alloc_detail */
            ol.item,
            NVL(w.physical_wh, -1),
            ah.wh,                                 /* x-docked from location */
            ad.to_loc_type,
            ad.to_loc,                             /* x-docked to location */
            NVL(w.rounding_seq, ol.location),
            ad.qty_prescaled,
            rr.orig_raw_roq_pack,
            os.non_scale_ind,
            ad.non_scale_ind,
            im.pack_ind,
            im.dept,
            /* get supplier unit cost and supplier currency code */
            iscl.unit_cost,
            s.currency_code,
            /* get order currency code and exchange rate */
            oh.currency_code,
            oh.exchange_rate,
            /* get item level order constraint and dimension data from isc */
            isc.min_order_qty,
            isc.max_order_qty,
            iscd.length,
            iscd.height,
            iscd.width,
            iscd.lwh_uom,
            iscd.weight,
            iscd.weight_uom,
            iscd.stat_cube, /* stat case */
            nvl(os.supp_pack_size,isc.supp_pack_size),
            ((isc.ti * isc.hi)*nvl(os.supp_pack_size,isc.supp_pack_size)),
            (isc.ti*nvl(os.supp_pack_size,isc.supp_pack_size)),
            isc.inner_pack_size,
            /* get replenish data from repl_results */
            rr.master_item,    /* master item of the simple packs */
            rr.item_type,
            rr.stock_cat,
            rr.repl_method,
            rr.due_ind,

            /* the order point holds the minimum replenishment value.  Subtracting */
            /* the net inventory results in the quantity needed to reach the       */
            /* minimum.                                                            */
            rr.order_point - rr.net_inventory,

            /* forecast start date */
            oh.written_date,

            /* order lead time date */
            oh.written_date+
                    DECODE(rr.stock_cat,
                           'C', (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0)+
                                 NVL(rr.wh_lead_time,0)),
                                (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0))),

            /* order upto point date */
            oh.written_date+
                    DECODE(rr.repl_method,
                           'D',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                                      -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                          (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                          (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                                  -1, NVL(rr.review_time,0),
                                                      NVL(rr.inv_selling_days,0))),
                           'DI',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                                      -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                          (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                          (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                                  -1, NVL(rr.review_time,0),
                                                      NVL(rr.inv_selling_days,0))),
                           'F',rr.next_order_lead_time,
                    DECODE(rr.stock_cat,
                           'C', (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0)+
                                 NVL(rr.wh_lead_time,0)),
                                (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0))))+
                    DECODE(rr.repl_method,
                           'T',  DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.max_supply_days,0)),
                           'TI', DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.max_supply_days,0)),
                                 DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.inv_selling_days,0))),

            /* max scale down days */
            DECODE(rr.repl_method,
                   'D',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                              -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                  (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                  (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                          -1, NVL(rr.review_time,0),
                                              NVL(rr.inv_selling_days,0))),
                   'DI',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                               -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                   (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                   (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                           -1, NVL(rr.review_time,0),
                                               NVL(rr.inv_selling_days,0))),
                   'F',rr.next_order_lead_time,
            DECODE(rr.stock_cat,
                   'C', (NVL(rr.supp_lead_time,0)+
                         NVL(rr.pickup_lead_time,0)+
                         NVL(rr.wh_lead_time,0)),
                        (NVL(rr.supp_lead_time,0)+
                         NVL(rr.pickup_lead_time,0))))+
            DECODE(rr.repl_method,
                   'T',  DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.max_supply_days,0)),
                   'TI', DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.max_supply_days,0)),
                         DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.inv_selling_days,0))),

            rr.season_id,
            rr.phase_id,
            NVL(rr.max_scale_value,0),
            /* get fields for order writeout */
            isc.supplier,
            isc.origin_country_id,
            oh.import_order_ind,
            oh.import_country_id,
            oh.earliest_ship_date,
            oh.latest_ship_date,
            ah.alloc_no,
            ad.qty_allocated,
            /* since only approved items can be on orders, we are trusting that the rounding */
            /* information has been all pushed down to the item/supp/country/loc level.  NVL */
            /* used for the sole purpose of handling bad data.                               */
            NVL(iscl.round_lvl, 'BAD'),
            NVL(iscl.round_to_inner_pct, -1)/100,
            NVL(iscl.round_to_case_pct, -1)/100,
            NVL(iscl.round_to_layer_pct, -1)/100,
            NVL(iscl.round_to_pallet_pct, -1)/100,
            NULL as non_scale_ind,
            NULL as store_close_date,
            NULL as store_ord_mult,
            NULL as buyer_pack_ind,
            NULL as uom_conv_factor,
            NULL as min_scale_value,
            NULL as volume,
            NULL as ord_unit_cost,
            NULL as pack_item_qty,
            NULL as break_pack_ind,
            NULL as scaled_ind,
            NULL as qty_scled_sol
       FROM ordhead oh,
            ordsku os,
            ordloc ol,
            alloc_header ah,
            alloc_detail ad,
            item_master im,
            item_supp_country isc,
            item_supp_country_dim iscd,
            sups s,
            repl_results rr,
            wh w,
            item_supp_country_loc iscl
      WHERE oh.order_no = LP_order_no
        AND os.order_no = oh.order_no
        AND ol.order_no = os.order_no
        AND ol.item = os.item
        AND im.item = ol.item
        AND im.orderable_ind = 'Y'
        AND isc.item = os.item
        AND isc.item = iscd.item(+)
        AND isc.item = iscl.item
        AND isc.supplier = oh.supplier
        AND isc.supplier = iscd.supplier(+)
        AND isc.supplier = iscl.supplier
        AND isc.origin_country_id = os.origin_country_id
        AND isc.origin_country_id = iscd.origin_country(+)
        AND isc.origin_country_id = iscl.origin_country_id
        AND s.supplier = isc.supplier
        AND NVL(ah.order_no, -1) = ol.order_no
        AND ah.item = ol.item
        AND ah.wh = w.wh
        AND w.wh(+) = ol.location
        AND ol.location = iscl.loc
        AND ad.alloc_no = ah.alloc_no
        AND rr.alloc_no(+) = ad.alloc_no
        AND rr.loc_type(+) = ad.to_loc_type
        AND rr.location(+) = ad.to_loc
        AND iscd.dim_object(+) = 'CA' /* cases only */
      UNION
     SELECT 'N',                           /* without ordloc or alloc_detail */
            rr.item,
            NVL(w.physical_wh, -1),
            NVL(rr.source_wh, -1),         /* x-docked from location */
            rr.loc_type,
            rr.location,                   /* x-docked to location */
            NVL(w.rounding_seq, rr.location),
            rr.prescale_roq,
            rr.orig_raw_roq_pack,
            'N',                           /* item non_scale_ind */
            rr.non_scaling_ind,
            im.pack_ind,
            im.dept,
            /* get supplier unit cost and supplier currency code */
            iscl.unit_cost,
            s.currency_code,
            /* get order currency code and exchange rate */
            oh.currency_code,
            oh.exchange_rate,
            /* get item level order constraint and dimension data from isc */
            isc.min_order_qty,
            isc.max_order_qty,
            iscd.length,
            iscd.height,
            iscd.width,
            iscd.lwh_uom,
            iscd.weight,
            iscd.weight_uom,
            iscd.stat_cube, /* stat case */
            isc.supp_pack_size,
            ((isc.ti * isc.hi)*isc.supp_pack_size),
            (isc.ti*isc.supp_pack_size),
            isc.inner_pack_size,
            /* get replenish data from repl_results */
            rr.master_item,    /* master item of the simple packs */
            rr.item_type,
            rr.stock_cat,
            rr.repl_method,
            rr.due_ind,

            /* the order point holds the minimum replenishment value.  Subtracting */
            /* the net inventory results in the quantity needed to reach the       */
            /* minimum.                                                            */
            rr.order_point - rr.net_inventory,

            /* forecast start date */
            oh.written_date,

            /* order lead time date */
            oh.written_date+
                    DECODE(rr.stock_cat,
                           'C', (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0)+
                                 NVL(rr.wh_lead_time,0)),
                                (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0))),
            /* order upto point date */
            oh.written_date+
                    DECODE(rr.repl_method,
                           'D',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                                      -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                          (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                          (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                                  -1, NVL(rr.review_time,0),
                                                      NVL(rr.inv_selling_days,0))),
                           'DI',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                                       -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                           (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                           (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                                   -1, NVL(rr.review_time,0),
                                                       NVL(rr.inv_selling_days,0))),
                           'F',rr.next_order_lead_time,
                    DECODE(rr.stock_cat,
                           'C', (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0)+
                                 NVL(rr.wh_lead_time,0)),
                                (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0))))+
                    DECODE(rr.repl_method,
                           'T',  DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.max_supply_days,0)),
                           'TI', DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.max_supply_days,0)),
                                 DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.inv_selling_days,0))),

            /* max scale down days */
            DECODE(rr.repl_method,
                   'D',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                              -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                  (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                  (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                          -1, NVL(rr.review_time,0),
                                              NVL(rr.inv_selling_days,0))),
                   'DI',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                               -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                   (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                   (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                           -1, NVL(rr.review_time,0),
                                               NVL(rr.inv_selling_days,0))),
                   'F',rr.next_order_lead_time,
            DECODE(rr.stock_cat,
                   'C', (NVL(rr.supp_lead_time,0)+
                         NVL(rr.pickup_lead_time,0)+
                         NVL(rr.wh_lead_time,0)),
                        (NVL(rr.supp_lead_time,0)+
                         NVL(rr.pickup_lead_time,0))))+
            DECODE(rr.repl_method,
                   'T',  DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.max_supply_days,0)),
                   'TI', DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.max_supply_days,0)),
                         DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.inv_selling_days,0))),

            rr.season_id,
            rr.phase_id,
            NVL(rr.max_scale_value,0),
            /* get fields for order writeout */
            isc.supplier,
            isc.origin_country_id,
            oh.import_order_ind,
            oh.import_country_id,
            oh.earliest_ship_date,
            oh.latest_ship_date,
            NVL(rr.alloc_no, -1),
            -1,                     /* not used */
            /* since only approved items can be on orders, we are trusting that the rounding */
            /* information has been all pushed down to the item/supp/country/loc level.  NVL */
            /* used for the sole purpose of handling bad data.                               */
            NVL(iscl.round_lvl, 'BAD'),
            NVL(iscl.round_to_inner_pct, -1)/100,
            NVL(iscl.round_to_case_pct, -1)/100,
            NVL(iscl.round_to_layer_pct, -1)/100,
            NVL(iscl.round_to_pallet_pct, -1)/100,
            NULL as non_scale_ind,
            NULL as store_close_date,
            NULL as store_ord_mult,
            NULL as buyer_pack_ind,
            NULL as uom_conv_factor,
            NULL as min_scale_value,
            NULL as volume,
            NULL as ord_unit_cost,
            NULL as pack_item_qty,
            NULL as break_pack_ind,
            NULL as scaled_ind,
            NULL as qty_scled_sol
       FROM repl_results rr,
            item_master im,
            ordhead oh,
            item_supp_country isc,
            item_supp_country_dim iscd,
            sups s,
            wh w,
            item_supp_country_loc iscl
      WHERE rr.order_no = LP_order_no
        AND rr.order_roq <= 0
        AND oh.order_no = rr.order_no
        AND im.item = rr.item
        AND im.orderable_ind = 'Y'
        AND isc.item = rr.item
        AND isc.item = iscd.item(+)
        AND isc.item = iscl.item
        AND isc.supplier = rr.primary_repl_supplier
        AND isc.supplier = iscd.supplier(+)
        AND isc.supplier = iscl.supplier
        AND isc.origin_country_id = rr.origin_country_id
        AND isc.origin_country_id = iscd.origin_country(+)
        AND isc.origin_country_id = iscl.origin_country_id
        AND s.supplier = isc.supplier
        AND w.wh(+) = NVL(rr.source_wh, rr.location)
        AND rr.location = iscl.loc
        AND iscd.dim_object(+) = 'CA' /* cases only */
   ORDER BY 2, 3, 4, 5, 6, 7;

   /*
    * Cursor for NO due order processing consists of two parts:
    * 1) non-crossdock manual and replenished orders that are due (due_ind='Y')
    *    (They will have ordloc, but may or may not have repl_results.)
    * 2) crossdock manual and replenished orders that are due (due_ind = 'Y')
    *    (They will have alloc_detail, but may or may not have repl_results.)
    */

   cursor C_NO_DUE_ORD_PROC is 
     SELECT 'Y',               /* with ordloc */
            ol.item,
            NVL(w.physical_wh, -1),
            -1,                /* x-docked from location */
            ol.loc_type,
            ol.location,
            NVL(w.rounding_seq, ol.location),
            ol.qty_prescaled,
            rr.orig_raw_roq_pack,
            os.non_scale_ind,
            ol.non_scale_ind,
            im.pack_ind,
            im.dept,
            /* get supplier unit cost and supplier currency code */
            iscl.unit_cost,
            s.currency_code,
            /* get order currency code and exchange rate */
            oh.currency_code,
            oh.exchange_rate,
            /* get item level order constraint and dimension data from isc */
            isc.min_order_qty,
            isc.max_order_qty,
            iscd.length,
            iscd.height,
            iscd.width,
            iscd.lwh_uom,
            iscd.weight,
            iscd.weight_uom,
            iscd.stat_cube, /* stat case */
            nvl(os.supp_pack_size,isc.supp_pack_size),
            ((isc.ti * isc.hi)*nvl(os.supp_pack_size,isc.supp_pack_size)),
            (isc.ti*nvl(os.supp_pack_size,isc.supp_pack_size)),
            isc.inner_pack_size,
            /* get replenish data from repl_results */
            rr.master_item,    /* master item of the simple packs */
            rr.item_type,
            rr.stock_cat,
            rr.repl_method,
            rr.due_ind,
            /* the order point holds the minimum replenishment value.  Subtracting */
            /* the net inventory results in the quantity needed to reach the       */
            /* minimum.                                                            */
            rr.order_point - rr.net_inventory,
            /* forecast start date */
            oh.written_date,
            /* order lead time date */
            oh.written_date+
                    DECODE(rr.stock_cat,
                           'C', (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0)+
                                 NVL(rr.wh_lead_time,0)),
                                (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0))),
            /* order upto point date */
            oh.written_date+
                    DECODE(rr.repl_method,
                           'D',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                                      -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                          (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                          (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                                  -1, NVL(rr.review_time,0),
                                                      NVL(rr.inv_selling_days,0))),
                           'DI',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                                       -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                           (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                           (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                                   -1, NVL(rr.review_time,0),
                                                       NVL(rr.inv_selling_days,0))),
                           'F',rr.next_order_lead_time,
                    DECODE(rr.stock_cat,
                           'C', (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0)+
                                 NVL(rr.wh_lead_time,0)),
                                (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0))))+
                    DECODE(rr.repl_method,
                           'T',  DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.max_supply_days,0)),
                           'TI', DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.max_supply_days,0)),
                                 DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                           NVL(rr.inv_selling_days,0))),
            /* max scale down days */
            DECODE(rr.repl_method,
                   'D',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                              -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                  (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                  (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                          -1, NVL(rr.review_time,0),
                                              NVL(rr.inv_selling_days,0))),
                   'DI',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                              -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                  (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                  (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                          -1, NVL(rr.review_time,0),
                                              NVL(rr.inv_selling_days,0))),
                   'F',rr.next_order_lead_time,
            DECODE(rr.stock_cat,
                   'C', (NVL(rr.supp_lead_time,0)+
                         NVL(rr.pickup_lead_time,0)+
                         NVL(rr.wh_lead_time,0)),
                        (NVL(rr.supp_lead_time,0)+
                         NVL(rr.pickup_lead_time,0))))+
            DECODE(rr.repl_method,
                   'T',  DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.max_supply_days,0)),
                   'TI', DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.max_supply_days,0)),
                         DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.inv_selling_days,0))),
            rr.season_id,
            rr.phase_id,
            NVL(rr.max_scale_value,0),
            /* get fields for order writeout */
            isc.supplier,
            isc.origin_country_id,
            oh.import_order_ind,
            oh.import_country_id,
            oh.earliest_ship_date,
            oh.latest_ship_date,
            -1,                    /* alloc_no */
            ol.qty_ordered,
            /* since only approved items can be on orders, we are trusting that the rounding */
            /* information has been all pushed down to the item/supp/country/loc level.  NVL */
            /* used for the sole purpose of handling bad data.                               */
            NVL(iscl.round_lvl, 'BAD'),
            NVL(iscl.round_to_inner_pct, -1)/100,
            NVL(iscl.round_to_case_pct, -1)/100,
            NVL(iscl.round_to_layer_pct, -1)/100,
            NVL(iscl.round_to_pallet_pct, -1)/100,
            NULL as non_scale_ind,
            NULL as store_close_date,
            NULL as store_ord_mult,
            NULL as buyer_pack_ind,
            NULL as uom_conv_factor,
            NULL as min_scale_value,
            NULL as volume,
            NULL as ord_unit_cost,
            NULL as pack_item_qty,
            NULL as break_pack_ind,
            NULL as scaled_ind,
            NULL as qty_scled_sol
       FROM ordhead oh,
            ordsku os,
            ordloc ol,
            item_master im,
            item_supp_country isc,
            item_supp_country_dim iscd,
            sups s,
            repl_results rr,
            wh w,
            item_supp_country_loc iscl
      WHERE oh.order_no = LP_order_no
        AND os.order_no = oh.order_no
        AND ol.order_no = os.order_no
        AND ol.item = os.item
        AND im.item = ol.item
        AND im.orderable_ind = 'Y'
        AND isc.item = os.item
        AND isc.item = iscd.item(+)
        AND isc.item = iscl.item
        AND isc.supplier = oh.supplier
        AND isc.supplier = iscd.supplier(+)
        AND isc.supplier = iscl.supplier
        AND isc.origin_country_id = os.origin_country_id
        AND isc.origin_country_id = iscd.origin_country(+)
        AND isc.origin_country_id = iscl.origin_country_id
        AND s.supplier = isc.supplier
        AND rr.order_no(+) = ol.order_no
        AND rr.item(+) = ol.item
        AND rr.location(+) = ol.location
        AND rr.loc_type(+) = ol.loc_type
        AND w.wh(+) = ol.location
        AND ol.location = iscl.loc
        AND rr.due_ind(+) = 'Y'
        AND iscd.dim_object(+) = 'CA' /* cases only */
      UNION
     SELECT 'Y',                                   /* with alloc_detail */
            ol.item,
            NVL(w.physical_wh, -1),
            ah.wh,                                 /* x-docked from location */
            ad.to_loc_type,
            ad.to_loc, /* x-docked to location */
            NVL(w.rounding_seq, ol.location),
            ad.qty_prescaled,
            rr.orig_raw_roq_pack,
            os.non_scale_ind,
            ad.non_scale_ind,
            im.pack_ind,
            im.dept,
            /* get supplier unit cost and supplier currency code */
            iscl.unit_cost,
            s.currency_code,
            /* get order currency code and exchange rate */
            oh.currency_code,
            oh.exchange_rate,
            /* get item level order constraint and dimension data from isc */
            isc.min_order_qty,
            isc.max_order_qty,
            iscd.length,
            iscd.height,
            iscd.width,
            iscd.lwh_uom,
            iscd.weight,
            iscd.weight_uom,
            iscd.stat_cube, /* stat case */
            nvl(os.supp_pack_size,isc.supp_pack_size),
            ((isc.ti * isc.hi)*nvl(os.supp_pack_size,isc.supp_pack_size)),
            (isc.ti*nvl(os.supp_pack_size,isc.supp_pack_size)),
            isc.inner_pack_size,
            /* get replenish data from repl_results */
            rr.master_item,    /* master item of the simple packs */
            rr.item_type,
            rr.stock_cat,
            rr.repl_method,
            rr.due_ind,
            /* the order point holds the minimum replenishment value.  Subtracting */
            /* the net inventory results in the quantity needed to reach the       */
            /* minimum.                                                            */
            rr.order_point - rr.net_inventory,
            /* forecast start date */
            oh.written_date,
            /* order lead time date */
            oh.written_date+
                    DECODE(rr.stock_cat,
                           'C', (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0)+
                                 NVL(rr.wh_lead_time,0)),
                                (NVL(rr.supp_lead_time,0)+
                    NVL(rr.pickup_lead_time,0))),
            /* order upto point date */
            oh.written_date+
                    DECODE(rr.repl_method,
                           'D',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                                      -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                          (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                          (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                                  -1, NVL(rr.review_time,0),
                                                      NVL(rr.inv_selling_days,0))),
                           'DI',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                                      -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                          (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                          (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                                  -1, NVL(rr.review_time,0),
                                                      NVL(rr.inv_selling_days,0))),
                           'F',rr.next_order_lead_time,
                    DECODE(rr.stock_cat,
                           'C', (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0)+
                                 NVL(rr.wh_lead_time,0)),
                                (NVL(rr.supp_lead_time,0)+
                                 NVL(rr.pickup_lead_time,0))))+
                    DECODE(rr.repl_method,
                           'T',  DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.max_supply_days,0)),
                           'TI', DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.max_supply_days,0)),
                                 DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                        -1, NVL(rr.review_time,0),
                                            NVL(rr.inv_selling_days,0))),
            /* max scale down days */
            DECODE(rr.repl_method,
                   'D',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                              -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                  (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                  (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                          -1, NVL(rr.review_time,0),
                                              NVL(rr.inv_selling_days,0))),
                   'DI',DECODE(SIGN(NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0) - (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0))),
                              -1, (NVL(rr.curr_order_lead_time,0) + NVL(rr.inv_selling_days,0)),
                                  (NVL(rr.next_order_lead_time,0) + NVL(rr.review_time,0))) -
                                  (DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                          -1, NVL(rr.review_time,0),
                                              NVL(rr.inv_selling_days,0))),
                   'F',rr.next_order_lead_time,
            DECODE(rr.stock_cat,
                   'C', (NVL(rr.supp_lead_time,0)+
                         NVL(rr.pickup_lead_time,0)+
                         NVL(rr.wh_lead_time,0)),
                        (NVL(rr.supp_lead_time,0)+
                         NVL(rr.pickup_lead_time,0))))+
            DECODE(rr.repl_method,
                   'T',  DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.max_supply_days,0)),
                   'TI', DECODE(SIGN(NVL(rr.max_supply_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.max_supply_days,0)),
                         DECODE(SIGN(NVL(rr.inv_selling_days,0) - NVL(rr.review_time,0)),
                                -1, NVL(rr.review_time,0),
                                    NVL(rr.inv_selling_days,0))),
            rr.season_id,
            rr.phase_id,
            NVL(rr.max_scale_value,0),
            /* get fields for order writeout */
            isc.supplier,
            isc.origin_country_id,
            oh.import_order_ind,
            oh.import_country_id,
            oh.earliest_ship_date,
            oh.latest_ship_date,
            ah.alloc_no,
            ad.qty_allocated,
            /* since only approved items can be on orders, we are trusting that the rounding */
            /* information has been all pushed down to the item/supp/country/loc level.  NVL */
            /* used for the sole purpose of handling bad data.                               */
            NVL(iscl.round_lvl, 'BAD'),
            NVL(iscl.round_to_inner_pct, -1)/100,
            NVL(iscl.round_to_case_pct, -1)/100,
            NVL(iscl.round_to_layer_pct, -1)/100,
            NVL(iscl.round_to_pallet_pct, -1)/100,
            NULL as non_scale_ind,
            NULL as store_close_date,
            NULL as store_ord_mult,
            NULL as buyer_pack_ind,
            NULL as uom_conv_factor,
            NULL as min_scale_value,
            NULL as volume,
            NULL as ord_unit_cost,
            NULL as pack_item_qty,
            NULL as break_pack_ind,
            NULL as scaled_ind,
            NULL as qty_scled_sol
       FROM ordhead oh,
            ordsku os,
            ordloc ol,
            alloc_header ah,
            alloc_detail ad,
            item_master im,
            item_supp_country isc,
            item_supp_country_dim iscd,
            sups s,
            repl_results rr,
            wh w,
            item_supp_country_loc iscl
      WHERE oh.order_no = LP_order_no
        AND os.order_no = oh.order_no
        AND ol.order_no = os.order_no
        AND ol.item = os.item
        AND im.item = ol.item
        AND im.orderable_ind = 'Y'
        AND isc.item = os.item
        AND isc.item = iscd.item(+)
        AND isc.item = iscl.item
        AND isc.supplier = oh.supplier
        AND isc.supplier = iscd.supplier(+)
        AND isc.supplier = iscl.supplier
        AND isc.origin_country_id = os.origin_country_id
        AND isc.origin_country_id = iscd.origin_country(+)
        AND isc.origin_country_id = iscl.origin_country_id
        AND s.supplier = isc.supplier
        AND NVL(ah.order_no, -1) = ol.order_no
        AND ah.item = ol.item
        AND ah.wh = ol.location
        AND ad.alloc_no = ah.alloc_no
        AND rr.alloc_no(+) = ad.alloc_no
        AND rr.loc_type(+) = ad.to_loc_type
        AND rr.location(+) = ad.to_loc
        AND ah.wh = w.wh
        AND w.wh(+) = ol.location
        AND ol.location = iscl.loc
        AND rr.due_ind(+) = 'Y'
        AND iscd.dim_object(+) = 'CA' /* cases only */
     ORDER BY 2, 3, 4, 5, 6, 7;

BEGIN
   if LP_ord_inv_mgmt_rec.due_ord_process_ind = 'Y' then
      open C_DUE_ORD_PROC;
      fetch C_DUE_ORD_PROC BULK COLLECT into LP_scaling_tbl;
      close C_DUE_ORD_PROC;
   else
      open C_NO_DUE_ORD_PROC;
      fetch C_NO_DUE_ORD_PROC BULK COLLECT into LP_scaling_tbl;
      close C_NO_DUE_ORD_PROC;      
   end if;
   if LP_scaling_tbl.count = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_NO_DATA',NULL,NULL,NULL);
      LP_NON_FATAL := TRUE;
   end if;
   
   if INIT_SCALE_ITEM_LOC_TBL(O_error_message) = FALSE then
      return FALSE;
   end if;

   if LOAD_FORECAST_DATA(O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if LOAD_CONVERS_DATA(O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if ADJUST_CONSTRAINT(O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if SCALE_PROCESS(O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if LP_bracket_costing_ind = 'Y' then
      if GET_BRACKET_INFO(O_error_message,
                          L_bracket_val,
                          L_bracket_type,
                          L_bracket_uom,
                          L_bracket_exists) = FALSE then
         return FALSE;
      end if;
      if (L_bracket_exists and
          LP_ord_inv_mgmt_rec.scale_cnstr_obj = 'M' and
          L_bracket_val < LP_scale_cnstr_tbl(LP_most_constraining).scale_cnstr_max_val) then
         /* number of constraints will be recalculated...count variable must be reset first */
         LP_total_scale_cnstr := 0;
         
         /* ensure scaling level is now order */
         LP_ord_inv_mgmt_rec.scale_cnstr_lvl := 'O';
         LP_total_vloc := 1;
         
         LP_ord_inv_mgmt_rec.scale_cnstr_type1 := L_bracket_type;
         LP_ord_inv_mgmt_rec.scale_cnstr_uom1 := L_bracket_uom;
         LP_ord_inv_mgmt_rec.scale_cnstr_min_val1 := L_bracket_val;
         LP_ord_inv_mgmt_rec.scale_cnstr_max_val1 := 0;
         
         LP_ord_inv_mgmt_rec.scale_cnstr_min_tol1 := 0;
         LP_ord_inv_mgmt_rec.scale_cnstr_max_tol1 := 0;
         
         /* wipe out secondary constraints, it is assumed that we're using primary bracket only */
         LP_ord_inv_mgmt_rec.scale_cnstr_type2 := NULL;
         LP_ord_inv_mgmt_rec.scale_cnstr_uom2 := NULL;
         LP_ord_inv_mgmt_rec.scale_cnstr_curr2 := NULL;
         LP_ord_inv_mgmt_rec.scale_cnstr_min_val2 := 0;
         LP_ord_inv_mgmt_rec.scale_cnstr_max_val2 := 0;
         LP_ord_inv_mgmt_rec.scale_cnstr_min_tol2 := 0;
         LP_ord_inv_mgmt_rec.scale_cnstr_max_tol2 := 0;
         
         if FILL_SCALE_CNSTR_TBL(O_error_message) = FALSE then
            return FALSE;
         end if;
         
         if SETUP_SCALE_CNSTR_SUPER(O_error_message) = FALSE then
            return FALSE;
         end if;
         
         if CALC_SUPER_MIN_MAX(O_error_message) = FALSE then
            return FALSE;
         end if;
         
         for a in 1..LP_total_vloc LOOP
            LP_scale_loc_tbl(a).last_index := 1;
            LP_scale_loc_tbl(a).sol_index := 1;
            LP_scale_loc_tbl(a).scaled_ind := 0;
            
            LP_scale_loc_case_tbl(a).last_index := 1;
            LP_scale_loc_case_tbl(a).sol_index := 1;
            LP_scale_loc_case_tbl(a).scaled_ind := 0;
            
            if CALC_TOTAL_CNSTR_QTY(O_error_message,
                                    LP_scale_loc_tbl,
                                    LP_scale_item_loc_tbl,
                                    a,
                                    1,
                                    TRUE) = FALSE then
               return FALSE;
            end if;
         END LOOP;
         if SCALE_PROCESS(O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   
   if CALC_FINAL_SOURCE_WH_QTY(O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if OUTPUT_SCALING(O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if UPDATE_GP_ROUND_QTY(O_error_message,
                          LP_order_no) = FALSE then
      return FALSE;
   end if;
   
   /* write number of truckload to ord_inv_mgmt */
   if UPDATE_TRUCKLOAD(O_error_message,
                       LP_order_no,
                       LP_total_truck_load) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SCALING;
-------------------------------------------------------------------------------
FUNCTION INIT_SCALE_ITEM_LOC_TBL(O_error_message   IN OUT  rtk_errors.rtk_text%TYPE)
RETURN BOOLEAN IS
   L_function          VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.INIT_SCALE_ITEM_LOC_TBL';
BEGIN
   for i in 1..LP_scaling_tbl.count LOOP
      if LP_scaling_tbl(i).qty_prescaled is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_QTY_PRESCL_DEF',
                                                TO_CHAR(LP_order_no),
                                                LP_scaling_tbl(i).item,
                                                TO_CHAR(LP_scaling_tbl(i).location));
         LP_NON_FATAL := TRUE;
      end if;
      LP_scale_item_loc_tbl(i).item                        := LP_scaling_tbl(i).item;
      LP_scale_item_loc_tbl(i).location                    := LP_scaling_tbl(i).location;
      LP_scale_item_loc_tbl(i).physical_wh                 := LP_scaling_tbl(i).physical_wh;
      LP_scale_item_loc_tbl(i).rounding_seq                := LP_scaling_tbl(i).rounding_seq;
      LP_scale_item_loc_tbl(i).qty_scaled_suom(1)          := LP_scaling_tbl(i).qty_prescaled;
      LP_scale_item_loc_tbl(i).qty_scaled_suom_rnd(1)      := LP_scaling_tbl(i).qty_prescaled;

      LP_scale_item_loc_case_tbl(i).item                   := LP_scaling_tbl(i).item;
      LP_scale_item_loc_case_tbl(i).location               := LP_scaling_tbl(i).location;
      LP_scale_item_loc_case_tbl(i).physical_wh            := LP_scaling_tbl(i).physical_wh;
      LP_scale_item_loc_case_tbl(i).rounding_seq           := LP_scaling_tbl(i).rounding_seq;
      LP_scale_item_loc_case_tbl(i).qty_scaled_suom(1)     := 0;
      LP_scale_item_loc_case_tbl(i).qty_scaled_suom_rnd(1) := 0;

      /* cannot cross-dock to a wh, so rounding does not apply to locations with source warehouses */
      if LP_scaling_tbl(i).source_wh <> -1 then
         LP_scaling_tbl(i).rounding_seq             := LP_scaling_tbl(i).location;
         LP_scale_item_loc_tbl(i).rounding_seq      := LP_scale_item_loc_tbl(i).location;
         LP_scale_item_loc_case_tbl(i).rounding_seq := LP_scale_item_loc_case_tbl(i).location;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INIT_SCALE_ITEM_LOC_TBL;
-------------------------------------------------------------------------------
FUNCTION LOAD_FORECAST_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function          VARCHAR2(40) := 'SUP_CONSTRAINTS_SQL.LOAD_FORECAST_DATA';
   L_max_forecast_days NUMBER;
   L_forecast_end_date ORDHEAD.WRITTEN_DATE%TYPE;
   L_sub_item_loc      ITEM_LOC.LOC%TYPE;
   
BEGIN
   for i in 1..LP_scaling_tbl.count LOOP
      LP_forecast_item_loc(i).item        := LP_scaling_tbl(i).item;
      LP_forecast_item_loc(i).location    := LP_scaling_tbl(i).location;
      LP_forecast_item_loc(i).physical_wh := LP_scaling_tbl(i).physical_wh;
      LP_forecast_item_loc(i).max_days    := 0;
      LP_forecast_item_loc(i).total_eow_fetched := 0;
      LP_forecast_item_loc(i).total_day_fetched := 0;

      if LP_scaling_tbl(i).repl_method is NULL or 
         LP_scaling_tbl(i).repl_method in ('C','M','F') then
         continue;
      end if;
      /*
       * get forecasting data for dynamic repl order; calculate date range as follows:
       * 1) forecast start date = order written date;
       * 2) forecast end date =
       *      order upto point date + MIN(max_scale_value, max_scaling_iterations)
       * Note: since it's dynamic replenishment, max_scale_value means max scaling
       * days. When max_scale_value is 0, it means infinity, use max_scaling_iterations.
       * 3) max num of eow forecasting data that can be fetched =
       *      (forecast end date + 7) - (forecast start date - 7) + 1 =
       *      max_scale_down_day + MIN(max_scale_value, max_scaling_iterations) + 15
       * Note: plus and minus 7 are needed here for scenarios when forecast start
       * date and end date are the same.
       */
       if (LP_scaling_tbl(i).max_scale_value = 0 or
           LP_scaling_tbl(i).max_scale_value > LP_ord_inv_mgmt_rec.max_scaling_iterations) then
          L_max_forecast_days := LP_ord_inv_mgmt_rec.max_scaling_iterations;
       else
          L_max_forecast_days := LP_scaling_tbl(i).max_scale_value;
       end if;
       if GET_ORDER_DATE(O_error_message,
                         LP_scaling_tbl(i).order_upto_point_date,
                         L_max_forecast_days,
                         L_forecast_end_date) = FALSE then
          return FALSE;
       end if;
       LP_forecast_item_loc(i).forecast_start_date := LP_scaling_tbl(i).order_written_date;
       LP_forecast_item_loc(i).forecast_end_date   := L_forecast_end_date;
       LP_forecast_item_loc(i).max_days := LP_scaling_tbl(i).max_scale_down_day + L_max_forecast_days + 15;
       
       /* If the store is warehouse stocked or crosslinked, then the
         source warehouse is used to define the sub items. */
       if LP_scaling_tbl(i).loc_type = 'S' then
          if LP_scaling_tbl(i).stock_cat in ('W','L') then
             L_sub_item_loc := LP_scaling_tbl(i).source_wh;
          else 
             L_sub_item_loc := LP_scaling_tbl(i).location;
          end if;
       else
          L_sub_item_loc := NULL;
       end if;
       if FETCH_FORECAST_DATA(O_error_message,
                              i,
                              LP_scaling_tbl(i).master_item,
                              LP_scaling_tbl(i).loc_type,
                              LP_scaling_tbl(i).location,
                              L_sub_item_loc,
                              LP_scaling_tbl(i).repl_method,
                              LP_scaling_tbl(i).stock_cat,  
                              LP_forecast_item_loc(i).forecast_start_date,
                              LP_forecast_item_loc(i).forecast_end_date) = FALSE then
          return FALSE;
       end if;
       if FETCH_DAILY_FORECAST_DATA(O_error_message,
                                    i,
                                    LP_scaling_tbl(i).master_item,
                                    LP_scaling_tbl(i).loc_type,
                                    LP_scaling_tbl(i).location,
                                    L_sub_item_loc,
                                    LP_scaling_tbl(i).repl_method,
                                    LP_scaling_tbl(i).stock_cat,  
                                    LP_forecast_item_loc(i).forecast_start_date,
                                    LP_forecast_item_loc(i).forecast_end_date) = FALSE then
          return FALSE;
       end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOAD_FORECAST_DATA;
-------------------------------------------------------------------------------
FUNCTION GET_ORDER_DATE(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_upto_point_date  IN      REPL_RESULTS.REPL_DATE%TYPE,
                        I_day                    IN      NUMBER,
                        O_order_date             OUT     REPL_RESULTS.REPL_DATE%TYPE)
RETURN BOOLEAN IS
   L_function          VARCHAR2(40) := 'SUP_CONSTRAINTS_SQL.GET_ORDER_DATE';
   
   cursor C_ORDER_DATE is
     SELECT I_order_upto_point_date + I_day
       FROM dual;
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ORDER_DATE',NULL,'I_order_upto_point_date:' || I_order_upto_point_date);
   open C_ORDER_DATE;
   SQL_LIB.SET_MARK('FETCH','C_ORDER_DATE',NULL,'I_order_upto_point_date:' || I_order_upto_point_date);
   fetch C_ORDER_DATE into O_order_date;
   SQL_LIB.SET_MARK('CLOSE','C_ORDER_DATE',NULL,'I_order_upto_point_date:' || I_order_upto_point_date);
   close C_ORDER_DATE;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ORDER_DATE;
-------------------------------------------------------------------------------
FUNCTION FETCH_FORECAST_DATA(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cur_index              IN      NUMBER,
                             I_master_item            IN      REPL_RESULTS.MASTER_ITEM%TYPE,
                             I_loc_type               IN      REPL_RESULTS.LOC_TYPE%TYPE,
                             I_location               IN      REPL_RESULTS.LOCATION%TYPE,
                             I_sub_item_loc           IN      REPL_RESULTS.LOCATION%TYPE,
                             I_repl_method            IN      REPL_RESULTS.REPL_METHOD%TYPE,
                             I_stock_cat              IN      REPL_RESULTS.STOCK_CAT%TYPE,
                             I_start_date             IN      REPL_RESULTS.REPL_DATE%TYPE,
                             I_end_date               IN      REPL_RESULTS.REPL_DATE%TYPE)
RETURN BOOLEAN IS
   L_function          VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.FETCH_FORECAST_DATA';
   
   /* get forecasting data for the master item and all its substitute items */
   /* for eow_dates that fall between the forecasting start and end date */
   cursor C_FORECAST is 
     select eow_date,
            SUM(NVL(forecast_sales, 0))/ 7
       from item_forecast
      where (item = I_master_item
         or  item in (select sid.sub_item
                        from sub_items_head sih,
                             sub_items_detail sid
                       where sih.item = sid.item
                         and sih.item = I_master_item
                         and sih.location = sid.location
                         and sid.location = I_sub_item_loc
                         and sih.use_forecast_sales_ind = 'y'))
        and loc = I_location
        and eow_date >= I_start_date - 7
        and eow_date <= I_end_date + 7
   group by eow_date
   order by eow_date;
   
   /* for warehouse replenishment with stock_cat of "W" and repl_method of "T" */
   /* and "D", get forecasting info for all stores sourced from the warehouse; */
   /* stores should be open and items should be on active replenishment. */
   cursor C_FORECAST_W is 
     select fi.eow_date,
            NVL(SUM(NVL(fi.forecast_sales, 0)), 0) / 7
       from item_forecast fi,
            repl_item_loc ril,
            store s
      where (fi.item = I_master_item
         or  fi.item in (select sid.sub_item
                           from sub_items_head sih,
                                sub_items_detail sid
                          where sih.item = sid.item
                            and sih.item = I_master_item
                            and sih.location = sid.location
                            and sid.location = I_sub_item_loc
                            and sih.use_forecast_sales_ind = 'y'))
        and ril.item = I_master_item
        and fi.loc = ril.location
        and ril.loc_type = 's'
        and ril.stock_cat = 'w'
        and ril.source_wh = I_location
        and fi.eow_date >= I_start_date - 7
        and fi.eow_date <= I_end_date + 7
        and s.store = ril.location
        and fi.eow_date <= nvl(s.store_close_date, fi.eow_date)
        and s.store_open_date - s.start_order_days <= fi.eow_date
        and ril.activate_date <= fi.eow_date
        and NVL(ril.deactivate_date, fi.eow_date+1) > fi.eow_date
   group by fi.eow_date;
   
BEGIN
 
   if (I_loc_type = 'W' and 
       I_stock_cat = 'W' and
       (I_repl_method in ('T','D'))) then
      SQL_LIB.SET_MARK('OPEN','C_FORECAST_W','ITEM_FORECAST,REPL_ITEM_LOC','item:' || I_master_item || ',location:' || I_location);
      open C_FORECAST_W;
      SQL_LIB.SET_MARK('FETCH','C_FORECAST_W','ITEM_FORECAST,REPL_ITEM_LOC','item:' || I_master_item || ',location:' || I_location);
      fetch C_FORECAST_W BULK COLLECT into LP_forecast_item_loc(I_cur_index).eow_date,
                                           LP_forecast_item_loc(I_cur_index).sales;
      SQL_LIB.SET_MARK('CLOSE','C_FORECAST_W','ITEM_FORECAST,REPL_ITEM_LOC','item:' || I_master_item || ',location:' || I_location);
      close C_FORECAST_W;
      LP_forecast_item_loc(I_cur_index).total_eow_fetched := LP_forecast_item_loc(I_cur_index).eow_date.COUNT;
   else
      SQL_LIB.SET_MARK('OPEN','C_FORECAST','ITEM_FORECAST','item:' || I_master_item || ',location:' || I_location);
      open C_FORECAST;
      SQL_LIB.SET_MARK('FETCH','C_FORECAST','ITEM_FORECAST','item:' || I_master_item || ',location:' || I_location);
      fetch C_FORECAST BULK COLLECT into LP_forecast_item_loc(I_cur_index).eow_date,
                                         LP_forecast_item_loc(I_cur_index).sales;
      SQL_LIB.SET_MARK('CLOSE','C_FORECAST','ITEM_FORECAST','item:' || I_master_item || ',location:' || I_location);
      close C_FORECAST;
      LP_forecast_item_loc(I_cur_index).total_eow_fetched := LP_forecast_item_loc(I_cur_index).eow_date.COUNT;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FETCH_FORECAST_DATA;
-------------------------------------------------------------------------------
FUNCTION FETCH_DAILY_FORECAST_DATA(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_cur_index              IN      NUMBER,
                                   I_master_item            IN      REPL_RESULTS.MASTER_ITEM%TYPE,
                                   I_loc_type               IN      REPL_RESULTS.LOC_TYPE%TYPE,
                                   I_location               IN      REPL_RESULTS.LOCATION%TYPE,
                                   I_sub_item_loc           IN      REPL_RESULTS.LOCATION%TYPE,
                                   I_repl_method            IN      REPL_RESULTS.REPL_METHOD%TYPE,
                                   I_stock_cat              IN      REPL_RESULTS.STOCK_CAT%TYPE,
                                   I_start_date             IN      REPL_RESULTS.REPL_DATE%TYPE,
                                   I_end_date               IN      REPL_RESULTS.REPL_DATE%TYPE)

RETURN BOOLEAN IS
   L_function          VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.FETCH_DAILY_FORECAST_DATA';
   
   /* get daily forecasting data for the master item and all its substitute items */
   /* for dates that fall between the forecasting start and end date              */
   cursor C_DAILY_FORECAST is 
      select dif.data_date,
             NVL(SUM(NVL(dif.forecast_sales, 0)), 0)
        from daily_item_forecast dif
       where (dif.item = I_master_item
          or  dif.item in (select sid.sub_item
                             from sub_items_head sih,
                                  sub_items_detail sid
                            where sih.item = sid.item
                              and sih.item = I_master_item
                              and sih.location = sid.location
                              and sid.location = I_sub_item_loc
                              and sih.use_forecast_sales_ind = 'y'))
         and dif.loc = I_location
         and dif.data_date >= I_start_date
         and dif.data_date <= I_end_date
    group by dif.data_date
    order by dif.data_date;
   
   /* for warehouse replenishment with stock_cat of "W" and repl_method of "T"       */
   /* and "D", get daily forecasting info for all stores sourced from the warehouse; */
   /* stores should be open and items should be on active replenishment.             */
   cursor C_DAILY_FORECAST_W is 
     select dif.data_date,
            NVL(SUM(NVL(dif.forecast_sales, 0)), 0)
       from daily_item_forecast dif,
            repl_item_loc ril,
            store s
      where (dif.item = I_master_item
         or  dif.item in (select sid.sub_item
                            from sub_items_head sih,
                                 sub_items_detail sid
                           where sih.item = sid.item
                             and sih.item = I_master_item
                             and sih.location = sid.location
                             and sid.location = I_sub_item_loc
                             and sih.use_forecast_sales_ind = 'y'))
        and ril.item = I_master_item
        and dif.loc = ril.location
        and ril.loc_type = 's'
        and ril.stock_cat = 'w'
        and ril.source_wh = I_location
        and dif.data_date >= I_start_date
        and dif.data_date <= I_end_date
        and s.store = ril.location
        and dif.data_date <= nvl(s.store_close_date, dif.data_date)
        and s.store_open_date - s.start_order_days <= dif.data_date
        and ril.activate_date <= dif.data_date
        and NVL(ril.deactivate_date, dif.data_date+1) > dif.data_date
   group by dif.data_date
   order by dif.data_date;
   
BEGIN
 
   if (I_loc_type = 'W' and 
       I_stock_cat = 'W' and
       (I_repl_method in ('T','D'))) then
      SQL_LIB.SET_MARK('OPEN','C_DAILY_FORECAST_W','DAILY_ITEM_FORECAST,REPL_ITEM_LOC','item:' || I_master_item || ',location:' || I_location);
      open C_DAILY_FORECAST_W;
      SQL_LIB.SET_MARK('FETCH','C_DAILY_FORECAST_W','DAILY_ITEM_FORECAST,REPL_ITEM_LOC','item:' || I_master_item || ',location:' || I_location);
      fetch C_DAILY_FORECAST_W BULK COLLECT into LP_forecast_item_loc(I_cur_index).data_date,
                                                 LP_forecast_item_loc(I_cur_index).day_sales;
      SQL_LIB.SET_MARK('CLOSE','C_DAILY_FORECAST_W','DAILY_ITEM_FORECAST,REPL_ITEM_LOC','item:' || I_master_item || ',location:' || I_location);
      close C_DAILY_FORECAST_W;
      
      LP_forecast_item_loc(I_cur_index).total_day_fetched := LP_forecast_item_loc(I_cur_index).data_date.COUNT;
   else
      SQL_LIB.SET_MARK('OPEN','C_DAILY_FORECAST','DAILY_ITEM_FORECAST','item:' || I_master_item || ',location:' || I_location);
      open C_DAILY_FORECAST;
      SQL_LIB.SET_MARK('FETCH','C_DAILY_FORECAST','DAILY_ITEM_FORECAST','item:' || I_master_item || ',location:' || I_location);
      fetch C_DAILY_FORECAST BULK COLLECT into LP_forecast_item_loc(I_cur_index).data_date,
                                               LP_forecast_item_loc(I_cur_index).day_sales;
      SQL_LIB.SET_MARK('CLOSE','C_DAILY_FORECAST','DAILY_ITEM_FORECAST','item:' || I_master_item || ',location:' || I_location);
      close C_DAILY_FORECAST;
      LP_forecast_item_loc(I_cur_index).total_day_fetched := LP_forecast_item_loc(I_cur_index).data_date.COUNT;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END FETCH_DAILY_FORECAST_DATA;
-------------------------------------------------------------------------------
FUNCTION LOAD_CONVERS_DATA(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(40) := 'SUP_CONSTRAINTS_SQL.LOAD_CONVERS_DATA';
   L_scale_cnstr_dimension_uom    VARCHAR2(4);
   L_scale_cnstr_weight_uom       VARCHAR2(4);
   L_scale_cnstr_curr             VARCHAR2(3);
   L_in_season                    VARCHAR2(1);
   L_stat_cube_ind                BOOLEAN := FALSE;
   L_standard_uom                 ITEM_MASTER.STANDARD_UOM%TYPE;
   
   cursor C_STORE_CLOSE_DATE(I_store    STORE.STORE%TYPE) is
      select store_close_date
        from store
       where store = I_store;
      
   cursor C_CHECK_SEASON(I_season_id         REPL_RESULTS.SEASON_ID%TYPE,
                         I_phase_id          REPL_RESULTS.PHASE_ID%TYPE,
                         I_date              REPL_RESULTS.REPL_DATE%TYPE) is 
      select 'x'
        from phases
       where season_id = I_season_id
         and phase_id = I_phase_id
         and start_date <= I_date
         and end_date >= I_date;
         
   cursor C_GET_STORE_ORD_MULT(I_item        ITEM_LOC.ITEM%TYPE,
                               I_location    ITEM_LOC.LOC%TYPE) is 
      select NVL(il.store_ord_mult, im.store_ord_mult),
             im.standard_uom,
             im.uom_conv_factor,
             DECODE(im.pack_ind, 'Y', DECODE(NVL(im.pack_type, 'V'), 'B', 'Y', 'N'), 'X')
        from item_master im,
             item_loc il
       where il.loc = I_location
         and im.item = I_item
         and im.item = il.item
      UNION
      select NVL(il.store_ord_mult, im.store_ord_mult),
             im.standard_uom,
             im.uom_conv_factor,
             DECODE(im.pack_ind, 'Y', DECODE(NVL(im.pack_type, 'V'), 'B', 'Y', 'N'), 'X')
        from item_master im,
             item_loc il
       where il.loc = I_location
         and im.item_parent = I_item
         and im.item_parent = il.item_parent
         and im.item_level >= im.tran_level;
         
   cursor C_GET_PACK_ITEM_QTY(I_pack_no      ITEM_MASTER.ITEM%TYPE) is
      select pack_item_qty
        from packitem_breakout
       where pack_no = i_pack_no;

BEGIN
   /* check what constraint types are defined for scaling; */
   /* if "M" (mass) or "V" (volume), get target weight or dimension uom  */
   /* if "A" (currency amount), get target currency code */
   for j in 1..LP_total_scale_cnstr LOOP
      if LP_scale_cnstr_tbl(j).scale_cnstr_type = 'V' then
         /* convert scaling constraint's volume uom to dimension uom */
         /* Oracle Retail convention: volume uom = dimension uom + "3" */
         /* e.g.: volume uom "IN3", corresponding dimension uom is "IN". */
         /* There are a few exceptions that need to be hard coded: */
         /* 1) CC (cubic centimeter) => 'CM' */
         /* 2) CFT (cubic feet) => 'FT' */
         /* 3) CBM (cubic meter) => 'M' */
         if LP_scale_cnstr_tbl(j).scale_cnstr_uom = 'CC' then
            L_scale_cnstr_dimension_uom := 'CM';
         elsif LP_scale_cnstr_tbl(j).scale_cnstr_uom = 'CFT' then
            L_scale_cnstr_dimension_uom := 'FT';
         elsif LP_scale_cnstr_tbl(j).scale_cnstr_uom = 'CBM' then
            L_scale_cnstr_dimension_uom := 'M'; 
         else 
            L_scale_cnstr_dimension_uom := LP_scale_cnstr_tbl(j).scale_cnstr_uom;
         end if;
      elsif LP_scale_cnstr_tbl(j).scale_cnstr_type = 'M' then
         L_scale_cnstr_weight_uom := LP_scale_cnstr_tbl(j).scale_cnstr_uom;
      elsif LP_scale_cnstr_tbl(j).scale_cnstr_type = 'A' then
         L_scale_cnstr_curr := LP_scale_cnstr_tbl(j).scale_cnstr_curr;
      elsif LP_scale_cnstr_tbl(j).scale_cnstr_type = 'S' then
         L_stat_cube_ind := TRUE;
      end if;
   END LOOP;
   /* For each order/item/location, do the following: */
   /* 1) determine non_scale_ind based on item and loc non_scale_ind */
   /* 2) get store close date */
   /* 3) possibly turn off scaling for season/phase replenishment */
   /* 4) get item/location minimum constraint */
   /* 5) get store order multiple */
   /* 6) get pack_item_qty for simple pack items in replenishment */
   /* 7) convert dimension and weight to scaling cnstr's uom, calculate volume */
   /* 8) convert unit_cost from supplier currency code to order currency code */
   /* 9) initialize scaled_ind to 'N' for order writeout */
   for i in 1..LP_scaling_tbl.count LOOP
      if (LP_scaling_tbl(i).item_non_scale_ind = 'Y' or
          LP_scaling_tbl(i).loc_non_scale_ind = 'Y') then
         LP_scaling_tbl(i).non_scale_ind := 'Y';
      else 
         LP_scaling_tbl(i).non_scale_ind := 'N';
      end if;
      
      if LP_scaling_tbl(i).loc_type = 'S' then
         SQL_LIB.SET_MARK('OPEN','C_STORE_CLOSE_DATE','STORE','store:' || LP_scaling_tbl(i).location);
         open C_STORE_CLOSE_DATE(LP_scaling_tbl(i).location);
         SQL_LIB.SET_MARK('FETCH','C_STORE_CLOSE_DATE','STORE','store:' || LP_scaling_tbl(i).location);
         fetch C_STORE_CLOSE_DATE into LP_scaling_tbl(i).store_close_date;
         SQL_LIB.SET_MARK('CLOSE','C_STORE_CLOSE_DATE','STORE','store:' || LP_scaling_tbl(i).location);
         close C_STORE_CLOSE_DATE;
      end if;
      /* for season/phase replenishment, do not scale the item by turning off */
      /* the scaling flag if order lead time falls outside of the season range */
      /* Note: seasonal/phase replenishment will come in as repl_method "T" or */
      /* "D", instead of "E" or "S", but with NOT NULL season_id and phase_id. */
      if (LP_scaling_tbl(i).non_scale_ind = 'N' and
          LP_scaling_tbl(i).repl_method in ('T','D') and
          LP_scaling_tbl(i).season_id is not NULL and 
          LP_scaling_tbl(i).phase_id is not NULL ) then
         L_in_season := 'y'; 
         SQL_LIB.SET_MARK('OPEN','C_CHECK_SEASON','PHASES','phase_id:' || LP_scaling_tbl(i).phase_id); 
         open C_CHECK_SEASON(LP_scaling_tbl(i).season_id,
                             LP_scaling_tbl(i).phase_id,
                             LP_scaling_tbl(i).order_lead_time_date);
         SQL_LIB.SET_MARK('FETCH','C_CHECK_SEASON','PHASES','phase_id:' || LP_scaling_tbl(i).phase_id); 
         fetch C_CHECK_SEASON into L_in_season;
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_SEASON','PHASES','phase_id:' || LP_scaling_tbl(i).phase_id); 
         close C_CHECK_SEASON;
         if L_in_season <> 'x' then
            LP_scaling_tbl(i).non_scale_ind := 'Y';
         end if;
      end if;
      
      /* derive item/location min constraint */
      if (LP_scaling_tbl(i).due_ind is NULL or 
          LP_scaling_tbl(i).due_ind = 'N') then   /* manual or not-due order */
         LP_scaling_tbl(i).min_scale_value := 0;
      else
         LP_scaling_tbl(i).min_scale_value := LP_scaling_tbl(i).order_point;
      end if;
      
      /* get store_ord_mult, buyer_pack_ind, conversion factor btw Each and SUOM */
      LP_scaling_tbl(i).buyer_pack_ind := 'N';
      SQL_LIB.SET_MARK('OPEN','C_GET_STORE_ORD_MULT','ITEM_MASTER,ITEM_LOC','ITEM:' || LP_scaling_tbl(i).item  || ',LOC:' || LP_scaling_tbl(i).location); 
      open C_GET_STORE_ORD_MULT(LP_scaling_tbl(i).item,
                                LP_scaling_tbl(i).location);
      SQL_LIB.SET_MARK('FETCH','C_GET_STORE_ORD_MULT','ITEM_MASTER,ITEM_LOC','ITEM:' || LP_scaling_tbl(i).item  || ',LOC:' || LP_scaling_tbl(i).location);
      fetch C_GET_STORE_ORD_MULT into LP_scaling_tbl(i).store_ord_mult,
                                      L_standard_uom,
                                      LP_scaling_tbl(i).uom_conv_factor,
                                      LP_scaling_tbl(i).buyer_pack_ind;
      SQL_LIB.SET_MARK('CLOSE','C_GET_STORE_ORD_MULT','ITEM_MASTER,ITEM_LOC','ITEM:' || LP_scaling_tbl(i).item  || ',LOC:' || LP_scaling_tbl(i).location);
      close C_GET_STORE_ORD_MULT;
      if LP_scaling_tbl(i).buyer_pack_ind = 'X' then
         LP_scaling_tbl(i).buyer_pack_ind := NULL;
      end if;
      if LP_scaling_tbl(i).uom_conv_factor is NULL then
         if L_standard_uom = 'EA' then
            LP_scaling_tbl(i).uom_conv_factor := 1;
         else 
            O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_UOM_CONV_DEF',
                                                   LP_scaling_tbl(i).item,
                                                   NULL,
                                                   NULL);
            LP_NON_FATAL := TRUE;
            return FALSE;
         end if;
      end if;
      
      /* for simple pack items in replenishment, get pack_item_qty for calculating scaling quantity using the forecasting data */
      if (LP_scaling_tbl(i).pack_ind ='Y' and
          LP_scaling_tbl(i).repl_method is not NULL) then
         SQL_LIB.SET_MARK('OPEN','C_GET_PACK_ITEM_QTY','PACKITEM_BREAKOUT','Item:' || LP_scaling_tbl(i).item); 
         open C_GET_PACK_ITEM_QTY(LP_scaling_tbl(i).item);
         SQL_LIB.SET_MARK('FETCH','C_GET_PACK_ITEM_QTY','PACKITEM_BREAKOUT','Item:' || LP_scaling_tbl(i).item); 
         fetch C_GET_PACK_ITEM_QTY into LP_scaling_tbl(i).pack_item_qty;
         SQL_LIB.SET_MARK('CLOSE','C_GET_PACK_ITEM_QTY','PACKITEM_BREAKOUT','Item:' || LP_scaling_tbl(i).item); 
         close C_GET_PACK_ITEM_QTY;
      end if;

      /* derive item/location min constraint */
      if (LP_scaling_tbl(i).due_ind is NULL or 
          LP_scaling_tbl(i).due_ind = 'N') then   /* manual or not-due order */
         LP_scaling_tbl(i).min_scale_value := 0;
      else /* due order */
         /* The order point value is in number of items needed.  This needs to be */
         /*  modified by the number of items in the simple pack if one is being used. */
         if LP_scaling_tbl(i).pack_item_qty > 0 then
            LP_scaling_tbl(i).min_scale_value := LP_scaling_tbl(i).order_point / LP_scaling_tbl(i).pack_item_qty;
         else
            LP_scaling_tbl(i).min_scale_value := LP_scaling_tbl(i).order_point;
         end if;
      end if;

      /* convert item dimension and weight to uom of scaling cnstr if defined */
      if LP_scaling_tbl(i).buyer_pack_ind = 'Y' then
         /* calculate dimension and weight for buyer pack */
         if (L_scale_cnstr_dimension_uom is not NULL or
             L_scale_cnstr_weight_uom is not NULL) then
            if CALC_BUYER_PACK_DIM_WGT(O_error_message,
                                       LP_scaling_tbl(i).item,
                                       LP_scaling_tbl(i).supplier,
                                       LP_scaling_tbl(i).origin_country_id,
                                       LP_scaling_tbl(i).supp_pack_size,
                                       L_scale_cnstr_dimension_uom,
                                       L_scale_cnstr_weight_uom,
                                       LP_scaling_tbl(i).volume,
                                       LP_scaling_tbl(i).ship_carton_wt,
                                       LP_scaling_tbl(i).stat_cube) = FALSE then
               return FALSE;
            end if;
         end if;
      else
         if (L_scale_cnstr_dimension_uom is not NULL) then
            if (LP_scaling_tbl(i).ship_carton_len is NULL or
               LP_scaling_tbl(i).ship_carton_hgt is NULL or 
               LP_scaling_tbl(i).ship_carton_wid is NULL or
               LP_scaling_tbl(i).dimension_uom is NULL) then 
               O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_ITEM_DIM_DEF',
                                                      LP_scaling_tbl(i).item,
                                                      NULL,
                                                      NULL);
               LP_NON_FATAL := TRUE;
               return FALSE;
            end if;
            if L_scale_cnstr_dimension_uom <> LP_scaling_tbl(i).dimension_uom then
               if UOM_CONVERSION(O_error_message,
                                 LP_scaling_tbl(i).dimension_uom,
                                 L_scale_cnstr_dimension_uom,
                                 LP_scaling_tbl(i).ship_carton_len,
                                 LP_scaling_tbl(i).ship_carton_len) = FALSE then
                  return FALSE;
               end if;
               if UOM_CONVERSION(O_error_message,
                                 LP_scaling_tbl(i).dimension_uom,
                                 L_scale_cnstr_dimension_uom,
                                 LP_scaling_tbl(i).ship_carton_hgt,
                                 LP_scaling_tbl(i).ship_carton_hgt) = FALSE then
                  return FALSE;
               end if;
               if UOM_CONVERSION(O_error_message,
                                 LP_scaling_tbl(i).dimension_uom,
                                 L_scale_cnstr_dimension_uom,
                                 LP_scaling_tbl(i).ship_carton_wid,
                                 LP_scaling_tbl(i).ship_carton_wid) = FALSE then
                  return FALSE;
               end if;
            end if;
            LP_scaling_tbl(i).volume := LP_scaling_tbl(i).ship_carton_len*LP_scaling_tbl(i).ship_carton_hgt*LP_scaling_tbl(i).ship_carton_wid;
            
         end if;/*L_scale_cnstr_dimension_uom is not NULL */
         if L_scale_cnstr_weight_uom is NOT NULL then
            if (LP_scaling_tbl(i).ship_carton_wt is NULL or 
                LP_scaling_tbl(i).weight_uom is NULL) then 
               O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_CASE_WT_DEF',
                                                      LP_scaling_tbl(i).item,
                                                      NULL,
                                                      NULL);
               LP_NON_FATAL := TRUE;
               return FALSE;
            end if;
            if L_scale_cnstr_weight_uom <> LP_scaling_tbl(i).weight_uom then
               if UOM_CONVERSION(O_error_message,
                                 LP_scaling_tbl(i).weight_uom,
                                 L_scale_cnstr_weight_uom,
                                 LP_scaling_tbl(i).ship_carton_wt,
                                 LP_scaling_tbl(i).ship_carton_wt) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
         
         if L_stat_cube_ind and LP_scaling_tbl(i).stat_cube is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_STAT_CUB_DEF',
                                                   LP_scaling_tbl(i).item,
                                                   NULL,
                                                   NULL);
            LP_NON_FATAL := TRUE;
            return FALSE;
         end if;
      end if;/* end else buyer pack ind != 'Y' */
      /* convert supplier unit_cost in supplier currency to order currency; */
      /* order currency code is always the same as scale cnstr currency code */
      if LP_scaling_tbl(i).sup_currency_code = LP_scaling_tbl(i).ord_currency_code then
         LP_scaling_tbl(i).ord_unit_cost := LP_scaling_tbl(i).sup_unit_cost;
      else
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 LP_scaling_tbl(i).sup_unit_cost,
                                 LP_scaling_tbl(i).sup_currency_code,
                                 LP_scaling_tbl(i).ord_currency_code,
                                 LP_scaling_tbl(i).ord_unit_cost,
                                 'C',
                                 NULL,
                                 'P',  -- purchase order exchange type
                                 NULL,
                                 LP_scaling_tbl(i).exchange_rate) = FALSE then
            return FALSE;
         end if;
      end if;
      /* init scaled flag to 'N' - indicate if we need to write out scaled solutions */
      LP_scaling_tbl(i).scaled_ind := 'N';
   END LOOP;

   /* setup a table for crossdock orders */
   if SETUP_XDOCK(O_error_message) = FALSE then
      return FALSE;
   end if;

   /* calculate direct to warehouse portion of order quantity */
   if CALC_DIR_TO_WH_QTY(O_error_message) = FALSE then
      return FALSE;
   end if;

   /* for location-level scaling, re-calculate item supplier min/max */
   if LP_ord_inv_mgmt_rec.scale_cnstr_lvl = 'L' then
      if ADJUST_ITEM_MIN_MAX(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOAD_CONVERS_DATA;
-------------------------------------------------------------------------------
FUNCTION CALC_BUYER_PACK_DIM_WGT(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_pack_no           IN      ITEM_MASTER.ITEM%TYPE,
                                 I_supplier          IN      ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                                 I_origin_country_id IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                 I_supp_pack_size    IN      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                 I_to_dimension_uom  IN      ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
                                 I_to_weight_uom     IN      ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                                 O_total_volume      OUT     ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE,
                                 O_total_weight      OUT     ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE,
                                 O_stat_cube         OUT     ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CALC_BUYER_PACK_DIM_WGT';
   
   L_item                         ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE;
   L_dimension_uom                ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_weight_uom                   ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_pack_item_qty                PACKITEM_BREAKOUT.ITEM_QTY%TYPE;
   L_supp_pack_size               ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_ship_carton_len              ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_ship_carton_hgt              ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE;
   L_ship_carton_wid              ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE;
   L_ship_carton_wt               ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_total_volume                 ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE := 0;
   L_total_weight                 ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE := 0;
   L_total_stat_cube              ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE := 0;
   L_stat_cube                    ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE := 0; /*stat case*/
   L_rec_exists                   BOOLEAN := FALSE;
   
   cursor C_PACKITEM is
     select item,
            qty
       from v_packsku_qty
      where pack_no = I_pack_no;
      
   cursor C_WGT_VOL is 
     select isc.supp_pack_size,
            iscd.length,
            iscd.height,
            iscd.width,
            iscd.lwh_uom,
            iscd.weight,
            iscd.weight_uom,
            iscd.stat_cube
       from item_supp_country isc,
            item_supp_country_dim iscd
      where isc.item = L_item
        and isc.item = iscd.item
        and isc.supplier = I_supplier
        and isc.supplier = iscd.supplier
        and isc.origin_country_id = I_origin_country_id
        and isc.origin_country_id = iscd.origin_country
        and iscd.dim_object = 'CA'; /* cases only */

BEGIN  
   for rec in C_PACKITEM LOOP
      L_item := rec.item;
      L_pack_item_qty := rec.qty;
      SQL_LIB.SET_MARK('OPEN','C_WGT_VOL','ITEM_SUPP_COUNTRY_DIM','Item:' || L_item || ',Supplier:' || I_supplier);
      open C_WGT_VOL;
      SQL_LIB.SET_MARK('FETCH','C_WGT_VOL','ITEM_SUPP_COUNTRY_DIM','Item:' || L_item || ',Supplier:' || I_supplier);
      fetch C_WGT_VOL into L_supp_pack_size,
                           L_ship_carton_len,
                           L_ship_carton_hgt,
                           L_ship_carton_wid,
                           L_dimension_uom,
                           L_ship_carton_wt,
                           L_weight_uom,
                           L_stat_cube;
      SQL_LIB.SET_MARK('CLOSE','C_WGT_VOL','ITEM_SUPP_COUNTRY_DIM','Item:' || L_item || ',Supplier:' || I_supplier);
      close C_WGT_VOL;
      
      if I_to_dimension_uom is not NULL then
         if (L_ship_carton_len is NULL or
             L_ship_carton_hgt is NULL or
             L_ship_carton_wid is NULL or
             L_dimension_uom is NULL) then
            O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_PACK_DIM_DEF',
                                                   I_pack_no,
                                                   L_item,
                                                   NULL);
            LP_NON_FATAL := TRUE;
            return FALSE;
         end if;
         if (I_to_dimension_uom <> L_dimension_uom) then
            if UOM_CONVERSION(O_error_message,
                              L_dimension_uom,
                              I_to_dimension_uom,
                              L_ship_carton_len,
                              L_ship_carton_len) = FALSE then
               return FALSE;
            end if;
            if UOM_CONVERSION(O_error_message,
                              L_dimension_uom,
                              I_to_dimension_uom,
                              L_ship_carton_hgt,
                              L_ship_carton_hgt) = FALSE then
               return FALSE;
            end if;
            if UOM_CONVERSION(O_error_message,
                              L_dimension_uom,
                              I_to_dimension_uom,
                              L_ship_carton_wid,
                              L_ship_carton_wid) = FALSE then
               return FALSE;
            end if;
         end if;
         L_total_volume := L_total_volume + ((L_ship_carton_len*L_ship_carton_hgt*L_ship_carton_wid)/L_supp_pack_size*L_pack_item_qty);
         
         if I_to_weight_uom is not NULL then
            if (L_ship_carton_wt is NULL or 
                L_weight_uom is NULL) then 
               O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_PACK_WT_DEF',
                                                      I_pack_no, 
                                                      L_item,
                                                      NULL);
               LP_NON_FATAL := TRUE;
               return FALSE;
            end if;
            if I_to_weight_uom <> L_weight_uom then
               if UOM_CONVERSION(O_error_message,
                                 L_weight_uom,
                                 I_to_weight_uom,
                                 L_ship_carton_wt,
                                 L_ship_carton_wt) = FALSE then
                  return FALSE;
               end if;
            end if;
            L_total_weight := L_total_weight + (L_ship_carton_wt/L_supp_pack_size*L_pack_item_qty);
         end if;
      end if;/*I_to_dimension_uom is not NULL*/
      
      L_total_stat_cube := L_total_stat_cube + (L_stat_cube/L_supp_pack_size*L_pack_item_qty);
      L_rec_exists := TRUE;
   end LOOP;
   if L_rec_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_PACK_NO_ITEM',I_pack_no,NULL,NULL);
      return FALSE;
   end if;
   
   /* store buyer pack total in supp_pack_size */
   O_total_volume := L_total_volume * I_supp_pack_size;
   O_total_weight := L_total_weight * I_supp_pack_size;
   if L_stat_cube is NOT NULL then
      O_stat_cube := L_total_stat_cube * I_supp_pack_size;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_BUYER_PACK_DIM_WGT;
-------------------------------------------------------------------------------
FUNCTION UOM_CONVERSION(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_from_uom          IN      VARCHAR2,
                        I_to_uom            IN      VARCHAR2,
                        I_from_value        IN      NUMBER,
                        O_to_value          OUT     NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.UOM_CONVERSION';
   L_factor                       NUMBER(12,10);
   L_operator                     VARCHAR2(1);
   
   cursor C_UOM is 
     select factor,
            operator
       from uom_conversion
      where from_uom = I_from_uom
        and to_uom = I_to_uom;
BEGIN
   if I_from_uom = I_to_uom then 
      O_to_value := I_from_value;
   end if;
   SQL_LIB.SET_MARK('OPEN','C_UOM','UOM_CONVERSION','from_uom:' || I_from_uom || ',to_uom:' || I_to_uom);
   open C_UOM;
   SQL_LIB.SET_MARK('FETCH','C_UOM','UOM_CONVERSION','from_uom:' || I_from_uom || ',to_uom:' || I_to_uom);
   fetch C_UOM into L_factor,
                    L_operator;
   SQL_LIB.SET_MARK('CLOSE','C_UOM','UOM_CONVERSION','from_uom:' || I_from_uom || ',to_uom:' || I_to_uom);
   close C_UOM;
   
   if L_operator = 'M' then
      O_to_value := I_from_value * L_factor;
   else
      O_to_value := I_from_value / L_factor;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UOM_CONVERSION;
-------------------------------------------------------------------------------
/* ----- functions for crossdock processing ----- */
FUNCTION SETUP_XDOCK(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.SETUP_XDOCK';
   L_xdock_from_loc               SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_TBL;
   
BEGIN
   for i in 1..LP_scaling_tbl.count LOOP
      if LP_scaling_tbl(i).source_wh = -1 then
         continue;
      end if;
      L_xdock_from_loc := LP_xdock_from_loc_tbl;

      for j in 1..L_xdock_from_loc.COUNT LOOP
         if (LP_scaling_tbl(i).source_wh = L_xdock_from_loc(j).source_wh and 
             LP_scaling_tbl(i).item = L_xdock_from_loc(j).item) then

            if ADD_XDOCK_TO_LOC(O_error_message,
                                L_xdock_from_loc(j),
                                i) = FALSE then
               return FALSE;
            end if;
            LP_scaling_tbl(i).break_pack_ind := L_xdock_from_loc(j).break_pack_ind;
            EXIT;
         end if;
      END LOOP;

      if L_xdock_from_loc.count = 0 or L_xdock_from_loc is NULL then
      
         if ADD_XDOCK_FROM_LOC(O_error_message,
                               LP_xdock_from_loc_tbl,
                               i) = FALSE then
           return FALSE;
         end if;
      end if;

   END LOOP;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_XDOCK;
-------------------------------------------------------------------------------
FUNCTION ADD_XDOCK_TO_LOC(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_xdock_from_loc_rec IN OUT  SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_REC,
                          I_cur_index          IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.ADD_XDOCK_TO_LOC';
   
   L_xdock_to_loc_tbl             SUP_CONSTRAINTS_SQL.XDOCK_TO_LOC_TBL;
   L_temp                         SUP_CONSTRAINTS_SQL.XDOCK_TO_LOC_REC;
   L_idx                          NUMBER;
   
BEGIN
   
   L_temp.xdock_loc := LP_scaling_tbl(I_cur_index).location;
   L_temp.item := LP_scaling_tbl(I_cur_index).item;
   L_temp.driv_cursor_index := I_cur_index;
   
   L_xdock_to_loc_tbl := O_xdock_from_loc_rec.xdock_to_loc;
   
   L_idx := O_xdock_from_loc_rec.xdock_to_loc.count;
   O_xdock_from_loc_rec.xdock_to_loc(L_idx + 1) := L_temp;


   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_XDOCK_TO_LOC;
-------------------------------------------------------------------------------
FUNCTION ADD_XDOCK_FROM_LOC(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_xdock_from_loc_tbl  IN OUT  SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_TBL,
                            I_cur_index           IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.ADD_XDOCK_FROM_LOC';
   L_break_pack_ind               VARCHAR2(1);
   L_idx                          NUMBER := 0;
   L_cnt                          BINARY_INTEGER := NULL;
   
   cursor C_BREAK_PACK_IND(I_wh      WH.WH%TYPE) is 
     select break_pack_ind
       from wh
      where wh = I_wh;
   
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_BREAK_PACK_IND','WH','wh:' || LP_scaling_tbl(I_cur_index).source_wh);
   open C_BREAK_PACK_IND(LP_scaling_tbl(I_cur_index).source_wh);
   SQL_LIB.SET_MARK('FETCH','C_BREAK_PACK_IND','WH','wh:' || LP_scaling_tbl(I_cur_index).source_wh);
   fetch C_BREAK_PACK_IND into L_break_pack_ind;
   SQL_LIB.SET_MARK('CLOSE','C_BREAK_PACK_IND','WH','wh:' || LP_scaling_tbl(I_cur_index).source_wh);
   close C_BREAK_PACK_IND;
   
   L_idx := O_xdock_from_loc_tbl.count + 1;
   
   O_xdock_from_loc_tbl(L_idx).source_wh := LP_scaling_tbl(I_cur_index).source_wh;
   O_xdock_from_loc_tbl(L_idx).item := LP_scaling_tbl(I_cur_index).item;
   O_xdock_from_loc_tbl(L_idx).break_pack_ind := L_break_pack_ind;
   O_xdock_from_loc_tbl(L_idx).residual_qty := 0;
   
   if ADD_XDOCK_TO_LOC(O_error_message,
                       O_xdock_from_loc_tbl(L_idx),
                       I_cur_index) = FALSE then
      return FALSE;
   end if;
   LP_scaling_tbl(I_cur_index).break_pack_ind := O_xdock_from_loc_tbl(L_idx).break_pack_ind;
   
   /* find out driving cursor index for source_wh */
   for i in 1..LP_scaling_tbl.count LOOP
      if (LP_scaling_tbl(I_cur_index).source_wh = LP_scaling_tbl(i).location and
          LP_scaling_tbl(I_cur_index).item = LP_scaling_tbl(i).item) then
         O_xdock_from_loc_tbl(L_idx).driv_cursor_index := i;
         L_cnt := i;
         EXIT;
      end if;
   END LOOP;
   
   if (L_cnt is NULL) then
     /* no ordloc record for source_wh in the driving cursor tbl due to */
     /* negative prescaled_qty in repl_results, set driv_cursor_index to -1 */
     O_xdock_from_loc_tbl(L_idx).driv_cursor_index := -1;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_XDOCK_FROM_LOC;
-------------------------------------------------------------------------------
FUNCTION CALC_DIR_TO_WH_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CALC_DIR_TO_WH_QTY';
   
   L_xdock_from_loc               SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_TBL;
   L_total_qty_som_rnd            ORDLOC.QTY_ORDERED%TYPE := 0;
   L_total_qty_oim_rnd            ORDLOC.QTY_ORDERED%TYPE := 0;
   L_residual_qty                 ORDLOC.QTY_ORDERED%TYPE := 0;
   L_driv_cursor_index            NUMBER := 1;
   ---
   
BEGIN
   /*
    * For each source_wh on the xdock tbl,
    * 1) calculate the residual qty of xdock allocation due to rounding;
    * 2) calculate the dir-to-wh portion of xdock allocation if ordloc exists for
    *    source_wh;
    * 3) set the non_scale_ind flag of source_wh to 'Y' for pure xdock;
    *
    * Note: when this program is called, all positive prescaled qty of crossdock
    * locations are properly rounded to store_ord_mult or to Case depending on
    * the warehouse break_pack_ind. Therefore, function calc_xdock_residual_qty
    * is called with an indicator of TRUE to avoid rounding the crossdock locations
    * again. The prescaled qty stored in index 1 of ga_scale_item_loc will be
    * updated for source_wh to the dir-to-wh qty.
    */
    L_xdock_from_loc := LP_xdock_from_loc_tbl;
    for j in 1..L_xdock_from_loc.COUNT LOOP
       L_driv_cursor_index := L_xdock_from_loc(j).driv_cursor_index;
       if CALC_XDOCK_RESIDUAL_QTY(O_error_message,
                                  TRUE,
                                  L_xdock_from_loc(j),
                                  LP_scale_item_loc_tbl,
                                  1,
                                  LP_scale_item_loc_tbl,
                                  1,
                                  L_total_qty_som_rnd,
                                  L_total_qty_oim_rnd,
                                  L_residual_qty) = FALSE then
          return FALSE;
       end if;
       
       L_xdock_from_loc(j).residual_qty := L_residual_qty;
       if L_driv_cursor_index <> -1 then /* ordloc exists for source_wh */
          /* need to round qty up since prescaled_qty on ordloc is not rounded */
          if ROUND_TO_CASE_LAYER_PALLET(O_error_message,
                                        LP_scaling_tbl(L_driv_cursor_index).round_lvl,
                                        0, /* round to case pct */
                                        0, /* round to layer pct */ 
                                        0, /* round to pallet pct */
                                        LP_scaling_tbl(L_driv_cursor_index).supp_pack_size,
                                        LP_scaling_tbl(L_driv_cursor_index).tier,
                                        LP_scaling_tbl(L_driv_cursor_index).pallet_size,
                                        LP_scale_item_loc_tbl(L_driv_cursor_index).qty_scaled_suom(1),
                                        LP_scale_item_loc_tbl(L_driv_cursor_index).qty_scaled_suom(1)) = FALSE then
             return FALSE;
          end if;

          if ROUND_TO_CASE_LAYER_PALLET(O_error_message,
                                        LP_scaling_tbl(L_driv_cursor_index).round_lvl,
                                        0, /* round to case pct */
                                        0, /* round to layer pct */ 
                                        0, /* round to pallet pct */
                                        LP_scaling_tbl(L_driv_cursor_index).supp_pack_size,
                                        LP_scaling_tbl(L_driv_cursor_index).tier,
                                        LP_scaling_tbl(L_driv_cursor_index).pallet_size,
                                        LP_scale_item_loc_tbl(L_driv_cursor_index).qty_scaled_suom_rnd(1),
                                        LP_scale_item_loc_tbl(L_driv_cursor_index).qty_scaled_suom_rnd(1)) = FALSE then
             return FALSE;
          end if;
          /* calc dir-to-wh qty */
          if (LP_scale_item_loc_tbl(L_driv_cursor_index).qty_scaled_suom(1) > L_total_qty_som_rnd) then /* non-pure xdock */
             LP_scale_item_loc_tbl(L_driv_cursor_index).qty_scaled_suom(1) := LP_scale_item_loc_tbl(L_driv_cursor_index).qty_scaled_suom(1) - L_total_qty_oim_rnd;
             LP_scale_item_loc_tbl(L_driv_cursor_index).qty_scaled_suom_rnd(1) := LP_scale_item_loc_tbl(L_driv_cursor_index).qty_scaled_suom_rnd(1) - L_total_qty_oim_rnd;
          elsif LP_scale_item_loc_tbl(L_driv_cursor_index).qty_scaled_suom(1) = L_total_qty_som_rnd then /* pure xdock */
             LP_scale_item_loc_tbl(L_driv_cursor_index).qty_scaled_suom(1) := 0;
             LP_scale_item_loc_tbl(L_driv_cursor_index).qty_scaled_suom_rnd(1) := 0;
             LP_scaling_tbl(L_xdock_from_loc(j).driv_cursor_index).non_scale_ind := 'Y';
          end if;
          
         /* do nothing if source_wh prescaled_qty is less than total xdock qty; */
         /* consider the qty (should be < 0) as dir-to-wh qty and scale wh separately */
       end if;
       
    END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_DIR_TO_WH_QTY;
-------------------------------------------------------------------------------
FUNCTION CALC_XDOCK_RESIDUAL_QTY(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_in_som              IN      BOOLEAN,
                                 I_xdock_from_loc      IN      SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_REC,
                                 I_from_scale_item_loc IN      SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                                 I_from_index          IN      NUMBER,
                                 O_to_scale_item_loc   IN OUT  SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                                 I_to_index            IN      NUMBER,
                                 O_total_qty_rnd_som   OUT     ORDLOC.QTY_ORDERED%TYPE,
                                 O_total_qty_rnd_case  OUT     ORDLOC.QTY_ORDERED%TYPE,
                                 O_residual_qty        OUT     ORDLOC.QTY_ORDERED%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CALC_XDOCK_RESIDUAL_QTY';

   L_xdock_to_loc                 SUP_CONSTRAINTS_SQL.XDOCK_TO_LOC_TBL;
   L_driv_cursor_index            NUMBER;
   L_qty_rnd_som                  ORDLOC.QTY_ORDERED%TYPE;
   L_total_qty_rnd_som            ORDLOC.QTY_ORDERED%TYPE := 0;
   L_total_qty_rnd_case           ORDLOC.QTY_ORDERED%TYPE := 0;
   
BEGIN
   /*
    * Function returns the total xdock qty after case/pallet rounding and the
    * residual qty due to case/pallet rounding:
    * 1) for each xdock location sourced from the wh, round the qty to store_ord_mult
    *    if wh break_pack_ind is 'Y' or to case if wh break_pack_ind is 'N';
    * 2) add up the positive rounded xdock qty in step 1;
    * 3) round the total in step 2 to the round_lvl defined on ord_inv_mgmt;
    *    always round up by using rounding pct of 0;
    * 4) the difference between step 2 and 3 is the residual ordered to the wh.
    */
    O_to_scale_item_loc := I_from_scale_item_loc;
    L_xdock_to_loc := I_xdock_from_loc.xdock_to_loc;
    for i in 1..L_xdock_to_loc.COUNT LOOP
       L_driv_cursor_index := L_xdock_to_loc(i).driv_cursor_index;
      /* if ii_in_som = TRUE, the qty is already rounded to store_ord_mult or */
      /* case, do not round; otherwise, round to store_ord_mult or case. */
      if I_in_som = FALSE then
         if I_xdock_from_loc.break_pack_ind = 'Y' then
             /* round xdock qty to store_ord_mult-'I'nner, 'C'ase, 'E'aches */
             if ROUND_TO_STORE_ORD_MULT(O_error_message,
                                        LP_scaling_tbl(L_driv_cursor_index).store_ord_mult,
                                        LP_scaling_tbl(L_driv_cursor_index).inner_pack_size,
                                        LP_scaling_tbl(L_driv_cursor_index).supp_pack_size,
                                        LP_scaling_tbl(L_driv_cursor_index).round_to_inner_pct,
                                        LP_scaling_tbl(L_driv_cursor_index).round_to_case_pct,
                                        I_from_scale_item_loc(L_driv_cursor_index).qty_scaled_suom_rnd(I_from_index),
                                        L_qty_rnd_som) = FALSE then
               return FALSE;
            end if;
         else
            if ROUND_TO_CASE_LAYER_PALLET(O_error_message,
                                          'C',
                                          LP_scaling_tbl(L_driv_cursor_index).round_to_case_pct,
                                          LP_scaling_tbl(L_driv_cursor_index).round_to_layer_pct,
                                          LP_scaling_tbl(L_driv_cursor_index).round_to_pallet_pct,
                                          LP_scaling_tbl(L_driv_cursor_index).supp_pack_size,
                                          LP_scaling_tbl(L_driv_cursor_index).tier,
                                          LP_scaling_tbl(L_driv_cursor_index).pallet_size,
                                          I_from_scale_item_loc(L_driv_cursor_index).qty_scaled_suom_rnd(I_from_index),
                                          L_qty_rnd_som) = FALSE then
               return FALSE;
            end if;
         end if;
      else
         L_qty_rnd_som := I_from_scale_item_loc(L_driv_cursor_index).qty_scaled_suom_rnd(I_from_index);
      end if; --I_in_som = FALSE
      
      O_to_scale_item_loc(L_driv_cursor_index).qty_scaled_suom(I_to_index) := L_qty_rnd_som;
      O_to_scale_item_loc(L_driv_cursor_index).qty_scaled_suom_rnd(I_to_index) := L_qty_rnd_som;
      
      if L_qty_rnd_som > 0 then/* negative qty add 0 */
         L_total_qty_rnd_som := L_total_qty_rnd_som + L_qty_rnd_som;
      end if;
   END LOOP;
    
   if (L_total_qty_rnd_som > 0) then
      /*
       * Round total alloc qty to ord_inv_mgmt.round_lvl using the following rule:
       * 1) if round level is layer/pallet, attempt to round up to pallet.  if this cannot be done,
       *    round up (not down) to layer by passing in round to layer percent as zero.
       * 2) if round level is case/layer, attempt to round up to layer.  if this cannot be done,
       *    round up (not down) to case by passing in round to case percent as zero.
       * 3) if round level is case/layer/pallet, attempt to round up to pallet.  if this cannot be
       *    done, attempt to round up to layer.  if unable to round up to layer, round the quantity
       *    up to case (not down) by passing round to case percent as zero.
       * 4) if round level is case, layer, or pallet, always round up by passing in rounding pct of
       *    zero.
       */

      /* Note: use driv_cursor_index instead of xdock_from_loc.driv_cursor_index */
      /* since the latter can be -1 if an all negative xdock order scales to positive. */
      if (LP_scaling_tbl(L_driv_cursor_index).round_lvl = 'LP') then /* layer/pallet rounding. */
         /* pass in the round to case percent and supplier pack size as zero, */
         /* since they are not used in layer/pallet rounding.                 */
         if ROUND_TO_CASE_LAYER_PALLET(O_error_message,
                                       LP_scaling_tbl(L_driv_cursor_index).round_lvl,
                                       0, /* round to case pct */
                                       0, /* round to layer pct */
                                       LP_scaling_tbl(L_driv_cursor_index).round_to_pallet_pct,
                                       0, /* supp pack size */
                                       LP_scaling_tbl(L_driv_cursor_index).tier,
                                       LP_scaling_tbl(L_driv_cursor_index).pallet_size,
                                       L_total_qty_rnd_som,
                                       L_total_qty_rnd_case) = FALSE then 
            return FALSE;
         end if;
      elsif (LP_scaling_tbl(L_driv_cursor_index).round_lvl = 'CL') then /* case/layer rounding. */
         /* pass in the round to pallet percent and pallet size as zero, */
         /* since they are not used in case/layer rounding. */
         if ROUND_TO_CASE_LAYER_PALLET(O_error_message,
                                       LP_scaling_tbl(L_driv_cursor_index).round_lvl,
                                       0, /* round to case pct */
                                       LP_scaling_tbl(L_driv_cursor_index).round_to_layer_pct, 
                                       0, /* round to pallet pct */
                                       LP_scaling_tbl(L_driv_cursor_index).supp_pack_size,
                                       LP_scaling_tbl(L_driv_cursor_index).tier,
                                       0, /* pallet size */
                                       L_total_qty_rnd_som,
                                       L_total_qty_rnd_case) = FALSE then 
            return FALSE;
         end if;
      elsif (LP_scaling_tbl(L_driv_cursor_index).round_lvl = 'CLP') then /* case/layer/pallet rounding. */
         if ROUND_TO_CASE_LAYER_PALLET(O_error_message,
                                       LP_scaling_tbl(L_driv_cursor_index).round_lvl,
                                       0, /* round to case pct */
                                       LP_scaling_tbl(L_driv_cursor_index).round_to_layer_pct, 
                                       LP_scaling_tbl(L_driv_cursor_index).round_to_pallet_pct,
                                       LP_scaling_tbl(L_driv_cursor_index).supp_pack_size,
                                       LP_scaling_tbl(L_driv_cursor_index).tier,
                                       LP_scaling_tbl(L_driv_cursor_index).pallet_size,
                                       L_total_qty_rnd_som,
                                       L_total_qty_rnd_case) = FALSE then 
            return FALSE;
         end if;
      else /* case, layer, or pallet rounding */
         if ROUND_TO_CASE_LAYER_PALLET(O_error_message,
                                       LP_scaling_tbl(L_driv_cursor_index).round_lvl,
                                       0, /* round to case pct */
                                       0, /* round to layer pct */ 
                                       0, /* round to pallet pct */
                                       LP_scaling_tbl(L_driv_cursor_index).supp_pack_size,
                                       LP_scaling_tbl(L_driv_cursor_index).tier,
                                       LP_scaling_tbl(L_driv_cursor_index).pallet_size,
                                       L_total_qty_rnd_som,
                                       L_total_qty_rnd_case) = FALSE then 
            return FALSE;
         end if;
      end if;
      O_total_qty_rnd_som := L_total_qty_rnd_som;
      O_total_qty_rnd_case := L_total_qty_rnd_case;
      O_residual_qty := L_total_qty_rnd_case - L_total_qty_rnd_som;
   
   else 
      O_total_qty_rnd_som := 0;
      O_total_qty_rnd_case := 0;
      O_residual_qty := 0;
   end if;
    
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_XDOCK_RESIDUAL_QTY;
-------------------------------------------------------------------------------
FUNCTION ROUND_TO_STORE_ORD_MULT(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_store_ord_mult     IN      ITEM_LOC.STORE_ORD_MULT%TYPE,
                                 I_inner_pack_size    IN      ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                                 I_supp_pack_size     IN      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                 I_round_to_inner_pct IN      ITEM_SUPP_COUNTRY.ROUND_TO_INNER_PCT%TYPE,
                                 I_round_to_case_pct  IN      ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT%TYPE,
                                 I_qty                IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                 O_qty_rnd            OUT     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.ROUND_TO_STORE_ORD_MULT';
   
   L_qty_rnd                      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
BEGIN
   if I_store_ord_mult = 'I' then
      L_qty_rnd := I_qty/I_inner_pack_size;
      if ROUNDING(O_error_message,
                  L_qty_rnd,
                  I_round_to_inner_pct) = FALSE then
         return FALSE;
      end if;
      O_qty_rnd := L_qty_rnd * I_inner_pack_size;
   elsif I_store_ord_mult = 'C' then
      L_qty_rnd := I_qty/I_supp_pack_size;
      if ROUNDING(O_error_message,
                  L_qty_rnd,
                  I_round_to_case_pct) = FALSE then
         return FALSE;
      end if;
      O_qty_rnd := L_qty_rnd * I_supp_pack_size;
   elsif I_store_ord_mult = 'E' then
      /* always round up Eaches by passing in rounding pct of 0 */
      L_qty_rnd := I_qty;
      if ROUNDING(O_error_message,
                  L_qty_rnd,
                  0) = FALSE then
         return FALSE;
      end if;
      O_qty_rnd := L_qty_rnd;
   else 
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_INV_SOM',I_store_ord_mult,NULL,NULL);
      LP_NON_FATAL := TRUE;
      return FALSE;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ROUND_TO_STORE_ORD_MULT;
-------------------------------------------------------------------------------
FUNCTION ROUND_TO_CASE_LAYER_PALLET(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_round_lvl           IN      ITEM_SUPP_COUNTRY.ROUND_LVL%TYPE,
                                    I_round_to_case_pct   IN      ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT%TYPE,
                                    I_round_to_layer_pct  IN      ITEM_SUPP_COUNTRY.ROUND_TO_LAYER_PCT%TYPE,
                                    I_round_to_pallet_pct IN      ITEM_SUPP_COUNTRY.ROUND_TO_PALLET_PCT%TYPE,
                                    I_supp_pack_size      IN      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                    I_tier                IN      ITEM_SUPP_COUNTRY.TI%TYPE,
                                    I_pallet_size         IN      ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                    I_qty                 IN      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                    O_qty_rnd             OUT     ITEM_LOC_SOH.STOCK_ON_HAND%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.ROUND_TO_CASE_LAYER_PALLET';

   L_num_case                     NUMBER(12,4) := 0;
   L_num_tier                     NUMBER(12,4) := 0;
   L_num_pallet                   NUMBER(12,4) := 0;
   L_qty_rnd                      ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := 0;
   L_rounded                      BOOLEAN := FALSE;

   
BEGIN
   /*
    * Based on the rounding level, round qty to integer num of case or pallet:
    * 1. round_lvl = "C"ase   : do case rounding;
    * 2. round_lvl = "L"ayer  : do layer rounding;
    * 3. round_lvl = "P"allet : do pallet rounding;
    * 4. round_lvl = "CL"     : do case/layer rounding;
    * 5. round_lvl = "CLP"    : do case/layer/pallet rounding;
    * 6. round_lvl = "LP"     : do layer/pallet rounding;
    */
   if I_round_lvl = 'C' then
      L_num_case := I_qty/I_supp_pack_size;
      if ROUNDING(O_error_message,
                  L_num_case,
                  I_round_to_case_pct) = FALSE then
         return FALSE;
      end if;
      O_qty_rnd := L_num_case * I_supp_pack_size;
   elsif I_round_lvl = 'L' then
      L_num_tier  := I_qty/I_tier;
      if ROUNDING(O_error_message,
                  L_num_tier,
                  I_round_to_layer_pct) = FALSE then
         return FALSE;
      end if;
      O_qty_rnd := L_num_tier * I_tier;
   elsif I_round_lvl = 'P' then
      L_num_pallet := I_qty/I_pallet_size;
      if ROUNDING(O_error_message,
                  L_num_pallet,
                  I_round_to_pallet_pct) = FALSE then
         return FALSE;
      end if;
      O_qty_rnd := L_num_pallet * I_pallet_size;
   elsif I_round_lvl = 'CL' then
     /* 1. compare qty to layer % to see if layer rounding is possible.
      *    if possible, then round up to layer and stop.
      * 2. if cannot round to layer, compare qty to case % to see if
      *    case rounding is possible.  if possible, then round up to
      *    case and stop.
      * 3. otherwise, round down to nearest case, if the result is
      *    greater than or equal to one case, and stop.
      */
      if EVAL_LAYER_PALLET(O_error_message,
                           I_qty,
                           I_tier,
                           I_round_to_layer_pct,
                           L_qty_rnd,
                           L_rounded) = FALSE then
         return FALSE;
      end if;
      if (L_rounded = FALSE) then /* if(I_qty == L_qty_rnd ) */  /* quantity was not rounded up to the next layer. */
        /* round up or down to case. */
         L_num_case := I_qty/I_supp_pack_size;
         if ROUNDING(O_error_message,
                     L_num_case,
                     I_round_to_case_pct) = FALSE then
            return FALSE;
         end if;
         if ((L_num_case < I_qty) and L_num_case = 0) then /* don't want to round down to zero. */
            O_qty_rnd := I_qty;
         else
            O_qty_rnd := L_num_case * I_supp_pack_size;
         end if;
      else /* pass out the layer-rounded value. */
         O_qty_rnd := L_qty_rnd;
      end if;
   elsif I_round_lvl = 'CLP' then /* case/layer/pallet rounding */
      /* 1. compare qty to pallet % to see if pallet rounding is possible.
       *    if possible, then round up to pallet and stop.
       * 2. compare qty to layer % to see if layer rounding is possible.
       *    if possible, then round up to layer and stop.
       * 3. if cannot round to layer, compare qty to case % to see if
       *    case rounding is possible.  if possible, then round up to
       *    case and stop.
       * 4. otherwise, round down to nearest case, if the result is
       *    greater than or equal to one case, and stop.
       */
      if EVAL_LAYER_PALLET(O_error_message,
                           I_qty,
                           I_pallet_size,
                           I_round_to_pallet_pct,
                           L_qty_rnd,
                           L_rounded) = FALSE then
         return FALSE;
      end if;
      if L_rounded = FALSE then /* I_qty == L_qty_rnd) */ /* quantity was not rounded up to the next pallet. */
         if EVAL_LAYER_PALLET(O_error_message,
                              I_qty,
                              I_tier,
                              I_round_to_layer_pct,
                              L_qty_rnd,
                              L_rounded) = FALSE then
           return FALSE;
         end if;
         if L_rounded = FALSE then /* quanity was not rounded up to the next layer. */
            /* round up or down to case. */
            L_num_case := I_qty/I_supp_pack_size;
            if ROUNDING(O_error_message,
                        L_num_case,
                        I_round_to_case_pct) = FALSE then
               return FALSE;
            end if;
            if (L_num_case < I_qty and L_num_case = 0) then
               O_qty_rnd := I_qty; /* don't want to round down to zero. */
            else
               O_qty_rnd  := L_num_case * I_supp_pack_size;
            end if;
         else /* pass out the pallet-rounded value. */
            O_qty_rnd := L_qty_rnd;
         end if;
      end if; -- L_rounded = FALSE
   elsif I_round_lvl = 'LP' then /* layer/pallet rounding */
      /* 1. compare qty to pallet % to see if pallet rounding is possible.
       *    if possible, then round up to pallet and stop.
       * 2. if cannot round to pallet, compare qty to layer % to see if
       *    layer rounding is possible.  if possible, then round up to
       *    layer and stop.
       * 3. otherwise, round down to nearest layer, if the result is
       *    greater than or equal to one layer, and stop.
       */
      if EVAL_LAYER_PALLET(O_error_message,
                           I_qty,
                           I_pallet_size,
                           I_round_to_pallet_pct,
                           L_qty_rnd,
                           L_rounded) = FALSE then
         return FALSE;
      end if;
      if L_rounded = FALSE then /* quantity was not rounded up to the next pallet. */
         /* round up or down to layer. */
         L_num_tier := I_qty/I_tier;
         if ROUNDING(O_error_message,
                     L_num_tier,
                     I_round_to_layer_pct) = FALSE then
            return FALSE;
         end if;
         if (L_num_tier < I_qty and L_num_tier = 0) then /* don't want to round down to zero. */
            O_qty_rnd := I_qty;
         else
            O_qty_rnd := L_num_tier * I_tier;
         end if;
      else  /* quantity was rounded up to the next pallet */
         O_qty_rnd := L_qty_rnd;
      end if;
   else /* invalid rounding level */
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_INV_RND_LVL_ORD',TO_CHAR(I_round_lvl),NULL,NULL);
      LP_NON_FATAL := TRUE;
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ROUND_TO_CASE_LAYER_PALLET;
-------------------------------------------------------------------------------
FUNCTION ROUNDING(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_num               IN OUT  NUMBER,
                  I_round_to_pct      IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.ROUNDING';
   
BEGIN
   if (TRUNC(O_num) <> O_num) then
      if(O_num < -1) then
         if I_round_to_pct = 0 then
            O_num := TRUNC(O_num);
         else
            O_num := TRUNC(O_num - (1 - I_round_to_pct));
         end if;
      elsif O_num < 0  then
         O_num := -1;
      elsif O_num = 0 then
         O_num := 0;
      elsif O_num <= 1 then
         O_num := 1;
      else
         O_num := TRUNC(O_num + (1 - I_round_to_pct));
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ROUNDING;
-------------------------------------------------------------------------------
FUNCTION EVAL_LAYER_PALLET(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_qty_suom          IN      NUMBER,
                           I_lp_size           IN      NUMBER,
                           I_round_to_lp_pct   IN      NUMBER,
                           O_qty_eval          OUT     NUMBER,
                           O_rounded           OUT     BOOLEAN)
RETURN BOOLEAN IS
   L_function                   VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.EVAL_LAYER_PALLET';
   L_num_lp                     NUMBER(12,4);

BEGIN
  /* round up if the partial layer/pallet is >= round_to_layer/pallet_pct; do not round down */
   L_num_lp := I_qty_suom/I_lp_size;
   
   if TRUNC(L_num_lp) = L_num_lp then
      O_qty_eval := I_qty_suom;
      O_rounded := FALSE;
   else
      if L_num_lp > 0 then
         if (L_num_lp - TRUNC(L_num_lp) >= I_round_to_lp_pct) then
            O_qty_eval := (TRUNC(L_num_lp) + 1) * I_lp_size;
            O_rounded := TRUE; 
         else
            O_qty_eval := I_qty_suom;
            O_rounded  := FALSE;
         end if;
      elsif L_num_lp < 0 then
         if (TRUNC(L_num_lp) - L_num_lp >= I_round_to_lp_pct) then
            O_qty_eval := (TRUNC(L_num_lp) - 1) * I_lp_size;
            O_rounded := TRUE;
         else 
            O_qty_eval := I_qty_suom;
            O_rounded := FALSE;
         end if;
      else
         O_qty_eval := 0;
         O_rounded := TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EVAL_LAYER_PALLET;
-------------------------------------------------------------------------------
FUNCTION ADJUST_ITEM_MIN_MAX(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.ADJUST_ITEM_MIN_MAX';
   L_total_order_qty              ORDLOC.QTY_ORDERED%TYPE;
   L_total_residual               ORDLOC.QTY_ORDERED%TYPE;
BEGIN
   /*
    * For Location level scaling, adjust item min/max based on the percentage
    * of the item ordered to the location. Ignore the negative qty.
    * For item/loc that starts with negative qty, it can be possibly scaled
    * beyond the item min/max. Rplprg.pc program will catch any order that
    * exceeds the item min/max.
    */
   for i in 1..LP_scaling_tbl.count LOOP
      if (LP_scaling_tbl(i).item_min_order_qty is NOT NULL and
          LP_scaling_tbl(i).item_max_order_qty is NOT NULL) or
          LP_scaling_tbl(i).non_scale_ind = 'Y' then
          continue;
      end if;
      
      if(LP_scale_item_loc_tbl(i).qty_scaled_suom_rnd(1) <= 0) then
         continue;
      end if;
      L_total_order_qty := 0;
      for j in 1..LP_scaling_tbl.count LOOP 
         if (LP_scaling_tbl(i).item = LP_scaling_tbl(j).item and
             LP_scale_item_loc_tbl(j).qty_scaled_suom_rnd(1) > 0) then
            L_total_order_qty := L_total_order_qty + LP_scale_item_loc_tbl(j).qty_scaled_suom_rnd(1);
         end if;
      END LOOP;
      /* add xdock residual */
      if CALC_TOTAL_ITEM_RESIDUAL(O_error_message,
                                  LP_scaling_tbl(i).item,
                                  L_total_residual) = FALSE then 
         return FALSE;
      end if;
      L_total_order_qty := L_total_order_qty + L_total_residual;
      if (LP_scaling_tbl(i).item_min_order_qty is NULL) then
         LP_scaling_tbl(i).item_min_order_qty := LP_scaling_tbl(i).item_min_order_qty * LP_scale_item_loc_tbl(i).qty_scaled_suom_rnd(1)/L_total_order_qty;
      end if;
      if (LP_scaling_tbl(i).item_max_order_qty is NULL) then
         LP_scaling_tbl(i).item_max_order_qty := LP_scaling_tbl(i).item_max_order_qty * LP_scale_item_loc_tbl(i).qty_scaled_suom_rnd(1)/L_total_order_qty;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADJUST_ITEM_MIN_MAX;
-------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_ITEM_RESIDUAL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_item              IN      ITEM_LOC.ITEM%TYPE,
                                  O_total_residual    IN OUT  ORDLOC.QTY_ORDERED%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CALC_TOTAL_ITEM_RESIDUAL';
   L_xdock_from_loc               SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_TBL;
   
BEGIN
   O_total_residual := 0;
   
   for i in 1..LP_xdock_from_loc_tbl.COUNT LOOP
      if (LP_xdock_from_loc_tbl(i).item = I_item and
          LP_xdock_from_loc_tbl(i).residual_qty > 0) then
         O_total_residual := O_total_residual + LP_xdock_from_loc_tbl(i).residual_qty;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_TOTAL_ITEM_RESIDUAL;
-------------------------------------------------------------------------------
FUNCTION ADJUST_CONSTRAINT(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.ADJUST_CONSTRAINT';
   
BEGIN
   if SETUP_SCALE_CNSTR_SUPER(O_error_message) = FALSE then
      return FALSE;
   end if;
   
   if INIT_SCALE_LOC_ARRAY(O_error_message) = FALSE then
      return FALSE;
   end if;

   for i in  1..LP_total_vloc LOOP
      if LP_ord_inv_mgmt_rec.scale_cnstr_lvl = 'L' then
         /* find index of pwh/store record in LP_scale_cnstr_super table. */
         for j in 1..LP_total_ordloc LOOP
            if (LP_scale_loc_tbl(i).pwh_index = -1 and
               (LP_scale_loc_tbl(i).physical_wh = LP_scale_cnstr_super_tbl(j)(1).location or
                LP_scale_loc_tbl(i).location = LP_scale_cnstr_super_tbl(j)(1).location)) then
               LP_scale_loc_tbl(i).pwh_index := j;
               LP_scale_loc_case_tbl(i).pwh_index := j;
            end if;
         END LOOP;
      else
         LP_scale_loc_tbl(i).pwh_index := 1;
         LP_scale_loc_case_tbl(i).pwh_index := 1;
      end if;
      if CALC_TOTAL_CNSTR_QTY(O_error_message,
                              LP_scale_loc_tbl,
                              LP_scale_item_loc_tbl,
                              i,
                              1,
                              TRUE) = FALSE then
         return FALSE;
      end if;
   END LOOP;
   
   if CALC_SUPER_MIN_MAX(O_error_message) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADJUST_CONSTRAINT;
-------------------------------------------------------------------------------
FUNCTION SETUP_SCALE_CNSTR_SUPER(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.SETUP_SCALE_CNSTR_SUPER';
   L_cnt                          NUMBER := 1;
BEGIN
   /*
    * Based on the scaling level defined on the ord_inv_mgmt table ('O'rder or
    * 'L'ocation), set up struct scale_cnstr_super.
    * For Order level scaling, only location index 1 of the table type is used
    * and the location is set to '-1'.
    * For Location level scaling, the xdock locations go by the source_wh.
    */
   LP_total_ordloc := 0;  
   
   if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl = 'O') then /* order level scaling */
      for i in 1..LP_total_scale_cnstr LOOP
         LP_scale_cnstr_super_tbl(1)(i).scale_cnstr_type := LP_scale_cnstr_tbl(i).scale_cnstr_type;
         LP_scale_cnstr_super_tbl(1)(i).location := -1;
      END LOOP;
      LP_total_ordloc := 1;
   else /* location level scaling */
      for j in 1..LP_scaling_tbl.count LOOP
         for k in 1..LP_total_ordloc LOOP
            L_cnt := k;
            
            if ((LP_scaling_tbl(j).source_wh = -1 and
                 LP_scaling_tbl(j).location = LP_scale_cnstr_super_tbl(k)(1).location) or
                (LP_scaling_tbl(j).source_wh <> -1 and 
                 LP_scaling_tbl(j).source_wh = LP_scale_cnstr_super_tbl(k)(1).location) or
                (LP_scaling_tbl(j).location <> LP_scaling_tbl(j).physical_wh and 
                 LP_scaling_tbl(j).physical_wh = LP_scale_cnstr_super_tbl(k)(1).location)) then
               EXIT;
            end if;
            if k = LP_total_ordloc then
               L_cnt := k + 1;
            end if;
         END LOOP;
        
         if (L_cnt > LP_total_ordloc) then
            /* add a new location to scale_cnstr_super */
            for i in 1..LP_total_scale_cnstr LOOP
               LP_scale_cnstr_super_tbl(L_cnt)(i).scale_cnstr_type := LP_scale_cnstr_tbl(i).scale_cnstr_type;
               if (LP_scaling_tbl(j).location <> LP_scaling_tbl(j).physical_wh and /* not crossdock loc, source and physical wh equal -1 */
                   LP_scaling_tbl(j).loc_type = 'S') then /* and store loc type */
                  LP_scale_cnstr_super_tbl(L_cnt)(i).location := LP_scaling_tbl(j).location;
               else
                  LP_scale_cnstr_super_tbl(L_cnt)(i).location := LP_scaling_tbl(j).physical_wh;
               end if;
            END LOOP;
            LP_total_ordloc := LP_total_ordloc + 1;
         end if;
      END LOOP;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_SCALE_CNSTR_SUPER;
-------------------------------------------------------------------------------
FUNCTION INIT_SCALE_LOC_ARRAY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.INIT_SCALE_LOC_ARRAY';
   L_next_index                   NUMBER := 1;
   L_duplicate                    BOOLEAN := FALSE;
BEGIN
   for i in 1..LP_scaling_tbl.count LOOP
      /* only fill the scale loc tbl with one record for each virtual warehouse or store. */
      if (LP_scaling_tbl(i).physical_wh <> LP_scaling_tbl(i).location) then
         L_duplicate := FALSE;
         for z in 1..LP_scale_loc_tbl.count LOOP
            if (LP_scaling_tbl(i).location = LP_scale_loc_tbl(z).location) then
               L_duplicate := TRUE;
               EXIT;
            end if;
         END LOOP;
         if L_duplicate = FALSE then
            LP_scale_loc_tbl(L_next_index).location := LP_scaling_tbl(i).location;
            LP_scale_loc_tbl(L_next_index).loc_type := LP_scaling_tbl(i).loc_type;
            LP_scale_loc_tbl(L_next_index).physical_wh := LP_scaling_tbl(i).physical_wh;
            LP_scale_loc_tbl(L_next_index).rounding_seq := LP_scaling_tbl(i).rounding_seq;
            LP_scale_loc_tbl(L_next_index).last_index := 1;
            LP_scale_loc_tbl(L_next_index).sol_index := 1;
            LP_scale_loc_tbl(L_next_index).pwh_index := -1;
            LP_scale_loc_tbl(L_next_index).scaled_ind := 0;
            
            LP_scale_loc_case_tbl(L_next_index).location := LP_scaling_tbl(i).location;
            LP_scale_loc_case_tbl(L_next_index).loc_type := LP_scaling_tbl(i).loc_type;
            LP_scale_loc_case_tbl(L_next_index).physical_wh := LP_scaling_tbl(i).physical_wh;
            LP_scale_loc_case_tbl(L_next_index).rounding_seq := LP_scaling_tbl(i).rounding_seq;
            LP_scale_loc_case_tbl(L_next_index).last_index := 1;
            LP_scale_loc_case_tbl(L_next_index).sol_index := 1;
            LP_scale_loc_case_tbl(L_next_index).pwh_index := -1;
            LP_scale_loc_case_tbl(L_next_index).scaled_ind := 0;
            
            if LP_scaling_tbl(i).source_wh <> -1 then
               LP_scale_loc_tbl(L_next_index).has_source := 1;
               LP_scale_loc_tbl(L_next_index).rounding_seq := LP_scaling_tbl(L_next_index).location;
               LP_scale_loc_case_tbl(L_next_index).has_source := 1;
               LP_scale_loc_case_tbl(L_next_index).rounding_seq := LP_scaling_tbl(L_next_index).location;
            else
               LP_scale_loc_tbl(L_next_index).has_source := 0;
               LP_scale_loc_case_tbl(L_next_index).has_source := 0;
            end if;
            LP_total_vloc := LP_total_vloc + 1;
            L_next_index := L_next_index + 1;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INIT_SCALE_LOC_ARRAY;
-------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_CNSTR_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_scale_loc         IN OUT  SUP_CONSTRAINTS_SQL.SCALE_LOC_TBL,
                              I_scale_item_loc    IN      SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                              I_loc_index         IN      NUMBER,
                              I_sol_index         IN      NUMBER,
                              I_want_residual     IN      BOOLEAN)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CALC_TOTAL_CNSTR_QTY';
   L_cnstr_qty                    NUMBER(12,4) := 0;
   L_pwh_index                    NUMBER;
   
BEGIN
   L_pwh_index := O_scale_loc(I_loc_index).pwh_index;
   /* calculate total qty for a given location in all scaling cnstr types; */
   /* for location level scaling, only add up qty in the same location.    */
   for i in 1..LP_total_scale_cnstr LOOP
      O_scale_loc(I_loc_index).solution_day(I_sol_index).qty_scaled(i) := 0;
      /* add up positive qty in the driving cursor for the location */
      for j in 1..LP_scaling_tbl.count LOOP
         if ((LP_scaling_tbl(j).source_wh = -1 and
              I_scale_item_loc(j).location <> O_scale_loc(I_loc_index).location) or
             (LP_scaling_tbl(j).source_wh <> -1 and 
              LP_scaling_tbl(j).location <> O_scale_loc(I_loc_index).location)) then
            continue;
         end if;
         if I_scale_item_loc(j).qty_scaled_suom_rnd(I_sol_index) > 0 then
            L_cnstr_qty := 0;
            if CALC_CNSTR_QTY(O_error_message,
                              LP_scale_cnstr_super_tbl(L_pwh_index)(i).scale_cnstr_type,
                              j,
                              I_scale_item_loc(j).qty_scaled_suom_rnd(I_sol_index),
                              L_cnstr_qty) = FALSE then
               return FALSE;
            end if;
            
            O_scale_loc(I_loc_index).solution_day(I_sol_index).qty_scaled(i) := O_scale_loc(I_loc_index).solution_day(I_sol_index).qty_scaled(i) + L_cnstr_qty;
         end if;
      END LOOP;--j
      if (I_want_residual) then
         /* add up positive residual qty in xdock source_wh for the location */
         L_cnstr_qty := 0;
         if CALC_TOTAL_RESIDUAL_CNSTR_QTY(O_error_message,
                                          LP_scale_cnstr_super_tbl(L_pwh_index)(i).scale_cnstr_type,
                                          O_scale_loc(I_loc_index).location,
                                          L_cnstr_qty) = FALSE then
            return FALSE;
         end if;
         O_scale_loc(I_loc_index).solution_day(I_sol_index).qty_scaled(i) := O_scale_loc(I_loc_index).solution_day(I_sol_index).qty_scaled(i) + L_cnstr_qty;
      end if;
   END LOOP;--i
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_TOTAL_CNSTR_QTY;
-------------------------------------------------------------------------------
FUNCTION CALC_CNSTR_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_scale_cnstr_type  IN      ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                        I_cur_index         IN      NUMBER,
                        I_qty               IN      NUMBER,
                        O_qty               OUT     NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CALC_CNSTR_QTY';
   
BEGIN
   if I_scale_cnstr_type = 'V' then
      O_qty := (I_qty/LP_scaling_tbl(I_cur_index).supp_pack_size)*LP_scaling_tbl(I_cur_index).volume;
   elsif I_scale_cnstr_type = 'M' then
      O_qty := (I_qty/LP_scaling_tbl(I_cur_index).supp_pack_size)*LP_scaling_tbl(I_cur_index).ship_carton_wt;   
   elsif I_scale_cnstr_type = 'A' then
      O_qty := I_qty * LP_scaling_tbl(I_cur_index).ord_unit_cost;
   elsif I_scale_cnstr_type = 'P' then
      O_qty := I_qty / LP_scaling_tbl(I_cur_index).pallet_size;
   elsif I_scale_cnstr_type = 'C' then
      O_qty := I_qty / LP_scaling_tbl(I_cur_index).supp_pack_size;
   elsif I_scale_cnstr_type = 'E' then
      O_qty := I_qty * LP_scaling_tbl(I_cur_index).uom_conv_factor;
   elsif I_scale_cnstr_type = 'S' then
      O_qty := (I_qty/LP_scaling_tbl(I_cur_index).supp_pack_size)*LP_scaling_tbl(I_cur_index).stat_cube;
   else
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_INV_CNSTR_ORD',I_scale_cnstr_type,NULL,NULL);
      LP_NON_FATAL := TRUE;
      return FALSE;
      
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_CNSTR_QTY;
-------------------------------------------------------------------------------
FUNCTION CALC_TOTAL_RESIDUAL_CNSTR_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_scale_cnstr_type  IN      ORD_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                                       I_location          IN      ITEM_LOC.LOC%TYPE,
                                       O_total_cnstr_qty   OUT     NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CALC_TOTAL_RESIDUAL_CNSTR_QTY';
   L_xdock_from_loc               SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_TBL;
   L_xdock_to_loc                 SUP_CONSTRAINTS_SQL.XDOCK_TO_LOC_TBL;
   L_cnstr_qty                    ORDLOC.QTY_ORDERED%TYPE;
   
BEGIN
   /*
    * Function traverses the xdock tbl and returns the total residual qty
    * for the source_wh in the scaling constraint type.
    * If is_location = "-1", it means Order level scaling; add up the residual
    * qty for all source_wh.  If is_location != "-1", it means Location level
    * scaling; return the residual qty for the given source_wh.
    */
   O_total_cnstr_qty := 0;
   L_xdock_from_loc := LP_xdock_from_loc_tbl;
   for i in 1..L_xdock_from_loc.count LOOP
      L_xdock_to_loc := L_xdock_from_loc(i).xdock_to_loc;
      if L_xdock_to_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_NO_CRSDCK_LOC',TO_CHAR(L_xdock_from_loc(i).source_wh),NULL,NULL);
         return FALSE;
      end if;
      if (I_location = -1) or
         (I_location <> -1 and 
          I_location = L_xdock_from_loc(i).source_wh) then
         if (L_xdock_from_loc(i).residual_qty > 0) then
            if CALC_CNSTR_QTY(O_error_message,
                              I_scale_cnstr_type,
                              L_xdock_to_loc(1).driv_cursor_index,
                              L_xdock_from_loc(i).residual_qty,
                              L_cnstr_qty) = FALSE then
               return FALSE;
            end if;
            O_total_cnstr_qty := O_total_cnstr_qty + L_cnstr_qty;
         end if;
      end if;
      
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_TOTAL_RESIDUAL_CNSTR_QTY;
-------------------------------------------------------------------------------
FUNCTION CALC_SUPER_MIN_MAX(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CALC_SUPER_MIN_MAX';
   
   L_locations                    NUMBER;
   L_num_truck_load               NUMBER(12,4);
   L_max_truck_load               NUMBER(12,4);
   L_target_truck_load            NUMBER;
   L_balanced_max_val             NUMBER(12,4);
   L_balanced_min_val             NUMBER(12,4);
   L_pwh_index                    NUMBER;
   
   cursor C_VEHICLE_ROUND is 
      select vehicle_qty
        from vehicle_round
       where low_value <= L_max_truck_load
         and high_value >= L_max_truck_load;
   
BEGIN
   /*
    * Guidelines for calculating super min and super max:
    * 1) if order quantity <= 0: target truck load = 1; super min/max = original
    *    scaling cnstr min/max; no balance needed;
    * 2) if scaling cnstr max == MAX_VALUE: target truck load = 1; super
    *    min/max = original scaling cnstr min/max; no balance needed;
    * 3) if secondary cnstr is "A" (currency): super min/max = original scaling
    *    cnstr min/max; no balance needed; target truck load can be >= 1;
    * 4) if primary cnstr is the most constraining: no need to balance any of
    *    the constraints; calculate super min/max as usual.
    * 5) order quantity is the total adjusted prescaled_qty defined in solution
    *    day 0 of the ga_scale_loc structure.
    */
   L_locations := LP_total_vloc;
   
   LP_total_truck_load := 0;
   for i in 1..L_locations LOOP
      L_pwh_index := LP_scale_loc_tbl(i).pwh_index;
      /* find the most constraining constraint */
      L_max_truck_load := 0;
      LP_most_constraining := 1;
      for j in 1..LP_total_scale_cnstr LOOP
         if ((LP_scale_loc_tbl(i).solution_day(1).qty_scaled(j) <=0)  or
             (LP_scale_cnstr_tbl(j).scale_cnstr_max_val = MAX_VALUE) or
             ( j <> 1 and LP_scale_cnstr_super_tbl(L_pwh_index)(j).scale_cnstr_type = 'A')) then
            continue;
         end if;
         L_num_truck_load := LP_scale_loc_tbl(i).solution_day(1).qty_scaled(j)/LP_scale_cnstr_tbl(j).scale_cnstr_max_val;
         if ( L_num_truck_load > L_max_truck_load) then
            L_max_truck_load := L_num_truck_load;
            LP_most_constraining := j;
         end if;
      END LOOP; --j
      /*
       * Guidelines for calculating the number of truck loads:
       * 1) for truck load > 10, use .5 rounding rule: 10.49=>10; 10.50=>11
       * 2) for truck load not defined on vehicle_round, use .5 rounding rule
       * 3) for truck load 0, set target truck load to 1
       */
      if L_max_truck_load = 0 then
         L_target_truck_load := 1;
      elsif L_max_truck_load > 10 then
         L_target_truck_load := TRUNC(L_max_truck_load + 0.5);
      else
         SQL_LIB.SET_MARK('OPEN','C_VEHICLE_ROUND','VEHICLE_ROUND',NULL);
         open C_VEHICLE_ROUND;
         SQL_LIB.SET_MARK('FETCH','C_VEHICLE_ROUND','VEHICLE_ROUND',NULL);
         fetch C_VEHICLE_ROUND into L_target_truck_load;
         if C_VEHICLE_ROUND%NOTFOUND then
            L_target_truck_load := TRUNC(L_max_truck_load + 0.5);
         end if;
         SQL_LIB.SET_MARK('CLOSE','C_VEHICLE_ROUND','VEHICLE_ROUND',NULL);
         close C_VEHICLE_ROUND;
         if L_target_truck_load = 0 then
            L_target_truck_load := 1;
         end if;
      end if;
      /* balance constraint and calculate super min/super max */
      for j in 1..LP_total_scale_cnstr LOOP
         if((LP_scale_loc_tbl(i).solution_day(1).qty_scaled(j) <= 0) or
            (LP_scale_cnstr_tbl(j).scale_cnstr_max_val = MAX_VALUE) or 
            ( j<> 0 and LP_scale_cnstr_super_tbl(L_pwh_index)(j).scale_cnstr_type = 'A')) then

            LP_scale_cnstr_super_tbl(L_pwh_index)(j).scale_cnstr_super_min := LP_scale_cnstr_tbl(j).scale_cnstr_min_val;
            LP_scale_cnstr_super_tbl(L_pwh_index)(j).scale_cnstr_super_max := LP_scale_cnstr_tbl(j).scale_cnstr_max_val;
         else
            if (LP_most_constraining = 1 or j = LP_most_constraining) then
               L_balanced_max_val := LP_scale_cnstr_tbl(j).scale_cnstr_max_val;
               L_balanced_min_val := LP_scale_cnstr_tbl(j).scale_cnstr_min_val;
            else
               L_balanced_max_val := LP_scale_loc_tbl(i).solution_day(1).qty_scaled(j) / L_max_truck_load;
               L_balanced_min_val := LP_scale_cnstr_tbl(j).scale_cnstr_min_val * (L_balanced_max_val / LP_scale_cnstr_tbl(j).scale_cnstr_max_val);
            end if;

            if LP_ord_inv_mgmt_rec.mult_vehicle_ind = 'Y' then /* multiple loads */
               LP_scale_cnstr_super_tbl(L_pwh_index)(j).scale_cnstr_super_max := L_balanced_max_val * L_target_truck_load;
               LP_scale_cnstr_super_tbl(L_pwh_index)(j).scale_cnstr_super_min := L_balanced_max_val * (L_target_truck_load - 1) + L_balanced_min_val;
            else /* single load */
               LP_scale_cnstr_super_tbl(L_pwh_index)(j).scale_cnstr_super_max := L_balanced_max_val;
               LP_scale_cnstr_super_tbl(L_pwh_index)(j).scale_cnstr_super_min := L_balanced_min_val;
            end if;
         end if;
         /* calculate super min/max with tolerance */
         LP_scale_cnstr_super_tbl(L_pwh_index)(j).scale_cnstr_super_min_tol := LP_scale_cnstr_super_tbl(L_pwh_index)(j).scale_cnstr_super_min *
                                                                               (1 - LP_scale_cnstr_tbl(j).scale_cnstr_min_tol / 100);
         LP_scale_cnstr_super_tbl(L_pwh_index)(j).scale_cnstr_super_max_tol := LP_scale_cnstr_super_tbl(L_pwh_index)(j).scale_cnstr_super_max *
                                                                               (1 + LP_scale_cnstr_tbl(j).scale_cnstr_max_tol / 100);
         /* calculate total number of truckload */

         if (LP_ord_inv_mgmt_rec.mult_vehicle_ind = 'Y') then
            LP_total_truck_load := LP_total_truck_load + L_target_truck_load;
         else
            LP_total_truck_load := LP_total_truck_load + 1;
         end if;

      END LOOP;
   END LOOP;--i

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_SUPER_MIN_MAX;
-------------------------------------------------------------------------------
FUNCTION SCALE_PROCESS(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.SCALE_PROCESS';
   L_sol_day                      NUMBER := 0;
   L_sol_case                     NUMBER := 0;
   L_done                         BOOLEAN;
   L_return                       BOOLEAN := FALSE;
   L_count                        NUMBER;
   L_scale_cnstr_rank2            VARCHAR2(6);
BEGIN

   for i in 1..LP_total_ordloc LOOP
      L_done := FALSE;
      /* ---- rank original order ---- */
      if RANK_SOLUTION(O_error_message,
                       LP_scale_loc_tbl,
                       i,
                       1) = FALSE then
         return FALSE;
      end if;
      
      for j in 1..LP_total_vloc LOOP
         if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and
             LP_scale_cnstr_super_tbl(i)(1).location not in(LP_scale_loc_tbl(j).physical_wh,LP_scale_loc_tbl(j).location)) then
            continue;
         end if;
         /* set initial scaling direction */
         
         if LP_scale_loc_tbl(j).solution_day(1).cnstr_rank.count > 1 then
            L_scale_cnstr_rank2 := LP_scale_loc_tbl(j).solution_day(1).cnstr_rank(2);
         else
            L_scale_cnstr_rank2 := NULL;
         end if;
         if CHECK_DIRECTION(O_error_message,
                            LP_scale_loc_tbl(j).solution_day(1).cnstr_rank(1),
                            L_scale_cnstr_rank2,
                            'Y',
                            SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP,
                            LP_scale_loc_tbl(j).direction) =  FALSE then
            return FALSE;             
         end if;
          /* ---- start scaling ---- */
          
         if LP_scale_loc_tbl(j).direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_NO then
            if(LP_ord_inv_mgmt_rec.scale_cnstr_lvl = 'L') then
               O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_ORD_NOT_SCL_L',TO_CHAR(LP_scale_loc_tbl(j).location),NULL,NULL);
            else
               O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_ORD_NOT_SCL_O',NULL,NULL,NULL);
            end if;
            LP_NON_FATAL := TRUE;
            return FALSE;
         elsif LP_scale_loc_tbl(j).direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_STOP then
            if (L_return = TRUE) then
               if LP_ord_inv_mgmt_rec.scale_cnstr_lvl = 'L' then
                  O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_ORD_ALRDY_SCL_L',TO_CHAR(LP_scale_loc_tbl(j).location),NULL,NULL);
               else
                  O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_ORD_ALRDY_SCL_O',NULL,NULL,NULL);
               end if;
               for k in 1..LP_scaling_tbl.count LOOP
                  if UPDATE_ORD_QTY(O_error_message,
                                    LP_scale_loc_tbl,
                                    LP_order_no,
                                    LP_scaling_tbl(k).item,
                                    LP_scaling_tbl(k).location,
                                    i) = FALSE then
                     return FALSE;
                  end if;
               END LOOP;
               L_done := TRUE;
               continue;
            end if;-- L_retutn
         end if;
      END LOOP;--vloc
      if L_done then
         continue;
      end if;
      L_count := 2;
 
      if SCALE(O_error_message,
               i,
               L_sol_day,
               L_sol_case,
               L_count) =  FALSE then
         return FALSE;
      else
         L_return := TRUE;
      end if;

      for k in 1..LP_total_vloc LOOP
         if(LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and
            LP_scale_cnstr_super_tbl(i)(1).location NOT IN (LP_scale_loc_tbl(k).physical_wh,LP_scale_loc_tbl(k).location)) then
            continue;
         end if;
         if LP_scale_loc_case_tbl(k).solution_day(L_sol_case).accept_ind = 'N' then 
            if LP_ord_inv_mgmt_rec.scale_cnstr_lvl = 'L' then
               O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_SCALE_NO_SOL_L',TO_CHAR(LP_scale_loc_case_tbl(k).location),NULL,NULL);
            else
               O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_SCALE_NO_SOL_O',NULL,NULL,NULL);
            end if;
            LP_NON_FATAL := TRUE;
            return FALSE;
         end if;
         if WRITE_SCALED_QTY(O_error_message,
                             k,
                             L_sol_case) = FALSE then
            return FALSE;
         end if;
      END LOOP;--vloc

   END LOOP;--i in total ordloc
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SCALE_PROCESS;
-------------------------------------------------------------------------------
FUNCTION RANK_SOLUTION(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_scale_loc         IN OUT  SUP_CONSTRAINTS_SQL.SCALE_LOC_TBL,
                       I_loc_index         IN      NUMBER,
                       I_sol_index         IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.RANK_SOLUTION';
   L_total_qty                    NUMBER(12,4) := 0;
   L_tmp_rank_code                VARCHAR2(1);
BEGIN

   for h in 1..LP_total_vloc LOOP
      if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and 
          LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (O_scale_loc(h).physical_wh,O_scale_loc(h).location)) then
         continue;
      end if;
      for i in 1..LP_total_scale_cnstr LOOP
         for j in 1..LP_total_vloc LOOP
            if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and 
                LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (O_scale_loc(j).physical_wh,O_scale_loc(j).location)) then
               continue;
            else
               L_total_qty := L_total_qty + O_scale_loc(j).solution_day(I_sol_index).qty_scaled(i);
            end if;
         END LOOP;
         
         if EVAL_RANK(O_error_message,
                      L_total_qty,
                      LP_scale_cnstr_super_tbl(I_loc_index)(i).scale_cnstr_super_min,
                      LP_scale_cnstr_super_tbl(I_loc_index)(i).scale_cnstr_super_min_tol,
                      LP_scale_cnstr_super_tbl(I_loc_index)(i).scale_cnstr_super_max,
                      LP_scale_cnstr_super_tbl(I_loc_index)(i).scale_cnstr_super_max_tol,
                      O_scale_loc(h).solution_day(I_sol_index).cnstr_rank(i)) = FALSE then
            return FALSE;
         end if;
         L_tmp_rank_code := SUBSTR(O_scale_loc(h).solution_day(I_sol_index).cnstr_rank(i),1,1); -- Substr to assign only 'G' for 'GX'
         if LENGTH(O_scale_loc(h).solution_day(I_sol_index).rank_codes) = 2
         then
            O_scale_loc(h).solution_day(I_sol_index).rank_codes := NULL;
         end if;
         O_scale_loc(h).solution_day(I_sol_index).rank_codes := CONCAT(O_scale_loc(h).solution_day(I_sol_index).rank_codes,L_tmp_rank_code);
         L_total_qty := 0;
      END LOOP; -- i in LP_total_scale_cnstr
      if GET_RANK_ORDER(O_error_message,
                        O_scale_loc(h).solution_day(I_sol_index).rank_codes,
                        O_scale_loc(h).solution_day(I_sol_index).rank_order,
                        O_scale_loc(h).solution_day(I_sol_index).accept_ind) = FALSE then
         return FALSE;
      end if;
   END LOOP; -- h in vloc
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END RANK_SOLUTION;
-------------------------------------------------------------------------------
FUNCTION EVAL_RANK(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   I_qty               IN      NUMBER,
                   I_min               IN      NUMBER,
                   I_min_tol           IN      NUMBER,
                   I_max               IN      NUMBER,
                   I_max_tol           IN      NUMBER,
                   O_rank              OUT     VARCHAR)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.EVAL_RANK';
   
BEGIN
   if I_qty < I_min_tol then
      O_rank := 'RL'; -- Red Low
   elsif I_qty < I_min then
      O_rank := 'YL'; -- Yellow Low
   elsif I_qty <= I_max then
      O_rank := 'GX'; -- Green
   elsif I_qty <= I_max_tol then
      O_rank := 'YH'; -- Yellow High
   else
      O_rank := 'RH'; -- Red High
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EVAL_RANK;
-------------------------------------------------------------------------------
FUNCTION GET_RANK_ORDER(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_rank_code         IN      VARCHAR2,
                        O_rank_order        IN OUT  NUMBER,
                        O_accept_ind        OUT     VARCHAR2)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.GET_RANK_ORDER';
   L_cnt                          BINARY_INTEGER := NULL;
BEGIN
   for i in 1..MAX_NUM_RANK LOOP
      if LP_rank_priority_tbl(i).rank_code = I_rank_code then
         O_rank_order := LP_rank_priority_tbl(i).rank_order;
         O_accept_ind := LP_rank_priority_tbl(i).accept_ind;
         L_cnt := i;
         EXIT;
      end if;
   END LOOP;
   if L_cnt is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_RANK_NO_DEF',I_rank_code,NULL,NULL);
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RANK_ORDER;
-------------------------------------------------------------------------------
FUNCTION CHECK_DIRECTION(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rank_1            IN      VARCHAR2,
                         I_rank_2            IN      VARCHAR2,
                         I_changeable        IN      VARCHAR2,
                         I_scale_dir         IN      NUMBER,
                         O_eval_result       OUT     NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CHECK_DIRECTION';
   L_rank_1                       VARCHAR2(3);
   L_rank_2                       VARCHAR2(3);
   L_direction                    NUMBER := 0;
BEGIN
   /*
    * Based on the current constraint ranking, determine which direction
    * scaling should be heading to reach a valid solution. It is used to
    * stop the process if the solution is only going to get worse.
    * Guidelines:
    * 1) if only one constraint is defined, set the 2nd rank to be the
    *    same as the 1st rank;
    * 2) if one of the ranks is "GX" (Green), set it to be the same as the
    *    other non-Green rank;
    * 3) if both ranks are "GX", for search LOW, stop the process; for
    *    search HIGH, continue in the original scaling direction if scaling
    *    direction is UP, stop if scaling direction is DOWN;
    * 4) otherwise, find direction from eval_system;
    * 5) if suggested direction contradicts the scaling direction, and
    *    changeable is "N", stop the process.
    */
   L_rank_1 := I_rank_1;
   L_rank_2 := I_rank_2;
   if L_rank_2 is NULL then -- only one constraint
      L_rank_2 := L_rank_1;
   end if;
   if L_rank_1 = 'GX' and L_rank_2 <> 'GX' then
      L_rank_1 := L_rank_2;
   end if;
   if L_rank_2 = 'GX' and L_rank_1 <> 'GX' then
      L_rank_2 := L_rank_1;
   end if;
   
   if (L_rank_1 = 'GX' and L_rank_2 = 'GX') or
      (L_rank_1 = 'YL' and L_rank_2 = 'YL') or
      (L_rank_1 = 'YH' and L_rank_2 = 'YH') then
      if LP_ord_inv_mgmt_rec.scale_cnstr_obj = 'M' then --search low
         O_eval_result := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_STOP;
      else -- search high
         if I_scale_dir = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP then
            O_eval_result := I_scale_dir;
         else
            if I_changeable = 'Y' then
               O_eval_result := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP;
            else
               O_eval_result := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_STOP;
            end if;
         end if;
      end if;
      return TRUE;
   end if;

   if GET_EVAL_SYSTEM(O_error_message,
                      L_rank_1,
                      L_rank_2,
                      L_direction) = FALSE then 
      return FALSE;
   end if;

   if L_direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_NO then
      O_eval_result := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_NO;
   elsif L_direction = I_scale_dir then
      O_eval_result := L_direction;
   else
      if I_changeable = 'Y' then
         O_eval_result := L_direction;
      else
         O_eval_result := SUP_CONSTRAINTS_SQL.LP_RANK_DIR_STOP;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_DIRECTION;
-------------------------------------------------------------------------------
FUNCTION GET_EVAL_SYSTEM(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rank_1            IN      VARCHAR2,
                         I_rank_2            IN      VARCHAR2,
                         O_direction         OUT     NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.GET_EVAL_SYSTEM';
   L_tmp                          NUMBER := NULL;
BEGIN
   for i in 1..MAX_NUM_EVAL LOOP
      if (LP_eval_system_tbl(i).rank_1 = I_rank_1 and 
          LP_eval_system_tbl(i).rank_2 = I_rank_2) then
         O_direction := LP_eval_system_tbl(i).direction;
         L_tmp := i;
         EXIT;
      end if;
   END LOOP;
   if L_tmp is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_RANK_DIR_DEF',I_rank_1,I_rank_2,NULL);
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_EVAL_SYSTEM;
-------------------------------------------------------------------------------
FUNCTION UPDATE_ORD_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_scale_loc         IN OUT  SUP_CONSTRAINTS_SQL.SCALE_LOC_TBL,
                        I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                        I_item              IN      ITEM_LOC.ITEM%TYPE,
                        I_location          IN      ITEM_LOC.LOC%TYPE,
                        I_loc_index         IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.UPDATE_ORD_QTY';
   L_total_qty                    NUMBER(12,4) := 0;
   RECORD_LOCKED                  EXCEPTION;
   PRAGMA                         EXCEPTION_INIT(Record_Locked, -54);
BEGIN
   for i in 1..LP_scaling_tbl.count LOOP
      for j in 1..LP_total_vloc LOOP 
         if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and 
             LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (O_scale_loc(j).physical_wh,O_scale_loc(j).location)) then
            continue;
         else
            L_total_qty := L_total_qty + LP_scaling_tbl(i).qty_ordered;
         end if;
      END LOOP; -- j
      if (LP_scale_cnstr_super_tbl(I_loc_index).EXISTS(i) = TRUE) then
         if (L_total_qty < LP_scale_cnstr_super_tbl(I_loc_index)(i).scale_cnstr_super_min_tol or
             L_total_qty > LP_scale_cnstr_super_tbl(I_loc_index)(i).scale_cnstr_super_max_tol) then
            SQL_LIB.SET_MARK('UPDATE',NULL,'ORDLOC','Order:' || TO_CHAR(I_order_no)|| ' item:'|| I_item);
            update ordloc
               set qty_ordered = qty_prescaled,
                   last_rounded_qty = qty_prescaled
             where order_no = I_order_no
               and item = I_item
               and location = I_location;
         end if;
      end if;
      L_total_qty := 0;
   END LOOP; -- i
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'ORDLOC',
                                             TO_CHAR(I_order_no),
                                             TO_CHAR(I_item));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_ORD_QTY;
-------------------------------------------------------------------------------
FUNCTION SCALE(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
               I_loc_index         IN      NUMBER,
               O_sol_day           OUT     NUMBER,
               O_sol_case          OUT     NUMBER,
               O_cnt               IN OUT  NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.SCALE';
   L_eval_result                  NUMBER;
   L_last_day                     NUMBER;
   L_sol_day                      NUMBER := 1; 
   L_sol_case                     NUMBER := 1;
   L_max_sol_day                  NUMBER := 1;
   L_max_sol_case                 NUMBER := 1;
   L_sol_index                    NUMBER; /* solution tbl index for case-pallet scaling */
   L_first                        BOOLEAN;
   L_tmp                          NUMBER;
   L_i                            NUMBER;
   L_scale_cnstr_rank2            VARCHAR2(6);
   
BEGIN
   /* days of supply scaling */
   for i in O_cnt..LP_ord_inv_mgmt_rec.max_scaling_iterations + 1 LOOP
      L_first := TRUE;
      L_i := i;
      for k in  1..LP_total_vloc LOOP
         if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and 
             LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (LP_scale_loc_tbl(k).physical_wh,LP_scale_loc_tbl(k).location)) then
            continue;
         end if;
         if COPY_FROM_PREV_ITER(O_error_message,
                                LP_scale_loc_tbl,
                                LP_scale_item_loc_tbl,
                                k,
                                i) = FALSE then
            return FALSE;
         end if;
         if GEN_ONE_DAY_SUPPLY(O_error_message,
                               k,
                               i) = FALSE then
            return FALSE;
         end if;

         for j in 1..LP_total_vloc LOOP
            if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and
                LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (LP_scale_loc_tbl(j).physical_wh,LP_scale_loc_tbl(j).location) or 
                LP_scale_loc_tbl(j).location = LP_scale_loc_tbl(k).location) then
               continue;
            end if;
            if L_first then
               if COPY_FROM_PREV_ITER(O_error_message,
                                      LP_scale_loc_tbl,
                                      LP_scale_item_loc_tbl,
                                      j,
                                      i) = FALSE then
                  return FALSE;
               end if;
               L_first := FALSE;
            end if;
            if CALC_TOTAL_CNSTR_QTY(O_error_message,
                                    LP_scale_loc_tbl,
                                    LP_scale_item_loc_tbl,
                                    j,
                                    i,
                                    TRUE) = FALSE then
               return FALSE;
            end if;
         END LOOP; -- end for j

         if CALC_TOTAL_CNSTR_QTY(O_error_message,
                                 LP_scale_loc_tbl,
                                 LP_scale_item_loc_tbl,
                                 k,
                                 i,
                                 TRUE) = FALSE then
            return FALSE;
         end if;
         if RANK_SOLUTION(O_error_message,
                          LP_scale_loc_tbl,
                          I_loc_index,
                          i) = FALSE then
            return FALSE;
         end if;
         if LP_scale_loc_tbl(k).solution_day(i).cnstr_rank.count > 1 then
            L_scale_cnstr_rank2 := LP_scale_loc_tbl(k).solution_day(i).cnstr_rank(2);
         else
            L_scale_cnstr_rank2 := NULL;
         end if;
         if CHECK_DIRECTION(O_error_message,
                            LP_scale_loc_tbl(k).solution_day(i).cnstr_rank(1),
                            L_scale_cnstr_rank2,
                            'N',
                            LP_scale_loc_tbl(k).direction,
                            L_eval_result) = FALSE then
            return FALSE;
         end if;

         LP_scale_loc_tbl(k).scaled_ind := 1;
         
         if (L_eval_result = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_NO or
             L_eval_result = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_STOP) then
            EXIT;
         end if;
      END LOOP; -- k in total vloc
      if (L_eval_result in (SUP_CONSTRAINTS_SQL.LP_RANK_DIR_NO,SUP_CONSTRAINTS_SQL.LP_RANK_DIR_STOP)) then 
         EXIT;
      end if;
      if L_i = LP_ord_inv_mgmt_rec.max_scaling_iterations + 1 then
         L_i := L_i + 1;
      end if;
   END LOOP; -- i 
   for l in 1..LP_total_vloc LOOP
      if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and 
          LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (LP_scale_loc_tbl(l).physical_wh,LP_scale_loc_tbl(l).location)) then
         continue;
      end if;
      if LP_scale_loc_tbl(l).has_source <> 0 then 
         continue;
      end if;
      /* find the best day of scaling */
      if (L_i > (LP_ord_inv_mgmt_rec.max_scaling_iterations + 1)) then
         L_last_day :=  L_i - 1;
      else
         L_last_day := L_i;
      end if;
      if EVAL_SOLUTION(O_error_message,
                       LP_scale_loc_tbl,
                       l,
                       L_last_day,
                       L_sol_day) = FALSE then
         return FALSE;
      end if;
      LP_scale_loc_tbl(l).last_index := L_last_day;
      LP_scale_loc_tbl(l).sol_index := L_sol_day;
   END LOOP; -- l

   for l in 1..LP_total_vloc LOOP
    
      if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and 
          LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (LP_scale_loc_tbl(l).physical_wh,LP_scale_loc_tbl(l).location)) then
         continue;
      end if;
      if LP_scale_loc_tbl(l).scaled_ind <> 0 then
         LP_scale_loc_case_tbl(l).scaled_ind := 1;
      end if;
      /* return NONFATAL error if day-of-supply scaling has reached max_scaling_iterations count */
      if ( L_i > (LP_ord_inv_mgmt_rec.max_scaling_iterations + 1)) then
         if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl = 'L') then
            O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_NO_VALID_SOL_L',TO_CHAR(LP_scale_loc_tbl(l).location),NULL,NULL);
         else
            O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_NO_VALID_SOL_OA',TO_CHAR(L_i),NULL,NULL);
         end if;
         LP_NON_FATAL := TRUE;
         return FALSE;
      end if;
   END LOOP;
   if TRUNCATE_SOLUTION_DAY(O_error_message,
                            L_sol_day) = FALSE then
      return FALSE;
   end if;
   /* case/pallet rounding on solution day */
   if CASE_PALLET_ROUND_ALL(O_error_message,
                            I_loc_index,
                            L_sol_day) = FALSE then
      return FALSE;
   end if;

   for l in 1..LP_total_vloc LOOP
      if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and 
          LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (LP_scale_loc_tbl(l).physical_wh,LP_scale_loc_tbl(l).location)) then
         continue;
      end if;
      if CALC_TOTAL_CNSTR_QTY(O_error_message,
                              LP_scale_loc_case_tbl,
                              LP_scale_item_loc_case_tbl,
                              l,
                              1,
                              FALSE) = FALSE then
         return FALSE;                 
      end if;
   END LOOP;

   for l in 1..LP_total_vloc LOOP
      if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and 
          LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (LP_scale_loc_tbl(l).physical_wh,LP_scale_loc_tbl(l).location)) then
         continue;
      end if;
      if RANK_SOLUTION(O_error_message,
                       LP_scale_loc_case_tbl,
                       I_loc_index,
                       1) = FALSE then
         return FALSE;
      end if;
      if LP_scale_loc_case_tbl(l).solution_day(1).cnstr_rank.count > 1 then
         L_scale_cnstr_rank2 := LP_scale_loc_case_tbl(l).solution_day(1).cnstr_rank(2);
      else
         L_scale_cnstr_rank2 := NULL;
      end if;
       /* can change scaling direction after rounding */
       if CHECK_DIRECTION(O_error_message,
                          LP_scale_loc_case_tbl(l).solution_day(1).cnstr_rank(1),
                          L_scale_cnstr_rank2,
                          'Y',
                          LP_scale_loc_tbl(l).direction,
                          LP_scale_loc_case_tbl(l).direction) = FALSE then
          return FALSE;
       end if;
       if (LP_scale_loc_case_tbl(l).direction in (SUP_CONSTRAINTS_SQL.LP_RANK_DIR_NO,SUP_CONSTRAINTS_SQL.LP_RANK_DIR_STOP)) then
          O_sol_day := L_sol_day;
          O_sol_case := 1;
          O_cnt := L_i;
          return TRUE;
       end if;
   END LOOP;

   /* case or pallet scaling */
   L_sol_index := 1;
   for i in O_cnt..LP_ord_inv_mgmt_rec.max_scaling_iterations + 1 LOOP
      L_i := i;
      /* Each iteration will use a single item for 'case or pallet' scaling unlike */
      /* all items are used for 'days of supply' scaling per iteration            */
      L_tmp := MOD(L_sol_index,LP_scaling_tbl.count) + 1;
      L_sol_index := L_sol_index + 1;
      L_first := TRUE;
      if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and 
         LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (LP_scaling_tbl(L_tmp).physical_wh,LP_scaling_tbl(L_tmp).location)) then
         continue;
      end if;

      if LP_scaling_tbl(L_tmp).non_scale_ind = 'Y' then
         for n in 1..LP_scaling_tbl.count LOOP
            -- Added to assign values to uninitialized indices
            if (LP_scale_item_loc_case_tbl(n).qty_scaled_suom.EXISTS(L_sol_index - 1) = FALSE) then
               LP_scale_item_loc_case_tbl(n).qty_scaled_suom(L_sol_index - 1) := 0;
            end if;
            if (LP_scale_item_loc_case_tbl(n).qty_scaled_suom_rnd.EXISTS(L_sol_index - 1) = FALSE) then
               LP_scale_item_loc_case_tbl(n).qty_scaled_suom_rnd(L_sol_index - 1) := 0;
            end if;
            LP_scale_item_loc_case_tbl(n).qty_scaled_suom(L_sol_index) := LP_scale_item_loc_case_tbl(n).qty_scaled_suom(L_sol_index -1);
            LP_scale_item_loc_case_tbl(n).qty_scaled_suom_rnd(L_sol_index) := LP_scale_item_loc_case_tbl(n).qty_scaled_suom_rnd(L_sol_index -1);
         END LOOP;
         continue;
      end if;
      for k in 1..LP_total_vloc LOOP
         if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and
             LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (LP_scale_loc_case_tbl(k).physical_wh,LP_scale_loc_case_tbl(k).location)) or
            (LP_scale_loc_case_tbl(k).location <> LP_scaling_tbl(L_tmp).location) then
            continue;
         end if;
         if COPY_FROM_PREV_ITER(O_error_message,
                                LP_scale_loc_case_tbl,
                                LP_scale_item_loc_case_tbl,
                                k,
                                L_sol_index) = FALSE then
            return FALSE;
         end if;
         if L_first then
            for l in 1..LP_total_vloc LOOP
               if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and
                   LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (LP_scale_loc_case_tbl(l).physical_wh,LP_scale_loc_case_tbl(l).location)) or
                  (LP_scale_loc_case_tbl(l).location = LP_scale_loc_case_tbl(k).location) then
                  continue;
               end if;
               
               if COPY_FROM_PREV_ITER(O_error_message,
                                      LP_scale_loc_case_tbl,
                                      LP_scale_item_loc_case_tbl,
                                      l,
                                      L_sol_index) = FALSE then
                  return FALSE;
               end if;
               
               if CALC_TOTAL_CNSTR_QTY(O_error_message,
                                       LP_scale_loc_case_tbl,
                                       LP_scale_item_loc_case_tbl,
                                       l,
                                       L_sol_index,
                                       FALSE) = FALSE then
                  return FALSE;
               end if;
            END LOOP; -- l
            L_first :=  FALSE;
         end if; -- End L_first

         if GEN_ONE_CASE_SUPPLY(O_error_message,
                                k,
                                L_sol_day,
                                L_tmp,
                                L_sol_index) = FALSE then 
            return FALSE;
         end if;
         
         if CALC_TOTAL_CNSTR_QTY(O_error_message,
                                 LP_scale_loc_case_tbl,
                                 LP_scale_item_loc_case_tbl,
                                 k,
                                 L_sol_index,
                                 FALSE) = FALSE then 
            return FALSE;
         end if;

         if RANK_SOLUTION(O_error_message,
                          LP_scale_loc_case_tbl,
                          I_loc_index,
                          L_sol_index) = FALSE then 
            return FALSE;
         end if;
         if LP_scale_loc_case_tbl(k).solution_day(L_sol_index).cnstr_rank.count > 1 then
            L_scale_cnstr_rank2 := LP_scale_loc_case_tbl(k).solution_day(L_sol_index).cnstr_rank(2);
         else
            L_scale_cnstr_rank2 := NULL;
         end if;
         if CHECK_DIRECTION(O_error_message,
                            LP_scale_loc_case_tbl(k).solution_day(L_sol_index).cnstr_rank(1),
                            L_scale_cnstr_rank2,
                            'N',
                            LP_scale_loc_case_tbl(k).direction,
                            L_eval_result) = FALSE then
           return FALSE;
         end if;

         if L_eval_result in (SUP_CONSTRAINTS_SQL.LP_RANK_DIR_NO,SUP_CONSTRAINTS_SQL.LP_RANK_DIR_STOP) then
            EXIT;
         end if;

         LP_scale_loc_case_tbl(k).scaled_ind := 1;
      END LOOP;--k in total vloc
      if L_eval_result in (SUP_CONSTRAINTS_SQL.LP_RANK_DIR_NO,SUP_CONSTRAINTS_SQL.LP_RANK_DIR_STOP) then
         EXIT;
      end if;
      if (L_i = (LP_ord_inv_mgmt_rec.max_scaling_iterations + 1)) then
         L_i := L_i + 1;
      end if;
   END LOOP;--i max scaling iterations

   for m in 1..LP_total_vloc LOOP
      if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and 
          LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (LP_scale_loc_tbl(m).physical_wh,LP_scale_loc_tbl(m).location)) then
         continue;
      end if;
      if LP_scale_loc_tbl(m).has_source <> 0 then
         continue;
      end if;

      /* find the best instance of case/pallet scaling */
      if EVAL_SOLUTION(O_error_message,
                       LP_scale_loc_case_tbl,
                       m,
                       L_sol_index,
                       L_sol_case) = FALSE then
         return FALSE;
      end if;
      LP_scale_loc_case_tbl(m).last_index := L_sol_index;
      
      if (L_sol_case < L_max_sol_case) then
         LP_scale_loc_case_tbl(m).sol_index := L_max_sol_case;
         if COPY_FROM_PREV_ITER(O_error_message,
                                LP_scale_loc_case_tbl,
                                LP_scale_item_loc_case_tbl,
                                m,
                                L_i) = FALSE then
            return FALSE;
         end if;
      else
         LP_scale_loc_case_tbl(m).sol_index := L_sol_case;
         L_max_sol_day := L_sol_day;
      end if;
   END LOOP; -- m in total vloc

   for m in 1..LP_total_vloc LOOP
      if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and 
          LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (LP_scale_loc_tbl(m).physical_wh,LP_scale_loc_tbl(m).location)) then
         continue;
      end if;
      
      if (L_i > (LP_ord_inv_mgmt_rec.max_scaling_iterations + 1)) then
         if LP_ord_inv_mgmt_rec.scale_cnstr_lvl = 'L' then
            O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_NO_VALID_SOL_L',TO_CHAR(LP_scale_loc_case_tbl(I_loc_index).location),NULL,NULL);
         else
            O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_NO_VALID_SOL_OB',TO_CHAR(L_i),NULL,NULL);
         end if;
         LP_NON_FATAL := TRUE;
         return FALSE;
      end if;
   END LOOP;
   
   O_sol_day := L_sol_day;
   O_sol_case := L_sol_case;
   O_cnt := L_i;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SCALE;
-------------------------------------------------------------------------------
FUNCTION COPY_FROM_PREV_ITER(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_scale_loc_tbl      IN OUT  SUP_CONSTRAINTS_SQL.SCALE_LOC_TBL,
                             O_scale_item_loc_tbl IN OUT  SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                             I_loc_index          IN      NUMBER,
                             I_sol_index          IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.COPY_FROM_PREV_ITER';
   L_pwh_index                    NUMBER;
BEGIN
   L_pwh_index := O_scale_loc_tbl(I_loc_index).pwh_index;

   for i in 1..LP_scaling_tbl.count LOOP
      if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl = 'L') then
         if (LP_scaling_tbl(i).source_wh = -1 and
             O_scale_loc_tbl(I_loc_index).location <> LP_scaling_tbl(i).location) or
            (LP_scaling_tbl(i).source_wh <> -1 and
             O_scale_loc_tbl(I_loc_index).location <> LP_scaling_tbl(i).source_wh) then
            continue;
         end if;
      end if;
      -- Added to assign values to uninitialized indices
      if (O_scale_item_loc_tbl(i).qty_scaled_suom.EXISTS(I_sol_index - 1) = FALSE) then
         O_scale_item_loc_tbl(i).qty_scaled_suom(I_sol_index - 1) := 0;
      end if;
      if (O_scale_item_loc_tbl(i).qty_scaled_suom_rnd.EXISTS(I_sol_index - 1) = FALSE) then
         O_scale_item_loc_tbl(i).qty_scaled_suom_rnd(I_sol_index - 1) := 0;
      end if;
      O_scale_item_loc_tbl(i).qty_scaled_suom(I_sol_index) := O_scale_item_loc_tbl(i).qty_scaled_suom(I_sol_index - 1);
      O_scale_item_loc_tbl(i).qty_scaled_suom_rnd(I_sol_index) := O_scale_item_loc_tbl(i).qty_scaled_suom_rnd(I_sol_index - 1);
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END COPY_FROM_PREV_ITER;
-------------------------------------------------------------------------------
FUNCTION GEN_ONE_DAY_SUPPLY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_loc_index         IN      NUMBER,
                            I_day               IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.GEN_ONE_DAY_SUPPLY';
   L_day                          NUMBER;
   L_order_date                   ORDHEAD.WRITTEN_DATE%TYPE;
   L_in_season                    VARCHAR2(1);
   L_adjust_qty                   NUMBER(12,4) := 0;
   L_proposed_qty                 NUMBER(12,4);
   L_scalable                     BOOLEAN := TRUE;
   
   
   cursor C_CHECK_SEASON(I_season_id         REPL_RESULTS.SEASON_ID%TYPE,
                         I_phase_id          REPL_RESULTS.PHASE_ID%TYPE,
                         I_date              REPL_RESULTS.REPL_DATE%TYPE) is 
      select 'x'
        from phases
       where season_id = I_season_id
         and phase_id = I_phase_id
         and start_date <= I_date
         and end_date >= I_date;
BEGIN
   /*
    * Function generate a day-of-supply for all item on the order:
    * 1) for manual order , buyer pack and non-dynamic repl order, use 1 case
    *    as a day-of-supply;
    * 2) for dynamic repl order, a day-of-supply is based on the forecasting data
    *    for the item/loc on order date; order date is item/loc order_upto_date
    *    +/- num of days into scaling;
    * 3) do not scale if store is closed on order date;
    * 4) for seasonal dynamic replenishment, scaling should be shut off if order
    *    date falls out side of the season range;
    * 5) for each scaled item, check item and item/loc level non-scaling cnstrs.
    */
   if LP_scale_loc_tbl(I_loc_index).direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN then
      L_day := I_day * (-1);
   else
      L_day := I_day;
   end if;
   for i in 1..LP_scaling_tbl.count LOOP
      if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl = 'L') then
         if (LP_scaling_tbl(i).source_wh = -1 and
             LP_scale_loc_tbl(I_loc_index).location <> LP_scaling_tbl(i).location) or
            (LP_scaling_tbl(i).source_wh <> -1 and
             LP_scaling_tbl(I_loc_index).location <> LP_scaling_tbl(i).source_wh) then
            continue;
         end if;
      end if;
      if LP_scaling_tbl(i).non_scale_ind = 'Y' then
         LP_scale_item_loc_tbl(i).message(I_day) := SQL_LIB.CREATE_MSG('SUPCSTR_NON_SCL_Y',NULL,NULL,NULL);
         continue;
      end if;
      /* if the scaling direction is DOWN and item/loc starts with a <= 0 qty, */
      /* turn off scaling to keep it from being scaled. */
      if LP_scale_loc_tbl(I_loc_index).direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN then
         if LP_scale_item_loc_tbl(i).qty_scaled_suom(I_day) <= 0 then
            LP_scaling_tbl(i).non_scale_ind := 'Y';
            LP_scale_item_loc_tbl(i).message(I_day) := SQL_LIB.CREATE_MSG('SUPCSTR_SCL_FLAG_OFF',NULL,NULL,NULL);
            continue;
         end if;
      end if;
      if GET_ORDER_DATE(O_error_message,
                        LP_scaling_tbl(i).order_upto_point_date,
                        L_day,
                        L_order_date) = FALSE then 
         return FALSE;
      end if;
      /* do not scale if store is closed */
      if LP_scaling_tbl(i).store_close_date is NOT NULL then
         if LP_scaling_tbl(i).store_close_date <= L_order_date then
            LP_scale_item_loc_tbl(i).message(I_day) := SQL_LIB.CREATE_MSG('SUPCSTR_STR_CLOSD',NULL,NULL,NULL);
            continue;
         end if;
      end if;
      
      /* for manual order, or buyer pack, or non-dynamic replenishment order */
      /* use one case as a day of supply */
      if (LP_scaling_tbl(i).repl_method is NULL or
          LP_scaling_tbl(i).buyer_pack_ind = 'Y' or
          LP_scaling_tbl(i).repl_method in ('C','M','F')) then
         L_adjust_qty := LP_scaling_tbl(i).supp_pack_size; /* one case */
      else
         /* for seasonal dynamic replenishment order ("T", "D" with NOT NULL */
         /* season and phase id), do not adjust qty if adjusted date falls out */
         /* of the season range */
         L_in_season := 'y';
         if (LP_scaling_tbl(i).repl_method in ('T','D') and
             LP_scaling_tbl(i).season_id is NOT NULL and
             LP_scaling_tbl(i).phase_id is NOT NULL) then
            SQL_LIB.SET_MARK('OPEN','C_CHECK_SEASON','PHASES','phase_id:' || LP_scaling_tbl(i).phase_id); 
            open C_CHECK_SEASON(LP_scaling_tbl(i).season_id,
                                LP_scaling_tbl(i).phase_id,
                                L_order_date);
            SQL_LIB.SET_MARK('FETCH','C_CHECK_SEASON','PHASES','phase_id:' || LP_scaling_tbl(i).phase_id); 
            fetch C_CHECK_SEASON into L_in_season;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_SEASON','PHASES','phase_id:' || LP_scaling_tbl(i).phase_id); 
            close C_CHECK_SEASON;
            if L_in_season <> 'x' then
               LP_scale_item_loc_tbl(i).message(I_day) := SQL_LIB.CREATE_MSG('SUPCSTR_NS_OUT_OF_SSN',NULL,NULL,NULL);
               continue;
            end if;
         end if;
         /* for dynamic replenishment order (T,D,TI,DI), get forecasting data */
         if GET_FORECAST_DATA(O_error_message,
                              i,
                              L_order_date,
                              L_adjust_qty) = FALSE then
            return FALSE;
         end if;
         
         if(L_adjust_qty = 0) then 
            LP_scale_item_loc_tbl(i).message(I_day) := SQL_LIB.CREATE_MSG('SUPCSTR_NS_NO_FRCST',NULL,NULL,NULL);
            continue;
         end if;
      end if;
      /* if pack item is on replenishment scale should be */
      /* based on pack item qty otherwise component qty   */
      if (LP_scaling_tbl(i).pack_ind = 'Y' and
          LP_scaling_tbl(i).repl_method is NOT NULL ) then
         L_adjust_qty := L_adjust_qty / LP_scaling_tbl(i).pack_item_qty;
      end if;
      /* if scale down, adjust a negative quantity */
      if (LP_scale_loc_tbl(I_loc_index).direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN) then
         L_adjust_qty := L_adjust_qty * (-1);
      end if;
      
      /* evaluate non-scaling constraint */
      L_proposed_qty := LP_scale_item_loc_tbl(i).qty_scaled_suom(I_day) + L_adjust_qty;

      if EVAL_NON_SCALE_CNSTR(O_error_message,
                              LP_scale_loc_tbl(I_loc_index).direction,
                              SUP_CONSTRAINTS_SQL.LP_DAY,
                              LP_scale_item_loc_tbl,
                              i,
                              I_day,
                              L_proposed_qty,
                              L_scalable) = FALSE then
         return FALSE;
      end if;
      if L_scalable = TRUE then
         /* when scale down to <= 0 during day scaling, change non-scaling flag */
         /* to prevent it from being added during case scaling */
         if (LP_scale_loc_tbl(I_loc_index).direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN and 
             L_proposed_qty <= 0 ) then
            LP_scaling_tbl(i).non_scale_ind  := 'Y';
            LP_scale_item_loc_tbl(i).message(I_day) := SQL_LIB.CREATE_MSG('SUPCSTR_QTY_SCLD_NEG',NULL,NULL,NULL);
         end if;
         LP_scale_item_loc_tbl(i).qty_scaled_suom(I_day) := L_proposed_qty;
         LP_scale_item_loc_tbl(i).qty_scaled_suom_rnd(I_day) := L_proposed_qty;
      end if;
   END LOOP;-- i in total count

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GEN_ONE_DAY_SUPPLY;
-------------------------------------------------------------------------------
FUNCTION GET_FORECAST_DATA(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cur_index         IN      NUMBER,
                           I_order_date        IN      ORDHEAD.WRITTEN_DATE%TYPE,
                           O_sales_qty         OUT     ORDLOC.QTY_ORDERED%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.GET_FORECAST_DATA';
   L_order_date                   ORDHEAD.WRITTEN_DATE%TYPE;
   L_eow_date                     ORDHEAD.WRITTEN_DATE%TYPE;
   L_daily_data_found_ind         BOOLEAN;
   /* indices for binary search */
   L_low                          BINARY_INTEGER;
   L_high                         BINARY_INTEGER;
   L_mid                          BINARY_INTEGER; 
   
BEGIN
   /* If there is daily forecasting data in the table (total_day_fetched != 0), */
   /* then call get_daily_forecast_data to get daily forecasted sales.          */
   if LP_forecast_item_loc(I_cur_index).total_day_fetched <> 0 then
      if GET_DAILY_FORECAST_DATA(O_error_message,
                                 I_cur_index,
                                 I_order_date,
                                 O_sales_qty,
                                 L_daily_data_found_ind) = FALSE then
         return FALSE;
      end if;
      /* If the indicator is TRUE, then we found the daily data */
      /* and we can exit without finding the weekly data.    */
      if (L_daily_data_found_ind) then
         return TRUE;
      end if;
   end if;
   /* find the forecasted sales for the order date in the forecasting tbl    */
   /* eow_date-7 < order date <= eow                                         */
   /* i.e.: eow_date < order date + 7 and eow_date >= order date             */
   O_sales_qty := 0;
   
   /* no forecasting data in the tbl */
   if LP_forecast_item_loc(I_cur_index).total_eow_fetched = 0 then
      return TRUE;
   end if;
   /* use binary search on the eow_date */
   L_low := 1;
   L_high := LP_forecast_item_loc(I_cur_index).total_eow_fetched;
   while (L_low <= L_high) LOOP
      L_mid := TRUNC((L_low + L_high) / 2);
      L_eow_date := LP_forecast_item_loc(I_cur_index).eow_date(L_mid);
      
      if (L_eow_date < L_order_date + 7) and 
         L_eow_date >= L_order_date then
         O_sales_qty := LP_forecast_item_loc(I_cur_index).sales(L_mid);
         EXIT;
      elsif L_eow_date < L_order_date then
         L_low := L_mid + 1;
      elsif L_eow_date >= L_order_date + 7 then
         L_high := L_mid - 1;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_FORECAST_DATA;
-------------------------------------------------------------------------------
FUNCTION GET_DAILY_FORECAST_DATA(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cur_index         IN      NUMBER,
                                 I_order_date        IN      ORDHEAD.WRITTEN_DATE%TYPE,
                                 O_sales_qty         OUT     ORDLOC.QTY_ORDERED%TYPE,
                                 O_daily_data_found  OUT     BOOLEAN)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.GET_DAILY_FORECAST_DATA';
   L_data_date                    ORDHEAD.WRITTEN_DATE%TYPE;
   /* indices for binary search */
   L_low                          BINARY_INTEGER;
   L_high                         BINARY_INTEGER;
   L_mid                          BINARY_INTEGER; 
   
BEGIN
   O_sales_qty := 0;
   O_daily_data_found :=  FALSE;
   
   L_low := 1;
   L_high := LP_forecast_item_loc(I_cur_index).total_day_fetched;
   while(L_low <= L_high) LOOP
      L_mid := (L_low + L_high )/2;
      L_data_date := LP_forecast_item_loc(I_cur_index).data_date(L_mid);
      if (L_data_date = I_order_date) then
         O_sales_qty := LP_forecast_item_loc(I_cur_index).day_sales(L_mid);
         O_daily_data_found := TRUE;
         EXIT;
      elsif L_data_date < I_order_date then
         L_low := L_mid + 1;
      elsif L_data_date > I_order_date then
         L_high := L_mid - 1;
      end if;
   END LOOP;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_DAILY_FORECAST_DATA;
-------------------------------------------------------------------------------
FUNCTION EVAL_NON_SCALE_CNSTR(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_direction         IN      NUMBER,
                              I_day_case          IN      NUMBER,
                              O_scale_item_loc    IN OUT  SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                              I_cur_index         IN      NUMBER,
                              I_sol_index         IN      NUMBER,
                              I_proposed_qty      IN OUT  NUMBER,
                              O_scalable          OUT     BOOLEAN)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.EVAL_NON_SCALE_CNSTR';
   
   L_item_qty                     NUMBER(12,4);
   L_total_residual               NUMBER(12,4);
BEGIN
  /*
   * This function checks the following non-scaling constraints if defined:
   * 1) item/loc max if scaling direction is UP
   * 2) item/loc min if scaling direction is DOWN
   * 3) item/supplier max if scaling direction is UP
   * 4) item/supplier min if scaling direction is DOWN
   */
   
   /* check item/loc max for replenishment order: */
   /* max_scale_value holds the number of days dynamic replenishment can be increased */
   /* and the amount over the unscaled quantity that a non-dynamic replenishment order */
   /* can be increased; if max_scale_value is 0, it means infinity, i.e. do not check. */
   
   if LP_scaling_tbl(I_cur_index).max_scale_value <> 0 and
      I_direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP then
      if LP_scaling_tbl(I_cur_index).repl_method in ('T','D','TI','DI') then
         if I_day_case = SUP_CONSTRAINTS_SQL.LP_DAY and
            I_sol_index > LP_scaling_tbl(I_cur_index).max_scale_value then
            O_scale_item_loc(I_cur_index).message(I_sol_index) := SQL_LIB.CREATE_MSG('SUPCSTR_NS_ORD_DATE',NULL,NULL,NULL);
            O_scalable := FALSE;
            return TRUE;
         end if;
      else
         if I_proposed_qty > LP_scaling_tbl(I_cur_index).max_scale_value then
            O_scale_item_loc(I_cur_index).message(I_sol_index) := SQL_LIB.CREATE_MSG('SUPCSTR_NS_MAX_QTY',NULL,NULL,NULL);
            O_scalable := FALSE;
            return TRUE;
         end if;
      end if;
   end if;
   /* check item/loc min */
   if I_direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN then 
      if LP_scaling_tbl(I_cur_index).repl_method is NOT NULL then
         if LP_scaling_tbl(I_cur_index).repl_method in ('T','D','TI','DI') then
            if I_day_case = SUP_CONSTRAINTS_SQL.LP_DAY and
               I_sol_index > LP_scaling_tbl(I_cur_index).max_scale_down_day then
               O_scale_item_loc(I_cur_index).message(I_sol_index) := SQL_LIB.CREATE_MSG('SUPCSTR_NS_ORD_DATE',NULL,NULL,NULL);
               O_scalable := FALSE;
               return TRUE;
            end if;
         end if;
      else
         if I_proposed_qty < LP_scaling_tbl(I_cur_index).min_scale_value then
            /* if min_scale_value is 0, allow scaling back to 0 */
            if LP_scaling_tbl(I_cur_index).min_scale_value = 0 then
               I_proposed_qty := 0;
               O_scalable := TRUE;
               return TRUE;
            else
               O_scale_item_loc(I_cur_index).message(I_sol_index) := SQL_LIB.CREATE_MSG('SUPCSTR_NS_MIN_QTY',NULL,NULL,NULL);
               O_scalable := FALSE;
               return TRUE;
            end if;
           
         end if;
      end if;
   end if;--direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN
   
   /* check item/supplier min and max for entries with positive prescaled qty */
   if (LP_scaling_tbl(I_cur_index).item_min_order_qty is NULL and
       LP_scaling_tbl(I_cur_index).item_max_order_qty is NULL) or
       LP_scaling_tbl(I_cur_index).qty_prescaled <= 0 then
      O_scalable := TRUE;
      return TRUE;
   end if;
   if LP_ord_inv_mgmt_rec.scale_cnstr_lvl = 'L' then
      if I_proposed_qty > 0 then
         L_item_qty := I_proposed_qty;
      end if;
   else -- Order level scaling
      /* add up all positive scaled qty for the item in this iteration */
      L_item_qty := 0;
      for i in 1..LP_scaling_tbl.count LOOP
         if O_scale_item_loc(i).item = O_scale_item_loc(I_cur_index).item then
            continue;
         end if;
         if i = I_cur_index then
            if I_proposed_qty > 0 then
               L_item_qty := L_item_qty + I_proposed_qty;
            end if;
         else
            if O_scale_item_loc(i).qty_scaled_suom_rnd(I_sol_index) > 0 then
               L_item_qty := L_item_qty + O_scale_item_loc(i).qty_scaled_suom_rnd(I_sol_index);
            end if;
         end if; 
      END LOOP;
      /* add all xdock residual qty for the item */
      if CALC_TOTAL_ITEM_RESIDUAL(O_error_message,
                                  O_scale_item_loc(I_cur_index).item,
                                  L_total_residual) = FALSE then
         return FALSE;
      end if;
      L_item_qty  := L_item_qty + L_total_residual;
   end if; -- scaling level
   
   if LP_scaling_tbl(I_cur_index).item_min_order_qty is NOT NULL and
      I_direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN then
      if L_item_qty < LP_scaling_tbl(I_cur_index).item_min_order_qty then
         O_scale_item_loc(I_cur_index).message(I_sol_index) := SQL_LIB.CREATE_MSG('SUPCSTR_NS_ITM_MIN_QTY',NULL,NULL,NULL);
         O_scalable := FALSE;
      else
         O_scalable := TRUE;
      end if;
   end if;
   if LP_scaling_tbl(I_cur_index).item_max_order_qty is NOT NULL and
      I_direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP then
      if L_item_qty > LP_scaling_tbl(I_cur_index).item_max_order_qty then
         O_scale_item_loc(I_cur_index).message(I_sol_index) := SQL_LIB.CREATE_MSG('SUPCSTR_NS_ITM_MIN_QTY',NULL,NULL,NULL);
         O_scalable := FALSE;
      else
         O_scalable := TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EVAL_NON_SCALE_CNSTR;
-------------------------------------------------------------------------------
FUNCTION EVAL_SOLUTION(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_scale_loc         IN OUT  SUP_CONSTRAINTS_SQL.SCALE_LOC_TBL,
                       I_loc_index         IN      NUMBER,
                       I_last_day          IN      NUMBER,
                       O_sol_day           IN OUT  NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.EVAL_SOLUTION';
   
   L_sol_day                      NUMBER;
   L_sol_rank                     NUMBER;
   L_sol_qty                      NUMBER(12,4);
   L_sum_alloc                    NUMBER(12,4) := 0;
BEGIN
   /*
    * find the solution day with the best (lowest) rank;
    * for the same rank:
    * 1) when all ranks are Green:
    *    a) if search high: find solution with the highest total order qty;
    *    b) if search low: find solution with the lowest day count (1st encounter);
    * 2) when one or more ranks are Yellow or Red:
    *    a) if scaling direction is UP, find solution with highest total order qty;
         b) if scaling direction is DOWN, find solution with lowest total order qty;
    */
    L_sol_day := 1;
    L_sol_rank := O_scale_loc(I_loc_index).solution_day(1).rank_order;
    L_sol_qty  := O_scale_loc(I_loc_index).solution_day(1).qty_scaled(1);
    
    for i in 1..I_last_day LOOP
       L_sum_alloc := 0;
       for e in 1..LP_total_vloc LOOP
          if (O_scale_loc(I_loc_index).location = O_scale_loc(e).location) then
             continue;
          end if;
          if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and
              O_scale_loc(I_loc_index).location NOT IN (O_scale_loc(e).physical_wh,O_scale_loc(e).location)) then
             continue;
          end if;
          L_sum_alloc := L_sum_alloc + O_scale_loc(e).solution_day(i).qty_scaled(1);
       END LOOP; --e in total vloc
       -- Added to assign values to uninitialized indices
       if O_scale_loc(I_loc_index).solution_day.EXISTS(i) = FALSE then
          O_scale_loc(I_loc_index).solution_day(i).qty_scaled(1) := 0;
       end if;
       O_scale_loc(I_loc_index).solution_day(i).qty_scaled(1) := O_scale_loc(I_loc_index).solution_day(i).qty_scaled(1) + L_sum_alloc;
       if NVL(O_scale_loc(I_loc_index).solution_day(i).rank_order,-1) < NVL(L_sol_rank,-1) then
          L_sol_day := i;
          L_sol_rank := O_scale_loc(I_loc_index).solution_day(i).rank_order;
          L_sol_qty := O_scale_loc(I_loc_index).solution_day(i).qty_scaled(1);
       elsif NVL(O_scale_loc(I_loc_index).solution_day(i).rank_order,-1) = NVL(L_sol_rank,-1) then 
          if L_sol_rank = 0 then  /* rank "G" or "GG" */
            /* search high: get the highest qty */
            /* search low: use the first encounter */
            if LP_ord_inv_mgmt_rec.scale_cnstr_obj = 'X' and 
               O_scale_loc(I_loc_index).solution_day(i).qty_scaled(1) > L_sol_qty then
               L_sol_day := i;
               L_sol_qty := LP_scale_loc_tbl(I_loc_index).solution_day(i).qty_scaled(1);
            end if;
          else /* rank involves R or Y */
             if (O_scale_loc(I_loc_index).direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_UP and
                 O_scale_loc(I_loc_index).solution_day(i).qty_scaled(1) > L_sol_qty) or
                (O_scale_loc(I_loc_index).direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN and
                 O_scale_loc(I_loc_index).solution_day(i).qty_scaled(1) < L_sol_qty) then
                L_sol_day := i;
                L_sol_qty := O_scale_loc(I_loc_index).solution_day(i).qty_scaled(1);
             end if;
          end if;
       end if;
    END LOOP;--i till last day
    O_sol_day := L_sol_day;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EVAL_SOLUTION;
-------------------------------------------------------------------------------
FUNCTION CASE_PALLET_ROUND_ALL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_loc_index         IN      NUMBER,
                               I_sol_day           IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CASE_PALLET_ROUND_ALL';
   
   L_xdock_from_loc               SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_TBL := LP_xdock_from_loc_tbl;
   L_qty                          NUMBER(12,4) := 0;
   L_total_alloc_qty_rnd          NUMBER(12,4) := 0;
   L_total_qty                    NUMBER(12,4) := 0;
   L_total_qty_rnd                NUMBER(12,4) := 0;
   L_overage                      NUMBER(12,4) := 0;
   L_addition                     BOOLEAN := FALSE;
   L_rnd_seq                      WH.ROUNDING_SEQ%TYPE;
   
BEGIN
   /*
    * Function performs case/pallet rounding for each item/loc on solution day;
    * store results in index 1 of case/pallet scaling tbl.
    * For non-crossdock item/loc, round solution day qty to program round_lvl.
    * For crossdock item/loc,
    * 1) round solution day qty to store_ord_mult if source_wh break_pack_ind = 'Y';
    *    round solution day qty to case if source_wh break_pack_ind = 'N';
    * 2) round the total xdock qty in step 1 to ord_inv_mgmt round_lvl;
    * 3) calculate case/pallet rounding residual for the source_wh.
    */
   for a in 1..LP_total_vloc LOOP
      if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and
          LP_scale_cnstr_super_tbl(I_loc_index)(1).location NOT IN (LP_scale_loc_tbl(a).physical_wh,LP_scale_loc_tbl(a).location)) then
         continue;
      end if;
      for b in 1..LP_scaling_tbl.count LOOP
         L_addition := FALSE;
         if (LP_scaling_tbl(b).location = LP_scale_loc_tbl(a).location and
             LP_scale_item_loc_case_tbl(b).qty_scaled_suom(1) = 0) then
            L_rnd_seq := LP_scaling_tbl(b).rounding_seq;
            /* add up xdock allocation quantities for item/loc */
            for j in 1..LP_xdock_from_loc_tbl.count LOOP
               if (LP_scale_loc_tbl(a).location <> LP_xdock_from_loc_tbl(j).source_wh and
                   LP_scale_loc_tbl(a).physical_wh <> LP_scale_cnstr_super_tbl(I_loc_index)(1).location) then
                  continue;
               end if;
               if CALC_XDOCK_QTY(O_error_message,
                                 FALSE,
                                 LP_xdock_from_loc_tbl(j),
                                 LP_scale_item_loc_tbl,
                                 I_sol_day,
                                 LP_scale_item_loc_case_tbl,
                                 1,
                                 L_total_alloc_qty_rnd,
                                 LP_scale_item_loc_tbl(b).item) = FALSE then
                 return FALSE;
               end if;
            END LOOP;
            /*add up direct-to-location quantities */
            for c in 1..LP_scaling_tbl.count LOOP
               if (LP_scale_item_loc_tbl(b).physical_wh = LP_scale_item_loc_tbl(c).physical_wh and
                   LP_scale_item_loc_tbl(b).item = LP_scale_item_loc_tbl(c).item and
                   L_rnd_seq = LP_scale_item_loc_tbl(c).rounding_seq) then
                  L_qty := L_qty + LP_scale_item_loc_tbl(c).qty_scaled_suom(I_sol_day);
                  LP_scale_item_loc_case_tbl(c).qty_scaled_suom(1) := LP_scale_item_loc_tbl(c).qty_scaled_suom(I_sol_day);
                  LP_scale_item_loc_case_tbl(c).qty_scaled_suom_rnd(1) := LP_scale_item_loc_tbl(c).qty_scaled_suom_rnd(I_sol_day);
                  LP_scale_item_loc_tbl(c).rounding_seq := -1;
               end if;
            END LOOP;
            L_total_qty := L_qty + L_total_alloc_qty_rnd;
            
            if ROUND_TO_CASE_LAYER_PALLET(O_error_message,
                                          LP_scaling_tbl(b).round_lvl,
                                          LP_scaling_tbl(b).round_to_case_pct,
                                          LP_scaling_tbl(b).round_to_layer_pct,
                                          LP_scaling_tbl(b).round_to_pallet_pct,
                                          LP_scaling_tbl(b).supp_pack_size,
                                          LP_scaling_tbl(b).tier,
                                          LP_scaling_tbl(b).pallet_size,
                                          L_total_qty,
                                          L_total_qty_rnd) = FALSE then
               return FALSE;
            end if;
            L_overage := L_total_qty_rnd - L_total_qty;
            
            for d in 1..LP_scaling_tbl.count LOOP
               if (LP_scale_item_loc_tbl(d).item = LP_scaling_tbl(b).item and 
                   LP_scale_item_loc_tbl(d).location = LP_scaling_tbl(b).rounding_seq) or
                  (LP_scale_item_loc_tbl(d).location = LP_scaling_tbl(b).location and
                   LP_scaling_tbl(b).rounding_seq = -1) then
                  LP_scale_item_loc_case_tbl(d).qty_scaled_suom(1) := LP_scale_item_loc_case_tbl(d).qty_scaled_suom(1) + L_overage;
                  LP_scale_item_loc_case_tbl(d).qty_scaled_suom_rnd(1) := LP_scale_item_loc_case_tbl(d).qty_scaled_suom_rnd(1) + L_overage;
                  
                  L_overage := 0;
                  L_total_qty := 0;
                  L_qty := 0;
                  L_total_alloc_qty_rnd := 0;
                  L_total_qty_rnd := 0;
                  L_addition := TRUE;
                  EXIT;
               end if;
            END LOOP;
            if L_addition = FALSE then
               for e in 1..LP_scaling_tbl.count LOOP
                  if(LP_scale_item_loc_tbl(e).item = LP_scaling_tbl(b).item and
                     LP_scaling_tbl(e).rounding_seq = LP_scaling_tbl(b).rounding_seq) then
                     LP_scale_item_loc_case_tbl(e).qty_scaled_suom(1) := LP_scale_item_loc_case_tbl(e).qty_scaled_suom(1) + L_overage;
                     LP_scale_item_loc_case_tbl(e).qty_scaled_suom_rnd(1) := LP_scale_item_loc_case_tbl(e).qty_scaled_suom_rnd(1) + L_overage;
                     
                     L_overage := 0;
                     L_total_qty := 0;
                     L_qty := 0;
                     L_total_alloc_qty_rnd := 0;
                     L_total_qty_rnd := 0;
                     EXIT;
                  end if;
               END LOOP;
            end if;
         end if;
      END LOOP; --b in total rec fetched
   END LOOP;-- a in total vloc
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CASE_PALLET_ROUND_ALL;
-------------------------------------------------------------------------------
FUNCTION CALC_XDOCK_QTY(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_in_som              IN      BOOLEAN,
                        I_xdock_from_loc_rec  IN      SUP_CONSTRAINTS_SQL.XDOCK_FROM_LOC_REC,
                        I_from_scale_item_loc IN      SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                        I_from_index          IN      NUMBER,
                        O_to_scale_item_loc   IN OUT  SUP_CONSTRAINTS_SQL.SCALE_ITEM_LOC_TBL,
                        I_to_index            IN      NUMBER,
                        O_total_qty_rnd_case  IN OUT  NUMBER,
                        I_item                IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CALC_XDOCK_QTY';
   
   L_xdock_to_loc                 SUP_CONSTRAINTS_SQL.XDOCK_TO_LOC_TBL;
   L_driv_cursor_index            NUMBER;
   L_qty_rnd_som                  NUMBER(12,4);
   LP_total_qty_rnd_som           NUMBER(12,4) := 0;
   LP_total_qty_rnd_case          NUMBER(12,4) := 0;
   
BEGIN
   /*
    * Function returns the total xdock qty after case/pallet rounding and the
    * residual qty due to case/pallet rounding:
    * 1) for each xdock location sourced from the wh, round the qty to store_ord_mult
    *    if wh break_pack_ind is 'Y' or to case if wh break_pack_ind is 'N';
    * 2) add up the positive rounded xdock qty in step 1;
    * 3) round the total in step 2 to the round_lvl defined on ord_inv_mgmt;
    *    always round up by using rounding pct of 0;
    * 4) the difference between step 2 and 3 is the residual ordered to the wh.
    */
   L_xdock_to_loc := I_xdock_from_loc_rec.xdock_to_loc;
   for i in 1..L_xdock_to_loc.COUNT LOOP
      L_driv_cursor_index := L_xdock_to_loc(i).driv_cursor_index;
      if LP_scaling_tbl(L_driv_cursor_index).item <> I_item then
         continue;
      end if;
      
      if I_in_som = FALSE then
         if I_xdock_from_loc_rec.break_pack_ind = 'Y' then
            /* round xdock qty to store_ord_mult-'I'nner, 'C'ase, 'E'aches */
            if ROUND_TO_STORE_ORD_MULT(O_error_message,
                                       LP_scaling_tbl(L_driv_cursor_index).store_ord_mult,
                                       LP_scaling_tbl(L_driv_cursor_index).inner_pack_size,
                                       LP_scaling_tbl(L_driv_cursor_index).supp_pack_size,
                                       LP_scaling_tbl(L_driv_cursor_index).round_to_inner_pct,
                                       LP_scaling_tbl(L_driv_cursor_index).round_to_case_pct,
                                       I_from_scale_item_loc(L_driv_cursor_index).qty_scaled_suom_rnd(I_from_index),
                                       L_qty_rnd_som) = FALSE then
               return FALSE;
            end if;
         else
            /* round xdock qty to Case */
            if ROUND_TO_CASE_LAYER_PALLET(O_error_message,
                                          'C',
                                          LP_scaling_tbl(L_driv_cursor_index).round_to_case_pct,
                                          LP_scaling_tbl(L_driv_cursor_index).round_to_layer_pct,
                                          LP_scaling_tbl(L_driv_cursor_index).round_to_pallet_pct,
                                          LP_scaling_tbl(L_driv_cursor_index).supp_pack_size,
                                          LP_scaling_tbl(L_driv_cursor_index).tier,
                                          LP_scaling_tbl(L_driv_cursor_index).pallet_size,
                                          I_from_scale_item_loc(L_driv_cursor_index).qty_scaled_suom_rnd(I_from_index),
                                          L_qty_rnd_som) = FALSE then
               return FALSE;
            end if;
         end if;
      else
         L_qty_rnd_som := I_from_scale_item_loc(L_driv_cursor_index).qty_scaled_suom_rnd(I_from_index);
      end if; -- in_som
      
      O_to_scale_item_loc(L_driv_cursor_index).qty_scaled_suom(I_to_index) := L_qty_rnd_som;
      O_to_scale_item_loc(L_driv_cursor_index).qty_scaled_suom_rnd(I_to_index) := L_qty_rnd_som;
      
      if L_qty_rnd_som > 0 then
         O_total_qty_rnd_case := O_total_qty_rnd_case + L_qty_rnd_som;
      end if;
   END LOOP;--i
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_XDOCK_QTY;
-------------------------------------------------------------------------------
FUNCTION TRUNCATE_SOLUTION_DAY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_sol_day           IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.TRUNCATE_SOLUTION_DAY';
   
BEGIN
   for a in 1..LP_total_vloc LOOP
      if I_sol_day <= LP_scale_item_loc_tbl(a).qty_scaled_suom.count then 
         LP_scale_item_loc_tbl(a).qty_scaled_suom(I_sol_day) := FLOOR(LP_scale_item_loc_tbl(a).qty_scaled_suom(I_sol_day));
         LP_scale_item_loc_tbl(a).qty_scaled_suom_rnd(I_sol_day) := FLOOR(LP_scale_item_loc_tbl(a).qty_scaled_suom_rnd(I_sol_day));
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END TRUNCATE_SOLUTION_DAY;
-------------------------------------------------------------------------------
FUNCTION GEN_ONE_CASE_SUPPLY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_loc_index         IN      NUMBER,
                             I_sol_day           IN      NUMBER,
                             I_cur_index         IN      NUMBER,
                             I_sol_index         IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.GEN_ONE_CASE_SUPPLY';
   
   L_sol_day                      NUMBER;
   L_order_date                   ORDHEAD.WRITTEN_DATE%TYPE;
   L_in_season                    VARCHAR2(1);
   L_adjust_qty                   NUMBER(12,4) := 0;
   L_proposed_qty                 NUMBER(12,4) := 0;
   L_rnd_qty                      NUMBER(12,4) := 0;
   L_overage_qty                  NUMBER(12,4) := 0;
   L_total_qty                    NUMBER(12,4) := 0;
   L_non_leader_total             NUMBER(12,4) := 0;
   L_scalable                     BOOLEAN := TRUE;
   L_rounded                      BOOLEAN := FALSE;
   L_count_positive               NUMBER := 0;
   L_leader_index                 NUMBER := 1;
   L_cmplx_rnd_lvl                BOOLEAN := FALSE;

   cursor C_CHECK_SEASON(I_season_id         REPL_RESULTS.SEASON_ID%TYPE,
                         I_phase_id          REPL_RESULTS.PHASE_ID%TYPE,
                         I_date              REPL_RESULTS.REPL_DATE%TYPE) is 
      select 'x'
        from phases
       where season_id = I_season_id
         and phase_id = I_phase_id
         and start_date <= I_date
         and end_date >= I_date;
   
BEGIN
   /*
    * Function performs case or pallet scaling based on rounding level
    * 1) case/pallet scaling starts from the rounded value of solution day;
    * 2) do not scale if store is closed on the solution date;
    * 3) for season dynamic replenishment, do not scale if solution date falls
    *    outside of the season range;
    * 4) if round_lvl = "C"ase, scale one case at a time;
    * 5) if round_lvl = "P"allet, scale one pallet at a time;
    * 6) if round_lvl = "B"oth, scale one case a a time and evaluate the proposed
    *    quantity to pallet;
    * 7) for each proposed item/loc scaling qty, evaluate non-scaling cnstrs;
    * 8) unlike day-of-supply scaling, solution is evaluated after scaling each
    *    item/loc
    */

   /* find out solution date to check if store is closed and if it is in season; */
   /* day-scaling direction should be used to decide solution date */
   if LP_scale_loc_tbl(I_loc_index).direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN then
      L_sol_day := I_sol_day * (-1);
   else
      L_sol_day := I_sol_day;
   end if;
   if GET_ORDER_DATE(O_error_message,
                     LP_scaling_tbl(I_cur_index).order_upto_point_date,
                     L_sol_day,
                     L_order_date) = FALSE then
      return FALSE;           
   end if;
   /* do not scale if store is closed on solution date */
   if (LP_scaling_tbl(I_cur_index).store_close_date is NOT NULL) then
      if LP_scaling_tbl(I_cur_index).store_close_date < L_order_date then
         LP_scale_item_loc_case_tbl(I_cur_index).message(I_sol_index) := SQL_LIB.CREATE_MSG('SUPCSTR_NS_STR_CLOSD',NULL,NULL,NULL);
         return TRUE;
      end if;
   end if;
   
   /* for seasonal dynamic replenishment order ("T", "D" w/ NOT NULL season/phaseid) */
   /* do not adjust quantity if solution date falls out of the season range */
   if (LP_scaling_tbl(I_cur_index).repl_method in ('T','D') and
       LP_scaling_tbl(I_cur_index).season_id is NOT NULL and
       LP_scaling_tbl(I_cur_index).phase_id is NOT NULL) then
      L_in_season := 'y';
      SQL_LIB.SET_MARK('OPEN','C_CHECK_SEASON','PHASES','phase_id:' || LP_scaling_tbl(I_cur_index).phase_id); 
      open C_CHECK_SEASON(LP_scaling_tbl(I_cur_index).season_id,
                          LP_scaling_tbl(I_cur_index).phase_id,
                          L_order_date);
      SQL_LIB.SET_MARK('FETCH','C_CHECK_SEASON','PHASES','phase_id:' || LP_scaling_tbl(I_cur_index).phase_id); 
      fetch C_CHECK_SEASON into L_in_season;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_SEASON','PHASES','phase_id:' || LP_scaling_tbl(I_cur_index).phase_id); 
      close C_CHECK_SEASON;
      if L_in_season <> 'x' then
         LP_scale_item_loc_case_tbl(I_cur_index).message(I_sol_index) := SQL_LIB.CREATE_MSG('SUPCSTR_NS_OUT_OF_SSN',NULL,NULL,NULL);
         return TRUE;
      end if;
   end if;
   
   if LP_scaling_tbl(I_cur_index).round_lvl = 'P' then
      L_adjust_qty := LP_scaling_tbl(I_cur_index).pallet_size;
   elsif LP_scaling_tbl(I_cur_index).round_lvl in ('L','LP') then
      L_adjust_qty := LP_scaling_tbl(I_cur_index).tier;
   else /* case, case/layer, or case/layer/pallet rounding */
      L_adjust_qty := LP_scaling_tbl(I_cur_index).supp_pack_size;
   end if;
   
   if (LP_scale_loc_case_tbl(I_loc_index).direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN) then
      L_adjust_qty := L_adjust_qty * (-1);
   end if;
   
   for a in 1..LP_scaling_tbl.count LOOP
      if (LP_scale_item_loc_case_tbl(I_cur_index).item <> LP_scale_item_loc_case_tbl(a).item or
          LP_scale_item_loc_case_tbl(I_cur_index).rounding_seq <> LP_scale_item_loc_case_tbl(a).rounding_seq) then
         continue;
      end if;
      /* if adding a case or pallet will make the qty go from negative to positive, */
      /* make the proposed qty a whole case or pallet. */
      /* Example: round level = case, case size = 10, */
      /* if scaled qty = -15, proposed qty = -15 + 10 = -5 */
      /* if scaled qty = -10, proposed qty = -10 + 10 = 0 */
      /* if scaled qty = -5, proposed qty = -5 + 10 = 5 => 10 */
      if(LP_scale_item_loc_case_tbl(a).qty_scaled_suom(I_sol_index - 1) < 0 and
         (LP_scale_item_loc_case_tbl(a).qty_scaled_suom(1) + L_adjust_qty) > 0) then
         L_total_qty := L_total_qty + L_adjust_qty;
      else
         L_total_qty := L_total_qty + LP_scale_item_loc_case_tbl(a).qty_scaled_suom(I_sol_index -1);
         L_count_positive := L_count_positive + 1;
      end if;
      if LP_scale_item_loc_case_tbl(I_cur_index).rounding_seq = LP_scale_item_loc_case_tbl(a).location then
         L_leader_index := a;
         continue;
      end if;
      L_non_leader_total := L_non_leader_total + LP_scale_item_loc_case_tbl(a).qty_scaled_suom(1);
   END LOOP;
   if L_count_positive > 0 then
      L_proposed_qty := L_total_qty + L_adjust_qty;
   else
      L_proposed_qty := L_total_qty;
   end if;
   if LP_scaling_tbl(I_cur_index).round_lvl = 'LP' then
      L_cmplx_rnd_lvl := TRUE;
      if EVAL_LAYER_PALLET(O_error_message,
                           L_proposed_qty,
                           LP_scaling_tbl(I_cur_index).pallet_size,
                           LP_scaling_tbl(I_cur_index).round_to_pallet_pct,
                           L_rnd_qty,
                           L_rounded) = FALSE then
         return FALSE;
      end if;
   elsif LP_scaling_tbl(I_cur_index).round_lvl = 'CL' then
      L_cmplx_rnd_lvl := TRUE;
      if EVAL_LAYER_PALLET(O_error_message,
                           L_proposed_qty,
                           LP_scaling_tbl(I_cur_index).tier,
                           LP_scaling_tbl(I_cur_index).round_to_layer_pct,
                           L_rnd_qty,
                           L_rounded) = FALSE then
         return FALSE;
      end if;
   elsif LP_scaling_tbl(I_cur_index).round_lvl = 'CLP' then
      L_cmplx_rnd_lvl := TRUE;
      if EVAL_LAYER_PALLET(O_error_message,
                           L_proposed_qty,
                           LP_scaling_tbl(I_cur_index).pallet_size,
                           LP_scaling_tbl(I_cur_index).round_to_pallet_pct,
                           L_rnd_qty,
                           L_rounded) = FALSE then
         return FALSE;
      end if;
      if L_rounded = FALSE then /* proposed qty was not able to be rounded up to nearest pallet. */
         if EVAL_LAYER_PALLET(O_error_message,
                              L_proposed_qty,
                              LP_scaling_tbl(I_cur_index).tier,
                              LP_scaling_tbl(I_cur_index).round_to_layer_pct,
                              L_rnd_qty,
                              L_rounded) = FALSE then
            return FALSE;
         end if;
      end if;
   else
      L_rnd_qty := L_proposed_qty;
   end if;
   
   L_overage_qty := L_rnd_qty - L_total_qty;
   
   if (L_adjust_qty <> 0) then
      if EVAL_NON_SCALE_CNSTR(O_error_message,
                              LP_scale_loc_case_tbl(I_loc_index).direction,
                              SUP_CONSTRAINTS_SQL.LP_CASE,
                              LP_scale_item_loc_case_tbl,
                              I_cur_index,
                              I_sol_index,
                              L_rnd_qty,
                              L_scalable) = FALSE then
         return FALSE;
      end if;
   end if;
   if (LP_scale_loc_case_tbl(I_loc_index).direction = SUP_CONSTRAINTS_SQL.LP_RANK_DIR_DOWN and 
       L_overage_qty > 0 ) then
      L_overage_qty := L_overage_qty * (-1);
   end if;
   if L_scalable = TRUE then
      if L_rnd_qty = 0 then
         LP_scale_item_loc_case_tbl(L_leader_index).qty_scaled_suom(I_sol_index) := 0;
         LP_scale_item_loc_case_tbl(L_leader_index).qty_scaled_suom_rnd(I_sol_index) := 0;
      elsif L_overage_qty = 0 then
         LP_scale_item_loc_case_tbl(L_leader_index).qty_scaled_suom(I_sol_index) := L_proposed_qty - L_non_leader_total;
         LP_scale_item_loc_case_tbl(L_leader_index).qty_scaled_suom_rnd(I_sol_index) := L_rnd_qty - L_non_leader_total;
      else
         LP_scale_item_loc_case_tbl(L_leader_index).qty_scaled_suom(I_sol_index) := L_proposed_qty - L_non_leader_total;
         LP_scale_item_loc_case_tbl(L_leader_index).qty_scaled_suom_rnd(I_sol_index) := L_rnd_qty - L_non_leader_total;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GEN_ONE_CASE_SUPPLY;
-------------------------------------------------------------------------------
FUNCTION WRITE_SCALED_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_loc_index         IN      NUMBER,
                          I_sol_case          IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.WRITE_SCALED_QTY';
   
BEGIN
   /* function to copy the final scaled quantity to the driving cursor */
   for i in 1..LP_scaling_tbl.count LOOP
      if LP_ord_inv_mgmt_rec.scale_cnstr_lvl = 'L' then
         if (LP_scaling_tbl(i).source_wh = -1 and
             LP_scale_loc_case_tbl(I_loc_index).location <> LP_scaling_tbl(i).location) or
            (LP_scaling_tbl(i).source_wh <> -1 and
             LP_scale_loc_case_tbl(I_loc_index).location <> LP_scaling_tbl(i).source_wh) then
            continue;
         end if;
      end if;
      if LP_scale_loc_case_tbl(I_loc_index).scaled_ind <> 0 then
         LP_scaling_tbl(i).scaled_ind := 'Y';
         LP_scaling_tbl(i).qty_scaled_sol := LP_scale_item_loc_case_tbl(i).qty_scaled_suom_rnd(I_sol_case);
      else
         LP_scaling_tbl(i).scaled_ind := 'N';
         LP_scaling_tbl(i).qty_scaled_sol := LP_scale_item_loc_case_tbl(i).qty_scaled_suom_rnd(1);
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END WRITE_SCALED_QTY;
-------------------------------------------------------------------------------
FUNCTION CALC_FINAL_SOURCE_WH_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CALC_FINAL_SOURCE_WH_QTY';
   L_xdock_to_loc                 SUP_CONSTRAINTS_SQL.XDOCK_TO_LOC_TBL;
   L_xdock_cur_index              NUMBER;
   L_total_qty                    NUMBER(12,4);
BEGIN
   /*
    * For each crossdocked location,
    * 1) sum up positive scaled qty for x-docked locations;
    * 2) if an entry already exists in the driving cursor for the source_wh
    *    (driv_cursor_index != -1), add total scaled qty for x-docked locations
    *    plus the residual qty to the source_wh scaled qty;
    * 3) if no entry exists in the driving cursor for the source_wh
    *    (driv_cursor_index == -1) and any of the xdocked location scaled to
    *    to a positive quantity, add an entry to the end of the driving cursor
    *    tbl so that an ordloc record can be written during write-out process.
    */
    if LP_xdock_from_loc_tbl.count > 0 and LP_xdock_from_loc_tbl is NOT NULL then
      for i in 1..LP_xdock_from_loc_tbl.COUNT LOOP
         L_total_qty := 0;
         L_xdock_to_loc := LP_xdock_from_loc_tbl(i).xdock_to_loc;
         for j in 1..L_xdock_to_loc.COUNT LOOP
            L_xdock_cur_index := L_xdock_to_loc(j).driv_cursor_index;
            if (LP_scaling_tbl(L_xdock_cur_index).qty_scaled_sol > 0 ) then
               L_total_qty := L_total_qty + LP_scaling_tbl(L_xdock_cur_index).qty_scaled_sol;
            end if;
         END LOOP;
         /* it was decided that the addition of the residual qty should not occur after pre-case scale rounding */
         if LP_xdock_from_loc_tbl(i).driv_cursor_index <> -1 then
            LP_scaling_tbl(LP_xdock_from_loc_tbl(i).driv_cursor_index).qty_scaled_sol := LP_scaling_tbl(LP_xdock_from_loc_tbl(i).driv_cursor_index).qty_scaled_sol + L_total_qty;
         else
            if (L_total_qty > 0) then
               /* add entry in driving cursor for source_wh */
               /* use ll_xdock_cur_index for initializing source_wh driv cursor data */
               if ADD_SOURCE_WH(O_error_message,
                                LP_xdock_from_loc_tbl(i).item,
                                LP_xdock_from_loc_tbl(i).source_wh,
                                LP_scaling_tbl.count + 1,
                                L_xdock_cur_index,
                                L_total_qty) = FALSE then
                  return FALSE;
               end if;
               LP_xdock_from_loc_tbl(i).driv_cursor_index := LP_scaling_tbl.count;
            end if;
         end if;
      END LOOP;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CALC_FINAL_SOURCE_WH_QTY;
-------------------------------------------------------------------------------
FUNCTION ADD_SOURCE_WH(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item              IN      REPL_ITEM_LOC.ITEM%TYPE,
                       I_source_wh         IN      REPL_ITEM_LOC.SOURCE_WH%TYPE,
                       I_wh_cur_index      IN      NUMBER,
                       I_xdock_cur_index   IN      NUMBER,
                       I_total_qty         IN      NUMBER) 
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.ADD_SOURCE_WH';
   
BEGIN
   /* set driving cursor values for source_wh used in order writeout */
   LP_scaling_tbl(I_wh_cur_index).ordloc_exist := 'N';
   LP_scaling_tbl(I_wh_cur_index).item := I_item;
   LP_scaling_tbl(I_wh_cur_index).source_wh := -1;
   LP_scaling_tbl(I_wh_cur_index).loc_type := 'W';
   LP_scaling_tbl(I_wh_cur_index).location := I_source_wh;
   LP_scaling_tbl(I_wh_cur_index).alloc_no := -1;
   
   LP_scaling_tbl(I_wh_cur_index).pack_ind := LP_scaling_tbl(I_xdock_cur_index).pack_ind;
   LP_scaling_tbl(I_wh_cur_index).sup_unit_cost := LP_scaling_tbl(I_xdock_cur_index).sup_unit_cost;
   LP_scaling_tbl(I_wh_cur_index).ord_unit_cost := LP_scaling_tbl(I_xdock_cur_index).ord_unit_cost;
   LP_scaling_tbl(I_wh_cur_index).sup_currency_code := LP_scaling_tbl(I_xdock_cur_index).sup_currency_code;
   LP_scaling_tbl(I_wh_cur_index).ord_currency_code := LP_scaling_tbl(I_xdock_cur_index).ord_currency_code;
   LP_scaling_tbl(I_wh_cur_index).exchange_rate := LP_scaling_tbl(I_xdock_cur_index).exchange_rate;
   LP_scaling_tbl(I_wh_cur_index).supp_pack_size := LP_scaling_tbl(I_xdock_cur_index).supp_pack_size;
   LP_scaling_tbl(I_wh_cur_index).pallet_size := LP_scaling_tbl(I_xdock_cur_index).pallet_size;
   LP_scaling_tbl(I_wh_cur_index).inner_pack_size := LP_scaling_tbl(I_xdock_cur_index).inner_pack_size;
   
   /* won't have an entry in repl_results */
   LP_scaling_tbl(I_wh_cur_index).repl_method := NULL;
   LP_scaling_tbl(I_wh_cur_index).stock_cat := NULL;
   
   LP_scaling_tbl(I_wh_cur_index).supplier := LP_scaling_tbl(I_xdock_cur_index).supplier;
   LP_scaling_tbl(I_wh_cur_index).origin_country_id := LP_scaling_tbl(I_xdock_cur_index).origin_country_id;
   LP_scaling_tbl(I_wh_cur_index).import_order_ind := LP_scaling_tbl(I_xdock_cur_index).import_order_ind;
   LP_scaling_tbl(I_wh_cur_index).import_country_id := LP_scaling_tbl(I_xdock_cur_index).import_country_id;
   LP_scaling_tbl(I_wh_cur_index).earliest_ship_date := LP_scaling_tbl(I_xdock_cur_index).earliest_ship_date;
   LP_scaling_tbl(I_wh_cur_index).latest_ship_date := LP_scaling_tbl(I_xdock_cur_index).latest_ship_date;
  
   LP_scaling_tbl(I_wh_cur_index).qty_ordered    := 0;
   LP_scaling_tbl(I_wh_cur_index).qty_prescaled  := 0;
   LP_scaling_tbl(I_wh_cur_index).qty_scaled_sol := I_total_qty;
   LP_scaling_tbl(I_wh_cur_index).scaled_ind     := 'Y';
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_SOURCE_WH;
-------------------------------------------------------------------------------
FUNCTION OUTPUT_SCALING(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.OUTPUT_SCALING';
   
   L_prev_item                    ITEM_MASTER.ITEM%TYPE;
   L_ordsku_exist                 BOOLEAN;
   L_exists                       VARCHAR2(1);
   L_repl_results_ind             VARCHAR2(1);
   
   cursor C_ORDSKU_EXIST(I_order_no   ORDSKU.ORDER_NO%TYPE,
                         I_item       ORDSKU.ITEM%TYPE) is
   select 'x'
     from ordsku
    where order_no = I_order_no
      and item = I_item;
   
   
BEGIN
   L_prev_item := -1;
   L_ordsku_exist := FALSE;
   for i in 1..LP_scaling_tbl.count LOOP
      if LP_scaling_tbl(i).scaled_ind = 'N' then
         continue;
      end if;
      if LP_scaling_tbl(i).physical_wh <> -1 then
         for j in 1..LP_scaling_tbl.count LOOP
            if (LP_ord_inv_mgmt_rec.scale_cnstr_lvl <> 'O' and
                LP_scaling_tbl(i).physical_wh <> LP_scaling_tbl(j).physical_wh or
                LP_scaling_tbl(i).location = LP_scaling_tbl(j).location) then
               continue;
            end if;
            LP_scaling_tbl(j).scaled_ind := 'Y';
         END LOOP; -- end for j
      end if; -- vwh
      /* xdock alloc to location will be processed along with the source wh */
      if LP_scaling_tbl(i).source_wh <> -1 then
         continue;
      end if;
      
      if LP_scaling_tbl(i).repl_method is NOT NULL then
         L_repl_results_ind := 'Y';
      else
         L_repl_results_ind := 'N';
      end if;
      if L_prev_item <> LP_scaling_tbl(i).item then
         L_ordsku_exist := FALSE;
         L_prev_item := LP_scaling_tbl(i).item;
      end if;
      if LP_scaling_tbl(i).qty_scaled_sol > 0 then
         if LP_scaling_tbl(i).ordloc_exist = 'N' then
            if L_ordsku_exist = FALSE then
               /* create ordsku if one does not exist */
               L_exists := 'y';
               SQL_LIB.SET_MARK('OPEN','C_ORDSKU_EXIST','ORDSKU','order_no:' || LP_order_no || ' item:' || LP_scaling_tbl(i).item); 
               open C_ORDSKU_EXIST(LP_order_no,LP_scaling_tbl(i).item);
               SQL_LIB.SET_MARK('FETCH','C_ORDSKU_EXIST','ORDSKU','order_no:' || LP_order_no || ' item:' || LP_scaling_tbl(i).item); 
               fetch C_ORDSKU_EXIST into L_exists;
               SQL_LIB.SET_MARK('CLOSE','C_ORDSKU_EXIST','ORDSKU','order_no:' || LP_order_no || ' item:' || LP_scaling_tbl(i).item); 
               close C_ORDSKU_EXIST;
               if L_exists = 'x' then
                  L_ordsku_exist := TRUE;
               else
                  L_ordsku_exist := FALSE;
               end if;
               
               if L_ordsku_exist = FALSE then
                  if CREATE_ORDSKU(O_error_message,
                                   LP_order_no,
                                   i) = FALSE then
                     return FALSE;
                  end if;
                  L_ordsku_exist := TRUE;
               end if;
            end if;
            if CREATE_ORDLOC(O_error_message,
                             LP_order_no,
                             i) = FALSE then
               return FALSE;
            end if;
         else
            /* no need to create ordsku - since ordloc exists, ordsku must exist. */
            L_ordsku_exist := TRUE;
            /* update ordloc */
            if LP_scaling_tbl(i).qty_scaled_sol <> LP_scaling_tbl(i).qty_ordered then
               if UPDATE_ORDLOC(O_error_message,
                                LP_order_no,
                                LP_scaling_tbl(i).item,
                                LP_scaling_tbl(i).location,
                                LP_scaling_tbl(i).loc_type,
                                LP_scaling_tbl(i).qty_scaled_sol) = FALSE then
                  return FALSE;
               end if;
               
               /* add item to elc update tbl for later processing */
               if LP_elc_ind = 'Y' then
                  LP_elc_update_tbl(LP_elc_update_tbl.count + 1) := LP_scaling_tbl(i).item;
               end if;
            end if;
         end if;-- ordloc exist
      elsif LP_scaling_tbl(i).ordloc_exist = 'Y' then
         /* since ordloc exists, ordsku must exist */
         L_ordsku_exist := TRUE;
         /* delete ordloc and its child records */
         if DELETE_ORDLOC(O_error_message,
                          LP_order_no,
                          LP_scaling_tbl(i).item,
                          LP_scaling_tbl(i).location,
                          LP_scaling_tbl(i).loc_type) = FALSE then
            return FALSE;
         end if;
         
         /* add item to elc update tbl for later processing */
         if LP_elc_ind = 'Y' then
            LP_elc_update_tbl(LP_elc_update_tbl.count + 1) := LP_scaling_tbl(i).item;
         end if;
         /* add item to item delete tbl for later processing */
         LP_item_delete_tbl(LP_item_delete_tbl.count + 1) := LP_scaling_tbl(i).item;
      end if;-- LP_scaling_tbl(i).qty_scaled_sol > 0
      /* process order writeout for crossdock locations associated with the wh*/
      if LP_scaling_tbl(i).loc_type = 'W' then
         if PROCESS_CROSSDOCK(O_error_message,
                              i) = FALSE then
            return FALSE;
         end if;
      end if;
      
      if UPDATE_ELC_ALL(O_error_message) = FALSE then
         return FALSE;
      end if;
      if DELETE_ORDSKU_ALL(O_error_message) = FALSE then
         return FALSE;
      end if;
      if DELETE_ALLOC_HEADER_ALL(O_error_message) = FALSE then
         return FALSE;
      end if;

   END LOOP; -- i in total records
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END OUTPUT_SCALING;
-------------------------------------------------------------------------------
FUNCTION CREATE_ORDSKU(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                       I_cur_index         IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CREATE_ORDSKU';
   
   L_refitem                      ITEM_MASTER.ITEM%TYPE := NULL;            
   L_unit_retail                  ITEM_LOC.UNIT_RETAIL%TYPE;
   L_av_cost                      ITEM_LOC_SOH.AV_COST%TYPE := 0;
   L_unit_cost                    ITEM_LOC_SOH.UNIT_COST%TYPE := 0;
   L_selling_unit_retail          ITEM_LOC.SELLING_UNIT_RETAIL%TYPE := 0;
   L_selling_uom                  ITEM_LOC.SELLING_UOM%TYPE;
   
   cursor C_GET_REFITEM_INFO(I_item        ITEM_MASTER.ITEM%TYPE,
                             I_supplier    ITEM_SUPPLIER.SUPPLIER%TYPE) is
     select im.item
       from item_master im,
            item_supplier ip
      where im.item_level > im.tran_level
        and im.primary_ref_item_ind = 'y'
        and im.item_parent = I_item
        and im.item = ip.item
        and ip.supplier = I_supplier;
   
   
BEGIN
   /*
    * Function to create an ordsku record:
    * 1) get ref_item info for the item/supplier
    * 2) insert into ordsku table
    * 3) get item and origin_country_id level default documents to order/item
    * 4) get HTS info for order/item
    */
   /* get unit_retail for inserting into ordloc */
   if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                               LP_scaling_tbl(I_cur_index).item,
                                               LP_scaling_tbl(I_cur_index).location,
                                               LP_scaling_tbl(I_cur_index).loc_type,
                                               L_av_cost,
                                               L_unit_cost,
                                               L_unit_retail,
                                               L_selling_unit_retail,
                                               L_selling_uom) = FALSE then
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('OPEN','C_GET_REFITEM_INFO','ITEM_MASTER','item:' || LP_scaling_tbl(I_cur_index).item); 
   open C_GET_REFITEM_INFO(LP_scaling_tbl(I_cur_index).item,
                           LP_scaling_tbl(I_cur_index).supplier);
   SQL_LIB.SET_MARK('FETCH','C_GET_REFITEM_INFO','ITEM_MASTER','item:' || LP_scaling_tbl(I_cur_index).item); 
   fetch C_GET_REFITEM_INFO into L_refitem;
   SQL_LIB.SET_MARK('CLOSE','C_GET_REFITEM_INFO','ITEM_MASTER','item:' || LP_scaling_tbl(I_cur_index).item); 
   close C_GET_REFITEM_INFO;
   
   /* insert into ordsku table */
   /* Note: earliest_ship_date and latest_ship_date will never be NULL on */
   /* ordhead even though they show up as NULL fields on the data dictionary. */
   if(LP_scaling_tbl(I_cur_index).earliest_ship_date is NULL or
      LP_scaling_tbl(I_cur_index).latest_ship_date is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_SHIP_DATE_DEF',TO_CHAR(I_order_no),NULL,NULL);
      return FALSE;
   end if;
   insert into ordsku (order_no,
                       item,
                       ref_item,
                       origin_country_id,
                       earliest_ship_date,
                       latest_ship_date,
                       supp_pack_size,
                       non_scale_ind)
               values (I_order_no,
                       LP_scaling_tbl(I_cur_index).item,
                       L_refitem,
                       LP_scaling_tbl(I_cur_index).origin_country_id,
                       LP_scaling_tbl(I_cur_index).earliest_ship_date,
                       LP_scaling_tbl(I_cur_index).latest_ship_date,
                       LP_scaling_tbl(I_cur_index).supp_pack_size,
                       'N');
   /* get item and origin_country_id level default documents to order/item */
   if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                 'IT',
                                 'POIT',
                                  LP_scaling_tbl(I_cur_index).item,
                                  I_order_no,
                                  NULL,
                                  LP_scaling_tbl(I_cur_index).item) = FALSE then
      return FALSE;
   end if;
   if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                 'CTRY',
                                 'POIT',
                                  LP_scaling_tbl(I_cur_index).origin_country_id,
                                  I_order_no,
                                  NULL,
                                  LP_scaling_tbl(I_cur_index).item) = FALSE then
      return FALSE;
   end if;
   /* get default HTS info for order/item */
   if LP_elc_ind = 'Y' and
      LP_scaling_tbl(I_cur_index).import_order_ind ='Y' then
      if LP_scaling_tbl(I_cur_index).import_country_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_IMP_CTRY_DEF',TO_CHAR(LP_order_no),NULL,NULL);
         return FALSE;
      end if;
      if ORDER_HTS_SQL.DEFAULT_HTS(O_error_message,
                                   I_order_no,
                                   LP_scaling_tbl(I_cur_index).item,
                                   NULL) = FALSE then
         return FALSE;
      end if;
      if GET_HTS_CHAPTER_DOC(O_error_message,
                             I_order_no,
                             LP_scaling_tbl(I_cur_index).item,
                             LP_scaling_tbl(I_cur_index).import_country_id) = FALSE then
         return FALSE;
      end if;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_ORDSKU;
-------------------------------------------------------------------------------
FUNCTION GET_HTS_CHAPTER_DOC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                             I_item              IN      ITEM_MASTER.ITEM%TYPE,
                             I_import_country_id IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.GET_HTS_CHAPTER_DOC';
   L_chapter                      HTS.CHAPTER%TYPE;

   cursor C_CHAPTER is 
     select distinct h.chapter
       from hts h,
            ordsku_hts osh
      where h.hts = osh.hts
        and h.import_country_id = osh.import_country_id
        and h.effect_from = osh.effect_from
        and h.effect_to = osh.effect_to
        and osh.order_no = I_order_no
        and osh.item = I_item
        and osh.import_country_id = I_import_country_id
        and osh.pack_item is null;
   
BEGIN
   for rec in C_CHAPTER LOOP
      L_chapter := rec.chapter;
      if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                    'HTSC',
                                    'POIT',
                                    L_chapter,
                                    I_order_no,
                                    NULL,
                                    I_item) = FALSE then
         return FALSE;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_HTS_CHAPTER_DOC;
-------------------------------------------------------------------------------
FUNCTION CREATE_ORDLOC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no          IN      ORDLOC.ORDER_NO%TYPE,
                       I_cur_index         IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CREATE_ORDLOC';
   L_unit_retail                  ITEM_LOC.UNIT_RETAIL%TYPE;
   L_av_cost                      ITEM_LOC_SOH.AV_COST%TYPE := 0;
   L_unit_cost                    ITEM_LOC_SOH.UNIT_COST%TYPE := 0;
   L_selling_unit_retail          ITEM_LOC.SELLING_UNIT_RETAIL%TYPE := 0;
   L_selling_uom                  ITEM_LOC.SELLING_UOM%TYPE;
   
BEGIN
   /* get unit_retail for inserting into ordloc */
   if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                               LP_scaling_tbl(I_cur_index).item,
                                               LP_scaling_tbl(I_cur_index).location,
                                               LP_scaling_tbl(I_cur_index).loc_type,
                                               L_av_cost,
                                               L_unit_cost,
                                               L_unit_retail,
                                               L_selling_unit_retail,
                                               L_selling_uom) = FALSE then
      return FALSE;
   end if;
   insert into ordloc(order_no,
                      item,
                      location,
                      loc_type,
                      unit_retail,
                      qty_ordered,
                      qty_prescaled,
                      last_rounded_qty,
                      unit_cost,
                      unit_cost_init,
                      cost_source,
                      non_scale_ind)
               values(I_order_no,
                      LP_scaling_tbl(I_cur_index).item,
                      LP_scaling_tbl(I_cur_index).location,
                      LP_scaling_tbl(I_cur_index).loc_type,
                      L_unit_retail,
                      LP_scaling_tbl(I_cur_index).qty_scaled_sol,
                      LP_scaling_tbl(I_cur_index).qty_prescaled,
                      LP_scaling_tbl(I_cur_index).qty_scaled_sol,
                      LP_scaling_tbl(I_cur_index).ord_unit_cost,
                      LP_scaling_tbl(I_cur_index).ord_unit_cost,
                      'NORM',
                      'N');
   
   /* calculate landed costs associated with order/item/loc if elc_ind is 'Y' */
   if LP_elc_ind = 'Y' then
      if ORDER_EXPENSE_SQL.INSERT_COST_COMP(O_error_message,
                                            I_order_no,
                                            LP_scaling_tbl(I_cur_index).item,
                                            NULL,
                                            LP_scaling_tbl(I_cur_index).location,
                                            LP_scaling_tbl(I_cur_index).loc_type) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_ORDLOC;
-------------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no          IN      ORDLOC.ORDER_NO%TYPE,
                       I_item              IN      ORDLOC.ITEM%TYPE,
                       I_location          IN      ORDLOC.LOCATION%TYPE,
                       I_loc_type          IN      ORDLOC.LOC_TYPE%TYPE,
                       I_update_qty        IN      ORDLOC.QTY_ORDERED%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.UPDATE_ORDLOC';
   RECORD_LOCKED                  EXCEPTION;
   PRAGMA                         EXCEPTION_INIT(Record_Locked, -54);
   L_table                        VARCHAR2(30)  := 'ORDLOC';
   L_key1                         VARCHAR2(100) := I_order_no;
   L_key2                         VARCHAR2(100) := I_item;
   L_rowid                        ROWID         := NULL;
   
   cursor C_ORDLOC_LOCK is 
      select rowid
        from ordloc
       where order_no =  I_order_no
         and item = I_item
         and location = I_location
         and loc_type = I_loc_type
         for update of qty_ordered,original_repl_qty,last_rounded_qty nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ORDLOC_LOCK','ORDLOC','Order_no:'||to_char(I_order_no)||
                    ' Item:'||I_item);
   open C_ORDLOC_LOCK;
   fetch C_ORDLOC_LOCK into L_rowid;
   close C_ORDLOC_LOCK;

   SQL_LIB.SET_MARK('UPDATE',NULL,'ORDLOC','rowid:'||L_rowid);
            update ordloc
               set qty_ordered = I_update_qty,
                   original_repl_qty = DECODE(NVL(original_repl_qty, -1),
                                              -1, NULL,
                                                  I_update_qty),
                   last_rounded_qty = I_update_qty
             where rowid = L_rowid;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_key1,
                                             L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_ORDLOC;
-------------------------------------------------------------------------------
FUNCTION DELETE_ORDLOC(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                       I_item              IN      ORDLOC.ITEM%TYPE,
                       I_location          IN      ORDLOC.LOCATION%TYPE,
                       I_loc_type          IN      ORDLOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.DELETE_ORDLOC';
   RECORD_LOCKED                  EXCEPTION;
   PRAGMA                         EXCEPTION_INIT(Record_Locked, -54);
   
BEGIN
   if ORDER_SETUP_SQL.DELETE_ORDER(O_error_message,
                                   I_order_no,
                                   I_item,
                                   I_location,
                                   I_loc_type,
                                   'ordloc') = FALSE then
      return FALSE;
   end if;
   -- delete from ordloc
   SQL_LIB.SET_MARK('DELETE',NULL,'ORDLOC','ORDER_NO: ' || TO_CHAR(I_order_no) || ' ITEM: ' || I_item);
   delete from ordloc
    where order_no = I_order_no
      and item = I_item
      and location = I_location
      and loc_type = I_loc_type;
   
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'ORDLOC',
                                             TO_CHAR(I_order_no),
                                             I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ORDLOC;
-------------------------------------------------------------------------------
FUNCTION PROCESS_CROSSDOCK(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cur_index         IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.PROCESS_CROSSDOCK';
   L_alloc_no                     ALLOC_HEADER.ALLOC_NO%TYPE;
   L_xdock_to_loc                 SUP_CONSTRAINTS_SQL.XDOCK_TO_LOC_TBL;
   
BEGIN
   /*
    * Function traverses the crossdock table to see if the wh
    * is a source_wh for the item in any crossdock situations.
    * If so, process write-out for all alloc_to locations sourced from the wh.
    * These alloc_detail records, if exist, should share the same alloc_no.
    */
   if LP_xdock_from_loc_tbl.count > 0 and LP_xdock_from_loc_tbl is NOT NULL then
      for i in LP_xdock_from_loc_tbl.FIRST..LP_xdock_from_loc_tbl.LAST LOOP
         if LP_xdock_from_loc_tbl(i).driv_cursor_index <> I_cur_index then
            continue;
         else
            /* wh is a source_wh for crossdock */
            L_xdock_to_loc := LP_xdock_from_loc_tbl(i).xdock_to_loc;
            L_alloc_no := -1;
            for j in 1..L_xdock_to_loc.COUNT LOOP
               if OUTPUT_ONE_XDOCK(O_error_message,
                                   L_xdock_to_loc(j).driv_cursor_index,
                                   L_alloc_no) = FALSE then
                  return FALSE;
               end if;
            END LOOP;
            EXIT;
         end if;
      END LOOP;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CROSSDOCK;
-------------------------------------------------------------------------------
FUNCTION OUTPUT_ONE_XDOCK(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cur_index         IN      NUMBER,
                          O_alloc_no          IN OUT  ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.OUTPUT_ONE_XDOCK';
   L_repl_results_ind             VARCHAR2(1);
   
   
   cursor C_ALLOC_HEADER_EXIST is
     select alloc_no
       from alloc_header
      where order_no = LP_order_no
        and wh = LP_scaling_tbl(I_cur_index).source_wh
        and item = LP_scaling_tbl(I_cur_index).item
        and status = 'A';
   
BEGIN
   if LP_scaling_tbl(I_cur_index).repl_method is NOT NULL then
      L_repl_results_ind := 'Y';
   else
      L_repl_results_ind := 'N';
   end if;
   if (LP_scaling_tbl(I_cur_index).qty_scaled_sol > 0) then
      /* for crossdock, s_ordloc_exist flag indicates if alloc_detail exists */
      if LP_scaling_tbl(I_cur_index).ordloc_exist = 'N' then
         /* create alloc_header if one does not exist */
         if O_alloc_no = -1 then
            SQL_LIB.SET_MARK('OPEN','C_ALLOC_HEADER_EXIST','ALLOC_HEADER','item:' || LP_scaling_tbl(I_cur_index).item || ' source_wh:' || LP_scaling_tbl(I_cur_index).source_wh); 
            open C_ALLOC_HEADER_EXIST;
            SQL_LIB.SET_MARK('FETCH','C_ALLOC_HEADER_EXIST','ALLOC_HEADER','item:' || LP_scaling_tbl(I_cur_index).item || ' source_wh:' || LP_scaling_tbl(I_cur_index).source_wh); 
            fetch C_ALLOC_HEADER_EXIST into O_alloc_no;
            SQL_LIB.SET_MARK('CLOSE','C_ALLOC_HEADER_EXIST','ALLOC_HEADER','item:' || LP_scaling_tbl(I_cur_index).item || ' source_wh:' || LP_scaling_tbl(I_cur_index).source_wh); 
            close C_ALLOC_HEADER_EXIST;
         end if;
         if NVL(O_alloc_no,-1) = -1 then
            if CREATE_ALLOC_HEADER(O_error_message,
                                   LP_order_no,
                                   LP_scaling_tbl(I_cur_index).item,
                                   LP_scaling_tbl(I_cur_index).source_wh,
                                   O_alloc_no) = FALSE then
               return FALSE;
            end if;
         end if;
         
         /* create alloc_detail */
         if CREATE_ALLOC_DETAIL(O_error_message,
                                O_alloc_no,
                                LP_scaling_tbl(I_cur_index).source_wh,
                                LP_scaling_tbl(I_cur_index).location,
                                LP_scaling_tbl(I_cur_index).loc_type,
                                LP_scaling_tbl(I_cur_index).item,
                                LP_scaling_tbl(I_cur_index).qty_prescaled,
                                LP_scaling_tbl(I_cur_index).qty_scaled_sol) = FALSE then
            return FALSE;
         end if;
         
         /* for updating repl_results */
         if L_repl_results_ind = 'Y' then 
            LP_scaling_tbl(I_cur_index).alloc_no := O_alloc_no;
         end if;
      else
         /* alloc_detail exists, no need to check alloc_header */
         O_alloc_no := LP_scaling_tbl(I_cur_index).alloc_no;
         
         /* update alloc_detail */
         if LP_scaling_tbl(I_cur_index).qty_scaled_sol <> LP_scaling_tbl(I_cur_index).qty_ordered then
            if UPDATE_ALLOC_DETAIL(O_error_message,
                                   O_alloc_no,
                                   LP_scaling_tbl(I_cur_index).loc_type,
                                   LP_scaling_tbl(I_cur_index).location,
                                   LP_scaling_tbl(I_cur_index).qty_scaled_sol) = FALSE then
               return FALSE;
            end if;
         end if;
      end if; -- Ordloc_exist 
   elsif LP_scaling_tbl(I_cur_index).ordloc_exist = 'Y' then
      /* alloc_detail exists, no need to check alloc_header */
      O_alloc_no := LP_scaling_tbl(I_cur_index).alloc_no;
      
      if DELETE_ALLOC_DETAIL(O_error_message,
                             O_alloc_no,
                             LP_scaling_tbl(I_cur_index).loc_type,
                             LP_scaling_tbl(I_cur_index).location) = FALSE then
         return FALSE;
      end if;
      
      LP_alloc_delete_tbl(LP_alloc_delete_tbl.last + 1) := O_alloc_no;
      
      if L_repl_results_ind = 'Y' then
         LP_scaling_tbl(I_cur_index).alloc_no := -1;
      end if;
   end if;--qty_scaled_sol > 0
   
   if L_repl_results_ind = 'Y' then
      if UPDATE_REPL_RESULTS(O_error_message,
                             LP_order_no,
                             I_cur_index) = FALSE then
         return FALSE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END OUTPUT_ONE_XDOCK;
-------------------------------------------------------------------------------
FUNCTION CREATE_ALLOC_HEADER(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                             I_item              IN      ITEM_MASTER.ITEM%TYPE,
                             I_source_wh         IN      REPL_ITEM_LOC.SOURCE_WH%TYPE,
                             O_alloc_no          OUT     ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CREATE_ALLOC_HEADER';
   
   L_alloc_no                     ALLOC_HEADER.ALLOC_NO%TYPE;
   L_return_code                  VARCHAR2(10);
   L_release_date                 ORDHEAD.WRITTEN_DATE%TYPE;
   
   cursor C_ALLOC_DATES is
      select NVL(oh.not_before_date,p.vdate)
        from ordhead oh,period p
       where order_no = I_order_no;
   
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ALLOC_DATES','ORDHEAD','order_no:' || I_order_no);
   open C_ALLOC_DATES;
   SQL_LIB.SET_MARK('FETCH','C_ALLOC_DATES','ORDHEAD','order_no:' || I_order_no);
   fetch C_ALLOC_DATES into L_release_date;
   SQL_LIB.SET_MARK('CLOSE','C_ALLOC_DATES','ORDHEAD','order_no:' || I_order_no);
   close C_ALLOC_DATES;
   NEXT_ALLOC_NO(L_alloc_no,
                 L_return_code,
                 O_error_message);
   if L_return_code = 'FALSE' then
      return FALSE;
   end if;
   
   insert into alloc_header(alloc_no,
                            order_no,
                            wh,
                            item,
                            status,
                            alloc_desc,
                            po_type,
                            alloc_method,
                            release_date,
                            origin_ind)
                     values(L_alloc_no,
                            I_order_no,
                            I_source_wh,
                            I_item,
                            'A',
                            'PO #' || I_order_no,
                            NULL,
                            LP_alloc_method,
                            L_release_date,
                            'RMS');
   O_alloc_no := L_alloc_no;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_ALLOC_HEADER;
-------------------------------------------------------------------------------
FUNCTION CREATE_ALLOC_DETAIL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_alloc_no          IN      ALLOC_DETAIL.ALLOC_NO%TYPE,
                             I_from_loc          IN      ALLOC_DETAIL.TO_LOC%TYPE,
                             I_to_loc            IN      ALLOC_DETAIL.TO_LOC%TYPE,
                             I_to_loc_type       IN      ALLOC_DETAIL.TO_LOC_TYPE%TYPE,
                             I_item              IN      ITEM_MASTER.ITEM%TYPE,
                             I_qty_prescaled     IN      ALLOC_DETAIL.QTY_PRESCALED%TYPE,
                             I_qty_scaled        IN      ALLOC_DETAIL.QTY_PRESCALED%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.CREATE_ALLOC_DETAIL';
   
BEGIN
   
   insert into alloc_detail(alloc_no,
                            to_loc,
                            to_loc_type,
                            qty_prescaled,
                            qty_allocated,
                            qty_transferred,
                            qty_distro,
                            qty_selected,
                            qty_cancelled,
                            qty_received,
                            non_scale_ind,
                            po_rcvd_qty)
                     values(I_alloc_no,
                            I_to_loc,
                            I_to_loc_type,
                            I_qty_prescaled,
                            I_qty_scaled,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            'N',
                            NULL);

   if ALLOC_CHARGE_SQL.DEFAULT_CHRGS(O_error_message, 
                                     I_alloc_no,
                                     I_from_loc,
                                     I_to_loc,
                                     I_to_loc_type,
                                     I_item) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_ALLOC_DETAIL;
-------------------------------------------------------------------------------
FUNCTION UPDATE_ALLOC_DETAIL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_alloc_no          IN      ALLOC_DETAIL.ALLOC_NO%TYPE,
                             I_loc_type          IN      ALLOC_DETAIL.TO_LOC_TYPE%TYPE,
                             I_location          IN      ALLOC_DETAIL.TO_LOC%TYPE,
                             I_qty_scaled        IN      ALLOC_DETAIL.QTY_PRESCALED%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.UPDATE_ALLOC_DETAIL';
   RECORD_LOCKED                  EXCEPTION;
   PRAGMA                         EXCEPTION_INIT(Record_Locked, -54);
   
BEGIN
   SQL_LIB.SET_MARK('UPDATE',NULL,'ALLOC_DETAIL','alloc_no:'|| I_alloc_no || ' to_loc:' || I_location );
   update alloc_detail
      set qty_allocated = I_qty_scaled
    where alloc_no = I_alloc_no
      and to_loc = I_location
      and to_loc_type = I_loc_type;
   
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'ALLOC_DETAIL',
                                             TO_CHAR(I_alloc_no),
                                             TO_CHAR(I_location));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_ALLOC_DETAIL;
-------------------------------------------------------------------------------
FUNCTION DELETE_ALLOC_DETAIL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_alloc_no          IN      ALLOC_DETAIL.ALLOC_NO%TYPE,
                             I_loc_type          IN      ALLOC_DETAIL.TO_LOC_TYPE%TYPE,
                             I_location          IN      ALLOC_DETAIL.TO_LOC%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.DELETE_ALLOC_DETAIL';
   
BEGIN
   if ALLOC_CHARGE_SQL.DELETE_CHRGS(O_error_message,
                                    I_alloc_no,
                                    I_location) = FALSE then
      return FALSE;
   end if;
   
   delete from alloc_detail
         where alloc_no = I_alloc_no
           and to_loc = I_location
           and to_loc_type = I_loc_type;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ALLOC_DETAIL;
-------------------------------------------------------------------------------
FUNCTION UPDATE_REPL_RESULTS(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                             I_cur_index         IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.UPDATE_REPL_RESULTS';
   RECORD_LOCKED                  EXCEPTION;
   PRAGMA                         EXCEPTION_INIT(Record_Locked, -54);
   
BEGIN
   SQL_LIB.SET_MARK('UPDATE',NULL,'REPL_RESULTS','order_no:'|| I_order_no || ' location:' || LP_scaling_tbl(I_cur_index).location );
   update repl_results
      set alloc_no = LP_scaling_tbl(I_cur_index).alloc_no,
          order_roq = LP_scaling_tbl(I_cur_index).qty_scaled_sol
    where order_no = I_order_no
      and item = LP_scaling_tbl(I_cur_index).item
      and location = LP_scaling_tbl(I_cur_index).location
      and loc_type = LP_scaling_tbl(I_cur_index).loc_type;
   
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'REPL_RESULTS',
                                             TO_CHAR(I_order_no),
                                             LP_scaling_tbl(I_cur_index).item);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_REPL_RESULTS;
-------------------------------------------------------------------------------
FUNCTION UPDATE_ELC_ALL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.UPDATE_ELC_ALL';
   
BEGIN
   if LP_elc_update_tbl is NOT NULL and LP_elc_update_tbl.count > 0  then
      for i in 1..LP_elc_update_tbl.count LOOP
         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PA',                          /* calc type */
                                   LP_elc_update_tbl(i),          /* item */
                                   NULL,                          /* supplier */
                                   NULL,                          /* item_exp_type */
                                   NULL,                          /* item_exp_seq */
                                   LP_order_no,                   /* order number */
                                   NULL,                          /* order sequence number */
                                   NULL,                          /* pack item */
                                   NULL,                          /* zone id */
                                   NULL,                          /* location */
                                   NULL,                          /* hts */
                                   NULL,                          /* import country id */
                                   NULL,                          /* origin country id */
                                   NULL,                          /* effect from */
                                   NULL) = FALSE then             /* effect to */
            return FALSE;
         end if;
      END LOOP;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_ELC_ALL;
-------------------------------------------------------------------------------
FUNCTION DELETE_ORDSKU_ALL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.DELETE_ORDSKU_ALL';
   L_ordloc_exist                 VARCHAR2(1);
   
   
   cursor C_ORDLOC_EXIST(I_order_no      ORDLOC.ORDER_NO%TYPE,
                         I_item          ORDLOC.ITEM%TYPE) is
     select 'x'
       from ordloc
      where order_no = I_order_no
        and item = I_item;
BEGIN
   /*
    * Function traverse the item_delete tbl to delete ordsku record
    * if no more ordloc records associated with the order/item.
    */
   for i in 1..LP_item_delete_tbl.count LOOP
      L_ordloc_exist := 'y';
      SQL_LIB.SET_MARK('OPEN','C_ORDLOC_EXIST','ORDLOC','order_no:' || LP_order_no || ' item:' || LP_item_delete_tbl(i));
      open C_ORDLOC_EXIST(LP_order_no,
                          LP_item_delete_tbl(i));
      SQL_LIB.SET_MARK('FETCH','C_ORDLOC_EXIST','ORDLOC','order_no:' || LP_order_no || ' item:' || LP_item_delete_tbl(i));
      fetch C_ORDLOC_EXIST into L_ordloc_exist;
      SQL_LIB.SET_MARK('CLOSE','C_ORDLOC_EXIST','ORDLOC','order_no:' || LP_order_no || ' item:' || LP_item_delete_tbl(i));
      close C_ORDLOC_EXIST;
      
      if NVL(L_ordloc_exist,'y') <> 'x' then
         if ORDER_SETUP_SQL.DELETE_ORDER(O_error_message,
                                         LP_order_no,
                                         LP_item_delete_tbl(i),
                                         NULL,
                                         NULL,
                                         'ordsku') = FALSE then
            return FALSE;
         end if;
         /* delete ordsku */
         SQL_LIB.SET_MARK('DELETE',NULL,'ORDSKU','ORDER_NO: ' || TO_CHAR(LP_order_no));
         delete from ordsku
               where order_no = LP_order_no
                 and item = LP_item_delete_tbl(i);
         
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ORDSKU_ALL;
-------------------------------------------------------------------------------
FUNCTION DELETE_ALLOC_HEADER_ALL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.DELETE_ALLOC_HEADER_ALL';
   L_alloc_detail_exist           VARCHAR2(1);
   
   cursor C_ALLOC_DETAIL_EXIST(I_alloc_no   ALLOC_HEADER.ALLOC_NO%TYPE) is 
      select 'x'
        from alloc_detail
       where alloc_no = I_alloc_no;
   
BEGIN
   /*
    * Function traverses the alloc_delete tbl to delete alloc_header record
    * if no more alloc_detail records associated with the allocation.
    */
   for i in 1..LP_alloc_delete_tbl.count LOOP
      L_alloc_detail_exist := 'y';
      SQL_LIB.SET_MARK('OPEN','C_ALLOC_DETAIL_EXIST','ALLOC_DETAIL','alloc_no:' || TO_CHAR(LP_alloc_delete_tbl(i)));
      open C_ALLOC_DETAIL_EXIST(LP_alloc_delete_tbl(i));
      SQL_LIB.SET_MARK('OPEN','C_ALLOC_DETAIL_EXIST','ALLOC_DETAIL','alloc_no:' || TO_CHAR(LP_alloc_delete_tbl(i)));
      fetch C_ALLOC_DETAIL_EXIST into L_alloc_detail_exist;
      SQL_LIB.SET_MARK('OPEN','C_ALLOC_DETAIL_EXIST','ALLOC_DETAIL','alloc_no:' || TO_CHAR(LP_alloc_delete_tbl(i)));
      close C_ALLOC_DETAIL_EXIST;
      if NVL(L_alloc_detail_exist,'y') <> 'x' then
         SQL_LIB.SET_MARK('DELETE',NULL,'ALLOC_HEADER','ALLOC_NO: ' || TO_CHAR(LP_alloc_delete_tbl(i)));
         delete from alloc_header
               where alloc_no = LP_alloc_delete_tbl(i);
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ALLOC_HEADER_ALL;
-------------------------------------------------------------------------------
FUNCTION GET_BRACKET_INFO(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_bracket_val1      OUT     NUMBER,
                          O_bracket_type1     OUT     VARCHAR2,
                          O_bracket_uom1      OUT     VARCHAR2,
                          O_next_bracket      OUT     BOOLEAN)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.GET_BRACKET_INFO';
   
   L_bracket_type                 SUP_INV_MGMT.BRACKET_TYPE1%TYPE;
   L_bracket_val1                 SUP_INV_MGMT.THRESHOLD_NEXT_BRACKET%TYPE;
   L_bracket_type1                SUP_INV_MGMT.BRACKET_TYPE1%TYPE;
   L_bracket_uom1                 SUP_INV_MGMT.BRACKET_UOM1%TYPE;
   L_bracket_val2                 SUP_INV_MGMT.THRESHOLD_NEXT_BRACKET%TYPE;
   L_bracket_type2                SUP_INV_MGMT.BRACKET_TYPE2%TYPE;
   L_bracket_uom2                 SUP_INV_MGMT.BRACKET_UOM2%TYPE;
   L_total_order_qty              ORDLOC.QTY_ORDERED%TYPE;
   L_next                         BOOLEAN := FALSE;
   L_max_bracket                  SUP_INV_MGMT.SCALE_CNSTR_MAX_VAL1%TYPE;
   L_prim_or_sec                  VARCHAR2(1);
   
   L_sup_dept_seq_no              SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE := 0;
   L_supplier                     SUP_INV_MGMT.SUPPLIER%TYPE;
   L_dept                         SUP_INV_MGMT.DEPT%TYPE;
   L_location                     SUP_INV_MGMT.LOCATION%TYPE;
   L_cnstr_qty                    ORDLOC.QTY_PRESCALED%TYPE := 0;
   
BEGIN
   L_supplier := LP_scaling_tbl(1).supplier;
   L_dept := LP_scaling_tbl(1).dept;
   L_location := LP_scaling_tbl(1).location;
   
   /* reset the starting location quantities */
   for a in 1..LP_total_vloc LOOP
      LP_scale_loc_tbl(a).solution_day(1).qty_scaled(1) := 0;
   END LOOP;
   if ITEM_BRACKET_COST_SQL.GET_BRACKET(O_error_message,
                                        L_bracket_type1,
                                        L_bracket_uom1,
                                        L_bracket_type2,
                                        L_bracket_uom2,
                                        L_sup_dept_seq_no,
                                        L_supplier,  /* supplier */
                                        L_dept,      /* dept */
                                        L_location) = FALSE then
      return FALSE;
   end if;
   
   if L_bracket_type1 is NULL then
      return TRUE;
   end if;
   
   if LP_most_constraining <> 1 then
      L_prim_or_sec := 'S';
      L_bracket_type := L_bracket_type2;
   else
      L_prim_or_sec := 'P';
      L_bracket_type := L_bracket_type1;
   end if;
   L_max_bracket := LP_scale_cnstr_tbl(LP_most_constraining).scale_cnstr_max_val;
   
   for a in 1..LP_scaling_tbl.count LOOP
      for b in 1..LP_total_vloc LOOP
         if LP_scale_loc_tbl(b).location <> LP_scaling_tbl(a).location then
            continue;
         end if;
         
         LP_scale_loc_tbl(b).solution_day(1).qty_scaled(1) := LP_scale_loc_tbl(b).solution_day(1).qty_scaled(1) + LP_scaling_tbl(a).qty_scaled_sol;
      END LOOP;
      
      LP_scale_item_loc_tbl(a).qty_scaled_suom(1) := LP_scaling_tbl(a).qty_scaled_sol;
      LP_scale_item_loc_tbl(a).qty_scaled_suom_rnd(1) := LP_scaling_tbl(a).qty_scaled_sol;
      
      if CALC_CNSTR_QTY(O_error_message,
                        L_bracket_type,
                        a,
                        LP_scaling_tbl(a).qty_scaled_sol,
                        L_cnstr_qty) = FALSE then
         return FALSE;
      end if;
      L_total_order_qty := L_total_order_qty + L_cnstr_qty;
   END LOOP;
   
   if ORDER_BRACKET_CALC_SQL.APPLY_THRESHOLDS(O_error_message,
                                              L_bracket_val1,
                                              L_bracket_type1,
                                              L_bracket_uom1,
                                              L_bracket_val2,
                                              L_bracket_type2,
                                              L_bracket_uom2,
                                              L_next,
                                              LP_total_truck_load,
                                              L_total_order_qty,
                                              L_max_bracket,
                                              L_prim_or_sec,
                                              LP_order_no) = FALSE then
      return FALSE;
   end if;
   
   if L_next then
      if L_prim_or_sec = 'P' then
         if (L_bracket_type1 <> LP_ord_inv_mgmt_rec.scale_cnstr_type1 or
             L_bracket_uom1 <> LP_ord_inv_mgmt_rec.scale_cnstr_uom1) then
            O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_PRIM_BKT_COMP',NULL,NULL,NULL);
            LP_NON_FATAL := TRUE;
            return FALSE;
         end if;
         
         O_bracket_val1 := L_bracket_val1;
         O_bracket_type1 := L_bracket_type1;
         O_bracket_uom1 := L_bracket_uom1;
      else
         if (L_bracket_type1 <> LP_ord_inv_mgmt_rec.scale_cnstr_type1 or
             L_bracket_uom1 <> LP_ord_inv_mgmt_rec.scale_cnstr_uom1) then
            O_error_message := SQL_LIB.CREATE_MSG('SUPCSTR_SEC_BKT_COMP',NULL,NULL,NULL);
            LP_NON_FATAL := TRUE;
            return FALSE;
         end if;
         O_bracket_val1 := L_bracket_val1;
         O_bracket_type1 := L_bracket_type1;
         O_bracket_uom1 := L_bracket_uom1;
      end if; -- Prim or Sec
   else
      O_bracket_val1 := -1;
      O_bracket_type1 := NULL;
      O_bracket_uom1 := NULL;
   end if; -- L_next
   
   O_next_bracket := L_next;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_BRACKET_INFO;
-------------------------------------------------------------------------------
FUNCTION UPDATE_GP_ROUND_QTY(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no          IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.UPDATE_GP_ROUND_QTY';
   
BEGIN
   if ROUNDING_SQL.CALC_ROUNDED_GROUP_QTYS(O_error_message,
                                           I_order_no) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_GP_ROUND_QTY;
-------------------------------------------------------------------------------
FUNCTION UPDATE_TRUCKLOAD(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                          I_num_truck_load    IN      NUMBER)
RETURN BOOLEAN IS
   L_function                     VARCHAR2(50) := 'SUP_CONSTRAINTS_SQL.UPDATE_TRUCKLOAD';
   RECORD_LOCKED                  EXCEPTION;
   PRAGMA                         EXCEPTION_INIT(Record_Locked, -54);
   
BEGIN
   SQL_LIB.SET_MARK('UPDATE',NULL,'ORD_INV_MGMT','order_no:'|| I_order_no);
   update ord_inv_mgmt
      set no_vehicles = I_num_truck_load
    where order_no = I_order_no;
    
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'ORD_INV_MGMT',
                                             TO_CHAR(I_order_no),
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_TRUCKLOAD;
-------------------------------------------------------------------------------
END SUP_CONSTRAINTS_SQL;
/