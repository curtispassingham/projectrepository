
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY POP_TERMS_SQL AS
-------------------------------------------------------------------------------------------
--Function Name:  GET_NEXT_POP_DEF_SEQ_NO
--Purpose:        Gets the next available POP_DEF_SEQ_NO from the POP_DEF_SEQ_NO_SEQUENCE
------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_POP_DEF_SEQ_NO(O_error_message          IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_pop_def_seq_no         IN OUT     POP_TERMS_DEF.POP_DEF_SEQ_NO%TYPE)
RETURN BOOLEAN IS

BEGIN

   select pop_def_seq_no_sequence.nextval
     into O_pop_def_seq_no
     from sys.dual;
   ---
   return TRUE;      
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'POP_SQL.GET_NEXT_POP_DEF_SEQ_NO',
                                             to_char(SQLCODE));
   return FALSE;
END GET_NEXT_POP_DEF_SEQ_NO;    
-------------------------------------------------------------------------------------------
--Function Name:  GET_NEXT_POP_FULFILL_SEQ_NO
--Purpose:        Gets the next available POP_DEF_SEQ_NO from the POP_DEF_SEQ_NO_SEQUENCE
------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_POP_FULFILL_SEQ_NO(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_pop_fulfill_seq_no IN OUT POP_TERMS_FULFILLMENT.POP_FULFILL_SEQ_NO%TYPE)
RETURN BOOLEAN IS

BEGIN

   select pop_fulfill_seq_no_sequence.nextval
     into O_pop_fulfill_seq_no
     from sys.dual;
   ---
   return TRUE;      

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'POP_SQL.GET_NEXT_POP_FULFILL_SEQ_NO',
                                             to_char(SQLCODE));
   return FALSE;
END GET_NEXT_POP_FULFILL_SEQ_NO;    
--------------------------------------------------------------------------------------------
FUNCTION CHECK_POP_FULFILL_EXIST    (O_error_message      IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_fulfills_exist     IN OUT    BOOLEAN,
                                     I_pop_def_seq_no     IN        POP_TERMS_DEF.POP_DEF_SEQ_NO%TYPE)
RETURN BOOLEAN

IS
      cursor C_CHECK_POP_FULFILL_EXIST is
      select 'Y'
        from pop_terms_fulfillment
       where pop_def_seq_no = I_pop_def_seq_no;

   L_dummy     VARCHAR2(1) := 'N';

BEGIN
   ---
   if I_pop_def_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            'POP_SQL.GET_NEXT_POP_FULFILL_SEQ_NO',
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_POP_FULFILL_EXIST' ,'POP_TERMS_FULFILLMENT',to_char(I_pop_def_seq_no));
   open C_CHECK_POP_FULFILL_EXIST;
   ---
   SQL_LIB.SET_MARK('FETCH','C_CHECK_POP_FULFILL_EXIST','POP_TERMS_FULFILLMENT',to_char(I_pop_def_seq_no));
   fetch C_CHECK_POP_FULFILL_EXIST into L_dummy;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_POP_FULFILL_EXIST','POP_TERMS_FULFILLMENT',to_char(I_pop_def_seq_no));
   close C_CHECK_POP_FULFILL_EXIST;
   ---
   if L_dummy = 'Y' then
      O_fulfills_exist := TRUE;
   else
      O_fulfills_exist := FALSE;
   end if;
   ---
   return TRUE;  
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'POP_SQL.CHECK_POP_FULFILL_EXIST',
                                             to_char(SQLCODE));
   return FALSE;
END CHECK_POP_FULFILL_EXIST; 
-------------------------------------------------------------------------------------------
FUNCTION DELETE_FULFILLMENTS        (O_error_message      IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_pop_def_seq_no     IN        POP_TERMS_DEF.POP_DEF_SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_table            VARCHAR2(30);
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

cursor C_LOCK_POP_TERMS_FULFILLMENT is 
      select 'x'
       from pop_terms_fulfillment
      where pop_def_seq_no = I_pop_def_seq_no     
        for update nowait;

BEGIN

 --lock table pop_terms_fulfillment and remove fulfillments
   ---
   L_table := 'pop_terms_fulfillment';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_POP_TERMS_FULFILLMENT',
                    'POP_TERMS_FULFILLMENT',
                    'POP DEF SEQ NO: '||to_char(I_pop_def_seq_no));

   open C_LOCK_POP_TERMS_FULFILLMENT;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_POP_TERMS_FULFILLMENT',
                    'POP_TERMS_FULFILLMENT',
                    'POP DEF SEQ NO: '||to_char(I_pop_def_seq_no));

   close C_LOCK_POP_TERMS_FULFILLMENT;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL,'POP_TERMS_FULFILLMENT', 'POP DEF SEQ NO: '||to_char(I_pop_def_seq_no));

   delete from pop_terms_fulfillment
    where pop_def_seq_no = I_pop_def_seq_no;
   ---
   return TRUE;  
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                       				  NULL);
         return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'POP_TERMS_SQL.DELETE_FULFILLMENTS',
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_FULFILLMENTS; 
-------------------------------------------------------------------------------------------
END POP_TERMS_SQL;
/
