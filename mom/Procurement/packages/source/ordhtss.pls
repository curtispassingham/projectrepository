CREATE OR REPLACE PACKAGE ORDER_HTS_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name:  GET_NEXT_SEQ
--Purpose      :  Retrieves the next sequence number. 
-------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ(O_error_message IN OUT VARCHAR2,
                      O_seq_no        IN OUT ORDSKU_HTS.SEQ_NO%TYPE,
                      I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  HTS_EXIST
--Purpose      :  Checks if a passed in Order Item HTS record
--                exists on the ordsku_hts table.
-------------------------------------------------------------------------------
FUNCTION HTS_EXIST(O_error_message     IN OUT VARCHAR2,
                   O_exists            IN OUT BOOLEAN,
                   I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                   I_item              IN     ITEM_MASTER.ITEM%TYPE,
                   I_pack_item         IN     ITEM_MASTER.ITEM%TYPE,
                   I_hts               IN     HTS.HTS%TYPE)
RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  ASSESS_EXIST
--Purpose      :  Checks if a passed in Order Item HTS Assessment record
--                exists on the ordsku_hts_assess table.
-------------------------------------------------------------------------------
FUNCTION ASSESS_EXIST(O_error_message     IN OUT VARCHAR2,
                      O_exists            IN OUT BOOLEAN,
                      I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                      I_seq_no            IN     ORDSKU_HTS_ASSESS.SEQ_NO%TYPE,
                      I_comp_id           IN     ELC_COMP.COMP_ID%TYPE)
RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  DEL_ASSESS
--Purpose      :  Deletes all the Order Item HTS Assessment records for
--                the Order Item HTS record passed in.
-------------------------------------------------------------------------------
FUNCTION DELETE_ASSESS(O_error_message  IN OUT VARCHAR2,
                       I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                       I_seq_no         IN     ORDSKU_HTS_ASSESS.SEQ_NO%TYPE)
RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  GET_HTS_DETAILS
--Purpose      :  Retrieves the details of the HTS passed in. 
-------------------------------------------------------------------------------
FUNCTION GET_HTS_DETAILS(O_error_message        IN OUT VARCHAR2,
                         O_tariff_treatment     IN OUT HTS_TARIFF_TREATMENT.TARIFF_TREATMENT%TYPE,
                         O_qty_1                IN OUT NUMBER,
                         O_qty_2                IN OUT NUMBER,
                         O_qty_3                IN OUT NUMBER,
                         O_units_1              IN OUT HTS.UNITS_1%TYPE,
                         O_units_2              IN OUT HTS.UNITS_2%TYPE,
                         O_units_3              IN OUT HTS.UNITS_3%TYPE,
                         O_specific_rate        IN OUT HTS_TARIFF_TREATMENT.SPECIFIC_RATE%TYPE,
                         O_av_rate              IN OUT HTS_TARIFF_TREATMENT.AV_RATE%TYPE,
                         O_other_rate           IN OUT HTS_TARIFF_TREATMENT.OTHER_RATE%TYPE,
                         O_cvd_case_no          IN OUT HTS_CVD.CASE_NO%TYPE,
                         O_ad_case_no           IN OUT HTS_AD.CASE_NO%TYPE,
                         O_duty_comp_code       IN OUT HTS.DUTY_COMP_CODE%TYPE,
                         I_item                 IN     ITEM_MASTER.ITEM%TYPE,
                         I_order_no             IN     ORDHEAD.ORDER_NO%TYPE,
                         I_hts                  IN     HTS.HTS%TYPE,
                         I_import_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                         I_duty_calc_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                         I_effect_from          IN     HTS.EFFECT_FROM%TYPE,
                         I_effect_to            IN     HTS.EFFECT_TO%TYPE,
                         I_pack_item            IN     ITEM_MASTER.ITEM%TYPE DEFAULT NULL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name : DEFAULT_ASSESS
-- Purpose       : This function insert assessment records into the Order Item HTS 
--                 Assessment table for HTS codes attached to Items on the Order.
------------------------------------------------------------------------------------------
FUNCTION DEFAULT_ASSESS(O_error_message         IN OUT VARCHAR2,
                        I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                        I_seq_no                IN     ORDSKU_HTS.SEQ_NO%TYPE,
                        I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                        I_hts                   IN     HTS.HTS%TYPE,
                        I_import_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                        I_origin_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                        I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                        I_effect_to             IN     HTS.EFFECT_TO%TYPE)
   RETURN BOOLEAN; 
------------------------------------------------------------------------------------------
-- Function Name : DEFAULT_CALC_ASSESS
-- Purpose       : This function inserts assessment records into the Order Item HTS 
--                 Assessment table for HTS codes attached to Items on the Order and
--                 then it will calculate all of the assessments that were inserted.
------------------------------------------------------------------------------------------
FUNCTION DEFAULT_CALC_ASSESS(O_error_message         IN OUT VARCHAR2,
                             I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                             I_seq_no                IN     ORDSKU_HTS.SEQ_NO%TYPE,
                             I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                             I_hts                   IN     HTS.HTS%TYPE,
                             I_import_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_origin_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                             I_effect_to             IN     HTS.EFFECT_TO%TYPE)
RETURN BOOLEAN; 
------------------------------------------------------------------------------------------
-- Function Name : DEFAULT_ITEM_ASSESS
-- Purpose       : This function inserts assessment records into the Order Item HTS 
--                 Assessment table for HTS codes attached to Items on the Order using 
--                 Item HTS Assessment table.
------------------------------------------------------------------------------------------
FUNCTION DEFAULT_ITEM_ASSESS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                             I_seq_no                IN     ORDSKU_HTS.SEQ_NO%TYPE,
                             I_item                  IN     ITEM_MASTER.ITEM%TYPE,
                             I_hts                   IN     HTS.HTS%TYPE,
                             I_import_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_origin_country_id     IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_effect_from           IN     HTS.EFFECT_FROM%TYPE,
                             I_effect_to             IN     HTS.EFFECT_TO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: DEFAULT_HTS
-- Purpose      : Attaches the appropriate HTS codes to an Order/Item combination 
--                when the Item is added to the Order.
------------------------------------------------------------------------------------------
FUNCTION DEFAULT_HTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                     I_item              IN     ITEM_MASTER.ITEM%TYPE,
                     I_component_item    IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: DEFAULT_CALC_HTS
-- Purpose      : To attach the appropriate HTS codes to an Order/Item combination 
--                when the Item is added to the Order and then calculates all of the 
--                assessments that were added.
------------------------------------------------------------------------------------------
FUNCTION DEFAULT_CALC_HTS(O_error_message     IN OUT VARCHAR2,
                          I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_component_item    IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:  GET_UOM_RATE_VALUE
--Purpose      :  Retrieves the per count uom, component rate, and estimated
--                assessment value from the ordsku_hts_assess table.  If a currency is
--                passed in then converts rate and estimated assessment value to
--                the passed in currency.
--------------------------------------------------------------------------------------
FUNCTION GET_UOM_RATE_VALUE(O_error_message    IN OUT  VARCHAR2,
                            O_exists           IN OUT  BOOLEAN,
                            O_per_count_uom    IN OUT  UOM_CLASS.UOM%TYPE,
                            O_comp_rate        IN OUT  ELC_COMP.COMP_RATE%TYPE,
                            O_est_assess_value IN OUT  ORDSKU_HTS_ASSESS.EST_ASSESS_VALUE%TYPE,
                            I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                            I_item             IN      ITEM_MASTER.ITEM%TYPE,
                            I_pack_item        IN      ITEM_MASTER.ITEM%TYPE,
                            I_hts              IN      HTS.HTS%TYPE,
                            I_comp_id          IN      ELC_COMP.COMP_ID%TYPE,
                            I_currency_code    IN      CURRENCIES.CURRENCY_CODE%TYPE,
                            I_exchange_rate    IN      CURRENCY_RATES.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:  CHECK_ORDLOC_WKSHT
--Purpose      :  Checks if the order no exists on ordloc_wksht table given that the origin country is different
--                from the import country.
--------------------------------------------------------------------------------------
FUNCTION CHECK_ORDLOC_WKSHT(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT VARCHAR2,
                            I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--Function Name:  CHECK_HTS
--Purpose      :  Checks if the order no is valid for HTS.
--------------------------------------------------------------------------------------
FUNCTION CHECK_HTS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists          IN OUT VARCHAR2,
                   I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: DEFAULT_COM_HTS
-- Purpose      : Deletes the existing hts and assessments of an order/item and 
--                Attaches the appropriate HTS codes of a Country of Manufacture to 
--                an Order/Item. This function is called from ordhts form, whenever the
--                Country of Manufacture is changed.
----------------------------------------------------------------------------------------
FUNCTION DEFAULT_COM_HTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack              IN     ITEM_MASTER.ITEM%TYPE,
                         I_com               IN     COUNTRY.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
END ORDER_HTS_SQL;
/
