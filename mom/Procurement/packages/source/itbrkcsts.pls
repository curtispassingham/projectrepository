
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEM_BRACKET_COST_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------
-- Name:       GET_BRACKET
-- Purpose:    This function will retreive the bracket types and seq no
--             for the supplier, dept, loc passed in.
-----------------------------------------------------------------------------
FUNCTION GET_BRACKET(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_bracket_type1     IN OUT   SUP_INV_MGMT.BRACKET_TYPE1%TYPE,
                     O_bracket_uom1      IN OUT   SUP_INV_MGMT.BRACKET_UOM1%TYPE,
                     O_bracket_type2     IN OUT   SUP_INV_MGMT.BRACKET_TYPE2%TYPE,
                     O_bracket_uom2      IN OUT   SUP_INV_MGMT.BRACKET_UOM2%TYPE,
                     O_sup_dept_seq_no   IN OUT   SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE,
                     I_supplier          IN       SUP_INV_MGMT.SUPPLIER%TYPE,
                     I_dept              IN       SUP_INV_MGMT.DEPT%TYPE,
                     I_location          IN       SUP_INV_MGMT.LOCATION%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       UPDATE_ALL_LOCATION_BRACKETS
-- Purpose:    This function will update all location bracket costs with the costs
--             at the country bracket level.
-----------------------------------------------------------------------------
FUNCTION UPDATE_ALL_LOCATION_BRACKETS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_item               IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ITEM%TYPE,
                                      I_supplier           IN       ITEM_SUPP_COUNTRY_BRACKET_COST.SUPPLIER%TYPE,
                                      I_origin_country     IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ORIGIN_COUNTRY_ID%TYPE,
                                      I_process_children   IN       VARCHAR2)
 RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       UPDATE_LOCATION_COST
-- Purpose:    This function will update the location cost with default bracket cost.
-----------------------------------------------------------------------------
FUNCTION UPDATE_LOCATION_COST(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item               IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ITEM%TYPE,
                              I_supplier           IN       ITEM_SUPP_COUNTRY_BRACKET_COST.SUPPLIER%TYPE,
                              I_origin_country     IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ORIGIN_COUNTRY_ID%TYPE,
                              I_location           IN       ITEM_SUPP_COUNTRY_BRACKET_COST.LOCATION%TYPE,
                              I_process_children   IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       MC_UPDATE_LOCATIONS
-- Purpose:    This function will all virtual warehouse costs within the same physical
--             warehouse to the same cost.  It will then update the location level costs.
-----------------------------------------------------------------------------
FUNCTION MC_UPDATE_LOCATIONS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item               IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ITEM%TYPE,
                             I_supplier           IN       ITEM_SUPP_COUNTRY_BRACKET_COST.SUPPLIER%TYPE,
                             I_origin_country     IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ORIGIN_COUNTRY_ID%TYPE,
                             I_location           IN       ITEM_SUPP_COUNTRY_BRACKET_COST.LOCATION%TYPE,
                             I_process_children   IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       UPDATE_CHILDREN
-- Purpose:    This function will update the child record brackets with the parent costs.
-----------------------------------------------------------------------------
FUNCTION UPDATE_CHILDREN(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item             IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ITEM%TYPE,
                         I_supplier         IN       ITEM_SUPP_COUNTRY_BRACKET_COST.SUPPLIER%TYPE,
                         I_origin_country   IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ORIGIN_COUNTRY_ID%TYPE,
                         I_location         IN       ITEM_SUPP_COUNTRY_BRACKET_COST.LOCATION%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       BRACKETS_EXIST
-- Purpose:    This function will verify bracket records exist for all the brackets for
--             the passed in item, supplier, country and location if it is passed.
-----------------------------------------------------------------------------
FUNCTION BRACKETS_EXIST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists           IN OUT   BOOLEAN,
                        I_item             IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ITEM%TYPE,
                        I_supplier         IN       ITEM_SUPP_COUNTRY_BRACKET_COST.SUPPLIER%TYPE,
                        I_origin_country   IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ORIGIN_COUNTRY_ID%TYPE,
                        I_location         IN       ITEM_SUPP_COUNTRY_BRACKET_COST.LOCATION%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       CREATE_BRACKET
-- Purpose:    This function will create the bracket records for an item.
---            If the location is passed in as NULL, one of two things will happen.
---            First, if no bracket structures were created for locations for the
---            passed in supplier, a bracket structure will be created at the
---            item/supplier/country level and the function will return TRUE and
---            stop processing.  Otherwise, if either a structure does exist at the location
---            level or a record has already been created for the item/supplier/country,
---            then bracket records will be created for every location attached to the
---            item.  First, all location with specific brackets will be created, then
---            the locations without specific brackets will be created.
-----------------------------------------------------------------------------
FUNCTION CREATE_BRACKET(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item             IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ITEM%TYPE,
                        I_supplier         IN       ITEM_SUPP_COUNTRY_BRACKET_COST.SUPPLIER%TYPE,
                        I_origin_country   IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ORIGIN_COUNTRY_ID%TYPE,
                        I_location         IN       ITEM_SUPP_COUNTRY_BRACKET_COST.LOCATION%TYPE,
                        I_all_locs         IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       BRACKET_COST_EXISTS
-- Purpose:    This function will verify costs exist for all the brackets for
--             the passed in item, supplier, country and location if it is passed.
-----------------------------------------------------------------------------
FUNCTION BRACKET_COST_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists           IN OUT   BOOLEAN,
                             I_item             IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ITEM%TYPE,
                             I_supplier         IN       ITEM_SUPP_COUNTRY_BRACKET_COST.SUPPLIER%TYPE,
                             I_origin_country   IN       ITEM_SUPP_COUNTRY_BRACKET_COST.ORIGIN_COUNTRY_ID%TYPE,
                             I_location         IN       ITEM_SUPP_COUNTRY_BRACKET_COST.LOCATION%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       SUPP_BRACKET_EXISTS
-- Purpose:    This function will verify if the supplier is set up for
--             bracket costing.
-----------------------------------------------------------------------------
FUNCTION SUPP_BRACKET_EXISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists           IN OUT   BOOLEAN,
                             I_supplier         IN       SUP_BRACKET_COST.SUPPLIER%TYPE,
                             I_origin_country   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_dept             IN       SUP_BRACKET_COST.DEPT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
END ITEM_BRACKET_COST_SQL;
/
