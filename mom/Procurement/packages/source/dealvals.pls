CREATE OR REPLACE PACKAGE DEAL_VALIDATE_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------
-- Name: DEAL_ID
-- Purpose: This function checks for existence of a passed-in
--  Deal ID on the DEAL_HEAD table.
-- Created by: Corey Nash(7/20/99)
-------------------------------------------------------------------------------
FUNCTION DEAL_ID (O_error_message IN OUT VARCHAR2,
                  O_exist         IN OUT BOOLEAN,
                  I_deal_id       IN     deal_head.deal_id%TYPE)
RETURN BOOLEAN;



-------------------------------------------------------------------------------
-- Name: THRESHOLD_OVERLAP
-- Purpose: This function checks for overlapping deal thresholds.
-- Created by: Corey Nash(7/20/99)
-------------------------------------------------------------------------------
FUNCTION THRESHOLD_OVERLAP(O_error_message   IN OUT VARCHAR2,
                           O_overlap_exists  IN OUT BOOLEAN,
                           I_deal_id         IN     deal_head.deal_id%TYPE,
                           I_deal_detail_id  IN     deal_detail.deal_detail_id%TYPE,
                           I_lower_limit     IN     deal_threshold.lower_limit%TYPE,
                           I_upper_limit     IN     deal_threshold.upper_limit%TYPE,
                           I_row_id          IN     ROWID)
RETURN BOOLEAN;


-------------------------------------------------------------------------------
-- Name: VALID_ITEM_SUPP_HIER
-- Purpose: This function will verify that the passed in item is valid for the
-- given supplier (at any level of hierarchy) and origin country.  An item and
-- supplier must be entered but the origin country parameter is optional.
-- Either supplier or partner type and partner ID must be entered.
-- Created by: Corey Nash(7/22/99)
-------------------------------------------------------------------------------
FUNCTION VALID_ITEM_SUPP_HIER(O_error_message   IN OUT VARCHAR2,
                              O_exist           IN OUT BOOLEAN,
                              I_item            IN     ITEM_MASTER.ITEM%TYPE,
                              I_partner_type    IN     deal_head.partner_type%TYPE,
                              I_partner_id      IN     deal_head.partner_id%TYPE,
                              I_supplier        IN     sups.supplier%TYPE,
                              I_origin_country  IN     country.country_id%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name: CHK_ACTIVE_DEALS_SUP_HIER
-- Purpose: This function will check for active deals for a given
-- item/country/supplier hierarchy level relationship.  I_sku,
-- I_origin_country_id, I_partner_id and I_partner_type are required.
-- Created by: Corey Nash(7/26/99)
-------------------------------------------------------------------------------
FUNCTION CHK_ACTIVE_DEALS_SUP_HIER(O_error_message       IN OUT VARCHAR2,
                                   O_exist               IN OUT BOOLEAN,
                                   I_item                IN     deal_itemloc.item%TYPE,
                                   I_origin_country_id   IN     deal_itemloc.origin_country_id%TYPE,
                                   I_partner_id          IN     deal_head.partner_id%TYPE,
                                   I_partner_type        IN     deal_head.partner_type%TYPE)

RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name: CHECK_DELETE_ITEMLOC
-- Purpose: This function will check that the deal_itemloc record being deleted
-- does not have records at a lower merchandise hierarchy excluded from it.
-- The deal_id and deal_detail_id must be passed into this function.  Only
-- inclusion records (i.e. I_excl_ind = 'N') may be passed into this function.
-- Created by: Corey Nash(8/2/99)
-------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_ITEMLOC(O_error_message     IN OUT VARCHAR2,
                              O_exist             IN OUT BOOLEAN,
                              I_deal_id           IN     deal_itemloc.deal_id%TYPE,
                              I_deal_detail_id    IN     deal_itemloc.deal_detail_id%TYPE,
                              I_merch_level       IN     deal_itemloc.merch_level%TYPE,
                              I_company_ind       IN     deal_itemloc.company_ind%TYPE,
                              I_division          IN     deal_itemloc.division%TYPE,
                              I_group             IN     deal_itemloc.group_no%TYPE,
                              I_dept              IN     deal_itemloc.dept%TYPE,
                              I_class             IN     deal_itemloc.class%TYPE,
                              I_subclass          IN     deal_itemloc.subclass%TYPE,
                              I_item_grandparent  IN     deal_itemloc.item_grandparent%TYPE,
                              I_item_parent       IN     deal_itemloc.item_parent%TYPE,
                              I_diff_1            IN     deal_itemloc.diff_1%TYPE,
                              I_diff_2            IN     deal_itemloc.diff_2%TYPE,
                              I_diff_3            IN     deal_itemloc.diff_3%TYPE,
                              I_diff_4            IN     deal_itemloc.diff_4%TYPE,
                              I_item              IN     deal_itemloc.item%TYPE,
                              I_org_level         IN     deal_itemloc.org_level%TYPE,
                              I_chain             IN     deal_itemloc.chain%TYPE,
                              I_area              IN     deal_itemloc.area%TYPE,
                              I_region            IN     deal_itemloc.region%TYPE,
                              I_district          IN     deal_itemloc.district%TYPE,
                              I_location          IN     deal_itemloc.location%TYPE,
                              I_origin_country_id IN     deal_itemloc.origin_country_id%TYPE,
                              I_excl_ind          IN     deal_itemloc.excl_ind%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name: CHECK_DUP_ITEMLOC
-- Purpose: This function will be called before a new record is inserted into
-- the deal_itemloc table to verify if the passed in Deal Item/Location record
-- values exist on the DEAL_ITEMLOC table.  The Deal ID, Deal Detail ID must be
-- passed into this function.
-- Created by: Corey Nash(8/2/99)
-------------------------------------------------------------------------------
FUNCTION CHECK_DUP_ITEMLOC(O_error_message     IN OUT VARCHAR2,
                           O_exists            IN OUT BOOLEAN,
                           I_deal_id           IN     deal_itemloc.deal_id%TYPE,
                           I_deal_detail_id    IN     deal_itemloc.deal_detail_id%TYPE,
                           I_merch_level       IN     deal_itemloc.merch_level%TYPE,
                           I_org_level         IN     deal_itemloc.org_level%TYPE,
                           I_origin_country_id IN     country.country_id%TYPE,
                           I_company_ind       IN     deal_itemloc.company_ind%TYPE,
                           I_division          IN     deal_itemloc.division%TYPE,
                           I_group             IN     deal_itemloc.group_no%TYPE,
                           I_dept              IN     deal_itemloc.dept%TYPE,
                           I_class             IN     deal_itemloc.class%TYPE,
                           I_subclass          IN     deal_itemloc.subclass%TYPE,
                           I_item_grandparent  IN     deal_itemloc.item_grandparent%TYPE,
                           I_item_parent       IN     deal_itemloc.item_parent%TYPE,
                           I_diff_1            IN     deal_itemloc.diff_1%TYPE,
                           I_diff_2            IN     deal_itemloc.diff_2%TYPE,
                           I_diff_3            IN     deal_itemloc.diff_3%TYPE,
                           I_diff_4            IN     deal_itemloc.diff_4%TYPE,
                           I_chain             IN     deal_itemloc.chain%TYPE,
                           I_area              IN     deal_itemloc.area%TYPE,
                           I_region            IN     deal_itemloc.region%TYPE,
                           I_district          IN     deal_itemloc.district%TYPE,
                           I_loc_type          IN     deal_itemloc.loc_type%TYPE,
                           I_location          IN     deal_itemloc.location%TYPE,
                           I_item              IN     deal_itemloc.item%TYPE,
                           I_excl_ind          IN     deal_itemloc.excl_ind%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name: CONFLICT_COLLECT_DATES_EXIST
-- Purpose: This function will verify if the passed in Deal has any deal
-- components with collect start dates that conflict with the deal active date.
-- The Deal ID must be passed into the function.
-- Created by: Corey Nash(8/3/99)
-------------------------------------------------------------------------------
FUNCTION CONFLICT_COLLECT_DATES_EXIST(O_error_message     IN OUT VARCHAR2,
                                      O_exists            IN OUT BOOLEAN,
                                      O_confl_deal_comp   IN OUT deal_itemloc.deal_detail_id%TYPE,
                                      I_deal_id           IN     deal_itemloc.deal_id%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name: ITEMLOC_EXISTS
-- Purpose: This function will check if deal_itemloc records exist for a given
-- deal_id and deal component.  The deal id and deal component must be passed
-- into this function.
-- Created by: Corey Nash(8/3/99)
-------------------------------------------------------------------------------
FUNCTION ITEMLOC_EXISTS(O_error_message     IN OUT VARCHAR2,
                        O_exists            IN OUT BOOLEAN,
                        I_deal_id           IN     deal_head.deal_id%TYPE,
                        I_deal_detail_id    IN     deal_detail.deal_detail_id%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name: VALIDATE_ITEMLOC
-- Purpose: This function will check that the passed in deal item/location
-- record does not conflict with existing records for the same deal ID and deal
-- detail ID. Record validation will be based on the following rules:
--    - All deal_itemloc records must have a merchandise level.
--    - All inclusion records for a given deal component must be at the same
--      merchandize level, organization level and origin country level.
-- Parameters I_deal_id, I_deal_detail_id and  I_merch_level must be passed
-- into this function.
-- Created by: Corey Nash(8/3/99)
-------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEMLOC (O_error_message     IN OUT VARCHAR2,
                           O_valid             IN OUT BOOLEAN,
                           I_deal_id           IN     deal_itemloc.deal_id%TYPE,
                           I_deal_detail_id    IN     deal_itemloc.deal_detail_id%TYPE,
                           I_merch_level       IN     deal_itemloc.merch_level%TYPE,
                           I_org_level         IN     deal_itemloc.org_level%TYPE,
                           I_origin_country_id IN     deal_itemloc.origin_country_id%TYPE,
                           I_company_ind       IN     deal_itemloc.company_ind%TYPE,
                           I_division          IN     deal_itemloc.division%TYPE,
                           I_group             IN     deal_itemloc.group_no%TYPE,
                           I_dept              IN     deal_itemloc.dept%TYPE,
                           I_class             IN     deal_itemloc.class%TYPE,
                           I_subclass          IN     deal_itemloc.subclass%TYPE,
                           I_item_grandparent  IN     deal_itemloc.item_grandparent%TYPE,
                           I_item_parent       IN     deal_itemloc.item_parent%TYPE,
                           I_diff_1            IN     deal_itemloc.diff_1%TYPE,
                           I_diff_2            IN     deal_itemloc.diff_2%TYPE,
                           I_diff_3            IN     deal_itemloc.diff_3%TYPE,
                           I_diff_4            IN     deal_itemloc.diff_4%TYPE,
                           I_item              IN     deal_itemloc.item%TYPE,
                           I_chain             IN     deal_itemloc.chain%TYPE,
                           I_area              IN     deal_itemloc.area%TYPE,
                           I_region            IN     deal_itemloc.region%TYPE,
                           I_district          IN     deal_itemloc.district%TYPE,
                           I_loc_type          IN     deal_itemloc.loc_type%TYPE,
                           I_location          IN     deal_itemloc.location%TYPE,
                           I_excl_ind          IN     deal_itemloc.excl_ind%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  CHECK_THRESH_LIMITS
--Purpose:   This package function will indicate if a deal contains components that do not have an
--           unlimited upper limit record.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_THRESH_LIMITS(O_error_message     IN OUT VARCHAR2,
                              O_exists           IN OUT BOOLEAN,
                              I_deal_id          IN     DEAL_HEAD.DEAL_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  PO_DEAL_EXIST
--Purpose:   This function will check for a PO-specific deal for the passed in PO number.  If a deal
--           exists, it will return the deal ID.
--------------------------------------------------------------------------------------------------------
FUNCTION PO_DEAL_EXIST(O_error_message  IN OUT  VARCHAR2,
                       O_exist          IN OUT  BOOLEAN,
                       O_deal_id        IN OUT  deal_head.deal_id%TYPE,
                       I_order_no       IN      deal_head.order_no%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  CHECK_DELETE_DEAL
--Purpose:   This package function will check if a deal ID or deal ID and component combination exists
--           on the PRICE_SUSP_HEAD and ORDSKU_DISCOUNT tables.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_DEAL(O_error_message    IN OUT VARCHAR2,
                           O_exists           IN OUT BOOLEAN,
                           I_deal_id          IN     DEAL_HEAD.DEAL_ID%TYPE,
                           I_deal_detail_id   IN     DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  TARGET_LEVEL_EXISTS
--Purpose:   This package function will check that a target level threshold is indicated for every deal
--           component within a deal.
--------------------------------------------------------------------------------------------------------
FUNCTION TARGET_LEVEL_EXISTS(O_error_message    IN OUT VARCHAR2,
                             O_exists           IN OUT BOOLEAN,
                             O_deal_detail_id   IN OUT DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                             I_deal_id          IN     DEAL_HEAD.DEAL_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  CHK_FREE_GOODS_DEALS
--Purpose:   This function will check the validity of free goods deals.  If there is a free good on
--           the PO that has a unit cost that is greater than the ordsku.unit_cost of the buy item,
--           the free good deal will not be applied to prevent the buy item's cost from going negative.
--------------------------------------------------------------------------------------------------------
FUNCTION CHK_FREE_GOODS_DEALS(O_error_message         IN OUT            VARCHAR2,
                              O_exist                 IN OUT            BOOLEAN,
                              O_item                  IN OUT            item_master.item%TYPE,
                              I_order_no              IN                ordhead.order_no%TYPE,
                              I_location              IN                ordloc.location%TYPE,
                              I_not_before_date       IN                ordhead.not_before_date%TYPE,
                              I_supplier              IN                ordhead.supplier%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  BUY_GET_ITEMS
--Purpose:   This function will take two buy/get items that were added to a deal, and will check for
--           any conflicts on any other deal detail lines.  A cross-check to ensure that there is no deal
--           w/ the buy item as buy item and get item as get item, or no deal w/ the buy item as get item
--           and get item as buy item must be performed.  If the user is on the current record, it should
--           not return false.
--------------------------------------------------------------------------------------------------------
FUNCTION BUY_GET_ITEMS(O_error_message     IN OUT    VARCHAR2,
                       O_overlap_exists    IN OUT    BOOLEAN,
                       I_deal_id           IN        DEAL_HEAD.DEAL_ID%TYPE,
                       I_deal_detail_id    IN        DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                       I_active_date       IN        DEAL_HEAD.ACTIVE_DATE%TYPE,
                       I_close_date        IN        DEAL_HEAD.CLOSE_DATE%TYPE,
                       I_partner_type      IN        DEAL_HEAD.PARTNER_TYPE%TYPE,
                       I_partner_id        IN        DEAL_HEAD.PARTNER_ID%TYPE,
                       I_supplier          IN        DEAL_HEAD.SUPPLIER%TYPE,
                       I_buy_item          IN        DEAL_DETAIL.QTY_THRESH_BUY_ITEM%TYPE,
                       I_get_item          IN        DEAL_DETAIL.QTY_THRESH_GET_ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  ALL_DEAL_IDS
--Purpose:   This function will check both the deal_head and fixed_deal tables for deal_ids.
--------------------------------------------------------------------------------------------------------
FUNCTION ALL_DEAL_IDS (O_error_message IN OUT VARCHAR2,
                       O_exist         IN OUT BOOLEAN,
                       O_type          IN OUT VARCHAR2,
                       I_deal_id       IN     deal_head.deal_id%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  ORDHEAD_DATES_EXIST
--Purpose:   This function will be used when calling the dealmain form from ordsku.  It is necessary
--           to validate that the user has entered a not before data and not after date on the ordhead
--           form as the date fields are required to create the deal_head record.
--------------------------------------------------------------------------------------------------------
FUNCTION ORDHEAD_DATES_EXIST (O_error_message IN OUT VARCHAR2,
                              O_exist         IN OUT BOOLEAN,
                              I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  DEAL_ORDER_EXIST
--Purpose:   This function checks if the order has a cost source equal to DEAL.
--           If the cost source on ordloc equals deal, the function will
--           return TRUE for O_exist.
--------------------------------------------------------------------------------------------------------
FUNCTION DEAL_ORDER_EXIST (O_error_message    IN OUT      VARCHAR2,
                           O_exists           IN OUT      BOOLEAN,
                           I_order_no         IN          ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  CHECK_DELETE_DETAIL_THRESH
--Purpose:   This package function will check if a deal itemloc or deal threshold records exist as
--           child records for the deal_detail record.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_DETAIL_THRESH(O_error_message    IN OUT VARCHAR2,
                                    O_detail_exists    IN OUT BOOLEAN,
                                    O_thresh_exists    IN OUT BOOLEAN,
                                    I_deal_id          IN     DEAL_HEAD.DEAL_ID%TYPE,
                                    I_deal_detail_id   IN     DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  CHECK_DELETE_ACTUALS
--Purpose:   This package function will check if a deal actauls itemloc or deal actuals forecast records
--           exist as child records for the deal_detail record.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_ACTUALS(O_error_message    IN OUT VARCHAR2,
                              O_actuals_exists   IN OUT BOOLEAN,
                              I_deal_id          IN     DEAL_HEAD.DEAL_ID%TYPE,
                              I_deal_detail_id   IN     DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  CHECK_PO_DEAL_DATES
--Purpose:   This function will insert a new active date into a record
--           on deal_head if the not-before-date on ordhead is changed and
--           the order is associated with a PO-specific deal.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_PO_DEAL_DATES (O_error_message    IN OUT      VARCHAR2,
                              I_order_no         IN          ORDHEAD.ORDER_NO%TYPE,
                              I_not_before_date  IN          ORDHEAD.NOT_BEFORE_DATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  DEAL_CALC_QUEUE_EXIST
--Purpose:   This function checks if the order has been placed on the
--           deal_calc_queue table.
--------------------------------------------------------------------------------------------------------
FUNCTION DEAL_CALC_QUEUE_EXIST(O_error_message    IN OUT      VARCHAR2,
                               O_exist            IN OUT      BOOLEAN,
                               O_approved         IN OUT      VARCHAR2,
                               I_order_no         IN          DEAL_CALC_QUEUE.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  GET_DEAL_STATUS
--Purpose:   This function checks the status of a deal on deal_head.
--------------------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_STATUS(O_error_message    IN OUT      VARCHAR2,
                         O_status           IN OUT      DEAL_HEAD.STATUS%TYPE,
                         I_order_no         IN          DEAL_HEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function:  DEAL_PROM_EXIST
--Purpose:   This function checks if the deal has attached promotions.
--           The function will return TRUE for O_exist.
--------------------------------------------------------------------------------------------------------
FUNCTION DEAL_PROM_EXIST(O_error_message    IN OUT      VARCHAR2,
                         O_exists           IN OUT      BOOLEAN,
                         I_deal_id          IN          DEAL_PROM.DEAL_ID%TYPE,
                         I_promotion        IN          DEAL_PROM.PROMOTION%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name: TRAN_COMPONENT_DATE_OVERLAP
-- Purpose: This function will verify if the passed in Deal has any transaction
--          deal components with overlapping dates for a particular supplier.
--          The Deal ID, supplier, and active date must be passed into the function.
-------------------------------------------------------------------------------
FUNCTION TRAN_COMPONENT_DATE_OVERLAP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists          IN OUT   BOOLEAN,
                                     I_active_date     IN       DEAL_HEAD.ACTIVE_DATE%TYPE,
                                     I_close_date      IN       DEAL_HEAD.CLOSE_DATE%TYPE,
                                     I_supplier        IN       DEAL_HEAD.SUPPLIER%TYPE,
                                     I_deal_id         IN       DEAL_HEAD.DEAL_ID%TYPE,
                                     I_deal_type       IN       DEAL_HEAD.TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function: TRAN_LVL_COMP_EXISTS
--Purpose:  This function will check for the existence of a transaction level deal component
--          for a given deal id.
--------------------------------------------------------------------------------------------------------
FUNCTION TRAN_LVL_COMP_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_deal_id         IN       DEAL_DETAIL.DEAL_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--Function: CHECK_DEAL_PAST_DATE
--Purpose:  This function will check the existence of an approved deal with past active date for a
--          given order no.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DEAL_PAST_DATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
 --Function:  CHECK_ORG_WH_DEAL
 --Purpose:   This package function used to check the existanc of wh in organizational hierarchy.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ORG_WH_DEAL (O_error_message    IN OUT VARCHAR2,
                            O_exists           IN OUT BOOLEAN,
                            I_loc_number       IN     DEAL_ITEMLOC.LOCATION%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
 --Function:  CHECK_ORG_STORE_DEAL
 --Purpose:   This package function will check the existence of a store in a given organizational hierarchy.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ORG_STORE_DEAL (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists           IN OUT   BOOLEAN,
                               I_org_level        IN OUT   DEAL_ITEMLOC.ORG_LEVEL%TYPE,
                               I_deal_id          IN       DEAL_HEAD.DEAL_ID%TYPE,
                               I_deal_detail_id   IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                               I_loc              IN       DEAL_ITEMLOC.LOCATION%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
 --Function:  CHECK_WF_EXISTS
 --Purpose:   This package function will check the existence of a WF location in a given organizational hierarchy.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_WF_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   BOOLEAN,
                         I_org_type        IN       NUMBER,
                         I_org_value       IN       V_STORE.CHAIN%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
 --Function:  CHECK_THRESHOLD_VALUE
 --Purpose:   This package function will check if the threshold value is valid.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_THRESHOLD_VALUE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_deal_detail_id   IN OUT   DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                               I_deal_id          IN       DEAL_HEAD.DEAL_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
END;
/
